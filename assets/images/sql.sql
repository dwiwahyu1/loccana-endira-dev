/*
SQLyog Ultimate v10.42 
MySQL - 5.6.19-log : Database - los2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`los2` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `los2`;

/*Table structure for table `u_menu` */

DROP TABLE IF EXISTS `u_menu`;

CREATE TABLE `u_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `url_mobile` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `u_menu` */

insert  into `u_menu`(`id`,`parent_id`,`url`,`url_mobile`,`label`,`status`,`icon`,`sequence`) values (1,0,'dashboard','HomePage','Home',1,'fa fa-group',NULL),(2,0,'pengajuan/list','PengajuanListPage','Pengajuan',1,'fa fa-inbox',NULL),(3,1,'pengajuan/tambah','PengajuanPage','Tambah Pengajuan',1,'fa fa-book',NULL),(4,0,'survey/checklist','SurveyListPage','Survey',1,NULL,NULL);

/*Table structure for table `u_menu_group` */

DROP TABLE IF EXISTS `u_menu_group`;

CREATE TABLE `u_menu_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `u_menu_group` */

insert  into `u_menu_group`(`id`,`group_id`,`menu_id`,`status`) values (1,22,1,1),(2,22,2,1),(3,22,3,1),(4,22,4,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
