<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto;
  }

  .scroll-overflow {
    min-height: 650px;
  }

  #modal-distributor::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
          <div class="card-box table-responsive">
            <div class="dropdown pull-right">
              <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                <i class="zmdi zmdi-more-vert"></i>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>

            <h4 class="header-title m-t-0 m-b-30">List Penerimaan Barang</h4>

            <table id="datatable_pricipal" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th width="5%">No</th>
                  <th>Nomor DO</th>
                  <th>Tanggal DO</th>
                  <th>Nomor PO</th>
                  <th>Nama Principal</th>
                  <th>Tanggal Penerimaan</th>
                  <th>Harga</th>
                  <th>Diskon</th>
                  <th>Value</th>
                  <!-- <th>Status</th> -->
                  <th width="13%">Action</th>
                </tr>
              </thead>

              <tbody style="">

              </tbody>
            </table>
          </div>
        </div><!-- end col -->
      </div>
    </div>
  </div>
  <span id="laod"></span>
</div>


<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content p-0 b-0">
      <div class="panel panel-color panel-primary panel-filled">
        <div class="panel-heading">
          <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 class="panel-title"></h3>
        </div>
        <div class="panel-body force-overflow" id="modal-distributor">
          <div class="scroll-overflow">
            <p></p>
          </div>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  function deletepo(id) {
    swal({
      title: 'Yakin akan Menghapus ?',
      text: 'data tidak dapat dikembalikan bila sudah dihapus !',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak'
    }).then(function() {
      var datapost = {
        "id": id
      };

      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'penerimaan_barang/delete_po'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
          $('.panel-heading button').trigger('click');
          listdist();
          swal({
            title: 'Success!',
            text: response.message,
            type: 'success',
            showCancelButton: false,
            confirmButtonText: 'Ok'
          }).then(function() {})
        }
      });
    });
  }

  function listdist() {
    var user_id = '0001';
    var token = '093940349';
	var filter = '<?php echo $today_month;  ?>'; 
	var filter_year = '<?php echo $today_year;  ?>'; 


    $('#datatable_pricipal').DataTable({
      //"dom": 'rtip',
      "bFilter": false,
      "aaSorting": [],
      "bLengthChange": true,
      'iDisplayLength': 10,
      "sPaginationType": "simple_numbers",
      "Info": false,
      "processing": true,
      "serverSide": true,
      "destroy": true,
      "ajax": "<?php echo base_url() . 'penerimaan_barang/lists' ?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token+ "&filter=" + filter+ "&filter_year=" + filter_year,
      "searching": true,
      "language": {
        "decimal": ",",
        "thousands": "."
      },
      "dom": 'l<"toolbar">frtip',
      "initComplete": function() {
        $("div.toolbar").prepend('<div class="btn-group pull-left"><a href="<?php echo base_url() . 'penerimaan_barang/add'; ?>" type="button" class="btn btn-custon-rounded-two btn-primary" style="display: <?php echo $priv['insert'] ?>;" data-toggle="tooltip" title="Insert"> <i class="fa fa-plus"></i> </a><?php echo $filter_year.''.$filter_month ?></div>');
      }
    });
  }
  
  function changeFilter() {

   var user_id = '0001';
    var token = '093940349';
	var filter = $('#filter_month').val(); 
	var filter_year = $('#filter_year').val(); 
	
	var sul = '';
	var sul2 = '';
	var sul1 = '';

	// if(filter == 0){
		// var sul = 'selected';
	// }else if(filter == 1){
		// var sul1 = 'selected';
	// }else{
		// var sul2 = 'selected';
	// }

	//alert(filter);
	
    $('#datatable_pricipal').DataTable({
     "bFilter": false,
      "aaSorting": [],
      "bLengthChange": true,
      'iDisplayLength': 10,
      "sPaginationType": "simple_numbers",
      "Info": false,
      "processing": true,
      "serverSide": true,
      "destroy": true,
      "ajax": "<?php echo base_url() . 'penerimaan_barang/lists' ?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token+ "&filter=" + filter+ "&filter_year=" + filter_year,
      "searching": true,
      "language": {
        "decimal": ",",
        "thousands": "."
      },
      "dom": 'l<"toolbar">frtip',
      "initComplete": function() {
        $("div.toolbar").prepend('<div class="btn-group pull-left"><a href="<?php echo base_url() . 'penerimaan_barang/add'; ?>" type="button" class="btn btn-custon-rounded-two btn-primary" style="display: <?php echo $priv['insert'] ?>;" data-toggle="tooltip" title="Insert" > <i class="fa fa-plus"></i> </a><?php echo $filter_year.''.$filter_month ?></div>');
		
		$('#filter_month').val(filter); 
		$('#filter_year').val(filter_year); 
      }
    });
	
	
	

  } 

  $(document).ready(function() {
    listdist();
  });

  function updatepo(id) {


    var url = '<?php echo base_url(); ?>penerimaan_barang/updatepo';

    var form = $("<form action='" + url + "' method='post'>" +
      "<input type='hidden' name='idpo' value='" + id + "' />" +
      "</form>");
    $('body').append(form);
    form.submit();

    //window.location.href = "<?php echo base_url() . 'penerimaan_barang/updateprincipal/'; ?>"+id;

  }  
  
  function detail(id) {


    var url = '<?php echo base_url(); ?>penerimaan_barang/detail';

    var form = $("<form action='" + url + "' method='post'>" +
      "<input type='hidden' name='idpo' value='" + id + "' />" +
      "</form>");
    $('body').append(form);
    form.submit();

    //window.location.href = "<?php echo base_url() . 'penerimaan_barang/updateprincipal/'; ?>"+id;

  }  
  
  function print_pdf(id){
	  
	   var url = '<?php echo base_url(); ?>penerimaan_barang/print_pdf';

    var form = $("<form action='" + url + "' method='post'>" +
      "<input type='hidden' name='idpr' value='" + id + "' />" +
      "</form>");
    $('body').append(form);
    form.submit();
	  
	  
  }

  
  function konfirmasi(id) {


    var url = '<?php echo base_url(); ?>penerimaan_barang/konfirmasi';

    var form = $("<form action='" + url + "' method='post'>" +
      "<input type='hidden' name='idpo' value='" + id + "' />" +
      "</form>");
    $('body').append(form);
    form.submit();

    //window.location.href = "<?php echo base_url() . 'penerimaan_barang/updateprincipal/'; ?>"+id;

  }
</script>