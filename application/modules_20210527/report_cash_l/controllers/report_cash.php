<?php

use function PHPSTORM_META\type;

defined('BASEPATH') or exit('No direct script access allowed');

class Report_cash extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('report_cash/report_cash_model', 'reportCashModel');
		$this->load->model('selling/selling_model');
		$this->load->model('invoice_selling/return_model');
		$this->load->library('log_activity');
		$this->load->library('priv');

		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong');
		$this->form_validation->set_message('min_length', '%s Minimal 4 Karakter');
		$this->form_validation->set_message('max_length', '%s Maksimal 10 Karakter');
	}

	public function index()
	{
		$coa_list = $this->reportCashModel->get_coa_list();
		$coa_list = array_filter($coa_list, function($coa){
			return $coa['id_parent'] == 3;
		});
		

		// echo '<pre>'; var_dump($coa_list); die;

		$this->template->load('maintemplate', 'report_cash/views/index', compact('coa_list'));
	}

	public function list()
	{
		$sess_user_id = !empty($_GET['sess_user_id']) ? $_GET['sess_user_id'] : NULL;
		$sess_token = !empty($_GET['sess_token']) ? $_GET['sess_token'] : NULL;

		$coa_id = !empty($_GET['coa_id']) ? $_GET['coa_id'] : NULL;
		$start_date = !empty($_GET['start_date']) ? $_GET['start_date'] : NULL;
		$end_date = !empty($_GET['end_date']) ? $_GET['end_date'] : NULL;

		$filter = !empty($_GET['filter']) ? $_GET['filter'] : NULL;
		$filter_month = !empty($_GET['filter_month']) ? $_GET['filter_month'] : NULL;
		$filter_year = !empty($_GET['filter_year']) ? $_GET['filter_year'] : NULL;
		$draw = $this->input->get_post('draw') != FALSE ? $this->input->get_post('draw') : 1;
		$length = $this->input->get_post('length') != FALSE ? $this->input->get_post('length') : 10;
		$start = $this->input->get_post('start') != FALSE ? $this->input->get_post('start') : 0;

		$order_fields = array('id_hp', 'no_payment', 'total', 'tgl_terbit', 'keterangan', 'id_hp', 'id_hp'); // , 'COST'

		$order = $this->input->get_post('order');
		$order_dir = !empty($order[0]['dir']) ? $order[0]['dir'] : 'desc';
		$order_column = !empty($order[0]['column']) ? $order[0]['column'] : 0;

		$params['limit']		= (int) $length;
		$params['offset']		= (int) $start;
		$params['order_column']	= $order_fields[$order_column];
		$params['order_dir']	= $order_dir;
		$params['sess_user_id']	= $sess_user_id;
		$params['sess_token']	= $sess_token;
		$params['filter']		= $filter;
		$params['filter_month']	= $filter_month;
		$params['filter_year']	= $filter_year;
		$params['searchtxt']	= $_GET['search']['value'] ?? null;

		$params['coa_id']		= $coa_id;
		$params['start_date']	= $start_date;
		$params['end_date']		= $end_date;

		// var_dump($params); die;

		// print_r($params);die;
		$priv = $this->priv->get_priv();
		
		if($start_date == "2021-01-01"){
		
		$list_saldo = $this->reportCashModel->list_report_cash_saldo($params);

		$list = $this->reportCashModel->list_report_cash($params);
		$typeCoa = ['Debit', 'Kredit'];
		$data = [];
		$balance = 0.00;

		foreach ($list_saldo['data'] as $row) {
			
			array_push($data, [
				'', // tanggal
				'', // tanggal
				// $typeCoa[$row['type_cash']] ?? '-', // transaksi
				'', 
				'Saldo Awal', // transaksi
				// kode akun
				'', // akun
				'', // uraian
				'-', // debit
				'-', // kredit
				indonesia_currency_format($row['saldo_awal']), // saldo
				// '-', // action
			]);
			
			
			$balance = $row['saldo_awal'];
			
		}

		foreach ($list['data'] as $row) {

			if ($row['type_cash'] == 0) {
				$balance += $row['sum_amnt'];
			} else if ($row['type_cash'] == 1) {
				$balance -= $row['sum_amnt'];
			}
			
			// if($row['kr_note'] == 'Utang Dagang' || $row['kr_note'] == 'Piutang Dagang'){ 
				// $row['banks'] = $row['keterangan']; 
			// }
			
			// if ($row['bukti'] == "Bayar Piutang") {
				// $faktur = $row['jual'];
				// $note_ac = $row['cust_name'];
			// }elseif ($row['bukti'] == "Bayar Hutang") {
				// $faktur = $row['beli'];
				// $note_ac = $row['name_eksternal'];
			// }else{
				$faktur = '';
				$note_ac = $row['k_note'].''.$row['users'];
			// }
			
			array_push($data, [
				$row['date'], // tanggal
				$row['banks'], // tanggal
				// $typeCoa[$row['type_cash']] ?? '-', // transaksi
				$row['kr_coa'], 
				$row['kr_note'], // transaksi
				// kode akun 
				$note_ac, // akun
				$row['invoice'], // uraian
				$row['type_cash'] == 0 ? indonesia_currency_format($row['sum_amnt']) : '-', // debit
				$row['type_cash'] == 1 ? indonesia_currency_format($row['sum_amnt']) : '-', // kredit
				indonesia_currency_format($balance), // saldo
				// '-', // action
			]);
		}
		
		}else{
			
			$params_awal['coa_id']		= $coa_id;
			$params_awal['start_date']	= "2021-01-01";
			$params_awal['end_date']	= date("Y-m-d", strtotime($start_date ."-1 days"));
			
		
			$list_saldo = $this->reportCashModel->list_report_cash_saldo($params_awal);

			$list_awal = $this->reportCashModel->list_report_cash($params_awal);
			
			$list = $this->reportCashModel->list_report_cash($params);
			
			//print_r($list);die;
			
			$typeCoa = ['Debit', 'Kredit'];
			$data = [];
			$balance = 0.00;

			foreach ($list_saldo['data'] as $row) {
				
				// array_push($data, [
					// '', // tanggal
					// '', // tanggal
					// // $typeCoa[$row['type_cash']] ?? '-', // transaksi
					// '', 
					// 'Saldo Awal', // transaksi
					// // kode akun
					// '', // akun
					// '', // uraian
					// '-', // debit
					// '-', // kredit
					// indonesia_currency_format($row['saldo_awal']), // saldo
					// // '-', // action
				// ]);
				
				
				$balance = $row['saldo_awal'];
				
			}
			
			foreach ($list_awal['data'] as $row) {

				if ($row['type_cash'] == 0) {
					$balance += $row['sum_amnt'];
				} else if ($row['type_cash'] == 1) {
					$balance -= $row['sum_amnt'];
				}
			}
			
			array_push($data, [
					'', // tanggal
					'', // tanggal
					// $typeCoa[$row['type_cash']] ?? '-', // transaksi
					'', 
					'Saldo Awal', // transaksi
					// kode akun
					'', // akun
					'', // uraian
					'-', // debit
					'-', // kredit
					indonesia_currency_format($balance), // saldo
					// '-', // action
				]);

			foreach ($list['data'] as $row) {

				if ($row['type_cash'] == 0) {
					$balance += $row['sum_amnt'];
				} else if ($row['type_cash'] == 1) {
					$balance -= $row['sum_amnt'];
				}
				
				// if($row['kr_note'] == 'Utang Dagang' || $row['kr_note'] == 'Piutang Dagang'){ 
					// $row['banks'] = $row['keterangan']; 
				// }
				
				// if ($row['bukti'] == "Bayar Piutang") {
					// $faktur = $row['jual'];
					// $note_ac = $row['cust_name'];
				// }elseif ($row['bukti'] == "Bayar Hutang") {
					// $faktur = $row['beli'];
					// $note_ac = $row['name_eksternal'];
				// }else{
					$faktur = '';
					$note_ac = $row['k_note'].''.$row['users'];
				// }
				
				array_push($data, [
					$row['date'], // tanggal
					$row['banks'], // tanggal
					// $typeCoa[$row['type_cash']] ?? '-', // transaksi
					$row['kr_coa'], 
					$row['kr_note'], // transaksi
					// kode akun 
					$note_ac, // akun
					$row['invoice'], // uraian
					$row['type_cash'] == 0 ? indonesia_currency_format($row['sum_amnt']) : '-', // debit
					$row['type_cash'] == 1 ? indonesia_currency_format($row['sum_amnt']) : '-', // kredit
					indonesia_currency_format($balance), // saldo
					// '-', // action
				]);
			}
		
			
		}

		$result = [
			'data' => $data,
			// 'recordsTotal' => $list['total'],
			// 'recordsFiltered' => $list['total_filtered'],
			// 'draw' => $draw,
		];

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function excel()
	{
		$params = array(
			'coa_id' => anti_sql_injection($this->input->post('coa_id')),
			'end_date' => anti_sql_injection($this->input->post('end_date')),
			'start_date' => anti_sql_injection($this->input->post('start_date')),
		);
		
		//print_r($params);die;

		$this->load->library('excel');

		$objPHPExcel = new PHPExcel();

		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'Tanggal')
			->setCellValue('B1', 'Kode')
			->setCellValue('C1', 'Akun')
			->setCellValue('D1', 'Uraian')
			->setCellValue('E1', 'No. Faktur')
			->setCellValue('F1', 'Debit')
			->setCellValue('G1', 'Kredit')
			->setCellValue('H1', 'Saldo');

		if($params['start_date'] == "2021-01-01"){

			$list = $this->reportCashModel->list_report_cash($params);
			$start = 2;
			
			$list_saldo = $this->reportCashModel->list_report_cash_saldo($params);
			//print_r($list_saldo);die;
			
			foreach ($list_saldo['data'] as $row) {
				
				
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('C' . $start, 'Saldo Awal')
					->setCellValue('H' . $start, indonesia_currency_format($row['saldo_awal'])); // saldo
				
				
				$balance = $row['saldo_awal'];
				$start++;
			}
			
			//$balance = 0.00;
			foreach ($list['data'] as $row) {
				if ($row['type_cash'] == 0) {
					$balance += $row['sum_amnt'];
				} else if ($row['type_cash'] == 1) {
					$balance -= $row['sum_amnt'];
				}
				
				// if ($row['bukti'] == "Bayar Piutang") {
					// $faktur = $row['jual'];
					// $note_ac = $row['cust_name'];
				// }elseif ($row['bukti'] == "Bayar Hutang") {
					// $faktur = $row['beli'];
					// $note_ac = $row['name_eksternal'];
				// }else{
					// $faktur = '';
					// $note_ac = $row['note_ac'];
				// }
				
					$faktur = '';
					$note_ac = $row['k_note'].''.$row['users'];

				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A' . $start, $row['date']) // tanggal
					->setCellValue('B' . $start, $row['kr_coa']) // transaksi
					->setCellValue('C' . $start, $row['kr_note']) // kode akun
					->setCellValue('D' . $start, $note_ac) // akun
					->setCellValue('E' . $start, $row['invoice']) // uraian
					->setCellValue('F' . $start, $row['type_cash'] == 0 ? number_format($row['sum_amnt'],2,',','') : '-') // debit
					->setCellValue('G' . $start, $row['type_cash'] == 1 ? number_format($row['sum_amnt'],2,',','') : '-')
					->setCellValue('h' . $start, number_format($balance,2,',',''));

				$start++;
			}
		
		}else{
			
			$params_awal['coa_id']		= $params['coa_id'];
			$params_awal['start_date']	= "2021-01-01";
			$params_awal['end_date']	= date("Y-m-d", strtotime($params['start_date'] ."-1 days"));
			
			
			$list = $this->reportCashModel->list_report_cash($params);
			$start = 2;
			
			$list_saldo = $this->reportCashModel->list_report_cash_saldo($params_awal);
			$list_awal = $this->reportCashModel->list_report_cash($params_awal);
						
			foreach ($list_saldo['data'] as $row) {
						
				$balance = $row['saldo_awal'];
				
			}
			
			foreach ($list_awal['data'] as $row) {

				if ($row['type_cash'] == 0) {
					$balance += $row['sum_amnt'];
				} else if ($row['type_cash'] == 1) {
					$balance -= $row['sum_amnt'];
				}
			}
			
			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('C' . $start, 'Saldo Awal')
			->setCellValue('H' . $start, indonesia_currency_format($balance)); // saldo
				
			$start++;
			// foreach ($list_saldo['data'] as $row) {
				
				
				// $objPHPExcel->setActiveSheetIndex(0)
					// ->setCellValue('C' . $start, 'Saldo Awal')
					// ->setCellValue('H' . $start, indonesia_currency_format($row['saldo_awal'])); // saldo
				
				
				// $balance = $row['saldo_awal'];
				// $start++;
			// }
			
			//$balance = 0.00;
			foreach ($list['data'] as $row) {
				if ($row['type_cash'] == 0) {
					$balance += $row['sum_amnt'];
				} else if ($row['type_cash'] == 1) {
					$balance -= $row['sum_amnt'];
				}
				
				// if ($row['bukti'] == "Bayar Piutang") {
					// $faktur = $row['jual'];
					// $note_ac = $row['cust_name'];
				// }elseif ($row['bukti'] == "Bayar Hutang") {
					// $faktur = $row['beli'];
					// $note_ac = $row['name_eksternal'];
				// }else{
					// $faktur = '';
					// $note_ac = $row['note_ac'];
				// }
				
					$faktur = '';
					$note_ac = $row['k_note'].''.$row['users'];

				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A' . $start, $row['date']) // tanggal
					->setCellValue('B' . $start, $row['kr_coa']) // transaksi
					->setCellValue('C' . $start, $row['kr_note']) // kode akun
					->setCellValue('D' . $start, $note_ac) // akun
					->setCellValue('E' . $start, $row['invoice']) // uraian
					->setCellValue('F' . $start, $row['type_cash'] == 0 ? number_format($row['sum_amnt'],2,',','') : '-') // debit
					->setCellValue('G' . $start, $row['type_cash'] == 1 ? number_format($row['sum_amnt'],2,',','') : '-')
					->setCellValue('h' . $start, number_format($balance,2,',',''));

				$start++;
			}
			
		}

		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename=report_cash.xls');
		header('Cache-Control: max-age=0');
		// If you’re serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you’re serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		unset($objPHPExcel);
	}
}
