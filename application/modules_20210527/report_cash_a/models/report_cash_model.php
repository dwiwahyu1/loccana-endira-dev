<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Report_cash_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function list_report_cash($params = [])
	{
		$total = $this->db
			->select('id')
			->from('t_coa_value')
			->join('t_coa', 't_coa_value.id_coa = t_coa.id_coa')
			// ->join('(SELECT `date` AS `jdate`, SUM(`value`) AS jvalue  FROM t_coa_value GROUP BY `date`) as j', 't_coa_value.date = j.jdate')
			// ->join('t_po_mat', 't_coa_value.id_coa_temp = t_po_mat.id_t_ps', 'left')
			->get()
			->result_array();

		$filteredQueryBuilder = $this->db
			->select('*')
			->from('t_coa_value')
			->join('t_coa', 't_coa_value.id_coa = t_coa.id_coa');
		// ->join('(SELECT `date` AS `jdate`, SUM(`value`) AS jvalue  FROM t_coa_value GROUP BY `date`) as j', 't_coa_value.date = j.jdate')
		// ->join('t_po_mat', 't_coa_value.id_coa_temp = t_po_mat.id_t_ps', 'left');

		if (isset($params['coa_id']) && !empty($params['coa_id'])) {
			$filteredQueryBuilder = $filteredQueryBuilder->where(['t_coa.id_coa' => $params['coa_id']]);
		}
		if (isset($params['start_date']) && !empty($params['start_date'])) {
			$filteredQueryBuilder = $filteredQueryBuilder->where(['t_coa_value.date >=' => $params['start_date']]);
		}
		if (isset($params['end_date']) && !empty($params['end_date'])) {
			$filteredQueryBuilder = $filteredQueryBuilder->where(['t_coa_value.date <=' => $params['end_date']]);
		}

		if (isset($params['searchtxt']) && !empty($params['searchtxt'])) {
			$filteredQueryBuilder = $filteredQueryBuilder->like('t_coa_value.date_insert', $params['searchtxt']);
			$filteredQueryBuilder = $filteredQueryBuilder->or_like('t_coa.coa', $params['searchtxt']);
			$filteredQueryBuilder = $filteredQueryBuilder->or_like('t_coa.keterangan', $params['searchtxt']);
			$filteredQueryBuilder = $filteredQueryBuilder->or_like('t_po_mat.remarks', $params['searchtxt']);
		}

		$filteredQueryBuilder = $filteredQueryBuilder->order_by('t_coa_value.date ASC');

		$filteredResult = $filteredQueryBuilder->get()->result_array();

		$paginatedQueryBuilder = $this->db
			->select('*')
			->from('t_coa_value')
			->join('t_coa', 't_coa_value.id_coa = t_coa.id_coa');
		// ->join('(SELECT `date` AS `jdate`, SUM(`value`) AS jvalue  FROM t_coa_value GROUP BY `date`) as j', 't_coa_value.date = j.jdate')
		// ->join('t_po_mat', 't_coa_value.id_coa_temp = t_po_mat.id_t_ps', 'left');

		if (isset($params['coa_id']) && !empty($params['coa_id'])) {
			$paginatedQueryBuilder = $paginatedQueryBuilder->where(['t_coa.id_coa' => $params['coa_id']]);
		}
		if (isset($params['start_date']) && !empty($params['start_date'])) {
			$paginatedQueryBuilder = $paginatedQueryBuilder->where(['t_coa_value.date >=' => $params['start_date']]);
		}
		if (isset($params['end_date']) && !empty($params['end_date'])) {
			$paginatedQueryBuilder = $paginatedQueryBuilder->where(['t_coa_value.date <=' => $params['end_date']]);
		}

		if (isset($params['searchtxt']) && !empty($params['searchtxt'])) {
			$paginatedQueryBuilder = $paginatedQueryBuilder->like('t_coa_value.date_insert', $params['searchtxt']);
			$paginatedQueryBuilder = $paginatedQueryBuilder->or_like('t_coa.coa', $params['searchtxt']);
			$paginatedQueryBuilder = $paginatedQueryBuilder->or_like('t_coa.keterangan', $params['searchtxt']);
			$paginatedQueryBuilder = $paginatedQueryBuilder->or_like('t_po_mat.remarks', $params['searchtxt']);
		}

		$paginatedQueryBuilder = $paginatedQueryBuilder->order_by('t_coa_value.date ASC');

		if (isset($params['limit']) && $params['limit'] >= 0 && isset($params['offset'])) {
			$paginatedQueryBuilder = $paginatedQueryBuilder->limit($params['limit'], $params['offset']);
		}

		$paginatedResult = $paginatedQueryBuilder->get()->result_array();

		return [
			'data' => $paginatedResult,
			'total_filtered' => count($filteredResult),
			'total' => count($total),
		];
	}

	/**
	 * get list of coa accounts
	 * 
	 * @return array
	 */
	public function get_coa_list()
	{
		$queryBuilder = $this->db
			->select('*')
			->from('t_coa');

		$result = $queryBuilder->get()->result_array();

		return $result;
	}
}
