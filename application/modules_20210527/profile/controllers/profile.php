<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profile extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('profile_model');
    }

    public function index()
    {
        $sessionUser = $this->session->userdata['logged_in'];
        $user = $this->profile_model->get_user_by_id($sessionUser['user_id']);

        $this->template->load('maintemplate', 'profile/views/index', ['sessionUser' => $sessionUser, 'user' => $user]);
    }

    public function update()
    {
        $this->form_validation->set_rules('password', 'Password', 'trim|min_length[3]|matches[password_confirmation]', [
            'min_length' => 'Password harus lebih atau sama dengan 3 karakter',
            'matches' => 'Konfirmasi password tidak cocok',
        ]);
        $this->form_validation->set_rules('password_confirmation', 'Password', 'trim|matches[password]', [
            'min_length' => 'Password harus lebih atau sama dengan 3 karakter',
            'matches' => 'Konfirmasi password tidak cocok',
        ]);

        if ($this->form_validation->run() == false) {
            return $this->index();
        } else {
            $nama = anti_sql_injection($this->input->post('nama', true));
            $username = anti_sql_injection($this->input->post('username', true));
            $email = anti_sql_injection($this->input->post('email', true));
            $alamat = anti_sql_injection($this->input->post('alamat', true));
            $telepon = anti_sql_injection($this->input->post('telepon', true));
            $user_id = anti_sql_injection($this->input->post('user_id', true));
            $image = anti_sql_injection($this->input->post('image', true));

            // upload profile picture
            if (isset($_FILES['new_image']['name']) && !empty($_FILES['new_image']['name'])) {

                $this->load->library('upload');
                $config = array(
                    'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/profile",
                    'upload_url' => base_url() . "uploads/profile",
                    'encrypt_name' => TRUE,
                    'overwrite' => FALSE,
                    'allowed_types' => 'jpg|jpeg|png',
                    'max_size' => '10000'
                );

                $this->upload->initialize($config);

                if ($this->upload->do_upload("new_image")) { // Success
                    // General result data
                    $result = $this->upload->data();

                    // Load resize library
                    $this->load->library('image_lib');

                    // Resizing parameters large
                    $resize = array(
                        'source_image' => $result['full_path'],
                        'new_image' => $result['full_path'],
                        'maintain_ratio' => TRUE,
                        'width' => 300,
                        'height' => 300
                    );

                    // Do resize
                    $this->image_lib->initialize($resize);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    // Add our stuff
                    $image = $result['file_name'];
                } else {
                    $pesan = $this->upload->display_errors();
                    // var_dump($pesan);
                    // die;
                    // $upload_error = strip_tags(str_replace("\n", '', $pesan));

                    // $result = array(
                    //     'success' => false,
                    //     'message' => $upload_error
                    // );
                }
            }

            // update profile param
            $data = [
                'nama' => $nama,
                'username' => $username,
                'nokontak' => $telepon,
                'email' => $email,
                'alamat' => $alamat,
                'image' => $image,
            ];

            // password check
            $password = anti_sql_injection($this->input->post('password'), true);
            if (!empty($password)) {
                $password_hash = password_hash($password, PASSWORD_BCRYPT);
                $data['password'] = $password_hash;
            }

            // var_dump($data);
            // die;

            // update profile
            $this->profile_model->update_profile($user_id, $data);

            // modify session data
            $sessionData = $this->session->userdata['logged_in'];
            $sessionData['nama'] = $nama;
            $sessionData['username'] = $username;

            if ($image) {
                $url = base_url('uploads/profile/' . $image);
                $sessionData['profile_picture'] = $url;
            }

            $this->session->set_userdata('logged_in', $sessionData);

            $this->session->set_flashdata('success', 'Profile sudah diupdate');
            // return $this->index();
            // redirect('/profile', 'refresh');

            $sessionUser = $this->session->userdata['logged_in'];
            $user = $this->profile_model->get_user_by_id($sessionUser['user_id']);

            $this->template->load('maintemplate', 'profile/views/index', ['sessionUser' => $sessionUser, 'user' => $user, 'success' => 'Profile sudah diupdate']);
        }
    }
}
