<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Profile_model extends MX_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_user_by_id($id)
    {
        $result = $this->db
            ->select('*')
            ->from('u_user')
            ->where(['id' => $id])
            ->get();

        return $result->row_array();
    }

    public function update_profile($id, $param = [])
    {
        // u_user
        $queryBuilder = $this->db;

        if (isset($param['image']) && !empty($param['image'])) {
            $queryBuilder = $queryBuilder->set('image', $param['image']);
        }
        if (isset($param['nama']) && !empty($param['nama'])) {
            $queryBuilder = $queryBuilder->set('nama', $param['nama']);
        }
        if (isset($param['email']) && !empty($param['email'])) {
            $queryBuilder = $queryBuilder->set('email', $param['email']);
        }
        if (isset($param['alamat']) && !empty($param['alamat'])) {
            $queryBuilder = $queryBuilder->set('alamat', $param['alamat']);
        }
        if (isset($param['nokontak']) && !empty($param['nokontak'])) {
            $queryBuilder = $queryBuilder->set('nokontak', $param['nokontak']);
        }

        $queryBuilder
            ->where('id', $id)
            ->update('u_user');

        // u_user_group
        $queryBuilder2 = $this->db;

        if (isset($param['username']) && !empty($param['username'])) {
            $queryBuilder2 = $queryBuilder2->set('username', $param['username']);
        }

        if (isset($param['password']) && !empty($param['password'])) {
            $queryBuilder2 = $queryBuilder2->set('password', $param['password']);
        }

        $queryBuilder2
            ->where('id_user', $id)
            ->update('u_user_group');
    }
}
