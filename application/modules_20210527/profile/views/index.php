<div class="container" style="margin-top: 4px;">
    <h1 style="margin-top: 24px; margin-left: 36px;">My Profile</h1>

    <?php if (isset($success) && !empty($success)) : ?>
        <div class="alert-success" style="display: none;"><?= $success; ?></div>
    <?php endif; ?>

    <?php if (validation_errors()) : ?>
        <div class="alert-error" style="display: none;"></div>
    <?php endif; ?>

    <form action="<?= base_url('/profile/update'); ?>" method="POST" style="margin-top: 24px; margin-bottom: 24px; margin-left: 36px;" enctype="multipart/form-data">
        <div class="form-group col-lg-12">
            <div class="image-wrapper" style="margin-top: 24px; margin-bottom: 24px;">
                <img style="width: 200px; height:200px; border-radius:50%; object-fit:cover; object-position:center;" src="<?= $sessionUser['profile_picture']; ?>" alt="profile picture">
            </div>
            <input type="hidden" name="image" value="<?= $user['image'] ?? ''; ?>" />
            <input type="file" name="new_image" />
        </div>
        <div class="form-group col-lg-6">
            <label for="name">Nama:</label>
            <input type="text" class="form-control" name="nama" id="name" value="<?= $user['nama'] ?? ''; ?>" autocomplete="off" required>
            <?= form_error('nama', '<small class="text-danger pl-3">', '</small>'); ?>
        </div>
        <div class="form-group col-lg-6">
            <label for="username">Username:</label>
            <input type="text" class="form-control" name="username" id="username" value="<?= $sessionUser['username'] ?? ''; ?>" autocomplete="off" required>
            <?= form_error('username', '<small class="text-danger pl-3">', '</small>'); ?>
        </div>
        <div class="form-group col-lg-6">
            <label for="email">Email:</label>
            <input type="email" class="form-control" name="email" id="email" value="<?= $user['email'] ?? ''; ?>" autocomplete="off">
            <?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
        </div>
        <div class="form-group col-lg-6">
            <label for="telepon">Telepon:</label>
            <input type="text" class="form-control" name="telepon" id="telepon" value="<?= $user['nokontak'] ?? ''; ?>" autocomplete="off">
            <?= form_error('telepon', '<small class="text-danger pl-3">', '</small>'); ?>
        </div>
        <div class="form-group col-lg-12">
            <label for="alamat">Alamat:</label>
            <textarea name="alamat" id="alamat" cols="30" rows="3" class="form-control" style="resize: none;"><?= $user['alamat'] ?? ''; ?></textarea>
            <!-- <input type="text" class="form-control" name="alamat" id="alamat" value="<?= $user['alamat'] ?? ''; ?>" autocomplete="off"> -->
            <?= form_error('alamat', '<small class="text-danger pl-3">', '</small>'); ?>
        </div>
        <div class="form-group col-lg-6">
            <label for="password">Password:</label>
            <input type="password" name="password" class="form-control" id="password">
            <?= form_error('password', '<small class="text-danger pl-3">', '</small>'); ?>
        </div>
        <div class="form-group col-lg-6">
            <label for="password_confirmation">Konfirmasi Password:</label>
            <input type="password" name="password_confirmation" class="form-control" id="password_confirmation">
            <?= form_error('password_confirmation', '<small class="text-danger pl-3">', '</small>'); ?>
        </div>
        <div class="form-group col-lg-12 text-right" style="margin-top: 24px;">
            <button type="submit" class="btn btn-primary">Save</button>
        </div>

        <input type="hidden" name="user_id" value="<?= $sessionUser['user_id'] ?? ''; ?>" />
    </form>
</div>

<script>
    $(document).ready(function() {

        if ($('.alert-success').length > 0) {
            swal({
                type: 'success',
                text: 'Profile telah diupdate'
            });

            // removing the confirm form submission dialog when doing browser refresh.
            if (window.history.replaceState) {
                window.history.replaceState(null, null, window.location.href);
            }
        }

        if ($('.alert-error').length > 0) {
            swal({
                type: 'error',
                text: 'Error Validasi Data'
            });
        }

    });
</script>