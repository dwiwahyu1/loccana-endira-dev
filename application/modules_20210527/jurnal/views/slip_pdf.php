<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style>
        body {
            margin-right: 100px;
            font-family: "Times New Roman", Times, serif;
        }

        .paper {
            width: 100%;
            page-break-after: always;
            /* border: 1px solid blue; */
        }

        .paper:last-child {
            page-break-after: avoid;
        }

        .subtitle {
            font-size: 20px;
        }

        .subtitle2 {
            font-size: 18px;
        }

        .content-table tr>td {
            padding-left: 5px;
            padding-right: 0px;
            padding-top: 5px;
            padding-bottom: 5px;
            font-size: smaller;
        }

        thead>tr>td {
            font-weight: bolder;
        }

        .header-table tr td {
            font-size: smaller;
        }

        .sign-placeholder {
            font-size: smaller;
        }
    </style>
</head>

<body>
    <div class="paper" style="width: 120%;margin-top:-10px">
        <div class="row" style="height: 170px;">
            <table class="header-table" table border="0.0px" style="width: 55%; float: left; border-collapse:collapse;">
                <tr>
                    <td colspan="3"><strong><span class="subtitle">PT. ENDIRA ALDA</span></strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr class="subtitle2">
                    <td>JL. Sangkuriang No.38-A</td>
                </tr>
                <tr class="subtitle2">
                    <td>Cimahi - 40511</td>
                </tr>
                <tr class="subtitle2">
                    <td>NPWP: 01.555.161.7.428.000</td>
                </tr>

            </table>

            <table class="header-table" border="0.0px" style="width: 45%; float: left; border-collapse:collapse;">
                <tr>
                    <td colspan="3"><strong><span class="subtitle">SLIP KAS / BANK MASUK</span></strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr class="subtitle2">
                    <td>Tanggal</td>
                    <td>:</td>
                    <td><?= $detail[0]['date_head'] ?? ''; ?></td>
                </tr>
                <tr class="subtitle2">
                    <td>Transaksi</td>
                    <td>:</td>
                    <td><?= '' ?? ''; ?></td>
                </tr>
                <tr class="subtitle2"> 
                    <td>Akun</td>
                    <td>:</td>
                    <td><?= $detail[0]['head_coa'] ?? ''; ?></td>
                </tr>
                <tr class="subtitle2">
                    <td>Deskripsi</td>
                    <td>:</td>
                    <td><?= $detail[0]['note_head'] ?? ''; ?></td>
                </tr>
            </table>
        </div><br>

        <div class="row" style="margin-top:10px">
            <span style="font-weight:bolder; font-size:smaller;">Pembayaran</span>
            <table class="content-table" style="width: 98%; border-collapse:collapse; font-size: 16px; ">
                <thead>
                    <tr>
                        <td style="border: 1px solid black; border-width: 1px 0px 1px 1px;">Kode Akun</td>
                        <td style="border: 1px solid black; border-width: 1px 0px;">Nama Akun</td>
                        <td style="border: 1px solid black; border-width: 1px 0px;">Uraian</td>
                        <td style="border: 1px solid black; border-width: 1px 0px 1px 0px;text-align:center">Nilai</td>
						 <td style="border: 1px solid black; border-width: 1px 1px 1px 0px;"><?= ''; ?></td>
                    </tr>
                </thead>
                <tbody>
                    <?php $total = 0; ?>
					
                    <?php $rr = 0; foreach (($detail ?? []) as $the_item) : ?>
                        <tr>
                            <td style="border: 1px solid black; border-width: 1px 0px 1px 1px;"><?= $the_item['v_coa'] ?? ''; ?></td>
                            <td style="border: 1px solid black; border-width: 1px 0px; text-align:left;"><?= $the_item['v_ket'] ?? ''; ?></td>
                            <td style="border: 1px solid black; border-width: 1px 0px; text-align:left;"><?= $the_item['note'] ?? ''; ?></td>
                            <td style="border: 1px solid black; border-width: 1px 0px 1px 0px; text-align:right; margin-right"><?= number_format($the_item['value'], 2, ',', '.') ?? ''; ?> </td>
							 <td style="border: 1px solid black; border-width: 1px 1px 1px 0px;"><?= ''; ?></td>
                        </tr>
                        <?php $total += $the_item['value']; $rr++; ?>
                    <?php endforeach;  ?>
                    <tr>
                        <td style="text-align:right; font-weight:bolder;" colspan="3">Total:</td>
                        <td style="text-align:right; font-weight:bolder;"><?= number_format($total, 2, ',', '.'); ?></td>
						 <td style="border: 1px solid black; border-width: 0px 0px 0px 0px;"><?= ''; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="row" style="margin-top:10px">
            <table style="width: 98%; border-collapse:collapse; font-size: 16px; ">
                <tr>
                    <td class="sign-placeholder" style="text-align:center;"><span>Mengetahui,</span><br><br><br><span>__________</span></td>
                    <td class="sign-placeholder" style="text-align:center;"><span>Membayar,</span><br><br><br><span>__________</span></td>
                    <td class="sign-placeholder" style="text-align:center;"><span>Menerima,</span><br><br><br><span>__________</span></td>
                    <td class="sign-placeholder" style="text-align:center;"><span>Verifikasi,</span><br><br><br><span>__________</span></td>
                </tr>
            </table>
        </div>

        <div class="row" style="margin-top: 15px;">
            <span style="font-size: smaller;">Tanggal Cetak: <?= date('d F Y'); ?></span>
        </div>
    </div><!-- paper -->
</body>

</html>