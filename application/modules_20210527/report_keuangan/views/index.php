<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto
  }

  .scroll-overflow {
    min-height: 650px
  }

  #modal-distributor::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
          <div class="card-box table-responsive">
            <div class="dropdown pull-right">
              <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                <i class="zmdi zmdi-more-vert"></i>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>

            <h4 class="header-title m-t-0 m-b-30">Sales Perbulan</h4>
			<br><br>
			<select class="btn btn-custon-rounded-two btn-primary" id='sales_perbulan_year' >
					<option value="2020" selected="selected">2020</option>
					<option value="2019" >2019</option>
					<option value="2018" >2018</option>
					<option value="2017" >2017</option>
			</select>
			<br><br>
            <table id="listpemohons" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="dt-body-center" >Row Label</th>
                  <th>Sum of Total</th>
                  <th>Sum of PPN</th>
                  <th>Sum of Total + PPN</th>
                </tr>
              </thead>
              <tbody id='body_salesp'><?php echo $sales_b; ?></tbody>
            </table>
          </div>
        </div><!-- end col -->
      </div>
	  
    </div>
  </div>
  <span id="laod"></span>
</div>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
          <div class="card-box table-responsive">
            <div class="dropdown pull-right">
              <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                <i class="zmdi zmdi-more-vert"></i>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>

            <h4 class="header-title m-t-0 m-b-30">Sales Perproduk</h4>
			<br><br>
			<select class="btn btn-custon-rounded-two btn-primary" id='sales_perproduk' >
					<option value="2020" selected="selected">2020</option>
					<option value="2019" >2019</option>
					<option value="2018" >2018</option>
					<option value="2017" >2017</option>
			</select>
			<select class="btn btn-custon-rounded-two btn-primary" id='sales_perproduk_m' >
					<option value="All" selected="selected">All</option>
					<option value="01" >January</option>
					<option value="02" >February</option>
					<option value="03" >March</option>
					<option value="04" >April</option>
					<option value="05" >May</option>
					<option value="06" >June</option>
					<option value="07" >July</option>
					<option value="08" >August</option>
					<option value="09" >September</option>
					<option value="10" >October</option>
					<option value="11" >November</option>
					<option value="12" >December</option>
					
			</select>
			<br><br>
            <table id="listpemohons2" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="dt-body-center" >Principle</th>
                  <th>Produk</th>
                  <th>Kemasan</th>
                  <th>Total Sum of Lt | Kg</th>
                  <th>Total Sum of Total</th>
                </tr>
              </thead>
              <tbody id='body_salesp'><?php echo $sales_p; ?></tbody>
            </table>
          </div>
        </div><!-- end col -->
      </div>
	  
    </div>
  </div>
  <span id="laod"></span>
</div>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
          <div class="card-box table-responsive" style="overflow-x:auto;">
            <div class="dropdown pull-right">
              <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                <i class="zmdi zmdi-more-vert"></i>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>

            <h4 class="header-title m-t-0 m-b-30">Sales Percustomer</h4>
			<br><br>
			<select class="btn btn-custon-rounded-two btn-primary" id='sales_perproduk' >
					<option value="2020" selected="selected">2020</option>
					<option value="2019" >2019</option>
					<option value="2018" >2018</option>
					<option value="2017" >2017</option>
			</select>
			<select class="btn btn-custon-rounded-two btn-primary" id='sales_perproduk_m' >
					<option value="All" selected="selected">All</option>
					<option value="01" >January</option>
					<option value="02" >February</option>
					<option value="03" >March</option>
					<option value="04" >April</option>
					<option value="05" >May</option>
					<option value="06" >June</option>
					<option value="07" >July</option>
					<option value="08" >August</option>
					<option value="09" >September</option>
					<option value="10" >October</option>
					<option value="11" >November</option>
					<option value="12" >December</option>
					
			</select>
			<br><br>
            <table id="listpemohons3aa" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th rowspan='2'>Produk</th>
                  <th rowspan='2'>Customer</th>
                  <th rowspan='2'>Kemasan</th>
                  <th colspan='2'>Januari</th>
                  <th colspan='2'>Februari</th>
                  <th colspan='2'>Maret</th>
                  <th colspan='2'>April</th>
                  <th colspan='2'>Mei</th>
                  <th colspan='2'>Juni</th>
                  <th colspan='2'>Juli</th>
                  <th colspan='2'>Agustus</th>
                  <th colspan='2'>September</th>
                  <th colspan='2'>Oktober</th>
                  <th colspan='2'>November</th>
                  <th colspan='2'>Desember</th>
				    <th rowspan='2'>Total Sum of Lt | Kg</th>
                  <th rowspan='2'>Total Sum of Total</th>
                </tr>
				<tr>
					<td>Total Sum of Lt | Kg</td>
					<td>Total Sum of Total</td>
					<td>Total Sum of Lt | Kg</td>
					<td>Total Sum of Total</td>
					<td>Total Sum of Lt | Kg</td>
					<td>Total Sum of Total</td>
					<td>Total Sum of Lt | Kg</td>
					<td>Total Sum of Total</td>
					<td>Total Sum of Lt | Kg</td>
					<td>Total Sum of Total</td>
					<td>Total Sum of Lt | Kg</td>
					<td>Total Sum of Total</td>
					<td>Total Sum of Lt | Kg</td>
					<td>Total Sum of Total</td>
					<td>Total Sum of Lt | Kg</td>
					<td>Total Sum of Total</td>
					<td>Total Sum of Lt | Kg</td>
					<td>Total Sum of Total</td>
					<td>Total Sum of Lt | Kg</td>
					<td>Total Sum of Total</td>
					<td>Total Sum of Lt | Kg</td>
					<td>Total Sum of Total</td>
					<td>Total Sum of Lt | Kg</td>
					<td>Total Sum of Total</td>
				</tr>
              </thead>
              <tbody id='body_salespc'><?php echo $sales_pc; ?></tbody>
            </table>
          </div>
        </div><!-- end col -->
      </div>
	  
    </div>
  </div>
  <span id="laod"></span>
</div>

<!--
<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
          <div class="card-box table-responsive">
            <div class="dropdown pull-right">
              <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                <i class="zmdi zmdi-more-vert"></i>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>

            <h4 class="header-title m-t-0 m-b-30">Jurnal</h4>

            <table id="listpemohon" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="dt-body-center" style="width: 5%;">No</th>
                  <th>Coa</th>
                  <th>Tipe</th>
                  <th>Total</th>
                  <th>Keterangan</th>
                  <th style="width: 10%;">Option</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div><!-- end col
      </div> -->
	  
	  <!--
	    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
          <div class="card-box table-responsive">
            <div class="dropdown pull-right">
              <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                <i class="zmdi zmdi-more-vert"></i>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>

            <h4 class="header-title m-t-0 m-b-30">Jurnal</h4>

            <table id="listpemohon" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="dt-body-center" style="width: 5%;">No</th>
                  <th>Coa</th>
                  <th>Tipe</th>
                  <th>Total</th>
                  <th>Keterangan</th>
                  <th style="width: 10%;">Option</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div><!-- end col 
      </div>-->
	  
    </div>
  </div>
  <span id="laod"></span>
</div>




<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content p-0 b-0">
      <div class="panel panel-color panel-primary panel-filled">
        <div class="panel-heading">
          <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 class="panel-title"></h3>
        </div>
        <div class="panel-body">
          <p></p>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
  function add_user() {
    $('#panel-modal').removeData('bs.modal');
    $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    $('#panel-modal  .panel-body').load('<?php echo base_url('report_keuangan/add'); ?>');
    $('#panel-modal  .panel-title').html('<i class="fa fa-plus"></i> Add Uom');
    $('#panel-modal').modal({
      backdrop: 'static',
      keyboard: false
    }, 'show');
  }

  function edituom(id) {
    var url = '<?php echo base_url(); ?>report_keuangan/edit';

    $("#laod").append(' <img id="loading" src="<?php echo base_url(); ?>assets/urate-frontend-master/assets/images/icon_loader.gif">');
    var form = $("<form action='" + url + "' method='post'>" +
      "<input type='hidden' name='iduom' value='" + id + "' />" +
      "</form>");
    $('body').append(form);
    form.submit();
  }

  function deleteuom(id) {
    swal({
      title: 'Yakin akan Menghapus ?',
      text: 'data tidak dapat dikembalikan bila sudah dihapus !',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak'
    }).then(function() {
      var datapost = {
        "id": id
      };
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>report_keuangan/deletes",
        data: JSON.stringify(datapost),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function(response) {

          swal({
            title: 'Success!',
            text: response.message,
            type: 'success',
            showCancelButton: false,
            confirmButtonText: 'Ok'
          }).then(function() {
            window.location.href = "<?php echo base_url('jurnal'); ?>";
          })

          if (response.status == "success") {

          } else {
            swal("Failed!", response.message, "error");
          }
        }
      });
    })

  }

  $(document).ready(function() {
	  
	    $('#listpemohons2').DataTable();
	    $('#listpemohons3aa').DataTable();
	  
    $("#listpemohon").dataTable({
      "processing": true,
      "serverSide": true,
      "ajax": "<?php echo base_url() . 'report_keuangan/lists/'; ?>",
      "searchDelay": 700,
      "responsive": true,
      "lengthChange": false,
      "info": false,
      "bSort": false,
      "dom": 'l<"toolbar">frtip',
      "initComplete": function() {
        $("div.toolbar").prepend('<div class="btn-group pull-left"><a href="<?php echo base_url() . 'report_keuangan/add'; ?>" type="button" class="btn btn-custon-rounded-two btn-primary" style="display: <?php echo $priv['insert'] ?>;" > Tambah </a></div>');
      },
      "columnDefs": [{
        targets: [0],
        className: 'dt-body-center'
      }]
    });
  });
</script>