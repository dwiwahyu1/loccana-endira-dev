<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto;
  }

  .scroll-overflow {
    min-height: 650px;
  }

  #modal-distributor::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">

      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
          <div class="card-box ">
            <h4 class="header-title">Laba Rugi</h4>
            <div class="row">
			<div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
              <input class="form-control" type="text" name="start_time" id="start_time" value="<?php echo date('Y-m-d'); ?>" />
			  </div>
              <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                <input class="form-control" type="text" name="end_time" id="end_time" value="<?php echo date('Y-m-d'); ?>" />
              </div>
              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                <a href="#" type="button" class="btn btn-custon-rounded-two btn-primary" onClick="filter()" id="btn_filter">Filter</a>
                <a href="#" type="button" class="btn btn-custon-rounded-two btn-primary" onClick="export_excel()" id="btn_export">Export</a>
              </div>
            </div>
          </div>
        </div><!-- end col -->
      </div> <br> <br>

      <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12" style="margin-top:10px">
        <div class="sparkline13-list">
          <div class="card-box table-responsive">
            <div class="dropdown pull-right">
              <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                <i class="zmdi zmdi-more-vert"></i>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>

            <h4 class="header-title"></h4>

            <table id="listpemohons" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="dt-body-center">COA</th>
                  <th>Keterangan</th>
                  <th>Debit</th>
                  <th>Credit</th>
                </tr>
              </thead>
              <tbody id='body_salesa'>
                <?php
                $tot_a = 0;
                $tot_b = 0;
                $html_a = '';
                foreach ($get_aktiva as $get_aktivas) {
                  if ($get_aktivas['type_data'] == 0) {


                    $html_a = ' <tr>
								  <th >' . $get_aktivas['coa'] . '</th>
								  <th>' . $get_aktivas['keterangan'] . '</th>
								  <th>' . number_format($get_aktivas['nilai'], 2, ',', '.') . '</th>
								  <th>0.00</th>
								</tr>';


                    $tot_a += $get_aktivas['nilai'];
                  } else {

                    $html_a = ' <tr>
							  <th >' . $get_aktivas['coa'] . '</th>
							  <th>' . $get_aktivas['keterangan'] . '</th>
							  <th>0.00</th>
							  <th>' . number_format($get_aktivas['nilai'], 2, ',', '.') . '</th>
							</tr>';

                    $tot_b += $get_aktivas['nilai'];
                  }
                  echo $html_a;
                }

                ?>
                <tr>
                  <th colspan=2>Total</th>
                  <th><?php echo number_format($tot_a, 2, '.', ',') ?></th>
                  <th><?php echo number_format($tot_b, 2, '.', ',') ?></th>
                </tr>
                <tr>
                  <th colspan=3>Laba / Rugi</th>
                  <th><?php echo number_format($tot_b - $tot_a, 2, '.', ',') ?></th>
                </tr>
              </tbody>
            </table>
          </div>
        </div><!-- end col -->
      </div>

      <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12" style="margin-top:10px">
        <div class="sparkline13-list">
          <div class="card-box table-responsive">
            <div class="dropdown pull-right">
              <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                <i class="zmdi zmdi-more-vert"></i>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>

            <h4 class="header-title"></h4>

            <table id="listpemohons" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>

              </thead>
              <tbody id='body_salesb'>
                <!--<tr><th>PENJUALAN BERSIH</th><th>0.00</th></tr>
				<tr><th>BONUS PRINCIPLE</th><th>0.00</th></tr>
				<tr><th>HARGA POKOK PENJUALAN</th><th>0.00</th></tr>
				<tr><th><STRONG>LABA KOTOR</STRONG></th><th>0.00</th></tr>
				<tr><th>BIAYA PENJUALAN</th><th>0.00</th></tr>
				<tr><th>BIAYA ADMINISTRASI dan UMUM</th><th>0.00</th></tr>
				<tr><th>BIAYA PENYUSUTAN</th><th>0.00</th></tr>
				<tr><th><STRONG>BIAYA OPERASIONAL</STRONG></th><th>0.00</th></tr>
				<tr><th><STRONG>LABA (RUGI) OPERASI</STRONG></th><th>0.00</th></tr>
				<tr><th>PENDAPATAN NON OPERASI</th><th>0.00</th></tr>
				<tr><th>BIAYA NON OPERASI</th><th>0.00</th></tr>
				<tr><th><STRONG>LABA / RUGI BERSIH SEBELUM PAJAK</STRONG></th><th>0.00</th></tr>-->
                <?php echo $t_pa; ?>
              </tbody>
            </table>
          </div>
        </div><!-- end col -->
      </div>

    </div>
  </div>
  <span id="laod"></span>
</div>



<!--
<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
          <div class="card-box table-responsive">
            <div class="dropdown pull-right">
              <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                <i class="zmdi zmdi-more-vert"></i>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>

            <h4 class="header-title m-t-0 m-b-30">Jurnal</h4>

            <table id="listpemohon" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="dt-body-center" style="width: 5%;">No</th>
                  <th>Coa</th>
                  <th>Tipe</th>
                  <th>Total</th>
                  <th>Keterangan</th>
                  <th style="width: 10%;">Option</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div><!-- end col
      </div> -->

<!--
	    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
          <div class="card-box table-responsive">
            <div class="dropdown pull-right">
              <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                <i class="zmdi zmdi-more-vert"></i>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>

            <h4 class="header-title m-t-0 m-b-30">Jurnal</h4>

            <table id="listpemohon" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="dt-body-center" style="width: 5%;">No</th>
                  <th>Coa</th>
                  <th>Tipe</th>
                  <th>Total</th>
                  <th>Keterangan</th>
                  <th style="width: 10%;">Option</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div><!-- end col 
      </div>-->

</div>
</div>
<span id="laod"></span>
</div>




<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content p-0 b-0">
      <div class="panel panel-color panel-primary panel-filled">
        <div class="panel-heading">
          <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 class="panel-title"></h3>
        </div>
        <div class="panel-body">
          <p></p>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
  function add_user() {
    $('#panel-modal').removeData('bs.modal');
    $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    $('#panel-modal  .panel-body').load('<?php echo base_url('laba_rugi/add'); ?>');
    $('#panel-modal  .panel-title').html('<i class="fa fa-plus"></i> Add Uom');
    $('#panel-modal').modal({
      backdrop: 'static',
      keyboard: false
    }, 'show');
  }

  function export_excel() {
    var form = document.getElementById('export-excel');

    if (!form) {
      form = document.createElement('form');
      document.body.append(form);
    }

  //  var dateFilter = document.getElementById('end_time');

    //if (!dateFilter) {
      dateFilter = document.createElement('input');
      dateFilter.setAttribute('id', 'start_time');
      dateFilter.setAttribute('name', 'start_time');
      dateFilter.setAttribute('type', 'hidden');
      form.appendChild(dateFilter);
	  
	  dateFilter2 = document.createElement('input');
      dateFilter2.setAttribute('id', 'end_time');
      dateFilter2.setAttribute('name', 'end_time');
      dateFilter2.setAttribute('type', 'hidden');
      form.appendChild(dateFilter2);
  //  }

    dateFilter.value = document.getElementById('start_time').value;
    dateFilter2.value = document.getElementById('end_time').value;

    form.setAttribute('id', 'export-excel');
    form.setAttribute('method', 'POST');
    form.setAttribute('action', '<?= base_url('laba_rugi/export'); ?>');

    form.submit();
  }


  function filter() {



    var datapost = {
      "start_time": $('#start_time').val(),
      "end_time": $('#end_time').val()
    };

    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>laba_rugi/filter",
      data: JSON.stringify(datapost),
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      success: function(response) {
        $('#body_salesa').html('');
        $('#body_salesa').html(response.t_ac);

        $('#body_salesb').html('');
        $('#body_salesb').html(response.t_pa);


      }
    });

  }

  function edituom(id) {
    var url = '<?php echo base_url(); ?>laba_rugi/edit';

    $("#laod").append(' <img id="loading" src="<?php echo base_url(); ?>assets/urate-frontend-master/assets/images/icon_loader.gif">');
    var form = $("<form action='" + url + "' method='post'>" +
      "<input type='hidden' name='iduom' value='" + id + "' />" +
      "</form>");
    $('body').append(form);
    form.submit();
  }

  function deleteuom(id) {
    swal({
      title: 'Yakin akan Menghapus ?',
      text: 'data tidak dapat dikembalikan bila sudah dihapus !',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak'
    }).then(function() {
      var datapost = {
        "id": id
      };
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>laba_rugi/deletes",
        data: JSON.stringify(datapost),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function(response) {

          swal({
            title: 'Success!',
            text: response.message,
            type: 'success',
            showCancelButton: false,
            confirmButtonText: 'Ok'
          }).then(function() {
            window.location.href = "<?php echo base_url('jurnal'); ?>";
          })

          if (response.status == "success") {

          } else {
            swal("Failed!", response.message, "error");
          }
        }
      });
    })

  }

  $(document).ready(function() {

    // $('#listpemohons2').DataTable();
    // $('#listpemohons3aa').DataTable();

    $('#start_time').datepicker({
      isRTL: true,
      format: "yyyy-mm-dd",
      autoclose: true,
      todayHighlight: true,
      changeYear: true,
    });

    $('#end_time').datepicker({
      isRTL: true,
      format: "yyyy-mm-dd",
      autoclose: true,
      todayHighlight: true,
      changeYear: true,
    });

    // $("#listpemohon").dataTable({
    // "processing": true,
    // "serverSide": true,
    // "ajax": "<?php echo base_url() . 'laba_rugi/lists/'; ?>",
    // "searchDelay": 700,
    // "responsive": true,
    // "lengthChange": false,
    // "info": false,
    // "bSort": false,
    // "dom": 'l<"toolbar">frtip',
    // "initComplete": function() {
    // $("div.toolbar").prepend('<div class="btn-group pull-left"><a href="<?php echo base_url() . 'laba_rugi/add'; ?>" type="button" class="btn btn-custon-rounded-two btn-primary" style="display: <?php echo $priv['insert'] ?>;" > Tambah </a></div>');
    // },
    // "columnDefs": [{
    // targets: [0],
    // className: 'dt-body-center'
    // }]
    // });
  });
</script>