<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Selling_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}


	public function list_penjualan($params = array()){
		
			$query = 	'
				SELECT COUNT(*) AS jumlah 
				FROM t_payment_hp a 
				
			';
						
			$query2 = 	'
				SELECT z.*, rank() over ( ORDER BY tgl_terbit DESC) AS Rangking from ( 
					SELECT a.*
					FROM t_payment_hp a
				) z
				ORDER BY tgl_terbit DESC
				
				LIMIT '.$params['limit'].' 
				OFFSET '.$params['offset'].' 
			';
		
			//echo $query2;die;
		
			 $out		= array();
			  $querys		= $this->db->query($query);
			  $result = $querys->row();
			  
			  $total_filtered = $result->jumlah;
			  $total 			= $result->jumlah;
		  
				if(($params['offset']+10) > $total_filtered){
				$limit_data = $total_filtered - $params['offset'];
			  }else{
				$limit_data = $params['limit'] ;
			  }
		  
		
		  
		  //echo $query;die;
			//echo $query;die;
			 $query2s		= $this->db->query($query2);
		  $result2 = $query2s->result_array();						
		  $return = array(
			  'data' => $result2,
			  'total_filtered' => $total_filtered,
			  'total' => $total,
		  );
		  return $return;
		
		
	}
	
	
	public function get_customer($params = array()){
		
		//$query = $this->db->get_where('t_customer', array('region' => $params['region']));
		
		$this->db->select('*')
		 ->from('t_customer')
		 ->where(array('region' => $params['region']))
		 ->order_by('cust_name', 'asc');
		 $query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}			
	
	public function get_price_mat($params = array()){
		
		// $query = $this->db->get_where('t_po_mat', array('id_po' => $params['kode']));
		// $query = $this->db->order_by('id_t_ps', 'desc');
		
		$this->db->select('*')
		 ->from('t_po_mat')
		 ->where(array('id_material' => $params['kode']))
		 ->order_by('id_t_ps', 'desc');
		 $query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}		
	
	public function get_customer_byid($params = array()){
		
		$query = $this->db->get_where('t_customer', array('id_t_cust' => $params['id_t_cust']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
		
		
	public function get_item_byprin($params = array()){
		$this->db->select('m_material.*, m_uom.uom_symbol')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('m_material')
        ->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
		->where(array('dist_id' => $params['id_prin']));
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function get_item_byprin_all(){
		$this->db->select('m_material.*, m_uom.uom_symbol')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('m_material')
		->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
		->where(array('status' => 0))
		->order_by('stock_name','asc');
		//->where(array('dist_id' => $params['id_prin']));
		// ->where('m_material.dist_id != ',$params['id_prin'],FALSE);
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function get_kode($params = array()){
		
		$query =  $this->db->select('seq_max AS `max`')
		->from('t_ordering')
		->where(array('nama_menu' => 'pembayaran'));
		$query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function get_penjualan($params = array()){
		
		// $query = $this->db->get_where('t_purchase_order', array('id_po' => $params['id']));
		$this->db->select('t_penjualan.*, t_customer.*')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('t_penjualan')
        ->join('t_customer', 't_penjualan.id_customer = t_customer.id_t_cust')
		->where(array('id_penjualan' => $params['id']));
		$query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_penjualan_detail($params = array()){
		
		$this->db->select('d_penjualan.*, m_material.qty as kkk')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('d_penjualan')
        ->join('m_material', 'd_penjualan.id_material = m_material.id_mat')
		->where(array('id_penjualan' => $params['id']));
		
		//$query = $this->db->get_where('d_penjualan', array('id_penjualan' => $params['id']));
		$query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
		
	public function get_po_detail_full($params = array()){
		
		//$query = $this->db->get_where('t_po_mat', array('id_po' => $params['id']));
		$this->db->select('t_po_mat.*,t_po_mat.qty as qty_order, m_material.*, m_uom.*')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('t_po_mat')
        ->join('m_material', 't_po_mat.id_material = m_material.id_mat')
        ->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
		->where(array('id_po' => $params['id']));
		$query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function check_principal($params = array()){
		
		$query = $this->db->get_where('t_eksternal', array('kode_eksternal' => $params['kode']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_penjualan($data) {
	
		$datas = array(
			'id_sales' => $data['id_sales'],
			'no_penjualan' => $data['no_penjualan'],
			'date_penjualan' => $data['date_penjualan'],
			'id_customer' => $data['id_customer'],
			'term_of_payment' => $data['term_of_payment'],
			'total_amount' => $data['total_amount'],
			'status' => 0,
			'id_valas' =>1,
			'rate' => 1,
			'ppn' => 10,
			'seq_n' => $data['seq_n'],
			'keterangan' => $data['keterangan']
		);

	//	print_r($datas);die;

		$this->db->insert('t_penjualan',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}	
	
	public function add_mutasi($data) {
	
	  $data_insert_mutasi = array(
        'id_stock_awal'  => $data['id_material'],
        'id_stock_akhir' => $data['id_material'],
        'date_mutasi'    => date('Y-m-d H:i:s'),
        'amount_mutasi'  => $data['qty'],
        'type_mutasi'    => 0,
        'user_id'    => $this->session->userdata['logged_in']['user_id'],
      );

	//	print_r($datas);die;

		$this->db->insert('t_mutasi',$data_insert_mutasi);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function add_detail_penjualan($data,$id_mutasi) {
		
		$datas = array(
			'id_penjualan' => $data['id_penjualan'],
			'id_sales' => $data['id_sales'],
			'id_material' => $data['id_material'],
			'unit_price' => $data['unit_price'],
			'qty' => $data['qty'],
			'qty_box' => $data['qty_box'],
			'qty_satuan' => $data['qty_satuan'],
			'box_ammount' => $data['box_ammount'],
			'price' => $data['price'],
			'diskon' => 0,
			'status' => 0,
			'mutasi_id' => $id_mutasi['lastid']
			
		);

	//	print_r($datas);die;

		$this->db->insert('d_penjualan',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function edit_qty($data) {
		$sql 	= 'update m_material set qty = qty - '.$data['qty'].' 
		where id_mat = '.$data['id_material'].' ';

		// $datas = array(
			// 'qty' => $data['qty_stock']
		// );

	// //	print_r($datas);die;
		// $this->db->where('id_mat',$data['id_material'], false);
		// $this->db->update('m_material',$datas, false);

			
		$query 	= $this->db->query($sql);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}	
	
	public function update_qty_m($data) {
		$sql 	= 'update m_material set qty = qty + '.$data['qty'].' 
		where id_mat = '.$data['id_mat'].' ';
		
		//echo $sql;die;
		// $datas = array(
			// 'qty' => $data['qty_stock']
		// );

	//	print_r($datas);die;
		// $this->db->where('id_mat',$data['id_material'], false);
		// $this->db->update('m_material',$datas, false);

			
		$query 	= $this->db->query($sql);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function edit_credit($data) {
		$sql 	= "
		update t_customer set sisa_credit = sisa_credit - ".$data['total_amount']." where id_t_cust = ".$data['id_customer']."
		";
		
		// $total = $data['sisa'] - $data['total_amount'];
		// $datas = array(
			// 'sisa_credit' => $total
		// );

	//	print_r($datas);die;
		// $this->db->where('id_t_cust',$data['id_customer'], false);
		// $this->db->update('t_customer',$datas, false);

			
		$query 	= $this->db->query($sql);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}	
	
	public function edit_credit2($data,$selling_data) {
		$sql 	= 'update t_customer set sisa_credit = sisa_credit + '.floatval($selling_data['total_amount']).'
		where id_t_cust = '.$data['id_customer'];
		$query 	= $this->db->query($sql);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function edit_penjualan($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			
			'id_sales' => $data['id_sales'],
			'date_penjualan' => $data['date_penjualan'],
			'id_customer' => $data['id_customer'],
			'term_of_payment' => $data['term_of_payment'],
			'total_amount' => $data['total_amount'],
			'status' => 0,
			'id_valas' => 1,
			'rate' => 1,
			'ppn' => 10,
			'keterangan' => $data['keterangan']
			
		);

	//	print_r($datas);die;
		$this->db->where('id_penjualan',$data['id']);
		$this->db->update('t_penjualan',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function edit_detail_penjualan($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'id_penjualan' => $data['id_penjualan'],
			'id_sales' => $data['id_sales'],
			'id_material' => $data['id_material'],
			'unit_price' => $data['unit_price'],
			'qty' => $data['qty'],
			'price' => $data['price'],
			'diskon' => $data['diskon'],
			'status' => $data['status'],
			'approval_date' => $data['approval_date'],
			'notes' => $data['notes'],
			'qty_diterima' => $data['qty_diterima']
		);

	//	print_r($datas);die;
		$this->db->where('id_dp',$data['id_dp']);
		$this->db->update('t_detail_penjualan',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	function delete_penjualan($id) {

		$this->db->where('id_penjualan', $id);
		$this->db->delete('t_penjualan'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		

		return $result;
	}	
	
	function delete_detail_penjualan($id) {

		$this->db->where('id_penjualan', $id);
		$this->db->delete('d_penjualan'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		

		return $result;
	}	
	
	function delete_mutasi($data_q) {

		$this->db->where('id_mutasi', $data_q['mutasi_id']);
		$this->db->delete('t_mutasi'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		

		return $result;
	}

}