<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Jurnal_luar extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('jurnal_luar/uom_model');
	$this->load->library('log_activity');
		$this->load->library('priv');
  }

  /**
   * anti sql injection
   */
  public function Anti_sql_injection($string)
  {
    $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
    return $string;
  }

  public function index()
  {
	  $year_count = date('Y') - 2019;
	  
	  $filter_year = '<select class="form-control" id="filter_year" onChange="changeFilter()"><option value="ALL" >ALL</option>';
		
		for($i=$year_count;$i>=0;$i--){
			
			$y = 2019+$i;
			
			if($y == date('Y')){
				$filter_year .= '<option value="'.$y.'" selected="selected" >'.$y.'</option>';
			}else{
				$filter_year .= '<option value="'.$y.'" >'.$y.'</option>';
			}
			
			
		}
		$filter_year .= '</select>';
		
		$array_month = array('January','February','March','April','May','June','July','August','September','October','November','December');
		
		$filter_month = '<select class="form-control" id="filter_month" onChange="changeFilter()"><option value="ALL" >ALL</option>';
		foreach($array_month as $array_months){
			if($array_months == date('F')){
				$filter_month .= '<option value="'.$array_months.'" selected="selected">'.$array_months.'</option>';
			}else{
				$filter_month .= '<option value="'.$array_months.'" >'.$array_months.'</option>';
			}
		}
		
		$filter_month .= '</select>';
		$today_month = date('F');
		$today_year = date('Y');
	  
	  $priv = $this->priv->get_priv();

		$data = array(
			'priv' => $priv,
			'filter_month' => $filter_month,
			'filter_year' => $filter_year,
			'today_month' => $today_month,
			'today_year' => $today_year
		);
	  
    $this->template->load('maintemplate', 'jurnal_luar/views/index',$data);
  }

  function lists()
  {

    if( !empty($_GET['sess_user_id']) ) {
			  $sess_user_id = $_GET['sess_user_id'];
		  } else {
			  $sess_user_id = NULL;
		  }
		  
		   if( !empty($_GET['sess_token']) ) {
			  $sess_token = $_GET['sess_token'];
		  } else {
			  $sess_token = NULL;
		  }
		  
		  if( !empty($_GET['filter_month']) ) {
			  $filter_month = $_GET['filter_month'];
		  } else {
			  $filter_month = NULL;
		  }
		  
		  if( !empty($_GET['filter_year']) ) {
			  $filter_year = $_GET['filter_year'];
		  } else {
			  $filter_year = NULL;
		  }
		  
		    if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		  if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		  if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 				
		  $order_fields = array('coa'); // , 'COST'
		  $order = $this->input->get_post('order');
		  if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'desc';}; 
		  if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 	
		  
		    $params['limit'] 		= (int) $length;
			$params['offset'] 		= (int) $start;
			$params['order_column'] = $order_fields[$order_column];
			$params['order_dir'] 	= $order_dir;
			$params['sess_user_id'] = $sess_user_id;
			$params['sess_token'] 	= $sess_token;
			$params['filter_month'] 		= $_GET['filter_month'];
			$params['filter_year'] 		= $_GET['filter_year'];
			$params['searchtxt'] 	= $_GET['search']['value'];
		  

    //print_r($params);die;

    $list = $this->uom_model->lists($params);
	// echo '<pre>'; print_r($list); die;
	$priv = $this->priv->get_priv();
    //print_r($list);die;

    $result["recordsTotal"] = $list['total'];
    $result["recordsFiltered"] = $list['total_filtered'];
    $result["draw"] = $draw;

    //print_r($result);die;

    $data = array();
    $i = $params['offset'];
    $username = $this->session->userdata['logged_in']['username'];
    foreach ($list['data'] as $k => $v) {
      $i = $i + 1;


      $status_akses = '
                <div class="btn-group" style="display:'.$priv['update'].'"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edituom(\'' . $v['id'] . '\')"><i class="fa fa-edit"></i></button></div>
				  <div class="btn-group" style="display:'.$priv['delete'].'"><button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteuom(\'' . $v['id'] . '\')"><i class="fa fa-trash"></i></button></div>
				  <div class="btn-group" style="display:'.$priv['detail'].'"><button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="detail(\'' . $v['id'] . '\')"><i class="fa fa-search-plus"></i></button></div>';
				  
				  if($v['type_cash'] == 0){
					  $sss = 'Pemasukan';
				  }else{
					  $sss = 'Pengeluaran';
				  }

      array_push($data, array(
        $i,
        $v['coa'].'-'.$v['keterangan'],
		$v['coa2'] . '-' . $v['ket2'],
		$v['date'],
        number_format($v['value_real'],2,',','.'),
        $v['note'],
        $status_akses
      ));
    }

    $result["data"] = $data;

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function add()
  {
	  
	  
    $coa = $this->uom_model->get_cash_acc();
    $coa_p = $this->uom_model->get_cash_acc_cr();
    $principle = $this->uom_model->get_principle();



    // $this->load->view('add_modal_view', $data);

    // $result = $this->distributor_model->location();

    $data = array(
      'group' => '',
      'coa' => $coa,
      'coa_p' => $coa_p,
	  'principle' => $principle
    );

    $this->template->load('maintemplate', 'jurnal_luar/views/add_modal_view', $data);
  }

  public function edit()
  {
    //$id = $this->Anti_sql_injection($this->input->post('sid', TRUE));
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('iduom', TRUE)),
		);
   
    $coa = $this->uom_model->get_cash_acc();
    $coa_p = $this->uom_model->get_cash_acc_cr();
    $principle = $this->uom_model->get_principle();
	
	$detail = $this->uom_model->get_detail_2($data);
// print_r($detail);die;
   // print_r($detail);die;
    // $roles = $this->uom_model->roles($id);

    $data = array(
      'id' => $this->Anti_sql_injection($this->input->post('iduom', TRUE)),
	   'group' => '',
      'coa' => $coa,
      'coa_p' => $coa_p,
      'detail' => $detail,
	  'principle' => $principle
    );
	

    $this->template->load('maintemplate', 'jurnal_luar/views/edit_modal_view', $data);
  }  
  
  public function detail()
  {
    //$id = $this->Anti_sql_injection($this->input->post('sid', TRUE));
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('iduom', TRUE)),
		);
   
    $coa = $this->uom_model->get_cash_acc();
    $coa_p = $this->uom_model->get_cash_acc_cr();
    $principle = $this->uom_model->get_principle();
	
	$detail = $this->uom_model->get_detail($data);

   // print_r($detail);die;
    // $roles = $this->uom_model->roles($id);

    $data = array(
      'id' => $this->Anti_sql_injection($this->input->post('iduom', TRUE)),
	   'group' => '',
      'coa' => $coa,
      'coa_p' => $coa_p,
      'detail' => $detail,
	  'principle' => $principle
    );
	

    $this->template->load('maintemplate', 'jurnal_luar/views/detailPo', $data);
  }

  public function deletes()
  {

    $data   = file_get_contents("php://input");
    $params   = json_decode($data, true);

    $list = $this->uom_model->delete_detail($this->Anti_sql_injection($params['id']));
    $list = $this->uom_model->deletes($this->Anti_sql_injection($params['id']));

    $res = array(
      'status' => 'success',
      'message' => 'Data telah di hapus'
    );

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }

  public function edit_uom()
  {
    $this->form_validation->set_rules('coa', 'coa', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('tgl', 'tgl', 'trim|required|max_length[255]');
    //$this->form_validation->set_rules('cash', 'cash', 'trim|required|max_length[255]');
    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));
      $result = array(
        'success' => false,
        'message' => $msg
      );

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      // die;
      $message = "";
	  
	  
	  $int_val = $this->Anti_sql_injection($this->input->post('int_val', TRUE));
	  $id = $this->Anti_sql_injection($this->input->post('id', TRUE));
			
			$array_items = [];
			
			$total_amount = 0;
			for($i=0;$i<=$int_val;$i++){
				
				//if(isset($this->input->post('kode_'.$i, TRUE))){
					$array_items[$i]['coa_p'] = $this->Anti_sql_injection($this->input->post('coa_p_'.$i, TRUE));
					$array_items[$i]['jumlahp'] = floatval(str_replace(',','.',str_replace('.','',$this->Anti_sql_injection($this->input->post('jumlahp_'.$i, TRUE)))));
					$array_items[$i]['ket_p'] = $this->Anti_sql_injection($this->input->post('ket_p_'.$i, TRUE));
					$array_items[$i]['dist_p'] = $this->Anti_sql_injection($this->input->post('dist_p_'.$i, TRUE));


			}
			
			//print_r($array_items);die; 
	  
	  // if($this->Anti_sql_injection($this->input->post('coa', TRUE)) <> '122'  ){
		  
		  $this->uom_model->delete_detail($id);
		 $this->uom_model->deletes($id);
		 
		  
		  
		  $data = array(
			'user_id' => $this->session->userdata['logged_in']['user_id'],
			'coa'     => $this->Anti_sql_injection($this->input->post('coa', TRUE)),
			'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
			//'prin' => $this->Anti_sql_injection($this->input->post('prin', TRUE)),
			'jumlah' => floatval(str_replace(',','.',str_replace('.','',$this->Anti_sql_injection($this->input->post('jumlah', TRUE))))),
			'pph' => 0,
			'type_cash' => 1,
			'parent' => 0,
			'id_coa_temp' => 0,
			'ket' => $this->Anti_sql_injection($this->input->post('ket', TRUE))
		  );
		  
		  $resultsss = $this->uom_model->add_uom($data);

				foreach($array_items as $array_itemss){
					
					if($array_itemss['coa_p'] == ""){
						
					}else{
					
						if( $array_itemss['dist_p'] == ''){
							$sss = 0;
						}else{
							$sss = $array_itemss['dist_p'];
						}
						
						$datas = array(
						 
							'user_id' => $this->session->userdata['logged_in']['user_id'],
							'coa'     => $array_itemss['coa_p'],
							'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
							//'prin' => $this->Anti_sql_injection($this->input->post('prin', TRUE)),
							'jumlah' => $array_itemss['jumlahp'],
							'pph' => 0,
							'type_cash' => 0,
							'parent' =>  $resultsss['lastid'],
							'ket' => $array_itemss['ket_p'],
							'id_coa_temp' => $sss
						
						);
						
						$prin_result = $this->uom_model->add_uom($datas);
					 
					}
					
				}
		
		// }else{
		  // $data = array(
			// 'user_id' => $this->session->userdata['logged_in']['user_id'],
			// 'coa'     => $this->Anti_sql_injection($this->input->post('coa', TRUE)),
			// 'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
			// 'prin' => $this->Anti_sql_injection($this->input->post('prin', TRUE)),
			// 'jumlah' => $this->Anti_sql_injection($this->input->post('jumlah', TRUE)),
			// //'jumlah' => $this->Anti_sql_injection($this->input->post('jumlah', TRUE))-($this->Anti_sql_injection($this->input->post('jumlah', TRUE))*0.1),
			// 'pph' => 10,
			// 'ppn' => ($this->Anti_sql_injection($this->input->post('jumlah', TRUE))*0.1),
			// 'ket' => $this->Anti_sql_injection($this->input->post('ket', TRUE))
		  // );
		   // $result = $this->uom_model->add_uom($data);
		    // $result2 = $this->uom_model->add_trans($data,$result['lastid']);
	  // }
		//print_r($data);die;

		//$result = $this->uom_model->add_uom($data);
      
	 
	  
      if ($resultsss > 0) {
        $msg = 'Berhasil Merubah cash .';

        $result = array(
          'success' => true,
          'message' => $msg
        );
      } else {
        $msg = 'Gagal Merubah cash.';
        $result = array(
          'success' => false,
          'message' => $msg
        );
      }
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
  }

  public function add_uom()
  {
    $this->form_validation->set_rules('coa', 'coa', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('tgl', 'tgl', 'trim|required|max_length[255]');
    //$this->form_validation->set_rules('cash', 'cash', 'trim|required|max_length[255]');
    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));
      $result = array(
        'success' => false,
        'message' => $msg
      );

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      // die;
      $message = "";
	  
	  
	  $int_val = $this->Anti_sql_injection($this->input->post('int_val', TRUE));
			
			$array_items = [];
			
			$total_amount = 0;
			for($i=0;$i<=$int_val;$i++){
				
				//if(isset($this->input->post('kode_'.$i, TRUE))){
					$array_items[$i]['coa_p'] = $this->Anti_sql_injection($this->input->post('coa_p_'.$i, TRUE));
					$array_items[$i]['jumlahp'] = floatval(str_replace(',','.',str_replace('.','',$this->Anti_sql_injection($this->input->post('jumlahp_'.$i, TRUE)))));
					$array_items[$i]['ket_p'] = $this->Anti_sql_injection($this->input->post('ket_p_'.$i, TRUE));
					$array_items[$i]['dist_p'] = $this->Anti_sql_injection($this->input->post('dist_p_'.$i, TRUE));
			}
			
			//print_r($array_items);die;
	  
	  // if($this->Anti_sql_injection($this->input->post('coa', TRUE)) <> '122'  ){
		  
		  $data = array(
			'user_id' => $this->session->userdata['logged_in']['user_id'],
			'coa'     => $this->Anti_sql_injection($this->input->post('coa', TRUE)),
			'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
			//'prin' => $this->Anti_sql_injection($this->input->post('prin', TRUE)),
			'jumlah' => floatval(str_replace(',','.',str_replace('.','',$this->Anti_sql_injection($this->input->post('jumlah', TRUE))))),
			'pph' => 0,
			'type_cash' => 1,
			'parent' => 0,
			'id_coa_temp' => NULL,
			'ket' => $this->Anti_sql_injection($this->input->post('ket', TRUE))
		  );
		  
		  $resultsss = $this->uom_model->add_uom($data);

				foreach($array_items as $array_itemss){
					
					if($array_itemss['coa_p'] == ""){
						
					}else{
					
						
						if( $array_itemss['dist_p'] == ''){
							$sss = 0;
						}else{
							$sss = $array_itemss['dist_p'];
						}
					
						$datas = array(
						
							'user_id' => $this->session->userdata['logged_in']['user_id'],
							'coa'     => $array_itemss['coa_p'],
							'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
							//'prin' => $this->Anti_sql_injection($this->input->post('prin', TRUE)),
							'jumlah' => $array_itemss['jumlahp'],
							'pph' => 0,
							'type_cash' => 0,
							'parent' =>  $resultsss['lastid'],
							'ket' => $array_itemss['ket_p'],
							'id_coa_temp' => $sss
						
						);
						
						$prin_result = $this->uom_model->add_uom($datas);
					
					}
					
				}
		
		// }else{
		  // $data = array(
			// 'user_id' => $this->session->userdata['logged_in']['user_id'],
			// 'coa'     => $this->Anti_sql_injection($this->input->post('coa', TRUE)),
			// 'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
			// 'prin' => $this->Anti_sql_injection($this->input->post('prin', TRUE)),
			// 'jumlah' => $this->Anti_sql_injection($this->input->post('jumlah', TRUE)),
			// //'jumlah' => $this->Anti_sql_injection($this->input->post('jumlah', TRUE))-($this->Anti_sql_injection($this->input->post('jumlah', TRUE))*0.1),
			// 'pph' => 10,
			// 'ppn' => ($this->Anti_sql_injection($this->input->post('jumlah', TRUE))*0.1),
			// 'ket' => $this->Anti_sql_injection($this->input->post('ket', TRUE))
		  // );
		   // $result = $this->uom_model->add_uom($data);
		    // $result2 = $this->uom_model->add_trans($data,$result['lastid']);
	  // }
		//print_r($data);die;

		//$result = $this->uom_model->add_uom($data);
      
	 
	  
      if ($resultsss > 0) {
        $msg = 'Berhasil menambahkan cash .';

        $result = array(
          'success' => true,
          'message' => $msg
        );
      } else {
        $msg = 'Gagal menambahkan cash ke database.';
        $result = array(
          'success' => false,
          'message' => $msg
        );
      }
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
  }

  public function export()
  {
	  
	$param = [
		'filter_year' => $this->input->post('year_filter'),
		'filter_month' => $this->input->post('month_filter'),
	];
		
	$list = $this->uom_model->list_for_excel([]);
	// echo '<pre>'; print_r($list); die;

	$this->load->library('excel');

	$objPHPExcel = new PHPExcel();

	$objPHPExcel->getActiveSheet()->setTitle('Jurnal Luar')
		->setCellValue('A1', 'COA')
		->setCellValue('B1', 'Tanggal')
		->setCellValue('C1', 'Total')
		->setCellValue('D1', 'Keterangan');

	$row = 2;
	foreach ($list as $item) {
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A' . $row, $item['coa'] . '-' . $item['keterangan'])
			->setCellValue('B' . $row, $item['date'] ?? '')
			->setCellValue('C' . $row, $item['value_real'] ?? '')
			->setCellValue('D' . $row, $item['note'] ?? '');

		$row++;
	}

	$objPHPExcel->setActiveSheetIndex(0);

	// Redirect output to a client’s web browser (Excel2007)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename=jurnal_luar.xls');
	header('Cache-Control: max-age=0');
	// If you’re serving to IE 9, then the following may be needed
	header('Cache-Control: max-age=1');
	// If you’re serving to IE over SSL, then the following may be needed
	header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
	header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
	header('Pragma: public'); // HTTP/1.0

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');
	unset($objPHPExcel);
  }
}
