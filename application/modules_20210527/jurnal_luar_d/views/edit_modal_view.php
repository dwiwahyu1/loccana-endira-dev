<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto
  }

  .scroll-overflow {
    min-height: 650px
  }

  #modal-distributor::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <input name="int_flo" id="int_flo" type="hidden" class="form-control" placeholder="Qty" value=<?php echo count($detail) ?>;>

      <div class="product-sales-area mg-tb-30">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="product-payment-inner-st">
                <ul id="myTabedu1" class="tab-review-design">
                  <li class="active"><a href="#description">Update Jurnal Keluar</a></li> 
                </ul>
                <div id="myTabContent" class="tab-content custom-product-edit">
                  <div class="product-tab-list tab-pane fade active in" id="description">
                    <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="review-content-section">
                          <form class="form-horizontal form-label-left" id="add_uom" role="form" action="<?= base_url('jurnal_luar/edit_uom'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>

                            <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Cash Account Kredit<span class="required"><sup>*</sup></span>
                              </label>
                              <div class="col-md-8 col-sm-6 col-xs-12">
                                <select name="coa" id="coa" class="form-control" placeholder="Pilih Coa">
                                  <option></option>
                                  <?php
                                  foreach ($coa as $principals) {
                                    echo '<option value="' . $principals['id_coa'] . '" >' . $principals['coa'] . '-' . $principals['keterangan'] . '</option>';
                                  }
                                  ?>
                                </select>
                              </div>
                            </div>


                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Tanggal <span class="required"><sup>*</sup></span>
                              </label>
                              <div class="col-md-8 col-sm-6 col-xs-12">
                                <input data-parsley-maxlength="255" type="text" id="tgl" name="tgl" class="form-control col-md-7 col-xs-12" placeholder="Tanggal" required="required">
                              </div>
                            </div>


                            <div class="item form-group" id="prin_div" style="display:none">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Principle <span class="required"><sup>*</sup></span>
                              </label>
                              <div class="col-md-8 col-sm-6 col-xs-12">
                                <select name="prin" id="prin" class="form-control" placeholder="Principle">
                                  <option></option>
                                  <?php foreach ($principle as $principles) {

                                    echo "<option value='" . $principles['id'] . "' >" . $principles['name_eksternal'] . "</option>";
                                  } ?>
                                </select>

                              </div>
                            </div>


                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Jumlah <span class="required"><sup>*</sup></span>
                              </label>
                              <div class="col-md-8 col-sm-6 col-xs-12">
                                <input data-parsley-maxlength="255" type="text" id="jumlah" name="jumlah" class="form-control col-md-7 col-xs-12 rupiahs" placeholder="Jumlah" disabled="disabled">
                              </div>
                            </div>

                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Keterangan <span class="required"><sup>*</sup></span>
                              </label>
                              <div class="col-md-8 col-sm-6 col-xs-12">
                                <textarea data-parsley-maxlength="255" type="text" id="ket" name="ket" class="form-control col-md-7 col-xs-12" placeholder="Keterangan" required="required"> </textarea>
                              </div>
                            </div>

                            <div class="item form-group" style="border-top-style:solid;">
                              <label class="control-label col-md-1 col-sm-1 col-xs-12" for="nama">Cash Kredit <span class="required"><sup>*</sup></span>
                              </label>
                            </div>
                            <div id='cash_part'>
                              <?php $sa = 0;
                              foreach ($detail as $details) { ?>
                                <div class="item form-group">

                                  <div class="col-md-3 col-sm-3 col-xs-12">
                                    <select name="coa_p_<?php echo $sa; ?>" id="coa_p_<?php echo $sa; ?>" class="form-control" placeholder="Cash Kredit" onChange="select_coa(0)">
                                      <option></option>
                                      <?php
                                      foreach ($coa_p as $principals) {
                                        if ($principals['id_coa'] == $details['id_coa']) {
                                          echo '<option value="' . $principals['id_coa'] . '" selected="selected" >' . $principals['coa'] . '-' . $principals['keterangan'] . '</option>';
                                        } else {
                                          echo '<option value="' . $principals['id_coa'] . '" >' . $principals['coa'] . '-' . $principals['keterangan'] . '</option>';
                                        }
                                      }
                                      ?>
                                    </select>

                                  </div>

                                  <div class="col-md-3 col-sm-3 col-xs-12">
                                    <!-- <input type="text" id="jumlahp_<?= $sa; ?>" name="jumlahp_<?= $sa; ?>" class="form-control col-md-7 col-xs-12 rupiahs" placeholder="Jumlah" required="required" value=<?= number_format($details['value_real'], 2, ',', '.'); ?> onKeyup="change_sum(<?= $sa; ?>)"> -->
                                    <input type="text" id="jumlahp_<?= $sa; ?>" name="jumlahp_<?= $sa; ?>" class="form-control col-md-7 col-xs-12 rupiahs" placeholder="Jumlah" required="required" value=<?= (int) $details['value_real']; ?> onKeyup="change_sum(<?= $sa; ?>)">
                                  </div>

                                  <div class="col-md-3 col-sm-3 col-xs-12">
                                    <input type="text" id="ket_p_<?= $sa; ?>" name="ket_p_<?= $sa; ?>" class="form-control col-md-7 col-xs-12" placeholder="Keterangan" value='<?= $details['note']; ?>'>
                                  </div>

                                  <div class="col-md-2 col-sm-2 col-xs-12" id="dist_div_0">
                                    <select name="dist_p_<?php echo $sa; ?>" id="dist_p_<?php echo $sa; ?>" class="form-control" placeholder="Principle" disabled>
                                      <option></option>
                                      <?php
                                      foreach ($principle as $principals) {
                                        if ($principals['id'] == $details['id_dist']) {
                                          echo '<option value="' . $principals['id'] . '" selected="selected" >' . $principals['name_eksternal'] . '</option>';
                                        } else {
                                          echo '<option value="' . $principals['id'] . '" >' . $principals['name_eksternal'] . '</option>';
                                        }
                                      }
                                      ?>
                                    </select>

                                  </div>


                                </div>

                              <?php $sa++;
                              } ?>
                            </div>
                            <div class="item form-group" style="border-top-style:solid;">
                              <div class="col-md-11 col-sm-3 col-xs-12">
                              </div>
                              <div class="col-md-1 col-sm-1 col-xs-12"><br>
                                <button type="button" class="btn btn-danger waves-effect waves-light tbh_btn" onClick="tambah_row()">+</button>
                              </div>

                            </div>

                            <div class="item form-group" style="border-top-style:solid;">
                              <div class="col-md-8 col-sm-3 col-xs-12">
                              </div>
                              <div class="col-md-2 col-sm-1 col-xs-12">
                                <div class="item form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Total <span class="required"></span>
                                  </label>

                                </div>
                              </div>
                              <div class="col-md-2 col-sm-1 col-xs-12">
                                <div class="item form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama" id="total_l">0 <span class="required"></span>
                                  </label>

                                </div>
                              </div>

                            </div>


                            <div class="item form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
                              <div class="col-md-8 col-sm-6 col-xs-12">
                                <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Submit</button>
                                <button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()">Batal</button>
                              </div>
                            </div>

                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="PrimaryModalalert" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-close-area modal-close-df">
              <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
            </div>
            <div class="modal-body">
              <i class="educate-icon educate-checked modal-check-pro"></i>
              <h2>Data Berhasil Disimpan</h2>
              <p>Apakah Anda Ingin Menambah Data UoM Lagi ?</p>
            </div>
            <div class="modal-footer">
              <a data-dismiss="modal" href="#" onClick="back()">Tidak</a>
              <a data-dismiss="modal" href="#" onClick="clearform()">Ya</a>
            </div>
          </div>
        </div>
      </div>

      <div id="WarningModalftblack" class="modal modal-edu-general Customwidth-popup-WarningModal PrimaryModal-bgcolor fade" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-close-area modal-close-df">
              <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
            </div>
            <div class="modal-body">
              <span class="educate-icon educate-warning modal-check-pro information-icon-pro"></span>
              <h2>Warning!</h2>
              <p id="msg_err">The Modal plugin is a dialog box/popup window that is displayed on top of the current page</p>
            </div>
            <div class="modal-footer footer-modal-admin warning-md">
              <a data-dismiss="modal" href="#">Ok</a>
            </div>
          </div>
        </div>
      </div>

      <script>
        function clearform() {

          $('#add_uom').trigger("reset");

        }

        function tambah_row() {
          //$('.hapus_btn').hide();
          var values = $('#int_flo').val();

          var new_fl = parseInt(values) + 1;

          $('#int_flo').val(new_fl);

          var htl = '';
          htl += ' <div class="item form-group" id="tcashs_' + new_fl + '">';

          htl += '                   <div class="col-md-3 col-sm-4 col-xs-12">';
          htl += '                    <select name="coa_p_' + new_fl + '" id="coa_p_' + new_fl + '" class="form-control" placeholder="Cash Kredit" onChange="select_coa(' + new_fl + ')">';
          htl += '						<option></option>';
          <?php
          foreach ($coa_p as $principals) { ?>
            htl += '<option value="<?php echo $principals['id_coa']; ?>" > <?php echo $principals['coa'] . '-' . $principals['keterangan'] ?></option>';
          <?php  }   ?>
          htl += '				  </select>';

          htl += '                  </div>';
          htl += '					<div class="col-md-3 col-sm-3 col-xs-12">';
          htl += '                    <input type="text" id="jumlahp_' + new_fl + '" name="jumlahp_' + new_fl + '" class="form-control col-md-7 col-xs-12 rupiahs" placeholder="Jumlah" required="required" onKeyup="change_sum(' + new_fl + ')">';
          htl += '                     </div>';
          htl += '					<div class="col-md-3 col-sm-3 col-xs-12">';
          htl += '                    <input type="text" id="ket_p_' + new_fl + '" name="ket_p_' + new_fl + '" class="form-control col-md-7 col-xs-12 " placeholder="Keterangan" required="required">';
          htl += '                     </div>';
          htl += '';
          htl += '					<div class="col-md-2 col-sm-2 col-xs-12"  id="dist_div_' + new_fl + '">';
          htl += '                      <select name="dist_p_' + new_fl + '" id="dist_p_' + new_fl + '" class="form-control"  placeholder="Principle" disabled>';
          htl += '						<option></option>';
          <?php foreach ($principle as $principals) { ?>
            htl += '<option value="<?php echo $principals["id"] ?>" ><?php echo $principals["name_eksternal"]; ?></option>';
          <?php  } ?>
          htl += '				  </select>';

          htl += '               </div>';
          htl += '';
          htl += '										<div class="col-md-1 col-sm-1 col-xs-12">';
          htl += '											<div class="form-group">';
          htl += '												<button type="button" class="btn btn-danger waves-effect waves-light " onClick="kurang_row(' + new_fl + ')">-</button>';
          htl += '											</div>';
          htl += '										</div>';
          htl += '              </div>';

          $('#cash_part').append(htl);
          $('#coa_p_' + new_fl).select2();
          $('#dist_p_' + new_fl).select2();

          $(".rupiahs").inputFilter(function(value) {
            console.log('rupiah filter triggered');
            return /^-?\d*[,]?\d*$/.test(value);
          });

        }

        function select_coa(new_fl) {

          //alert (new_fl);

          var coa_v = $('#coa_p_' + new_fl).val();
          // $('#dist_p_'+new_fl).show();
          // lert (coa_v);

          if (coa_v > 139 && coa_v < 154) {

            $("#dist_p_" + new_fl).prop('disabled', false);
            //$('#dist_div_'+new_fl).show();

            // document.getElementById("dist_p_0").style.display = "block";

          } else {
            $("#dist_p_" + new_fl).prop('disabled', true);
            // $('#dist_div_'+new_fl).hide();
          }

        }

        function kurang_row(new_fl) {

          var int_val = parseInt($('#int_flo').val());

          $('#tcashs_' + new_fl).remove();
          change_sum(int_val);

        }

        function change_sum(fl) {

          // var harga = 	$('#bayar_'+fl).val();


          // var harga = harga.replace('.','');
          // var harga = harga.replace('.','');
          // var harga = harga.replace('.','');
          // var harga = harga.replace('.','');
          // var harga = harga.replace(',','.');



          // // if( isNaN(total)){
          // // //var total_num = Number((total).toFixed(1)).toLocaleString();
          // // //$('#subttl').html('0');
          // // $('#total_'+fl).val('0');
          // // }else{
          // // //var total_num = Number((total).toFixed(1)).toLocaleString();
          // // //$('#subttl').html(total);
          // // $('#total_'+fl).val(totals);
          // // }

          var sub_total = 0;
          var sub_total_bd = 0;
          var sub_disk = 0;
          var int_val = parseInt($('#int_flo').val());

          for (var yo = 0; yo <= int_val; yo++) {

            if ($('#coa_p_' + yo).val() !== undefined) {

              var tots = $('#jumlahp_' + yo).val();
              var tots = tots.replace('.', '');
              var tots = tots.replace('.', '');
              var tots = tots.replace('.', '');
              var tots = tots.replace('.', '');
              var tots = tots.replace(',', '.');





              sub_total = sub_total + parseFloat(tots);

            }

          }


          var sub_totals = new Intl.NumberFormat('de-DE', {
            minimumFractionDigits: 2
          }).format(sub_total);

          // $('#subttl').html(total_bds);
          // $('#subttl2').html(sub_totals);
          // $('#subttl3').html(total_sub_disk);
          // $('#vatppn').html(total_ppns);
          $('#jumlah').val(sub_totals);
          $('#total_l').html(sub_totals);




          //alert(qty+' '+harga+' '+diskon);

        }

        function back() {

          window.location.href = "<?php echo base_url() . 'jurnal_luar'; ?>";

        }

        function listdist() {
          var user_id = '0001';
          var token = '093940349';


          $('#datatable_pricipal').DataTable({
            //"dom": 'rtip',
            "bFilter": false,
            "aaSorting": [],
            "bLengthChange": true,
            'iDisplayLength': 10,
            "sPaginationType": "simple_numbers",
            "Info": false,
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "ajax": "<?php echo base_url() . 'jurnal_luar/lists' ?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token,
            "searching": true,
            "language": {
              "decimal": ",",
              "thousands": "."
            },
            "dom": 'l<"toolbar">frtip',
            "initComplete": function() {
              $("div.toolbar").prepend('<div class="btn-group pull-left"><button type="button" class="btn btn-custon-rounded-two btn-primary" > Tambah </button></div>');
            }
          });
        }

        $(document).ready(function() {
          $(".rupiahs").inputFilter(function(value) {
            return /^-?\d*[,]?\d*$/.test(value);
          });

          <?php $sa = 0;
          foreach ($detail as $details) { ?>

            $('#coa_p_<?php echo $sa; ?>').select2();
            $('#dist_p_<?php echo $sa; ?>').select2();
            // $('#coa_p_<?php echo $sa; ?>').val('<?php echo $details['id_coa'] ?>');  
            // $('#jumlahp_<?php echo $sa; ?>').val('<?php echo $details['date'] ?>');  
            // $('#ket_p_<?php echo $sa; ?>').val('<?php echo $details['value_real'] ?>');  
            // $('#dist_p_<?php echo $sa; ?>').val('<?php echo $details['note'] ?>');  

          <?php $sa++;
          } ?>



          $('#coa').val('<?php echo $detail[0]['id_head'] ?>');
          $('#tgl').val('<?php echo $detail[0]['date_head'] ?>');
          $('#total_l').html('<?php echo number_format($detail[0]['value_real'], 2, ',', '.') ?>');
          $('#jumlah').val('<?php echo number_format($detail[0]['value_real'], 2, ',', '.') ?>');
          $('#ket').val('<?php echo $detail[0]['note_head'] ?>');


          $('#coa').select2();

          $('#prin').select2();

          $('#tgl').datepicker({
            format: "yyyy-mm-dd",
            autoclose: true,
            todayHighlight: true
          });
          listdist();

          $('#coa').on('change', function(e) {


            if ($(this).val() == 122 || $(this).val() == 123) {
              $('#prin_div').css("display", "block");
              $('#prin_div').val(0);
              $('#prin').select2();
            } else {
              $('#prin_div').css("display", "none");
            }

            //alert($(this).val());

          });

          $('#add_uom').on('submit', function(e) {
            // validation code here
            //if(!valid) {
            e.preventDefault();


            swal({
              title: 'Yakin akan Simpan Data ?',
              text: '',
              type: 'warning',
              showCancelButton: true,
              confirmButtonText: 'Ya',
              cancelButtonText: 'Tidak'
            }).then(function() {

              var int_val = parseInt($('#int_flo').val());
              var formData = new FormData();
              formData.append('id', <?php echo $id; ?>);
              formData.append('coa', $('#coa').val());
              formData.append('tgl', $('#tgl').val());
              formData.append('prin', $('#prin').val());
              formData.append('jumlah', $('#jumlah').val());
              formData.append('ket', $('#ket').val());
              formData.append('int_val', $('#int_flo').val());


              for (var yo = 0; yo <= int_val; yo++) {

                if (typeof($('#coa_p_' + yo).val()) !== 'undefined') {

                  if (!$('#coa_p_' + yo).val() || !$('#jumlahp_' + yo).val()) {
                    swal({
                      title: 'Bank dari Cash Credit harus dipilih dan Jumlah tidak boleh kosong',
                      type: 'warning'
                    });

                    return false;
                  }

                  formData.append('coa_p_' + yo, $('#coa_p_' + yo).val());
                  formData.append('jumlahp_' + yo, $('#jumlahp_' + yo).val());
                  formData.append('ket_p_' + yo, $('#ket_p_' + yo).val());
                  formData.append('dist_p_' + yo, $('#dist_p_' + yo).val());

                }

              }


              $.ajax({
                type: 'POST',
                url: '<?php echo base_url('jurnal_luar/edit_uom'); ?>',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(response) {

                  //console.log(response);
                  if (response.success == true) {
                    // $('#PrimaryModalalert').modal('show');
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function() {
                      back();
                    })
                  } else {
                    // $('#msg_err').html(response.message);
                    // $('#WarningModalftblack').modal('show');

                    swal({
                      title: 'Data Inputan Tidak Seusai !! ',
                      text: response.message,
                      type: 'error',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function() {


                    });

                  }
                }
              });

            })
            //alert(kode);


            //}
          });
        });
      </script>


      <!-- /page content -->