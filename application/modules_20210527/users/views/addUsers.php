<style>
    .dt-body-left {
        text-align: left;
    }

    .dt-body-right {
        text-align: right;
    }

    .dt-body-center {
        text-align: center;
        vertical-align: middle;
    }

    .force-overflow {
        height: 650px;
        overflow-y: auto;
        overflow-x: auto
    }

    .scroll-overflow {
        min-height: 650px
    }

    #modal-distributor::-webkit-scrollbar-track {
        box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
        background-color: #F5F5F5;
    }

    #modal-distributor::-webkit-scrollbar {
        width: 10px;
        background-color: #F5F5F5;
    }

    #modal-distributor::-webkit-scrollbar-thumb {
        background-image: -webkit-gradient(linear,
                left bottom,
                left top,
                color-stop(0.44, rgb(122, 153, 217)),
                color-stop(0.72, rgb(73, 125, 189)),
                color-stop(0.86, rgb(28, 58, 148)));
    }
</style>

<div class="product-sales-area mg-tb-30">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="product-payment-inner-st">
                    <ul id="myTabedu1" class="tab-review-design">
                        <li class="active"><a href="#description">Tambah Users</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content custom-product-edit">
                        <div class="product-tab-list tab-pane fade active in" id="description">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="review-content-section">
                                        <form id="add_users" action="<?php echo base_url() . 'users/add_users'; ?>" class="add-department">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Nama</label>
                                                        <input name="name" id="name" type="text" class="form-control" placeholder="Nama User">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Alamat</label>
                                                        <input name="alamat" id="alamat" type="text" class="form-control" placeholder="Alamat User">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>No Telp</label>
                                                        <input name="telp" id="telp" type="text" class="form-control" placeholder="Telephone">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <input name="email" id="email" type="email" class="form-control" placeholder="Email">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Role</label>
                                                        <select name="role" id="role" type="text" class="form-control" placeholder="Role">
                                                            <option selected="selected" disabled>-- Pilih Role --</option>
                                                            <?php foreach ($role as $roleGroup) : ?>
                                                                <option value="<?= $roleGroup['id']; ?>"><?= $roleGroup['group']; ?></option>;
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Wilayah</label>
                                                        <select name="region" id="region" type="text" class="form-control" placeholder="Wilayah">
                                                            <option selected="selected" disabled>-- Pilih Wilayah --</option>
                                                            <option value="0">Tanpa Wilayah</option>
                                                            <?php foreach ($wilayah as $wilayahs) : ?>
                                                                <option value="<?php $wilayahs['id_t_region']; ?>"><?= $wilayahs['region']; ?></option>;
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Username</label>
                                                        <input name="username" id="username" type="text" class="form-control" placeholder="Username">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Password</label>
                                                        <input name="password" id="password" type="password" class="form-control" placeholder="Passowrd">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Gambar</label>
                                                        <input name="picture" id="picture" type="file" class="form-control" placeholder="Gambar">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="payment-adress">
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                                        <button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()"> Batal</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="PrimaryModalalert" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-close-area modal-close-df">
                <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
            </div>
            <div class="modal-body">
                <i class="educate-icon educate-checked modal-check-pro"></i>
                <h2>Data Berhasil Disimpan</h2>
                <p>Apakah Anda Ingin Menambah Data Users Lagi ?</p>
            </div>
            <div class="modal-footer">
                <a data-dismiss="modal" href="#" onClick="back()">Tidak</a>
                <a data-dismiss="modal" href="#" onClick="clearform()">Ya</a>
            </div>
        </div>
    </div>
</div>

<div id="WarningModalftblack" class="modal modal-edu-general Customwidth-popup-WarningModal PrimaryModal-bgcolor fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-close-area modal-close-df">
                <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
            </div>
            <div class="modal-body">
                <span class="educate-icon educate-warning modal-check-pro information-icon-pro"></span>
                <h2>Warning!</h2>
                <p id="msg_err">The Modal plugin is a dialog box/popup window that is displayed on top of the current page</p>
            </div>
            <div class="modal-footer footer-modal-admin warning-md">
                <a data-dismiss="modal" href="#">Ok</a>
            </div>
        </div>
    </div>
</div>

<script>
    function clearform() {
        $('#add_users').trigger("reset");
    }

    function back() {
        window.location.href = "<?php echo base_url() . 'users'; ?>";
    }

    function listdist() {
        var user_id = '0001';
        var token = '093940349';


        $('#datatable_pricipal').DataTable({
            //"dom": 'rtip',
            "bFilter": false,
            "aaSorting": [],
            "bLengthChange": true,
            'iDisplayLength': 10,
            "sPaginationType": "simple_numbers",
            "Info": false,
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "ajax": "<?php echo base_url() . 'users/lists' ?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token,
            "searching": true,
            "language": {
                "decimal": ",",
                "thousands": "."
            },
            "dom": 'l<"toolbar">frtip',
            "initComplete": function() {
                $("div.toolbar").prepend('<div class="btn-group pull-left"><button type="button" class="btn btn-custon-rounded-two btn-primary" > Tambah </button></div>');
            }
        });
    }

    $(document).ready(function() {

        listdist();

        $('#add_users').on('submit', function(e) {
            // validation code here
            //if(!valid) {
            e.preventDefault();

            var formData = new FormData(this);

            $.ajax({
                type: 'POST',
                url: $(this).attr('action'),
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(response) {

                    //console.log(response);
                    if (response.success == true) {
                        $('#PrimaryModalalert').modal('show');
                    } else {
                        $('#msg_err').html(response.message);
                        $('#WarningModalftblack').modal('show');
                    }
                }
            });


            //alert(kode);


            //}
        });
    });
</script>