<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Return_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}


	public function list_penjualan($params = array())
	{
		$query = 	'
				SELECT COUNT(*) AS jumlah 
				 FROM `t_retur` a
					JOIN `t_penjualan` b ON a.id_penjualan = b.`id_penjualan`
					LEFT JOIN `u_user` c ON a.`pic` = c.id
					LEFT JOIN `u_user` d ON a.`approval` = d.id
					LEFT JOIN `t_customer` e ON b.id_customer = e.`id_t_cust`
					where 1 = 1  AND ( 
						c.nama LIKE "%' . $params['searchtxt'] . '%" OR
						e.cust_name LIKE "%' . $params['searchtxt'] . '%"  OR
						b.no_penjualan LIKE "%' . $params['searchtxt'] . '%"
					)
			';

		$query2 = 	'
				SELECT z.*, rank() over ( ORDER BY date DESC) AS Rangking from ( 
					SELECT a.id_retur,a.date,a.status AS status_retur,a.approval_date,b.*, c.nama AS pic, d.nama AS approval, e.`cust_name` FROM `t_retur` a
					JOIN `t_penjualan` b ON a.id_penjualan = b.`id_penjualan`
					LEFT JOIN `u_user` c ON a.`pic` = c.id
					LEFT JOIN `u_user` d ON a.`approval` = d.id
					LEFT JOIN `t_customer` e ON b.id_customer = e.`id_t_cust`
					where 1 = 1  AND ( 
					c.nama LIKE "%' . $params['searchtxt'] . '%" OR
					e.cust_name LIKE "%' . $params['searchtxt'] . '%"  OR
					b.no_penjualan LIKE "%' . $params['searchtxt'] . '%"
					)  order by date DESC) z
				ORDER BY date DESC
				
				LIMIT ' . $params['limit'] . ' 
				OFFSET ' . $params['offset'] . ' 
			';

		//echo $query2;die;

		$out		= array();
		$querys		= $this->db->query($query);
		$result = $querys->row();

		$total_filtered = $result->jumlah;
		$total 			= $result->jumlah;

		if (($params['offset'] + 10) > $total_filtered) {
			$limit_data = $total_filtered - $params['offset'];
		} else {
			$limit_data = $params['limit'];
		}



		//echo $query;die;
		//echo $query;die;
		$query2s		= $this->db->query($query2);
		$result2 = $query2s->result_array();
		$return = array(
			'data' => $result2,
			'total_filtered' => $total_filtered,
			'total' => $total,
		);
		return $return;
	}


	public function get_customer($params = array())
	{

		//$query = $this->db->get_where('t_customer', array('region' => $params['region']));

		$this->db->select('*')
			->from('t_customer')
			# ->where(array('region' => $params['region']))
			->order_by('cust_name', 'asc');
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_price_mat($params = array())
	{

		// $query = $this->db->get_where('t_po_mat', array('id_po' => $params['kode']));
		// $query = $this->db->order_by('id_t_ps', 'desc');

		$this->db->select('*')
			->from('t_po_mat')
			->where(array('id_material' => $params['kode']))
			->order_by('id_t_ps', 'desc');
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_penjualan_byid($params = array())
	{

		$this->db->select('t_penjualan.*, t_customer.*')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_penjualan')
			->join('t_customer', 't_penjualan.id_customer = t_customer.id_t_cust')
			->where(array('id_penjualan' => $params['id_penjualan']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}


	public function get_item_byprin($params = array())
	{
		$this->db->select('m_material.*, m_uom.uom_symbol')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('m_material')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('dist_id' => $params['id_prin']));
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_retur_id($params = array())
	{
		$this->db->select('t_retur.date,t_retur.id_penjualan,t_retur.keterangan, t_detail_retur.*')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_retur')
			->join('t_detail_retur', 't_retur.id_retur = t_detail_retur.id_retur')
			->where(array('t_retur.id_retur' => $params['id']));
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_item_byprin_all()
	{
		$this->db->select('m_material.*, m_uom.uom_symbol')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('m_material')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('status' => 0))
			->order_by('stock_name', 'asc');
		//->where(array('dist_id' => $params['id_prin']));
		// ->where('m_material.dist_id != ',$params['id_prin'],FALSE);
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_penjualan_all()
	{
		$this->db->select('t_penjualan.*, t_customer.cust_name')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_penjualan')
			->join('t_customer', 't_penjualan.id_customer = t_customer.id_t_cust')
			#->where(array('status' => 0))
			->order_by('date_penjualan', 'desc');
		//->where(array('dist_id' => $params['id_prin']));
		// ->where('m_material.dist_id != ',$params['id_prin'],FALSE);
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_kode($params = array())
	{

		$query =  $this->db->select_max('seq_n', 'max');
		$query = $this->db->get('t_penjualan');

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_penjualan($params = array())
	{

		// $query = $this->db->get_where('t_purchase_order', array('id_po' => $params['id']));
		$this->db->select('t_penjualan.*, t_customer.*')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_penjualan')
			->join('t_customer', 't_penjualan.id_customer = t_customer.id_t_cust')
			->where(array('id_penjualan' => $params['id']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_penjualan_detail_update($params = array())
	{

		//$query = $this->db->get_where('d_penjualan', array('id_penjualan' => $params['id_penjualan']));
		$this->db->select('d_penjualan.*, m_material.stock_name, m_material.base_qty, m_material.stock_name, m_uom.uom_symbol,t_retur.date_retur,
		t_detail_retur.qty as qty_retur,t_detail_retur.qty_satuan as qty_retur_sat,t_detail_retur.qty_box as qty_box_retur,t_detail_retur.amount')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('d_penjualan')
			->join('m_material', 'd_penjualan.id_material = m_material.id_mat')
			->join('t_detail_retur', 'd_penjualan.id_dp = t_detail_retur.id_detail_mat')
			->join('t_retur', 't_detail_retur.id_retur = t_retur.id_retur')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('t_detail_retur.id_retur' => $params['id_penjualan']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_penjualan_detail($params = array())
	{

		//$query = $this->db->get_where('d_penjualan', array('id_penjualan' => $params['id_penjualan']));
		$this->db->select('d_penjualan.*, m_material.stock_name, m_material.base_qty, m_material.stock_name, m_uom.uom_symbol')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('d_penjualan')
			->join('m_material', 'd_penjualan.id_material = m_material.id_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('id_penjualan' => $params['id_penjualan']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_po_detail_full($params = array())
	{

		//$query = $this->db->get_where('t_po_mat', array('id_po' => $params['id']));
		$this->db->select('t_po_mat.*,t_po_mat.qty as qty_order, m_material.*, m_uom.*')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_po_mat')
			->join('m_material', 't_po_mat.id_material = m_material.id_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('id_po' => $params['id']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function check_principal($params = array())
	{

		$query = $this->db->get_where('t_eksternal', array('kode_eksternal' => $params['kode']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_returm($data)
	{

		$this->db->insert('t_retur', $data);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_mutasi($data)
	{

		$data_insert_mutasi = array(
			'id_stock_awal'  => $data['id_material'],
			'id_stock_akhir' => $data['id_material'],
			'date_mutasi'    => date('Y-m-d H:i:s'),
			'amount_mutasi'  => $data['qty'],
			'type_mutasi'    => 0,
			'user_id'    => $this->session->userdata['logged_in']['user_id'],
		);

		//	print_r($datas);die;

		$this->db->insert('t_mutasi', $data_insert_mutasi);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_mutasi_out($data)
	{

		$data_insert_mutasi = array(
			'id_stock_awal'  => $data['id_material'],
			'id_stock_akhir' => $data['id_material'],
			//'date_mutasi'    => date('Y-m-d H:i:s'),
			'date_mutasi'    => $data['date_retur'].' 00:00:00',
			'amount_mutasi'  => $data['qty'],
			'type_mutasi'    => 1,
			'user_id'    => $this->session->userdata['logged_in']['user_id'],
		);


		$this->db->insert('t_mutasi', $data_insert_mutasi);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_detail_retur($data)
	{

		//	print_r($datas);die;

		$this->db->insert('t_detail_retur', $data);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_qty($data)
	{
		$sql 	= 'update m_material set qty = qty - ' . $data['qty'] . ' 
		where id_mat = ' . $data['id_material'] . ' ';

		// $datas = array(
		// 'qty' => $data['qty_stock']
		// );

		// //	print_r($datas);die;
		// $this->db->where('id_mat',$data['id_material'], false);
		// $this->db->update('m_material',$datas, false);


		$query 	= $this->db->query($sql);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function update_qty_m($data)
	{
		$sql 	= 'update m_material set qty = qty + ' . $data['qty'] . ' 
		where id_mat = ' . $data['id_mat'] . ' ';

		//echo $sql;die;
		// $datas = array(
		// 'qty' => $data['qty_stock']
		// );

		//	print_r($datas);die;
		// $this->db->where('id_mat',$data['id_material'], false);
		// $this->db->update('m_material',$datas, false);


		$query 	= $this->db->query($sql);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_credit($data)
	{
		$sql 	= "
		update t_customer set sisa_credit = sisa_credit - " . $data['total_amount'] . " where id_t_cust = " . $data['id_customer'] . "
		";

		// $total = $data['sisa'] - $data['total_amount'];
		// $datas = array(
		// 'sisa_credit' => $total
		// );

		//	print_r($datas);die;
		// $this->db->where('id_t_cust',$data['id_customer'], false);
		// $this->db->update('t_customer',$datas, false);


		$query 	= $this->db->query($sql);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_credit2($data, $selling_data)
	{
		$sql 	= 'update t_customer set sisa_credit = sisa_credit + ' . floatval($selling_data['total_amount']) . '
		where id_t_cust = ' . $data['id_customer'];
		$query 	= $this->db->query($sql);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_retur_apr($data)
	{

		$datas = array(

			'status' => $data['sts']

		);

		$this->db->where('id_retur', $data['id_po']);
		$this->db->update('t_retur', $datas);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function history_retur_apr($data)
	{

		$sql 	= "
		INSERT INTO `history_d_penjualan`
		SELECT NULL AS jo,a.* FROM `d_penjualan` a
		JOIN  `t_detail_retur` b ON a.id_dp = b.`id_detail_mat` 
		WHERE b.id_retur  = " . $data['id_po'] . "
		
		";

		// $total = $data['sisa'] - $data['total_amount'];
		// $datas = array(
		// 'sisa_credit' => $total
		// );

		//	print_r($datas);die;
		// $this->db->where('id_t_cust',$data['id_customer'], false);
		// $this->db->update('t_customer',$datas, false);


		$query 	= $this->db->query($sql);


		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_retur($data)
	{
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(

			'date' => $data['date'],
			'id_penjualan' => $data['id_penjualan'],
			'keterangan' => $data['keterangan']

		);

		//	print_r($datas);die;
		$this->db->where('id_retur', $data['id_retur']);
		$this->db->update('t_retur', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_detail_penjualan($data)
	{
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'qty' => $data['qty_retur'],
			'qty_box' => $data['qty_box_retur'],
			'qty_satuan' => $data['qty_retur_sat'],
			'price' => $data['amount'],
			'mutasi_id' => $data['mutasi_id']
		);

		//	print_r($datas);die;
		$this->db->where('id_dp', $data['id_dp']);
		$this->db->update('d_penjualan', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	function delete_retur($id)
	{

		$this->db->where('id_retur', $id);
		$this->db->delete('t_retur');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $result;
	}

	function delete_detail_retur($id)
	{

		$this->db->where('id_retur', $id);
		$this->db->delete('t_detail_retur');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $result;
	}

	function delete_mutasi($data_q)
	{

		$this->db->where('id_mutasi', $data_q['mutasi_id']);
		$this->db->delete('t_mutasi');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $result;
	}
}
