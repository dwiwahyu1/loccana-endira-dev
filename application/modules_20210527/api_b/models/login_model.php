<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();

	}

	function login($params){

		$sql 	= 'CALL login_auth(?)';
		$query 	= $this->db->query($sql,
					array(
						$params['username']
					));
		$result = $query->row_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	function login_menu($idrole){

		$sql 	= 'SELECT um.url_mobile AS component, um.label AS title
					FROM u_menu um
					LEFT JOIN u_menu_group umg ON umg.menu_id = um.id
					WHERE umg.group_id = '.$idrole.' AND umg.status = 1';
		$query 	= $this->db->query($sql);
		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

}
