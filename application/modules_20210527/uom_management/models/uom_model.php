<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Uom_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function edit_uom($data)
	{
		$datas = array(
			'uom_name' => $data['nama_uom'],
			'uom_symbol' => $data['simbol'],
			'description' => $data['keterangan']
		);

	//	print_r($datas);die;
		$this->db->where('id_uom',$data['id_uom']);
		$this->db->update('m_uom',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_uom($data)
	{
		$datas = array(
			'uom_name' => $data['nama_uom'],
			'uom_symbol' => $data['simbol'],
			'description' => $data['keterangan']
		);

	//	print_r($datas);die;

		$this->db->insert('m_uom',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	
	public function deletes($id_uom)
	{
		$this->db->where('id_uom', $id_uom);
		$this->db->delete('m_uom'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function uom_list($params = [])
	{
		// echo '<pre>'; print_r($params); die;

		$totalRows = $this->db
			->select('*')
			->from('m_uom')
			->get()
			->result_array();

		$filteredQueryBuilder = $this->db
			->select('*')
			->from('m_uom');

		if (isset($params['filter']) && !empty($params['filter'])) {
			$filteredQueryBuilder = $filteredQueryBuilder->like('m_uom.uom_name', $params['filter']);
			$filteredQueryBuilder = $filteredQueryBuilder->like('m_uom.uom_symbol', $params['filter']);
			$filteredQueryBuilder = $filteredQueryBuilder->like('m_uom.description', $params['filter']);
		}

		$filteredResult = $filteredQueryBuilder->get()->result_array();

		$paginatedQueryBuilder = $this->db
			->select('*')
			->from('m_uom');

		if (isset($params['filter']) && !empty($params['filter'])) {
			$paginatedQueryBuilder = $paginatedQueryBuilder->like('m_uom.uom_name', $params['filter']);
			$paginatedQueryBuilder = $paginatedQueryBuilder->like('m_uom.uom_symbol', $params['filter']);
			$paginatedQueryBuilder = $paginatedQueryBuilder->like('m_uom.description', $params['filter']);
		}

		if (isset($params['limit']) && $params['limit'] >= 0 && isset($params['offset'])) {
			$paginatedQueryBuilder = $paginatedQueryBuilder->limit($params['limit'], $params['offset']);
		}

		$paginatedResult = $paginatedQueryBuilder->get()->result_array();

		return [
			'data' => $paginatedResult,
			'total_filtered' => count($filteredResult),
			'total' => count($totalRows),
		];
	}

	public function get_uom_by_id($id)
	{
		return $this->db->select('*')
			->from('m_uom')
			->where('id_uom', (int) $id)
			->get()
			->row_array();
	}
}
