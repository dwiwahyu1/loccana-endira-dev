<?php 
class Formatnumbering {
	public function qty($number){
		return number_format($number,0,',','.');
	}
	public function amount($number){
		return number_format($number,2,',','.');
  }
  public function price($number){
    return number_format(($number),0,',','.');
  }
	public function unit_price($number,$id_valas){
		if($id_valas==1)
			return number_format($number,2,',','.');
		return number_format($number,5,',','.');
	}
}
