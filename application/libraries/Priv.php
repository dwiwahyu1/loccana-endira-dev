<?php 
class Priv {
	
	function get_priv() {
		$CI =& get_instance();
		$CI->load->helper('url');
		$CI->load->library('session');
		$ip = $_SERVER['REMOTE_ADDR'];
		$user_agent = $_SERVER['HTTP_USER_AGENT'];
		$menu = $CI->uri->segment(1);
		$id_role = $CI->session->userdata['logged_in']['id_role'];

		$tz = 'Asia/Jakarta';
		$timestamp = time();
		$dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
		$dt->setTimestamp($timestamp); //adjust the object to correct timestamp
		$date_now =  $dt->format('Y_d_m_H_i_s');
		$date_now_f =  $dt->format('Y-m-d H:i:s');
		
		$query =   $CI->db->query("
		SELECT B.* 
		FROM `u_menu` A 
		JOIN `u_menu_group` B ON A.`id` = B.`menu_id` 
		WHERE A.status = 1 
		AND A.url = '".$menu."' 
		AND B.`group_id` = ".$id_role);
		
		$list = $query->result_array();
		
		$array_button = array('none','block');
		
		$priv = array(
			'insert' => $array_button[$list[0]['insert']],
			'update' => $array_button[$list[0]['update']],
			'delete' => $array_button[$list[0]['delete']],
			'detail' => $array_button[$list[0]['detail']],
			'approve' => $array_button[$list[0]['update']],
			'role' => $CI->session->userdata['logged_in']['id_role'],
		);
		
		return $priv;
	}
}

?>