<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Purchase_order_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}


	public function list_po($params = array())
	{

		if ($params['filter'] == 'ALL') {
			$query_m = ' ';
		} else {
			$query_m = ' AND DATE_FORMAT(a.tanggal,"%M") = "' . $params['filter'] . '"';
		}

		if ($params['filter_year'] == 'ALL') {
			$query_y = ' ';
		} else {
			$query_y = 'AND DATE_FORMAT(a.tanggal,"%Y") = "' . $params['filter_year'] . '" ';
		}

		$where_sts = $query_m . ' ' . $query_y;

		$query =   '
				SELECT COUNT(*) AS jumlah FROM ( 
				SELECT a.id_bpb,
					a.no_do,a.angkutan,a.tanggal,b.no_po,c.name_eksternal,a.status, b.date_po , f.no_invoice
					FROM t_bpb a
					JOIN t_purchase_order b ON a.id_po=b.id_po
					JOIN t_eksternal c ON b.id_distributor=c.id
					JOIN t_po_mat d ON b.id_po=d.id_po
					JOIN t_bpb_detail e ON d.id_t_ps=e.id_po_mat
					LEFT JOIN `t_invoice_pembelian` f ON a.id_bpb = f.id_bpb
				where 1 = 1 ' . $where_sts . '
				AND ( 
					no_do LIKE "%' . $params['searchtxt'] . '%" OR
					no_po LIKE "%' . $params['searchtxt'] . '%" OR
					name_eksternal LIKE "%' . $params['searchtxt'] . '%" OR
					angkutan LIKE "%' . $params['searchtxt'] . '%" 
				)
				group by a.id_bpb
				order by no_do,id_bpb DESC
				
				) k
			';

		$query2 =   '
				SELECT z.*, rank() over ( ORDER BY id_bpb,no_do DESC) AS Rangking from ( 
					
					SELECT a.id_bpb,a.no_do,a.angkutan,a.tanggal,
					 b.no_po,c.name_eksternal,a.status, 
					 b.date_po , f.no_invoice,SUM(d.`unit_price`*e.qty) as harga_t,
					 SUM((d.`unit_price`*e.qty*(d.diskon/100))) as diskon_t,
					 SUM(e.qty) as value_t
					FROM t_bpb a
					JOIN t_bpb_detail e ON a.`id_bpb`=e.`id_bpb`
					JOIN t_purchase_order b ON a.id_po=b.id_po
					JOIN t_po_mat d ON e.`id_po_mat`=d.`id_t_ps`
					JOIN t_eksternal c ON b.id_distributor=c.id
					LEFT JOIN t_invoice_pembelian f ON a.id_bpb = f.id_bpb
					where 1 = 1 ' . $where_sts . '
				AND ( 
					no_do LIKE "%' . $params['searchtxt'] . '%" OR
					no_po LIKE "%' . $params['searchtxt'] . '%" OR
					name_eksternal LIKE "%' . $params['searchtxt'] . '%" OR
					angkutan LIKE "%' . $params['searchtxt'] . '%" 
				)
					GROUP BY a.id_bpb
					ORDER BY ' . $params['order_column'] . ' ' . $params['order_dir'] . '
        ) z
				ORDER BY ' . $params['order_column'] . ' ' . $params['order_dir'] . '
				
				LIMIT ' . $params['limit'] . ' 
				OFFSET ' . $params['offset'] . ' 
			';

		//echo $query2;die;

		$out    = array();
		$querys    = $this->db->query($query);
		$result = $querys->row();

		$total_filtered = $result->jumlah;
		$total       = $result->jumlah;

		if (($params['offset'] + 10) > $total_filtered) {
			$limit_data = $total_filtered - $params['offset'];
		} else {
			$limit_data = $params['limit'];
		}



		//echo $query;die;
		//echo $query;die;
		$query2s    = $this->db->query($query2);
		$result2 = $query2s->result_array();
		$return = array(
			'data' => $result2,
			'total_filtered' => $total_filtered,
			'total' => $total,
		);
		return $return;
	}


	public function get_principal($params = array())
	{

		$query = $this->db->get_where('t_eksternal', array('type_eksternal' => 1));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_po_byid($params = array())
	{

		$this->db->select('t_purchase_order.*,t_eksternal.* ')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_purchase_order')
			->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id')
			->where(array('t_purchase_order.id_po' => $params['id']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_po_by_inv($params = array())
	{

		$this->db->select('t_purchase_order.*,t_eksternal.*,t_invoice_pembelian.id_invoice,t_invoice_pembelian.no_invoice,t_invoice_pembelian.tanggal_invoice,t_invoice_pembelian.due_date, 
		,t_bpb.*,t_invoice_pembelian.note, count(id_t_ps) as rowr ')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_purchase_order')
			->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id')
			->join('t_bpb', 't_bpb.id_po = t_purchase_order.id_po')
			->join('t_invoice_pembelian', 't_bpb.id_bpb = t_bpb.id_bpb', 'left')
			->join('t_bpb_detail', 't_bpb.id_bpb = t_bpb_detail.id_bpb')
			->join('t_po_mat', 't_bpb_detail.id_po_mat = t_po_mat.id_t_ps')

			->where(array('t_bpb.id_bpb' => $params['id']))
			->group_by('t_bpb.id_bpb');
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}


	public function get_po($params = array())
	{

		// $this->db->select('t_purchase_order.*,t_eksternal.name_eksternal ')
		// #$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
		// ->from('t_purchase_order')
		// ->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id');
		// $query = $this->db->get();

		$query = "
		SELECT * FROM (
			SELECT a.*,b.*,c.qty AS qty_order,c.id_t_ps, 
			c.qty - SUM((d.qty+d.qty_bonus+d.qty_titipan)) AS sisa_q
			FROM `t_purchase_order` a
			JOIN t_eksternal b ON a.id_distributor = b.id
			JOIN `t_po_mat` c ON a.`id_po` = c.id_po
			LEFT JOIN `t_bpb_detail` d ON c.`id_t_ps` = d.id_po_mat
			WHERE a.status = 1
			GROUP BY c.id_t_ps
		) a WHERE sisa_q > 1 OR sisa_q IS NULL
		GROUP BY a.id_po
		";

		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_price_mat($params = array())
	{

		// $query = $this->db->get_where('t_po_mat', array('id_po' => $params['kode']));
		// $query = $this->db->order_by('id_t_ps', 'desc');

		$this->db->select('*')
			->from('t_po_mat')
			->where(array('id_material' => $params['kode']))
			->order_by('id_t_ps', 'desc');
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_principal_byid($params = array())
	{

		$query = $this->db->get_where('t_eksternal', array('id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}


	public function get_item_byprin($params = array())
	{
		$this->db->select('m_material.*, m_uom.uom_symbol')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('m_material')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('dist_id' => $params['id_prin']));
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_item_byprin_all($params = array())
	{
		$this->db->select('m_material.*, m_uom.uom_symbol')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('m_material')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			//->where(array('dist_id' => $params['id_prin']));
			->where('m_material.dist_id != ', $params['id_prin'], FALSE);
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_kode($params = array())
	{

		$query =  $this->db->select_max('seq_n', 'max');
		$query = $this->db->get('t_purchase_order');

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function edit_po($data)
	{
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'status' => 5,
			'total_amount' => $data['total_amount']
		);

		//	print_r($datas);die;
		$this->db->where('id_po', $data['id_po']);
		$this->db->update('t_purchase_order', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	// public function get_po($params = array()){

	// // $query = $this->db->get_where('t_purchase_order', array('id_po' => $params['id']));
	// $this->db->select('t_purchase_order.*, t_eksternal.*')
	// #$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
	// ->from('t_purchase_order')
	// ->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id')
	// ->where(array('id_po' => $params['id']));
	// $query = $this->db->get();

	// $return = $query->result_array();

	// $this->db->close();
	// $this->db->initialize();

	// return $return;
	// }

	public function get_po_detail($params = array())
	{

		$query = $this->db->get_where('t_po_mat', array('id_po' => $params['id']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_po_detail_update($params = array())
	{

		$query = $this->db->get_where('t_bpb_detail', array('id_bpb' => $params['id']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}


	public function get_convertion($params)
	{

		$query = "
		
		SELECT a.*,IF(convertion IS NULL,1,convertion) satuan_b, (IF(convertion IS NULL,1,convertion)) / c.`base_qty` AS mult 
		FROM `m_uom` a
		LEFT JOIN `t_uom_convert` b ON a.id_uom = b.`id_uom`
		LEFT JOIN `m_material` c ON c.`unit_terkecil` = a.`id_uom`
		WHERE c.`id_mat` = '" . $params . "'
		
		";
		$query = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_qty_mat($params = array(), $detail_mm)
	{

		$query = "
		
		update m_material a join t_detail_material b set 
		a.buy_price = (( b.qty * b.buy_price )) / (a.qty + b.qty),
		a.qty = a.qty + " . $params['qty_big'] . ", 
		a.qty_big = a.qty_big + " . $params['qty'] . ",
		a.qty_sol = a.qty_sol + " . $params['qty_sol_big'] . ", 
		a.qty_sol_big = a.qty_sol_big + " . $params['qty_sol'] . ",
		a.qty_bonus = a.qty_bonus + " . $params['qty_bonus_big'] . ", 
		a.qty_bonus_big = a.qty_bonus_big + " . $params['qty_bonus'] . ",
		a.qty_titip = a.qty_titip + " . $params['qty_titip_big'] . ", 
		a.qty_titip_big = a.qty_titip_big + " . $params['qty_titip'] . "
		where id_mat = " . $params['id_material'] . "
		and b.id_dm = " . $detail_mm['lastid'] . "
		
		";
		//$this->db->insert('t_bpb',$data);
		// echo $query;die;

		$query 	= $this->db->query($query);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_qty_mat_edt($params = array(), $detail_mm)
	{

		$query = "
		
		update m_material a join t_detail_material b on a.id_mat = b.id_material set 
		a.buy_price = (( b.qty * b.buy_price )) / (a.qty + b.qty),
		a.qty = a.qty + " . $params['qty_big'] . ", 
		a.qty_big = a.qty_big + " . $params['qty'] . ",
		a.qty_sol = a.qty_sol + " . $params['qty_sol_big'] . ", 
		a.qty_sol_big = a.qty_sol_big + " . $params['qty_sol'] . ",
		a.qty_bonus = a.qty_bonus + " . $params['qty_bonus_big'] . ", 
		a.qty_bonus_big = a.qty_bonus_big + " . $params['qty_bonus'] . ",
		a.qty_titip = a.qty_titip + " . $params['qty_titip_big'] . ", 
		a.qty_titip_big = a.qty_titip_big + " . $params['qty_titip'] . "
		where id_mat = " . $params['id_material'] . "
		
		";
		//$this->db->insert('t_bpb',$data);
		// echo $query;die;

		$query 	= $this->db->query($query);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function update_buy_price($detail_mm)
	{

		$query = "
		
		UPDATE `m_material` A JOIN (

			SELECT *,(SUM(qty*buy_price) / SUM(qty)) buy_price_s 
			FROM `t_detail_material`
			WHERE `id_material` = " . $detail_mm . "
			GROUP BY id_material

			) B ON A.`id_mat` = B.`id_material`
			SET A.buy_price = B.buy_price_s
		
		";
		//$this->db->insert('t_bpb',$data);
		// echo $query;die;

		$query 	= $this->db->query($query);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function delete_mutasi($params)
	{

		//print_r($params);die;	

		$query = "
		
			DELETE b FROM  t_mutasi b
			JOIN `t_bpb_detail` a ON a.`mutasi_id` = b.`id_mutasi`
			JOIN m_material c ON b.id_stock_awal = c.id_mat
			#SET c.qty = c.qty + b.`mutasi_big`, c.qty_big = c.qty_big + b.`amount_mutasi`
			WHERE `id_bpb` = " . $params . "
		
		";
		//$this->db->insert('t_bpb',$data);


		$query 	= $this->db->query($query);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function delete_detail_mat($params)
	{

		$query = "
		
			DELETE b FROM  t_detail_material b
			JOIN `t_bpb_detail` a ON a.`id_bpb_detail` = b.`id_bpb_detail`
			WHERE `id_bpb` = " . $params . "
		
		";
		//$this->db->insert('t_bpb',$data);


		$query 	= $this->db->query($query);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function delete_detail_bpp($params)
	{

		$query = "
		
			DELETE  FROM  `t_bpb_detail` 
			WHERE `id_bpb` = " . $params . "
		
		";
		//$this->db->insert('t_bpb',$data);


		$query 	= $this->db->query($query);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function delete_bpp($params)
	{

		$query = "
		
			DELETE  FROM  `t_bpb` 
			WHERE `id_bpb` = " . $params . "
		
		";
		//$this->db->insert('t_bpb',$data);


		$query 	= $this->db->query($query);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function get_po_detail_full($params = array())
	{

		// //$query = $this->db->get_where('t_po_mat', array('id_po' => $params['id']));
		// $this->db->select('t_po_mat.*,t_po_mat.qty as qty_order, m_material.stock_code,m_material.id_mat as idm,m_material.stock_name,m_material.base_qty,m_material.unit_box, m_uom.*')
		// #$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
		// ->from('t_po_mat')
		// ->join('m_material', 't_po_mat.id_material = m_material.id_mat')
		// ->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
		// ->where(array('id_po' => $params['id']));
		// $query = $this->db->get();

		$query = "
		
		SELECT a.*,a.qty AS qty_order, b.stock_code,b.id_mat AS idm,b.stock_name,b.base_qty,b.unit_box, c.*, 
		a.qty - IF(SUM(d.`qty`+d.`qty_bonus`+d.`qty_titipan`) IS NULL,0,SUM(d.`qty`+d.`qty_bonus`+d.`qty_titipan`)) AS qty_sisa
		FROM t_po_mat a
		JOIN m_material b ON a.id_material = b.id_mat
		JOIN m_uom c ON b.unit_terkecil = c.id_uom
		LEFT JOIN `t_bpb_detail` d ON d.`id_po_mat` = a.`id_t_ps`
		WHERE a.id_po = " . $params['id'] . " 
		GROUP BY a.id_t_ps
		
		";

		$query 	= $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_po_detail_full_edit($params = array())
	{

		// //$query = $this->db->get_where('t_po_mat', array('id_po' => $params['id']));
		// $this->db->select('t_po_mat.*,t_po_mat.qty as qty_order, m_material.stock_code,m_material.id_mat as idm,m_material.stock_name,m_material.base_qty,m_material.unit_box, m_uom.*')
		// #$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
		// ->from('t_po_mat')
		// ->join('m_material', 't_po_mat.id_material = m_material.id_mat')
		// ->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
		// ->where(array('id_po' => $params['id']));
		// $query = $this->db->get();

		$query = "
		
		SELECT a.*,a.qty AS qty_order, b.stock_code,b.id_mat AS idm,b.stock_name,b.base_qty,b.unit_box, c.*, 
		a.qty - IF(SUM(d.`qty`+d.`qty_bonus`+d.`qty_titipan`) IS NULL,0,SUM(d.`qty`+d.`qty_bonus`+d.`qty_titipan`)) AS qty_sisa,
		d.`qty` AS qty_bpb, d.`qty_bonus` AS qty_bonus_bpb, d.`qty_titipan` AS qty_titip_bpb,d.keterangan as ket_bpb, d.id_bpb_detail 
		FROM t_po_mat a
		JOIN m_material b ON a.id_material = b.id_mat
		JOIN m_uom c ON b.unit_terkecil = c.id_uom
		LEFT JOIN `t_bpb_detail` d ON d.`id_po_mat` = a.`id_t_ps`
		WHERE d.id_bpb = " . $params['id_bpb'] . " 
		GROUP BY a.id_t_ps
		
		";

		$query 	= $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_po_detail_edit($params = array())
	{

		//$query = $this->db->get_where('t_po_mat', array('id_po' => $params['id']));
		$this->db->select('t_po_mat.*,t_po_mat.qty as qty_order, m_material.*, m_uom.*, d_invoice_pembelian.qty as qty_inv,
		d_invoice_pembelian.unit_price as unit_price_inv,d_invoice_pembelian.price as price_inv, d_invoice_pembelian.diskon as diskon_inv')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_po_mat')
			->join('m_material', 't_po_mat.id_material = m_material.id_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->join('d_invoice_pembelian', 'd_invoice_pembelian.id_po_mat = t_po_mat.id_t_ps')
			->join('t_invoice_pembelian', 'd_invoice_pembelian.id_inv_pembelian = t_invoice_pembelian.id_invoice')
			->where(array('t_invoice_pembelian.id_invoice' => $params['id_invoice']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function check_principal($params = array())
	{

		$query = $this->db->get_where('t_eksternal', array('kode_eksternal' => $params['kode']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_bpp($data)
	{

		$this->db->insert('t_bpb', $data);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_mutasi($data)
	{


		$data_mutasi = array(

			'id_stock_awal' => $data['id_stock_awal'],
			'id_stock_akhir' => $data['id_stock_akhir'],
			'date_mutasi' => $data['date_mutasi'],
			'amount_mutasi' =>  $data['mutasi_big'],
			'mutasi_big' => $data['amount_mutasi'],
			'qty_sol' =>  $data['mutasi_big_qty'],
			'qty_sol_big' => $data['amount_mutasi_qty'],
			'qty_bonus' => $data['mutasi_big_bonus'],
			'qty_bonus_big' => $data['amount_mutasi_bonus'],
			'qty_titip' => $data['mutasi_big_titip'],
			'qty_titip_big' => $data['amount_mutasi_titip'],
			'type_mutasi' => $data['type_mutasi'],
			'user_id' => $data['user_id'],
			'note' => 'Pembelian : ' . $data['note']

		);

		$this->db->insert('t_mutasi', $data_mutasi);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_detail_bpp($data)
	{

		//	print_r($datas);die;

		$this->db->insert('t_bpb_detail', $data);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}


	public function add_po($data)
	{
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'no_po' => $data['no_po'],
			'date_po' => $data['date_po'],
			'id_distributor' => $data['id_distributor'],
			'ppn' => $data['ppn'],
			'term_of_payment' => $data['term_of_patyment'],
			'status' => 0,
			'id_valas' => 1,
			'rate' => 1,
			'seq_n' => $data['seq_n'],
			'keterangan' => $data['keterangan'],
			'total_amount' => $data['total_amount']
		);

		//	print_r($datas);die;

		$this->db->insert('t_purchase_order', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_po_mat($data)
	{
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'id_po' => $data['id_po'],
			'id_material' => $data['id_material'],
			'unit_price' => $data['unit_price'],
			'price' => $data['price'],
			'remarks' => $data['remark'],
			'qty' => $data['qty'],
			'diskon' => $data['diskon']
		);

		//	print_r($datas);die;

		$this->db->insert('t_po_mat', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}



	public function edit_qty_material($id_bpb)
	{

		$query = "
			UPDATE `t_bpb_detail` a
			JOIN t_mutasi b ON a.`mutasi_id` = b.`id_mutasi`
			JOIN m_material c ON b.id_stock_awal = c.id_mat
			SET 
			c.qty = c.qty - b.`amount_mutasi`, 
			c.qty_big = c.qty_big - b.`mutasi_big`,
			c.qty_sol = c.qty_sol - b.`qty_sol`,
			c.qty_sol_big = c.qty_sol_big - b.`qty_sol_big`,
			c.qty_bonus = c.qty_bonus - b.`qty_bonus` ,
			c.qty_bonus_big = c.qty_bonus_big - b.`qty_bonus_big`,
			c.qty_titip = c.qty_titip - b.`qty_titip`,
			c.qty_titip_big = c.qty_titip_big - b.`qty_titip_big`
			WHERE `id_bpb_detail` = " . $id_bpb . "
		";

		$query 	= $this->db->query($query);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_qty_material_delete($id_bpb)
	{

		$query = "
			UPDATE `t_bpb_detail` a
			JOIN t_mutasi b ON a.`mutasi_id` = b.`id_mutasi`
			JOIN m_material c ON b.id_stock_awal = c.id_mat
			SET c.qty = c.qty - b.`mutasi_big`, c.qty_big = c.qty_big - b.`amount_mutasi`
			WHERE `id_bpb` = " . $id_bpb . "
		";

		$query 	= $this->db->query($query);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_mutasi_bpb($data_mutasi)
	{

		$query = "
			UPDATE `t_bpb_detail` a
			JOIN t_mutasi b ON a.`mutasi_id` = b.`id_mutasi`
			JOIN m_material c ON b.id_stock_awal = c.id_mat
			SET b.amount_mutasi =  " . $data_mutasi['mutasi_big'] . " , 
			b.mutasi_big = " . $data_mutasi['amount_mutasi'] . ", 
			b.qty_sol =  " . $data_mutasi['mutasi_big_qty'] . ",
			b.qty_sol_big = " . $data_mutasi['amount_mutasi_qty'] . " ,
			b.qty_bonus = " . $data_mutasi['mutasi_big_bonus'] . ",
			b.qty_bonus_big =  " . $data_mutasi['amount_mutasi_bonus'] . " ,
			b.qty_titip =  " . $data_mutasi['mutasi_big_titip'] . ",
			b.qty_titip_big = " . $data_mutasi['amount_mutasi_titip'] . "  ,
			b.note = '" . $data_mutasi['note'] . "'
			WHERE `id_bpb_detail` = " . $data_mutasi['id_bpb_detail'] . "
		";

		$query 	= $this->db->query($query);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_detail_bpp($data_mutasi)
	{

		$query = "
			UPDATE `t_bpb_detail` a
			JOIN t_mutasi b ON a.`mutasi_id` = b.`id_mutasi`
			JOIN m_material c ON b.id_stock_awal = c.id_mat
			SET a.qty = " . $data_mutasi['qty'] . " , a.qty_bonus = " . $data_mutasi['qty_bonus'] . ",
			a.qty_titipan = " . $data_mutasi['qty_titipan'] . ", 
			a.keterangan = '" . $data_mutasi['keterangan'] . "'
			WHERE `id_bpb_detail` = " . $data_mutasi['id_bpb_detail'] . "
		";

		$query 	= $this->db->query($query);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_detail_mat($data_mutasi)
	{

		$query = "
			UPDATE `t_detail_material` 
			SET qty =  " . $data_mutasi['qty_big'] . " , qty_big = " . $data_mutasi['qty'] . ",
			 qty_sol =  " . $data_mutasi['qty_sol_big'] . ", qty_sol_big = " . $data_mutasi['qty_sol'] . ",
			 qty_bonus =  " . $data_mutasi['qty_bonus_big'] . " , qty_bonus_big =" . $data_mutasi['qty_bonus'] . ",
			 qty_titip = " . $data_mutasi['qty_titip_big'] . " , qty_titip_big = " . $data_mutasi['qty_titip'] . "
			WHERE `id_bpb_detail` = " . $data_mutasi['id_bpb_detail'] . "
		";

		$query 	= $this->db->query($query);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	// public function add_detail_mat($data) {


	// $this->db->insert('t_detail_material',$data);


	// //$query 	= $this->db->query($sql,$data);

	// $query = $this->db->query('SELECT LAST_INSERT_ID()');
	// $row = $query->row_array();
	// $lastid = $row['LAST_INSERT_ID()'];

	// $result	= $this->db->affected_rows();

	// $this->db->close();
	// $this->db->initialize();

	// $arr_result['lastid'] = $lastid;
	// $arr_result['result'] = $result;

	// return $arr_result;
	// }	

	public function add_detail_mat($data)
	{


		$query = "
			insert into t_detail_material
			SELECT NULL A," . $data['id_material'] . " B," . $data['id_bpb_detail'] . " C,'" . $data['qty_big'] . "' D ,'" . $data['qty'] . "' E, NULL F,'" . date('Y-m-d H:i:s') . "',b.unit_price amnt,'" . $data['qty_sol_big'] . "' G,'" . $data['qty_sol'] . "' H,'" . $data['qty_bonus'] . "' I,'" . $data['qty_bonus_big'] . "' J,'" . $data['qty_titip_big'] . "' K,'" . $data['qty_titip'] . "' L  FROM `t_bpb_detail` a
			JOIN `t_po_mat` b ON a.`id_po_mat` = b.`id_t_ps`
			WHERE `id_bpb_detail` = " . $data['id_bpb_detail'] . " 
		";

		$query 	= $this->db->query($query);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_inv($data)
	{
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			//'no_po' => $data['no_po'],
			'no_invoice' => $data['no_invoice'],
			'id_po' => $data['id_po'],
			'tanggal_invoice' => $data['tanggal_invoice'],
			'due_date' => $data['due_date'],
			'attention' => $data['attention']
		);

		//	print_r($datas);die;
		$this->db->where('id_invoice', $data['id_invoice']);
		$this->db->update('t_invoice_pembelian', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_bpb($data)
	{
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			//'no_po' => $data['no_po'],
			'no_do' => $data['no_do'],
			'angkutan' => $data['angkutan'],
			'no_polisi' => $data['no_polisi'],
			'id_pic' => $data['id_pic'],
			'tanggal' => $data['tanggal']
		);

		//	print_r($datas);die;
		$this->db->where('id_bpb', $data['id_bpb']);
		$this->db->update('t_bpb', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_po_mat($data)
	{
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			//'no_po' => $data['no_po'],
			'unit_price' => $data['unit_price'],
			'qty' => $data['qty'],
			'price' => $data['price'],
			'diskon' => $data['diskon']
		);

		//	print_r($datas);die;
		$this->db->where('id_t_ps', $data['id_po_mat']);
		$this->db->update('t_po_mat', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_po_apr($data)
	{
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'status' => $data['sts']
		);

		//	print_r($datas);die;
		$this->db->where('id_po', $data['id_po']);
		$this->db->update('t_purchase_order', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	function delete_po($id)
	{

		$this->db->where('id_invoice', $id);
		$this->db->delete('t_invoice_pembelian');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $result;
	}

	function delete_po_mat($id)
	{

		$this->db->where('id_inv_pembelian', $id);
		$this->db->delete('d_invoice_pembelian');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $result;
	}

	function delete_detail_invoiced($id)
	{

		$this->db->where('id_inv_pembelian', $id);
		$this->db->delete('d_invoice_pembelian');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $result;
	}
}
