<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Material_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}


	public function type_material($params = array())
	{
		$sql_all 	= 'CALL type_mate_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query(
			$sql_all,
			array(
				1000,
				0,
				NULL,
				NULL,
				''
			)
		);

		$result = $query_all->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function jenis_material()
	{
		$sql 	= 'SELECT id_coa,id_parent,coa,keterangan FROM t_coa WHERE id_parent = 80300 order by coa';

		$query_all 	=  $this->db->query($sql);

		$result = $query_all->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_distributor()
	{
		$query = $this->db->get_where('t_eksternal', array('type_eksternal' => 1));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}


	public function get_unit()
	{
		$query = $this->db->get('m_uom');

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	public function get_tipe()
	{
		$query = $this->db->get('m_type_material');

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	public function get_material($id_mat)
	{
		$query = $this->db->get_where('m_material', array('id_mat' => $id_mat));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function lists($params = array())
	{
		$query = 	'
					SELECT COUNT(*) AS jumlah
					FROM m_material a
					JOIN m_uom b ON a.unit_terkecil=b.id_uom
					JOIN t_eksternal c ON a.dist_id=c.id
					WHERE  ( 
						stock_code LIKE "%' . $params['searchtxt'] . '%" OR
						stock_name LIKE "%' . $params['searchtxt'] . '%" OR
						name_eksternal LIKE "%' . $params['searchtxt'] . '%" 
					)  
			';

		$query2 = 	'
				SELECT z.*, rank() over ( ORDER BY kode_eksternal, id_mat ASC) AS Rangking from ( 
					
					SELECT a.id_mat,a.stock_code,a.stock_name,
					a.stock_description,a.base_qty,b.uom_name,b.uom_symbol,a.unit_box,c.name_eksternal,c.kode_eksternal 
					FROM m_material a
					JOIN m_uom b ON a.unit_terkecil=b.id_uom
					JOIN t_eksternal c ON a.dist_id=c.id
					WHERE  ( 
					stock_code LIKE "%' . $params['searchtxt'] . '%" OR
					stock_name LIKE "%' . $params['searchtxt'] . '%" OR
					name_eksternal LIKE "%' . $params['searchtxt'] . '%" 
				)  order by kode_eksternal, id_mat) z
				ORDER BY kode_eksternal, id_mat ASC
				
				LIMIT ' . $params['limit'] . ' 
				OFFSET ' . $params['offset'] . ' 
			';

		//echo $query2;die;

		$out		= array();
		$querys		= $this->db->query($query);
		$result = $querys->row();

		$total_filtered = $result->jumlah;
		$total 			= $result->jumlah;

		if (($params['offset'] + 10) > $total_filtered) {
			$limit_data = $total_filtered - $params['offset'];
		} else {
			$limit_data = $params['limit'];
		}


		//echo $query;die;
		//echo $query;die;
		$query2s		= $this->db->query($query2);
		$result2 = $query2s->result_array();
		$return = array(
			'data' => $result2,
			'total_filtered' => $total_filtered,
			'total' => $total,
		);
		return $return;
	}

	public function add_material($data)
	{
		$sql 	= 'CALL material_add(?,?,?,?,?,?,?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql, array(
			"",
			$data['kode_stok'],
			$data['nama_stok'],
			$data['desk_stok'],
			$data['unit'],
			$data['type_material'],
			$data['qty'],
			10,
			$data['detail'],
			$data['gudang'],
			1,
			0,
			0
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];


		//$id = $this->db->insert_id();
		//print_r($lastid);die;

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function new_add_material($data)
	{
		$datas = array(
			'stock_code' => $data['kode_stok'],
			'stock_name' => $data['nama_stok'],
			'stock_description' => $data['desk_stok'],
			'unit_terkecil' => $data['unit'],
			'base_qty' => $data['base_qty'],
			'unit_box' => $data['unit_box'],
			'type' => $data['tipe'],
			'pajak' => $data['pajak'],
			'dist_id' => $data['dist_id'],
			//'type' => 1
		);

		//	print_r($datas);die;

		$this->db->insert('m_material', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function get_komponen($data)
	{
		$sql 	= 'CALL get_komponen(?)';

		$out = array();
		$query 	=  $this->db->query($sql, array(
			$data['detail']
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_komponen2($id, $idm)
	{
		$sql 	= 'CALL get_komponen2(?, ?)';

		$out = array();
		$query 	=  $this->db->query(
			$sql,
			array(
				$id, $idm
			)
		);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function clear_detail($data)
	{
		$sql 	= 'delete from m_detail_values where id_master_prop = ?';

		$out = array();
		$query 	=  $this->db->query(
			$sql,
			array(
				$data['id_type_material']
			)
		);

		//$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		//return $result;

	}

	public function edit_material($data)
	{
		$datas = array(
			'stock_code' => $data['kode_stok'],
			'stock_name' => $data['nama_stok'],
			'stock_description' => $data['desk_stok'],
			'unit_terkecil' => $data['unit'],
			'unit_box' => $data['unit_box'],
			'base_qty' => $data['base_qty'],
			'daily_stock' => isset($data['limit']) && !empty($data['limit']) ? (float) $data['limit'] : null,
			'type' => $data['tipe'],
			'pajak' => $data['pajak'],
			'status' => $data['status'],
			'dist_id' => $data['dist_id']
		);

		//	print_r($datas);die;
		$this->db->where('id_mat', $data['id_mat']);
		$this->db->update('m_material', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_mutasi($data, $id_komp)
	{
		$sql 	= 'CALL mutasi_add(?,?,?)';

		$query 	=  $this->db->query(
			$sql,
			array(
				$id_komp,
				$id_komp,
				$data['harga']
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function deletes($id_mat)
	{
		$datas = array(
			'status' => 2
		);
		$this->db->where('id_mat', $id_mat);
		$this->db->delete('m_material');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}
