    <!--Parsley-->
    <script type="text/javascript" src="<?php echo base_url('assets'); ?>/gentelella-master/vendors/parsleyjs/dist/parsley.min.js"></script>

	<style>
	#tick{display:none}

	#loading-level{display:none}
	#cross{display:none}
	</style>

                    <form class="form-horizontal form-label-left" id="add_eskalasi" role="form" action="<?php echo base_url('users/add_eskalasi');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

                      <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Title <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input data-parsley-maxlength="255" type="text" id="title" name="title" class="form-control col-md-7 col-xs-12" placeholder="Title" required="required">
						</div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="keterangan">Keterangan <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <textarea data-parsley-maxlength="255" type="text" id="keterangan" name="keterangan" class="form-control col-md-7 col-xs-12" placeholder="Keterangan" required="required"/>
						</div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="lokasi_id">Lokasi <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
							<select class="form-control" name="lokasi_id" id="lokasi_id" style="width: 100%" required>
								<option value="" >-- Select  lokasi--</option>
								<?php foreach($lokasi as $key) { ?>
								<option value="<?php echo $key['idlokasi']; ?>" ><?php echo $key['namalokasi']; ?></option>
								<?php } ?>
							</select>
                        </div>
                      </div>
					  
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="level">Level <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input data-parsley-maxlength="100" type="number" id="level" name="level" class="form-control col-md-7 col-xs-12" placeholder="Level" required="required">
						  <span id="loading-level" class="fa fa-spinner fa-spin fa-fw"> Checking level...</span>
						  <span id="cross"></span>
						</div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Eskalasi</button>
                        </div>
                      </div>

                    </form>

        <!-- /page content -->

<script type="text/javascript">
 
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
	});


var lastlevel = $('#level').val();

$('#level').on('input',function(event) {
   if($('#level').val() != lastlevel) {
       level_check();
   }
});

function level_check() {
    var level = $("#level").val();
    var lokasi_id = $("#lokasi_id").val();

    $('#cross').empty();
    $('#cross').hide();
    $('#loading-level').show();

    if (level != "") {
        var post_data = {
          'level': level,
          'lokasi_id': lokasi_id
        };

        jQuery.ajax({
    	   type: "POST",
    	   url: "<?php echo base_url('users/check_level');?>",
    	   data: post_data,
    	   cache: false,
    	   success: function(response){
    			if(response.success == true){
    				$('#level').css('border', '3px #090 solid');
    				$('#loading-level').hide();
    				$('#cross').empty();
    				$("#cross").append('<span class="fa fa-check"> '+response.message+'</span>');
    				$('#cross').show();
    				}else{
    				$('#level').css('border', '3px #C33 solid');
    				$('#loading-level').hide();
    				$('#cross').empty();
    				$("#cross").append('<span class="fa fa-close"> '+response.message+'</span>');
    				$('#cross').show();
    			}
    		}
    	});
    } else {
        $('#level').css('border', '3px #C33 solid');
        $('#loading-mail').hide();
        $('#cross').empty();
        $("#cross").append('<span class="fa fa-close"> Not valid level.</span>');
        $('#cross').show();
    }
    return false;
}

$('#add_eskalasi').on('submit',(function(e) {
    $('#btn-submit').attr('disabled','disabled');
    $('#btn-submit').text("Memasukkan data...");
    e.preventDefault();
    var formData = new FormData(this);

    $.ajax({
        type:'POST',
        url: $(this).attr('action'),
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(response) {
            if (response.success == true) {
                swal({
                  title: 'Success!',
                  text: response.message,
                  type: 'success',
                  showCancelButton: false,
                  confirmButtonText: 'Ok'
                }).then(function () {
                  window.location.href = "<?php echo base_url('users/eskalasi');?>";
                })
            } else{
                $('#btn-submit').removeAttr('disabled');
                $('#btn-submit').text("Tambah Eskalasi");
                swal("Failed!", response.message, "error");
            }
        }
    }).fail(function(xhr, status, message) {
        $('#btn-submit').removeAttr('disabled');
        $('#btn-submit').text("Tambah Eskalasi");
        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
    });
}));
</script>
