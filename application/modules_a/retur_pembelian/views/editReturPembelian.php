<style>
	.dt-body-left {
		text-align: left;
	}

	.dt-body-right {
		text-align: right;
	}

	.dt-body-center {
		text-align: center;
		vertical-align: middle;
	}

	.force-overflow {
		height: 650px;
		overflow-y: auto;
		overflow-x: auto;
	}

	.scroll-overflow {
		min-height: 650px
	}

	#modal-distributor::-webkit-scrollbar-track {
		box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
		-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar {
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar-thumb {
		background-image: -webkit-gradient(linear,
				left bottom,
				left top,
				color-stop(0.44, rgb(122, 153, 217)),
				color-stop(0.72, rgb(73, 125, 189)),
				color-stop(0.86, rgb(28, 58, 148)));
	}

	.select2-container .select2-choice {
		height: 35px !important;
	}
</style>

<div class="product-sales-area mg-tb-30">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="product-payment-inner-st">
					<ul id="myTabedu1" class="tab-review-design">
						<li class="active"><a href="#description">Edit Retur Pembelian</a></li>
					</ul>
					<form id="edit_retur_pembelian" action="<?= base_url('retur_pembelian/update_retur_pembelian'); ?>" class="add-department" autocomplete="off">
						<input type="hidden" name="id_retur_pembelian" id="id_retur_pembelian" value="<?= $returPembelian['id_retur_pembelian']; ?>">
						<div id="myTabContent" class="tab-content custom-product-edit">
							<div class="product-tab-list tab-pane fade active in" id="description">
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="review-content-section">
											<div class="row">
												<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
													<div class="form-group">
														<label>Nomor Pembelian</label>
														<select name="id_pembelian" id="id_pembelian" onChange="populate_detail_pembelian()" class="form-control" placeholder="Nama Principle">
															<option value="" selected="selected" disabled>-- Pilih Pembelian --</option>
															<?php foreach ($list_pembelian as $pembelian) : ?>
																<?php if ($pembelian['id_pembelian'] == $returPembelian['id_pembelian']) : ?>
																	<option value="<?= $pembelian['id_pembelian']; ?>" selected><?= implode(' - ', [$pembelian['no_pembelian'], $pembelian['principle']]); ?></option>
																<?php else : ?>
																	<option value="<?= $pembelian['id_pembelian']; ?>"><?= implode(' - ', [$pembelian['no_pembelian'], $pembelian['principle']]); ?></option>
																<?php endif; ?>
															<?php endforeach; ?>
														</select>
													</div>

													<div class="form-group date">
														<label>Tanggal Pembelian</label>
														<input name="tgl" id="tgl" type="text" class="form-control" placeholder="Tanggal Penjualan" value="<?= $returPembelian['date_po'] ?? ''; ?>" readonly disabled>
													</div>
													<div class="form-group">
														<label>Principle</label>
														<input name="principle" id="principle" type="text" class="form-control" placeholder="Principle" value="<?= $returPembelian['name_eksternal'] ?? ''; ?>" readonly>
													</div>
													<div class="form-group">
														<label>Alamat</label>
														<textarea name="alamat" id="alamat" type="text" class="form-control" placeholder="Alamat Principal" readonly><?= $returPembelian['eksternal_address'] ?? ''; ?></textarea>
													</div>
													<!-- <div class="form-group">
														<label>Att</label>
														<input name="att" id="att" type="text" class="form-control" placeholder="Att" readonly>
													</div> -->
													<div class="form-group">
														<label>No. Telp</label>
														<input name="telp" id="telp" type="text" class="form-control" placeholder="Telephone" value="<?= $returPembelian['phone_1']; ?>" readonly>
													</div>
													<div class="form-group">
														<label>Email</label>
														<input name="email" id="email" type="text" class="form-control" placeholder="Email" value="<?= $returPembelian['email']; ?>" readonly>
													</div>
													<!-- <div class="form-group">
														<label>Limit Kredit</label>
														<input name="limit" id="limit" type="text" class="form-control" placeholder="Limit Kredit" readonly>
													</div>
													<div class="form-group">
														<label>Sisa Kredit</label>
														<input name="sisa" id="sisa" type="text" class="form-control" placeholder="Sisa Kredit" readonly>
													</div> -->
												</div>
												<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
													<div class="form-group">
														<label>Ship From:</label>
														<textarea name="alamat_kirim" id="alamat_kirim" type="text" class="form-control" placeholder="Alamat Principal" readonly>JL. Sangkuriang NO.38-A NPWP: 01.555.161.7.428.000</textarea>
													</div>
													<div class="form-group date">
														<label>Email</label>
														<input name="emaila" id="emaila" type="email" class="form-control" placeholder="Email" readonly>
													</div>
													<div class="form-group date">
														<label>Telp/Fax</label>
														<input name="telpfax" id="telpfax" type="text" class="form-control" placeholder="Telp/Fax" value="(022) 6626-946" readonly>
													</div>

													<input name="ppn" id="ppn" type="hidden" class="form-control" placeholder="VAT/PPN" onKeyup="change_ppn()" value="10">

													<div class="form-group date">
														<label>Term Pembayaran</label>
														<input name="term" id="term" type="hidden" class="form-control" placeholder="Term (Hari)" value="<?= $returPembelian['term_of_payment'] ?? 0; ?>" style="" readonly>
														<input name="term_o" id="term_o" type="number" class="form-control" placeholder="Term (Hari)" value="<?= $returPembelian['term_of_payment'] ?? 0; ?>" style="" readonly>
													</div>

													<div class="form-group date">
														<label>Keterangan Beli</label>
														<textarea name="keterangan_beli" id="ket" class="form-control" placeholder="keterangan" readonly></textarea>
													</div>

													<div class="form-group date">
														<label>Keterangan Retur</label>
														<textarea name="keterangan_retur" id="keterangan_retur" class="form-control" placeholder="keterangan"><?= $returPembelian['keterangan_retur'] ?? ''; ?></textarea>
													</div>
												</div>
											</div>

										</div>
									</div>
								</div>
								<br><br>
								<div class="row">
									<h3>Items</h3>
								</div>

								<div class="row">
									<div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
										<div class="form-group">
											<label>Kode</label>
										</div>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
										<div class="form-group">
											<label>Qty</label>
										</div>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 text-center">
										<div class="form-group">
											<label>Harga</label>
										</div>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
										<div class="form-group">
											<label>Retur Qty</label>
										</div>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
										<div class="form-group">
											<label>Sisa Qty</label>
										</div>
									</div>
									<!-- <div class="col-lg-3 col-md-2 col-sm-3 col-xs-12 text-center">
									<div class="form-group">
										<label>Harga Retur</label>
									</div>
								</div> -->
								</div>
								<input name="total_items" id="total_items" type="hidden" value="<?= count($detailReturPembelian); ?>" />
								<div id="table_items">
									<?php
									$ci = get_instance();
									foreach ($detailReturPembelian as $index => $item) {
										$ci->load->view('../modules/retur_pembelian/views/partials/items_row', ['index' => $index, 'item' => $item]);
									}
									?>
								</div>
								<br>

								<!-- <div class="row" style="border-top-style:solid;">
								<div style="margin-top:10px">
									<div class="col-lg-8 col-md-3 col-sm-3 col-xs-12">

									</div>
									<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
										<label>Total Harga Retur</label>
									</div>
									<div class="col-lg-2 col-md-1 col-sm-3 col-xs-12">
										<label id="grand_total_label" style="float:right">0</label>
										<input type="hidden" name="grand_total" value="0" />
									</div>
									<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
									</div>
								</div>
							</div> -->

								<div class="row">
									<div class="col-lg-12">
										<div class="payment-adress">
											<button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
											<button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()"> Batal</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	function indonesia_currency_format(value, printSymbol = false) {
		var options = {};

		if (printSymbol) {
			options = {
				style: 'currency',
				currency: 'IDR'
			}
		}

		value = value.toFixed(2);
		remain = value % 1;

		value = new Intl.NumberFormat('id-ID', options).format(value);

		if (remain <= 0) {
			value = value + ',00';
		}

		return value;
	}

	function populate_detail_pembelian() {
		var datapost = {
			id_pembelian: $('#id_pembelian').val()
		};

		$.ajax({
			type: 'POST',
			url: "<?= base_url('retur_pembelian/get_detail_pembelian'); ?>",
			data: JSON.stringify(datapost),
			cache: false,
			contentType: false,
			processData: false,
			success: function(response) {

				$('#table_items').html('');

				if (response.data) {
					if (response.data.pembelian) {
						$('#tgl').val(response.data.pembelian.purchase_date || '');
						$('#principle').val(response.data.pembelian.principle || '');
						$('#alamat').val(response.data.pembelian.principle_address || '');
						$('#telp').val(response.data.pembelian.principle_phone || '');
						$('#email').val(response.data.pembelian.principle_email || '');
						$('#term, #term_o').val(response.data.pembelian.purchase_term || '');
						$('#keterangan_beli').val(response.data.pembelian.purchase_note || '');
					}

					if (response.data.items) {
						$('#table_items').html(response.data.items);
					}

					$('#total_items').val(response.data.total_items);
					// calculate_items();
				}
			}
		});
	}

	function change_item_qty(idx) {
		var qty = parseFloat($('#qty_' + idx).val());
		var qty_retur = parseFloat($('#qty_retur_' + idx).val());

		$('#total_qty_' + idx + '_label').val(indonesia_currency_format(qty - qty_retur));
		$('#total_qty_' + idx).val(qty - qty_retur);

		// calculate_item(idx);
	}

	function back() {
		window.location.href = "<?= base_url('retur_pembelian'); ?>";
	}

	$(document).ready(function() {
		$('#id_pembelian').select2();

		$('.rupiah').priceFormat({
			prefix: '',
			centsSeparator: ',',
			centsLimit: 0,
			thousandsSeparator: '.'
		});

		$('#tgl').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		});

		$('#edit_retur_pembelian').on('submit', function(e) {

			e.preventDefault();

			var formUrl = $(this).attr('action');
			var formData = new FormData();
			var totalItems = parseInt($('#total_items').val() || 0);

			formData.append('id_retur_pembelian', $('#id_retur_pembelian').val() || '');
			formData.append('id_pembelian', $('#id_pembelian').val() || '');
			formData.append('keterangan_retur', $('#keterangan_retur').val() || '');
			formData.append('total_items', totalItems);

			for (var idx = 0; idx < totalItems; idx++) {
				formData.append('id_t_ps_' + idx, $('#id_t_ps_' + idx).val());
				formData.append('qty_retur_' + idx, $('#qty_retur_' + idx).val());
			}

			swal({
				title: 'Yakin akan Simpan Data ?',
				text: '',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak'
			}).then(function() {

				$.ajax({
					type: 'POST',
					url: formUrl,
					data: formData,
					cache: false,
					contentType: false,
					processData: false,
					success: function(response) {

						if (response.success == true) {

							swal({
								title: 'Success!',
								text: "Data berhasil disimpan",
								type: 'success',
								showCancelButton: true,
								confirmButtonText: 'Ya',
								cancelButtonText: 'Tidak'
							});

						} else {
							swal({
								title: 'Data Inputan Tidak Sesuai !! ',
								html: response.message,
								type: 'error',
								showCancelButton: true,
								confirmButtonText: 'Ok'
							});
						}
					}
				});

			});
		});
	});
</script>