<style>
	.dt-body-left {text-align:left;}
	.dt-body-right {text-align:right;}
	.dt-body-center {text-align:center; vertical-align: middle;}
	.force-overflow {height: 650px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow {min-height: 650px}
	#modal-distributor::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar
	{
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar-thumb
	{
		background-image: -webkit-gradient(linear,
		   left bottom,
		   left top,
		   color-stop(0.44, rgb(122,153,217)),
		   color-stop(0.72, rgb(73,125,189)),
		   color-stop(0.86, rgb(28,58,148)));
	}
	
	.select2-container .select2-choice {
		
		height : 35px !important;
		
	}

</style>

 <div class="product-sales-area mg-tb-30">
            <div class="container-fluid">
                        <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            <ul id="myTabedu1" class="tab-review-design">
                                <li class="active"><a href="#description">Pambayaran Piutang</a></li>
                            </ul>
                            <div id="myTabContent" class="tab-content custom-product-edit">
                                <div class="product-tab-list tab-pane fade active in" id="description">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="review-content-section">
                                                <form id="add_po" action="<?php echo base_url().'piutang/edit_bayar';?>" class="add-department" autocomplete="off">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<div class="form-group">
																<label>Kode</label>
                                                                <input name="kode" id="kode" type="text" class="form-control" placeholder="Kode Purchase Order" value="<?php echo $date_detail['no_payment']; ?>"  >
                                                            </div>
															<div class="form-group date">
																<label>Tanggal</label>
																<input name="tgl" id="tgl" type="text" class="form-control" value="<?php echo $date_detail['date']; ?>" placeholder="Tanggal Pembayaran"  >
                                                            </div>
                                                            <div class="form-group">
																<label>Customer</label>
																<input name="ints" id="ints" type="hidden" class="form-control" placeholder="Tanggal Purchase Order" value= '<?php echo count($date_detail_item); ?>' >
																<input name="ints2" id="ints2" type="hidden" class="form-control" placeholder="Tanggal Purchase Order" value= '<?php echo count($date_detail_item); ?>' >
                                                                <select name="name" id="name" onChange="select_prin()" class="form-control" placeholder="Nama Principal" >
																	<option value="0" selected="selected" disabled>-- Pilih Customer --</option>
																	<?php
																		foreach($principal as $principals){
																			
																			if($principals['id_t_cust'] == $cust['id_customer']){
																			
																				echo '<option value="'.$principals['id_t_cust'].'" selected >'.$principals['cust_name'].'</option>';
																			
																			}else{
																				echo '<option value="'.$principals['id_t_cust'].'" >'.$principals['cust_name'].'</option>';
																			}
																		}
																	?>
																</select>
                                                            </div>
															<div class="form-group date">
																<label>Tipe Pembayaran</label>
																 <select name="term" id="term" class="form-control" placeholder="Nama Principal" onChange="term_other()"  >
																	<option value="0" selected="selected" disabled>-- Pilih Pembayaran --</option>
																	<option value="Cash/Transfer" >Cash/Transfer</option>
																	<option value="Giro" >Giro</option>
																	<option value="Beban" >Beban</option>
																	<option value="Diskon" >Diskon Penjualan</option>
																</select>
																
                                                            </div>
                                                        </div>
														 <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<div class="form-group date">
															<label>Cash Account</label>
																<select name="casha" id="casha" class="form-control" placeholder="Cash Account" >
																	<option value="0" selected="selected"  disabled>-- Pilih Cash Account --</option>
																	<?php foreach($cash_account as $cash_accounts){ ?>
																		
																		<option value="<?php echo $cash_accounts['id_coa']; ?>" ><?php echo $cash_accounts['keterangan']; ?></option>
																		
																	<?php } ?>
																</select>
															</div>
															
																	<div class="form-group">
																		<label>Tanggal Terbit</label>
																		<input name="tglterbit" id="tglterbit" type="text" class="form-control"  placeholder="" value="<?php echo $date_detail['tgl_aktif']; ?>"  >
																	</div>

																	<div class="form-group">
																		<label>Jatuh Tempo</label>
																		<input name="tgljatuhtempo" id="tgljatuhtempo" type="text" class="form-control"  placeholder="" value="<?php echo $date_detail['tgl_jatuh_tempo']; ?>"  >
																	</div>
																	<div class="form-group">
																		<label>Keterangan</label>
																		<textarea name="keterangan" id="keterangan" type="text" class="form-control"  placeholder="Keterangan"  ><?php echo $date_detail['note']; ?></textarea>
																	</div>
																	
															
														 </div>
                                                    
													</div>

                                                    
                                               
                                            </div>
                                        </div>
                                    </div>
									<br><br>
									<div class="row">
										<h3>Invoice</h3>
									</div>
									
										<div class="row">
											<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Invoice</label>
												</div>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Nilai</label>
												</div>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Sisa</label>
												</div>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Terbayar</label>
												</div>
											</div>
											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
												<div class="form-group">
													<label></label>
												</div>
											</div>
										</div>
											<input name="int_flo" id="int_flo" type="hidden" class="form-control" placeholder="Qty" value=<?php echo count($date_detail_item); ?>; >
									<div id="table_items">
									
										<?php $rr = 0;$totals = 0; foreach($date_detail_item as $date_detail_items){ ?>
									
										<div id='row_0' >
											<div class="row" style="border-top-style:solid;">
											<div style="margin-top:10px">
												<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
													<div class="form-group">
														<select name="kode_<?php echo $rr; ?>" id="kode_<?php echo $rr; ?>" class="form-control" placeholder="Item" onChange=inv_val(0)>
															<option></option>
															<?php //echo $list; 
															
															
																foreach($invoice as $items){ 
																
																		if($items['id_invoice'] == $date_detail_items['id_invoice']){
																			echo '<option value="'.$items['id_invoice'].'|'.$items['bayar'].'|'.$items['sisa'].'|'.$items['amount'].'" selected >'.$items['no_invoice'].'</option>';
																		}else{
																			echo '<option value="'.$items['id_invoice'].'|'.$items['bayar'].'|'.$items['sisa'].'|'.$items['amount'].'" >'.$items['no_invoice'].'</option>';
																		}
																
																} ?>
															
														</select>
													</div>
													
												</div>
												<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
													<div class="form-group">
														<input name="jt_<?php echo $rr; ?>" id="jt_<?php echo $rr; ?>" type="text"  class="form-control " placeholder="" value="<?php echo number_format($date_detail_items['total_amount'],2,',','.'); ?>" readOnly >
													</div>
												</div>
												<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
													<div class="form-group">
														<input name="sisa_<?php echo $rr; ?>" id="sisa_<?php echo $rr; ?>" type="text" onKeyup="change_sum(<?php echo $rr; ?>)" class="form-control" placeholder="Sisa" value="<?php echo number_format($date_detail_items['total_amount']-$date_detail_items['bayar'],2,',','.'); ?>"  readOnly  >
													</div>
												</div>
												<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
													<div class="form-group">
														<input name="bayar_<?php echo $rr; ?>" id="bayar_<?php echo $rr; ?>" type="text" class="form-control" placeholder="Terbayar" value="<?php echo number_format($date_detail_items['ammount'],2,',','.'); ?>"  onKeyup="change_sum(<?php echo $rr; ?>)"  >
													</div>
												</div>
												
											</div>
											</div>
										</div>
										
										<?php $totals = $totals + $date_detail_items['ammount']; $rr++; } ?>
										
									</div>
									<div class="row" style="border-top-style:solid;" id='plus' >
									<br>
										<div class="col-lg-11 col-md-11 col-sm-3 col-xs-12">
										</div>
											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
											
													<div class="form-group">
														<button type="button" class="btn btn-danger waves-effect waves-light tbh_btn" onClick="tambah_row()">+</button>
													</div>
												</div>
									</div>
									<br>
								
									<div class="row" style="border-top-style:solid;">
											<div style="margin-top:10px">
												<div class="col-lg-8 col-md-3 col-sm-3 col-xs-12">
													
												</div>
												<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
													<label>Total</label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<label id="grand_total" style="float:right"><?php echo number_format($totals,2,',','.'); ?></label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
												</div>
											</div>
									</div>
									<div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="payment-adress">
                                                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
																<button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()"> Batal</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                </div>
                               </div> </form>
                        </div>
                    </div>
                </div>
</div>
</div>

                        <div id="ModalAddPo" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-close-area modal-close-df">
                                        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                                    </div>
                                    <div class="modal-body">
                                        <i class="educate-icon educate-checked modal-check-pro"></i>
                                        <h2>Data Berhasil Disimpan</h2>
                                        <p>Apakah Anda Ingin Menambah Data Po Lagi ?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <a data-dismiss="modal" href="#" onClick="back()">Tidak</a>
                                        <a data-dismiss="modal" href="#" onClick="clearform()" >Ya</a>
                                    </div>
                                </div>
                            </div>
                        </div>
						
						<div id="ModalChangePrincipal" class="modal modal-edu-general Customwidth-popup-WarningModal PrimaryModal-bgcolor fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-close-area modal-close-df">
                                        <!--<a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>-->
                                    </div>
                                    <div class="modal-body">
                                        <span class="educate-icon educate-warning modal-check-pro information-icon-pro"></span>
                                        <h2>Warning!</h2>
                                        <p id="msg_err">The Modal plugin is a dialog box/popup window that is displayed on top of the current page</p>
                                    </div>
                                    <div class="modal-footer footer-modal-admin warning-md">
                                        <a data-dismiss="modal" href="#" onClick="okchange()">Ok</a>
                                        <a data-dismiss="modal" href="#" onClick="cancelchange()">Batal</a>
                                    </div>
                                </div>
                            </div>
                        </div>

<script>



function  change_kode(rs){
	
	  var datapost = {
		"kode": $('#kode_'+rs+'').val()
      };
	
	$.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'piutang/get_price_mat'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
			
			$('#harga_'+rs+'').val(response.list);
			
	    }
    });
}

function inv_val(vals){
	
	var id_invoice = $('#kode_'+vals).val();
	
	var datas = id_invoice.split("|");

		var tots = datas[3];
			// var tots = tots.replace('.','');
			// var tots = tots.replace('.','');
			// var tots = tots.replace('.','');
			// var tots = tots.replace('.','');
			
			var prcs = datas[2];
			// var prcs = prcs.replace('.','');
			// var prcs = prcs.replace('.','');
			// var prcs = prcs.replace('.','');
			// var prcs = prcs.replace('.','');
			
			var total_bds = new Intl.NumberFormat('de-DE', { minimumFractionDigits: 2}).format(tots);
			var total_sub_disk = new Intl.NumberFormat('de-DE', { minimumFractionDigits: 2}).format(prcs);

	$('#jt_'+vals).val(total_bds);
	$('#sisa_'+vals).val(total_sub_disk);
	$('#bayar_'+vals).val(total_sub_disk);
	
	change_sum(vals); 
	  // var datapost = {
		// "id_invoice": $('#kode_'+vals).val()
      // };
	
	  // $.ajax({
        // type: 'POST',
        // url: "<?php echo base_url() . 'piutang/get_invoice_detail'; ?>",
        // data: JSON.stringify(datapost),
        // cache: false,
        // contentType: false,
        // processData: false,
        // success: function(response) {

			
        // }
      // });
	
}

function tambah_row(){
	
	$('.hapus_btn').hide();
	var values = $('#int_flo').val();
	
	var new_fl = parseInt(values)+1;
	
	$('#int_flo').val(new_fl);
   var datapost = {
		"id_prin": $('#name').val()
      };

      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'piutang/get_invoice'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {

		
			var htl = '';
		htl +=	'											<div id="row_'+new_fl+'" >';
		htl +=	'									<div class="row" style="border-top-style:solid;">';
		htl +=	'									<div style="margin-top:10px">';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'												<select name="kode_'+new_fl+'" id="kode_'+new_fl+'" class="form-control" placeholder="Nama Principal" onChange=inv_val('+new_fl+') >';
		htl +=	'													<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		htl +=	'												</select>';
													
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="jt_'+new_fl+'" id="jt_'+new_fl+'" type="text"  class="form-control " placeholder="" value=""  readOnly>';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="sisa_'+new_fl+'" id="sisa_'+new_fl+'" type="text" onKeyup="change_sum('+new_fl+')" class="form-control rupiahs" placeholder="Sisa" value=0  readOnly  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="bayar_'+new_fl+'" id="bayar_'+new_fl+'" type="text" class="form-control rupiahs" placeholder="Terbayar" value=0 onKeyup="change_sum(0)"  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<button type="button" class="btn btn-danger waves-effect waves-light " onClick="kurang_row('+new_fl+')">-</button>';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'									</div>';
		htl +=	'									</div>';
		htl +=	'								</div>';
		
	
	
		$('#table_items').append(htl);
			
			$('#kode_'+new_fl+'').html(response.list);
			
			$('#kode_'+new_fl).select2();
			//$("kode_'+new_fl).css("background-color", "yellow");
			
			$(".rupiahs").inputFilter(function(value) {
			return /^-?\d*[,]?\d*$/.test(value); 
		});
			
        }
      });
	
}

function cancelchange(){
	
	var prin = $('#ints2').val();
	$('#ints').val(prin);
	$('#name').val(prin);
	
}

function okchange(){

	   var datapost = {
		"id_prin": $('#name').val()
      };

      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'piutang/get_invoice'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
			//var obj = JSON.parse(response);
			// $('#alamat').val(response['principal'][0].eksternal_address);
			// $('#att').val(response['principal'][0].pic);
			// $('#telp').val(response['principal'][0].phone_1);
			// $('#fax').val(response['principal'][0].fax);
			//$('#kode').val(response['code']);
			//console.log(response.phone_1);
			
		$('#table_items').html('')	
		
		var htl = '';
		htl +=	'											<div id="row_0" >';
		htl +=	'									<div class="row" style="border-top-style:solid;">';
		htl +=	'									<div style="margin-top:10px">';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'												<select name="kode_0" id="kode_0" class="form-control" placeholder="Nama Principal" onChange=inv_val(0)>';
		htl +=	'													<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		htl +=	'												</select>';
													
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="jt_0" id="jt_0" type="text"  class="form-control " placeholder="" value="0"  readOnly>';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="sisa_0" id="sisa_0" type="text" onKeyup="change_sum(0)" class="form-control" placeholder="Sisa" value=0  readOnly  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="bayar_0" id="bayar_0" type="text" class="form-control rupiahs" placeholder="Terbayar" value=0 onKeyup="change_sum(0)"  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
												
		htl +=	'									</div>';
		htl +=	'									</div>';
		htl +=	'								</div>';
	
		$('#table_items').append(htl);
			
			$('#kode_0').html(response.list);
			
			$('#kode_0').select2();
			
			$(".rupiahs").inputFilter(function(value) {
			return /^-?\d*[,]?\d*$/.test(value); 
		});
			
        }
      });
	  
}

function lainnya(int_fal){
	
	 $('#itemk_'+int_fal).show();
	$('#iteml_'+int_fal).hide();
	
	 var datapost = {
		"id_prin": $('#name').val()
      };
	
	  $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'piutang/get_all_item'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {

			$('#kode_'+int_fal).html(response.list);
			
			$('#kode_'+int_fal).select2();
        }
      });
	
}


function kembali(int_fal){
	
	 $('#itemk_'+int_fal).hide();
	$('#iteml_'+int_fal).show();
	
	
	 var datapost = {
		"id_prin": $('#name').val()
      };
	
	  $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'piutang/get_prin_item'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {

			$('#kode_'+int_fal).html(response.list);
			
			$('#kode_'+int_fal).select2();
        }
      });
	
}


function select_prin(){
	
	var prin = $('#ints').val();
	var prin2 = $('#name').val();
	
	$('#ints2').val(prin);
//	alert(prin2);
	$('#ints').val(prin2);

	if(prin !== '0'){
		
		$('#msg_err').html('Jika Pilih Principal Lain, Data Yang Tidak Tersimpan Akan Terhapus, Yakin Akan Melanjutkan ? ');
		$('#ModalChangePrincipal').modal({backdrop: 'static', keyboard: false});
	}else{
		
	  var datapost = {
		"id_prin": $('#name').val()
      };

      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'piutang/get_invoice'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
			//var obj = JSON.parse(response);
			// $('#alamat').val(response['principal'][0].eksternal_address);
			// $('#att').val(response['principal'][0].pic);
			// $('#telp').val(response['principal'][0].phone_1);
			// $('#fax').val(response['principal'][0].fax);
			//console.log(response.phone_1);
			
			
			$('#table_items').html('')	
		
			var htl = '';
			// htl +=	'<div id="row_0" >';
			// htl +=	'									<div class="row" style="border-top-style:solid;">';
			// htl +=	'									<div style="margin-top:10px">';
			// htl +=	'										<div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">';
// htl +=	'<div class="form-group col-lg-10 col-md-10" style="margin-left:-15px">';
// htl +=	'														<select name="kode_0" id="kode_0" class="form-control" onChange="change_kode(0)" placeholder="Nama Principal">';
// htl +=	'															<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
// htl +=	'														</select>';
// htl +=	'													</div>';
// htl +=	'													<div class="form-group col-lg-2 col-md-2" style="margin-left:-25px">';
// htl +=	'														<button type="button" id="iteml_0" class="btn btn-danger waves-effect waves-light tbh_btn" onClick="lainnya(0)">Lainnya</button>';
// htl +=	'														<button type="button" id="itemk_0" class="btn btn-danger waves-effect waves-light tbh_btn" style="display:none" onClick="kembali(0)">Kembali</button>';

// htl +=	'													</div>';
			// htl +=	'										</div>';
			// htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			// htl +=	'											<div class="form-group">';
			// htl +=	'												<input name="qty_0" id="qty_0" type="text" class="form-control rupiahs" onKeyup="change_sum(0)" placeholder="Qty" value=0 >';
			// htl +=	'											</div>';
			// htl +=	'										</div>';
			// htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			// htl +=	'											<div class="form-group">';
			// htl +=	'												<input name="harga_0" id="harga_0" type="text" class="form-control rupiahs" onKeyup="change_sum(0)" placeholder="Harga" value=0 >';
			// htl +=	'											</div>';
			// htl +=	'										</div>';
			// htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			// htl +=	'											<div class="form-group">';
			// htl +=	'												<input name="diskon_0" id="diskon_0" type="text" class="form-control rupiahs" onKeyup="change_sum(0)" placeholder="Diskon" value=0 >';
			// htl +=	'											</div>';
			// htl +=	'										</div>';
			// htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			// htl +=	'											<div class="form-group">';
			// htl +=	'												<input name="total_0" id="total_0" type="text" class="form-control rupiahs" placeholder="Total" value=0 readOnly  >';
			// htl +=	'											</div>';
			// htl +=	'										</div>';
			// htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			// htl +=	'											<div class="form-group">';
			// htl +=	'												';
			// htl +=	'											</div>';
			// htl +=	'										</div>';
			// htl +=	'									</div>';
			// htl +=	'									</div>';
			// htl +=	'								</div>';
			
		htl +=	'											<div id="row_0" >';
		htl +=	'									<div class="row" style="border-top-style:solid;">';
		htl +=	'									<div style="margin-top:10px">';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'												<select name="kode_0" id="kode_0" class="form-control" placeholder="Nama Principal" onChange=inv_val(0)>';
		htl +=	'													<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		htl +=	'												</select>';
													
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="jt_0" id="jt_0" type="text"  class="form-control " placeholder="" value=""  readOnly>';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="sisa_0" id="sisa_0" type="text" onKeyup="change_sum(0)" class="form-control rupiahs" placeholder="Sisa" value=0  readOnly  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="bayar_0" id="bayar_0" type="text" class="form-control rupiahs" placeholder="Terbayar" value=0 onKeyup="change_sum(0)"  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
												
		htl +=	'									</div>';
		htl +=	'									</div>';
		htl +=	'								</div>';
		
			$('#table_items').append(htl);
			$('#kode_0').html(response.list);
			
			$('#kode_0').select2();
			
			//$($("#kode_0").select2("container")).addClass("form-control");
			//$("#kode_0").select2({ height: '300px' });	
			
			$(".rupiahs").inputFilter(function(value) {
			return /^-?\d*[,]?\d*$/.test(value); 
		});
        }
      });
		
	}
	
	//$('#ints').val('1');
	
}

function kurang_row(new_fl){
	
	$('#row_'+new_fl).remove();
	
	var int_val = parseInt($('#int_flo').val());
	var sub_total = 0;
	
	for(var yo = 0;yo <= int_val;yo++ ){
				
		if($('#kode_'+yo).val() !== undefined){
			
			var tots = $('#bayar_'+yo).val();
			var tots = tots.replace('.','');
			var tots = tots.replace('.','');
			var tots = tots.replace('.','');
			var tots = tots.replace('.','');
			var tots = tots.replace(',','.');
			
				sub_total = sub_total +	parseFloat(tots);
				
		}
				
	}
	
	
	var sub_totals = new Intl.NumberFormat('de-DE', {minimumFractionDigits: 2 }).format(sub_total);
	$('#grand_total').html(sub_totals);
	
}

function clearform(){
	
	var datapost = {
		"id_prin": $('#name').val()
      };

      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'piutang/get_code'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
		
			$('#add_po').trigger("reset");
	$('#table_items').html('')	
		
			var htl = '';
			htl +=	'											<div id="row_0" >';
		htl +=	'									<div class="row" style="border-top-style:solid;">';
		htl +=	'									<div style="margin-top:10px">';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'												<select name="kode_0" id="kode_0" class="form-control" placeholder="Nama Principal" onChange=inv_val(0)>';
		htl +=	'													<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		htl +=	'												</select>';
													
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="jt_0" id="jt_0" type="text"  class="form-control " placeholder="" value=""  readOnly>';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="sisa_0" id="sisa_0" type="text" onKeyup="change_sum(0)" class="form-control rupiahs" placeholder="Sisa" value=0  readOnly  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="bayar_0" id="bayar_0" type="text" class="form-control rupiahs" placeholder="Terbayar" value=0 onKeyup="change_sum(0)"  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
												
		htl +=	'									</div>';
		htl +=	'									</div>';
		htl +=	'								</div>';
		
		
			$('#table_items').append(htl);
			
			$('#kode_0').select2();
			$('#kode').val(response['kode']);
			
			$(".rupiahs").inputFilter(function(value) {
			return /^-?\d*[,]?\d*$/.test(value); 
		});
			
			
		
		}
    });

}

function change_ppn(){
	
	var sub_total = 0;
	var sub_total_bd = 0;
	var sub_disk = 0;
	var int_val = parseInt($('#int_flo').val());
		
	for(var yo = 0;yo <= int_val;yo++ ){
				
		if($('#kode_'+yo).val() !== undefined){
			
			var tots = $('#total_'+yo).val();
			var tots = tots.replace('.','');
			var tots = tots.replace('.','');
			var tots = tots.replace('.','');
			var tots = tots.replace('.','');
			
			var prcs = $('#harga_'+yo).val();
			var prcs = prcs.replace('.','');
			var prcs = prcs.replace('.','');
			var prcs = prcs.replace('.','');
			var prcs = prcs.replace('.','');
			
			var qtys = $('#qty_'+yo).val();
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			
			
			
				sub_total = sub_total +	parseFloat(tots);
				sub_total_bd = sub_total_bd + (parseFloat(prcs)*parseFloat(qtys) );
				sub_disk = sub_disk + ( (parseFloat(prcs)*parseFloat(qtys))*(parseFloat($('#diskon_'+yo).val())/100) );
		}
				
	}
	
	if($('#ppn').val() == ''){
		var ppns = 0;
	}else{
		var ppns = $('#ppn').val();
	}
	
	var total_ppn = (sub_total * (parseFloat(ppns)/100 ));
	var grand_total = sub_total + (sub_total * (parseFloat(ppns)/100 ));
	
	var gt = new Intl.NumberFormat('de-DE', {minimumFractionDigits: 2 }).format(grand_total);
	var sub_totals = new Intl.NumberFormat('de-DE', {minimumFractionDigits: 2 }).format(sub_total);
	var total_ppns = new Intl.NumberFormat('de-DE', { minimumFractionDigits: 2}).format(total_ppn);
	var total_bds = new Intl.NumberFormat('de-DE', { minimumFractionDigits: 2}).format(sub_total_bd);
	var total_sub_disk = new Intl.NumberFormat('de-DE', { minimumFractionDigits: 2}).format(sub_disk);
	
	$('#vatppn').html(total_ppns);
	$('#grand_total').html(gt);
	
}

function change_sum(fl){
	
	var harga = 	$('#bayar_'+fl).val();

	
	var harga = harga.replace('.','');
	var harga = harga.replace('.','');
	var harga = harga.replace('.','');
	var harga = harga.replace('.','');
	var harga = harga.replace(',','.');
	
	
	
	// if( isNaN(total)){
		// //var total_num = Number((total).toFixed(1)).toLocaleString();
		// //$('#subttl').html('0');
		// $('#total_'+fl).val('0');
	// }else{
		// //var total_num = Number((total).toFixed(1)).toLocaleString();
		// //$('#subttl').html(total);
		// $('#total_'+fl).val(totals);
	// }
	
	var sub_total = 0;
	var sub_total_bd = 0;
	var sub_disk = 0;
	var int_val = parseInt($('#int_flo').val());
		
	for(var yo = 0;yo <= int_val;yo++ ){
				
		if($('#kode_'+yo).val() !== undefined){
			
			var tots = $('#bayar_'+yo).val();
			var tots = tots.replace('.','');
			var tots = tots.replace('.','');
			var tots = tots.replace('.','');
			var tots = tots.replace('.','');
			var tots = tots.replace(',','.');
			
		
			
			
			
				sub_total = sub_total +	parseFloat(tots);
				
		}
				
	}
	
	
	var sub_totals = new Intl.NumberFormat('de-DE', {minimumFractionDigits: 2 }).format(sub_total);
	
	// $('#subttl').html(total_bds);
	// $('#subttl2').html(sub_totals);
	// $('#subttl3').html(total_sub_disk);
	// $('#vatppn').html(total_ppns);
	$('#grand_total').html(sub_totals);
	
	
	
	
	//alert(qty+' '+harga+' '+diskon);
	
}

function back(){
	
	window.location.href = "<?php echo base_url().'piutang/add';?>";
	
}

function term_other(){
	
	//alert($('#term').val());
	
	if( $('#term').val() == "other"){
		//$('#term_o').show();
		$("#term_o").prop("readonly",false);

		
	}else{
		// $('#term_o').hide();
		
		$("#term_o").prop("readonly",true);
	}
	
}

  function listdist(){
	  var user_id = '0001';
	  var token = '093940349';
	  
	  
   $('#datatable_pricipal').DataTable({
	   //"dom": 'rtip',
		"bFilter": false,
		"aaSorting": [],
		"bLengthChange": true,
		'iDisplayLength': 10,
		"sPaginationType": "simple_numbers",
		"Info" : false,
		"processing": true,
        "serverSide": true,
        "destroy": true,
		"ajax": "<?php echo base_url().'principal_management/lists'?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token,
		"searching": true,
		"language": {
            "decimal": ",",
            "thousands": "."
        },
		"dom": 'l<"toolbar">frtip',
		"initComplete": function(){
                           $("div.toolbar").prepend('<div class="btn-group pull-left"><button type="button" class="btn btn-custon-rounded-two btn-primary" > Tambah </button></div>');
        }
	});	
  }

	$(document).ready(function(){
		

		$(".rupiahs").inputFilter(function(value) {
			return /^-?\d*[,]?\d*$/.test(value); 
		});
		//$('#ppn').select2();
		
		
		<?php $rr1 = 0; foreach($date_detail_item as $date_detail_itemss){ ?>
		$('#kode_<?php echo $rr1; ?>').select2();
		
		//$('#kode_<?php echo $rr1; ?>').select2('val','<?php echo $date_detail_itemss['id_invoice'] ?>');
		<?php $rr1++; } ?>
		
		
		$('#name').select2();
		$('#term').val('<?php echo $date_detail["account_type"]; ?>');
		$('#casha').val('<?php echo $date_detail["cash_account"]; ?>');
		
		 $('.rupiah').priceFormat({
          		prefix: '',
          		centsSeparator: ',',
          		centsLimit: 2,
          		thousandsSeparator: '.'
        	});	
		
		$('#tgl').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		});
		
		$('#tglterbit').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		});
		
		$('#tglaktif').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		});	
		
		$('#tgljatuhtempo').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		});
		
      listdist();
	  
	  $('#add_po').on('submit', function(e){
		// validation code here
		//if(!valid) {
			e.preventDefault();
			
			// var formData = new FormData(this);
			var formData = new FormData();
			 var urls = $(this).attr('action');
			formData.append('kode', $('#kode').val());
			formData.append('tgl', $('#tgl').val());
			formData.append('ints', $('#ints').val());
			formData.append('ints2', $('#ints2').val());
			formData.append('id', <?php echo $id; ?>);
			
			formData.append('name', $('#name').val());
			formData.append('term', $('#term').val());
			formData.append('casha', $('#casha').val());
			formData.append('tglterbit', $('#tglterbit').val());
			formData.append('keterangan', $('#keterangan').val());
			formData.append('tgljatuhtempo', $('#tgljatuhtempo').val());
			formData.append('int_flo', $('#int_flo').val());
			
			var int_val = parseInt($('#int_flo').val());
			
			for(var yo = 0;yo <= int_val;yo++ ){
				
				if($('#kode_'+yo).val() !== undefined){
					
					formData.append('kode_'+yo, $('#kode_'+yo).val());
					formData.append('jt_'+yo, $('#jt_'+yo).val());
					formData.append('sisa_'+yo, $('#sisa_'+yo).val());
					formData.append('bayar_'+yo, $('#bayar_'+yo).val());
				//	formData.append('remark_'+yo, $('#remark_'+yo).val());
				//	formData.append('diskon_'+yo, $('#diskon_'+yo).val());
					
				}
				
			}
			
		
			
					 swal({
      title: 'Yakin akan Konfirmasi Data ?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak'
    }).then(function() {
			//console.log(formData);

          $.ajax({
              type:'POST',
				url: urls,
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                success: function(response) {
					
					//console.log(response);
					// if(response.success == true){
						// $('#ModalAddPo').modal('show');
					// }else{
						// $('#msg_err').html(response.message);
						// $('#ModalChangePo').modal('show');
					// }
					
						if(response.success == true){
						//$('#ModalAddPo').modal('show');
						swal({
						  title: 'Success!',
						  text: response.message,
						  type: 'success',
						  showCancelButton: false,
						  confirmButtonText: 'Ok'
						}).then(function() {
						  back();
						})
					}else{
						// $('#msg_err').html(response.message);
						// $('#ModalChangePo').modal('show');
						
							swal({
							  title: 'Data Inputan Tidak Seusai !! ',
							  text: response.message,
							  type: 'error',
							  showCancelButton: false,
							  confirmButtonText: 'Ok'
							}).then(function() {
							
							
							});
					}
					
                }
            });
			
		 });		
			//alert(kode);
	
	
		//}
	  });
  });
</script>