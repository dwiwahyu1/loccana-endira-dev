<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto;
  }

  .scroll-overflow {
    min-height: 650px
  }

  #modal-distributor::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }

  table.dataTable thead>tr>th {
    padding-right: 10px;
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
          <div class="card-box table-responsive">
            <div style="display: flex; justify-content:space-between; align-items:center; margin-bottom: 12px;">
              <h4 class="header-title" style="margin:0;">Report Hutang</h4>
            </div>
            <form id="report-cash-filter-form" action="<?= base_url('report_hutang/list'); ?>" class="add-department" method="post" target="_blank" autocomplete="off">
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label>Principle</label>
                    <select name="principle_id" id="principle_id" class="form-control">
                      <option value="0" selected>Semua Principle</option>
                      <?php foreach ($principle_list as $principle) : ?>
                        <option value="<?= $principle['id']; ?>"><?= $principle['name_eksternal']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="text-center row" style="margin-top: 24px; margin-bottom: 24px;">
                <button class="btn btn-primary submit-button" type="submit">Submit</button>
                <button class="btn btn-primary m-t-4" onclick="print_excel()">Export</button>
              </div>
            </form>
            <table id="datatable-report-hutang" class="table table-striped table-bordered m-t-4">
              <thead>
                <tr>
                  <th>Principle</th>
                  <th>Tanggal Terima</th>
                  <th>Keterangan</th>
                  <th>Kemasan</th>
                  <th>Order Pembelian</th>
                  <th>No. Invoice</th>
                  <th>Jatuh Tempo</th>
                  <th>Umur</th>
                  <th>Saldo</th>
                  <th>Bulan Berjalan</th>
                  <th>Tgl</th>
                  <th>Dibayar</th>
                  <!-- <th>Action</th> -->
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div><!-- end col -->
      </div>
    </div>
  </div>
  <span id="laod"></span>
</div>

<script>
  $('select').select2();

  function load_list() {
    var user_id = '0001';
    var token = '093940349';

    var params = [
      'sess_user_id=' + user_id,
      'sess_token=' + token,
      'principle_id=' + $('#principle_id').val() || '',
    ];

    var params_string = '?' + params.join('&');

    var config = {
      //"dom": 'rtip',
      "bFilter": false,
      "aaSorting": [],
      "bLengthChange": true,
      'iDisplayLength': 10,
      "sPaginationType": "simple_numbers",
      "info": true,
      "processing": true,
      "serverSide": true,
      "destroy": true,
      "ajax": "<?= base_url('report_hutang/list'); ?>" + params_string,
      "searching": true,
      "language": {
        "decimal": ",",
        "thousands": "."
      },
      "dom": 'l<"toolbar">frtip',
      "columnDefs": [{
          className: 'text-center',
          targets: [1, 4, 5, 6, 7, -2]
        },
        {
          className: 'text-right',
          targets: [-1, 3, -3, -4]
        }
      ],
      "paging": true,
      "ordering": false,
      "initComplete": function() {
        $('.submit-button').attr('disabled', false);
      }
    };

    if (!$.fn.DataTable.isDataTable('#datatable-report-hutang')) {
      $('#datatable-report-hutang').DataTable(config);
    } else {
      $('#datatable-report-hutang').dataTable().draw();
    }
  }

  function print_excel() {
    var url = '<?= base_url('report_hutang/excel'); ?>';
    var principle_id = $('#principle_id').val();
    var form = $("<form action='" + url + "' method='post' target='_blank'><input type='hidden' name='principle_id' value='" + principle_id + "' /></form>");
    $('body').append(form);
    form.submit();
  }

  $(document).ready(function() {
    $('#report-cash-filter-form').on('submit', function(e) {
      e.preventDefault();
      $('.submit-button').attr('disabled', true);
      // $('#datatable-report-hutang').dataTable().fnClearTable();
      // $('#datatable-report-hutang').dataTable().fnDestroy();
      load_list();
    });
  });
</script>