<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Report_hutang_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param array $params
	 * @return mixed
	 */
	public function list_report_hutang($params = [])
	{
		$total = $this->db
			->select('t_purchase_order.id_po')
			->from('t_purchase_order')
			->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id')
			->join('t_po_mat', 't_purchase_order.id_po = t_po_mat.id_po')
			->join('m_material', 't_po_mat.id_material = m_material.id_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->join('t_bpb_detail', 't_po_mat.id_t_ps = t_bpb_detail.id_po_mat')
			->join('t_invoice_pembelian', 't_bpb_detail.id_bpb = t_invoice_pembelian.id_bpb')
			->join('d_payment_hp', 't_invoice_pembelian.id_invoice = d_payment_hp.id_invoice')
			->join('t_payment_hp', 'd_payment_hp.id_hp = t_payment_hp.id_hp')
			->get()
			->result_array();

		$queryBuilder = $this->db
			->select('*, t_payment_hp.date AS `t_payment_hp.date`')
			->from('t_purchase_order')
			->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id')
			->join('t_po_mat', 't_purchase_order.id_po = t_po_mat.id_po')
			->join('m_material', 't_po_mat.id_material = m_material.id_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->join('t_bpb_detail', 't_po_mat.id_t_ps = t_bpb_detail.id_po_mat')
			->join('t_invoice_pembelian', 't_bpb_detail.id_bpb = t_invoice_pembelian.id_bpb')
			->join('d_payment_hp', 't_invoice_pembelian.id_invoice = d_payment_hp.id_invoice')
			->join('t_payment_hp', 'd_payment_hp.id_hp = t_payment_hp.id_hp')
			//
		;

		if (isset($params['principle_id']) && !empty($params['principle_id'])) {
			$queryBuilder = $queryBuilder->where(['t_purchase_order.id_distributor' => $params['principle_id']]);
		}

		if (isset($params['searchtxt']) && !empty($params['searchtxt'])) {
			$queryBuilder = $queryBuilder->like('t_eksternal.name_eksternal', $params['searchtxt']);
			$queryBuilder = $queryBuilder->or_like('t_eksternal.kode_eksternal', $params['searchtxt']);
			$queryBuilder = $queryBuilder->or_like('m_material.stock_name', $params['searchtxt']);
			$queryBuilder = $queryBuilder->or_like('t_purchase_order.no_po', $params['searchtxt']);
			$queryBuilder = $queryBuilder->or_like('t_purchase_order.date_po', $params['searchtxt']);
			$queryBuilder = $queryBuilder->or_like('t_invoice_pembelian.no_invoice', $params['searchtxt']);
			$queryBuilder = $queryBuilder->or_like('t_payment_hp.date', $params['searchtxt']);
		}

		if (isset($params['limit']) && $params['limit'] >= 0 && isset($params['offset'])) {
			$queryBuilder = $queryBuilder->limit($params['limit'], $params['offset']);
		}

		$result = $queryBuilder->get()->result_array();

		return [
			'data' => $result,
			'total_filtered' => count($total),
			'total' => count($total),
		];
	}

	/**
	 * get list of principle
	 * 
	 * @return array
	 */
	public function get_principle_list()
	{
		$queryBuilder = $this->db
			->select('*')
			->from('t_eksternal');

		$result = $queryBuilder->get()->result_array();

		return $result;
	}
}
