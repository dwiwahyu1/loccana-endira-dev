<style>
    .dt-body-left {
        text-align: left;
    }

    .dt-body-right {
        text-align: right;
    }

    .dt-body-center {
        text-align: center;
        vertical-align: middle;
    }

    .force-overflow {
        height: 650px;
        overflow-y: auto;
        overflow-x: auto
    }

    .scroll-overflow {
        min-height: 650px
    }

    #modal-distributor::-webkit-scrollbar-track {
        box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
        background-color: #F5F5F5;
    }

    #modal-distributor::-webkit-scrollbar {
        width: 10px;
        background-color: #F5F5F5;
    }

    #modal-distributor::-webkit-scrollbar-thumb {
        background-image: -webkit-gradient(linear,
                left bottom,
                left top,
                color-stop(0.44, rgb(122, 153, 217)),
                color-stop(0.72, rgb(73, 125, 189)),
                color-stop(0.86, rgb(28, 58, 148)));
    }

    /* remove arrows from number input field */

    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }
</style>

<div class="product-sales-area mg-tb-30">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="product-payment-inner-st">
                    <ul id="myTabedu1" class="tab-review-design">
                        <li class="active"><a href="#description">Tambah Principal</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content custom-product-edit">
                        <div class="product-tab-list tab-pane fade active in" id="description">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="review-content-section">
                                        <form id="add_principle" action="<?= base_url('principal_management/add_principal'); ?>" class="add-department">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Kode</label>
                                                        <input name="kode" id="kode" type="text" class="form-control" placeholder="Kode Principal">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Nama</label>
                                                        <input name="name" id="name" type="text" class="form-control" placeholder="Nama Principal">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Alamat</label>
                                                        <input name="alamat" id="alamat" type="text" class="form-control" placeholder="Alamat Principal">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>No Telp</label>
                                                        <input name="telp" id="telp" type="text" class="form-control" placeholder="Telephone">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Fax</label>
                                                        <input name="fax" id="fax" type="text" class="form-control" placeholder="Fax">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <input name="email" id="email" type="email" class="form-control" placeholder="Email">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

                                                    <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                                        <label>Rekening Bank 1</label>
                                                        <input name="bank1" id="bank1" type="text" class="form-control" placeholder="Bank 1">
                                                    </div>
                                                    <div class="form-group col-lg-8 col-md-8 col-sm-8">
                                                        <label>&nbsp </label>
                                                        <input name="norek1" id="norek1" type="number" class="form-control text-number" placeholder="No Rek 1">
                                                    </div>


                                                    <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                                        <label>Rekening Bank 2</label>
                                                        <input name="bank2" id="bank2" type="text" class="form-control" placeholder="Bank 2">
                                                    </div>
                                                    <div class="form-group col-lg-8 col-md-8 col-sm-8">
                                                        <label>&nbsp </label>
                                                        <input name="norek2" id="norek2" type="number" class="form-control text-number" placeholder="No Rek 2">
                                                    </div>


                                                    <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                                        <label>Rekening Bank 3</label>
                                                        <input name="bank3" id="bank3" type="text" class="form-control" placeholder="Bank 3">
                                                    </div>
                                                    <div class="form-group col-lg-8 col-md-8 col-sm-8">
                                                        <label>&nbsp </label>
                                                        <input name="norek3" id="norek3" type="number" class="form-control text-number" placeholder="No Rek 3">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="payment-adress">
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                                        <button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()"> Batal</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function clearform() {
        $('#add_principle').trigger("reset");
    }

    function back() {
        window.location.href = "<?= base_url('principal_management'); ?>";
    }

    function listdist() {
        var user_id = '0001';
        var token = '093940349';

        $('#datatable_pricipal').DataTable({
            //"dom": 'rtip',
            "bFilter": false,
            "aaSorting": [],
            "bLengthChange": true,
            'iDisplayLength': 10,
            "sPaginationType": "simple_numbers",
            "Info": false,
            "processing": true,
            "serverSide": true,
            "destroy": true,
            "ajax": "<?php echo base_url() . 'principal_management/lists' ?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token,
            "searching": true,
            "language": {
                "decimal": ",",
                "thousands": "."
            },
            "dom": 'l<"toolbar">frtip',
            "initComplete": function() {
                $("div.toolbar").prepend('<div class="btn-group pull-left"><button type="button" class="btn btn-custon-rounded-two btn-primary" > Tambah </button></div>');
            }
        });
    }

    $(document).ready(function() {

        listdist();

        $('#add_principle').on('submit', function(e) {
            e.preventDefault();

            var formData = new FormData(this);
            var urls = $(this).attr('action');

            swal({
                title: 'Yakin akan Simpan Data ?',
                text: '',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak'
            }).then(function() {

                $.ajax({
                    type: 'POST',
                    url: urls,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(response) {

                        if (response.success == true) {
                            swal({
                                title: 'Success!',
                                text: "Apakah Anda Akan Input Principle Lagi ?",
                                type: 'success',
                                showCancelButton: true,
                                confirmButtonText: 'Ya',
                                cancelButtonText: 'Tidak'
                            }).then(function() {
                                $('#add_principle').trigger("reset");
                            }, function(dismiss) {
                                back();
                            })

                        } else {
                            swal({
                                title: 'Data Inputan Tidak Sesuai',
                                html: response.message,
                                type: 'error',
                                confirmButtonText: 'Ok'
                            });
                        }
                    }
                });

            });
        });
    });
</script>