<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class M_informasi_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function get_informasi_data($id)
	{
		$sql 	= 'CALL informasi_search_id(?)';

		$out = array();
		$query 	=  $this->db->query(
			$sql,
			array(
				$id
			)
		);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function lists($params = array())
	{
		$sql 	= 'CALL m_informasi_list_all(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query(
			$sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			)
		);

		$result = $query->result_array();

		//print_r();die;

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered as filtered, @total as total')->row_array();
		$return = array(
			'data' => $result,
			'total_filtered' => $total['filtered'],
			'total' => $total['total'],
		);

		return $return;
	}

	public function edit_informasi($data)
	{
		//	print_r($datas);die;
		$this->db->where('id_info', $data['id_info']);
		$this->db->update('t_informasi', $data);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_informasi($data)
	{
		$this->db->insert('t_informasi', $data);
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}


	public function deletes($id)
	{
		$this->db->where('id_info', $id);
		$this->db->delete('t_informasi');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}
