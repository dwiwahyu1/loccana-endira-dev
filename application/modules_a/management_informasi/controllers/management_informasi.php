<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Management_informasi extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('management_informasi/m_informasi_model');
		$this->load->library('priv');
	}

	public function index()
	{
		$priv = $this->priv->get_priv();
		$data = array('priv' => $priv);

		$this->template->load('maintemplate', 'management_informasi/views/index', $data);
	}

	function lists()
	{
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

		$order_fields = array('', 'judul', 'date');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->m_informasi_model->lists($params);
		$priv = $this->priv->get_priv();

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = $params['offset'];
		foreach ($list['data'] as $row) {
			$status_akses = '
				<div class="btn-group" style="display:' . $priv['update'] . '"><button type="button" class="btn btn-custon-rounded-two btn-warning" data-toggle="tooltip" data-placement="top" title="Edit"  onClick="edituom(\'' . $row['id_info'] . '\')"><i class="fa fa-edit"></i></button></div>
				<div class="btn-group" style="display:' . $priv['delete'] . '"><button type="button" class="btn btn-custon-rounded-two btn-danger" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteuom(\'' . $row['id_info'] . '\')"><i class="fa fa-trash"></i></button></div>
			';

			array_push($data, array(
				++$i,
				$row['judul'],
				$row['keterangan'],
				$row['date'],
				'<img src="' . base_url('uploads/informasi/' . $row['gambar']) . '" alt="picture" class="img-circle profile_img" style="width: 40px; height:40px">',
				$status_akses
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add()
	{
		$this->template->load('maintemplate', 'management_informasi/views/add_modal_view');
	}

	public function edit()
	{
		$id = anti_sql_injection($this->input->post('id_info', TRUE));
		$result = $this->m_informasi_model->get_informasi_data(anti_sql_injection($id));

		$data = array('informasi' => $result);

		$this->template->load('maintemplate', 'management_informasi/views/edit_modal_view', $data);
	}

	public function deletes()
	{
		$data   = file_get_contents("php://input");
		$params   = json_decode($data, true);

		$list = $this->m_informasi_model->deletes(anti_sql_injection($params['id']));

		$res = array(
			'status' => 'success',
			'message' => 'Data telah di hapus'
		);

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function edit_informasi()
	{
		$this->load_form_validations();

		if ($this->form_validation->run() == FALSE) {
			$msg = validation_errors();
			$result = array(
				'success' => false,
				'message' => $msg
			);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			// die;
			$upload_error = NULL;
			$picture = NULL;

			if ($_FILES['gambar_informasi']['name']) {
				$this->load->library('upload');
				$config = array(
					'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/informasi",
					'upload_url' => base_url() . "uploads/informasi",
					'encrypt_name' => TRUE,
					'overwrite' => FALSE,
					'allowed_types' => 'jpg|jpeg|png',
					'max_size' => '10000'
				);

				$this->upload->initialize($config);

				if ($this->upload->do_upload("gambar_informasi")) { // Success
					// General result data
					$result = $this->upload->data();

					// Load resize library
					$this->load->library('image_lib');

					// Resizing parameters large
					$resize = array(
						'source_image' => $result['full_path'],
						'new_image' => $result['full_path'],
						'maintain_ratio' => TRUE,
						'width' => 300,
						'height' => 300
					);

					// Do resize
					$this->image_lib->initialize($resize);
					$this->image_lib->resize();
					$this->image_lib->clear();

					// Add our stuff
					$picture = $result['file_name'];
				} else {
					$pesan = $this->upload->display_errors();
					$upload_error = strip_tags(str_replace("\n", '', $pesan));

					$result = array(
						'success' => false,
						'message' => $upload_error
					);
				}
			} else {
				$old_logo = $this->input->post('old_logo');

				if (!empty($old_logo)) {
					$picture = $old_logo;
				}
			}
			$message = "";
			$data = array(
				'id_info' 		=> anti_sql_injection($this->input->post('id_info', TRUE)),
				'user_id' 		=> $this->session->userdata['logged_in']['user_id'],
				'judul'     	=> anti_sql_injection($this->input->post('judul_informasi', TRUE)),
				'gambar' 		=> $picture,
				'keterangan' 	=> anti_sql_injection($this->input->post('informasi_ket', TRUE)),
				'date' 			=> date('Y-m-d'),
			);
			$result = $this->m_informasi_model->edit_informasi($data);
			if ($result > 0) {
				$msg = 'Berhasil merubah informasi.';

				$result = array(
					'success' => true,
					'message' => $msg
				);
				$msg = 'Gagal merubah informasi.';
			} else {

				$result = array(
					'success' => false,
					'message' => $msg
				);
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function add_informasi()
	{
		$this->load_form_validations();

		if ($this->form_validation->run() == FALSE) {
			$msg = validation_errors();
			$result = array(
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			$upload_error = NULL;
			$picture = NULL;

			if ($_FILES['gambar_informasi']['name']) {
				$this->load->library('upload');
				$config = array(
					'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/informasi",
					'upload_url' => base_url() . "uploads/informasi",
					'encrypt_name' => TRUE,
					'overwrite' => FALSE,
					'allowed_types' => 'jpg|jpeg|png',
					'max_size' => '10000'
				);

				$this->upload->initialize($config);

				if ($this->upload->do_upload("gambar_informasi")) { // Success
					// General result data
					$result = $this->upload->data();

					// Load resize library
					$this->load->library('image_lib');

					// Resizing parameters large
					$resize = array(
						'source_image' => $result['full_path'],
						'new_image' => $result['full_path'],
						'maintain_ratio' => TRUE,
						'width' => 300,
						'height' => 300
					);

					// Do resize
					$this->image_lib->initialize($resize);
					$this->image_lib->resize();
					$this->image_lib->clear();

					// Add our stuff
					$picture = $result['file_name'];
				} else {
					$pesan = $this->upload->display_errors();
					$upload_error = strip_tags(str_replace("\n", '', $pesan));

					$result = array(
						'success' => false,
						'message' => $upload_error
					);
				}
			}

			$data = array(
				'user_id' 		=> $this->session->userdata['logged_in']['user_id'],
				'judul'     	=> anti_sql_injection($this->input->post('judul_informasi', TRUE)),
				'gambar' 		=> $picture,
				'keterangan' 	=> anti_sql_injection($this->input->post('informasi_ket', TRUE)),
				'date' 			=> date('Y-m-d'),
			);

			$result = $this->m_informasi_model->add_informasi($data);

			if ($result > 0) {
				$msg = 'Berhasil menambahkan informasi.';

				$result = array(
					'success' => true,
					'message' => $msg
				);
			} else {
				$msg = 'Gagal menambahkan informasi ke database.';
				$result = array(
					'success' => false,
					'message' => $msg
				);
			}
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	private function load_form_validations()
	{
		$this->form_validation->set_rules('judul_informasi', 'Judul Informasi', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('informasi_ket', 'Keterangan Informasi', 'trim|required|max_length[255]');
	}
}
