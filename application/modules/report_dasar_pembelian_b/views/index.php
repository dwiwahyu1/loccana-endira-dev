<style>
	.dt-body-left {
		text-align: left;
	}

	.dt-body-right {
		text-align: right;
	}

	.dt-body-center {
		text-align: center;
		vertical-align: middle;
	}

	.force-overflow {
		height: 650px;
		overflow-y: auto;
		overflow-x: auto;
	}

	.scroll-overflow {
		min-height: 650px
	}

	#modal-distributor::-webkit-scrollbar-track {
		box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
		-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar {
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar-thumb {
		background-image: -webkit-gradient(linear,
				left bottom,
				left top,
				color-stop(0.44, rgb(122, 153, 217)),
				color-stop(0.72, rgb(73, 125, 189)),
				color-stop(0.86, rgb(28, 58, 148)));
	}

	.select2-container .select2-choice {
		height: 35px !important;
	}
</style>

<div class="product-sales-area mg-tb-30">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="product-payment-inner-st">
					<ul id="myTabedu1" class="tab-review-design">
						<li class="active"><a href="#description">Report Dasar Pembelian</a></li>
					</ul>
					<div id="myTabContent" class="tab-content custom-product-edit">
						<div class="product-tab-list tab-pane fade active in" id="description">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="review-content-section">
										<!--<form id="add_po" action="<?= base_url('report_dasar_pembelian/export_excel'); ?>" class="add-department" method="post" target="_blank" autocomplete="off">-->
										<form id="submit_filter" action="<?= base_url('report_dasar_pembelian/reports'); ?>" class="add-department" method="post" autocomplete="off">
											<div class="row">
												<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
													<div class="form-group">
														<label>Principle</label>
														<select name="principle_id" id="principle_id" class="form-control" placeholder="Nama Principle">
															<option value="0" selected="selected">Semua Principle</option>
															<?php foreach ($principles as $principle) : ?>
																<option value="<?= $principle['id']; ?>"><?= $principle['name_eksternal']; ?></option>;
															<?php endforeach; ?>
														</select>
													</div>
													<div class="form-group date">
														<label>Tanggal Awal</label>
														<input name="tgl_awal" id="tgl_awal" type="text" class="form-control" placeholder="Tanggal Awal">
													</div>
													<div class="form-group date">
														<label>Tanggal Akhir</label>
														<input name="tgl_akhir" id="tgl_akhir" type="text" class="form-control" placeholder="Tanggal Akhir">
													</div>
												</div>
											</div>
									</div>
								</div>
							</div>
							<br><br>
							<input name="int_flo" id="int_flo" type="hidden" class="form-control" placeholder="Qty" value="0">
							<br>
							<div class="row">
								<div class="col-lg-12">
									<div class="payment-adress">
										<button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="product-sales-area mg-tb-30">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="product-payment-inner-st">
					<ul id="myTabedu1" class="tab-review-design">
						<li class="active"><a href="#description">Report Dasar Pembelian</a></li>
					</ul>
					<div id="myTabContent" class="tab-content custom-product-edit">
						<div class="product-tab-list tab-pane fade active in" id="description">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

									<div id="table_items">

										<table id="tabel-report-dasar-pembelian" class="table table-striped table-bordered">
											<thead>
												<tr>
													<th width="5%">Tanggal</th>
													<th>Kode</th>
													<th>Nama Barang</th>
													<th>Principle</th>
													<th>Qty</th>
													<th>Harga</th>
													<th>Jumlah</th>
													<th>PPN</th>
													<th>Jumlah+PPN</th>
												</tr>
											</thead>
											<tbody></tbody>
										</table>
									</div>
								</div>
							</div>
							<br><br>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$('#principle_id').select2();
	$('#tgl_awal').datepicker({
		format: "yyyy-mm-dd",
		autoclose: true,
		todayHighlight: true
	});

	$('#tgl_akhir').datepicker({
		format: "yyyy-mm-dd",
		autoclose: true,
		todayHighlight: true
	});

	function load_list() {
		var user_id = '0001';
		var token = '093940349';
		var params = [
			'sess_user_id=' + user_id,
			'sess_token=' + token,
			'principle_id=' + $('#principle_id').val() || '',
			'tgl_awal=' + $('#tgl_awal').val() || '',
			'tgl_akhir=' + $('#tgl_akhir').val() || ''
		];

		var params_string = '?' + params.join('&');

		$('#tabel-report-dasar-pembelian').DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": '<?= base_url('report_dasar_pembelian/list') ?>' + params_string,
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false,
			"info": true,
			"bSort": false,
			"dom": 'l<"toolbar">frtip',
			"bLengthChange": true,
			"ordering": false,
			"initComplete": function() {
				$("div.toolbar").prepend('<div class="btn-group pull-left"><a href="#" onClick="print_excel()" type="button" class="btn btn-custon-rounded-two btn-primary" > Print </a></div>');
			}
		});

	}

	function print_excel() {
		var principle_id = $('#principle_id').val();
		var tgl_awal = $('#tgl_awal').val();
		var tgl_akhir = $('#tgl_akhir').val();

		var url = '<?= base_url('report_dasar_pembelian/export_excel'); ?>';

		var form = $("<form action='" + url + "' method='post' target='_blank'>" +
			"<input type='hidden' name='principle_id' value='" + principle_id + "' />" +
			"<input type='hidden' name='tgl_awal' value='" + tgl_awal + "' />" +
			"<input type='hidden' name='tgl_akhir' value='" + tgl_akhir + "' />" +
			"</form>");
		$('body').append(form);
		form.submit();
	}

	$(document).ready(function() {
		$('#submit_filter').on('submit', function(e) {
			e.preventDefault();

			$('#tabel-report-dasar-pembelian').dataTable().fnClearTable();
			$('#tabel-report-dasar-pembelian').dataTable().fnDestroy();

			load_list();
		});
	});
</script>