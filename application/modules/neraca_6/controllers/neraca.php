<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Neraca extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('neraca/uom_model');
		$this->load->model('report_piutang/return_model');
		$this->load->model('report_hutang/hutang_model');
		$this->load->model('laba_rugi/rl_model');
		$this->load->library('log_activity');
		$this->load->library('priv');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$priv = $this->priv->get_priv();

		// sales per montg

		$year = date('Y');

		$html_sb = '';
		$html_sbs = '';
		$html_all = '';
		$cur_month = '';

		$params  = array(
			'start_time' => date('Y-m-d'),
			'end_time' => date('Y-m-d'),
			'report' => "aktiva"
		);

		$params_p  = array(
			'start_time' => date('Y-m-d'),
			'end_time' => date('Y-m-d'),
			'report' => "passiva"
		);


		//$datas['get_piutang'] = $this->uom_model->get_piutang($params);
		$datas['get_piutang'] = $this->reports_piutang($params);
		$datas['get_persediaan'] = $this->uom_model->get_persediaan($params);
		$datas['get_hutang'] = $this->get_hutang($params);

		//	print_r($datas['get_piutang']);die;

		$data['get_aktiva'] = $this->uom_model->get_report_keu($params);
		$data['get_pasiva'] = $this->uom_model->get_report_keu($params_p);




		$data['priv'] = $priv;

		$html_a = '';

		$tot_a = 0;
		foreach ($data['get_aktiva'] as $get_aktivas) {

			if ($get_aktivas['coa'] == 11301) {
				$html_a .= ' <tr>
                  <th >' . $get_aktivas['coa'] . '</th>
                  <th>' . $get_aktivas['keterangan'] . '</th>
                  <th>' . number_format($datas['get_piutang']['piutang'], 2, '.', ',') . '</th>
                </tr>';
				$tot_a += $datas['get_piutang']['piutang'];
			} elseif ($get_aktivas['coa'] == 11600) {
				$html_a .= ' <tr>
                  <th >' . $get_aktivas['coa'] . '</th>
                  <th>' . $get_aktivas['keterangan'] . '</th>
                  <th>' . number_format($datas['get_persediaan'][0]['persediaan'], 2, '.', ',') . '</th>
                </tr>';
				$tot_a += $datas['get_persediaan'][0]['persediaan'];
			} elseif ($get_aktivas['coa'] == 12102 || $get_aktivas['coa'] == 12202 || $get_aktivas['coa'] == 12302) {

				$data['get_depresiasi'] = $this->uom_model->get_depresiasi($params_p, $get_aktivas['id_coa']);

				$html_a .= ' <tr>
                  <th >' . $get_aktivas['coa'] . '</th>
                  <th>' . $get_aktivas['keterangan'] . '</th>
                  <th>' . number_format($datas['get_persediaan'][0]['persediaan'], 2, '.', ',') . '</th>
                </tr>';
				$tot_a += $datas['get_persediaan'][0]['persediaan'];
			} else {
				$html_a .= ' <tr>
                  <th >' . $get_aktivas['coa'] . '</th>
                  <th>' . $get_aktivas['keterangan'] . '</th>
                  <th>' . number_format($get_aktivas['nilai'], 2, '.', ',') . '</th>
                </tr>';
				$tot_a += $get_aktivas['nilai'];
			}
		}

		$html_a .= ' <tr>
					<th colspan=2>Total</th>
					<th>' . number_format($tot_a, 2, '.', ',') . '</th>
				</tr>';

		$html_p = '';


		$tot_p = 0;
		foreach ($data['get_pasiva'] as $get_aktivas) {

			if ($get_aktivas['coa'] == 20100) {
				$html_p .= ' <tr>
                  <th >' . $get_aktivas['coa'] . '</th>
                  <th>' . $get_aktivas['keterangan'] . '</th>
                  <th>' . number_format($datas['get_hutang'][0]['hutang'], 2, '.', ',') . '</th>
                </tr>';
				$tot_p += $datas['get_hutang'][0]['hutang'];
			} else {
				$html_p .= ' <tr>
                  <th >' . $get_aktivas['coa'] . '</th>
                  <th>' . $get_aktivas['keterangan'] . '</th>
                  <th>' . number_format($get_aktivas['nilai'], 2, '.', ',') . '</th>
                </tr>';
				$tot_p += $get_aktivas['nilai'];
			}
		}

		$html_p .= ' <tr>
					<th colspan=2>Total</th>
					<th>' . number_format($tot_p, 2, '.', ',') . '</th>
				</tr>';

		$data['t_ac'] = $html_a;
		$data['t_pa'] = $html_p;


		//print_r($get_aktiva);die;

		$this->template->load('maintemplate', 'neraca/views/index', $data);
	}


	public function reports_piutang($params) {

		$data = array(
			'principal' => 0,
			'tgl_awal' => date_format(date_create($params['end_time']), "Y-m-01"),
			'tgl_akhir' => $params['end_time'],
			'region' => 0

		);

		$kuantiti_a = 0;
		$harga_satuan_a = 0;
		$nilai_a = 0;
		$pembelian_a = 0;
		$kuantiti_sol_a = 0;
		$kuantiti_titip_a = 0;
		$kuantiti_bonus_a = 0;
		$harga_sat_pemb_a = 0;
		$nilai_pemb_a = 0;
		$harga_pokok_a = 0;
		$penjualan_a = 0;
		$pengeluaran_a = 0;
		$saldo_akhir_a = 0;
		$nilai_akhir_a = 0;
		$invoice_saldo_a = 0;
		$sisa_aa = 0;
		$d0_a = 0;
		$d30_a = 0;
		$d60_a = 0;
		$d90_a = 0;
		$d120_a = 0;
		$total_t_a = 0;

		if ($data['principal'] == 0) {

			$dist_a = $this->return_model->get_distribution($data);
			$html = '';


			//print_r($dist_a);die;

			foreach ($dist_a as $dist_as) {

				$datat = array(
					'customer' => $dist_as['id_customer'],
					'tgl_awal' => date_format(date_create($params['end_time']), "Y-m-01"),
					'tgl_akhir' => $params['end_time'],
					'region' => $this->Anti_sql_injection($this->input->post('region', TRUE))

				);

				$result = $this->return_model->get_report_persediaan($datat);

				//print_r($result);die;



				$kuantiti = 0;
				$harga_satuan = 0;
				$nilai = 0;
				$pembelian = 0;
				$kuantiti_sol = 0;
				$kuantiti_titip = 0;
				$kuantiti_bonus = 0;
				$harga_sat_pemb = 0;
				$nilai_pemb = 0;
				$harga_pokok = 0;
				$penjualan = 0;
				$pengeluaran = 0;
				$saldo_akhir = 0;
				$nilai_akhir = 0;
				$invoice_saldo = 0;
				$sisa_a = 0;
				$prin = '';
				$kode_prin = '';

				$d0 = 0;
				$d30 = 0;
				$d60 = 0;
				$d90 = 0;
				$d120 = 0;
				$total_t = 0;

				$curr_invoice = '';
				foreach ($result as $datas) {

					if ($curr_invoice <> $datas['no_invoice']) {

						$bayar_r_te =  number_format($datas['bayar'], 2, ',', '.');
						$bayar_r =  $datas['bayar'];
						$bb =  $datas['bayar'];
						$bb_saldo =  $datas['bayar_saldo'];
						$retur_saldo =  $datas['nilai_retur_saldo'];
						$curr_invoice = $datas['no_invoice'];
						$sisa_r_te =  number_format($datas['price_full'] - $datas['bayar'], 2, ',', '.');
						$sisa_r =  $datas['price_full'] - $datas['bayar'];
					} else {
						$bayar_r_te = null;
						$sisa_r_te = null;
						$bayar_r = 0;
						$retur_saldo = 0;
						$bb_saldo = 0;
						$sisa_r = 0;
						$bb = 0;
					}

					$prcs =  $datas['price_full'] - $datas['bayar'] - $datas['bayar_saldo'] - $datas['nilai_retur'] - $datas['nilai_retur_saldo'];

					$bb = $bb - $prcs;

					$ssisss = ($datas['saldo'] - $bb_saldo - $retur_saldo) + $datas['bulan_berjalan'] - ($datas['bayar'] + $datas['nilai_retur']);

					if ($bb > 0) {
						$ss_sisa = 0;
					} else {
						$ss_sisa = abs($bb);
					}

					$ss_sisa = $ssisss;


					if ($datas['umur'] < 31) {
						$m1 = $ss_sisa;
						$m2 = 0;
						$m3 = 0;
						$m4 = 0;
						$m5 = 0;
					} elseif ($datas['umur'] > 30 && $datas['umur'] < 61) {
						$m1 = 0;
						$m2 = $ss_sisa;
						$m3 = 0;
						$m4 = 0;
						$m5 = 0;
					} elseif ($datas['umur'] > 60 && $datas['umur'] < 91) {
						$m1 = 0;
						$m2 = 0;
						$m3 = $ss_sisa;
						$m4 = 0;
						$m5 = 0;
					} elseif ($datas['umur'] > 90 && $datas['umur'] < 121) {
						$m1 = 0;
						$m2 = 0;
						$m3 = 0;
						$m4 = $ss_sisa;
						$m5 = 0;
					} else {
						$m1 = 0;
						$m2 = 0;
						$m3 = 0;
						$m4 = 0;
						$m5 = $ss_sisa;
					}

					$ttl = $m1 + $m2 + $m3 + $m4 + $m5;
					// $ttl = $datas['min_30']-( $datas['min_30']*($datas['diskons']/100)) + ( ($datas['min_30']-( $datas['min_30']*($datas['diskons']/100) ))*($datas['pajak']/100)) + $datas['d30']-( $datas['d30']*($datas['diskons']/100)) + ( ($datas['d30']-( $datas['d30']*($datas['diskons']/100) ))*($datas['pajak']/100) ) + $datas['d60']-( $datas['d60']*($datas['diskons']/100)) + ( ($datas['d60']-( $datas['d60']*($datas['diskons']/100) ))*($datas['pajak']/100) ) + $datas['d90']-( $datas['d90']*($datas['diskons']/100)) + ( ($datas['d90']-( $datas['d90']*($datas['diskons']/100) ))*($datas['pajak']/100) ) + $datas['d120']-( $datas['d120']*($datas['diskons']/100)) + ( ($datas['d120']-( $datas['d120']*($datas['diskons']/100) ))*($datas['pajak']/100) );

					// $prcs = $datas['prices']-( $datas['prices']*($datas['diskons']/100)) + ( ($datas['prices']-( $datas['prices']*($datas['diskons']/100) ))*($datas['pajak']/100) );


					$html .= '<tr>
								<th >' . $datas['cust_name'] . '</th>
								<th>' . $datas['tanggal_invoice'] . '</th>
								<th>' . $datas['no_invoice'] . '</th>
								<th>' . $datas['due_date'] . '</th>
								<th>' . $datas['umur'] . '</th>
								 <th>' . number_format($datas['saldo'] - $bb_saldo - $retur_saldo, 2, ',', '.') . '</th>
								  <th>' . number_format($datas['bulan_berjalan'], 2, ',', '.') . '</th>
								  <th>' . ($datas['tgl_bayar'] . ' ' . $datas['tgl_retur']) . '</th>
								   <th>' . number_format($datas['bayar'] + $datas['nilai_retur'], 2, ',', '.') . '</th>
								   <th>' . number_format($ss_sisa, 2, ',', '.') . '</th>
								<th>' . number_format($m1, 2, ',', '.') . '</th>
								<th>' . number_format($m2, 2, ',', '.') . '</th>
								<th>' . number_format($m3, 2, ',', '.') . '</th>
								<th>' . number_format($m4, 2, ',', '.') . '</th>
								<th>' . number_format($m5, 2, ',', '.') . '</th>
								<th>' . number_format($ss_sisa, 2, ',', '.') . '</th>
							</tr>';

					$prin = $datas['cust_name'];


					$curr_invoice = $datas['no_invoice'];

					$penjualan = $penjualan + ($datas['saldo'] - ($datas['saldo'] * ($datas['diskon'] / 100))) - $bb_saldo - $retur_saldo;
					$pengeluaran = $pengeluaran + ($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskon'] / 100)));
					$saldo_akhir = $saldo_akhir + $bayar_r;
					$sisa_a = $sisa_a + $ss_sisa;
					$nilai_akhir = $nilai_akhir + $datas['bayar'] + $datas['nilai_retur'];
					$invoice_saldo = $invoice_saldo + $datas['invoice_saldo'];
					// // $d0 = $d0 + $datas['min_30']-( $datas['min_30']*($datas['diskons']/100)) + ( ($datas['min_30']-( $datas['min_30']*($datas['diskons']/100) ))*($datas['pajak']/100));
					// // $d30 = $d30 + $datas['d30']-( $datas['d30']*($datas['diskons']/100)) + ( ($datas['d30']-( $datas['d30']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// // $d60 = $d60 + $datas['d60']-( $datas['d60']*($datas['diskons']/100)) + ( ($datas['d60']-( $datas['d60']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// // $d90 = $d90 + $datas['d90']-( $datas['d90']*($datas['diskons']/100)) + ( ($datas['d90']-( $datas['d90']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// // $d120 = $d120 + $datas['d120']-( $datas['d120']*($datas['diskons']/100)) + ( ($datas['d120']-( $datas['d120']*($datas['diskons']/100) ))*($datas['pajak']/100) );

					$d0 = $d0 + $m1;
					$d30 = $d30 + $m2;
					$d60 = $d60 + $m3;
					$d90 = $d90 + $m4;
					$d120 = $d120 + $m5;
					$total_t = $total_t + $ttl;

					$penjualan_a = $penjualan_a + ($datas['saldo'] - ($datas['saldo'] * ($datas['diskon'] / 100))) - $bb_saldo - $retur_saldo;
					$pengeluaran_a = $pengeluaran_a + ($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskon'] / 100)));
					$saldo_akhir_a = $saldo_akhir_a +  $bayar_r;
					$sisa_aa = $sisa_aa + $ss_sisa;
					$nilai_akhir_a = $nilai_akhir_a +  $datas['bayar'] + $datas['nilai_retur'];
					$invoice_saldo_a = $invoice_saldo_a +  $datas['invoice_saldo'];

					// // $d0_a = $d0_a + $datas['min_30']-( $datas['min_30']*($datas['diskons']/100)) + ( ($datas['min_30']-( $datas['min_30']*($datas['diskons']/100) ))*($datas['pajak']/100));
					// // $d30_a = $d30_a + $datas['d30']-( $datas['d30']*($datas['diskons']/100)) + ( ($datas['d30']-( $datas['d30']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// // $d60_a = $d60_a + $datas['d60']-( $datas['d60']*($datas['diskons']/100)) + ( ($datas['d60']-( $datas['d60']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// // $d90_a = $d90_a + $datas['d90']-( $datas['d90']*($datas['diskons']/100)) + ( ($datas['d90']-( $datas['d90']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// // $d120_a = $d120_a + $datas['d120']-( $datas['d120']*($datas['diskons']/100)) + ( ($datas['d120']-( $datas['d120']*($datas['diskons']/100) ))*($datas['pajak']/100) );

					$d0_a = $d0_a + $m1;
					$d30_a = $d30_a + $m2;
					$d60_a = $d60_a + $m3;
					$d90_a = $d90_a + $m4;
					$d120_a = $d120_a + $m5;
					$total_t_a = $total_t_a + $ttl;
				}

				$html .= '<tr style="background:yellow">
							<th>' . $prin . '</th>
							<th></th>
							<th ></th>
							<th ></th>	
							<th></th>
							<th>' . number_format($penjualan, 2, ',', '.') . '</th>
							<th>' . number_format($pengeluaran, 2, ',', '.') . '</th>
							<th></th>
							<th>' . number_format($saldo_akhir, 2, ',', '.') . '</th>
							<th>' . number_format($sisa_a, 2, ',', '.') . '</th>
							<th>' . number_format($d0, 2, ',', '.') . '</th>
							<th>' . number_format($d30, 2, ',', '.') . '</th>
							<th>' . number_format($d60, 2, ',', '.') . '</th>
							<th>' . number_format($d90, 2, ',', '.') . '</th>
							<th>' . number_format($d120, 2, ',', '.') . '</th>
							<th>' . number_format($penjualan + $pengeluaran - $saldo_akhir, 2, ',', '.') . '</th>
						</tr>';
			}
		} else {

			$datat = array(
				'customer' => $data['principal'],
				'tgl_awal' => $this->Anti_sql_injection($this->input->post('tgl_awal', TRUE)),
				'tgl_akhir' => $this->Anti_sql_injection($this->input->post('tgl_akhir', TRUE)),
				'region' => $this->Anti_sql_injection($this->input->post('region', TRUE))

			);

			$result = $this->return_model->get_report_persediaan($datat);

			//print_r($result);die;

			$html = '';

			$kuantiti = 0;
			$harga_satuan = 0;
			$nilai = 0;
			$pembelian = 0;
			$kuantiti_sol = 0;
			$kuantiti_titip = 0;
			$kuantiti_bonus = 0;
			$harga_sat_pemb = 0;
			$nilai_pemb = 0;
			$harga_pokok = 0;
			$penjualan = 0;
			$pengeluaran = 0;
			$saldo_akhir = 0;
			$nilai_akhir = 0;
			$invoice_saldo = 0;
			$prin = '';
			$sisa_a = 0;
			$kode_prin = '';

			$d0 = 0;
			$d30 = 0;
			$d60 = 0;
			$d90 = 0;
			$d120 = 0;
			$total_t = 0;

			$curr_invoice = '';
			foreach ($result as $datas) {

				//ECHO $datas['nilai_retur'];

				if ($curr_invoice <> $datas['no_invoice']) {

					$bayar_r_te =  number_format($datas['bayar'], 2, ',', '.');
					$bayar_r =  $datas['bayar'];
					$bb =  $datas['bayar'];
					$bb_saldo =  $datas['bayar_saldo'];
					$retur_saldo =  $datas['nilai_retur_saldo'];
					$curr_invoice = $datas['no_invoice'];
					$sisa_r_te =  number_format($datas['price_full'] - $datas['bayar'], 2, ',', '.');
					$sisa_r =  $datas['price_full'] - $datas['bayar'];
				} else {
					$bayar_r_te = null;
					$sisa_r_te = null;
					$bayar_r = 0;
					$retur_saldo = 0;
					$bb_saldo = 0;
					$sisa_r = 0;
					$bb = 0;
				}

				$prcs = $datas['price_full'] - $datas['bayar'] - $datas['bayar_saldo'] - $datas['nilai_retur'] - $datas['nilai_retur_saldo'];

				$bb = $bb - $prcs;

				$ssisss = ($datas['saldo'] - $bb_saldo - $retur_saldo) + $datas['bulan_berjalan'] - ($datas['bayar'] + $datas['nilai_retur']);

				if ($bb > 0) {
					$ss_sisa = 0;
				} else {
					$ss_sisa = abs($bb);
				}

				$ss_sisa = $ssisss;


				if ($datas['umur'] < 31) {
					$m1 = $ss_sisa;
					$m2 = 0;
					$m3 = 0;
					$m4 = 0;
					$m5 = 0;
				} elseif ($datas['umur'] > 30 && $datas['umur'] < 61) {
					$m1 = 0;
					$m2 = $ss_sisa;
					$m3 = 0;
					$m4 = 0;
					$m5 = 0;
				} elseif ($datas['umur'] > 60 && $datas['umur'] < 91) {
					$m1 = 0;
					$m2 = 0;
					$m3 = $ss_sisa;
					$m4 = 0;
					$m5 = 0;
				} elseif ($datas['umur'] > 90 && $datas['umur'] < 121) {
					$m1 = 0;
					$m2 = 0;
					$m3 = 0;
					$m4 = $ss_sisa;
					$m5 = 0;
				} else {
					$m1 = 0;
					$m2 = 0;
					$m3 = 0;
					$m4 = 0;
					$m5 = $ss_sisa;
				}

				$ttl = $m1 + $m2 + $m3 + $m4 + $m5;
				// $ttl = $datas['min_30']-( $datas['min_30']*($datas['diskons']/100)) + ( ($datas['min_30']-( $datas['min_30']*($datas['diskons']/100) ))*($datas['pajak']/100)) + $datas['d30']-( $datas['d30']*($datas['diskons']/100)) + ( ($datas['d30']-( $datas['d30']*($datas['diskons']/100) ))*($datas['pajak']/100) ) + $datas['d60']-( $datas['d60']*($datas['diskons']/100)) + ( ($datas['d60']-( $datas['d60']*($datas['diskons']/100) ))*($datas['pajak']/100) ) + $datas['d90']-( $datas['d90']*($datas['diskons']/100)) + ( ($datas['d90']-( $datas['d90']*($datas['diskons']/100) ))*($datas['pajak']/100) ) + $datas['d120']-( $datas['d120']*($datas['diskons']/100)) + ( ($datas['d120']-( $datas['d120']*($datas['diskons']/100) ))*($datas['pajak']/100) );

				// $prcs = $datas['prices']-( $datas['prices']*($datas['diskons']/100)) + ( ($datas['prices']-( $datas['prices']*($datas['diskons']/100) ))*($datas['pajak']/100) );


				$html .= '<tr>
								<th >' . $datas['cust_name'] . '</th>
								<th>' . $datas['tanggal_invoice'] . '</th>
								<th>' . $datas['no_invoice'] . '</th>
								<th>' . $datas['due_date'] . '</th>
								<th>' . $datas['umur'] . '</th>
								 <th>' . number_format($datas['saldo'] - $bb_saldo - $retur_saldo, 2, ',', '.') . '</th>
								  <th>' . number_format($datas['bulan_berjalan'], 2, ',', '.') . '</th>
								  <th>' . ($datas['tgl_bayar'] . ' ' . $datas['tgl_retur']) . '</th>
								   <th>' . number_format($datas['bayar'] + $datas['nilai_retur'], 2, ',', '.') . '</th>
								   <th>' . number_format($ss_sisa, 2, ',', '.') . '</th>
								<th>' . number_format($m1, 2, ',', '.') . '</th>
								<th>' . number_format($m2, 2, ',', '.') . '</th>
								<th>' . number_format($m3, 2, ',', '.') . '</th>
								<th>' . number_format($m4, 2, ',', '.') . '</th>
								<th>' . number_format($m5, 2, ',', '.') . '</th>
								<th>' . number_format($ss_sisa, 2, ',', '.') . '</th>
							</tr>';

				$prin = $datas['cust_name'];


				$curr_invoice = $datas['no_invoice'];

				$penjualan = $penjualan + ($datas['saldo'] - ($datas['saldo'] * ($datas['diskon'] / 100))) - $bb_saldo - $retur_saldo;
				$pengeluaran = $pengeluaran + ($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskon'] / 100)));
				$saldo_akhir = $saldo_akhir + $bayar_r + $datas['nilai_retur'];
				$sisa_a = $sisa_a + $ss_sisa;
				$nilai_akhir = $nilai_akhir + $datas['bayar'] + $datas['nilai_retur'];
				$invoice_saldo = $invoice_saldo + $datas['invoice_saldo'];
				// // $d0 = $d0 + $datas['min_30']-( $datas['min_30']*($datas['diskons']/100)) + ( ($datas['min_30']-( $datas['min_30']*($datas['diskons']/100) ))*($datas['pajak']/100));
				// // $d30 = $d30 + $datas['d30']-( $datas['d30']*($datas['diskons']/100)) + ( ($datas['d30']-( $datas['d30']*($datas['diskons']/100) ))*($datas['pajak']/100) );
				// // $d60 = $d60 + $datas['d60']-( $datas['d60']*($datas['diskons']/100)) + ( ($datas['d60']-( $datas['d60']*($datas['diskons']/100) ))*($datas['pajak']/100) );
				// // $d90 = $d90 + $datas['d90']-( $datas['d90']*($datas['diskons']/100)) + ( ($datas['d90']-( $datas['d90']*($datas['diskons']/100) ))*($datas['pajak']/100) );
				// // $d120 = $d120 + $datas['d120']-( $datas['d120']*($datas['diskons']/100)) + ( ($datas['d120']-( $datas['d120']*($datas['diskons']/100) ))*($datas['pajak']/100) );

				$d0 = $d0 + $m1;
				$d30 = $d30 + $m2;
				$d60 = $d60 + $m3;
				$d90 = $d90 + $m4;
				$d120 = $d120 + $m5;
				$total_t = $total_t + $ttl;

				$penjualan_a = $penjualan_a + ($datas['saldo'] - ($datas['saldo'] * ($datas['diskon'] / 100))) - $bb_saldo - $retur_saldo;
				$pengeluaran_a = $pengeluaran_a + ($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskon'] / 100)));
				$saldo_akhir_a = $saldo_akhir_a +  $bayar_r + $datas['nilai_retur'];
				$nilai_akhir_a = $nilai_akhir_a +  $datas['bayar'] + $datas['nilai_retur'];
				$invoice_saldo_a = $invoice_saldo_a +  $datas['invoice_saldo'];
				$sisa_aa = $sisa_aa + $ss_sisa;

				// // $d0_a = $d0_a + $datas['min_30']-( $datas['min_30']*($datas['diskons']/100)) + ( ($datas['min_30']-( $datas['min_30']*($datas['diskons']/100) ))*($datas['pajak']/100));
				// // $d30_a = $d30_a + $datas['d30']-( $datas['d30']*($datas['diskons']/100)) + ( ($datas['d30']-( $datas['d30']*($datas['diskons']/100) ))*($datas['pajak']/100) );
				// // $d60_a = $d60_a + $datas['d60']-( $datas['d60']*($datas['diskons']/100)) + ( ($datas['d60']-( $datas['d60']*($datas['diskons']/100) ))*($datas['pajak']/100) );
				// // $d90_a = $d90_a + $datas['d90']-( $datas['d90']*($datas['diskons']/100)) + ( ($datas['d90']-( $datas['d90']*($datas['diskons']/100) ))*($datas['pajak']/100) );
				// // $d120_a = $d120_a + $datas['d120']-( $datas['d120']*($datas['diskons']/100)) + ( ($datas['d120']-( $datas['d120']*($datas['diskons']/100) ))*($datas['pajak']/100) );

				$d0_a = $d0_a + $m1;
				$d30_a = $d30_a + $m2;
				$d60_a = $d60_a + $m3;
				$d90_a = $d90_a + $m4;
				$d120_a = $d120_a + $m5;
				$total_t_a = $total_t_a + $ttl;
			}

			$html .= '<tr style="background:yellow">
							<th>' . $prin . '</th>
							<th></th>
							<th ></th>
							<th ></th>	
							<th></th>
							<th>' . number_format($penjualan, 2, ',', '.') . '</th>
							<th>' . number_format($pengeluaran, 2, ',', '.') . '</th>
							<th></th>
							<th>' . number_format($saldo_akhir, 2, ',', '.') . '</th>
							<th>' . number_format($sisa_a, 2, ',', '.') . '</th>
							<th>' . number_format($d0, 2, ',', '.') . '</th>
							<th>' . number_format($d30, 2, ',', '.') . '</th>
							<th>' . number_format($d60, 2, ',', '.') . '</th>
							<th>' . number_format($d90, 2, ',', '.') . '</th>
							<th>' . number_format($d120, 2, ',', '.') . '</th>
							<th>' . number_format($penjualan + $pengeluaran - $saldo_akhir, 2, ',', '.') . '</th>
						</tr>';
		}

		$html .= '<tr style="background:green">
						<th colspan="3">Total </th>
						<th></th>
							<th ></th>
						<th>' . number_format($penjualan_a, 2, ',', '.') . '</th>
						<th>' . number_format($pengeluaran_a, 2, ',', '.') . '</th>
						<th></th>
						<th>' . number_format($saldo_akhir_a, 2, ',', '.') . '</th>
						<th>' . number_format($sisa_aa, 2, ',', '.') . '</th>
							<th>' . number_format($d0_a, 2, ',', '.') . '</th>
							<th>' . number_format($d30_a, 2, ',', '.') . '</th>
							<th>' . number_format($d60_a, 2, ',', '.') . '</th>
							<th>' . number_format($d90_a, 2, ',', '.') . '</th>
							<th>' . number_format($d120_a, 2, ',', '.') . '</th>
							<th>' . number_format($total_t_a, 2, ',', '.') . '</th>
					</tr>';

		$results['html'] = $html;
		$return['piutang'] = $sisa_aa;

		return $return;

		//$this->output->set_content_type('application/json')->set_output(json_encode($results));
		//print_r($result);die;


	}

	public function reports_persediaan_rl($params) {

		$data = array(
			'principal' => 0,
			'tgl_awal' => $params['start_time'],
			'tgl_akhir' => $params['end_time']

		);



		//print_r($params_awalss);die;

		$kuantiti_a = 0;
		$harga_satuan_a = 0;
		$nilai_a = 0;
		$pembelian_a = 0;
		$kuantiti_sol_a = 0;
		$kuantiti_titip_a = 0;
		$kuantiti_bonus_a = 0;
		$kuantiti_diskon_a = 0;
		$harga_sat_pemb_a = 0;
		$nilai_pemb_a = 0;
		$harga_pokok_a = 0;
		$penjualan_a = 0;
		$pengeluaran_a = 0;
		$saldo_akhir_a = 0;
		$nilai_akhir_a = 0;


		$dist_a = $this->rl_model->get_distribution($data);
		$html = '';

		foreach ($dist_a as $dist_as) {

			$datat = array(
				'principal' => $dist_as['dist_id'],
				'tgl_awal' => $params['start_time'],
				'tgl_akhir' => $params['end_time']

			);

			$params_awalss['principal']		= $dist_as['dist_id'];
			$params_awalss['tgl_akhir']	= date("Y-m-d", strtotime($datat['tgl_awal'] . "-1 days"));
			$params_awalss['tgl_awal']	= date_format(date_create($params_awalss['tgl_akhir']), "Y-m-01");

			if ($datat['tgl_awal'] == '2021-01-01') {
				$result = $this->rl_model->get_report_persediaan($datat);
				$result_awal = $this->rl_model->get_report_persediaan($datat);
			} else {
				$result_awal = $this->rl_model->get_report_persediaan_newest($params_awalss);
				$result = $this->rl_model->get_report_persediaan_newest($datat);
			}
			// echo "<pre>";
			//print_r($result_awal);die; 


			$kuantiti = 0;
			$harga_satuan = 0;
			$nilai = 0;
			$pembelian = 0;
			$kuantiti_sol = 0;
			$kuantiti_titip = 0;
			$kuantiti_bonus = 0;
			$kuantiti_diskon = 0;
			$harga_sat_pemb = 0;
			$nilai_pemb = 0;
			$harga_pokok = 0;
			$penjualan = 0;
			$pengeluaran = 0;
			$saldo_akhir = 0;
			$nilai_akhir = 0;
			$prin = '';
			$kode_prin = '';

			$int_iuy = 0;
			foreach ($result as $datas) {

				if ($datat['tgl_awal'] == '2021-01-01') {
					$datas['harga_satuan'] = $datas['harga_satuan'];
				} else {
					$datas['harga_satuan'] = $result_awal[$int_iuy]['harga_pokok'];
				}

				$html .= '<tr>
								<th >' . $datas['stock_code'] . '</th>
								<th>' . $datas['stock_name'] . '</th>
								<th>' . $datas['base_qty'] . ' ' . $datas['uom_symbol'] . ' x ' . $datas['unit_box'] . '</th>
								<th>' . number_format($datas['saldo_awals'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['harga_satuan'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['saldo_awals'] * $datas['harga_satuan'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['pembelian'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['kuantiti_diskon'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['kuantiti_titip'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['kuantiti_bonus'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['harga_sat_pemb'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['nilai_pemb'], 2, ',', '.') . '</th>
								<th>' . $datas['ket'] . '</th>
								<th>' . number_format($datas['harga_pokok'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['penjualan'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['pengeluaran'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['saldo_akhir'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['saldo_akhir'] * $datas['harga_pokok'], 2, ',', '.') . '</th>
							</tr>';

				$prin = $datas['name_eksternal'];
				$kode_prin = substr($datas['stock_code'], 0, 3);
				$kuantiti = $kuantiti + $datas['saldo_awals'];
				$harga_satuan = $harga_satuan + $datas['harga_satuan'];
				$nilai = $nilai + $datas['saldo_awals'] * $datas['harga_satuan'];
				$pembelian = $pembelian + $datas['pembelian'];
				$kuantiti_sol = $kuantiti_sol + $datas['kuantiti_sol'];
				$kuantiti_diskon = $kuantiti_diskon + $datas['kuantiti_diskon'];
				$kuantiti_titip = $kuantiti_titip + $datas['kuantiti_titip'];
				$kuantiti_bonus = $kuantiti_bonus + $datas['kuantiti_bonus'];
				$harga_sat_pemb = $harga_sat_pemb + $datas['harga_sat_pemb'];
				$nilai_pemb = $nilai_pemb + $datas['nilai_pemb'];
				$harga_pokok = $harga_pokok + $datas['harga_pokok'];
				$penjualan = $penjualan + $datas['penjualan'];
				$pengeluaran = $pengeluaran + $datas['pengeluaran'];
				$saldo_akhir = $saldo_akhir + $datas['saldo_akhir'];
				$nilai_akhir = $nilai_akhir + $datas['saldo_akhir'] * $datas['harga_pokok'];

				$kuantiti_a = $kuantiti_a + $datas['saldo_awals'];
				$harga_satuan_a = $harga_satuan_a +  $datas['harga_satuan'];
				$nilai_a = $nilai_a + $datas['saldo_awals'] * $datas['harga_satuan'];
				$pembelian_a = $pembelian_a +  $datas['pembelian'];
				$kuantiti_sol_a = $kuantiti_sol_a +  $datas['kuantiti_sol'];
				$kuantiti_titip_a = $kuantiti_titip_a +  $datas['kuantiti_titip'];
				$kuantiti_diskon_a = $kuantiti_diskon_a +  $datas['kuantiti_diskon'];
				$kuantiti_bonus_a = $kuantiti_bonus_a +  $datas['kuantiti_bonus'];
				$harga_sat_pemb_a = $harga_sat_pemb_a +  $datas['harga_sat_pemb'];
				$nilai_pemb_a = $nilai_pemb_a +  $datas['nilai_pemb'];
				$harga_pokok_a = $harga_pokok_a + $datas['harga_pokok'];
				$penjualan_a = $penjualan_a +  $datas['penjualan'];
				$pengeluaran_a = $pengeluaran_a +  $datas['pengeluaran'];
				$saldo_akhir_a = $saldo_akhir_a +  $datas['saldo_akhir'];
				$nilai_akhir_a = $nilai_akhir_a +  $datas['saldo_akhir'] * $datas['harga_pokok'];

				$int_iuy++;
			}

			$html .= '<tr style="background:yellow">
							<th>' . $kode_prin . '</th>
							<th>' . $prin . '</th>
							<th></th>
							<th>' . number_format($kuantiti, 2, ',', '.') . '</th>
							<th>' . number_format($harga_satuan, 2, ',', '.') . '</th>
							<th>' . number_format($nilai, 2, ',', '.') . '</th>
							<th>' . number_format($pembelian, 2, ',', '.') . '</th>
							<th>' . number_format($kuantiti_diskon, 2, ',', '.') . '</th>
							<th>' . number_format($kuantiti_titip, 2, ',', '.') . '</th>
							<th>' . number_format($kuantiti_bonus, 2, ',', '.') . '</th>
							<th>' . number_format($harga_sat_pemb, 2, ',', '.') . '</th>
							<th>' . number_format($nilai_pemb, 2, ',', '.') . '</th>
							<th></th>
							<th>' . number_format($harga_pokok, 2, ',', '.') . '</th>
							<th>' . number_format($penjualan, 2, ',', '.') . '</th>
							<th>' . number_format($pengeluaran, 2, ',', '.') . '</th>
							<th>' . number_format($saldo_akhir, 2, ',', '.') . '</th>
							<th>' . number_format($nilai_akhir, 2, ',', '.') . '</th>
						</tr>';
		}

		$html .= '<tr style="background:green">
						<th colspan="3">Total </th>
						<th>' . number_format($kuantiti_a, 2, ',', '.') . '</th>
						<th>' . number_format($harga_satuan_a, 2, ',', '.') . '</th>
						<th>' . number_format($nilai_a, 2, ',', '.') . '</th>
						<th>' . number_format($pembelian_a, 2, ',', '.') . '</th>
						<th>' . number_format($kuantiti_diskon_a, 2, ',', '.') . '</th>
						<th>' . number_format($kuantiti_titip_a, 2, ',', '.') . '</th>
						<th>' . number_format($kuantiti_bonus_a, 2, ',', '.') . '</th>
						<th>' . number_format($harga_sat_pemb_a, 2, ',', '.') . '</th>
						<th>' . number_format($nilai_pemb_a, 2, ',', '.') . '</th> 
						<th></th>
						<th>' . number_format($harga_pokok_a, 2, ',', '.') . '</th>
						<th>' . number_format($penjualan_a, 2, ',', '.') . '</th>
						<th>' . number_format($pengeluaran_a, 2, ',', '.') . '</th>
						<th>' . number_format($saldo_akhir_a, 2, ',', '.') . '</th>
						<th>' . number_format($nilai_akhir_a, 2, ',', '.') . '</th>
					</tr>';

		$results['html'] = $html;

		$datae['persediaan_awal'] = $nilai_a;
		$datae['persediaan_akhir'] = $nilai_akhir_a;
		$datae['pembelian'] = $nilai_pemb_a;

		return $datae;
		//$this->output->set_content_type('application/json')->set_output(json_encode($results));
		//print_r($result);die;


	}

	
	function laba_rugi_s2($param) {

		$s_date = explode('-', $param['end_time']);
		$start_date = $s_date[0] . '-03-01';

		//print_r($param);die;
		// $data   = file_get_contents("php://input");
		// $param   = json_decode($data, true);

		$params  = array(
			'start_time' => $start_date,
			'end_time' => $param['end_time'],
			'report' => "laba_rugi"
		);

		$params_p  = array(
			'start_time' => $start_date,
			'end_time' => $param['end_time'],
			'report' => "laba_rugi"
		);


		$datas['get_aktiva'] = $this->rl_model->get_report_keu($params);
		$datas['get_summ'] = $this->rl_model->get_report_keu_summ($params_p);
		$datas['get_piutang'] = $this->rl_model->get_piutang($params);
		//$datas['get_persediaan'] = $this->rl_model->get_persediaan($params);
		$datas['get_persediaan'] = $this->reports_persediaan_rl($params);
		$datas['get_hutang'] = $this->rl_model->get_hutang($params);
		$datas['get_penjualan'] = $this->rl_model->get_penjualan($params);
		$datas['get_pembelian'] = $this->rl_model->get_pembelian($params);
		$datas['get_retur'] = $this->rl_model->get_retur($params);

		//print_r($datas['get_persediaan']);die;

		$html_a = '';

		$tot_a = 0;
		$tot_b = 0;

		$hp1 = 0;
		$hp2 = 0;

		$penj = 0;
		$penj_pes = 0;
		$retur = 0;

		foreach ($datas['get_aktiva'] as $get_aktivas) {

			if ($get_aktivas['type_data'] == 0) {



				if ($get_aktivas['coa'] == 50100) {

					$html_a .= ' <tr>
						  <th >' . $get_aktivas['coa'] . '</th>
						  <th>' . $get_aktivas['keterangan'] . '</th>
						  <th>' . number_format($datas['get_persediaan']['persediaan_awal'], 2, ',', '.') . '</th>
						  <th>0.00</th>
						</tr>';
					$tot_a += $datas['get_persediaan']['persediaan_awal'];

					$nn = $datas['get_persediaan']['persediaan_awal'];
				} elseif ($get_aktivas['coa'] == 50201) {

					$html_a .= ' <tr>
						  <th >' . $get_aktivas['coa'] . '</th>
						  <th>' . $get_aktivas['keterangan'] . '</th>
						  <th>' . number_format($datas['get_pembelian'][0]['pestisida'], 2, ',', '.') . '</th>
						  <th>0.00</th>
						</tr>';
					$tot_a += $datas['get_pembelian'][0]['pestisida'];

					$nn = $datas['get_pembelian'][0]['pestisida'];
					$pemb = $datas['get_pembelian'][0]['pestisida'];
				} elseif ($get_aktivas['coa'] == 50202) {

					$html_a .= ' <tr>
						  <th >' . $get_aktivas['coa'] . '</th>
						  <th>' . $get_aktivas['keterangan'] . '</th>
						  <th>' . number_format($datas['get_pembelian'][0]['non_pestisida'], 2, ',', '.') . '</th>
						  <th>0.00</th>
						</tr>';
					$tot_a += $datas['get_pembelian'][0]['non_pestisida'];

					$nn = $datas['get_pembelian'][0]['non_pestisida'];
				} elseif ($get_aktivas['coa'] == 41104) {

					$html_a .= ' <tr>
						  <th >' . $get_aktivas['coa'] . '</th>
						  <th>' . $get_aktivas['keterangan'] . '</th>
						  <th>' . number_format($datas['get_retur'][0]['nilai_retur'], 2, ',', '.') . '</th>
						  <th>0.00</th>
						</tr>';
					$tot_a += $datas['get_retur'][0]['nilai_retur'];
					$penj -= $datas['get_retur'][0]['nilai_retur'];

					$nn = $datas['get_retur'][0]['nilai_retur'];
				} else {

					$html_a .= ' <tr>
						  <th >' . $get_aktivas['coa'] . '</th>
						  <th>' . $get_aktivas['keterangan'] . '</th>
						  <th>' . number_format($get_aktivas['nilai'], 2, ',', '.') . '</th>
						  <th>0.00</th>
						</tr>';
					$tot_a += $get_aktivas['nilai'];

					$nn = $get_aktivas['nilai'];
				}

				if ($get_aktivas['coa'] == 50100 | $get_aktivas['coa'] == 50201 | $get_aktivas['coa'] == 50202 | $get_aktivas['coa'] == 50500 | $get_aktivas['coa'] == 50600) {
					$hp1 += $nn;
				}
			} else {

				if ($get_aktivas['coa'] == 50700) {

					$html_a .= ' <tr>
                  <th >' . $get_aktivas['coa'] . '</th>
                  <th>' . $get_aktivas['keterangan'] . '</th>
				  <th>0.00</th>
                  <th>' . number_format($datas['get_persediaan']['persediaan_akhir'], 2, ',', '.') . '</th>
                </tr>';
					$tot_b += $datas['get_persediaan']['persediaan_akhir'];

					$nn = $datas['get_persediaan']['persediaan_akhir'];
				} elseif ($get_aktivas['coa'] == 41102) {

					$html_a .= ' <tr>
						  <th >' . $get_aktivas['coa'] . '</th>
						  <th>' . $get_aktivas['keterangan'] . '</th>
						  <th>0.00</th>
						  <th>' . number_format($datas['get_penjualan'][0]['pestisida'], 2, ',', '.') . '</th>
						</tr>';
					$tot_b += $datas['get_penjualan'][0]['pestisida'];

					$nn = $datas['get_penjualan'][0]['pestisida'];
					$penj += $datas['get_penjualan'][0]['pestisida'];
				} elseif ($get_aktivas['coa'] == 41103) {

					$html_a .= ' <tr>
						  <th >' . $get_aktivas['coa'] . '</th>
						 <th>' . $get_aktivas['keterangan'] . '</th>
						  <th>0.00</th>
						   <th>' . number_format($datas['get_penjualan'][0]['non_pestisida'], 2, ',', '.') . '</th>
						</tr>';
					$tot_b += $datas['get_penjualan'][0]['non_pestisida'];

					$nn = $datas['get_penjualan'][0]['non_pestisida'];
					$penj += $datas['get_penjualan'][0]['non_pestisida'];
				} else {

					$html_a .= ' <tr>
                  <th >' . $get_aktivas['coa'] . '</th>
                  <th>' . $get_aktivas['keterangan'] . '</th>
				  <th>0.00</th>
                  <th>' . number_format($get_aktivas['nilai'], 2, ',', '.') . '</th>
                </tr>';
					$tot_b += $get_aktivas['nilai'];

					$nn = $get_aktivas['nilai'];
				}

				if ($get_aktivas['coa'] == 50300 | $get_aktivas['coa'] == 50400 | $get_aktivas['coa'] == 50700) {
					$hp2 += $nn;
				}
			}
		}

		$html_a .= ' <tr>
					<th colspan=2>Total</th>
					<th>' . number_format($tot_a, 2, '.', ',') . '</th>
					<th>' . number_format($tot_b, 2, '.', ',') . '</th>
				</tr> <tr>
					<th colspan=3>Laba / Rugi </th>
					<th>' . number_format($tot_b - $tot_a, 2, '.', ',') . '</th>
				</tr>';

		$html_p = '';


		$tot_p = 0;
		// foreach($datas['get_pasiva'] as $get_aktivas){

		// $html_p .= ' <tr>
		// <th >'.$get_aktivas['coa'].'</th>
		// <th>'.$get_aktivas['keterangan'].'</th>
		// <th>'.number_format($get_aktivas['nilai'],2,'.',',').'</th>
		// </tr>';
		// $tot_p +=$get_aktivas['nilai'];
		// }

		// $html_p .= ' <tr>
		// <th colspan=2>Total</th>
		// <th>'.number_format($tot_p,2,'.',',').'</th>
		// </tr>';
		//print_r($datas['get_summ']);die;

		$html_s = '
		<tr><th>PENJUALAN BERSIH</th><th>' . number_format($penj, 2, ',', '.') . '</th></tr>
				<tr><th>BONUS PRINCIPLE</th><th>' . number_format($datas['get_summ'][1]['nilai'], 2, ',', '.') . '</th></tr>
				<tr><th>HARGA POKOK PENJUALAN</th><th>' . number_format(($hp1 - $hp2), 2, ',', '.') . '</th></tr>
				<tr><th><STRONG>LABA KOTOR</STRONG></th><th>' . number_format($penj + $datas['get_summ'][1]['nilai'] - ($hp1 - $hp2), 2, ',', '.') . '</th></tr>
				<tr><th>BIAYA PENJUALAN</th><th>' . number_format($datas['get_summ'][3]['nilai'], 2, ',', '.') . '</th></tr>
				<tr><th>BIAYA ADMINISTRASI dan UMUM</th><th>' . number_format($datas['get_summ'][4]['nilai'], 2, ',', '.') . '</th></tr>
				<tr><th>BIAYA PENYUSUTAN</th><th>' . number_format($datas['get_summ'][5]['nilai'], 2, ',', '.') . '</th></tr>
				<tr><th><STRONG>BIAYA OPERASIONAL</STRONG></th><th>' . number_format($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai'], 2, ',', '.') . '</th></tr>
				<tr><th><STRONG>LABA (RUGI) OPERASI</STRONG></th><th>' . number_format(($datas['get_summ'][0]['nilai'] + $datas['get_summ'][1]['nilai'] - $datas['get_summ'][2]['nilai']) - ($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai']), 2, ',', '.') . '</th></tr>
				<tr><th>PENDAPATAN NON OPERASI</th><th>' . number_format($datas['get_summ'][6]['nilai'], 2, ',', '.') . '</th></tr>
				<tr><th>BIAYA NON OPERASI</th><th>' . number_format($datas['get_summ'][7]['nilai'], 2, ',', '.') . '</th></tr>
				<tr><th><STRONG>LABA / RUGI BERSIH SEBELUM PAJAK</STRONG></th><th>' . number_format(($penj + $datas['get_summ'][1]['nilai'] - ($hp1 - $hp2)) - ($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai']) + $datas['get_summ'][6]['nilai'] - $datas['get_summ'][7]['nilai'], 2, ',', '.') . '</th></tr>
		';

		$datas['t_ac'] = $html_a;
		//$datas['t_pa'] = $html_p;
		$datas['t_pa'] = $html_s;

		$data_ret['penjualan'] = $penj;
		$data_ret['pembelian'] = $pemb;
		$data_ret['total_rl'] = ($penj + $datas['get_summ'][1]['nilai'] - ($hp1 - $hp2)) - ($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai']) + $datas['get_summ'][6]['nilai'] - $datas['get_summ'][7]['nilai'];


		$data_ret['ppn'] = (($pemb * 0.1) + ($retur * 0.1)) - ($penj * 0.1);


		return $data_ret;
		//$this->output->set_content_type('application/json')->set_output(json_encode($datas));
		//print_r($datas['get_aktiva']);die;

	}
	

	function laba_rugi($param) {

		$s_date = explode('-', $param['end_time']);
		$start_date = $s_date[0] . '-01-01';

		//print_r($param);die;
		// $data   = file_get_contents("php://input");
		// $param   = json_decode($data, true);

		$params  = array(
			'start_time' => $start_date,
			'end_time' => $param['end_time'],
			'report' => "laba_rugi"
		);

		$params_p  = array(
			'start_time' => $start_date,
			'end_time' => $param['end_time'],
			'report' => "laba_rugi"
		);


		$datas['get_aktiva'] = $this->rl_model->get_report_keu($params);
		$datas['get_summ'] = $this->rl_model->get_report_keu_summ($params_p);
		$datas['get_piutang'] = $this->rl_model->get_piutang($params);
		//$datas['get_persediaan'] = $this->rl_model->get_persediaan($params);
		$datas['get_persediaan'] = $this->reports_persediaan_rl($params);
		$datas['get_hutang'] = $this->rl_model->get_hutang($params);
		$datas['get_penjualan'] = $this->rl_model->get_penjualan($params);
		$datas['get_pembelian'] = $this->rl_model->get_pembelian($params);
		$datas['get_retur'] = $this->rl_model->get_retur($params);

		//print_r($datas['get_persediaan']);die;

		$html_a = '';

		$tot_a = 0;
		$tot_b = 0;

		$hp1 = 0;
		$hp2 = 0;

		$penj = 0;
		$penj_pes = 0;
		$retur = 0;

		foreach ($datas['get_aktiva'] as $get_aktivas) {

			if ($get_aktivas['type_data'] == 0) {



				if ($get_aktivas['coa'] == 50100) {

					$html_a .= ' <tr>
						  <th >' . $get_aktivas['coa'] . '</th>
						  <th>' . $get_aktivas['keterangan'] . '</th>
						  <th>' . number_format($datas['get_persediaan']['persediaan_awal'], 2, ',', '.') . '</th>
						  <th>0.00</th>
						</tr>';
					$tot_a += $datas['get_persediaan']['persediaan_awal'];

					$nn = $datas['get_persediaan']['persediaan_awal'];
				} elseif ($get_aktivas['coa'] == 50201) {

					$html_a .= ' <tr>
						  <th >' . $get_aktivas['coa'] . '</th>
						  <th>' . $get_aktivas['keterangan'] . '</th>
						  <th>' . number_format($datas['get_pembelian'][0]['pestisida'], 2, ',', '.') . '</th>
						  <th>0.00</th>
						</tr>';
					$tot_a += $datas['get_pembelian'][0]['pestisida'];

					$nn = $datas['get_pembelian'][0]['pestisida'];
					$pemb = $datas['get_pembelian'][0]['pestisida'];
				} elseif ($get_aktivas['coa'] == 50202) {

					$html_a .= ' <tr>
						  <th >' . $get_aktivas['coa'] . '</th>
						  <th>' . $get_aktivas['keterangan'] . '</th>
						  <th>' . number_format($datas['get_pembelian'][0]['non_pestisida'], 2, ',', '.') . '</th>
						  <th>0.00</th>
						</tr>';
					$tot_a += $datas['get_pembelian'][0]['non_pestisida'];

					$nn = $datas['get_pembelian'][0]['non_pestisida'];
				} elseif ($get_aktivas['coa'] == 41104) {

					$html_a .= ' <tr>
						  <th >' . $get_aktivas['coa'] . '</th>
						  <th>' . $get_aktivas['keterangan'] . '</th>
						  <th>' . number_format($datas['get_retur'][0]['nilai_retur'], 2, ',', '.') . '</th>
						  <th>0.00</th>
						</tr>';
					$tot_a += $datas['get_retur'][0]['nilai_retur'];
					$penj -= $datas['get_retur'][0]['nilai_retur'];

					$nn = $datas['get_retur'][0]['nilai_retur'];
				} else {

					$html_a .= ' <tr>
						  <th >' . $get_aktivas['coa'] . '</th>
						  <th>' . $get_aktivas['keterangan'] . '</th>
						  <th>' . number_format($get_aktivas['nilai'], 2, ',', '.') . '</th>
						  <th>0.00</th>
						</tr>';
					$tot_a += $get_aktivas['nilai'];

					$nn = $get_aktivas['nilai'];
				}

				if ($get_aktivas['coa'] == 50100 | $get_aktivas['coa'] == 50201 | $get_aktivas['coa'] == 50202 | $get_aktivas['coa'] == 50500 | $get_aktivas['coa'] == 50600) {
					$hp1 += $nn;
				}
			} else {

				if ($get_aktivas['coa'] == 50700) {

					$html_a .= ' <tr>
                  <th >' . $get_aktivas['coa'] . '</th>
                  <th>' . $get_aktivas['keterangan'] . '</th>
				  <th>0.00</th>
                  <th>' . number_format($datas['get_persediaan']['persediaan_akhir'], 2, ',', '.') . '</th>
                </tr>';
					$tot_b += $datas['get_persediaan']['persediaan_akhir'];

					$nn = $datas['get_persediaan']['persediaan_akhir'];
				} elseif ($get_aktivas['coa'] == 41102) {

					$html_a .= ' <tr>
						  <th >' . $get_aktivas['coa'] . '</th>
						  <th>' . $get_aktivas['keterangan'] . '</th>
						  <th>0.00</th>
						  <th>' . number_format($datas['get_penjualan'][0]['pestisida'], 2, ',', '.') . '</th>
						</tr>';
					$tot_b += $datas['get_penjualan'][0]['pestisida'];

					$nn = $datas['get_penjualan'][0]['pestisida'];
					$penj += $datas['get_penjualan'][0]['pestisida'];
				} elseif ($get_aktivas['coa'] == 41103) {

					$html_a .= ' <tr>
						  <th >' . $get_aktivas['coa'] . '</th>
						 <th>' . $get_aktivas['keterangan'] . '</th>
						  <th>0.00</th>
						   <th>' . number_format($datas['get_penjualan'][0]['non_pestisida'], 2, ',', '.') . '</th>
						</tr>';
					$tot_b += $datas['get_penjualan'][0]['non_pestisida'];

					$nn = $datas['get_penjualan'][0]['non_pestisida'];
					$penj += $datas['get_penjualan'][0]['non_pestisida'];
				} else {

					$html_a .= ' <tr>
                  <th >' . $get_aktivas['coa'] . '</th>
                  <th>' . $get_aktivas['keterangan'] . '</th>
				  <th>0.00</th>
                  <th>' . number_format($get_aktivas['nilai'], 2, ',', '.') . '</th>
                </tr>';
					$tot_b += $get_aktivas['nilai'];

					$nn = $get_aktivas['nilai'];
				}

				if ($get_aktivas['coa'] == 50300 | $get_aktivas['coa'] == 50400 | $get_aktivas['coa'] == 50700) {
					$hp2 += $nn;
				}
			}
		}

		$html_a .= ' <tr>
					<th colspan=2>Total</th>
					<th>' . number_format($tot_a, 2, '.', ',') . '</th>
					<th>' . number_format($tot_b, 2, '.', ',') . '</th>
				</tr> <tr>
					<th colspan=3>Laba / Rugi </th>
					<th>' . number_format($tot_b - $tot_a, 2, '.', ',') . '</th>
				</tr>';

		$html_p = '';


		$tot_p = 0;
		// foreach($datas['get_pasiva'] as $get_aktivas){

		// $html_p .= ' <tr>
		// <th >'.$get_aktivas['coa'].'</th>
		// <th>'.$get_aktivas['keterangan'].'</th>
		// <th>'.number_format($get_aktivas['nilai'],2,'.',',').'</th>
		// </tr>';
		// $tot_p +=$get_aktivas['nilai'];
		// }

		// $html_p .= ' <tr>
		// <th colspan=2>Total</th>
		// <th>'.number_format($tot_p,2,'.',',').'</th>
		// </tr>';
		//print_r($datas['get_summ']);die;

		$html_s = '
		<tr><th>PENJUALAN BERSIH</th><th>' . number_format($penj, 2, ',', '.') . '</th></tr>
				<tr><th>BONUS PRINCIPLE</th><th>' . number_format($datas['get_summ'][1]['nilai'], 2, ',', '.') . '</th></tr>
				<tr><th>HARGA POKOK PENJUALAN</th><th>' . number_format(($hp1 - $hp2), 2, ',', '.') . '</th></tr>
				<tr><th><STRONG>LABA KOTOR</STRONG></th><th>' . number_format($penj + $datas['get_summ'][1]['nilai'] - ($hp1 - $hp2), 2, ',', '.') . '</th></tr>
				<tr><th>BIAYA PENJUALAN</th><th>' . number_format($datas['get_summ'][3]['nilai'], 2, ',', '.') . '</th></tr>
				<tr><th>BIAYA ADMINISTRASI dan UMUM</th><th>' . number_format($datas['get_summ'][4]['nilai'], 2, ',', '.') . '</th></tr>
				<tr><th>BIAYA PENYUSUTAN</th><th>' . number_format($datas['get_summ'][5]['nilai'], 2, ',', '.') . '</th></tr>
				<tr><th><STRONG>BIAYA OPERASIONAL</STRONG></th><th>' . number_format($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai'], 2, ',', '.') . '</th></tr>
				<tr><th><STRONG>LABA (RUGI) OPERASI</STRONG></th><th>' . number_format(($datas['get_summ'][0]['nilai'] + $datas['get_summ'][1]['nilai'] - $datas['get_summ'][2]['nilai']) - ($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai']), 2, ',', '.') . '</th></tr>
				<tr><th>PENDAPATAN NON OPERASI</th><th>' . number_format($datas['get_summ'][6]['nilai'], 2, ',', '.') . '</th></tr>
				<tr><th>BIAYA NON OPERASI</th><th>' . number_format($datas['get_summ'][7]['nilai'], 2, ',', '.') . '</th></tr>
				<tr><th><STRONG>LABA / RUGI BERSIH SEBELUM PAJAK</STRONG></th><th>' . number_format(($penj + $datas['get_summ'][1]['nilai'] - ($hp1 - $hp2)) - ($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai']) + $datas['get_summ'][6]['nilai'] - $datas['get_summ'][7]['nilai'], 2, ',', '.') . '</th></tr>
		';

		$datas['t_ac'] = $html_a;
		//$datas['t_pa'] = $html_p;
		$datas['t_pa'] = $html_s;

		$data_ret['penjualan'] = $penj;
		$data_ret['pembelian'] = $pemb;
		$data_ret['total_rl'] = ($penj + $datas['get_summ'][1]['nilai'] - ($hp1 - $hp2)) - ($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai']) + $datas['get_summ'][6]['nilai'] - $datas['get_summ'][7]['nilai'];


		$data_ret['ppn'] = (($pemb * 0.1) + ($retur * 0.1)) - ($penj * 0.1);


		return $data_ret;
		//$this->output->set_content_type('application/json')->set_output(json_encode($datas));
		//print_r($datas['get_aktiva']);die;

	}


	public function reports_persediaan($params) {

		$data = array(
			'principal' => 0,
			'tgl_awal' => $params['start_time'],
			'tgl_akhir' => $params['end_time']

		);



		//print_r($data);die;

		$kuantiti_a = 0;
		$harga_satuan_a = 0;
		$nilai_a = 0;
		$pembelian_a = 0;
		$kuantiti_sol_a = 0;
		$kuantiti_titip_a = 0;
		$kuantiti_bonus_a = 0;
		$kuantiti_diskon_a = 0;
		$harga_sat_pemb_a = 0;
		$nilai_pemb_a = 0;
		$harga_pokok_a = 0;
		$penjualan_a = 0;
		$pengeluaran_a = 0;
		$saldo_akhir_a = 0;
		$nilai_akhir_a = 0;
		$retur1_a = 0;
		$retur2_a = 0;


		$dist_a = $this->uom_model->get_distribution($data);
		$html = '';

		foreach ($dist_a as $dist_as) {

			$datat = array(
				'principal' => $dist_as['dist_id'],
				'tgl_akhir' => $params['end_time'],
				'tgl_awal' => date_format(date_create($params['end_time']), "Y-m-01")


			);

			//print_r($datat);

			$params_awalss['principal']		= $dist_as['dist_id'];
			$params_awalss['tgl_akhir']	= date("Y-m-d", strtotime(date_format(date_create($params['end_time']), "Y-m-01") . "-1 days"));
			$params_awalss['tgl_awal']	= date_format(date_create($params_awalss['tgl_akhir']), "Y-m-01");

			//print_r($params_awalss);die;

			//print_r($params_awalss);die;
			if ($datat['tgl_awal'] == '2021-01-01') {
				$result = $this->uom_model->get_report_persediaan($datat);
				$result_awal = $this->uom_model->get_report_persediaan($datat);
			} else {
				$result_awal = $this->uom_model->get_report_persediaan_newest($params_awalss);
				$result = $this->uom_model->get_report_persediaan_newest($datat);
			}
			// echo "<pre>";
			//print_r($result_awal);die; 


			$kuantiti = 0;
			$harga_satuan = 0;
			$nilai = 0;
			$pembelian = 0;
			$kuantiti_sol = 0;
			$kuantiti_titip = 0;
			$kuantiti_bonus = 0;
			$kuantiti_diskon = 0;
			$harga_sat_pemb = 0;
			$nilai_pemb = 0;
			$harga_pokok = 0;
			$penjualan = 0;
			$pengeluaran = 0;
			$saldo_akhir = 0;
			$nilai_akhir = 0;
			$prin = '';
			$retur1 = 0;
			$retur2 = 0;
			$kode_prin = '';

			$int_iuy = 0;
			foreach ($result as $datas) {

				if ($datat['tgl_awal'] == '2021-01-01') {
					$datas['harga_satuan'] = $datas['harga_satuan'];
				} else {
					$datas['harga_satuan'] = $result_awal[$int_iuy]['harga_pokok'];
				}

				$html .= '<tr>
								<th >' . $datas['stock_code'] . '</th>
								<th>' . $datas['stock_name'] . '</th>
								<th>' . $datas['base_qty'] . ' ' . $datas['uom_symbol'] . ' x ' . $datas['unit_box'] . '</th>
								<th>' . number_format($datas['saldo_awals'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['harga_satuan'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['saldo_awals'] * $datas['harga_satuan'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['pembelian'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['kuantiti_diskon'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['kuantiti_titip'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['kuantiti_bonus'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['harga_sat_pemb'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['nilai_pemb'], 2, ',', '.') . '</th>
								<th>' . $datas['ket'] . '</th>
								<th>' . number_format($datas['harga_pokok'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['penjualan'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['pengeluaran'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['saldo_akhir'], 2, ',', '.') . '</th>
								<th>' . number_format($datas['saldo_akhir'] * $datas['harga_pokok'], 2, ',', '.') . '</th>
							</tr>';

				$prin = $datas['name_eksternal'];
				$kode_prin = substr($datas['stock_code'], 0, 3);
				$kuantiti = $kuantiti + $datas['saldo_awals'];
				$harga_satuan = $harga_satuan + $datas['harga_satuan'];
				$nilai = $nilai + $datas['saldo_awals'] * $datas['harga_satuan'];
				$pembelian = $pembelian + $datas['pembelian'];
				$kuantiti_sol = $kuantiti_sol + $datas['kuantiti_sol'];
				$kuantiti_diskon = $kuantiti_diskon + $datas['kuantiti_diskon'];
				$kuantiti_titip = $kuantiti_titip + $datas['kuantiti_titip'];
				$kuantiti_bonus = $kuantiti_bonus + $datas['kuantiti_bonus'];
				$harga_sat_pemb = $harga_sat_pemb + $datas['harga_sat_pemb'];
				$nilai_pemb = $nilai_pemb + $datas['nilai_pemb'];
				$harga_pokok = $harga_pokok + $datas['harga_pokok'];
				$penjualan = $penjualan + $datas['penjualan'];
				$pengeluaran = $pengeluaran + $datas['pengeluaran'];
				$saldo_akhir = $saldo_akhir + $datas['saldo_akhir'];
				$retur1 = $retur1 + $datas['qty_retur_jual'];
				$retur2 = $retur1 + 0;
				$nilai_akhir = $nilai_akhir + $datas['saldo_akhir'] * $datas['harga_pokok'];

				$kuantiti_a = $kuantiti_a + $datas['saldo_awals'];
				$harga_satuan_a = $harga_satuan_a +  $datas['harga_satuan'];
				$nilai_a = $nilai_a + $datas['saldo_awals'] * $datas['harga_satuan'];
				$pembelian_a = $pembelian_a +  $datas['pembelian'];
				$kuantiti_sol_a = $kuantiti_sol_a +  $datas['kuantiti_sol'];
				$kuantiti_titip_a = $kuantiti_titip_a +  $datas['kuantiti_titip'];
				$kuantiti_diskon_a = $kuantiti_diskon_a +  $datas['kuantiti_diskon'];
				$kuantiti_bonus_a = $kuantiti_bonus_a +  $datas['kuantiti_bonus'];
				$harga_sat_pemb_a = $harga_sat_pemb_a +  $datas['harga_sat_pemb'];
				$nilai_pemb_a = $nilai_pemb_a +  $datas['nilai_pemb'];
				$harga_pokok_a = $harga_pokok_a + $datas['harga_pokok'];
				$penjualan_a = $penjualan_a +  $datas['penjualan'];
				$pengeluaran_a = $pengeluaran_a +  $datas['pengeluaran'];
				$saldo_akhir_a = $saldo_akhir_a +  $datas['saldo_akhir'];
				$nilai_akhir_a = $nilai_akhir_a +  $datas['saldo_akhir'] * $datas['harga_pokok'];
				$retur1_a = $retur1 + $datas['qty_retur_jual'];
				$retur2_a = $retur1 + 0;

				$int_iuy++;
			}

			$html .= '<tr style="background:yellow">
							<th>' . $kode_prin . '</th>
							<th>' . $prin . '</th>
							<th></th>
							<th>' . number_format($kuantiti, 2, ',', '.') . '</th>
							<th>' . number_format($harga_satuan, 2, ',', '.') . '</th>
							<th>' . number_format($nilai, 2, ',', '.') . '</th>
							<th>' . number_format($pembelian, 2, ',', '.') . '</th>
							<th>' . number_format($kuantiti_diskon, 2, ',', '.') . '</th>
							<th>' . number_format($kuantiti_titip, 2, ',', '.') . '</th>
							<th>' . number_format($kuantiti_bonus, 2, ',', '.') . '</th>
							<th>' . number_format($harga_sat_pemb, 2, ',', '.') . '</th>
							<th>' . number_format($nilai_pemb, 2, ',', '.') . '</th>
							<th></th>
							<th>' . number_format($harga_pokok, 2, ',', '.') . '</th>
							<th>' . number_format($penjualan, 2, ',', '.') . '</th>
							<th>' . number_format($pengeluaran, 2, ',', '.') . '</th>
							<th>' . number_format($saldo_akhir, 2, ',', '.') . '</th>
							<th>' . number_format($nilai_akhir, 2, ',', '.') . '</th>
						</tr>';
		}

		$html .= '<tr style="background:green">
						<th colspan="3">Total </th>
						<th>' . number_format($kuantiti_a, 2, ',', '.') . '</th>
						<th>' . number_format($harga_satuan_a, 2, ',', '.') . '</th>
						<th>' . number_format($nilai_a, 2, ',', '.') . '</th>
						<th>' . number_format($pembelian_a, 2, ',', '.') . '</th>
						<th>' . number_format($kuantiti_diskon_a, 2, ',', '.') . '</th>
						<th>' . number_format($kuantiti_titip_a, 2, ',', '.') . '</th>
						<th>' . number_format($kuantiti_bonus_a, 2, ',', '.') . '</th>
						<th>' . number_format($harga_sat_pemb_a, 2, ',', '.') . '</th>
						<th>' . number_format($nilai_pemb_a, 2, ',', '.') . '</th> 
						<th></th>
						<th>' . number_format($harga_pokok_a, 2, ',', '.') . '</th>
						<th>' . number_format($penjualan_a, 2, ',', '.') . '</th>
						<th>' . number_format($pengeluaran_a, 2, ',', '.') . '</th>
						<th>' . number_format($saldo_akhir_a, 2, ',', '.') . '</th>
						<th>' . number_format($nilai_akhir_a, 2, ',', '.') . '</th>
					</tr>';

		$results['html'] = $html;

		$datae['persediaan_awal'] = $nilai_a;
		$datae['persediaan_akhir'] = $nilai_akhir_a;
		$datae['pembelian'] = $nilai_pemb_a;

		return $datae;
		//$this->output->set_content_type('application/json')->set_output(json_encode($results));
		//print_r($result);die;


	}


	function filter() {

		$data   = file_get_contents("php://input");
		$param   = json_decode($data, true);


		$params  = array(
			'start_time' => $param['start_time'],
			'end_time' => $param['end_time'],
			'report' => "aktiva"
		);

		$params_p  = array(
			'start_time' => $param['start_time'],
			'end_time' => $param['end_time'],
			'report' => "passiva"
		);

		$params_start  = array(
			'start_time' => "2021-01-01",
			'end_time' => "2021-01-31",
			'report' => "aktiva"
		);

		$s_dater = explode('-', $param['end_time']);
		$start_dater = $s_dater[0] . '-' . $s_dater[0] . '-01';

		$params_terur  = array(
			'start_time' => $start_dater,
			'end_time' => $param['end_time'],
			'report' => "aktiva"
		);


		//$datas['get_piutang'] = $this->uom_model->get_piutang($params);
		$datas['get_piutang'] = $this->reports_piutang($params);
		//$datas['get_persediaan'] = $this->uom_model->get_persediaan($params);
		$datas['get_persediaan'] = $this->reports_persediaan($params);
		$datas['get_hutang'] = $this->get_hutang($params);
		$datas['get_penjualan'] = $this->uom_model->get_penjualan($params);
		$datas['get_pembelian'] = $this->uom_model->get_pembelian($params);
		$datas['get_ppn'] = $this->uom_model->get_ppn_new($params_terur);
		


		// print_r($datas['get_penjualan']);
		// echo "<br>";
		// print_r($datas['get_pembelian']);
		// die;


		if ($param['end_time'] > '2021-01-01') {

			$datas['get_aktiva'] = $this->uom_model->get_report_keu($params);
			$datas['get_aktiva_saldo'] = $this->uom_model->get_report_keu($params_start);
			$datas['get_report_keu_now'] = $this->uom_model->get_report_keu_now($params);
			//$datas['get_laba_rugi_saldo'] = $this->laba_rugi($params_start);
			$datas['get_pasiva'] = $this->uom_model->get_report_keu($params_p);
			$datas['get_retur'] = $this->rl_model->get_retur($params_terur);
			

			//print_r($datas['get_retur']);die;

			$html_a = '';

			$tot_a = 0;
			foreach ($datas['get_aktiva'] as $get_aktivas) {



				if ($get_aktivas['coa'] == 11301) {
					$html_a .= ' <tr>
                  <th >' . $get_aktivas['coa'] . '</th>
                  <th>' . $get_aktivas['keterangan'] . '</th>
                  <th>' . number_format($datas['get_piutang']['piutang'], 2, '.', ',') . '</th>
                </tr>';
					$tot_a += $datas['get_piutang']['piutang'];
				} elseif ($get_aktivas['coa'] == 11600) {
					$html_a .= ' <tr>
                  <th >' . $get_aktivas['coa'] . '</th>
                  <th>' . $get_aktivas['keterangan'] . '</th>
                  <th>' . number_format($datas['get_persediaan']['persediaan_akhir'], 2, '.', ',') . '</th>
                </tr>';
					$tot_a += $datas['get_persediaan']['persediaan_akhir'];
				} elseif ($get_aktivas['coa'] == 12102 || $get_aktivas['coa'] == 12202 || $get_aktivas['coa'] == 12302) {

					$get_depresiasi = $this->uom_model->get_depresiasi($params_p, $get_aktivas['id_coa']);

					$html_a .= ' <tr>
                  <th >' . $get_aktivas['coa'] . '</th>
                  <th>' . $get_aktivas['keterangan'] . '</th>
                  <th>' . number_format($get_depresiasi[0]['nilai'], 2, '.', ',') . '</th>
                </tr>';
					$tot_a += $get_depresiasi[0]['nilai'];
				} elseif ($get_aktivas['coa'] == 70100) {

					if ($param['end_time'] == "2020-12-31") {

						$html_a .= ' <tr>
					  <th >' . $get_aktivas['coa'] . '</th>
					  <th>' . $get_aktivas['keterangan'] . '</th>
					  <th>' . number_format(($get_aktivas['nilai'] + ($datas['get_ppn'][0]['nilai_ppn'])), 2, '.', ',') . '</th>
					</tr>';
						$tot_a += ($get_aktivas['nilai'] + ($datas['get_ppn'][0]['nilai_ppn']));
					} else {

						$sal_ppn = 0;
						$now_ppn = 0;
						// foreach ($datas['get_aktiva_saldo'] as $get_aktiva_saldo) {

							// if ($get_aktiva_saldo['coa'] == 70100) {

								// //echo $get_aktiva_saldo['nilai'];die;
								// $sal_ppn += $get_aktiva_saldo['nilai'] + ($datas['get_laba_rugi_saldo']['ppn']);
							// }
						// }

						// foreach ($datas['get_report_keu_now'] as $get_aktiva_now) {

							// if ($get_aktiva_now['coa'] == 70100) {

								// //echo $get_aktiva_saldo['nilai'];die;
								// $now_ppn += $get_aktiva_now['nilai'];;
								// $now_ppn = 0;
							// }
						// }

						//$saldo_ppn = $sal_ppn + ($now_ppn + ($datas['get_retur'][0]['nilai_retur'] * 0.1) + $datas['get_pembelian'][0]['pembelian']) - $datas['get_penjualan'][0]['penjualan'];
						$saldo_ppn = $datas['get_ppn'][0]['nilai_ppn']; // nilai ppn fix

						$html_a .= ' <tr>
					  <th >' . $get_aktivas['coa'] . '</th>
					  <th>' . $get_aktivas['keterangan'] . '</th>
					 <!-- <th>' . $sal_ppn . ' ' . $now_ppn . ' ' . ($datas['get_retur'][0]['nilai_retur'] * 0.1) . ' ' . $datas['get_pembelian'][0]['pembelian'] . ' ' . $datas['get_penjualan'][0]['penjualan'] . '</th> -->
					 <th>' . number_format($saldo_ppn, 2, '.', ',') . '</th>
					</tr>';
						$tot_a += $saldo_ppn;
						// $html_a .= ' <tr>
						// <th >'.$get_aktivas['coa'].'</th>
						// <th>'.$get_aktivas['keterangan'].'</th>
						// <th>'.number_format(($get_aktivas['nilai']+($datas['get_laba_rugi']['ppn'])),2,'.',',').'</th>
						// </tr>';
						// $tot_a +=($get_aktivas['nilai']+($datas['get_laba_rugi']['ppn']));


					}
				} else {

					if ($get_aktivas['id_parent'] == 3) {

						$params_c  = array(
							'start_date' => $param['start_time'],
							'end_date' => $param['end_time'],
							'coa_id' => $get_aktivas['id_coa']
						);

						$get_cash = $this->get_cash($params_c);

						$html_a .= ' <tr>
					<th >' . $get_aktivas['coa'] . '</th>
					<th>' . $get_aktivas['keterangan'] . '</th>
					<th>' . number_format($get_cash, 2, '.', ',') . '</th>
					</tr>';
						$tot_a += $get_cash;
					} else {
						$html_a .= ' <tr>
					<th >' . $get_aktivas['coa'] . '</th>
					<th>' . $get_aktivas['keterangan'] . '</th>
					<th>' . number_format($get_aktivas['nilai'], 2, '.', ',') . '</th>
					</tr>';
						$tot_a += $get_aktivas['nilai'];
					}
				}
			}

			$html_a .= ' <tr>
					<th colspan=2>Total</th>
					<th>' . number_format($tot_a, 2, '.', ',') . '</th>
				</tr>';

			$html_p = '';


			$tot_p = 0;
			foreach ($datas['get_pasiva'] as $get_aktivas) {

				if ($get_aktivas['coa'] == 20100) {
					$html_p .= ' <tr>
                  <th >' . $get_aktivas['coa'] . '</th>
                  <th>' . $get_aktivas['keterangan'] . '</th>
                  <th>' . number_format($datas['get_hutang'], 2, '.', ',') . '</th>
                </tr>';
					$tot_p += $datas['get_hutang'];
				} elseif ($get_aktivas['coa'] == 30400) {
					
					if ($param['end_time'] <= '2021-02-28'){ 
					
						$datas['get_laba_rugi'] = $this->laba_rugi($params);
					
						$html_p .= ' <tr>
						  <th >' . $get_aktivas['coa'] . '</th>
						  <th>' . $get_aktivas['keterangan'] . '</th>
						<th>' . number_format(($get_aktivas['nilai'] + ($datas['get_laba_rugi']['total_rl'])), 2, '.', ',') . '</th>
						   <!-- <th>' . number_format(($get_aktivas['nilai']), 2, '.', ',') .' - '. number_format(($datas['get_laba_rugi']['total_rl']), 2, '.', ',').' == '. number_format(($get_aktivas['nilai'] + ($datas['get_laba_rugi']['total_rl'])), 2, '.', ',') . '</th> -->
						</tr>';
						$tot_p += ($get_aktivas['nilai'] + ($datas['get_laba_rugi']['total_rl']));
					
					}else{
						
						$params_rl1  = array(
							'start_time' => $param['end_time'],
							'end_time' => "2021-02-28",
							'report' => "laba_rugi"
						);
						
						$params_rl_a  = array(
							'start_time' => $param['end_time'],
							'end_time' => $param['end_time'],
							'report' => "laba_rugi"
						);
						
						$datas['get_laba_rugi'] = $this->laba_rugi($params_rl1);
						$datas['get_laba_rugi_noe'] = $this->laba_rugi_s2($params_rl_a);
					
						$html_p .= ' <tr>
						  <th >' . $get_aktivas['coa'] . '</th>
						  <th>' . $get_aktivas['keterangan'] . '</th>
						<th>' . number_format(($get_aktivas['nilai'] + ($datas['get_laba_rugi']['total_rl']) + ($datas['get_laba_rugi_noe']['total_rl']) ), 2, '.', ',') . '</th>
						   <!-- <th>' . number_format(($get_aktivas['nilai']), 2, '.', ',') .' - '. number_format(($datas['get_laba_rugi']['total_rl']), 2, '.', ',').' == '. number_format(($get_aktivas['nilai'] + ($datas['get_laba_rugi']['total_rl'])), 2, '.', ',') . '</th> -->
						</tr>';
						$tot_p += ($get_aktivas['nilai'] + ($datas['get_laba_rugi']['total_rl']) + ($datas['get_laba_rugi_noe']['total_rl']));
						
					}
					
				} else {
					$html_p .= ' <tr>
                  <th >' . $get_aktivas['coa'] . '</th>
                  <th>' . $get_aktivas['keterangan'] . '</th>
                  <th>' . number_format($get_aktivas['nilai'], 2, '.', ',') . '</th>
                </tr>';
					$tot_p += $get_aktivas['nilai'];
				}
			}

			$html_p .= ' <tr>
					<th colspan=2>Total</th>
					<th>' . number_format($tot_p, 2, '.', ',') . '</th>
				</tr>';

			$datas['t_ac'] = $html_a;
			$datas['t_pa'] = $html_p;
		} else {

			$params  = array(
				'start_time' => date('Y-m-d'),
				'end_time' => date('Y-m-d'),
				'report' => "aktiva_saldo"
			);

			$datas['get_aktiva'] = $this->uom_model->get_report_keu_awal($params);
			$datas['get_pasiva'] = $this->uom_model->get_report_keu($params_p);


			$html_a = '';

			$tot_a = 0;

			foreach ($datas['get_aktiva'] as $get_aktivas) {

				$html_a .= ' <tr>
					<th >' . $get_aktivas['coa'] . '</th>
					<th>' . $get_aktivas['keterangan'] . '</th>
					<th>' . number_format($get_aktivas['nilai'], 2, '.', ',') . '</th>
					</tr>';
				$tot_a += $get_aktivas['nilai'];
			}

			$html_a .= ' <tr>
					<th colspan=2>Total</th>
					<th>' . number_format($tot_a, 2, '.', ',') . '</th>
				</tr>';


			$html_p = '';
			$tot_p = 0;

			foreach ($datas['get_pasiva'] as $get_aktivas) {

				$html_p .= ' <tr>
					  <th >' . $get_aktivas['coa'] . '</th>
					  <th>' . $get_aktivas['keterangan'] . '</th>
					  <th>' . number_format($get_aktivas['nilai'], 2, '.', ',') . '</th>
					</tr>';
				$tot_p += $get_aktivas['nilai'];
			}


			$html_p .= ' <tr>
					<th colspan=2>Total</th>
					<th>' . number_format($tot_p, 2, '.', ',') . '</th>
				</tr>';

			$datas['t_ac'] = $html_a;
			$datas['t_pa'] = $html_p;
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($datas));
		//print_r($datas['get_aktiva']);die;

	}


	public function get_cash($param) {

		$params['coa_id']		= $param['coa_id'];
		$params['start_date']	= $param['start_date'];
		$params['end_date']		= $param['end_date'];

		if ($params['end_date'] == "2021-01-01") {

			$list_saldo = $this->uom_model->list_report_cash_saldo($params);

			$list = $this->uom_model->list_report_cash($params);
			$typeCoa = ['Debit', 'Kredit'];
			$data = [];
			$balance = 0.00;

			foreach ($list_saldo['data'] as $row) {

				array_push($data, [
					'', // tanggal
					'', // tanggal
					// $typeCoa[$row['type_cash']] ?? '-', // transaksi
					'',
					'Saldo Awal', // transaksi
					// kode akun
					'', // akun
					'', // uraian
					'-', // debit
					'-', // kredit
					indonesia_currency_format($row['saldo_awal']), // saldo
					// '-', // action
				]);


				$balance = $row['saldo_awal'];
			}

			foreach ($list['data'] as $row) {

				if ($row['type_cash'] == 0) {
					$balance += $row['sum_amnt'];
				} else if ($row['type_cash'] == 1) {
					$balance -= $row['sum_amnt'];
				}

				// if($row['kr_note'] == 'Utang Dagang' || $row['kr_note'] == 'Piutang Dagang'){ 
				// $row['banks'] = $row['keterangan']; 
				// }

				// if ($row['bukti'] == "Bayar Piutang") {
				// $faktur = $row['jual'];
				// $note_ac = $row['cust_name'];
				// }elseif ($row['bukti'] == "Bayar Hutang") {
				// $faktur = $row['beli'];
				// $note_ac = $row['name_eksternal'];
				// }else{
				$faktur = '';
				$note_ac = $row['k_note'] . '' . $row['users'];
				// }

				array_push($data, [
					$row['date'], // tanggal
					$row['banks'], // tanggal
					// $typeCoa[$row['type_cash']] ?? '-', // transaksi
					$row['kr_coa'],
					$row['kr_note'], // transaksi
					// kode akun 
					$note_ac, // akun
					$row['invoice'], // uraian
					$row['type_cash'] == 0 ? indonesia_currency_format($row['sum_amnt']) : '-', // debit
					$row['type_cash'] == 1 ? indonesia_currency_format($row['sum_amnt']) : '-', // kredit
					indonesia_currency_format($balance), // saldo
					// '-', // action
				]);
			}
		} else {

			$params_awal['coa_id']		= $params['coa_id'];
			$params_awal['start_date']	= "2021-01-01";
			$params_awal['end_date']	= date("Y-m-d", strtotime(date_format(date_create($param['end_date']), "Y-m-01") . "-1 days"));

			$paramss['coa_id']		= $params['coa_id'];
			$paramss['start_date']	= date_format(date_create($param['end_date']), "Y-m-01");
			$paramss['end_date']	= $param['end_date'];


			// print_r($params_awal);

			// print_r($paramss);die;

			$list_saldo = $this->uom_model->list_report_cash_saldo($params_awal);

			$list_awal = $this->uom_model->list_report_cash($params_awal);

			$list = $this->uom_model->list_report_cash($paramss);

			//print_r($list);die;

			$typeCoa = ['Debit', 'Kredit'];
			$data = [];
			$balance = 0.00;

			foreach ($list_saldo['data'] as $row) {

				// array_push($data, [
				// '', // tanggal
				// '', // tanggal
				// // $typeCoa[$row['type_cash']] ?? '-', // transaksi
				// '', 
				// 'Saldo Awal', // transaksi
				// // kode akun
				// '', // akun
				// '', // uraian
				// '-', // debit
				// '-', // kredit
				// indonesia_currency_format($row['saldo_awal']), // saldo
				// // '-', // action
				// ]);


				$balance = $row['saldo_awal'];
			}

			//echo $balance;die;

			foreach ($list_awal['data'] as $row) {

				if ($row['type_cash'] == 0) {
					$balance += $row['sum_amnt'];
				} else if ($row['type_cash'] == 1) {
					$balance -= $row['sum_amnt'];
				}
			}

			//echo $balance;die; 

			array_push($data, [
				'', // tanggal
				'', // tanggal
				// $typeCoa[$row['type_cash']] ?? '-', // transaksi
				'',
				'Saldo Awal', // transaksi
				// kode akun
				'', // akun
				'', // uraian
				'-', // debit
				'-', // kredit
				indonesia_currency_format($balance), // saldo
				// '-', // action
			]);

			foreach ($list['data'] as $row) {

				if ($row['type_cash'] == 0) {
					$balance += $row['sum_amnt'];
				} else if ($row['type_cash'] == 1) {
					$balance -= $row['sum_amnt'];
				}

				// if($row['kr_note'] == 'Utang Dagang' || $row['kr_note'] == 'Piutang Dagang'){ 
				// $row['banks'] = $row['keterangan']; 
				// }

				// if ($row['bukti'] == "Bayar Piutang") {
				// $faktur = $row['jual'];
				// $note_ac = $row['cust_name'];
				// }elseif ($row['bukti'] == "Bayar Hutang") {
				// $faktur = $row['beli'];
				// $note_ac = $row['name_eksternal'];
				// }else{
				$faktur = '';
				$note_ac = $row['k_note'] . '' . $row['users'];
				// }

				array_push($data, [
					$row['date'], // tanggal
					$row['banks'], // tanggal
					// $typeCoa[$row['type_cash']] ?? '-', // transaksi
					$row['kr_coa'],
					$row['kr_note'], // transaksi
					// kode akun 
					$note_ac, // akun
					$row['invoice'], // uraian
					$row['type_cash'] == 0 ? indonesia_currency_format($row['sum_amnt']) : '-', // debit
					$row['type_cash'] == 1 ? indonesia_currency_format($row['sum_amnt']) : '-', // kredit
					indonesia_currency_format($balance), // saldo
					// '-', // action
				]);
			}
		}

		// foreach(){



		// }

		return $balance;

		//print_r($data);

	}

	public function get_hutang($param) {



		$data = array(
			'principal' => 0,
			'tgl_awal' => $param['start_time'],
			'tgl_akhir' => $param['end_time']

		);

		//print_r($data);die;

		$kuantiti_a = 0;
		$harga_satuan_a = 0;
		$nilai_a = 0;
		$pembelian_a = 0;
		$kuantiti_sol_a = 0;
		$kuantiti_titip_a = 0;
		$kuantiti_bonus_a = 0;
		$harga_sat_pemb_a = 0;
		$nilai_pemb_a = 0;
		$harga_pokok_a = 0;
		$penjualan_a = 0;
		$pengeluaran_a = 0;
		$saldo_akhir_a = 0;
		$nilai_akhir_a = 0;
		$invoice_saldo_a = 0;
		$d0_a = 0;
		$d30_a = 0;
		$d60_a = 0;
		$d90_a = 0;
		$d120_a = 0;
		$total_t_a = 0;

		if ($data['principal'] == 0) {

			$dist_a = $this->hutang_model->get_distribution($data);
			$html = '';

			foreach ($dist_a as $dist_as) {

				$datat = array(
					'principal' => $dist_as['dist_id'],
					'tgl_awal' => date_format(date_create($param['end_time']), "Y-m-01"),
					'tgl_akhir' => $param['end_time']

				);

				$result = $this->hutang_model->get_report_persediaan($datat);
				// echo "<pre>";
				// print_r($result);die;


				$kuantiti = 0;
				$harga_satuan = 0;
				$nilai = 0;
				$pembelian = 0;
				$kuantiti_sol = 0;
				$kuantiti_titip = 0;
				$kuantiti_bonus = 0;
				$harga_sat_pemb = 0;
				$nilai_pemb = 0;
				$harga_pokok = 0;
				$penjualan = 0;
				$pengeluaran = 0;
				$saldo_akhir = 0;
				$nilai_akhir = 0;
				$invoice_saldo = 0;
				$prin = '';
				$kode_prin = '';

				$d0 = 0;
				$d30 = 0;
				$d60 = 0;
				$d90 = 0;
				$d120 = 0;
				$total_t = 0;

				$curr_invoice = '';
				foreach ($result as $datas) {

					if ($curr_invoice <> $datas['no_invoice']) {

						$bayar_r_te =  number_format($datas['bayar'], 2, ',', '.');
						$bayar_r =  $datas['bayar'];
						$bb =  $datas['bayar'];
						$bb_saldo =  $datas['bayar_saldo'];
						$curr_invoice = $datas['no_invoice'];
						$sisa_r_te =  number_format(($datas['prices'] - ($datas['prices'] * ($datas['diskons'] / 100)) + (($datas['prices'] - ($datas['prices'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100))) - $datas['bayar'], 2, ',', '.');
						$sisa_r =  ($datas['prices'] - ($datas['prices'] * ($datas['diskons'] / 100)) + (($datas['prices'] - ($datas['prices'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100))) - $datas['bayar'];
						$curr_invoice = $datas['no_invoice'];
					} else {
						$bayar_r_te = null;
						$sisa_r_te = null;
						$bayar_r = 0;
						$bb_saldo = 0;
						$sisa_r = 0;
						$bb = 0;
					}

					$ttl = $datas['min_30'] - ($datas['min_30'] * ($datas['diskons'] / 100)) + (($datas['min_30'] - ($datas['min_30'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)) + $datas['d30'] - ($datas['d30'] * ($datas['diskons'] / 100)) + (($datas['d30'] - ($datas['d30'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)) + $datas['d60'] - ($datas['d60'] * ($datas['diskons'] / 100)) + (($datas['d60'] - ($datas['d60'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)) + $datas['d90'] - ($datas['d90'] * ($datas['diskons'] / 100)) + (($datas['d90'] - ($datas['d90'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)) + $datas['d120'] - ($datas['d120'] * ($datas['diskons'] / 100)) + (($datas['d120'] - ($datas['d120'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100));

					$prcs = $datas['prices'] - ($datas['prices'] * ($datas['diskons'] / 100)) + (($datas['prices'] - ($datas['prices'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100));

					//echo $datas['saldo'];die;

					if ($datas['saldo'] == 0) {
						$bb = 0;
					} else {
						$bb = $bb - $prcs;
					}

					if ($bb > 0) {
						$ss_sisa = 0;
					} else {
						$ss_sisa = abs($bb);
					}

					if ($datas['umur'] < 31) {
						$m1 = $ss_sisa;
						$m2 = 0;
						$m3 = 0;
						$m4 = 0;
						$m5 = 0;
					} elseif ($datas['umur'] > 30 && $datas['umur'] < 61) {
						$m1 = 0;
						$m2 = $ss_sisa;
						$m3 = 0;
						$m4 = 0;
						$m5 = 0;
					} elseif ($datas['umur'] > 60 && $datas['umur'] < 91) {
						$m1 = 0;
						$m2 = 0;
						$m3 = $ss_sisa;
						$m4 = 0;
						$m5 = 0;
					} elseif ($datas['umur'] > 90 && $datas['umur'] < 121) {
						$m1 = 0;
						$m2 = 0;
						$m3 = 0;
						$m4 = $ss_sisa;
						$m5 = 0;
					} else {
						$m1 = 0;
						$m2 = 0;
						$m3 = 0;
						$m4 = 0;
						$m5 = $ss_sisa;
					}

					$ttl = $m1 + $m2 + $m3 + $m4 + $m5;

					// $html .= '<tr>
					// <th >'.$datas['name_eksternal'].'</th>
					// <th>'.$datas['date_po'].'</th>
					// <th>'.$datas['items'].'</th>
					// <th>'.$datas['no_po'].'</th>
					// <th>'.$datas['no_invoice'].'</th>
					// <th>'.$datas['jatuh_tempo'].'</th>
					// <th>'.$datas['umur'].'</th>
					// <th>'.number_format($datas['saldo']-( $datas['saldo']*($datas['diskons']/100)) + ( ($datas['saldo']-( $datas['saldo']*($datas['diskons']/100) ))*($datas['pajak']/100) ),2,',','.').'</th>
					// <th>'.number_format($datas['bulan_berjalan']-( $datas['bulan_berjalan']*($datas['diskons']/100)) + ( ($datas['bulan_berjalan']-( $datas['bulan_berjalan']*($datas['diskons']/100) ))*($datas['pajak']/100) ),2,',','.').'</th>
					// <th>'.$datas['tgl_bayar'].'</th>
					// <th>'.$bayar_r_te.'</th>
					// <th>'.$sisa_r_te.'</th>
					// <th>'.number_format($datas['invoice_saldo'],2,',','.').'</th>
					// <th>'.number_format($datas['min_30']-( $datas['min_30']*($datas['diskons']/100)) + ( ($datas['min_30']-( $datas['min_30']*($datas['diskons']/100) ))*($datas['pajak']/100)),2,',','.').'</th>
					// <th>'.number_format($datas['d30']-( $datas['d30']*($datas['diskons']/100)) + ( ($datas['d30']-( $datas['d30']*($datas['diskons']/100) ))*($datas['pajak']/100) ),2,',','.').'</th>
					// <th>'.number_format($datas['d60']-( $datas['d60']*($datas['diskons']/100)) + ( ($datas['d60']-( $datas['d60']*($datas['diskons']/100) ))*($datas['pajak']/100) ),2,',','.').'</th>
					// <th>'.number_format($datas['d90']-( $datas['d90']*($datas['diskons']/100)) + ( ($datas['d90']-( $datas['d90']*($datas['diskons']/100) ))*($datas['pajak']/100) ),2,',','.').'</th>
					// <th>'.number_format($datas['d120']-( $datas['d120']*($datas['diskons']/100)) + ( ($datas['d120']-( $datas['d120']*($datas['diskons']/100) ))*($datas['pajak']/100) ),2,',','.').'</th>
					// <th>'.number_format($ttl,2,',','.').'</th>
					// </tr>';

					$date_filer = date('Y-m-d', strtotime($data['tgl_awal']));
					$date_data = date('Y-m-d', strtotime($datas['tanggal']));

					//	if($ss_sisa <> 0 || $date_data >= $date_filer){		

					$html .= '<tr>
									<th >' . $datas['name_eksternal'] . '</th>
								<th>' . $datas['date_po'] . '</th>
								<th>' . $datas['no_pos'] . '</th>
								<th>' . $datas['no_invoice'] . '</th>
								<th>' . $datas['jatuh_tempo'] . '</th>
								<th>' . $datas['umur'] . '</th>
								<th>' . number_format($datas['saldo'] - ($datas['saldo'] * ($datas['diskons'] / 100)) + (($datas['saldo'] - ($datas['saldo'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)) - $bb_saldo, 2, ',', '.') . '</th>
								<th>' . number_format($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskons'] / 100)) + (($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)), 2, ',', '.') . '</th>
								<th>' . $datas['tgl_bayar'] . '</th>
								<th>' . $bayar_r_te . '</th>
								<th>' . number_format($ss_sisa, 2, ',', '.') . '</th>
								<th>' . number_format($datas['invoice_saldo'], 2, ',', '.') . '</th>
								<th>' . number_format($m1, 2, ',', '.') . '</th>
								<th>' . number_format($m2, 2, ',', '.') . '</th>
								<th>' . number_format($m3, 2, ',', '.') . '</th>
								<th>' . number_format($m4, 2, ',', '.') . '</th>
								<th>' . number_format($m5, 2, ',', '.') . '</th>
								<th>' . number_format($ttl, 2, ',', '.') . '</th>
							</tr>';

					$prin = $datas['name_eksternal'];


					$curr_invoice = $datas['no_invoice'];

					$penjualan = $penjualan + ($datas['saldo'] - ($datas['saldo'] * ($datas['diskons'] / 100)) + (($datas['saldo'] - ($datas['saldo'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100))) - $bb_saldo;
					$pengeluaran = $pengeluaran + ($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskons'] / 100)) + (($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)));
					$saldo_akhir = $saldo_akhir + $bayar_r;
					$nilai_akhir = $nilai_akhir + $datas['bayar'];
					$invoice_saldo = $invoice_saldo + $datas['invoice_saldo'];

					// $d0 = $d0 + $datas['min_30']-( $datas['min_30']*($datas['diskons']/100)) + ( ($datas['min_30']-( $datas['min_30']*($datas['diskons']/100) ))*($datas['pajak']/100));
					// $d30 = $d30 + $datas['d30']-( $datas['d30']*($datas['diskons']/100)) + ( ($datas['d30']-( $datas['d30']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// $d60 = $d60 + $datas['d60']-( $datas['d60']*($datas['diskons']/100)) + ( ($datas['d60']-( $datas['d60']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// $d90 = $d90 + $datas['d90']-( $datas['d90']*($datas['diskons']/100)) + ( ($datas['d90']-( $datas['d90']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// $d120 = $d120 + $datas['d120']-( $datas['d120']*($datas['diskons']/100)) + ( ($datas['d120']-( $datas['d120']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// $total_t = $total_t + $ttl;

					$d0 = $d0 + $m1;
					$d30 = $d30 + $m2;
					$d60 = $d60 + $m3;
					$d90 = $d90 + $m4;
					$d120 = $d120 + $m5;
					$total_t = $total_t + $ttl;

					$penjualan_a = $penjualan_a +  ($datas['saldo'] - ($datas['saldo'] * ($datas['diskons'] / 100)) + (($datas['saldo'] - ($datas['saldo'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100))) - $bb_saldo;
					$pengeluaran_a = $pengeluaran_a +  ($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskons'] / 100)) + (($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)));
					$saldo_akhir_a = $saldo_akhir_a +  $bayar_r;
					$nilai_akhir_a = $nilai_akhir_a +  $datas['bayar'];
					$invoice_saldo_a = $invoice_saldo_a +  $datas['invoice_saldo'];

					// $d0_a = $d0_a + $datas['min_30']-( $datas['min_30']*($datas['diskons']/100)) + ( ($datas['min_30']-( $datas['min_30']*($datas['diskons']/100) ))*($datas['pajak']/100));
					// $d30_a = $d30_a + $datas['d30']-( $datas['d30']*($datas['diskons']/100)) + ( ($datas['d30']-( $datas['d30']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// $d60_a = $d60_a + $datas['d60']-( $datas['d60']*($datas['diskons']/100)) + ( ($datas['d60']-( $datas['d60']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// $d90_a = $d90_a + $datas['d90']-( $datas['d90']*($datas['diskons']/100)) + ( ($datas['d90']-( $datas['d90']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// $d120_a = $d120_a + $datas['d120']-( $datas['d120']*($datas['diskons']/100)) + ( ($datas['d120']-( $datas['d120']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// $total_t_a = $total_t_a + $ttl;

					$d0_a = $d0_a + $m1;
					$d30_a = $d30_a + $m2;
					$d60_a = $d60_a + $m3;
					$d90_a = $d90_a + $m4;
					$d120_a = $d120_a + $m5;
					$total_t_a = $total_t_a + $ttl;
					//	}
				}

				$html .= '<tr style="background:yellow">
							<th>' . $prin . '</th>
							<th></th>
							<th ></th>
							<th ></th>	
							<th></th>
							<th></th>
							<th>' . number_format($penjualan, 2, ',', '.') . '</th>
							<th>' . number_format($pengeluaran, 2, ',', '.') . '</th>
							<th></th>
							<th>' . number_format($saldo_akhir, 2, ',', '.') . '</th>
							<th>' . number_format($penjualan + $pengeluaran - $saldo_akhir, 2, ',', '.') . '</th>
							<th>' . number_format($invoice_saldo, 2, ',', '.') . '</th>
							<th>' . number_format($d0, 2, ',', '.') . '</th>
							<th>' . number_format($d30, 2, ',', '.') . '</th>
							<th>' . number_format($d60, 2, ',', '.') . '</th>
							<th>' . number_format($d90, 2, ',', '.') . '</th>
							<th>' . number_format($d120, 2, ',', '.') . '</th>
							<th>' . number_format($penjualan + $pengeluaran - $saldo_akhir, 2, ',', '.') . '</th>
						</tr>';
			}
		}

		$html .= '<tr style="background:green">
						<th colspan="3">Total </th>
						<th></th>
							<th ></th>
							<th></th>
						<th>' . number_format($penjualan_a, 2, ',', '.') . '</th>
						<th>' . number_format($pengeluaran_a, 2, ',', '.') . '</th>
						<th></th>
						<th>' . number_format($saldo_akhir_a, 2, ',', '.') . '</th>
						<th>' . number_format($penjualan_a + $pengeluaran_a - $saldo_akhir_a, 2, ',', '.') . '</th>
						<th>' . number_format($invoice_saldo_a, 2, ',', '.') . '</th>
							<th>' . number_format($d0_a, 2, ',', '.') . '</th>
							<th>' . number_format($d30_a, 2, ',', '.') . '</th>
							<th>' . number_format($d60_a, 2, ',', '.') . '</th>
							<th>' . number_format($d90_a, 2, ',', '.') . '</th>
							<th>' . number_format($d120_a, 2, ',', '.') . '</th>
							<th>' . number_format($penjualan_a + $pengeluaran_a - $saldo_akhir_a, 2, ',', '.') . '</th>
					</tr>';

		$results['html'] = $html;

		//return $total_t_a;
		return $penjualan_a + $pengeluaran_a - $saldo_akhir_a;
		//$this->output->set_content_type('application/json')->set_output(json_encode($results));
		//print_r($html);die;


	}




	function lists() {

		if (!empty($_GET['sess_user_id'])) {
			$sess_user_id = $_GET['sess_user_id'];
		} else {
			$sess_user_id = NULL;
		}

		if (!empty($_GET['sess_token'])) {
			$sess_token = $_GET['sess_token'];
		} else {
			$sess_token = NULL;
		}

		if ($this->input->get_post('draw') != FALSE) {
			$draw   = $this->input->get_post('draw');
		} else {
			$draw   = 1;
		};
		if ($this->input->get_post('length') != FALSE) {
			$length = $this->input->get_post('length');
		} else {
			$length = 10;
		};
		if ($this->input->get_post('start') != FALSE) {
			$start  = $this->input->get_post('start');
		} else {
			$start  = 0;
		};
		$order_fields = array('coa'); // , 'COST'
		$order = $this->input->get_post('order');
		if (!empty($order[0]['dir'])) {
			$order_dir    = $order[0]['dir'];
		} else {
			$order_dir    = 'desc';
		};
		if (!empty($order[0]['column'])) {
			$order_column = $order[0]['column'];
		} else {
			$order_column = 0;
		};

		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['sess_user_id'] = $sess_user_id;
		$params['sess_token'] 	= $sess_token;
		$params['searchtxt'] 	= $_GET['search']['value'];


		//print_r($params);die;

		$list = $this->uom_model->lists($params);
		$priv = $this->priv->get_priv();
		//print_r($list);die;

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		//print_r($result);die;

		$data = array();
		$i = $params['offset'];
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;


			$status_akses = '
                <div class="btn-group" style="display:' . $priv['update'] . '"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edituom(\'' . $v['id'] . '\')"><i class="fa fa-edit"></i></button></div>
				  <div class="btn-group" style="display:' . $priv['delete'] . '"><button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteuom(\'' . $v['id'] . '\')"><i class="fa fa-trash"></i></button></div>';

			if ($v['type_cash'] == 0) {
				$sss = 'Pemasukan';
			} else {
				$sss = 'Pengeluaran';
			}

			array_push($data, array(
				$i,
				$v['coa'] . '-' . $v['keterangan'],
				$sss,
				number_format($v['value_real'], 2, ',', '.'),
				$v['note'],
				$status_akses
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add() {


		$coa = $this->uom_model->get_coa();



		// $this->load->view('add_modal_view', $data);

		// $result = $this->distributor_model->location();

		$data = array(
			'group' => '',
			'coa' => $coa
		);

		$this->template->load('maintemplate', 'neraca/views/add_modal_view', $data);
	}

	public function edit() {
		//$id = $this->Anti_sql_injection($this->input->post('sid', TRUE));
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('iduom', TRUE)),
		);
		$result = $this->uom_model->get_uom_data($this->Anti_sql_injection($data['id']));
		$coa = $this->uom_model->get_coa();

		//print_r($result);die;
		// $roles = $this->uom_model->roles($id);

		$data = array(
			'uom' => $result[0],
			'id' => $this->Anti_sql_injection($this->input->post('iduom', TRUE)),
			'coa' => $coa
		);

		$this->template->load('maintemplate', 'neraca/views/edit_modal_view', $data);
	}

	public function deletes() {

		$data   = file_get_contents("php://input");
		$params   = json_decode($data, true);

		$list = $this->uom_model->deletes($this->Anti_sql_injection($params['id']));

		$res = array(
			'status' => 'success',
			'message' => 'Data telah di hapus'
		);

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function edit_uom() {
		$this->form_validation->set_rules('coa', 'coa', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('tgl', 'tgl', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('cash', 'cash', 'trim|required|max_length[255]');
		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));
			$result = array(
				'success' => false,
				'message' => $msg
			);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			// die;
			$message = "";
			$data = array(
				'user_id' => $this->session->userdata['logged_in']['user_id'],
				'coa'     => $this->Anti_sql_injection($this->input->post('coa', TRUE)),
				'id'     => $this->Anti_sql_injection($this->input->post('id', TRUE)),
				'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
				'cash' => $this->Anti_sql_injection($this->input->post('cash', TRUE)),
				'jumlah' => $this->Anti_sql_injection($this->input->post('jumlah', TRUE)),
				'ket' => $this->Anti_sql_injection($this->input->post('ket', TRUE))
			);
			$result = $this->uom_model->edit_uom($data);
			if ($result > 0) {
				$msg = 'Berhasil merubah uom.';

				$result = array(
					'success' => true,
					'message' => $msg
				);
			} else {
				$msg = 'Gagal merubah uom.';

				$result = array(
					'success' => false,
					'message' => $msg
				);
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function add_uom() {
		$this->form_validation->set_rules('coa', 'coa', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('tgl', 'tgl', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('cash', 'cash', 'trim|required|max_length[255]');
		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));
			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			// die;
			$message = "";
			$data = array(
				'user_id' => $this->session->userdata['logged_in']['user_id'],
				'coa'     => $this->Anti_sql_injection($this->input->post('coa', TRUE)),
				'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
				'cash' => $this->Anti_sql_injection($this->input->post('cash', TRUE)),
				'jumlah' => $this->Anti_sql_injection($this->input->post('jumlah', TRUE)),
				'ket' => $this->Anti_sql_injection($this->input->post('ket', TRUE))
			);

			//print_r($data);die;

			$result = $this->uom_model->add_uom($data);

			if ($result > 0) {
				$msg = 'Berhasil menambahkan uom.';

				$result = array(
					'success' => true,
					'message' => $msg
				);
			} else {
				$msg = 'Gagal menambahkan uom ke database.';
				$result = array(
					'success' => false,
					'message' => $msg
				);
			}
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function export() {


		//die;
		$this->load->library('excel');

		$objPHPExcel = new PHPExcel();

		$end_date = anti_sql_injection($this->input->post('date_filter'));
		$end_date = Carbon::parse($end_date)->isValid() ? $end_date : null;

		$params  = array(
			'start_time' => date('Y-m-d'),
			'end_time' => $end_date ?? date('Y-m-d'),
			'report' => "aktiva"
		);

		$datas['get_piutang'] = $this->uom_model->get_piutang($params);
		$datas['get_persediaan'] = $this->uom_model->get_persediaan($params);
		$datas['get_hutang'] = $this->uom_model->get_hutang($params);

		# FIRST SHEET
		$aktivas = $this->uom_model->get_report_keu($params);
		// echo '<pre>'; print_r($aktivas); die;

		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'Aktiva');

		$row = 3;
		$totalNilai = 0;
		$objPHPExcel->getActiveSheet()->setTitle('Neraca');
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A2', 'COA')
			->setCellValue('B2', 'Keterangan')
			->setCellValue('C2', 'Nilai');
		foreach ($aktivas as $aktiva) {

			if ($aktiva['coa'] == 11301) {

				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A' . $row, $aktiva['coa'] ?? '')
					->setCellValue('B' . $row, $aktiva['keterangan'] ?? '')
					->setCellValue('C' . $row, $datas['get_piutang'][0]['piutang'] ?? '');

				$row++;
				$totalNilai += $datas['get_piutang'][0]['piutang'];
			} elseif ($aktiva['coa'] == 11600) {

				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A' . $row, $aktiva['coa'] ?? '')
					->setCellValue('B' . $row, $aktiva['keterangan'] ?? '')
					->setCellValue('C' . $row, $datas['get_persediaan'][0]['persediaan'] ?? '');

				$row++;
				$totalNilai += $datas['get_persediaan'][0]['persediaan'];
			} else {

				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A' . $row, $aktiva['coa'] ?? '')
					->setCellValue('B' . $row, $aktiva['keterangan'] ?? '')
					->setCellValue('C' . $row, $aktiva['nilai'] ?? '');

				$row++;
				$totalNilai += $aktiva['nilai'];
			}
		}

		$objPHPExcel->setActiveSheetIndex(0)
			->mergeCells('A' . $row . ':' . 'B' . $row)
			->setCellValue('A' . $row, 'Total')
			->setCellValue('C' . $row, $totalNilai);

		# SECOND SHEET
		$params_passiva  = array(
			'start_time' => date('Y-m-d'),
			'end_time' => $end_date ?? date('Y-m-d'),
			'report' => "passiva"
		);
		$passivas = $this->uom_model->get_report_keu($params_passiva);

		//   $objPHPExcel->createSheet(1)->setTitle('Passiva');
		$objPHPExcel->setActiveSheetIndex(0);

		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('E1', 'Passiva');
		$row = 3;
		$totalNilai = 0;
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('E2', 'COA')
			->setCellValue('F2', 'Keterangan')
			->setCellValue('G2', 'Nilai');
		foreach ($passivas as $passiva) {

			if ($passiva == 20100) {

				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('E' . $row, $passiva['coa'] ?? '')
					->setCellValue('F' . $row, $passiva['keterangan'] ?? '')
					->setCellValue('G' . $row, $datas['get_hutang'][0]['hutang'] ?? '');

				$row++;
				$totalNilai += $datas['get_hutang'][0]['hutang'];
			} else {
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('E' . $row, $passiva['coa'] ?? '')
					->setCellValue('F' . $row, $passiva['keterangan'] ?? '')
					->setCellValue('G' . $row, $passiva['nilai'] ?? '');

				$row++;
				$totalNilai += $passiva['nilai'];
			}
		}

		$objPHPExcel->setActiveSheetIndex(0)
			->mergeCells('E' . $row . ':' . 'F' . $row)
			->setCellValue('E' . $row, 'Total')
			->setCellValue('G' . $row, $totalNilai);

		$objPHPExcel->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename=neraca.xls');
		header('Cache-Control: max-age=0');
		// If you’re serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you’re serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		unset($objPHPExcel);
	}
}
