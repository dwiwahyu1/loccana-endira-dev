<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengeluaran extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('pengeluaran/selling_model');
		$this->load->library('log_activity');
		
		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong');
		$this->form_validation->set_message('min_length', '%s Minimal 4 Karakter');
		$this->form_validation->set_message('max_length', '%s Maksimal 10 Karakter');
	}

	/**
	 * anti sql injection
	 * @return string
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	  * This function is redirect to index distributor page
	  * @return Void
	  */
	public function index() {
		$this->template->load('maintemplate', 'pengeluaran/views/index');
	}

	/**
	  * This function is used for showing distributor list
	  * @return Array
	  */
	function lists() {
		
		 if( !empty($_GET['sess_user_id']) ) {
			  $sess_user_id = $_GET['sess_user_id'];
		  } else {
			  $sess_user_id = NULL;
		  }
		  
		   if( !empty($_GET['sess_token']) ) {
			  $sess_token = $_GET['sess_token'];
		  } else {
			  $sess_token = NULL;
		  }
		  
		    if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		  if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		  if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 				
		  $order_fields = array('kode_eksternal'); // , 'COST'
		  $order = $this->input->get_post('order');
		  if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'desc';}; 
		  if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 	
		  
		    $params['limit'] 		= (int) $length;
			$params['offset'] 		= (int) $start;
			$params['order_column'] = $order_fields[$order_column];
			$params['order_dir'] 	= $order_dir;
			$params['sess_user_id'] = $sess_user_id;
			$params['sess_token'] 	= $sess_token;
			$params['searchtxt'] 	= $_GET['search']['value'];
		  
		 // print_r($params);die;
		  
			$list = $this->selling_model->list_penjualan($params); 
			//print_r($list['data']);die;
			  $data = array();	
				   foreach ( $list['data'] as $k => $v ) {

					if($v['status']==0){
						$sts = '<button type="button" class="btn btn-custon-rounded-two btn-danger" onClick="konfirmasi('. $v['id_penjualan'].')"  > Konfirmasi </button>';
						$action = '<button type="button" class="btn btn-custon-rounded-two btn-default" onClick="updatepo('. $v['id_penjualan'].')" > Edit </button>
						<button type="button" class="btn btn-custon-rounded-two btn-danger" onClick="deletepo('. $v['id_penjualan'].')" > Hapus </button>
						<button type="button" class="btn btn-custon-rounded-two btn-primary" onClick="detail('. $v['id_penjualan'].')" > Detail </button>
						<button type="button" class="btn btn-custon-rounded-two btn-primary" onClick="print_pdf('. $v['id_penjualan'].')" > Print </button>
						';
					}else{
						$sts = '<button type="button" class="btn btn-custon-rounded-two btn-primary" disabled > Disetujui </button>';
						$action = '<button type="button" class="btn btn-custon-rounded-two btn-primary" onClick="detail('. $v['id_penjualan'].')" > Detail </button>
						';
					}
					
					if($v['term_of_payment'] == '0'){
						$hhh = 'Cash';
					}else{
						$hhh =  $v['term_of_payment'].' Hari';
					}
					
					if( $v['status'] == 0){
						$stt = " Diterima ";
					}else{
						$stt = " Invoice ";
					}
						
						$date=date_create($v['date_penjualan']);
						$date_due = date_add($date,date_interval_create_from_date_string($v['term_of_payment']." days"));
						$due_date = date_format($date_due,"Y-m-d");;
						
						  
						  array_push($data, 
							  array(
								  number_format($v['Rangking'],0,',','.'),
								  $v['no_penjualan'],
								  $v['cust_name'],
								  $v['date_penjualan'],
								  $v['symbol_valas'].' '.number_format($v['total_amount'],0,',','.'),
								  $due_date,
								  $v['nama'],
								  $stt,
								  $action
							  )
							);
							//$idx++;
				   }
		   
			 $result["data"] = $data;
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		//$result["data"] = $list['data'];
	  
		echo json_encode($result);
	  
				//$this->json_result($result);
		  
	}

	/**
	  * This function is redirect to add distributor page
	  * @return Void
	  */
	  
	 public function get_customer(){ 
		 
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);
		
		$result['customer'] = $this->selling_model->get_customer_byid($params);
		
		//print_r($result['customer']);die;
		$item = $this->selling_model->get_item_byprin_all();
		
		//print_r($item);die;
		
		$list = '<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		foreach($item as $items){
			
			
			$list .= '<option value="'.$items['id_mat'].'|'.$items['unit_box'].'|'.$items['bottom_price'].'|'.$items['qty'].'" >'.$items['stock_code'].' - '.$items['stock_name'].' - '.$items['base_qty'].' '.$items['uom_symbol'].'</option>';
			
		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';
		
		$result['list'] = $list;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		 
	 }	

	 public function get_all_item(){
		 
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);
		
		$item = $this->selling_model->get_item_byprin($params);
		
		$list = '<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		foreach($item as $items){
			
			$list .= '<option value="'.$items['id_mat'].'" >'.$items['stock_code'].' - '.$items['stock_name'].' - '.$items['base_qty'].' '.$items['uom_symbol'].'</option>';
			
		}
		$list .= '<option value="0" selected="selected" disabled>-- Pilih Item Lainnya --</option>';
		
		$item2 = $this->selling_model->get_item_byprin_all($params);
		
		foreach($item2 as $items){
			
			$list .= '<option value="'.$items['id_mat'].'" >'.$items['stock_code'].' - '.$items['stock_name'].' - '.$items['base_qty'].' '.$items['uom_symbol'].'</option>';
			
		}
	//	$list .= '------------------';
		
		$result['list'] = $list;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		 
	 }	 
	 
	 
	 public function get_price_mat(){
		 
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);
		
		//print_r($params);die;
		
		$ddd = $this->selling_model->get_price_mat($params);
		//$item = $this->selling_model->get_item_byprin($params);
		
		if(count($ddd) == 0){
			$list = 0;
		}else{
			$list = number_format($ddd[0]['unit_price'],0,',','.');
		}
		//print_r($ddd);die;
		
		$result['list'] = $list;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		 
	 }
	 
	public function add() {
		
		$params['region'] = 4;
		
		$result = $this->selling_model->get_customer($params);
		
		$result_g = $this->selling_model->get_kode();
		if($result_g[0]['max']+1 < 1000){
			if($result_g[0]['max']+1 < 100){
				if($result_g[0]['max']+1 < 10){
					$kode = ''.date('Y').'000'.($result_g[0]['max']+1);
				}else{
					$kode = ''.date('Y').'00'.($result_g[0]['max']+1);
				}
			}else{
				$kode = ''.date('Y').'0'.($result_g[0]['max']+1);
			}
		}else{
			$kode = ''.date('Y').''.($result_g[0]['max']+1);
		}
		//print_r($kode);die;

		$data = array(
			'principal' => $result,
			'kode' => $kode,
			'item' => $result_g
		);

		$this->template->load('maintemplate', 'pengeluaran/views/addPo',$data);
		//$this->load->view('addPrinciple',$data);
	}	


	public function updatepo() {
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			
		);
		
		$params['region'] = 4;
		
		$result = $this->selling_model->get_customer($params);
		
		$po_result = $this->selling_model->get_penjualan($data);
		$po_detail_result = $this->selling_model->get_penjualan_detail($data);
		
		
		$params = array(
			'id_cust' => $po_result[0]['id_customer'],
		);
		
		$item = $this->selling_model->get_item_byprin_all();
		//print_r($po_detail_result);die;
		
		// $result = $this->distributor_model->location();

		$data = array(
			'po' => $po_result[0],
			'principal' => $result,
			'po_mat' => $po_detail_result,
			'item' => $item
		);

		$this->template->load('maintemplate', 'pengeluaran/views/updatePo',$data);
		//$this->load->view('addPrinciple',$data);
	}		
	
	public function detail() {
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			
		);
		
		$params['region'] = 4;
		
		$result = $this->selling_model->get_customer($params);
		
		$po_result = $this->selling_model->get_penjualan($data);
		$po_detail_result = $this->selling_model->get_penjualan_detail($data);
		
		
		$params = array(
			'id_cust' => $po_result[0]['id_customer'],
		);
		
		$item = $this->selling_model->get_item_byprin_all();
		//print_r($principal_result);die;
		
		// $result = $this->distributor_model->location();

		$data = array(
			'po' => $po_result[0],
			'principal' => $result,
			'po_mat' => $po_detail_result,
			'item' => $item
		);

		$this->template->load('maintemplate', 'pengeluaran/views/detailPo',$data);
		//$this->load->view('addPrinciple',$data);
	}	
	
	public function konfirmasi() {
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			
		);
		
		//print_r($data);
		
		$po_result = $this->selling_model->get_po($data);
		$po_detail_result = $this->selling_model->get_po_detail($data);
		$result = $this->selling_model->get_principal();
		
		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);
		
		$item = $this->selling_model->get_item_byprin($params);
		//print_r($principal_result);die;
		
		// $result = $this->distributor_model->location();

		$data = array(
			'po' => $po_result[0],
			'principal' => $result,
			'po_mat' => $po_detail_result,
			'item' => $item
		);

		$this->template->load('maintemplate', 'pengeluaran/views/approvallPo',$data);
		//$this->load->view('addPrinciple',$data);
	}		
	
	public function approve_po(){
		
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);
		
		$data = array(
			'id_po' => $params['id'],
			'sts' => $params['sts']
			
		);
		
		//print_r($data);die;
		
		$add_prin_result = $this->selling_model->edit_po_apr($data);
		
	}
	
	public function add_po(){
		
		// $this->form_validation->set_rules('kode', 'Kode Principal', 'required|min_length[4]|max_length[10]');
		// $this->form_validation->set_rules('alamat', 'Alamat Principal', 'required');
		$this->form_validation->set_rules('tgl', 'Tanggal Tidak Boleh Kosong', 'required');
		
		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			
			//echo $pesan;die;
			
			//$msg = strip_tags(str_replace("\r", '', $pesan));
			$msg = $pesan;

			$result = array('success' => false, 'message' => $msg);
		}else {
			
			$int_val = $this->Anti_sql_injection($this->input->post('int_flo', TRUE));
			
			$array_items = [];
			
			$total_amount = 0;
			for($i=0;$i<=$int_val;$i++){
				
				
					$kode_split = explode('|',$this->Anti_sql_injection($this->input->post('kode_'.$i, TRUE)));
				if($kode_split[0]){
					$array_items[$i]['kode'] = $kode_split[0];
					$array_items[$i]['qty_stock'] = $kode_split[3];
					//$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE));
					$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE));
					$array_items[$i]['qty'] = $this->Anti_sql_injection($this->input->post('qty_'.$i, TRUE));
					$array_items[$i]['qtys'] = $this->Anti_sql_injection($this->input->post('qtys_'.$i, TRUE));
					$array_items[$i]['qty_box'] = $kode_split[1];
					$array_items[$i]['total'] = ( (intval(str_replace('.','',$array_items[$i]['qty'])) + intval(str_replace('.','',$array_items[$i]['qtys'])) ) * intval($kode_split[1]) ) * floatval(str_replace(',','.',str_replace('.','',$this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE))))) ;
				//	$array_items[$i]['remark'] = $this->Anti_sql_injection($this->input->post('remark_'.$i, TRUE));
					$array_items[$i]['totalqty'] = $this->Anti_sql_injection($this->input->post('totalqty_'.$i, TRUE));
					$total_amount = $total_amount + ( ((intval(str_replace('.','',$array_items[$i]['qty'])) + intval(str_replace('.','',$array_items[$i]['qtys']))) * intval($kode_split[1]) ) * floatval(str_replace(',','.',str_replace('.','',$this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE)))))  );
				
				}
				
				
			}
			
			//print_r($array_items);die;
			
			//$total_amounts = $total_amount + ($total_amount * (floatval($this->Anti_sql_injection($this->input->post('ppn', TRUE)))/100));
			
			$result_g = $this->selling_model->get_kode();
			$new_seq = $result_g[0]['max']+1;
			
			if( $this->Anti_sql_injection($this->input->post('term', TRUE)) == 'other' ){
				$terms = $this->Anti_sql_injection($this->input->post('term_o', TRUE));
			}else{
				$terms = $this->Anti_sql_injection($this->input->post('term', TRUE));
			}
			
			
			$data = array(
				'no_penjualan' => $this->Anti_sql_injection($this->input->post('kode', TRUE)),
				'id' => $this->Anti_sql_injection($this->input->post('kode', TRUE)),
				'date_penjualan' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
				'id_sales' => $this->session->userdata['logged_in']['user_id'],
				'id_customer' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
				'term_of_payment' => $terms,
				'keterangan' => $this->Anti_sql_injection($this->input->post('ket', TRUE)),
				'sisa' => floatval(str_replace(',','.',str_replace('.','',$this->Anti_sql_injection($this->input->post('sisa', TRUE))))),
				'total_amount' => $total_amount,
				'seq_n' => $new_seq
			);
			
			//print_r($data);die;
		
				
				$add_prin_result = $this->selling_model->add_penjualan($data);
				$this->selling_model->edit_credit($data);
				
				foreach($array_items as $array_itemss){
					
					if($array_itemss['kode'] == ""){
						
					}else{
					
						$datas = array(
						
							'id_penjualan' => $add_prin_result['lastid'],
							'id_sales' =>$this->session->userdata['logged_in']['user_id'],
							'id_material' => $array_itemss['kode'],
							'id_customer' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
							'unit_price' => str_replace(',','.',str_replace('.','',$array_itemss['harga'])),
							'price' => intval(str_replace('.','',$array_itemss['harga']))* ( (intval(str_replace('.','',$array_itemss['qty'])) + intval(str_replace('.','',$array_itemss['qtys'])) ) * intval($array_itemss['qty_box'])  ),
							'qty' => (intval(str_replace('.','',$array_itemss['qty'])) + intval(str_replace('.','',$array_itemss['qtys'])) ) * intval($array_itemss['qty_box']), 
							'qty_stock' => intval(str_replace('.','',$array_itemss['qty_stock'])) - ((intval(str_replace('.','',$array_itemss['qty'])) + intval(str_replace('.','',$array_itemss['qtys'])) ) * intval($array_itemss['qty_box'])) ,
							'box_ammount' => intval($array_itemss['qty_box']),
							'qty_box' => intval(str_replace('.','',$array_itemss['qty'])),
							'qty_satuan' => intval(str_replace('.','',$array_itemss['qtys']))
							
							//'remark' => $array_itemss['remark'],
						
						);
						
						$id_mutasi = $this->selling_model->add_mutasi($datas);
						$this->selling_model->edit_qty($datas);
						
						
						
						$prin_result = $this->selling_model->add_detail_penjualan($datas,$id_mutasi);
					
					}
					
				}
				
				
				
				//print_r($add_prin_result);die;
				
				if ($prin_result['result'] > 0) {
					
					
					
					
					
					$msg = 'Berhasil Menambah data Purchase Order';

					$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ');
					$result = array('success' => true, 'message' => $msg);
					
				}else{
					$msg = 'Gagal Menambah data Purchase Order';

					$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ');
					$result = array('success' => false, 'message' => $msg);
					
				}
		
			
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);
		
		//echo $kode;die;
		
	}	
	
	public function edit_po(){
		
		// $this->form_validation->set_rules('kode', 'Kode Principal', 'required|min_length[4]|max_length[10]');
		// $this->form_validation->set_rules('alamat', 'Alamat Principal', 'required');
		$this->form_validation->set_rules('tgl', 'Nama Principal', 'required');
		
		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			
			//echo $pesan;die;
			
			//$msg = strip_tags(str_replace("\r", '', $pesan));
			$msg = $pesan;

			$result = array('success' => false, 'message' => $msg);
		}else {
			
			$int_val = $this->Anti_sql_injection($this->input->post('int_flo', TRUE));
			
			$array_items = [];
			
			$total_amount = 0;
			for($i=0;$i<=$int_val;$i++){
				
				$kode_split = explode('|',$this->Anti_sql_injection($this->input->post('kode_'.$i, TRUE)));
				if($kode_split[0]){
					$array_items[$i]['kode'] = $kode_split[0];
					$array_items[$i]['qty_stock'] = $kode_split[3];
					//$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE));
					$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE));
					$array_items[$i]['qty'] = $this->Anti_sql_injection($this->input->post('qty_'.$i, TRUE));
					$array_items[$i]['qtys'] = $this->Anti_sql_injection($this->input->post('qtys_'.$i, TRUE));
					$array_items[$i]['qty_box'] = $kode_split[1];
					$array_items[$i]['total'] = ( (intval(str_replace('.','',$array_items[$i]['qty'])) + intval(str_replace('.','',$array_items[$i]['qtys'])) ) * intval($kode_split[1]) ) * floatval(str_replace(',','.',str_replace('.','',$this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE))))) ;
				//	$array_items[$i]['remark'] = $this->Anti_sql_injection($this->input->post('remark_'.$i, TRUE));
					$array_items[$i]['totalqty'] = $this->Anti_sql_injection($this->input->post('totalqty_'.$i, TRUE));
					$total_amount = $total_amount + ( ((intval(str_replace('.','',$array_items[$i]['qty'])) + intval(str_replace('.','',$array_items[$i]['qtys']))) * intval($kode_split[1]) ) * floatval(str_replace(',','.',str_replace('.','',$this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE)))))  );
				
				}
			}
			
			$total_amounts = $total_amount + ($total_amount * (floatval($this->Anti_sql_injection($this->input->post('ppn', TRUE)))/100));
			
			$result_g = $this->selling_model->get_kode();
			$new_seq = $result_g[0]['max']+1;
			
			if( $this->Anti_sql_injection($this->input->post('term', TRUE)) == 'other' ){
				$terms = $this->Anti_sql_injection($this->input->post('term_o', TRUE));
			}else{
				$terms = $this->Anti_sql_injection($this->input->post('term', TRUE));
			}
					
			$data = array(
				'id' => $this->Anti_sql_injection($this->input->post('id', TRUE)),
				'no_penjualan' => $this->Anti_sql_injection($this->input->post('kode', TRUE)),
				'date_penjualan' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
				'id_sales' => $this->session->userdata['logged_in']['user_id'],
				'id_customer' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
				'term_of_payment' => $terms,
				'keterangan' => $this->Anti_sql_injection($this->input->post('ket', TRUE)),
				'sisa' => floatval(str_replace(',','.',str_replace('.','',$this->Anti_sql_injection($this->input->post('sisa', TRUE))))),
				'total_amount' => $total_amount,
				'seq_n' => $new_seq
			);
		
			$selling_data = $this->selling_model->get_penjualan($data);
			// $sisa = $selling_data[0]['sisa'] + $selling_data[0]['total_amount'];
			// $sisa_new = $sisa - $total_amount;
		
		
				$add_prin_result = $this->selling_model->edit_penjualan($data);
				
				$add_prin_result = $this->selling_model->edit_credit2($data,$selling_data[0]);
				
				
				$selling_data_detail = $this->selling_model->get_penjualan_detail($data);
				
					foreach($selling_data_detail as $selling_data_detailss){
							
							$data_q = array(
								'id_mat' => $selling_data_detailss['id_material'],
								'qty' => floatval($selling_data_detailss['qty']),
								'mutasi_id' => $selling_data_detailss['mutasi_id']
							);
							
							
							$this->selling_model->update_qty_m($data_q);
							$this->selling_model->delete_mutasi($data_q);
							
						
					}
				
				$this->selling_model->delete_detail_penjualan($this->Anti_sql_injection($this->input->post('id', TRUE)));
				
				$add_prin_result = $this->selling_model->edit_penjualan($data);
				$this->selling_model->edit_credit($data);
				
				foreach($array_items as $array_itemss){
					
					if($array_itemss['kode'] == ""){
						
					}else{
					
						$datas = array(
							'id' => $this->Anti_sql_injection($this->input->post('id', TRUE)),
							'id_penjualan' => $this->Anti_sql_injection($this->input->post('id', TRUE)),
							'id_sales' =>$this->session->userdata['logged_in']['user_id'],
							'id_material' => $array_itemss['kode'],
							'id_customer' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
							'unit_price' => str_replace(',','.',str_replace('.','',$array_itemss['harga'])),
							'price' => intval(str_replace('.','',$array_itemss['harga']))* ( (intval(str_replace('.','',$array_itemss['qty'])) + intval(str_replace('.','',$array_itemss['qtys'])) ) * intval($array_itemss['qty_box'])  ),
							'qty' => (intval(str_replace('.','',$array_itemss['qty'])) + intval(str_replace('.','',$array_itemss['qtys'])) ) * intval($array_itemss['qty_box']), 
							'qty_stock' => intval(str_replace('.','',$array_itemss['qty_stock'])) - ((intval(str_replace('.','',$array_itemss['qty'])) + intval(str_replace('.','',$array_itemss['qtys'])) ) * intval($array_itemss['qty_box'])) ,
							'box_ammount' => intval($array_itemss['qty_box']),
							'qty_box' => intval(str_replace('.','',$array_itemss['qty'])),
							'qty_satuan' => intval(str_replace('.','',$array_itemss['qtys']))
							
						
						);
						$id_mutasi = $this->selling_model->add_mutasi($datas);
						$this->selling_model->edit_qty($datas);
						
						$prin_result = $this->selling_model->add_detail_penjualan($datas,$id_mutasi);
						//$this->selling_model->update_qty_m($data_q);
					
					}
					
				}
				
				
				
				//print_r($add_prin_result);die;
				
				if ($add_prin_result['result'] > 0) {
					
					
					
					
					
					$msg = 'Berhasil Merubah data Purchase Order';

					$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ');
					$result = array('success' => true, 'message' => $msg);
					
				}else{
					$msg = 'Gagal Menambah data Purchase Order';

					$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ');
					$result = array('success' => false, 'message' => $msg);
					
				}
		
			
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);
		
		//echo $kode;die;
		
	}

	public function delete_po() {
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);


		//print_r($params);die;
		$selling_data = $this->selling_model->get_penjualan($params);
		
		$data2 = array(
			'id_customer' => $selling_data[0]['id_t_cust'],
			'id' => $params['id']
		);
		
		$add_prin_result = $this->selling_model->edit_credit2($data2,$selling_data[0]);
		
		
		$selling_data_detail = $this->selling_model->get_penjualan_detail($data2);
		//print_r($selling_data_detail);die;
		
		foreach($selling_data_detail as $selling_data_detailss){
							
				$data_q = array(
					'id_mat' => $selling_data_detailss['id_material'],
					'qty' => floatval($selling_data_detailss['qty']),
					'mutasi_id' => $selling_data_detailss['mutasi_id']
				);
							
							
				$this->selling_model->update_qty_m($data_q);
				$this->selling_model->delete_mutasi($data_q);
							
						
		}
		
		$this->selling_model->delete_detail_penjualan($params['id']);
		$this->selling_model->delete_penjualan($params['id']);


		$result_dist 		= $this->selling_model->delete_po($params['id']);
		//$result_dist2 		= $this->selling_model->delete_po_mat($params['id']);

		$msg = 'Berhasil menghapus data Purchase Order.';
		$result = array('success' => true, 'message' => $msg);
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function print_pdf(){
		
		$data   	= file_get_contents("php://input");
		//$params     = json_decode($data,true);
		
		
		//echo "aaa";
		$params = (explode('=',$data));
		
		$data = array(
			'id' => $params[1],
			
		);
		
		//print_r($data);die;
		
		$po_result = $this->selling_model->get_po($data);
		$po_detail_result = $this->selling_model->get_po_detail_full($data);
		$result = $this->selling_model->get_principal();
		
		
		
		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);
		
		$array_items = [];
			
		$total_sub = 0;
		$total_sub_all = 0;
		$total_disk = 0;
		$grand_total = 0;
		
		$htmls = '';
		foreach($po_detail_result as $po_detail_results){
		
			$amn = number_format(($po_detail_results['price']-($po_detail_results['price']*($po_detail_results['diskon']/100))),0,',',''); 
			$htmls = $htmls.'
			<tr style="border-top: 1px solid black;" >
								<td  align="left" style="border-top: 1px solid black;"><p>'.$po_detail_results['stock_code'].'</p></td>
								<td  align="left" style="border-top: 1px solid black;"><p>'.$po_detail_results['stock_name'].'</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>'.number_format($po_detail_results['base_qty'],'2',',','.').' '.$po_detail_results['uom_symbol'].'</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>'.number_format($po_detail_results['qty_order'],'2',',','.').'</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>'.number_format($po_detail_results['unit_price'],'0',',','.').'</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>'.number_format($po_detail_results['diskon'],'2',',','.').'</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>'.number_format($amn,'0',',','.').'</p></td>

							</tr>
			';
			
			//$total_sub = $total_sub + number_format(($po_detail_results['price']-($po_detail_results['price']*($po_detail_results['diskon']/100))),0,',',''); 
			$total_sub = $total_sub + number_format(($po_detail_results['price']-($po_detail_results['price']*($po_detail_results['diskon']/100))),0,',',''); 
			$total_sub_all = $total_sub_all + number_format(($po_detail_results['price']),0,',',''); 
			$total_disk = $total_disk + ($po_detail_results['price']*($po_detail_results['diskon']/100)); 
			//$grand_total = $grand_total +  number_format(($po_detail_results['price']),0,',',''); 

		}
		
		$total_ppn = $total_sub * (floatval($po_result[0]['ppn']))/100;
		$grand_total = $total_ppn + $total_sub;
		
		//print_r($htmls);die;
		
		$item = $this->selling_model->get_item_byprin($params);
		
		$rtext = '<br><p style="font-size: 10px;">PT. ENDRIA ALDA</p> ';
		
		$ctext = '<br><p style="font-size: 12px"> PURCHASE ORDER</p> ';
		
		$this->load->library('Pdf');
		
		$pdf = new Pdf('P', 'mm', 'A4', false, 'UTF-8', false);
		$pdf->SetTitle('Purchase Order');
		$pdf->SetHeaderMargin(25);
		$pdf->setPrintHeader(false);
		$pdf->SetTopMargin(6);
		$pdf->setFooterMargin(5);
		$pdf->SetAutoPageBreak(true);
		$pdf->SetAuthor('Author');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->SetPrintFooter(false);
		
		$pdf->AddPage();


		$html = '
		
		<style>
		
		  .floatedTable {
            float:left;
        }
        .inlineTable {
            display: inline-block;
        }
		p {
							font-size: 8px;
							
						}
		p {
							font-size: 8px;
							
						}
		</style>
  <table style=" float:left;border: none;" >
						<tr style="border: none;">
							<th  align="left" ><h5>PT.ENDIRA ALDA</h5></th>
							<th  align="RIGHT"></th>
						</tr>
						<tr style="border: none;">
							<th  align="left" ><h4>PURCHASE ORDER</h4></th>
							<th  align="RIGHT"><p>NOMOR, TGL : '.$po_result[0]['no_po'].'   '.$po_result[0]['date_po'].'</p></th>
						</tr>
					  </table><br><br><br>
					  
					<table>
					  <tr>
						<td>

						  <table cellspacing="0" cellpadding="0" border="0" style="float:right;width:100%.">
							<tr>
							  <td style="width:18%;"><p>Kepada</p></td>
							  <td><p>: '.$po_result[0]['name_eksternal'].'</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Alamat</p></td>
							  <td><p>: '.$po_result[0]['eksternal_address'].'</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Att</p></td>
							  <td><p>: '.$po_result[0]['pic'].'</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Telp</p></td>
							  <td><p>: '.$po_result[0]['phone_1'].'</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Fax</p></td>
							  <td><p>: '.$po_result[0]['fax'].'</p></td>
							</tr>

						  </table>
						</td>
						<td>
						  <table cellspacing="0" cellpadding="0" border="0" style="float:right;width:100%">
							<tr>
							  <td style="width:18%;"><p>Ship To</p></td>
							  <td><p>:JL.SANGKURIANG NO 38-A<BR>NPWP:01.555.161.7.428.000</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Email</p></td>
							  <td><p>: </p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Telp/Fax</p></td>
							  <td><p>: (022)6626-946</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Delivery</p></td>
							  <td><p>: </p></td>
							</tr>
						

						  </table>
						</td>
					  </tr>
					</table>
					<br><br><br>

					
					  <table style=" float:left;border-top: 1px solid black;border-bottom: 1px solid black;" >
						<tr style=" border-top: 1px solid black;font-size: 10px;">
							<th  align="left" >Code</th>
							<th  align="RIGHT">Description</th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Quantity<br>Lt/Kg</th>
							<th  align="RIGHT">Unit Price</th>
							<th  align="RIGHT">Disc</th>
							<th  align="RIGHT">Amount</th>
						</tr>
						'.$htmls.'
					  </table>
					  	<br><br><br>
						<br><br><br>
					  
					    <table style=" float:left;border-top: 1px solid black;" >
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Sub Total :</th>
							<th  align="RIGHT">'.number_format($total_sub_all,0,',','.').'</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Discount :</th>
							<th  align="RIGHT">'.number_format($total_disk,0,',','.').'</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"> Taxable :</th>
							<th  align="RIGHT">'.number_format($total_sub,0,',','.').'</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Vat/PPN :</th>
							<th  align="RIGHT">'.number_format($total_ppn,0,',','.').'</th>
						</tr>
					  </table>
					  
					  <table style=" float:left;border-top: 1px solid black;" >
						<tr style=" border-top: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Total :</th>
							<th  align="RIGHT">'.number_format($grand_total,0,',','.').'</th>
						</tr>
					  </table>
					<br><br><br><br><br> 
					<br><br><br><br><br> 
					
					 <table style="font-size: 10px;" >
						<tr style="">
							<th  align="CENTER" style="width:25%" ></th>
							<th  align="CENTER" style="width:12%" ></th>
							<th  align="CENTER" style="width:25%"></th>
							<th  align="CENTER" style="width:13%" ></th>
							<th  align="CENTER" style="width:25%">Cimahi, '.date('m/d/Y').'</th>
						</tr>
						<tr style="">
							<th  align="CENTER" style="width:25%" >Disetujui Oleh,</th>
							<th  align="CENTER" style="width:12%" ></th>
							<th  align="CENTER" style="width:25%">Diperiksa Oleh,</th>
							<th  align="CENTER" style="width:13%" ></th>
							<th  align="CENTER" style="width:25%">Dipesan Oleh,</th>
						</tr>
					  </table>
					  <br><br><br><br><br> 
					  <table style="font-size: 10px;" >
						<tr style="">
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Ratna S. Iskandar Dinata</span></th>
							<th  align="CENTER" style="width:12%"></th>
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Rienaldy Aryanto</span></th>
							<th  align="CENTER" style="width:13%"></th>
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Rangga Dean</span></th>
						</tr>
					  </table>
';

//echo $html;die;

		// $html = <<<EOD
// <h5>PT.ENDIRA ALDA</h5>
  // <table style=" float:left;border: none;" >
						// <tr style="border: none;">
							// <th  align="left" ><h4>PURCHASE ORDER</h4></th>
							// <th  align="RIGHT">NOMOR, TGL : ".."</th>
						// </tr>
					  // </table>

// EOD;

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
		
		
		
		$pdf->SetPrintFooter(false);
		
		$pdf->lastPage();
		
		// $pdf->writeHTMLCell(85, 5, '<p>'.$kategoriby.' : '.$kategori.'<br>Periode : '.$start_date.' / '.$end_date.'</p> ', 1, 'L', 0, 0, '', '', true);
		// $pdf->writeHTMLCell(85, 5, $img, 1, 'R', 0, 1, '', '', true);
		// $pdf->writeHTMLCell(180, 5, '[CENTER] ', 1, 'C', 0, 1, '', '', true);
		// $pdf->writeHTMLCell(85, 5, '[LEFT] ', 1, 'L', 0, 0, '', '', true);
		// $pdf->writeHTMLCell(85, 5, '[RIGHT] ', 1, 'L', 0, 1, '', '', true);

		// $pdf->Output('/var/www/html/tmp_doc/Report_Postbuy.pdf', 'F');

		// $this->output->set_content_type('application/json')->set_output(json_encode('aaa'));
		
		$pdf->Output('Purchase_order.pdf', 'I');

		
		// if ( $list ) {			
			  // $this->output->set_content_type('application/json')->set_output(json_encode($list));
		// } else {
			  // $result = array( 'Value not found!' );
			  // $this->output->set_content_type('application/json')->set_output(json_encode($list));
		// }
		
	}
	
}