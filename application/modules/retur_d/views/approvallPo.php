<style>
	.dt-body-left {text-align:left;}
	.dt-body-right {text-align:right;}
	.dt-body-center {text-align:center; vertical-align: middle;}
	.force-overflow {height: 650px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow {min-height: 650px}
	#modal-distributor::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar
	{
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar-thumb
	{
		background-image: -webkit-gradient(linear,
		   left bottom,
		   left top,
		   color-stop(0.44, rgb(122,153,217)),
		   color-stop(0.72, rgb(73,125,189)),
		   color-stop(0.86, rgb(28,58,148)));
	}

</style>

 <div class="product-sales-area mg-tb-30">
            <div class="container-fluid">
                        <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            <ul id="myTabedu1" class="tab-review-design">
                                <li class="active"><a href="#description">Approve Purchase Order</a></li>
                            </ul>
                            <div id="myTabContent" class="tab-content custom-product-edit">
                                <div class="product-tab-list tab-pane fade active in" id="description">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="review-content-section">
                                                <form id="edit_po" action="<?php echo base_url().'purchase_order/edit_po';?>" class="add-department">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<div class="form-group">
																<label>Kode</label>
                                                                <input name="kode" id="kode" type="text" class="form-control" placeholder="Kode Purchase Order" value="<?php echo $po['no_po']; ?>" readOnly>
                                                            </div>
															<div class="form-group date">
																<label>Tanggal</label>
																<input name="tgl" id="tgl" type="text" class="form-control" placeholder="Tanggal Purchase Order" value="<?php echo $po['date_po']; ?>" readOnly >
                                                            </div>
                                                            <div class="form-group">
																<label>Principal</label>
																<input name="ints" id="ints" type="hidden" class="form-control" placeholder="Tanggal Purchase Order" value= '<?php echo count($po_mat); ?>' >
																<input name="ints2" id="ints2" type="hidden" class="form-control" placeholder="Tanggal Purchase Order" value= '<?php echo count($po_mat); ?>' >
                                                                <select name="name" id="name" onChange="select_prin()" class="form-control" placeholder="Nama Principal" disabled>
																	<option value="0" selected="selected" disabled>-- Pilih Principal --</option>
																	<?php
																		foreach($principal as $principals){
																			
																			if($principals['id'] == $po['id_distributor']){
																				echo '<option value="'.$principals['id'].'" selected="selected" >'.$principals['name_eksternal'].'</option>';
																			}else{
																				echo '<option value="'.$principals['id'].'" >'.$principals['name_eksternal'].'</option>';
																			}
																			
																		}
																	?>
																</select>
                                                            </div>
                                                            <div class="form-group">
																<label>Alamat</label>
                                                                <textarea name="alamat" id="alamat"  type="text" class="form-control" placeholder="Alamat Principal" readOnly><?php echo $po['eksternal_address']; ?></textarea>
                                                            </div>
                                                            <div class="form-group">
																<label>Att</label>
                                                                <input name="att" id="att"  type="text" class="form-control" placeholder="Att" value="<?php echo ''; ?>" readOnly>
                                                            </div><div class="form-group">
																<label>No Telp</label>
                                                                <input name="telp" id="telp"  type="text" class="form-control" placeholder="Telephone" value="<?php echo $po['phone_1']; ?>" readOnly>
                                                            </div>
															 <div class="form-group">
																<label>Fax</label>
                                                                <input name="fax" id="fax"  type="text" class="form-control" placeholder="Fax" value="<?php echo $po['fax']; ?>" readOnly>
                                                            </div>
                                                        </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<div class="form-group">
																<label>Ship To:</label>
                                                                 <textarea name="alamat_kirim" id="alamat_kirim"  type="text" class="form-control" placeholder="Alamat Principal" readOnly >JL. Sangkuriang NO.38-A 
NPWP: 01.555.161.7.428.000</textarea>
                                                            </div>
															<div class="form-group date">
																<label>Email</label>
																<input name="email" id="email" type="email" class="form-control" placeholder="Email" readOnly >
                                                            </div>
															<div class="form-group date">
																<label>Telp/Fax</label>
																<input name="telpfax" id="telpfax" type="text" class="form-control" placeholder="Telp/Fax" value="(022) 6626-946" readOnly >
                                                            </div>
															<div class="form-group date">
																<label>VAT/PPN</label>
																<input name="ppn" id="ppn" type="text" class="form-control" placeholder="VAT/PPN" value="<?php echo $po['ppn']; ?>" readOnly >
                                                            </div>
															<div class="form-group date">
																<label>Term Pembayaran</label>
																  <select name="term" id="term" class="form-control" placeholder="Nama Principal" disabled>
																	<option value="h" selected="selected" disabled>-- Pilih Term --</option>
																	<option value="0" <?php if($po['term_of_payment'] == 0){ echo "selected='selected'"; } ?> >Cash</option>
																	<option value="15" <?php if($po['term_of_payment'] == 15){ echo "selected='selected'"; } ?> >15 Hari</option>
																	<option value="30" <?php if($po['term_of_payment'] == 30){ echo "selected='selected'"; } ?> >30 Hari</option>
																	<option value="45" <?php if($po['term_of_payment'] == 45){ echo "selected='selected'"; } ?>  >45 Hari</option>
																	<option value="60" <?php if($po['term_of_payment'] == 60){ echo "selected='selected'"; } ?> >60 Hari</option>
																	<option value="90" <?php if($po['term_of_payment'] == 90){ echo "selected='selected'"; } ?> >90 Hari</option>
																	<option value="other" <?php $people = array(0,15,30,45,60,90);
																	if (!in_array($po['term_of_payment'], $people)){ echo "selected='selected'"; } ?> >Lainnya</option>
														
																</select>
                                                            </div>
															
															<div class="form-group date">
																<label>Term Pembayaran Lain</label> 
																<input name="term_o" id="term_o" type="number" class="form-control" placeholder="Term (Hari)" 
																<?php $people = array(0,15,30,45,60,90);
																	if (in_array($po['term_of_payment'], $people)){ echo 'value="0" style="" readonly >'; }else{ echo 'value="'.$po['term_of_payment'].'" style="" readonly >'; } 
																?> 
																
                                                            </div>
                                                    </div>
													</div>

                                                    
                                               
                                            </div>
                                        </div>
                                    </div>
									<br><br>
									<div class="row">
										<h3>Items</h3>
									</div>
									
										<div class="row">
											<div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Kode</label>
												</div>
											</div>
											<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Qty Lt/Kg</label>
												</div>
											</div>
											<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Harga</label>
												</div>
											</div>
											<div class="col-lg-1 col-md-2 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Diskon</label>
												</div>
											</div>
											<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Total</label>
												</div>
											</div>
											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
												<div class="form-group">
													<label></label>
												</div>
											</div>
										</div>
											<input name="int_flo" id="int_flo" type="hidden" class="form-control" placeholder="Qty" value=<?php echo count($po_mat); ?>; >
									<div id="table_items">
									
										<?php $total_sub = 0;$total_sub_all = 0;$total_disk = 0; $int_row = 0; foreach($po_mat as $po_mats){ ?>
									
										<div id='row_<?php echo $int_row; ?>' >
											<div class="row" style="border-top-style:solid;">
											<div style="margin-top:10px">
												<div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
													<div class="form-group">
														<select name="kode_<?php echo $int_row; ?>" id="kode_<?php echo $int_row; ?>" class="form-control" placeholder="Nama Principal" disabled>
															<option value="0" selected="selected" disabled>-- Pilih Item --</option>
															<?php foreach($item as $items){ if($po_mats['id_material'] == $items['id_mat']){ $selc = "selected='selected'"; }else{ $selc = ''; } echo '<option value="'.$items['id_mat'].'" '.$selc.' >'.$items['stock_code'].' - '.$items['stock_name'].' - '.$items['base_qty'].' '.$items['uom_symbol'].'</option>'; } ?>
														</select>
													</div>
												</div>
												<div class="col-lg-2 col-md-1 col-sm-3 col-xs-12">
													<div class="form-group">
														<input readOnly name="qty_<?php echo $int_row; ?>" id="qty_<?php echo $int_row; ?>" type="text" onKeyup="change_sum(<?php echo $int_row; ?>)" class="form-control" placeholder="Qty" value=<?php echo number_format($po_mats['qty'],0,',','.'); ?>  >
													</div>
												</div>
												<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
													<div class="form-group">
														<input readOnly name="harga_<?php echo $int_row; ?>" id="harga_<?php echo $int_row; ?>" type="text" onKeyup="change_sum(<?php echo $int_row; ?>)" class="form-control" placeholder="Harga" value=<?php echo number_format($po_mats['unit_price'],0,',','.'); ?> >
													</div>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<div class="form-group">
														<input readOnly name="diskon_<?php echo $int_row; ?>" id="diskon_<?php echo $int_row; ?>" type="text" onKeyup="change_sum(<?php echo $int_row; ?>)" class="form-control" placeholder="Diskon" value=<?php echo number_format($po_mats['diskon'],0,',',''); ?>  >
													</div>
												</div>
												<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
													<div class="form-group">
														<input readOnly name="total_<?php echo $int_row; ?>" id="total_<?php echo $int_row; ?>" type="text" class="form-control" placeholder="Total" value=<?php echo number_format(($po_mats['price']-($po_mats['price']*($po_mats['diskon']/100))),0,',','.'); ?> readOnly  >
													</div>
												</div>
												
												
											</div>
											</div>
										</div>
										
										<?php $total_sub = $total_sub + number_format(($po_mats['price']-($po_mats['price']*($po_mats['diskon']/100))),0,',',''); ?>
										<?php $total_sub_all = $total_sub_all + number_format(($po_mats['price']),0,',',''); ?>
										<?php $total_disk = $total_disk + ($po_mats['price']*($po_mats['diskon']/100)); ?>
										<?php $int_row++; } ?>
										
									</div>
									
									<br>
									<div class="row" style="border-top-style:solid;">
											<div style="margin-top:10px">
												<div class="col-lg-8 col-md-3 col-sm-3 col-xs-12">
													
												</div>
												<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
													<label>Sub Total</label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<label id='subttl' style="float:right"><?php echo number_format($total_sub_all,0,',','.'); ?></label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
												</div>
											</div>
									</div>
									<div class="row" style="">
											<div style="margin-top:10px">
												<div class="col-lg-8 col-md-3 col-sm-3 col-xs-12">
													
												</div>
												<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
													<label>Diskon</label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<label id='subttl3' style="float:right"><?php echo number_format($total_disk,0,',','.') ?></label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
												</div>
											</div>
									</div>
									<div class="row" style="">
											<div style="margin-top:10px">
												<div class="col-lg-8 col-md-3 col-sm-3 col-xs-12">
													
												</div>
												<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
													<label>Taxable</label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<label id='subttl2' style="float:right"><?php echo number_format($total_sub,0,',','.'); ?></label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
												</div>
											</div>
									</div>
									<div class="row" style="">
											<div style="margin-top:10px">
												<div class="col-lg-8 col-md-3 col-sm-3 col-xs-12">
													
												</div>
												<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
													<label>VAT/PPN</label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<label id="vatppn" style="float:right"><?php echo number_format($total_sub*($po['ppn']/100),0,',','.'); ?></label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
												</div>
											</div>
									</div>
									<div class="row" style="border-top-style:solid;">
											<div style="margin-top:10px">
												<div class="col-lg-8 col-md-3 col-sm-3 col-xs-12">
													
												</div>
												<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
													<label>Total</label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<label id="grand_total" style="float:right"><?php echo number_format($total_sub+($total_sub*($po['ppn']/100)),0,',','.'); ?></label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
												</div>
											</div>
									</div>
									<div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="payment-adress">
																<button type="button" class="btn btn-danger waves-effect waves-light" onClick="approve(1)"> Setuju</button>
																<button type="button" class="btn btn-danger waves-effect waves-light" onClick="approve(2)"> Tidak</button>
																<button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()"> Kembali</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                </div>
                               </div> </form>
                        </div>
                    </div>
                </div>
</div>
</div>

                        <div id="ModalAddPo" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-close-area modal-close-df">
                                        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                                    </div>
                                    <div class="modal-body">
                                        <i class="educate-icon educate-checked modal-check-pro"></i>
                                        <h2>Data Berhasil Dirubah</h2>
                                        <p></p>
                                    </div>
                                    <div class="modal-footer">
                                        <a data-dismiss="modal" href="#" onClick="back()">Ok</a>
                                        <!--<a data-dismiss="modal" href="#" onClick="clearform()" >Ya</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
						
						<div id="ModalChangePrincipal" class="modal modal-edu-general Customwidth-popup-WarningModal PrimaryModal-bgcolor fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-close-area modal-close-df">
                                        <!--<a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>-->
                                    </div>
                                    <div class="modal-body">
                                        <span class="educate-icon educate-warning modal-check-pro information-icon-pro"></span>
                                        <h2>Konfirmasi</h2>
                                        <p id="msg_err">Anda Yakin ? data yang sudah di proses tidak bisa dikembalikan lagi</p>
										<input name="apr" id="apr" type="hidden" class="form-control" placeholder="apr" value='' >
                                    </div>
                                    <div class="modal-footer footer-modal-admin warning-md">
                                        <a data-dismiss="modal" href="#" onClick="okchange()">Ok</a>
                                        <a data-dismiss="modal" href="#" onClick="cancelchange()">Batal</a>
                                    </div>
                                </div>
                            </div>
                        </div>

<script>

function approve(des){
	
	
	$('#apr').val(des);
	$('#ModalChangePrincipal').modal('show');
	
}

function tambah_row(){
	
	$('.hapus_btn').hide();
	var values = $('#int_flo').val();
	
	var new_fl = parseInt(values)+1;
	
	$('#int_flo').val(new_fl);
   var datapost = {
		"id_prin": $('#name').val()
      };

      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'purchase_order/get_principal'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {

		var htl = '';
		htl +=	'<div id="row_'+new_fl+'" >';
		htl +=	'									<div class="row" style="border-top-style:solid;">';
		htl +=	'									<div style="margin-top:10px">';
		htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<select name="kode_'+new_fl+'" id="kode_'+new_fl+'" class="form-control list_code" placeholder="Nama Principal">';
		htl +=	'													<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		htl +=	'												</select>';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="qty_'+new_fl+'" id="qty_'+new_fl+'" type="text" class="form-control" onKeyup="change_sum('+new_fl+')" placeholder="Qty" value=0  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="harga_'+new_fl+'" id="harga_'+new_fl+'" type="text" class="form-control" onKeyup="change_sum('+new_fl+')" placeholder="Harga" value=0 >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="diskon_'+new_fl+'" id="diskon_'+new_fl+'" type="text" class="form-control" onKeyup="change_sum('+new_fl+')" placeholder="Diskon" value=0 >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="total_'+new_fl+'" id="total_'+new_fl+'" type="text" class="form-control" placeholder="Total" value=0 readOnly  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<button type="button" class="btn btn-danger waves-effect waves-light " onClick="kurang_row('+new_fl+')">-</button>';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'									</div>';
		htl +=	'									</div>';
		htl +=	'								</div>';
	
		$('#table_items').append(htl);
			
			$('#kode_'+new_fl+'').html(response.list);
			
			
			
        }
      });
	
}

function cancelchange(){
	
	$('#ModalChangePrincipal').modal('hide');
	
}

function okchange(){

	

	   var datapost = {
		"sts": $('#apr').val(),
		"id": <?php echo $po['id_po']; ?>
		
      };

      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'purchase_order/approve_po'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {


			window.location.href = "<?php echo base_url().'purchase_order';?>";

			
			
        }
      });
	  
}


function select_prin(){
	
	var prin = $('#ints').val();
	var prin2 = $('#name').val();
	
	$('#ints2').val(prin);
//	alert(prin2);
	$('#ints').val(prin2);

	if(prin !== '0'){
		
		$('#msg_err').html('Jika Pilih Principal Lain, Data Yang Tidak Tersimpan Akan Terhapus, Yakin Akan Melanjutkan ? ');
		$('#ModalChangePrincipal').modal({backdrop: 'static', keyboard: false});
	}else{
		
	  var datapost = {
		"id_prin": $('#name').val()
      };

      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'purchase_order/get_principal'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
			//var obj = JSON.parse(response);
			$('#alamat').val(response['principal'][0].eksternal_address);
			$('#att').val(response['principal'][0].pic);
			$('#telp').val(response['principal'][0].phone_1);
			$('#fax').val(response['principal'][0].fax);
			//console.log(response.phone_1);
			
			
			$('#table_items').html('')	
		
			var htl = '';
			htl +=	'<div id="row_0" >';
			htl +=	'									<div class="row" style="border-top-style:solid;">';
			htl +=	'									<div style="margin-top:10px">';
			htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
			htl +=	'											<div class="form-group">';
			htl +=	'												<select name="kode_0" id="kode_0" class="form-control list_code" placeholder="Nama Principal">';
			htl +=	'													<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
			htl +=	'												</select>';
			htl +=	'											</div>';
			htl +=	'										</div>';
			htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			htl +=	'											<div class="form-group">';
			htl +=	'												<input name="qty_0" id="qty_0" type="text" class="form-control" onKeyup="change_sum(0)" placeholder="Qty" value=0 >';
			htl +=	'											</div>';
			htl +=	'										</div>';
			htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			htl +=	'											<div class="form-group">';
			htl +=	'												<input name="harga_0" id="harga_0" type="text" class="form-control" onKeyup="change_sum(0)" placeholder="Harga" value=0 >';
			htl +=	'											</div>';
			htl +=	'										</div>';
			htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			htl +=	'											<div class="form-group">';
			htl +=	'												<input name="diskon_0" id="diskon_0" type="text" class="form-control" onKeyup="change_sum(0)" placeholder="Diskon" value=0 >';
			htl +=	'											</div>';
			htl +=	'										</div>';
			htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			htl +=	'											<div class="form-group">';
			htl +=	'												<input name="total_0" id="total_0" type="text" class="form-control" placeholder="Total" value=0 readOnly  >';
			htl +=	'											</div>';
			htl +=	'										</div>';
			htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			htl +=	'											<div class="form-group">';
			htl +=	'												';
			htl +=	'											</div>';
			htl +=	'										</div>';
			htl +=	'									</div>';
			htl +=	'									</div>';
			htl +=	'								</div>';
		
			$('#table_items').append(htl);
			$('#kode_0').html(response.list);
        }
      });
		
	}
	
	//$('#ints').val('1');
	
}

function kurang_row(new_fl){
	
	$('#row_'+new_fl).remove();
	change_sum_all();
	
}

function clearform(){
	
	$('#edit_po').trigger("reset");
	$('#term').val("h");
	$('#ppn').val("10");
	$('#tgl').val("");
	$('#subttl').html("0");
	$('#subttl2').html("0");
	$('#tvatppngl').html("0");
	$('#grand_total').html("0");
	
	$('#table_items').html('')	
		
			var htl = '';
			htl +=	'<div id="row_0" >';
			htl +=	'									<div class="row" style="border-top-style:solid;">';
			htl +=	'									<div style="margin-top:10px">';
			htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
			htl +=	'											<div class="form-group">';
			htl +=	'												<select name="kode_0" id="kode_0" class="form-control list_code" placeholder="Nama Principal">';
			htl +=	'													<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
			htl +=	'												</select>';
			htl +=	'											</div>';
			htl +=	'										</div>';
			htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			htl +=	'											<div class="form-group">';
			htl +=	'												<input name="qty_0" id="qty_0" type="text" class="form-control" onKeyup="change_sum(0)" placeholder="Qty" value=0 >';
			htl +=	'											</div>';
			htl +=	'										</div>';
			htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			htl +=	'											<div class="form-group">';
			htl +=	'												<input name="harga_0" id="harga_0" type="text" class="form-control" onKeyup="change_sum(0)" placeholder="Harga" value=0 >';
			htl +=	'											</div>';
			htl +=	'										</div>';
			htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			htl +=	'											<div class="form-group">';
			htl +=	'												<input name="diskon_0" id="diskon_0" type="text" class="form-control" onKeyup="change_sum(0)" placeholder="Diskon" value=0 >';
			htl +=	'											</div>';
			htl +=	'										</div>';
			htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			htl +=	'											<div class="form-group">';
			htl +=	'												<input name="total_0" id="total_0" type="text" class="form-control" placeholder="Total" value=0 readOnly  >';
			htl +=	'											</div>';
			htl +=	'										</div>';
			htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			htl +=	'											<div class="form-group">';
			htl +=	'												';
			htl +=	'											</div>';
			htl +=	'										</div>';
			htl +=	'									</div>';
			htl +=	'									</div>';
			htl +=	'								</div>';
		
			$('#table_items').append(htl);
	
}

function change_sum(fl){
	
	var qty = 	$('#qty_'+fl).val();
	var harga = 	$('#harga_'+fl).val();
	var diskon = 	$('#diskon_'+fl).val();
	var ppn = 	$('#ppn').val();
	
	var total = (parseFloat(qty) * parseFloat(harga) )- ((parseFloat(qty) * parseFloat(harga))*(parseFloat(diskon)/100)); 
	
	
	
	if( isNaN(total)){
		//var total_num = Number((total).toFixed(1)).toLocaleString();
		//$('#subttl').html('0');
		$('#total_'+fl).val('0');
	}else{
		//var total_num = Number((total).toFixed(1)).toLocaleString();
		//$('#subttl').html(total);
		$('#total_'+fl).val(total);
	}
	
	var sub_total = 0;
	var int_val = parseInt($('#int_flo').val());
		
	for(var yo = 0;yo <= int_val;yo++ ){
				
		if($('#kode_'+yo).val() !== undefined){
				sub_total = sub_total +	parseFloat($('#total_'+yo).val());
		}
				
	}
	
	var total_ppn = (sub_total * (parseFloat($('#ppn').val())/100 ));
	var grand_total = sub_total + (sub_total * (parseFloat($('#ppn').val())/100 ));
	
	$('#subttl').html(sub_total);
	$('#subttl2').html(sub_total);
	$('#vatppn').html(total_ppn);
	$('#grand_total').html(grand_total);
	
	
	
	
	//alert(qty+' '+harga+' '+diskon);
	
}

function change_sum_all(){
	
	var sub_total = 0;
	var int_val = parseInt($('#int_flo').val());
		
	for(var yo = 0;yo <= int_val;yo++ ){
				
		if($('#kode_'+yo).val() !== undefined){
				sub_total = sub_total +	parseFloat($('#total_'+yo).val());
		}
				
	}
	
	var total_ppn = (sub_total * (parseFloat($('#ppn').val())/100 ));
	var grand_total = sub_total + (sub_total * (parseFloat($('#ppn').val())/100 ));
	
	$('#subttl').html(sub_total);
	$('#subttl2').html(sub_total);
	$('#vatppn').html(total_ppn);
	$('#grand_total').html(grand_total);
	
	
	
	
	//alert(qty+' '+harga+' '+diskon);
	
}

function back(){
	
	window.location.href = "<?php echo base_url().'purchase_order';?>";
	
}

  function listdist(){
	  var user_id = '0001';
	  var token = '093940349';
	  
	  
   $('#datatable_pricipal').DataTable({
	   //"dom": 'rtip',
		"bFilter": false,
		"aaSorting": [],
		"bLengthChange": true,
		'iDisplayLength': 10,
		"sPaginationType": "simple_numbers",
		"Info" : false,
		"processing": true,
        "serverSide": true,
        "destroy": true,
		"ajax": "<?php echo base_url().'principal_management/lists'?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token,
		"searching": true,
		"language": {
            "decimal": ",",
            "thousands": "."
        },
		"dom": 'l<"toolbar">frtip',
		"initComplete": function(){
                           $("div.toolbar").prepend('<div class="btn-group pull-left"><button type="button" class="btn btn-custon-rounded-two btn-primary" > Tambah </button></div>');
        }
	});	
  }

	$(document).ready(function(){
		
		$('#tgl').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		});
		
      listdist();
	  
	  $('#edit_po').on('submit', function(e){
		// validation code here
		//if(!valid) {
			e.preventDefault();
			
			// var formData = new FormData(this);
			var formData = new FormData();
			 var urls = $(this).attr('action');
			 
			formData.append('id', <?php echo $po['id_po']; ?>);
			formData.append('kode', $('#kode').val());
			formData.append('tgl', $('#tgl').val());
			formData.append('ints', $('#ints').val());
			formData.append('ints2', $('#ints2').val());
			formData.append('name', $('#name').val());
			formData.append('ppn', $('#ppn').val());
			formData.append('term', $('#term').val());
			formData.append('int_flo', $('#int_flo').val());
			
			var int_val = parseInt($('#int_flo').val());
			
			for(var yo = 0;yo <= int_val;yo++ ){
				
				if($('#kode_'+yo).val() !== undefined){
					
					formData.append('kode_'+yo, $('#kode_'+yo).val());
					formData.append('harga_'+yo, $('#harga_'+yo).val());
					formData.append('total_'+yo, $('#total_'+yo).val());
					formData.append('qty_'+yo, $('#qty_'+yo).val());
					formData.append('diskon_'+yo, $('#diskon_'+yo).val());
					
				}
				
			}
			
		 title: 'Yakin akan Menyetujui Data ?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak'
    }).then(function() {
			
			
			//console.log(formData);

          $.ajax({
              type:'POST',
				url: $(this).attr('action'),
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                success: function(response) {
					
					//console.log(response);
					// if(response.success == true){
						// $('#ModalAddPo').modal('show');
					// }else{
						// $('#msg_err').html(response.message);
						// $('#ModalChangePo').modal('show');
					// }
					
					swal({
						  title: 'Success!',
						  text: "Data Berhasil di Approve ",
						  type: 'success',
						  showCancelButton: false,
						  confirmButtonText: 'Ya'
						}).then(function () {
							//$("#add_material").form('reset');
							back();
						})
                }
            });
			
		  });	
			//alert(kode);
	
	
		//}
	  });
  });
</script>