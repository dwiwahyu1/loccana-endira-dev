<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .paper {
            width: 100%;
            page-break-after: always;
        }

        .paper:last-child {
            page-break-after: avoid;
        }

        body {
            font-weight: bold;
        }

        .subtitle {
            font-size: 24px;
        }
    </style>
</head>

<body>
    <div class="paper" id="faktur" style="width: 100%;">
        <div class="row" style="height: 180px;">
            <table border="0.0px" style="width: 50%; float: left; border-collapse:collapse;">
                <tr>
                    <td colspan="3"><strong><span class="subtitle">PT. ENDIRA ALDA</span></strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td>JL. Sangkuriang No.38-A</td>
                </tr>
                <tr>
                    <td>NPWP</td>
                    <td>:</td>
                    <td>01.555.161.7.428.000</td>
                </tr>
            </table>

            <table border="0.0px" style="width: 50%; float: left; border-collapse:collapse;">
                <tr>
                    <td colspan="3"><strong><span class="subtitle">FAKTUR</span></strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><strong>No.Faktur</strong></td>
                    <td>:</td>
                    <td><strong><?= $result_penjualan[0]['no_penjualan'] ?? ''; ?></strong></td>
                </tr>
                <tr>
                    <td><strong>Tanggal</strong></td>
                    <td>:</td>
                    <td><strong><?= $result_penjualan[0]['date_penjualan'] ?? ''; ?></strong></td>
                </tr>
                <tr>
                    <td>Alamat pengiriman</td>
                    <td>:</td>
                    <td><?= $result_penjualan[0]['cust_address'] ?? ''; ?></td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td>:</td>
                    <td><?= $result_penjualan[0]['phone'] ?? ''; ?></td>
                </tr>
            </table>
        </div>
        <div class="row" style="margin-bottom: 24px;">
            <span><strong><span class="subtitle">Pembeli</span></strong></span><br><br>
            <span><?= $result_penjualan[0]['cust_name'] ?? ''; ?></span><br>
            <span><?= $result_penjualan[0]['cust_address'] ?? ''; ?></span>
        </div>
        <div class="row">
            <table style="width: 100%; border-collapse:collapse;">
                <thead>
                    <tr>
                        <td style="border: 1px solid black; padding-left: 4px; width: 5px;">No.</td>
                        <td style="border: 1px solid black; padding-left: 4px;">Kode</td>
                        <td style="border: 1px solid black; padding-left: 4px;">Nama Barang</td>
                        <td style="border: 1px solid black; padding-left: 4px;">Qty</td>
                        <td style="border: 1px solid black; padding-left: 4px;">Jumlah Box</td>
                        <td style="border: 1px solid black; text-align: left; padding-left: 4px;">Harga</td>
                        <td colspan="2" style="border: 1px solid black; text-align: left; padding-left: 4px;">Jumlah Rp</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $rowNumber = 1;
                    $total_retur = 0;
                    $dpp = 0;
                    foreach ($items as $item) : ?>
                        <tr>
                            <td style="border: 1px solid black; padding-left: 4px; width: 5px;"><?= $rowNumber; ?></td>
                            <td style="border: 1px solid black; padding-left: 4px;"><?= $item['stock_code'] ?? ''; ?></td>
                            <td style="border: 1px solid black; padding-left: 4px;"><?= implode(' ', [$item['stock_name'], $item['base_qty'], $item['uom_symbol']]) ?></td>
                            <td style="border: 1px solid black; padding-left: 4px; text-align: center;"><?= $item['qty_box_retur'] ?? ''; ?></td>
                            <td style="border: 1px solid black; padding-left: 4px; text-align: center;"><?= 'Box @ ' . ($item['box_ammount'] ?? ''); ?></td>
                            <td style="border: 1px solid black; text-align: right; padding-right: 4px;"><?= indonesia_currency_format($item['unit_price']); ?></td>
                            <td colspan="2" style="border: 1px solid black; text-align: right; padding-right: 4px;"><?= indonesia_currency_format($item['price']); ?></td>
                        </tr>
                    <?php
                        $rowNumber++;
                        $total_retur += $item['price'];
                        $dpp += (($item['price'] / (10 + ($item['pajak'] / 10))) * 10);
                    endforeach; ?>
                    <tr>
                        <td></td>
                        <td style="border:none; text-align:center;">Mengetahui,</td>
                        <td colspan="3" style="border:none;"></td>
                        <td style="border:none;">Total</td>
                        <td style="border: 1px solid black; padding-left: 4px; width: 10px;">Rp</td>
                        <td style="border: 1px solid black; padding-right: 4px; text-align: right;"><?= indonesia_currency_format($total_retur); ?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="4" style="border:none;"></td>
                        <td style="border:none;">DPP</td>
                        <td style="border: 1px solid black; padding-left: 4px; width: 10px;">Rp</td>
                        <td style="border: 1px solid black; padding-right: 4px; text-align: right;"><?= indonesia_currency_format($dpp); ?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="border:none; text-align:center;">________</td>
                        <td colspan="3" style="border:none;"></td>
                        <td style="border:none;">PPN</td>
                        <td style="border: 1px solid black; padding-left: 4px; width: 10px;">Rp</td>
                        <td style="border: 1px solid black; padding-right: 4px; text-align: right;"><?= indonesia_currency_format($total_retur - $dpp); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div><!-- paper -->

	

    <div class="paper">
        <div class="row" style="height: 180px;">
            <table border="0.0px" style="width: 50%; float: left; border-collapse:collapse;">
                <tr>
                    <td colspan="3"><strong><span class="subtitle">PT. ENDIRA ALDA</span></strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td>JL. Sangkuriang No.38-A</td>
                </tr>
                <tr>
                    <td>NPWP</td>
                    <td>:</td>
                    <td>01.555.161.7.428.000</td>
                </tr>
            </table>

            <table border="0.0px" style="width: 50%; float: left; border-collapse:collapse;">
                <tr>
                    <td colspan="3"><strong><span class="subtitle">DO</span></strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><strong>No.Faktur</strong></td>
                    <td>:</td>
                    <td><strong><?= $result_penjualan[0]['no_penjualan'] ?? ''; ?></strong></td>
                </tr>
                <tr>
                    <td><strong>Tanggal</strong></td>
                    <td>:</td>
                    <td><strong><?= $result_penjualan[0]['date_penjualan'] ?? ''; ?></strong></td>
                </tr>
                <tr>
                    <td>Alamat pengiriman</td>
                    <td>:</td>
                    <td><?= $result_penjualan[0]['cust_address'] ?? ''; ?></td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td>:</td>
                    <td><?= $result_penjualan[0]['phone'] ?? ''; ?></td>
                </tr>
            </table>
        </div>
        <div class="row" style="margin-bottom: 24px;">
            <span><strong><span class="subtitle">Pembeli</span></strong></span><br><br>
            <span><?= $result_penjualan[0]['cust_name'] ?? ''; ?></span><br>
            <span><?= $result_penjualan[0]['cust_address'] ?? ''; ?></span>
        </div>
        <div class="row">
            <table style="width: 100%; border-collapse:collapse;">
                <thead>
                    <tr>
                        <td style="border: 1px solid black; padding-left: 4px; width: 5px;">No.</td>
                        <td style="border: 1px solid black; padding-left: 4px;">Kode</td>
                        <td style="border: 1px solid black; padding-left: 4px;">Nama Barang</td>
                        <td style="border: 1px solid black; padding-left: 4px;">Qty</td>
                        <td style="border: 1px solid black; padding-left: 4px;">Jumlah Box</td>
                        <td style="border: 1px solid black; text-align: left; padding-left: 4px;">Banyaknya Kemasan</td>
                        <td style="border: 1px solid black; text-align: left; padding-left: 4px;">Liter/Kg</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $rowNumber = 1;
                    $qty = 0;
                    $price = 0;
                    $qty_retur = 0;
                    $ltkg = 0;
                    foreach ($items as $item) : ?>
                        <tr>
                            <td style="border: 1px solid black; padding-left: 4px; width: 5px;"><?= $rowNumber; ?></td>
                            <td style="border: 1px solid black; padding-left: 4px;"><?= $item['stock_code'] ?? ''; ?></td>
                            <td style="border: 1px solid black; padding-left: 4px;"><?= implode(' ', [$item['stock_name'], $item['base_qty'], $item['uom_symbol']]) ?></td>
                            <td style="border: 1px solid black; padding-left: 4px; text-align: center;"><?= $item['qty_box_retur'] ?? ''; ?></td>
                            <td style="border: 1px solid black; padding-left: 4px; text-align: center;"><?= 'Box @ ' . ($item['box_ammount'] ?? ''); ?></td>
                            <td style="border: 1px solid black; text-align: right; padding-right: 4px;"><?= indonesia_currency_format($item['qty_retur']); ?></td>
                            <td style="border: 1px solid black; text-align: right; padding-right: 4px;"><?= indonesia_currency_format($item['bpl'] * $item['qty_box_retur']); ?></td>
                        </tr>
                    <?php
                        $rowNumber++;
                        $qty += $item['qty_box_retur'];
                        $qty_retur += $item['qty_retur'];
                        $price += $item['unit_price'];
                        $ltkg += $item['bpl'] * $item['qty_box_retur'];
                    endforeach; ?>
                    <tr>
                        <td style="border: 1px solid black;"></td>
                        <td style="border: 1px solid black;"></td>
                        <td style="border: 1px solid black;">Jumlah</td>
                        <td style="border: 1px solid black; text-align:center;"><?= $qty; ?></td>
                        <td style="border: 1px solid black;"></td>
                        <td style="border: 1px solid black; text-align:right; padding-right: 4px;"><?= indonesia_currency_format($qty_retur); ?></td>
                        <td style="border: 1px solid black; text-align:right; padding-right: 4px;"><?= indonesia_currency_format($ltkg); ?></td>
                    </tr>
                </tbody>
            </table>
            <table style="width: 100%; border-collapse:collapse;">
                <tbody>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">Penerima</td>
                        <td style="text-align:center;">Menyetujui</td>
                        <td style="text-align:center;">Pengirim</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">__________</td>
                        <td style="text-align:center;">__________</td>
                        <td style="text-align:center;">__________</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="paper">
        <h5><span class="subtitle">Permintaan Barang <?= date('Y-M-d'); ?></span></h5>
        <table style="width: 100%; border-collapse:collapse;">
            <thead>
                <tr>
                    <td style="border: 1px solid black; padding-left: 4px;">No.</td>
                    <td style="border: 1px solid black; padding-left: 4px;">Customer</td>
                    <td style="border: 1px solid black; padding-left: 4px;">Kota</td>
                    <td style="border: 1px solid black; padding-left: 4px;">Produk</td>
                    <td style="border: 1px solid black; padding-right: 4px; text-align:right;">Pack</td>
                    <td style="border: 1px solid black; padding-right: 4px; text-align:right;">Box</td>
                    <td style="border: 1px solid black; padding-right: 4px; text-align:right;">Liter/Kg</td>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                $qty = 0;
                $price = 0;
                $qty_retur = 0;
                $ltkg = 0;
                foreach ($items as $item) : ?>
                    <tr>
                        <td style="border: 1px solid black; padding-left: 4px;"><?= $i; ?></td>
                        <td style="border: 1px solid black; padding-left: 4px;"><?= $item['cust_name'] ?? ''; ?></td>
                        <td style="border: 1px solid black; padding-left: 4px;"><?= $item['city'] ?? ''; ?></td>
                        <td style="border: 1px solid black; padding-left: 4px;"><?= $item['stock_name'] ?? ''; ?></td>
                        <td style="border: 1px solid black; padding-right: 4px; text-align: right;"><?= $item['base_qty'] ?? ''; ?></td>
                        <td style="border: 1px solid black; padding-right: 4px; text-align: right;"><?= indonesia_currency_format($item['qty_box_retur']); ?></td>
                        <td style="border: 1px solid black; padding-right: 4px; text-align: right;"><?= indonesia_currency_format($item['bpl'] * $item['qty_box_retur']); ?></td>
                    </tr>
                <?php
                    $i++;
                    $qty += $item['qty_box_retur'];
                    $qty_retur += $item['qty_retur'];
                    $price += $item['unit_price'];
                    $ltkg += $item['bpl'] * $item['qty_box_retur'];
                endforeach; ?>
                <tr>
                    <td colspan="5"></td>
                    <td style="border: 1px solid black; padding-right: 4px; text-align: right;"><?= indonesia_currency_format($qty); ?></td>
                    <td style="border: 1px solid black; padding-right: 4px; text-align: right;"><?= indonesia_currency_format($ltkg); ?></td>
                </tr>
            </tbody>
        </table>
        <table style="width: 100%; border-collapse:collapse;">
            <tbody>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:center;">Diminta oleh</td>
                    <td style="text-align:center;">Menyetujui</td>
                    <td style="text-align:center;">Penerima</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:center;">__________</td>
                    <td style="text-align:center;">__________</td>
                    <td style="text-align:center;">__________</td>
                </tr>
            </tbody>
        </table>
    </div><!-- paper -->
</body>

</html>