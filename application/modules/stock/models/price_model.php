<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Price_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
    
	
	public function get_price($params = array()){
		
		$query = $this->db->get_where('m_material', array('id_mat' => $params['id_mat']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	
	
    public function get_material($id_mat) {
    	$query = $this->db->get_where('m_material', array('id_mat' => $id_mat));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function lists($params = array()) {
		$query = 	'
				SELECT COUNT(*) AS jumlah FROM t_eksternal where type_eksternal = 1
				AND ( 
					kode_eksternal LIKE "%'.$params['searchtxt'].'%" OR
					name_eksternal LIKE "%'.$params['searchtxt'].'%" 
				)
			';
						
			$query2 = 	'
				SELECT z.*, rank() over ( ORDER BY stock_code ASC) AS Rangking from ( 
					
					SELECT a.id_mat,a.stock_code,a.stock_name,a.top_price,a.bottom_price,a.base_price,
					a.stock_description,b.uom_name,a.unit_box,c.name_eksternal,c.kode_eksternal 
					FROM m_material a
					JOIN m_uom b ON a.unit_terkecil=b.id_uom
					JOIN t_eksternal c ON a.dist_id=c.id
					WHERE  ( 
					stock_code LIKE "%'.$params['searchtxt'].'%" OR
					stock_name LIKE "%'.$params['searchtxt'].'%" 
				)  order by stock_code) z
				ORDER BY stock_code ASC
				
				LIMIT '.$params['limit'].' 
				OFFSET '.$params['offset'].' 
			';
		
			//echo $query2;die;
		
			 $out		= array();
			  $querys		= $this->db->query($query);
			  $result = $querys->row();
			  
			  $total_filtered = $result->jumlah;
			  $total 			= $result->jumlah;
		  
				if(($params['offset']+10) > $total_filtered){
				$limit_data = $total_filtered - $params['offset'];
			  }else{
				$limit_data = $params['limit'] ;
			  }
		  
		
		  
		  //echo $query;die;
			//echo $query;die;
			 $query2s		= $this->db->query($query2);
		  $result2 = $query2s->result_array();						
		  $return = array(
			  'data' => $result2,
			  'total_filtered' => $total_filtered,
			  'total' => $total,
		  );
		  return $return;
	}


	public function edit_price($data) {
		$datas = array(
			'top_price' => $data['top_price'],
			'bottom_price' => $data['bottom_price']
		);

	//	print_r($datas);die;
		$this->db->where('id_mat',$data['id_mat']);
		$this->db->update('m_material',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	
	public function add_price_history($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'id_mat' => $data['id_mat'],
			'top_price' => $data['top_price'],
			'bottom_price' => $data['bottom_price'],
			'harga_pokok' => $data['harga_pokok'],
			'create_date' => $data['create_date'],
			'id_pengaju' => $data['id_pengaju']
		);

	//	print_r($datas);die;

		$this->db->insert('t_price_history',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}	
	
	public function update_price($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'top_price' => $data['top_price'],
			'bottom_price' => $data['bottom_price'],
			'harga_pokok' => $data['harga_pokok'],
			'create_date' => $data['create_date'],
			'id_pengaju' => $data['id_pengaju']
		);

	//	print_r($datas);die;
		$this->db->where('id_ph',$data['id_ph']);
		$this->db->update('t_price_history',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function get_price_history($params = array()){
		
		$query=$this->db->order_by('id_ph','desc')
						->get_where('t_price_history', array('id_mat' => $params['id_mat']), 1,0);
	
		// $this->db->order_by('id_hl','asc');
		// $query=$this->db->get();
		$return = $query->result_array();

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $return;
	}

	public function update_price_app($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			
			'id_approval' => $data['id_approval'],
			'approval_date' => $data['approval_date']
		);

	//	print_r($datas);die;
		$this->db->where('id_ph',$data['id_ph']);
		$this->db->update('t_price_history',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function update_status($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			
			'status_harga' => $data['status_harga']
		);

	//	print_r($datas);die;
		$this->db->where('id_mat',$data['id_mat']);
		$this->db->update('m_material',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function deletes($id_mat) {
		$this->db->where('id_mat', $id_mat);
		$this->db->delete('m_material'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}


}
