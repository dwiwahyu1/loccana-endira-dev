<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto;
  }

  .scroll-overflow {
    min-height: 650px;
  }

  #modal-distributor::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }

  table thead {
    position: sticky;
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
          <div class="card-box table-responsive">
            <div style="display: flex; justify-content:space-between; align-items:center; margin-bottom: 12px;">
              <h4 class="header-title" style="margin:0;">Rekap Saldo per Bank</h4>
            </div>
            <form id="report-cash-filter-form" action="<?= base_url('report_pembelian/export_excel'); ?>" class="add-department" method="post" target="_blank" autocomplete="off">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <!-- <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                     <div class="form-group">
                      <label>Akun</label>
                      <select name="coa_id" id="coa_id" class="form-control">
                        <option value="0" selected>Semua Akun</option>
                        <?php foreach ($coa_list as $coa) : ?>
                          <option value="<?= $coa['id_coa']; ?>"><?= implode(' - ', [$coa['coa'], $coa['keterangan']]); ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div> 
                  </div>-->
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group date">
                      <label>Tanggal Awal</label>
                      <input name="start_date" id="start_date" type="text" class="form-control" value="<?php echo date('Y-m-1') ?>" placeholder="Tanggal Awal">
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group date">
                      <label>Tanggal Akhir</label>
                      <input name="end_date" id="end_date" type="text" class="form-control" value="<?php echo date('Y-m-d') ?>" placeholder="Tanggal Akhir">
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group" style="margin-top:35px;margin-bottom:35px;">
                      <div class="row">
                        <div class="col-lg-12">
                          <div class="col-lg-4">
                            <button class="btn btn-primary submit-button" type="submit">Submit</button>
                          </div>
                          <!-- <div class="col-lg-4">
                            <button class="btn btn-primary m-t-4" onclick="print_excel()">Export</button>
                          </div> -->
                          <div class="col-lg-4">
                            <a href="<?= base_url('report_cash'); ?>/index_rekap" class="btn btn-primary m-t-4">Detail</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
            <div class="col-lg-10 col-md-6 col-sm-6 col-xs-12" style="margin-top:10px">
              <div class="sparkline13-list">
                <div class="card-box table-responsive">

                  <table id="listpemohons" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead>
                      <tr>
                        <th class="dt-body-center">COA</th>
                        <th>Keterangan</th>
                        <th>Nilai</th>
                      </tr>
                    </thead>
                    <tbody id='body_salesa'>
                      <?php
                      $tot_a = 0;
                      foreach ($get_aktiva as $get_aktivas) {

                        echo ' <tr>
                        <th >' . $get_aktivas['coa'] . '</th>
                        <th>' . $get_aktivas['keterangan'] . '</th>
                        <th>' . number_format($get_aktivas['nilai'], 2, '.', ',') . '</th>
                      </tr>';

                        $tot_a += $get_aktivas['nilai'];
                      }

                      ?>
                      <tr>
                        <th colspan=2>Total</th>
                        <th><?php echo number_format($tot_a, 2, '.', ',') ?></th>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div><!-- end col -->
      </div>
    </div>
  </div>
  <span id="laod"></span>
</div>

<script>
  // $('table#datatable_pricipal').floatThead({
  //   top: 100
  // });

  $('select').select2();

  $('#start_date, #end_date').datepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayHighlight: true
  });

  function load_list() {

    swal({
      title: 'Loading',
      text: 'Loading Data',
      // type: 'success',
      showCancelButton: false,
      showConfirmButton: false,
      imageUrl: '<?php echo base_url(); ?>assets/images/ajax-loader.gif',
      confirmButtonText: 'Ok',
      allowOutsideClick: true

    }).then(function() {})

    var datapost = {
      "start_time": $('#start_date').val(),
      "end_time": $('#end_date').val()
    };

    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>report_cash/filter",
      data: JSON.stringify(datapost),
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      success: function(response) {
        $('#body_salesa').html('');
        $('#body_salesa').html(response.t_ac);

        swal.close();

      }
    });

  }


  function print_excel() {

    var user_id = '0001';
    var token = '093940349';

    var params = [
      'sess_user_id=' + user_id,
      'sess_token=' + token,
      'coa_id=' + $('#coa_id').val() || '',
      'start_date=' + $('#start_date').val() || '',
      'end_date=' + $('#end_date').val() || '',
    ];

    var params_string = '?' + params.join('&');
    var url = '<?= base_url('report_cash/excel'); ?>' + params_string;
    var coa_id = $('#coa_id').val();
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var form = $("<form action='" + url + "' method='post' target='_blank'><input type='hidden' name='coa_id' value='" + coa_id + "' /><input type='hidden' name='start_date' value='" + start_date + "' /><input type='hidden' name='end_date' value='" + end_date + "' /></form>");
    $('body').append(form);
    form.submit();
  }

  $(document).ready(function() {
    $('#report-cash-filter-form').on('submit', function(e) {
      e.preventDefault();
      //  $('.submit-button').attr('disabled', true);
      load_list();
    });
    load_list();
  });
</script>