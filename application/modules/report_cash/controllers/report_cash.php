<?php

use function PHPSTORM_META\type;

defined('BASEPATH') or exit('No direct script access allowed');

class Report_cash extends MX_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('report_cash/report_cash_model', 'reportCashModel');
    $this->load->model('selling/selling_model');
    $this->load->model('invoice_selling/return_model');
    $this->load->model('neraca/uom_model');
    $this->load->library('log_activity');
    $this->load->library('priv');

    $this->form_validation->set_message('required', '%s Tidak Boleh Kosong');
    $this->form_validation->set_message('min_length', '%s Minimal 4 Karakter');
    $this->form_validation->set_message('max_length', '%s Maksimal 10 Karakter');
  }


  public function getDatesFromRange($start, $end, $format = 'Y-m-01') {
    $array = array();
    $interval = new DateInterval('P1D');

    $realEnd = new DateTime($end);
    $realEnd->add($interval);

    $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

    $curr = '';
    foreach ($period as $date) {
      if ($curr == '') {
        $array[] = $date->format($format);
      } else {
        if ($curr == $date->format($format)) {
        } else {
          $array[] = $date->format($format);
        }
      }

      $curr = $date->format($format);
    }

    return $array;
  }

  function filter() {

    $data   = file_get_contents("php://input");
    $param   = json_decode($data, true);

    $params  = array(
      'start_time' => $param['start_time'],
      'end_time' => $param['end_time'],
      'report' => "aktiva"
    );
    $datas['get_aktiva'] = $this->uom_model->get_report_keu($params);



    // echo"<pre>";print_r($params);die;

    $html_a = '';

    $tot_a = 0;
    foreach ($datas['get_aktiva'] as $get_aktivas) {
      if ($get_aktivas['id_parent'] == 3) {

        $params_c  = array(
          'start_date' => $param['start_time'],
          'end_date' => $param['end_time'],
          'coa_id' => $get_aktivas['id_coa']
        );

        $get_cash = $this->get_cash($params_c);

        $html_a .= ' <tr>
          <th >' . $get_aktivas['coa'] . '</th>
          <th>' . $get_aktivas['keterangan'] . '</th>
          <th>' . number_format($get_cash, 2, ',', '.') . '</th>
          </tr>';
        $tot_a += $get_cash;
      }
    }

    $html_a .= ' <tr>
					<th colspan=2>Total</th>
					<th>' . number_format($tot_a, 2, ',', '.') . '</th>
				</tr>';

    $datas['t_ac'] = $html_a;

    $this->output->set_content_type('application/json')->set_output(json_encode($datas));
  }


  public function get_cash($param) {

    $params['coa_id']    = $param['coa_id'];
    $params['start_date']  = $param['start_date'];
    $params['end_date']    = $param['end_date'];

    if ($params['end_date'] == "2021-01-01") {

      $list_saldo = $this->uom_model->list_report_cash_saldo($params);

      $list = $this->uom_model->list_report_cash($params);
      $typeCoa = ['Debit', 'Kredit'];
      $data = [];
      $balance = 0.00;

      foreach ($list_saldo['data'] as $row) {

        if ($row['kr_coa'] == '61301' || $row['kr_coa'] == '61302' || $row['kr_coa'] == '61303' || $row['kr_coa'] == '12100' || $row['kr_coa'] == '12102' || $row['kr_coa'] == '12200' || $row['kr_coa'] == '12202' || $row['kr_coa'] == '12300' || $row['kr_coa'] == '12302') {
        } else {

          array_push($data, [
            '', // tanggal
            '', // tanggal
            // $typeCoa[$row['type_cash']] ?? '-', // transaksi
            '',
            'Saldo Awal', // transaksi
            // kode akun
            '', // akun
            '', // uraian
            '-', // debit
            '-', // kredit
            indonesia_currency_format($row['saldo_awal']), // saldo
            // '-', // action
          ]);
        }

        $balance = $row['saldo_awal'];
      }

      foreach ($list['data'] as $row) {

        if ($row['kr_coa'] == '61301' || $row['kr_coa'] == '61302' || $row['kr_coa'] == '61303' || $row['kr_coa'] == '12100' || $row['kr_coa'] == '12102' || $row['kr_coa'] == '12200' || $row['kr_coa'] == '12202' || $row['kr_coa'] == '12300' || $row['kr_coa'] == '12302') {
        } else {


          if ($row['type_cash'] == 0) {
            $balance += $row['sum_amnt'];
          } else if ($row['type_cash'] == 1) {
            $balance -= $row['sum_amnt'];
          }

          // if($row['kr_note'] == 'Utang Dagang' || $row['kr_note'] == 'Piutang Dagang'){ 
          // $row['banks'] = $row['keterangan']; 
          // }

          // if ($row['bukti'] == "Bayar Piutang") {
          // $faktur = $row['jual'];
          // $note_ac = $row['cust_name'];
          // }elseif ($row['bukti'] == "Bayar Hutang") {
          // $faktur = $row['beli'];
          // $note_ac = $row['name_eksternal'];
          // }else{
          $faktur = '';
          $note_ac = $row['k_note'] . '' . $row['users'];
          // }

          array_push($data, [
            $row['date'], // tanggal
            $row['banks'], // tanggal
            // $typeCoa[$row['type_cash']] ?? '-', // transaksi
            $row['kr_coa'],
            $row['kr_note'], // transaksi
            // kode akun 
            $note_ac, // akun
            $row['invoice'], // uraian
            $row['type_cash'] == 0 ? indonesia_currency_format($row['sum_amnt']) : '-', // debit
            $row['type_cash'] == 1 ? indonesia_currency_format($row['sum_amnt']) : '-', // kredit
            indonesia_currency_format($balance), // saldo
            // '-', // action
          ]);
        }
      }
    } else {

      $params_awal['coa_id']    = $params['coa_id'];
      $params_awal['start_date']  = "2021-01-01";
      $params_awal['end_date']  = date("Y-m-d", strtotime(date_format(date_create($param['end_date']), "Y-m-01") . "-1 days"));

      $paramss['coa_id']    = $params['coa_id'];
      $paramss['start_date']  = date_format(date_create($param['end_date']), "Y-m-01");
      $paramss['end_date']  = $param['end_date'];


      // print_r($params_awal);

      // print_r($paramss);die;

      $list_saldo = $this->uom_model->list_report_cash_saldo($params_awal);

      $list_awal = $this->uom_model->list_report_cash($params_awal);

      $list = $this->uom_model->list_report_cash($paramss);

      //print_r($list);die;

      $typeCoa = ['Debit', 'Kredit'];
      $data = [];
      $balance = 0.00;

      foreach ($list_saldo['data'] as $row) {

        // array_push($data, [
        // '', // tanggal
        // '', // tanggal
        // // $typeCoa[$row['type_cash']] ?? '-', // transaksi
        // '', 
        // 'Saldo Awal', // transaksi
        // // kode akun
        // '', // akun
        // '', // uraian
        // '-', // debit
        // '-', // kredit
        // indonesia_currency_format($row['saldo_awal']), // saldo
        // // '-', // action
        // ]);


        $balance = $row['saldo_awal'];
      }

      //echo $balance;die;

      foreach ($list_awal['data'] as $row) {

        if ($row['kr_coa'] == '61301' || $row['kr_coa'] == '61302' || $row['kr_coa'] == '61303' || $row['kr_coa'] == '12100' || $row['kr_coa'] == '12102' || $row['kr_coa'] == '12200' || $row['kr_coa'] == '12202' || $row['kr_coa'] == '12300' || $row['kr_coa'] == '12302') {
        } else {
          if ($row['type_cash'] == 0) {
            $balance += $row['sum_amnt'];
          } else if ($row['type_cash'] == 1) {
            $balance -= $row['sum_amnt'];
          }
        }
      }

      //echo $balance;die; 

      array_push($data, [
        '', // tanggal
        '', // tanggal
        // $typeCoa[$row['type_cash']] ?? '-', // transaksi
        '',
        'Saldo Awal', // transaksi
        // kode akun
        '', // akun
        '', // uraian
        '-', // debit
        '-', // kredit
        indonesia_currency_format($balance), // saldo
        // '-', // action
      ]);

      foreach ($list['data'] as $row) {

        if ($row['kr_coa'] == '61301' || $row['kr_coa'] == '61302' || $row['kr_coa'] == '61303' || $row['kr_coa'] == '12100' || $row['kr_coa'] == '12102' || $row['kr_coa'] == '12200' || $row['kr_coa'] == '12202' || $row['kr_coa'] == '12300' || $row['kr_coa'] == '12302') {
        } else {


          if ($row['type_cash'] == 0) {
            $balance += $row['sum_amnt'];
          } else if ($row['type_cash'] == 1) {
            $balance -= $row['sum_amnt'];
          }

          // if($row['kr_note'] == 'Utang Dagang' || $row['kr_note'] == 'Piutang Dagang'){ 
          // $row['banks'] = $row['keterangan']; 
          // }

          // if ($row['bukti'] == "Bayar Piutang") {
          // $faktur = $row['jual'];
          // $note_ac = $row['cust_name'];
          // }elseif ($row['bukti'] == "Bayar Hutang") {
          // $faktur = $row['beli'];
          // $note_ac = $row['name_eksternal'];
          // }else{
          $faktur = '';
          $note_ac = $row['k_note'] . '' . $row['users'];
          // }

          array_push($data, [
            $row['date'], // tanggal
            $row['banks'], // tanggal
            // $typeCoa[$row['type_cash']] ?? '-', // transaksi
            $row['kr_coa'],
            $row['kr_note'], // transaksi
            // kode akun 
            $note_ac, // akun
            $row['invoice'], // uraian
            $row['type_cash'] == 0 ? indonesia_currency_format($row['sum_amnt']) : '-', // debit
            $row['type_cash'] == 1 ? indonesia_currency_format($row['sum_amnt']) : '-', // kredit
            indonesia_currency_format($balance), // saldo
            // '-', // action
          ]);
        }
      }
    }

    // foreach(){



    // }

    return $balance;

    //print_r($data);

  }
  public function index() {
   
    $coa_list = $this->reportCashModel->get_coa_list();
    $coa_list = array_filter($coa_list, function ($coa) {
      return $coa['id_parent'] == 3;
    });
    $data = array(
      'get_aktiva' => $coa_list,
      // 'total_month' => $total_month
    );
    $this->template->load('maintemplate', 'report_cash/views/index_rekap', $data);
  }

  public function index_rekap() {
    $coa_list = $this->reportCashModel->get_coa_list();
    $coa_list = array_filter($coa_list, function ($coa) {
      return $coa['id_parent'] == 3;
    });

    $total_month = '
		<div class="analytics-content pull-right">
			<h5>Total Per Periode </h5>
			<h3>Rp 
				<span class="counter">
					<label id="mtd_pembelian" >0</label>
				</span> 
				<span class="tuition-fees">
				</span>
			</h2>
		</div>';
    // echo '<pre>'; var_dump($coa_list); die;
    $data = array(
      'coa_list' => $coa_list,
      'total_month' => $total_month
    );
    $this->template->load('maintemplate', 'report_cash/views/index', $data);
  }
  public function list() {
    $sess_user_id = !empty($_GET['sess_user_id']) ? $_GET['sess_user_id'] : NULL;
    $sess_token = !empty($_GET['sess_token']) ? $_GET['sess_token'] : NULL;

    $coa_id = !empty($_GET['coa_id']) ? $_GET['coa_id'] : NULL;
    $start_date = !empty($_GET['start_date']) ? $_GET['start_date'] : NULL;
    $end_date = !empty($_GET['end_date']) ? $_GET['end_date'] : NULL;

    $filter = !empty($_GET['filter']) ? $_GET['filter'] : NULL;
    $filter_month = !empty($_GET['filter_month']) ? $_GET['filter_month'] : NULL;
    $filter_year = !empty($_GET['filter_year']) ? $_GET['filter_year'] : NULL;
    $draw = $this->input->get_post('draw') != FALSE ? $this->input->get_post('draw') : 1;
    $length = $this->input->get_post('length') != FALSE ? $this->input->get_post('length') : -1;
    $start = $this->input->get_post('start') != FALSE ? $this->input->get_post('start') : 0;

    $order_fields = array('id_hp', 'no_payment', 'total', 'tgl_terbit', 'keterangan', 'id_hp', 'id_hp'); // , 'COST'

    $order = $this->input->get_post('order');
    $order_dir = !empty($order[0]['dir']) ? $order[0]['dir'] : 'desc';
    $order_column = !empty($order[0]['column']) ? $order[0]['column'] : 0;

    $params['limit']    = (int) $length;
    $params['offset']    = (int) $start;
    $params['order_column']  = $order_fields[$order_column];
    $params['order_dir']  = $order_dir;
    $params['sess_user_id']  = $sess_user_id;
    $params['sess_token']  = $sess_token;
    $params['filter']    = $filter;
    $params['filter_month']  = $filter_month;
    $params['filter_year']  = $filter_year;
    $params['searchtxt']  = $_GET['search']['value'] ?? null;

    $params['coa_id']    = $coa_id;
    $params['start_date']  = $start_date;
    $params['end_date']    = $end_date;

    // var_dump($params); die;

    // print_r($params);die;
    $priv = $this->priv->get_priv();

    if ($start_date == "2021-01-01") {

      $list_saldo = $this->reportCashModel->list_report_cash_saldo($params);

      $list = $this->reportCashModel->list_report_cash($params);
      
      $typeCoa = ['Debit', 'Kredit'];
      $data = [];
      $balance = 0.00;

      foreach ($list_saldo['data'] as $row) {

        array_push($data, [
          '', // tanggal
          '', // tanggal
          // $typeCoa[$row['type_cash']] ?? '-', // transaksi
          '',
          'Saldo Awal', // transaksi
          // kode akun
          '', // akun
          '', // uraian
          '-', // debit
          '-', // kredit
          indonesia_currency_format($row['saldo_awal']), // saldo
          // '-', // action
        ]);


        $balance = $row['saldo_awal'];
      }

      foreach ($list['data'] as $row) {
        
        if ($row['type_cash'] == 0) {
          $balance += $row['sum_amnt'];
        } else if ($row['type_cash'] == 1) {
          $balance -= $row['sum_amnt'];
        }

        $faktur = '';
        $note_ac = $row['k_note'] . '' . $row['users'];

        array_push($data, [
          $row['date'], // tanggal
          $row['banks'], // tanggal
          // $typeCoa[$row['type_cash']] ?? '-', // transaksi
          $row['kr_coa'],
          $row['kr_note'], // transaksi
          // kode akun 
          $note_ac, // akun
          $row['invoice'], // uraian
          $row['type_cash'] == 0 ? indonesia_currency_format($row['sum_amnt']) : '-', // debit
          $row['type_cash'] == 1 ? indonesia_currency_format($row['sum_amnt']) : '-', // kredit
          indonesia_currency_format($balance), // saldo
          // '-', // action
        ]);
      }
    } else {

      $params_awal['coa_id']    = $coa_id;
      $params_awal['start_date']  = "2021-01-01";
      $params_awal['end_date']  = date("Y-m-d", strtotime($start_date . "-1 days"));

      $array_date = $this->getDatesFromRange($params_awal['start_date'], $params_awal['end_date']);

      $typeCoa = ['Debit', 'Kredit'];
      $data = [];
      $balance = 0.00;

      $list_saldo = $this->reportCashModel->list_report_cash_saldo($params_awal);
      foreach ($list_saldo['data'] as $row) {

        $balance += $row['saldo_awal'];
      }

      foreach ($array_date as $array_dates) {

        $split_date = explode('-', $array_dates);
        $end_date_m = cal_days_in_month(CAL_GREGORIAN, $split_date[1], $split_date[0]);

        $params_awals['coa_id']    = $coa_id;
        $params_awals['start_date']  = $array_dates;
        $params_awals['end_date']  = $split_date[0] . '-' . $split_date[1] . '-' . $end_date_m;


        $list_awal = $this->reportCashModel->list_report_cash($params_awals);

        foreach ($list_awal['data'] as $row) {

          if ($row['kr_coa'] == '61301' || $row['kr_coa'] == '61302' || $row['kr_coa'] == '61303' || $row['kr_coa'] == '12100' || $row['kr_coa'] == '12102' || $row['kr_coa'] == '12200' || $row['kr_coa'] == '12202' || $row['kr_coa'] == '12300' || $row['kr_coa'] == '12302') {
          } else {

            if ($row['type_cash'] == 0) {
              $balance += $row['sum_amnt'];
            } else if ($row['type_cash'] == 1) {
              $balance -= $row['sum_amnt'];
            }
          }
        }
      }

      $list = $this->reportCashModel->list_report_cash($params);
      // echo "<pre>";print_r($list);echo "</pre>";die;
      array_push($data, [
        '', // tanggal
        '', // tanggal
        // $typeCoa[$row['type_cash']] ?? '-', // transaksi
        '',
        'Saldo Awal', // transaksi
        // kode akun
        '', // akun
        '', // uraian
        '-', // debit
        '-', // kredit
        indonesia_currency_format($balance), // saldo
        // '-', // action
      ]);

      foreach ($list['data'] as $row) {
        if (
          $row['kr_coa'] == '61301' || $row['kr_coa'] == '61302' || $row['kr_coa'] == '61303'
          || $row['kr_coa'] == '12100' || $row['kr_coa'] == '12102' || $row['kr_coa'] == '12200'
          || $row['kr_coa'] == '12202' || $row['kr_coa'] == '12300' || $row['kr_coa'] == '12302'
        ) {
        } else {

          if ($row['type_cash'] == 0) {
            $balance += $row['sum_amnt'];
          } else if ($row['type_cash'] == 1) {
            $balance -= $row['sum_amnt'];
          }

          $faktur = '';
          $note_ac = $row['k_note'] . '' . $row['users'];
          // }

          array_push($data, [
            $row['date'], // tanggal
            $row['banks'], // bank
            // $typeCoa[$row['type_cash']] ?? '-', // transaksi
            $row['kr_coa'],
            $row['kr_note'], // transaksi
            // kode akun 
            $note_ac, // akun
            $row['invoice'], // uraian
            $row['type_cash'] == 0 ? indonesia_currency_format($row['sum_amnt']) : '-', // debit
            $row['type_cash'] == 1 ? indonesia_currency_format($row['sum_amnt']) : '-', // kredit
            indonesia_currency_format($balance), // saldo
            // '-', // action
          ]);
        }
      }
    }

    $total_month = indonesia_currency_format($balance);
    $result = [
      'data' => $data,
      'total' => $total_month
    ];
    $this->output->set_content_type('application/json')->set_output(json_encode($result));

    return $data;
  }

  public function list_rekap() {
    $sess_user_id = !empty($_GET['sess_user_id']) ? $_GET['sess_user_id'] : NULL;
    $sess_token = !empty($_GET['sess_token']) ? $_GET['sess_token'] : NULL;

    $coa_id = !empty($_GET['coa_id']) ? $_GET['coa_id'] : NULL;
    $start_date = !empty($_GET['start_date']) ? $_GET['start_date'] : NULL;
    $end_date = !empty($_GET['end_date']) ? $_GET['end_date'] : NULL;

    $filter = !empty($_GET['filter']) ? $_GET['filter'] : NULL;
    $filter_month = !empty($_GET['filter_month']) ? $_GET['filter_month'] : NULL;
    $filter_year = !empty($_GET['filter_year']) ? $_GET['filter_year'] : NULL;
    $draw = $this->input->get_post('draw') != FALSE ? $this->input->get_post('draw') : 1;
    $length = $this->input->get_post('length') != FALSE ? $this->input->get_post('length') : -1;
    $start = $this->input->get_post('start') != FALSE ? $this->input->get_post('start') : 0;

    $order_fields = array('id_hp', 'no_payment', 'total', 'tgl_terbit', 'keterangan', 'id_hp', 'id_hp'); // , 'COST'

    $order = $this->input->get_post('order');
    $order_dir = !empty($order[0]['dir']) ? $order[0]['dir'] : 'desc';
    $order_column = !empty($order[0]['column']) ? $order[0]['column'] : 0;

    $params['limit']    = (int) $length;
    $params['offset']    = (int) $start;
    $params['order_column']  = $order_fields[$order_column];
    $params['order_dir']  = $order_dir;
    $params['sess_user_id']  = $sess_user_id;
    $params['sess_token']  = $sess_token;
    $params['filter']    = $filter;
    $params['filter_month']  = $filter_month;
    $params['filter_year']  = $filter_year;
    $params['searchtxt']  = $_GET['search']['value'] ?? null;

    $params['coa_id']    = $coa_id;
    $params['start_date']  = $start_date;
    $params['end_date']    = $end_date;

    // var_dump($params); die;

    // print_r($params);die;
    $priv = $this->priv->get_priv();

    if ($start_date == "2021-01-01") {

      $list_saldo = $this->reportCashModel->list_report_cash_saldo_rekap($params);

      $list = $this->reportCashModel->list_report_cash_rekap($params);
      $typeCoa = ['Debit', 'Kredit'];
      $data = [];
      $balance = 0.00;

      foreach ($list_saldo['data'] as $row) {

        array_push($data, [
          '', // tanggal
          '', // tanggal
          // $typeCoa[$row['type_cash']] ?? '-', // transaksi
          '',
          'Saldo Awal', // transaksi
          // kode akun
          '', // akun
          '', // uraian
          '-', // debit
          '-', // kredit
          indonesia_currency_format($row['saldo_awal']), // saldo
          // '-', // action
        ]);


        $balance = $row['saldo_awal'];
      }

      foreach ($list['data'] as $row) {

        if ($row['type_cash'] == 0) {
          $balance += $row['sum_amnt'];
        } else if ($row['type_cash'] == 1) {
          $balance -= $row['sum_amnt'];
        }

        $faktur = '';
        $note_ac = $row['k_note'] . '' . $row['users'];

        array_push($data, [
          $row['date'], // tanggal
          $row['banks'], // tanggal
          // $typeCoa[$row['type_cash']] ?? '-', // transaksi
          $row['kr_coa'],
          $row['kr_note'], // transaksi
          // kode akun 
          $note_ac, // akun
          $row['invoice'], // uraian
          $row['type_cash'] == 0 ? indonesia_currency_format($row['sum_amnt']) : '-', // debit
          $row['type_cash'] == 1 ? indonesia_currency_format($row['sum_amnt']) : '-', // kredit
          indonesia_currency_format($balance), // saldo
          // '-', // action
        ]);
      }
    } else {

      $params_awal['coa_id']    = $coa_id;
      $params_awal['start_date']  = "2021-01-01";
      $params_awal['end_date']  = date("Y-m-d", strtotime($start_date . "-1 days"));

      $array_date = $this->getDatesFromRange($params_awal['start_date'], $params_awal['end_date']);

      $typeCoa = ['Debit', 'Kredit'];
      $data = [];
      $balance = 0.00;

      $list_saldo = $this->reportCashModel->list_report_cash_saldo_rekap($params_awal);
      foreach ($list_saldo['data'] as $row) {

        $balance += $row['saldo_awal'];
      }

      foreach ($array_date as $array_dates) {

        $split_date = explode('-', $array_dates);
        $end_date_m = cal_days_in_month(CAL_GREGORIAN, $split_date[1], $split_date[0]);

        $params_awals['coa_id']    = $coa_id;
        $params_awals['start_date']  = $array_dates;
        $params_awals['end_date']  = $split_date[0] . '-' . $split_date[1] . '-' . $end_date_m;


        $list_awal = $this->reportCashModel->list_report_cash_rekap($params_awals);

        foreach ($list_awal['data'] as $row) {

          if (
            $row['kr_coa'] == '61301'
            || $row['kr_coa'] == '61302'
            || $row['kr_coa'] == '61303'
            || $row['kr_coa'] == '12100'
            || $row['kr_coa'] == '12102'
            || $row['kr_coa'] == '12200'
            || $row['kr_coa'] == '12202'
            || $row['kr_coa'] == '12300'
            || $row['kr_coa'] == '12302'
          ) {
          } else {

            if ($row['type_cash'] == 0) {
              $balance += $row['sum_amnt'];
            } else if ($row['type_cash'] == 1) {
              $balance -= $row['sum_amnt'];
            }
          }
        }
      }

      $list = $this->reportCashModel->list_report_cash_rekap($params);

      array_push($data, [
        '', // tanggal
        '', // tanggal
        // $typeCoa[$row['type_cash']] ?? '-', // transaksi
        '',
        'Saldo Awal', // transaksi
        // kode akun
        '', // akun
        '', // uraian
        '-', // debit
        '-', // kredit
        indonesia_currency_format($balance), // saldo
        // '-', // action
      ]);

      foreach ($list['data'] as $row) {

        if (
          $row['kr_coa'] == '61301' || $row['kr_coa'] == '61302' || $row['kr_coa'] == '61303'
          || $row['kr_coa'] == '12100' || $row['kr_coa'] == '12102' || $row['kr_coa'] == '12200'
          || $row['kr_coa'] == '12202' || $row['kr_coa'] == '12300' || $row['kr_coa'] == '12302'
        ) {
        } else {

          if ($row['type_cash'] == 0) {
            $balance += $row['sum_amnt'];
          } else if ($row['type_cash'] == 1) {
            $balance -= $row['sum_amnt'];
          }

          $faktur = '';
          $note_ac = $row['k_note'] . '' . $row['users'];
          // }

          array_push($data, [
            $row['date'], // tanggal
            $row['banks'], // tanggal
            // $typeCoa[$row['type_cash']] ?? '-', // transaksi
            $row['kr_coa'],
            $row['kr_note'], // transaksi
            // kode akun 
            $note_ac, // akun
            $row['invoice'], // uraian
            $row['type_cash'] == 0 ? indonesia_currency_format($row['sum_amnt']) : '-', // debit
            $row['type_cash'] == 1 ? indonesia_currency_format($row['sum_amnt']) : '-', // kredit
            indonesia_currency_format($balance), // saldo
            // '-', // action
          ]);
        }
      }
    }

    $result = [
      'data' => $data
    ];
    $this->output->set_content_type('application/json')->set_output(json_encode($result));

    return $data;
  }
  public function excel() {
    $params = array(
      'coa_id' => anti_sql_injection($this->input->post('coa_id')),
      'end_date' => anti_sql_injection($this->input->post('end_date')),
      'start_date' => anti_sql_injection($this->input->post('start_date')),
    );

    //print_r($params);die;

    $this->load->library('excel');

    $objPHPExcel = new PHPExcel();

    $objPHPExcel->setActiveSheetIndex(0)
      ->setCellValue('A1', 'Tanggal')
      ->setCellValue('B1', 'Bank')
      ->setCellValue('C1', 'Kode')
      ->setCellValue('D1', 'Akun')
      ->setCellValue('E1', 'Uraian')
      ->setCellValue('F1', 'No Faktur')
      ->setCellValue('G1', 'Debit')
      ->setCellValue('H1', 'Kredit')
      ->setCellValue('I1', 'Saldo');

    $start = 2;
    foreach ($this->list() as $data) {
      $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A' . $start, $data[0]) // tanggal
        ->setCellValue('B' . $start, $data[1]) // transaksi
        ->setCellValue('C' . $start, $data[2]) // kode akun
        ->setCellValue('D' . $start, $data[3]) // akun
        ->setCellValue('E' . $start, $data[4]) // uraian
        ->setCellValue('F' . $start, $data[5]) // debit
        ->setCellValue('G' . $start, str_replace(',', '.', str_replace('.', '', $data[6])))
        ->setCellValue('H' . $start, str_replace(',', '.', str_replace('.', '', $data[7])))
        ->setCellValue('I' . $start, str_replace(',', '.', str_replace('.', '', $data[8])));

      $start++;
    }

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename=Report Cash Tanggal : ' . $params['start_date'] . ' - ' . $params['end_date'] . '.xls');
    header('Cache-Control: max-age=0');
    // If you’re serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');
    // If you’re serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
    unset($objPHPExcel);
  }
}
