<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Return_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}


	public function list_penjualan($params = array()) {

		$query = 	'
				SELECT COUNT(*) AS jumlah 
				 FROM `t_retur` a
					JOIN `t_penjualan` b ON a.id_penjualan = b.`id_penjualan`
					LEFT JOIN `u_user` c ON a.`pic` = c.id
					LEFT JOIN `u_user` d ON a.`approval` = d.id
					LEFT JOIN `t_customer` e ON b.id_customer = e.`id_t_cust`
					where 1 = 1  AND ( 
						c.nama LIKE "%' . $params['searchtxt'] . '%" OR
						e.cust_name LIKE "%' . $params['searchtxt'] . '%"  OR
						b.no_penjualan LIKE "%' . $params['searchtxt'] . '%"
					)
			';

		$query2 = 	'
				SELECT z.*, rank() over ( ORDER BY date DESC) AS Rangking from ( 
					SELECT a.id_retur,a.date,a.status AS status_retur,a.approval_date,b.*, c.nama AS pic, d.nama AS approval, e.`cust_name` FROM `t_retur` a
					JOIN `t_penjualan` b ON a.id_penjualan = b.`id_penjualan`
					LEFT JOIN `u_user` c ON a.`pic` = c.id
					LEFT JOIN `u_user` d ON a.`approval` = d.id
					LEFT JOIN `t_customer` e ON b.id_customer = e.`id_t_cust`
					where 1 = 1  AND ( 
					c.nama LIKE "%' . $params['searchtxt'] . '%" OR
					e.cust_name LIKE "%' . $params['searchtxt'] . '%"  OR
					b.no_penjualan LIKE "%' . $params['searchtxt'] . '%"
					)  order by date DESC) z
				ORDER BY date DESC
				
				LIMIT ' . $params['limit'] . ' 
				OFFSET ' . $params['offset'] . ' 
			';

		//echo $query2;die;

		$out		= array();
		$querys		= $this->db->query($query);
		$result = $querys->row();

		$total_filtered = $result->jumlah;
		$total 			= $result->jumlah;

		if (($params['offset'] + 10) > $total_filtered) {
			$limit_data = $total_filtered - $params['offset'];
		} else {
			$limit_data = $params['limit'];
		}



		//echo $query;die;
		//echo $query;die;
		$query2s		= $this->db->query($query2);
		$result2 = $query2s->result_array();
		$return = array(
			'data' => $result2,
			'total_filtered' => $total_filtered,
			'total' => $total,
		);
		return $return;
	}


	public function get_customer($params = array()) {

		//$query = $this->db->get_where('t_customer', array('region' => $params['region']));

		$this->db->select('*')
			->from('t_customer')
			# ->where(array('region' => $params['region']))
			->order_by('cust_name', 'asc');
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_report_penjualan($params = array()) {

		if ($params['principal'] == '0') {


			$query = "
				SELECT COUNT(*) AS jumlah from (
				SELECT a.*
				FROM t_penjualan a
				JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
				JOIN m_material c ON b.`id_material`=c.`id_mat`
				JOIN t_invoice_penjualan e ON a.`id_penjualan`=e.`id_penjualan`
				WHERE `tanggal_invoice` BETWEEN '" . $params['tgl_awal'] . "' and '" . $params['tgl_akhir'] . "'
				) op
				";

			$query2 = "
				SELECT g.region,
				b.id_material,
				a.no_penjualan AS no_invoice,
				e.tanggal_invoice,
				f.code_cust,
				f.cust_name,
				d.uom_symbol,
				b.diskon,
				b.unit_price-(b.unit_price/11)-(b.unit_price*(b.diskon/100)) AS harga_jual_perkemasan,
				(b.price-(b.price/11))+(b.price/11) AS tot_ppn,
				(b.unit_price*(b.diskon/100))+(b.price/11) AS tot_ppn_diskon,
				b.price-(b.price/11)-(b.price*(b.diskon/100)) total,
				b.price/11 AS ppn,
				c.stock_code AS Kode_produk, 
				c.stock_name AS Produk,
				CONCAT(c.base_qty,' ',d.uom_name) AS kemasan,
				h.name_eksternal,
				f.city,
				i.harga_pokok,
				b.qty AS Sum_of_pcs,
				b.qty*(c.base_qty/convertion) AS Sum_of_ltkg,
				a.total_amount-(a.total_amount/11) AS Sum_of_total,
				a.total_amount/11 AS Sum_of_ppn,
				a.total_amount AS Sum_of_TotPPN,
				cc.type_material_name,
				c.pajak AS pajak_pst,
				b.price AS non_ppn_price,
				i.harga_pokok/(dd.convertion/c.base_qty)AS HargaKemasan,
				AVG(b.unit_price*b.qty)/b.qty*(convertion/c.base_qty)AS Average_Hargalt,
				(b.unit_price-(b.unit_price/11))-(i.harga_pokok/(dd.convertion/c.base_qty)) AS laba_rugi_kemasan,
				(b.unit_price-(b.unit_price*(b.diskon/100))-(b.unit_price/11))-(i.harga_pokok/(dd.convertion/c.base_qty)) AS laba_rugi_kemasan_diskon,
				((b.unit_price-(b.unit_price/11))-(i.harga_pokok/(dd.convertion/c.base_qty)))*b.qty AS laba_rugi_produk,
				((b.unit_price-(b.unit_price*(b.diskon/100))-(b.unit_price/11))-(i.harga_pokok/(dd.convertion/c.base_qty)))*b.qty AS laba_rugi_produk_diskon,
				(b.unit_price-(b.unit_price/11))*(dd.convertion/c.base_qty) AS harga_jual_produk,
				(b.unit_price-(b.unit_price*(b.diskon/100))-(b.unit_price/11))*(dd.convertion/c.base_qty) AS harga_jual_produk_diskon,
				(((b.unit_price-(b.unit_price/11))-(i.harga_pokok/(dd.convertion/c.base_qty)))/(b.unit_price-(b.unit_price/11)))*100 AS persen,
				(((b.unit_price-(b.unit_price*(b.diskon/100))-(b.unit_price/11))-(i.harga_pokok/(dd.convertion/c.base_qty)))/(b.unit_price-(b.unit_price/11)))*100 AS persen_diskon,
				i.harga_pokok*(b.qty*(c.base_qty/convertion)) AS hpp,
				WEEK(a.date_penjualan) AS minggu
				FROM t_penjualan a
				JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
				JOIN m_material c ON b.`id_material`=c.`id_mat`
				JOIN m_type_material cc ON c.type=cc.`id_type_material`
				JOIN m_uom d ON c.unit_terkecil=d.id_uom
				JOIN t_uom_convert dd ON d.`id_uom`=dd.`id_uom`
				JOIN t_invoice_penjualan e ON a.`id_penjualan`=e.`id_penjualan`
				JOIN t_customer f ON f.`id_t_cust`=a.`id_customer`
				JOIN t_region g ON g.`id_t_region`=f.`region`
				JOIN t_eksternal h ON c.`dist_id`=h.id
				JOIN (
					SELECT 
					a.`id_material`,SUM(b.`buy_price`*a.`qty_big`)/SUM(a.`qty_big`) harga_pokok 
					FROM t_detail_material a
					JOIN m_material b ON a.id_material=b.`id_mat` 
					WHERE a.`date_insert` < '" . $params['tgl_akhir'] . "'
					GROUP BY a.`id_material`
				)i ON c.id_mat=i.id_material
				WHERE `tanggal_invoice` BETWEEN '" . $params['tgl_awal'] . "' and '" . $params['tgl_akhir'] . "'
				AND (
					c.stock_code like '%".$params['searchtxt']."%'
					OR c.stock_name like '%".$params['searchtxt']."%'
					OR c.base_qty like '%".$params['searchtxt']."%'
					OR d.uom_name like '%".$params['searchtxt']."%'
					OR f.cust_name like '%".$params['searchtxt']."%' 
					)
				GROUP BY b.`id_dp`

					LIMIT " . $params['limit'] . " 
						OFFSET " . $params['offset'] . " 
				";
		} else {

			$query = "
				SELECT COUNT(*) AS jumlah 
				FROM t_penjualan a
				JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
				JOIN m_material c ON b.`id_material`=c.`id_mat`
				JOIN t_invoice_penjualan e ON a.`id_penjualan`=e.`id_penjualan`
				WHERE `tanggal_invoice` BETWEEN '" . $params['tgl_awal'] . "' and '" . $params['tgl_akhir'] . "'
				AND c.dist_id = " . $params['principal'] . "
				
				";

			$query2 = "
				SELECT g.region,
				a.no_penjualan AS no_invoice,
				e.tanggal_invoice,
				f.code_cust,
				f.cust_name,
				d.uom_symbol,
				b.diskon,
				b.unit_price-(b.unit_price/11)-(b.unit_price*(b.diskon/100)) AS harga_jual_perkemasan,
				(b.price-(b.price/11))+(b.price/11) AS tot_ppn,
				(b.unit_price*(b.diskon/100))+(b.price/11) AS tot_ppn_diskon,
				b.price-(b.price/11)-(b.price*(b.diskon/100)) total,
				b.price/11 AS ppn,
				c.stock_code AS Kode_produk, 
				c.stock_name AS Produk,
				CONCAT(c.base_qty,' ',d.uom_name) AS kemasan,
				h.name_eksternal,
				f.city,
				i.harga_pokok,
				b.qty AS Sum_of_pcs,
				b.qty*(c.base_qty/convertion) AS Sum_of_ltkg,
				a.total_amount-(a.total_amount/11) AS Sum_of_total,
				a.total_amount/11 AS Sum_of_ppn,
				a.total_amount AS Sum_of_TotPPN,
				cc.type_material_name,
				c.pajak AS pajak_pst,
				b.price AS non_ppn_price,
				i.harga_pokok/(dd.convertion/c.base_qty)AS HargaKemasan,
				AVG(b.unit_price*b.qty)/b.qty*(convertion/c.base_qty)AS Average_Hargalt,
				(b.unit_price-(b.unit_price/11))-(i.harga_pokok/(dd.convertion/c.base_qty)) AS laba_rugi_kemasan,
				(b.unit_price-(b.unit_price*(b.diskon/100))-(b.unit_price/11))-(i.harga_pokok/(dd.convertion/c.base_qty)) AS laba_rugi_kemasan_diskon,
				((b.unit_price-(b.unit_price/11))-(i.harga_pokok/(dd.convertion/c.base_qty)))*b.qty AS laba_rugi_produk,
				((b.unit_price-(b.unit_price*(b.diskon/100))-(b.unit_price/11))-(i.harga_pokok/(dd.convertion/c.base_qty)))*b.qty AS laba_rugi_produk_diskon,
				(b.unit_price-(b.unit_price/11))*(dd.convertion/c.base_qty) AS harga_jual_produk,
				(b.unit_price-(b.unit_price*(b.diskon/100))-(b.unit_price/11))*(dd.convertion/c.base_qty) AS harga_jual_produk_diskon,
				(((b.unit_price-(b.unit_price/11))-(i.harga_pokok/(dd.convertion/c.base_qty)))/(b.unit_price-(b.unit_price/11)))*100 AS persen,
				(((b.unit_price-(b.unit_price*(b.diskon/100))-(b.unit_price/11))-(i.harga_pokok/(dd.convertion/c.base_qty)))/(b.unit_price-(b.unit_price/11)))*100 AS persen_diskon,
				i.harga_pokok*(b.qty*(c.base_qty/convertion)) AS hpp,
				WEEK(a.date_penjualan) AS minggu
				FROM t_penjualan a
				JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
				JOIN m_material c ON b.`id_material`=c.`id_mat`
				JOIN m_type_material cc ON c.type=cc.`id_type_material`
				JOIN m_uom d ON c.unit_terkecil=d.id_uom
				JOIN t_uom_convert dd ON d.`id_uom`=dd.`id_uom`
				JOIN t_invoice_penjualan e ON a.`id_penjualan`=e.`id_penjualan`
				JOIN t_customer f ON f.`id_t_cust`=a.`id_customer`
				JOIN t_region g ON g.`id_t_region`=f.`region`
				JOIN t_eksternal h ON c.`dist_id`=h.id
				JOIN (
					SELECT  
					a.`id_material`,SUM(b.`buy_price`*a.`qty_big`)/SUM(a.`qty_big`) harga_pokok 
					FROM t_detail_material a
					JOIN m_material b ON a.id_material=b.`id_mat` 
					WHERE a.`date_insert` < '" . $params['tgl_akhir'] . "'
					GROUP BY a.`id_material`
				)i ON c.id_mat=i.id_material
				WHERE `tanggal_invoice` BETWEEN '" . $params['tgl_awal'] . "' and '" . $params['tgl_akhir'] . "'
				AND c.dist_id = " . $params['principal'] . "
				AND (
					c.stock_code like '%".$params['searchtxt']."%'
					OR c.stock_name like '%".$params['searchtxt']."%'
					OR c.base_qty like '%".$params['searchtxt']."%'
					OR d.uom_name like '%".$params['searchtxt']."%'
					OR f.cust_name like '%".$params['searchtxt']."%' 
					)
				GROUP BY b.`id_dp`

			LIMIT " . $params['limit'] . " 
						OFFSET " . $params['offset'] . " 
				";
		}
		// echo "<pre>";print_r($query2);die;

		$out		= array();
		$querys		= $this->db->query($query);
		$result = $querys->row();

		$total_filtered = $result->jumlah;
		$total 			= $result->jumlah;

		if (($params['offset'] + 10) > $total_filtered) {
			$limit_data = $total_filtered - $params['offset'];
		} else {
			$limit_data = $params['limit'];
		}



		//echo $query;die;
		//echo $query;die;
		$query2s		= $this->db->query($query2);
		$result2 = $query2s->result_array();
		$return = array(
			'data' => $result2,
			'total_filtered' => $total_filtered,
			'total' => $total,
		);
		return $return;
	}

	public function get_report_penjualan_export($params = array()) {


		// $sql = "

		// SELECT g.`region`,
		// e.`no_invoice`,
		// e.tanggal_invoice,
		// DATE_FORMAT(e.tanggal_invoice,'%Y') AS tahun,
		// DATE_FORMAT(e.tanggal_invoice,'%M') AS bulan,
		// f.`code_cust`,
		// f.`cust_name`,
		// d.uom_symbol,
		// a.keterangan,
		// b.`unit_price`-(b.`unit_price`/11) AS harga_jual_perkemasan,
		// (b.price-(b.`price`/11))+(b.`price`/11) AS tot_ppn,
		// b.price-(b.`price`/11) total,
		// b.`price`/11 AS ppn,
		// c.`stock_code` AS Kode_produk, 
		// c.`stock_name` AS Produk,
		// CONCAT(c.base_qty,' ',d.uom_name) AS kemasan,
		// h.`name_eksternal`,
		// f.`city`,
		// i.harga_pokok,
		// b.qty AS Sum_of_pcs,
		// b.qty*(c.base_qty/1000) AS Sum_of_ltkg,
		// a.total_amount-(a.total_amount/11) AS Sum_of_total,
		// a.total_amount/11 AS Sum_of_ppn,
		// a.total_amount AS Sum_of_TotPPN,
		// cc.`type_material_name`,
		// i.harga_pokok/(dd.convertion/c.base_qty)AS HargaKemasan,
		// (b.`unit_price`-(b.`unit_price`/11))-(i.harga_pokok/(dd.convertion/c.base_qty)) AS laba_rugi_kemasan,
		// ((b.`unit_price`-(b.`unit_price`/11))-(i.harga_pokok/(dd.convertion/c.base_qty)))*b.qty AS laba_rugi_produk,
		// (b.`unit_price`-(b.`unit_price`/11))*(dd.convertion/c.base_qty) AS harga_jual_produk,
		// (((b.`unit_price`-(b.`unit_price`/11))-(i.harga_pokok/(dd.convertion/c.base_qty)))/(b.`unit_price`-(b.`unit_price`/11)))*100 AS persen,
		// i.harga_pokok*(b.qty*(c.base_qty/1000)) AS hpp,
		// WEEK(a.date_penjualan) AS minggu
		// FROM t_penjualan a
		// JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
		// JOIN m_material c ON b.`id_material`=c.`id_mat`
		// JOIN m_type_material cc ON c.type=cc.`id_type_material`
		// JOIN m_uom d ON c.unit_terkecil=d.id_uom
		// JOIN t_uom_convert dd ON d.`id_uom`=dd.`id_uom`
		// JOIN t_invoice_penjualan e ON a.`id_penjualan`=e.`id_penjualan`
		// JOIN t_customer f ON f.`id_t_cust`=a.`id_customer`
		// JOIN t_region g ON g.`id_t_region`=f.`region`
		// JOIN t_eksternal h ON c.`dist_id`=h.id
		// JOIN (SELECT  a.`id_material`,SUM(a.`buy_price`*a.`qty_big`)/SUM(a.`qty_big`) harga_pokok FROM t_detail_material a
		// WHERE a.`date_insert` < '".$params['tgl_akhir']."'
		// GROUP BY a.`id_material`)i ON c.id_mat=i.id_material
		// WHERE `tanggal_invoice` BETWEEN '".$params['tgl_awal']."' and '".$params['tgl_akhir']."'
		// ";

		// $sql = "
		// select 
		// DATE_FORMAT(a.date_penjualan,'%Y') AS tahun,
		// DATE_FORMAT(a.date_penjualan,'%M') AS bulan,a.* from t_query_report a
		// where `date_penjualan` BETWEEN '".$params['tgl_awal']."' and '".$params['tgl_akhir']."'
		// ";


		$sql = "
		SELECT g.`region`,
			a.no_penjualan as no_invoice,
		e.tanggal_invoice,
		DATE_FORMAT(e.tanggal_invoice,'%Y') AS tahun,
		DATE_FORMAT(e.tanggal_invoice,'%M') AS bulan,
		f.`code_cust`,
		f.`cust_name`,
		d.uom_symbol,
		a.keterangan,
		b.`unit_price`-(b.`unit_price`/11) AS harga_jual_perkemasan,
		(b.price-(b.`price`/11))+(b.`price`/11) AS tot_ppn,
		b.price-(b.`price`/11) total,
		b.`price`/11 AS ppn,
		c.`stock_code` AS kode_produk, 
		c.`stock_name` AS produk,
		CONCAT(c.base_qty,' ',d.uom_name) AS kemasan,
		h.`name_eksternal`,
		f.`city`,
		i.harga_pokok,
		b.qty AS Sum_of_pcs,
		b.qty*(c.base_qty/convertion) AS Sum_of_ltkg,
		a.total_amount-(a.total_amount/11) AS Sum_of_total,
		a.total_amount/11 AS Sum_of_ppn,
		a.total_amount AS Sum_of_TotPPN,
		cc.`type_material_name`,
		c.pajak as pajak_pst,
		b.price as non_ppn_price,
		i.harga_pokok/(dd.convertion/c.base_qty)AS HargaKemasan,
		(b.`unit_price`-(b.`unit_price`/11))-(i.harga_pokok/(dd.convertion/c.base_qty)) AS laba_rugi_kemasan,
		((b.`unit_price`-(b.`unit_price`/11))-(i.harga_pokok/(dd.convertion/c.base_qty)))*b.qty AS laba_rugi_produk,
		(b.`unit_price`-(b.`unit_price`/11))*(dd.convertion/c.base_qty) AS harga_jual_produk,
		(((b.`unit_price`-(b.`unit_price`/11))-(i.harga_pokok/(dd.convertion/c.base_qty)))/(b.`unit_price`-(b.`unit_price`/11)))*100 AS persen,
		 i.harga_pokok*(b.qty*(c.base_qty/convertion)) AS hpp,
		WEEK(a.date_penjualan) AS minggu
		FROM t_penjualan a
		JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
		JOIN m_material c ON b.`id_material`=c.`id_mat`
		JOIN m_type_material cc ON c.type=cc.`id_type_material`
		JOIN m_uom d ON c.unit_terkecil=d.id_uom
		JOIN t_uom_convert dd ON d.`id_uom`=dd.`id_uom`
		JOIN t_invoice_penjualan e ON a.`id_penjualan`=e.`id_penjualan`
		JOIN t_customer f ON f.`id_t_cust`=a.`id_customer`
		JOIN t_region g ON g.`id_t_region`=f.`region`
		JOIN t_eksternal h ON c.`dist_id`=h.id
		JOIN (SELECT  a.`id_material`,SUM(a.`buy_price`*a.`qty_big`)/SUM(a.`qty_big`) harga_pokok FROM t_detail_material a
		WHERE a.`date_insert` < '" . $params['tgl_akhir'] . "'
		GROUP BY a.`id_material`)i ON c.id_mat=i.id_material
		WHERE `tanggal_invoice` BETWEEN '" . $params['tgl_awal'] . "' and '" . $params['tgl_akhir'] . "'
		
		";

		//echo $sql;die;
		$query 	= $this->db->query($sql);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_report_penjualan_prin($params = array()) {

		$sql = "
		SELECT g.`region`,
			a.no_penjualan as no_invoice,
		e.tanggal_invoice,
		DATE_FORMAT(e.tanggal_invoice,'%Y') AS tahun,
		DATE_FORMAT(e.tanggal_invoice,'%M') AS bulan,
		f.`code_cust`,
		f.`cust_name`,
		d.uom_symbol,
		a.keterangan,
		b.`unit_price`-(b.`unit_price`/11) AS harga_jual_perkemasan,
		(b.price-(b.`price`/11))+(b.`price`/11) AS tot_ppn,
		b.price-(b.`price`/11) total,
		b.`price`/11 AS ppn,
		c.`stock_code` AS kode_produk, 
		c.`stock_name` AS produk,
		CONCAT(c.base_qty,' ',d.uom_name) AS kemasan,
		h.`name_eksternal`,
		f.`city`,
		i.harga_pokok,
		b.qty AS Sum_of_pcs,
		b.qty*(c.base_qty/convertion) AS Sum_of_ltkg,
		a.total_amount-(a.total_amount/11) AS Sum_of_total,
		a.total_amount/11 AS Sum_of_ppn,
		a.total_amount AS Sum_of_TotPPN,
		cc.`type_material_name`,
		c.pajak as pajak_pst,
		b.price as non_ppn_price,
		i.harga_pokok/(dd.convertion/c.base_qty)AS HargaKemasan,
		(b.`unit_price`-(b.`unit_price`/11))-(i.harga_pokok/(dd.convertion/c.base_qty)) AS laba_rugi_kemasan,
		((b.`unit_price`-(b.`unit_price`/11))-(i.harga_pokok/(dd.convertion/c.base_qty)))*b.qty AS laba_rugi_produk,
		(b.`unit_price`-(b.`unit_price`/11))*(dd.convertion/c.base_qty) AS harga_jual_produk,
		(((b.`unit_price`-(b.`unit_price`/11))-(i.harga_pokok/(dd.convertion/c.base_qty)))/(b.`unit_price`-(b.`unit_price`/11)))*100 AS persen,
		 i.harga_pokok*(b.qty*(c.base_qty/convertion)) AS hpp,
		WEEK(a.date_penjualan) AS minggu
		FROM t_penjualan a
		JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
		JOIN m_material c ON b.`id_material`=c.`id_mat`
		JOIN m_type_material cc ON c.type=cc.`id_type_material`
		JOIN m_uom d ON c.unit_terkecil=d.id_uom
		JOIN t_uom_convert dd ON d.`id_uom`=dd.`id_uom`
		JOIN t_invoice_penjualan e ON a.`id_penjualan`=e.`id_penjualan`
		JOIN t_customer f ON f.`id_t_cust`=a.`id_customer`
		JOIN t_region g ON g.`id_t_region`=f.`region`
		JOIN t_eksternal h ON c.`dist_id`=h.id
		JOIN (SELECT  a.`id_material`,SUM(a.`buy_price`*a.`qty_big`)/SUM(a.`qty_big`) harga_pokok FROM t_detail_material a
		WHERE a.`date_insert` < '" . $params['tgl_akhir'] . "'
		GROUP BY a.`id_material`)i ON c.id_mat=i.id_material
		WHERE `tanggal_invoice` BETWEEN '" . $params['tgl_awal'] . "' and '" . $params['tgl_akhir'] . "'
		AND c.dist_id = " . $params['principal'] . "
		";

		//echo $sql;die;

		$query 	= $this->db->query($sql);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_principal($params = array()) {

		//$query = $this->db->get_where('t_customer', array('region' => $params['region']));

		$this->db->select('*')
			->from('t_eksternal')
			# ->where(array('region' => $params['region']))
			->order_by('name_eksternal', 'asc');
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_price_mat($params = array()) {

		// $query = $this->db->get_where('t_po_mat', array('id_po' => $params['kode']));
		// $query = $this->db->order_by('id_t_ps', 'desc');

		$this->db->select('*')
			->from('t_po_mat')
			->where(array('id_material' => $params['kode']))
			->order_by('id_t_ps', 'desc');
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_penjualan_byid($params = array()) {

		$this->db->select('t_penjualan.*, t_customer.*')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_penjualan')
			->join('t_customer', 't_penjualan.id_customer = t_customer.id_t_cust')
			->where(array('id_penjualan' => $params['id_penjualan']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}


	public function get_item_byprin($params = array()) {
		$this->db->select('m_material.*, m_uom.uom_symbol')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('m_material')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('dist_id' => $params['id_prin']));
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_retur_id($params = array()) {
		$this->db->select('t_retur.date,t_retur.id_penjualan,t_retur.keterangan, t_detail_retur.*')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_retur')
			->join('t_detail_retur', 't_retur.id_retur = t_detail_retur.id_retur')
			->where(array('t_retur.id_retur' => $params['id']));
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_item_byprin_all() {
		$this->db->select('m_material.*, m_uom.uom_symbol')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('m_material')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('status' => 0))
			->order_by('stock_name', 'asc');
		//->where(array('dist_id' => $params['id_prin']));
		// ->where('m_material.dist_id != ',$params['id_prin'],FALSE);
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_penjualan_all() {
		$this->db->select('t_penjualan.*, t_customer.cust_name')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_penjualan')
			->join('t_customer', 't_penjualan.id_customer = t_customer.id_t_cust')
			->where(array('status' => 0))
			->order_by('date_penjualan', 'desc');
		//->where(array('dist_id' => $params['id_prin']));
		// ->where('m_material.dist_id != ',$params['id_prin'],FALSE);
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_kode($params = array()) {

		$query =  $this->db->select_max('seq_n', 'max');
		$query = $this->db->get('t_penjualan');

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_penjualan($params = array()) {

		// $query = $this->db->get_where('t_purchase_order', array('id_po' => $params['id']));
		$this->db->select('t_penjualan.*, t_customer.*')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_penjualan')
			->join('t_customer', 't_penjualan.id_customer = t_customer.id_t_cust')
			->where(array('id_penjualan' => $params['id']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_penjualan_detail_update($params = array()) {

		//$query = $this->db->get_where('d_penjualan', array('id_penjualan' => $params['id_penjualan']));
		$this->db->select('d_penjualan.*, m_material.stock_name, m_material.base_qty, m_material.stock_name, m_uom.uom_symbol,t_detail_retur.qty as qty_retur,t_detail_retur.qty_satuan as qty_retur_sat,t_detail_retur.qty_box as qty_box_retur,t_detail_retur.amount ')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('d_penjualan')
			->join('m_material', 'd_penjualan.id_material = m_material.id_mat')
			->join('t_detail_retur', 'd_penjualan.id_dp = t_detail_retur.id_detail_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('t_detail_retur.id_retur' => $params['id_penjualan']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_penjualan_detail($params = array()) {

		//$query = $this->db->get_where('d_penjualan', array('id_penjualan' => $params['id_penjualan']));
		$this->db->select('d_penjualan.*, m_material.stock_name, m_material.base_qty, m_material.stock_name, m_uom.uom_symbol')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('d_penjualan')
			->join('m_material', 'd_penjualan.id_material = m_material.id_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('id_penjualan' => $params['id_penjualan']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_po_detail_full($params = array()) {

		//$query = $this->db->get_where('t_po_mat', array('id_po' => $params['id']));
		$this->db->select('t_po_mat.*,t_po_mat.qty as qty_order, m_material.*, m_uom.*')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_po_mat')
			->join('m_material', 't_po_mat.id_material = m_material.id_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('id_po' => $params['id']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function check_principal($params = array()) {

		$query = $this->db->get_where('t_eksternal', array('kode_eksternal' => $params['kode']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_returm($data) {

		$this->db->insert('t_retur', $data);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_mutasi($data) {

		$data_insert_mutasi = array(
			'id_stock_awal'  => $data['id_material'],
			'id_stock_akhir' => $data['id_material'],
			'date_mutasi'    => date('Y-m-d H:i:s'),
			'amount_mutasi'  => $data['qty'],
			'type_mutasi'    => 0,
			'user_id'    => $this->session->userdata['logged_in']['user_id'],
		);

		//	print_r($datas);die;

		$this->db->insert('t_mutasi', $data_insert_mutasi);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_mutasi_out($data) {

		$data_insert_mutasi = array(
			'id_stock_awal'  => $data['id_material'],
			'id_stock_akhir' => $data['id_material'],
			'date_mutasi'    => date('Y-m-d H:i:s'),
			'amount_mutasi'  => $data['qty'],
			'type_mutasi'    => 1,
			'user_id'    => $this->session->userdata['logged_in']['user_id'],
		);

		//	print_r($datas);die;

		$this->db->insert('t_mutasi', $data_insert_mutasi);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_detail_retur($data) {

		//	print_r($datas);die;

		$this->db->insert('t_detail_retur', $data);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_qty($data) {
		$sql 	= 'update m_material set qty = qty - ' . $data['qty'] . ' 
		where id_mat = ' . $data['id_material'] . ' ';

		// $datas = array(
		// 'qty' => $data['qty_stock']
		// );

		// //	print_r($datas);die;
		// $this->db->where('id_mat',$data['id_material'], false);
		// $this->db->update('m_material',$datas, false);


		$query 	= $this->db->query($sql);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function update_qty_m($data) {
		$sql 	= 'update m_material set qty = qty + ' . $data['qty'] . ' 
		where id_mat = ' . $data['id_mat'] . ' ';

		//echo $sql;die;
		// $datas = array(
		// 'qty' => $data['qty_stock']
		// );

		//	print_r($datas);die;
		// $this->db->where('id_mat',$data['id_material'], false);
		// $this->db->update('m_material',$datas, false);


		$query 	= $this->db->query($sql);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_credit($data) {
		$sql 	= "
		update t_customer set sisa_credit = sisa_credit - " . $data['total_amount'] . " where id_t_cust = " . $data['id_customer'] . "
		";

		// $total = $data['sisa'] - $data['total_amount'];
		// $datas = array(
		// 'sisa_credit' => $total
		// );

		//	print_r($datas);die;
		// $this->db->where('id_t_cust',$data['id_customer'], false);
		// $this->db->update('t_customer',$datas, false);


		$query 	= $this->db->query($sql);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_credit2($data, $selling_data) {
		$sql 	= 'update t_customer set sisa_credit = sisa_credit + ' . floatval($selling_data['total_amount']) . '
		where id_t_cust = ' . $data['id_customer'];
		$query 	= $this->db->query($sql);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_retur_apr($data) {

		$datas = array(

			'status' => $data['sts']

		);

		$this->db->where('id_retur', $data['id_po']);
		$this->db->update('t_retur', $datas);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function history_retur_apr($data) {

		$sql 	= "
		INSERT INTO `history_d_penjualan`
		SELECT NULL AS jo,a.* FROM `d_penjualan` a
		JOIN  `t_detail_retur` b ON a.id_dp = b.`id_detail_mat` 
		WHERE b.id_retur  = " . $data['id_po'] . "
		
		";

		// $total = $data['sisa'] - $data['total_amount'];
		// $datas = array(
		// 'sisa_credit' => $total
		// );

		//	print_r($datas);die;
		// $this->db->where('id_t_cust',$data['id_customer'], false);
		// $this->db->update('t_customer',$datas, false);


		$query 	= $this->db->query($sql);


		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_retur($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(

			'date' => $data['date'],
			'id_penjualan' => $data['id_penjualan'],
			'keterangan' => $data['keterangan']

		);

		//	print_r($datas);die;
		$this->db->where('id_retur', $data['id_retur']);
		$this->db->update('t_retur', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_detail_penjualan($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'qty' => $data['qty_retur'],
			'qty_box' => $data['qty_box_retur'],
			'qty_satuan' => $data['qty_retur_sat'],
			'price' => $data['amount'],
			'mutasi_id' => $data['mutasi_id']
		);

		//	print_r($datas);die;
		$this->db->where('id_dp', $data['id_dp']);
		$this->db->update('d_penjualan', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	function delete_retur($id) {

		$this->db->where('id_retur', $id);
		$this->db->delete('t_retur');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $result;
	}

	function delete_detail_retur($id) {

		$this->db->where('id_retur', $id);
		$this->db->delete('t_detail_retur');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $result;
	}

	function delete_mutasi($data_q) {

		$this->db->where('id_mutasi', $data_q['mutasi_id']);
		$this->db->delete('t_mutasi');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $result;
	}
}
