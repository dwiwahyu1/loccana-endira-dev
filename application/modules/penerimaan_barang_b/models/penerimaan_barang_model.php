<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Penerimaan_barang_model extends CI_Model {

  public function __construct() {
    parent::__construct();
  }


  public function get_detail_po($id_po) {
    $this->db->select('
      a.id_po,
      a.no_po,
      a.id_distributor,
      b.id_t_ps,
      b.qty,
      b.unit_price,
      b.diskon,
      c.stock_code,
      c.stock_name,
      c.base_qty,
      d.uom_name,
      d.uom_symbol,
      b.qty_diterima,
      e.name_eksternal,
	  sum(f.qty_diterima) as qty_sudah,
	  b.qty - sum(f.qty_diterima) as qty_sisa
	  
    ');
    $this->db->from('t_purchase_order a');
    $this->db->join('t_po_mat b', 'b.id_po=a.id_po');
    $this->db->join('m_material c', 'c.id_mat=b.id_material');
    $this->db->join('m_uom d', 'd.id_uom=c.unit_terkecil');
    $this->db->join('t_eksternal e', 'e.id=a.id_distributor');
    $this->db->join('t_bpb_detail f', 'f.id_po_mat=b.id_t_ps','left');
    $this->db->group_by('b.id_t_ps', 'asc');
    $this->db->order_by('b.id_t_ps', 'asc');

    $this->db->where('a.id_po = ' . $id_po);
    $query = $this->db->get();

    return $query->result_array();
  }
  public function get_detail_po_when_edit($id_po) {
    $this->db->select('
      a.id_po,
      a.no_po,
      a.id_distributor,
      b.id_t_ps,
      b.qty,
      b.unit_price,
      b.diskon,
      c.stock_code,
      c.stock_name,
      d.uom_name,
      d.uom_symbol,
      e.name_eksternal,
      g.qty_diterima,
	  sum(g.qty_diterima) as qty_sudah,
	  b.qty - sum(g.qty_diterima) as qty_sisa
	  
    ');
    $this->db->from('t_purchase_order a');
    $this->db->join('t_po_mat b', 'b.id_po=a.id_po');
    $this->db->join('m_material c', 'c.id_mat=b.id_material');
    $this->db->join('m_uom d', 'd.id_uom=c.unit_terkecil');
    $this->db->join('t_eksternal e', 'e.id=a.id_distributor');
    $this->db->join('t_bpb f', 'f.id_po=a.id_po');
    $this->db->join('t_bpb_detail g', 'b.id_t_ps=g.id_po_mat','left');
	 $this->db->group_by('b.id_t_ps', 'asc');
    $this->db->order_by('b.id_t_ps', 'asc');

    $this->db->where('a.id_po = ' . $id_po);
    // $this->db->where('f.id_po = null');
    // $this->db->or_where('f.id_bpb =' . $data['id_bpb']);
    $query = $this->db->get();

    return $query->result_array();
  }
  public function get_bpb($id_bpb) {
    $this->db->select('
      a.id_bpb,
      a.no_do,
      a.angkutan,
      a.no_polisi,
      a.tanggal,
      a.id_po,
      b.qty_diterima,
      b.id_bpb_detail,
      b.qty_diterima,
      c.qty,
      c.unit_price,
      c.id_material
    ');
    $this->db->from('t_bpb a');
    $this->db->join('t_bpb_detail b', 'b.id_bpb=a.id_bpb');
    $this->db->join('t_po_mat c', 'b.id_po_mat=c.id_t_ps');
    $this->db->join('m_material d', 'c.id_material=d.id_mat');
    $this->db->order_by('b.id_po_mat', 'asc');

    $this->db->where('a.id_bpb = ' . $id_bpb);
    $query = $this->db->get();

    return $query->result_array();
  }
  public function get_po($data = null) {

    $this->db->select('a.id_po,a.no_po, c.name_eksternal');
    $this->db->from('t_purchase_order a');
    $this->db->join('t_bpb b', 'b.id_po=a.id_po', 'left');
    $this->db->join('t_eksternal c', 'c.id=a.id_distributor');
    $this->db->where('a.status <> 3');
    if ($data != null)
      $this->db->or_where('b.id_bpb =' . $data['id_bpb']);
    $query = $this->db->get();
    // print_r($this->db->last_query());
    // die;
    return $query->result();
  }

  public function lists($params = array()) {
    $query =   '
				SELECT COUNT(*) AS jumlah FROM t_bpb where 1 = 1
				AND ( 
					no_do LIKE "%' . $params['searchtxt'] . '%" OR
					angkutan LIKE "%' . $params['searchtxt'] . '%" 
				)
			';

    $query2 =   '
				SELECT z.*, rank() over ( ORDER BY no_do ASC) AS Rangking from ( 
					
					SELECT a.id_bpb,
					a.no_do,a.angkutan,a.tanggal,b.no_po,c.name_eksternal,a.status 
					FROM t_bpb a
					JOIN t_purchase_order b ON a.id_po=b.id_po
					JOIN t_eksternal c ON a.id_principal=c.id
					JOIN t_po_mat d ON b.id_po=d.id_po
					JOIN t_bpb_detail e ON d.id_t_ps=e.id_po_mat
					WHERE  ( 
					no_po LIKE "%' . $params['searchtxt'] . '%" OR
					name_eksternal LIKE "%' . $params['searchtxt'] . '%" 
        )  
        group by a.id_bpb
        order by no_do
        ) z
				ORDER BY no_do ASC
				
				LIMIT ' . $params['limit'] . ' 
				OFFSET ' . $params['offset'] . ' 
			';

    //echo $query2;die;

    $out    = array();
    $querys    = $this->db->query($query);
    $result = $querys->row();

    $total_filtered = $result->jumlah;
    $total       = $result->jumlah;

    if (($params['offset'] + 10) > $total_filtered) {
      $limit_data = $total_filtered - $params['offset'];
    } else {
      $limit_data = $params['limit'];
    }



    //echo $query;die;
    //echo $query;die;
    $query2s    = $this->db->query($query2);
    $result2 = $query2s->result_array();
    $return = array(
      'data' => $result2,
      'total_filtered' => $total_filtered,
      'total' => $total,
    );
    return $return;
  }
  public function edit_bpb($data) {
    $this->db->trans_start();
    $result = 0;
    $data_po = $this->get_detail_po_when_edit($data[0]['id_po']);
    //check if input from frontend is correct from backend
    if (count($data) != count($data_po)) {
      $result['result'] = 0;
      return $result;
    }
    $data_ori_bpb = $this->get_bpb($data[0]['id_bpb']);
    $datas = array(
      'id_po'         => $data[0]['id_po'],
      'id_principal'  => $data_po[0]['id_distributor'],
      'no_do'         => $data[0]['no_do'],
      'angkutan'      => $data[0]['angkutan'],
      'no_polisi'     => $data[0]['no_polisi'],
      'id_pic'        => $data[0]['id_pic'],
      'tanggal'       => $data[0]['tanggal_btb'],
      //'status'        =>  0 //asumsi sementara
    );
    $this->db->where('id_bpb',$data[0]['id_bpb']);
    $this->db->update('t_bpb', $datas);
    $result  = $result + $this->db->affected_rows();

    if (count($data) < count($data_ori_bpb)) {
      $data_deletes = array();
      for ($i = count($data); $i < count($data_ori_bpb); $i++) {
        $data_delete = array(
          'id_bpb_detail' => $data_ori_bpb[$i]['id_bpb_detail']
        );
        array_push($data_deletes, $data_delete);
      }
      $this->db->where_in('id_bpb_detail', $data_deletes);
      $this->db->delete('t_bpb_detail');
    }

    $data_updates = array();
    for ($i = 0; $i < count($data); $i++) {
      if (isset($data_ori_bpb[$i]['id_bpb_detail']))
        $id_bpb_detail = $data_ori_bpb[$i]['id_bpb_detail'];
      else
        $id_bpb_detail = null;
      $data_update = array(
        'id_bpb_detail' => $id_bpb_detail,
        'id_bpb'        => $data[$i]['id_bpb'],
        'id_po_mat'     => $data_po[$i]['id_t_ps'],
        'type'          => 0,
        'qty_diterima'  => $data[$i]['qty_diterima'],
        'keterangan'    => '', //sementara di nullin belum tau sumbernya
      );
      array_push($data_updates, $data_update);
    }

    $this->db->update_or_insert_batch('t_bpb_detail', $data_updates, 'id_bpb_detail');
    $result  = $result + $this->db->affected_rows();
    $arr_result['result'] = $result;

    $this->db->trans_complete();
    // echo "<pre>";
    // print_r($arr_result);
    // die;
    return $arr_result;
  }
  public function add_bpb($data) {
    $this->db->trans_start();
    $result = 0;
    $data_po = $this->get_detail_po($data[0]['id_po']);

    $datas = array(
      'id_po'         => $data[0]['id_po'],
      'id_principal'  => $data_po[0]['id_distributor'],
      'no_do'         => $data[0]['no_do'],
      'angkutan'      => $data[0]['angkutan'],
      'no_polisi'     => $data[0]['no_polisi'],
      'id_pic'        => $data[0]['id_pic'],
      'tanggal'       => $data[0]['tanggal_btb'],
    );

    $this->db->insert('t_bpb', $datas);
    $arr_result['lastid'] = $this->db->insert_id();
    $result  = $result + $this->db->affected_rows();

    $datas = array();
    for ($i = 0; $i < count($data); $i++) {
      $data_sub = array(
        'id_bpb'        => $arr_result['lastid'],
        'id_po_mat'     => $data_po[$i]['id_t_ps'],
        'type'          => 0,
        'qty_diterima'  => $data[$i]['qty_diterima'],
        'keterangan'    => '', //sementara di nullin belum tau sumbernya
      );
      array_push($datas, $data_sub);
    }
    // echo"<pre>";
    // print_r($datas);
    // die;
    $this->db->insert_batch('t_bpb_detail', $datas);
    $result  = $result + $this->db->affected_rows();
    $arr_result['result'] = $result;

    $this->db->trans_complete();
    return $arr_result;
  }

  public function approve_bpb($id_bpb) {
    $this->db->trans_start();
    $result = 0;
    $data_bpb = $this->get_bpb($id_bpb);
    $data_update_status_bpb = array(
      'id_bpb'        => $id_bpb,
      'status'        => 1,
      'approval_date' => date('Y-m-d'),
      'approval_by'   => $this->session->userdata['logged_in']['user_id'],
    );
    $this->db->where('id_bpb', $id_bpb);
    $this->db->update('t_bpb', $data_update_status_bpb);
    $result  = $result + $this->db->affected_rows();

    $data_update_status_bpb_detail = array(
      'id_bpb'        => $id_bpb,
      'type'          => 1,
    );
    $this->db->where('id_bpb', $id_bpb);
    $this->db->update('t_bpb_detail', $data_update_status_bpb_detail);
    $result  = $result + $this->db->affected_rows();
    $data_update_qty_materials = array();
    for ($i = 0; $i < count($data_bpb); $i++) {
      $data_update_qty_material = array(
        'id_mat' => $data_bpb[$i]['id_material'],
        'qty'    => 'qty +' . $data_bpb[$i]['qty_diterima'],
      );

      array_push($data_update_qty_materials, $data_update_qty_material);
    }
    $this->db->update_batch('m_material', $data_update_qty_materials, 'id_mat', false);
    $this->db->last_query();
    // echo"<pre>";
    // print_r($data_update_qty_materials);
    // print_r($this->db->last_query());
    // die;
    $result  = $result + $this->db->affected_rows();
    $data_insert_mutasis = array();
    for ($i = 0; $i < count($data_bpb); $i++) {
      $data_insert_mutasi = array(
        'id_stock_awal'  => $data_bpb[$i]['id_material'],
        'id_stock_akhir' => $data_bpb[$i]['id_material'],
        'date_mutasi'    => date('Y-m-d H:i:s'),
        'amount_mutasi'  => $data_bpb[$i]['qty_diterima'],
        'type_mutasi'    => 0,
        'user_id'    => $this->session->userdata['logged_in']['user_id'],
      );
      array_push($data_insert_mutasis, $data_insert_mutasi);
    }
    $this->db->insert_batch('t_mutasi', $data_insert_mutasis);
    $result  = $result + $this->db->affected_rows();
    $data_insert_detail_materials = array();
    for ($i = 0; $i < count($data_bpb); $i++) {
      $data_insert_detail_material = array(
        'id_material'  => $data_bpb[$i]['id_material'],
        'base_price' => $data_bpb[$i]['unit_price'],
        'qty'  => $data_bpb[$i]['qty_diterima'],
      );
      array_push($data_insert_detail_materials, $data_insert_detail_material);
	  
    }
    $this->db->insert_batch('t_detail_material', $data_insert_detail_materials, 'id_mat');
    $result  = $result + $this->db->affected_rows();

    $arr_result['result'] = $result;

	 $this->db->trans_complete();

	 // for ($i = 0; $i < count($data_bpb); $i++) {
		// $this->db->where('id_mat', $data_bpb[$i]['id_material']);
		// $this->db->update('m_material', array( 'base_price' => ' ( SELECT IF(SUM(base_price*qty)/SUM(qty) IS NULL,0,SUM(base_price*qty)/SUM(qty)) hp FROM `t_detail_material` WHERE id_material =  '.$data_bpb[$i]['id_material'].' GROUP BY `id_material` ) ' ) );
	 // }

   
    return $arr_result;
  }  
  
  public function add_hpp($id_bpb) {
    $this->db->trans_start();
    $result = 0;
    $data_bpb = $this->get_bpb($id_bpb);
  
  
	 for ($i = 0; $i < count($data_bpb); $i++) {

			$query = '
			UPDATE `m_material` a JOIN (
			SELECT id_material,ROUND(SUM(base_price*qty)/SUM(qty)) hp FROM `t_detail_material` WHERE id_material = '.$data_bpb[$i]['id_material'].' GROUP BY `id_material` 
			) b ON a.id_mat = b.id_material
			SET a.base_price = b.hp
			WHERE id_material =  '.$data_bpb[$i]['id_material'].'

			';
			
			$out    = array();
			$querys    = $this->db->query($query);

		}
		
		    
	
	$result  = $result + $this->db->affected_rows();
  
    $arr_result['result'] = $result;

	 $this->db->trans_complete();

    return $arr_result;
  }

  public function delete_bpb($id_bpb) {
    $result = 0;
    $this->db->where('id_bpb', $id_bpb);
    $this->db->delete('t_bpb');
    $result  = $result + $this->db->affected_rows();

    $this->db->where('id_bpb', $id_bpb);
    $this->db->delete('t_bpb_detail');

    $result  = $result + $this->db->affected_rows();

    return $result;
  }
}
