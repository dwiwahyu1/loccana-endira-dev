<style>
	
	#no_inv_loading-us{display:none}
	#no_inv_tick{display:none}
	.dt-body-left {
		text-align: left;
	}

	.dt-body-right {
		text-align: right;
	}

	.dt-body-center {
		text-align: center;
		vertical-align: middle;
	}

	.force-overflow {
		height: 650px;
		overflow-y: auto;
		overflow-x: auto
	}

	.scroll-overflow {
		min-height: 650px
	}

	#modal-distributor::-webkit-scrollbar-track {
		-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar {
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar-thumb {
		background-image: -webkit-gradient(linear,
				left bottom,
				left top,
				color-stop(0.44, rgb(122, 153, 217)),
				color-stop(0.72, rgb(73, 125, 189)),
				color-stop(0.86, rgb(28, 58, 148)));
	}

	.select2-container .select2-choice {

		height: 35px !important;

	}
</style>

<div class="product-sales-area mg-tb-30">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="product-payment-inner-st">
					<ul id="myTabedu1" class="tab-review-design">
						<li class="active"><a href="#description">Tambah Invoice</a></li>
					</ul>
					<div id="myTabContent" class="tab-content custom-product-edit">
						<div class="product-tab-list tab-pane fade active in" id="description">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="review-content-section">
										<form id="add_po" action="<?php echo base_url() . 'invoice_buying/add_po'; ?>" class="add-department" autocomplete="off">
											<div class="row">
												<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">

													<div class="form-group">
														<label>No. DO</label>
														<select name="no_pos" id="no_pos" onChange="select_pos()" class="form-control" placeholder="No. DO">
															<option></option>
															<?php
															foreach ($principal as $principals) {
																echo '<option value="' . $principals['id_bpb'] . '" >' . $principals['no_do'] . ' - ' . $principals['name_eksternal'] . ' - ' . $principals['tanggal_bpb'] . '</option>';
															}
															?>
														</select>
													</div>

													<div class="form-group">
														<label>No. Purchase Order</label>
														<input name="kode" id="kode" type="text" class="form-control" placeholder="No. Purchase Order" value="" readOnly>
													</div>
													<div class="form-group date">
														<label>Tanggal</label>
														<input name="tgl" id="tgl" type="text" class="form-control" placeholder="Tanggal Purchase Order" readOnly>
													</div>
													<div class="form-group">
														<label>Principal</label>
														<input name="ints" id="ints" type="hidden" class="form-control" placeholder="Tanggal Purchase Order" value='0'>
														<input name="ints2" id="ints2" type="hidden" class="form-control" placeholder="Tanggal Purchase Order" value='0'>
														<input name="name" id="name" type="text" class="form-control" placeholder="Principal" value='0' readonly>
														<!--  <select name="name" id="name" onChange="select_prin()" class="form-control" placeholder="Nama Principal">
																	<option value="0" selected="selected" disabled>-- Pilih Principal --</option>
																	<?php
																	foreach ($principal as $principals) {
																		echo '<option value="' . $principals['id'] . '" >' . $principals['name_eksternal'] . '</option>';
																	}
																	?>
																</select> -->
													</div>
													<div class="form-group">
														<label>Alamat</label>
														<textarea name="alamat" id="alamat" type="text" class="form-control" placeholder="Alamat Principal" readOnly></textarea>
													</div>
													<div class="form-group">
														<label>Att</label>
														<input name="att" id="att" type="text" class="form-control" placeholder="Att" readOnly>
													</div>
													<div class="form-group">
														<label>No Telp</label>
														<input name="telp" id="telp" type="text" class="form-control" placeholder="Telephone" readOnly>
													</div>
													<div class="form-group">
														<label>Fax</label>
														<input name="fax" id="fax" type="text" class="form-control" placeholder="Fax" readOnly>
													</div>
												</div>
												<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
													<div class="form-group">
														<label>Ship To:</label>
														<textarea name="alamat_kirim" id="alamat_kirim" type="text" class="form-control" placeholder="Alamat Principal" readonly>JL. Sangkuriang NO.38-A 
NPWP: 01.555.161.7.428.000</textarea>
													</div>
													<div class="form-group date">
														<label>Email</label>
														<input name="email" id="email" type="email" class="form-control" placeholder="Email" readonly>
													</div>
													<div class="form-group date">
														<label>Telp/Fax</label>
														<input name="telpfax" id="telpfax" type="text" class="form-control" placeholder="Telp/Fax" value="(022) 6626-946" readonly>
													</div>
													<div class="form-group date">
														<label>VAT/PPN</label>
														<input name="ppn" id="ppn" type="text" class="form-control" placeholder="VAT/PPN" onKeyup="change_ppn()" value="10" readonly>
													</div>

													<div class="form-group date">
														<label>Term Pembayaran</label>
														<input name="term_o" id="term_o" type="number" class="form-control" placeholder="Term (Hari)" value="0" style="" readonly>
													</div>

													<div class="form-group date">
														<label>Keterangan</label>
														<textarea name="ket" id="ket" class="form-control" placeholder="keterangan" readonly></textarea>
													</div>

													<div class="form-group date">
														<label>No Invoice</label>
														<input name="no_inv" id="no_inv" type="text" class="form-control" placeholder="No Invoice">
														<span id="no_inv_loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking No Pendaftaran...</span>
														<span id="no_inv_tick"></span>
													</div>

													<div class="form-group date">
														<label>Tanggal Invoice</label>
														<input name="tgl_inv" id="tgl_inv" type="text" class="form-control" placeholder="Tanggal Penjualan">
													</div>

													<div class="form-group date">
														<label>Tanggal Jatuh Tempo</label>
														<input name="tgl_due" id="tgl_due" type="text" class="form-control" placeholder="Tanggal Penjualan">
													</div>

													<div class="form-group date">
														<label>Keterangan Invoice</label>
														<textarea name="ket_inv" id="ket_inv" class="form-control" placeholder="keterangan"></textarea>
													</div>


													<div class="form-group date">
														<label>Faktur Pajak</label>
														<input name="fak_paj" id="fak_paj" type="text" class="form-control" placeholder="Faktur Pajak">
													</div>

													<!--<div class="form-group date">
																<label>Tanggal Faktur Pajak</label>
																<input name="tgl_fak" id="tgl_fak" type="text" class="form-control" placeholder="Tanggal Penjualan" >
                                                            </div>-->

												</div>
											</div>



									</div>
								</div>
							</div>
							<br><br>
							<div class="row">
								<h3>Items</h3>
							</div>

							<div class="row">
								<div class="col-lg-5 col-md-5 col-sm-3 col-xs-12">
									<div class="form-group">
										<label>Kode</label>
									</div>
								</div>
								<div class="col-lg-2 col-md-1 col-sm-3 col-xs-12">
									<div class="form-group">
										<label>Qty Lt/Kg</label>
									</div>
								</div>
								<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
									<div class="form-group">
										<label>Harga</label>
									</div>
								</div>
								<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
									<div class="form-group">
										<label>Diskon</label>
									</div>
								</div>
								<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
									<div class="form-group">
										<label>Total</label>
									</div>
								</div>
								<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
									<div class="form-group">
										<label></label>
									</div>
								</div>
							</div>
							<input name="int_flo" id="int_flo" type="hidden" class="form-control" placeholder="Qty" value=0;>
							<div id="table_items">


							</div>

							<br>
							<div class="row" style="border-top-style:solid;">
								<div style="margin-top:10px">
									<div class="col-lg-8 col-md-3 col-sm-3 col-xs-12">

									</div>
									<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
										<label>Sub Total</label>
									</div>
									<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
										<label id='subttl' style="float:right">0</label>
									</div>
									<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
									</div>
								</div>
							</div>
							<div class="row" style="">
								<div style="margin-top:10px">
									<div class="col-lg-8 col-md-3 col-sm-3 col-xs-12">

									</div>
									<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
										<label>Diskon</label>
									</div>
									<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
										<label id='subttl3' style="float:right">0</label>
									</div>
									<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
									</div>
								</div>
							</div>
							<div class="row" style="">
								<div style="margin-top:10px">
									<div class="col-lg-8 col-md-3 col-sm-3 col-xs-12">

									</div>
									<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
										<label>Taxable</label>
									</div>
									<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
										<label id='subttl2' style="float:right">0</label>
									</div>
									<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
									</div>
								</div>
							</div>
							<div class="row" style="">
								<div style="margin-top:10px">
									<div class="col-lg-8 col-md-3 col-sm-3 col-xs-12">

									</div>
									<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
										<label>VAT/PPN</label>
									</div>
									<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
										<label id="vatppn" style="float:right">0</label>
									</div>
									<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
									</div>
								</div>
							</div>
							<div class="row" style="border-top-style:solid;">
								<div style="margin-top:10px">
									<div class="col-lg-8 col-md-3 col-sm-3 col-xs-12">

									</div>
									<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
										<label>Total</label>
									</div>
									<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
										<label id="grand_total" style="float:right">0</label>
									</div>
									<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="payment-adress">
										<button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
										<button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()"> Batal</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="ModalAddPo" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-close-area modal-close-df">
				<a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
			</div>
			<div class="modal-body">
				<i class="educate-icon educate-checked modal-check-pro"></i>
				<h2>Data Berhasil Disimpan</h2>
				<p>Apakah Anda Ingin Menambah Data Po Lagi ?</p>
			</div>
			<div class="modal-footer">
				<a data-dismiss="modal" href="#" onClick="back()">Tidak</a>
				<a data-dismiss="modal" href="#" onClick="clearform()">Ya</a>
			</div>
		</div>
	</div>
</div>

<div id="ModalChangePrincipal" class="modal modal-edu-general Customwidth-popup-WarningModal PrimaryModal-bgcolor fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-close-area modal-close-df">
				<!--<a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>-->
			</div>
			<div class="modal-body">
				<span class="educate-icon educate-warning modal-check-pro information-icon-pro"></span>
				<h2>Warning!</h2>
				<p id="msg_err">The Modal plugin is a dialog box/popup window that is displayed on top of the current page</p>
			</div>
			<div class="modal-footer footer-modal-admin warning-md">
				<a data-dismiss="modal" href="#" onClick="okchange()">Ok</a>
				<a data-dismiss="modal" href="#" onClick="cancelchange()">Batal</a>
			</div>
		</div>
	</div>
</div>

<script>
	function change_kode(rs) {

		var datapost = {
			"kode": $('#kode_' + rs + '').val()
		};

		$.ajax({
			type: 'POST',
			url: "<?php echo base_url() . 'invoice_buying/get_price_mat'; ?>",
			data: JSON.stringify(datapost),
			cache: false,
			contentType: false,
			processData: false,
			success: function(response) {

				$('#harga_' + rs + '').val(response.list);

			}
		});
	}

	function tambah_row() {

		$('.hapus_btn').hide();
		var values = $('#int_flo').val();

		var new_fl = parseInt(values) + 1;

		$('#int_flo').val(new_fl);
		var datapost = {
			"id_prin": $('#name').val()
		};

		$.ajax({
			type: 'POST',
			url: "<?php echo base_url() . 'invoice_buying/get_principal'; ?>",
			data: JSON.stringify(datapost),
			cache: false,
			contentType: false,
			processData: false,
			success: function(response) {



				$('#table_items').html('');
				$('#table_items').append(response.list);


				$(".rupiah").inputFilter(function(value) {
					return /^-?\d*[,]?\d*$/.test(value);
				});

				// $('.rupiah').priceFormat({
				// prefix: '',
				// centsSeparator: ',',
				// centsLimit: 2,
				// thousandsSeparator: '.'
				// });	

			}
		});

	}

	function cancelchange() {

		var prin = $('#ints2').val();
		$('#ints').val(prin);
		$('#name').val(prin);

	}

	function okchange() {

		var datapost = {
			"id_prin": $('#name').val()
		};

		$.ajax({
			type: 'POST',
			url: "<?php echo base_url() . 'invoice_buying/get_principal'; ?>",
			data: JSON.stringify(datapost),
			cache: false,
			contentType: false,
			processData: false,
			success: function(response) {
				//var obj = JSON.parse(response);
				$('#alamat').val(response['principal'][0].eksternal_address);
				$('#att').val(response['principal'][0].pic);
				$('#telp').val(response['principal'][0].phone_1);
				$('#fax').val(response['principal'][0].fax);
				//console.log(response.phone_1);

				$('#table_items').html('')

				var htl = '';
				htl += '<div id="row_0" >';
				htl += '									<div class="row" style="border-top-style:solid;">';
				htl += '									<div style="margin-top:10px">';
				htl += '										<div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">';
				htl += '<div class="form-group col-lg-10 col-md-10" style="margin-left:-15px">';
				htl += '														<select name="kode_0" id="kode_0" class="form-control" placeholder="Nama Principal">';
				htl += '															<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
				htl += '														</select>';
				htl += '													</div>';
				htl += '													<div class="form-group col-lg-2 col-md-2" style="margin-left:-25px">';
				htl += '														<button type="button" class="btn btn-danger waves-effect waves-light tbh_btn" onClick="lainnya(0)">Lainnya</button>';
				htl += '													</div>';
				htl += '										</div>';
				htl += '										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
				htl += '											<div class="form-group">';
				htl += '												<input name="qty_0" id="qty_0" type="text" class="form-control rupiah" onKeyup="change_sum(0)"  placeholder="Qty" value=0 >';
				htl += '											</div>';
				htl += '										</div>';
				htl += '										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
				htl += '											<div class="form-group">';
				htl += '												<input name="harga_0" id="harga_0" type="text" class="form-control rupiah" onKeyup="change_sum(0)" placeholder="Harga" value=0 >';
				htl += '											</div>';
				htl += '										</div>';
				htl += '										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
				htl += '											<div class="form-group">';
				htl += '												<input name="diskon_0" id="diskon_0" type="number" class="form-control" onKeyup="change_sum(0)" placeholder="Diskon" value=0 >';
				htl += '											</div>';
				htl += '										</div>';
				htl += '										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
				htl += '											<div class="form-group">';
				htl += '												<input name="total_0" id="total_0" type="text" class="form-control rupiah" placeholder="Total" readOnly value=0 >';
				htl += '											</div>';
				htl += '										</div>';
				htl += '										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
				htl += '											<div class="form-group">';
				htl += '												<button type="button" class="btn btn-danger waves-effect waves-light " onClick="kurang_row()">-</button>';
				htl += '											</div>';
				htl += '										</div>';
				htl += '									</div>';
				htl += '									</div>';
				htl += '								</div>';

				$('#table_items').append(htl);

				$('#kode_0').html(response.list);

				$('#kode_0').select2();

				// $('.rupiah').priceFormat({
				// prefix: '',
				// centsSeparator: '',
				// centsLimit: 0,
				// thousandsSeparator: '.'
				// });	

				$(".rupiah").inputFilter(function(value) {
					return /^-?\d*[,]?\d*$/.test(value);
				});

			}
		});

	}

	function lainnya(int_fal) {

		var datapost = {
			"id_prin": $('#name').val()
		};

		$.ajax({
			type: 'POST',
			url: "<?php echo base_url() . 'invoice_buying/get_all_item'; ?>",
			data: JSON.stringify(datapost),
			cache: false,
			contentType: false,
			processData: false,
			success: function(response) {

				$('#kode_' + int_fal).html(response.list);

				$('#kode_' + int_fal).select2();
			}
		});

	}


	function select_pos() {

		var datapost = {
			"id": $('#no_pos').val()
		};

		$.ajax({
			type: 'POST',
			url: "<?php echo base_url() . 'invoice_buying/get_po_detail'; ?>",
			data: JSON.stringify(datapost),
			cache: false,
			contentType: false,
			processData: false,
			success: function(response) {
				//var obj = JSON.parse(response);
				$('#kode').val(response['invoice'][0].no_po);
				$('#tgl').val(response['invoice'][0].date_po);
				$('#name').val(response['invoice'][0].name_eksternal);
				$('#ppn').val(response['invoice'][0].ppn);
				$('#term_o').val(response['invoice'][0].term_of_payment);
				$('#ket').val(response['invoice'][0].keterangan);

				$('#alamat').val(response['invoice'][0].eksternal_address);
				$('#att').val(response['invoice'][0].pic);
				$('#telp').val(response['invoice'][0].phone_1);
				$('#fax').val(response['invoice'][0].fax);

				$('#tgl_inv').val(response['invoice'][0].date_po);
				$('#tgl_due').val(response.due_date);
				//console.log(response.phone_1);


				$('#table_items').html('');

				$('#table_items').append(response.list);
				$('#grand_total').html(response.grand_total);
				$('#subttl').html(response.sub_total_amount);
				$('#subttl3').html(response.sub_diskon);
				$('#subttl2').html(response.total_amount);
				$('#vatppn').html(response.sub_ppn);
				$('#int_flo').val(response.int_val);

				//$($("#kode_0").select2("container")).addClass("form-control");
				//$("#kode_0").select2({ height: '300px' });	

				// $('.rupiah').priceFormat({
				// prefix: '',
				// centsSeparator: ',',
				// centsLimit: 2,
				// thousandsSeparator: '.'
				// });	

				$(".rupiah").inputFilter(function(value) {
					return /^-?\d*[,]?\d*$/.test(value);
				});
			}
		});


	}


	function select_prin() {

		var prin = $('#ints').val();
		var prin2 = $('#name').val();

		$('#ints2').val(prin);
		//	alert(prin2);
		$('#ints').val(prin2);

		if (prin !== '0') {

			$('#msg_err').html('Jika Pilih Principal Lain, Data Yang Tidak Tersimpan Akan Terhapus, Yakin Akan Melanjutkan ? ');
			$('#ModalChangePrincipal').modal({
				backdrop: 'static',
				keyboard: false
			});
		} else {

			var datapost = {
				"id_prin": $('#name').val()
			};

			$.ajax({
				type: 'POST',
				url: "<?php echo base_url() . 'invoice_buying/get_principal'; ?>",
				data: JSON.stringify(datapost),
				cache: false,
				contentType: false,
				processData: false,
				success: function(response) {
					//var obj = JSON.parse(response);
					$('#alamat').val(response['principal'][0].eksternal_address);
					$('#att').val(response['principal'][0].pic);
					$('#telp').val(response['principal'][0].phone_1);
					$('#fax').val(response['principal'][0].fax);
					//console.log(response.phone_1);


					$('#table_items').html('')

					var htl = '';
					htl += '<div id="row_0" >';
					htl += '									<div class="row" style="border-top-style:solid;">';
					htl += '									<div style="margin-top:10px">';
					htl += '										<div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">';
					htl += '<div class="form-group col-lg-10 col-md-10" style="margin-left:-15px">';
					htl += '														<select name="kode_0" id="kode_0" class="form-control" placeholder="Nama Principal">';
					htl += '															<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
					htl += '														</select>';
					htl += '													</div>';
					htl += '													<div class="form-group col-lg-2 col-md-2" style="margin-left:-25px">';
					htl += '														<button type="button" class="btn btn-danger waves-effect waves-light tbh_btn" onClick="lainnya(0)">Lainnya</button>';
					htl += '													</div>';
					htl += '										</div>';
					htl += '										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
					htl += '											<div class="form-group">';
					htl += '												<input name="qty_0" id="qty_0" type="text" class="form-control rupiah" onKeyup="change_sum(0)" placeholder="Qty" value=0 >';
					htl += '											</div>';
					htl += '										</div>';
					htl += '										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
					htl += '											<div class="form-group">';
					htl += '												<input name="harga_0" id="harga_0" type="text" class="form-control rupiah" onKeyup="change_sum(0)" placeholder="Harga" value=0 >';
					htl += '											</div>';
					htl += '										</div>';
					htl += '										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
					htl += '											<div class="form-group">';
					htl += '												<input name="diskon_0" id="diskon_0" type="text" class="form-control rupiah" onKeyup="change_sum(0)" placeholder="Diskon" value=0 >';
					htl += '											</div>';
					htl += '										</div>';
					htl += '										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
					htl += '											<div class="form-group">';
					htl += '												<input name="total_0" id="total_0" type="text" class="form-control rupiah" placeholder="Total" value=0 readOnly  >';
					htl += '											</div>';
					htl += '										</div>';
					htl += '										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
					htl += '											<div class="form-group">';
					htl += '												';
					htl += '											</div>';
					htl += '										</div>';
					htl += '									</div>';
					htl += '									</div>';
					htl += '								</div>';

					$('#table_items').append(htl);
					$('#kode_0').html(response.list);

					$('#kode_0').select2();

					//$($("#kode_0").select2("container")).addClass("form-control");
					//$("#kode_0").select2({ height: '300px' });	

					// $('.rupiah').priceFormat({
					// prefix: '',
					// centsSeparator: ',',
					// centsLimit: 2,
					// thousandsSeparator: '.'
					// });	

					$(".rupiah").inputFilter(function(value) {
						return /^-?\d*[,]?\d*$/.test(value);
					});
				}
			});

		}

		//$('#ints').val('1');

	}

	function kurang_row(new_fl) {

		$('#row_' + new_fl).remove();

	}

	function clearform() {

		$('#add_po').trigger("reset");
		$('#table_items').html('')

		var htl = '';
		htl += '<div id="row_0" >';
		htl += '									<div class="row" style="border-top-style:solid;">';
		htl += '									<div style="margin-top:10px">';
		htl += '										<div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">';
		htl += '<div class="form-group col-lg-10 col-md-10" style="margin-left:-15px">';
		htl += '														<select name="kode_0" id="kode_0" class="form-control" placeholder="Nama Principal">';
		htl += '															<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		htl += '														</select>';
		htl += '													</div>';
		htl += '													<div class="form-group col-lg-2 col-md-2" style="margin-left:-25px">';
		htl += '														<button type="button" class="btn btn-danger waves-effect waves-light tbh_btn" onClick="lainnya(0)">Lainnya</button>';
		htl += '													</div>';
		htl += '										</div>';
		htl += '										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl += '											<div class="form-group">';
		htl += '												<input name="qty_0" id="qty_0" type="text" class="form-control rupiah" onKeyup="change_sum(0)" placeholder="Qty" value=0 >';
		htl += '											</div>';
		htl += '										</div>';
		htl += '										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl += '											<div class="form-group">';
		htl += '												<input name="harga_0" id="harga_0" type="text" class="form-control rupiah" onKeyup="change_sum(0)" placeholder="Harga" value=0 >';
		htl += '											</div>';
		htl += '										</div>';
		htl += '										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl += '											<div class="form-group">';
		htl += '												<input name="diskon_0" id="diskon_0" type="text" class="form-control rupiah" onKeyup="change_sum(0)" placeholder="Diskon" value=0 >';
		htl += '											</div>';
		htl += '										</div>';
		htl += '										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl += '											<div class="form-group">';
		htl += '												<input name="total_0" id="total_0" type="text" class="form-control rupiah" placeholder="Total" value=0 readOnly  >';
		htl += '											</div>';
		htl += '										</div>';
		htl += '										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl += '											<div class="form-group">';
		htl += '												';
		htl += '											</div>';
		htl += '										</div>';
		htl += '									</div>';
		htl += '									</div>';
		htl += '								</div>';

		$('#table_items').append(htl);

		$('#kode_0').select2();

		$(".rupiah").inputFilter(function(value) {
			return /^-?\d*[,]?\d*$/.test(value);
		});

		// $('.rupiah').priceFormat({
		// prefix: '',
		// centsSeparator: ',',
		// centsLimit: 2,
		// thousandsSeparator: '.'
		// });	

	}

	function change_ppn() {

		var sub_total = 0;
		var sub_total_bd = 0;
		var sub_disk = 0;
		var int_val = parseInt($('#int_flo').val());

		for (var yo = 0; yo <= int_val; yo++) {

			if ($('#kode_' + yo).val() !== undefined) {

				var tots = $('#total_' + yo).val();
				var tots = tots.replace('.', '');
				var tots = tots.replace('.', '');
				var tots = tots.replace('.', '');
				var tots = tots.replace('.', '');

				var prcs = $('#harga_' + yo).val();
				var prcs = prcs.replace('.', '');
				var prcs = prcs.replace('.', '');
				var prcs = prcs.replace('.', '');
				var prcs = prcs.replace('.', '');

				var qtys = $('#qty_' + yo).val();
				var qtys = qtys.replace('.', '');
				var qtys = qtys.replace('.', '');
				var qtys = qtys.replace('.', '');
				var qtys = qtys.replace('.', '');



				sub_total = sub_total + parseFloat(tots);
				sub_total_bd = sub_total_bd + (parseFloat(prcs) * parseFloat(qtys));
				sub_disk = sub_disk + ((parseFloat(prcs) * parseFloat(qtys)) * (parseFloat($('#diskon_' + yo).val()) / 100));
			}

		}

		if ($('#ppn').val() == '') {
			var ppns = 0;
		} else {
			var ppns = $('#ppn').val();
		}

		var total_ppn = (sub_total * (parseFloat(ppns) / 100));
		var grand_total = sub_total + (sub_total * (parseFloat(ppns) / 100));

		var gt = new Intl.NumberFormat('de-DE', {}).format(grand_total);
		var sub_totals = new Intl.NumberFormat('de-DE', {}).format(sub_total);
		var total_ppns = new Intl.NumberFormat('de-DE', {}).format(total_ppn);
		var total_bds = new Intl.NumberFormat('de-DE', {}).format(sub_total_bd);
		var total_sub_disk = new Intl.NumberFormat('de-DE', {}).format(sub_disk);

		$('#vatppn').html(total_ppns);
		$('#grand_total').html(gt);

	}

	function change_sum_as(fl) {

		var qty = $('#qty_' + fl).val();
		var harga = $('#harga_' + fl).val();
		var diskon = $('#diskon_' + fl).val();
		var remark = $('#remark_' + fl).val();
		var ppn = $('#ppn').val();


		var harga = harga.replace('.', '');
		var harga = harga.replace('.', '');
		var harga = harga.replace('.', '');
		var harga = harga.replace('.', '');
		var harga = harga.replace(',', '.');

		var diskon = diskon.replace('.', '');
		var diskon = diskon.replace('.', '');
		var diskon = diskon.replace('.', '');
		var diskon = diskon.replace('.', '');
		var diskon = diskon.replace(',', '.');

		var qty = qty.replace('.', '');
		var qty = qty.replace('.', '');
		var qty = qty.replace('.', '');
		var qty = qty.replace('.', '');
		var qty = qty.replace(',', '.');
		//alert(harga);


		var total = (parseFloat(qty) * parseFloat(harga)) - ((parseFloat(qty) * parseFloat(harga)) * (parseFloat(diskon) / 100));
		var totals = new Intl.NumberFormat('de-DE', {
			minimumFractionDigits: 2
		}).format(total);


		if (isNaN(total)) {
			//var total_num = Number((total).toFixed(1)).toLocaleString();
			//$('#subttl').html('0');
			$('#total_' + fl).val('0');
		} else {
			//var total_num = Number((total).toFixed(1)).toLocaleString();
			//$('#subttl').html(total);
			$('#total_' + fl).val(totals);
		}

		var sub_total = 0;
		var sub_total_bd = 0;
		var sub_disk = 0;
		var int_val = parseInt($('#int_flo').val());

		for (var yo = 0; yo <= int_val; yo++) {

			if ($('#kode_' + yo).val() !== undefined) {

				var tots = $('#total_' + yo).val();
				var tots = tots.replace('.', '');
				var tots = tots.replace('.', '');
				var tots = tots.replace('.', '');
				var tots = tots.replace('.', '');
				var tots = tots.replace(',', '.');

				var prcs = $('#harga_' + yo).val();
				var prcs = prcs.replace('.', '');
				var prcs = prcs.replace('.', '');
				var prcs = prcs.replace('.', '');
				var prcs = prcs.replace('.', '');
				var prcs = prcs.replace(',', '.');

				var qtys = $('#qty_' + yo).val();
				var qtys = qtys.replace('.', '');
				var qtys = qtys.replace('.', '');
				var qtys = qtys.replace('.', '');
				var qtys = qtys.replace('.', '');
				var qtys = qtys.replace(',', '.');

				var diskons = $('#diskon_' + yo).val();
				var diskons = diskons.replace('.', '');
				var diskons = diskons.replace('.', '');
				var diskons = diskons.replace('.', '');
				var diskons = diskons.replace('.', '');
				var diskons = diskons.replace(',', '.');



				sub_total = sub_total + parseFloat(tots);
				sub_total_bd = sub_total_bd + (parseFloat(prcs) * parseFloat(qtys));
				sub_disk = sub_disk + ((parseFloat(prcs) * parseFloat(qtys)) * (parseFloat(diskons) / 100));
			}

		}

		var total_ppn = (sub_total * (parseFloat($('#ppn').val()) / 100));
		var grand_total = sub_total + (sub_total * (parseFloat($('#ppn').val()) / 100));



		var gt = new Intl.NumberFormat('de-DE', {
			minimumFractionDigits: 2
		}).format(grand_total);
		var sub_totals = new Intl.NumberFormat('de-DE', {
			minimumFractionDigits: 2
		}).format(sub_total);
		var total_ppns = new Intl.NumberFormat('de-DE', {
			minimumFractionDigits: 2
		}).format(total_ppn);
		var total_bds = new Intl.NumberFormat('de-DE', {
			minimumFractionDigits: 2
		}).format(sub_total_bd);
		var total_sub_disk = new Intl.NumberFormat('de-DE', {
			minimumFractionDigits: 2
		}).format(sub_disk);

		$('#subttl').html(total_bds);
		$('#subttl2').html(sub_totals);
		$('#subttl3').html(total_sub_disk);
		$('#vatppn').html(total_ppns);
		$('#grand_total').html(gt);




		//alert(qty+' '+harga+' '+diskon);

	}

	function change_sum(fl) {

		var qty = $('#qty_' + fl).val();
		var harga = $('#harga_' + fl).val();
		var diskon = $('#diskon_' + fl).val();
		var remark = $('#remark_' + fl).val();
		var ppn = $('#ppn').val();

		//alert(qty);

		var harga = harga.replace('.', '');
		var harga = harga.replace('.', '');
		var harga = harga.replace('.', '');
		var harga = harga.replace('.', '');
		var harga = harga.replace(',', '.');

		var diskon = diskon.replace('.', '');
		var diskon = diskon.replace('.', '');
		var diskon = diskon.replace('.', '');
		var diskon = diskon.replace('.', '');
		var diskon = diskon.replace(',', '.');

		var qty = qty.replace('.', '');
		var qty = qty.replace('.', '');
		var qty = qty.replace('.', '');
		var qty = qty.replace('.', '');
		var qty = qty.replace(',', '.');
		//alert(harga);


		var total = (parseFloat(qty) * parseFloat(harga)) - ((parseFloat(qty) * parseFloat(harga)) * (parseFloat(diskon) / 100));
		var totals = new Intl.NumberFormat('de-DE', {
			minimumFractionDigits: 2
		}).format(total);


		if (isNaN(total)) {
			//var total_num = Number((total).toFixed(1)).toLocaleString();
			//$('#subttl').html('0');
			$('#total_' + fl).val('0');
		} else {
			//var total_num = Number((total).toFixed(1)).toLocaleString();
			//$('#subttl').html(total);
			$('#total_' + fl).val(totals);
		}

		var sub_total = 0;
		var sub_total_bd = 0;
		var sub_disk = 0;
		var int_val = parseInt($('#int_flo').val());

		for (var yo = 0; yo <= int_val; yo++) {

			if ($('#kode_' + yo).val() !== undefined) {

				var tots = $('#total_' + yo).val();
				var tots = tots.replace('.', '');
				var tots = tots.replace('.', '');
				var tots = tots.replace('.', '');
				var tots = tots.replace('.', '');
				var tots = tots.replace(',', '.');

				var prcs = $('#harga_' + yo).val();
				var prcs = prcs.replace('.', '');
				var prcs = prcs.replace('.', '');
				var prcs = prcs.replace('.', '');
				var prcs = prcs.replace('.', '');
				var prcs = prcs.replace(',', '.');

				var qtys = $('#qty_' + yo).val();
				var qtys = qtys.replace('.', '');
				var qtys = qtys.replace('.', '');
				var qtys = qtys.replace('.', '');
				var qtys = qtys.replace('.', '');
				var qtys = qtys.replace(',', '.');

				var diskons = $('#diskon_' + yo).val();
				var diskons = diskons.replace('.', '');
				var diskons = diskons.replace('.', '');
				var diskons = diskons.replace('.', '');
				var diskons = diskons.replace('.', '');
				var diskons = diskons.replace(',', '.');



				sub_total = sub_total + parseFloat(tots);
				sub_total_bd = sub_total_bd + (parseFloat(prcs) * parseFloat(qtys));
				sub_disk = sub_disk + ((parseFloat(prcs) * parseFloat(qtys)) * (parseFloat(diskons) / 100));
			}

		}

		var total_ppn = (sub_total * (parseFloat($('#ppn').val()) / 100));
		var grand_total = sub_total + (sub_total * (parseFloat($('#ppn').val()) / 100));



		var gt = new Intl.NumberFormat('de-DE', {
			minimumFractionDigits: 2
		}).format(grand_total);
		var sub_totals = new Intl.NumberFormat('de-DE', {
			minimumFractionDigits: 2
		}).format(sub_total);
		var total_ppns = new Intl.NumberFormat('de-DE', {
			minimumFractionDigits: 2
		}).format(total_ppn);
		var total_bds = new Intl.NumberFormat('de-DE', {
			minimumFractionDigits: 2
		}).format(sub_total_bd);
		var total_sub_disk = new Intl.NumberFormat('de-DE', {
			minimumFractionDigits: 2
		}).format(sub_disk);

		$('#subttl').html(total_bds);
		$('#subttl2').html(sub_totals);
		$('#subttl3').html(total_sub_disk);
		$('#vatppn').html(total_ppns);
		$('#grand_total').html(gt);



		//change_sum_as(fl);
		//alert(qty+' '+harga+' '+diskon);

	}

	function back() {

		window.location.href = "<?php echo base_url() . 'invoice_buying'; ?>";

	}

	function term_other() {

		//alert($('#term').val());

		if ($('#term').val() == "other") {
			//$('#term_o').show();
			$("#term_o").prop("readonly", false);


		} else {
			// $('#term_o').hide();

			$("#term_o").prop("readonly", true);
		}

	}

	function listdist() {
		var user_id = '0001';
		var token = '093940349';


		$('#datatable_pricipal').DataTable({
			//"dom": 'rtip',
			"bFilter": false,
			"aaSorting": [],
			"bLengthChange": true,
			'iDisplayLength': 10,
			"sPaginationType": "simple_numbers",
			"Info": false,
			"processing": true,
			"serverSide": true,
			"destroy": true,
			"ajax": "<?php echo base_url() . 'principal_management/lists' ?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token,
			"searching": true,
			"language": {
				"decimal": ",",
				"thousands": "."
			},
			"dom": 'l<"toolbar">frtip',
			"initComplete": function() {
				$("div.toolbar").prepend('<div class="btn-group pull-left"><button type="button" class="btn btn-custon-rounded-two btn-primary" > Tambah </button></div>');
			}
		});
	}

	var last_no_inv = $('#no_inv').val();
	$('#no_inv').on('input',function(event) {
		if($('#no_inv').val() != last_no_inv) {
			no_inv_check();
		}
	});

	function no_inv_check() {
		var no_inv = $('#no_inv').val();
		if(no_inv.length > 3) {
			var post_data = {
				'no_inv': no_inv
			};

			$('#no_inv_tick').empty();
			$('#no_inv_tick').hide();
			$('#no_inv_loading-us').show();
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url('invoice_buying/check_no_inv');?>",
				data: post_data,
				cache: false,
				success: function(response){
					if(response.success == true){
						$('#no_inv').css('border', '3px #090 solid');
						$('#no_inv_loading-us').hide();
						$('#no_inv_tick').empty();
						$("#no_inv_tick").append('<span class="fa fa-check"> '+response.message+'</span>');
						$('#no_inv_tick').show();
					}else {
						$('#no_inv').css('border', '3px #C33 solid');
						$('#no_inv_loading-us').hide();
						$('#no_inv_tick').empty();
						$("#no_inv_tick").append('<span class="fa fa-close"> '+response.message+'</span>');
						$('#no_inv_tick').show();
					}
				}
			});
		}else {
			$('#no_inv').css('border', '3px #C33 solid');
			$('#no_inv_loading-us').hide();
			$('#no_inv_tick').empty();
			$("#no_inv_tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
			$('#no_inv_tick').show();
		}
	}
	$(document).ready(function() {

		//$('#kode_0').select2();
		$('#no_pos').select2({
			placeholder: "Nomor PO"
		});
		//$('#name').select2();

		// $('.rupiah').priceFormat({
		// prefix: '',
		// centsSeparator: ',',
		// centsLimit: 2,
		// thousandsSeparator: '.'
		// });	

		$(".rupiah").inputFilter(function(value) {
			return /^-?\d*[,]?\d*$/.test(value);
		});

		// $('#tgl').datepicker({
		// format: "yyyy-mm-dd",
		// autoclose: true,
		// todayHighlight: true
		// });
		$('#tgl_inv').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		});
		$('#tgl_due').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		});
		$('#tgl_fak').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		});

		listdist();

		$('#add_po').on('submit', function(e) {
			// validation code here
			//if(!valid) {
			e.preventDefault();

			// var formData = new FormData(this);
			var formData = new FormData();
			var urls = $(this).attr('action');

			formData.append('kode', $('#kode').val());
			formData.append('tgl', $('#tgl').val());
			formData.append('ints', $('#ints').val());
			formData.append('ints2', $('#ints2').val());
			formData.append('name', $('#name').val());
			formData.append('ppn', $('#ppn').val());
			formData.append('term', $('#term').val());
			formData.append('term_o', $('#term_o').val());
			formData.append('ket', $('#ket').val());
			formData.append('int_flo', $('#int_flo').val());

			formData.append('no_inv', $('#no_inv').val());
			formData.append('tgl_inv', $('#tgl_inv').val());
			formData.append('tgl_due', $('#tgl_due').val());
			formData.append('ket_inv', $('#ket_inv').val());
			formData.append('no_pos', $('#no_pos').val());
			formData.append('fak_paj', $('#fak_paj').val());
			formData.append('tgl_fak', $('#tgl_fak').val());

			var int_val = parseInt($('#int_flo').val());

			for (var yo = 0; yo <= int_val; yo++) {

				if ($('#kode_' + yo).val() !== undefined) {

					formData.append('kode_' + yo, $('#kode_' + yo).val());
					formData.append('po_mat_' + yo, $('#po_mat_' + yo).val());
					formData.append('idpo_mat_' + yo, $('#idpo_mat_' + yo).val());
					formData.append('harga_' + yo, $('#harga_' + yo).val());
					formData.append('total_' + yo, $('#total_' + yo).val());
					formData.append('qty_' + yo, $('#qty_' + yo).val());
					//	formData.append('remark_'+yo, $('#remark_'+yo).val());
					formData.append('diskon_' + yo, $('#diskon_' + yo).val());

				}

			}

			swal({
				title: 'Yakin akan Simpan Data ?',
				text: '',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak'
			}).then(function() {


				//console.log(formData);

				$.ajax({
					type: 'POST',
					url: urls,
					data: formData,
					cache: false,
					contentType: false,
					processData: false,
					success: function(response) {

						//console.log(response);
						// if(response.success == true){
						// $('#ModalAddPo').modal('show');
						// }else{
						// $('#msg_err').html(response.message);
						// $('#ModalChangePo').modal('show');
						// }

						if (response.success == true) {
							//$('#ModalAddPo').modal('show');

							swal({
								title: 'Success!',
								text: "Apakah Anda Akan Input Data Invoice Lagi ?",
								type: 'success',
								showCancelButton: true,
								confirmButtonText: 'Ya',
								cancelButtonText: 'Tidak'
							}).then(function() {
								//$("#add_material").form('reset');
								$('#add_po').trigger("reset");
								$('#no_pos').html(response.new_po);
								$('#table_items').html('');
								// $('#no_pos').val("0");
								$("#no_pos").select2("val", "");
							}, function(dismiss) {
								back();
							})

						} else {
							// $('#msg_err').html(response.message);
							// $('#ModalChangePo').modal('show');

							swal({
								title: 'Data Inputan Tidak Sesuai !! ',
								text: response.message,
								type: 'error',
								showCancelButton: true,
								confirmButtonText: 'Ok'
							}).then(function() {


							});


						}

					}
				});

			});
			//alert(kode);


			//}
		});
	});
</script>