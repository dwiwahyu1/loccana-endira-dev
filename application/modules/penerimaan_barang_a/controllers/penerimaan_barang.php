<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Penerimaan_Barang extends MX_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('penerimaan_barang/penerimaan_barang_model');
    $this->load->library('log_activity');
    $this->load->library('formatnumbering');
  }

  /**
   * anti sql injection
   */
  public function Anti_sql_injection($string) {
    $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
    return $string;
  }

  public function index() {
    $this->template->load('maintemplate', 'penerimaan_barang/views/index');
  }

  function lists() {
    $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
    $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
    $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
    $order = $this->input->get_post('order');
    $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
    $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

    $order_fields = array('', 'stock_code', 'stock_name');

    $search = $this->input->get_post('search');

    $search_val = (!empty($search['value'])) ? $search['value'] : null;

    $search_value = $this->Anti_sql_injection($search_val);

    // Build params for calling model
    $params['limit'] = (int) $length;
    $params['offset'] = (int) $start;
    $params['order_column'] = $order_fields[$order_column];
    $params['order_dir'] = $order_dir;
    $params['searchtxt'] = $search_value;

    $list = $this->penerimaan_barang_model->lists($params);
    $result["recordsTotal"] = $list['total'];
    $result["recordsFiltered"] = $list['total_filtered'];
    $result["draw"] = $draw;

    $data = array();
    //$i = 0;
    $i = $params['offset'];
    $username = $this->session->userdata['logged_in']['username'];
    foreach ($list['data'] as $k => $v) {
      $i = $i + 1;
      $button_detail = '<div class="btn-group"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit BPB" onClick="editmaterial(\'' . $v['id_bpb'] . '\')"><i class="fa fa-edit"></i></button></div>';
      $button_edit = '<div class="btn-group"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit BPB" onClick="editmaterial(\'' . $v['id_bpb'] . '\')"><i class="fa fa-edit"></i></button></div>';
      $button_delete = '<div class="btn-group"><button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete BPB" onClick="deletematerial(\'' . $v['id_bpb'] . '\')"><i class="fa fa-trash"></i></button></div>';
      $button_approval = '<div class="btn-group"><button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Approve BPB" onClick="approvematerial(\'' . $v['id_bpb'] . '\')"><i class="fa fa-check"></i></button></div>';
      // if($v['status_harga']==0){
      //   $status = 'Konfirmasi';
      if ($v['status'] == 1){
        $button_approval = '';
        $button_edit = '';
        $button_delete = '';

      }
      $button = $button_edit . $button_delete . $button_approval;
      // }
      // else {
      $status = 'Setuju';
      //   $button = $button_edit;
      // }
      array_push($data, array(
        $i,
        $v['tanggal'],
        $v['no_po'],
        $v['name_eksternal'],
        $button
      ));
    }

    $result["data"] = $data;

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }
  public function edit() {
    $data = array(
      'id_bpb' => $this->Anti_sql_injection($this->input->post('iditems', TRUE)),
    );
    $result = $this->penerimaan_barang_model->get_bpb($data['id_bpb']);
    $listpo = $this->penerimaan_barang_model->get_po($data);

    $data = array(
      'stok'         => $result,
      'po'           => $listpo
    );

    $this->template->load('maintemplate', 'penerimaan_barang/views/edit_modal_view', $data);
  }
  public function add() {
    $result = $this->penerimaan_barang_model->get_po();

    $data = array(
      'po'         => $result,
    );

    $this->template->load('maintemplate', 'penerimaan_barang/views/add_modal_view', $data);
  }
  public function get_detail_po() {
    $data   = file_get_contents("php://input");
    $params = json_decode($data, true);

    $explode = explode(',', $this->Anti_sql_injection($params['po']));

    $var = array(
      'id_po' => $explode[1],
    );

    $list = $this->penerimaan_barang_model->get_detail_po($var['id_po']);
	
	//print_r($list);die;	
    if (empty($list)) {
      $data = array();
      $res = array(
        'success'  => false,
        'message'  => 'Data PO tidak ditemukan',
        'data'    => $data
      );
    } else {
      $data = array();
      $tempObj = new stdClass();

      if (sizeof($params['tempPO']) > 0) {
        foreach ($params['tempPO'] as $p => $pk) {
          for ($i = 0; $i <= sizeof($list); $i++) {
            if (!empty($list[$i])) {
              if ($list[$i]['no_po'] == $pk[1] && $list[$i]['no_po'] == $pk[2]) {
                unset($list[$i]);
              }
            }
          }
        }
      }

      $i = 0;
      foreach ($list as $k => $v) {
        if ($v['diskon'] != 0 || $v['diskon'] != '') {
          $diskon = $v['diskon'];
        } else {
          $diskon = 0;
        }
        if ($v['no_po'] != 0 || $v['no_po'] != '') {
          $po = $v['no_po'];
        } else {
          $po = '-';
        }
        $strQty =
          '<input type="number" class="form-control" min="0" id="qty_diterima' . $i . '" name="qty_diterima[]' . $i . '" onInput="valQty(' . $i . ')" style="height:25px; width: 100px;" value="' . $v['qty_diterima'] . '">';
        array_push($data, array(
          // $v['id_t_ps'],
          // $v['id_po'],
          // $v['id_material'],
          $v['no_po'] . ' - ' . $v['name_eksternal'],
          $v['stock_code'],
          $v['stock_name'].' '.$v['base_qty'].' '.$v['uom_symbol'],
          '(' . $v['uom_symbol'] . ') - ' . $v['uom_name'],
          $this->formatnumbering->qty($v['qty_sisa']),
          //number_format($v['qty_btb'],0,'.','.'),
          $strQty,
          // $status,
          $this->formatnumbering->price($v['unit_price']),
          // $v['notes_po'],
          // $strOption
        ));
        $i++;
      }

      $res = array(
        'success'  => true,
        'status'  => 'success',
        'data'    => $data
      );
    }

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }
  public function get_detail_po_when_edit() {
    $data   = file_get_contents("php://input");
    $params = json_decode($data, true);

    $explode = explode(',', $this->Anti_sql_injection($params['po']));

    $var = array(
      'id_po' => $explode[1],
    );

    $list = $this->penerimaan_barang_model->get_detail_po_when_edit($var['id_po']);
    if (empty($list)) {
      $data = array();
      $res = array(
        'success'  => false,
        'message'  => 'Data PO tidak ditemukan',
        'data'    => $data
      );
    } else {
      $data = array();
      $tempObj = new stdClass();

      if (sizeof($params['tempPO']) > 0) {
        foreach ($params['tempPO'] as $p => $pk) {
          for ($i = 0; $i <= sizeof($list); $i++) {
            if (!empty($list[$i])) {
              if ($list[$i]['no_po'] == $pk[1] && $list[$i]['no_po'] == $pk[2]) {
                unset($list[$i]);
              }
            }
          }
        }
      }

      $i = 0;
      foreach ($list as $k => $v) {
        if ($v['diskon'] != 0 || $v['diskon'] != '') {
          $diskon = $v['diskon'];
        } else {
          $diskon = 0;
        }
        if ($v['no_po'] != 0 || $v['no_po'] != '') {
          $po = $v['no_po'];
        } else {
          $po = '-';
        }
        $strQty =
          '<input type="number" class="form-control" min="0" id="qty_diterima' . $i . '" name="qty_diterima[]' . $i . '" onInput="valQty(' . $i . ')" style="height:25px; width: 100px;" value="' . $v['qty_diterima'] . '">';
        array_push($data, array(
          // $v['id_t_ps'],
          // $v['id_po'],
          // $v['id_material'],
          $v['no_po'] . ' - ' . $v['name_eksternal'],
          $v['stock_code'],
          $v['stock_name'],
          '(' . $v['uom_symbol'] . ') - ' . $v['uom_name'],
          $this->formatnumbering->qty($v['qty_sisa']),
          //number_format($v['qty_btb'],0,'.','.'),
          $strQty,
          // $status,
          $this->formatnumbering->price($v['unit_price']),
          // $v['notes_po'],
          // $strOption
        ));
        $i++;
      }

      $res = array(
        'success'  => true,
        'status'  => 'success',
        'data'    => $data
      );
    }

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }
  public function detail() {
    $data = array(
      'id' => $this->Anti_sql_injection($this->input->post('iditems', TRUE)),
    );
    $result = $this->penerimaan_barang_model->get_material($data['id']);

    // $unit = $this->penerimaan_barang_model->get_unit();?
    // $Gudang = $this->penerimaan_barang_model->get_distributor();

    $data = array(
      'stok'         => $result,
      // 'unit' 				=> $unit,
      // 'Gudang' 			=> $Gudang
    );

    $this->template->load('maintemplate', 'penerimaan_barang/views/mutasi_view', $data);
  }

  public function add_bpb() {
    $this->form_validation->set_rules('no_do', 'Nomor DO', 'trim|required');
    $this->form_validation->set_rules('angkutan', 'Nomor Angkutan', 'trim|required');
    $this->form_validation->set_rules('no_polisi', 'Nomor Polisi', 'trim|required');
    $this->form_validation->set_rules('no_po', 'Nomor PO', 'trim|required');
    $this->form_validation->set_rules('tanggal_btb', 'Tanggal BTB', 'trim|required');
    $this->form_validation->set_rules('qty_diterima[]', 'QTY Diterima', 'trim|required');
    // $this->form_validation->set_rules('id_t_sp[]', 'Detail Purchase Order', 'trim|required');


    // print_r(preg_replace("/[^0-9\,]/","",$this->Anti_sql_injection($this->input->post('arrTemp')[0])));
    // preg_replace("/[^0-9\,]/", "", $v[10]);

    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));

      $result = array('success' => false, 'message' => $msg);
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      $no_do = $this->Anti_sql_injection($this->input->post('no_do', TRUE));
      $angkutan = $this->Anti_sql_injection($this->input->post('angkutan', TRUE));
      $no_polisi = $this->Anti_sql_injection($this->input->post('no_polisi', TRUE));
      $tanggal_btb =  $this->Anti_sql_injection($this->input->post('tanggal_btb', TRUE));
      $id_po =  $this->Anti_sql_injection($this->input->post('no_po', TRUE));
      $qty_diterima =  ($this->input->post('qty_diterima', TRUE));
      // $id_t_sp =  ($this->input->post('id_t_sp', TRUE));
      $data = array();
      for ($i = 0; $i < count($qty_diterima); $i++) {
        $data_sub = array(
          'no_do'           => $no_do,
          'angkutan'        => $angkutan,
          'no_polisi'       => $no_polisi,
          'tanggal_btb'     => date('Y-m-d H:i:s', strtotime($tanggal_btb . ' ' . date('H:i:s'))),
          'id_po'           => $id_po,
          'id_pic'          => $this->session->userdata['logged_in']['user_id'],
          // 'id_principal'    => $get_principal_data[0]['id'],
          // 'id_t_sp'         => $id_t_sp[$i],
          'qty_diterima'    => $qty_diterima[$i]
        );
        array_push($data, $data_sub);
      }
      // echo "<pre>";
      // print_r($data);
      // die;
      $result = $this->penerimaan_barang_model->add_bpb($data);
      
	  
      if (($result['result']) > 0) {
        $msg = 'Berhasil insert data bpb ke database';

        $this->log_activity->insert_activity('insert', $msg . ' dengan id PO ' . $id_po);
        $results = array(
          'success' => true,
          'message' => $msg
        );
      } else {
        $msg = 'Gagal insert data bpb ke database';

        $this->log_activity->insert_activity('insert', $msg . ' dengan id PO = ' . $id_po);
        $results = array(
          'success' => false,
          'message' => $msg
        );
      }

      $this->output->set_content_type('application/json')->set_output(json_encode($results), true);
    }
  }
  public function edit_bpb() {
    $this->form_validation->set_rules('id_bpb', 'Nomor DO', 'trim|required');
    $this->form_validation->set_rules('no_do', 'Nomor DO', 'trim|required');
    $this->form_validation->set_rules('angkutan', 'Nomor Angkutan', 'trim|required');
    $this->form_validation->set_rules('no_polisi', 'Nomor Polisi', 'trim|required');
    $this->form_validation->set_rules('no_po', 'Nomor PO', 'trim|required');
    $this->form_validation->set_rules('tanggal_btb', 'Tanggal BTB', 'trim|required');
    $this->form_validation->set_rules('qty_diterima[]', 'QTY Diterima', 'trim|required');
    // $this->form_validation->set_rules('id_t_sp[]', 'Detail Purchase Order', 'trim|required');


    // print_r(preg_replace("/[^0-9\,]/","",$this->Anti_sql_injection($this->input->post('arrTemp')[0])));
    // preg_replace("/[^0-9\,]/", "", $v[10]);

    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));

      $result = array('success' => false, 'message' => $msg);
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      $id_bpb = $this->Anti_sql_injection($this->input->post('id_bpb', TRUE));
      $no_do = $this->Anti_sql_injection($this->input->post('no_do', TRUE));
      $angkutan = $this->Anti_sql_injection($this->input->post('angkutan', TRUE));
      $no_polisi = $this->Anti_sql_injection($this->input->post('no_polisi', TRUE));
      $tanggal_btb =  $this->Anti_sql_injection($this->input->post('tanggal_btb', TRUE));
      $id_po =  $this->Anti_sql_injection($this->input->post('no_po', TRUE));
      $qty_diterima =  ($this->input->post('qty_diterima', TRUE));
      // $id_t_sp =  ($this->input->post('id_t_sp', TRUE));
      $data = array();
      for ($i = 0; $i < count($qty_diterima); $i++) {
        $data_sub = array(
          'id_bpb'          => $id_bpb,
          'no_do'           => $no_do,
          'angkutan'        => $angkutan,
          'no_polisi'       => $no_polisi,
          'tanggal_btb'     => date('Y-m-d H:i:s', strtotime($tanggal_btb . ' ' . date('H:i:s'))),
          'id_po'           => $id_po,
          'id_pic'          => $this->session->userdata['logged_in']['user_id'],
          // 'id_principal' => $get_principal_data[0]['id'],
          // 'id_t_sp'      => $id_t_sp[$i],
          'qty_diterima'    => $this->Anti_sql_injection($qty_diterima[$i])
        );
        array_push($data, $data_sub);
      }
      // echo "<pre>";
      // print_r($data);
      // die;
      $result = $this->penerimaan_barang_model->edit_bpb($data);
      if (($result['result']) > 0) {
        $msg = 'Berhasil edit data bpb ke database';

        $this->log_activity->insert_activity('insert', $msg . ' dengan id BPB  = ' . $id_bpb);
        $results = array(
          'success' => true,
          'message' => $msg
        );
      } else {
        $msg = 'Gagal edit data bpb ke database';

        $this->log_activity->insert_activity('insert', $msg . ' dengan id BPB = ' . $id_bpb);
        $results = array(
          'success' => false,
          'message' => $msg
        );
      }

      $this->output->set_content_type('application/json')->set_output(json_encode($results), true);
    }
  }
  public function approve_bpb() {
    $data   = file_get_contents("php://input");
    $params   = json_decode($data, true);
    $id_bpb = $this->Anti_sql_injection($params['id']);
    $list = $this->penerimaan_barang_model->approve_bpb($id_bpb);
    $this->penerimaan_barang_model->add_hpp($id_bpb);
    if ($list > 0) {
      $msg = 'Berhasil Approve BPB';
      $this->log_activity->insert_activity('insert', $msg . ' ke database dengan id : ' . $id_bpb);
      $res = array('status' => 'success', 'message' => 'Data telah di approve');
    } else {
      $msg = 'Gagal Approve BPB';
      $this->log_activity->insert_activity('insert', $msg . '  dengan id : ' . $id_bpb);
      $res = array('status' => 'failed', 'message' => 'Data gagal  di approve');
    }

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }
  public function delete_bpb() {
    $data   = file_get_contents("php://input");
    $params   = json_decode($data, true);

    $list = $this->penerimaan_barang_model->delete_bpb($this->Anti_sql_injection($params['id']));

    if ($list > 0) {
      $this->log_activity->insert_activity('delete', 'Berhasil Delete Permintaan Barang');
      $res = array('status' => 'success', 'message' => 'Data telah di hapus');
    } else {
      $this->log_activity->insert_activity('delete', 'Gagal Delete Permintaan Barang');
      $res = array('status' => 'success', 'message' => 'Data gagal di hapus');
    }

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }
}
