<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penerimaan_barang extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('penerimaan_barang/purchase_order_model');
		$this->load->library('log_activity');
		$this->load->library('priv');

		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong');
		$this->form_validation->set_message('min_length', '%s Minimal 4 Karakter');
		$this->form_validation->set_message('max_length', '%s Maksimal 10 Karakter');
	}

	/**
	 * anti sql injection
	 * @return string
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	 * This function is redirect to index distributor page
	 * @return Void
	 */
	public function index() {

		// var_dump($this->db->get('t_purchase_order')->result());
		// die;

		$priv = $this->priv->get_priv();

		$year_count = date('Y') - 2019;

		$filter_year = '<select class="form-control" id="filter_year" onChange="changeFilter()"><option value="ALL" >ALL</option>';

		for ($i = $year_count; $i >= 0; $i--) {

			$y = 2019 + $i;

			if ($y == date('Y')) {
				$filter_year .= '<option value="' . $y . '" selected="selected" >' . $y . '</option>';
			} else {
				$filter_year .= '<option value="' . $y . '" >' . $y . '</option>';
			}
		}
		$filter_year .= '</select>';

		$array_month = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

		$filter_month = '<select class="form-control" id="filter_month" onChange="changeFilter()"><option value="ALL" >ALL</option>';
		foreach ($array_month as $array_months) {
			if ($array_months == date('F')) {
				$filter_month .= '<option value="' . $array_months . '" selected="selected">' . $array_months . '</option>';
			} else {
				$filter_month .= '<option value="' . $array_months . '" >' . $array_months . '</option>';
			}
		}

		$filter_month .= '</select>';
		$today_month = date('F');
		$today_year = date('Y');
		$total_amount = number_format((float)$this->purchase_order_model->mtd_pembelian(date('F'), date('Y'))[0]['mtd_pembelian'], 2, ',', '.');
		// $total_amount= 0;
		$total_month = '
		<div class="analytics-content pull-right">
			<h5>Total Per Bulan </h5>
			<h3>Rp 
				<span class="counter">
					<label id="mtd_pembelian" >' .
			$total_amount .
			'</label>
				</span> 
				<span class="tuition-fees">
				</span>
			</h2>
		</div>';
		$data = array(
			'priv' => $priv,
			'filter_month' => $filter_month,
			'filter_year' => $filter_year,
			'today_month' => $today_month,
			'today_year' => $today_year,
			'total_month' => $total_month
		);

		$this->template->load('maintemplate', 'penerimaan_barang/views/index', $data);
	}

	/**
	 * This function is used for showing distributor list
	 * @return Array
	 */
	function lists() {

		if (!empty($_GET['sess_user_id'])) {
			$sess_user_id = $_GET['sess_user_id'];
		} else {
			$sess_user_id = NULL;
		}

		if (!empty($_GET['sess_token'])) {
			$sess_token = $_GET['sess_token'];
		} else {
			$sess_token = NULL;
		}

		if (!empty($_GET['filter'])) {
			$filter = $_GET['filter'];
		} else {
			$filter = NULL;
		}

		if (!empty($_GET['filter_year'])) {
			$filter_year = $_GET['filter_year'];
		} else {
			$filter_year = NULL;
		}

		if ($this->input->get_post('draw') != FALSE) {
			$draw   = $this->input->get_post('draw');
		} else {
			$draw   = 1;
		};
		if ($this->input->get_post('length') != FALSE) {
			$length = $this->input->get_post('length');
		} else {
			$length = 10;
		};
		if ($this->input->get_post('start') != FALSE) {
			$start  = $this->input->get_post('start');
		} else {
			$start  = 0;
		};
		$order_fields = array('id_bpb', 'no_do', 'tanggal', 'no_po', 'name_eksternal', 'date_po', 'harga_t', 'value_t', 'status', 'status'); // , 'COST'
		$order = $this->input->get_post('order');
		if (!empty($order[0]['dir'])) {
			$order_dir    = $order[0]['dir'];
		} else {
			$order_dir    = 'desc';
		};
		if (!empty($order[0]['column'])) {
			$order_column = $order[0]['column'];
		} else {
			$order_column = 0;
		};

		if (!empty($_GET['search']['value'])) {
			$search_value = $_GET['search']['value'];
		} else {
			$search_value = NULL;
		}
		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['sess_user_id'] = $sess_user_id;
		$params['sess_token'] 	= $sess_token;
		$params['filter'] 		= $_GET['filter'];
		$params['filter_year'] 		= $_GET['filter_year'];
		$params['searchtxt'] 	= $search_value;

		// print_r($params);die;
		$priv = $this->priv->get_priv();

		$list = $this->purchase_order_model->list_po($params);
		//print_r($list['data']);die;
		$data = array();
		foreach ($list['data'] as $k => $v) {

			if ($v['status'] == 0) {
				$sts = '<span type="button" class="btn btn-custon-rounded-two btn-danger"  > Konfirmasi </button>';

				if ($v['no_invoice'] == NULL) {
					$action = '
							<div class="btn-group" style="display:' . $priv['update'] . '"><button type="button" class="btn btn-custon-rounded-two btn-warning" data-toggle="tooltip" data-placement="top" title="Edit"  onClick="updatepo(' . $v['id_bpb'] . ')"  > <i class="fa fa-edit"></i> </button></div>
							<div class="btn-group" style="display:' . $priv['delete'] . '"><button type="button" class="btn btn-custon-rounded-two btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"  onClick="deletepo(' . $v['id_bpb'] . ')"  > <i class="fa fa-trash"></i> </button></div>
							<div class="btn-group" style="display:' . $priv['detail'] . '"><button type="button" class="btn btn-custon-rounded-two btn-info" data-toggle="tooltip" data-placement="top" title="Detail" onClick="detail(' . $v['id_bpb'] . ')"  > <i class="fa fa-search-plus"></i>  </button></div>
							';
				} else {
					$action = '
							<div class="btn-group" style="display:' . $priv['update'] . '"><button type="button" class="btn btn-custon-rounded-two btn-warning" data-toggle="tooltip" data-placement="top" title="Edit"  onClick="updatepo(' . $v['id_bpb'] . ')"  > <i class="fa fa-edit"></i> </button></div>
							<div class="btn-group" style="display:' . $priv['detail'] . '"><button type="button" class="btn btn-custon-rounded-two btn-info" data-toggle="tooltip" data-placement="top" title="Detail" onClick="detail(' . $v['id_bpb'] . ')"  > <i class="fa fa-search-plus"></i>  </button></div>
							';
				}
			} else {
				$sts = '<button type="button" class="btn btn-custon-rounded-two btn-primary" disabled > Disetujui </button>';
				$action = '<div class="btn-group" style="display:' . $priv['detail'] . '"><button type="button" class="btn btn-custon-rounded-two btn-info" data-toggle="tooltip" data-placement="top" title="Detail" onClick="detail(' . $v['id_bpb'] . ')" > <i class="fa fa-search-plus"></i>  </button></div>
						';
			}





			array_push(
				$data,
				array(
					number_format($v['Rangking'], 0, ',', '.'),
					$v['no_do'],
					$v['tanggal'],
					$v['no_po'],
					$v['name_eksternal'],
					$v['date_po'],
					number_format($v['harga_t'], 2, ',', '.'),
					number_format($v['diskon_t'], 2, ',', '.'),
					number_format($v['value_t'], 2, ',', '.'),
					//$sts,
					$action
				)
			);
			//$idx++;
		}

		$result["data"] = $data;
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		$result["mtd_pembelian"] = number_format((float)$this->purchase_order_model->mtd_pembelian($_GET['filter'], $_GET['filter_year'])[0]['mtd_pembelian'], 2, ',', '.');
		//$result["data"] = $list['data'];

		echo json_encode($result);

		//$this->json_result($result);

	}

	/**
	 * This function is redirect to add distributor page
	 * @return Void
	 */

	public function get_principal() {

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$result['principal'] = $this->purchase_order_model->get_principal_byid($params);
		$item = $this->purchase_order_model->get_item_byprin($params);

		$list = '<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		foreach ($item as $items) {

			$list .= '<option value="' . $items['id_mat'] . '" >' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '</option>';
		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function get_po_detail() {

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$result['invoice'] = $this->purchase_order_model->get_po_byid($params);
		$result['id_pos'] = $params['id'];
		$Date = $result['invoice'][0]['date_po'];
		$result['due_date'] = date('Y-m-d', strtotime($Date . ' + ' . $result['invoice'][0]['term_of_payment'] . ' days'));
		//print_r($result['invoice']);die;

		$item = $this->purchase_order_model->get_po_detail_full($params);
		//print_r($item);die;

		$htl = '';
		$new_fl = 0;
		$total_amount = 0;
		$sub_total_amount = 0;
		$sub_diskon = 0;
		foreach ($item as $items) {
			//
			$htl .=	'<div id="row_' . $new_fl . '" >';
			$htl .=	'	<div class="row" style="border-top-style:solid;">';
			$htl .=	'	<div style="margin-top:10px">';
			$htl .=	'		<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="kode_' . $new_fl . '" id="kode_' . $new_fl . '" type="hidden" class="form-control" placeholder="Qty" value="' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '" readOnly>';
			$htl .=	'				<input name="po_mat_' . $new_fl . '" id="po_mat_' . $new_fl . '" type="hidden" class="form-control" placeholder="Qty" value="' . $items['id_t_ps'] . '" readOnly  >';
			$htl .=	'				<input name="idm_' . $new_fl . '" id="idm_' . $new_fl . '" type="hidden" class="form-control" placeholder="Qty" value="' . $items['idm'] . '" readOnly  >';
			$htl .=	'				<input name="unit_box_' . $new_fl . '" id="unit_box_' . $new_fl . '" type="hidden" class="form-control" placeholder="Qty" value="' . $items['unit_box'] . '" readOnly>';
			$htl .=	'				<textarea style="resize:none; height:94px;" readonly>' . $items['stock_code'] . '&#13;&#10;' . $items['stock_name'] . '&#13;&#10;' . $items['base_qty'] . '' . $items['uom_symbol'] . '</textarea>';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				Box @ ' . $items['unit_box'] . '';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="qtyOrder_' . $new_fl . '" id="qtyOrder_' . $new_fl . '" type="text" class="form-control " onKeydown="change_sum(' . $new_fl . ')" placeholder="Qty" value="' . number_format($items['qty_order'], 3, ',', '.') . '" readOnly  >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="qtySisa_' . $new_fl . '" id="qtySisa_' . $new_fl . '" type="text" class="form-control " onKeyup="change_sum(' . $new_fl . ')" placeholder="Sisa" value="' . number_format($items['qty_sisa'], 3, ',', '.') . '" readOnly >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="qtyTerima_' . $new_fl . '" id="qtyTerima_' . $new_fl . '" type="text" class="form-control" onKeyup="change_sum(' . $new_fl . ')" placeholder="Diterima" value="' . number_format($items['qty_order'] - $items['qty_sisa'], 3, ',', '.') . '" readOnly >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="qty_' . $new_fl . '" id="qty_' . $new_fl . '" type="text" class="form-control" placeholder="Total" onKeyup="change_qty(' . $new_fl . ')" value="0,00"  >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="qtyBonus_' . $new_fl . '" id="qtyBonus_' . $new_fl . '" type="text" class="form-control" placeholder="Total" onKeyup="change_qty(' . $new_fl . ')" value="0,00"  >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="qtyTitip_' . $new_fl . '" id="qtyTitip_' . $new_fl . '" type="text" class="form-control" placeholder="Total" onKeyup="change_qty(' . $new_fl . ')" value="0,00"  >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="diskon_' . $new_fl . '" id="diskon_' . $new_fl . '" type="text" class="form-control" placeholder="Diskon" onKeyup="change_qty(' . $new_fl . ')" value="0,00"  >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="note_' . $new_fl . '" id="note_' . $new_fl . '" type="text" class="form-control" placeholder="Note" value=""  >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'	</div>';
			$htl .=	'	</div>';
			$htl .=	'</div>';

			$new_fl++;

			$total_amount = $total_amount + ((floatval($items['unit_price']) * floatval($items['qty_order'])) - ((floatval($items['unit_price']) * floatval($items['qty_order'])) * (floatval($items['diskon']) / 100)));
			$sub_total_amount = $sub_total_amount + ((floatval($items['unit_price']) * floatval($items['qty_order'])));
			$sub_diskon = $sub_diskon + ((floatval($items['unit_price']) * floatval($items['qty_order'])) * (floatval($items['diskon']) / 100));
			//$sub_diskon = $sub_diskon + ((intval($items['unit_price'])*intval($items['qty_order']))*(floatval($items['diskon'])/100));

			//$total_amount = $total_amount + $items['price'];

		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';

		$result['list'] = $htl;
		$result['int_val'] = $new_fl;
		$result['total_amount'] = number_format($total_amount, 2, ',', '.');
		$result['sub_total_amount'] = number_format($sub_total_amount, 2, ',', '.');
		$result['sub_diskon'] = number_format($sub_diskon, 2, ',', '.');
		$result['sub_ppn'] = number_format($total_amount * (floatval($result['invoice'][0]['ppn']) / 100), 2, ',', '.');

		$result['grand_total'] = number_format(($total_amount * (floatval($result['invoice'][0]['ppn']) / 100)) + $total_amount, 2, ',', '.');

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function get_po_detail_detail() {

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$result['invoice'] = $this->purchase_order_model->get_po_byid($params);
		$result['id_pos'] = $params['id'];
		$Date = $result['invoice'][0]['date_po'];
		$result['due_date'] = date('Y-m-d', strtotime($Date . ' + ' . $result['invoice'][0]['term_of_payment'] . ' days'));
		//print_r($result['invoice']);die;

		$item = $this->purchase_order_model->get_po_detail_full($params);
		//print_r($item);die;

		$htl = '';
		$new_fl = 0;
		$total_amount = 0;
		$sub_total_amount = 0;
		$sub_diskon = 0;
		foreach ($item as $items) {

			$htl .=	'<div id="row_' . $new_fl . '" >';
			$htl .=	'									<div class="row" style="border-top-style:solid;">';
			$htl .=	'									<div style="margin-top:10px">';
			$htl .=	'										<div class="col-lg-5 col-md-5 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="kode_' . $new_fl . '" id="kode_' . $new_fl . '" type="text" class="form-control" placeholder="Qty" value="' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '" readOnly  >';
			$htl .=	'												<input name="po_mat_' . $new_fl . '" id="po_mat_' . $new_fl . '" type="hidden" class="form-control" placeholder="Qty" value="' . $items['id_t_ps'] . '" readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_' . $new_fl . '" id="qty_' . $new_fl . '" type="text" class="form-control rupiah" onKeydown="change_sum(' . $new_fl . ')" placeholder="Qty" value="' . number_format($items['qty_order'], 2, ',', '.') . '" readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="harga_' . $new_fl . '" id="harga_' . $new_fl . '" type="text" class="form-control rupiah" onKeyup="change_sum(' . $new_fl . ')" placeholder="Harga" value="' . number_format($items['unit_price'], 2, ',', '.') . '" readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="diskon_' . $new_fl . '" id="diskon_' . $new_fl . '" type="text" class="form-control rupiah" onKeyup="change_sum(' . $new_fl . ')" placeholder="Diskon" value="' . number_format($items['diskon'], 2, ',', '.') . '" readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="total_' . $new_fl . '" id="total_' . $new_fl . '" type="text" class="form-control rupiah" placeholder="Total" value="' . number_format(((floatval($items['unit_price']) * floatval($items['qty_order'])) - ((floatval($items['unit_price']) * floatval($items['qty_order'])) * (floatval($items['diskon']) / 100))), 2, ',', '.') . '" readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'									</div>';
			$htl .=	'									</div>';
			$htl .=	'								</div>';

			$new_fl++;

			$total_amount = $total_amount + ((floatval($items['unit_price']) * floatval($items['qty_order'])) - ((floatval($items['unit_price']) * floatval($items['qty_order'])) * (floatval($items['diskon']) / 100)));
			$sub_total_amount = $sub_total_amount + ((floatval($items['unit_price']) * floatval($items['qty_order'])));
			$sub_diskon = $sub_diskon + ((floatval($items['unit_price']) * floatval($items['qty_order'])) * (floatval($items['diskon']) / 100));
			//$sub_diskon = $sub_diskon + ((intval($items['unit_price'])*intval($items['qty_order']))*(floatval($items['diskon'])/100));

			//$total_amount = $total_amount + $items['price'];

		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';

		$result['list'] = $htl;
		$result['int_val'] = $new_fl;
		$result['total_amount'] = number_format($total_amount, 2, ',', '.');
		$result['sub_total_amount'] = number_format($sub_total_amount, 2, ',', '.');
		$result['sub_diskon'] = number_format($sub_diskon, 2, ',', '.');
		$result['sub_ppn'] = number_format($total_amount * (floatval($result['invoice'][0]['ppn']) / 100), 2, ',', '.');

		$result['grand_total'] = number_format(($total_amount * (floatval($result['invoice'][0]['ppn']) / 100)) + $total_amount, 2, ',', '.');

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function get_po_detail_edit() {

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$result['invoice'] = $this->purchase_order_model->get_po_byid($params);
		$result['id_pos'] = $params['id'];
		$Date = $result['invoice'][0]['date_po'];
		$result['due_date'] = date('Y-m-d', strtotime($Date . ' + ' . $result['invoice'][0]['term_of_payment'] . ' days'));
		//print_r($result['invoice']);die;

		$item = $this->purchase_order_model->get_po_detail_full_edit($params);
		// echo '<pre>'; print_r($item); die;

		$htl = '';
		$new_fl = 0;
		$total_amount = 0;
		$sub_total_amount = 0;
		$sub_diskon = 0;
		foreach ($item as $items) {

			$htl .=	'<div id="row_' . $new_fl . '" >';
			$htl .=	'	<div class="row" style="border-top-style:solid;">';
			$htl .=	'	<div style="margin-top:10px">';
			$htl .=	'		<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="kode_' . $new_fl . '" id="kode_' . $new_fl . '" type="hidden" class="form-control" placeholder="Qty" value="' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '" readOnly  >';
			$htl .=	'				<input name="po_mat_' . $new_fl . '" id="po_mat_' . $new_fl . '" type="hidden" class="form-control" placeholder="Qty" value="' . $items['id_bpb_detail'] . '" readOnly  >';
			$htl .=	'				<input name="idm_' . $new_fl . '" id="idm_' . $new_fl . '" type="hidden" class="form-control" placeholder="Qty" value="' . $items['idm'] . '" readOnly  >';
			$htl .=	'				<input name="unit_box_' . $new_fl . '" id="unit_box_' . $new_fl . '" type="hidden" class="form-control" placeholder="Qty" value="' . $items['unit_box'] . '" readOnly  >';
			$htl .=	'				<textarea style="resize:none; height:94px;" readonly>' . $items['stock_code'] . '&#13;&#10;' . $items['stock_name'] . '&#13;&#10;' . $items['base_qty'] . '' . $items['uom_symbol'] . '</textarea>';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				Box @ ' . $items['unit_box'] . '';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="qtyOrder_' . $new_fl . '" id="qtyOrder_' . $new_fl . '" type="text" class="form-control " onKeydown="change_sum(' . $new_fl . ')" placeholder="Qty" value="' . number_format($items['qty_order'], 3, ',', '.') . '" readOnly  >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="qtySisa_' . $new_fl . '" id="qtySisa_' . $new_fl . '" type="text" class="form-control " onKeyup="change_sum(' . $new_fl . ')" placeholder="Sisa" value="' . number_format($items['qty_sisa'], 3, ',', '.') . '" readOnly >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="qtyTerima_' . $new_fl . '" id="qtyTerima_' . $new_fl . '" type="text" class="form-control " onKeyup="change_sum(' . $new_fl . ')" placeholder="Diterima" value="' . number_format($items['qty_order'] - $items['qty_sisa'], 3, ',', '.') . '" readOnly >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="qty_' . $new_fl . '" id="qty_' . $new_fl . '" type="text" class="form-control " placeholder="Total" onKeyup="change_qty(' . $new_fl . ')" value="' . number_format($items['qty_bpb'], 3, ',', '.') . '"  >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="qtyBonus_' . $new_fl . '" id="qtyBonus_' . $new_fl . '" type="text" class="form-control " placeholder="Bonus" onKeyup="change_qty(' . $new_fl . ')" value="' . number_format($items['qty_bonus_bpb'], 3, ',', '.') . '"  >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="qtyTitip_' . $new_fl . '" id="qtyTitip_' . $new_fl . '" type="text" class="form-control " placeholder="Titipan" onKeyup="change_qty(' . $new_fl . ')" value="' . number_format($items['qty_titip_bpb'], 3, ',', '.') . '"  >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="diskon_' . $new_fl . '" id="diskon_' . $new_fl . '" type="text" class="form-control " placeholder="Titipan" onKeyup="change_qty(' . $new_fl . ')" value="' . number_format($items['discount'] ?? 0, 3, ',', '.') . '"  >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="note_' . $new_fl . '" id="note_' . $new_fl . '" type="text" class="form-control" placeholder="Note" value="' . $items['ket_bpb'] . '"  >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'	</div>';
			$htl .=	'	</div>';
			$htl .=	'</div>';

			$new_fl++;

			$total_amount = $total_amount + ((floatval($items['unit_price']) * floatval($items['qty_order'])) - ((floatval($items['unit_price']) * floatval($items['qty_order'])) * (floatval($items['diskon']) / 100)));
			$sub_total_amount = $sub_total_amount + ((floatval($items['unit_price']) * floatval($items['qty_order'])));
			$sub_diskon = $sub_diskon + ((floatval($items['unit_price']) * floatval($items['qty_order'])) * (floatval($items['diskon']) / 100));
			//$sub_diskon = $sub_diskon + ((intval($items['unit_price'])*intval($items['qty_order']))*(floatval($items['diskon'])/100));

			//$total_amount = $total_amount + $items['price'];

		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';

		$result['list'] = $htl;
		$result['int_val'] = $new_fl;
		$result['total_amount'] = number_format($total_amount, 2, ',', '.');
		$result['sub_total_amount'] = number_format($sub_total_amount, 2, ',', '.');
		$result['sub_diskon'] = number_format($sub_diskon, 2, ',', '.');
		$result['sub_ppn'] = number_format($total_amount * (floatval($result['invoice'][0]['ppn']) / 100), 2, ',', '.');

		$result['grand_total'] = number_format(($total_amount * (floatval($result['invoice'][0]['ppn']) / 100)) + $total_amount, 2, ',', '.');

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function get_po_detail_details() {

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$result['invoice'] = $this->purchase_order_model->get_po_byid($params);
		$result['id_pos'] = $params['id'];
		$Date = $result['invoice'][0]['date_po'];
		$result['due_date'] = date('Y-m-d', strtotime($Date . ' + ' . $result['invoice'][0]['term_of_payment'] . ' days'));
		//print_r($result['invoice']);die;

		$item = $this->purchase_order_model->get_po_detail_full_edit($params);
		//print_r($item);die;

		$htl = '';
		$new_fl = 0;
		$total_amount = 0;
		$sub_total_amount = 0;
		$sub_diskon = 0;
		foreach ($item as $items) {

			$htl .=	'<div id="row_' . $new_fl . '" >';
			$htl .=	'	<div class="row" style="border-top-style:solid;">';
			$htl .=	'	<div style="margin-top:10px">';
			$htl .=	'		<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="kode_' . $new_fl . '" id="kode_' . $new_fl . '" type="hidden" class="form-control" placeholder="Qty" value="' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '" readOnly  >';
			$htl .=	'				<input name="po_mat_' . $new_fl . '" id="po_mat_' . $new_fl . '" type="hidden" class="form-control" placeholder="Qty" value="' . $items['id_bpb_detail'] . '" readOnly  >';
			$htl .=	'				<input name="idm_' . $new_fl . '" id="idm_' . $new_fl . '" type="hidden" class="form-control" placeholder="Qty" value="' . $items['idm'] . '" readOnly  >';
			$htl .=	'				<input name="unit_box_' . $new_fl . '" id="unit_box_' . $new_fl . '" type="hidden" class="form-control" placeholder="Qty" value="' . $items['unit_box'] . '" readOnly  >';
			$htl .=	'				<textarea style="resize:none; height:94px;" readonly>' . $items['stock_code'] . '&#13;&#10;' . $items['stock_name'] . '&#13;&#10;' . $items['base_qty'] . '' . $items['uom_symbol'] . '</textarea>';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				Box @ ' . $items['unit_box'] . '';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="qtyOrder_' . $new_fl . '" id="qtyOrder_' . $new_fl . '" type="text" class="form-control " onKeydown="change_sum(' . $new_fl . ')" placeholder="Qty" value="' . number_format($items['qty_order'], 3, ',', '.') . '" readOnly  >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="qtySisa_' . $new_fl . '" id="qtySisa_' . $new_fl . '" type="text" class="form-control " onKeyup="change_sum(' . $new_fl . ')" placeholder="Sisa" value="' . number_format($items['qty_sisa'], 3, ',', '.') . '" readOnly >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="qtyTerima_' . $new_fl . '" id="qtyTerima_' . $new_fl . '" type="text" class="form-control " onKeyup="change_sum(' . $new_fl . ')" placeholder="Diterima" value="' . number_format($items['qty_order'] - $items['qty_sisa'], 3, ',', '.') . '" readOnly >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="qty_' . $new_fl . '" id="qty_' . $new_fl . '" type="text" class="form-control " placeholder="Total" onKeyup="change_qty(' . $new_fl . ')" value="' . number_format($items['qty_bpb'], 3, ',', '.') . '" readOnly >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="qtyBonus_' . $new_fl . '" id="qtyBonus_' . $new_fl . '" type="text" class="form-control " placeholder="Bonus" onKeyup="change_qty(' . $new_fl . ')" value="' . number_format($items['qty_bonus_bpb'], 3, ',', '.') . '" readOnly >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="qtyTitip_' . $new_fl . '" id="qtyTitip_' . $new_fl . '" type="text" class="form-control " placeholder="Titipan" onKeyup="change_qty(' . $new_fl . ')" value="' . number_format($items['qty_titip_bpb'], 3, ',', '.') . '" readOnly >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="diskon_' . $new_fl . '" id="diskon_' . $new_fl . '" type="text" class="form-control " placeholder="Titipan" onKeyup="change_qty(' . $new_fl . ')" value="' . number_format($items['discount'] ?? 0, 3, ',', '.') . '" readOnly >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'		<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'			<div class="form-group">';
			$htl .=	'				<input name="note_' . $new_fl . '" id="note_' . $new_fl . '" type="text" class="form-control" placeholder="Note" value="' . $items['ket_bpb'] . '" readOnly >';
			$htl .=	'			</div>';
			$htl .=	'		</div>';
			$htl .=	'	</div>';
			$htl .=	'	</div>';
			$htl .=	'</div>';

			$new_fl++;

			$total_amount = $total_amount + ((floatval($items['unit_price']) * floatval($items['qty_order'])) - ((floatval($items['unit_price']) * floatval($items['qty_order'])) * (floatval($items['diskon']) / 100)));
			$sub_total_amount = $sub_total_amount + ((floatval($items['unit_price']) * floatval($items['qty_order'])));
			$sub_diskon = $sub_diskon + ((floatval($items['unit_price']) * floatval($items['qty_order'])) * (floatval($items['diskon']) / 100));
			//$sub_diskon = $sub_diskon + ((intval($items['unit_price'])*intval($items['qty_order']))*(floatval($items['diskon'])/100));

			//$total_amount = $total_amount + $items['price'];

		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';

		$result['list'] = $htl;
		$result['int_val'] = $new_fl;
		$result['total_amount'] = number_format($total_amount, 2, ',', '.');
		$result['sub_total_amount'] = number_format($sub_total_amount, 2, ',', '.');
		$result['sub_diskon'] = number_format($sub_diskon, 2, ',', '.');
		$result['sub_ppn'] = number_format($total_amount * (floatval($result['invoice'][0]['ppn']) / 100), 2, ',', '.');

		$result['grand_total'] = number_format(($total_amount * (floatval($result['invoice'][0]['ppn']) / 100)) + $total_amount, 2, ',', '.');

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function get_all_item() {

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$item = $this->purchase_order_model->get_item_byprin($params);

		$list = '<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		foreach ($item as $items) {

			$list .= '<option value="' . $items['id_mat'] . '" >' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '</option>';
		}
		$list .= '<option value="0" selected="selected" disabled>-- Pilih Item Lainnya --</option>';

		$item2 = $this->purchase_order_model->get_item_byprin_all($params);

		foreach ($item2 as $items) {

			$list .= '<option value="' . $items['id_mat'] . '" >' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '</option>';
		}
		//	$list .= '------------------';

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}


	public function get_price_mat() {

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		//print_r($params);die;

		$ddd = $this->purchase_order_model->get_price_mat($params);
		//$item = $this->purchase_order_model->get_item_byprin($params);

		if (count($ddd) == 0) {
			$list = 0;
		} else {
			$list = number_format($ddd[0]['unit_price'], 0, ',', '.');
		}
		//print_r($ddd);die;

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add() {
		//$result = $this->purchase_order_model->get_principal();
		$result = $this->purchase_order_model->get_po();

		$result_g = $this->purchase_order_model->get_kode();
		if ($result_g[0]['max'] + 1 < 1000) {
			$kode = 'R' . date('Y') . '0' . ($result_g[0]['max'] + 1);
		} else {
			$kode = 'R' . date('Y') . '' . ($result_g[0]['max'] + 1);
		}
		//print_r($kode);die;

		$data = array(
			'principal' => $result,
			'kode' => $kode
		);

		$this->template->load('maintemplate', 'penerimaan_barang/views/addPo', $data);
		//$this->load->view('addPrinciple',$data);
	}


	public function updatepo() {
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			//	'id_invoice' => $this->Anti_sql_injection($this->input->post('id_invoice', TRUE)),

		);

		//print_r($data);die;

		$po_result = $this->purchase_order_model->get_po_by_inv($data);
		$po_detail_result = $this->purchase_order_model->get_po_detail_update($data);
		//$result = $this->purchase_order_model->get_principal();
		$result = $this->purchase_order_model->get_po();

		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);

		$item = $this->purchase_order_model->get_item_byprin($params);
		//print_r($po_detail_result);die;

		// $result = $this->distributor_model->location();

		$data = array(
			'po' => $po_result[0],
			'principal' => $result,
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			'po_mat' => $po_detail_result,
			'item' => $item
		);

		$this->template->load('maintemplate', 'penerimaan_barang/views/updatePo', $data);
		//$this->load->view('addPrinciple',$data);
	}

	public function detail() {
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			//	'id_invoice' => $this->Anti_sql_injection($this->input->post('id_invoice', TRUE)),

		);

		//print_r($data);die;

		$po_result = $this->purchase_order_model->get_po_by_inv($data);
		//print_r($po_result);die;

		$po_detail_result = $this->purchase_order_model->get_po_detail_update($data);
		//$result = $this->purchase_order_model->get_principal();
		$result = $this->purchase_order_model->get_po();

		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);

		$item = $this->purchase_order_model->get_item_byprin($params);
		//print_r($po_detail_result);die;

		// $result = $this->distributor_model->location();

		$data = array(
			'po' => $po_result[0],
			'principal' => $result,
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			'po_mat' => $po_detail_result,
			'item' => $item
		);

		$this->template->load('maintemplate', 'penerimaan_barang/views/detailPo', $data);
		//$this->load->view('addPrinciple',$data);
	}

	public function konfirmasi() {
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),

		);

		//print_r($data);

		$po_result = $this->purchase_order_model->get_po($data);
		$po_detail_result = $this->purchase_order_model->get_po_detail($data);
		$result = $this->purchase_order_model->get_principal();

		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);

		$item = $this->purchase_order_model->get_item_byprin($params);
		//print_r($principal_result);die;

		// $result = $this->distributor_model->location();

		$data = array(
			'po' => $po_result[0],
			'principal' => $result,
			'po_mat' => $po_detail_result,
			'item' => $item
		);

		$this->template->load('maintemplate', 'penerimaan_barang/views/approvallPo', $data);
		//$this->load->view('addPrinciple',$data);
	}

	public function approve_po() {

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$data = array(
			'id_po' => $params['id'],
			'sts' => $params['sts']

		);

		//print_r($data);die;

		$add_prin_result = $this->purchase_order_model->edit_po_apr($data);
	}

	public function add_po() {
		$this->form_validation->set_rules('no_pos', 'No PO', 'required');
		$this->form_validation->set_rules('no_do', 'No DO', 'required');
		$this->form_validation->set_rules('tgl_do', 'Tanggal DO', 'required');

		if ($this->form_validation->run() == FALSE) {
			$msg = validation_errors();
			$result = array('success' => false, 'message' => $msg);
		} else {

			$int_val = $this->Anti_sql_injection($this->input->post('int_flo', TRUE));

			$array_items = [];

			$total_amount = 0;
			for ($i = 0; $i < $int_val; $i++) {

				//if(isset($this->input->post('kode_'.$i, TRUE))){
				$array_items[$i]['po_mat'] = $this->Anti_sql_injection($this->input->post('po_mat_' . $i, TRUE));
				$array_items[$i]['idm'] = $this->Anti_sql_injection($this->input->post('idm_' . $i, TRUE));
				$array_items[$i]['kode'] = $this->Anti_sql_injection($this->input->post('kode_' . $i, TRUE));
				$array_items[$i]['unit_box'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('unit_box_' . $i, TRUE)))));
				$array_items[$i]['qty'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('qty_' . $i, TRUE)))));
				$array_items[$i]['qtyBonus'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('qtyBonus_' . $i, TRUE)))));
				$array_items[$i]['qtyTitip'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('qtyTitip_' . $i, TRUE)))));
				$array_items[$i]['diskon'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('diskon_' . $i, TRUE)))));
				$array_items[$i]['note'] = $this->Anti_sql_injection($this->input->post('note_' . $i, TRUE));
			}

			//  echo '<pre>';print_r($array_items);die;

			$data = array(
				'id_po' => $this->Anti_sql_injection($this->input->post('no_pos', TRUE)),
				'no_do' => $this->Anti_sql_injection($this->input->post('no_do', TRUE)),
				'angkutan' => $this->Anti_sql_injection($this->input->post('angk', TRUE)),
				'no_polisi' => $this->Anti_sql_injection($this->input->post('no_pol', TRUE)),
				'id_pic' => $this->session->userdata['logged_in']['user_id'],
				'tanggal' => $this->Anti_sql_injection($this->input->post('tgl_do', TRUE))
			);

			// $data2 = array(
			// 'id_po' => $this->Anti_sql_injection($this->input->post('no_pos', TRUE)),
			// 'total_amount' => $total_amounts
			// );

			//$this->purchase_order_model->edit_po($data2);
			//print_r($data);die;


			$add_prin_result = $this->purchase_order_model->add_bpp($data);

			foreach ($array_items as $array_itemss) {

				if ($array_itemss['kode'] == "") {
				} else {



					$conv = $this->purchase_order_model->get_convertion($array_itemss['idm']);

					$mutasi = $array_itemss['qty'] + $array_itemss['qtyBonus'] + $array_itemss['qtyTitip'] + $array_itemss['diskon'];

					if ($mutasi == 0) {
					} else {


						$mutasi_sm = $mutasi * $conv[0]['mult'];
						$note = "Qty :" . $array_itemss['qty'] . " Qty Bonus :" . $array_itemss['qtyBonus'] . " Qty Titip :" . $array_itemss['qtyTitip'] . " Qty Diskon :" . $array_itemss['diskon'];

						$data_mutasi = array(

							'id_stock_awal' => $array_itemss['idm'],
							'id_stock_akhir' => $array_itemss['idm'],
							//'date_mutasi' => date('Y-m-d H:i:s'),
							'date_mutasi' => $data['tanggal'],
							'amount_mutasi' => $mutasi,
							'mutasi_big' => $mutasi_sm,
							'amount_mutasi_qty' => $array_itemss['qty'],
							'mutasi_big_qty' => $array_itemss['qty'] * $conv[0]['mult'],
							'amount_mutasi_bonus' => ($array_itemss['qtyBonus']),
							'mutasi_big_bonus' => ($array_itemss['qtyBonus']) * $conv[0]['mult'],
							'amount_mutasi_titip' => $array_itemss['qtyTitip'],
							'mutasi_big_titip' => $array_itemss['qtyTitip'] * $conv[0]['mult'],
							'amount_mutasi_diskon' => $array_itemss['diskon'],
							'mutasi_big_diskon' => $array_itemss['diskon'] * $conv[0]['mult'],
							'type_mutasi' => 0,
							'user_id' => $this->session->userdata['logged_in']['user_id'],
							'note' => $note

						);

						// echo '<pre>';print_r($array_itemss);die;
						$mutasi_id = $this->purchase_order_model->add_mutasi($data_mutasi);

						$datas = array(

							'id_bpb' => $add_prin_result['lastid'],
							'id_po_mat' => $array_itemss['po_mat'],
							'qty' => $array_itemss['qty'],
							'qty_bonus' => $array_itemss['qtyBonus'],
							'qty_titipan' => $array_itemss['qtyTitip'],
							'discount' => (float) $array_itemss['diskon'],
							'keterangan' => $array_itemss['note'],
							'mutasi_id' => $mutasi_id['lastid']

						);

						$prin_resultb = $this->purchase_order_model->add_detail_bpp($datas);

						$data_detail_m = array(
							'id_material' => $array_itemss['idm'],
							'qty' => $mutasi,
							'qty_big' => $mutasi_sm,
							'qty_sol' => $array_itemss['qty'],
							'qty_sol_big' => $array_itemss['qty'] * $conv[0]['mult'],
							'qty_bonus' => $array_itemss['qtyBonus'],
							'qty_bonus_big' => $array_itemss['qtyBonus'] * $conv[0]['mult'],
							'qty_titip' => $array_itemss['qtyTitip'],
							'qty_titip_big' => $array_itemss['qtyTitip'] * $conv[0]['mult'],
							'qty_diskon' => $array_itemss['diskon'],
							'qty_diskon_big' => $array_itemss['diskon'] * $conv[0]['mult'],
							'date_insert' => $data['tanggal'], ' 00:00:00',
							'id_bpb_detail' => $prin_resultb['lastid']
						);

						$detail_mm = $this->purchase_order_model->add_detail_mat($data_detail_m);
						$this->purchase_order_model->add_qty_mat($data_detail_m, $detail_mm);

						$this->purchase_order_model->update_buy_price($array_itemss['idm']);



						//$this->purchase_order_model->add_qty_mat($data_mutasi,$detail_mm);
						//$this->purchase_order_model->edit_po_mat($datas);
					}
				}
			}



			//print_r($add_prin_result);die;

			if ($add_prin_result['result'] > 0) {


				$result_pr = $this->purchase_order_model->get_po();
				//print_r($result_pr);die;

				$ddd = '<option></option>';
				foreach ($result_pr as $principals) {
					$ddd .= '<option value="' . $principals['id_po'] . '" >' . $principals['no_po'] . ' - ' . $principals['name_eksternal'] . ' - ' . $principals['date_po'] . '</option>';
				}


				$msg = 'Berhasil Menambah data Bpb';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => true, 'message' => $msg, 'net_select' => $ddd);
			} else {
				$msg = 'Gagal Menambah data Bpb';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => false, 'message' => $msg);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);

		//echo $kode;die;

	}

	public function update_po() {

		// $this->form_validation->set_rules('kode', 'Kode Principal', 'required|min_length[4]|max_length[10]');
		// $this->form_validation->set_rules('alamat', 'Alamat Principal', 'required');
		$this->form_validation->set_rules('no_do', 'Nomor DO', 'required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();

			//echo $pesan;die;

			//$msg = strip_tags(str_replace("\r", '', $pesan));
			$msg = $pesan;

			$result = array('success' => false, 'message' => $msg);
		} else {


			$id_bpb = $this->Anti_sql_injection($this->input->post('id_bpb', TRUE));
			$int_val = $this->Anti_sql_injection($this->input->post('int_flo', TRUE));

			//echo $id_bpb;die;

			$array_items = [];

			$total_amount = 0;
			for ($i = 0; $i <= $int_val; $i++) {

				//if(isset($this->input->post('kode_'.$i, TRUE))){
				$array_items[$i]['po_mat'] = $this->Anti_sql_injection($this->input->post('po_mat_' . $i, TRUE));
				$array_items[$i]['idm'] = $this->Anti_sql_injection($this->input->post('idm_' . $i, TRUE));
				$array_items[$i]['kode'] = $this->Anti_sql_injection($this->input->post('kode_' . $i, TRUE));
				$array_items[$i]['unit_box'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('unit_box_' . $i, TRUE)))));
				$array_items[$i]['qty'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('qty_' . $i, TRUE)))));
				$array_items[$i]['qtyBonus'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('qtyBonus_' . $i, TRUE)))));
				$array_items[$i]['qtyTitip'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('qtyTitip_' . $i, TRUE)))));
				$array_items[$i]['diskon'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('diskon_' . $i, TRUE)))));
				$array_items[$i]['note'] = $this->Anti_sql_injection($this->input->post('note_' . $i, TRUE));
			}

			// echo '<pre>'; print_r($array_items); die;

			$data = array(
				'id_bpb' => $id_bpb,
				'no_do' => $this->Anti_sql_injection($this->input->post('no_do', TRUE)),
				'angkutan' => $this->Anti_sql_injection($this->input->post('angk', TRUE)),
				'no_polisi' => $this->Anti_sql_injection($this->input->post('no_pol', TRUE)),
				'id_pic' => $this->session->userdata['logged_in']['user_id'],
				'tanggal' => $this->Anti_sql_injection($this->input->post('tgl_do', TRUE))
			);

			$add_prin_result = $this->purchase_order_model->edit_bpb($data);

			//$add_prin_result = $this->purchase_order_model->edit_inv($data);

			//$this->purchase_order_model->delete_po_mat($this->Anti_sql_injection($this->input->post('id_invoice', TRUE)));

			foreach ($array_items as $array_itemss) {

				if ($array_itemss['kode'] == "") {
				} else {

					$edit_dddd = $this->purchase_order_model->edit_qty_material($array_itemss['po_mat']);

					$conv = $this->purchase_order_model->get_convertion($array_itemss['idm']);

					$mutasi = $array_itemss['qty'] + $array_itemss['qtyBonus'] + $array_itemss['qtyTitip'] + $array_itemss['diskon'];
					$mutasi_sm = $mutasi * $conv[0]['mult'];
					$note = "Qty :" . $array_itemss['qty'] . " Qty Bonus :" . $array_itemss['qtyBonus'] . " Qty Titip :" . $array_itemss['qtyTitip'] . " Qty Diskon :" . $array_itemss['diskon'];

					$data_mutasi = array(
						'id_bpb_detail' => $array_itemss['po_mat'],
						'id_stock_awal' => $array_itemss['idm'],
						'id_stock_akhir' => $array_itemss['idm'],
						//'date_mutasi' => date('Y-m-d H:i:s'),
						'date_mutasi' => $data['tanggal'],
						'amount_mutasi' => $mutasi,
						'mutasi_big' => $mutasi_sm,
						'amount_mutasi_qty' => $array_itemss['qty'],
						'mutasi_big_qty' => $array_itemss['qty'] * $conv[0]['mult'],
						'amount_mutasi_bonus' => $array_itemss['qtyBonus'],
						'mutasi_big_bonus' => $array_itemss['qtyBonus'] * $conv[0]['mult'],
						'amount_mutasi_diskon' => $array_itemss['diskon'],
						'mutasi_big_diskon' => $array_itemss['diskon'] * $conv[0]['mult'],
						'amount_mutasi_titip' => $array_itemss['qtyTitip'],
						'mutasi_big_titip' => $array_itemss['qtyTitip'] * $conv[0]['mult'],
						'type_mutasi' => 0,
						'user_id' => $this->session->userdata['logged_in']['user_id'],
						'note' => $note
					);

					$mutasi_id = $this->purchase_order_model->edit_mutasi_bpb($data_mutasi);

					$datas = array(

						'id_bpb_detail' => $array_itemss['po_mat'],
						'id_po_mat' => $array_itemss['po_mat'],
						'qty' => $array_itemss['qty'],
						'qty_bonus' => $array_itemss['qtyBonus'],
						'qty_titipan' => $array_itemss['qtyTitip'],
						'discount' => $array_itemss['diskon'],
						'keterangan' => $array_itemss['note'],
						'mutasi_id' => $mutasi_id['lastid']
					);


					$prin_resultb = $this->purchase_order_model->edit_detail_bpp($datas);

					$data_detail_m = array(

						'id_material' => $array_itemss['idm'],
						'qty' => $mutasi,
						'qty_big' => $mutasi_sm,
						'qty_sol' => $array_itemss['qty'],
						'qty_sol_big' => $array_itemss['qty'] * $conv[0]['mult'],
						'qty_bonus' => $array_itemss['qtyBonus'],
						'qty_bonus_big' => $array_itemss['qtyBonus'] * $conv[0]['mult'],
						'qty_titip' => $array_itemss['qtyTitip'],
						'qty_titip_big' => $array_itemss['qtyTitip'] * $conv[0]['mult'],
						'qty_diskon' => $array_itemss['diskon'],
						'qty_diskon_big' => $array_itemss['diskon'] * $conv[0]['mult'],
						'date_insert' => $data['tanggal'] . ' 00:00:00',
						'date_mutasi' => $data['tanggal'],
						'id_bpb_detail' => $array_itemss['po_mat']

					);

					$detail_mm = $this->purchase_order_model->edit_detail_mat($data_detail_m);
					$this->purchase_order_model->add_qty_mat_edt($data_detail_m, $array_itemss['po_mat']);

					$this->purchase_order_model->update_buy_price($array_itemss['idm']);
				}
			}

			//print_r($add_prin_result);die;

			if ($add_prin_result['result'] > 0) {

				$msg = 'Berhasil Merubah data Bpb';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => true, 'message' => $msg);
			} else {
				$msg = 'Gagal Menambah data Bpb';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => false, 'message' => $msg);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);

		//echo $kode;die;

	}

	public function delete_po() {
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		//print_r($params);die;

		$result_dist 		= $this->purchase_order_model->edit_qty_material_delete($params['id']);
		$result_dist2 		= $this->purchase_order_model->delete_mutasi($params['id']);
		$result_dist3 		= $this->purchase_order_model->delete_detail_mat($params['id']);
		$result_dist4 		= $this->purchase_order_model->delete_detail_bpp($params['id']);
		$result_dist5 		= $this->purchase_order_model->delete_bpp($params['id']);

		$msg = 'Berhasil menghapus data Purchase Order.';
		$result = array('success' => true, 'message' => $msg);
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function print_pdf() {

		$data   	= file_get_contents("php://input");
		//$params     = json_decode($data,true);

		//echo "aaa";
		$params = (explode('=', $data));

		$data = array(
			'id' => $params[1],
		);

		//print_r($data);die;

		$po_result = $this->purchase_order_model->get_po($data);
		$po_detail_result = $this->purchase_order_model->get_po_detail_full($data);
		$result = $this->purchase_order_model->get_principal();

		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);

		$array_items = [];

		$total_sub = 0;
		$total_sub_all = 0;
		$total_disk = 0;
		$grand_total = 0;

		$htmls = '';
		foreach ($po_detail_result as $po_detail_results) {

			$amn = number_format(($po_detail_results['price'] - ($po_detail_results['price'] * ($po_detail_results['diskon'] / 100))), 0, ',', '');
			$htmls = $htmls . '
				<tr style="border-top: 1px solid black;" >
					<td  align="left" style="border-top: 1px solid black;"><p>' . $po_detail_results['stock_code'] . '</p></td>
					<td  align="left" style="border-top: 1px solid black;"><p>' . $po_detail_results['stock_name'] . '</p></td>
					<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['base_qty'], '2', ',', '.') . ' ' . $po_detail_results['uom_symbol'] . '</p></td>
					<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['qty_order'], '2', ',', '.') . '</p></td>
					<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['unit_price'], '0', ',', '.') . '</p></td>
					<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['diskon'], '2', ',', '.') . '</p></td>
					<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($amn, '0', ',', '.') . '</p></td>
				</tr>
			';

			//$total_sub = $total_sub + number_format(($po_detail_results['price']-($po_detail_results['price']*($po_detail_results['diskon']/100))),0,',',''); 
			$total_sub = $total_sub + number_format(($po_detail_results['price'] - ($po_detail_results['price'] * ($po_detail_results['diskon'] / 100))), 0, ',', '');
			$total_sub_all = $total_sub_all + number_format(($po_detail_results['price']), 0, ',', '');
			$total_disk = $total_disk + ($po_detail_results['price'] * ($po_detail_results['diskon'] / 100));
			//$grand_total = $grand_total +  number_format(($po_detail_results['price']),0,',',''); 

		}

		$total_ppn = $total_sub * (floatval($po_result[0]['ppn'])) / 100;
		$grand_total = $total_ppn + $total_sub;

		//print_r($htmls);die;

		$item = $this->purchase_order_model->get_item_byprin($params);

		$rtext = '<br><p style="font-size: 10px;">PT. ENDRIA ALDA</p> ';

		$ctext = '<br><p style="font-size: 12px"> PURCHASE ORDER</p> ';

		$this->load->library('Pdf');

		$pdf = new Pdf('P', 'mm', 'A4', false, 'UTF-8', false);
		$pdf->SetTitle('Purchase Order');
		$pdf->SetHeaderMargin(25);
		$pdf->setPrintHeader(false);
		$pdf->SetTopMargin(6);
		$pdf->setFooterMargin(5);
		$pdf->SetAutoPageBreak(true);
		$pdf->SetAuthor('Author');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->SetPrintFooter(false);

		$pdf->AddPage();


		$html = '
		
		<style>
		
		  .floatedTable {
            float:left;
        }
        .inlineTable {
            display: inline-block;
        }
		p {
							font-size: 8px;
							
						}
		p {
							font-size: 8px;
							
						}
		</style>
  <table style=" float:left;border: none;" >
						<tr style="border: none;">
							<th  align="left" ><h5>PT.ENDIRA ALDA</h5></th>
							<th  align="RIGHT"></th>
						</tr>
						<tr style="border: none;">
							<th  align="left" ><h4>PURCHASE ORDER</h4></th>
							<th  align="RIGHT"><p>NOMOR, TGL : ' . $po_result[0]['no_po'] . '   ' . $po_result[0]['date_po'] . '</p></th>
						</tr>
					  </table><br><br><br>
					  
					<table>
					  <tr>
						<td>

						  <table cellspacing="0" cellpadding="0" border="0" style="float:right;width:100%.">
							<tr>
							  <td style="width:18%;"><p>Kepada</p></td>
							  <td><p>: ' . $po_result[0]['name_eksternal'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Alamat</p></td>
							  <td><p>: ' . $po_result[0]['eksternal_address'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Att</p></td>
							  <td><p>: ' . $po_result[0]['pic'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Telp</p></td>
							  <td><p>: ' . $po_result[0]['phone_1'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Fax</p></td>
							  <td><p>: ' . $po_result[0]['fax'] . '</p></td>
							</tr>

						  </table>
						</td>
						<td>
						  <table cellspacing="0" cellpadding="0" border="0" style="float:right;width:100%">
							<tr>
							  <td style="width:18%;"><p>Ship To</p></td>
							  <td><p>:JL.SANGKURIANG NO 38-A<BR>NPWP:01.555.161.7.428.000</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Email</p></td>
							  <td><p>: </p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Telp/Fax</p></td>
							  <td><p>: (022)6626-946</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Delivery</p></td>
							  <td><p>: </p></td>
							</tr>
						

						  </table>
						</td>
					  </tr>
					</table>
					<br><br><br>

					
					  <table style=" float:left;border-top: 1px solid black;border-bottom: 1px solid black;" >
						<tr style=" border-top: 1px solid black;font-size: 10px;">
							<th  align="left" >Code</th>
							<th  align="RIGHT">Description</th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Quantity<br>Lt/Kg</th>
							<th  align="RIGHT">Unit Price</th>
							<th  align="RIGHT">Disc</th>
							<th  align="RIGHT">Amount</th>
						</tr>
						' . $htmls . '
					  </table>
					  	<br><br><br>
						<br><br><br>
					  
					    <table style=" float:left;border-top: 1px solid black;" >
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Sub Total :</th>
							<th  align="RIGHT">' . number_format($total_sub_all, 0, ',', '.') . '</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Discount :</th>
							<th  align="RIGHT">' . number_format($total_disk, 0, ',', '.') . '</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"> Taxable :</th>
							<th  align="RIGHT">' . number_format($total_sub, 0, ',', '.') . '</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Vat/PPN :</th>
							<th  align="RIGHT">' . number_format($total_ppn, 0, ',', '.') . '</th>
						</tr>
					  </table>
					  
					  <table style=" float:left;border-top: 1px solid black;" >
						<tr style=" border-top: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Total :</th>
							<th  align="RIGHT">' . number_format($grand_total, 0, ',', '.') . '</th>
						</tr>
					  </table>
					<br><br><br><br><br> 
					<br><br><br><br><br> 
					
					 <table style="font-size: 10px;" >
						<tr style="">
							<th  align="CENTER" style="width:25%" ></th>
							<th  align="CENTER" style="width:12%" ></th>
							<th  align="CENTER" style="width:25%"></th>
							<th  align="CENTER" style="width:13%" ></th>
							<th  align="CENTER" style="width:25%">Cimahi, ' . date('m/d/Y') . '</th>
						</tr>
						<tr style="">
							<th  align="CENTER" style="width:25%" >Disetujui Oleh,</th>
							<th  align="CENTER" style="width:12%" ></th>
							<th  align="CENTER" style="width:25%">Diperiksa Oleh,</th>
							<th  align="CENTER" style="width:13%" ></th>
							<th  align="CENTER" style="width:25%">Dipesan Oleh,</th>
						</tr>
					  </table>
					  <br><br><br><br><br> 
					  <table style="font-size: 10px;" >
						<tr style="">
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Ratna S. Iskandar Dinata</span></th>
							<th  align="CENTER" style="width:12%"></th>
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Rienaldy Aryanto</span></th>
							<th  align="CENTER" style="width:13%"></th>
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Rangga Dean</span></th>
						</tr>
					  </table>
';

		//echo $html;die;

		// $html = <<<EOD
		// <h5>PT.ENDIRA ALDA</h5>
		// <table style=" float:left;border: none;" >
		// <tr style="border: none;">
		// <th  align="left" ><h4>PURCHASE ORDER</h4></th>
		// <th  align="RIGHT">NOMOR, TGL : ".."</th>
		// </tr>
		// </table>

		// EOD;

		// Print text using writeHTMLCell()
		$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);



		$pdf->SetPrintFooter(false);

		$pdf->lastPage();

		// $pdf->writeHTMLCell(85, 5, '<p>'.$kategoriby.' : '.$kategori.'<br>Periode : '.$start_date.' / '.$end_date.'</p> ', 1, 'L', 0, 0, '', '', true);
		// $pdf->writeHTMLCell(85, 5, $img, 1, 'R', 0, 1, '', '', true);
		// $pdf->writeHTMLCell(180, 5, '[CENTER] ', 1, 'C', 0, 1, '', '', true);
		// $pdf->writeHTMLCell(85, 5, '[LEFT] ', 1, 'L', 0, 0, '', '', true);
		// $pdf->writeHTMLCell(85, 5, '[RIGHT] ', 1, 'L', 0, 1, '', '', true);

		// $pdf->Output('/var/www/html/tmp_doc/Report_Postbuy.pdf', 'F');

		// $this->output->set_content_type('application/json')->set_output(json_encode('aaa'));

		$pdf->Output('Purchase_order.pdf', 'I');


		// if ( $list ) {			
		// $this->output->set_content_type('application/json')->set_output(json_encode($list));
		// } else {
		// $result = array( 'Value not found!' );
		// $this->output->set_content_type('application/json')->set_output(json_encode($list));
		// }

	}
}
