 <div class="row">
     <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Management Eskalasi</h2>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                      <div class="clearfix margin-bottom-10px">

                      </div>

                    <table id="listeskalasi" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Title</th>
                          <th>Keterangan</th>
                          <th>Lokasi</th>
                          <th>level</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
						  <th>No</th>
                          <th>Title</th>
						  <th>Keterangan</th>
                          <th>Lokasi</th>
                          <th>level</th>
                          <th>Action</th>
                      </tbody>
                    </table>


                  </div>
                </div>
              </div>
            </div>

    <div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content p-0 b-0">
                <div class="panel panel-color panel-primary panel-filled">
                    <div class="panel-heading">
                        <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="panel-title"></h3>
                    </div>
                    <div class="panel-body">
                        <p></p>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

        <script type="text/javascript">

        function add_ekalasi(){
            $('#panel-modal').removeData('bs.modal');
            $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
            $('#panel-modal  .panel-body').load('<?php echo base_url('users/add_ekalasi_view');?>');
            $('#panel-modal  .panel-title').html('<i class="fa fa-user-plus"></i> Add Eskalasi');
            $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
        }

        $(document).ready(function(){

        	$("#listeskalasi").dataTable({
        				"processing": true,
        				"serverSide": true,
        				"ajax": "<?php echo base_url().'users/lists_ekalasi/';?>",
        				"searchDelay": 700,
        				"responsive": true,
        				"lengthChange": false,
        				"info": false,
                        "bSort": false,
                        "dom": 'l<"toolbar">frtip',
                        "initComplete": function(){
                           $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-default" onClick="add_ekalasi()"><i class="fa fa-user-plus"></i> Add Eskalasi</a></div>');
                        }
        	});


        });

		function edit_param(sid){
           $('#panel-modal').removeData('bs.modal');
           $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
           $('#panel-modal  .panel-body').load('<?php echo base_url('users/edit_ekalasi_view');?>/'+sid);
           $('#panel-modal  .panel-title').html('<i class="fa fa-pencil-square-o"></i> Edit Eskalasi');
           $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
       }
	   function del_eskalasi(sid){
           swal({
             title: 'Anda yakin?',
             text: "Eskalasi ini akan dihapus!",
             type: 'warning',
             showCancelButton: true,
             confirmButtonColor: '#3085d6',
             cancelButtonColor: '#d33',
             confirmButtonText: 'Yes!'
           }).then(function () {
               var form_data = {
                   sid : sid
               }

               $.ajax({
                   type	: "POST",
                   url	: "<?php echo base_url();?>" + "users/deleted_eskalasi/",
                   data	: JSON.stringify(form_data),
                   dataType	: 'json',
                   contentType	: 'application/json; charset=utf-8',
                   success	: function(response) {
                       if (response.success) {
                           $("#listeskalasi").DataTable().ajax.reload();
                       } else {
                           swal({
                               title    : "Error!",
                               text	: response.message,
                               type	: "error"
                           })
                       }
                   }
               }).fail(function(xhr, status, message) {
                   swal("Gagal!", "Invalid respon, silahkan cek koneksi.", "error");
               })
           });

       }

        </script>
