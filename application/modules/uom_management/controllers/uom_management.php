<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Uom_Management extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('uom_management/uom_model');
	$this->load->library('log_activity');
	$this->load->library('priv');
  }

  /**
   * anti sql injection
   */
  public function Anti_sql_injection($string)
  {
    $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
    return $string;
  }

  public function index()
  {
	  $priv = $this->priv->get_priv();
		
		//print_r($priv);die;
		
		$data = array(
			'priv' => $priv
		);
	  
    $this->template->load('maintemplate', 'uom_management/views/index', $data);
  }

  function lists()
  {

    $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
    $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
    $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
    $order = $this->input->get_post('order');
    $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
    $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

    $order_fields = array('', 'uom_name', 'uom_symbol');

    $search = $this->input->get_post('search');

    $search_val = (!empty($search['value'])) ? $search['value'] : null;

    $search_value = $this->Anti_sql_injection($search_val);

    // Build params for calling model
    $params['limit'] = (int) $length;
    $params['offset'] = (int) $start;
    $params['order_column'] = $order_fields[$order_column];
    $params['order_dir'] = $order_dir;
    $params['filter'] = $search_value;

    //print_r($params);die;

    $list = $this->uom_model->uom_list($params);
    //print_r($list);die;

    $result["recordsTotal"] = $list['total'];
    $result["recordsFiltered"] = $list['total_filtered'];
    $result["draw"] = $draw;

    //print_r($result);die;
$priv = $this->priv->get_priv();

    $data = array();
    $i = $params['offset'];
    $username = $this->session->userdata['logged_in']['username'];
    foreach ($list['data'] as $k => $v) {
      $i = $i + 1;


      $status_akses = '
                <div class="btn-group" style="display:'.$priv['update'].'"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edituom(\'' . $v['id_uom'] . '\')"><i class="fa fa-edit"></i></button></div>
				  <div class="btn-group" style="display:'.$priv['delete'].'"><button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteuom(\'' . $v['id_uom'] . '\')"><i class="fa fa-trash"></i></button></div>';

      array_push($data, array(
        $i,
        $v['uom_name'],
        $v['uom_symbol'],
        $v['description'],
        $status_akses
      ));
    }

    $result["data"] = $data;

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function add()
  {
    //$result = $this->uom_model->group();



    // $this->load->view('add_modal_view', $data);

    // $result = $this->distributor_model->location();

    $data = array(
      'group' => ''
    );

    $this->template->load('maintemplate', 'uom_management/views/add_modal_view', $data);
  }

  public function edit()
  {
    //$id = $this->Anti_sql_injection($this->input->post('sid', TRUE));
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('iduom', TRUE)),
		);
    $result = $this->uom_model->get_uom_by_id($this->Anti_sql_injection($data['id']));

    //print_r($result);die;
    // $roles = $this->uom_model->roles($id);

    $data = array(
      'uom' => $result,
    );

    $this->template->load('maintemplate', 'uom_management/views/edit_modal_view', $data);
  }

  public function deletes()
  {

    $data   = file_get_contents("php://input");
    $params   = json_decode($data, true);

    $list = $this->uom_model->deletes($this->Anti_sql_injection($params['id']));

    $res = array(
      'status' => 'success',
      'message' => 'Data telah di hapus'
    );

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }

  public function edit_uom()
  {
    $this->form_validation->set_rules('uom', 'uom', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('uom_sym', 'uom_sym', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('uom_ket', 'uom_ket', 'trim|required|max_length[255]');
    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));
      $result = array(
        'success' => false,
        'message' => $msg
      );
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      // die;
      $message = "";
      $data = array(
        'id_uom' => $this->Anti_sql_injection($this->input->post('id_uom', TRUE)),
        'nama_uom' =>  $this->Anti_sql_injection($this->input->post('uom', TRUE)),
        'simbol' => $this->Anti_sql_injection($this->input->post('uom_sym', TRUE)),
        'keterangan' => $this->Anti_sql_injection($this->input->post('uom_ket', TRUE))
      );
      $result = $this->uom_model->edit_uom($data);
      if ($result > 0) {
        $msg = 'Berhasil merubah uom.';

        $result = array(
          'success' => true,
          'message' => $msg
        );
      } else {
        $msg = 'Gagal merubah uom.';

        $result = array(
          'success' => false,
          'message' => $msg
        );
      }

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
  }

  public function add_uom()
  {
    $this->form_validation->set_rules('uom', 'uom', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('uom_sym', 'uom_sym', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('uom_ket', 'uom_ket', 'trim|required|max_length[255]');
    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));
      $result = array(
        'success' => false,
        'message' => $msg
      );

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      // die;
      $message = "";
      $data = array(
        'user_id' => $this->session->userdata['logged_in']['user_id'],
        'nama_uom'     => $this->Anti_sql_injection($this->input->post('uom', TRUE)),
        'simbol' => $this->Anti_sql_injection($this->input->post('uom_sym', TRUE)),
        'keterangan' => $this->Anti_sql_injection($this->input->post('uom_ket', TRUE))
      );

      $result = $this->uom_model->add_uom($data);

      if ($result > 0) {
        $msg = 'Berhasil menambahkan uom.';

        $result = array(
          'success' => true,
          'message' => $msg
        );
      } else {
        $msg = 'Gagal menambahkan uom ke database.';
        $result = array(
          'success' => false,
          'message' => $msg
        );
      }
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
  }

    public function test()
    {
        echo '<pre>'; print_r($this->uom_model->get_uom_by_id(2)); die;
    }
}
