<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Report_cash_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	// public function list_report_cash($params = [])
	// {
		// $total = $this->db
			// ->select('id')
			// ->from('t_coa_value')
			// ->join('t_coa', 't_coa_value.id_coa = t_coa.id_coa')
			// // ->join('(SELECT `date` AS `jdate`, SUM(`value`) AS jvalue  FROM t_coa_value GROUP BY `date`) as j', 't_coa_value.date = j.jdate')
			// // ->join('t_po_mat', 't_coa_value.id_coa_temp = t_po_mat.id_t_ps', 'left')
			// ->get()
			// ->result_array();

		// $filteredQueryBuilder = $this->db
			// ->select('*')
			// ->from('t_coa_value')
			// ->join('t_coa', 't_coa_value.id_coa = t_coa.id_coa');
		// // ->join('(SELECT `date` AS `jdate`, SUM(`value`) AS jvalue  FROM t_coa_value GROUP BY `date`) as j', 't_coa_value.date = j.jdate')
		// // ->join('t_po_mat', 't_coa_value.id_coa_temp = t_po_mat.id_t_ps', 'left');

		// if (isset($params['coa_id']) && !empty($params['coa_id'])) {
			// $filteredQueryBuilder = $filteredQueryBuilder->where(['t_coa.id_coa' => $params['coa_id']]);
		// }
		// if (isset($params['start_date']) && !empty($params['start_date'])) {
			// $filteredQueryBuilder = $filteredQueryBuilder->where(['t_coa_value.date >=' => $params['start_date']]);
		// }
		// if (isset($params['end_date']) && !empty($params['end_date'])) {
			// $filteredQueryBuilder = $filteredQueryBuilder->where(['t_coa_value.date <=' => $params['end_date']]);
		// }

		// if (isset($params['searchtxt']) && !empty($params['searchtxt'])) {
			// $filteredQueryBuilder = $filteredQueryBuilder->like('t_coa_value.date_insert', $params['searchtxt']);
			// $filteredQueryBuilder = $filteredQueryBuilder->or_like('t_coa.coa', $params['searchtxt']);
			// $filteredQueryBuilder = $filteredQueryBuilder->or_like('t_coa.keterangan', $params['searchtxt']);
			// $filteredQueryBuilder = $filteredQueryBuilder->or_like('t_po_mat.remarks', $params['searchtxt']);
		// }

		// $filteredQueryBuilder = $filteredQueryBuilder->order_by('t_coa_value.date ASC');

		// $filteredResult = $filteredQueryBuilder->get()->result_array();

		// $paginatedQueryBuilder = $this->db
			// ->select('*')
			// ->from('t_coa_value')
			// ->join('t_coa', 't_coa_value.id_coa = t_coa.id_coa');
		// // ->join('(SELECT `date` AS `jdate`, SUM(`value`) AS jvalue  FROM t_coa_value GROUP BY `date`) as j', 't_coa_value.date = j.jdate')
		// // ->join('t_po_mat', 't_coa_value.id_coa_temp = t_po_mat.id_t_ps', 'left');

		// if (isset($params['coa_id']) && !empty($params['coa_id'])) {
			// $paginatedQueryBuilder = $paginatedQueryBuilder->where(['t_coa.id_coa' => $params['coa_id']]);
		// }
		// if (isset($params['start_date']) && !empty($params['start_date'])) {
			// $paginatedQueryBuilder = $paginatedQueryBuilder->where(['t_coa_value.date >=' => $params['start_date']]);
		// }
		// if (isset($params['end_date']) && !empty($params['end_date'])) {
			// $paginatedQueryBuilder = $paginatedQueryBuilder->where(['t_coa_value.date <=' => $params['end_date']]);
		// }

		// if (isset($params['searchtxt']) && !empty($params['searchtxt'])) {
			// $paginatedQueryBuilder = $paginatedQueryBuilder->like('t_coa_value.date_insert', $params['searchtxt']);
			// $paginatedQueryBuilder = $paginatedQueryBuilder->or_like('t_coa.coa', $params['searchtxt']);
			// $paginatedQueryBuilder = $paginatedQueryBuilder->or_like('t_coa.keterangan', $params['searchtxt']);
			// $paginatedQueryBuilder = $paginatedQueryBuilder->or_like('t_po_mat.remarks', $params['searchtxt']);
		// }

		// $paginatedQueryBuilder = $paginatedQueryBuilder->order_by('t_coa_value.date ASC');

		// if (isset($params['limit']) && $params['limit'] >= 0 && isset($params['offset'])) {
			// $paginatedQueryBuilder = $paginatedQueryBuilder->limit($params['limit'], $params['offset']);
		// }

		// $paginatedResult = $paginatedQueryBuilder->get()->result_array();

		// return [
			// 'data' => $paginatedResult,
			// 'total_filtered' => count($filteredResult),
			// 'total' => count($total),
		// ];
	// }
	
	public function list_report_cash_newest($params = []){
			
		$sql 	= '
		SELECT *,value_real as sum_amnt FROM t_coa_value a
		WHERE id_coa = '.$params['coa_id'].'
			AND a.date BETWEEN "'.$params['start_date'].'" AND "'.$params['end_date'].'"
			order by a.date,a.id
			';
		
		
		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return [
			'data' => $result
		];


	}
	
	public function list_report_cash($params = []){
		
		
		// $sql 	= '
		// SELECT b.coa, b.keterangan, a.* FROM (
		// SELECT IF(a.id_coa = '.$params['coa_id'].',b.id_coa,a.id_coa) AS coa_ac,
		// IF(a.id_coa = '.$params['coa_id'].',a.type_cash,b.type_cash) AS type_ac,
		// IF(a.id_coa = '.$params['coa_id'].',b.value_real,a.value_real) AS value_ac,
		// CONCAT(a.note," ",b.note) note_ac , a.*, e.`no_invoice` jual, f.`no_invoice` beli,
		// `cust_name`,`name_eksternal`
		// FROM `t_coa_value` a
		// JOIN `t_coa_value` b ON b.id_parent = a.id
		// LEFT JOIN `t_payment_hp` c ON a.id_coa_temp = c.`id_hp`
		// LEFT JOIN `d_payment_hp` d ON c.`id_hp` = d.`id_hp` AND a.value_real = d.ammount
		// LEFT JOIN `t_invoice_penjualan` e ON d.id_invoice = e.`id_invoice`
		// LEFT JOIN `t_penjualan` g ON e.`id_penjualan` = g.`id_penjualan`
		// LEFT JOIN `t_customer` h ON g.`id_customer` = `id_t_cust`

		// LEFT JOIN `t_invoice_pembelian` f ON d.id_invoice = f.`id_invoice`
		// LEFT JOIN `t_bpb` i ON f.`id_bpb` = i.`id_bpb`
		// LEFT JOIN `t_purchase_order` j ON j.id_po = i.`id_po`
		// LEFT JOIN `t_eksternal` k ON j.id_distributor = k.`id`
		// WHERE (a.id_coa = '.$params['coa_id'].' OR b.id_coa = '.$params['coa_id'].')
		// AND a.date BETWEEN  "'.$params['start_date'].'" AND "'.$params['end_date'].'"
		// ) a JOIN t_coa b ON a.coa_ac = b.id_coa
		// ORDER BY a.`date`
		// ';
		
		// $sql 	= '
		// SELECT * FROM (			
			
			// SELECT a.*,kr_coa,kr_note, k_note,sum_amnt, b.id AS ch_parent,f.`no_invoice` invoice, k.name_eksternal users FROM (
					// SELECT b.coa, b.keterangan, a.* FROM t_coa_value a JOIN t_coa b 
					// ON a.id_coa = b.id_coa
					// WHERE a.`id_parent` =0 AND a.id_coa = '.$params['coa_id'].'
					// AND a.type_cash = 1 AND `value` > 0
					// ) a JOIN (
					// SELECT b.coa kr_coa,b.keterangan AS kr_note,
						// a.note AS k_note,
						// a.id_parent,a.id, value_real sum_amnt FROM t_coa_value a JOIN t_coa b 
						// ON a.id_coa = b.id_coa
						// WHERE a.`id_parent` <> 0 
						// and a.type_cash = 0
						// #GROUP BY a.`id_parent`
					// ) b ON a.id = b.id_parent
					// LEFT JOIN `t_payment_hp` c ON a.id_coa_temp = c.`id_hp`
					// LEFT JOIN `d_payment_hp` d ON c.`id_hp` = d.`id_hp` AND a.value_real = d.ammount
					// LEFT JOIN `t_invoice_pembelian` f ON d.id_invoice = f.`id_invoice`
					// LEFT JOIN `t_bpb` i ON f.`id_bpb` = i.`id_bpb`
					// LEFT JOIN `t_purchase_order` j ON j.id_po = i.`id_po`
					// LEFT JOIN `t_eksternal` k ON j.id_distributor = k.`id`
					// where a.`date` BETWEEN  "'.$params['start_date'].'" AND "'.$params['end_date'].'"
					// and c.payment_type = 0
					
					
					// ) A UNION  SELECT * FROM(
// SELECT a.*,kr_coa,kr_note, k_note,sum_amnt, b.id AS ch_parent,e.`no_invoice` AS invoice, h.cust_name users FROM (
					// SELECT b.coa, b.keterangan, a.* FROM t_coa_value a JOIN t_coa b 
					// ON a.id_coa = b.id_coa
					// WHERE a.`id_parent` =0 AND a.id_coa = '.$params['coa_id'].'
					// AND a.type_cash = 0 AND `value` > 0
					// ) a LEFT JOIN (
					// SELECT b.coa kr_coa,b.keterangan AS kr_note,
						// a.note AS k_note,
						// a.id_parent,a.id, value_real sum_amnt FROM t_coa_value a JOIN t_coa b 
						// ON a.id_coa = b.id_coa
						// WHERE a.`id_parent` <> 0
						// #GROUP BY a.`id_parent`
					// ) b ON a.id = b.id_parent
					// LEFT JOIN `t_payment_hp` c ON a.id_coa_temp = c.`id_hp`
					// LEFT JOIN `d_payment_hp` d ON c.`id_hp` = d.`id_hp` AND a.value_real = d.ammount
					// LEFT JOIN `t_invoice_penjualan` e ON d.id_invoice = e.`id_invoice`
					// LEFT JOIN `t_penjualan` g ON e.`id_penjualan` = g.`id_penjualan`
					// LEFT JOIN `t_customer` h ON g.`id_customer` = `id_t_cust`
					// where a.`date` BETWEEN  "'.$params['start_date'].'" AND "'.$params['end_date'].'"
					// and c.payment_type = 1
					
					// ) B 
					// ORDER BY `date`,kr_coa,kr_note,users,sum_amnt
		// ';

if($params['coa_id'] == 0){
	
	$coa_cash = $this->get_cash_all();
	
	//print_r($coa_cash);die;
	
	$cnt_all = count($coa_cash['data']);
	$cnt=0;
	
	$sql_all = "";
	
	foreach($coa_cash['data'] as $coa_cashs){
		$where = " AND b.id_coa=".$coa_cashs['id_coa']." ";
		
		$sql = "
		
	select *,'".$coa_cashs['keterangan']."' as banks from (

SELECT a.*,
b.coa kr_coa,b.note as k_note,
if(b.keterangan IS NULL,a.keterangan,b.keterangan) kr_note,
IF(a.value_real IS NULL,a.value_real,a.value_real) sum_amnt,
p.`no_invoice` invoice_p, p.name_eksternal users FROM (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=1 AND a.id_parent = 0 ".$where."
)a LEFT JOIN (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=0 AND a.id_parent <> 0
)b ON a.id=b.id_parent
LEFT JOIN (select c.id_hp,f.no_invoice,k.name_eksternal from `t_payment_hp` c 
					LEFT JOIN `d_payment_hp` d ON c.`id_hp` = d.`id_hp`
					LEFT JOIN `t_invoice_pembelian` f ON d.id_invoice = f.`id_invoice`
					LEFT JOIN `t_bpb` i ON f.`id_bpb` = i.`id_bpb`
					LEFT JOIN `t_purchase_order` j ON j.id_po = i.`id_po`
					LEFT JOIN `t_eksternal` k ON j.id_distributor = k.`id` 
					WHERE c.payment_type = 0
					group by d.id_hp) p ON a.id_coa_temp = p.`id_hp`
WHERE a.date BETWEEN  '".$params['start_date']."' AND '".$params['end_date']."'
and b.coa IS NOT NULL
) a union all select *,'".$coa_cashs['keterangan']."' as banks from (

SELECT a.*,
b.coa kr_coa,b.note as k_note,
IF(b.keterangan IS NULL,a.keterangan,b.keterangan) kr_note,
IF(a.value_real IS NULL,a.value_real,a.value_real) sum_amnt,
p.`no_invoice` invoice_p, p.name_eksternal users FROM (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=1 AND a.id_parent <> 0 ".$where."
)a LEFT JOIN (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=0 AND a.id_parent = 0
)b ON a.id_parent=b.id
LEFT JOIN (SELECT c.id_hp,f.no_invoice,k.name_eksternal FROM `t_payment_hp` c 
					LEFT JOIN `d_payment_hp` d ON c.`id_hp` = d.`id_hp`
					LEFT JOIN `t_invoice_pembelian` f ON d.id_invoice = f.`id_invoice`
					LEFT JOIN `t_bpb` i ON f.`id_bpb` = i.`id_bpb`
					LEFT JOIN `t_purchase_order` j ON j.id_po = i.`id_po`
					LEFT JOIN `t_eksternal` k ON j.id_distributor = k.`id` 
					WHERE c.payment_type = 0
					GROUP BY d.id_hp) p ON a.id_coa_temp = p.`id_hp`
WHERE a.date BETWEEN  '".$params['start_date']."' AND '".$params['end_date']."'
AND b.coa IS NOT NULL
) b union all SELECT *,'".$coa_cashs['keterangan']."' as banks FROM (

SELECT a.*,
b.coa kr_coa,b.note as k_note,
IF(b.keterangan IS NULL,a.keterangan,b.keterangan) kr_note,
IF(a.value_real IS NULL,a.value_real,a.value_real) sum_amnt,
p.`no_invoice` invoice_p, p.cust_name users FROM (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=0 AND a.id_parent = 0 ".$where."
)a LEFT JOIN (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=1 AND a.id_parent <> 0
)b ON a.id=b.id_parent
LEFT JOIN (SELECT c.id_hp,e.no_invoice,h.cust_name FROM `t_payment_hp` c 
					LEFT JOIN `d_payment_hp` d ON c.`id_hp` = d.`id_hp`
					LEFT JOIN `t_invoice_penjualan` e ON d.id_invoice = e.`id_invoice`
					LEFT JOIN `t_penjualan` g ON e.`id_penjualan` = g.`id_penjualan`
					LEFT JOIN `t_customer` h ON g.`id_customer` = `id_t_cust`
					where c.payment_type = 1
					GROUP BY d.id_hp) p ON a.id_coa_temp = p.`id_hp`
WHERE a.date BETWEEN  '".$params['start_date']."' AND '".$params['end_date']."'
AND b.coa IS NOT NULL
) c union all SELECT *,'".$coa_cashs['keterangan']."' as banks FROM  (

SELECT a.*,
b.coa kr_coa,b.note as k_note,
IF(b.keterangan IS NULL,a.keterangan,b.keterangan) kr_note,
IF(a.value_real IS NULL,a.value_real,a.value_real) sum_amnt,
p.`no_invoice` invoice_p, p.cust_name users FROM (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=0 AND a.id_parent <> 0 ".$where."
)a LEFT JOIN (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=1 AND a.id_parent = 0
)b ON a.id_parent=b.id
LEFT JOIN (SELECT c.id_hp,e.no_invoice,h.cust_name FROM `t_payment_hp` c 
					LEFT JOIN `d_payment_hp` d ON c.`id_hp` = d.`id_hp`
					LEFT JOIN `t_invoice_penjualan` e ON d.id_invoice = e.`id_invoice`
					LEFT JOIN `t_penjualan` g ON e.`id_penjualan` = g.`id_penjualan`
					LEFT JOIN `t_customer` h ON g.`id_customer` = `id_t_cust`
					WHERE c.payment_type = 1
					GROUP BY d.id_hp) p ON a.id_coa_temp = p.`id_hp`
WHERE a.date BETWEEN  '".$params['start_date']."' AND '".$params['end_date']."'
AND b.coa IS NOT NULL
) d union ALL SELECT *,'".$coa_cashs['keterangan']."' as banks FROM (

SELECT a.*,
b.coa kr_coa,b.note as k_note,
b.keterangan kr_note,
b.value_real sum_amnt,
p.`no_invoice` invoice_p, p.name_eksternal users FROM (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=1 AND a.id_parent = 0 ".$where."
)a LEFT JOIN (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=1 AND a.id_parent <> 0
)b ON a.id=b.id_parent
LEFT JOIN (SELECT c.id_hp,f.no_invoice,k.name_eksternal FROM `t_payment_hp` c 
					LEFT JOIN `d_payment_hp` d ON c.`id_hp` = d.`id_hp`
					LEFT JOIN `t_invoice_pembelian` f ON d.id_invoice = f.`id_invoice`
					LEFT JOIN `t_bpb` i ON f.`id_bpb` = i.`id_bpb`
					LEFT JOIN `t_purchase_order` j ON j.id_po = i.`id_po`
					LEFT JOIN `t_eksternal` k ON j.id_distributor = k.`id` 
					WHERE c.payment_type = 0
					GROUP BY d.id_hp) p ON a.id_coa_temp = p.`id_hp`
WHERE a.date BETWEEN  '".$params['start_date']."' AND '".$params['end_date']."'
AND b.coa IS not NULL
) e UNION ALL SELECT *,'".$coa_cashs['keterangan']."' as banks FROM (

SELECT a.*,
b.coa kr_coa,b.note as k_note,
b.keterangan kr_note,
b.value_real sum_amnt,
p.`no_invoice` invoice_p, p.name_eksternal users FROM (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=0 AND a.id_parent = 0 ".$where."
)a LEFT JOIN (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=0 AND a.id_parent <> 0
)b ON a.id=b.id_parent
LEFT JOIN (SELECT c.id_hp,f.no_invoice,k.name_eksternal FROM `t_payment_hp` c 
					LEFT JOIN `d_payment_hp` d ON c.`id_hp` = d.`id_hp`
					LEFT JOIN `t_invoice_pembelian` f ON d.id_invoice = f.`id_invoice`
					LEFT JOIN `t_bpb` i ON f.`id_bpb` = i.`id_bpb`
					LEFT JOIN `t_purchase_order` j ON j.id_po = i.`id_po`
					LEFT JOIN `t_eksternal` k ON j.id_distributor = k.`id` 
					WHERE c.payment_type = 0
					GROUP BY d.id_hp) p ON a.id_coa_temp = p.`id_hp`
WHERE a.date BETWEEN  '".$params['start_date']."' AND '".$params['end_date']."'
AND b.coa IS NOT NULL
) f 

";
		
		$cnt++;
		
		if($cnt < $cnt_all){
			$sql = $sql." union all  ";
		}
		
		$sql_all = $sql_all." ".$sql;
		
	}
	//print_r($sql_all);die;
	$sql = $sql_all." order by `date` ";
	
	//$where = "";
}else{
	$where = " AND b.id_coa=".$params['coa_id']." ";


		$sql = "
		
	select *,keterangan as banks from (

SELECT a.*,
b.coa kr_coa,b.note as k_note,
if(b.keterangan IS NULL,a.keterangan,b.keterangan) kr_note,
IF(a.value_real IS NULL,a.value_real,a.value_real) sum_amnt,
p.`no_invoice` invoice_p, p.name_eksternal users FROM (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=1 AND a.id_parent = 0 ".$where."
)a LEFT JOIN (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=0 AND a.id_parent <> 0
)b ON a.id=b.id_parent
LEFT JOIN (select c.id_hp,f.no_invoice,k.name_eksternal from `t_payment_hp` c 
					LEFT JOIN `d_payment_hp` d ON c.`id_hp` = d.`id_hp`
					LEFT JOIN `t_invoice_pembelian` f ON d.id_invoice = f.`id_invoice`
					LEFT JOIN `t_bpb` i ON f.`id_bpb` = i.`id_bpb`
					LEFT JOIN `t_purchase_order` j ON j.id_po = i.`id_po`
					LEFT JOIN `t_eksternal` k ON j.id_distributor = k.`id` 
					WHERE c.payment_type = 0
					group by d.id_hp) p ON a.id_coa_temp = p.`id_hp`
WHERE a.date BETWEEN  '".$params['start_date']."' AND '".$params['end_date']."'
and b.coa IS NOT NULL
) a union all select *,keterangan as banks from (

SELECT a.*,
b.coa kr_coa,b.note as k_note,
IF(b.keterangan IS NULL,a.keterangan,b.keterangan) kr_note,
IF(a.value_real IS NULL,a.value_real,a.value_real) sum_amnt,
p.`no_invoice` invoice_p, p.name_eksternal users FROM (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=1 AND a.id_parent <> 0 ".$where."
)a LEFT JOIN (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=0 AND a.id_parent = 0
)b ON a.id_parent=b.id
LEFT JOIN (SELECT c.id_hp,f.no_invoice,k.name_eksternal FROM `t_payment_hp` c 
					LEFT JOIN `d_payment_hp` d ON c.`id_hp` = d.`id_hp`
					LEFT JOIN `t_invoice_pembelian` f ON d.id_invoice = f.`id_invoice`
					LEFT JOIN `t_bpb` i ON f.`id_bpb` = i.`id_bpb`
					LEFT JOIN `t_purchase_order` j ON j.id_po = i.`id_po`
					LEFT JOIN `t_eksternal` k ON j.id_distributor = k.`id` 
					WHERE c.payment_type = 0
					GROUP BY d.id_hp) p ON a.id_coa_temp = p.`id_hp`
WHERE a.date BETWEEN  '".$params['start_date']."' AND '".$params['end_date']."'
AND b.coa IS NOT NULL
) b union all SELECT *,keterangan as banks FROM (

SELECT a.*,
b.coa kr_coa,b.note as k_note,
IF(b.keterangan IS NULL,a.keterangan,b.keterangan) kr_note,
IF(a.value_real IS NULL,a.value_real,a.value_real) sum_amnt,
p.`no_invoice` invoice_p, p.cust_name users FROM (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=0 AND a.id_parent = 0 ".$where."
)a LEFT JOIN (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=1 AND a.id_parent <> 0
)b ON a.id=b.id_parent
LEFT JOIN (SELECT c.id_hp,e.no_invoice,h.cust_name FROM `t_payment_hp` c 
					LEFT JOIN `d_payment_hp` d ON c.`id_hp` = d.`id_hp`
					LEFT JOIN `t_invoice_penjualan` e ON d.id_invoice = e.`id_invoice`
					LEFT JOIN `t_penjualan` g ON e.`id_penjualan` = g.`id_penjualan`
					LEFT JOIN `t_customer` h ON g.`id_customer` = `id_t_cust`
					where c.payment_type = 1
					GROUP BY d.id_hp) p ON a.id_coa_temp = p.`id_hp`
WHERE a.date BETWEEN  '".$params['start_date']."' AND '".$params['end_date']."'
AND b.coa IS NOT NULL
) c union all SELECT *,keterangan as banks FROM  (

SELECT a.*,
b.coa kr_coa,b.note as k_note,
IF(b.keterangan IS NULL,a.keterangan,b.keterangan) kr_note,
IF(b.value_real IS NULL,b.value_real,a.value_real) sum_amnt,
p.`no_invoice` invoice_p, p.cust_name users FROM (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=0 AND a.id_parent <> 0 ".$where."
)a LEFT JOIN (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=1 AND a.id_parent = 0
)b ON a.id_parent=b.id
LEFT JOIN (SELECT c.id_hp,e.no_invoice,h.cust_name FROM `t_payment_hp` c 
					LEFT JOIN `d_payment_hp` d ON c.`id_hp` = d.`id_hp`
					LEFT JOIN `t_invoice_penjualan` e ON d.id_invoice = e.`id_invoice`
					LEFT JOIN `t_penjualan` g ON e.`id_penjualan` = g.`id_penjualan`
					LEFT JOIN `t_customer` h ON g.`id_customer` = `id_t_cust`
					WHERE c.payment_type = 1
					GROUP BY d.id_hp) p ON a.id_coa_temp = p.`id_hp`
WHERE a.date BETWEEN  '".$params['start_date']."' AND '".$params['end_date']."'
AND b.coa IS NOT NULL
) d union ALL SELECT *,keterangan as banks FROM (

SELECT a.*,
b.coa kr_coa,b.note as k_note,
b.keterangan kr_note,
b.value_real sum_amnt,
p.`no_invoice` invoice_p, p.name_eksternal users FROM (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=1 AND a.id_parent = 0 ".$where."
)a LEFT JOIN (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=1 AND a.id_parent <> 0
)b ON a.id=b.id_parent
LEFT JOIN (SELECT c.id_hp,f.no_invoice,k.name_eksternal FROM `t_payment_hp` c 
					LEFT JOIN `d_payment_hp` d ON c.`id_hp` = d.`id_hp`
					LEFT JOIN `t_invoice_pembelian` f ON d.id_invoice = f.`id_invoice`
					LEFT JOIN `t_bpb` i ON f.`id_bpb` = i.`id_bpb`
					LEFT JOIN `t_purchase_order` j ON j.id_po = i.`id_po`
					LEFT JOIN `t_eksternal` k ON j.id_distributor = k.`id` 
					WHERE c.payment_type = 0
					GROUP BY d.id_hp) p ON a.id_coa_temp = p.`id_hp`
WHERE a.date BETWEEN  '".$params['start_date']."' AND '".$params['end_date']."'
AND b.coa IS not NULL
) e UNION ALL SELECT *,keterangan as banks FROM (

SELECT a.*,
b.coa kr_coa,b.note as k_note,
b.keterangan kr_note,
b.value_real sum_amnt,
p.`no_invoice` invoice_p, p.name_eksternal users FROM (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=0 AND a.id_parent = 0 ".$where."
)a LEFT JOIN (
SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
JOIN t_coa b ON a.id_coa=b.id_coa
WHERE a.type_cash=0 AND a.id_parent <> 0
)b ON a.id=b.id_parent
LEFT JOIN (SELECT c.id_hp,f.no_invoice,k.name_eksternal FROM `t_payment_hp` c 
					LEFT JOIN `d_payment_hp` d ON c.`id_hp` = d.`id_hp`
					LEFT JOIN `t_invoice_pembelian` f ON d.id_invoice = f.`id_invoice`
					LEFT JOIN `t_bpb` i ON f.`id_bpb` = i.`id_bpb`
					LEFT JOIN `t_purchase_order` j ON j.id_po = i.`id_po`
					LEFT JOIN `t_eksternal` k ON j.id_distributor = k.`id` 
					WHERE c.payment_type = 0
					GROUP BY d.id_hp) p ON a.id_coa_temp = p.`id_hp`
WHERE a.date BETWEEN  '".$params['start_date']."' AND '".$params['end_date']."'
AND b.coa IS NOT NULL
) e 

order by `date`

		
		";
}
		//print_r($sql);

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return [
			'data' => $result
		];

		//return $result;
		
	}	
	
	public function list_report_cash_saldo($params = []){
		
		if($params['coa_id'] == 0){
			
			$sql 	= '
			SELECT (masuk-keluar) AS saldo_awal FROM (
			SELECT IF(SUM(value_real) IS NULL,0,SUM(value_real)) AS masuk FROM t_coa_value a
			WHERE type_cash = 0
			AND a.date < "'.$params['start_date'].'"
			) a,(
			SELECT IF(SUM(value_real) IS NULL,0,SUM(value_real)) AS keluar FROM t_coa_value a
			WHERE type_cash = 1
			AND a.date < "'.$params['start_date'].'"

			) b
			';
			
		}else{
			
			$sql 	= '
			SELECT (masuk-keluar) AS saldo_awal FROM (
			SELECT IF(SUM(value_real) IS NULL,0,SUM(value_real)) AS masuk FROM t_coa_value a
			WHERE id_coa = '.$params['coa_id'].' AND type_cash = 0
			AND a.date < "'.$params['start_date'].'"
			) a,(
			SELECT IF(SUM(value_real) IS NULL,0,SUM(value_real)) AS keluar FROM t_coa_value a
			WHERE id_coa = '.$params['coa_id'].' AND type_cash = 1
			AND a.date < "'.$params['start_date'].'"

			) b
			';
			
		}
		
		

		//print_r($sql);die;

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return [
			'data' => $result
		];

		//return $result;
		
	}
	
	public function get_cash_all(){
		
				
			$sql 	= '
			SELECT * FROM t_coa WHERE id_parent = 3
			order by id_coa
			';

		//print_r($sql);die;

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return [
			'data' => $result
		];

		//return $result;
		
	}

	/**
	 * get list of coa accounts
	 * 
	 * @return array
	 */
	public function get_coa_list()
	{
		$queryBuilder = $this->db
			->select('*')
			->from('t_coa');

		$result = $queryBuilder->get()->result_array();

		return $result;
	}
}
