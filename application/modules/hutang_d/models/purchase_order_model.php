<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Purchase_order_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}


	public function list_po($params = array())
	{

		if ($params['filter'] == 2) {

			$query_s = ' ';
		} else {

			$query_s = ' AND a.status = ' . $params['filter'];
		}

		if ($params['filter_month'] == 'ALL') {
			$query_m = ' ';
		} else {
			$query_m = ' AND DATE_FORMAT(b.date_po,"%M") = "' . $params['filter_month'] . '"';
		}

		if ($params['filter_year'] == 'ALL') {
			$query_y = ' ';
		} else {
			$query_y = 'AND DATE_FORMAT(b.date_po,"%Y") = "' . $params['filter_year'] . '" ';
		}

		$where_sts = $query_s . ' ' . $query_m . ' ' . $query_y;

		$query = 	'
				SELECT COUNT(*) AS jumlah FROM 
				( 
					SELECT uom_symbol as symbol,(f.price + (f.price * (b.ppn/100))) as total_invoice,j.bayar total_bayar, bb.no_do,a.id_invoice,a.no_invoice,a.tanggal_invoice,a.due_date,attention,a.faktur_pajak,a.note,sign,a.status as status_invoice,b.*, e.`name_eksternal` 
					FROM `t_invoice_pembelian` a
					JOIN t_bpb bb on a.id_bpb  = bb.id_bpb
					JOIN `t_purchase_order` b ON bb.id_po = b.`id_po`
					LEFT JOIN `t_eksternal` e ON b.id_distributor = e.`id`
					LEFT JOIN d_invoice_pembelian f on a.id_invoice = f.id_inv_pembelian
					LEFT JOIN t_bpb_detail fa on fa.id_bpb_detail = f.id_bpb_detail
					LEFT JOIN t_po_mat g on g.id_t_ps = fa.id_po_mat
					LEFT JOIN m_material h on h.id_mat = g.id_material
					LEFT JOIN m_uom i on h.unit_terkecil = i.id_uom
					LEFT JOIN (
						SELECT b.`id_invoice`,SUM(b.ammount) AS bayar FROM  `t_payment_hp` a
						JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp` GROUP BY id_invoice
					) j on j.id_invoice = a.id_invoice
					where 1 = 1  ' . $where_sts . '
					AND ( 
					a.no_invoice LIKE "%' . $params['searchtxt'] . '%"  OR
						e.name_eksternal LIKE "%' . $params['searchtxt'] . '%"  OR
						b.no_po LIKE "%' . $params['searchtxt'] . '%"
					) group by a.id_invoice )z
			';

		$query2 = 	'
				SELECT z.*, rank() over ( ORDER BY tanggal_invoice,total_invoice DESC) AS Rangking from ( 
					SELECT uom_symbol as symbol,( ((SUM(f.qty) - IF(SUM(tp.retur_qty) IS NULL,0,SUM(tp.retur_qty)))*g.unit_price ) + ((SUM(f.price)- (IF(SUM(tp.retur_qty) IS NULL,0,SUM(tp.retur_qty))*g.unit_price)) * (b.ppn/100)))  as total_invoice,j.bayar total_bayar, bb.no_do,a.id_invoice,a.no_invoice,a.tanggal_invoice,a.due_date,attention,a.faktur_pajak,a.note,sign,a.status as status_invoice,b.*, e.`name_eksternal` 
					FROM `t_invoice_pembelian` a
					JOIN t_bpb bb on a.id_bpb  = bb.id_bpb
					JOIN `t_purchase_order` b ON bb.id_po = b.`id_po`
					LEFT JOIN `t_eksternal` e ON b.id_distributor = e.`id`
					LEFT JOIN d_invoice_pembelian f on a.id_invoice = f.id_inv_pembelian
					LEFT JOIN t_bpb_detail fa on fa.id_bpb_detail = f.id_bpb_detail
					LEFT JOIN t_po_mat g on g.id_t_ps = fa.id_po_mat
					LEFT JOIN t_detail_retur_pembelian tp on g.id_t_ps = tp.id_po_mat
					LEFT JOIN m_material h on h.id_mat = g.id_material
					LEFT JOIN m_uom i on h.unit_terkecil = i.id_uom
					LEFT JOIN (
						SELECT b.`id_invoice`,SUM(b.ammount) AS bayar FROM  `t_payment_hp` a
						JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp` where a.payment_status = 1 GROUP BY id_invoice
					) j on j.id_invoice = a.id_invoice
					where 1 = 1  ' . $where_sts . '
					AND ( 
					a.no_invoice LIKE "%' . $params['searchtxt'] . '%"  OR
						e.name_eksternal LIKE "%' . $params['searchtxt'] . '%"  OR
						b.no_po LIKE "%' . $params['searchtxt'] . '%"
					) group by a.id_invoice order by tanggal_invoice DESC) z
				ORDER BY tanggal_invoice,total_invoice DESC
				
				LIMIT ' . $params['limit'] . ' 
				OFFSET ' . $params['offset'] . ' 
			';

		//echo $query2;die;

		$out		= array();
		$querys		= $this->db->query($query);
		$result = $querys->row();

		$total_filtered = $result->jumlah;
		$total 			= $result->jumlah;

		if (($params['offset'] + 10) > $total_filtered) {
			$limit_data = $total_filtered - $params['offset'];
		} else {
			$limit_data = $params['limit'];
		}



		//echo $query;die;
		//echo $query;die;
		$query2s		= $this->db->query($query2);
		$result2 = $query2s->result_array();
		$return = array(
			'data' => $result2,
			'total_filtered' => $total_filtered,
			'total' => $total,
		);
		return $return;
	}

	public function get_list_bk($params)
	{
		//$sql 	= 'CALL uom_search_id(?)';


		$sql 	= '
		select a.*,b.coa,b.keterangan,c.id_external FROM t_coa_value a 
		join t_coa b on a.id_coa = b.id_coa
		join t_jurnal_trans c on a.id = c.coa_value
		where c.tipe_trans = 1 and c.id_external = ' . $params['name'] . ' ';

		//print_r($sql);die;

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}	
	
	public function get_list_bk_p($params)
	{
		//$sql 	= 'CALL uom_search_id(?)';


		$sql 	= '
			
		SELECT z.*,total_invoice - bayar AS sisa, RANK() OVER ( ORDER BY tanggal_invoice,total_invoice DESC) AS Rangking FROM ( 
					SELECT SUM(f.price) AS total_invoice,a.id_invoice,a.no_invoice,a.tanggal_invoice,a.due_date,attention,a.faktur_pajak,note,SIGN,a.status AS status_invoice,b.*, e.`cust_name` ,
					IF(g.bayar IS NULL,0,g.bayar) AS bayar
					FROM `t_invoice_penjualan` a
					JOIN `t_penjualan` b ON a.id_penjualan = b.`id_penjualan`
					LEFT JOIN `t_customer` e ON b.id_customer = e.`id_t_cust`
					LEFT JOIN d_invoice_penjualan f ON a.id_invoice = f.id_inv_penjualan
					LEFT JOIN (
						SELECT b.`id_invoice`,SUM(b.ammount) AS bayar FROM  `t_payment_hp` a
						JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp` WHERE a.payment_type = 1 GROUP BY id_invoice
					) g ON g.`id_invoice` = f.`id_inv_penjualan`
					WHERE 1 = 1  GROUP BY id_invoice ORDER BY tanggal_invoice DESC) z

		';

		//print_r($sql);die;

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_bayar($params)
	{
		//$sql 	= 'CALL uom_search_id(?)';


		$sql 	= '
		SELECT a.*,e.`id_distributor`,b.`id_invoice`,`ammount`,c.due_date,c.no_invoice, f.keterangan as coa_name FROM t_payment_hp a 
		JOIN d_payment_hp b ON a.id_hp = b.id_hp
		JOIN t_invoice_pembelian c ON b.id_invoice = c.id_invoice
		JOIN t_bpb d ON d.id_bpb = c.id_bpb
		JOIN `t_purchase_order` e ON d.id_po = e.id_po
		JOIN `t_coa` f ON f.id_coa = a.cash_account
		where a.id_hp = ' . $params['id'] . ' ';

		//print_r($sql);die;

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_bayar_detail($params)
	{
		//$sql 	= 'CALL uom_search_id(?)';


		// $sql 	= ' 
				// SELECT c.*,a.ammount FROM `d_payment_hp` a
				// JOIN `t_payment_hp` b ON a.id_hp = b.id_hp
				// JOIN `t_invoice_pembelian` c ON a.`id_invoice` = c.`id_invoice`
				// WHERE b.id_hp =  ' . $params['id'] . ' ';
				
		$sql 	= '
				SELECT c.*,
			a.ammount,SUM(d.price-(d.price*(d.diskon/100))) terbilang
			FROM d_payment_hp a
        JOIN t_payment_hp b ON a.id_hp = b.id_hp
        JOIN t_invoice_pembelian c ON a.id_invoice = c.id_invoice
        JOIN d_invoice_pembelian d ON c.id_invoice=d.id_inv_pembelian
        WHERE b.id_hp =  ' . $params['id'] . '
        GROUP BY c.id_invoice';

		//print_r($sql);die;

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_principal($params = array())
	{

		$query = $this->db->get_where('t_eksternal', array('type_eksternal' => 1));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}


	public function get_po_byid($params = array())
	{

		$this->db->select('t_purchase_order.*,t_eksternal.* ,no_do,t_bpb.tanggal as tanggal_bpb,id_bpb')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_purchase_order')
			->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id')
			->join('t_bpb', 't_bpb.id_po = t_purchase_order.id_po')
			->where(array('t_bpb.id_bpb' => $params['id']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function list_pembayaran($params = array())
	{

		// if($params['filter'] == 2 || $params['filter'] == ''){

		$query_s = ' ';

		// }else{

		// $query_s = ' AND a.payment_status = '.$params['filter'];
		// }

		if ($params['filter_month'] == 'ALL') {
			$query_m = ' ';
		} else {
			$query_m = ' AND DATE_FORMAT(a.date,"%M") = "' . $params['filter_month'] . '"';
		}

		if ($params['filter_year'] == 'ALL') {
			$query_y = ' ';
		} else {
			$query_y = 'AND DATE_FORMAT(a.date,"%Y") = "' . $params['filter_year'] . '" ';
		}

		$where_sts = $query_s . ' ' . $query_m . ' ' . $query_y;


		$query = 	'
				SELECT COUNT(*) AS jumlah 
				 FROM  `t_payment_hp` a
					JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp`
					JOIN `t_invoice_pembelian` c ON b.id_invoice = c.`id_invoice`
					JOIN `t_bpb` d ON d.`id_bpb` = c.`id_bpb`
					JOIN `t_purchase_order` f ON d.`id_po` = f.`id_po`
					JOIN `t_eksternal` e ON e.`id` = f.`id_distributor`
					where 1 = 1 ' . $where_sts . ' AND ( 
						c.no_invoice LIKE "%' . $params['searchtxt'] . '%" OR
						a.no_payment LIKE "%' . $params['searchtxt'] . '%" OR
						name_eksternal LIKE "%' . $params['searchtxt'] . '%" OR
						a.date LIKE "%' . $params['searchtxt'] . '%"
						
					)
			';

		$query2 = 	'
					SELECT z.*, RANK() OVER ( ORDER BY id_hp DESC) AS Rangking FROM ( 

					SELECT a.*,SUM(b.ammount) AS total,`name_eksternal`, eg.keterangan FROM  `t_payment_hp` a
					JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp`
					JOIN `t_invoice_pembelian` c ON b.id_invoice = c.`id_invoice`
					JOIN `t_bpb` d ON d.`id_bpb` = c.`id_bpb`
					JOIN `t_purchase_order` f ON d.`id_po` = f.`id_po`
					JOIN `t_eksternal` e ON e.`id` = f.`id_distributor`
					JOIN `t_coa` eg ON a.`cash_account` = eg.`id_coa`
					WHERE a.`payment_type` = 0 ' . $where_sts . ' AND ( 
						a.no_payment LIKE "%' . $params['searchtxt'] . '%" OR
						c.no_invoice LIKE "%' . $params['searchtxt'] . '%" OR
						name_eksternal LIKE "%' . $params['searchtxt'] . '%" OR
						a.date LIKE "%' . $params['searchtxt'] . '%"
					)
					GROUP BY a.`id_hp`

					) z
				order by ' . $params['order_column'] . ' ' . $params['order_dir'] . '
				
				
				
				LIMIT ' . $params['limit'] . ' 
				OFFSET ' . $params['offset'] . ' 
			';

		//echo $query2;die;

		$out		= array();
		$querys		= $this->db->query($query);
		$result = $querys->row();

		$total_filtered = $result->jumlah;
		$total 			= $result->jumlah;

		if (($params['offset'] + 10) > $total_filtered) {
			$limit_data = $total_filtered - $params['offset'];
		} else {
			$limit_data = $params['limit'];
		}



		//echo $query;die;
		//echo $query;die;
		$query2s		= $this->db->query($query2);
		$result2 = $query2s->result_array();
		$return = array(
			'data' => $result2,
			'total_filtered' => $total_filtered,
			'total' => $total,
		);
		return $return;
	}

	public function add_value($data)
	{
		$datas = array(
			'id_coa' => $data['coa'],
			'date' => $data['tgl'],
			'date_insert' => date('Y-m-d H:i:s'),
			'value' => $data['jumlah'],
			'value_real' => $data['jumlah'],
			'type_cash' => 0,
			'id_valas' => 1,
			'note' => $data['ket']
		);

		//	print_r($datas);die;

		$this->db->insert('t_coa_value', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function update_seq($data)
	{
		$sql 	= 'update t_ordering set seq_max = seq_max+1 where nama_menu = "' . $data . '" ';


		$query 	= $this->db->query($sql);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_payment($data)
	{
		$data_insert_mutasi = array(
			'no_payment'  => $data['kode'],
			'payment_type' => 0,
			'date'    		=> $data['tgl'],
			'tgl_terbit'  => $data['tglterbit'],
			// 'tgl_aktif'  => $data['tglaktif'],
			'tgl_jatuh_tempo'  => $data['tgljatuhtempo'],
			'cash_account'    => $data['cash_acc'],
			'note'    => $data['keterangan'],
			'id_invoice'    => $data['inv'],
			'account_type'    => $data['tipe_bayar']
		);

		//	print_r($datas);die;

		$this->db->insert('t_payment_hp', $data_insert_mutasi);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_detail_payment($data)
	{



		$this->db->insert('d_payment_hp', $data);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function get_po_by_inv($params = array())
	{

		$this->db->select('t_purchase_order.*,t_eksternal.*,t_invoice_pembelian.id_invoice,t_invoice_pembelian.no_invoice,t_invoice_pembelian.tanggal_invoice,t_invoice_pembelian.due_date,faktur_pajak,tanggal_faktur,,t_bpb.id_bpb, t_invoice_pembelian.note, count(id_t_ps) as rowr ')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_purchase_order')
			->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id')
			->join('t_bpb', 't_bpb.id_po = t_purchase_order.id_po')
			->join('t_invoice_pembelian', 't_bpb.id_bpb = t_invoice_pembelian.id_bpb')
			->join('t_bpb_detail', 't_bpb_detail.id_bpb = t_bpb.id_bpb')
			->join('t_po_mat', 't_bpb_detail.id_po_mat = t_po_mat.id_t_ps')
			->where(array('t_invoice_pembelian.id_invoice' => $params['id']))
			->group_by('t_bpb.id_bpb');
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}


	public function get_po_by_payment($params = array())
	{

		$this->db->select('t_purchase_order.*,t_eksternal.*,t_invoice_pembelian.id_invoice,t_invoice_pembelian.no_invoice,t_invoice_pembelian.tanggal_invoice,t_invoice_pembelian.due_date,faktur_pajak,tanggal_faktur,,t_bpb.id_bpb, t_invoice_pembelian.note, count(id_t_ps) as rowr ')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_purchase_order')
			->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id')
			->join('t_bpb', 't_bpb.id_po = t_purchase_order.id_po')
			->join('t_invoice_pembelian', 't_bpb.id_bpb = t_invoice_pembelian.id_bpb')
			->join('t_bpb_detail', 't_bpb_detail.id_bpb = t_bpb.id_bpb')
			->join('t_po_mat', 't_bpb_detail.id_po_mat = t_po_mat.id_t_ps')
			->join('d_payment_hp', 'd_payment_hp.id_invoice = t_invoice_pembelian.id_invoice')
			->where(array('d_payment_hp.id_hp' => $params['id']))
			->group_by('t_bpb.id_bpb');
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_cash_account($params = array())
	{

		// $this->db->select('t_purchase_order.*,t_eksternal.name_eksternal,no_do,t_bpb.tanggal as tanggal_bpb,id_bpb ')
		// #$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
		// ->from('t_purchase_order')
		// ->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id')
		// ->join('t_bpb', 't_bpb.id_po = t_purchase_order.id_po')
		// ->where(array('t_purchase_order.status' => 1));
		// $query = $this->db->get();

		// $query = "
		// SELECT * FROM t_coa
		// WHERE id_coa BETWEEN 4 AND 21
		// #OR id_coa IN (122,123)		
		// ORDER BY id_coa 
		// ";
		
		$query = "
		SELECT * FROM t_coa
		WHERE id_parent = 3
		#OR id_coa IN (122,123)		
		ORDER BY id_coa 
		";

		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_po($params = array())
	{

		// $this->db->select('t_purchase_order.*,t_eksternal.name_eksternal,no_do,t_bpb.tanggal as tanggal_bpb,id_bpb ')
		// #$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
		// ->from('t_purchase_order')
		// ->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id')
		// ->join('t_bpb', 't_bpb.id_po = t_purchase_order.id_po')
		// ->where(array('t_purchase_order.status' => 1));
		// $query = $this->db->get();

		$query = "
		SELECT t_purchase_order.*,t_eksternal.name_eksternal,no_do,t_bpb.tanggal AS tanggal_bpb,t_bpb.id_bpb,`no_invoice`  FROM
		t_purchase_order JOIN t_eksternal ON t_purchase_order.id_distributor = t_eksternal.id
		JOIN t_bpb ON t_bpb.id_po = t_purchase_order.id_po
		LEFT JOIN `t_invoice_pembelian` ON t_bpb.`id_bpb` = `t_invoice_pembelian`.`id_bpb`
		WHERE `no_invoice` IS NULL
		";

		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_bayar_inv($params = array())
	{

		// $this->db->select('t_purchase_order.*,t_eksternal.name_eksternal,no_do,t_bpb.tanggal as tanggal_bpb,id_bpb ')
		// #$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
		// ->from('t_purchase_order')
		// ->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id')
		// ->join('t_bpb', 't_bpb.id_po = t_purchase_order.id_po')
		// ->where(array('t_purchase_order.status' => 1));
		// $query = $this->db->get();

		$query = "
		SELECT * FROM t_payment_hp 
		WHERE `id_invoice` = " . $params['id'] . "
		";

		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_price_mat($params = array())
	{

		// $query = $this->db->get_where('t_po_mat', array('id_po' => $params['kode']));
		// $query = $this->db->order_by('id_t_ps', 'desc');

		$this->db->select('*')
			->from('t_po_mat')
			->where(array('id_material' => $params['kode']))
			->order_by('id_t_ps', 'desc');
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_principal_byid($params = array())
	{

		$query = $this->db->get_where('t_eksternal', array('id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}


	public function get_item_byprin($params = array())
	{
		$this->db->select('m_material.*, m_uom.uom_symbol')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('m_material')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('dist_id' => $params['id_prin']));
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_uom($data)
	{
		$datas = array(
			'id_coa' => $data['coa'],
			'date' => $data['tgl'],
			'date_insert' => date('Y-m-d H:i:s'),
			'value' => $data['jumlah'],
			'value_real' => $data['jumlah'],
			'type_cash' => $data['type_cash'],
			'bukti' => $data['bukti'],
			'id_coa_temp' => $data['id_coa_temp'],
			'id_parent' => $data['id_parent'],
			'id_valas' => 1,
			'note' => $data['ket']
		);

		//	print_r($datas);die;

		$this->db->insert('t_coa_value', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function get_invoice_byprin($params = array())
	{

		$query = "
		SELECT * FROM (
			SELECT A.*,(SUM(B.price) - (if(sum(tp.retur_qty) is null,0,sum(tp.retur_qty))*ga.unit_price) )+ ( (SUM(B.price) - (if(sum(tp.retur_qty) is null,0,sum(tp.retur_qty))*ga.unit_price)  )*(ppn/100) ) AS amount, IF(bayar IS NULL,0,bayar) AS bayar, (( (SUM(B.price) - (if(sum(tp.retur_qty) is null,0,sum(tp.retur_qty))*ga.unit_price) )+ ( (SUM(B.price)- (if(sum(tp.retur_qty) is null,0,sum(tp.retur_qty))*ga.unit_price))*ppn/100 )) - IF(bayar IS NULL,0,bayar)) AS sisa , IF(bayar_giro IS NULL,0,bayar_giro) AS bayar_giro 
				FROM `t_invoice_pembelian` A
				JOIN `d_invoice_pembelian` B ON A.`id_invoice` = B.`id_inv_pembelian` 
				JOIN `t_bpb` C ON A.`id_bpb` = C.`id_bpb`
				JOIN `t_purchase_order` f ON C.`id_po` = f.`id_po`
					LEFT JOIN t_bpb_detail fa on fa.id_bpb_detail = B.id_bpb_detail
					LEFT JOIN t_po_mat ga on ga.id_t_ps = fa.id_po_mat
					LEFT JOIN t_detail_retur_pembelian tp on ga.id_t_ps = tp.id_po_mat
					LEFT JOIN m_material ha on ha.id_mat = ga.id_material
				LEFT JOIN (SELECT a.*,b.id_invoice as inv ,SUM(b.`ammount`) AS bayar FROM `t_payment_hp` a 
				join `d_payment_hp` b on a.`id_hp` = b.`id_hp`				
				WHERE a.payment_type = 0 AND ( a.account_type = 0 or a.payment_status = 1 ) GROUP BY b.id_invoice) g ON g.`inv` = A.`id_invoice`
				
				LEFT JOIN (SELECT a.*,b.id_invoice as inv ,SUM(b.`ammount`) AS bayar_giro FROM `t_payment_hp` a 
				join `d_payment_hp` b on a.`id_hp` = b.`id_hp`				
				WHERE a.payment_type = 0 AND a.account_type = 1 AND a.payment_status = 0 GROUP BY b.id_invoice) h ON h.`inv` = A.`id_invoice`
				
				WHERE `id_distributor` = " . $params['id_prin'] . "
				GROUP BY A.`no_invoice`
		) S WHERE sisa > 0
		";

		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_payment_detail_now($id)
	{

		// $query = "
		// SELECT a.*, SUM(b.`ammount`) AS bayar FROM `t_payment_hp` a 
		// join `d_payment_hp` b on a.`id_hp` = b.`id_hp`				
		// WHERE a.payment_type = 0 and a.payment_status = 0 
		// and a.id_hp = ".$id."
		// GROUP BY id_invoice
		// ";

		$query = "
			SELECT a.*,bayar FROM `t_payment_hp` a 
				LEFT JOIN ( 
					SELECT *,SUM(a.`ammount`) AS bayar FROM `d_payment_hp` a 
					WHERE a.payment_type = 1 
					GROUP BY a.id_invoice
				 ) b ON a.`id_hp` = b.`id_hp`					
				
				WHERE a.id_hp = " . $id . "
		";

		//	echo $query;die;

		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_payment_detail($id)
	{

		$query = "
		SELECT A.*,SUM(price) + ( SUM(price)*(f.ppn/100) ) AS amount, IF(bayar IS NULL,0,bayar) AS bayar, ((SUM(price) + ( SUM(price)*(f.ppn/100) )) - IF(bayar IS NULL,0,bayar)) AS sisa , kl.cash_account, sum(k.ammount) as bayar_now, g.account_type,
			kl.date as date_pay
				FROM `t_invoice_pembelian` A
				JOIN `d_invoice_pembelian` B ON A.`id_invoice` = B.`id_inv_pembelian` 
				JOIN `t_bpb` C ON A.`id_bpb` = C.`id_bpb`
				JOIN `t_purchase_order` f ON C.`id_po` = f.`id_po`
				LEFT JOIN (SELECT a.*,b.id_invoice AS inv ,SUM(b.`ammount`) AS bayar FROM `t_payment_hp` a 
				JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp`				
				WHERE a.payment_type = 0  GROUP BY b.id_invoice) g ON g.`inv` = A.`id_invoice`
				JOIN d_payment_hp k ON A.`id_invoice` = k.`id_invoice`
				JOIN t_payment_hp kl ON kl.`id_hp` = k.`id_hp`
				WHERE k.`id_hp` = " . $id . "
				GROUP BY A.`no_invoice`
		";

		//	echo $query;die;

		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_item_byprin_all($params = array())
	{
		$this->db->select('m_material.*, m_uom.uom_symbol')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('m_material')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			//->where(array('dist_id' => $params['id_prin']));
			->where('m_material.dist_id != ', $params['id_prin'], FALSE);
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_kode($params = array())
	{

		$query =  $this->db->select('seq_max as max')
			->from('t_ordering')
			->where('nama_menu', 'pembayaran');;

		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function edit_po($data)
	{
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'status' => 5
		);

		//	print_r($datas);die;
		$this->db->where('id_po', $data['id_po']);
		$this->db->update('t_purchase_order', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function update_konfirmas($id)
	{

		$datas = array(
			'payment_status' => 1
		);

		//	print_r($datas);die;
		$this->db->where('id_hp', $id);
		$this->db->update('t_payment_hp', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function update_invoice_sts($id)
	{

		$datas = array(
			'status' => 1
		);

		//	print_r($datas);die;
		$this->db->where('id_invoice', $id);
		$this->db->update('t_invoice_pembelian', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	// public function get_po($params = array()){

	// // $query = $this->db->get_where('t_purchase_order', array('id_po' => $params['id']));
	// $this->db->select('t_purchase_order.*, t_eksternal.*')
	// #$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
	// ->from('t_purchase_order')
	// ->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id')
	// ->where(array('id_po' => $params['id']));
	// $query = $this->db->get();

	// $return = $query->result_array();

	// $this->db->close();
	// $this->db->initialize();

	// return $return;
	// }

	public function get_po_detail($params = array())
	{

		$query = $this->db->get_where('t_po_mat', array('id_po' => $params['id']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_po_detail_full($params = array())
	{

		//$query = $this->db->get_where('t_po_mat', array('id_po' => $params['id']));
		//$this->db->select('t_po_mat.*,(t_bpb_detail.qty+t_bpb_detail.qty_bonus) as qty_order, m_material.stock_code,m_material.stock_name,m_material.base_qty, m_uom.*,t_bpb_detail.id_bpb_detail')
		$this->db->select('t_po_mat.*,(t_bpb_detail.qty) - t_detail_retur_pembelian.qty_retur as qty_order, m_material.stock_code,m_material.stock_name,m_material.base_qty, m_uom.*,t_bpb_detail.id_bpb_detail')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_po_mat')
			->join('m_material', 't_po_mat.id_material = m_material.id_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->join('t_detail_retur_pembelian', 't_bpb_detail.id_po_mat = t_detail_retur_pembelian.id_po_mat','LEFT')
			->join('t_bpb_detail', 't_bpb_detail.id_po_mat = t_po_mat.id_t_ps')
			->where(array('id_bpb' => $params['id']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_po_detail_full_edit($params = array())
	{

		//$query = $this->db->get_where('t_po_mat', array('id_po' => $params['id']));
		//$this->db->select('t_po_mat.*,(t_bpb_detail.qty+t_bpb_detail.qty_bonus) as qty_order, m_material.stock_code,m_material.stock_name,m_material.base_qty, m_uom.*,t_bpb_detail.id_bpb_detail')
		$this->db->select('t_po_mat.*,(t_bpb_detail.qty) - t_detail_retur_pembelian.retur_qty is null as qty_order, m_material.stock_code,m_material.stock_name,m_material.base_qty, m_uom.*,t_bpb_detail.id_bpb_detail')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_po_mat')
			->join('m_material', 't_po_mat.id_material = m_material.id_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->join('t_bpb_detail', 't_bpb_detail.id_po_mat = t_po_mat.id_t_ps')
			->join('t_detail_retur_pembelian', 't_bpb_detail.id_po_mat = t_detail_retur_pembelian.id_po_mat','LEFT')
			->where(array('id_bpb' => $params['id_btb']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_po_detail_edit($params = array())
	{

		//$query = $this->db->get_where('t_po_mat', array('id_po' => $params['id']));
		$this->db->select('t_po_mat.*,t_po_mat.qty as qty_order, m_material.*, m_uom.*, d_invoice_pembelian.qty as qty_inv,
		d_invoice_pembelian.unit_price as unit_price_inv,d_invoice_pembelian.price as price_inv, d_invoice_pembelian.diskon as diskon_inv')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_po_mat')
			->join('m_material', 't_po_mat.id_material = m_material.id_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->join('d_invoice_pembelian', 'd_invoice_pembelian.id_po_mat = t_po_mat.id_t_ps')
			->join('t_invoice_pembelian', 'd_invoice_pembelian.id_inv_pembelian = t_invoice_pembelian.id_invoice')
			->where(array('t_invoice_pembelian.id_invoice' => $params['id_invoice']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function check_principal($params = array())
	{

		$query = $this->db->get_where('t_eksternal', array('kode_eksternal' => $params['kode']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_invoice($data)
	{

		$this->db->insert('t_invoice_pembelian', $data);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_pay_invoice($data)
	{

		// print_r($data);die;

		$this->db->insert('t_payment_hp', $data);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_detail_invoice($data)
	{

		//	print_r($datas);die;

		$this->db->insert('d_invoice_pembelian', $data);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}


	public function add_po($data)
	{
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'no_po' => $data['no_po'],
			'date_po' => $data['date_po'],
			'id_distributor' => $data['id_distributor'],
			'ppn' => $data['ppn'],
			'term_of_payment' => $data['term_of_patyment'],
			'status' => 0,
			'id_valas' => 1,
			'rate' => 1,
			'seq_n' => $data['seq_n'],
			'keterangan' => $data['keterangan'],
			'total_amount' => $data['total_amount']
		);

		//	print_r($datas);die;

		$this->db->insert('t_purchase_order', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_po_mat($data)
	{
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'id_po' => $data['id_po'],
			'id_material' => $data['id_material'],
			'unit_price' => $data['unit_price'],
			'price' => $data['price'],
			'remarks' => $data['remark'],
			'qty' => $data['qty'],
			'diskon' => $data['diskon']
		);

		//	print_r($datas);die;

		$this->db->insert('t_po_mat', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_inv($data)
	{
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			//'no_po' => $data['no_po'],
			'no_invoice' => $data['no_invoice'],
			'tanggal_invoice' => $data['tanggal_invoice'],
			'due_date' => $data['due_date'],
			'faktur_pajak' => $data['faktur_pajak'],
			'note' => $data['note'],
			'tanggal_faktur' => $data['tanggal_faktur'],
			'attention' => $data['attention']
		);

		//	print_r($datas);die;
		$this->db->where('id_invoice', $data['id_invoice']);
		$this->db->update('t_invoice_pembelian', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_po_mat($data)
	{
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			//'no_po' => $data['no_po'],
			'unit_price' => $data['unit_price'],
			//'qty' => $data['qty'],
			'price' => $data['price'],
			'diskon' => $data['diskon']
		);

		//	print_r($datas);die;
		$this->db->where('id_t_ps', $data['id_po_mat']);
		$this->db->update('t_po_mat', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_po_apr($data)
	{
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'status' => $data['sts']
		);

		//	print_r($datas);die;
		$this->db->where('id_po', $data['id_po']);
		$this->db->update('t_purchase_order', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	function delete_po($id)
	{

		$this->db->where('id_invoice', $id);
		$this->db->delete('t_invoice_pembelian');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $result;
	}

	function delete_po_mat($id)
	{

		$this->db->where('id_inv_pembelian', $id);
		$this->db->delete('d_invoice_pembelian');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $result;
	}

	function delete_detail_invoiced($id)
	{

		$this->db->where('id_inv_pembelian', $id);
		$this->db->delete('d_invoice_pembelian');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $result;
	}

	function delete_hp($id)
	{

		$this->db->where('id_hp', $id);
		$this->db->delete('t_payment_hp');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $result;
	}

	function delete_hp_detail($id)
	{

		$this->db->where('id_hp', $id);
		$this->db->delete('d_payment_hp');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $result;
	}
}
