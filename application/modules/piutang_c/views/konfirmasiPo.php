<style>
	.dt-body-left {text-align:left;}
	.dt-body-right {text-align:right;}
	.dt-body-center {text-align:center; vertical-align: middle;}
	.force-overflow {height: 650px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow {min-height: 650px}
	#modal-distributor::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar
	{
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar-thumb
	{
		background-image: -webkit-gradient(linear,
		   left bottom,
		   left top,
		   color-stop(0.44, rgb(122,153,217)),
		   color-stop(0.72, rgb(73,125,189)),
		   color-stop(0.86, rgb(28,58,148)));
	}
	
	.select2-container .select2-choice {
		
		height : 35px !important;
		
	}

</style>

 <div class="product-sales-area mg-tb-30">
            <div class="container-fluid">
                        <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            <ul id="myTabedu1" class="tab-review-design">
                                <li class="active"><a href="#description">Konfirmasi Invoice Penjualan</a></li>
                            </ul>
                            <div id="myTabContent" class="tab-content custom-product-edit">
                                <div class="product-tab-list tab-pane fade active in" id="description">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="review-content-section">
                                                <form id="add_po" action="<?php echo base_url().'invoice_selling/approve_retur';?>" class="add-department" autocomplete="off">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
														  <div class="form-group">
																<label>Nomor Penjualan</label>
																<input name="ints" id="ints" type="hidden" class="form-control" placeholder="Tanggal Purchase Order" value= '0' >
																<input name="ints2" id="ints2" type="hidden" class="form-control" placeholder="Tanggal Purchase Order" value= '0' >
                                                                <select name="name" id="name" onChange="select_prin()" class="form-control" placeholder="Nama Customer" disabled="disabled">
																	<option value="0" selected="selected" disabled>-- Pilih Penjualan --</option>
																	<?php
																		foreach($result_p as $principals){
																			if($penjualan == $principals['id_penjualan'] ){
																				$cc = 'Selected="selected"';
																			}else{
																				$cc = '';
																			}
																			echo '<option value="'.$principals['id_penjualan'].'" '.$cc.' >'.$principals['no_penjualan'].' - '.$principals['cust_name'].'</option>';
																		}
																	?>
																</select>
                                                            </div>

															<div class="form-group date">
																<label>Tanggal</label>
																<input name="tgl" id="tgl" type="text" class="form-control" placeholder="Tanggal Penjualan" readOnly>
                                                            </div>
                                                            <div class="form-group">
																<label>Customer</label>
																 <input name="att" id="customer"  type="text" class="form-control" placeholder="Customer" readOnly>
                                                             
                                                            </div>
                                                            <div class="form-group">
																<label>Alamat</label>
                                                                <textarea name="alamat" id="alamat"  type="text" class="form-control" placeholder="Alamat Principal" readOnly></textarea>
                                                            </div>
                                                            <div class="form-group">
																<label>Att</label>
                                                                <input name="att" id="att"  type="text" class="form-control" placeholder="Att" readOnly>
                                                            </div><div class="form-group">
																<label>No Telp</label>
                                                                <input name="telp" id="telp"  type="text" class="form-control" placeholder="Telephone" readOnly>
                                                            </div>
															 <div class="form-group">
																<label>Email</label>
                                                                <input name="email" id="email"  type="text" class="form-control" placeholder="Email" readOnly>
                                                            </div>
															 <div class="form-group">
																<label>Limit Kredit</label>
                                                                <input name="limit" id="limit"  type="text" class="form-control" placeholder="Limit Kredit" readOnly>
                                                            </div>
														 <div class="form-group">
																<label>Sisa Kredit</label>
                                                                <input name="sisa" id="sisa"  type="text" class="form-control" placeholder="Sisa Kredit" readOnly>
                                                            </div></div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<div class="form-group">
																<label>Ship From:</label>
                                                                 <textarea name="alamat_kirim" id="alamat_kirim"  type="text" class="form-control" placeholder="Alamat Principal" readOnly >JL. Sangkuriang NO.38-A 
NPWP: 01.555.161.7.428.000</textarea>
                                                            </div>
															<div class="form-group date">
																<label>Email</label>
																<input name="emaila" id="emaila" type="email" class="form-control" placeholder="Email" readOnly >
                                                            </div>
															<div class="form-group date">
																<label>Telp/Fax</label>
																<input name="telpfax" id="telpfax" type="text" class="form-control" placeholder="Telp/Fax" value="(022) 6626-946" readOnly >
                                                            </div>
														
																<input name="ppn" id="ppn" type="hidden" class="form-control" placeholder="VAT/PPN" onKeyup="change_ppn()" value="10" >
															
															<div class="form-group date">
																<label>Term Pembayaran</label>
																<input name="term" id="term" type="hidden" class="form-control" placeholder="Term (Hari)" value="0" style="" readonly >
																<input name="term_o" id="term_o" type="number" class="form-control" placeholder="Term (Hari)" value="0" style="" readonly >
                                                            </div> 
															
															<div class="form-group date">
																<label>Keterangan Beli</label>
																<textarea name="ket" id="ket" class="form-control" placeholder="keterangan"  readOnly ></textarea>
                                                            </div>
															
															<div class="form-group date">
																<label>No Invoice</label>
																<input name="no_inv" id="no_inv" type="text" class="form-control" placeholder="Tanggal Penjualan" readOnly >
                                                            </div>
															
															<div class="form-group date">
																<label>Tanggal Invoice</label>
																<input name="tgl_inv" id="tgl_inv" type="text" class="form-control" placeholder="Tanggal Penjualan" readOnly >
                                                            </div>
															
															<div class="form-group date">
																<label>Tanggal Jatuh Tempo</label>
																<input name="tgl_due" id="tgl_due" type="text" class="form-control" placeholder="Tanggal Penjualan" readOnly >
                                                            </div>
															
															<div class="form-group date">
																<label>Keterangan Invoice</label>
																<textarea name="ket_inv" id="ket_inv" class="form-control" placeholder="keterangan" readOnly  ></textarea>
                                                            </div>
                                                    </div>
													</div>

                                                    
                                               
                                            </div>
                                        </div>
                                    </div>
									<br><br>
									<div class="row">
										<h3>Items</h3>
									</div>
									
										<div class="row">
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Kode</label>
												</div>
											</div>
											<div class="col-lg-2 col-md-1 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Retur Box</label>
												</div>
											</div>
											<div class="col-lg-1 col-md-2 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Retur Satuan</label>
												</div>
											</div>
											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Qty Retur</label>
												</div>
											</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Qty Order</label>
												</div>
											</div>
											
											<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Harga</label>
												</div>
											</div>
										
											<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Total</label>
												</div>
											</div>
										</div>
											<input name="int_flo" id="int_flo" type="hidden" class="form-control" placeholder="Qty" value=<?php echo $int_flo; ?> >
											<div id="table_items"><?php echo $item_form; ?></div>
									<br>
									
									
									<div class="row" style="border-top-style:solid;">
											<div style="margin-top:10px">
												<div class="col-lg-8 col-md-3 col-sm-3 col-xs-12">
													
												</div>
												<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
													<label>Total Invoice</label>
												</div>
												<div class="col-lg-2 col-md-1 col-sm-3 col-xs-12">
													<!--<label id="grand_total" style="float:right">0</label>-->
													<input name="grand_total" id="grand_total" onKeyup="" type="text" class="form-control " placeholder="Total" value=0 readOnly >
												</div>
												
											</div>
									</div>
									
									<div class="row" style="">
											<div style="margin-top:10px">
												<div class="col-lg-8 col-md-3 col-sm-3 col-xs-12">
													
												</div>
												<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
													<label>DPP</label>
												</div>
												<div class="col-lg-2 col-md-1 col-sm-3 col-xs-12">
													<!--<label id="grand_total" style="float:right">0</label>-->
													<input name="dpp" id="dpp" type="text" class="form-control " placeholder="Total" value=0 readOnly >
												</div>
												
											</div>
									</div>
									
									<div class="row" style="">
											<div style="margin-top:10px">
												<div class="col-lg-8 col-md-3 col-sm-3 col-xs-12">
													
												</div>
												<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
													<label>PPN</label>
												</div>
												<div class="col-lg-2 col-md-1 col-sm-3 col-xs-12">
													<!--<label id="grand_total" style="float:right">0</label>-->
													<input name="ppn_in" id="ppn_in" type="text" class="form-control " placeholder="Total" value=0 readOnly >
												</div>
												
											</div>
									</div>
							
								
									<div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="payment-adress">
																<button type="button" class="btn btn-danger waves-effect waves-light" onClick="bayar(<?php echo $id_invoice; ?>)"> Sudah Dibayar</button>
																<button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()"> Kembali</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                </div>
                               </div> </form>
                        </div>
                    </div>
                </div>
</div>
</div>

                        <div id="ModalAddPo" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-close-area modal-close-df">
                                        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                                    </div>
                                    <div class="modal-body">
                                        <i class="educate-icon educate-checked modal-check-pro"></i>
                                        <h2>Data Berhasil Disimpan</h2>
                                        <p>Apakah Anda Ingin Menambah Data Po Lagi ?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <a data-dismiss="modal" href="#" onClick="back()">Tidak</a>
                                        <a data-dismiss="modal" href="#" onClick="clearform()" >Ya</a>
                                    </div>
                                </div>
                            </div>
                        </div>
						
						<div id="ModalChangePrincipal" class="modal modal-edu-general Customwidth-popup-WarningModal PrimaryModal-bgcolor fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-close-area modal-close-df">
                                        <!--<a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>-->
                                    </div>
                                    <div class="modal-body">
                                        <span class="educate-icon educate-warning modal-check-pro information-icon-pro"></span>
                                        <h2>Warning!</h2>
                                        <p id="msg_err">The Modal plugin is a dialog box/popup window that is displayed on top of the current page</p>
                                    </div>
                                    <div class="modal-footer footer-modal-admin warning-md">
                                        <a data-dismiss="modal" href="#" onClick="okchange()">Ok</a>
                                        <a data-dismiss="modal" href="#" onClick="cancelchange()">Batal</a>
                                    </div>
                                </div>
                            </div>
                        </div>

<script>

  function bayar(id) {
    swal({
      title: 'Yakin akan Konfirmasi ?',
      text: 'data tidak dapat dikembalikan bila sudah di konfirmasi !',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak'
    }).then(function() {
      var datapost = {
        "id": id
      };

      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'invoice_selling/approve_retur'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
          $('.panel-heading button').trigger('click');
         // listdist();
          swal({
            title: 'Success!',
            text: response.message,
            type: 'success',
            showCancelButton: false,
            confirmButtonText: 'Ok'
          }).then(function() {
			  
			  back();
			  
		  })
        }
      });
    });
  }

function change_item(rs){
	
	
	
	var val = $('#kode_'+rs+'').val();
	
	var sss = val.split('|');
	
	//alert(val);
	
	var harga = new Intl.NumberFormat('de-DE', {}).format(sss[2]);
			
	
	$('#iteml_'+rs+'').html("Box @ "+sss[1]);
	$('#harga_'+rs+'').val(harga);
	
	change_sum(rs);
	
	
}

function  change_kode(rs){
	
	  var datapost = {
		"kode": $('#kode_'+rs+'').val()
      };
	
	$.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'invoice_selling/get_price_mat'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
			
			//var harga = new Intl.NumberFormat('de-DE', { }).format(response.list);
			
			$('#harga_'+rs+'').val(response.list);
			
	    }
    });
}

function tambah_row(){
	
	$('.hapus_btn').hide();
	var values = $('#int_flo').val();
	
	var new_fl = parseInt(values)+1;
	
	$('#int_flo').val(new_fl);
   var datapost = {
		"id_t_cust": $('#name').val()
      };

      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'invoice_selling/get_customer'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {

		var htl = '';
		htl +=	'<div id="row_'+new_fl+'" >';
		htl +=	'									<div class="row" style="border-top-style:solid;">';
		htl +=	'									<div style="margin-top:10px">';
		htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
		htl +=	'												<select onChange="change_item('+new_fl+')" name="kode_'+new_fl+'" id="kode_'+new_fl+'" class="form-control" placeholder="Nama Item">';
		htl +=	'													<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		htl +=	'												</select>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="qty_'+new_fl+'" id="qty_'+new_fl+'" type="text" onKeyup="change_sum('+new_fl+')" class="form-control rupiah" placeholder="Qty" value=0  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<label id="iteml_'+new_fl+'"></label>';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="qtys_'+new_fl+'" id="qtys_'+new_fl+'" type="text" onKeyup="change_sum('+new_fl+')" class="form-control rupiah" placeholder="Qty" value=0  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="totalqty_'+new_fl+'" id="totalqty_'+new_fl+'" type="number"  class="form-control" placeholder="Diskon" value=0 readOnly >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="harga_'+new_fl+'" id="harga_'+new_fl+'" type="text" onKeyup="change_sum('+new_fl+')" class="form-control rupiah" placeholder="Harga" value=0 >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="total_'+new_fl+'" id="total_'+new_fl+'" type="text" class="form-control rupiah" placeholder="Total" value=0 readOnly  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<button type="button" class="btn btn-danger waves-effect waves-light " onClick="kurang_row('+new_fl+')">-</button>';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'									</div>';
		htl +=	'									</div>';
		htl +=	'								</div>';
	
		$('#table_items').append(htl);
			
			$('#kode_'+new_fl+'').html(response.list);
			
			$('#kode_'+new_fl).select2();
			//$("kode_'+new_fl).css("background-color", "yellow");
			
			$('.rupiah').priceFormat({
          		prefix: '',
          		centsSeparator: ',',
          		centsLimit: 0,
          		thousandsSeparator: '.'
        	});	
			
        }
      });
	
}

function cancelchange(){
	
	var prin = $('#ints2').val();
	$('#ints').val(prin);
	$('#name').val(prin);
	
}

// function okchange(){

	   // var datapost = {
		// "id_prin": $('#name').val()
      // };

      // $.ajax({
        // type: 'POST',
        // url: "<?php echo base_url() . 'invoice_selling/get_principal'; ?>",
        // data: JSON.stringify(datapost),
        // cache: false,
        // contentType: false,
        // processData: false,
        // success: function(response) {
			// //var obj = JSON.parse(response);
			// $('#alamat').val(response['principal'][0].eksternal_address);
			// $('#att').val(response['principal'][0].pic);
			// $('#telp').val(response['principal'][0].phone_1);
			// $('#fax').val(response['principal'][0].fax);
			// //console.log(response.phone_1);
			
		// $('#table_items').html('')	
		
		// var htl = '';
		// htl +=	'<div id="row_0" >';
		// htl +=	'									<div class="row" style="border-top-style:solid;">';
		// htl +=	'									<div style="margin-top:10px">';
		// htl +=	'										<div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">';
// htl +=	'														<select name="kode_0" id="kode_0" class="form-control" placeholder="Nama Principal">';
// htl +=	'															<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
// htl +=	'														</select>';
		// htl +=	'										</div>';
		// htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		// htl +=	'											<div class="form-group">';
		// htl +=	'												<input name="qty_0" id="qty_0" type="text" class="form-control rupiah" onKeyup="change_sum(0)"  placeholder="Qty" value=0 >';
		// htl +=	'											</div>';
		// htl +=	'										</div>';
		// htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		// htl +=	'											<div class="form-group">';
		// htl +=	'												<input name="harga_0" id="harga_0" type="text" class="form-control rupiah" onKeyup="change_sum(0)" placeholder="Harga" value=0 >';
		// htl +=	'											</div>';
		// htl +=	'										</div>';
		// htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		// htl +=	'											<div class="form-group">';
		// htl +=	'												<input name="diskon_0" id="diskon_0" type="number" class="form-control" onKeyup="change_sum(0)" placeholder="Diskon" value=0 >';
		// htl +=	'											</div>';
		// htl +=	'										</div>';
		// htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		// htl +=	'											<div class="form-group">';
		// htl +=	'												<input name="total_0" id="total_0" type="text" class="form-control rupiah" placeholder="Total" readOnly value=0 >';
		// htl +=	'											</div>';
		// htl +=	'										</div>';
		// htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		// htl +=	'											<div class="form-group">';
		// htl +=	'												<button type="button" class="btn btn-danger waves-effect waves-light " onClick="kurang_row()">-</button>';
		// htl +=	'											</div>';
		// htl +=	'										</div>';
		// htl +=	'									</div>';
		// htl +=	'									</div>';
		// htl +=	'								</div>';
	
		// $('#table_items').append(htl);
			
			// $('#kode_0').html(response.list);
			
			// $('#kode_0').select2();
			
			 // $('.rupiah').priceFormat({
          		// prefix: '',
          		// centsSeparator: '',
          		// centsLimit: 0,
          		// thousandsSeparator: '.'
        	// });	
			
        // }
      // });
	  
// }

function lainnya(int_fal){
	
	 var datapost = {
		"id_prin": $('#name').val()
      };
	
	  $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'invoice_selling/get_all_item'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {

			$('#kode_'+int_fal).html(response.list);
			
			$('#kode_'+int_fal).select2();
        }
      });
	
}


function select_prin(){
	
	var prin = $('#ints').val();
	var prin2 = $('#name').val();
	
	$('#ints2').val(prin);
//	alert(prin2);
	$('#ints').val(prin2);

		
	  var datapost = {
		"id_penjualan": $('#name').val()
      };

      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'invoice_selling/get_penjualan_detail'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
			//var obj = JSON.parse(response);
			$('#tgl').val(response['penjualan'][0].date_penjualan);
			$('#customer').val(response['penjualan'][0].cust_name);
			$('#alamat').val(response['penjualan'][0].cust_address);
			$('#att').val(response['penjualan'][0].contact_person);
			$('#telp').val(response['penjualan'][0].phone);
			$('#email').val(response['penjualan'][0].email);
			$('#term').val(response['penjualan'][0].term_of_payment);
			$('#term_o').val(response['penjualan'][0].term_of_payment);
			$('#ket').val(response['penjualan'][0].keterangan);
			
			var credit_limits = new Intl.NumberFormat('de-DE', {}).format(response['penjualan'][0].credit_limit);
			
			$('#limit').val(credit_limits);
			
			var sisa_credits = new Intl.NumberFormat('de-DE', {}).format(response['penjualan'][0].sisa_credit);
			
			$('#sisa').val(sisa_credits);
			//console.log(response.phone_1);
			
			
			$('#tgl_inv').val(response['penjualan'][0].date_penjualan);
			$('#tgl_due').val(response['due_date']);
			$('#no_inv').val(response['penjualan'][0].no_penjualan);
			
			$('#table_items').html(response.list)	
		
			$('#vatppn').html(response.ppn);
			$('#subttl2').html(response.dpp);
			$('#grand_total').html(response.total_amount);
			$('#int_flo').val(response.int);
			$('#grand_total').val(response.grand_total);
			$('#dpp').val(response.dpp);
			$('#ppn_in').val(response.ppn);
			// $('#table_items').append(htl);
			// $('#kode_0').html(response.list);
			
			// $('#kode_0').select2();
			
			//$($("#kode_0").select2("container")).addClass("form-control");
			//$("#kode_0").select2({ height: '300px' });	
			
			 $('.rupiah').priceFormat({
          		prefix: '',
          		centsSeparator: ',',
          		centsLimit: 0,
          		thousandsSeparator: '.'
        	});	
        }
      });
		
	
	//$('#ints').val('1');
	
}

function kurang_row(new_fl){
	
	$('#row_'+new_fl).remove();
	
		var int_val = parseInt($('#int_flo').val());
	
	var total_all = 0;
	
	for(var yo = 0;yo <= int_val;yo++ ){
				
		if($('#kode_'+yo).val() !== undefined){
			
			var qty = 	$('#qty_'+yo).val();
			var kode = 	$('#kode_'+yo).val();
			var qtys = 	$('#qtys_'+yo).val();
			var harga = $('#harga_'+yo).val();
			
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace(',','.');
			
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');

			//var val = $('#kode_'+rs+'').val();
			
			var sss = kode.split('|');
			
			//alert(qtys);
			if(qtys == ""){
				var qtys = 0;
			}else{
				var qtys = qtys;
			}
			
			if(qty == ""){
				var qty = 0;
			}else{
				var qty = qty;
			}
			
			var total_qty = ( parseInt(qty)*parseInt(sss[1]) ) + parseInt(qtys);
			
			var total_harga = total_qty * parseFloat(harga);
			//var total_harga = new Intl.NumberFormat('de-DE', {}).format(total_harga);
			
			total_all = total_all + total_harga;
			
		}
				
	}
	
	//alert(total_all);
	
	var gt = new Intl.NumberFormat('de-DE', {}).format(total_all.toFixed(2));
	$('#grand_total').html(gt);
	
	var ppn = total_all/11;
	var ppns = new Intl.NumberFormat('de-DE', {}).format(ppn.toFixed(2));
	$('#vatppn').html(ppns);
	
	var dpp = total_all - ppn;
	var dpps = new Intl.NumberFormat('de-DE', {}).format(dpp.toFixed(2));
	$('#subttl2').html(dpps);
	
	
}

function clearform(){
	
	$('#add_po').trigger("reset");
	$('#table_items').html('')	
		
						var htl = '';
		htl +=	'<div id="row_0" >';
		htl +=	'									<div class="row" style="border-top-style:solid;">';
		htl +=	'									<div style="margin-top:10px">';
		htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
		htl +=	'												<select name="kode_0" onKeyup="change_sum(0)" id="kode_0" class="form-control" placeholder="Nama Principal">';
		htl +=	'													<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		htl +=	'												</select>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="qty_0" id="qty_0" type="text" onKeyup="change_sum(0)" class="form-control rupiah" placeholder="Qty" value=0  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<label id="iteml_0"></label>';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="qtys_0" id="qtys_0" type="text" onKeyup="change_sum(0)" class="form-control rupiah" placeholder="Qty" value=0  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="totalqty_0" id="totalqty_0" type="number"  class="form-control" placeholder="Diskon" value=0 readOnly >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="harga_0" id="harga_0" type="text" onKeyup="change_sum(0)" class="form-control rupiah" placeholder="Harga" value=0 >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="total_0" id="total_0" type="text" class="form-control rupiah" placeholder="Total" value=0 readOnly  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<button type="button" class="btn btn-danger waves-effect waves-light " onClick="kurang_row()">-</button>';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'									</div>';
		htl +=	'									</div>';
		htl +=	'								</div>';
		
			$('#table_items').append(htl);
			
			$('#kode_0').select2();
			
			 $('.rupiah').priceFormat({
          		prefix: '',
          		centsSeparator: ',',
          		centsLimit: 0,
          		thousandsSeparator: '.'
        	});	
			
			
			
	
}

function change_ppn(){
	
	var sub_total = 0;
	var sub_total_bd = 0;
	var sub_disk = 0;
	var int_val = parseInt($('#int_flo').val());
		
	for(var yo = 0;yo <= int_val;yo++ ){
				
		if($('#kode_'+yo).val() !== undefined){
			
			var tots = $('#total_'+yo).val();
			var tots = tots.replace('.','');
			var tots = tots.replace('.','');
			var tots = tots.replace('.','');
			var tots = tots.replace('.','');
			
			var prcs = $('#harga_'+yo).val();
			var prcs = prcs.replace('.','');
			var prcs = prcs.replace('.','');
			var prcs = prcs.replace('.','');
			var prcs = prcs.replace('.','');
			
			var qtys = $('#qty_'+yo).val();
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			
			
			
				sub_total = sub_total +	parseFloat(tots);
				sub_total_bd = sub_total_bd + (parseFloat(prcs)*parseFloat(qtys) );
				sub_disk = sub_disk + ( (parseFloat(prcs)*parseFloat(qtys))*(parseFloat($('#diskon_'+yo).val())/100) );
		}
				
	}
	
	if($('#ppn').val() == ''){
		var ppns = 0;
	}else{
		var ppns = $('#ppn').val();
	}
	
	var total_ppn = (sub_total * (parseFloat(ppns)/100 ));
	var grand_total = sub_total + (sub_total * (parseFloat(ppns)/100 ));
	
	var gt = new Intl.NumberFormat('de-DE', { }).format(grand_total);
	var sub_totals = new Intl.NumberFormat('de-DE', { }).format(sub_total);
	var total_ppns = new Intl.NumberFormat('de-DE', { }).format(total_ppn);
	var total_bds = new Intl.NumberFormat('de-DE', { }).format(sub_total_bd);
	var total_sub_disk = new Intl.NumberFormat('de-DE', { }).format(sub_disk);
	
	$('#vatppn').html(total_ppns);
	$('#grand_total').html(gt);
	
}

function change_sum(fl){
	
	var qty = 	$('#qty_'+fl).val();
	var kode = 	$('#kode_'+fl).val();
	var qtys = 	$('#qtys_'+fl).val();
	var harga = $('#harga_'+fl).val();
	
	var harga = harga.replace('.','');
	var harga = harga.replace('.','');
	var harga = harga.replace('.','');
	var harga = harga.replace('.','');
	var harga = harga.replace('.','');
	var harga = harga.replace(',','.');
	
	var qty = qty.replace('.','');
	var qty = qty.replace('.','');
	var qty = qty.replace('.','');
	var qty = qty.replace('.','');
	var qty = qty.replace('.','');
	
	var qtys = qtys.replace('.','');
	var qtys = qtys.replace('.','');
	var qtys = qtys.replace('.','');
	var qtys = qtys.replace('.','');
	var qtys = qtys.replace('.','');

	//var val = $('#kode_'+rs+'').val();
	
	var sss = kode.split('|');
	
	//alert(qtys);
	if(qtys == ""){
		var qtys = 0;
	}else{
		var qtys = qtys;
	}
	
	if(qty == ""){
		var qty = 0;
	}else{
		var qty = qty;
	}
	
	var total_qty = ( parseInt(qty)*parseInt(sss[1]) ) + parseInt(qtys);
	
	var total_harga = total_qty * parseFloat(harga);
	var total_harga = new Intl.NumberFormat('de-DE', {}).format(total_harga);
	
	$('#qty_retur_'+fl+'').val(total_qty);
	$('#total_'+fl+'').val(total_harga);
	
	// if(total_qty > parseInt(sss[3]) ){
		
		
		
	// }
	
	// var diskon = diskon.replace('.','');
	// var diskon = diskon.replace('.','');
	// var diskon = diskon.replace('.','');
	// var diskon = diskon.replace('.','');

	// var qty = qty.replace('.','');
	// var qty = qty.replace('.','');
	// var qty = qty.replace('.','');
	// var qty = qty.replace('.','');
	// //alert(harga);
	
	
	//var total = (parseFloat(qty) * parseFloat(harga) )- ((parseFloat(qty) * parseFloat(harga))*(parseFloat(diskon)/100)); 
	// var totals = new Intl.NumberFormat('de-DE', { }).format(total);
	
	
	// if( isNaN(total)){
		// //var total_num = Number((total).toFixed(1)).toLocaleString();
		// //$('#subttl').html('0');
		// $('#total_'+fl).val('0');
	// }else{
		// //var total_num = Number((total).toFixed(1)).toLocaleString();
		// //$('#subttl').html(total);
		// $('#total_'+fl).val(totals);
	// }
	
	// var sub_total = 0;
	// var sub_total_bd = 0;
	// var sub_disk = 0;
	var int_val = parseInt($('#int_flo').val());
	
	var total_all = 0;
	
	for(var yo = 0;yo <= int_val;yo++ ){
				
		if($('#kode_'+yo).val() !== undefined){
			
			var qty = 	$('#qty_'+yo).val();
			var kode = 	$('#kode_'+yo).val();
			var qtys = 	$('#qtys_'+yo).val();
			var harga = $('#harga_'+yo).val();
			
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace(',','.');
			
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');

			//var val = $('#kode_'+rs+'').val();
			
			var sss = kode.split('|');
			
			//alert(qtys);
			if(qtys == ""){
				var qtys = 0;
			}else{
				var qtys = qtys;
			}
			
			if(qty == ""){
				var qty = 0;
			}else{
				var qty = qty;
			}
			
			var total_qty = ( parseInt(qty)*parseInt(sss[1]) ) + parseInt(qtys);
			
			var total_harga = total_qty * parseFloat(harga);
			//var total_harga = new Intl.NumberFormat('de-DE', {}).format(total_harga);
			
			total_all = total_all + total_harga;
			
		}
				
	}
	
	//alert(total_all);
	
	var gt = new Intl.NumberFormat('de-DE', {}).format(total_all.toFixed(2));
	$('#grand_total').html(gt);
	
	var ppn = total_all/11;
	var ppns = new Intl.NumberFormat('de-DE', {}).format(ppn.toFixed(2));
	//$('#vatppn').html(ppns);
	
	var dpp = total_all - ppn;
	var dpps = new Intl.NumberFormat('de-DE', {}).format(dpp.toFixed(2));
	//$('#subttl2').html(dpps);
	
	// var total_ppn = (sub_total * (parseFloat($('#ppn').val())/100 ));
	// var grand_total = sub_total + (sub_total * (parseFloat($('#ppn').val())/100 ));
	
	
	
	// var gt = new Intl.NumberFormat('de-DE', { }).format(grand_total);
	// var sub_totals = new Intl.NumberFormat('de-DE', { }).format(sub_total);
	// var total_ppns = new Intl.NumberFormat('de-DE', { }).format(total_ppn);
	// var total_bds = new Intl.NumberFormat('de-DE', { }).format(sub_total_bd);
	// var total_sub_disk = new Intl.NumberFormat('de-DE', { }).format(sub_disk);
	
	// $('#subttl').html(total_bds);
	// $('#subttl2').html(sub_totals);
	// $('#subttl3').html(total_sub_disk);
	// $('#vatppn').html(total_ppns);
	// $('#grand_total').html(gt);
	
	
	
	
	//alert(qty+' '+harga+' '+diskon);
	
}

function back(){
	
	window.location.href = "<?php echo base_url().'invoice_selling';?>";
	
}

function term_other(){
	
	//alert($('#term').val());
	
	if( $('#term').val() == "other"){
		//$('#term_o').show();
		$("#term_o").prop("readonly",false);

		
	}else{
		// $('#term_o').hide();
		
		$("#term_o").prop("readonly",true);
	}
	
}


function select_prin2(){
	
	var prin = $('#ints').val();
	var prin2 = $('#name').val();
	
	$('#ints2').val(prin);
//alert(prin2);
	$('#ints').val(prin2);

		
	  var datapost = {
		"id_penjualan": <?php echo $id_invoice; ?>
      };

      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'invoice_selling/get_penjualan_detail_update'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
			//var obj = JSON.parse(response);
			$('#tgl').val(response['penjualan'][0].date_penjualan);
			$('#customer').val(response['penjualan'][0].cust_name);
			$('#alamat').val(response['penjualan'][0].cust_address);
			$('#att').val(response['penjualan'][0].contact_person);
			$('#telp').val(response['penjualan'][0].phone);
			$('#email').val(response['penjualan'][0].email);
			$('#term').val(response['penjualan'][0].term_of_payment);
			$('#term_o').val(response['penjualan'][0].term_of_payment);
			$('#ket').val(response['penjualan'][0].keterangan);
			
			$('#no_inv').val(response['penjualan'][0].no_invoice);
			$('#tgl_inv').val(response['penjualan'][0].tanggal_invoice);
			$('#tgl_due').val(response['penjualan'][0].due_date);
			$('#ket_inv').val(response['penjualan'][0].note);
			
			var credit_limits = new Intl.NumberFormat('de-DE', {}).format(response['penjualan'][0].credit_limit);
			
			$('#limit').val(credit_limits);
			
			var sisa_credits = new Intl.NumberFormat('de-DE', {}).format(response['penjualan'][0].sisa_credit);
			
			$('#sisa').val(sisa_credits);
			//console.log(response.phone_1);
			
			$('#int_flo').val(response.int);
			$('#grand_total').val(response.grand_total);
			$('#dpp').val(response.dpp);
			$('#ppn_in').val(response.ppn);
			
			// $('#table_items').html(response.list)	
		
			// $('#vatppn').html(response.ppn);
			// $('#subttl2').html(response.dpp);
			// $('#grand_total').html(response.total_amount);
			// $('#int_flo').val(response.int)
			// $('#table_items').append(htl);
			// $('#kode_0').html(response.list);
			
			// $('#kode_0').select2();
			
			//$($("#kode_0").select2("container")).addClass("form-control");
			//$("#kode_0").select2({ height: '300px' });	
			
			 $('.rupiah').priceFormat({
          		prefix: '',
          		centsSeparator: ',',
          		centsLimit: 0,
          		thousandsSeparator: '.'
        	});	
        }
      });
		
	
	//$('#ints').val('1');
	
}

  function listdist(){
	  var user_id = '0001';
	  var token = '093940349';
	  
	  
   $('#datatable_pricipal').DataTable({
	   //"dom": 'rtip',
		"bFilter": false,
		"aaSorting": [],
		"bLengthChange": true,
		'iDisplayLength': 10,
		"sPaginationType": "simple_numbers",
		"Info" : false,
		"processing": true,
        "serverSide": true,
        "destroy": true,
		"ajax": "<?php echo base_url().'invoice_selling/lists'?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token,
		"searching": true,
		"language": {
            "decimal": ",",
            "thousands": "."
        },
		"dom": 'l<"toolbar">frtip',
		"initComplete": function(){
                           $("div.toolbar").prepend('<div class="btn-group pull-left"><button type="button" class="btn btn-custon-rounded-two btn-primary" > Tambah </button></div>');
        }
	});	
  }

	$(document).ready(function(){
		
		select_prin2();
		//select_prin();
		//$('#kode_0').select2();
		$('#name').select2();
		
		 $('.rupiah').priceFormat({
          		prefix: '',
          		centsSeparator: ',',
          		centsLimit: 0,
          		thousandsSeparator: '.'
        	});	
		
		$('#tgl').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		});
		
     // listdist();
	  
	  $('#add_po').on('submit', function(e){
		// validation code here
		//if(!valid) {
			e.preventDefault();
			
			// var formData = new FormData(this);
			var formData = new FormData();
			
			formData.append('id_invoice', <?php echo $id_invoice; ?>);
			formData.append('name', $('#name').val());
			formData.append('ket_inv', $('#ket_inv').val());
			formData.append('int_flo', $('#int_flo').val());
			formData.append('att', $('#att').val());
			formData.append('tgl_due', $('#tgl_due').val());
			formData.append('tgl_inv', $('#tgl_inv').val());
			formData.append('no_inv', $('#no_inv').val());			
			
			var int_val = parseInt($('#int_flo').val());
			
			for(var yo = 0;yo <= int_val;yo++ ){
				
				if($('#kode_'+yo).val() !== undefined){
					
					formData.append('kode_'+yo, $('#kode_'+yo).val());
					formData.append('harga_'+yo, $('#harga_'+yo).val());
					formData.append('total_'+yo, $('#total_'+yo).val());
					formData.append('qty_'+yo, $('#qty_'+yo).val());
					formData.append('totalqty_'+yo, $('#totalqty_'+yo).val());
					formData.append('qty_retur_'+yo, $('#qty_retur_'+yo).val());
					formData.append('qtys_'+yo, $('#qtys_'+yo).val());
					formData.append('id_d_'+yo, $('#id_d_'+yo).val());
					
				}
				
			}
			
		
			
			
			//console.log(formData);

          $.ajax({
              type:'POST',
				url: $(this).attr('action'),
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                success: function(response) {
					
					//console.log(response);
					if(response.success == true){
						$('#ModalAddPo').modal('show');
					}else{
						$('#msg_err').html(response.message);
						$('#ModalChangePo').modal('show');
					}
                }
            });
			
			
			//alert(kode);
	
	
		//}
	  });
  });
</script>