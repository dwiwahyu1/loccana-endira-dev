<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Buku_besar_pembantu_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}


	public function list_report_cash($params = []) {
			$keyword="where_replace";
			$sql = "
			select *,keterangan as banks from (

			SELECT a.*,
			b.coa kr_coa,b.note as k_note,
			if(b.keterangan IS NULL,a.keterangan,b.keterangan) kr_note,
			IF(b.value_real IS NULL,a.value_real,b.value_real) sum_amnt,
			p.`no_invoice` invoice_p, p.name_eksternal users FROM (
			SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
			JOIN t_coa b ON a.id_coa=b.id_coa
			WHERE a.type_cash=1 AND a.id_parent = 0 ".$keyword."
			)a LEFT JOIN (
			SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
			JOIN t_coa b ON a.id_coa=b.id_coa
			WHERE a.type_cash=0 AND a.id_parent <> 0
			)b ON a.id=b.id_parent
			LEFT JOIN (select c.id_hp,f.no_invoice,k.name_eksternal from `t_payment_hp` c 
								LEFT JOIN `d_payment_hp` d ON c.`id_hp` = d.`id_hp`
								LEFT JOIN `t_invoice_pembelian` f ON d.id_invoice = f.`id_invoice`
								LEFT JOIN `t_bpb` i ON f.`id_bpb` = i.`id_bpb`
								LEFT JOIN `t_purchase_order` j ON j.id_po = i.`id_po`
								LEFT JOIN `t_eksternal` k ON j.id_distributor = k.`id` 
								WHERE c.payment_type = 0
								group by d.id_hp) p ON a.id_coa_temp = p.`id_hp`
			WHERE a.date BETWEEN  '" . $params['start_date'] . "' AND '" . $params['end_date'] . "'
			and b.coa IS NOT NULL
			) a union all select *,keterangan as banks from (

			SELECT a.*,
			b.coa kr_coa,b.note as k_note,
			IF(b.keterangan IS NULL,a.keterangan,b.keterangan) kr_note,
			IF(b.value_real IS NULL,a.value_real,b.value_real) sum_amnt,
			p.`no_invoice` invoice_p, p.name_eksternal users FROM (
			SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
			JOIN t_coa b ON a.id_coa=b.id_coa
			WHERE a.type_cash=1 AND a.id_parent <> 0 ".$keyword."
			)a LEFT JOIN (
			SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
			JOIN t_coa b ON a.id_coa=b.id_coa
			WHERE a.type_cash=0 AND a.id_parent = 0
			)b ON a.id_parent=b.id
			LEFT JOIN (SELECT c.id_hp,f.no_invoice,k.name_eksternal FROM `t_payment_hp` c 
								LEFT JOIN `d_payment_hp` d ON c.`id_hp` = d.`id_hp`
								LEFT JOIN `t_invoice_pembelian` f ON d.id_invoice = f.`id_invoice`
								LEFT JOIN `t_bpb` i ON f.`id_bpb` = i.`id_bpb`
								LEFT JOIN `t_purchase_order` j ON j.id_po = i.`id_po`
								LEFT JOIN `t_eksternal` k ON j.id_distributor = k.`id` 
								WHERE c.payment_type = 0
								GROUP BY d.id_hp) p ON a.id_coa_temp = p.`id_hp`
			WHERE a.date BETWEEN  '" . $params['start_date'] . "' AND '" . $params['end_date'] . "'
			#AND b.coa IS NOT NULL
			) b union all SELECT *,keterangan as banks FROM (

			SELECT a.*,
			b.coa kr_coa,b.note as k_note,
			IF(b.keterangan IS NULL,a.keterangan,b.keterangan) kr_note,
			IF(b.value_real IS NULL,a.value_real,b.value_real) sum_amnt,
			p.`no_invoice` invoice_p, p.cust_name users FROM (
			SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
			JOIN t_coa b ON a.id_coa=b.id_coa
			WHERE a.type_cash=0 AND a.id_parent = 0 ".$keyword."
			)a LEFT JOIN (
			SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
			JOIN t_coa b ON a.id_coa=b.id_coa
			WHERE a.type_cash=1 AND a.id_parent <> 0
			)b ON a.id=b.id_parent
			LEFT JOIN (SELECT c.id_hp,e.no_invoice,h.cust_name FROM `t_payment_hp` c 
								LEFT JOIN `d_payment_hp` d ON c.`id_hp` = d.`id_hp`
								LEFT JOIN `t_invoice_penjualan` e ON d.id_invoice = e.`id_invoice`
								LEFT JOIN `t_penjualan` g ON e.`id_penjualan` = g.`id_penjualan`
								LEFT JOIN `t_customer` h ON g.`id_customer` = `id_t_cust`
								where c.payment_type = 1
								GROUP BY d.id_hp) p ON a.id_coa_temp = p.`id_hp`
			WHERE a.date BETWEEN  '" . $params['start_date'] . "' AND '" . $params['end_date'] . "'
			AND b.coa IS NOT NULL
			) c union all SELECT *,keterangan as banks FROM  (

			SELECT a.*,
			b.coa kr_coa,b.note as k_note,
			IF(b.keterangan IS NULL,a.keterangan,b.keterangan) kr_note,
			IF(b.value_real IS NULL,b.value_real,a.value_real) sum_amnt,
			p.`no_invoice` invoice_p, p.cust_name users FROM (
			SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
			JOIN t_coa b ON a.id_coa=b.id_coa
			WHERE a.type_cash=0 AND a.id_parent <> 0 ".$keyword."
			)a LEFT JOIN (
			SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
			JOIN t_coa b ON a.id_coa=b.id_coa
			WHERE a.type_cash=1 AND a.id_parent = 0
			)b ON a.id_parent=b.id
			LEFT JOIN (SELECT c.id_hp,e.no_invoice,h.cust_name FROM `t_payment_hp` c 
								LEFT JOIN `d_payment_hp` d ON c.`id_hp` = d.`id_hp`
								LEFT JOIN `t_invoice_penjualan` e ON d.id_invoice = e.`id_invoice`
								LEFT JOIN `t_penjualan` g ON e.`id_penjualan` = g.`id_penjualan`
								LEFT JOIN `t_customer` h ON g.`id_customer` = `id_t_cust`
								WHERE c.payment_type = 1
								GROUP BY d.id_hp) p ON a.id_coa_temp = p.`id_hp`
			WHERE a.date BETWEEN  '" . $params['start_date'] . "' AND '" . $params['end_date'] . "'
			AND b.coa IS NOT NULL
			) d union ALL SELECT *,keterangan as banks FROM (

			SELECT a.*,
			b.coa kr_coa,b.note as k_note,
			b.keterangan kr_note,
			b.value_real sum_amnt,
			p.`no_invoice` invoice_p, p.name_eksternal users FROM (
			SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
			JOIN t_coa b ON a.id_coa=b.id_coa
			WHERE a.type_cash=1 AND a.id_parent = 0 ".$keyword."
			)a LEFT JOIN (
			SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
			JOIN t_coa b ON a.id_coa=b.id_coa
			WHERE a.type_cash=1 AND a.id_parent <> 0
			)b ON a.id=b.id_parent
			LEFT JOIN (SELECT c.id_hp,f.no_invoice,k.name_eksternal FROM `t_payment_hp` c 
								LEFT JOIN `d_payment_hp` d ON c.`id_hp` = d.`id_hp`
								LEFT JOIN `t_invoice_pembelian` f ON d.id_invoice = f.`id_invoice`
								LEFT JOIN `t_bpb` i ON f.`id_bpb` = i.`id_bpb`
								LEFT JOIN `t_purchase_order` j ON j.id_po = i.`id_po`
								LEFT JOIN `t_eksternal` k ON j.id_distributor = k.`id` 
								WHERE c.payment_type = 0
								GROUP BY d.id_hp) p ON a.id_coa_temp = p.`id_hp`
			WHERE a.date BETWEEN  '" . $params['start_date'] . "' AND '" . $params['end_date'] . "'
			AND b.coa IS not NULL
			) e UNION ALL SELECT *,keterangan as banks FROM (

			SELECT a.*,
			b.coa kr_coa,b.note as k_note,
			b.keterangan kr_note,
			b.value_real sum_amnt,
			p.`no_invoice` invoice_p, p.name_eksternal users FROM (
			SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
			JOIN t_coa b ON a.id_coa=b.id_coa
			WHERE a.type_cash=0 AND a.id_parent = 0 ".$keyword."
			)a LEFT JOIN (
			SELECT a.*,b.coa,b.keterangan FROM t_coa_value a 
			JOIN t_coa b ON a.id_coa=b.id_coa
			WHERE a.type_cash=0 AND a.id_parent <> 0
			)b ON a.id=b.id_parent
			LEFT JOIN (SELECT c.id_hp,f.no_invoice,k.name_eksternal FROM `t_payment_hp` c 
								LEFT JOIN `d_payment_hp` d ON c.`id_hp` = d.`id_hp`
								LEFT JOIN `t_invoice_pembelian` f ON d.id_invoice = f.`id_invoice`
								LEFT JOIN `t_bpb` i ON f.`id_bpb` = i.`id_bpb`
								LEFT JOIN `t_purchase_order` j ON j.id_po = i.`id_po`
								LEFT JOIN `t_eksternal` k ON j.id_distributor = k.`id` 
								WHERE c.payment_type = 0
								GROUP BY d.id_hp) p ON a.id_coa_temp = p.`id_hp`
			WHERE a.date BETWEEN  '" . $params['start_date'] . "' AND '" . $params['end_date'] . "'
			AND b.coa IS NOT NULL
			) e 
		";
		if ($params['coa_id'] == 0) {

			$coa_cash = $this->get_cash_all();

			//print_r($coa_cash);die;

			$cnt_all = count($coa_cash['data']);
			$cnt = 0;

			$sql_all = "";

			foreach ($coa_cash['data'] as $coa_cashs) {
				$where = " AND b.id_coa=" . $coa_cashs['id_coa'] . " ";

				$sql_new =str_replace($keyword,$where,$sql);

				$cnt++;

				if ($cnt < $cnt_all) {
					$sql_new = $sql_new . " union all  ";
				}

				$sql_all = $sql_all . " " . $sql_new;

			}
			// if($coa_cashs['id_coa']==9){
			// 	echo"<pre>";print_r($sql);die;
			// }
			$sql = $sql_all . " order by `date` ";

			//$where = "";
		} else {
			$where = " AND b.id_coa=" . $params['coa_id'] . " ";
			$sql_new =str_replace($keyword,$where,$sql);
			$sql = $sql_new . " order by `date` ";
		}
		if($params['start_date']=='2021-10-01'){
			// echo"<pre>";print_r($sql);die;

		}

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return [
			'data' => $result
		];

		//return $result;

	}

	public function list_report_cash_saldo($params = []) {

		if ($params['coa_id'] == 0) {

			$sql 	= '
			SELECT (masuk-keluar) AS saldo_awal FROM (
			SELECT IF(SUM(value_real) IS NULL,0,SUM(value_real)) AS masuk FROM t_coa_value a
			WHERE type_cash = 0
			AND a.date < "' . $params['start_date'] . '"
			) a,(
			SELECT IF(SUM(value_real) IS NULL,0,SUM(value_real)) AS keluar FROM t_coa_value a
			WHERE type_cash = 1
			AND a.date < "' . $params['start_date'] . '"

			) b
			';
		} else {

			$sql 	= '
			SELECT (masuk-keluar) AS saldo_awal FROM (
			SELECT IF(SUM(value_real) IS NULL,0,SUM(value_real)) AS masuk FROM t_coa_value a
			WHERE id_coa = ' . $params['coa_id'] . ' AND type_cash = 0
			AND a.date < "' . $params['start_date'] . '"
			) a,(
			SELECT IF(SUM(value_real) IS NULL,0,SUM(value_real)) AS keluar FROM t_coa_value a
			WHERE id_coa = ' . $params['coa_id'] . ' AND type_cash = 1
			AND a.date < "' . $params['start_date'] . '"

			) b
			';
		}



		// echo"<pre>";print_r($sql);die;

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return [
			'data' => $result
		];

		//return $result;

	}

	public function get_cash_all() {


		$sql 	= '
			SELECT * FROM t_coa WHERE id_parent = 3
			order by id_coa
			';

		//print_r($sql);die;

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return [
			'data' => $result
		];

		//return $result;

	}

	/**
	 * get list of coa accounts
	 * 
	 * @return array
	 */
	public function get_coa_list() {
		$queryBuilder = $this->db
			->select('*')
			->from('t_coa');

		$result = $queryBuilder->get()->result_array();

		return $result;
	}
}
