<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Buku_besar_pembantu_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}


	public function list_report_cash($params = []) {
			$keyword="where_replace";
			$sql = "
			SELECT a.id,a.date,b.keterangan,a.id_coa,a.note,
			IF(a.type_cash=0,(a.value_real),0) debit,
			IF(a.type_cash=1,(a.value_real),0) kredit,
			SUM(IF(a.type_cash=0,(a.value_real),0)) OVER (ORDER BY a.date DESC, a.id DESC) AS cumulative_debit,
			SUM(IF(a.type_cash=1,(a.value_real),0)) OVER (ORDER BY a.date DESC, a.id DESC) AS cumulative_kredit,
			SUM(IF(a.type_cash=0,(a.value_real),0)) OVER (ORDER BY a.date DESC, a.id DESC)
			-SUM(IF(a.type_cash=1,(a.value_real),0)) OVER (ORDER BY a.date DESC, a.id DESC) AS cumulative
			,d.keterangan AS kr_note,d.coa as kr_coa,a.type_cash,a.note as uraian
			FROM t_coa_value a
			JOIN t_coa b ON a.id_coa=b.id_coa
			LEFT JOIN `t_coa_value` c ON c.id=a.id_parent
			LEFT JOIN `t_coa` d ON c.id_coa = d.id_coa
			WHERE a.`date`BETWEEN '" . $params['start_date'] . "' AND '" . $params['end_date'] . "'
			AND a.id_coa = " . $params['coa_id'] . "
			AND b.id_parent NOT IN (3)
			#GROUP BY a.id_coa
			ORDER BY a.date DESC, a.id DESC
			
		";
		if ($params['coa_id'] == 0) {

			$coa_cash = $this->get_cash_all();

			//print_r($coa_cash);die;

			$cnt_all = count($coa_cash['data']);
			$cnt = 0;

			$sql_all = "";

			foreach ($coa_cash['data'] as $coa_cashs) {
				$where = " AND b.id_coa=" . $coa_cashs['id_coa'] . " ";

				$sql_new =str_replace($keyword,$where,$sql);

				$cnt++;

				if ($cnt < $cnt_all) {
					$sql_new = $sql_new . " union all  ";
				}

				$sql_all = $sql_all . " " . $sql_new;

			}
			// if($coa_cashs['id_coa']==9){
			// 	echo"<pre>";print_r($sql);die;
			// }
			$sql = $sql_all . " order by `date` ";

			//$where = "";
		} else {
			$where = "";
			$sql_new =str_replace($keyword,$where,$sql);
			// $sql = $sql_new . " order by `date` ";
		}
		if($params['start_date']=='2021-10-01'){
			// echo"<pre>";print_r($sql);die;
		}
		// echo"<pre>";var_dump($sql);echo"</pre>";die;
		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return [
			'data' => $result
		];

		//return $result;

	}

	public function list_report_cash_saldo($params = []) {

		if ($params['coa_id'] == 0) {

			$sql 	= '
			SELECT (masuk-keluar) AS saldo_awal FROM (
			SELECT IF(SUM(value_real) IS NULL,0,SUM(value_real)) AS masuk FROM t_coa_value a
			WHERE type_cash = 0
			AND a.date < "' . $params['start_date'] . '"
			) a,(
			SELECT IF(SUM(value_real) IS NULL,0,SUM(value_real)) AS keluar FROM t_coa_value a
			WHERE type_cash = 1
			AND a.date < "' . $params['start_date'] . '"

			) b
			';
		} else {

			$sql 	= '
			SELECT (masuk-keluar) AS saldo_awal FROM (
			SELECT IF(SUM(value_real) IS NULL,0,SUM(value_real)) AS masuk FROM t_coa_value a
			WHERE id_coa = ' . $params['coa_id'] . ' AND type_cash = 0
			AND a.date < "' . $params['start_date'] . '"
			) a,(
			SELECT IF(SUM(value_real) IS NULL,0,SUM(value_real)) AS keluar FROM t_coa_value a
			WHERE id_coa = ' . $params['coa_id'] . ' AND type_cash = 1
			AND a.date < "' . $params['start_date'] . '"

			) b
			';
		}



		// echo"<pre>";print_r($sql);die;

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return [
			'data' => $result
		];

		//return $result;

	}

	public function get_cash_all() {


		$sql 	= '
			SELECT * FROM t_coa WHERE id_parent = 3
			order by id_coa
			';

		//print_r($sql);die;

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return [
			'data' => $result
		];

		//return $result;

	}

	/**
	 * get list of coa accounts
	 * 
	 * @return array
	 */
	public function get_coa_list() {
		$queryBuilder = $this->db
			->select('*')
			->where(array('sts_show'=>1))
			->from('t_coa');

		$result = $queryBuilder->get()->result_array();

		return $result;
	}
}
