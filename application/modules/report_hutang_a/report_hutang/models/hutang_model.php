<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Hutang_model extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->library('format');
	}
	public function reports($data) {

		$kuantiti_a = 0;
		$harga_satuan_a = 0;
		$nilai_a = 0;
		$pembelian_a = 0;
		$kuantiti_sol_a = 0;
		$kuantiti_titip_a = 0;
		$kuantiti_bonus_a = 0;
		$harga_sat_pemb_a = 0;
		$nilai_pemb_a = 0;
		$harga_pokok_a = 0;
		$penjualan_a = 0;
		$pengeluaran_a = 0;
		$saldo_akhir_a = 0;
		$nilai_akhir_a = 0;
		$invoice_saldo_a = 0;
		$d0_a = 0;
		$d30_a = 0;
		$d60_a = 0;
		$d90_a = 0;
		$d120_a = 0;
		$total_t_a = 0;
		$array_print = [];

    $retur_harga = 0;

		if ($data['principal'] == 0) {

			$dist_a = $this->get_distribution($data);
			$html = '';

			foreach ($dist_a as $dist_as) {

				$data['principal']= $dist_as['dist_id'];

				$result = $this->get_report_persediaan($data);
				// echo "<pre>";
				// print_r($result);die;


				$kuantiti = 0;
				$harga_satuan = 0;
				$nilai = 0;
				$pembelian = 0;
				$kuantiti_sol = 0;
				$kuantiti_titip = 0;
				$kuantiti_bonus = 0;
				$harga_sat_pemb = 0;
				$nilai_pemb = 0;
				$harga_pokok = 0;
				$penjualan = 0;
				$pengeluaran = 0;
				$saldo_akhir = 0;
				$nilai_akhir = 0;
				$invoice_saldo = 0;
				$retur_harga = 0;
				$prin = '';
				$kode_prin = '';

				$d0 = 0;
				$d30 = 0;
				$d60 = 0;
				$d90 = 0;
				$d120 = 0;
				$total_t = 0;

				$curr_invoice = '';
				$i=0;
				foreach ($result as $datas) {

					if ($curr_invoice <> $datas['no_invoice']) {

						$bayar_r_te =  $this->format->format_excel($datas['bayar']);
						$bayar_r =  $datas['bayar'];
						$bb =  $datas['bayar'];
						$bb_saldo =  $datas['bayar_saldo'];
						$curr_invoice = $datas['no_invoice'];
						$sisa_r_te =  number_format(($datas['prices'] - ($datas['prices'] * ($datas['diskons'] / 100)) + (($datas['prices'] - ($datas['prices'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100))) - $datas['bayar'], 2, ',', '.');
						$sisa_r =  ($datas['prices'] - ($datas['prices'] * ($datas['diskons'] / 100)) + (($datas['prices'] - ($datas['prices'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100))) - $datas['bayar'];
						$curr_invoice = $datas['no_invoice'];
					} else {
						$bayar_r_te = null;
						$sisa_r_te = null;
						$bayar_r = 0;
						$bb_saldo = 0;
						$sisa_r = 0;
						$bb = 0;
					}

					$ttl = $datas['min_30'] - ($datas['min_30'] * ($datas['diskons'] / 100)) + (($datas['min_30'] - ($datas['min_30'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)) + $datas['d30'] - ($datas['d30'] * ($datas['diskons'] / 100)) + (($datas['d30'] - ($datas['d30'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)) + $datas['d60'] - ($datas['d60'] * ($datas['diskons'] / 100)) + (($datas['d60'] - ($datas['d60'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)) + $datas['d90'] - ($datas['d90'] * ($datas['diskons'] / 100)) + (($datas['d90'] - ($datas['d90'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)) + $datas['d120'] - ($datas['d120'] * ($datas['diskons'] / 100)) + (($datas['d120'] - ($datas['d120'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100));

					$prcs = $datas['prices'] - ($datas['prices'] * ($datas['diskons'] / 100)) + (($datas['prices'] - ($datas['prices'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100));

					// echo $datas['saldo'];die;

					if ($datas['saldo'] == 0) {
						$bb = 0;
					} else {
						$bb = $bb - $prcs;
					}

					if ($bb > 0) {
						$ss_sisa = 0;
					} else {
						$ss_sisa = abs($bb);
					}

					if ($datas['umur'] < 31) {
						$m1 = $ss_sisa;
						$m2 = 0;
						$m3 = 0;
						$m4 = 0;
						$m5 = 0;
					} elseif ($datas['umur'] > 30 && $datas['umur'] < 61) {
						$m1 = 0;
						$m2 = $ss_sisa;
						$m3 = 0;
						$m4 = 0;
						$m5 = 0;
					} elseif ($datas['umur'] > 60 && $datas['umur'] < 91) {
						$m1 = 0;
						$m2 = 0;
						$m3 = $ss_sisa;
						$m4 = 0;
						$m5 = 0;
					} elseif ($datas['umur'] > 90 && $datas['umur'] < 121) {
						$m1 = 0;
						$m2 = 0;
						$m3 = 0;
						$m4 = $ss_sisa;
						$m5 = 0;
					} else {
						$m1 = 0;
						$m2 = 0;
						$m3 = 0;
						$m4 = 0;
						$m5 = $ss_sisa;
					}

					$ttl = $m1 + $m2 + $m3 + $m4 + $m5;

					$date_filer = date('Y-m-d', strtotime($data['tgl_awal']));
					$date_data = date('Y-m-d', strtotime($datas['tanggal']));

					$html .= '<tr>
									<th >' . $datas['name_eksternal'] . '</th>
								<th>' . $datas['date_po'] . '</th>
								<th>' . $datas['no_pos'] . '</th>
								<th>' . $datas['no_invoice'] . '</th>
								<th>' . $datas['jatuh_tempo'] . '</th>
								<th>' . $datas['umur'] . '</th>
								<th>' . number_format($datas['saldo']- ($datas['saldo'] * ($datas['diskons'] / 100)) +(($datas['saldo'] - ($datas['saldo'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)) ,2,',','.') . '</th>
								<th>' . number_format($datas['bulan_berjalan']- ($datas['bulan_berjalan'] * ($datas['diskons'] / 100))+ (($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100))+$bayar_r,2,',','.') . '</th>
								<th>' . $datas['tgl_bayar'] . '</th>
								<th>' . $bayar_r_te . '</th>
								<th>' . number_format($ss_sisa, 2, ',', '.') . '</th>
								<th>' . number_format($datas['invoice_saldo'], 2, ',', '.') . '</th>
								<th>' . number_format($m1, 2, ',', '.') . '</th>
								<th>' . number_format($m2, 2, ',', '.') . '</th>
								<th>' . number_format($m3, 2, ',', '.') . '</th>
								<th>' . number_format($m4, 2, ',', '.') . '</th>
								<th>' . number_format($m5, 2, ',', '.') . '</th>
								<th>' . number_format($ttl, 2, ',', '.') . '</th>
							</tr>';

					$prin = $datas['name_eksternal'];


					$curr_invoice = $datas['no_invoice'];

					$penjualan = $penjualan + ($datas['saldo'] - ($datas['saldo'] * ($datas['diskons'] / 100)) + (($datas['saldo'] - ($datas['saldo'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100))) - $bb_saldo;
					$pengeluaran = $pengeluaran + ($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskons'] / 100)) + (($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)))+$bayar_r;
					$saldo_akhir = $saldo_akhir + $bayar_r;
					$nilai_akhir = $nilai_akhir + $datas['bayar'];
					$invoice_saldo = $invoice_saldo + $datas['invoice_saldo'];


					$d0 = $d0 + $m1;
					$d30 = $d30 + $m2;
					$d60 = $d60 + $m3;
					$d90 = $d90 + $m4;
					$d120 = $d120 + $m5;
					$total_t = $total_t + $ttl;

          
					$penjualan_a = $penjualan_a +  ($datas['saldo'] - ($datas['saldo'] * ($datas['diskons'] / 100)) + (($datas['saldo'] - ($datas['saldo'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100))) - $bb_saldo;
					$pengeluaran_a = $pengeluaran_a +  ($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskons'] / 100)) + (($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)));
					$saldo_akhir_a = $saldo_akhir_a +  $bayar_r;
					$nilai_akhir_a = $nilai_akhir_a +  $datas['bayar'];
					$invoice_saldo_a = $invoice_saldo_a +  $datas['invoice_saldo'];
					$retur_harga = $retur_harga + $datas['retur_harga'];


					$d0_a = $d0_a + $m1;
					$d30_a = $d30_a + $m2;
					$d60_a = $d60_a + $m3;
					$d90_a = $d90_a + $m4;
					$d120_a = $d120_a + $m5;
					$total_t_a = $total_t_a + $ttl;
					$i++;
				}
				if($i!=0)
				  $html .= '<tr style="background:yellow">
							<th>' .$dist_as['dist_id']. $prin . '</th>
							<th></th>
							<th ></th>
							<th ></th>	
							<th></th>
							<th></th>
							<th>' . number_format($penjualan, 2, ',', '.') . '</th>
							<th>' . number_format($pengeluaran, 2, ',', '.') . '</th>
							<th></th>
							<th>' . number_format($saldo_akhir, 2, ',', '.') . '</th>
							<th>' . number_format($penjualan + $pengeluaran - $saldo_akhir, 2, ',', '.') . '</th>
							<th>' . number_format($invoice_saldo, 2, ',', '.') . '</th>
							<th>' . number_format($d0, 2, ',', '.') . '</th>
							<th>' . number_format($d30, 2, ',', '.') . '</th>
							<th>' . number_format($d60, 2, ',', '.') . '</th>
							<th>' . number_format($d90, 2, ',', '.') . '</th>
							<th>' . number_format($d120, 2, ',', '.') . '</th>
							<th>' . number_format($penjualan + $pengeluaran - $saldo_akhir, 2, ',', '.') . '</th>
						</tr>';
			}
		} else {

			$result = $this->get_report_persediaan($data);

			$html = '';

			$kuantiti = 0;
			$harga_satuan = 0;
			$nilai = 0;
			$pembelian = 0;
			$kuantiti_sol = 0;
			$kuantiti_titip = 0;
			$kuantiti_bonus = 0;
			$harga_sat_pemb = 0;
			$nilai_pemb = 0;
			$harga_pokok = 0;
			$penjualan = 0;
			$pengeluaran = 0;
			$saldo_akhir = 0;
			$nilai_akhir = 0;
			$invoice_saldo = 0;
			$prin = '';
			$kode_prin = '';

			$d0 = 0;
			$d30 = 0;
			$d60 = 0;
			$d90 = 0;
			$d120 = 0;
			$total_t = 0;

			$curr_invoice = '';
			foreach ($result as $datas) {


				if ($curr_invoice <> $datas['no_invoice']) {

					$bayar_r_te =  $this->format->format_excel($datas['bayar'], 2, ',', '.');
					$bayar_r =  $datas['bayar'];
					$bb =  $datas['bayar'];
					$bb_saldo =  $datas['bayar_saldo'];
					$curr_invoice = $datas['no_invoice'];
					$sisa_r_te =  number_format(($datas['prices'] - ($datas['prices'] * ($datas['diskons'] / 100)) + (($datas['prices'] - ($datas['prices'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100))) - $datas['bayar'], 2, ',', '.');
					$sisa_r =  ($datas['prices'] - ($datas['prices'] * ($datas['diskons'] / 100)) + (($datas['prices'] - ($datas['prices'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100))) - $datas['bayar'];
					$curr_invoice = $datas['no_invoice'];
				} else {
					$bayar_r_te = null;
					$sisa_r_te = null;
					$bayar_r = 0;
					$bb_saldo = 0;
					$sisa_r = 0;
					$bb = 0;
				}

				$ttl = $datas['min_30'] - ($datas['min_30'] * ($datas['diskons'] / 100)) + (($datas['min_30'] - ($datas['min_30'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)) + $datas['d30'] - ($datas['d30'] * ($datas['diskons'] / 100)) + (($datas['d30'] - ($datas['d30'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)) + $datas['d60'] - ($datas['d60'] * ($datas['diskons'] / 100)) + (($datas['d60'] - ($datas['d60'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)) + $datas['d90'] - ($datas['d90'] * ($datas['diskons'] / 100)) + (($datas['d90'] - ($datas['d90'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)) + $datas['d120'] - ($datas['d120'] * ($datas['diskons'] / 100)) + (($datas['d120'] - ($datas['d120'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100));

				$prcs = $datas['prices'] - ($datas['prices'] * ($datas['diskons'] / 100)) + (($datas['prices'] - ($datas['prices'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100));


				$bb = $bb - $prcs;

				if ($bb > 0) {
					$ss_sisa = 0;
				} else {
					$ss_sisa = abs($bb);
				}

				if ($datas['umur'] < 31) {
					$m1 = $ss_sisa;
					$m2 = 0;
					$m3 = 0;
					$m4 = 0;
					$m5 = 0;
				} elseif ($datas['umur'] > 30 && $datas['umur'] < 61) {
					$m1 = 0;
					$m2 = $ss_sisa;
					$m3 = 0;
					$m4 = 0;
					$m5 = 0;
				} elseif ($datas['umur'] > 60 && $datas['umur'] < 91) {
					$m1 = 0;
					$m2 = 0;
					$m3 = $ss_sisa;
					$m4 = 0;
					$m5 = 0;
				} elseif ($datas['umur'] > 90 && $datas['umur'] < 121) {
					$m1 = 0;
					$m2 = 0;
					$m3 = 0;
					$m4 = $ss_sisa;
					$m5 = 0;
				} else {
					$m1 = 0;
					$m2 = 0;
					$m3 = 0;
					$m4 = 0;
					$m5 = $ss_sisa;
				}

				$ttl = $m1 + $m2 + $m3 + $m4 + $m5;


				$date_filer = date('Y-m-d', strtotime($data['tgl_akhir']));
				$date_data = date('Y-m-d', strtotime($datas['tanggal']));


				$penjualan = $penjualan + ($datas['saldo']- ($datas['saldo'] * ($datas['diskons'] / 100)) + (($datas['saldo'] - ($datas['saldo'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)))- $bb_saldo;
				$pengeluaran = $pengeluaran + ($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskons'] / 100)) + (($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)))+ $bayar_r;
				$saldo_akhir = $saldo_akhir + $bayar_r;
				$nilai_akhir = $nilai_akhir + $datas['bayar'];
				$invoice_saldo = $invoice_saldo + $datas['invoice_saldo'];

				$d0 = $d0 + $m1;
				$d30 = $d30 + $m2;
				$d60 = $d60 + $m3;
				$d90 = $d90 + $m4;
				$d120 = $d120 + $m5;
				$total_t = $total_t + $ttl;

				$penjualan_a = $penjualan_a +  ($datas['saldo'] ($datas['saldo'] * ($datas['diskons'] / 100)) + (($datas['saldo'] - ($datas['saldo'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)))- $bb_saldo;
				$pengeluaran_a = $pengeluaran_a +  ($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskons'] / 100)) + (($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)));
				$saldo_akhir_a = $saldo_akhir_a +  $bayar_r;
				$nilai_akhir_a = $nilai_akhir_a +  $datas['bayar'];
				$invoice_saldo_a = $invoice_saldo_a +  $datas['invoice_saldo'];

			
				$d0_a = $d0_a + $m1;
				$d30_a = $d30_a + $m2;
				$d60_a = $d60_a + $m3;
				$d90_a = $d90_a + $m4;
				$d120_a = $d120_a + $m5;
				$total_t_a = $total_t_a + $ttl;

				$sssis =($datas['saldo']- ($datas['saldo'] * ($datas['diskons'] / 100))+ (($datas['saldo'] - ($datas['saldo'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)) - $bb_saldo)+($datas['bulan_berjalan']- ($datas['bulan_berjalan'] * ($datas['diskons'] / 100))+ (($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)));
				
				$html .= '<tr>
								<th >' . $datas['name_eksternal'] . '</th>
								<th>' . $datas['tanggal'] . '</th>
								<th>' . $datas['no_pos'] . '</th>
								<th>' . $datas['no_invoice'] . '</th>
								<th>' . $datas['jatuh_tempo'] . '</th>
								<th>' . $datas['umur'] . '</th>
								<th>' . number_format($datas['saldo']- ($datas['saldo'] * ($datas['diskons'] / 100)) +(($datas['saldo'] - ($datas['saldo'] * ($datas['diskons'] / 100))) * ($datas['pajak'] / 100)) ,2,',','.') . '</th>
								<th>' . number_format($datas['bulan_berjalan'],2,',',
						'.'
					) . '</th>
								<!th>'
					// .$datas['bulan_berjalan']
					// .'-'.( $datas['bulan_berjalan']*($datas['diskons']/100)) 
					// .'+'.($datas['bulan_berjalan']*$datas['pajak']/100) 
					// .'='.$datas['bulan_berjalan']
					// -( $datas['bulan_berjalan']*($datas['diskons']/100)) 
					// // +( ($datas['bulan_berjalan'] -( $datas['bulan_berjalan']*($datas['diskons']/100) )))
					// +($datas['bulan_berjalan']*$datas['pajak']/100) 
					. '<th-->
								<th>' . $datas['tgl_bayar'] . '</th>
								<th>' . $bayar_r_te . '</th>
								<th>' . number_format($sssis, 2, ',', '.') . '</th>
								<th>' . number_format(
						0
							+ $datas['invoice_saldo'],
						2,
						',',
						'.'
					) . '</th>
								<th>' . number_format($m1, 2, ',', '.') . '</th>
								<th>' . number_format($m2, 2, ',', '.') . '</th>
								<th>' . number_format($m3, 2, ',', '.') . '</th>
								<th>' . number_format($m4, 2, ',', '.') . '</th>
								<th>' . number_format($m5, 2, ',', '.') . '</th>
								<th>' . number_format($sssis, 2, ',', '.') . '</th>
							</tr>';

				$prin = $datas['name_eksternal'];


				$curr_invoice = $datas['no_invoice'];


			}

			$html .= '<tr style="background:yellow">
							<th>' . $prin . '</th>
							<th></th>
							<th ></th>
							<th></th>
							<th></th>
							<th></th>
							<th>' . number_format($penjualan, 2, ',', '.') . '</th>
							<th>' . number_format($pengeluaran, 2, ',', '.') . '</th>
							<th></th>
							<th>' . number_format($saldo_akhir, 2, ',', '.') . '</th>
							<th>' . number_format($penjualan + $pengeluaran - $saldo_akhir, 2, ',', '.') . '</th>
							<th>' . number_format($invoice_saldo, 2, ',', '.') . '</th>
							<th>' . number_format($d0, 2, ',', '.') . '</th>
							<th>' . number_format($d30, 2, ',', '.') . '</th>
							<th>' . number_format($d60, 2, ',', '.') . '</th>
							<th>' . number_format($d90, 2, ',', '.') . '</th>
							<th>' . number_format($d120, 2, ',', '.') . '</th>
							<th>' . number_format($penjualan + $pengeluaran - $saldo_akhir, 2, ',', '.') . '</th>
						</tr>';
		}

    $retur_harga_plus_ppn = ($retur_harga + ($retur_harga * 10/100));

		$html .= '<tr style="background:green">
						<th colspan="3">Total </th>
						<th></th>
							<th ></th>
							<th></th>
						<th>' . number_format($penjualan_a - $retur_harga_plus_ppn, 2, ',', '.') . '</th>
						<th>' . number_format($pengeluaran_a, 2, ',', '.') . '</th>
						<th></th>
						<th>' . number_format($saldo_akhir_a, 2, ',', '.') . '</th>
						<th>' . number_format($penjualan_a + $pengeluaran_a - $saldo_akhir_a, 2, ',', '.') . '</th>
						<th>' . number_format($invoice_saldo_a, 2, ',', '.') . '</th>
							<th>' . number_format($d0_a, 2, ',', '.') . '</th>
							<th>' . number_format($d30_a, 2, ',', '.') . '</th>
							<th>' . number_format($d60_a, 2, ',', '.') . '</th>
							<th>' . number_format($d90_a, 2, ',', '.') . '</th>
							<th>' . number_format($d120_a, 2, ',', '.') . '</th>
							<th>' . number_format($penjualan_a + $pengeluaran_a - $saldo_akhir_a, 2, ',', '.') . '</th>
					</tr>';

		$results['html'] = $html;

		// $this->output->set_content_type('application/json')->set_output(json_encode($results));
		$return['data']=$array_print;
		$return['html']=$results;
		$return['total']=$penjualan_a + $pengeluaran_a - $saldo_akhir_a;
		return $return;

	}

	public function list_penjualan($params = array()) {

		$query = 	'
				SELECT COUNT(*) AS jumlah 
				 FROM `t_retur` a
					JOIN `t_penjualan` b ON a.id_penjualan = b.`id_penjualan`
					LEFT JOIN `u_user` c ON a.`pic` = c.id
					LEFT JOIN `u_user` d ON a.`approval` = d.id
					LEFT JOIN `t_customer` e ON b.id_customer = e.`id_t_cust`
					where 1 = 1  AND ( 
						c.nama LIKE "%' . $params['searchtxt'] . '%" OR
						e.cust_name LIKE "%' . $params['searchtxt'] . '%"  OR
						b.no_penjualan LIKE "%' . $params['searchtxt'] . '%"
					)
			';

		$query2 = 	'
				SELECT z.*, rank() over ( ORDER BY date DESC) AS Rangking from ( 
					SELECT a.id_retur,a.date,a.status AS status_retur,a.approval_date,b.*, c.nama AS pic, d.nama AS approval, e.`cust_name` FROM `t_retur` a
					JOIN `t_penjualan` b ON a.id_penjualan = b.`id_penjualan`
					LEFT JOIN `u_user` c ON a.`pic` = c.id
					LEFT JOIN `u_user` d ON a.`approval` = d.id
					LEFT JOIN `t_customer` e ON b.id_customer = e.`id_t_cust`
					where 1 = 1  AND ( 
					c.nama LIKE "%' . $params['searchtxt'] . '%" OR
					e.cust_name LIKE "%' . $params['searchtxt'] . '%"  OR
					b.no_penjualan LIKE "%' . $params['searchtxt'] . '%"
					)  order by date DESC) z
				ORDER BY date DESC
				
				LIMIT ' . $params['limit'] . ' 
				OFFSET ' . $params['offset'] . ' 
			';

		//echo $query2;die;

		$out		= array();
		$querys		= $this->db->query($query);
		$result = $querys->row();

		$total_filtered = $result->jumlah;
		$total 			= $result->jumlah;

		if (($params['offset'] + 10) > $total_filtered) {
			$limit_data = $total_filtered - $params['offset'];
		} else {
			$limit_data = $params['limit'];
		}



		//echo $query;die;
		//echo $query;die;
		$query2s		= $this->db->query($query2);
		$result2 = $query2s->result_array();
		$return = array(
			'data' => $result2,
			'total_filtered' => $total_filtered,
			'total' => $total,
		);
		return $return;
	}


	public function get_customer($params = array()) {

		//$query = $this->db->get_where('t_customer', array('region' => $params['region']));

		$this->db->select('*')
			->from('t_customer')
			# ->where(array('region' => $params['region']))
			->order_by('cust_name', 'asc');
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_report_persediaan($params = array()) {


		if ($params['principal'] == 0) {

			$where_dis = "";
		} else {

			$where_dis = " and a.id_distributor = " . $params['principal'];
		}

		if ($params['principal'] == 825) {

			$sql = "
		
			SELECT *, SUM(pricesa) prices, SUM(saldoa) saldo, SUM(bulan_berjalana) bulan_berjalan, SUM(pajak) pajaks, SUM(diskons) diskonst,
			SUM(invoice_saldoa) invoice_saldo, SUM(min_30a) min_30, SUM(d30a) d30, SUM(d60a) d60, SUM(d90a) d90, SUM(d120a) d120, GROUP_CONCAT(items) AS items, GROUP_CONCAT(DISTINCT(no_po)) AS no_pos  FROM (
			SELECT  bpb.tanggal,h.id_invoice,name_eksternal,date_po,CONCAT(stock_name,' ',base_qty,' ',uom_symbol) items,no_po,DATE_ADD(date_po, INTERVAL `term_of_payment` DAY ) jatuh_tempo, 
			DATEDIFF(NOW(),bpb.tanggal) umur,`no_invoice`, 
			((IF(f.qty IS NULL,f.qty,f.qty))*b.unit_price)-(IF(f.qty IS NULL,b.qty,f.qty)*(b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100))) + (IF(a.seq_n = 8,0,IF(c.pajak=0,0,(IF(f.qty IS NULL,b.qty,f.qty)*(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))-(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))*0.1))) ))) AS pricesa ,
			(IF(bpb.tanggal < '" . $params['tgl_awal'] . "',(IF(f.qty IS NULL,b.qty,f.qty)*b.unit_price)-(IF(f.qty IS NULL,b.qty,f.qty)*(b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100))) + (IF(a.seq_n = 8,0,IF(c.pajak=0,0,(IF(f.qty IS NULL,b.qty,f.qty)*(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))-(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))*0.1))) ))),0 )) saldoa, 
			(IF(bpb.tanggal > '" . $params['tgl_awal'] . "',(IF(f.qty IS NULL,b.qty,f.qty)*b.unit_price)-(IF(f.qty IS NULL,b.qty,f.qty)*(b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100))) + (IF(a.seq_n = 8,0,IF(c.pajak=0,0,(IF(f.qty IS NULL,b.qty,f.qty)*(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))-(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))*0.1))) ))),0 )) bulan_berjalana,
			0 pajak,
			0 diskons,
			IF(bayar IS NULL,0,bayar) AS bayar,IF(bayar_saldo IS NULL,0,bayar_saldo) AS bayar_saldo,tgl_bayar,
			IF(no_invoice IS NULL,0,((IF(f.qty IS NULL,b.qty,f.qty))*b.unit_price))-(IF(f.qty IS NULL,b.qty,f.qty)*(b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100))) + (IF(a.seq_n = 8,0,IF(c.pajak=0,0,(IF(f.qty IS NULL,b.qty,f.qty)*(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))-(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))*0.1))) ))) AS invoice_saldoa,
			IF(DATEDIFF(NOW(),bpb.tanggal) <= 30,((IF(f.qty IS NULL,b.qty,f.qty))*b.unit_price)-(IF(f.qty IS NULL,b.qty,f.qty)*(b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100))) + (IF(a.seq_n = 8,0,IF(c.pajak=0,0,(IF(f.qty IS NULL,b.qty,f.qty)*(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))-(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))*0.1))) ))) - IF(bayar IS NULL,0,bayar) - IF(bayar_saldo IS NULL,0,bayar_saldo),0 ) AS min_30a,
			IF(DATEDIFF(NOW(),bpb.tanggal) BETWEEN 31 AND 60,((IF(f.qty IS NULL,b.qty,f.qty))*b.unit_price)-(IF(f.qty IS NULL,b.qty,f.qty)*(b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100))) + (IF(a.seq_n = 8,0,IF(c.pajak=0,0,(IF(f.qty IS NULL,b.qty,f.qty)*(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))-(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))*0.1))) ))) - IF(bayar IS NULL,0,bayar) - IF(bayar_saldo IS NULL,0,bayar_saldo),0 ) AS d30a,
			IF(DATEDIFF(NOW(),bpb.tanggal) BETWEEN 61 AND 90,((IF(f.qty IS NULL,b.qty,f.qty))*b.unit_price)-(IF(f.qty IS NULL,b.qty,f.qty)*(b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100))) + (IF(a.seq_n = 8,0,IF(c.pajak=0,0,(IF(f.qty IS NULL,b.qty,f.qty)*(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))-(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))*0.1))) ))) - IF(bayar IS NULL,0,bayar) - IF(bayar_saldo IS NULL,0,bayar_saldo),0 ) AS d60a,
			IF(DATEDIFF(NOW(),bpb.tanggal) BETWEEN 91 AND 120,((IF(f.qty IS NULL,b.qty,f.qty))*b.unit_price)-(IF(f.qty IS NULL,b.qty,f.qty)*(b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100))) + (IF(a.seq_n = 8,0,IF(c.pajak=0,0,(IF(f.qty IS NULL,b.qty,f.qty)*(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))-(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))*0.1))) ))) - IF(bayar IS NULL,0,bayar) - IF(bayar_saldo IS NULL,0,bayar_saldo),0 ) AS d90a,
			IF(DATEDIFF(NOW(),bpb.tanggal) > 120,((IF(f.qty IS NULL,b.qty,f.qty))*b.unit_price)-(IF(f.qty IS NULL,b.qty,f.qty)*(b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100))) + (IF(a.seq_n = 8,0,IF(c.pajak=0,0,(IF(f.qty IS NULL,b.qty,f.qty)*(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))-(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))*0.1))) ))) - IF(bayar IS NULL,0,bayar) - IF(bayar_saldo IS NULL,0,bayar_saldo),0 ) AS d120a
			FROM `t_purchase_order` a
			JOIN `t_po_mat` b ON a.`id_po` = b.`id_po`
			JOIN m_material c ON b.id_material = c.id_mat
			JOIN m_uom d ON c.unit_terkecil = d.id_uom
			JOIN `t_eksternal` e ON a.id_distributor = e.`id`
			JOIN `t_bpb_detail` f ON b.id_t_ps = f.id_po_mat
			JOIN t_bpb bpb ON f.id_bpb=bpb.id_bpb
			LEFT JOIN `d_invoice_pembelian` g ON f.`id_bpb_detail` = g.`id_bpb_detail`
			LEFT JOIN `t_invoice_pembelian` h ON g.`id_inv_pembelian` = h.`id_invoice`
			LEFT JOIN
			(
				SELECT a.date as tgl_bayar,b.id_invoice,sum(b.ammount) bayar FROM `t_payment_hp` a
				JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp` WHERE a.`payment_type` = 0 
				AND a.`date` between '" . $params['tgl_awal'] . "' and '" . $params['tgl_akhir'] . "'
				GROUP BY b.id_invoice
			) j ON h.id_invoice = j.id_invoice
			LEFT JOIN
			(
				SELECT a.date as tgl_bayar_saldo,b.id_invoice inv_saldo,sum(b.ammount) bayar_saldo FROM `t_payment_hp` a
				JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp` WHERE a.`payment_type` = 0 
				AND a.`date` < '" . $params['tgl_awal'] . "' 
				GROUP BY b.id_invoice
			) k ON k.inv_saldo = h.id_invoice
			WHERE 1 = 1 " . $where_dis . "
			AND bpb.tanggal <= '" . $params['tgl_akhir'] . "'
			#GROUP BY h.id_invoice
			ORDER BY tanggal,no_po,no_invoice
			
			) A GROUP BY id_invoice
			ORDER BY tanggal,no_invoice


			";
		} else {

			$sql = "	
		
			SELECT *, 
			SUM(pricesa-retur_harga) prices,
			SUM(saldoa) saldo, 
			SUM(bulan_berjalana-retur_harga) bulan_berjalan, 
			SUM(invoice_saldoa-retur_harga) invoice_saldo, 
			SUM(min_30a) min_30, 
			SUM(d30a) d30, 
			SUM(d60a) d60, 
			SUM(d90a) d90, 
			SUM(d120a-retur_harga) d120, 
			GROUP_CONCAT(items) AS items, 
			GROUP_CONCAT(DISTINCT(no_po)) AS no_pos  
			FROM 
			(
			SELECT  
			bpb.tanggal,
			h.id_invoice,
			name_eksternal,
			date_po,
			CONCAT(stock_name,' ',base_qty,' ',uom_symbol) items,no_po,
			DATE_ADD(date_po, INTERVAL `term_of_payment` DAY ) jatuh_tempo, 
			DATEDIFF(NOW(),bpb.tanggal) umur,
			`no_invoice`, 
			(IF(f.qty IS NULL,b.qty,f.qty+f.`qty_bonus`)*b.unit_price) AS pricesa ,
			(IF(l.retur_qty IS NULL,0,l.retur_qty)*b.unit_price) AS retur_harga ,
			(IF(bpb.tanggal < '" . $params['tgl_awal'] . "',((IF(f.qty IS NULL,b.qty,f.qty+f.`qty_bonus`))*b.unit_price),0 )) saldoa, 
			(IF(bpb.tanggal > '" . $params['tgl_awal'] . "',((IF(f.qty IS NULL,b.qty,f.qty+f.`qty_bonus`))*b.unit_price),0 )) bulan_berjalana,
			IF(a.seq_n = 8,0,c.pajak) pajak,IF(g.qty IS NULL,b.diskon,g.diskon) diskons,
			IF(bayar IS NULL,0,bayar) AS bayar,
			IF(bayar_saldo IS NULL,0,bayar_saldo) AS bayar_saldo,
			tgl_bayar,
			IF(no_invoice IS NULL,0,((IF(f.qty IS NULL,b.qty,f.qty+f.`qty_bonus`))*b.unit_price)) AS invoice_saldoa,
			IF(DATEDIFF(NOW(),bpb.tanggal) <= 30,((IF(f.qty IS NULL,b.qty,f.qty+f.`qty_bonus`))*b.unit_price),0 ) AS min_30a,
			IF(DATEDIFF(NOW(),bpb.tanggal) BETWEEN 31 AND 60,((IF(f.qty IS NULL,b.qty,f.qty+f.`qty_bonus`))*b.unit_price),0 ) AS d30a,
			IF(DATEDIFF(NOW(),bpb.tanggal) BETWEEN 61 AND 90,((IF(f.qty IS NULL,b.qty,f.qty+f.`qty_bonus`))*b.unit_price),0 ) AS d60a,
			IF(DATEDIFF(NOW(),bpb.tanggal) BETWEEN 91 AND 120,((IF(f.qty IS NULL,b.qty,f.qty+f.`qty_bonus`))*b.unit_price),0 ) AS d90a,
			f.id_bpb_detail,
			b.id_t_ps,
			IF(DATEDIFF(NOW(),bpb.tanggal) > 120,((IF(f.qty IS NULL,b.qty,f.qty+f.`qty_bonus`))*b.unit_price),0 ) AS d120a
			FROM `t_purchase_order` a
			JOIN `t_po_mat` b ON a.`id_po` = b.`id_po`
			JOIN m_material c ON b.id_material = c.id_mat
			JOIN m_uom d ON c.unit_terkecil = d.id_uom
			JOIN `t_eksternal` e ON a.id_distributor = e.`id`
			JOIN `t_bpb_detail` f ON b.id_t_ps = f.id_po_mat
			JOIN t_bpb bpb ON f.id_bpb=bpb.id_bpb
      LEFT JOIN t_invoice_pembelian h ON bpb.id_bpb=h.id_bpb
      LEFT JOIN d_invoice_pembelian g ON f.id_bpb_detail=g.id_bpb_detail
      #LEFT JOIN t_retur_pembelian i ON h.id_invoice=i.id_invoice AND i.`retur_date` <='" . $params['tgl_akhir'] . "'
      #LEFT JOIN t_detail_retur_pembelian l ON i.`id_retur_pembelian`=l.`id_retur_pembelian`
			left join (
				select a1.`retur_qty`,b1.`retur_date`,b1.`id_retur_pembelian`,a1.`id_po_mat`
				from `t_detail_retur_pembelian` a1
				join `t_retur_pembelian` b1 on a1.`id_retur_pembelian` = b1.`id_retur_pembelian`
				where b1.`retur_date` <= '" . $params['tgl_akhir'] . "'
      )l on l.id_po_mat = b.`id_t_ps`
			LEFT JOIN
			(
				SELECT a.date AS tgl_bayar,b.id_invoice,SUM(b.ammount) bayar 
				FROM `t_payment_hp` a
				JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp` WHERE a.`payment_type` = 0 
				AND a.`date` BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "'
				GROUP BY b.id_invoice
			) j ON h.id_invoice = j.id_invoice
			LEFT JOIN
			(
				SELECT a.date AS tgl_bayar_saldo,b.id_invoice inv_saldo,SUM(b.ammount) bayar_saldo 
				FROM `t_payment_hp` a
				JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp` WHERE a.`payment_type` = 0 
				AND a.`date` < '" . $params['tgl_awal'] . "' 
				GROUP BY b.id_invoice
			) k ON k.inv_saldo = h.id_invoice
			WHERE 1 = 1  " . $where_dis . "
			AND bpb.tanggal <= '" . $params['tgl_akhir'] . "'
			#GROUP BY h.id_invoice
			ORDER BY tanggal,no_po,no_invoice
			
			) A 
			#GROUP BY id_invoice
			GROUP BY id_invoice,id_bpb_detail,id_t_ps
			ORDER BY tanggal,no_invoice			
			";
		}
		if($params['principal']==894){
			// echo "<pre>";print_r($sql);die;

		}

		$query 	= $this->db->query($sql);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_report_persediaan_old($params = array()) {


		if ($params['principal'] == 0) {

			$where_dis = "";
		} else {

			$where_dis = " and a.id_distributor = " . $params['principal'];
		}

		if ($params['principal'] == 825) {

			$sql = "
		
			SELECT *, SUM(pricesa) prices, SUM(saldoa) saldo, SUM(bulan_berjalana) bulan_berjalan, SUM(pajak) pajaks, SUM(diskons) diskonst,
			SUM(invoice_saldoa) invoice_saldo, SUM(min_30a) min_30, SUM(d30a) d30, SUM(d60a) d60, SUM(d90a) d90, SUM(d120a) d120, GROUP_CONCAT(items) AS items, GROUP_CONCAT(DISTINCT(no_po)) AS no_pos  FROM (
			SELECT  bpb.tanggal,h.id_invoice,name_eksternal,date_po,CONCAT(stock_name,' ',base_qty,' ',uom_symbol) items,no_po,DATE_ADD(date_po, INTERVAL `term_of_payment` DAY ) jatuh_tempo, 
			DATEDIFF(NOW(),bpb.tanggal) umur,`no_invoice`, 
			((IF(f.qty IS NULL,f.qty,f.qty))*b.unit_price)-(IF(f.qty IS NULL,b.qty,f.qty)*(b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100))) + (IF(a.seq_n = 8,0,IF(c.pajak=0,0,(IF(f.qty IS NULL,b.qty,f.qty)*(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))-(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))*0.1))) ))) AS pricesa ,
			(IF(bpb.tanggal < '" . $params['tgl_awal'] . "',(IF(f.qty IS NULL,b.qty,f.qty)*b.unit_price)-(IF(f.qty IS NULL,b.qty,f.qty)*(b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100))) + (IF(a.seq_n = 8,0,IF(c.pajak=0,0,(IF(f.qty IS NULL,b.qty,f.qty)*(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))-(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))*0.1))) ))),0 )) saldoa, 
			(IF(bpb.tanggal > '" . $params['tgl_awal'] . "',(IF(f.qty IS NULL,b.qty,f.qty)*b.unit_price)-(IF(f.qty IS NULL,b.qty,f.qty)*(b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100))) + (IF(a.seq_n = 8,0,IF(c.pajak=0,0,(IF(f.qty IS NULL,b.qty,f.qty)*(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))-(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))*0.1))) ))),0 )) bulan_berjalana,
			0 pajak,
			0 diskons,
			IF(bayar IS NULL,0,bayar) AS bayar,IF(bayar_saldo IS NULL,0,bayar_saldo) AS bayar_saldo,tgl_bayar,
			IF(no_invoice IS NULL,0,((IF(f.qty IS NULL,b.qty,f.qty))*b.unit_price))-(IF(f.qty IS NULL,b.qty,f.qty)*(b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100))) + (IF(a.seq_n = 8,0,IF(c.pajak=0,0,(IF(f.qty IS NULL,b.qty,f.qty)*(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))-(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))*0.1))) ))) AS invoice_saldoa,
			IF(DATEDIFF(NOW(),bpb.tanggal) <= 30,((IF(f.qty IS NULL,b.qty,f.qty))*b.unit_price)-(IF(f.qty IS NULL,b.qty,f.qty)*(b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100))) + (IF(a.seq_n = 8,0,IF(c.pajak=0,0,(IF(f.qty IS NULL,b.qty,f.qty)*(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))-(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))*0.1))) ))) - IF(bayar IS NULL,0,bayar) - IF(bayar_saldo IS NULL,0,bayar_saldo),0 ) AS min_30a,
			IF(DATEDIFF(NOW(),bpb.tanggal) BETWEEN 31 AND 60,((IF(f.qty IS NULL,b.qty,f.qty))*b.unit_price)-(IF(f.qty IS NULL,b.qty,f.qty)*(b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100))) + (IF(a.seq_n = 8,0,IF(c.pajak=0,0,(IF(f.qty IS NULL,b.qty,f.qty)*(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))-(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))*0.1))) ))) - IF(bayar IS NULL,0,bayar) - IF(bayar_saldo IS NULL,0,bayar_saldo),0 ) AS d30a,
			IF(DATEDIFF(NOW(),bpb.tanggal) BETWEEN 61 AND 90,((IF(f.qty IS NULL,b.qty,f.qty))*b.unit_price)-(IF(f.qty IS NULL,b.qty,f.qty)*(b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100))) + (IF(a.seq_n = 8,0,IF(c.pajak=0,0,(IF(f.qty IS NULL,b.qty,f.qty)*(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))-(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))*0.1))) ))) - IF(bayar IS NULL,0,bayar) - IF(bayar_saldo IS NULL,0,bayar_saldo),0 ) AS d60a,
			IF(DATEDIFF(NOW(),bpb.tanggal) BETWEEN 91 AND 120,((IF(f.qty IS NULL,b.qty,f.qty))*b.unit_price)-(IF(f.qty IS NULL,b.qty,f.qty)*(b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100))) + (IF(a.seq_n = 8,0,IF(c.pajak=0,0,(IF(f.qty IS NULL,b.qty,f.qty)*(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))-(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))*0.1))) ))) - IF(bayar IS NULL,0,bayar) - IF(bayar_saldo IS NULL,0,bayar_saldo),0 ) AS d90a,
			IF(DATEDIFF(NOW(),bpb.tanggal) > 120,((IF(f.qty IS NULL,b.qty,f.qty))*b.unit_price)-(IF(f.qty IS NULL,b.qty,f.qty)*(b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100))) + (IF(a.seq_n = 8,0,IF(c.pajak=0,0,(IF(f.qty IS NULL,b.qty,f.qty)*(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))-(((b.unit_price * (IF(g.qty IS NULL,b.diskon,g.diskon)/100)))*0.1))) ))) - IF(bayar IS NULL,0,bayar) - IF(bayar_saldo IS NULL,0,bayar_saldo),0 ) AS d120a
			FROM `t_purchase_order` a
			JOIN `t_po_mat` b ON a.`id_po` = b.`id_po`
			JOIN m_material c ON b.id_material = c.id_mat
			JOIN m_uom d ON c.unit_terkecil = d.id_uom
			JOIN `t_eksternal` e ON a.id_distributor = e.`id`
			JOIN `t_bpb_detail` f ON b.id_t_ps = f.id_po_mat
			JOIN t_bpb bpb ON f.id_bpb=bpb.id_bpb
			LEFT JOIN `d_invoice_pembelian` g ON f.`id_bpb_detail` = g.`id_bpb_detail`
			LEFT JOIN `t_invoice_pembelian` h ON g.`id_inv_pembelian` = h.`id_invoice`
			LEFT JOIN
			(
				SELECT a.date as tgl_bayar,b.id_invoice,sum(b.ammount) bayar FROM `t_payment_hp` a
				JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp` WHERE a.`payment_type` = 0 
				AND a.`date` between '" . $params['tgl_awal'] . "' and '" . $params['tgl_akhir'] . "'
				GROUP BY b.id_invoice
			) j ON h.id_invoice = j.id_invoice
			LEFT JOIN
			(
				SELECT a.date as tgl_bayar_saldo,b.id_invoice inv_saldo,sum(b.ammount) bayar_saldo FROM `t_payment_hp` a
				JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp` WHERE a.`payment_type` = 0 
				AND a.`date` < '" . $params['tgl_awal'] . "' 
				GROUP BY b.id_invoice
			) k ON k.inv_saldo = h.id_invoice
			WHERE 1 = 1 " . $where_dis . "
			AND bpb.tanggal <= '" . $params['tgl_akhir'] . "'
			#GROUP BY h.id_invoice
			ORDER BY tanggal,no_po,no_invoice
			
			) A GROUP BY id_invoice
			ORDER BY tanggal,no_invoice


			";
		} else {

			$sql = "	
		
			SELECT *, 
			SUM(pricesa-retur_harga) prices,
			SUM(saldoa) saldo, 
			SUM(bulan_berjalana-retur_harga) bulan_berjalan, 
			SUM(invoice_saldoa-retur_harga) invoice_saldo, 
			SUM(min_30a) min_30, 
			SUM(d30a) d30, 
			SUM(d60a) d60, 
			SUM(d90a) d90, 
			SUM(d120a-retur_harga) d120, 
			GROUP_CONCAT(items) AS items, 
			GROUP_CONCAT(DISTINCT(no_po)) AS no_pos  
			FROM 
			(
			SELECT  
			bpb.tanggal,
			h.id_invoice,
			name_eksternal,
			date_po,
			CONCAT(stock_name,' ',base_qty,' ',uom_symbol) items,no_po,
			DATE_ADD(date_po, INTERVAL `term_of_payment` DAY ) jatuh_tempo, 
			DATEDIFF(NOW(),bpb.tanggal) umur,
			`no_invoice`, 
			(IF(f.qty IS NULL,b.qty,f.qty+f.`qty_bonus`)*b.unit_price) AS pricesa ,
			(IF(l.retur_qty IS NULL,0,l.retur_qty)*b.unit_price) AS retur_harga ,
			(IF(bpb.tanggal < '" . $params['tgl_awal'] . "',((IF(f.qty IS NULL,b.qty,f.qty+f.`qty_bonus`))*b.unit_price),0 )) saldoa, 
			(IF(bpb.tanggal > '" . $params['tgl_awal'] . "',((IF(f.qty IS NULL,b.qty,f.qty+f.`qty_bonus`))*b.unit_price),0 )) bulan_berjalana,
			IF(a.seq_n = 8,0,c.pajak) pajak,IF(g.qty IS NULL,b.diskon,g.diskon) diskons,
			IF(bayar IS NULL,0,bayar) AS bayar,
			IF(bayar_saldo IS NULL,0,bayar_saldo) AS bayar_saldo,
			tgl_bayar,
			IF(no_invoice IS NULL,0,((IF(f.qty IS NULL,b.qty,f.qty+f.`qty_bonus`))*b.unit_price)) AS invoice_saldoa,
			IF(DATEDIFF(NOW(),bpb.tanggal) <= 30,((IF(f.qty IS NULL,b.qty,f.qty+f.`qty_bonus`))*b.unit_price),0 ) AS min_30a,
			IF(DATEDIFF(NOW(),bpb.tanggal) BETWEEN 31 AND 60,((IF(f.qty IS NULL,b.qty,f.qty+f.`qty_bonus`))*b.unit_price),0 ) AS d30a,
			IF(DATEDIFF(NOW(),bpb.tanggal) BETWEEN 61 AND 90,((IF(f.qty IS NULL,b.qty,f.qty+f.`qty_bonus`))*b.unit_price),0 ) AS d60a,
			IF(DATEDIFF(NOW(),bpb.tanggal) BETWEEN 91 AND 120,((IF(f.qty IS NULL,b.qty,f.qty+f.`qty_bonus`))*b.unit_price),0 ) AS d90a,
			f.id_bpb_detail,
			b.id_t_ps,
			IF(DATEDIFF(NOW(),bpb.tanggal) > 120,((IF(f.qty IS NULL,b.qty,f.qty+f.`qty_bonus`))*b.unit_price),0 ) AS d120a
			FROM `t_purchase_order` a
			JOIN `t_po_mat` b ON a.`id_po` = b.`id_po`
			JOIN m_material c ON b.id_material = c.id_mat
			JOIN m_uom d ON c.unit_terkecil = d.id_uom
			JOIN `t_eksternal` e ON a.id_distributor = e.`id`
			JOIN `t_bpb_detail` f ON b.id_t_ps = f.id_po_mat
			JOIN t_bpb bpb ON f.id_bpb=bpb.id_bpb
      LEFT JOIN t_invoice_pembelian h ON bpb.id_bpb=h.id_bpb
      LEFT JOIN d_invoice_pembelian g ON f.id_bpb_detail=g.id_bpb_detail
      LEFT JOIN t_retur_pembelian i ON h.id_invoice=i.id_invoice AND i.`retur_date` <='" . $params['tgl_akhir'] . "'
      LEFT JOIN t_detail_retur_pembelian l ON l.id_po_mat=b.id_t_ps
			LEFT JOIN
			(
				SELECT a.date AS tgl_bayar,b.id_invoice,SUM(b.ammount) bayar 
				FROM `t_payment_hp` a
				JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp` WHERE a.`payment_type` = 0 
				AND a.`date` BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "'
				GROUP BY b.id_invoice
			) j ON h.id_invoice = j.id_invoice
			LEFT JOIN
			(
				SELECT a.date AS tgl_bayar_saldo,b.id_invoice inv_saldo,SUM(b.ammount) bayar_saldo 
				FROM `t_payment_hp` a
				JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp` WHERE a.`payment_type` = 0 
				AND a.`date` < '" . $params['tgl_awal'] . "' 
				GROUP BY b.id_invoice
			) k ON k.inv_saldo = h.id_invoice
			WHERE 1 = 1  " . $where_dis . "
			AND bpb.tanggal <= '" . $params['tgl_akhir'] . "'
			#GROUP BY h.id_invoice
			ORDER BY tanggal,no_po,no_invoice
			
			) A 
			GROUP BY id_bpb_detail,id_t_ps
			ORDER BY tanggal,no_invoice			
			";
		}
		// echo "<pre>";print_r($sql);die;


		$query 	= $this->db->query($sql);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	public function get_distribution($params = array()) {




		$sql = "
			SELECT `id_distributor` dist_id,b.`name_eksternal` 
			FROM `t_purchase_order` a
			JOIN `t_eksternal` b ON a.`id_distributor` = b.id
			GROUP BY `id_distributor`
			ORDER BY `kode_eksternal`
		";





		$query 	= $this->db->query($sql);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}


	public function get_report_pembelian($params = array()) {


		if ($params['principal'] == 0) {


			$query = "
				SELECT COUNT(*) AS jumlah from (
				SELECT a.`date_po` AS Tanggal,
				d.`stock_code` AS Kode ,
				d.`stock_name` AS Nama_barang,
				CONCAT(d.base_qty,' ',f.uom_name) AS kemasan,
				e.`name_eksternal`,
				c.qty,
				c.`unit_price` -(c.`unit_price`*(c.diskon/100)) AS harga,
				SUM(c.`price`) AS jumlah,
				SUM(c.`price`*0.01) AS PPN,
				SUM(c.`price`)+SUM(c.`price`*0.01) AS Rp_PPN,
				a.no_po AS No_trans,
				b.`no_invoice`,
				DATE_ADD(a.date_po,INTERVAL a.`term_of_payment` DAY) AS jatuh_tempo,
				a.`term_of_payment` AS lama_hari,
				b.`faktur_pajak`,
				b.`tanggal_faktur`

				FROM t_purchase_order a
				JOIN t_po_mat aa ON a.id_po=aa.id_po
				JOIN `t_bpb` cc ON a.`id_po` = cc.`id_po`
				JOIN t_invoice_pembelian b ON cc.id_bpb=b.id_bpb
				JOIN d_invoice_pembelian c ON b.id_invoice=c.id_inv_pembelian 
				JOIN m_material d ON aa.id_material=d.id_mat
				JOIN t_eksternal e ON d.dist_id=e.id
				JOIN m_uom f ON d.unit_terkecil=f.id_uom
				WHERE date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "'
				GROUP BY aa.id_t_ps) z
								
						";


							$query2 = "
						SELECT z.*, rank() over ( ORDER BY Tanggal,no_invoice,Kode,Nama_barang DESC) AS Rangking from ( 
						SELECT a.`date_po` AS Tanggal,
				d.`stock_code` AS Kode ,
				d.`stock_name` AS Nama_barang,
				CONCAT(d.base_qty,' ',f.uom_name) AS kemasan,
				e.`name_eksternal`,
				c.qty,
				c.`unit_price` -(c.`unit_price`*(c.diskon/100)) AS harga,
				SUM(c.`price`) AS jumlah,
				SUM(c.`price`*0.01) AS PPN,
				SUM(c.`price`)+SUM(c.`price`*0.01) AS Rp_PPN,
				a.no_po AS No_trans,
				b.`no_invoice`,
				DATE_ADD(a.date_po,INTERVAL a.`term_of_payment` DAY) AS jatuh_tempo,
				a.`term_of_payment` AS lama_hari,
				b.`faktur_pajak`,
				b.`tanggal_faktur`


				FROM t_purchase_order a
				JOIN t_po_mat aa ON a.id_po=aa.id_po
				JOIN `t_bpb` cc ON a.`id_po` = cc.`id_po`
				JOIN t_invoice_pembelian b ON cc.id_bpb=b.id_bpb
				JOIN d_invoice_pembelian c ON b.id_invoice=c.id_inv_pembelian 
				JOIN m_material d ON aa.id_material=d.id_mat
				JOIN t_eksternal e ON d.dist_id=e.id
				JOIN m_uom f ON d.unit_terkecil=f.id_uom
				WHERE date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "'
				GROUP BY aa.id_t_ps) z
								ORDER BY Tanggal,no_invoice,Kode,Nama_barang DESC
								LIMIT " . $params['limit'] . " 
								OFFSET " . $params['offset'] . " 
						";
						} else {

							$query = "
						SELECT COUNT(*) AS jumlah from (
						SELECT a.`date_po` AS Tanggal,
				d.`stock_code` AS Kode ,
				d.`stock_name` AS Nama_barang,
				CONCAT(d.base_qty,' ',f.uom_name) AS kemasan,
				e.`name_eksternal`,
				c.qty,
				c.`unit_price` -(c.`unit_price`*(c.diskon/100)) AS harga,
				SUM(c.`price`) AS jumlah,
				SUM(c.`price`*0.01) AS PPN,
				SUM(c.`price`)+SUM(c.`price`*0.01) AS Rp_PPN,
				a.no_po AS No_trans,
				b.`no_invoice`,
				DATE_ADD(a.date_po,INTERVAL a.`term_of_payment` DAY) AS jatuh_tempo,
				a.`term_of_payment` AS lama_hari,
				b.`faktur_pajak`,
				b.`tanggal_faktur`

				FROM t_purchase_order a
				JOIN t_po_mat aa ON a.id_po=aa.id_po
				JOIN `t_bpb` cc ON a.`id_po` = cc.`id_po`
				JOIN t_invoice_pembelian b ON cc.id_bpb=b.id_bpb
				JOIN d_invoice_pembelian c ON b.id_invoice=c.id_inv_pembelian 
				JOIN m_material d ON aa.id_material=d.id_mat
				JOIN t_eksternal e ON d.dist_id=e.id
				JOIN m_uom f ON d.unit_terkecil=f.id_uom
				WHERE date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "'
				AND a.id_distributor = " . $params['principal'] . "
				GROUP BY aa.id_t_ps) z
								
						";


							$query2 = "
						SELECT z.*, rank() over ( ORDER BY Tanggal,no_invoice,Kode,Nama_barang DESC) AS Rangking from ( 
						SELECT a.`date_po` AS Tanggal,
				d.`stock_code` AS Kode ,
				d.`stock_name` AS Nama_barang,
				CONCAT(d.base_qty,' ',f.uom_name) AS kemasan,
				e.`name_eksternal`,
				c.qty,
				c.`unit_price` -(c.`unit_price`*(c.diskon/100)) AS harga,
				SUM(c.`price`) AS jumlah,
				SUM(c.`price`*0.01) AS PPN,
				SUM(c.`price`)+SUM(c.`price`*0.01) AS Rp_PPN,
				a.no_po AS No_trans,
				b.`no_invoice`,
				DATE_ADD(a.date_po,INTERVAL a.`term_of_payment` DAY) AS jatuh_tempo,
				a.`term_of_payment` AS lama_hari,
				b.`faktur_pajak`,
				b.`tanggal_faktur`


				FROM t_purchase_order a
				JOIN t_po_mat aa ON a.id_po=aa.id_po
				JOIN `t_bpb` cc ON a.`id_po` = cc.`id_po`
				JOIN t_invoice_pembelian b ON cc.id_bpb=b.id_bpb
				JOIN d_invoice_pembelian c ON b.id_invoice=c.id_inv_pembelian 
				JOIN m_material d ON aa.id_material=d.id_mat
				JOIN t_eksternal e ON d.dist_id=e.id
				JOIN m_uom f ON d.unit_terkecil=f.id_uom
				WHERE date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "'
				AND a.id_distributor = " . $params['principal'] . "
				GROUP BY aa.id_t_ps) z
				ORDER BY Tanggal,no_invoice,Kode,Nama_barang DESC
				LIMIT " . $params['limit'] . " 
				OFFSET " . $params['offset'] . " 
		";
		}

		$out		= array();
		$querys		= $this->db->query($query);
		$result = $querys->row();

		$total_filtered = $result->jumlah;
		$total 			= $result->jumlah;

		if (($params['offset'] + 10) > $total_filtered) {
			$limit_data = $total_filtered - $params['offset'];
		} else {
			$limit_data = $params['limit'];
		}



		//echo $query;die;
		//echo $query;die;
		$query2s		= $this->db->query($query2);
		$result2 = $query2s->result_array();
		$return = array(
			'data' => $result2,
			'total_filtered' => $total_filtered,
			'total' => $total,
		);
		return $return;
	}


	public function get_report_pembelian_export($params = array()) {

		if ($params['principal'] == '0') {

			$sql = "
					
			SELECT z.*, rank() over ( ORDER BY Tanggal,no_invoice,Kode,Nama_barang DESC) AS Rangking from ( 
					SELECT a.`date_po` AS Tanggal,
			d.`stock_code` AS Kode ,
			d.`stock_name` AS Nama_barang,
			CONCAT(d.base_qty,' ',f.uom_name) AS kemasan,
			e.`name_eksternal`,
			c.qty,
			c.`unit_price` -(c.`unit_price`*(c.diskon/100)) AS harga,
			SUM(c.`price`) AS jumlah,
			SUM(c.`price`*0.01) AS PPN,
			SUM(c.`price`)+SUM(c.`price`*0.01) AS Rp_PPN,
			a.no_po AS No_trans,
			b.`no_invoice`,
			DATE_ADD(a.date_po,INTERVAL a.`term_of_payment` DAY) AS jatuh_tempo,
			a.`term_of_payment` AS lama_hari,
			b.`faktur_pajak`,
			b.`tanggal_faktur`


			FROM t_purchase_order a
			JOIN t_po_mat aa ON a.id_po=aa.id_po
			JOIN `t_bpb` cc ON a.`id_po` = cc.`id_po`
			JOIN t_invoice_pembelian b ON cc.id_bpb=b.id_bpb
			JOIN d_invoice_pembelian c ON b.id_invoice=c.id_inv_pembelian 
			JOIN m_material d ON aa.id_material=d.id_mat
			JOIN t_eksternal e ON d.dist_id=e.id
			JOIN m_uom f ON d.unit_terkecil=f.id_uom
			WHERE date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "'
			GROUP BY aa.id_t_ps) z
				ORDER BY Tanggal,no_invoice,Kode,Nama_barang DESC
		";
		} else {

			$sql = "
						
				SELECT z.*, rank() over ( ORDER BY Tanggal,no_invoice,Kode,Nama_barang DESC) AS Rangking from ( 
						SELECT a.`date_po` AS Tanggal,
				d.`stock_code` AS Kode ,
				d.`stock_name` AS Nama_barang,
				CONCAT(d.base_qty,' ',f.uom_name) AS kemasan,
				e.`name_eksternal`,
				c.qty,
				c.`unit_price` -(c.`unit_price`*(c.diskon/100)) AS harga,
				SUM(c.`price`) AS jumlah,
				SUM(c.`price`*0.01) AS PPN,
				SUM(c.`price`)+SUM(c.`price`*0.01) AS Rp_PPN,
				a.no_po AS No_trans,
				b.`no_invoice`,
				DATE_ADD(a.date_po,INTERVAL a.`term_of_payment` DAY) AS jatuh_tempo,
				a.`term_of_payment` AS lama_hari,
				b.`faktur_pajak`,
				b.`tanggal_faktur`


				FROM t_purchase_order a
				JOIN t_po_mat aa ON a.id_po=aa.id_po
				JOIN `t_bpb` cc ON a.`id_po` = cc.`id_po`
				JOIN t_invoice_pembelian b ON cc.id_bpb=b.id_bpb
				JOIN d_invoice_pembelian c ON b.id_invoice=c.id_inv_pembelian 
				JOIN m_material d ON aa.id_material=d.id_mat
				JOIN t_eksternal e ON d.dist_id=e.id
				JOIN m_uom f ON d.unit_terkecil=f.id_uom
				WHERE date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "'
				AND a.id_distributor = " . $params['principal'] . "
				GROUP BY aa.id_t_ps) z
				ORDER BY Tanggal,no_invoice,Kode,Nama_barang DESC
		";
		}


		$query 	= $this->db->query($sql);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}


	public function get_report_pembelian_prin($params = array()) {

		$sql = "
		SELECT a.`date_po` AS Tanggal,
			d.`stock_code` AS Kode ,
			d.`stock_name` AS Nama_barang,
			CONCAT(d.base_qty,' ',f.uom_name) AS kemasan,
			e.`name_eksternal`,
			c.qty,
			c.`unit_price` -(c.`unit_price`*(c.diskon/100)) AS harga,
			SUM(c.`price`) AS jumlah,
			SUM(c.`price`*0.01) AS PPN,
			SUM(c.`price`)+SUM(c.`price`*0.01) AS Rp_PPN,
			a.no_po AS No_trans,
			b.`no_invoice`,
			DATE_ADD(a.date_po,INTERVAL a.`term_of_payment` DAY) AS jatuh_tempo,
			a.`term_of_payment` AS lama_hari,
			b.`faktur_pajak`

			FROM t_purchase_order a
			JOIN t_po_mat aa ON a.id_po=aa.id_po
			JOIN t_invoice_pembelian b ON a.id_po=b.id_po
			JOIN d_invoice_pembelian c ON b.id_invoice=c.id_inv_pembelian and aa.id_t_ps = c.id_po_mat
			JOIN m_material d ON aa.id_material=d.id_mat
			JOIN t_eksternal e ON d.dist_id=e.id
			JOIN m_uom f ON d.unit_terkecil=f.id_uom
			WHERE date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "'
			AND e.id = " . $params['principal'] . "
			GROUP BY aa.id_t_ps
		";

		//echo $sql;die;

		$query 	= $this->db->query($sql);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_principal($params = array()) {

		//$query = $this->db->get_where('t_customer', array('region' => $params['region']));

		$this->db->select('*')
			->from('t_eksternal')
			->where(array('sts_show' => 1))
			->order_by('name_eksternal', 'asc');
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_price_mat($params = array()) {

		// $query = $this->db->get_where('t_po_mat', array('id_po' => $params['kode']));
		// $query = $this->db->order_by('id_t_ps', 'desc');

		$this->db->select('*')
			->from('t_po_mat')
			->where(array('id_material' => $params['kode']))
			->order_by('id_t_ps', 'desc');
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_penjualan_byid($params = array()) {

		$this->db->select('t_penjualan.*, t_customer.*')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_penjualan')
			->join('t_customer', 't_penjualan.id_customer = t_customer.id_t_cust')
			->where(array('id_penjualan' => $params['id_penjualan']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}


	public function get_item_byprin($params = array()) {
		$this->db->select('m_material.*, m_uom.uom_symbol')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('m_material')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('dist_id' => $params['id_prin']));
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_retur_id($params = array()) {
		$this->db->select('t_retur.date,t_retur.id_penjualan,t_retur.keterangan, t_detail_retur.*')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_retur')
			->join('t_detail_retur', 't_retur.id_retur = t_detail_retur.id_retur')
			->where(array('t_retur.id_retur' => $params['id']));
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_item_byprin_all() {
		$this->db->select('m_material.*, m_uom.uom_symbol')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('m_material')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('status' => 0))
			->order_by('stock_name', 'asc');
		//->where(array('dist_id' => $params['id_prin']));
		// ->where('m_material.dist_id != ',$params['id_prin'],FALSE);
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_penjualan_all() {
		$this->db->select('t_penjualan.*, t_customer.cust_name')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_penjualan')
			->join('t_customer', 't_penjualan.id_customer = t_customer.id_t_cust')
			->where(array('status' => 0))
			->order_by('date_penjualan', 'desc');
		//->where(array('dist_id' => $params['id_prin']));
		// ->where('m_material.dist_id != ',$params['id_prin'],FALSE);
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_kode($params = array()) {

		$query =  $this->db->select_max('seq_n', 'max');
		$query = $this->db->get('t_penjualan');

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_penjualan($params = array()) {

		// $query = $this->db->get_where('t_purchase_order', array('id_po' => $params['id']));
		$this->db->select('t_penjualan.*, t_customer.*')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_penjualan')
			->join('t_customer', 't_penjualan.id_customer = t_customer.id_t_cust')
			->where(array('id_penjualan' => $params['id']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_penjualan_detail_update($params = array()) {

		//$query = $this->db->get_where('d_penjualan', array('id_penjualan' => $params['id_penjualan']));
		$this->db->select('d_penjualan.*, m_material.stock_name, m_material.base_qty, m_material.stock_name, m_uom.uom_symbol,t_detail_retur.qty as qty_retur,t_detail_retur.qty_satuan as qty_retur_sat,t_detail_retur.qty_box as qty_box_retur,t_detail_retur.amount ')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('d_penjualan')
			->join('m_material', 'd_penjualan.id_material = m_material.id_mat')
			->join('t_detail_retur', 'd_penjualan.id_dp = t_detail_retur.id_detail_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('t_detail_retur.id_retur' => $params['id_penjualan']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_penjualan_detail($params = array()) {

		//$query = $this->db->get_where('d_penjualan', array('id_penjualan' => $params['id_penjualan']));
		$this->db->select('d_penjualan.*, m_material.stock_name, m_material.base_qty, m_material.stock_name, m_uom.uom_symbol')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('d_penjualan')
			->join('m_material', 'd_penjualan.id_material = m_material.id_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('id_penjualan' => $params['id_penjualan']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_po_detail_full($params = array()) {

		//$query = $this->db->get_where('t_po_mat', array('id_po' => $params['id']));
		$this->db->select('t_po_mat.*,t_po_mat.qty as qty_order, m_material.*, m_uom.*')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_po_mat')
			->join('m_material', 't_po_mat.id_material = m_material.id_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('id_po' => $params['id']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function check_principal($params = array()) {

		$query = $this->db->get_where('t_eksternal', array('kode_eksternal' => $params['kode']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_returm($data) {

		$this->db->insert('t_retur', $data);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_mutasi($data) {

		$data_insert_mutasi = array(
			'id_stock_awal'  => $data['id_material'],
			'id_stock_akhir' => $data['id_material'],
			'date_mutasi'    => date('Y-m-d H:i:s'),
			'amount_mutasi'  => $data['qty'],
			'type_mutasi'    => 0,
			'user_id'    => $this->session->userdata['logged_in']['user_id'],
		);

		//	print_r($datas);die;

		$this->db->insert('t_mutasi', $data_insert_mutasi);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_mutasi_out($data) {

		$data_insert_mutasi = array(
			'id_stock_awal'  => $data['id_material'],
			'id_stock_akhir' => $data['id_material'],
			'date_mutasi'    => date('Y-m-d H:i:s'),
			'amount_mutasi'  => $data['qty'],
			'type_mutasi'    => 1,
			'user_id'    => $this->session->userdata['logged_in']['user_id'],
		);

		//	print_r($datas);die;

		$this->db->insert('t_mutasi', $data_insert_mutasi);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_detail_retur($data) {

		//	print_r($datas);die;

		$this->db->insert('t_detail_retur', $data);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_qty($data) {
		$sql 	= 'update m_material set qty = qty - ' . $data['qty'] . ' 
		where id_mat = ' . $data['id_material'] . ' ';

		// $datas = array(
		// 'qty' => $data['qty_stock']
		// );

		// //	print_r($datas);die;
		// $this->db->where('id_mat',$data['id_material'], false);
		// $this->db->update('m_material',$datas, false);


		$query 	= $this->db->query($sql);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function update_qty_m($data) {
		$sql 	= 'update m_material set qty = qty + ' . $data['qty'] . ' 
		where id_mat = ' . $data['id_mat'] . ' ';

		//echo $sql;die;
		// $datas = array(
		// 'qty' => $data['qty_stock']
		// );

		//	print_r($datas);die;
		// $this->db->where('id_mat',$data['id_material'], false);
		// $this->db->update('m_material',$datas, false);


		$query 	= $this->db->query($sql);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_credit($data) {
		$sql 	= "
		update t_customer set sisa_credit = sisa_credit - " . $data['total_amount'] . " where id_t_cust = " . $data['id_customer'] . "
		";

		// $total = $data['sisa'] - $data['total_amount'];
		// $datas = array(
		// 'sisa_credit' => $total
		// );

		//	print_r($datas);die;
		// $this->db->where('id_t_cust',$data['id_customer'], false);
		// $this->db->update('t_customer',$datas, false);


		$query 	= $this->db->query($sql);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_credit2($data, $selling_data) {
		$sql 	= 'update t_customer set sisa_credit = sisa_credit + ' . floatval($selling_data['total_amount']) . '
		where id_t_cust = ' . $data['id_customer'];
		$query 	= $this->db->query($sql);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_retur_apr($data) {

		$datas = array(

			'status' => $data['sts']

		);

		$this->db->where('id_retur', $data['id_po']);
		$this->db->update('t_retur', $datas);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function history_retur_apr($data) {

		$sql 	= "
		INSERT INTO `history_d_penjualan`
		SELECT NULL AS jo,a.* FROM `d_penjualan` a
		JOIN  `t_detail_retur` b ON a.id_dp = b.`id_detail_mat` 
		WHERE b.id_retur  = " . $data['id_po'] . "
		
		";

		// $total = $data['sisa'] - $data['total_amount'];
		// $datas = array(
		// 'sisa_credit' => $total
		// );

		//	print_r($datas);die;
		// $this->db->where('id_t_cust',$data['id_customer'], false);
		// $this->db->update('t_customer',$datas, false);


		$query 	= $this->db->query($sql);


		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_retur($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(

			'date' => $data['date'],
			'id_penjualan' => $data['id_penjualan'],
			'keterangan' => $data['keterangan']

		);

		//	print_r($datas);die;
		$this->db->where('id_retur', $data['id_retur']);
		$this->db->update('t_retur', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_detail_penjualan($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'qty' => $data['qty_retur'],
			'qty_box' => $data['qty_box_retur'],
			'qty_satuan' => $data['qty_retur_sat'],
			'price' => $data['amount'],
			'mutasi_id' => $data['mutasi_id']
		);

		//	print_r($datas);die;
		$this->db->where('id_dp', $data['id_dp']);
		$this->db->update('d_penjualan', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	function delete_retur($id) {

		$this->db->where('id_retur', $id);
		$this->db->delete('t_retur');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $result;
	}

	function delete_detail_retur($id) {

		$this->db->where('id_retur', $id);
		$this->db->delete('t_detail_retur');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $result;
	}

	function delete_mutasi($data_q) {

		$this->db->where('id_mutasi', $data_q['mutasi_id']);
		$this->db->delete('t_mutasi');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $result;
	}
}
