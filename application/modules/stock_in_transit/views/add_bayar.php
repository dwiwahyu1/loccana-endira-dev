<style>
	.dt-body-left {
		text-align: left;
	}

	.dt-body-right {
		text-align: right;
	}

	.dt-body-center {
		text-align: center;
		vertical-align: middle;
	}

	.force-overflow {
		height: 650px;
		overflow-y: auto;
		overflow-x: auto
	}

	.scroll-overflow {
		min-height: 650px
	}

	#modal-distributor::-webkit-scrollbar-track {
		-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar {
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar-thumb {
		background-image: -webkit-gradient(linear,
				left bottom,
				left top,
				color-stop(0.44, rgb(122, 153, 217)),
				color-stop(0.72, rgb(73, 125, 189)),
				color-stop(0.86, rgb(28, 58, 148)));
	}
</style>

<div class="product-sales-area mg-tb-30">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="product-payment-inner-st">
					<ul id="myTabedu1" class="tab-review-design">
						<li class="active"><a href="#description">Add Stock in Transit</a></li>
					</ul>
					<div id="myTabContent" class="tab-content custom-product-edit">
						<div class="product-tab-list tab-pane fade active in" id="description">
							<div class="row">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="review-content-section">

										<form class="form-horizontal form-label-left" id="edit_material" role="form" action="<?php echo base_url('stock_in_transit/edit_material_by_sales'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
											<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Tanggal Transit <span class="required"><sup>*</sup></span></label>
												<div class="col-md-8 col-sm-6 col-xs-12">
													<input type="text" id="date_mutasi" name="date_mutasi" class="form-control" placeholder="" <?php
																																																											echo date('d-M-Y'); ?>" value="<?php echo date('d-M-Y'); ?>" value="<?php echo date('d-M-Y'); ?>" autocomplete="off" required>
													<!-- <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                          </div> -->
												</div>
											</div>
											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="gudang">Sales <span class="required"><sup>*</sup></span></label>
												<div class="col-md-8 col-sm-6 col-xs-12">
													<select class="form-control" name="type_mutasi" id="type_mutasi" style="width: 100%" required autocomplete="off">
														<option value="" selected='selected'>-- Pilih Sales --</option>
														<?php
														foreach ($sales as $sale) { ?>
															<option value="<?php echo $sale['id_user']; ?>"><?php echo $sale['nama']; ?></option>
														<?php } ?>
													</select>
												</div>
											</div>


											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Keterangan Transit <span class="required"></span></label>
												<div class="col-md-8 col-sm-6 col-xs-12">
													<textarea id="note" name="note" class="form-control" placeholder="Keterangan" value="" autocomplete="off" required step="0.01"></textarea>
												</div>
											</div>

											<br><br>
											<div class="row">
												<h3>Items</h3>
											</div>

											<div class="row">
												<div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">
													<div class="form-group">
														<label>Kode</label>
													</div>
												</div>
												<div class="col-lg-4 col-md-1 col-sm-3 col-xs-12">
													<div class="form-group">
														<label>Qty Box</label>
													</div>
												</div>
												<!-- <div class="col-lg-1 col-md-2 col-sm-3 col-xs-12">
									<div class="form-group">
										<label>Qty Satuan</label>
									</div>
								</div> -->
												<div class="col-lg-3 col-md-1 col-sm-3 col-xs-12">
													<div class="form-group">
														<label>Total Qty Box</label>
													</div>
												</div>

												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<div class="form-group">
														<label></label>
													</div>
												</div>
											</div>
											<input name="int_flo" id="int_flo" type="hidden" class="form-control" placeholder="Qty" value=0;>
											<div id="table_items">


											</div>

											<div class="row" style="border-top-style:solid;" id='plus'>
												<br>
												<div class="col-lg-11 col-md-11 col-sm-3 col-xs-12">
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">

													<div class="form-group">
														<button type="button" class="btn btn-danger waves-effect waves-light tbh_btn" onClick="tambah_row()">+</button>
													</div>
												</div>
											</div>
											<div class="item form-group">
												<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
												<div class="col-md-8 col-sm-6 col-xs-12">
													<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Submit</button>
													<button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()">Batal</button>
												</div>
											</div>
										</form><!-- /page content -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="PrimaryModalalert" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-close-area modal-close-df">
				<a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
			</div>
			<div class="modal-body">
				<i class="educate-icon educate-checked modal-check-pro"></i>
				<h2>Data Berhasil Dirubah</h2>
				<p></p>
			</div>
			<div class="modal-footer">
				<a data-dismiss="modal" href="#" onClick="back()">OK</a>
			</div>
		</div>
	</div>
</div>


<script>
	function clearform() {
		$('#edit_material').trigger("reset");
	}

	function back() {
		window.location.href = "<?php echo base_url() . 'stock_in_transit'; ?>";

	}


	$(document).ready(function() {
		$('#type_mutasi').select2()
		$('#date_mutasi').datepicker({
			isRTL: true,
			format: "dd-M-yyyy",
			autoclose: true,
			todayHighlight: true,
			changeYear: true,
			minDate: '-3M',
			maxDate: '+30D',
		});
		$('#edit_material').on('submit', function(e) {
			// validation code here
			//if(!valid) {
			e.preventDefault();

			var formData = new FormData(this);

			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: formData,
				cache: false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						// $('#PrimaryModalalert').modal('show');
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function() {
							back();
						})
					} else {
						$('#msg_err').html(response.message);
						$('#WarningModalftblack').modal('show');
					}
				}
			});
		});

		var datapost = {
			"id_t_cust": '1'
		};
		$.ajax({
			type: 'POST',
			url: "<?php echo base_url() . 'stock_in_transit/get_all_item'; ?>",
			data: JSON.stringify(datapost),
			cache: false,
			contentType: false,
			processData: false,
			success: function(response) {
				$('#table_items').html('')

				var htl = '';
				htl += '<div id="row_0" >';
				htl += '									<div class="row" style="border-top-style:solid;">';
				htl += '									<div style="margin-top:10px">';
				htl += '										<div class="col-lg-4 col-md-3 col-sm-3 col-xs-12">';
				htl += '												<select onChange="change_item(0)" name="kode_0" id="kode_0" class="form-control" placeholder="Nama Principal">';
				htl += '													<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
				htl += '												</select>';
				htl += '										</div>';
				htl += '										<div class="col-lg-4 col-md-1 col-sm-3 col-xs-12">';
				htl += '											<div class="form-group">';
				htl += '												<input name="qty_0" id="qty_0" type="text" "change_sum(0)" class="form-control rupiahs" placeholder="Qty" value=0  >';
				htl += '												<label id="iteml_0"></label><label id="iteml2_0"></label>';
				htl += '											</div>';
				htl += '										</div>';
				// htl += '										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
				// htl += '											<div class="form-group">';
				// htl += '												<input name="qtys_0" id="qtys_0" type="text" "change_sum(0)" class="form-control rupiahs" placeholder="Qty" value=0  >';
				// htl += '											</div>';
				// htl += '										</div>';
				htl += '											<div class="col-lg-3 col-md-1 col-sm-3 col-xs-12">';
				htl += '											<div class="form-group">';
				htl += '												<input name="totalqty_0" id="totalqty_0" type="number"  class="form-control" placeholder="Diskon" value=0 readOnly step="0.0001">';
				htl += '											</div>';
				htl += '										</div>';
				htl += '										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
				htl += '											<div class="form-group">';
				htl += '												<button type="button" class="btn btn-danger waves-effect waves-light " onClick="kurang_row(0)">-</button>';
				htl += '											</div>';
				htl += '										</div>';
				htl += '									</div>';
				htl += '									</div>';
				htl += '								</div>';

				$('#table_items').append(htl);
				$('#kode_0').html(response.list);

				$('#kode_0').select2();


				$(".rupiahs").inputFilter(function(value) {
					return /^-?\d*[,]?\d*$/.test(value);
				});

				$('.rupiah').priceFormat({
					prefix: '',
					centsSeparator: ',',
					centsLimit: 0,
					thousandsSeparator: '.'
				});
			}
		});
	});

	function tambah_row() {

		$('.hapus_btn').hide();
		var values = $('#int_flo').val();

		var new_fl = parseInt(values) + 1;

		$('#int_flo').val(new_fl);
		var datapost = {
			"id_t_cust": $('#name').val()
		};

		$.ajax({
			type: 'POST',
			url: "<?php echo base_url() . 'stock_in_transit/get_all_item'; ?>",
			data: JSON.stringify(datapost),
			cache: false,
			contentType: false,
			processData: false,
			success: function(response) {

				var htl = '';
				htl += '<div id="row_' + new_fl + '" >';
				htl += '									<div class="row" style="border-top-style:solid;">';
				htl += '									<div style="margin-top:10px">';
				htl += '										<div class="col-lg-4 col-md-3 col-sm-2 col-xs-12">';
				htl += '												<select onChange="change_item(' + new_fl + ')" name="kode_' + new_fl + '" id="kode_' + new_fl + '" class="form-control" placeholder="Nama Item">';
				htl += '													<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
				htl += '												</select>';
				htl += '										</div>';
				htl += '										<div class="col-lg-4 col-md-1 col-sm-3 col-xs-12">';
				htl += '											<div class="form-group">';
				htl += '												<input name="qty_' + new_fl + '" id="qty_' + new_fl + '" type="text" "change_sum(' + new_fl + ')" class="form-control rupiahs" placeholder="Qty" value=0  >';
				htl += '												<label id="iteml_' + new_fl + '"></label><label id="iteml2_' + new_fl + '"></label>';
				htl += '											</div>';
				htl += '										</div>';
				// htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
				// htl +=	'											<div class="form-group">';
				// htl +=	'												<label id="iteml_'+new_fl+'"></label><label id="iteml2_0"></label>';
				// htl +=	'											</div>';
				// htl +=	'										</div>';
				// htl += '										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
				// htl += '											<div class="form-group">';
				// htl += '												<input name="qtys_' + new_fl + '" id="qtys_' + new_fl + '" type="text" "change_sum(' + new_fl + ')" class="form-control rupiahs" placeholder="Qty" value=0  >';
				// htl += '											</div>';
				// htl += '										</div>';
				htl += '											<div class="col-lg-3 col-md-1 col-sm-3 col-xs-12">';
				htl += '											<div class="form-group">';
				htl += '												<input name="totalqty_' + new_fl + '" id="totalqty_' + new_fl + '" type="number"  class="form-control" placeholder="Diskon" value=0 readOnly  step="0.0001">';
				htl += '											</div>';
				htl += '										</div>';
				htl += '										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
				htl += '											<div class="form-group">';
				htl += '												<button type="button" class="btn btn-danger waves-effect waves-light " onClick="kurang_row(' + new_fl + ')">-</button>';
				htl += '											</div>';
				htl += '										</div>';
				htl += '									</div>';
				htl += '									</div>';
				htl += '								</div>';

				$('#table_items').append(htl);

				$('#kode_' + new_fl + '').html(response.list);

				$('#kode_' + new_fl).select2();
				//$("kode_'+new_fl).css("background-color", "yellow");

				$(".rupiahs").inputFilter(function(value) {
					return /^-?\d*[,]?\d*$/.test(value);
				});

				$('.rupiah').priceFormat({
					prefix: '',
					centsSeparator: ',',
					centsLimit: 0,
					thousandsSeparator: '.'
				});

			}
		});

	}

	function kurang_row(new_fl) {
		$('#row_' + new_fl).remove();
		console.log(new_fl)
	}

	function change_item(rs) {
		var val = $('#kode_' + rs + '').val();

		var sss = val.split('|');

		//console.log(sss);
		//alert(val);

		var qty_s = new Intl.NumberFormat('de-DE', {}).format(sss[3]);
		var box_pm = new Intl.NumberFormat('de-DE', {}).format(sss[1]);
		var stock = new Intl.NumberFormat('de-DE', {}).format(sss[5]);

		var sisa = stock;
		//var sisa = qty_s; 	

		// $('#iteml_' + rs + '').html("Box @ " + sss[1] + "<br>Stock :" + sisa);
		$('#totalqty_' + rs + '').val(sss[3]);
		//$('#harga_'+rs+'').val(harga);

		// change_sum(rs);


	}
</script>