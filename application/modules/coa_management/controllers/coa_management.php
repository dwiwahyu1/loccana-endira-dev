<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Coa_Management extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('coa_management/uom_model');
    $this->load->library('log_activity');
    $this->load->library('priv');
  }

  /**
   * anti sql injection
   */
  public function Anti_sql_injection($string)
  {
    $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
    return $string;
  }

  public function index()
  {
    $priv = $this->priv->get_priv();

    //print_r($priv);die;

    $data = array(
      'priv' => $priv
    );

    $this->template->load('maintemplate', 'coa_management/views/index', $data);
  }

  function lists()
  {

    $draw = ($this->input->get_post('draw') != FALSE) ? $this->input->get_post('draw') : 1;
    $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
    $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
    $order = $this->input->get_post('order');
    $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
    $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

    $order_fields = array('', 'uom_name', 'uom_symbol');

    $search = $this->input->get_post('search');

    $search_val = (!empty($search['value'])) ? $search['value'] : null;

    $search_value = $this->Anti_sql_injection($search_val);

    // Build params for calling model
    $params['limit'] = (int) $length;
    $params['offset'] = (int) $start;
    $params['order_column'] = $order_fields[$order_column];
    $params['order_dir'] = $order_dir;
    $params['filter'] = $search_value;
    $params['searchtxt']  = $_GET['search']['value'];


    //print_r($params);die;

    $list = $this->uom_model->lists($params);
    //print_r($list);die;

    $result["recordsTotal"] = $list['total'];
    $result["recordsFiltered"] = $list['total_filtered'];
    $result["draw"] = $draw;

    //print_r($result);die;
    $priv = $this->priv->get_priv();

    $data = array();
    $i = $params['offset'];
    $username = $this->session->userdata['logged_in']['username'];
    foreach ($list['data'] as $k => $v) {
      $i = $i + 1;


      $status_akses = '
          <div class="btn-group" style="display:' . $priv['update'] . '"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edituom(\'' . $v['id_coa'] . '\')"><i class="fa fa-edit"></i></button></div>
				  <div class="btn-group" style="display:' . $priv['delete'] . '"><button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteuom(\'' . $v['id_coa'] . '\')"><i class="fa fa-trash"></i></button></div>
          ';
      if($v['sts_show']==1)
        $status_akses=$status_akses.'<div class="btn-group" style="display:' . $priv['delete'] . '"><button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Show" "><i class="fa fa-search">Unhide</i></button></div>';
      else
      $status_akses=$status_akses.'<div class="btn-group" style="display:' . $priv['delete'] . '"><button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Show" "><i class="fa fa-search">Hide</i></button></div>';

      array_push($data, array(
        $i,
        $v['coa_parent'] . "-" . $v['ket_parent'],
        $v['coa'],
        $v['keterangan'],
        $status_akses
      ));
    }

    $result["data"] = $data;

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function add()
  {
    //$result = $this->uom_model->group();


    $coa = $this->uom_model->get_cash_acc();
    // $this->load->view('add_modal_view', $data);

    // $result = $this->distributor_model->location();

    $data = array(
      'list_coa' => $coa
    );

    $this->template->load('maintemplate', 'coa_management/views/add_modal_view', $data);
  }

  public function edit()
  {
    //$id = $this->Anti_sql_injection($this->input->post('sid', TRUE));
    $data = array(
      'id' => $this->Anti_sql_injection($this->input->post('iduom', TRUE)),
    );
    $result = $this->uom_model->get_uom_data($this->Anti_sql_injection($data['id']));


    // var_dump($result);
    // die;

    $coa = $this->uom_model->get_cash_acc();

    //print_r($result);die;
    // $roles = $this->uom_model->roles($id);

    $data = array(
      '_coa' => $result,
      'list_coa' => $coa,
    );

    $this->template->load('maintemplate', 'coa_management/views/edit_modal_view', $data);
  }

  public function deletes()
  {

    $data   = file_get_contents("php://input");
    $params   = json_decode($data, true);

    $list = $this->uom_model->deletes($this->Anti_sql_injection($params['id']));

    $res = array(
      'status' => 'success',
      'message' => 'Data telah di hapus'
    );

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }

  public function edit_uom()
  {
    $this->form_validation->set_rules('uom', 'Nama COA', 'trim');
    $this->form_validation->set_rules('coa', 'Parent COA', 'trim');
    $this->form_validation->set_rules('uom_ket', 'Keterangan COA', 'trim');
    $this->form_validation->set_rules('sts_show', 'Status Show COA', 'required');
    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));
      $result = array(
        'success' => false,
        'message' => $msg
      );
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      // die;
      $message = "";

      if ($this->Anti_sql_injection($this->input->post('coa', TRUE)) == 's') {
        $pcoa = '';
      } else {
        $pcoa = $this->Anti_sql_injection($this->input->post('coa', TRUE));
      }


      $data = array(
        'id_uom' => $this->Anti_sql_injection($this->input->post('id_uom', TRUE)),
        'nama_uom' =>  $this->Anti_sql_injection($this->input->post('uom', TRUE)),
        'coa' => $pcoa,
        'keterangan' => $this->Anti_sql_injection($this->input->post('uom_ket', TRUE)),
        'sts_show' => $this->Anti_sql_injection($this->input->post('sts_show', TRUE))
      );
      $result = $this->uom_model->edit_uom($data);
      if ($result > 0) {
        $msg = 'Berhasil merubah COA.';

        $result = array(
          'success' => true,
          'message' => $msg
        );
      } else {
        $msg = 'Gagal merubah COA.';

        $result = array(
          'success' => false,
          'message' => $msg
        );
      }

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
  }

  public function add_uom()
  {
    $this->form_validation->set_rules('uom', 'Nama COA', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('coa', 'Parent COA', 'trim|required');
    $this->form_validation->set_rules('uom_ket', 'Keterangan COA', 'trim|required|max_length[255]');
    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));
      $result = array(
        'success' => false,
        'message' => $msg
      );

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {

      if ($this->Anti_sql_injection($this->input->post('coa', TRUE)) == 's') {
        $pcoa = '';
      } else {
        $pcoa = $this->Anti_sql_injection($this->input->post('coa', TRUE));
      }

      $message = "";
      $data = array(
        'user_id' => $this->session->userdata['logged_in']['user_id'],
        'nama_uom'     => $this->Anti_sql_injection($this->input->post('uom', TRUE)),
        'coa' => $pcoa,
        'keterangan' => $this->Anti_sql_injection($this->input->post('uom_ket', TRUE))
      );

      $result = $this->uom_model->add_uom($data);

      if ($result > 0) {
        $msg = 'Berhasil menambahkan COA.';

        $result = array(
          'success' => true,
          'message' => $msg
        );
      } else {
        $msg = 'Gagal menambahkan COA ke database.';
        $result = array(
          'success' => false,
          'message' => $msg
        );
      }
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
  }
}
