<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Uom_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
    
	// function get_report_keu($params){
		
		// $sql 	= "
		// SELECT a.* ,coa,keterangan,
		// IF(masuk IS  NULL,0,masuk) masuk,
		// IF(keluar IS  NULL,0,keluar) keluar,
		// IF(masuk IS  NULL,0,masuk)-IF(keluar IS  NULL,0,keluar) AS nilai
		// FROM t_report_keu a
		// LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS masuk FROM t_coa_value b 
		// WHERE b.`type_cash`=0 AND b.date  <= '".$params['end_time']."' GROUP BY b.`id_coa`)b ON a.id_coa=b.id_coa
		// LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS keluar FROM t_coa_value b 
		// WHERE b.`type_cash`=1 AND b.date <= '".$params['end_time']."' GROUP BY b.`id_coa`)c ON a.id_coa=c.id_coa
		// JOIN t_coa t ON a.id_coa=t.id_coa
		// WHERE a.`nama_report`='".$params['report']."'
		// ORDER BY a.`seq`
		
		// ";
		
		// $out = array();
		// $query 	=  $this->db->query($sql);

		// $result = $query->result_array();

		// $this->db->close();
		// $this->db->initialize();

		// return $result;
		
		
	// }		
	
	function get_report_keu($params){
		
		$start_time = explode('-',$params['end_time']);
		
		$sql 	= "
			SELECT A.coa,A.keterangan,A.`id_report`,A.`nama_report`,A.`id_coa`,A.`seq`,A.`type_data`,A.`summ_type`,A.`summ_type_detail`,IF(B.nilai IS NULL,A.nilai,B.nilai) AS nilai FROM (
				SELECT a.* ,coa,keterangan,
				IF(masuk IS  NULL,0,masuk) masuk,
				IF(keluar IS  NULL,0,keluar) keluar,
				IF(masuk IS  NULL,0,masuk)-IF(keluar IS  NULL,0,keluar) AS nilai
				FROM t_report_keu a
				LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS masuk FROM t_coa_value b 
				WHERE b.`type_cash`=0 AND b.date between '".$params['start_time']."' and '".$params['end_time']."' GROUP BY b.`id_coa`)b ON a.id_coa=b.id_coa
				LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS keluar FROM t_coa_value b 
				WHERE b.`type_cash`=1 AND b.date between '".$params['start_time']."' and  '".$params['end_time']."' GROUP BY b.`id_coa`)c ON a.id_coa=c.id_coa
				JOIN t_coa t ON a.id_coa=t.id_coa
				WHERE a.`nama_report`='laba_rugi'
				ORDER BY a.`seq`
			) A LEFT JOIN (
				SELECT a.* ,coa,'Persediaan Awal' keterangan,
				IF(masuk IS  NULL,0,masuk) masuk,
				IF(keluar IS  NULL,0,keluar) keluar,
				IF(masuk IS  NULL,0,masuk)-IF(keluar IS  NULL,0,keluar) AS nilai
				FROM t_report_keu a
				LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS masuk FROM t_coa_value b 
				WHERE b.`type_cash`=0 AND b.date between '".$params['start_time']."' and '".$params['end_time']."' GROUP BY b.`id_coa`)b ON a.id_coa=b.id_coa
				LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS keluar FROM t_coa_value b 
				WHERE b.`type_cash`=1 AND b.date between '".$params['start_time']."' and '".$params['end_time']."' GROUP BY b.`id_coa`)c ON a.id_coa=c.id_coa
				JOIN t_coa t ON a.id_coa=t.id_coa
				WHERE a.id_coa = 33
				ORDER BY a.`seq`
				AND a.nama_report <> 'aktiva_saldo'
			) B ON A.keterangan = B.keterangan
		
		";
		
		//echo $sql;die;
		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
		
		
	}	
	
		function get_piutang($params){
		
		$sql 	= "
		
		SELECT k.*,SUM(price_fulls) AS price_full, SUM(invoice_saldos) AS invoice_saldo,SUM(dpps) dpp, SUM(invoice_saldos) - bayar AS piutang 
		FROM (
		SELECT c.`no_invoice`,a.`tanggal_invoice`,a.`due_date`,`stock_code`,`stock_name`,`base_qty`,`uom_symbol`,CONCAT(stock_name,' ',base_qty,'',uom_symbol) AS items,
		b.`unit_price`,b.`qty`,b.`qty_box`,b.`qty_satuan`,b.`box_ammount`,b.`diskon`,
		b.price-(b.price*(b.diskon/100)) AS price_fulls,
		b.price-(b.price*(b.diskon/100)) AS invoice_saldos,
		b.price-(b.price*(b.diskon/100)) - ((b.price-(b.price*(b.diskon/100)))/(11)) AS dpps,
		((b.price-(b.price*(b.diskon/100)))/(11)) AS ppn,IF(`term_of_payment` = 'cash',0,`term_of_payment`) topys,
		DATEDIFF(NOW(),a.`tanggal_invoice`) umur, `cust_name`,`id_t_region`,h.`region`
		 FROM `t_invoice_penjualan` a 
		JOIN `d_invoice_penjualan` b ON a.`id_invoice` = b.`id_inv_penjualan`
		JOIN `t_invoice_penjualan` c ON a.`id_penjualan` = c.`id_penjualan` 
		JOIN d_penjualan d ON b.`id_d_penjualan` = d.`id_dp`
		JOIN `t_penjualan` jj ON a.`id_penjualan` = jj.`id_penjualan`
		JOIN `t_customer` g ON jj.id_customer = g.`id_t_cust`
		JOIN t_region h ON h.`id_t_region` = g.`region`
		JOIN `m_material` e ON d.id_material = e.`id_mat`
		JOIN `m_uom` f ON f.`id_uom` = e.`unit_terkecil`
		WHERE a.tanggal_invoice between '".$params['start_time']."' AND '".$params['end_time']."'
		) k,
		(
				SELECT a.date AS tgl_bayar,b.id_invoice inv_bayar,SUM(b.ammount) bayar FROM `t_payment_hp` a
				JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp` WHERE a.`payment_type` = 1 AND
				a.`date` between '".$params['start_time']."' AND '".$params['end_time']."'
				#GROUP BY b.id_invoice
		) jk 
		
		
		";
		
		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
		
		
	}
			
			
	function get_penjualan($params){
		
		$sql 	= "
		
		
		SELECT SUM(pestisida) AS pestisida, SUM(non_pestisida) AS non_pestisida  FROM (
		SELECT 
		IF(type_material_name ='Pestisida',(b.price-(b.`price`/11)),0) AS pestisida,
		IF(type_material_name ='Non Pestisida',(b.price-(b.`price`/11)),0) AS non_pestisida,
		g.`region`,
				a.no_penjualan AS no_invoice,
				e.tanggal_invoice,
				f.`code_cust`,
				f.`cust_name`,
				d.uom_symbol,
				b.`unit_price`-(b.`unit_price`/11) AS harga_jual_perkemasan,
				(b.price-(b.`price`/11))+(b.`price`/11) AS tot_ppn,
				b.price-(b.`price`/11) total,
				b.`price`/11 AS ppn,
				c.`stock_code` AS Kode_produk, 
				c.`stock_name` AS Produk,
				CONCAT(c.base_qty,' ',d.uom_name) AS kemasan,
				h.`name_eksternal`,
				f.`city`,
				i.harga_pokok,
				b.qty AS Sum_of_pcs,
				b.qty*(c.base_qty/convertion) AS Sum_of_ltkg,
				a.total_amount-(a.total_amount/11) AS Sum_of_total,
				a.total_amount/11 AS Sum_of_ppn,
				a.total_amount AS Sum_of_TotPPN,
				cc.`type_material_name`,
				i.harga_pokok/(dd.convertion/c.base_qty)AS HargaKemasan,
				AVG(b.unit_price*b.qty)/b.qty*(convertion/c.base_qty)AS Average_Hargalt,
				(b.`unit_price`-(b.`unit_price`/11))-(i.harga_pokok/(dd.convertion/c.base_qty)) AS laba_rugi_kemasan,
				((b.`unit_price`-(b.`unit_price`/11))-(i.harga_pokok/(dd.convertion/c.base_qty)))*b.qty AS laba_rugi_produk,
				(b.`unit_price`-(b.`unit_price`/11))*(dd.convertion/c.base_qty) AS harga_jual_produk,
				(((b.`unit_price`-(b.`unit_price`/11))-(i.harga_pokok/(dd.convertion/c.base_qty)))/(b.`unit_price`-(b.`unit_price`/11)))*100 AS persen,
				 i.harga_pokok*(b.qty*(c.base_qty/convertion)) AS hpp,
				WEEK(a.date_penjualan) AS minggu
				FROM t_penjualan a
				JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
				JOIN m_material c ON b.`id_material`=c.`id_mat`
				JOIN m_type_material cc ON c.type=cc.`id_type_material`
				JOIN m_uom d ON c.unit_terkecil=d.id_uom
				JOIN t_uom_convert dd ON d.`id_uom`=dd.`id_uom`
				JOIN t_invoice_penjualan e ON a.`id_penjualan`=e.`id_penjualan`
				JOIN t_customer f ON f.`id_t_cust`=a.`id_customer`
				JOIN t_region g ON g.`id_t_region`=f.`region`
				JOIN t_eksternal h ON c.`dist_id`=h.id
				JOIN (SELECT  a.`id_material`,SUM(a.`buy_price`*a.`qty_big`)/SUM(a.`qty_big`) harga_pokok FROM t_detail_material a
				WHERE a.`date_insert` < '".$params['end_time']."'
				GROUP BY a.`id_material`)i ON c.id_mat=i.id_material
				WHERE `tanggal_invoice` BETWEEN '".$params['start_time']."' AND '".$params['end_time']."'
				GROUP BY b.`id_dp`
		
		) A 
		
		
		";
		
		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
		
		
	}
	
	function get_persediaan($params){
		
		$s_date = explode('-',$params['end_time']);
		
		//$start_date = $s_date[0].'-01-01';
		$start_date = $params['start_time'];
		
		$sql 	= "
		
		SELECT SUM(nilai_akhir) persediaans,sum(saldo_akhir*harga_pokok) persediaan,SUM(nilai) persediaan_awal, sum(nilai_pemb) pembelian FROM (

		SELECT A.*,(A.kuantiti - IF(E.penjualan_old IS NULL,0,E.penjualan_old)) AS saldo_awals,
		IF(B.kuantiti_sol IS NULL,0,B.kuantiti_sol) pembelian, 
		IF(kuantiti_sol IS NULL,0,kuantiti_sol) kuantiti_sol ,
		IF(kuantiti_titip IS NULL,0,kuantiti_titip) kuantiti_titip ,
		IF(kuantiti_bonus IS NULL,0,kuantiti_bonus) kuantiti_bonus,
		IF(kuantiti_diskon IS NULL,0,kuantiti_diskon) kuantiti_diskon,
		IF(B.harga_satuan IS NULL,0,B.harga_satuan) AS harga_sat_pemb,
		IF(B.nilai IS NULL,0,B.nilai) AS nilai_pemb,
		'' AS ket, 
		(A.nilai+IF(B.nilai IS NULL,0,B.nilai))/(A.kuantiti+IF(B.kuantiti IS NULL,0,B.kuantiti)+IF(kuantiti_sol IS NULL,0,kuantiti_sol)) harga_pokok_old,
		(IF(A.nilai IS NULL,0,A.nilai)+IF(B.nilai IS NULL,0,B.nilai)) / (IF(A.kuantiti IS NULL,0,A.kuantiti)+IF(B.kuantiti IS NULL,0,B.kuantiti)) harga_pokok,

		IF(C.penjualan IS NULL,0,C.penjualan) AS penjualan,
		IF(D.pengeluaran IS NULL,0,D.pengeluaran) AS pengeluaran,
		((A.kuantiti - IF(E.penjualan_old IS NULL,0,E.penjualan_old))+IF(kuantiti_sol IS NULL,0,kuantiti_sol)+IF(kuantiti_titip IS NULL,0,kuantiti_titip)+IF(kuantiti_bonus IS NULL,0,kuantiti_bonus)+IF(kuantiti_diskon IS NULL,0,kuantiti_diskon))-
		(IF(C.penjualan IS NULL,0,C.penjualan)+IF(D.pengeluaran IS NULL,0,D.pengeluaran)) AS saldo_akhir,
		((A.kuantiti+IF(B.kuantiti IS NULL,0,B.kuantiti)+IF(kuantiti_sol IS NULL,0,kuantiti_sol)+IF(kuantiti_titip IS NULL,0,kuantiti_titip)+IF(kuantiti_bonus IS NULL,0,kuantiti_bonus))-
		(IF(C.penjualan IS NULL,0,C.penjualan)+IF(D.pengeluaran IS NULL,0,D.pengeluaran)) )*
		( (A.nilai+IF(B.nilai IS NULL,0,B.nilai))/(A.kuantiti+IF(B.kuantiti IS NULL,0,B.kuantiti)+IF(kuantiti_sol IS NULL,0,kuantiti_sol)) ) AS nilai_akhir_n,
		(IF(A.nilai IS NULL,0,A.nilai)+ IF(B.nilai IS NULL,0,B.nilai)) AS nilai_akhir_n2, 
		((A.kuantiti+IF(kuantiti_sol IS NULL,0,kuantiti_sol)+IF(kuantiti_titip IS NULL,0,kuantiti_titip)+IF(kuantiti_bonus IS NULL,0,kuantiti_bonus)+IF(kuantiti_diskon IS NULL,0,kuantiti_diskon))-
		(IF(C.penjualan IS NULL,0,C.penjualan)+IF(D.pengeluaran IS NULL,0,D.pengeluaran)))*((IF(A.nilai IS NULL,0,A.nilai)+IF(B.nilai IS NULL,0,B.nilai)) / (IF(A.kuantiti IS NULL,0,A.kuantiti)+IF(B.kuantiti IS NULL,0,B.kuantiti))) AS nilai_akhir


		FROM (
			SELECT a.`id_mat`,stock_code, stock_name,base_qty,`uom_symbol`,`unit_box`,g.name_eksternal,
			IF(SUM(c.qty_big) IS NULL,0,SUM(c.qty_big)) AS kuantiti_old ,
			SUM(c.qty_sol_big+c.qty_diskon_big) AS kuantiti ,
			IF(SUM(c.`qty_big`*c.`buy_price`)/SUM(c.qty_big) IS NULL,0,SUM(c.`qty_big`*c.`buy_price`)/SUM(c.qty_big) ) AS harga_satuan_old , 
			(SUM(c.`qty_sol_big`*(c.buy_price - (c.`buy_price`*(c.diskon/100))))+SUM(c.`qty_diskon_big`*(c.buy_price - (c.`buy_price`*(c.diskon/100)))))/(SUM(c.qty_sol_big)+SUM(c.qty_diskon_big)) harga_satuan , 
			IF(SUM(c.`qty_big`*c.`buy_price`) IS NULL,0,SUM(c.`qty_big`*c.`buy_price`) ) nilai_old, 
			((SUM(c.`qty_sol_big`*(c.`buy_price`-(c.`buy_price`*(c.diskon/100))))+SUM(c.`qty_diskon_big`*(c.`buy_price`-(c.`buy_price`*(c.diskon/100)))))/(SUM(c.qty_sol_big)+SUM(c.qty_diskon_big)))*(SUM(c.`qty_sol_big`)+SUM(c.`qty_diskon_big`)) AS nilai
			FROM m_material a
			JOIN (
				SELECT c.*,IF(d.diskon IS NULL,0,d.diskon) diskon FROM `t_detail_material` c 
				LEFT JOIN t_bpb_detail b ON c.id_bpb_detail=b.id_bpb_detail
				LEFT JOIN t_po_mat d ON b.id_po_mat=d.id_t_ps WHERE c.`date_insert` < '".$start_date." 00:00:00'
			) c ON a.`id_mat` = c.`id_material`
			JOIN t_eksternal g ON a.dist_id = g.id
			JOIN `m_uom` b ON a.unit_terkecil = b.`id_uom`
			
			GROUP BY a.`id_mat`
			ORDER BY a.`stock_code`
		) A LEFT JOIN (
			SELECT a.`id_mat`,stock_code, stock_name,base_qty,`uom_symbol`,`unit_box`,
			IF(SUM(c.qty_big) IS NULL,0,SUM(c.qty_big)) AS kuantiti_old ,
			SUM(c.`qty_sol_big`)+SUM(c.`qty_diskon_big`) AS kuantiti ,
			IF(SUM(c.`qty_sol_big`) IS NULL,0,SUM(c.qty_sol_big)) AS kuantiti_sol ,
			IF(SUM(c.`qty_bonus_big`) IS NULL,0,SUM(c.qty_bonus_big)) AS kuantiti_bonus ,
			IF(SUM(c.`qty_titip_big`) IS NULL,0,SUM(c.qty_titip_big)) AS kuantiti_titip ,
			IF(SUM(c.`qty_diskon_big`) IS NULL,0,SUM(c.qty_diskon_big)) AS kuantiti_diskon ,
			IF(SUM(c.`qty_big`*(c.`buy_price`-(c.`buy_price`*(M.diskon/100))))/SUM(c.qty_big) IS NULL,0,SUM(c.`qty_big`*(c.`buy_price`-(c.`buy_price`*(M.diskon/100))))/SUM(c.qty_big) ) AS harga_satuan_old , 
			(SUM(c.`qty_sol_big`*(c.`buy_price`-(c.`buy_price`*(M.diskon/100)))))/(SUM(c.qty_sol_big)+SUM(c.qty_diskon_big)) harga_satuan,
			IF(SUM(c.`qty_sol_big`*(c.`buy_price`-(c.`buy_price`*(M.diskon/100)))) IS NULL,0,SUM(c.`qty_sol_big`*(c.`buy_price`-(c.`buy_price`*(M.diskon/100)))) ) nilai,
			(SUM(c.qty_sol_big*c.buy_price)) + (SUM(c.qty_bonus_big*c.buy_price)) AS nilai_n
			FROM m_material a
			LEFT JOIN `t_detail_material` c ON a.`id_mat` = c.`id_material`
			JOIN t_bpb_detail J ON c.id_bpb_detail = J.id_bpb_detail
			JOIN t_po_mat M ON J.id_po_mat = M.id_t_ps
			JOIN `m_uom` b ON a.unit_terkecil = b.`id_uom`
			AND `date_insert` BETWEEN '".$start_date." 00:00:00' AND '".$params['end_time']." 23:59:59'
			GROUP BY a.`id_mat`
			ORDER BY a.`stock_code`
		) B ON A.id_mat = B.id_mat
		LEFT JOIN (
			
			SELECT *, SUM(lt_kg*price_per_liter)/SUM(lt_kg) AS unit_price_penjualan,id_material AS id_stock_awal,SUM(lt_kg) AS penjualan FROM (
			SELECT b.*,b.qty/(convertion/base_qty) AS lt_kg, unit_price * (convertion/base_qty) price_per_liter FROM t_penjualan a
			JOIN d_penjualan b ON a.id_penjualan=b.id_penjualan
			JOIN m_material c ON b.id_material=c.id_mat
			JOIN m_uom d ON c.unit_terkecil=d.id_uom
			JOIN t_uom_convert e ON d.id_uom=e.id_uom
			WHERE a.date_penjualan BETWEEN '".$start_date."' AND '".$params['end_time']."' 

			) a GROUP BY id_material
			

		) C ON A.id_mat = C.id_stock_awal
		LEFT JOIN (

			SELECT `id_stock_awal`,SUM(`amount_mutasi`) AS pengeluaran FROM `t_mutasi`
			WHERE `date_mutasi` BETWEEN '2021-01-01' AND '".$params['end_time']."'
			AND `type_mutasi` = 3
			GROUP BY `id_stock_awal`

		) D ON A.id_mat = D.id_stock_awal
		LEFT JOIN (
			
			SELECT *, SUM(lt_kg*price_per_liter)/SUM(lt_kg) AS unit_price_penjualan,id_material AS id_stock_awal,SUM(lt_kg) AS penjualan_old FROM (
			SELECT b.*,b.qty/(convertion/base_qty) AS lt_kg, unit_price * (convertion/base_qty) price_per_liter FROM t_penjualan a
			JOIN d_penjualan b ON a.id_penjualan=b.id_penjualan
			JOIN m_material c ON b.id_material=c.id_mat
			JOIN m_uom d ON c.unit_terkecil=d.id_uom
			JOIN t_uom_convert e ON d.id_uom=e.id_uom
			WHERE a.date_penjualan < '".$start_date."' AND mutasi_id IS NOT NULL

			) a GROUP BY id_material
			

		) E ON A.id_mat = E.id_stock_awal

		)A
		
		
		";
		
		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
		
		
	}
	
	function get_hutang($params){
		
		$s_date = explode('-',$params['end_time']);
		
		//$start_date = $s_date[0].'-01-01';
		$start_date = $params['start_time'];
		
		$sql 	= "
		
		SELECT SUM(prices)-SUM(bayar) hutang,SUM(bayar) bayar,SUM(prices) prices FROM (
SELECT *, SUM(pricesa) prices, 
			SUM(invoice_saldoa) invoice_saldo ,SUM(saldoa) saldo, SUM( bulan_berjalana) bulan_berjalan FROM (
			SELECT  bpb.tanggal,h.id_invoice,name_eksternal,date_po,CONCAT(stock_name,' ',base_qty,' ',uom_symbol) items,no_po,DATE_ADD(date_po, INTERVAL `term_of_payment` DAY ) jatuh_tempo, 
			DATEDIFF(NOW(),date_po) umur,`no_invoice`, ((IF(f.qty IS NULL,f.qty,f.qty))*b.unit_price) AS pricesa ,
			IF(a.seq_n = 8,0,c.pajak) pajak,IF(g.qty IS NULL,b.diskon,g.diskon) diskons,
			IF(bayar IS NULL,0,bayar) AS bayar,tgl_bayar,IF(no_invoice IS NULL,0,((IF(f.qty IS NULL,b.qty,f.qty))*b.unit_price)) AS invoice_saldoa,
			(IF(date_po < '".$start_date ."',(IF(f.qty IS NULL,b.qty,f.qty)*b.unit_price),0 )) saldoa, 
			(IF(date_po >= '".$start_date ."',(IF(f.qty IS NULL,b.qty,f.qty)*b.unit_price),0 )) bulan_berjalana
			FROM `t_purchase_order` a
			JOIN `t_po_mat` b ON a.`id_po` = b.`id_po`
			JOIN m_material c ON b.id_material = c.id_mat
			JOIN m_uom d ON c.unit_terkecil = d.id_uom
			JOIN `t_eksternal` e ON a.id_distributor = e.`id`
			JOIN `t_bpb_detail` f ON b.id_t_ps = f.id_po_mat
			JOIN t_bpb bpb ON f.id_bpb=bpb.id_bpb
			LEFT JOIN `d_invoice_pembelian` g ON f.`id_bpb_detail` = g.`id_bpb_detail`
			LEFT JOIN `t_invoice_pembelian` h ON g.`id_inv_pembelian` = h.`id_invoice`
			LEFT JOIN
			(
				SELECT a.date AS tgl_bayar,b.id_invoice,SUM(b.ammount) bayar FROM `t_payment_hp` a
				JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp` WHERE a.`payment_type` = 0 
				AND a.`date` < '".$params['end_time']."'
				GROUP BY b.id_invoice
			) j ON h.id_invoice = j.id_invoice
			WHERE  bpb.tanggal <= '".$params['end_time']."'
			#GROUP BY h.id_invoice
			ORDER BY tanggal,no_po,no_invoice
			
			) A GROUP BY id_invoice,no_po
			ORDER BY tanggal,no_po,no_invoice
			
			
			) A
		
		
		";
		
		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
		
		
	}
	
	// function get_report_keu_summ($params){
		
		// $sql 	= "
			// SELECT SUM(IF(summ_type_detail = 0,nilai,0)) plus, SUM(IF(summ_type_detail = 1,nilai,0)) minus,SUM(IF(summ_type_detail = 0,nilai,0)) - SUM(IF(summ_type_detail = 1,nilai,0)) AS nilai, `summ_type` FROM (

			// SELECT a.* ,coa,keterangan,
			// IF(masuk IS  NULL,0,masuk) masuk,
			// IF(keluar IS  NULL,0,keluar) keluar,
			// IF(masuk IS  NULL,0,masuk)-IF(keluar IS  NULL,0,keluar) AS nilai
			// FROM t_report_keu a
			// LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS masuk FROM t_coa_value b 
			// WHERE b.`type_cash`=0 AND b.date <= '".$params['end_time']."' GROUP BY b.`id_coa`)b ON a.id_coa=b.id_coa
			// LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS keluar FROM t_coa_value b 
			// WHERE b.`type_cash`=1 AND b.date <= '".$params['end_time']."' GROUP BY b.`id_coa`)c ON a.id_coa=c.id_coa
			// JOIN t_coa t ON a.id_coa=t.id_coa
			// WHERE a.`nama_report`='".$params['report']."'
			// ORDER BY a.`seq`
			
			
	// ) OP
 // GROUP BY `summ_type`
 // ORDER BY SEQ
// ";
		
		// // echo $sql;die;
		// $out = array();
		// $query 	=  $this->db->query($sql);

		// $result = $query->result_array();

		// $this->db->close();
		// $this->db->initialize();

		// return $result;
		
		
	// }	
	
	function get_report_keu_summ($params){
		
		$start_time = explode('-',$params['end_time']);
		
		$sql 	= "
			SELECT SUM(IF(summ_type_detail = 0,nilai,0)) plus, SUM(IF(summ_type_detail = 1,nilai,0)) minus,SUM(IF(summ_type_detail = 0,nilai,0)) - SUM(IF(summ_type_detail = 1,nilai,0)) AS nilai, `summ_type` FROM (

			SELECT A.coa,A.keterangan,A.`id_report`,A.`nama_report`,A.`id_coa`,A.`seq`,A.`type_data`,A.`summ_type`,A.`summ_type_detail`,IF(B.nilai IS NULL,A.nilai,B.nilai) AS nilai FROM (
				SELECT a.* ,coa,keterangan,
				IF(masuk IS  NULL,0,masuk) masuk,
				IF(keluar IS  NULL,0,keluar) keluar,
				IF(masuk IS  NULL,0,masuk)-IF(keluar IS  NULL,0,keluar) AS nilai
				FROM t_report_keu a
				LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS masuk FROM t_coa_value b 
				WHERE b.`type_cash`=0 AND b.date between '".$params['start_time']."' and '".$params['end_time']."' GROUP BY b.`id_coa`)b ON a.id_coa=b.id_coa
				LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS keluar FROM t_coa_value b 
				WHERE b.`type_cash`=1 AND b.date between '".$params['start_time']."' and '".$params['end_time']."' GROUP BY b.`id_coa`)c ON a.id_coa=c.id_coa
				JOIN t_coa t ON a.id_coa=t.id_coa
				WHERE a.`nama_report`='".$params['report']."'
				ORDER BY a.`seq`
			) A LEFT JOIN (
				SELECT a.* ,coa,'Persediaan Awal' keterangan,
				IF(masuk IS  NULL,0,masuk) masuk,
				IF(keluar IS  NULL,0,keluar) keluar,
				IF(masuk IS  NULL,0,masuk)-IF(keluar IS  NULL,0,keluar) AS nilai
				FROM t_report_keu a
				LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS masuk FROM t_coa_value b 
				WHERE b.`type_cash`=0 AND b.date between '".$params['start_time']."' and '".$params['end_time']."' GROUP BY b.`id_coa`)b ON a.id_coa=b.id_coa
				LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS keluar FROM t_coa_value b 
				WHERE b.`type_cash`=1 AND b.date between '".$params['start_time']."' and '".$params['end_time']."' GROUP BY b.`id_coa`)c ON a.id_coa=c.id_coa
				JOIN t_coa t ON a.id_coa=t.id_coa
				WHERE a.id_coa = 33
				ORDER BY a.`seq`
			) B ON A.keterangan = B.keterangan
			
			
	) OP
 GROUP BY `summ_type`
 ORDER BY SEQ
";
		
		// echo $sql;die;
		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
		
		
	}
	
    public function get_uom_data($id)
	{
		//$sql 	= 'CALL uom_search_id(?)';
		$sql 	= 'select a.*,b.coa,b.keterangan FROM t_coa_value a join t_coa b on a.id_coa = b.id_coa
		where a.id = ?';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$id
			));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}   

	public function sales_perbulan($year,$month)
	{
		//$sql 	= 'CALL uom_search_id(?)';
		$sql 	= "
			SELECT SUM(b.`price`) AS sumtotppn,SUM(b.`price`)/11 AS sumppn,SUM(b.`price`)- (SUM(b.`price`)/11) AS sumtot,d.`type_material_name`,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan
	FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_type_material d ON c.`type`=d.`id_type_material`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=?
	AND DATE_FORMAT(a.`date_penjualan`,'%c')=?
	GROUP BY c.`type`,DATE_FORMAT(a.`date_penjualan`,'%m')
	ORDER BY DATE_FORMAT(a.`date_penjualan`,'%m'),c.`type`
		";

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$year,$month
			));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function sales_perproduct($year,$month)
	{
		//$sql 	= 'CALL uom_search_id(?)';
		
		if($month == 'All'){
		$sql 	= "
			SELECT e.`name_eksternal` AS Principle,c.`stock_name` AS Produk,CONCAT(c.`base_qty`,' ',d.`uom_symbol`) Kemasan,
	(SUM(b.`unit_price`)*(f.`convertion`/c.`unit_terkecil`))/f.`convertion` sumoftotal_lt_kg,SUM(b.`price`) totalsumoftotal,
	DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,c.`unit_terkecil`/f.`convertion` AS konversi_rate,g.uom_symbol AS konversi_symbol
	FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom d ON c.`unit_terkecil`=d.`id_uom`
	JOIN t_uom_convert f ON d.`id_uom`=f.`id_uom`
	JOIN m_uom g ON f.`id_uom_parent`=g.id_uom
	JOIN t_eksternal e ON c.`dist_id`=e.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')= ?
	#AND DATE_FORMAT(a.`date_penjualan`,'%M')='?'
	GROUP BY e.`name_eksternal`,c.id_mat,DATE_FORMAT(a.`date_penjualan`,'%Y')
	ORDER BY `kode_eksternal`,c.`stock_name`,c.`base_qty`
		";
		}else{
			$sql 	= "
			SELECT e.`name_eksternal` AS Principle,c.`stock_name` AS Produk,CONCAT(c.`base_qty`,' ',d.`uom_symbol`) Kemasan,
	(SUM(b.`unit_price`)*(f.`convertion`/c.`unit_terkecil`))/f.`convertion` sumoftotal_lt_kg,SUM(b.`price`) totalsumoftotal,
	DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,c.`unit_terkecil`/f.`convertion` AS konversi_rate,g.uom_symbol AS konversi_symbol
	FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom d ON c.`unit_terkecil`=d.`id_uom`
	JOIN t_uom_convert f ON d.`id_uom`=f.`id_uom`
	JOIN m_uom g ON f.`id_uom_parent`=g.id_uom
	JOIN t_eksternal e ON c.`dist_id`=e.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')= ?
	AND DATE_FORMAT(a.`date_penjualan`,'%M')='?'
	GROUP BY e.`name_eksternal`,c.id_mat,DATE_FORMAT(a.`date_penjualan`,'%Y')
	ORDER BY `kode_eksternal`,c.`stock_name`,c.`base_qty`
		";
		}

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$year,$month
			));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function sales_perproductcust($year,$month)
	{
		//$sql 	= 'CALL uom_search_id(?)';
		
		if($month == 'All'){
		$sql 	= "
			SELECT A.*, 
IF(B.totalsumoftotal IS NULL,0,B.sumoftotal_lt_kg) AS sumtot_jan,
IF(B.sumoftotal_lt_kg IS NULL,0,B.sumoftotal_lt_kg) AS sumtotkg_jan,
IF(C.totalsumoftotal IS NULL,0,C.sumoftotal_lt_kg) AS sumtot_feb,
IF(C.sumoftotal_lt_kg IS NULL,0,C.sumoftotal_lt_kg) AS sumtotkg_feb,
IF(D.totalsumoftotal IS NULL,0,D.sumoftotal_lt_kg) AS sumtot_mar,
IF(D.sumoftotal_lt_kg IS NULL,0,D.sumoftotal_lt_kg) AS sumtotkg_mar,
IF(E.totalsumoftotal IS NULL,0,E.sumoftotal_lt_kg) AS sumtot_apr,
IF(E.sumoftotal_lt_kg IS NULL,0,E.sumoftotal_lt_kg) AS sumtotkg_apr,
IF(F.totalsumoftotal IS NULL,0,F.sumoftotal_lt_kg) AS sumtot_may,
IF(F.sumoftotal_lt_kg IS NULL,0,F.sumoftotal_lt_kg) AS sumtotkg_may,
IF(G.totalsumoftotal IS NULL,0,G.sumoftotal_lt_kg) AS sumtot_jun,
IF(G.sumoftotal_lt_kg IS NULL,0,G.sumoftotal_lt_kg) AS sumtotkg_jun,
IF(H.totalsumoftotal IS NULL,0,H.sumoftotal_lt_kg) AS sumtot_jul,
IF(H.sumoftotal_lt_kg IS NULL,0,H.sumoftotal_lt_kg) AS sumtotkg_jul,
IF(I.totalsumoftotal IS NULL,0,I.sumoftotal_lt_kg) AS sumtot_aug,
IF(I.sumoftotal_lt_kg IS NULL,0,I.sumoftotal_lt_kg) AS sumtotkg_aug,
IF(J.totalsumoftotal IS NULL,0,J.sumoftotal_lt_kg) AS sumtot_sep,
IF(J.sumoftotal_lt_kg IS NULL,0,J.sumoftotal_lt_kg) AS sumtotkg_sep,
IF(K.totalsumoftotal IS NULL,0,K.sumoftotal_lt_kg) AS sumtot_oct,
IF(K.sumoftotal_lt_kg IS NULL,0,K.sumoftotal_lt_kg) AS sumtotkg_oct,
IF(L.totalsumoftotal IS NULL,0,L.sumoftotal_lt_kg) AS sumtot_nov,
IF(L.sumoftotal_lt_kg IS NULL,0,L.sumoftotal_lt_kg) AS sumtotkg_nov,
IF(M.totalsumoftotal IS NULL,0,M.sumoftotal_lt_kg) AS sumtot_dec,
IF(M.sumoftotal_lt_kg IS NULL,0,M.sumoftotal_lt_kg) AS sumtotkg_dec

FROM (

SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
SUM(b.`price`) totalsumoftotal,
(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
c.id_mat, d.`id_t_cust`
#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')='2020' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`

) A LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year." 
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='01' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) B ON A.id_mat = B.id_mat AND A.id_t_cust = B.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year."  
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='02' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) C ON A.id_mat = C.id_mat AND A.id_t_cust = C.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year."  
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='03' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) D ON A.id_mat = D.id_mat AND A.id_t_cust = D.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year." 
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='04' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) E ON A.id_mat = E.id_mat AND A.id_t_cust = E.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year." 
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='05' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) F ON A.id_mat = F.id_mat AND A.id_t_cust = F.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year." 
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='06' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) G ON A.id_mat = G.id_mat AND A.id_t_cust = G.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year."  
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='07' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) H ON A.id_mat = H.id_mat AND A.id_t_cust = H.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year."  
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='08' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) I ON A.id_mat = I.id_mat AND A.id_t_cust = I.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year."  
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='09' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) J ON A.id_mat = J.id_mat AND A.id_t_cust = J.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year."  
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='10' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) K ON A.id_mat = K.id_mat AND A.id_t_cust = K.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year."  
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='11' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) L ON A.id_mat = L.id_mat AND A.id_t_cust = L.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year."  
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='12' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) M ON A.id_mat = M.id_mat AND A.id_t_cust = M.id_t_cust

		";
		}else{
			$sql 	= "
			SELECT e.`name_eksternal` AS Principle,c.`stock_name` AS Produk,CONCAT(c.`base_qty`,' ',d.`uom_symbol`) Kemasan,
	(SUM(b.`unit_price`)*(f.`convertion`/c.`unit_terkecil`))/f.`convertion` sumoftotal_lt_kg,SUM(b.`price`) totalsumoftotal,
	DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,c.`unit_terkecil`/f.`convertion` AS konversi_rate,g.uom_symbol AS konversi_symbol
	FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom d ON c.`unit_terkecil`=d.`id_uom`
	JOIN t_uom_convert f ON d.`id_uom`=f.`id_uom`
	JOIN m_uom g ON f.`id_uom_parent`=g.id_uom
	JOIN t_eksternal e ON c.`dist_id`=e.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')= ?
	AND DATE_FORMAT(a.`date_penjualan`,'%M')='?'
	GROUP BY e.`name_eksternal`,c.id_mat,DATE_FORMAT(a.`date_penjualan`,'%Y')
	ORDER BY `kode_eksternal`,c.`stock_name`,c.`base_qty`
		";
		}

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$year,$month
			));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_coa(){
		
		$query =  $this->db->select('*')
		->from('t_coa');
		
		
		$query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
		
	}

	public function lists($params = array())
	{
			$query = 	'
				SELECT COUNT(*) AS jumlah FROM t_coa_value a join t_coa b on a.id_coa = b.id_coa where 1 = 1
				AND ( 
					coa LIKE "%'.$params['searchtxt'].'%" OR
					keterangan LIKE "%'.$params['searchtxt'].'%" 
				)
			';
						
			$query2 = 	'
				SELECT z.*, rank() over ( ORDER BY `id` DESC) AS Rangking from ( 
					select a.*,b.coa,b.keterangan FROM t_coa_value a join t_coa b on a.id_coa = b.id_coa
					where 1 = 1  AND ( 
					coa LIKE "%'.$params['searchtxt'].'%" OR
					keterangan LIKE "%'.$params['searchtxt'].'%" 
				)  order by `id` DESC) z
				ORDER BY `id` DESC
				
				LIMIT '.$params['limit'].' 
				OFFSET '.$params['offset'].' 
			';
		
			//echo $query2;die;
		
			 $out		= array();
			  $querys		= $this->db->query($query);
			  $result = $querys->row();
			  
			  $total_filtered = $result->jumlah;
			  $total 			= $result->jumlah;
		  
				if(($params['offset']+10) > $total_filtered){
				$limit_data = $total_filtered - $params['offset'];
			  }else{
				$limit_data = $params['limit'] ;
			  }
		  
		
		  
		  //echo $query;die;
			//echo $query;die;
			 $query2s		= $this->db->query($query2);
		  $result2 = $query2s->result_array();						
		  $return = array(
			  'data' => $result2,
			  'total_filtered' => $total_filtered,
			  'total' => $total,
		  );
		  return $return;
	}
	
	public function edit_uom($data)
	{
		$datas = array(
			'id_coa' => $data['coa'],
			'date' => $data['tgl'],
			'date_insert' => date('Y-m-d H:i:s'),
			'value' => $data['jumlah'],
			'value_real' => $data['jumlah'],
			'type_cash' => $data['cash'],
			'id_valas' => 1,
			'note' => $data['ket']
		);

	//	print_r($datas);die;
		$this->db->where('id',$data['id']);
		$this->db->update('t_coa_value',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_uom($data)
	{
		$datas = array(
			'id_coa' => $data['coa'],
			'date' => $data['tgl'],
			'date_insert' => date('Y-m-d H:i:s'),
			'value' => $data['jumlah'],
			'value_real' => $data['jumlah'],
			'type_cash' => $data['cash'],
			'id_valas' => 1,
			'note' => $data['ket']
		);

	//	print_r($datas);die;

		$this->db->insert('t_coa_value',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	
	public function deletes($id_uom)
	{
		$this->db->where('id', $id_uom);
		$this->db->delete('t_coa_value'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}


}
