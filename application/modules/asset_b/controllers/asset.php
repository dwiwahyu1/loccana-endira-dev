<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Asset extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('asset/stock_model');
    $this->load->library('log_activity');
    $this->load->library('formatnumbering');
  }

  /**
   * anti sql injection
   */
  public function Anti_sql_injection($string)
  {
    $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
    return $string;
  }

  public function index()
  {
    $this->template->load('maintemplate', 'asset/views/index');
  }

  function lists()
  {
    $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
    $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
    $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
    $order = $this->input->get_post('order');
    $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
    $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

    $order_fields = array('', 'stock_code', 'stock_name');

    $search = $this->input->get_post('search');

    $search_val = (!empty($search['value'])) ? $search['value'] : null;

    $search_value = $this->Anti_sql_injection($search_val);

    // Build params for calling model
    $params['limit'] = (int) $length;
    $params['offset'] = (int) $start;
    $params['order_column'] = $order_fields[$order_column];
    $params['order_dir'] = $order_dir;
    $params['searchtxt'] = $search_value;

    $list = $this->stock_model->lists($params);

    $result["recordsTotal"] = $list['total'];
    $result["recordsFiltered"] = $list['total_filtered'];
    $result["draw"] = $draw;

    $data = array();
    //$i = 0;
    $i = $params['offset'];
    $username = $this->session->userdata['logged_in']['username'];
    foreach ($list['data'] as $k => $v) {
      $i = $i + 1;
      $button_mutasi = '<div class="btn-group"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Mutasi" onClick="editmaterial(\'' . $v['id_mat'] . '\')"><i class="fa fa-history"></i></button></div>';
      // $button_approve = '<div class="btn-group"><button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Approve" onClick="approvematerial(\'' . $v['id_mat'] . '\')"><i class="fa fa-check"></i></button></div>';
      $button_detail = '<div class="btn-group"><button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Detail" onClick="detailmaterial(\'' . $v['id_mat'] . '\')"><i class="fa fa-search"></i></button></div>';
      // if($v['status_harga']==0){
      //   $status = 'Konfirmasi';
      $button = $button_detail . $button_mutasi;
      // }
      // else {
      //   $status = 'Setuju';
      //   $button = $button_edit;
      // }
      array_push($data, array(
        $v['stock_code'],
        $v['stock_name'],
        $v['base_qty'].' '.$v['uom_symbol'],
		 $v['name_eksternal'],
        number_format($v['box_per_lt'],2,',','.'),
        //number_format($v['qty'],0,',','.').' Pcs',
        number_format($v['saldo_awal_big'],2,',','.').'',
        number_format($v['saldo_awal']/$v['unit_box'],2,',','.').'',
        number_format($v['pemasukan']/$v['unit_box'],2,',','.').'',
        number_format($v['pengeluaran_01']/$v['unit_box'],2,',','.').'',
        // number_format($v['pengeluaran_02']/$v['unit_box'],2,',','.').'',
        // number_format($v['pengeluaran_03']/$v['unit_box'],2,',','.').'',
        // number_format($v['pengeluaran_04']/$v['unit_box'],2,',','.').'',
        number_format($v['saldo_akhir_big'],2,',','.').'',
        number_format($v['saldo_akhir']/$v['unit_box'],2,',','.').'',
       // $this->formatnumbering->qty($v['qty_big']),
        // $status,
        $button
      ));
    }

    $result["data"] = $data;

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }
  function lists_mutasi()
  {
    $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
    $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
    $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
    $order = $this->input->get_post('order');
    $id_mat = (int)$this->Anti_sql_injection($this->uri->segment(3));
    $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
    $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

    $order_fields = array('', 'stock_code', 'stock_name');

    $search = $this->input->get_post('search');

    $search_val = (!empty($search['value'])) ? $search['value'] : null;

    $search_value = $this->Anti_sql_injection($search_val);

    // Build params for calling model
    $params['limit'] = (int) $length;
    $params['offset'] = (int) $start;
    $params['order_column'] = $order_fields[$order_column];
    $params['order_dir'] = $order_dir;
    $params['searchtxt'] = $search_value;
    $params['id_mat'] = $id_mat;
    // var_dump($params);die;
    $list = $this->stock_model->list_mutasi($params);

    $result["recordsTotal"] = $list['total'];
    $result["recordsFiltered"] = $list['total_filtered'];
    $result["draw"] = $draw;

    $data = array();
    //$i = 0;
    $i = $params['offset'];
    $username = $this->session->userdata['logged_in']['username'];
    foreach ($list['data'] as $k => $v) {
      $i = $i + 1;
      $button_mutasi = '<div class="btn-group"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Mutasi" onClick="editmaterial(\'' . $v['id_mat'] . '\')"><i class="fa fa-history"></i></button></div>';
      $button_approve = '<div class="btn-group"><button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Approve" onClick="approvematerial(\'' . $v['id_mat'] . '\')"><i class="fa fa-check"></i></button></div>';
      $button_detail = '<div class="btn-group"><button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Detail" onClick="approvematerial(\'' . $v['id_mat'] . '\')"><i class="fa fa-search"></i></button></div>';
      // if($v['status_harga']==0){
      //   $status = 'Konfirmasi';
      $button = $button_detail . $button_mutasi . $button_approve;
      // }
      // else {
      //   $status = 'Setuju';
      //   $button = $button_edit;
      // }
	  
	  if( $v['type_mutasi'] == 0){
		 // $tp_mutasi = "Pemasukan";
		  $tp_mutasi = "Pembelian";
	  }else{
		 // $tp_mutasi = "Pengeluaran";
		  $tp_mutasi = "Penjualan";
	  }
	  
      array_push($data, array(
        $i,
        $v['stock_code'],
        $v['stock_name'],
        $v['date_mutasi'],
         $tp_mutasi,
        $this->formatnumbering->qty($v['mutasi_big']),
        $v['name_eksternal'],
        $v['note'],
        // $status,
        // $button
      ));
    }

    $result["data"] = $data;

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }
  
  function getDatesFromRange($start, $end, $format = 'Y-m-d') {
    $array = array();
    $interval = new DateInterval('P1D');

    $realEnd = new DateTime($end);
    $realEnd->add($interval);

    $period = new DatePeriod(new DateTime($start), $interval, $realEnd);

    foreach($period as $date) { 
        $array[] = $date->format($format); 
    }

    return $array;
}
  
   public function export(){
	
	// $data   	= file_get_contents("php://input");
	// $params     = json_decode($data,true);
	  
	$params = array(
      'start_time' => $this->Anti_sql_injection($this->input->post('tgl_awal', TRUE)),
      'end_time' => $this->Anti_sql_injection($this->input->post('tgl_akhir', TRUE))
    );
	
	//print_r($params);die;
	  
	  $array_date = $this->getDatesFromRange($params['start_time'], $params['end_time']);
	  
	  $html_day = '';
	  $query_head = ' SELECT a.*,`kode_eksternal`,`name_eksternal`,(a.base_qty*a.unit_box)/convertion box_per_lt ,uom.*,b.mutasi_big_fix AS saldo_awal,b.amount_mutasi_fix/a.unit_box AS saldo_awal_big, ';
	  $query_sum = '  b.amount_mutasi_fix ';
	  $query_big = '  b.mutasi_big_fix ';
	  $query_penerimaan = ' ';
	  $query_pengeluaran[1] = ' ';
	  $query_pengeluaran[2] = ' ';
	  $query_pengeluaran[3] = ' ';
	  $query_pengeluaran[4] = ' ';
	  
	  $itr = 0;
	  foreach($array_date as $array_dates){
		  
		  $dt = strtotime($array_dates);
		  
		  $html_day .= '<th>'.date("d/M/y",$dt).'</th>';
		  $query_head .= "
					IF(p_".$itr.".amount_mutasi_fix IS NULL,0,p_".$itr.".amount_mutasi_fix)AS pemasukan_".$itr.",IF(p_".$itr.".mutasi_big_fix IS NULL,0,p_".$itr.".mutasi_big_fix) AS pemasukan_big_".$itr.",
						";

					// $query_head .= "
					// IF(p_".$itr.".amount_mutasi_fix IS NULL,0,p_".$itr.".amount_mutasi_fix)AS pemasukan_".$itr.",IF(p_".$itr.".mutasi_big_fix IS NULL,0,p_".$itr.".mutasi_big_fix) AS pemasukan_big_".$itr.",
					// IF(l1_".$itr.".amount_mutasi_fix IS NULL,0,l1_".$itr.".amount_mutasi_fix)AS pengeluaran_01_".$itr.",IF(l1_".$itr.".mutasi_big_fix IS NULL,0,l1_".$itr.".mutasi_big_fix) AS pengeluaran_big_01_".$itr.",
					// IF(l2_".$itr.".amount_mutasi_fix IS NULL,0,l2_".$itr.".amount_mutasi_fix)AS pengeluaran_02_".$itr.",IF(l2_".$itr.".mutasi_big_fix IS NULL,0,l2_".$itr.".mutasi_big_fix) AS pengeluaran_big_02_".$itr.",
					// IF(l3_".$itr.".amount_mutasi_fix IS NULL,0,l3_".$itr.".amount_mutasi_fix)AS pengeluaran_03_".$itr.",IF(l3_".$itr.".mutasi_big_fix IS NULL,0,l3_".$itr.".mutasi_big_fix) AS pengeluaran_big_03_".$itr.",
					// IF(l4_".$itr.".amount_mutasi_fix IS NULL,0,l4_".$itr.".amount_mutasi_fix)AS pengeluaran_04_".$itr.",IF(l4_".$itr.".mutasi_big_fix IS NULL,0,l4_".$itr.".mutasi_big_fix) AS pengeluaran_big_04_".$itr.",
					// ";
			
			// $query_sum .=" + IF(p_".$itr.".amount_mutasi_fix IS NULL,0,p_".$itr.".amount_mutasi_fix) - IF(l1_".$itr.".amount_mutasi_fix IS NULL,0,l1_".$itr.".amount_mutasi_fix) -IF(l2_".$itr.".amount_mutasi_fix IS NULL,0,l2_".$itr.".amount_mutasi_fix) - IF(l3_".$itr.".amount_mutasi_fix IS NULL,0,l3_".$itr.".amount_mutasi_fix) - IF(l4_".$itr.".amount_mutasi_fix IS NULL,0,l4_".$itr.".amount_mutasi_fix) ";
			// $query_big .="  + IF(p_".$itr.".mutasi_big_fix IS NULL,0,p_".$itr.".mutasi_big_fix) - IF(l1_".$itr.".mutasi_big_fix IS NULL,0,l1_".$itr.".mutasi_big_fix) -IF(l2_".$itr.".mutasi_big_fix IS NULL,0,l2_".$itr.".mutasi_big_fix) - IF(l3_".$itr.".mutasi_big_fix IS NULL,0,l3_".$itr.".mutasi_big_fix) - IF(l4_".$itr.".mutasi_big_fix IS NULL,0,l4_".$itr.".mutasi_big_fix) ";		
		
			$query_penerimaan .="
				LEFT JOIN 
					(

					SELECT a.id_stock_awal,
					(IF(a.amount_mutasi IS NULL,0,a.amount_mutasi)) AS amount_mutasi_fix,
					IF(a.mutasi_big IS NULL,0,a.mutasi_big) AS mutasi_big_fix,
					(IF(a.qty_sol IS NULL,0,a.qty_sol)) AS qty_sol_fix,
					IF(a.qty_sol_big IS NULL,0,a.qty_sol_big) AS qty_sol_big_fix,
					(IF(a.qty_bonus IS NULL,0,a.qty_bonus)) AS qty_bonus_fix,
					IF(a.qty_bonus_big IS NULL,0,a.qty_bonus_big) AS qty_bonus_big_fix,
					(IF(a.qty_titip IS NULL,0,a.qty_titip)) AS qty_titip_fix,
					IF(a.qty_titip_big IS NULL,0,a.qty_titip_big) AS qty_titip_big_fix
					FROM ( SELECT id_stock_awal,b.unit_box,
					SUM(a.`amount_mutasi`) amount_mutasi, 
					SUM(a.`mutasi_big`) mutasi_big,
					SUM(a.`qty_sol`) qty_sol,
					SUM(a.`qty_sol_big`) qty_sol_big,
					SUM(a.`qty_bonus`) qty_bonus,
					SUM(a.`qty_bonus_big`) qty_bonus_big,
					SUM(a.`qty_titip`) qty_titip,
					SUM(a.`qty_titip_big`) qty_titip_big
					FROM t_mutasi a 
					JOIN m_material b ON a.id_stock_awal = b.id_mat
					WHERE a.`type_mutasi`=0 AND a.date_mutasi = '".$array_dates."'
					GROUP BY a.`id_stock_awal`) a 

					)p_".$itr." ON a.id_mat=p_".$itr.".id_stock_awal
			";
			
			for($tyr = 1;$tyr < 5; $tyr++){
				
				$query_pengeluaran[$tyr] .="
				LEFT JOIN 
					(
					SELECT a.id_stock_awal,
					(IF(a.amount_mutasi IS NULL,0,a.amount_mutasi)) AS amount_mutasi_fix,
					IF(a.mutasi_big IS NULL,0,a.mutasi_big) AS mutasi_big_fix,
					(IF(a.qty_sol IS NULL,0,a.qty_sol)) AS qty_sol_fix,
					IF(a.qty_sol_big IS NULL,0,a.qty_sol_big) AS qty_sol_big_fix,
					(IF(a.qty_bonus IS NULL,0,a.qty_bonus)) AS qty_bonus_fix,
					IF(a.qty_bonus_big IS NULL,0,a.qty_bonus_big) AS qty_bonus_big_fix,
					(IF(a.qty_titip IS NULL,0,a.qty_titip)) AS qty_titip_fix,
					IF(a.qty_titip_big IS NULL,0,a.qty_titip_big) AS qty_titip_big_fix
					FROM ( SELECT id_stock_awal,f.unit_box,
					SUM(a.`amount_mutasi`) amount_mutasi, 
					SUM(a.`mutasi_big`) mutasi_big,
					SUM(a.`qty_sol`) qty_sol,
					SUM(a.`qty_sol_big`) qty_sol_big,
					SUM(a.`qty_bonus`) qty_bonus,
					SUM(a.`qty_bonus_big`) qty_bonus_big,
					SUM(a.`qty_titip`) qty_titip,
					SUM(a.`qty_titip_big`) qty_titip_big
					FROM t_mutasi a 
					JOIN d_penjualan b ON a.`id_mutasi`=b.`mutasi_id`
					JOIN t_penjualan c ON b.`id_penjualan`=c.`id_penjualan`
					JOIN t_customer d ON c.`id_customer`=d.`id_t_cust`
					JOIN t_region e ON d.`region`=e.`id_t_region`
					JOIN m_material f ON a.id_stock_awal=f.`id_mat`
					WHERE a.`type_mutasi`=1 AND a.date_mutasi = '".$array_dates."' 
					AND d.region = ".$tyr."
					GROUP BY a.`id_stock_awal`) a 
					
					)p_".$itr." ON a.id_mat=p_".$itr.".id_stock_awal
				";
				
			}
			
			
			
		 $itr++; 
	  }
	  
	  //print_r($query_sum);die;
	  
	 // $query_full =  $query_head.' '.$query_sum.' as saldo_akhir, '.$query_big.' as saldo_akhir_big
	  $query_full =  $query_head.' "a" 
	   FROM m_material a
					 JOIN `m_uom` uom ON a.`unit_terkecil` = uom.`id_uom`
					 JOIN `t_eksternal` ek ON a.`dist_id` = ek.`id`
					 LEFT JOIN `t_uom_convert` uc ON uom.id_uom = uc.`id_uom`
					LEFT JOIN (
					SELECT a.id_stock_awal,
					(IF(a.amount_mutasi IS NULL,0,a.amount_mutasi)-IF(b.amount_mutasi IS NULL,0,b.amount_mutasi)) AS amount_mutasi_fix,
					IF(a.mutasi_big IS NULL,0,a.mutasi_big)-IF(b.mutasi_big IS NULL,0,b.mutasi_big) AS mutasi_big_fix,
					(IF(a.qty_sol IS NULL,0,a.qty_sol)-IF(b.qty_sol IS NULL,0,b.qty_sol)) AS qty_sol_fix,
					IF(a.qty_sol_big IS NULL,0,a.qty_sol_big)-IF(b.qty_sol_big IS NULL,0,b.qty_sol_big) AS qty_sol_big_fix,
					(IF(a.qty_bonus IS NULL,0,a.qty_bonus)-IF(b.qty_bonus IS NULL,0,b.qty_bonus)) AS qty_bonus_fix,
					IF(a.qty_bonus_big IS NULL,0,a.qty_bonus_big)-IF(b.qty_bonus_big IS NULL,0,b.qty_bonus_big) AS qty_bonus_big_fix,
					(IF(a.qty_titip IS NULL,0,a.qty_titip)-IF(b.qty_titip IS NULL,0,b.qty_titip)) AS qty_titip_fix,
					IF(a.qty_titip_big IS NULL,0,a.qty_titip_big)-IF(b.qty_titip_big IS NULL,0,b.qty_titip_big) AS qty_titip_big_fix
					FROM ( SELECT id_stock_awal,b.unit_box,
					SUM(a.`amount_mutasi`) amount_mutasi, 
					SUM(a.`mutasi_big`) mutasi_big,
					SUM(a.`qty_sol`) qty_sol,
					SUM(a.`qty_sol_big`) qty_sol_big,
					SUM(a.`qty_bonus`) qty_bonus,
					SUM(a.`qty_bonus_big`) qty_bonus_big,
					SUM(a.`qty_titip`) qty_titip,
					SUM(a.`qty_titip_big`) qty_titip_big
					FROM t_mutasi a 
					JOIN m_material b ON a.id_stock_awal = b.id_mat
					WHERE a.`type_mutasi`=0 AND a.date_mutasi < "'.$params['start_time'].'"
					GROUP BY a.`id_stock_awal`) a LEFT JOIN ( SELECT id_stock_awal,
					SUM(a.`amount_mutasi`) amount_mutasi, 
					SUM(a.`mutasi_big`) mutasi_big,
					SUM(a.`qty_sol`) qty_sol,
					SUM(a.`qty_sol_big`) qty_sol_big,
					SUM(a.`qty_bonus`) qty_bonus,
					SUM(a.`qty_bonus_big`) qty_bonus_big,
					SUM(a.`qty_titip`) qty_titip,
					SUM(a.`qty_titip_big`) qty_titip_big
					FROM t_mutasi a
					WHERE a.`type_mutasi`=1 AND a.`date_mutasi` < "'.$params['start_time'].'"
					GROUP BY a.`id_stock_awal`) b ON a.id_stock_awal=b.id_stock_awal) b ON a.id_mat=b.id_stock_awal
					
					
	  ';
	  
	 //echo $query_full;die;
	  
	   $data_r = $this->stock_model->get_stock_data($query_full,$query_penerimaan);
	   $data_r1 = $this->stock_model->get_stock_data($query_full,$query_pengeluaran[1]);
	   $data_r2 = $this->stock_model->get_stock_data($query_full,$query_pengeluaran[2]);
	   $data_r3 = $this->stock_model->get_stock_data($query_full,$query_pengeluaran[3]);
	   $data_r4 = $this->stock_model->get_stock_data($query_full,$query_pengeluaran[4]);
	   // echo "<pre>";
	 // print_r($data_r);die;
	 // echo "</pre>";
	  
	  
	$this->load->library('excel');
	   
		$objPHPExcel = new PHPExcel();
	  
	  
	  //print_r($this->getNameFromNumber(0));die;
	  $objPHPExcel->setActiveSheetIndex(0)
            ->mergeCells('A5:A6')
            ->setCellValue('A5', 'Kode')
            ->mergeCells('B5:B6')
            ->setCellValue('B5', 'Produk')
            ->mergeCells('C5:C6')
            ->setCellValue('C5', 'Kemasan')
            ->mergeCells('D5:D6')
            ->setCellValue('D5', 'Principal')
            ->mergeCells('E5:E6')
            ->setCellValue('E5', 'Box per Lt/Kg')
            ->mergeCells('F5:G5')
            ->setCellValue('F5', 'Saldo Awal')
            ->setCellValue('F6', 'Lt/Box')
            ->setCellValue('G6', 'Box');
			
		
	  
	  $html = "<tr>
                  <th rowspan=2>Kode</th>
                  <th rowspan=2 width='100'>Produk</th>
                  <th rowspan=2>Kemasan</th>
                  <th rowspan=2 width='150px'>Principal</th>
				  <th rowspan=2>Box per LT/KG</th>
                  <th colspan=2>Stock Awal </th>
                  <th colspan=".count($array_date).">Penerimaan </th>
                  <th colspan=".count($array_date).">DO Wilayah 1</th>
                  <th colspan=".count($array_date).">DO Wilayah 2</th>
                  <th colspan=".count($array_date).">DO Wilayah 3</th>
                  <th colspan=".count($array_date).">DO Office</th>
				  <th colspan=2>Stok Akhir </th>
                  <th rowspan=2 width='150px'>Option</th>
                </tr>
				<tr>
					<th>Lt/Kg</th>
					<th>Box</th>
					".$html_day."
					".$html_day."
					".$html_day."
					".$html_day."
					".$html_day."
					<th>Lt/Kg</th>
					<th>Box</th>
				</tr>";
	
	$html_body = '';
	
	 
	$rtr = 0;
	$cell_a_data = 7;
	foreach($data_r as $v){
		
		 $button_mutasi = '<div class="btn-group"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Mutasi" onClick="editmaterial(\'' . $v['id_mat'] . '\')"><i class="fa fa-history"></i></button></div>';

			$button_detail = '<div class="btn-group"><button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Detail" onClick="detailmaterial(\'' . $v['id_mat'] . '\')"><i class="fa fa-search"></i></button></div>';

      $button = $button_detail . $button_mutasi;
		
		$penerimaan_html = "";
		$pengeluaran_01_html = "";
		$pengeluaran_02_html = "";
		$pengeluaran_03_html = "";
		$pengeluaran_04_html = "";
		
		$pmi = 0;
		
		$cell_a = 7;
		
		$saldo_akhir_a = 0;
		$saldo_akhir_a_big = 0;
		
		$total_pemasukan = 0;
		$total_peneluaran1 = 0;
		$total_peneluaran2 = 0;
		$total_peneluaran3 = 0;
		$total_peneluaran4 = 0;
		
		$total_pemasukan_big = 0;
		$total_peneluaran1_big = 0;
		$total_peneluaran2_big = 0;
		$total_peneluaran3_big = 0;
		$total_peneluaran4_big = 0;
			
		foreach($array_date as $array_datess){
			
			$dts = strtotime($array_datess);
		  
			$dayf = date("d/M/y",$dts);
			$curr_cell = $this->getNameFromNumber($cell_a);
			$curr_cell_1 = $this->getNameFromNumber($cell_a+count($array_date));
			$curr_cell_2 = $this->getNameFromNumber($cell_a+count($array_date)+count($array_date));
			$curr_cell_3 = $this->getNameFromNumber($cell_a+count($array_date)+count($array_date)+count($array_date));
			$curr_cell_4 = $this->getNameFromNumber($cell_a+count($array_date)+count($array_date)+count($array_date)+count($array_date));
			$curr_cell_A1 = $this->getNameFromNumber($cell_a+count($array_date)+count($array_date)+count($array_date)+count($array_date)+1);
			$curr_cell_A2 = $this->getNameFromNumber($cell_a+count($array_date)+count($array_date)+count($array_date)+count($array_date)+2);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($curr_cell.'6', $dayf);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($curr_cell_1.'6', $dayf);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($curr_cell_2.'6', $dayf);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($curr_cell_3.'6', $dayf);
			$objPHPExcel->setActiveSheetIndex(0)->setCellValue($curr_cell_4.'6', $dayf);
			
			$penerimaan_html .= "<td>".number_format($v['pemasukan_'.$pmi],2,',','.')."</td>";
			$pengeluaran_01_html .= "<td>".number_format($data_r1[$rtr]['pemasukan_'.$pmi]/$v['unit_box'],2,'.','')."</td>";
			$pengeluaran_02_html .= "<td>".number_format($data_r2[$rtr]['pemasukan_'.$pmi]/$v['unit_box'],2,'.','')."</td>";
			$pengeluaran_03_html .= "<td>".number_format($data_r3[$rtr]['pemasukan_'.$pmi]/$v['unit_box'],2,'.','')."</td>";
			$pengeluaran_04_html .= "<td>".number_format($data_r4[$rtr]['pemasukan_'.$pmi]/$v['unit_box'],2,'.','')."</td>";
			
			$total_pemasukan += $v['pemasukan_'.$pmi];
			$total_peneluaran1 += $data_r1[$rtr]['pemasukan_'.$pmi];
			$total_peneluaran2 += $data_r2[$rtr]['pemasukan_'.$pmi];
			$total_peneluaran3 += $data_r3[$rtr]['pemasukan_'.$pmi];
			$total_peneluaran4 += $data_r4[$rtr]['pemasukan_'.$pmi];
			
			$total_pemasukan_big += $v['pemasukan_big_'.$pmi];
			$total_peneluaran1_big += $data_r1[$rtr]['pemasukan_big_'.$pmi];
			$total_peneluaran2_big += $data_r2[$rtr]['pemasukan_big_'.$pmi];
			$total_peneluaran3_big += $data_r3[$rtr]['pemasukan_big_'.$pmi];
			$total_peneluaran4_big += $data_r4[$rtr]['pemasukan_big_'.$pmi];
			
			$saldo_akhir_a += $v['pemasukan_'.$pmi] - $data_r1[$rtr]['pemasukan_'.$pmi] - $data_r2[$rtr]['pemasukan_'.$pmi] -  $data_r3[$rtr]['pemasukan_'.$pmi] - $data_r4[$rtr]['pemasukan_'.$pmi];
			$saldo_akhir_a_big += $v['pemasukan_big_'.$pmi] - $data_r1[$rtr]['pemasukan_big_'.$pmi] - $data_r2[$rtr]['pemasukan_big_'.$pmi] - $data_r3[$rtr]['pemasukan_big_'.$pmi] - $data_r4[$rtr]['pemasukan_big_'.$pmi];
			
			$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue($curr_cell.$cell_a_data, number_format($v['pemasukan_'.$pmi]/$v['unit_box'],2,'.',''))
			->setCellValue($curr_cell_1.$cell_a_data, number_format($data_r1[$rtr]['pemasukan_'.$pmi]/$v['unit_box'],2,'.',''))
			->setCellValue($curr_cell_2.$cell_a_data, number_format($data_r2[$rtr]['pemasukan_'.$pmi]/$v['unit_box'],2,'.',''))
			->setCellValue($curr_cell_3.$cell_a_data, number_format($data_r3[$rtr]['pemasukan_'.$pmi]/$v['unit_box'],2,'.',''))
			->setCellValue($curr_cell_4.$cell_a_data, number_format($data_r4[$rtr]['pemasukan_'.$pmi]/$v['unit_box'],2,'.',''));
			
			
			$pmi++;
			$cell_a++;
		}
		
		// if($v['id_mat'] == 6){
		
		// echo $v['saldo_awal'] .'+'. ($total_pemasukan/$v['unit_box']) .'-'. ($total_peneluaran1/$v['unit_box']) .'+'. ($total_peneluaran2/$v['unit_box']) .'+'. ($total_peneluaran3/$v['unit_box']) .'+'. ($total_peneluaran4/$v['unit_box']);
		// die;
		
		// }
		$saa = $v['saldo_awal_big'] + ($total_pemasukan/$v['unit_box']) - ($total_peneluaran1/$v['unit_box']) - ($total_peneluaran2/$v['unit_box']) - ($total_peneluaran3/$v['unit_box']) - ($total_peneluaran4/$v['unit_box']);
		$saab = $v['saldo_awal'] + $total_pemasukan_big - $total_peneluaran1_big - $total_peneluaran2_big - $total_peneluaran3_big - $total_peneluaran4_big;
		$objPHPExcel->setActiveSheetIndex(0)
		// ->setCellValue($curr_cell_A1.$cell_a_data, number_format($saab,2,'.',''))
		// ->setCellValue($curr_cell_A2.$cell_a_data, number_format($saa,2,'.',''));
		->setCellValue($curr_cell_A1.$cell_a_data, number_format($saab,2,'.',''))
		->setCellValue($curr_cell_A2.$cell_a_data, number_format($saab/$v['box_per_lt'],2,'.',''));
		//ECHO $cell_a;die;
		
		$sh1 = 7+count($array_date);
		$sh2 = 7+count($array_date)+count($array_date);
		$sh3 = 7+count($array_date)+count($array_date)+count($array_date);
		$sh4 = 7+count($array_date)+count($array_date)+count($array_date)+count($array_date);
		$sh5 = 7+count($array_date)+count($array_date)+count($array_date)+count($array_date)+count($array_date);

		$objPHPExcel->setActiveSheetIndex(0)
        ->mergeCells('H5:'.$curr_cell.'5')
        ->setCellValue('H5', 'Penerimaan/Pembelian')
		->mergeCells($this->getNameFromNumber($sh1).'5:'.$curr_cell_1.'5')
        ->setCellValue($this->getNameFromNumber($sh1).'5', 'DO. WILAYAH I')
		->mergeCells($this->getNameFromNumber($sh2).'5:'.$curr_cell_2.'5')
        ->setCellValue($this->getNameFromNumber($sh2).'5', 'DO. WILAYAH II')
		->mergeCells($this->getNameFromNumber($sh3).'5:'.$curr_cell_3.'5')
        ->setCellValue($this->getNameFromNumber($sh3).'5', 'DO. WILAYAH III')
		->mergeCells($this->getNameFromNumber($sh4).'5:'.$curr_cell_4.'5')
        ->setCellValue($this->getNameFromNumber($sh4).'5', 'DO. WILAYAH IV')
		->mergeCells($this->getNameFromNumber($sh5).'5:'.$this->getNameFromNumber($sh5+2).'5')
        ->setCellValue($this->getNameFromNumber($sh5).'5', 'Saldo Akhir')
        ->setCellValue($this->getNameFromNumber($sh5).'6', 'Lt/Kg')
        ->setCellValue($this->getNameFromNumber($sh5+1).'6', 'Box')
        ->setCellValue($this->getNameFromNumber($sh5+2).'6', 'Keterangan');
		
		
		$objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$cell_a_data, $v['stock_code'])
        ->setCellValue('B'.$cell_a_data, $v['stock_name'])
        ->setCellValue('C'.$cell_a_data, $v['base_qty'].' '.$v['uom_symbol'])
        ->setCellValue('D'.$cell_a_data, $v['name_eksternal'])
        ->setCellValue('E'.$cell_a_data, number_format($v['box_per_lt'],2,'.',''))
        ->setCellValue('F'.$cell_a_data, number_format($v['saldo_awal'],2,'.',''))
        ->setCellValue('G'.$cell_a_data, number_format($v['saldo_awal_big'],2,'.',''));
		
		
		$rtr++;
		$cell_a_data++;
	}
	  
		
		$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'PT ENDIRA ALDA')
            ->setCellValue('A2', 'JL PASIRKALIKI 145')
            ->setCellValue('A3', 'B A N D U N G');
			
			
			
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename=Report_persediaan.xls');
			header('Cache-Control: max-age=0');
			// If you’re serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');
			// If you’re serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			unset($objPHPExcel);
  }
  
	public function getNameFromNumber($num) {
		$numeric = $num % 26;
		$letter = chr(65 + $numeric);
		$num2 = intval($num / 26);
		if ($num2 > 0) {
			return $this->getNameFromNumber($num2 - 1) . $letter;
		} else {
			return $letter;
		}
	}
  
  
  public function filter(){
	
	$data   	= file_get_contents("php://input");
	$params     = json_decode($data,true);
	  
	// $data = array(
      // 'start_time' => $this->Anti_sql_injection($this->input->post('start_time', TRUE)),
      // 'end_time' => $this->Anti_sql_injection($this->input->post('end_time', TRUE))
    // );
	  
	  $array_date = $this->getDatesFromRange($params['start_time'], $params['end_time']);
	  

	 
	 $data_r = $this->stock_model->get_stock_data_range($params);
	 
	// print_r($data_r);die;
	  
	  $html = "<tr>
                   <th>Asset</th>
                  <th width='100'>Keterangan</th>
                  <th >Tanggal Pembelian</th>
                  <th >Tahun</th>
                  <th >Bulan </th>
                  <th >Tanggal Akhir </th>
                  <th >Harga</th>
                  <th >Depresiasi Perbulan </th>
                  <th >Akumulasi Depresiasi ".(date('Y')-1)." </th>
                  <th >Depresiasi ".(date('Y'))." </th>
                  <th >Total</th>
                  <th >Value</th>
                </tr>";
	
	$html_body = '';
	
	 
	$rtr = 0;
	foreach($data_r as $v){
		
		$mth = $v['bln']*12;
		
		if($mth == 0){
			$mthv = 1;
		}else{
			IF($v['note'] == 'Motor Honda Legenda' || $v['note'] == 'Honda WIN' ){
				 $mth = 60;
			}
			$mthv = $mth;
		}
		
		
		$mth_day = $mth*30;
		
		$end_data = date('Y-m-d', strtotime($v['date']. ' + '.$mth_day.' days'));
		
		 $html_body .= "<tr>
                  <td>".$v['keterangan']."</td>
                  <td>".$v['note']."</td>
                  <td>".$v['date']."</td>
                  <td>".$v['bln']."</th>
                  <td>".$mth."</th>
				  <td>".$end_data."</th>
				   <td>".number_format($v['value_real'],2,',','.')."</td>
				   <td>".number_format($v['value_real']/$mthv,2,',','.')."</td>
				   <td>".number_format($v['valuea'],2,',','.')."</td>
				   <td>".number_format($v['valuea2'],2,',','.')."</td>
				   <td>".number_format($v['valuea']+$v['valuea2'],2,',','.')."</td>
				   <td>".number_format($v['value_real'] - ($v['valuea']+$v['valuea2']),2,',','.')."</td>
				  
                </tr>";
		
		$rtr++;
	}
	  
	// print_r($array_date);die;
	  
	  $result['table'] = $html;
	  $result['body'] = $html_body;
	  $result['data'] = $data_r;
		
	$this->output->set_content_type('application/json')->set_output(json_encode($result));
  }
  
  public function filter_old(){
	
	$data   	= file_get_contents("php://input");
	$params     = json_decode($data,true);
	  
	// $data = array(
      // 'start_time' => $this->Anti_sql_injection($this->input->post('start_time', TRUE)),
      // 'end_time' => $this->Anti_sql_injection($this->input->post('end_time', TRUE))
    // );
	  
	  $array_date = $this->getDatesFromRange($params['start_time'], $params['end_time']);
	  
	  $html_day = '';
	  $query_head = ' SELECT a.*,`kode_eksternal`,`name_eksternal`,(a.base_qty*a.unit_box)/convertion box_per_lt ,uom.*,b.amount_mutasi_fix AS saldo_awal,b.mutasi_big_fix AS saldo_awal_big, ';
	  $query_sum = '  b.amount_mutasi_fix ';
	  $query_big = '  b.mutasi_big_fix ';
	  $query_penerimaan = ' ';
	  $query_pengeluaran[1] = ' ';
	  $query_pengeluaran[2] = ' ';
	  $query_pengeluaran[3] = ' ';
	  $query_pengeluaran[4] = ' ';
	  
	  $itr = 0;
	  foreach($array_date as $array_dates){
		  
		  $dt = strtotime($array_dates);
		  
		  $html_day .= '<th>'.date("d/M/y",$dt).'</th>';
		  $query_head .= "
					IF(p_".$itr.".amount_mutasi_fix IS NULL,0,p_".$itr.".amount_mutasi_fix)AS pemasukan_".$itr.",IF(p_".$itr.".mutasi_big_fix IS NULL,0,p_".$itr.".mutasi_big_fix) AS pemasukan_big_".$itr.",
						";

					// $query_head .= "
					// IF(p_".$itr.".amount_mutasi_fix IS NULL,0,p_".$itr.".amount_mutasi_fix)AS pemasukan_".$itr.",IF(p_".$itr.".mutasi_big_fix IS NULL,0,p_".$itr.".mutasi_big_fix) AS pemasukan_big_".$itr.",
					// IF(l1_".$itr.".amount_mutasi_fix IS NULL,0,l1_".$itr.".amount_mutasi_fix)AS pengeluaran_01_".$itr.",IF(l1_".$itr.".mutasi_big_fix IS NULL,0,l1_".$itr.".mutasi_big_fix) AS pengeluaran_big_01_".$itr.",
					// IF(l2_".$itr.".amount_mutasi_fix IS NULL,0,l2_".$itr.".amount_mutasi_fix)AS pengeluaran_02_".$itr.",IF(l2_".$itr.".mutasi_big_fix IS NULL,0,l2_".$itr.".mutasi_big_fix) AS pengeluaran_big_02_".$itr.",
					// IF(l3_".$itr.".amount_mutasi_fix IS NULL,0,l3_".$itr.".amount_mutasi_fix)AS pengeluaran_03_".$itr.",IF(l3_".$itr.".mutasi_big_fix IS NULL,0,l3_".$itr.".mutasi_big_fix) AS pengeluaran_big_03_".$itr.",
					// IF(l4_".$itr.".amount_mutasi_fix IS NULL,0,l4_".$itr.".amount_mutasi_fix)AS pengeluaran_04_".$itr.",IF(l4_".$itr.".mutasi_big_fix IS NULL,0,l4_".$itr.".mutasi_big_fix) AS pengeluaran_big_04_".$itr.",
					// ";
			
			// $query_sum .=" + IF(p_".$itr.".amount_mutasi_fix IS NULL,0,p_".$itr.".amount_mutasi_fix) - IF(l1_".$itr.".amount_mutasi_fix IS NULL,0,l1_".$itr.".amount_mutasi_fix) -IF(l2_".$itr.".amount_mutasi_fix IS NULL,0,l2_".$itr.".amount_mutasi_fix) - IF(l3_".$itr.".amount_mutasi_fix IS NULL,0,l3_".$itr.".amount_mutasi_fix) - IF(l4_".$itr.".amount_mutasi_fix IS NULL,0,l4_".$itr.".amount_mutasi_fix) ";
			// $query_big .="  + IF(p_".$itr.".mutasi_big_fix IS NULL,0,p_".$itr.".mutasi_big_fix) - IF(l1_".$itr.".mutasi_big_fix IS NULL,0,l1_".$itr.".mutasi_big_fix) -IF(l2_".$itr.".mutasi_big_fix IS NULL,0,l2_".$itr.".mutasi_big_fix) - IF(l3_".$itr.".mutasi_big_fix IS NULL,0,l3_".$itr.".mutasi_big_fix) - IF(l4_".$itr.".mutasi_big_fix IS NULL,0,l4_".$itr.".mutasi_big_fix) ";		
		
			$query_penerimaan .="
				LEFT JOIN 
					(
					SELECT a.id_stock_awal,
					IF(a.amount_mutasi IS NULL,0,a.amount_mutasi) AS amount_mutasi_fix,
					IF(a.mutasi_big IS NULL,0,a.mutasi_big) AS mutasi_big_fix,
					IF(a.qty_sol IS NULL,0,a.qty_sol) AS qty_sol_fix,
					IF(a.qty_sol_big IS NULL,0,a.qty_sol_big)AS qty_sol_big_fix,
					IF(a.qty_bonus IS NULL,0,a.qty_bonus)AS qty_bonus_fix,
					IF(a.qty_bonus_big IS NULL,0,a.qty_bonus_big) AS qty_bonus_big_fix,
					IF(a.qty_titip IS NULL,0,a.qty_titip) AS qty_titip_fix,
					IF(a.qty_titip_big IS NULL,0,a.qty_titip_big) AS qty_titip_big_fix
					FROM ( SELECT id_stock_awal,
					IF(SUM(a.`amount_mutasi`) IS NULL,0,SUM(a.`amount_mutasi`)) amount_mutasi, 
					IF(SUM(a.`mutasi_big`) IS NULL,0,SUM(a.`mutasi_big`)) mutasi_big,
					IF(SUM(a.`qty_sol`) IS NULL,0,SUM(a.`qty_sol`)) qty_sol,
					IF(SUM(a.`qty_sol_big`) IS NULL,0,SUM(a.`qty_sol_big`)) qty_sol_big,
					IF(SUM(a.`qty_bonus`) IS NULL,0,SUM(a.`qty_bonus`)) qty_bonus,
					IF(SUM(a.`qty_bonus_big`) IS NULL,0,SUM(a.`qty_bonus_big`)) qty_bonus_big,
					IF(SUM(a.`qty_titip`) IS NULL,0,SUM(a.`qty_titip`)) qty_titip,
					IF(SUM(a.`qty_titip_big`) IS NULL,0,SUM(a.`qty_titip_big`)) qty_titip_big
					FROM t_mutasi a 
					WHERE a.`type_mutasi`=0 AND a.`date_mutasi` = '".$array_dates."'
					GROUP BY a.`id_stock_awal`) a )p_".$itr." ON a.id_mat=p_".$itr.".id_stock_awal
			";
			
			for($tyr = 1;$tyr < 5; $tyr++){
				
				$query_pengeluaran[$tyr] .="
				LEFT JOIN 
					(
					SELECT a.id_stock_awal,
					IF(a.amount_mutasi IS NULL,0,a.amount_mutasi) AS amount_mutasi_fix,
					IF(a.mutasi_big IS NULL,0,a.mutasi_big) AS mutasi_big_fix,
					IF(a.qty_sol IS NULL,0,a.qty_sol) AS qty_sol_fix,
					IF(a.qty_sol_big IS NULL,0,a.qty_sol_big)AS qty_sol_big_fix,
					IF(a.qty_bonus IS NULL,0,a.qty_bonus)AS qty_bonus_fix,
					IF(a.qty_bonus_big IS NULL,0,a.qty_bonus_big) AS qty_bonus_big_fix,
					IF(a.qty_titip IS NULL,0,a.qty_titip) AS qty_titip_fix,
					IF(a.qty_titip_big IS NULL,0,a.qty_titip_big) AS qty_titip_big_fix
					FROM ( SELECT id_stock_awal,
					IF(SUM(a.`amount_mutasi`) IS NULL,0,SUM(a.`amount_mutasi`)) amount_mutasi, 
					IF(SUM(a.`mutasi_big`) IS NULL,0,SUM(a.`mutasi_big`)) mutasi_big,
					IF(SUM(a.`qty_sol`) IS NULL,0,SUM(a.`qty_sol`)) qty_sol,
					IF(SUM(a.`qty_sol_big`) IS NULL,0,SUM(a.`qty_sol_big`)) qty_sol_big,
					IF(SUM(a.`qty_bonus`) IS NULL,0,SUM(a.`qty_bonus`)) qty_bonus,
					IF(SUM(a.`qty_bonus_big`) IS NULL,0,SUM(a.`qty_bonus_big`)) qty_bonus_big,
					IF(SUM(a.`qty_titip`) IS NULL,0,SUM(a.`qty_titip`)) qty_titip,
					IF(SUM(a.`qty_titip_big`) IS NULL,0,SUM(a.`qty_titip_big`)) qty_titip_big
					FROM t_mutasi a 
					JOIN d_penjualan b ON a.id_mutasi=b.mutasi_id
					JOIN t_penjualan c ON b.id_penjualan=c.id_penjualan
					JOIN t_customer d ON c.`id_customer`=d.id_t_cust
					WHERE a.`type_mutasi`=1 AND a.`date_mutasi` = '".$array_dates."' AND d.region=".$tyr."
					GROUP BY a.`id_stock_awal`) a )p_".$itr." ON a.id_mat=p_".$itr.".id_stock_awal
				";
				
			}
			
			
			
		 $itr++; 
	  }
	  
	  //print_r($query_sum);die;
	  
	 // $query_full =  $query_head.' '.$query_sum.' as saldo_akhir, '.$query_big.' as saldo_akhir_big
	  $query_full =  $query_head.' "a" 
	   FROM m_material a
					 JOIN `m_uom` uom ON a.`unit_terkecil` = uom.`id_uom`
					 JOIN `t_eksternal` ek ON a.`dist_id` = ek.`id`
					 LEFT JOIN `t_uom_convert` uc ON uom.id_uom = uc.`id_uom`
					 LEFT JOIN (
					SELECT a.id_stock_awal,
					IF(a.amount_mutasi IS NULL,0,a.amount_mutasi)-IF(b.amount_mutasi IS NULL,0,b.amount_mutasi) AS amount_mutasi_fix,
					IF(a.mutasi_big IS NULL,0,a.mutasi_big)-IF(b.mutasi_big IS NULL,0,b.mutasi_big) AS mutasi_big_fix,
					IF(a.qty_sol IS NULL,0,a.qty_sol)-IF(b.qty_sol IS NULL,0,b.qty_sol) AS qty_sol_fix,
					IF(a.qty_sol_big IS NULL,0,a.qty_sol_big)-IF(b.qty_sol_big IS NULL,0,b.qty_sol_big) AS qty_sol_big_fix,
					IF(a.qty_bonus IS NULL,0,a.qty_bonus)-IF(b.qty_bonus IS NULL,0,b.qty_bonus) AS qty_bonus_fix,
					IF(a.qty_bonus_big IS NULL,0,a.qty_bonus_big)-IF(b.qty_bonus_big IS NULL,0,b.qty_bonus_big) AS qty_bonus_big_fix,
					IF(a.qty_titip IS NULL,0,a.qty_titip)-IF(b.qty_titip IS NULL,0,b.qty_titip) AS qty_titip_fix,
					IF(a.qty_titip_big IS NULL,0,a.qty_titip_big)-IF(b.qty_titip_big IS NULL,0,b.qty_titip_big) AS qty_titip_big_fix
					FROM ( SELECT id_stock_awal,
					SUM(a.`amount_mutasi`) amount_mutasi, 
					SUM(a.`mutasi_big`) mutasi_big,
					SUM(a.`qty_sol`) qty_sol,
					SUM(a.`qty_sol_big`) qty_sol_big,
					SUM(a.`qty_bonus`) qty_bonus,
					SUM(a.`qty_bonus_big`) qty_bonus_big,
					SUM(a.`qty_titip`) qty_titip,
					SUM(a.`qty_titip_big`) qty_titip_big
					FROM t_mutasi a 
					WHERE a.`type_mutasi`=0 AND a.`date_mutasi` < "'.$params['start_time'].'"
					GROUP BY a.`id_stock_awal`) a LEFT JOIN ( SELECT id_stock_awal,
					SUM(a.`amount_mutasi`) amount_mutasi, 
					SUM(a.`mutasi_big`) mutasi_big,
					SUM(a.`qty_sol`) qty_sol,
					SUM(a.`qty_sol_big`) qty_sol_big,
					SUM(a.`qty_bonus`) qty_bonus,
					SUM(a.`qty_bonus_big`) qty_bonus_big,
					SUM(a.`qty_titip`) qty_titip,
					SUM(a.`qty_titip_big`) qty_titip_big
					FROM t_mutasi a
					WHERE a.`type_mutasi`=1 AND a.`date_mutasi` < "'.$params['start_time'].'"
					GROUP BY a.`id_stock_awal`) b ON a.id_stock_awal=b.id_stock_awal) b ON a.id_mat=b.id_stock_awal
					
					
	  ';
	  
	 //echo $query_full;die;
	  
	   $data_r = $this->stock_model->get_stock_data($query_full,$query_penerimaan);
	   $data_r1 = $this->stock_model->get_stock_data($query_full,$query_pengeluaran[1]);
	   $data_r2 = $this->stock_model->get_stock_data($query_full,$query_pengeluaran[2]);
	   $data_r3 = $this->stock_model->get_stock_data($query_full,$query_pengeluaran[3]);
	   $data_r4 = $this->stock_model->get_stock_data($query_full,$query_pengeluaran[4]);
	 //print_r($data_r);die;
	  
	  $html = "<tr>
                  <th rowspan=2>Kode</th>
                  <th rowspan=2 width='100'>Produk</th>
                  <th rowspan=2>Kemasan</th>
                  <th rowspan=2 width='150px'>Principal</th>
				  <th rowspan=2>Box per LT/KG</th>
                  <th colspan=2>Stock Awal </th>
                  <th colspan=".count($array_date).">Penerimaan </th>
                  <th colspan=".count($array_date).">DO Wilayah 1</th>
                  <th colspan=".count($array_date).">DO Wilayah 2</th>
                  <th colspan=".count($array_date).">DO Wilayah 3</th>
                  <th colspan=".count($array_date).">DO Office</th>
				  <th colspan=2>Stok Akhir </th>
                  <th rowspan=2 width='150px'>Option</th>
                </tr>
				<tr>
					<th>Lt/Kg</th>
					<th>Box</th>
					".$html_day."
					".$html_day."
					".$html_day."
					".$html_day."
					".$html_day."
					<th>Lt/Kg</th>
					<th>Box</th>
				</tr>";
	
	$html_body = '';
	
	 
	$rtr = 0;
	foreach($data_r as $v){
		
		 $button_mutasi = '<div class="btn-group"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Mutasi" onClick="editmaterial(\'' . $v['id_mat'] . '\')"><i class="fa fa-history"></i></button></div>';

			$button_detail = '<div class="btn-group"><button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Detail" onClick="detailmaterial(\'' . $v['id_mat'] . '\')"><i class="fa fa-search"></i></button></div>';

      $button = $button_detail . $button_mutasi;
		
		$penerimaan_html = "";
		$pengeluaran_01_html = "";
		$pengeluaran_02_html = "";
		$pengeluaran_03_html = "";
		$pengeluaran_04_html = "";
		
		$pmi = 0;
		foreach($array_date as $array_datess){
			
			$penerimaan_html .= "<td>".number_format($v['pemasukan_'.$pmi]/$v['unit_box'],2,',','.')."</td>";
			$pengeluaran_01_html .= "<td>".number_format($data_r1[$rtr]['pemasukan_'.$pmi]/$v['unit_box'],2,',','.')."</td>";
			$pengeluaran_02_html .= "<td>".number_format($data_r2[$rtr]['pemasukan_'.$pmi]/$v['unit_box'],2,',','.')."</td>";
			$pengeluaran_03_html .= "<td>".number_format($data_r3[$rtr]['pemasukan_'.$pmi]/$v['unit_box'],2,',','.')."</td>";
			$pengeluaran_04_html .= "<td>".number_format($data_r4[$rtr]['pemasukan_'.$pmi]/$v['unit_box'],2,',','.')."</td>";
			
			$saldo_akhir_a = $v['saldo_awal'] + $v['pemasukan_'.$pmi] - $data_r1[$rtr]['pemasukan_'.$pmi] - $data_r2[$rtr]['pemasukan_'.$pmi] -  $data_r3[$rtr]['pemasukan_'.$pmi] - $data_r4[$rtr]['pemasukan_'.$pmi];
			$saldo_akhir_a_big = $v['saldo_awal_big'] + $v['pemasukan_big_'.$pmi] - $data_r1[$rtr]['pemasukan_big_'.$pmi] - $data_r2[$rtr]['pemasukan_big_'.$pmi] - $data_r3[$rtr]['pemasukan_big_'.$pmi] - $data_r4[$rtr]['pemasukan_big_'.$pmi];
			
			$pmi++;
		}
		
		 $html_body .= "<tr>
                  <td>".$v['stock_code']."</td>
                  <td>".$v['stock_name']."</td>
                  <td>".$v['base_qty'].' '.$v['uom_symbol']."</td>
                  <td>".$v['name_eksternal']."</th>
				  <th>".number_format($v['box_per_lt'],2,',','.')."</td>
                  <td>".number_format($v['saldo_awal'],2,',','.')."</td>
                  <td>".number_format($v['saldo_awal_big'],2,',','.')."</td>
				  ".$penerimaan_html."
                  ".$pengeluaran_01_html."
                  ".$pengeluaran_02_html."
                  ".$pengeluaran_03_html."
                  ".$pengeluaran_04_html."
				  <td>".number_format($saldo_akhir_a,2,',','.')."</td>
				  <td>".number_format($saldo_akhir_a_big,2,',','.')."</td>
                  <th >".$button."</th>
                </tr>";
		
		$rtr++;
	}
	  
	// print_r($array_date);die;
	  
	  $result['table'] = $html;
	  $result['body'] = $html_body;
	  $result['data'] = $data_r;
		
	$this->output->set_content_type('application/json')->set_output(json_encode($result));
  }
  
  public function edit()
  {
    $data = array(
      'id' => $this->Anti_sql_injection($this->input->post('iditems', TRUE)),
    );
    $data_r = $this->stock_model->get_material($data['id']);

    // $unit = $this->stock_model->get_unit();?
    // $Gudang = $this->stock_model->get_distributor();

    $data = array(
      'stok'         => $data_r,
      // 'unit' 				=> $unit,
      // 'Gudang' 			=> $Gudang
    );

    $this->template->load('maintemplate', 'asset/views/edit_modal_view', $data);
  }
  public function detail()
  {
    $data = array(
      'id' => $this->Anti_sql_injection($this->input->post('iditems', TRUE)),
    );
    $result = $this->stock_model->get_material($data['id']);

	//print_r($result);die;

    // $unit = $this->stock_model->get_unit();?
    // $Gudang = $this->stock_model->get_distributor();

    $data = array(
      'stok'         => $result,
      // 'unit' 				=> $unit,
      // 'Gudang' 			=> $Gudang
    );

    $this->template->load('maintemplate', 'asset/views/mutasi_view', $data);
  }

  public function edit_material()
  {
    $this->form_validation->set_rules('id_mat', 'Unit', 'trim|required');
    $this->form_validation->set_rules('type_mutasi', 'Type Mutasi', 'trim|required');
    $this->form_validation->set_rules('amount_mutasi', 'Jumlah Mutasi', 'trim|required');
    $this->form_validation->set_rules('date_mutasi', 'Jumlah Mutasi', 'trim|required');
    $this->form_validation->set_rules('qty', 'Qty Awal Barang', 'trim|required');


    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));

      $result = array('success' => false, 'message' => $msg);
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      $id_mat = $this->Anti_sql_injection($this->input->post('id_mat', TRUE));
      $type_mutasi = $this->Anti_sql_injection($this->input->post('type_mutasi', TRUE));
      $note = $this->Anti_sql_injection($this->input->post('note', TRUE));
      $amount_mutasi = $this->Anti_sql_injection($this->input->post('amount_mutasi', TRUE));
      $qty = (float) $this->Anti_sql_injection($this->input->post('qty', TRUE));
	  
	  $conv = $this->stock_model->get_convertion($id_mat);
	  
	 // print_r($conv);die;
	  
      if ($type_mutasi == 0){
        $qty = $qty + $amount_mutasi;
        $qty_sm = ($qty* $conv[0]['mult']) + ($amount_mutasi* $conv[0]['mult']);
      }else{
        $qty = $qty - $amount_mutasi;
         $qty_sm = ($qty* $conv[0]['mult']) - ($amount_mutasi* $conv[0]['mult']);
	  }
      $data = array(
        'id_mat'          => $id_mat,
        'id_stock_awal'   => $id_mat,
        'id_stock_akhir'  => $id_mat,
        'type_mutasi'     => $type_mutasi,
        'amount_mutasi'   => $amount_mutasi,
        'amount_mutasi_sm'=> $amount_mutasi * $conv[0]['mult'],
        'qty'             => $qty,
        'note'             => $note,
        'qty_sm'          => $qty_sm,
        'date_mutasi'     =>  date('Y-m-d H:i:s', strtotime($this->Anti_sql_injection($this->input->post('date_mutasi', TRUE)) . ' ' . date('H:i:s'))),
        'user_id'         => $this->session->userdata['logged_in']['user_id'],
      );
      $result = $this->stock_model->add_mutasi($data);
      $result = $result + $this->stock_model->update_qty_material($data);
      if (count($result) > 1) {
        $msg = 'Berhasil mengubah data stock ke database';

        $this->log_activity->insert_activity('insert', 'Berhasil Update Stock dan Mutasi dengan id Material ' . $id_mat);
        $results = array(
          'success' => true,
          'message' => $msg
        );
      } else {
        $msg = 'Gagal mengubah data stock ke database';

        $this->log_activity->insert_activity('insert', 'Gagal Update Stock dan Mutasi dengan id material = ' . $id_mat);
        $results = array(
          'success' => false,
          'message' => $msg
        );
      }

      $this->output->set_content_type('application/json')->set_output(json_encode($results), true);
    }
  }

}
