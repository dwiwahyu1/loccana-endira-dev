<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto
  }

  .scroll-overflow {
    min-height: 650px
  }

  #modal-distributor::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="product-payment-inner-st">
          <ul id="myTabedu1" class="tab-review-design">
            <li class="active"><a href="#description"><?php echo $stok[0]['stock_name'].' '.$stok[0]['base_qty'].' '.$stok[0]['uom_symbol']; ?></a></li>
          </ul>
          <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="review-content-section">
						
                    <form class="form-horizontal form-label-left" id="edit_material" role="form" action="<?php echo base_url('items/edit_material'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
                      <input data-parsley-maxlength="255" type="hidden" id="id_mat" name="id_mat" class="form-control" placeholder="Kode Stok" value="<?php if (isset($stok[0]['id_mat'])) {
                                                                                                                                                        echo $stok[0]['id_mat'];
                                                                                                                                                      } ?>" autocomplete="off" required="required" readOnly>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Kode Stok <span class="required"></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input data-parsley-maxlength="255" type="text" id="kode_stok" name="kode_stok" class="form-control" placeholder="Kode Stok" value="<?php if (isset($stok[0]['stock_code'])) {
                                                                                                                                                                echo $stok[0]['stock_code'];
                                                                                                                                                              } ?>" autocomplete="off" required="required" readOnly>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Stok <span class="required"></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input data-parsley-maxlength="255" type="text" id="nama_stok" name="nama_stok" class="form-control" placeholder="Nama Stok" value="<?php if (isset($stok[0]['stock_name'])) {
                                                                                                                                                                echo $stok[0]['stock_name'];
                                                                                                                                                              } ?>" autocomplete="off" required="required" readOnly>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Deskripsi Stok <span class="required"></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <textarea data-parsley-maxlength="255" id="desk_stok" name="desk_stok" class="form-control" autocomplete="off" readOnly><?php if (isset($stok[0]['stock_description'])) {
                                                                                                                                            echo $stok[0]['stock_description'];
                                                                                                                                          } ?> </textarea>
                        </div>
                      </div>


                        <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Satuan <span class="required"></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input type="text" id="qty" name="qty" class="form-control" placeholder="Quantity" value="<?php if (isset($stok[0]['uom_symbol'])) {
                                                                                                                        echo $stok[0]['uom_symbol'];
                                                                                                                      } ?>" autocomplete="off" required  readOnly>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Qty per Box <span class="required"></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input type="number" id="qty" name="qty" class="form-control" placeholder="Quantity" value="<?php if (isset($stok[0]['unit_box'])) {
                                                                                                                        echo number_format($stok[0]['unit_box'],0,'.',',');
                                                                                                                      } ?>" autocomplete="off" required step="0.01" readOnly>
                        </div>
                      </div>

  
						<div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama"><span class="required"></span></label>
						    <ul id="myTabedu1" class="tab-review-design">
						  <li class="active"><a href="#description">Qty Sisa</a></li>
						  </ul>
                      </div>
                   
				     <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Qty <span class="required"></span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input type="text" id="qty" name="qty" class="form-control" placeholder="Quantity" value="<?php if (isset($stok[0]['qty'])) {
                                                                                                                        echo number_format($stok[0]['qty']/$stok[0]['unit_box'],2,'.',',').' Box';
                                                                                                                      } ?>" autocomplete="off" required step="0.01" readOnly>
                        </div>
						  <div class="col-md-4 col-sm-4 col-xs-12">
                          <input type="text" id="qty" name="qty" class="form-control" placeholder="Quantity" value="<?php if (isset($stok[0]['qty_big'])) {
                                                                                                                        echo number_format($stok[0]['qty_big'],2,'.',',').' Lt / Kg';
                                                                                                                      } ?>" autocomplete="off" required step="0.01" readOnly>
                        </div>
                      </div>
				   
				   
						   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Qty Normal <span class="required"></span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input type="text" id="qty" name="qty" class="form-control" placeholder="Quantity" value="<?php if (isset($stok[0]['qty_sol'])) {
                                                                                                                        echo number_format($stok[0]['qty_sol']/$stok[0]['unit_box'],2,'.',',').' Box';
                                                                                                                      } ?>" autocomplete="off" required step="0.01" readOnly>
                        </div>
						 <div class="col-md-4 col-sm-4 col-xs-12">
                          <input type="text" id="qty" name="qty" class="form-control" placeholder="Quantity" value="<?php if (isset($stok[0]['qty_sol_big'])) {
                                                                                                                        echo number_format($stok[0]['qty_sol_big'],2,'.',',').' Lt / Kg';
                                                                                                                      } ?>" autocomplete="off" required step="0.01" readOnly>
                        </div>
                      </div>
					  
					  
					     <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Qty Titipan <span class="required"></span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input type="text" id="qty" name="qty" class="form-control" placeholder="Quantity" value="<?php if (isset($stok[0]['qty_titip'])) {
                                                                                                                        echo number_format($stok[0]['qty_titip']/$stok[0]['unit_box'],2,'.',',').' Box';
                                                                                                                      } ?>" autocomplete="off" required step="0.01" readOnly>
                        </div>
						 <div class="col-md-4 col-sm-4 col-xs-12">
                          <input type="text" id="qty" name="qty" class="form-control" placeholder="Quantity" value="<?php if (isset($stok[0]['qty_titip_big'])) {
                                                                                                                        echo number_format($stok[0]['qty_titip_big'],2,'.',',').' Lt / Kg';
                                                                                                                      } ?>" autocomplete="off" required step="0.01" readOnly>
                        </div>
                      </div>
					  
					  
					     <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Qty Bonus <span class="required"></span></label>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input type="text" id="qty" name="qty" class="form-control" placeholder="Quantity" value="<?php if (isset($stok[0]['qty_bonus'])) {
                                                                                                                        echo number_format($stok[0]['qty_bonus']/$stok[0]['unit_box'],2,'.',',').' Box';
                                                                                                                      } ?>" autocomplete="off" required step="0.01" readOnly>
                        </div>
						 <div class="col-md-4 col-sm-4 col-xs-12">
                          <input type="text" id="qty" name="qty" class="form-control" placeholder="Quantity" value="<?php if (isset($stok[0]['qty_bonus_big'])) {
                                                                                                                        echo number_format($stok[0]['qty_bonus_big'],2,'.',',').' Lt / Kg';
                                                                                                                      } ?>" autocomplete="off" required step="0.01" readOnly>
                        </div>
                      </div>
				   

                    </form><!-- /page content -->
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
	  
    </div>
  </div>
</div>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="product-payment-inner-st">
          <ul id="myTabedu1" class="tab-review-design">
            <li class="active"><a href="#description">History Mutasi Item</a></li>
          </ul>
          <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="review-content-section">
                    <table id="listitems" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Kode Item</th>
                          <th>Nama Item</th>
                          <th>Tanggal Mutasi</th>
                          <th>Type Mutasi</th>
                          <th>Jumlah Mutasi</th>
                          <th>Nama Priciple</th>
                          <th>Keterangan</th>
                          <!-- <th>Status</th> -->
                          <!-- <th>Option</th> -->
                        </tr>
                      </thead>
                      <tbody></tbody>
                    </table>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
	  
    </div>
  </div>
</div>

<div id="PrimaryModalalert" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-close-area modal-close-df">
        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
      </div>
      <div class="modal-body">
        <i class="educate-icon educate-checked modal-check-pro"></i>
        <h2>Data Berhasil Dirubah</h2>
        <p></p>
      </div>
      <!-- <div class="modal-footer">
                                        <a data-dismiss="modal" href="#" onClick="back()">Ya</a>
                                    </div>-->
    </div>
  </div>
</div>


<script>
  function clearform() {

    $('#edit_material').trigger("reset");

  }

  function back() {

    window.location.href = "<?php echo base_url() . 'stock'; ?>";

  }


  $(document).ready(function() {
    $('#date_mutasi').datepicker({
      isRTL: true,
      format: "dd-M-yyyy",
      autoclose: true,
      todayHighlight: true,
      changeYear: true,
      minDate: '-3M',
      maxDate: '+30D',
    });

    $(document).ready(function() {

      $("#listitems").dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo base_url() . 'stock/lists_mutasi/'.$stok[0]['id_mat']; ?>",
        "searchDelay": 700,
        "responsive": true,
        "lengthChange": false,
        "info": false,
        "bSort": false,
        "dom": 'l<"toolbar">frtip',
        "initComplete": function() {
        },
        "columnDefs": [{
          "targets": [0],
          "className": 'dt-body-right',
          "width": "5%"
        },{
          "targets": [3,4,5],
          "className": 'dt-body-right'
        }]
      });
    });
  });
</script>