<?php

use function PHPSTORM_META\type;

defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

class Report_piutang extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('report_piutang/report_piutang_model', 'reportPiutangModel');
		$this->load->library('log_activity');
		$this->load->library('priv');
	}

	public function index()
	{
		$customer_list = $this->reportPiutangModel->get_customer_list();
		$region_list = $this->reportPiutangModel->get_region_list();

		$this->template->load('maintemplate', 'report_piutang/views/index', compact('customer_list', 'region_list'));
	}

	public function rows()
	{
		$params = [
			'customer_id' => anti_sql_injection($this->input->post('customer_id')),
			'region_id' => anti_sql_injection($this->input->post('region_id')),
			'filter_month' => anti_sql_injection($this->input->post('filter_month')),
			'filter_year' => anti_sql_injection($this->input->post('filter_year')),
		];
		
		//print_r($params);die;

		$list = $this->reportPiutangModel->list($params);
		$group_rows = array_group_by_column_value($list['data'], 'id_t_cust');

		//print_r($group_rows);die;
		$rows = $this->load->view('../modules/report_piutang/views/rows', [
			'group_rows' => $group_rows,
			'params' => $params,
			'currentYearMonth' => date('Y-m'),
			'now' => Carbon::now(),
		], true);

		$this->output->set_content_type('application/json')->set_output(json_encode([
			'data' => [
				'rows' => $rows,
			],
		]));
	}

	public function list()
	{
		$sess_user_id = !empty($_GET['sess_user_id']) ? $_GET['sess_user_id'] : NULL;
		$sess_token = !empty($_GET['sess_token']) ? $_GET['sess_token'] : NULL;
		$customer_id = !empty($_GET['customer_id']) ? $_GET['customer_id'] : NULL;
		$region_id = !empty($_GET['region_id']) ? $_GET['region_id'] : NULL;
		$filter = !empty($_GET['filter']) ? $_GET['filter'] : NULL;
		$filter_month = !empty($_GET['filter_month']) ? $_GET['filter_month'] : NULL;
		$filter_year = !empty($_GET['filter_year']) ? $_GET['filter_year'] : NULL;
		$draw = $this->input->get_post('draw') != FALSE ? $this->input->get_post('draw') : 1;
		$length = $this->input->get_post('length') != FALSE ? $this->input->get_post('length') : 10;
		$start = $this->input->get_post('start') != FALSE ? $this->input->get_post('start') : 0;

		$order_fields = array('id_hp', 'no_payment', 'total', 'tgl_terbit', 'keterangan', 'id_hp', 'id_hp'); // , 'COST'

		$order = $this->input->get_post('order');
		$order_dir = !empty($order[0]['dir']) ? $order[0]['dir'] : 'desc';
		$order_column = !empty($order[0]['column']) ? $order[0]['column'] : 0;

		$params['limit']		= (int) $length;
		$params['offset']		= (int) $start;
		$params['order_column']	= $order_fields[$order_column];
		$params['order_dir']	= $order_dir;
		$params['sess_user_id']	= $sess_user_id;
		$params['sess_token']	= $sess_token;
		$params['filter']		= $filter;
		$params['filter_month']	= $filter_month;
		$params['filter_year']	= $filter_year;
		$params['searchtxt']	= $_GET['search']['value'] ?? null;
		$params['customer_id']	= $customer_id;
		$params['region_id']	= $region_id;

		// print_r($params);die;
		$priv = $this->priv->get_priv();

		$list = $this->reportPiutangModel->list($params);
		
		//print_r($list);die;
		$data = [];
		$currentYearMonth = date('Y-m');
		$now = Carbon::now();
		$groupedRows = array_group_by_column_value($list['data'], 'id_t_cust');

		foreach ($groupedRows as $key => $group) {

			$total_qty = 0;
			$total_saldo = 0;
			$total_bln_berjalan = 0;

			foreach ($group as $row) {
				$blnBerjalan = false;
				if (strpos($row['date_penjualan'], $currentYearMonth) === 0 || strpos($row['date_penjualan'], $currentYearMonth) > 0) {
					$blnBerjalan = true;
				}

				$umur = null;
				if (isset($row['tanggal_invoice']) && !empty($row['tanggal_invoice'])) {
					$umur = $now->diffInDays($row['tanggal_invoice']);
				}

				array_push($data, [
					$row['cust_name'] ?? '-', // customer
					$row['tanggal_invoice'] ?? '-', // tanggal pesan
					$row['no_invoice'] ?? '-', //faktur 
					$row['stock_name'] ?? '-', // produk
					($row['base_qty'] ?? '') . ($row['uom_symbol'] ?? ''), // kemasan
					indonesia_currency_format($row['qty']), //qty
					$blnBerjalan ? '-' : indonesia_currency_format($row['price']), //saldo
					$blnBerjalan ? indonesia_currency_format($row['price']) : '-', //bln berjalan
					$umur ?? '-', //umur
				]);

				$total_qty += ($row['qty'] ?? 0);
				$total_saldo += ($blnBerjalan ? 0 : $row['price']);
				$total_bln_berjalan += ($blnBerjalan ? $row['price'] : 0);
			}

			array_push($data, [
				'', // customer
				'', // tanggal pesan
				'', // faktur
				'', // produk
				'', // kemasan
				indonesia_currency_format($total_qty), // qty
				indonesia_currency_format($total_saldo), // saldo
				indonesia_currency_format($total_bln_berjalan), // bln berjalan
				'', // umur
			]);
		}

		$result = [
			'data' => $data,
			'recordsTotal' => $list['total'],
			'recordsFiltered' => $list['total_filtered'],
			'draw' => $draw,
		];

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function excel()
	{
		$params = [
			'customer_id' => anti_sql_injection($this->input->post('customer_id')),
			'region_id' => anti_sql_injection($this->input->post('region_id')),
			'filter_month' => anti_sql_injection($this->input->post('filter_month')),
			'filter_year' => anti_sql_injection($this->input->post('filter_year')),
		];

		$this->load->library('excel');

		$objPHPExcel = new PHPExcel();

		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'Customer')
			->setCellValue('B1', 'Tanggal Pesan')
			->setCellValue('C1', 'Faktur')
			->setCellValue('D1', 'Produk')
			->setCellValue('E1', 'Kemasan')
			->setCellValue('F1', 'Qty')
			->setCellValue('G1', 'Saldo')
			->setCellValue('H1', 'Bln Berjalan')
			->setCellValue('I1', 'Umur')
			//
		;

		$params['print'] = true;

		$list = $this->reportPiutangModel->list($params);
		
		
		$start = 2;
		$currentYearMonth = date('Y-m');
		$now = Carbon::now();
		$group_rows = array_group_by_column_value($list['data'], 'id_t_cust');

		//print_r($group_rows);die;

		$grand_total_qty = 0;
		$grand_total_saldo = 0;
		$grand_total_bln_berjalan = 0;

		if (isset($group_rows) && is_array($group_rows) && count($group_rows) > 0) {
			foreach ($group_rows as $key => $rows) {

				$total_qty = 0;
				$total_saldo = 0;
				$total_bln_berjalan = 0;

				foreach ($rows as $row) {

					$blnBerjalan = false;
					if (strpos($row['date_penjualan'], $currentYearMonth) === 0 || strpos($row['date_penjualan'], $currentYearMonth) > 0) {
						$blnBerjalan = true;
					}

					$umur = null;
					if (isset($row['tanggal_invoice']) && !empty($row['tanggal_invoice'])) {
						$umur = $now->diffInDays($row['tanggal_invoice']);
					}

					$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A' . $start, $row['cust_name'] ?? '') // customer
						->setCellValue('B' . $start, $row['tanggal_invoice'] ?? '') // tanggal pesan
						->setCellValue('C' . $start, $row['no_invoice'] ?? '') // faktur
						->setCellValue('D' . $start, $row['stock_name'] ?? '') // produk
						->setCellValue('E' . $start, ($row['base_qty'] ?? '') . ($row['uom_symbol'] ?? '')) // kemasan
						->setCellValue('F' . $start, indonesia_currency_format($row['qty'])) // qty
						->setCellValue('G' . $start, $blnBerjalan ? '' : indonesia_currency_format($row['price'])) // saldo
						->setCellValue('H' . $start, $blnBerjalan ? indonesia_currency_format($row['price']) : '') // bln berjalan
						->setCellValue('I' . $start, $umur ?? '') // umur
						//
					;


					$total_qty += ($row['qty'] ?? 0);
					$total_saldo += ($blnBerjalan ? 0 : $row['price']);
					$total_bln_berjalan += ($blnBerjalan ? $row['price'] : 0);
					$start++;
				}

				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A' . $start, 'Total') // customer
					->setCellValue('B' . $start, '') // tanggal pesan
					->setCellValue('C' . $start, '') // faktur
					->setCellValue('D' . $start, '') // produk
					->setCellValue('E' . $start, '') // kemasan
					->setCellValue('F' . $start, indonesia_currency_format($total_qty)) // qty
					->setCellValue('G' . $start, indonesia_currency_format($total_saldo)) // saldo
					->setCellValue('H' . $start, indonesia_currency_format($total_bln_berjalan)) // bln berjalan
					->setCellValue('I' . $start, '') // umur
					//
				;

				$grand_total_qty += $total_qty;
				$grand_total_saldo += $total_saldo;
				$grand_total_bln_berjalan += $total_bln_berjalan;
				$start++;
			}

			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A' . $start, 'Grand Total') // customer
				->setCellValue('B' . $start, '') // tanggal pesan
				->setCellValue('C' . $start, '') // faktur
				->setCellValue('D' . $start, '') // produk
				->setCellValue('E' . $start, '') // kemasan
				->setCellValue('F' . $start, indonesia_currency_format($grand_total_qty)) // qty
				->setCellValue('G' . $start, indonesia_currency_format($grand_total_saldo)) // saldo
				->setCellValue('H' . $start, indonesia_currency_format($grand_total_bln_berjalan)) // bln berjalan
				->setCellValue('I' . $start, '') // umur
				//
			;
		}


		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename=report_piutang.xls');
		header('Cache-Control: max-age=0');
		// If you’re serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you’re serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		unset($objPHPExcel);
	}
}
