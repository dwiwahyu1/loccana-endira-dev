<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Report_piutang_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function list($params = [])
	{
		// get total
		$totalCount = 0;
		// if (!isset($params['print']) || $params['print'] == false) {
		// 	$totalQueryBuilder = $this->db->select('COUNT(d_penjualan.id_penjualan) AS row_count');
		// 	$totalQueryBuilder = $this->joinedTables($totalQueryBuilder)->limit(1);
		// 	$totalCount = $totalQueryBuilder->get()->row_array()['row_count'];
		// }

		// get filtered data
		$filteredResultCount = 0;
		// if (!isset($params['print']) || $params['print'] == false) {
		// 	$filteredQueryBuilder = $this->db->select('COUNT(d_penjualan.id_penjualan) AS row_count');
		// 	$filteredQueryBuilder = $this->joinedTables($filteredQueryBuilder);
		// 	$filteredQueryBuilder = $this->filterQueries($filteredQueryBuilder, $params)->limit(1);
		// 	$filteredResultCount = $filteredQueryBuilder->get()->row_array()['row_count'];
		// }

		// paginated result
		$paginatedQueryBuilder = $this->db->select('*');
		$paginatedQueryBuilder = $this->joinedTables($paginatedQueryBuilder);
		$paginatedQueryBuilder = $this->filterQueries($paginatedQueryBuilder, $params);
		$paginatedQueryBuilder = $this->sortQueries($paginatedQueryBuilder, $params);

		

		if (isset($params['limit']) && $params['limit'] >= 0 && isset($params['offset'])) {
			$paginatedQueryBuilder = $paginatedQueryBuilder->limit($params['limit'], $params['offset']);
		}

		$paginatedResult = $paginatedQueryBuilder->get()->result_array();

		return [
			'data' => $paginatedResult,
			'total_filtered' => (int) $filteredResultCount,
			'total' => (int) $totalCount,
		];
	}

	protected function joinedTables($queryBuilder)
	{
		return $queryBuilder->from('d_penjualan')
			->join('t_penjualan', 'd_penjualan.id_penjualan = t_penjualan.id_penjualan')
			->join('t_customer', 't_penjualan.id_customer = t_customer.id_t_cust', 'left')
			->join('m_material', 'd_penjualan.id_material = m_material.id_mat', 'left')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom', 'left')
			->join('d_invoice_penjualan', 'd_penjualan.id_dp = d_invoice_penjualan.id_d_penjualan', 'left')
			->join('t_invoice_penjualan', 'd_invoice_penjualan.id_inv_penjualan = t_invoice_penjualan.id_invoice', 'left');
	}

	protected function filterQueries($queryBuilder, $params = [])
	{
		if (isset($params['customer_id']) && !empty($params['customer_id'])) {
			$queryBuilder = $queryBuilder->where(['t_customer.id_t_cust' => ($params['customer_id'] ?? '')]);
		}

		if (isset($params['region_id']) && !empty($params['region_id'])) {
			$queryBuilder = $queryBuilder->where(['t_customer.region' => $params['region_id']]);
		}

		if (($params['filter_month'] ?? false) && ($params['filter_year'] ?? false)) {
			if (!empty(trim($params['filter_month'])) && !empty(trim($params['filter_year']))) {
				$q = implode('-', [$params['filter_year'], $params['filter_month']]);
				$queryBuilder = $queryBuilder->like('t_penjualan.date_penjualan', $q);
			}
		} else if (isset($params['filter_month']) && !empty(trim($params['filter_month']))) {
			$q = '-' . $params['filter_month'] . '-';
			$queryBuilder = $queryBuilder->like('t_penjualan.date_penjualan', $q);
		} else if (isset($params['filter_year']) && !empty(trim($params['filter_year']))) {
			$q = $params['filter_year'] . '-';
			$queryBuilder = $queryBuilder->like('t_penjualan.date_penjualan', $q);
		}



		if (isset($params['searchtxt']) && !empty($params['searchtxt'])) {
			$queryBuilder = $queryBuilder->like('t_customer.cust_name', $params['searchtxt']);
			$queryBuilder = $queryBuilder->or_like('t_invoice_penjualan.faktur_pajak', $params['searchtxt']);
			$queryBuilder = $queryBuilder->or_like('m_material.stock_name', $params['searchtxt']);
		}

		return $queryBuilder;
	}

	protected function sortQueries($queryBuilder, $params = [])
	{
		return $queryBuilder->order_by('t_customer.id_t_cust', 'ASC');
	}

	/**
	 * get list of customer
	 * 
	 * @return array
	 */
	public function get_customer_list()
	{
		$queryBuilder = $this->db
			->select('*')
			->from('t_customer')
			->join('t_region', 't_customer.region = t_region.id_t_region');

		$result = $queryBuilder->get()->result_array();

		return $result;
	}

	/**
	 * get list of region
	 * 
	 * @return array
	 */
	public function get_region_list()
	{
		$queryBuilder = $this->db
			->select('*')
			->from('t_region');

		$result = $queryBuilder->get()->result_array();

		return $result;
	}
}
