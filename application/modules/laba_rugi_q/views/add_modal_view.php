<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto
  }

  .scroll-overflow {
    min-height: 650px
  }

  #modal-distributor::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="product-payment-inner-st">
          <ul id="myTabedu1" class="tab-review-design">
            <li class="active"><a href="#description">Tambah Jurnal</a></li>
          </ul>
          <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="review-content-section">
                    <form class="form-horizontal form-label-left" id="add_uom" role="form" action="<?php echo base_url('jurnal/add_uom'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>

                      <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Coa <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <select name="coa" id="coa" class="form-control" placeholder="Coa">
																	<option value="0" selected="selected" disabled>-- Pilih Coa --</option>
																	<?php
																		foreach($coa as $principals){
																			echo '<option value="'.$principals['id_coa'].'" >'.$principals['coa'].'-'.$principals['keterangan'].'</option>';
																		}
																	?>
																</select>
                        </div>
                      </div>


                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Tanggal <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input data-parsley-maxlength="255" type="text" id="tgl" name="tgl" class="form-control col-md-7 col-xs-12" placeholder="Tanggal" required="required">
                        </div>
                      </div>
					  
					    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Tipe Cash <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <select name="cash" id="cash" class="form-control" placeholder="Pengeluaran">
							<option value='' disabled selected="selected">-- Tipe Cash --</option>
							<option value='0'>Pemasukan</option>
							<option value='1'>Pengeluaran</option>
						  </select>
						  
                        </div>
                      </div>
					  
					    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Jumlah <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input data-parsley-maxlength="255" type="text" id="jumlah" name="jumlah" class="form-control col-md-7 col-xs-12" placeholder="Jumlah" required="required">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Keterangan <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <textarea data-parsley-maxlength="255" type="text" id="ket" name="ket" class="form-control col-md-7 col-xs-12" placeholder="Keterangan" required="required"> </textarea>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Submit</button>
                            <button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()">Batal</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="PrimaryModalalert" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-close-area modal-close-df">
        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
      </div>
      <div class="modal-body">
        <i class="educate-icon educate-checked modal-check-pro"></i>
        <h2>Data Berhasil Disimpan</h2>
        <p>Apakah Anda Ingin Menambah Data UoM Lagi ?</p>
      </div>
      <div class="modal-footer">
        <a data-dismiss="modal" href="#" onClick="back()">Tidak</a>
        <a data-dismiss="modal" href="#" onClick="clearform()">Ya</a>
      </div>
    </div>
  </div>
</div>

<div id="WarningModalftblack" class="modal modal-edu-general Customwidth-popup-WarningModal PrimaryModal-bgcolor fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-close-area modal-close-df">
        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
      </div>
      <div class="modal-body">
        <span class="educate-icon educate-warning modal-check-pro information-icon-pro"></span>
        <h2>Warning!</h2>
        <p id="msg_err">The Modal plugin is a dialog box/popup window that is displayed on top of the current page</p>
      </div>
      <div class="modal-footer footer-modal-admin warning-md">
        <a data-dismiss="modal" href="#">Ok</a>
      </div>
    </div>
  </div>
</div>

<script>
  function clearform() {

    $('#add_uom').trigger("reset");

  }

  function back() {

    window.location.href = "<?php echo base_url() . 'jurnal'; ?>";

  }

  function listdist() {
    var user_id = '0001';
    var token = '093940349';


    $('#datatable_pricipal').DataTable({
      //"dom": 'rtip',
      "bFilter": false,
      "aaSorting": [],
      "bLengthChange": true,
      'iDisplayLength': 10,
      "sPaginationType": "simple_numbers",
      "Info": false,
      "processing": true,
      "serverSide": true,
      "destroy": true,
      "ajax": "<?php echo base_url() . 'jurnal/lists' ?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token,
      "searching": true,
      "language": {
        "decimal": ",",
        "thousands": "."
      },
      "dom": 'l<"toolbar">frtip',
      "initComplete": function() {
        $("div.toolbar").prepend('<div class="btn-group pull-left"><button type="button" class="btn btn-custon-rounded-two btn-primary" > Tambah </button></div>');
      }
    });
  }

  $(document).ready(function() {
$('#coa').select2();

$('#tgl').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		});
    listdist();

    $('#add_uom').on('submit', function(e) {
      // validation code here
      //if(!valid) {
      e.preventDefault();


  swal({
      title: 'Yakin akan Simpan Data ?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak'
    }).then(function() {


      var formData = new FormData();
			formData.append('coa', $('#coa').val());
			formData.append('tgl', $('#tgl').val());
			formData.append('cash', $('#cash').val());
			formData.append('jumlah', $('#jumlah').val());
			formData.append('ket', $('#ket').val());

		  $.ajax({
			type: 'POST',
			url: '<?php echo base_url('jurnal/add_uom'); ?>',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			success: function(response) {

			  //console.log(response);
			  if (response.success == true) {
				// $('#PrimaryModalalert').modal('show');
				swal({
				  title: 'Success!',
				  text: response.message,
				  type: 'success',
				  showCancelButton: false,
				  confirmButtonText: 'Ok'
				}).then(function() {
				  back();
				})
			  } else {
				$('#msg_err').html(response.message);
				$('#WarningModalftblack').modal('show');
			  }
			}
		  });

	  })
      //alert(kode);


      //}
    });
  });
</script>


<!-- /page content -->
">
												<div class="form-group">
													<label></label>
												</div>
											</div>
										</div>
											<input name="int_flo" id="int_flo" type="hidden" class="form-control" placeholder="Qty" value=0; >
									<div id="table_items">
										<div id='row_0' >
											<div class="row" style="border-top-style:solid;">
											<div style="margin-top:10px">
												<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
														<select name="kode_0" id="kode_0" class="form-control"  onChange="change_item(0)" placeholder="Nama Principal">
															<option value="0" selected="selected" disabled>-- Pilih Item --</option>
														</select>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<div class="form-group">
														<input name="qty_0" id="qty_0" type="text" onKeyup="change_sum(0)" class="form-control rupiahs" placeholder="Qty" value=0  >
													</div>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<div class="form-group">
														<label id="iteml_0"></label>
														<label id="iteml2_0"></label>
													</div>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<div class="form-group">
														<input name="qtys_0" id="qtys_0" type="text" onKeyup="change_sum(0)" class="form-control rupiahs" placeholder="Qty" value=0   >
													</div>
												</div>
													<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<div class="form-group">
														<input name="totalqty_0" id="totalqty_0" type="number"  class="form-control" placeholder="Diskon" value=0 readOnly >
													</div>
												</div>
												<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
													<div class="form-group">
														<input name="harga_0" id="harga_0" type="text" onKeyup="change_sum(0)" class="form-control rupiahs" placeholder="Harga" value=0 >
													</div>
												</div>
											
												<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
													<div class="form-group">
														<input name="total_0" id="total_0" type="text" class="form-control rupiahs" placeholder="Total" value=0 readOnly  >
													</div>
												</div>
												
											</div>
											</div>
										</div>
										
									</div>
									<div class="row" style="border-top-style:solid;" id='plus' >
									<br>
										<div class="col-lg-11 col-md-11 col-sm-3 col-xs-12">
										</div>
											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
											
													<div class="form-group">
														<button type="button" class="btn btn-danger waves-effect waves-light tbh_btn" onClick="tambah_row()">+</button>
													</div>
												</div>
									</div>
									<br>
									
									
									<div class="row" style="border-top-style:solid;">
											<div style="margin-top:10px">
												<div class="col-lg-8 col-md-3 col-sm-3 col-xs-12">
													
												</div>
												<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
													<label>Total</label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<label id="grand_total" style="float:right">0</label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
												</div>
											</div>
									</div>
									<div class="row" style="">
											<div style="margin-top:10px">
												<div class="col-lg-8 col-md-3 col-sm-3 col-xs-12">
													
												</div>
												<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
													<label>DPP</label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<label id='subttl2' style="float:right">0</label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
												</div>
											</div>
									</div>
									<div class="row" style="">
											<div style="margin-top:10px">
												<div class="col-lg-8 col-md-3 col-sm-3 col-xs-12">
													
												</div>
												<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
													<label>VAT/PPN</label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<label id="vatppn" style="float:right">0</label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
												</div>
											</div>
									</div>
								
									<div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="payment-adress">
                                                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
																<button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()"> Batal</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                </div>
                               </div> </form>
                        </div>
                    </div>
                </div>
</div>
</div>

                        <div id="ModalAddPo" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-close-area modal-close-df">
                                        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                                    </div>
                                    <div class="modal-body">
                                        <i class="educate-icon educate-checked modal-check-pro"></i>
                                        <h2>Data Berhasil Disimpan</h2>
                                        <p>Apakah Anda Ingin Menambah Data Po Lagi ?</p>
                                    </div>
                                    <div class="modal-footer">
                                        <a data-dismiss="modal" href="#" onClick="back()">Tidak</a>
                                        <a data-dismiss="modal" href="#" onClick="clearform()" >Ya</a>
                                    </div>
                                </div>
                            </div>
                        </div>
						
						<div id="ModalChangePrincipal" class="modal modal-edu-general Customwidth-popup-WarningModal PrimaryModal-bgcolor fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-close-area modal-close-df">
                                        <!--<a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>-->
                                    </div>
                                    <div class="modal-body">
                                        <span class="educate-icon educate-warning modal-check-pro information-icon-pro"></span>
                                        <h2>Warning!</h2>
                                        <p id="msg_err">The Modal plugin is a dialog box/popup window that is displayed on top of the current page</p>
                                    </div>
                                    <div class="modal-footer footer-modal-admin warning-md">
                                        <a data-dismiss="modal" href="#" onClick="okchange()">Ok</a>
                                        <a data-dismiss="modal" href="#" onClick="cancelchange()">Batal</a>
                                    </div>
                                </div>
                            </div>
                        </div>

<script>

function change_item(rs){
	
	
	
	var val = $('#kode_'+rs+'').val();
	
	var sss = val.split('|');
	
	//console.log(sss);
	//alert(val);
	
	var harga = new Intl.NumberFormat('de-DE', {}).format(sss[2]);
	var qty_s = new Intl.NumberFormat('de-DE', {}).format(sss[3]);
			
	
	$('#iteml_'+rs+'').html("Box @ "+sss[1]+"<br>Stock :"+qty_s);
	//$('#harga_'+rs+'').val(harga);
	
	change_sum(rs);
	
	
}

function  change_kode(rs){
	
	  var datapost = {
		"kode": $('#kode_'+rs+'').val()
      };
	
	$.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'selling/get_price_mat'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
			
			//var harga = new Intl.NumberFormat('de-DE', { }).format(response.list);
			
			$('#harga_'+rs+'').val(response.list);
			
	    }
    });
}

function tambah_row(){
	
	$('.hapus_btn').hide();
	var values = $('#int_flo').val();
	
	var new_fl = parseInt(values)+1;
	
	$('#int_flo').val(new_fl);
   var datapost = {
		"id_t_cust": $('#name').val()
      };

      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'selling/get_customer'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {

		var htl = '';
		htl +=	'<div id="row_'+new_fl+'" >';
		htl +=	'									<div class="row" style="border-top-style:solid;">';
		htl +=	'									<div style="margin-top:10px">';
		htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
		htl +=	'												<select onChange="change_item('+new_fl+')" name="kode_'+new_fl+'" id="kode_'+new_fl+'" class="form-control" placeholder="Nama Item">';
		htl +=	'													<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		htl +=	'												</select>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="qty_'+new_fl+'" id="qty_'+new_fl+'" type="text" onKeyup="change_sum('+new_fl+')" class="form-control rupiahs" placeholder="Qty" value=0  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<label id="iteml_'+new_fl+'"></label><label id="iteml2_0"></label>';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="qtys_'+new_fl+'" id="qtys_'+new_fl+'" type="text" onKeyup="change_sum('+new_fl+')" class="form-control rupiahs" placeholder="Qty" value=0  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="totalqty_'+new_fl+'" id="totalqty_'+new_fl+'" type="number"  class="form-control" placeholder="Diskon" value=0 readOnly >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="harga_'+new_fl+'" id="harga_'+new_fl+'" type="text" onKeyup="change_sum('+new_fl+')" class="form-control rupiahs" placeholder="Harga" value=0 >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="total_'+new_fl+'" id="total_'+new_fl+'" type="text" class="form-control rupiahs" placeholder="Total" value=0 readOnly  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<button type="button" class="btn btn-danger waves-effect waves-light " onClick="kurang_row('+new_fl+')">-</button>';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'									</div>';
		htl +=	'									</div>';
		htl +=	'								</div>';
	
		$('#table_items').append(htl);
			
			$('#kode_'+new_fl+'').html(response.list);
			
			$('#kode_'+new_fl).select2();
			//$("kode_'+new_fl).css("background-color", "yellow");
			
			$(".rupiahs").inputFilter(function(value) {
			return /^-?\d*[,]?\d*$/.test(value); 
			});
			
			$('.rupiah').priceFormat({
          		prefix: '',
          		centsSeparator: ',',
          		centsLimit: 0,
          		thousandsSeparator: '.'
        	});	
			
        }
      });
	
}

function cancelchange(){
	
	var prin = $('#ints2').val();
	$('#ints').val(prin);
	$('#name').val(prin);
	
}

// function okchange(){

	   // var datapost = {
		// "id_prin": $('#name').val()
      // };

      // $.ajax({
        // type: 'POST',
        // url: "<?php echo base_url() . 'selling/get_principal'; ?>",
        // data: JSON.stringify(datapost),
        // cache: false,
        // contentType: false,
        // processData: false,
        // success: function(response) {
			// //var obj = JSON.parse(response);
			// $('#alamat').val(response['principal'][0].eksternal_address);
			// $('#att').val(response['principal'][0].pic);
			// $('#telp').val(response['principal'][0].phone_1);
			// $('#fax').val(response['principal'][0].fax);
			// //console.log(response.phone_1);
			
		// $('#table_items').html('')	
		
		// var htl = '';
		// htl +=	'<div id="row_0" >';
		// htl +=	'									<div class="row" style="border-top-style:solid;">';
		// htl +=	'									<div style="margin-top:10px">';
		// htl +=	'										<div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">';
// htl +=	'														<select name="kode_0" id="kode_0" class="form-control" placeholder="Nama Principal">';
// htl +=	'															<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
// htl +=	'														</select>';
		// htl +=	'										</div>';
		// htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		// htl +=	'											<div class="form-group">';
		// htl +=	'												<input name="qty_0" id="qty_0" type="text" class="form-control rupiahs" onKeyup="change_sum(0)"  placeholder="Qty" value=0 >';
		// htl +=	'											</div>';
		// htl +=	'										</div>';
		// htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		// htl +=	'											<div class="form-group">';
		// htl +=	'												<input name="harga_0" id="harga_0" type="text" class="form-control rupiahs" onKeyup="change_sum(0)" placeholder="Harga" value=0 >';
		// htl +=	'											</div>';
		// htl +=	'										</div>';
		// htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		// htl +=	'											<div class="form-group">';
		// htl +=	'												<input name="diskon_0" id="diskon_0" type="number" class="form-control" onKeyup="change_sum(0)" placeholder="Diskon" value=0 >';
		// htl +=	'											</div>';
		// htl +=	'										</div>';
		// htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		// htl +=	'											<div class="form-group">';
		// htl +=	'												<input name="total_0" id="total_0" type="text" class="form-control rupiahs" placeholder="Total" readOnly value=0 >';
		// htl +=	'											</div>';
		// htl +=	'										</div>';
		// htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		// htl +=	'											<div class="form-group">';
		// htl +=	'												<button type="button" class="btn btn-danger waves-effect waves-light " onClick="kurang_row()">-</button>';
		// htl +=	'											</div>';
		// htl +=	'										</div>';
		// htl +=	'									</div>';
		// htl +=	'									</div>';
		// htl +=	'								</div>';
	
		// $('#table_items').append(htl);
			
			// $('#kode_0').html(response.list);
			
			// $('#kode_0').select2();
			
			 // $('.rupiah').priceFormat({
          		// prefix: '',
          		// centsSeparator: '',
          		// centsLimit: 0,
          		// thousandsSeparator: '.'
        	// });	
			
        // }
      // });
	  
// }

function lainnya(int_fal){
	
	 var datapost = {
		"id_prin": $('#name').val()
      };
	
	  $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'selling/get_all_item'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {

			$('#kode_'+int_fal).html(response.list);
			
			$('#kode_'+int_fal).select2();
        }
      });
	
}


function select_prin(){
	
	var prin = $('#ints').val();
	var prin2 = $('#name').val();
	
	$('#ints2').val(prin);
//	alert(prin2);
	$('#ints').val(prin2);

		
	  var datapost = {
		"id_t_cust": $('#name').val()
      };

      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'selling/get_customer'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
			//var obj = JSON.parse(response);
			$('#alamat').val(response['customer'][0].cust_address);
			$('#att').val(response['customer'][0].contact_person);
			$('#telp').val(response['customer'][0].phone);
			$('#email').val(response['customer'][0].email);
			
			var credit_limits = new Intl.NumberFormat('de-DE', {}).format(response['customer'][0].credit_limit);
			
			$('#limit').val(credit_limits);
			
			var sisa_credits = new Intl.NumberFormat('de-DE', {}).format(response['customer'][0].sisa_credit);
			
			$('#sisa').val(sisa_credits);
			//console.log(response.phone_1);
			
			
			$('#table_items').html('')	
		
			var htl = '';
		htl +=	'<div id="row_0" >';
		htl +=	'									<div class="row" style="border-top-style:solid;">';
		htl +=	'									<div style="margin-top:10px">';
		htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
		htl +=	'												<select onChange="change_item(0)" name="kode_0" id="kode_0" class="form-control" placeholder="Nama Principal">';
		htl +=	'													<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		htl +=	'												</select>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="qty_0" id="qty_0" type="text" onKeyup="change_sum(0)" class="form-control rupiahs" placeholder="Qty" value=0  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<label id="iteml_0"></label><label id="iteml2_0"></label>';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="qtys_0" id="qtys_0" type="text" onKeyup="change_sum(0)" class="form-control rupiahs" placeholder="Qty" value=0  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="totalqty_0" id="totalqty_0" type="number"  class="form-control" placeholder="Diskon" value=0 readOnly >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="harga_0" id="harga_0" type="text" onKeyup="change_sum(0)" class="form-control rupiahs" placeholder="Harga" value=0 >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="total_0" id="total_0" type="text" class="form-control rupiahs" placeholder="Total" value=0 readOnly  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'									</div>';
		htl +=	'									</div>';
		htl +=	'								</div>';
		
			$('#table_items').append(htl);
			$('#kode_0').html(response.list);
			
			$('#kode_0').select2();
			
			//$($("#kode_0").select2("container")).addClass("form-control");
			//$("#kode_0").select2({ height: '300px' });	
			
			$(".rupiahs").inputFilter(function(value) {
			return /^-?\d*[,]?\d*$/.test(value); 
			});
			
			 $('.rupiah').priceFormat({
          		prefix: '',
          		centsSeparator: ',',
          		centsLimit: 0,
          		thousandsSeparator: '.'
        	});	
        }
      });
		
	
	//$('#ints').val('1');
	
}

function kurang_row(new_fl){
	
	$('#row_'+new_fl).remove();
	
		var int_val = parseInt($('#int_flo').val());
	
	var total_all = 0;
	
	for(var yo = 0;yo <= int_val;yo++ ){
				
		if($('#kode_'+yo).val() !== undefined){
			
			var qty = 	$('#qty_'+yo).val();
			var kode = 	$('#kode_'+yo).val();
			var qtys = 	$('#qtys_'+yo).val();
			var harga = $('#harga_'+yo).val();
			
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace(',','.');
			
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');

			//var val = $('#kode_'+rs+'').val();
			
			var sss = kode.split('|');
			
			//alert(qtys);
			if(qtys == ""){
				var qtys = 0;
			}else{
				var qtys = qtys;
			}
			
			if(qty == ""){
				var qty = 0;
			}else{
				var qty = qty;
			}
			
			var total_qty = ( parseInt(qty)*parseInt(sss[1]) ) + parseInt(qtys);
			
			var total_harga = total_qty * parseFloat(harga);
			//var total_harga = new Intl.NumberFormat('de-DE', {}).format(total_harga);
			
			total_all = total_all + total_harga;
			
		}
				
	}
	
	//alert(total_all);
	
	var gt = new Intl.NumberFormat('de-DE', {}).format(total_all.toFixed(2));
	$('#grand_total').html(gt);
	
	var ppn = total_all/11;
	var ppns = new Intl.NumberFormat('de-DE', {}).format(ppn.toFixed(2));
	$('#vatppn').html(ppns);
	
	var dpp = total_all - ppn;
	var dpps = new Intl.NumberFormat('de-DE', {}).format(dpp.toFixed(2));
	$('#subttl2').html(dpps);
	
	
}

function clearform(){
	
	$('#add_po').trigger("reset");
	$('#table_items').html('')	
		
						var htl = '';
		htl +=	'<div id="row_0" >';
		htl +=	'									<div class="row" style="border-top-style:solid;">';
		htl +=	'									<div style="margin-top:10px">';
		htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
		htl +=	'												<select name="kode_0" onKeyup="change_sum(0)" id="kode_0" class="form-control" placeholder="Nama Principal">';
		htl +=	'													<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		htl +=	'												</select>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="qty_0" id="qty_0" type="text" onKeyup="change_sum(0)" class="form-control rupiahs" placeholder="Qty" value=0  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<label id="iteml_0"></label><label id="iteml2_0"></label>';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="qtys_0" id="qtys_0" type="text" onKeyup="change_sum(0)" class="form-control rupiahs" placeholder="Qty" value=0  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="totalqty_0" id="totalqty_0" type="number"  class="form-control" placeholder="Diskon" value=0 readOnly >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="harga_0" id="harga_0" type="text" onKeyup="change_sum(0)" class="form-control rupiahs" placeholder="Harga" value=0 >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="total_0" id="total_0" type="text" class="form-control rupiahs" placeholder="Total" value=0 readOnly  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<button type="button" class="btn btn-danger waves-effect waves-light " onClick="kurang_row()">-</button>';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'									</div>';
		htl +=	'									</div>';
		htl +=	'								</div>';
		
			$('#table_items').append(htl);
			
			$('#kode_0').select2();
			
			$(".rupiahs").inputFilter(function(value) {
			return /^-?\d*[,]?\d*$/.test(value); 
			});
			
			 $('.rupiah').priceFormat({
          		prefix: '',
          		centsSeparator: ',',
          		centsLimit: 0,
          		thousandsSeparator: '.'
        	});	
	
}

function change_ppn(){
	
	var sub_total = 0;
	var sub_total_bd = 0;
	var sub_disk = 0;
	var int_val = parseInt($('#int_flo').val());
		
	for(var yo = 0;yo <= int_val;yo++ ){
				
		if($('#kode_'+yo).val() !== undefined){
			
			var tots = $('#total_'+yo).val();
			var tots = tots.replace('.','');
			var tots = tots.replace('.','');
			var tots = tots.replace('.','');
			var tots = tots.replace('.','');
			
			var prcs = $('#harga_'+yo).val();
			var prcs = prcs.replace('.','');
			var prcs = prcs.replace('.','');
			var prcs = prcs.replace('.','');
			var prcs = prcs.replace('.','');
			
			var qtys = $('#qty_'+yo).val();
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			
			
			
				sub_total = sub_total +	parseFloat(tots);
				sub_total_bd = sub_total_bd + (parseFloat(prcs)*parseFloat(qtys) );
				sub_disk = sub_disk + ( (parseFloat(prcs)*parseFloat(qtys))*(parseFloat($('#diskon_'+yo).val())/100) );
		}
				
	}
	
	if($('#ppn').val() == ''){
		var ppns = 0;
	}else{
		var ppns = $('#ppn').val();
	}
	
	var total_ppn = (sub_total * (parseFloat(ppns)/100 ));
	var grand_total = sub_total + (sub_total * (parseFloat(ppns)/100 ));
	
	var gt = new Intl.NumberFormat('de-DE', { }).format(grand_total);
	var sub_totals = new Intl.NumberFormat('de-DE', { }).format(sub_total);
	var total_ppns = new Intl.NumberFormat('de-DE', { }).format(total_ppn);
	var total_bds = new Intl.NumberFormat('de-DE', { }).format(sub_total_bd);
	var total_sub_disk = new Intl.NumberFormat('de-DE', { }).format(sub_disk);
	
	$('#vatppn').html(total_ppns);
	$('#grand_total').html(gt);
	
}

function change_sum(fl){
	
	var qty = 	$('#qty_'+fl).val();
	var kode = 	$('#kode_'+fl).val();
	var qtys = 	$('#qtys_'+fl).val();
	var harga = $('#harga_'+fl).val();
	
	var harga = harga.replace('.','');
	var harga = harga.replace('.','');
	var harga = harga.replace('.','');
	var harga = harga.replace('.','');
	var harga = harga.replace('.','');
	var harga = harga.replace(',','.');
	
	var qty = qty.replace('.','');
	var qty = qty.replace('.','');
	var qty = qty.replace('.','');
	var qty = qty.replace('.','');
	var qty = qty.replace('.','');
	
	var qtys = qtys.replace('.','');
	var qtys = qtys.replace('.','');
	var qtys = qtys.replace('.','');
	var qtys = qtys.replace('.','');
	var qtys = qtys.replace('.','');

	//var val = $('#kode_'+rs+'').val();
	
	var sss = kode.split('|');
	
	//alert(qtys);
	if(qtys == ""){
		var qtys = 0;
	}else{
		var qtys = qtys;
	}
	
	if(qty == ""){
		var qty = 0;
	}else{
		var qty = qty;
	}
	
	var total_qty = ( parseInt(qty)*parseInt(sss[1]) ) + parseInt(qtys);
	
	if(total_qty > parseInt(sss[3])){
		
		$('#iteml2_'+fl).html('<span style="color:red">Stok Tidak Cukup</span>');
		
	}else{
		$('#iteml2_'+fl).html('');
	}
	
	var total_harga = total_qty * parseFloat(harga);
	var total_harga = new Intl.NumberFormat('de-DE', {}).format(total_harga);
	
	$('#totalqty_'+fl+'').val(total_qty);
	$('#total_'+fl+'').val(total_harga);
	
	// if(total_qty > parseInt(sss[3]) ){
		
		
		
	// }
	
	// var diskon = diskon.replace('.','');
	// var diskon = diskon.replace('.','');
	// var diskon = diskon.replace('.','');
	// var diskon = diskon.replace('.','');

	// var qty = qty.replace('.','');
	// var qty = qty.replace('.','');
	// var qty = qty.replace('.','');
	// var qty = qty.replace('.','');
	// //alert(harga);
	
	
	//var total = (parseFloat(qty) * parseFloat(harga) )- ((parseFloat(qty) * parseFloat(harga))*(parseFloat(diskon)/100)); 
	// var totals = new Intl.NumberFormat('de-DE', { }).format(total);
	
	
	// if( isNaN(total)){
		// //var total_num = Number((total).toFixed(1)).toLocaleString();
		// //$('#subttl').html('0');
		// $('#total_'+fl).val('0');
	// }else{
		// //var total_num = Number((total).toFixed(1)).toLocaleString();
		// //$('#subttl').html(total);
		// $('#total_'+fl).val(totals);
	// }
	
	// var sub_total = 0;
	// var sub_total_bd = 0;
	// var sub_disk = 0;
	var int_val = parseInt($('#int_flo').val());
	
	var total_all = 0;
	
	for(var yo = 0;yo <= int_val;yo++ ){
				
		if($('#kode_'+yo).val() !== undefined){
			
			var qty = 	$('#qty_'+yo).val();
			var kode = 	$('#kode_'+yo).val();
			var qtys = 	$('#qtys_'+yo).val();
			var harga = $('#harga_'+yo).val();
			
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace(',','.');
			
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');

			//var val = $('#kode_'+rs+'').val();
			
			var sss = kode.split('|');
			
			//alert(qtys);
			if(qtys == ""){
				var qtys = 0;
			}else{
				var qtys = qtys;
			}
			
			if(qty == ""){
				var qty = 0;
			}else{
				var qty = qty;
			}
			
			var total_qty = ( parseInt(qty)*parseInt(sss[1]) ) + parseInt(qtys);
			
			var total_harga = total_qty * parseFloat(harga);
			//var total_harga = new Intl.NumberFormat('de-DE', {}).format(total_harga);
			
			total_all = total_all + total_harga;
			
		}
				
	}
	
	//alert(total_all);
	
	var gt = new Intl.NumberFormat('de-DE', {}).format(total_all.toFixed(2));
	$('#grand_total').html(gt);
	
	var ppn = total_all/11;
	var ppns = new Intl.NumberFormat('de-DE', {}).format(ppn.toFixed(2));
	$('#vatppn').html(ppns);
	
	var dpp = total_all - ppn;
	var dpps = new Intl.NumberFormat('de-DE', {}).format(dpp.toFixed(2));
	$('#subttl2').html(dpps);
	
	// var total_ppn = (sub_total * (parseFloat($('#ppn').val())/100 ));
	// var grand_total = sub_total + (sub_total * (parseFloat($('#ppn').val())/100 ));
	
	
	
	// var gt = new Intl.NumberFormat('de-DE', { }).format(grand_total);
	// var sub_totals = new Intl.NumberFormat('de-DE', { }).format(sub_total);
	// var total_ppns = new Intl.NumberFormat('de-DE', { }).format(total_ppn);
	// var total_bds = new Intl.NumberFormat('de-DE', { }).format(sub_total_bd);
	// var total_sub_disk = new Intl.NumberFormat('de-DE', { }).format(sub_disk);
	
	// $('#subttl').html(total_bds);
	// $('#subttl2').html(sub_totals);
	// $('#subttl3').html(total_sub_disk);
	// $('#vatppn').html(total_ppns);
	// $('#grand_total').html(gt);
	
	
	
	
	//alert(qty+' '+harga+' '+diskon);
	
}

function back(){
	
	window.location.href = "<?php echo base_url().'selling';?>";
	
}

function term_other(){
	
	//alert($('#term').val());
	
	if( $('#term').val() == "other"){
		//$('#term_o').show();
		$("#term_o").prop("readonly",false);

		
	}else{
		// $('#term_o').hide();
		
		$("#term_o").prop("readonly",true);
	}
	
}

  function listdist(){
	  var user_id = '0001';
	  var token = '093940349';
	  
	  
   $('#datatable_pricipal').DataTable({
	   //"dom": 'rtip',
		"bFilter": false,
		"aaSorting": [],
		"bLengthChange": true,
		'iDisplayLength': 10,
		"sPaginationType": "simple_numbers",
		"Info" : false,
		"processing": true,
        "serverSide": true,
        "destroy": true,
		"ajax": "<?php echo base_url().'selling/lists'?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token,
		"searching": true,
		"language": {
            "decimal": ",",
            "thousands": "."
        },
		"dom": 'l<"toolbar">frtip',
		"initComplete": function(){
                           $("div.toolbar").prepend('<div class="btn-group pull-left"><button type="button" class="btn btn-custon-rounded-two btn-primary" > Tambah </button></div>');
        }
	});	
  }

	$(document).ready(function(){
		
		$('#kode_0').select2();
		$('#name').select2();
		
		$(".rupiahs").inputFilter(function(value) {
			return /^-?\d*[,]?\d*$/.test(value); 
			});
		
		 $('.rupiah').priceFormat({
          		prefix: '',
          		centsSeparator: ',',
          		centsLimit: 0,
          		thousandsSeparator: '.'
        	});	
		
		$('#tgl').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		});
		
      listdist();
	  
	  $('#add_po').on('submit', function(e){
		// validation code here
		//if(!valid) {
			e.preventDefault();
			
			// var formData = new FormData(this);
			var formData = new FormData();
			  var urls = $(this).attr('action');
			  
			formData.append('kode', $('#kode').val());
			formData.append('tgl', $('#tgl').val());
			formData.append('ints', $('#ints').val());
			formData.append('ints2', $('#ints2').val());
			formData.append('name', $('#name').val());
			formData.append('ppn', $('#ppn').val());
			formData.append('term', $('#term').val());
			formData.append('term_o', $('#term_o').val());
			formData.append('ket', $('#ket').val());
			formData.append('sisa', $('#sisa').val());
			formData.append('int_flo', $('#int_flo').val());
			
			var int_val = parseInt($('#int_flo').val());
			
			for(var yo = 0;yo <= int_val;yo++ ){
				
				if($('#kode_'+yo).val() !== undefined){
					
					formData.append('kode_'+yo, $('#kode_'+yo).val());
					formData.append('harga_'+yo, $('#harga_'+yo).val());
					formData.append('total_'+yo, $('#total_'+yo).val());
					formData.append('qty_'+yo, $('#qty_'+yo).val());
					formData.append('totalqty_'+yo, $('#totalqty_'+yo).val());
				//	formData.append('remark_'+yo, $('#remark_'+yo).val());
					formData.append('qtys_'+yo, $('#qtys_'+yo).val());
					
				}
				
			}
			
		 swal({
      title: 'Yakin akan Simpan Data ?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak'
    }).then(function() {
			
			
			//console.log(formData);

          $.ajax({
              type:'POST',
				url: urls,
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                success: function(response) {
					
					//console.log(response);
					if(response.success == true){
						$('#ModalAddPo').modal('show');
					}else{
						$('#msg_err').html(response.message);
						$('#ModalChangePo').modal('show');
					}
                }
            });
			
			 });
			//alert(kode);
	
	
		//}
	  });
  });
</script>