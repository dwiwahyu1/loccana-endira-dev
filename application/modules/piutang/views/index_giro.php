<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  /* Stop Line Break inside Cell */
  .dt-body-nowrap {
    white-space: nowrap;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto
  }

  .scroll-overflow {
    min-height: 650px
  }

  #modal-distributor::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }
  .nopadding {
   padding: 0 !important;
   margin: 0 !important;
}

</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
          <div class="card-box table-responsive">
            <div class="dropdown pull-right">
              <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                <i class="zmdi zmdi-more-vert"></i>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>

            <div class="col-lg-6  nopadding">
              <h4 class="header-title">List Giro</h4>
              <div class="btn-group pull-left"><a href="<?php echo base_url() . 'piutang/index_giro_tempo' ?>" type="button" class="btn btn-custon-rounded-two btn-primary" style="display: block"> Daftar Rekap Giro </a></div>
            </div>
            <div class="col-lg-6">            
              <?php echo $total_month?>
            </div>
            <table id="datatable_pricipal" class="table table-striped table-bordered" style="width:100%">
              <thead>
                <tr>
                  <th width="5%">No</th>
                  <th>Tanggal Terima</th>
                  <th>Nomor Giro</th>
                  <th>Bank Penerbit</th>
                  <th>Tanggal Jatuh Tempo</th>
                  <th>Bank Setor</th>
                  <th>Nama Toko</th>
                  <th>Tipe Pembayaran</th>
                  <th>Total Giro</th>
                  <th width="17%">Action</th>
                </tr>
              </thead>

              <tbody style="">

              </tbody>
            </table>
          </div>
        </div><!-- end col -->
      </div>
    </div>
  </div>
  <span id="laod"></span>
</div>


<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content p-0 b-0">
      <div class="panel panel-color panel-primary panel-filled">
        <div class="panel-heading">
          <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 class="panel-title"></h3>
        </div>
        <div class="panel-body force-overflow" id="modal-distributor">
          <div class="scroll-overflow">
            <p></p>
          </div>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  function cancelpo(id) {
    swal({
      title: 'Yakin akan Cancel ?',
      text: 'data tidak dapat dikembalikan bila sudah dicancel !',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak'
    }).then(function() {
      var datapost = {
        "id": id
      };

      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'piutang/cancel_po'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
          $('.panel-heading button').trigger('click');
          listdist();
          swal({
            title: 'Success!',
            text: response.message,
            type: 'success',
            showCancelButton: false,
            confirmButtonText: 'Ok'
          }).then(function() {})
        }
      });
    });
  }

  function listdist() {
    var user_id = '0001';
    var token = '093940349';
    var filter = '0';
    var filter_month = "<?php echo date('Y-m-01'); ?>";
    var filter_year = "<?php echo date('Y-m-d'); ?>";
    var filter_payment_status = 'ALL';


    $('#datatable_pricipal').DataTable({
      //"dom": 'rtip',
      "bFilter": false,
      "aaSorting": [],
      "bLengthChange": true,
      'iDisplayLength': 10,
      "sPaginationType": "simple_numbers",
      "Info": false,
      "processing": true,
      "serverSide": true,
      "destroy": true,
      "ajax": "<?php echo base_url() . 'piutang/lists_giro' ?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token + "&filter_month=" + filter_month + "&filter_year=" + filter_year + "&filter_payment_status=" + filter_payment_status,
      "searching": true,
      // "scrollX": true,
      // "sScrollY": "300px",
      // fixedColumns: {
      //   leftColumns: 3
      // },
      "language": {
        "decimal": ",",
        "thousands": "."
      },
      "dom": 'l<"toolbar">frtip',
      "initComplete": function() {
        $("div.toolbar").prepend('<div class="btn-group pull-left"><a href="<?php echo base_url() . 'piutang'; ?>" type="button" class="btn btn-custon-rounded-two btn-primary" > Kembali </a><input class="form-control" type="text" name="start_time" id="start_time" value="' + filter_month + '" /> s/d <input class="form-control" type="text" name="end_time" id="end_time" value="' + filter_year + '" /> <?php echo $filter_payment_status ?></div><div class="btn-group pull-left"><a href="#" type="button" class="btn btn-custon-rounded-two btn-primary" onClick="filter()" id="btn_filter" > Filter </a> <a href="#" type="button" class="btn btn-custon-rounded-two btn-primary" onClick="export_exc()" id="btn_filter_exp" > Export </a></div>');

        $("#btn_filter").prop('disabled', false);

        // $('#start_time').val(datapost.start_time)
        // $('#end_time').val(datapost.end_time)

        swal.close()


        $('#start_time').datepicker({
          isRTL: true,
          format: "yyyy-mm-dd",
          autoclose: true,
          todayHighlight: true,
          changeYear: true,
        });

        $('#end_time').datepicker({
          isRTL: true,
          format: "yyyy-mm-dd",
          autoclose: true,
          todayHighlight: true,
          changeYear: true,
        });
      },
      "columnDefs": [{
        "targets": [9],
        "className": 'dt-body-nowrap'
      }]
    });
  }

  $(document).ready(function() {
    listdist();
  });

  function filter() {

    var user_id = '0001';
    var token = '093940349';
    var filter = $('#filter_table').val();
    var filter_month = $('#start_time').val();
    var filter_year = $('#end_time').val();
    var filter_payment_status = $('#filter_payment_status').val();

    var sul = '';
    var sul2 = '';
    var sul1 = '';

    if (filter == 0) {
      var sul = 'selected';
    } else if (filter == 1) {
      var sul1 = 'selected';
    } else {
      var sul2 = 'selected';
    }

    //alert(filter);

    $('#datatable_pricipal').DataTable({
      //"dom": 'rtip',
      "bFilter": false,
      "aaSorting": [],
      "bLengthChange": true,
      'iDisplayLength': 10,
      "sPaginationType": "simple_numbers",
      "Info": false,
      "processing": true,
      "serverSide": true,
      "destroy": true,
      "ajax": "<?php echo base_url() . 'piutang/lists_giro' ?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token + "&filter=" + filter + "&filter_month=" + filter_month + "&filter_year=" + filter_year + "&filter_payment_status=" + filter_payment_status,
      "searching": true,
      // "scrollX": true,
      // "sScrollY": "300px",
      // fixedColumns: {
      //   leftColumns: 3
      // },
      "language": {
        "decimal": ",",
        "thousands": "."
      },
      "dom": 'l<"toolbar">frtip',
      "initComplete": function() {
        $("div.toolbar").prepend('<div class="btn-group pull-left"><a href="<?php echo base_url() . 'piutang'; ?>" type="button" class="btn btn-custon-rounded-two btn-primary" > Kembali </a><input class="form-control" type="text" name="start_time" id="start_time" value="' + filter_month + '" /> s/d <input class="form-control" type="text" name="end_time" id="end_time" value="' + filter_year + '" /> <?php echo $filter_payment_status ?></div><div class="btn-group pull-left"><a href="#" type="button" class="btn btn-custon-rounded-two btn-primary" onClick="filter()" id="btn_filter" > Filter </a> <a href="#" type="button" class="btn btn-custon-rounded-two btn-primary" onClick="export_exc()" id="btn_filter_exp" > Export </a></div>');
        $("#btn_filter").prop('disabled', false);

        // $('#start_time').val(datapost.start_time)
        // $('#end_time').val(datapost.end_time)
        // $('#end_time').val(datapost.end_time)
        $('#filter_payment_status').val(filter_payment_status);
        changetotalmonth(filter_month,filter_year,filter_payment_status)

        swal.close()


        $('#start_time').datepicker({
          isRTL: true,
          format: "yyyy-mm-dd",
          autoclose: true,
          todayHighlight: true,
          changeYear: true,
        });

        $('#end_time').datepicker({
          isRTL: true,
          format: "yyyy-mm-dd",
          autoclose: true,
          todayHighlight: true,
          changeYear: true,
        });
      }
    });

  }

  function changetotalmonth(filter_month,filter_year,filter_payment_status){
    var user_id = '0001';
    var token = '093940349';
    var sul = '';
    var sul2 = '';
    var sul1 = '';

    $.ajax({
        type: 'GET',
        url: "<?php echo base_url() . 'piutang/lists_giro' ?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token + "&filter_month=" + filter_month + "&filter_year=" + filter_year+ "&filter_payment_status=" + filter_payment_status,
        // data: JSON.stringify(filter_year),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
          // response=JSON.parse(response);
          $("#mtd_penjualan").text(response.mtd_penjualan);
          $("#mtd_penjualan").html(response.mtd_penjualan);
        }
      });
  }
  function updatepo(id) {


    var url = '<?php echo base_url(); ?>piutang/updatepo';

    var form = $("<form action='" + url + "' method='post'>" +
      "<input type='hidden' name='idpo' value='" + id + "' />" +
      "</form>");
    $('body').append(form);
    form.submit();

    //window.location.href = "<?php echo base_url() . 'piutang/updateprincipal/'; ?>"+id;

  }

  function detail(id) {


    var url = '<?php echo base_url(); ?>piutang/detail';

    var form = $("<form action='" + url + "' method='post'>" +
      "<input type='hidden' name='idpo' value='" + id + "' />" +
      "</form>");
    $('body').append(form);
    form.submit();

    //window.location.href = "<?php echo base_url() . 'piutang/updateprincipal/'; ?>"+id;

  }

  function print_pdf(id) {

    var url = '<?php echo base_url(); ?>piutang/print_pdf';

    var form = $("<form action='" + url + "' method='post'>" +
      "<input type='hidden' name='idpr' value='" + id + "' />" +
      "</form>");
    $('body').append(form);
    form.submit();


  }


  function konfirmasi(id) {


    var url = '<?php echo base_url(); ?>piutang/konfirmasi';

    var form = $("<form action='" + url + "' method='post'>" +
      "<input type='hidden' name='idpo' value='" + id + "' />" +
      "</form>");
    $('body').append(form);
    form.submit();

    //window.location.href = "<?php echo base_url() . 'piutang/updateprincipal/'; ?>"+id;

  }

  function export_exc() {

    // var name = $('#name').val();
    // var region = $('#region').val();
    var filter_month = $('#start_time').val();
    var filter_year = $('#end_time').val();
    var filter_payment_status = $('#filter_payment_status').val();

    var url = '<?php echo base_url(); ?>piutang/export_excel' + "/?filter_month=" + filter_month + "&filter_year=" + filter_year + "&filter_payment_status=" + filter_payment_status;

    window.open(url, '_blank').focus();


  }
</script>