<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Item_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();

	}

	function get_invoice_penjualan($params = array()){
		
		$query = 	'
		SELECT status_request,z.*,total_invoice  AS sisa, RANK() OVER ( ORDER BY tanggal_invoice,total_invoice DESC) AS Rangking FROM ( 		
			SELECT SUM(aa.price) AS total_invoice,a.id_invoice,a.no_invoice,
			a.tanggal_invoice,a.due_date,attention,a.faktur_pajak,
			note,SIGN,a.status AS status_invoice,b.*, e.`cust_name` ,
			IF(h.`id_req_inv` IS NULL,0,1) AS status_request 
			FROM `t_invoice_penjualan` a
			JOIN `t_penjualan` b ON a.id_penjualan = b.`id_penjualan`
			JOIN d_penjualan aa ON b.id_penjualan=aa.id_penjualan
			LEFT JOIN `t_customer` e ON b.id_customer = e.`id_t_cust`
			LEFT JOIN `t_request_invoice` h ON a.`id_invoice` = h.`id_invoice`
			WHERE b.`id_penjualan` = '.$params['id'].' 
			
			ORDER BY tanggal_invoice DESC) z
			ORDER BY tanggal_invoice,total_invoice DESC
				';
		
		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
		
	}	
	
	function get_info_material($idmat){
		
		$query = 	'
			SELECT * FROM m_material WHERE id_mat =  '.$idmat.'
		';
		
		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
		
	}

	function get_selling($params = array(),$region){
		
		// $this->db->select('t_purchase_order.*,t_eksternal.name_eksternal ')
		// #$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        // ->from('t_purchase_order')
        // ->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id');
		// $query = $this->db->get();
		
		// $query = "
	 
					// SELECT a.`id_coa`,a.`id_customer`,a.`id_penjualan`,a.`id_sales`,a.`id_valas`,a.`keterangan`,a.`no_penjualan`,a.`payment_coa`,
					// a.`payment_coa`,a.`ppn`,a.`rate`,a.`seq_n`,a.`status`,
					// a.`term_of_payment`,FORMAT(SUM(aa.`price`), 2, 'id_ID') AS total_amount,
					// a.`date_penjualan`,a.`delivery_date`,
					// b.nama,c.username,d.cust_name,e.nama_valas,e.symbol_valas
					// FROM t_penjualan a
					// JOIN d_penjualan aa ON a.`id_penjualan`=aa.`id_penjualan`
					// JOIN u_user b ON a.id_sales=b.id
					// JOIN u_user_group c ON b.id=c.id_user
					// JOIN t_customer d ON a.id_customer=d.id_t_cust
					// JOIN `u_user` u ON d.region = u.`lokasi`
					// JOIN m_valas e ON a.id_valas=e.valas_id
					// WHERE a.seq_n <> 8 AND u.id = ".$params['id']."
					// GROUP BY aa.`id_penjualan`
					// order by date_penjualan desc,no_penjualan DESC
		// ";
		$query = "
	 
					SELECT a.`id_coa`,a.`id_customer`,a.`id_penjualan`,a.`id_sales`,a.`id_valas`,a.`keterangan`,a.`no_penjualan`,a.`payment_coa`,
					a.`payment_coa`,a.`ppn`,a.`rate`,a.`seq_n`,a.`status`,
					a.`term_of_payment`,FORMAT(SUM(aa.`price`), 2, 'id_ID') AS total_amount,
					a.`date_penjualan`,a.`delivery_date`,
					b.nama,c.username,d.cust_name,e.nama_valas,e.symbol_valas
					FROM t_penjualan a
					JOIN d_penjualan aa ON a.`id_penjualan`=aa.`id_penjualan`
					JOIN u_user b ON a.id_sales=b.id
					JOIN u_user_group c ON b.id=c.id_user
					JOIN t_customer d ON a.id_customer=d.id_t_cust
					JOIN `u_user` u ON d.region = u.`lokasi`
					JOIN m_valas e ON a.id_valas=e.valas_id
					WHERE a.seq_n <> 8 AND d.region = ".$region."
					GROUP BY aa.`id_penjualan`
					order by date_penjualan desc,no_penjualan DESC
		";
		
		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}		
	
	function get_selling_a($params = array()){
		
		// $this->db->select('t_purchase_order.*,t_eksternal.name_eksternal ')
		// #$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        // ->from('t_purchase_order')
        // ->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id');
		// $query = $this->db->get();
		
		$query = "
	 
					SELECT a.`id_coa`,a.`id_customer`,a.`id_penjualan`,a.`id_sales`,a.`id_valas`,a.`keterangan`,a.`no_penjualan`,a.`payment_coa`,
					a.`payment_coa`,a.`ppn`,a.`rate`,a.`seq_n`,a.`status`,
					a.`term_of_payment`,FORMAT(SUM(aa.`price`), 2, 'id_ID') AS total_amount,
					a.`date_penjualan`,a.`delivery_date`,
					b.nama,c.username,d.cust_name,e.nama_valas,e.symbol_valas
					FROM t_penjualan a
					JOIN d_penjualan aa ON a.`id_penjualan`=aa.`id_penjualan`
					JOIN u_user b ON a.id_sales=b.id
					JOIN u_user_group c ON b.id=c.id_user
					JOIN t_customer d ON a.id_customer=d.id_t_cust
					JOIN m_valas e ON a.id_valas=e.valas_id
					WHERE a.seq_n <> 8
					GROUP BY aa.`id_penjualan`
					order by date_penjualan desc,no_penjualan DESC
		";
		
		
		// echo $query;die;
		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	function get_lokasi($params = array()){
		
		// $this->db->select('t_purchase_order.*,t_eksternal.name_eksternal ')
		// #$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        // ->from('t_purchase_order')
        // ->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id');
		// $query = $this->db->get();
		
		$query = "
					select lokasi from u_user u where u.id = ".$params['id']."
					
		";
		
		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	

	function get_item($params = array()){
		
		$query = "
		SELECT
		a.id_mat,
		(a.base_qty * a.unit_box) / convertion box_per_lt,
		a.stock_code,
		a.stock_name,
		IF(a.pajak=10,11,10) AS pajak,
		a.stock_description,
		a.qty,
		a.base_qty,
		uom.uom_name,
		uom.uom_symbol,
		a.unit_box,
		ek.name_eksternal,
		ek.kode_eksternal,
		a.qty / a.unit_box as box_sisa,
		IF (
		  b.mutasi_big_fix IS NULL,
		  0,
		  b.mutasi_big_fix
		) + IF (
		  c.mutasi_big_fix IS NULL,
		  0,
		  c.mutasi_big_fix
		) - IF (
		  d.mutasi_big_fix IS NULL,
		  0,
		  d.mutasi_big_fix
		) - IF (
		  e.mutasi_big_fix IS NULL,
		  0,
		  e.mutasi_big_fix
		) - IF (
		  f.mutasi_big_fix IS NULL,
		  0,
		  f.mutasi_big_fix
		) - IF (
		  g.mutasi_big_fix IS NULL,
		  0,
		  g.mutasi_big_fix
		) - (
		  IF (
			transit_keluar.mutasi_big_fix IS NULL,
			0,
			transit_keluar.mutasi_big_fix
		  ) - IF (
			transit_masuk.mutasi_big_fix IS NULL,
			0,
			transit_masuk.mutasi_big_fix
		  )
		) + IF (
		  qty_retur_jual IS NULL,
		  0,
		  qty_retur_jual
		) + IF (
		  qty_retur_jual_awal IS NULL,
		  0,
		  qty_retur_jual_awal
		) AS saldo_akhir_big,
		(select
		  saldo_akhir_big) /
		(select
		  box_per_lt) AS box_sisa2
	  FROM
		m_material a
		JOIN `m_uom` uom
		  ON a.`unit_terkecil` = uom.`id_uom`
		JOIN `t_eksternal` ek
		  ON a.`dist_id` = ek.`id`
		LEFT JOIN `t_uom_convert` uc
		  ON uom.id_uom = uc.`id_uom`
		LEFT JOIN
		  (SELECT
			a.id_stock_awal,
			(
			  IF (
				a.amount_mutasi IS NULL,
				0,
				a.amount_mutasi
			  ) - IF (
				b.amount_mutasi IS NULL,
				0,
				b.amount_mutasi
			  )
			) / a.unit_box AS amount_mutasi_fix,
			IF (
			  a.mutasi_big IS NULL,
			  0,
			  a.mutasi_big
			) - IF (
			  b.mutasi_big IS NULL,
			  0,
			  b.mutasi_big
			) AS mutasi_big_fix,
			(
			  IF (a.qty_sol IS NULL, 0, a.qty_sol) - IF (b.qty_sol IS NULL, 0, b.qty_sol)
			) / a.unit_box AS qty_sol_fix,
			IF (
			  a.qty_sol_big IS NULL,
			  0,
			  a.qty_sol_big
			) - IF (
			  b.qty_sol_big IS NULL,
			  0,
			  b.qty_sol_big
			) AS qty_sol_big_fix,
			(
			  IF (a.qty_bonus IS NULL, 0, a.qty_bonus) - IF (b.qty_bonus IS NULL, 0, b.qty_bonus)
			) / a.unit_box AS qty_bonus_fix,
			IF (
			  a.qty_bonus_big IS NULL,
			  0,
			  a.qty_bonus_big
			) - IF (
			  b.qty_bonus_big IS NULL,
			  0,
			  b.qty_bonus_big
			) AS qty_bonus_big_fix,
			(
			  IF (a.qty_titip IS NULL, 0, a.qty_titip) - IF (b.qty_titip IS NULL, 0, b.qty_titip)
			) / a.unit_box AS qty_titip_fix,
			IF (
			  a.qty_titip_big IS NULL,
			  0,
			  a.qty_titip_big
			) - IF (
			  b.qty_titip_big IS NULL,
			  0,
			  b.qty_titip_big
			) AS qty_titip_big_fix
		  FROM
			(SELECT
			  id_stock_awal,
			  b.unit_box,
			  sum(a.`amount_mutasi`) amount_mutasi,
			  sum(a.`mutasi_big`) mutasi_big,
			  sum(a.`qty_sol`) qty_sol,
			  sum(a.`qty_sol_big`) qty_sol_big,
			  sum(a.`qty_bonus`) qty_bonus,
			  sum(a.`qty_bonus_big`) qty_bonus_big,
			  sum(a.`qty_titip`) qty_titip,
			  sum(a.`qty_titip_big`) qty_titip_big
			FROM
			  t_mutasi a
			  JOIN m_material b
				ON a.id_stock_awal = b.id_mat
			WHERE a.`type_mutasi` = 0
			  AND a.date_mutasi < '2022-04-1'
			GROUP BY a.`id_stock_awal`) a
			LEFT JOIN
			  (SELECT
				id_stock_awal,
				sum(a.`amount_mutasi`) amount_mutasi,
				sum(a.`mutasi_big`) mutasi_big,
				sum(a.`qty_sol`) qty_sol,
				sum(a.`qty_sol_big`) qty_sol_big,
				sum(a.`qty_bonus`) qty_bonus,
				sum(a.`qty_bonus_big`) qty_bonus_big,
				sum(a.`qty_titip`) qty_titip,
				sum(a.`qty_titip_big`) qty_titip_big
			  FROM
				t_mutasi a
			  WHERE a.`type_mutasi` = 1
				AND a.`date_mutasi` < '2022-04-1'
			  GROUP BY a.`id_stock_awal`) b
			  ON a.id_stock_awal = b.id_stock_awal) b
		  ON a.id_mat = b.id_stock_awal
		LEFT JOIN
		  (SELECT
			a.id_stock_awal,
			(
			  IF (
				a.amount_mutasi IS NULL,
				0,
				a.amount_mutasi
			  )
			) / a.unit_box AS amount_mutasi_fix,
			IF (
			  a.mutasi_big IS NULL,
			  0,
			  a.mutasi_big
			) AS mutasi_big_fix,
			(IF (a.qty_sol IS NULL, 0, a.qty_sol)) / a.unit_box AS qty_sol_fix,
			IF (
			  a.qty_sol_big IS NULL,
			  0,
			  a.qty_sol_big
			) AS qty_sol_big_fix,
			(
			  IF (a.qty_bonus IS NULL, 0, a.qty_bonus)
			) / a.unit_box AS qty_bonus_fix,
			IF (
			  a.qty_bonus_big IS NULL,
			  0,
			  a.qty_bonus_big
			) AS qty_bonus_big_fix,
			(
			  IF (a.qty_titip IS NULL, 0, a.qty_titip)
			) / a.unit_box AS qty_titip_fix,
			IF (
			  a.qty_titip_big IS NULL,
			  0,
			  a.qty_titip_big
			) AS qty_titip_big_fix
		  FROM
			(SELECT
			  id_stock_awal,
			  b.unit_box,
			  sum(a.`amount_mutasi`) amount_mutasi,
			  sum(a.`mutasi_big`) mutasi_big,
			  sum(a.`qty_sol`) qty_sol,
			  sum(a.`qty_sol_big`) qty_sol_big,
			  sum(a.`qty_bonus`) qty_bonus,
			  sum(a.`qty_bonus_big`) qty_bonus_big,
			  sum(a.`qty_titip`) qty_titip,
			  sum(a.`qty_titip_big`) qty_titip_big
			FROM
			  t_mutasi a
			  JOIN m_material b
				ON a.id_stock_awal = b.id_mat
			WHERE a.`type_mutasi` = 0
			  AND a.date_mutasi BETWEEN '2022-04-1'
			  AND '2022-04-14'
			  AND a.type_trans <> 5
			GROUP BY a.`id_stock_awal`) a) c
		  ON a.id_mat = c.id_stock_awal
		LEFT JOIN
		  (SELECT
			a.id_stock_awal,
			(
			  IF (
				a.amount_mutasi IS NULL,
				0,
				a.amount_mutasi
			  )
			) / a.unit_box AS amount_mutasi_fix,
			IF (
			  a.mutasi_big IS NULL,
			  0,
			  a.mutasi_big
			) AS mutasi_big_fix,
			(IF (a.qty_sol IS NULL, 0, a.qty_sol)) / a.unit_box AS qty_sol_fix,
			IF (
			  a.qty_sol_big IS NULL,
			  0,
			  a.qty_sol_big
			) AS qty_sol_big_fix,
			(
			  IF (a.qty_bonus IS NULL, 0, a.qty_bonus)
			) / a.unit_box AS qty_bonus_fix,
			IF (
			  a.qty_bonus_big IS NULL,
			  0,
			  a.qty_bonus_big
			) AS qty_bonus_big_fix,
			(
			  IF (a.qty_titip IS NULL, 0, a.qty_titip)
			) / a.unit_box AS qty_titip_fix,
			IF (
			  a.qty_titip_big IS NULL,
			  0,
			  a.qty_titip_big
			) AS qty_titip_big_fix
		  FROM
			(SELECT
			  id_stock_awal,
			  f.unit_box,
			  sum(a.`amount_mutasi`) amount_mutasi,
			  sum(a.`mutasi_big`) mutasi_big,
			  sum(a.`qty_sol`) qty_sol,
			  sum(a.`qty_sol_big`) qty_sol_big,
			  sum(a.`qty_bonus`) qty_bonus,
			  sum(a.`qty_bonus_big`) qty_bonus_big,
			  sum(a.`qty_titip`) qty_titip,
			  sum(a.`qty_titip_big`) qty_titip_big
			FROM
			  t_mutasi a
			  JOIN d_penjualan b
				ON a.`id_mutasi` = b.`mutasi_id`
			  JOIN t_penjualan c
				ON b.`id_penjualan` = c.`id_penjualan`
			  JOIN t_customer d
				ON c.`id_customer` = d.`id_t_cust`
			  JOIN t_region e
				ON d.`region` = e.`id_t_region`
			  JOIN m_material f
				ON a.id_stock_awal = f.`id_mat`
			WHERE a.`type_mutasi` = 1
			  AND a.date_mutasi BETWEEN '2022-04-1'
			  AND '2022-04-14' #WHERE a.`type_mutasi`=1 and c.date_penjualan between '2022-04-1' AND '2022-04-14' AND mutasi_id IS NOT NULL
			   AND d.region = 1
			  AND a.type_trans <> 5
			GROUP BY a.`id_stock_awal`) a) d
		  ON a.id_mat = d.id_stock_awal
		LEFT JOIN
		  (SELECT
			a.id_stock_awal,
			(
			  IF (
				a.amount_mutasi IS NULL,
				0,
				a.amount_mutasi
			  )
			) / a.unit_box AS amount_mutasi_fix,
			IF (
			  a.mutasi_big IS NULL,
			  0,
			  a.mutasi_big
			) AS mutasi_big_fix,
			(IF (a.qty_sol IS NULL, 0, a.qty_sol)) / a.unit_box AS qty_sol_fix,
			IF (
			  a.qty_sol_big IS NULL,
			  0,
			  a.qty_sol_big
			) AS qty_sol_big_fix,
			(
			  IF (a.qty_bonus IS NULL, 0, a.qty_bonus)
			) / a.unit_box AS qty_bonus_fix,
			IF (
			  a.qty_bonus_big IS NULL,
			  0,
			  a.qty_bonus_big
			) AS qty_bonus_big_fix,
			(
			  IF (a.qty_titip IS NULL, 0, a.qty_titip)
			) / a.unit_box AS qty_titip_fix,
			IF (
			  a.qty_titip_big IS NULL,
			  0,
			  a.qty_titip_big
			) AS qty_titip_big_fix
		  FROM
			(SELECT
			  id_stock_awal,
			  f.unit_box,
			  sum(a.`amount_mutasi`) amount_mutasi,
			  sum(a.`mutasi_big`) mutasi_big,
			  sum(a.`qty_sol`) qty_sol,
			  sum(a.`qty_sol_big`) qty_sol_big,
			  sum(a.`qty_bonus`) qty_bonus,
			  sum(a.`qty_bonus_big`) qty_bonus_big,
			  sum(a.`qty_titip`) qty_titip,
			  sum(a.`qty_titip_big`) qty_titip_big
			FROM
			  t_mutasi a
			  JOIN d_penjualan b
				ON a.`id_mutasi` = b.`mutasi_id`
			  JOIN t_penjualan c
				ON b.`id_penjualan` = c.`id_penjualan`
			  JOIN t_customer d
				ON c.`id_customer` = d.`id_t_cust`
			  JOIN t_region e
				ON d.`region` = e.`id_t_region`
			  JOIN m_material f
				ON a.id_stock_awal = f.`id_mat`
			WHERE a.`type_mutasi` = 1
			  AND a.date_mutasi BETWEEN '2022-04-1'
			  AND '2022-04-14' #WHERE a.`type_mutasi`=1 and c.date_penjualan between '2022-04-1' AND '2022-04-14' AND mutasi_id IS NOT NULL
			   AND d.region = 2
			  AND a.type_trans <> 5
			GROUP BY a.`id_stock_awal`) a) e
		  ON a.id_mat = e.id_stock_awal
		LEFT JOIN
		  (SELECT
			a.id_stock_awal,
			(
			  IF (
				a.amount_mutasi IS NULL,
				0,
				a.amount_mutasi
			  )
			) / a.unit_box AS amount_mutasi_fix,
			IF (
			  a.mutasi_big IS NULL,
			  0,
			  a.mutasi_big
			) AS mutasi_big_fix,
			(IF (a.qty_sol IS NULL, 0, a.qty_sol)) / a.unit_box AS qty_sol_fix,
			IF (
			  a.qty_sol_big IS NULL,
			  0,
			  a.qty_sol_big
			) AS qty_sol_big_fix,
			(
			  IF (a.qty_bonus IS NULL, 0, a.qty_bonus)
			) / a.unit_box AS qty_bonus_fix,
			IF (
			  a.qty_bonus_big IS NULL,
			  0,
			  a.qty_bonus_big
			) AS qty_bonus_big_fix,
			(
			  IF (a.qty_titip IS NULL, 0, a.qty_titip)
			) / a.unit_box AS qty_titip_fix,
			IF (
			  a.qty_titip_big IS NULL,
			  0,
			  a.qty_titip_big
			) AS qty_titip_big_fix
		  FROM
			(SELECT
			  id_stock_awal,
			  f.unit_box,
			  sum(a.`amount_mutasi`) amount_mutasi,
			  sum(a.`mutasi_big`) mutasi_big,
			  sum(a.`qty_sol`) qty_sol,
			  sum(a.`qty_sol_big`) qty_sol_big,
			  sum(a.`qty_bonus`) qty_bonus,
			  sum(a.`qty_bonus_big`) qty_bonus_big,
			  sum(a.`qty_titip`) qty_titip,
			  sum(a.`qty_titip_big`) qty_titip_big
			FROM
			  t_mutasi a
			  JOIN d_penjualan b
				ON a.`id_mutasi` = b.`mutasi_id`
			  JOIN t_penjualan c
				ON b.`id_penjualan` = c.`id_penjualan`
			  JOIN t_customer d
				ON c.`id_customer` = d.`id_t_cust`
			  JOIN t_region e
				ON d.`region` = e.`id_t_region`
			  JOIN m_material f
				ON a.id_stock_awal = f.`id_mat`
			WHERE a.`type_mutasi` = 1
			  AND a.date_mutasi BETWEEN '2022-04-1'
			  AND '2022-04-14' #WHERE a.`type_mutasi`=1 and c.date_penjualan between '2022-04-1' AND '2022-04-14' AND mutasi_id IS NOT NULL
			   AND mutasi_id IS NOT NULL
			  AND d.region = 3
			  AND a.type_trans <> 5
			GROUP BY a.`id_stock_awal`) a) f
		  ON a.id_mat = f.id_stock_awal
		LEFT JOIN
		  (SELECT
			a.id_stock_awal,
			(
			  IF (
				a.amount_mutasi IS NULL,
				0,
				a.amount_mutasi
			  )
			) / a.unit_box AS amount_mutasi_fix,
			IF (
			  a.mutasi_big IS NULL,
			  0,
			  a.mutasi_big
			) AS mutasi_big_fix,
			(IF (a.qty_sol IS NULL, 0, a.qty_sol)) / a.unit_box AS qty_sol_fix,
			IF (
			  a.qty_sol_big IS NULL,
			  0,
			  a.qty_sol_big
			) AS qty_sol_big_fix,
			(
			  IF (a.qty_bonus IS NULL, 0, a.qty_bonus)
			) / a.unit_box AS qty_bonus_fix,
			IF (
			  a.qty_bonus_big IS NULL,
			  0,
			  a.qty_bonus_big
			) AS qty_bonus_big_fix,
			(
			  IF (a.qty_titip IS NULL, 0, a.qty_titip)
			) / a.unit_box AS qty_titip_fix,
			IF (
			  a.qty_titip_big IS NULL,
			  0,
			  a.qty_titip_big
			) AS qty_titip_big_fix
		  FROM
			(SELECT
			  id_stock_awal,
			  f.unit_box,
			  sum(a.`amount_mutasi`) amount_mutasi,
			  sum(a.`mutasi_big`) mutasi_big,
			  sum(a.`qty_sol`) qty_sol,
			  sum(a.`qty_sol_big`) qty_sol_big,
			  sum(a.`qty_bonus`) qty_bonus,
			  sum(a.`qty_bonus_big`) qty_bonus_big,
			  sum(a.`qty_titip`) qty_titip,
			  sum(a.`qty_titip_big`) qty_titip_big
			FROM
			  t_mutasi a
			  JOIN d_penjualan b
				ON a.`id_mutasi` = b.`mutasi_id`
			  JOIN t_penjualan c
				ON b.`id_penjualan` = c.`id_penjualan`
			  JOIN t_customer d
				ON c.`id_customer` = d.`id_t_cust`
			  JOIN t_region e
				ON d.`region` = e.`id_t_region`
			  JOIN m_material f
				ON a.id_stock_awal = f.`id_mat`
			WHERE a.`type_mutasi` = 1
			  AND a.date_mutasi BETWEEN '2022-04-1'
			  AND '2022-04-14' #WHERE a.`type_mutasi`=1 and c.date_penjualan between '2022-04-1' AND '2022-04-14' AND mutasi_id IS NOT NULL
			   AND mutasi_id IS NOT NULL
			  AND d.region = 4
			  AND a.type_trans <> 5
			GROUP BY a.`id_stock_awal`) a) g
		  ON a.id_mat = g.id_stock_awal
		LEFT JOIN
		  (SELECT
			a.id_stock_awal,
			(
			  IF (
				a.amount_mutasi IS NULL,
				0,
				a.amount_mutasi
			  )
			) / a.unit_box AS amount_mutasi_fix,
			IF (
			  a.mutasi_big IS NULL,
			  0,
			  a.mutasi_big
			) AS mutasi_big_fix,
			(IF (a.qty_sol IS NULL, 0, a.qty_sol)) / a.unit_box AS qty_sol_fix,
			IF (
			  a.qty_sol_big IS NULL,
			  0,
			  a.qty_sol_big
			) AS qty_sol_big_fix,
			(
			  IF (a.qty_bonus IS NULL, 0, a.qty_bonus)
			) / a.unit_box AS qty_bonus_fix,
			IF (
			  a.qty_bonus_big IS NULL,
			  0,
			  a.qty_bonus_big
			) AS qty_bonus_big_fix,
			(
			  IF (a.qty_titip IS NULL, 0, a.qty_titip)
			) / a.unit_box AS qty_titip_fix,
			IF (
			  a.qty_titip_big IS NULL,
			  0,
			  a.qty_titip_big
			) AS qty_titip_big_fix
		  FROM
			(SELECT
			  id_stock_awal,
			  f.unit_box,
			  sum(a.`amount_mutasi`) amount_mutasi,
			  sum(a.`mutasi_big`) mutasi_big,
			  sum(a.`qty_sol`) qty_sol,
			  sum(a.`qty_sol_big`) qty_sol_big,
			  sum(a.`qty_bonus`) qty_bonus,
			  sum(a.`qty_bonus_big`) qty_bonus_big,
			  sum(a.`qty_titip`) qty_titip,
			  sum(a.`qty_titip_big`) qty_titip_big
			FROM
			  t_mutasi a
			  JOIN m_material f
				ON a.id_stock_awal = f.`id_mat`
			WHERE a.`type_mutasi` = 0
			  AND a.`type_trans` = 5
			  AND a.`date_mutasi` BETWEEN '2022-04-1'
			  AND '2022-04-14'
			GROUP BY a.`id_stock_awal`) a) transit_masuk
		  ON a.id_mat = transit_masuk.id_stock_awal
		LEFT JOIN
		  (SELECT
			a.id_stock_awal,
			(
			  IF (
				a.amount_mutasi IS NULL,
				0,
				a.amount_mutasi
			  )
			) / a.unit_box AS amount_mutasi_fix,
			IF (
			  a.mutasi_big IS NULL,
			  0,
			  a.mutasi_big
			) AS mutasi_big_fix,
			(IF (a.qty_sol IS NULL, 0, a.qty_sol)) / a.unit_box AS qty_sol_fix,
			IF (
			  a.qty_sol_big IS NULL,
			  0,
			  a.qty_sol_big
			) AS qty_sol_big_fix,
			(
			  IF (a.qty_bonus IS NULL, 0, a.qty_bonus)
			) / a.unit_box AS qty_bonus_fix,
			IF (
			  a.qty_bonus_big IS NULL,
			  0,
			  a.qty_bonus_big
			) AS qty_bonus_big_fix,
			(
			  IF (a.qty_titip IS NULL, 0, a.qty_titip)
			) / a.unit_box AS qty_titip_fix,
			IF (
			  a.qty_titip_big IS NULL,
			  0,
			  a.qty_titip_big
			) AS qty_titip_big_fix
		  FROM
			(SELECT
			  id_stock_awal,
			  f.unit_box,
			  sum(a.`amount_mutasi`) amount_mutasi,
			  sum(a.`mutasi_big`) mutasi_big,
			  sum(a.`qty_sol`) qty_sol,
			  sum(a.`qty_sol_big`) qty_sol_big,
			  sum(a.`qty_bonus`) qty_bonus,
			  sum(a.`qty_bonus_big`) qty_bonus_big,
			  sum(a.`qty_titip`) qty_titip,
			  sum(a.`qty_titip_big`) qty_titip_big
			FROM
			  t_mutasi a
			  JOIN m_material f
				ON a.id_stock_awal = f.`id_mat`
			WHERE a.`type_mutasi` = 1
			  AND a.`type_trans` = 5
			  AND a.`date_mutasi` BETWEEN '2022-04-1'
			  AND '2022-04-14'
			GROUP BY a.`id_stock_awal`) a) transit_keluar
		  ON a.id_mat = transit_keluar.id_stock_awal
		LEFT JOIN
		  (SELECT
			b.id_material,
			sum(a.qty) AS qty_retur_jual_tanpa_convertion,
			sum(a.qty / (f.convertion / d.base_qty)) AS qty_retur_jual,
			sum(a.qty_box) AS qty_box_retur_jual
		  FROM
			t_detail_retur a
			JOIN d_penjualan b
			  ON a.id_detail_mat = b.id_dp
			JOIN t_retur c
			  ON a.id_retur = c.id_retur
			JOIN m_material d
			  ON b.id_material = d.id_mat
			JOIN m_uom e
			  ON d.unit_terkecil = e.id_uom
			JOIN t_uom_convert f
			  ON e.id_uom = f.id_uom
		  WHERE c.date BETWEEN '2022-04-1'
			AND '2022-04-14'
		  GROUP BY id_material) retur_do
		  ON a.id_mat = retur_do.id_material
		LEFT JOIN
		  (SELECT
			b.id_material,
			sum(a.qty) AS qty_retur_jual_awal_tanpa_convertion,
			sum(a.qty / (f.convertion / d.base_qty)) AS qty_retur_jual_awal,
			sum(a.qty_box) AS qty_box_retur_jual_awal
		  FROM
			t_detail_retur a
			JOIN d_penjualan b
			  ON a.id_detail_mat = b.id_dp
			JOIN t_retur c
			  ON a.id_retur = c.id_retur
			JOIN m_material d
			  ON b.id_material = d.id_mat
			JOIN m_uom e
			  ON d.unit_terkecil = e.id_uom
			JOIN t_uom_convert f
			  ON e.id_uom = f.id_uom
		  WHERE c.date < '2022-04-1'
		  GROUP BY id_material) retur_do_awal
		  ON a.id_mat = retur_do_awal.id_material
		LEFT JOIN
		  (SELECT
			id_detail_retur_pembelian,
			d.id_material,
			sum(c.retur_qty) AS retur_po,
			sum(c.retur_qty) / e.unit_box AS retur_po_box
		  FROM
			t_retur_pembelian a
			JOIN t_detail_retur_pembelian c
			  ON a.id_invoice = c.id_pembelian
			JOIN t_po_mat d
			  ON c.id_po_mat = d.id_t_ps
			JOIN m_material e
			  ON d.id_material = e.id_mat
		  WHERE a.retur_date BETWEEN '2022-04-1'
			AND '2022-04-14'
		  GROUP BY id_material) retur_pembelian_akhir
		  ON a.id_mat = retur_pembelian_akhir.id_material
		LEFT JOIN
		  (SELECT
			id_detail_retur_pembelian,
			d.id_material,
			sum(c.retur_qty) AS retur_po_awal
		  FROM
			t_retur_pembelian a
			JOIN t_detail_retur_pembelian c
			  ON a.id_invoice = c.id_pembelian
			JOIN t_po_mat d
			  ON c.id_po_mat = d.id_t_ps
			JOIN m_material e
			  ON d.id_material = e.id_mat
		  WHERE a.retur_date < '2022-04-1'
		  GROUP BY id_material) retur_pembelian_awal
		  on retur_pembelian_awal.id_material = a.id_mat
	  where a.sts_show = 1
	  order by a.stock_name

		";
		
		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	

	function get_info($params = array()){
		
		// $this->db->select('t_purchase_order.*,t_eksternal.name_eksternal ')
		// #$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        // ->from('t_purchase_order')
        // ->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id');
		// $query = $this->db->get();
		
		$query = "
	 
		Select *,concat('".base_url()."uploads/informasi/','',gambar) as gambar_full from t_informasi
		ORDER BY `date` DESC 
		";
		
		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function update_seq($data) {
		$sql 	= 'update t_ordering set seq_max = seq_max+1 where nama_menu = "'.$data.'" ';

			
		$query 	= $this->db->query($sql);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function get_kode($params = array()){
		
		$query =  $this->db->select('seq_max as max')
		->from('t_ordering')
		->where('nama_menu','penjualan');
		;
		
		$query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	// public function get_kode($params = array()){
		
		// $query =  $this->db->select_max('seq_n', 'max');
		// $query = $this->db->get('t_penjualan');
		
		// $return = $query->result_array();

		// $this->db->close();
		// $this->db->initialize();

		// return $return;
	// }	
	
	function insert_penjualan($params){
		
		
		
		$this->db->insert('t_penjualan',$params);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
		
	}
	
	function get_item_by_id($params = array()){
		
		// $this->db->select('t_purchase_order.*,t_eksternal.name_eksternal ')
		// #$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        // ->from('t_purchase_order')
        // ->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id');
		// $query = $this->db->get();
		
		$query = "
	 
					SELECT a.id_mat,a.stock_code,a.stock_name,a.id_mat as kode,
					a.stock_description,a.base_qty,b.uom_name,b.uom_symbol,a.unit_box,c.name_eksternal,c.kode_eksternal
					FROM m_material a
					JOIN m_uom b ON a.unit_terkecil=b.id_uom
					JOIN t_eksternal c ON a.dist_id=c.id
					where a.id_mat = ".$params["id_mat"]."
					order by a.stock_name
		";
		
		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	function insert_customer($data = array()){
		
		$datas = array(
			'code_cust' => $data['code_cust'],
			'cust_name' => $data['cust_name'],
			'region' => $data['region'],
			'no_npwp' => $data['no_npwp'],
			'npwp_name' => $data['npwp_name'],
			'npwp_address' => $data['npwp_address'],
			'cust_address' => $data['cust_address'],
			'district' => $data['district'],
			'city' => $data['city'],
			'group' => $data['group'],
			'phone' => $data['phone'],
			'email' => $data['email'],
			'contact_person' => $data['contact_person'],
			'credit_limit' => $data['credit_limit'],
			'status_credit' => $data['status_credit']
		);

		// print_r($datas);die;

		$this->db->insert('t_customer',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}		
	
	public function get_penjualan($params = array()){
		
		// $query = $this->db->get_where('t_purchase_order', array('id_po' => $params['id']));
		$this->db->select('`t_penjualan`.`id_penjualan`,`t_penjualan`.`id_sales`,`t_penjualan`.`no_penjualan`,`t_penjualan`.`date_penjualan`,`t_penjualan`.`id_customer`,`t_penjualan`.`term_of_payment`,SUM(`d_penjualan`.`price`) as total_amount,`t_penjualan`.`status`,`t_penjualan`.`id_coa`,`t_penjualan`.`payment_coa`,`t_penjualan`.`id_valas`,`t_penjualan`.`rate`,`t_penjualan`.`ppn`,`t_penjualan`.`seq_n`,`t_penjualan`.`keterangan`, t_customer.*')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
		->from('t_penjualan')
		->join('d_penjualan', ' t_penjualan.id_penjualan = d_penjualan.id_penjualan')
        ->join('t_customer', 't_penjualan.id_customer = t_customer.id_t_cust')
		->where(array('t_penjualan.id_penjualan' => $params['id']));
		$query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_penjualan_detail($params = array()){
		
		$this->db->select('d_penjualan.`id_dp`,d_penjualan.`id_penjualan`,d_penjualan.`id_sales`,d_penjualan.`id_material`,d_penjualan.`unit_price`,d_penjualan.`qty`,d_penjualan.`qty_box`,d_penjualan.`qty_satuan`,d_penjualan.`box_ammount`,d_penjualan.`price`,d_penjualan.`diskon`,d_penjualan.`status`, m_material.qty as kkk,m_material.pajak,m_material.stock_name, unit_box as qty_per_box, base_qty as unit_terkecil, uom_symbol ')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('d_penjualan')
        ->join('m_material', 'd_penjualan.id_material = m_material.id_mat')
		->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
		->where(array('id_penjualan' => $params['id']));
		
		//$query = $this->db->get_where('d_penjualan', array('id_penjualan' => $params['id']));
		$query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	function insert_request($data = array()){
		
		$datas = array(
			'id_sales' => $data['id'],
			'id_invoice' => $data['id_invoice'],
			'tgl_req' => date('Y-m-d')
		);

		// print_r($datas);die;

		$this->db->insert('t_request_invoice',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}	

	function edit_customer($data = array()){
		
		$datas = array(
			
			'code_cust' => $data['code_cust'],
			'cust_name' => $data['cust_name'],
			'region' => $data['region'],
			'no_npwp' => $data['no_npwp'],
			'npwp_name' => $data['npwp_name'],
			'npwp_address' => $data['npwp_address'],
			'cust_address' => $data['cust_address'],
			'district' => $data['district'],
			'city' => $data['city'],
			'group' => $data['group'],
			'phone' => $data['phone'],
			'email' => $data['email'],
			'contact_person' => $data['contact_person'],
			'credit_limit' => $data['credit_limit'],
			'status_credit' => $data['status_credit']
		);

		// print_r($datas);die;
		$this->db->where('id_t_cust',$data['id_t_cust']);
		$this->db->update('t_customer',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	function delete_customer($data = array()){
		
		

		// print_r($datas);die;
		$this->db->where('id_t_cust', $data['id_t_cust']);
		$this->db->delete('t_customer');

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	function get_customer($params = array(),$lokasi){
		
	//print_r($lokasi);die;
		// $this->db->select('t_purchase_order.*,t_eksternal.name_eksternal ')
		// #$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        // ->from('t_purchase_order')
        // ->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id');
		// $query = $this->db->get();
		if($lokasi==0 || $lokasi==4){
			$where='0=0';
		}else{
			$where="c.id=".$params['id'];
		}
		$query = "
	 
			SELECT * FROM t_customer a
			JOIN t_region b ON a.region=b.id_t_region
			JOIN u_user c ON b.id_t_region=c.lokasi WHERE 
			".$where." GROUP BY a.`id_t_cust` "			;
		
		//echo $query;die;
		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	

	function get_user_location($params = array()){
		
		// $this->db->select('t_purchase_order.*,t_eksternal.name_eksternal ')
		// #$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        // ->from('t_purchase_order')
        // ->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id');
		// $query = $this->db->get();
		
		$query = " 
	 
		SELECT b.*,a.lokasi FROM u_user a
		JOIN u_user_group b ON a.`id`=b.`id_user`
		WHERE a.id=".$params['id']."			
		";
		
		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	function get_dpenjualan ($params = array()){
		
		// $this->db->select('t_purchase_order.*,t_eksternal.name_eksternal ')
		// #$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        // ->from('t_purchase_order')
        // ->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id');
		// $query = $this->db->get();
		
		$query = "
	 
				SELECT a.`id_coa`,a.`id_customer`,a.`id_penjualan`,a.`id_sales`,a.`id_valas`,a.`keterangan`,a.`no_penjualan`,a.`payment_coa`,
				a.`payment_coa`,a.`ppn`,a.`rate`,a.`seq_n`,a.`status`,
				a.`term_of_payment`,FORMAT(sum(c.price), 2, 'id_ID') AS total_amount,
				a.`date_penjualan`,a.`delivery_date`,
				c.`unit_price`,c.`qty`,c.`qty_box`,c.`qty_satuan`,c.`diskon`,c.`price`,b.`cust_name`,
				d.`stock_code`,d.`stock_name`,d.`base_qty`,f.`uom_symbol`
				FROM t_penjualan a
				JOIN d_penjualan c ON a.`id_penjualan`=c.`id_penjualan`
				JOIN t_customer b ON a.`id_customer`=b.`id_t_cust`
				JOIN m_material d ON c.`id_material`=d.`id_mat`
				JOIN m_uom f ON d.`unit_terkecil`=f.`id_uom`
				WHERE a.id_penjualan=".$params['id_penjualan']."
		";
		
		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	function insert_detail_penjualan ($data = array()){
		
		

		// print_r($datas);die;
		// $this->db->where('id_t_cust',$data['id_t_cust']);
		$this->db->insert('d_penjualan',$data);

			
		// $query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	function get_user($params = array()){
		
		// $this->db->select('t_purchase_order.*,t_eksternal.name_eksternal ')
		// #$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        // ->from('t_purchase_order')
        // ->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id');
		// $query = $this->db->get();
		
		$query = "
	 
					SELECT * FROM u_user
					WHERE id=".$params['id']."
		";
		
		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	
	function check_token($params){
		
		// print_r($params);die;
		
		$query = " select count(*) as CNT from u_user where `id` = '".$params['id']."' and token = '".$params['token']."' ";
		
		$query    = $this->db->query($query);
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
		
		
	}

}
