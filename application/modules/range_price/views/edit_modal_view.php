<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto
  }

  .scroll-overflow {
    min-height: 650px
  }

  #modal-distributor::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="product-payment-inner-st">
          <ul id="myTabedu1" class="tab-review-design">
            <li class="active"><a href="#description">Update Price</a></li>
          </ul>
          <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="review-content-section">

                    <form class="form-horizontal form-label-left" id="edit_material" role="form" action="<?= base_url('price_management/edit_material'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
                      <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
                      <input data-parsley-maxlength="255" type="hidden" id="id_mat" name="id_mat" class="form-control" placeholder="Kode Stok" value="<?= $stok[0]['id_mat'] ?? ''; ?>" autocomplete="off" required="required">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Kode Item <span class="required"><sup>*</sup></span></label>
                        <label class="control-label value col-md-2 col-sm-3 col-xs-12" for="nama"><?= $stok[0]['stock_code'] ?? ''; ?></sup></span></label>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Item <span class="required"><sup>*</sup></span></label>

                        <label class="control-label value col-md-2 col-sm-3 col-xs-12" for="nama"><?= $stok[0]['stock_name'] . ' ' . $stok[0]['base_qty'] . ' ' . $stok[0]['uom_symbol'] ?? ''; ?></sup></span></label>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Harga Atas <span class="required"><sup>*</sup></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input type="number" id="top_price" name="top_price" class="form-control" placeholder="Top Price" value="<?= $stok[0]['top_price'] ?? 0; ?>" autocomplete="off" required step="0.0001">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Submit</button>
                          <button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()">Batal</button>
                        </div>
                      </div>
                    </form><!-- /page content -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="PrimaryModalalert" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-close-area modal-close-df">
        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
      </div>
      <div class="modal-body">
        <i class="educate-icon educate-checked modal-check-pro"></i>
        <h2>Data Berhasil Dirubah</h2>
        <p></p>
      </div>
      <div class="modal-footer">
        <a data-dismiss="modal" href="#" onClick="back()">OK</a>
      </div>
    </div>
  </div>
</div>


<script>
  function clearform() {

    $('#edit_material').trigger("reset");

  }

  function back() {

    window.location.href = "<?php echo base_url() . 'price_management'; ?>";

  }

  function listdist() {
    var user_id = '0001';
    var token = '093940349';

  }

  $(document).ready(function() {

    listdist();

    $('#edit_material').on('submit', function(e) {
      // validation code here
      //if(!valid) {
      e.preventDefault();

      var formData = new FormData(this);
      var urls = $(this).attr('action');

      swal({
        title: 'Yakin akan Merubah Data ?',
        text: '',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak'
      }).then(function() {

        $.ajax({
          type: 'POST',
          url: urls,
          data: formData,
          cache: false,
          contentType: false,
          processData: false,
          success: function(response) {
            if (response.success == true) {
              // $('#PrimaryModalalert').modal('show');
              swal({
                title: 'Success!',
                text: response.message,
                type: 'success',
                showCancelButton: false,
                confirmButtonText: 'Ok'
              }).then(function() {
                back();
              })
            } else {
              $('#msg_err').html(response.message);
              $('#WarningModalftblack').modal('show');
            }
          }
        });
      });

    });

  });
</script>