<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto;
  }

  .scroll-overflow {
    min-height: 650px;
  }

  #modal-distributor::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }

  .select2-container .select2-choice {

    height: 35px !important;

  }

  .DTFC_LeftBodyLiner {
    overflow-x: hidden;
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="product-payment-inner-st">
          <ul id="myTabedu1" class="tab-review-design">
            <li class="active"><a href="#description">Report Hutang</a></li>
          </ul>
          <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="review-content-section" style="margin-bottom:-35px;">
                    <form id="add_po" action="<?php echo base_url() . 'report_hutang/reports'; ?>" class="add-department" method="post" target="_blank" autocomplete="off">
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                              <label>Principal</label>
                              <input name="ints" id="ints" type="hidden" class="form-control" placeholder="Tanggal Purchase Order" value='0'>
                              <input name="ints2" id="ints2" type="hidden" class="form-control" placeholder="Tanggal Purchase Order" value='0'>
                              <select name="name" id="name" class="form-control" placeholder="Nama Customer">
                                <option value="0" selected="selected">Semua Principal</option>
                                <?php
                                foreach ($result as $principals) {
                                  echo '<option value="' . $principals['id'] . '" >' . $principals['name_eksternal'] . '</option>';
                                }
                                ?>
                              </select>
                            </div>
                          </div>
                          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                            <div class="form-group date">
                              <label>Tanggal Awal</label>
                              <input name="tgl_awal" id="tgl_awal" type="text" class="form-control" placeholder="Tanggal Awal">
                            </div>
                          </div>
                          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                            <div class="form-group date">
                              <label>Tanggal Akhir</label>
                              <input name="tgl_akhir" id="tgl_akhir" type="text" class="form-control" placeholder="Tanggal Akhir">
                            </div>
                          </div>
                          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="form-group" style="margin-top:35px;">
                                  <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                  </div>
                                  <div class="col-md-6">
                                    <button type="button" class="btn btn-danger waves-effect waves-light" onClick="export_exc()"> Export</button>
                                    <!-- <button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()"> Batal</button> -->
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
                  <br><br>
                  <input name="int_flo" id="int_flo" type="hidden" class="form-control" placeholder="Qty" value=0;>
                  <div id="table_items"></div>
                  <br>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr>
        <div id="myTabContent" class="tab-content custom-product-edit">
          <div class="product-tab-list tab-pane fade active in" id="description">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <input name="int_flo" id="int_flo" type="hidden" class="form-control" placeholder="Qty" value=0;>
                <div id="table_items" style="overflow-x:auto;height:600px">
                  <!-- <button type="button" class="btn btn-danger waves-effect waves-light" onClick="export_exc()"> Export</button> -->
                  <table id="datatable_pricipal" class="table table-striped table-bordered">
                    <thead>
                      <tr>
                        <th>Principle</th>
                        <th>Tgl Terima</th>
                        <th>Order Pembelian</th>
                        <th>No Invoice</th>
                        <th>Jatuh Tempo</th>
                        <th>Umur</th>
                        <th>Saldo</th>
                        <th>Bulan Berjalan</th>
                        <th>Tgl Bayar</th>
                        <th>Dibayar</th>
                        <th>Sisa</th>
                        <th>Sudah Diinvoice</th>
                        <th>
                          < 1 bulan</th>
                        <th>1 bulan</th>
                        <th>2 bulan</th>
                        <th>3 bulan</th>
                        <th>> 3 bulan</th>
                        <th>Jumlah</th>
                      </tr>
                    </thead>

                    <tbody id='body_tbl' style="">

                    </tbody>
                  </table>


                </div>

              </div>
            </div>
            <br><br>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<div id="ModalAddPo" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-close-area modal-close-df">
        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
      </div>
      <div class="modal-body">
        <i class="educate-icon educate-checked modal-check-pro"></i>
        <h2>Data Berhasil Disimpan</h2>
        <p>Apakah Anda Ingin Menambah Data Po Lagi ?</p>
      </div>
      <div class="modal-footer">
        <a data-dismiss="modal" href="#" onClick="back()">Tidak</a>
        <a data-dismiss="modal" href="#" onClick="clearform()">Ya</a>
      </div>
    </div>
  </div>
</div>

<div id="ModalChangePrincipal" class="modal modal-edu-general Customwidth-popup-WarningModal PrimaryModal-bgcolor fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-close-area modal-close-df">
        <!--<a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>-->
      </div>
      <div class="modal-body">
        <span class="educate-icon educate-warning modal-check-pro information-icon-pro"></span>
        <h2>Warning!</h2>
        <p id="msg_err">The Modal plugin is a dialog box/popup window that is displayed on top of the current page</p>
      </div>
      <div class="modal-footer footer-modal-admin warning-md">
        <a data-dismiss="modal" href="#" onClick="okchange()">Ok</a>
        <a data-dismiss="modal" href="#" onClick="cancelchange()">Batal</a>
      </div>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {

    var user_id = '0001';
    var token = '093940349';

    // $('#datatable_pricipal').DataTable({
    // //"dom": 'rtip',
    // "bFilter": false,
    // "aaSorting": false,
    // "bLengthChange": true,
    // 'iDisplayLength': 10,
    // "sPaginationType": "simple_numbers",
    // "Info": false,
    // "processing": true,
    // "serverSide": true,
    // "destroy": true,
    // "ajax": "<?php echo base_url() . 'report_penjualan/lists' ?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token,
    // "searching": true,
    // "language": {
    // "decimal": ",",
    // "thousands": "."
    // },
    // "dom": 'l<"toolbar">frtip',
    // "initComplete": function() {
    // $("div.toolbar").prepend('');
    // }
    // });

    //$('#kode_0').select2();
    $('#name').val('0');
    $('#name').select2();

    $('.rupiah').priceFormat({
      prefix: '',
      centsSeparator: ',',
      centsLimit: 0,
      thousandsSeparator: '.'
    });

    $('#tgl_awal').datepicker({
      format: "yyyy-mm-dd",
      autoclose: true,
      todayHighlight: true
    });

    $('#tgl_akhir').datepicker({
      format: "yyyy-mm-dd",
      autoclose: true,
      todayHighlight: true
    });

    // listdist();


  });

  $('#add_po').on('submit', function(e) {
    // validation code here
    //if(!valid) {
    e.preventDefault();

    // var formData = new FormData(this);
    var formData = new FormData();
    formData.append('name', $('#name').val());
    formData.append('tgl_awal', $('#tgl_awal').val());
    formData.append('tgl_akhir', $('#tgl_akhir').val());

    swal({
      title: 'Loading',
      text: 'Loading Data',
      // type: 'success',
      showCancelButton: false,
      showConfirmButton: false,
      imageUrl: '<?php echo base_url(); ?>assets/images/ajax-loader.gif',
      confirmButtonText: 'Ok',
      allowOutsideClick: false

    }).then(function() {})

    var user_id = '0001';
    var token = '093940349';

    var int_val = parseInt($('#int_flo').val());
    //console.log(formData);

    $.fn.dataTable.ext.errMode = 'none';

    $('#datatable_pricipal').on('error.dt', function(e, settings, techNote, message) {
      console.log('An error has been reported by DataTables: ', message);
    });

    $.ajax({
      type: 'POST',
      url: $(this).attr('action'),
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success: function(response) {

        //console.log(response.html);
        $('#body_tbl').html('')

        $('#datatable_pricipal').dataTable().fnClearTable();
        $('#datatable_pricipal').dataTable().fnDestroy();

        $('#body_tbl').html(response.html)
        //body_tbl
        $("#datatable_pricipal").dataTable({
          "processing": true,
          "searchDelay": 700,
          "responsive": false,
          "lengthChange": false,
          "bPaginate": false,
          "info": false,
          "bSort": false,
          "dom": 'l<"toolbar">frtip',
          "scrollX": true,
          "sScrollY": "300px",
          // fixedColumns: {
          // leftColumns: 2
          // },
          "initComplete": function() {
            swal.close()
          },
        });

      }
    });


    //alert(kode);


    //}
  });

  function export_exc() {

    var name = $('#name').val();
    var tgl_awal = $('#tgl_awal').val();
    var tgl_akhir = $('#tgl_akhir').val();

    var url = '<?php echo base_url(); ?>report_hutang/export_excel';

    var form = $("<form action='" + url + "' method='post' target='_blank'>" +
      "<input type='hidden' name='name' value='" + name + "' />" +
      "<input type='hidden' name='tgl_awal' value='" + tgl_awal + "' />" +
      "<input type='hidden' name='tgl_akhir' value='" + tgl_akhir + "' />" +
      "</form>");
    $('body').append(form);
    form.submit();


  }
</script>