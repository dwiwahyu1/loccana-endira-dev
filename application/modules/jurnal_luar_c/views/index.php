<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto;
  }

  .scroll-overflow {
    min-height: 650px;
  }

  #modal-distributor::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
          <div class="card-box table-responsive">
            <div class="dropdown pull-right">
              <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                <i class="zmdi zmdi-more-vert"></i>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>

            <h4 class="header-title m-t-0 m-b-30">Jurnal Keluar</h4>

            <table id="listpemohon" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th class="dt-body-center" style="width: 5%;">No</th>
                  <th>Coa Debit</th>
                  <th>Coa Kredit</th>
                  <th>Tanggal</th>
                  <th>Total</th>
                  <th>Keterangan</th>
                  <th>Option</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div><!-- end col -->
      </div>
    </div>
  </div>
  <span id="laod"></span>
</div>



<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content p-0 b-0">
      <div class="panel panel-color panel-primary panel-filled">
        <div class="panel-heading">
          <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 class="panel-title"></h3>
        </div>
        <div class="panel-body">
          <p></p>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
  function add_user() {
    $('#panel-modal').removeData('bs.modal');
    $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    $('#panel-modal  .panel-body').load('<?php echo base_url('jurnal_luar/add'); ?>');
    $('#panel-modal  .panel-title').html('<i class="fa fa-plus"></i> Add Uom');
    $('#panel-modal').modal({
      backdrop: 'static',
      keyboard: false
    }, 'show');
  }

  function edituom(id) {
    var url = '<?php echo base_url(); ?>jurnal_luar/edit';

    $("#laod").append(' <img id="loading" src="<?php echo base_url(); ?>assets/urate-frontend-master/assets/images/icon_loader.gif">');
    var form = $("<form action='" + url + "' method='post'>" +
      "<input type='hidden' name='iduom' value='" + id + "' />" +
      "</form>");
    $('body').append(form);
    form.submit();
  }

  function detail(id) {
    var url = '<?php echo base_url(); ?>jurnal_luar/detail';

    $("#laod").append(' <img id="loading" src="<?php echo base_url(); ?>assets/urate-frontend-master/assets/images/icon_loader.gif">');
    var form = $("<form action='" + url + "' method='post'>" +
      "<input type='hidden' name='iduom' value='" + id + "' />" +
      "</form>");
    $('body').append(form);
    form.submit();
  }

  function deleteuom(id) {
    swal({
      title: 'Yakin akan Menghapus ?',
      text: 'data tidak dapat dikembalikan bila sudah dihapus !',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak'
    }).then(function() {
      var datapost = {
        "id": id
      };
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>jurnal_luar/deletes",
        data: JSON.stringify(datapost),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function(response) {

          swal({
            title: 'Success!',
            text: response.message,
            type: 'success',
            showCancelButton: false,
            confirmButtonText: 'Ok'
          }).then(function() {
            window.location.href = "<?php echo base_url('jurnal_luar'); ?>";
          })

          if (response.status == "success") {

          } else {
            swal("Failed!", response.message, "error");
          }
        }
      });
    })

  }

  $(document).ready(function() {
    var user_id = '0001';
    var token = '093940349';
    var filter_month = '<?php echo $today_month;  ?>';
    var filter_year = '<?php echo $today_year;  ?>';

    $("#listpemohon").dataTable({
      "processing": true,
      "serverSide": true,
      "ajax": "<?php echo base_url() . 'jurnal_luar/lists' ?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token + "&filter_month=" + filter_month + "&filter_year=" + filter_year,
      "searchDelay": 700,
      "responsive": true,
      "lengthChange": false,
      "info": false,
      "bSort": false,
      "dom": 'l<"toolbar">frtip',
      "initComplete": function() {
        $("div.toolbar").prepend('<div class="btn-group pull-left" style="display:flex; justify-content:left; align-items:center;"> <a href="<?php echo base_url() . 'jurnal_luar/add'; ?>" type="button" class="btn btn-custon-rounded-two btn-primary" style="display: <?php echo $priv['insert'] ?>;"> <i class="fa fa-plus"></i> </a> <?php echo $filter_year . '' . $filter_month ?> <button class="btn btn-primary" type="button" onclick="export_excel()">Export</button></div>');
      },
      "columnDefs": [{
        targets: [0],
        className: 'dt-body-center'
      }]
    });


  });

  function changeFilter() {

    var user_id = '0001';
    var token = '093940349';
    //var filter = $('#filter_table').val(); 
    var filter_month = $('#filter_month').val();
    var filter_year = $('#filter_year').val();


    //alert(filter);

    $('#listpemohon').DataTable({
      //"dom": 'rtip',
      "bFilter": false,
      "aaSorting": [],
      "bLengthChange": true,
      'iDisplayLength': 10,
      "sPaginationType": "simple_numbers",
      "Info": false,
      "processing": true,
      "serverSide": true,
      "destroy": true,
      "ajax": "<?php echo base_url() . 'jurnal_luar/lists' ?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token + "&filter_month=" + filter_month + "&filter_year=" + filter_year,
      "searching": true,
      "language": {
        "decimal": ",",
        "thousands": "."
      },
      "dom": 'l<"toolbar">frtip',
      "initComplete": function() {
        $("div.toolbar").prepend('<div class="btn-group pull-left" style="display:flex; justify-content:left; align-items:center;"> <a href="<?php echo base_url() . 'jurnal_luar/add'; ?>" type="button" class="btn btn-custon-rounded-two btn-primary" style="display: <?php echo $priv['insert'] ?>;"> <i class="fa fa-plus"></i> </a> <?php echo $filter_year . '' . $filter_month ?> <button class="btn btn-primary" type="button" onclick="export_excel()">Export</button></div>');
        $('#filter_month').val(filter_month);
        $('#filter_year').val(filter_year);
      }
    });
  }

  function export_excel() {
    var form = document.getElementById('export-excel');

    if (!form) {
      form = document.createElement('form');
      document.body.append(form);
    }

    var year_filter = document.getElementById('year_filter');

    if (!year_filter) {
      year_filter = document.createElement('input');
      year_filter.setAttribute('id', 'year_filter');
      year_filter.setAttribute('name', 'year_filter');
      year_filter.setAttribute('type', 'hidden');
      form.appendChild(year_filter);
    }

    year_filter.value = document.getElementById('filter_year').value;

    var month_filter = document.getElementById('month_filter');

    if (!month_filter) {
      month_filter = document.createElement('input');
      month_filter.setAttribute('id', 'month_filter');
      month_filter.setAttribute('name', 'month_filter');
      month_filter.setAttribute('type', 'hidden');
      form.appendChild(month_filter);
    }

    month_filter.value = document.getElementById('filter_month').value;

    form.setAttribute('id', 'export-excel');
    form.setAttribute('method', 'POST');
    form.setAttribute('action', '<?= base_url('jurnal_luar/export'); ?>');

    form.submit();
  }
</script>