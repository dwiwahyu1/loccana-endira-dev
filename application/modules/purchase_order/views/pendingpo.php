<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto;
  }

  .scroll-overflow {
    min-height: 650px
  }

  #modal-distributor::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }

  .select2-container .select2-choice {

    height: 35px !important;

  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="product-payment-inner-st">
          <ul id="myTabedu1" class="tab-review-design">
            <li class="active"><a href="#description">Pending Purchase Order</a></li>
          </ul>
          <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="review-content-section" style="margin-bottom: -35px;">
                    <form id="searchRekap">
                      <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="form-group">
                              <label>Principle</label>
                              <input name="ints" id="ints" type="hidden" class="form-control" placeholder="Tanggal Purchase Order" value='0'>
                              <input name="ints2" id="ints2" type="hidden" class="form-control" placeholder="Tanggal Purchase Order" value='0'>
                              <select name="principal" id="principal" class="form-control" placeholder="Nama Customer">
                                <option value="0" selected="selected">Semua Principle</option>
                                <?php foreach ($principal as $p) {
                                  echo '<option value="' . $p['id'] . '" >' . $p['name_eksternal'] . '</option>';
                                }
                                ?>
                              </select>
                            </div>
                          </div>
                          <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form-group date">
                              <label>Tahun</label>
                              <select class="form-control" id="filter_year">
                                
                                <?php foreach($years as $y) : ?>
                                  <option value="<?= $y ?>"><?= $y ?></option>
                                <?php endforeach; ?>
                              </select>
                              </select>
                            </div>
                          </div>
                          <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="form-group date">
                              <label>Bulan</label>
                              <select class="form-control" id="filter_month">
                                <?php foreach($month as $m) : ?>
                                <option value="<?= $m ?>" ><?= $m ?></option>
                                <?php endforeach; ?>
                              </select>
                            </div>
                          </div>
                          <div class="col-lg-1 col-md-1 col-sm-1">
                            <div class="form-group" style="margin-top:35px;">
                              <div class="payment-adress">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                <!-- <button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()"> Batal</button> -->
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-1 col-md-1 col-sm-1">
                            <div class="form-group" style="margin-top:35px;">
                                <button type="button" class="btn btn-primary waves-effect waves-light" onclick="exportExcel()">Export</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <input name="int_flo" id="int_flo" type="hidden" class="form-control" placeholder="Qty" value=0;>
                      <div id="table_items"></div>
                    </form>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <hr>
          <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                  <div id="table_items" style="overflow-x: scroll;">

                    <table id="datatableRekapPo" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th width="5%">No</th>
                          <th>Nomor PO</th>
                          <th>Tanggal PO</th>
                          <th>Produk</th>
                          <th>Kemasan</th>
                          <th>Tgl Receive</th>
                          <th>Sum Of Qlt</th>
                          <th>Sum Of Q Box</th>
                          <th>Receiving</th>
                          <th>Outstanding Qt</th>
                          <th>Sum of Sisa Box</th>
                          <th>Sum of V PO Outstanding</th>
                        </tr>
                      </thead>
                      <tbody id="bodyTableRekapPo">
                      
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
        

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>



<div id="ModalAddPo" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-close-area modal-close-df">
        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
      </div>
      <div class="modal-body">
        <i class="educate-icon educate-checked modal-check-pro"></i>
        <h2>Data Berhasil Disimpan</h2>
        <p>Apakah Anda Ingin Menambah Data Po Lagi ?</p>
      </div>
      <div class="modal-footer">
        <a data-dismiss="modal" href="#" onClick="back()">Tidak</a>
        <a data-dismiss="modal" href="#" onClick="clearform()">Ya</a>
      </div>
    </div>
  </div>
</div>

<div id="ModalChangePrincipal" class="modal modal-edu-general Customwidth-popup-WarningModal PrimaryModal-bgcolor fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-close-area modal-close-df">
        <!--<a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>-->
      </div>
      <div class="modal-body">
        <span class="educate-icon educate-warning modal-check-pro information-icon-pro"></span>
        <h2>Warning!</h2>
        <p id="msg_err">The Modal plugin is a dialog box/popup window that is displayed on top of the current page</p>
      </div>
      <div class="modal-footer footer-modal-admin warning-md">
        <a data-dismiss="modal" href="#" onClick="okchange()">Ok</a>
        <a data-dismiss="modal" href="#" onClick="cancelchange()">Batal</a>
      </div>
    </div>
  </div>
</div>

<script>
  $('#name').select2();
  $('#filter_month').select2();
  $('#filter_year').select2();
  


  $('#searchRekap').on('submit', function(e) {
    swal({
      title: 'Loading',
      text: 'Loading Data',
      // type: 'success',
      showCancelButton: false,
      showConfirmButton: false,
      imageUrl: '<?php echo base_url(); ?>assets/images/ajax-loader.gif',
      confirmButtonText: 'Ok',
      allowOutsideClick: false

    }).then(function() {})

    e.preventDefault();

    var formData = new FormData();
    formData.append('principal', $('#principal').val());
    formData.append('filter_month', $('#filter_month').val());
    formData.append('filter_year', $('#filter_year').val());


    var user_id = '0001';
    var token = '093940349';

    var int_val = parseInt($('#int_flo').val());


    $.ajax({
      type: 'get',
      url: "<?php echo base_url() . 'purchase_order/list_pending' ?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token + "&principal=" + $('#principal').val() + "&filter_month=" + $('#filter_month').val() + "&filter_year=" + $('#filter_year').val(),
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success: function(response) {

        $('#datatableRekapPo').dataTable().fnClearTable();
        $('#datatableRekapPo').dataTable().fnDestroy();

        $('#bodyTableRekapPo').html(response.html);
        $("#datatableRekapPo").dataTable({
          "processing": true,
          "searchDelay": 700,
          "responsive": false,
          "lengthChange": false,
          "bPaginate": false,
          "info": false,
          "bSort": false,
          "dom": 'l<"toolbar">frtip',
          "scrollX": true,
          "sScrollY": "300px",
          // fixedColumns: {
          // leftColumns: 2
          // },
          "initComplete": function() {
            swal.close()
          },
        });
      }
    });

  });

  function exportExcel(){
    swal({
      title: 'Loading',
      text: 'Loading Data',
      // type: 'success',
      showCancelButton: false,
      showConfirmButton: false,
      imageUrl: '<?php echo base_url(); ?>assets/images/ajax-loader.gif',
      confirmButtonText: 'Ok',
      allowOutsideClick: false

    }).then(function() {})

    var user_id = '0001';
    var token = '093940349';

    var formData = new FormData();
    formData.append('name', $('#name').val());
    formData.append('tgl_awal', $('#tgl_awal').val());
    formData.append('tgl_akhir', $('#tgl_akhir').val());

    $.ajax({
      type: 'get',
      url: "<?php echo base_url() . 'purchase_order/list_pending' ?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token + "&name=" + $('#name').val() + "&tgl_awal=" + $('#tgl_awal').val() + "&tgl_akhir=" + $('#tgl_akhir').val(),
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success: function(response) {
        swal.close()
      }
    });
  }
  $('#principal').select2();
</script>