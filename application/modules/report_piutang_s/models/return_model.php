<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Return_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}


	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}
	public function reports($data) {

		// echo"<pre>";print_r($data);die;
		$penjualan_a = 0;
		$pengeluaran_a = 0;
		$saldo_akhir_a = 0;
		$nilai_akhir_a = 0;
		$invoice_saldo_a = 0;
		$sisa_aa = 0;
		$d0_a = 0;
		$d30_a = 0;
		$d60_a = 0;
		$d90_a = 0;
		$d120_a = 0;
		$total_t_a = 0;

		if ($data['principal'] == 0) {

			$dist_a = $this->get_distribution($data);
			$html = '';


			//print_r($dist_a);die;

			foreach ($dist_a as $dist_as) {

				$datat = array(
					'customer' => $dist_as['id_customer'],
					'tgl_awal' => $data['tgl_awal'],
					'tgl_akhir' => $data['tgl_akhir'],
					'region' => $data['region']

				);

				$result = $this->get_report_persediaan($datat);

				//print_r($result);die;

				$penjualan = 0;
				$pengeluaran = 0;
				$saldo_akhir = 0;
				$nilai_akhir = 0;
				$invoice_saldo = 0;
				$sisa_a = 0;
				$sisa_b = 0;
				$prin = '';

				$d0 = 0;
				$d30 = 0;
				$d60 = 0;
				$d90 = 0;
				$d120 = 0;
				$total_t = 0;

				$curr_invoice = '';
				foreach ($result as $datas) {

					// $datas['bayar'] = $datas['bayar'] - $datas['bayar_giro'];
					if ($curr_invoice <> $datas['no_invoice']) {

						$bayar_r_te =  number_format($datas['bayar'], 2, ',', '.');
						$bayar_r =  $datas['bayar'];
						$bb =  $datas['bayar'];
						$bb_saldo =  $datas['bayar_saldo'];
						$retur_saldo =  $datas['nilai_retur_saldo'];
						$curr_invoice = $datas['no_invoice'];
					} else {
						$bayar_r = 0;
						$retur_saldo = 0;
						$bb_saldo = 0;
						$bb = 0;
					}

					$prcs =  $datas['price_full'] - $datas['bayar'] - $datas['bayar_saldo'] - $datas['nilai_retur'] - $datas['nilai_retur_saldo'];

					$bb = $bb - $prcs;

					$ssisss = ($datas['saldo'] - $bb_saldo - $retur_saldo) + $datas['bulan_berjalan'] - ($datas['bayar'] + $datas['nilai_retur']);

					// if ($datas['account_type'] == 1 && $datas['payment_status'] == 0) {
					// 	$datas['tgl_bayar'] = '';
					// 	$datas['tgl_retur'] = '';
					// } else {
					// 	// $datas['tgl_jatuh_tempo']='';
					// }

					if ($bb > 0) {
						$ss_sisa = 0;
					} else {
						$ss_sisa = abs($bb);
					}

					$ss_sisa = $ssisss;


					if ($datas['umur'] < 31) {
						$m1 = $ss_sisa;
						$m2 = 0;
						$m3 = 0;
						$m4 = 0;
						$m5 = 0;
					} elseif ($datas['umur'] > 30 && $datas['umur'] < 61) {
						$m1 = 0;
						$m2 = $ss_sisa;
						$m3 = 0;
						$m4 = 0;
						$m5 = 0;
					} elseif ($datas['umur'] > 60 && $datas['umur'] < 91) {
						$m1 = 0;
						$m2 = 0;
						$m3 = $ss_sisa;
						$m4 = 0;
						$m5 = 0;
					} elseif ($datas['umur'] > 90 && $datas['umur'] < 121) {
						$m1 = 0;
						$m2 = 0;
						$m3 = 0;
						$m4 = $ss_sisa;
						$m5 = 0;
					} else {
						$m1 = 0;
						$m2 = 0;
						$m3 = 0;
						$m4 = 0;
						$m5 = $ss_sisa;
					}

					$ttl = $m1 + $m2 + $m3 + $m4 + $m5;

					if ($ssisss > 1 || ($datas['bayar'] + $datas['nilai_retur'])>1) {
						$html .= '<tr>
												<th >' . $datas['cust_name'] . '</th>
												<th>' . $datas['tanggal_invoice'] . '</th>
												<th>' . $datas['no_penjualan'] . '</th>
												<th>' . $datas['due_date'] . '</th>
												<th>' . $datas['umur'] . '</th>
												<th>' . number_format($datas['saldo'] - $bb_saldo - $retur_saldo, 2, ',', '.') . '</th>
												<th>' . number_format($datas['bulan_berjalan'], 2, ',', '.') . '</th>
												<th>' . ($datas['tgl_bayar'] . ' ' . $datas['tgl_retur']) . '</th>
												<th>' .number_format($datas['bayar'] + $datas['nilai_retur'], 2, ',', '.') . '</th>
												<th>' . number_format($ss_sisa, 2, ',', '.') . '</th>
												<th>' . $datas['tgl_jatuh_tempo'] . '</th>
												<th>' . $datas['bayar_giro_concat'] . '</th>
												<th>' . number_format($m1, 2, ',', '.') . '</th>
												<th>' . number_format($m2, 2, ',', '.') . '</th>
												<th>' . number_format($m3, 2, ',', '.') . '</th>
												<th>' . number_format($m4, 2, ',', '.') . '</th>
												<th>' . number_format($m5, 2, ',', '.') . '</th>
												<th>' . number_format($ttl, 2, ',', '.') . '</th>
											</tr>';
					}
					// $ttl = $datas['min_30']-( $datas['min_30']*($datas['diskons']/100)) + ( ($datas['min_30']-( $datas['min_30']*($datas['diskons']/100) ))*($datas['pajak']/100)) + $datas['d30']-( $datas['d30']*($datas['diskons']/100)) + ( ($datas['d30']-( $datas['d30']*($datas['diskons']/100) ))*($datas['pajak']/100) ) + $datas['d60']-( $datas['d60']*($datas['diskons']/100)) + ( ($datas['d60']-( $datas['d60']*($datas['diskons']/100) ))*($datas['pajak']/100) ) + $datas['d90']-( $datas['d90']*($datas['diskons']/100)) + ( ($datas['d90']-( $datas['d90']*($datas['diskons']/100) ))*($datas['pajak']/100) ) + $datas['d120']-( $datas['d120']*($datas['diskons']/100)) + ( ($datas['d120']-( $datas['d120']*($datas['diskons']/100) ))*($datas['pajak']/100) );

					// $prcs = $datas['prices']-( $datas['prices']*($datas['diskons']/100)) + ( ($datas['prices']-( $datas['prices']*($datas['diskons']/100) ))*($datas['pajak']/100) );

					$prin = $datas['cust_name'];


					$curr_invoice = $datas['no_invoice'];

					$penjualan = $penjualan + ($datas['saldo'] - ($datas['saldo'] * ($datas['diskon'] / 100))) - $bb_saldo - $retur_saldo;
					$pengeluaran = $pengeluaran + ($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskon'] / 100)));
					$saldo_akhir = $saldo_akhir + $bayar_r;
					$sisa_a = $sisa_a + $ss_sisa;
					$nilai_akhir = $nilai_akhir + $datas['bayar'] + $datas['nilai_retur'];
					$invoice_saldo = $invoice_saldo + $datas['invoice_saldo'];
					// // $d0 = $d0 + $datas['min_30']-( $datas['min_30']*($datas['diskons']/100)) + ( ($datas['min_30']-( $datas['min_30']*($datas['diskons']/100) ))*($datas['pajak']/100));
					// // $d30 = $d30 + $datas['d30']-( $datas['d30']*($datas['diskons']/100)) + ( ($datas['d30']-( $datas['d30']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// // $d60 = $d60 + $datas['d60']-( $datas['d60']*($datas['diskons']/100)) + ( ($datas['d60']-( $datas['d60']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// // $d90 = $d90 + $datas['d90']-( $datas['d90']*($datas['diskons']/100)) + ( ($datas['d90']-( $datas['d90']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// // $d120 = $d120 + $datas['d120']-( $datas['d120']*($datas['diskons']/100)) + ( ($datas['d120']-( $datas['d120']*($datas['diskons']/100) ))*($datas['pajak']/100) );

					$d0 = $d0 + $m1;
					$d30 = $d30 + $m2;
					$d60 = $d60 + $m3;
					$d90 = $d90 + $m4;
					$d120 = $d120 + $m5;
					$total_t = $total_t + $ttl;

					$penjualan_a = $penjualan_a + ($datas['saldo'] - ($datas['saldo'] * ($datas['diskon'] / 100))) - $bb_saldo - $retur_saldo;
					$pengeluaran_a = $pengeluaran_a + ($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskon'] / 100)));
					$saldo_akhir_a = $saldo_akhir_a +  $bayar_r;
					$sisa_aa = $sisa_aa + $ss_sisa;

					$nilai_akhir_a = $nilai_akhir_a +  $datas['bayar'] + $datas['nilai_retur'];
					$invoice_saldo_a = $invoice_saldo_a +  $datas['invoice_saldo'];

					// // $d0_a = $d0_a + $datas['min_30']-( $datas['min_30']*($datas['diskons']/100)) + ( ($datas['min_30']-( $datas['min_30']*($datas['diskons']/100) ))*($datas['pajak']/100));
					// // $d30_a = $d30_a + $datas['d30']-( $datas['d30']*($datas['diskons']/100)) + ( ($datas['d30']-( $datas['d30']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// // $d60_a = $d60_a + $datas['d60']-( $datas['d60']*($datas['diskons']/100)) + ( ($datas['d60']-( $datas['d60']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// // $d90_a = $d90_a + $datas['d90']-( $datas['d90']*($datas['diskons']/100)) + ( ($datas['d90']-( $datas['d90']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// // $d120_a = $d120_a + $datas['d120']-( $datas['d120']*($datas['diskons']/100)) + ( ($datas['d120']-( $datas['d120']*($datas['diskons']/100) ))*($datas['pajak']/100) );

					$d0_a = $d0_a + $m1;
					$d30_a = $d30_a + $m2;
					$d60_a = $d60_a + $m3;
					$d90_a = $d90_a + $m4;
					$d120_a = $d120_a + $m5;
					$total_t_a = $total_t_a + $ttl;
				}

				if ($sisa_a > 1 || $saldo_akhir>1) {
					$html .= '<tr style="background:yellow">
				<th>' . $prin . '</th>
				<th></th>
				<th ></th>
				<th ></th>	
				<th></th>
				<th>' . number_format($penjualan, 2, ',', '.') . '</th>
				<th>' . number_format($pengeluaran, 2, ',', '.') . '</th>
				<th></th>
				<th>' . number_format($saldo_akhir, 2, ',', '.') . '</th>
				<th>' . number_format($sisa_a, 2, ',', '.') . '</th>
				<th>' . number_format(0, 2, ',', '.') . '</th>
				<th>' . number_format(0, 2, ',', '.') . '</th>
				<th>' . number_format($d0, 2, ',', '.') . '</th>
				<th>' . number_format($d30, 2, ',', '.') . '</th>
				<th>' . number_format($d60, 2, ',', '.') . '</th>
				<th>' . number_format($d90, 2, ',', '.') . '</th>
				<th>' . number_format($d120, 2, ',', '.') . '</th>
				<th>' . number_format($ttl, 2, ',', '.') . '</th>
				</tr>';
				}
			}
		} else {

			$datat = array(
				'customer' => $data['principal'],
				'tgl_awal' => $data['tgl_awal'],
				'tgl_akhir' => $data['tgl_akhir'],
				'region' => $data['region']

			);

			$result = $this->get_report_persediaan($datat);

			//print_r($result);die;

			$html = '';

			$penjualan = 0;
			$pengeluaran = 0;
			$saldo_akhir = 0;
			$nilai_akhir = 0;
			$invoice_saldo = 0;
			$prin = '';
			$sisa_a = 0;
			$sisa_b = 0;

			$d0 = 0;
			$d30 = 0;
			$d60 = 0;
			$d90 = 0;
			$d120 = 0;
			$total_t = 0;

			$curr_invoice = '';
			foreach ($result as $datas) {

				//ECHO $datas['nilai_retur'];
					// $datas['bayar'] = $datas['bayar'] - $datas['bayar_giro'];

				if ($curr_invoice <> $datas['no_invoice']) {
					$bayar_r =  $datas['bayar'];
					$bb =  $datas['bayar'];
					$bb_saldo =  $datas['bayar_saldo'];
					$retur_saldo =  $datas['nilai_retur_saldo'];
					$curr_invoice = $datas['no_invoice'];
				} else {
					$bayar_r = 0;
					$retur_saldo = 0;
					$bb_saldo = 0;
					$bb = 0;
				}

				$ssisss = ($datas['saldo'] - $bb_saldo - $retur_saldo) + $datas['bulan_berjalan'] - ($datas['bayar'] + $datas['nilai_retur']);


				if ($datas['account_type'] == 1 && $datas['payment_status'] == 0) {
					$datas['tgl_bayar'] = '';
					$datas['tgl_retur'] = '';
				} else {
					// $datas['tgl_jatuh_tempo']='';
				}

				$prcs = $datas['price_full'] - $datas['bayar'] - $datas['bayar_saldo'] - $datas['nilai_retur'] - $datas['nilai_retur_saldo'];

				$bb = $bb - $prcs;

				$ss_sisa = $ssisss;


				if ($datas['umur'] < 31) {
					$m1 = $ss_sisa;
					$m2 = 0;
					$m3 = 0;
					$m4 = 0;
					$m5 = 0;
				} elseif ($datas['umur'] > 30 && $datas['umur'] < 61) {
					$m1 = 0;
					$m2 = $ss_sisa;
					$m3 = 0;
					$m4 = 0;
					$m5 = 0;
				} elseif ($datas['umur'] > 60 && $datas['umur'] < 91) {
					$m1 = 0;
					$m2 = 0;
					$m3 = $ss_sisa;
					$m4 = 0;
					$m5 = 0;
				} elseif ($datas['umur'] > 90 && $datas['umur'] < 121) {
					$m1 = 0;
					$m2 = 0;
					$m3 = 0;
					$m4 = $ss_sisa;
					$m5 = 0;
				} else {
					$m1 = 0;
					$m2 = 0;
					$m3 = 0;
					$m4 = 0;
					$m5 = $ss_sisa;
				}

				$ttl = $m1 + $m2 + $m3 + $m4 + $m5;

				if ($ssisss > 1 || ($datas['bayar'] + $datas['nilai_retur'])>1) {
					$html .= '<tr>
												<th >' . $datas['cust_name'] . '</th>
												<th>' . $datas['tanggal_invoice'] . '</th>
												<th>' . $datas['no_penjualan'] . '</th>
												<th>' . $datas['due_date'] . '</th>
												<th>' . $datas['umur'] . '</th>
												<th>' . number_format($datas['saldo'] - $bb_saldo - $retur_saldo, 2, ',', '.') . '</th>
												<th>' . number_format($datas['bulan_berjalan'], 2, ',', '.') . '</th>
												<th>' . ($datas['tgl_bayar'] . ' ' . $datas['tgl_retur']) . '</th>
												<th>' .'<!--br>'
												. $datas['bayar']
												.$datas['nilai_retur']
												.'<br-->'.
												number_format($datas['bayar'] + $datas['nilai_retur'], 2, ',', '.') . '</th>
												<th>' . number_format($ss_sisa, 2, ',', '.') . '</th>
												<th>' . $datas['tgl_jatuh_tempo'] . '</th>
												<th>' . $datas['bayar_giro_concat'] . '</th>
												<th>' . number_format($m1, 2, ',', '.') . '</th>
												<th>' . number_format($m2, 2, ',', '.') . '</th>
												<th>' . number_format($m3, 2, ',', '.') . '</th>
												<th>' . number_format($m4, 2, ',', '.') . '</th>
												<th>' . number_format($m5, 2, ',', '.') . '</th>
												<th>' . number_format($ttl, 2, ',', '.') . '</th>
											</tr>';
				}

				// $ttl = $datas['min_30']-( $datas['min_30']*($datas['diskons']/100)) + ( ($datas['min_30']-( $datas['min_30']*($datas['diskons']/100) ))*($datas['pajak']/100)) + $datas['d30']-( $datas['d30']*($datas['diskons']/100)) + ( ($datas['d30']-( $datas['d30']*($datas['diskons']/100) ))*($datas['pajak']/100) ) + $datas['d60']-( $datas['d60']*($datas['diskons']/100)) + ( ($datas['d60']-( $datas['d60']*($datas['diskons']/100) ))*($datas['pajak']/100) ) + $datas['d90']-( $datas['d90']*($datas['diskons']/100)) + ( ($datas['d90']-( $datas['d90']*($datas['diskons']/100) ))*($datas['pajak']/100) ) + $datas['d120']-( $datas['d120']*($datas['diskons']/100)) + ( ($datas['d120']-( $datas['d120']*($datas['diskons']/100) ))*($datas['pajak']/100) );

				// $prcs = $datas['prices']-( $datas['prices']*($datas['diskons']/100)) + ( ($datas['prices']-( $datas['prices']*($datas['diskons']/100) ))*($datas['pajak']/100) );

				$prin = $datas['cust_name'];


				$curr_invoice = $datas['no_invoice'];

				$penjualan = $penjualan + ($datas['saldo'] - ($datas['saldo'] * ($datas['diskon'] / 100))) - $bb_saldo - $retur_saldo;
				$pengeluaran = $pengeluaran + ($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskon'] / 100)));
				$saldo_akhir = $saldo_akhir + $bayar_r + $datas['nilai_retur'];
				$sisa_a = $sisa_a + $ss_sisa;
				$nilai_akhir = $nilai_akhir + $datas['bayar'] + $datas['nilai_retur'];
				$invoice_saldo = $invoice_saldo + $datas['invoice_saldo'];
				// // $d0 = $d0 + $datas['min_30']-( $datas['min_30']*($datas['diskons']/100)) + ( ($datas['min_30']-( $datas['min_30']*($datas['diskons']/100) ))*($datas['pajak']/100));
				// // $d30 = $d30 + $datas['d30']-( $datas['d30']*($datas['diskons']/100)) + ( ($datas['d30']-( $datas['d30']*($datas['diskons']/100) ))*($datas['pajak']/100) );
				// // $d60 = $d60 + $datas['d60']-( $datas['d60']*($datas['diskons']/100)) + ( ($datas['d60']-( $datas['d60']*($datas['diskons']/100) ))*($datas['pajak']/100) );
				// // $d90 = $d90 + $datas['d90']-( $datas['d90']*($datas['diskons']/100)) + ( ($datas['d90']-( $datas['d90']*($datas['diskons']/100) ))*($datas['pajak']/100) );
				// // $d120 = $d120 + $datas['d120']-( $datas['d120']*($datas['diskons']/100)) + ( ($datas['d120']-( $datas['d120']*($datas['diskons']/100) ))*($datas['pajak']/100) );

				$d0 = $d0 + $m1;
				$d30 = $d30 + $m2;
				$d60 = $d60 + $m3;
				$d90 = $d90 + $m4;
				$d120 = $d120 + $m5;
				$total_t = $total_t + $ttl;

				$penjualan_a = $penjualan_a + ($datas['saldo'] - ($datas['saldo'] * ($datas['diskon'] / 100))) - $bb_saldo - $retur_saldo;
				$pengeluaran_a = $pengeluaran_a + ($datas['bulan_berjalan'] - ($datas['bulan_berjalan'] * ($datas['diskon'] / 100)));
				$saldo_akhir_a = $saldo_akhir_a +  $bayar_r + $datas['nilai_retur'];
				$nilai_akhir_a = $nilai_akhir_a +  $datas['bayar'] + $datas['nilai_retur'];
				$invoice_saldo_a = $invoice_saldo_a +  $datas['invoice_saldo'];
				$sisa_aa = $sisa_aa + $ss_sisa;

				// // $d0_a = $d0_a + $datas['min_30']-( $datas['min_30']*($datas['diskons']/100)) + ( ($datas['min_30']-( $datas['min_30']*($datas['diskons']/100) ))*($datas['pajak']/100));
				// // $d30_a = $d30_a + $datas['d30']-( $datas['d30']*($datas['diskons']/100)) + ( ($datas['d30']-( $datas['d30']*($datas['diskons']/100) ))*($datas['pajak']/100) );
				// // $d60_a = $d60_a + $datas['d60']-( $datas['d60']*($datas['diskons']/100)) + ( ($datas['d60']-( $datas['d60']*($datas['diskons']/100) ))*($datas['pajak']/100) );
				// // $d90_a = $d90_a + $datas['d90']-( $datas['d90']*($datas['diskons']/100)) + ( ($datas['d90']-( $datas['d90']*($datas['diskons']/100) ))*($datas['pajak']/100) );
				// // $d120_a = $d120_a + $datas['d120']-( $datas['d120']*($datas['diskons']/100)) + ( ($datas['d120']-( $datas['d120']*($datas['diskons']/100) ))*($datas['pajak']/100) );

				$d0_a = $d0_a + $m1;
				$d30_a = $d30_a + $m2;
				$d60_a = $d60_a + $m3;
				$d90_a = $d90_a + $m4;
				$d120_a = $d120_a + $m5;
				$total_t_a = $total_t_a + $ttl;

				// if ($ssisss != 0) {
				// }
			}
			if ($sisa_a > 1 || $saldo_akhir>1) {

				$html .= '<tr style="background:yellow">
					<th>' . $prin . '</th>
					<th></th>
					<th ></th>
					<th ></th>	
					<th></th>
					<th>' . number_format($penjualan, 2, ',', '.') . '</th>
					<th>' . number_format($pengeluaran, 2, ',', '.') . '</th>
					<th></th>
					<th>' . number_format($saldo_akhir, 2, ',', '.') . '</th>
					<th>' . number_format($sisa_a, 2, ',', '.') . '</th>
					<th>' . number_format(0, 2, ',', '.') . '</th>
					<th>' . number_format($sisa_b, 2, ',', '.') . '</th>
					<th>' . number_format($d0, 2, ',', '.') . '</th>
					<th>' . number_format($d30, 2, ',', '.') . '</th>
					<th>' . number_format($d60, 2, ',', '.') . '</th>
					<th>' . number_format($d90, 2, ',', '.') . '</th>
					<th>' . number_format($d120, 2, ',', '.') . '</th>
					<th>' . number_format($total_t, 2, ',', '.') . '</th>
					</tr>';
			}
		}

		$html .= '<tr style="background:green">
			<th colspan="3">Total </th>
			<th></th>
			<th ></th>
			<th>' . number_format($penjualan_a, 2, ',', '.') . '</th>
			<th>' . number_format($pengeluaran_a, 2, ',', '.') . '</th>
			<th></th>
			<th>' . number_format($saldo_akhir_a, 2, ',', '.') . '</th>
			<th>' . number_format($sisa_aa, 2, ',', '.') . '</th>
			<th></th>
			<th></th>
			<th>' . number_format($d0_a, 2, ',', '.') . '</th>
			<th>' . number_format($d30_a, 2, ',', '.') . '</th>
			<th>' . number_format($d60_a, 2, ',', '.') . '</th>
			<th>' . number_format($d90_a, 2, ',', '.') . '</th>
			<th>' . number_format($d120_a, 2, ',', '.') . '</th>
			<th>' . number_format($total_t_a, 2, ',', '.') . '</th>
		</tr>';
		$results['html'] = $html;
		//print_r($result);die;
		$results['data'] = $results['html'];
		$results['piutang'] = $total_t_a;
		return $results;
	}
	public function list_penjualan($params = array()) {

		$query = 	'
				SELECT COUNT(*) AS jumlah 
				 FROM `t_retur` a
					JOIN `t_penjualan` b ON a.id_penjualan = b.`id_penjualan`
					LEFT JOIN `u_user` c ON a.`pic` = c.id
					LEFT JOIN `u_user` d ON a.`approval` = d.id
					LEFT JOIN `t_customer` e ON b.id_customer = e.`id_t_cust`
					where 1 = 1  AND ( 
						c.nama LIKE "%' . $params['searchtxt'] . '%" OR
						e.cust_name LIKE "%' . $params['searchtxt'] . '%"  OR
						b.no_penjualan LIKE "%' . $params['searchtxt'] . '%"
					)
			';

		$query2 = 	'
				SELECT z.*, rank() over ( ORDER BY date DESC) AS Rangking from ( 
					SELECT a.id_retur,a.date,a.status AS status_retur,a.approval_date,b.*, c.nama AS pic, d.nama AS approval, e.`cust_name` FROM `t_retur` a
					JOIN `t_penjualan` b ON a.id_penjualan = b.`id_penjualan`
					LEFT JOIN `u_user` c ON a.`pic` = c.id
					LEFT JOIN `u_user` d ON a.`approval` = d.id
					LEFT JOIN `t_customer` e ON b.id_customer = e.`id_t_cust`
					where 1 = 1  AND ( 
					c.nama LIKE "%' . $params['searchtxt'] . '%" OR
					e.cust_name LIKE "%' . $params['searchtxt'] . '%"  OR
					b.no_penjualan LIKE "%' . $params['searchtxt'] . '%"
					)  order by date DESC) z
				ORDER BY date DESC
				
				LIMIT ' . $params['limit'] . ' 
				OFFSET ' . $params['offset'] . ' 
			';

		//echo $query2;die;

		$out		= array();
		$querys		= $this->db->query($query);
		$result = $querys->row();

		$total_filtered = $result->jumlah;
		$total 			= $result->jumlah;

		if (($params['offset'] + 10) > $total_filtered) {
			$limit_data = $total_filtered - $params['offset'];
		} else {
			$limit_data = $params['limit'];
		}



		//echo $query;die;
		//echo $query;die;
		$query2s		= $this->db->query($query2);
		$result2 = $query2s->result_array();
		$return = array(
			'data' => $result2,
			'total_filtered' => $total_filtered,
			'total' => $total,
		);
		return $return;
	}


	public function get_customer($params = array()) {

		//$query = $this->db->get_where('t_customer', array('region' => $params['region']));

		$this->db->select('*')
			->from('t_customer')
			# ->where(array('region' => $params['region']))
			->order_by('cust_name', 'asc');
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_report_persediaan($params = array()) {

		if ($params['region'] == 0) {
			$where_reg = " ";
		} else {
			$where_reg = " AND g.region = " . $params['region'];
		}

		// if($params['customer'] == 0){

		// $where_dis = "";

		// } else{

		$where_dis = " and jj.id_customer = " . $params['customer'];

		// }

		$sql = "
		
		select *,group_concat(items,' 
		') as items,sum(price_fulls) as price_full, sum(invoice_saldos) as invoice_saldo,sum(dpps) dpp, sum(bulan_berjalans) as bulan_berjalan,
		sum(saldos) as saldo 
		from (
		SELECT jj.no_penjualan,c.`no_invoice`,a.`tanggal_invoice`,a.`due_date`,`stock_code`,`stock_name`,`base_qty`,`uom_symbol`,CONCAT(stock_name,' ',base_qty,'',uom_symbol) as items,
		b.`unit_price`,b.`qty`,b.`qty_box`,b.`qty_satuan`,b.`box_ammount`,b.`diskon`,
		b.price-(b.price*(b.diskon/100)) AS price_fulls,
		b.price-(b.price*(b.diskon/100)) AS invoice_saldos,
		b.price-(b.price*(b.diskon/100)) - ((b.price-(b.price*(b.diskon/100)))/(11)) AS dpps,
		((b.price-(b.price*(b.diskon/100)))/(11)) AS ppn,IF(`term_of_payment` = 'cash',0,`term_of_payment`) topys,IF(bayar_saldo IS NULL,0,bayar_saldo) AS bayar_saldo,
		DATEDIFF(NOW(),a.`tanggal_invoice`) umur, `cust_name`,`id_t_region`,h.`region`,
		#IF(a.tanggal_invoice BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "',b.price-(b.price*(b.diskon/100)) - ((b.price-(b.price*(b.diskon/100)))/(11)),0 ) bulan_berjalans,
		#IF(a.tanggal_invoice < '" . $params['tgl_awal'] . "' ,b.price-(b.price*(b.diskon/100)) - ((b.price-(b.price*(b.diskon/100)))/(11)),0 ) saldos,
		IF(a.tanggal_invoice BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "',b.price-(b.price*(b.diskon/100)) ,0 ) bulan_berjalans,
		IF(a.tanggal_invoice < '" . $params['tgl_awal'] . "' ,b.price-(b.price*(b.diskon/100)) ,0 ) saldos,
		jk.*,jl.*,rt.nilai_retur_saldo, rtr.nilai_retur ,tgl_retur
		FROM `t_invoice_penjualan` a 
		JOIN `d_invoice_penjualan` b ON a.`id_invoice` = b.`id_inv_penjualan`
		JOIN `t_invoice_penjualan` c ON a.`id_penjualan` = c.`id_penjualan` 
		JOIN d_penjualan d ON b.`id_d_penjualan` = d.`id_dp`
		JOIN `t_penjualan` jj ON a.`id_penjualan` = jj.`id_penjualan`
		JOIN `t_customer` g ON jj.id_customer = g.`id_t_cust`
		JOIN t_region h ON h.`id_t_region` = g.`region`
		JOIN `m_material` e ON d.id_material = e.`id_mat`
		JOIN `m_uom` f ON f.`id_uom` = e.`unit_terkecil`
		LEFT JOIN
		(
				SELECT a.date as tgl_bayar,b.id_invoice inv_bayar,
				a.account_type,
				sum(b.ammount) bayar, (a.payment_status) as payment_status 
				FROM `t_payment_hp` a 
				JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp` 
				WHERE a.`payment_type` = 1 and a.payment_status=1 
				AND a.`date` between '" . $params['tgl_awal'] . "' and '" . $params['tgl_akhir'] . "'
				GROUP BY b.id_invoice
		) jk ON a.id_invoice = jk.inv_bayar
		LEFT JOIN
		(
				SELECT b.id_invoice inv_bayar_1,
				sum(b.ammount) bayar_giro,
				GROUP_CONCAT(a.tgl_jatuh_tempo,'<br>') as tgl_jatuh_tempo,
				GROUP_CONCAT(FORMAT(b.ammount,2,'de_DE'),'<br>') as bayar_giro_concat
				FROM `t_payment_hp` a
				JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp` 
				WHERE a.`payment_type` = 1 
				and a.account_type=1 and a.payment_status=0 
				AND a.`date` <= '" . $params['tgl_akhir'] . "'
				GROUP BY b.id_invoice
		) jl ON a.id_invoice = jl.inv_bayar_1
		LEFT JOIN
			(
				SELECT a.date as tgl_bayar_saldo,b.id_invoice inv_saldo,sum(b.ammount) bayar_saldo 
				FROM `t_payment_hp` a
				JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp` 
				WHERE a.`payment_type` = 1 and a.payment_status=1
				AND a.`date` < '" . $params['tgl_awal'] . "'  
				GROUP BY b.id_invoice
			) k ON k.inv_saldo = a.id_invoice
		LEFT JOIN
		(
			SELECT d.`id_penjualan`,SUM(if(b.`qty` is null,0,b.`qty`) *if(b.`harga_satuan` is null,0,b.`harga_satuan`)) AS nilai_retur_saldo 
			FROM t_retur a 
			JOIN t_detail_retur b ON a.id_retur = b.id_retur
			JOIN `d_penjualan` c ON c.`id_dp` = b.id_detail_mat
			JOIN `d_invoice_penjualan` e ON c.`id_dp` = e.`id_d_inv`
			JOIN `t_penjualan` d ON c.`id_penjualan` = d.id_penjualan
			WHERE a.`date` < '" . $params['tgl_awal'] . "'  
			GROUP BY d.`id_penjualan`
		) rt ON rt.`id_penjualan` = a.id_invoice
		LEFT JOIN
		(
			SELECT d.`id_penjualan`,SUM(if(b.`qty` is null,0,b.`qty`) *if(b.`harga_satuan` is null,0,b.`harga_satuan`)) AS nilai_retur , CONCAT(a.date,' ') as tgl_retur
			FROM t_retur a 
			JOIN t_detail_retur b ON a.id_retur = b.id_retur
			JOIN `d_penjualan` c ON c.`id_dp` = b.id_detail_mat
			JOIN `t_penjualan` d ON c.`id_penjualan` = d.id_penjualan
			WHERE a.`date` between '" . $params['tgl_awal'] . "' and '" . $params['tgl_akhir'] . "'
			GROUP BY d.`id_penjualan`
		) rtr ON rtr.`id_penjualan` = a.id_invoice
		where 1=1 " . $where_dis . " " . $where_reg . "
		) k where tanggal_invoice <= '" . $params['tgl_akhir'] . "' 
		group by no_invoice
		order by `tanggal_invoice`,stock_name

		";

		// if ($params['customer'] == 30932) {
			// echo "<pre>";print_r($sql);die;
		// }


		$query 	= $this->db->query($sql);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_distribution($params = array()) {

		// $sql = "
		// SELECT dist_id FROM m_material a
		// JOIN `t_eksternal` b ON a.dist_id = b.id
		// GROUP BY dist_id
		// ORDER BY stock_code
		// ";

		$sql = "
		SELECT id_customer FROM t_penjualan a
		JOIN `t_customer` b ON a.id_customer = b.id_t_cust
		GROUP BY id_customer
		ORDER BY id_customer
		";



		$query 	= $this->db->query($sql);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_region($params = array()) {

		// $sql = "
		// SELECT dist_id FROM m_material a
		// JOIN `t_eksternal` b ON a.dist_id = b.id
		// GROUP BY dist_id
		// ORDER BY stock_code
		// ";

		$sql = "
		SELECT * FROM t_region a
		ORDER BY	id_t_region
	";



		$query 	= $this->db->query($sql);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}


	public function get_report_pembelian($params = array()) {


		if ($params['principal'] == 0) {


			$query = "
		SELECT COUNT(*) AS jumlah from (
		SELECT a.`date_po` AS Tanggal,
d.`stock_code` AS Kode ,
d.`stock_name` AS Nama_barang,
CONCAT(d.base_qty,' ',f.uom_name) AS kemasan,
e.`name_eksternal`,
c.qty,
c.`unit_price` -(c.`unit_price`*(c.diskon/100)) AS harga,
SUM(c.`price`) AS jumlah,
SUM(c.`price`*0.01) AS PPN,
SUM(c.`price`)+SUM(c.`price`*0.01) AS Rp_PPN,
a.no_po AS No_trans,
b.`no_invoice`,
DATE_ADD(a.date_po,INTERVAL a.`term_of_payment` DAY) AS jatuh_tempo,
a.`term_of_payment` AS lama_hari,
b.`faktur_pajak`,
b.`tanggal_faktur`

FROM t_purchase_order a
JOIN t_po_mat aa ON a.id_po=aa.id_po
JOIN `t_bpb` cc ON a.`id_po` = cc.`id_po`
JOIN t_invoice_pembelian b ON cc.id_bpb=b.id_bpb
JOIN d_invoice_pembelian c ON b.id_invoice=c.id_inv_pembelian 
JOIN m_material d ON aa.id_material=d.id_mat
JOIN t_eksternal e ON d.dist_id=e.id
JOIN m_uom f ON d.unit_terkecil=f.id_uom
WHERE date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "'
GROUP BY aa.id_t_ps) z
				
		";


			$query2 = "
		SELECT z.*, rank() over ( ORDER BY Tanggal,no_invoice,Kode,Nama_barang DESC) AS Rangking from ( 
		SELECT a.`date_po` AS Tanggal,
d.`stock_code` AS Kode ,
d.`stock_name` AS Nama_barang,
CONCAT(d.base_qty,' ',f.uom_name) AS kemasan,
e.`name_eksternal`,
c.qty,
c.`unit_price` -(c.`unit_price`*(c.diskon/100)) AS harga,
SUM(c.`price`) AS jumlah,
SUM(c.`price`*0.01) AS PPN,
SUM(c.`price`)+SUM(c.`price`*0.01) AS Rp_PPN,
a.no_po AS No_trans,
b.`no_invoice`,
DATE_ADD(a.date_po,INTERVAL a.`term_of_payment` DAY) AS jatuh_tempo,
a.`term_of_payment` AS lama_hari,
b.`faktur_pajak`,
b.`tanggal_faktur`


FROM t_purchase_order a
JOIN t_po_mat aa ON a.id_po=aa.id_po
JOIN `t_bpb` cc ON a.`id_po` = cc.`id_po`
JOIN t_invoice_pembelian b ON cc.id_bpb=b.id_bpb
JOIN d_invoice_pembelian c ON b.id_invoice=c.id_inv_pembelian 
JOIN m_material d ON aa.id_material=d.id_mat
JOIN t_eksternal e ON d.dist_id=e.id
JOIN m_uom f ON d.unit_terkecil=f.id_uom
WHERE date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "'
GROUP BY aa.id_t_ps) z
				ORDER BY Tanggal,no_invoice,Kode,Nama_barang DESC
				LIMIT " . $params['limit'] . " 
				OFFSET " . $params['offset'] . " 
		";
		} else {

			$query = "
		SELECT COUNT(*) AS jumlah from (
		SELECT a.`date_po` AS Tanggal,
d.`stock_code` AS Kode ,
d.`stock_name` AS Nama_barang,
CONCAT(d.base_qty,' ',f.uom_name) AS kemasan,
e.`name_eksternal`,
c.qty,
c.`unit_price` -(c.`unit_price`*(c.diskon/100)) AS harga,
SUM(c.`price`) AS jumlah,
SUM(c.`price`*0.01) AS PPN,
SUM(c.`price`)+SUM(c.`price`*0.01) AS Rp_PPN,
a.no_po AS No_trans,
b.`no_invoice`,
DATE_ADD(a.date_po,INTERVAL a.`term_of_payment` DAY) AS jatuh_tempo,
a.`term_of_payment` AS lama_hari,
b.`faktur_pajak`,
b.`tanggal_faktur`

FROM t_purchase_order a
JOIN t_po_mat aa ON a.id_po=aa.id_po
JOIN `t_bpb` cc ON a.`id_po` = cc.`id_po`
JOIN t_invoice_pembelian b ON cc.id_bpb=b.id_bpb
JOIN d_invoice_pembelian c ON b.id_invoice=c.id_inv_pembelian 
JOIN m_material d ON aa.id_material=d.id_mat
JOIN t_eksternal e ON d.dist_id=e.id
JOIN m_uom f ON d.unit_terkecil=f.id_uom
WHERE date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "'
AND a.id_distributor = " . $params['principal'] . "
GROUP BY aa.id_t_ps) z
				
		";


			$query2 = "
		SELECT z.*, rank() over ( ORDER BY Tanggal,no_invoice,Kode,Nama_barang DESC) AS Rangking from ( 
		SELECT a.`date_po` AS Tanggal,
d.`stock_code` AS Kode ,
d.`stock_name` AS Nama_barang,
CONCAT(d.base_qty,' ',f.uom_name) AS kemasan,
e.`name_eksternal`,
c.qty,
c.`unit_price` -(c.`unit_price`*(c.diskon/100)) AS harga,
SUM(c.`price`) AS jumlah,
SUM(c.`price`*0.01) AS PPN,
SUM(c.`price`)+SUM(c.`price`*0.01) AS Rp_PPN,
a.no_po AS No_trans,
b.`no_invoice`,
DATE_ADD(a.date_po,INTERVAL a.`term_of_payment` DAY) AS jatuh_tempo,
a.`term_of_payment` AS lama_hari,
b.`faktur_pajak`,
b.`tanggal_faktur`


FROM t_purchase_order a
JOIN t_po_mat aa ON a.id_po=aa.id_po
JOIN `t_bpb` cc ON a.`id_po` = cc.`id_po`
JOIN t_invoice_pembelian b ON cc.id_bpb=b.id_bpb
JOIN d_invoice_pembelian c ON b.id_invoice=c.id_inv_pembelian 
JOIN m_material d ON aa.id_material=d.id_mat
JOIN t_eksternal e ON d.dist_id=e.id
JOIN m_uom f ON d.unit_terkecil=f.id_uom
WHERE date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "'
AND a.id_distributor = " . $params['principal'] . "
GROUP BY aa.id_t_ps) z
				ORDER BY Tanggal,no_invoice,Kode,Nama_barang DESC
				LIMIT " . $params['limit'] . " 
				OFFSET " . $params['offset'] . " 
		";
		}

		$out		= array();
		$querys		= $this->db->query($query);
		$result = $querys->row();

		$total_filtered = $result->jumlah;
		$total 			= $result->jumlah;

		if (($params['offset'] + 10) > $total_filtered) {
			$limit_data = $total_filtered - $params['offset'];
		} else {
			$limit_data = $params['limit'];
		}



		//echo $query;die;
		//echo $query;die;
		$query2s		= $this->db->query($query2);
		$result2 = $query2s->result_array();
		$return = array(
			'data' => $result2,
			'total_filtered' => $total_filtered,
			'total' => $total,
		);
		return $return;
	}


	public function get_report_pembelian_export($params = array()) {

		if ($params['principal'] == '0') {

			$sql = "
		
SELECT z.*, rank() over ( ORDER BY Tanggal,no_invoice,Kode,Nama_barang DESC) AS Rangking from ( 
		SELECT a.`date_po` AS Tanggal,
d.`stock_code` AS Kode ,
d.`stock_name` AS Nama_barang,
CONCAT(d.base_qty,' ',f.uom_name) AS kemasan,
e.`name_eksternal`,
c.qty,
c.`unit_price` -(c.`unit_price`*(c.diskon/100)) AS harga,
SUM(c.`price`) AS jumlah,
SUM(c.`price`*0.01) AS PPN,
SUM(c.`price`)+SUM(c.`price`*0.01) AS Rp_PPN,
a.no_po AS No_trans,
b.`no_invoice`,
DATE_ADD(a.date_po,INTERVAL a.`term_of_payment` DAY) AS jatuh_tempo,
a.`term_of_payment` AS lama_hari,
b.`faktur_pajak`,
b.`tanggal_faktur`


FROM t_purchase_order a
JOIN t_po_mat aa ON a.id_po=aa.id_po
JOIN `t_bpb` cc ON a.`id_po` = cc.`id_po`
JOIN t_invoice_pembelian b ON cc.id_bpb=b.id_bpb
JOIN d_invoice_pembelian c ON b.id_invoice=c.id_inv_pembelian 
JOIN m_material d ON aa.id_material=d.id_mat
JOIN t_eksternal e ON d.dist_id=e.id
JOIN m_uom f ON d.unit_terkecil=f.id_uom
WHERE date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "'
GROUP BY aa.id_t_ps) z
				ORDER BY Tanggal,no_invoice,Kode,Nama_barang DESC
		";
		} else {

			$sql = "
		
SELECT z.*, rank() over ( ORDER BY Tanggal,no_invoice,Kode,Nama_barang DESC) AS Rangking from ( 
		SELECT a.`date_po` AS Tanggal,
d.`stock_code` AS Kode ,
d.`stock_name` AS Nama_barang,
CONCAT(d.base_qty,' ',f.uom_name) AS kemasan,
e.`name_eksternal`,
c.qty,
c.`unit_price` -(c.`unit_price`*(c.diskon/100)) AS harga,
SUM(c.`price`) AS jumlah,
SUM(c.`price`*0.01) AS PPN,
SUM(c.`price`)+SUM(c.`price`*0.01) AS Rp_PPN,
a.no_po AS No_trans,
b.`no_invoice`,
DATE_ADD(a.date_po,INTERVAL a.`term_of_payment` DAY) AS jatuh_tempo,
a.`term_of_payment` AS lama_hari,
b.`faktur_pajak`,
b.`tanggal_faktur`


FROM t_purchase_order a
JOIN t_po_mat aa ON a.id_po=aa.id_po
JOIN `t_bpb` cc ON a.`id_po` = cc.`id_po`
JOIN t_invoice_pembelian b ON cc.id_bpb=b.id_bpb
JOIN d_invoice_pembelian c ON b.id_invoice=c.id_inv_pembelian 
JOIN m_material d ON aa.id_material=d.id_mat
JOIN t_eksternal e ON d.dist_id=e.id
JOIN m_uom f ON d.unit_terkecil=f.id_uom
WHERE date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "'
AND a.id_distributor = " . $params['principal'] . "
GROUP BY aa.id_t_ps) z
				ORDER BY Tanggal,no_invoice,Kode,Nama_barang DESC
		";
		}


		$query 	= $this->db->query($sql);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}


	public function get_report_pembelian_prin($params = array()) {

		$sql = "
		SELECT a.`date_po` AS Tanggal,
d.`stock_code` AS Kode ,
d.`stock_name` AS Nama_barang,
CONCAT(d.base_qty,' ',f.uom_name) AS kemasan,
e.`name_eksternal`,
c.qty,
c.`unit_price` -(c.`unit_price`*(c.diskon/100)) AS harga,
SUM(c.`price`) AS jumlah,
SUM(c.`price`*0.01) AS PPN,
SUM(c.`price`)+SUM(c.`price`*0.01) AS Rp_PPN,
a.no_po AS No_trans,
b.`no_invoice`,
DATE_ADD(a.date_po,INTERVAL a.`term_of_payment` DAY) AS jatuh_tempo,
a.`term_of_payment` AS lama_hari,
b.`faktur_pajak`

FROM t_purchase_order a
JOIN t_po_mat aa ON a.id_po=aa.id_po
JOIN t_invoice_pembelian b ON a.id_po=b.id_po
JOIN d_invoice_pembelian c ON b.id_invoice=c.id_inv_pembelian and aa.id_t_ps = c.id_po_mat
JOIN m_material d ON aa.id_material=d.id_mat
JOIN t_eksternal e ON d.dist_id=e.id
JOIN m_uom f ON d.unit_terkecil=f.id_uom
WHERE date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "'
AND e.id = " . $params['principal'] . "
GROUP BY aa.id_t_ps
		";

		//echo $sql;die;

		$query 	= $this->db->query($sql);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_principal($params = array()) {

		//$query = $this->db->get_where('t_customer', array('region' => $params['region']));

		$this->db->select('*')
			->from('t_customer')
			->where(array('sts_show' => 1))
			->order_by('cust_name', 'asc');
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_price_mat($params = array()) {

		// $query = $this->db->get_where('t_po_mat', array('id_po' => $params['kode']));
		// $query = $this->db->order_by('id_t_ps', 'desc');

		$this->db->select('*')
			->from('t_po_mat')
			->where(array('id_material' => $params['kode']))
			->order_by('id_t_ps', 'desc');
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_penjualan_byid($params = array()) {

		$this->db->select('t_penjualan.*, t_customer.*')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_penjualan')
			->join('t_customer', 't_penjualan.id_customer = t_customer.id_t_cust')
			->where(array('id_penjualan' => $params['id_penjualan']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}


	public function get_item_byprin($params = array()) {
		$this->db->select('m_material.*, m_uom.uom_symbol')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('m_material')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('dist_id' => $params['id_prin']));
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_retur_id($params = array()) {
		$this->db->select('t_retur.date,t_retur.id_penjualan,t_retur.keterangan, t_detail_retur.*')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_retur')
			->join('t_detail_retur', 't_retur.id_retur = t_detail_retur.id_retur')
			->where(array('t_retur.id_retur' => $params['id']));
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_item_byprin_all() {
		$this->db->select('m_material.*, m_uom.uom_symbol')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('m_material')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('status' => 0))
			->order_by('stock_name', 'asc');
		//->where(array('dist_id' => $params['id_prin']));
		// ->where('m_material.dist_id != ',$params['id_prin'],FALSE);
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_penjualan_all() {
		$this->db->select('t_penjualan.*, t_customer.cust_name')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_penjualan')
			->join('t_customer', 't_penjualan.id_customer = t_customer.id_t_cust')
			->where(array('status' => 0))
			->order_by('date_penjualan', 'desc');
		//->where(array('dist_id' => $params['id_prin']));
		// ->where('m_material.dist_id != ',$params['id_prin'],FALSE);
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_kode($params = array()) {

		$query =  $this->db->select_max('seq_n', 'max');
		$query = $this->db->get('t_penjualan');

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_penjualan($params = array()) {

		// $query = $this->db->get_where('t_purchase_order', array('id_po' => $params['id']));
		$this->db->select('t_penjualan.*, t_customer.*')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_penjualan')
			->join('t_customer', 't_penjualan.id_customer = t_customer.id_t_cust')
			->where(array('id_penjualan' => $params['id']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_penjualan_detail_update($params = array()) {

		//$query = $this->db->get_where('d_penjualan', array('id_penjualan' => $params['id_penjualan']));
		$this->db->select('d_penjualan.*, m_material.stock_name, m_material.base_qty, m_material.stock_name, m_uom.uom_symbol,t_detail_retur.qty as qty_retur,t_detail_retur.qty_satuan as qty_retur_sat,t_detail_retur.qty_box as qty_box_retur,t_detail_retur.amount ')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('d_penjualan')
			->join('m_material', 'd_penjualan.id_material = m_material.id_mat')
			->join('t_detail_retur', 'd_penjualan.id_dp = t_detail_retur.id_detail_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('t_detail_retur.id_retur' => $params['id_penjualan']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_penjualan_detail($params = array()) {

		//$query = $this->db->get_where('d_penjualan', array('id_penjualan' => $params['id_penjualan']));
		$this->db->select('d_penjualan.*, m_material.stock_name, m_material.base_qty, m_material.stock_name, m_uom.uom_symbol')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('d_penjualan')
			->join('m_material', 'd_penjualan.id_material = m_material.id_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('id_penjualan' => $params['id_penjualan']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_po_detail_full($params = array()) {

		//$query = $this->db->get_where('t_po_mat', array('id_po' => $params['id']));
		$this->db->select('t_po_mat.*,t_po_mat.qty as qty_order, m_material.*, m_uom.*')
			#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
			->from('t_po_mat')
			->join('m_material', 't_po_mat.id_material = m_material.id_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(array('id_po' => $params['id']));
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function check_principal($params = array()) {

		$query = $this->db->get_where('t_eksternal', array('kode_eksternal' => $params['kode']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_returm($data) {

		$this->db->insert('t_retur', $data);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_mutasi($data) {

		$data_insert_mutasi = array(
			'id_stock_awal'  => $data['id_material'],
			'id_stock_akhir' => $data['id_material'],
			'date_mutasi'    => date('Y-m-d H:i:s'),
			'amount_mutasi'  => $data['qty'],
			'type_mutasi'    => 0,
			'user_id'    => $this->session->userdata['logged_in']['user_id'],
		);

		//	print_r($datas);die;

		$this->db->insert('t_mutasi', $data_insert_mutasi);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_mutasi_out($data) {

		$data_insert_mutasi = array(
			'id_stock_awal'  => $data['id_material'],
			'id_stock_akhir' => $data['id_material'],
			'date_mutasi'    => date('Y-m-d H:i:s'),
			'amount_mutasi'  => $data['qty'],
			'type_mutasi'    => 1,
			'user_id'    => $this->session->userdata['logged_in']['user_id'],
		);

		//	print_r($datas);die;

		$this->db->insert('t_mutasi', $data_insert_mutasi);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_detail_retur($data) {

		//	print_r($datas);die;

		$this->db->insert('t_detail_retur', $data);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_qty($data) {
		$sql 	= 'update m_material set qty = qty - ' . $data['qty'] . ' 
		where id_mat = ' . $data['id_material'] . ' ';

		// $datas = array(
		// 'qty' => $data['qty_stock']
		// );

		// //	print_r($datas);die;
		// $this->db->where('id_mat',$data['id_material'], false);
		// $this->db->update('m_material',$datas, false);


		$query 	= $this->db->query($sql);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function update_qty_m($data) {
		$sql 	= 'update m_material set qty = qty + ' . $data['qty'] . ' 
		where id_mat = ' . $data['id_mat'] . ' ';

		//echo $sql;die;
		// $datas = array(
		// 'qty' => $data['qty_stock']
		// );

		//	print_r($datas);die;
		// $this->db->where('id_mat',$data['id_material'], false);
		// $this->db->update('m_material',$datas, false);


		$query 	= $this->db->query($sql);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_credit($data) {
		$sql 	= "
		update t_customer set sisa_credit = sisa_credit - " . $data['total_amount'] . " where id_t_cust = " . $data['id_customer'] . "
		";

		// $total = $data['sisa'] - $data['total_amount'];
		// $datas = array(
		// 'sisa_credit' => $total
		// );

		//	print_r($datas);die;
		// $this->db->where('id_t_cust',$data['id_customer'], false);
		// $this->db->update('t_customer',$datas, false);


		$query 	= $this->db->query($sql);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_credit2($data, $selling_data) {
		$sql 	= 'update t_customer set sisa_credit = sisa_credit + ' . floatval($selling_data['total_amount']) . '
		where id_t_cust = ' . $data['id_customer'];
		$query 	= $this->db->query($sql);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_retur_apr($data) {

		$datas = array(

			'status' => $data['sts']

		);

		$this->db->where('id_retur', $data['id_po']);
		$this->db->update('t_retur', $datas);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function history_retur_apr($data) {

		$sql 	= "
		INSERT INTO `history_d_penjualan`
		SELECT NULL AS jo,a.* FROM `d_penjualan` a
		JOIN  `t_detail_retur` b ON a.id_dp = b.`id_detail_mat` 
		WHERE b.id_retur  = " . $data['id_po'] . "
		
		";

		// $total = $data['sisa'] - $data['total_amount'];
		// $datas = array(
		// 'sisa_credit' => $total
		// );

		//	print_r($datas);die;
		// $this->db->where('id_t_cust',$data['id_customer'], false);
		// $this->db->update('t_customer',$datas, false);


		$query 	= $this->db->query($sql);


		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_retur($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(

			'date' => $data['date'],
			'id_penjualan' => $data['id_penjualan'],
			'keterangan' => $data['keterangan']

		);

		//	print_r($datas);die;
		$this->db->where('id_retur', $data['id_retur']);
		$this->db->update('t_retur', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_detail_penjualan($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'qty' => $data['qty_retur'],
			'qty_box' => $data['qty_box_retur'],
			'qty_satuan' => $data['qty_retur_sat'],
			'price' => $data['amount'],
			'mutasi_id' => $data['mutasi_id']
		);

		//	print_r($datas);die;
		$this->db->where('id_dp', $data['id_dp']);
		$this->db->update('d_penjualan', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	function delete_retur($id) {

		$this->db->where('id_retur', $id);
		$this->db->delete('t_retur');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $result;
	}

	function delete_detail_retur($id) {

		$this->db->where('id_retur', $id);
		$this->db->delete('t_detail_retur');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $result;
	}

	function delete_mutasi($data_q) {

		$this->db->where('id_mutasi', $data_q['mutasi_id']);
		$this->db->delete('t_mutasi');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $result;
	}
}
