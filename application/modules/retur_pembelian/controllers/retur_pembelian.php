<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Retur_pembelian extends MX_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->model('retur_pembelian/retur_pembelian_model', 'returPembelianModel');
    $this->load->library('log_activity');
    $this->load->library('priv');

    $this->form_validation->set_message('required', '%s Tidak Boleh Kosong');
    $this->form_validation->set_message('min_length', '%s Minimal 4 Karakter');
    $this->form_validation->set_message('max_length', '%s Maksimal 10 Karakter');
  }

  public function index() {
    $this->template->load('maintemplate', 'retur_pembelian/views/index', [
      'priv' => $this->priv->get_priv()
    ]);
  }

  public function lists() {
    $sess_user_id = !empty($_GET['sess_user_id']) ? $_GET['sess_user_id'] : NULL;
    $sess_token = !empty($_GET['sess_token']) ? $_GET['sess_token'] : NULL;

    $draw = ($this->input->get_post('draw') != FALSE) ? $this->input->get_post('draw') : 1;
    $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
    $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;

    $order = $this->input->get_post('order');
    $order_dir = !empty($order[0]['dir']) ? $order[0]['dir'] : 'desc';
    $order_column = !empty($order[0]['column']) ? $order[0]['column'] : 0;
    $order_fields = array('kode_eksternal'); // , 'COST'

    $params['limit']     = (int) $length;
    $params['offset']     = (int) $start;
    $params['order_column'] = $order_fields[$order_column];
    $params['order_dir']   = $order_dir;
    $params['sess_user_id'] = $sess_user_id;
    $params['sess_token']   = $sess_token;
    $params['searchtxt']   = $_GET['search']['value'];

    // print_r($params);die;
    $priv = $this->priv->get_priv();

    $list = $this->returPembelianModel->get_list_retur_pembelian($params);

    $data = array();
    foreach ($list['data'] as $row) {

      $action = $this->load->view('../modules/retur_pembelian/views/partials/table_action', ['returPembelian' => $row], true);
      $status = $this->load->view('../modules/retur_pembelian/views/partials/table_status', ['returPembelian' => $row], true);

      array_push($data, [
        $row['no_invoice'],
        $row['principle'],
        $row['date'],
        $row['pic_name'],
        $status,
        $action,
      ]);
    }

    $result["data"] = $data;
    $result["recordsTotal"] = $list['total'];
    $result["recordsFiltered"] = $list['total_filtered'];
    $result["draw"] = $draw;

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function add() {
    // $params['region'] = 4;

    $principles = $this->returPembelianModel->get_principles();
    // $list_pembelian = $this->returPembelianModel->get_list_pembelian();
    $list_invoice_pembelian = $this->returPembelianModel->get_list_invoice_pembelian();
    // echo '<pre>'; print_r($list_invoice_pembelian); die;

    $data = array(
      'principles' => $principles,
      // 'list_pembelian' => $list_pembelian,
      'list_invoice_pembelian' => $list_invoice_pembelian,
    );

    $this->template->load('maintemplate', 'retur_pembelian/views/addReturPembelian', $data);
  }

  public function add_retur_pembelian() {
    $this->form_validation->set_rules('id_invoice', 'Invoice', 'required');
    // $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

    if ($this->form_validation->run() == false) {
      $msg = validation_errors();
      $result = array('success' => false, 'message' => $msg);
    } else {

      try {
        // $this->db->trans_begin();

        $idInvoice = $this->input->post('id_invoice');

        $params = [
          'id_invoice' => $idInvoice,
          'keterangan_retur' => anti_sql_injection($this->input->post('keterangan_retur')),
          'retur_date' => anti_sql_injection($this->input->post('tgl_retur')),
          'pic' => $this->session->userdata['logged_in']['user_id'] ?? null,
        ];
        // echo"<pre>";print_r($this->input->post());die;

        $insertResult = $this->returPembelianModel->add_retur_pembelian($params);

        $totalItems = $this->input->post('total_items') ?? 0;

        for ($idx = 0; $idx < $totalItems; $idx++) {
          $idPoMat = $this->input->post('id_t_ps_' . $idx);
          $qtyRetur = $this->input->post('qty_retur_' . $idx);

          $detailParams = [
            'id_retur_pembelian' => $insertResult['lastid'] ?? null, //v
            'id_pembelian' => $idInvoice, //x
            'id_po_mat' => $idPoMat, //v
            'retur_qty' => $qtyRetur, //v
          ];

          $this->returPembelianModel->add_detail_retur_pembelian($detailParams);
        }

        $msg = 'Berhasil Menambah Data Retur Pembelian';
        // $this->db->trans_commit();
        $success = true;
      } catch (\Throwable $th) {
        // $this->db->trans_rollback();
        $msg = 'Gagal Menambah Data Retur Pembelian';
        $success = false;
      }

      $this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
      $result = array('success' => $success, 'message' => $msg);
    }

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function get_detail_pembelian_old() {
    $params = json_decode(file_get_contents("php://input"), true);
    $pembelian = ($params['id_invoice'] ?? false) ? $this->returPembelianModel->get_detail_pembelian_by_invoice($params['id_invoice']) : [];
    $pembelian_items = ($params['id_invoice'] ?? false) ? $this->returPembelianModel->get_detail_pembelian_items_by_invoice($params['id_invoice']) : [];

    $items = '';
    foreach ($pembelian_items as $key => $item) {
      // var_dump($item, $key);
      // die;
      $template = $this->load->view('../modules/retur_pembelian/views/partials/items_row', ['item' => $item, 'index' => $key], true);
      $items .= $template;
    }

    // var_dump($items);
    // die;

    $this->output->set_content_type('application/json')->set_output(json_encode([
      'data' => [
        'pembelian' => $pembelian,
        'items' => $items,
        'total_items' => count($pembelian_items),
      ],
    ]));
  }

  public function get_detail_pembelian() {

    $data     = file_get_contents("php://input");
    $params     = json_decode($data, true);

    $result['pembelian'] = $this->returPembelianModel->get_detail_pembelian_by_invoice($params['id_invoice']);

    //print_r($result['penjualan']);die;

    //print_r($result['customer']);die;
    $item = $this->returPembelianModel->get_detail_pembelian_items_by_invoice($params['id_invoice']);



    $htl = '';
    $ii = 0;
    foreach ($item as $items) {

      $htl .=  '<div id="row_' . $ii . '" >';
      $htl .=  '									<div class="row" style="border-top-style:solid;">';
      $htl .=  '									<div style="margin-top:10px">';
      $htl .=  '										<div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">';
      $htl .=  '												<input name="kode_v' . $ii . '" id="kode_v' . $ii . '" type="text" class="form-control " placeholder="Qty" value="' . $items['stock_code'] . $items['stock_name'] . ' ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '"  readOnly >';
      $htl .=  '												<input name="kode_' . $ii . '" id="kode_' . $ii . '" type="hidden" class="form-control " placeholder="Qty" value="' . $items['id_material'] . '|' . 1 . '|' . $items['unit_price'] . '|' . $items['qty'] . '" readOnly  >';
      $htl .=  '												<input name="id_t_ps_' . $ii . '" id="id_t_ps_' . $ii . '" type="hidden" class="form-control " placeholder="Qty" value="' . $items['id_t_ps'] . '" readOnly  >';
      $htl .=  '										</div>';
      $htl .=  '										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
      $htl .=  '											<div class="form-group">';
      $htl .=  '												<input name="qty_' . $ii . '" id="qty_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Qty" value=0  >';
      $htl .=  '											</div>';
      $htl .=  '										</div>';
      $htl .=  '										<div class="col-lg-2 col-md-1 col-sm-3 col-xs-12">';
      $htl .=  '											<div class="form-group">';
      $htl .=  '												<input name="qty_retur_' . $ii . '" id="qty_retur_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Qty" value=' . number_format($items['qty_satuan'], 0, ',', '.') . ' readOnly  >';
      $htl .=  '											</div>';
      $htl .=  '										</div>';
      $htl .=  '											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
      $htl .=  '											<div class="form-group">';
      $htl .=  '												<input name="totalqty_' . $ii . '" id="totalqty_' . $ii . '" type="number"  class="form-control" placeholder="Diskon" value=' . number_format($items['qty'], 0, ',', '.') . ' readOnly >';
      $htl .=  '											</div>';
      $htl .=  '										</div>';
      $htl .=  '										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
      $htl .=  '											<div class="form-group">';
      $htl .=  '												<input name="harga_' . $ii . '" id="harga_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Harga" value=' . number_format($items['unit_price'], 0, ',', '.') . ' readOnly >';
      $htl .=  '											</div>';
      $htl .=  '										</div>';
      $htl .=  '										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
      $htl .=  '											<div class="form-group">';
      $htl .=  '												<input name="diskon_' . $ii . '" id="diskon_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Harga" value=' . number_format($items['diskon'], 0, ',', '.') . ' readOnly >';
      $htl .=  '											</div>';
      $htl .=  '										</div>';
      $htl .=  '										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
      $htl .=  '											<div class="form-group">';
      $htl .=  '												<input name="total_' . $ii . '" id="total_' . $ii . '" type="text" class="form-control rupiah" placeholder="Total" value=0 readOnly  >';
      $htl .=  '											</div>';
      $htl .=  '										</div>';
      $htl .=  '									</div>';
      $htl .=  '									</div>';
      $htl .=  '								</div>';

      //$list .= '<option value="'.$items['id_mat'].'|'.$items['unit_box'].'|'.$items['bottom_price'].'|'.$items['qty'].'" >'.$items['stock_code'].' - '.$items['stock_name'].' - '.$items['base_qty'].' '.$items['uom_symbol'].'</option>';
      $ii++;
    }
    //$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';

    $result['list'] = $htl;
    $result['int'] = $ii;
    //$result['total_amount'] = number_format(floatval($result['penjualan'][0]['total_amount']),0,',','.');
    //$result['ppn'] = number_format(floatval($result['penjualan'][0]['total_amount'])/11,2,',','.');
    //$result['dpp'] = number_format(floatval($result['penjualan'][0]['total_amount'])-floatval($result['ppn']),2,',','.');


    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }
  public function detail() {
    $idReturPembelian = anti_sql_injection($this->input->post('id_retur_pembelian', TRUE));

    $returPembelian = $this->returPembelianModel->get_retur_pembelian($idReturPembelian);
    $detailReturPembelian = $this->returPembelianModel->get_detail_retur_pembelian($idReturPembelian);
    $total = 0;
    $diskon = 0;
    foreach ($detailReturPembelian as $detail) {
      $total_row = $detail['retur_qty'] * $detail['unit_price'];
      $diskon_row = $total_row * $detail['diskon'] / 100;
      $total = $total + $total_row *(100-$detail['diskon'])/100;
      $diskon = $diskon + $diskon_row;
      
    }

    // echo '<pre>'; print_r($detailReturPembelian); die;

    // PPN 11% jika tgl invoice lewat 1 april 2022
    // $satuApril = date("Y-m-d", "2022-04-01");
    $tglPo = $returPembelian['date_po'];
    if(strtotime($returPembelian['date_po']) >= strtotime("2022-04-01")){
      $total_ppn = $total * $returPembelian['ppn'] / 100; // ambil ppn dari tabel t_purchase_order
    }else{
      $total_ppn = $total * $detailReturPembelian[0]['pajak'] / 100; // ambil ppn dari tabel m_mat
    }


    $this->template->load('maintemplate', 'retur_pembelian/views/detailReturPembelian', [
      'returPembelian' => $returPembelian,
      'detailReturPembelian' => $detailReturPembelian,      
      'total' => $total,
      'total_ppn' => $total_ppn,
      'diskon' => $diskon
    ]);
  }

  public function edit() {
    // $params['region'] = 4;
    $idReturPembelian = anti_sql_injection($this->input->post('id_retur_pembelian', TRUE));

    $returPembelian = $this->returPembelianModel->get_retur_pembelian($idReturPembelian);
    // echo '<pre>'; print_r($returPembelian); die;
    $list_invoice_pembelian = $this->returPembelianModel->get_list_invoice_pembelian($returPembelian['id_invoice']);

    $detailReturPembelian = $this->returPembelianModel->get_detail_retur_pembelian($idReturPembelian);

    $total = 0;
    $diskon = 0;
    foreach ($detailReturPembelian as $detail) {
      $total_row = $detail['retur_qty'] * $detail['unit_price'];
      $diskon_row = $total_row * $detail['diskon'] / 100;
      $total = $total + $total_row;
      $diskon = $diskon + $diskon_row;
    }

    // PPN 11% jika tgl invoice lewat 1 april 2022
    // $satuApril = date('Y-m-d', '2022-04-01');
    $tglPo = $returPembelian['date_po'];
    if(strtotime($returPembelian['date_po']) >= strtotime("2022-04-01")){
      $total_ppn = $total * $returPembelian['ppn'] / 100; // ambil ppn dari tabel t_purchase_order
    }else{
      $total_ppn = $total * $detailReturPembelian[0]['pajak'] / 100; // ambil ppn dari tabel m_mat
    }

    $data = array(
      'list_invoice_pembelian' => $list_invoice_pembelian,
      'returPembelian' => $returPembelian,
      'detailReturPembelian' => $detailReturPembelian,
      'total' => $total,
      'total_ppn' => $total_ppn,
      'diskon' => $diskon
    );

    $this->template->load('maintemplate', 'retur_pembelian/views/editReturPembelian', $data);
  }

  public function update_retur_pembelian() {
    $this->form_validation->set_rules('id_retur_pembelian', 'Retur Pembelian', 'required');
    $this->form_validation->set_rules('id_invoice', 'Invoice', 'required');
    // $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

    if ($this->form_validation->run() == false) {
      $msg = validation_errors();
      $result = array('success' => false, 'message' => $msg);
    } else {

      try {
        // $this->db->trans_begin();

        $idReturPembelian = $this->input->post('id_retur_pembelian');
        $idInvoice = $this->input->post('id_invoice');

        $params = [
          'id_invoice' => $idInvoice,
          'keterangan_retur' => anti_sql_injection($this->input->post('keterangan_retur')),
          'retur_date' => anti_sql_injection($this->input->post('tgl_retur')),
          'status' => 0,
          'pic' => $this->session->userdata['logged_in']['user_id'] ?? null,
        ];

        // var_dump($params);
        // die;

        $this->returPembelianModel->update_retur_pembelian($idReturPembelian, $params);

        $totalItems = $this->input->post('total_items') ?? 0;

        if ($totalItems > 0) {
          // clear old detail retur pembelian
          $this->returPembelianModel->clear_detail_retur_pembelian($idReturPembelian);
        }

        for ($idx = 0; $idx < $totalItems; $idx++) {
          $idPoMat = $this->input->post('id_t_ps_' . $idx);
          $qtyRetur = $this->input->post('qty_retur_' . $idx);

          $detailParams = [
            'id_retur_pembelian' => $idReturPembelian,
            'id_pembelian' => $idInvoice,
            'id_po_mat' => $idPoMat,
            'retur_qty' => $qtyRetur,
          ];

          $this->returPembelianModel->add_detail_retur_pembelian($detailParams);
        }


        $msg = 'Berhasil Menambah Data Retur Pembelian';
        // $this->db->trans_commit();
        $success = true;
      } catch (\Throwable $th) {
        // $this->db->trans_rollback();
        $msg = 'Gagal Menambah Data Retur Pembelian';
        $success = false;
      }

      $this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
      $result = array('success' => $success, 'message' => $msg);
    }

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function konfirmasi() {
    $idReturPembelian = anti_sql_injection($this->input->post('id_retur_pembelian', TRUE));
    $returPembelian = $this->returPembelianModel->get_retur_pembelian($idReturPembelian);
    $detailReturPembelian = $this->returPembelianModel->get_detail_retur_pembelian($idReturPembelian);
    // $pembelian = ($returPembelian['id_pembelian'] ?? false) ? $this->returPembelianModel->get_detail_pembelian($returPembelian['id_pembelian']) : [];
    // $pembelian_items = ($returPembelian['id_pembelian'] ?? false) ? $this->returPembelianModel->get_detail_pembelian_items($returPembelian['id_pembelian']) : [];

    $total = 0;
    $diskon = 0;
    foreach ($detailReturPembelian as $detail) {
      $total_row = $detail['retur_qty'] * $detail['unit_price'];
      $diskon_row = $total_row * $detail['diskon'] / 100;
      $total = $total + $total_row *(100-$detail['diskon'])/100;
      $diskon = $diskon + $diskon_row;
      
    }
    $this->template->load('maintemplate', 'retur_pembelian/views/konfirmasiReturPembelian', [
      'returPembelian' => $returPembelian,
      'detailReturPembelian' => $detailReturPembelian,
      'total' => $total,
      'diskon' => $diskon
      // 'pembelian' => $pembelian,
      // 'pembelian_items' => $pembelian_items,
    ]);
  }

  public function delete_retur() {
    $data     = file_get_contents("php://input");
    $params     = json_decode($data, true);

    $this->returPembelianModel->clear_detail_retur_pembelian($params['id']);



    $msg = 'Berhasil menghapus data Retur Pembelian.';
    $result = array('success' => true, 'message' => $msg);
    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }
  public function approve_retur_pembelian() {
    $data     = file_get_contents("php://input");
    $params     = json_decode($data, true);

    $this->returPembelianModel->update_retur_pembelian($params['id'], [
      'status' => $params['status'],
      'approval_date' => $params['status'] == 1 ? date('Y-m-d') : null,
    ]);

    if ($params['status'] == 1) {

      //$sss = $this->returPembelianModel->update_pomat($params['id']);


      $sss = $this->returPembelianModel->update_qty($params['id']);
      $sss = $this->returPembelianModel->insert_mutasi($params['id']);
      //$sss = $this->returPembelianModel->insert_coa_value($params['id']);

    } else {
    }

    $msg = $params['status'] == 1 ? 'Berhasil Menyetujui data Retur' : 'Berhasil Menolak data Retur';
    $this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
    $result = array('success' => true, 'message' => $msg);
    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  // public function test()
  // {
  // 	echo '<pre>'; print_r($this->returPembelianModel->get_list_retur_pembelian_with_invoice()); die;
  // }
}
