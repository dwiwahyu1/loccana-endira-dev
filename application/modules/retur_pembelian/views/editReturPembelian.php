<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto;
  }

  .scroll-overflow {
    min-height: 650px;
  }

  #modal-distributor::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }

  .select2-container .select2-choice {
    height: 35px !important;
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="product-payment-inner-st">
          <ul id="myTabedu1" class="tab-review-design">
            <li class="active"><a href="#description">Edit Retur Pembelian</a></li>
          </ul>
          <form id="edit_retur_pembelian" action="<?= base_url('retur_pembelian/update_retur_pembelian'); ?>" class="add-department" autocomplete="off">
            <input type="hidden" name="id_retur_pembelian" id="id_retur_pembelian" value="<?= $returPembelian['id_retur_pembelian']; ?>">
            <div id="myTabContent" class="tab-content custom-product-edit">
              <div class="product-tab-list tab-pane fade active in" id="description">
                <div class="row">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="review-content-section">
                      <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group">
                            <label>Nomor Invoice</label>
                            <select name="id_invoice" id="id_invoice" onChange="populate_detail_pembelian()" class="form-control" placeholder="Nama Principle">
                              <option value="" selected="selected" disabled>-- Pilih Invoice --</option>
                              <?php foreach ($list_invoice_pembelian as $invoicePembelian) : ?>
                                <?php if ($invoicePembelian['id_invoice'] == $returPembelian['id_invoice']) : ?>
                                  <option value="<?= $invoicePembelian['id_invoice']; ?>" selected><?= implode(' - ', [$invoicePembelian['no_invoice'], $invoicePembelian['name_eksternal']]); ?></option>
                                <?php else : ?>
                                  <option value="<?= $invoicePembelian['id_invoice']; ?>"><?= implode(' - ', [$invoicePembelian['no_invoice'], $invoicePembelian['name_eksternal']]); ?></option>
                                <?php endif; ?>
                              <?php endforeach; ?>
                            </select>
                          </div>

                          <div class="form-group date">
                            <label>Tanggal Pembelian</label>
                            <input name="tgl" id="tgl" type="text" class="form-control" placeholder="Tanggal Penjualan" value="<?= $returPembelian['date_po'] ?? ''; ?>" readonly disabled>
                          </div>
                          <div class="form-group">
                            <label>Principle</label>
                            <input name="principle" id="principle" type="text" class="form-control" placeholder="Principle" value="<?= $returPembelian['name_eksternal'] ?? ''; ?>" readonly>
                          </div>
                          <div class="form-group">
                            <label>Alamat</label>
                            <textarea name="alamat" id="alamat" type="text" class="form-control" placeholder="Alamat Principal" readonly><?= $returPembelian['eksternal_address'] ?? ''; ?></textarea>
                          </div>
                          <!-- <div class="form-group">
														<label>Att</label>
														<input name="att" id="att" type="text" class="form-control" placeholder="Att" readonly>
													</div> -->
                          <div class="form-group">
                            <label>No. Telp</label>
                            <input name="telp" id="telp" type="text" class="form-control" placeholder="Telephone" value="<?= $returPembelian['phone_1']; ?>" readonly>
                          </div>
                          <div class="form-group">
                            <label>Email</label>
                            <input name="email" id="email" type="text" class="form-control" placeholder="Email" value="<?= $returPembelian['email']; ?>" readonly>
                          </div>
                          <!-- <div class="form-group">
														<label>Limit Kredit</label>
														<input name="limit" id="limit" type="text" class="form-control" placeholder="Limit Kredit" readonly>
													</div>
													<div class="form-group">
														<label>Sisa Kredit</label>
														<input name="sisa" id="sisa" type="text" class="form-control" placeholder="Sisa Kredit" readonly>
													</div> -->
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                          <div class="form-group">
                            <label>Ship From:</label>
                            <textarea name="alamat_kirim" id="alamat_kirim" type="text" class="form-control" placeholder="Alamat Principal" readonly>JL. Sangkuriang NO.38-A NPWP: 01.555.161.7.428.000</textarea>
                          </div>
                          <div class="form-group date">
                            <label>Email</label>
                            <input name="emaila" id="emaila" type="email" class="form-control" placeholder="Email" readonly>
                          </div>
                          <div class="form-group date">
                            <label>Telp/Fax</label>
                            <input name="telpfax" id="telpfax" type="text" class="form-control" placeholder="Telp/Fax" value="(022) 6626-946" readonly>
                          </div>

                          <div class="form-group date">
                            <label>PPN</label>
                            <input name="ppn" id="ppn" type="number" readonly class="form-control" placeholder="VAT/PPN" onKeyup="change_ppn()" value="<?= $returPembelian['ppn'] ?>">
                          </div>
                          <div class="form-group date">
                            <label>Term Pembayaran</label>
                            <input name="term" id="term" type="hidden" class="form-control" placeholder="Term (Hari)" value="<?= $returPembelian['term_of_payment'] ?? 0; ?>" style="" readonly>
                            <input name="term_o" id="term_o" type="number" class="form-control" placeholder="Term (Hari)" value="<?= $returPembelian['term_of_payment'] ?? 0; ?>" style="" readonly>
                          </div>

                          <div class="form-group date">
                            <label>Keterangan Beli</label>
                            <textarea name="keterangan_beli" id="ket" class="form-control" placeholder="keterangan" readonly></textarea>
                          </div>
                          <div class="form-group date">
                            <label>Tanggal Retur</label>
                            <input name="tgl_retur" id="tgl_retur" class="form-control" placeholder="Tanggal Retur" value="<?= $returPembelian['retur_date'] ?? 0; ?>" />
                          </div>
                          <div class="form-group date">
                            <label>Keterangan Retur</label>
                            <textarea name="keterangan_retur" id="keterangan_retur" class="form-control" placeholder="keterangan"><?= $returPembelian['keterangan_retur'] ?? ''; ?></textarea>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
                <br><br>
                <div class="row">
                  <h3>Items</h3>
                </div>

                <div class="row">
                  <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                    <div class="form-group">
                      <label>Kode</label>
                    </div>
                  </div>
                  <div class="col-lg-1 col-md-2 col-sm-3 col-xs-12">
                    <div class="form-group">
                      <label>Qty Order Total</label>
                    </div>
                  </div>
                  <div class="col-lg-2 col-md-1 col-sm-3 col-xs-12">
                    <div class="form-group">
                      <label>Qty Retur</label>
                    </div>
                  </div>
                  <div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
                    <div class="form-group">
                      <label>Qty Order</label>
                    </div>
                  </div>

                  <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                    <div class="form-group">
                      <label>Harga</label>
                    </div>
                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                    <div class="form-group">
                      <label>Diskon</label>
                    </div>
                  </div>

                  <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                    <div class="form-group">
                      <label>Total</label>
                    </div>
                  </div>
                </div>
              </div>
              <input name="total_items" id="total_items" type="hidden" value="<?= count($detailReturPembelian); ?>" />
              <input name="int_flo" id="int_flo" type="hidden" class="form-control" placeholder="Qty" value=<?php echo count($detailReturPembelian); ?>>
              <div id="table_items">
                <?php
                $ci = get_instance();
                // echo"<pre>";print_r($detailReturPembelian);die;
                foreach ($detailReturPembelian as $index => $item) {
                  $ci->load->view('../modules/retur_pembelian/views/partials/items_row', ['index' => $index, 'item' => $item]);
                }
                ?>
              </div>
              <br>
              <div class="row" style="border-top-style:solid;text-align:right">
                <div style="margin-top:10px">
                  <div class="col-lg-7 col-md-3 col-sm-3 col-xs-12">

                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 " style="text-align:right">
                    <label>Sub Total</label>
                  </div>
                  <div class="col-lg-2 col-md-1 col-sm-3 col-xs-12" style="text-align:right">
                    <label id='subttl'><?php echo number_format($total, 2, ',', '.'); ?></label>
                  </div>
                </div>
              </div>

              <div class="row" style="text-align:right">
                <div style="margin-top:10px">
                  <div class="col-lg-7 col-md-3 col-sm-3 col-xs-12">

                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 " style="text-align:right">
                    <label>Diskon</label>
                  </div>
                  <div class="col-lg-2 col-md-1 col-sm-3 col-xs-12" style="text-align:right">
                    <label id='subttl3' style="float:right"><?php echo number_format($diskon, 2, ',', '.') ?></label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div style="margin-top:10px">
                  <div class="col-lg-7 col-md-3 col-sm-3 col-xs-12" style="text-align:right">
                  </div>
                  <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12" style="text-align:right">
                    <label>VAT/PPN</label>
                  </div>
                  <div class="col-lg-2 col-md-1 col-sm-3 col-xs-12" style="text-align:right">
                    <label id="vatppn"><?php echo number_format($total_ppn, 2, ',', '.'); ?></label>
                  </div>
                </div>
              </div>
              <div class="row" style="border-top-style:solid;">
                <div style="margin-top:10px">
                  <div class="col-lg-7 col-md-3 col-sm-3 col-xs-12"></div>
                  <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12 " style="text-align:right">
                    <label>Total</label>
                  </div>
                  <div class="col-lg-2 col-md-1 col-sm-3 col-xs-12" style="text-align:right">
                    <label id="grand_total"><?php echo number_format($total +$total_ppn - $diskon,2, ',', '.'); ?></label>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-12">
                  <div class="payment-adress">
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                    <button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()"> Batal</button>
                  </div>
                </div>
              </div>
            </div>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>

<script>
  function indonesia_currency_format(value, printSymbol = false) {
    var options = {};

    if (printSymbol) {
      options = {
        style: 'currency',
        currency: 'IDR'
      }
    }

    value = value.toFixed(2);
    remain = value % 1;

    value = new Intl.NumberFormat('id-ID', options).format(value);

    if (remain <= 0) {
      value = value + ',00';
    }

    return value;
  }

  function populate_detail_pembelian() {
    var datapost = {
      id_invoice: $('#id_invoice').val()
    };

    $.ajax({
      type: 'POST',
      url: "<?= base_url('retur_pembelian/get_detail_pembelian'); ?>",
      data: JSON.stringify(datapost),
      cache: false,
      contentType: false,
      processData: false,
      success: function(response) {

        $('#table_items').html('');

        if (response.data) {
          if (response.data.pembelian) {
            $('#tgl').val(response.data.pembelian.purchase_date || '');
            $('#principle').val(response.data.pembelian.principle || '');
            $('#alamat').val(response.data.pembelian.principle_address || '');
            $('#telp').val(response.data.pembelian.principle_phone || '');
            $('#email').val(response.data.pembelian.principle_email || '');
            $('#term, #term_o').val(response.data.pembelian.purchase_term || '');
            $('#keterangan_beli').val(response.data.pembelian.purchase_note || '');
          }

          if (response.data.items) {
            $('#table_items').html(response.data.items);
          }

          $('#total_items').val(response.data.total_items);
          // calculate_items();
        }
      }
    });
  }

  function change_item_qty(fl) {

    var qty = $('#qty_retur_' + fl).val();
    var harga = $('#harga_' + fl).val();
    var diskon = $('#diskon_' + fl).val();
    var remark = $('#remark_' + fl).val();
    var ppn = $('#ppn').val();

    //alert(qty);

    var harga = harga.replace('.', '');
    var harga = harga.replace('.', '');
    var harga = harga.replace('.', '');
    var harga = harga.replace('.', '');
    var harga = harga.replace(',', '.');

    // var diskon = diskon.replace('.', '');
    // var diskon = diskon.replace('.', '');
    // var diskon = diskon.replace('.', '');
    // var diskon = diskon.replace('.', '');
    // var diskon = diskon.replace(',', '.');

    // var qty = qty.replace('.', '');
    // var qty = qty.replace('.', '');
    // var qty = qty.replace('.', '');
    // var qty = qty.replace('.', '');
    // var qty = qty.replace(',', '.');
    //alert(harga);


    var total = (parseFloat(qty) * parseFloat(harga));
    var totals = new Intl.NumberFormat('de-DE', {
      minimumFractionDigits: 2
    }).format(total);

    // console.log(qty,harga,diskon,totals);
    if (isNaN(total)) {
      //var total_num = Number((total).toFixed(1)).toLocaleString();
      //$('#subttl').html('0');
      $('#total_' + fl).val('0');
    } else {
      //var total_num = Number((total).toFixed(1)).toLocaleString();
      //$('#subttl').html(total);
      $('#total_' + fl).val(totals);
    }

    var sub_total = 0;
    var sub_total_bd = 0;
    var sub_disk = 0;
    var int_val = parseInt($('#int_flo').val());

    for (var yo = 0; yo <= int_val; yo++) {

      if ($('#kode_' + yo).val() !== undefined) {

        var tots = $('#total_' + yo).val();
        var tots = tots.replace('.', '');
        var tots = tots.replace('.', '');
        var tots = tots.replace('.', '');
        var tots = tots.replace('.', '');
        var tots = tots.replace(',', '.');

        var prcs = $('#harga_' + yo).val();
        var prcs = prcs.replace('.', '');
        var prcs = prcs.replace('.', '');
        var prcs = prcs.replace('.', '');
        var prcs = prcs.replace('.', '');
        var prcs = prcs.replace(',', '.');

        var qtys = $('#qty_retur_' + yo).val();
        // var qtys = qtys.replace('.', '');
        // var qtys = qtys.replace('.', '');
        // var qtys = qtys.replace('.', '');
        // var qtys = qtys.replace('.', '');
        // var qtys = qtys.replace(',', '.');

        var diskons = $('#diskon_' + yo).val();
        // var diskons = diskons.replace('.', '');
        // var diskons = diskons.replace('.', '');
        // var diskons = diskons.replace('.', '');
        // var diskons = diskons.replace('.', '');
        // var diskons = diskons.replace(',', '.');



        sub_total = sub_total + parseFloat(tots);
        sub_total_bd = sub_total_bd + (parseFloat(prcs) * parseFloat(qtys));
				sub_disk = sub_disk + ((parseFloat(prcs) * parseFloat(qtys)) * (parseFloat($('#diskon_' + yo).val()) / 100));
      }

    }

    var total_ppn = (sub_total * (parseFloat($('#ppn').val()) / 100));
    console.log(total_ppn,sub_total , (parseFloat($('#ppn').val()) / 100));
    var grand_total = sub_total + +total_ppn-sub_disk;



    var gt = new Intl.NumberFormat('de-DE', {
      minimumFractionDigits: 2
    }).format(grand_total);
    var sub_totals = new Intl.NumberFormat('de-DE', {
      minimumFractionDigits: 2
    }).format(sub_total);
    var total_ppns = new Intl.NumberFormat('de-DE', {
      minimumFractionDigits: 2
    }).format(total_ppn);
    var total_bds = new Intl.NumberFormat('de-DE', {
      minimumFractionDigits: 2
    }).format(sub_total_bd);
    var total_sub_disk = new Intl.NumberFormat('de-DE', {
      minimumFractionDigits: 2
    }).format(sub_disk);
    $('#subttl').html(total_bds);
    $('#subttl2').html(sub_totals);
    $('#subttl3').html(total_sub_disk);
    $('#vatppn').html(total_ppns);
    $('#grand_total').html(gt);

  }

  function back() {
    window.location.href = "<?= base_url('retur_pembelian'); ?>";
  }

  $(document).ready(function() {
    $('#id_invoice').select2();

    $('.rupiah').priceFormat({
      prefix: '',
      centsSeparator: ',',
      centsLimit: 0,
      thousandsSeparator: '.'
    });

    $('#tgl').datepicker({
      format: "yyyy-mm-dd",
      autoclose: true,
      todayHighlight: true
    });

    $('#tgl_retur').datepicker({
      format: "yyyy-mm-dd",
      autoclose: true,
      todayHighlight: true
    });
    $('#edit_retur_pembelian').on('submit', function(e) {

      e.preventDefault();

      var formUrl = $(this).attr('action');
      var formData = new FormData();
      var totalItems = parseInt($('#total_items').val() || 0);

      formData.append('id_retur_pembelian', $('#id_retur_pembelian').val() || '');
      formData.append('id_invoice', $('#id_invoice').val() || '');
      formData.append('keterangan_retur', $('#keterangan_retur').val() || '');
      formData.append('tgl_retur', $('#tgl_retur').val() || '');
      formData.append('total_items', totalItems);

      for (var idx = 0; idx < totalItems; idx++) {
        formData.append('id_t_ps_' + idx, $('#id_t_ps_' + idx).val());
        formData.append('qty_retur_' + idx, $('#qty_retur_' + idx).val());
      }

      swal({
        title: 'Yakin akan Simpan Data ?',
        text: '',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak'
      }).then(function() {

        $.ajax({
          type: 'POST',
          url: formUrl,
          data: formData,
          cache: false,
          contentType: false,
          processData: false,
          success: function(response) {

            if (response.success == true) {

              swal({
                title: 'Success!',
                text: "Data berhasil disimpan",
                type: 'success',
                // showCancelButton: true,
                confirmButtonText: 'OK',
                // cancelButtonText: 'Tidak'
              }).then(function() {
                back();
              });

            } else {
              swal({
                title: 'Data Inputan Tidak Sesuai !! ',
                html: response.message,
                type: 'error',
                showCancelButton: true,
                confirmButtonText: 'Ok'
              });
            }
          }
        });

      });
    });
  });
</script>