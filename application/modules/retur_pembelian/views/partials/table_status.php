<?php
$status = $returPembelian['status'] ?? 0;
?>

<?php if ((int) $status === 0) : ?>
    <button type="button" class="btn btn-custon-rounded-two btn-info" onclick="konfirmasi(<?= $returPembelian['id_retur_pembelian']; ?>)">Konfirmasi</button>
<?php elseif ((int) $status === 1) : ?>
    <button type="button" class="btn btn-custon-rounded-two btn-primary" disabled="">Disetujui</button>
<?php elseif ((int) $status === 2) : ?>
    <button type="button" class="btn btn-custon-rounded-two btn-danger" disabled="">Ditolak</button>
<?php endif; ?>