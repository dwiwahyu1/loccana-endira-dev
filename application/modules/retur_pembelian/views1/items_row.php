<div class="items-row-template" id="row_INDEX" style="display: none;">
    <div class="row" style="border-top-style:solid;">
        <div style="margin-top:10px">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <input name="kode_vINDEX" id="kode_vINDEX" type="text" class="form-control " placeholder="Qty" value="Pemulus 1.00 kg" readonly="">
                <input name="kode_INDEX" id="kode_INDEX" type="hidden" class="form-control " placeholder="Qty" value="22|20|41500.0000|100.0000" readonly="">
                <input name="id_d_INDEX" id="id_d_INDEX" type="hidden" class="form-control " placeholder="Qty" value="3213" readonly="">
            </div>
            <div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
                <div class="form-group">
                    <input name="qty_INDEX" id="qty_INDEX" type="text" onkeyup="change_sum(INDEX)" class="form-control rupiah" placeholder="Qty" value="0">
                </div>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
                <div class="form-group"> <label id="iteml_INDEX">Box @ 20</label> </div>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
                <div class="form-group">
                    <input name="qtys_INDEX" id="qtys_INDEX" type="text" onkeyup="change_sum(INDEX)" class="form-control rupiah" placeholder="Qty" value="0">
                </div>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
                <div class="form-group">
                    <input name="qty_retur_INDEX" id="qty_retur_INDEX" type="text" onkeyup="change_sum(INDEX)" class="form-control rupiah" placeholder="Qty" value="0" readonly="">
                </div>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
                <div class="form-group">
                    <input name="totalqty_INDEX" id="totalqty_INDEX" type="number" class="form-control" placeholder="Diskon" value="100" readonly="">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                <div class="form-group">
                    <input name="harga_INDEX" id="harga_INDEX" type="text" onkeyup="change_sum(INDEX)" class="form-control rupiah" placeholder="Harga" value="41.500" readonly="">
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                <div class="form-group">
                    <input name="total_INDEX" id="total_INDEX" type="text" class="form-control rupiah" placeholder="Total" value="0" readonly="">
                </div>
            </div>
        </div>
    </div>
</div>