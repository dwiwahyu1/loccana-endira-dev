<!-- <div class="btn-group" style="display:block"> -->
<button type="button" class="btn btn-custon-rounded-two btn-info" data-toggle="tooltip" data-placement="top" title="Detail" onclick="detail(<?= $returPembelian['id_retur_pembelian'] ?? 0; ?>)">
    <i class="fa fa-search-plus"></i>
</button>

<?php if (in_array($returPembelian['status'], [0, 2])) : ?>
    <button type="button" class="btn btn-custon-rounded-two btn-warning" data-toggle="tooltip" data-placement="top" title="Update" onclick="edit(<?= $returPembelian['id_retur_pembelian'] ?? 0; ?>)">
        <i class="fa fa-edit"></i>
    </button>
<?php endif; ?>

<button type="button" class="btn btn-custon-rounded-two btn-danger" data-toggle="tooltip" data-placement="top" title="Delete" onclick="deleteRetur(<?= $returPembelian['id_retur_pembelian'] ?? 0; ?>)">
        <i class="fa fa-trash"></i>
    </button>
<!-- </div> -->