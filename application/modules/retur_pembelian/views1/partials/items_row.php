<div id="row_<?= $index; ?>">
    <div class="row" style="border-top-style:solid;">
        <div style="margin-top:10px">
            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    <input name="kode_<?= $index; ?>" id="kode_<?= $index; ?>" type="text" class="form-control " placeholder="Qty" value="<?= implode(' ', [$item['stock_code'] ?? '', $item['stock_name'] ?? '', $item['base_qty'] ?? '', $item['uom_symbol'] ?? '']); ?>" readonly>
                    <input id="id_t_ps_<?= $index; ?>" type="hidden" name="id_t_ps_<?= $index; ?>" value="<?= $item['id_t_ps'] ?? ''; ?>">
                </div>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
                <div class="form-group">
                    <input name="qty_<?= $index; ?>_label" id="qty_<?= $index; ?>_label" type="text" class="form-control text-right" placeholder="Qty" value="<?= indonesia_currency_format($item['qty'] ?? 0); ?>" readonly />
                    <input name="qty_<?= $index; ?>" id="qty_<?= $index; ?>" type="hidden" value="<?= (float) $item['qty']; ?>" />
                </div>
            </div>
            <div class="col-lg-2 col-md-1 col-sm-3 col-xs-12">
                <div class="form-group">
                    <?php if ($readonly ?? false) : ?>
                        <input name="qty_retur_<?= $index; ?>" id="qty_retur_<?= $index; ?>" type="text" class="form-control text-right" placeholder="Qty" value="<?= indonesia_currency_format($item['retur_qty'] ?? 0); ?>" readonly />
                    <?php else : ?>
                        <input name="qty_retur_<?= $index; ?>" id="qty_retur_<?= $index; ?>" type="number" step="0.01" min="0" max="<?= $item['qty']; ?>" onchange="change_item_qty(<?= $index; ?>)" class="form-control text-right" placeholder="Qty" value="<?= $item['retur_qty'] ?? 0; ?>" />
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-lg-1 col-md-2 col-sm-3 col-xs-12">
                <div class="form-group">
                    <input name="total_qty_<?= $index; ?>_label" id="total_qty_<?= $index; ?>_label" type="text" class="form-control text-right" placeholder="Total" value="<?= isset($item['retur_qty']) ? indonesia_currency_format($item['qty'] - $item['retur_qty']) : indonesia_currency_format($item['qty'] ?? 0); ?>" readonly />
                    <input name="total_qty_<?= $index; ?>" id="total_qty_<?= $index; ?>" type="hidden" value="<?= isset($item['retur_qty']) ? (float) ($item['qty'] - $item['retur_qty']) : (float) $item['qty']; ?>" readonly />
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                <div class="form-group">
                    <input name="harga_<?= $index; ?>_label" id="harga_<?= $index; ?>_label" type="text" class="form-control text-right rupiah" placeholder="Harga" value=<?= ((float)$item['unit_price']?? 0); ?> readonly />
                    <input name="harga_<?= $index; ?>" id="harga_<?= $index; ?>" type="hidden" value=<?= (float) $item['unit_price']; ?> />
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                <div class="form-group">
                    <input name="diskon<?= $index; ?>_label" id="diskon_<?= $index; ?>_label" type="text" class="form-control text-right rupiah" placeholder="Harga" value=<?= ((float)$item['diskon']?? 0); ?> readonly />
                    <input name="diskon<?= $index; ?>" id="diskon_<?= $index; ?>" type="hidden" value=<?= (float) $item['diskon']; ?> />
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
                <div class="form-group">
                    <input name="total_<?= $index; ?>" id="total_<?= $index; ?>" type="text" class="form-control text-right rupiah" placeholder="Harga" value=<?= ((float)$item['unit_price']*$item['retur_qty']*(100-$item['diskon'])/100 ?? 0); ?> readonly />
                    <!-- <input name="total_<?= $index; ?>" id="total_<?= $index; ?>" type="hidden" value=<?= (float) $item['unit_price']*$item['retur_qty']*(100-$item['diskon'])/100; ?> /> -->
                </div>
            </div>

        </div>
    </div>
</div>