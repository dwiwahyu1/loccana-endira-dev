<?php
function laba_rugi($param) {

  $s_date = explode('-', $param['end_time']);
  $start_date = $s_date[0] . '-01-01';

  //print_r($param);die;
  // $data = file_get_contents("php://input");
  // $param = json_decode($data, true);

  $params = array(
    'start_time' => $start_date,
    'end_time' => $param['end_time'],
    'report' => "laba_rugi"
  );

  $params_p = array(
    'start_time' => $start_date,
    'end_time' => $param['end_time'],
    'report' => "laba_rugi"
  );


  $datas['get_aktiva'] = $this->rl_model->get_report_keu($params);
  $datas['get_summ'] = $this->rl_model->get_report_keu_summ($params_p);
  $datas['get_piutang'] = $this->rl_model->get_piutang($params);
  //$datas['get_persediaan'] = $this->rl_model->get_persediaan($params);
  $datas['get_persediaan'] = $this->reports_persediaan_rl($params);
  $datas['get_hutang'] = $this->rl_model->get_hutang($params);
  $datas['get_penjualan'] = $this->rl_model->get_penjualan($params);
  $datas['get_pembelian'] = $this->rl_model->get_pembelian($params);
  $datas['get_retur'] = $this->rl_model->get_retur($params);

  //print_r($datas['get_persediaan']);die;

  $html_a = '';

  $tot_a = 0;
  $tot_b = 0;

  $hp1 = 0;
  $hp2 = 0;

  $penj = 0;
  $penj_pes = 0;
  $retur = 0;

  foreach ($datas['get_aktiva'] as $get_aktivas) {

    if ($get_aktivas['type_data'] == 0) {



      if ($get_aktivas['coa'] == 50100) {

        $html_a .= ' <tr>
  <th>' . $get_aktivas['coa'] . '</th>
  <th>' . $get_aktivas['keterangan'] . '</th>
  <th>' . number_format($datas['get_persediaan']['persediaan_awal'], 2, ',', '.') . '</th>
  <th>0.00</th>
</tr>';
        $tot_a += $datas['get_persediaan']['persediaan_awal'];

        $nn = $datas['get_persediaan']['persediaan_awal'];
      } elseif ($get_aktivas['coa'] == 50201) {

        $html_a .= ' <tr>
  <th>' . $get_aktivas['coa'] . '</th>
  <th>' . $get_aktivas['keterangan'] . '</th>
  <th>' . number_format($datas['get_pembelian'][0]['pestisida'], 2, ',', '.') . '</th>
  <th>0.00</th>
</tr>';
        $tot_a += $datas['get_pembelian'][0]['pestisida'];

        $nn = $datas['get_pembelian'][0]['pestisida'];
        $pemb = $datas['get_pembelian'][0]['pestisida'];
      } elseif ($get_aktivas['coa'] == 50202) {

        $html_a .= ' <tr>
  <th>' . $get_aktivas['coa'] . '</th>
  <th>' . $get_aktivas['keterangan'] . '</th>
  <th>' . number_format($datas['get_pembelian'][0]['non_pestisida'], 2, ',', '.') . '</th>
  <th>0.00</th>
</tr>';
        $tot_a += $datas['get_pembelian'][0]['non_pestisida'];

        $nn = $datas['get_pembelian'][0]['non_pestisida'];
      } elseif ($get_aktivas['coa'] == 41104) {

        $html_a .= ' <tr>
  <th>' . $get_aktivas['coa'] . '</th>
  <th>' . $get_aktivas['keterangan'] . '</th>
  <th>' . number_format($datas['get_retur'][0]['nilai_retur'], 2, ',', '.') . '</th>
  <th>0.00</th>
</tr>';
        $tot_a += $datas['get_retur'][0]['nilai_retur'];
        $penj -= $datas['get_retur'][0]['nilai_retur'];

        $nn = $datas['get_retur'][0]['nilai_retur'];
      } else {

        $html_a .= ' <tr>
  <th>' . $get_aktivas['coa'] . '</th>
  <th>' . $get_aktivas['keterangan'] . '</th>
  <th>' . number_format($get_aktivas['nilai'], 2, ',', '.') . '</th>
  <th>0.00</th>
</tr>';
        $tot_a += $get_aktivas['nilai'];

        $nn = $get_aktivas['nilai'];
      }

      if ($get_aktivas['coa'] == 50100 | $get_aktivas['coa'] == 50201 | $get_aktivas['coa'] == 50202 | $get_aktivas['coa'] == 50500 | $get_aktivas['coa'] == 50600) {
        $hp1 += $nn;
      }
    } else {

      if ($get_aktivas['coa'] == 50700) {

        $html_a .= ' <tr>
  <th>' . $get_aktivas['coa'] . '</th>
  <th>' . $get_aktivas['keterangan'] . '</th>
  <th>0.00</th>
  <th>' . number_format($datas['get_persediaan']['persediaan_akhir'], 2, ',', '.') . '</th>
</tr>';
        $tot_b += $datas['get_persediaan']['persediaan_akhir'];

        $nn = $datas['get_persediaan']['persediaan_akhir'];
      } elseif ($get_aktivas['coa'] == 41102) {

        $html_a .= ' <tr>
  <th>' . $get_aktivas['coa'] . '</th>
  <th>' . $get_aktivas['keterangan'] . '</th>
  <th>0.00</th>
  <th>' . number_format($datas['get_penjualan'][0]['pestisida'], 2, ',', '.') . '</th>
</tr>';
        $tot_b += $datas['get_penjualan'][0]['pestisida'];

        $nn = $datas['get_penjualan'][0]['pestisida'];
        $penj += $datas['get_penjualan'][0]['pestisida'];
      } elseif ($get_aktivas['coa'] == 41103) {

        $html_a .= ' <tr>
  <th>' . $get_aktivas['coa'] . '</th>
  <th>' . $get_aktivas['keterangan'] . '</th>
  <th>0.00</th>
  <th>' . number_format($datas['get_penjualan'][0]['non_pestisida'], 2, ',', '.') . '</th>
</tr>';
        $tot_b += $datas['get_penjualan'][0]['non_pestisida'];

        $nn = $datas['get_penjualan'][0]['non_pestisida'];
        $penj += $datas['get_penjualan'][0]['non_pestisida'];
      } else {

        $html_a .= ' <tr>
  <th>' . $get_aktivas['coa'] . '</th>
  <th>' . $get_aktivas['keterangan'] . '</th>
  <th>0.00</th>
  <th>' . number_format($get_aktivas['nilai'], 2, ',', '.') . '</th>
</tr>';
        $tot_b += $get_aktivas['nilai'];

        $nn = $get_aktivas['nilai'];
      }

      if ($get_aktivas['coa'] == 50300 | $get_aktivas['coa'] == 50400 | $get_aktivas['coa'] == 50700) {
        $hp2 += $nn;
      }
    }
  }

  $html_a .= ' <tr>
  <th colspan=2>Total</th>
  <th>' . number_format($tot_a, 2, '.', ',') . '</th>
  <th>' . number_format($tot_b, 2, '.', ',') . '</th>
</tr>
<tr>
  <th colspan=3>Laba / Rugi </th>
  <th>' . number_format($tot_b - $tot_a, 2, '.', ',') . '</th>
</tr>';

  $html_p = '';


  $tot_p = 0;
  // foreach($datas['get_pasiva'] as $get_aktivas){

  // $html_p .= ' <tr>
  // <th>'.$get_aktivas['coa'].'</th>
  // <th>'.$get_aktivas['keterangan'].'</th>
  // <th>'.number_format($get_aktivas['nilai'],2,'.',',').'</th>
  // </tr>';
  // $tot_p +=$get_aktivas['nilai'];
  // }

  // $html_p .= ' <tr>
  // <th colspan=2>Total</th>
  // <th>'.number_format($tot_p,2,'.',',').'</th>
  // </tr>';
  //print_r($datas['get_summ']);die;

  $html_s = '
<tr>
  <th>PENJUALAN BERSIH</th>
  <th>' . number_format($penj, 2, ',', '.') . '</th>
</tr>
<tr>
  <th>BONUS PRINCIPLE</th>
  <th>' . number_format($datas['get_summ'][1]['nilai'], 2, ',', '.') . '</th>
</tr>
<tr>
  <th>HARGA POKOK PENJUALAN</th>
  <th>' . number_format(($hp1 - $hp2), 2, ',', '.') . '</th>
</tr>
<tr>
  <th><STRONG>LABA KOTOR</STRONG></th>
  <th>' . number_format($penj + $datas['get_summ'][1]['nilai'] - ($hp1 - $hp2), 2, ',', '.') . '</th>
</tr>
<tr>
  <th>BIAYA PENJUALAN</th>
  <th>' . number_format($datas['get_summ'][3]['nilai'], 2, ',', '.') . '</th>
</tr>
<tr>
  <th>BIAYA ADMINISTRASI dan UMUM</th>
  <th>' . number_format($datas['get_summ'][4]['nilai'], 2, ',', '.') . '</th>
</tr>
<tr>
  <th>BIAYA PENYUSUTAN</th>
  <th>' . number_format($datas['get_summ'][5]['nilai'], 2, ',', '.') . '</th>
</tr>
<tr>
  <th><STRONG>BIAYA OPERASIONAL</STRONG></th>
  <th>' . number_format($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai'], 2, ',', '.') . '</th>
</tr>
<tr>
  <th><STRONG>LABA (RUGI) OPERASI</STRONG></th>
  <th>' . number_format(($datas['get_summ'][0]['nilai'] + $datas['get_summ'][1]['nilai'] - $datas['get_summ'][2]['nilai']) - ($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai']), 2, ',', '.') . '</th>
</tr>
<tr>
  <th>PENDAPATAN NON OPERASI</th>
  <th>' . number_format($datas['get_summ'][6]['nilai'], 2, ',', '.') . '</th>
</tr>
<tr>
  <th>BIAYA NON OPERASI</th>
  <th>' . number_format($datas['get_summ'][7]['nilai'], 2, ',', '.') . '</th>
</tr>
<tr>
  <th><STRONG>LABA / RUGI BERSIH SEBELUM PAJAK</STRONG></th>
  <th>' . number_format(($penj + $datas['get_summ'][1]['nilai'] - ($hp1 - $hp2)) - ($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai']) + $datas['get_summ'][6]['nilai'] - $datas['get_summ'][7]['nilai'], 2, ',', '.') . '</th>
</tr>
';

  $datas['t_ac'] = $html_a;
  //$datas['t_pa'] = $html_p;
  $datas['t_pa'] = $html_s;

  $data_ret['penjualan'] = $penj;
  $data_ret['pembelian'] = $pemb;
  $data_ret['total_rl'] = ($penj + $datas['get_summ'][1]['nilai'] - ($hp1 - $hp2)) - ($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai']) + $datas['get_summ'][6]['nilai'] - $datas['get_summ'][7]['nilai'];


  $data_ret['ppn'] = (($pemb * 0.1) + ($retur * 0.1)) - ($penj * 0.1);


  return $data_ret;
  //$this->output->set_content_type('application/json')->set_output(json_encode($datas));
  //print_r($datas['get_aktiva']);die;

}
