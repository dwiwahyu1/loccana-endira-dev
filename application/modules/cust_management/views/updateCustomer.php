<style>
    .dt-body-left {
        text-align: left;
    }

    .dt-body-right {
        text-align: right;
    }

    .dt-body-center {
        text-align: center;
        vertical-align: middle;
    }

    .force-overflow {
        height: 650px;
        overflow-y: auto;
        overflow-x: auto
    }

    .scroll-overflow {
        min-height: 650px
    }

    #modal-distributor::-webkit-scrollbar-track {
        box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
        -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
        background-color: #F5F5F5;
    }

    #modal-distributor::-webkit-scrollbar {
        width: 10px;
        background-color: #F5F5F5;
    }

    #modal-distributor::-webkit-scrollbar-thumb {
        background-image: -webkit-gradient(linear,
                left bottom,
                left top,
                color-stop(0.44, rgb(122, 153, 217)),
                color-stop(0.72, rgb(73, 125, 189)),
                color-stop(0.86, rgb(28, 58, 148)));
    }

    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }
</style>

<div class="product-sales-area mg-tb-30">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="product-payment-inner-st">
                    <ul id="myTabedu1" class="tab-review-design">
                        <li class="active"><a href="#description">Update Customer</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content custom-product-edit">
                        <div class="product-tab-list tab-pane fade active in" id="description">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="review-content-section">
                                        <form id="edit_customer" action="<?php echo base_url() . 'cust_management/upodate_customer'; ?>" class="add-department">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label>Kode</label>
                                                        <input name="kode" id="kode" type="text" class="form-control" placeholder="Kode Customer" value="<?= $customer_result['code_cust'] ?? ''; ?>">
                                                        <input name="id" id="id" type="hidden" class="form-control" placeholder="Kode Customer" value="<?= $customer_result['id_t_cust'] ?? ''; ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Nama</label>
                                                        <input name="name" id="name" type="text" class="form-control" placeholder="Nama Customer" value="<?= $customer_result['cust_name'] ?? '' ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Nama Kontak</label>
                                                        <input name="cp" id="cp" type="text" class="form-control" placeholder="Nama Kontak" value="<?= $customer_result['contact_person'] ?? '' ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Wilayah</label>
                                                        <select name="region" id="region" type="text" class="form-control" placeholder="Wilayah">
                                                            <option selected="selected" disabled>-- Pilih Wilayah --</option>
                                                            <?php foreach ($wilayah as $wilayahs) : ?>
                                                                <?php if ($wilayahs['id_t_region'] == $customer_result['region']) : ?>
                                                                    <option value="<?= $wilayahs['id_t_region']; ?>" selected="selected"><?= $wilayahs['region']; ?></option>
                                                                <?php else : ?>
                                                                    <option value="<?= $wilayahs['id_t_region']; ?>"><?= $wilayahs['region']; ?></option>
                                                                <?php endif; ?>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label>Status Show/Hide</label>
                                                        <select class="form-control" name="sts_show" id="sts_show" required autocomplete="off">
                                                            <option value="0" <?php if ($customer_result['sts_show'] == 0) {
                                                                                    echo "selected='selected'";
                                                                                } ?>> Hide </option>
                                                            <option value="1" <?php if ($customer_result['sts_show'] == 1) {
                                                                                    echo "selected='selected'";
                                                                                } ?>> Show </option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Alamat</label>
                                                        <input name="alamat" id="alamat" type="text" class="form-control" placeholder="Alamat Customer" value="<?= $customer_result['cust_address'] ?? ''; ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>No. Telp</label>
                                                        <input name="telp" id="telp" type="text" class="form-control" placeholder="Telephone" value="<?= $customer_result['phone'] ?? ''; ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Email</label>
                                                        <input name="email" id="email" type="email" class="form-control" placeholder="Email" value="<?= $customer_result['email'] ?? ''; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                    <div class="form-group">
                                                        <label>No. NPWP</label>
                                                        <input name="npwp" id="npwp" type="text" class="form-control" placeholder="No. NPWP" value="<?= $customer_result['no_npwp'] ?? ''; ?>" onkeypress='return event.charCode == 46 || (event.charCode >= 48 && event.charCode <= 57)'>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Pemilik NPWP</label>
                                                        <input name="npwp_name" id="npwp_name" type="text" class="form-control" placeholder="Pemilik NPWP" value="<?= $customer_result['npwp_name'] ?? ''; ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Alamat Pemilik NPWP</label>
                                                        <input name="alamat_npwp" id="alamat_npwp" type="text" class="form-control" placeholder="Alamat Pemilik NPWP" value="<?= $customer_result['npwp_address'] ?? ''; ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Distrik</label>
                                                        <input name="distrik" id="distrik" type="text" class="form-control" placeholder="Distrik" value="<?= $customer_result['district'] ?? ''; ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Kota</label>
                                                        <input name="kota" id="kota" type="text" class="form-control" placeholder="Kota" value="<?= $customer_result['city'] ?? ''; ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Group</label>
                                                        <input name="group" id="group" type="text" class="form-control" placeholder="Group" value="<?= $customer_result['group'] ?? ''; ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Limit Kredit</label>
                                                        <input name="limit" id="limit" type="number" class="form-control" placeholder="Group" value="<?= $customer_result['credit_limit'] ?? ''; ?>">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="payment-adress">
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                                        <button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()"> Batal</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function clearform() {
        $('#add_principle').trigger("reset");
    }

    function back() {
        window.location.href = "<?php echo base_url() . 'cust_management'; ?>";
    }

    function listdist() {
        var user_id = '0001';
        var token = '093940349';
    }

    $(document).ready(function() {

        listdist();

        $('#edit_customer').on('submit', function(e) {
            // validation code here
            //if(!valid) {
            e.preventDefault();

            var formData = new FormData(this);
            var urls = $(this).attr('action');

            swal({
                title: 'Yakin akan Merubah Data ?',
                text: '',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak'
            }).then(function() {

                $.ajax({
                    type: 'POST',
                    url: urls,
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        if (response.success == true) {
                            swal({
                                title: 'Success!',
                                text: "Data Customer Berhasil Dirubah ",
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonText: 'Ok'
                            }).then(function() {
                                back();
                            })

                        } else {
                            swal({
                                title: 'Data Inputan Tidak Sesuai !! ',
                                text: response.message,
                                type: 'error',
                                showCancelButton: true,
                                confirmButtonText: 'Ok'
                            })
                        }
                    }
                });

            });
            //alert(kode);


            //}
        });
    });
</script>