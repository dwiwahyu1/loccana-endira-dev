<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cust_management_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}


	public function list_principal($params = array()){
		
			$query = 	'
				SELECT COUNT(*) AS jumlah 
				FROM t_customer A join t_region B on A.region = B.id_t_region 
				where 1 = 1
				AND ( 
					code_cust LIKE "%'.$params['searchtxt'].'%" OR
					cust_name LIKE "%'.$params['searchtxt'].'%" OR
					B.region LIKE "%'.$params['searchtxt'].'%" 
				)
			';
						
			$query2 = 	'
				SELECT z.*, rank() over ( ORDER BY code_cust ASC) AS Rangking from ( 
					select A.*,B.region as wilayah 
					FROM t_customer A join t_region B on A.region = B.id_t_region 
					where 1=1 
					AND ( 
					code_cust LIKE "%'.$params['searchtxt'].'%" OR 
					cust_name LIKE "%'.$params['searchtxt'].'%" OR
					B.region LIKE "%'.$params['searchtxt'].'%"
				)  order by code_cust) z
				ORDER BY code_cust ASC
				
				LIMIT '.$params['limit'].' 
				OFFSET '.$params['offset'].' 
			';
		
			//echo $query2;die;
		
			 $out		= array();
			  $querys		= $this->db->query($query);
			  $result = $querys->row();
			  
			  $total_filtered = $result->jumlah;
			  $total 			= $result->jumlah;
		  
				if(($params['offset']+10) > $total_filtered){
				$limit_data = $total_filtered - $params['offset'];
			  }else{
				$limit_data = $params['limit'] ;
			  }
		  
		
		  
		  //echo $query;die;
			//echo $query;die;
			 $query2s		= $this->db->query($query2);
		  $result2 = $query2s->result_array();						
		  $return = array(
			  'data' => $result2,
			  'total_filtered' => $total_filtered,
			  'total' => $total,
		  );
		  return $return;
		
		
	}
	
	public function get_wilayah($params = array()){
		
		$query = $this->db->get('t_region');
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_customer($params = array()){
		
		$query = $this->db->get_where('t_customer', array('id_t_cust' => $params['id_t_cust']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	
	public function check_customer($params = array()){
		
		$query = $this->db->get_where('t_customer', array('code_cust' => $params['code_cust']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_customer($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'code_cust' => $data['code_cust'],
			'cust_name' => $data['cust_name'],
			'region' => $data['region'],
			'no_npwp' => $data['no_npwp'],
			'npwp_name' => $data['npwp_name'],
			'npwp_address' => $data['npwp_address'],
			'cust_address' => $data['cust_address'],
			'district' => $data['district'],
			'city' => $data['city'],
			'group' => $data['group'],
			'phone' => $data['phone'],
			'email' => $data['email'],
			'contact_person' => $data['contact_person'],
			'credit_limit' => $data['credit_limit'],
			'status_credit' => $data['status_credit']
		);

		// print_r($datas);die;

		$this->db->insert('t_customer',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_limit_history($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'id_cust' => $data['id_cust'],
			'limit_price' => $data['limit_price'],
			'create_date' => $data['create_date']
		);

	//	print_r($datas);die;

		$this->db->insert('t_history_limit',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	
	public function update_customer($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'code_cust' => $data['code_cust'],
			'cust_name' => $data['cust_name'],
			'region' => $data['region'],
			'no_npwp' => $data['no_npwp'],
			'npwp_name' => $data['npwp_name'],
			'npwp_address' => $data['npwp_address'],
			'cust_address' => $data['cust_address'],
			'district' => $data['district'],
			'city' => $data['city'],
			'group' => $data['group'],
			'phone' => $data['phone'],
			'email' => $data['email'],
			'contact_person' => $data['contact_person'],
			'credit_limit' => $data['credit_limit'],
			'status_credit' => $data['status_credit'],
			'sts_show' => $data['sts_show']
		);

	//	print_r($datas);die;
		$this->db->where('id_t_cust',$data['id_t_cust']);
		$this->db->update('t_customer',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function update_status($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			
			'status_credit' => $data['status_credit']
		);

	//	print_r($datas);die;
		$this->db->where('id_t_cust',$data['id_t_cust']);
		$this->db->update('t_customer',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function get_limit_history($params = array()){
		
		$query=$this->db->order_by('id_hl','desc')
						->get_where('t_history_limit', array('id_cust' => $params['id_cust']), 1,0);
	
		// $this->db->order_by('id_hl','asc');
		// $query=$this->db->get();
		$return = $query->result_array();

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $return;
	}


	public function update_limit_history($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			
			'id_cust' => $data['id_cust'],
			'approval_date' => $data['approval_date']
		);

	//	print_r($datas);die;
		$this->db->where('id_hl',$data['id_hl']);
		$this->db->update('t_history_limit',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	

	function delete_customer($id_t_cust) {
		
		//print_r($id);die;
		
		$this->db->where('id_t_cust', $id_t_cust);
		$this->db->delete('t_customer'); 
		 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$this->db->where('id_cust', $id_t_cust);
		$this->db->delete('t_history_limit');

		return $result;
	}

}