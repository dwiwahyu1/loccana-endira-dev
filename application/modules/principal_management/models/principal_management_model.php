<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Principal_management_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function list_principal($params = array())
	{
		$query = '
				SELECT COUNT(*) AS jumlah FROM t_eksternal WHERE type_eksternal = 1
				AND ( 
					kode_eksternal LIKE "%' . $params['searchtxt'] . '%" OR
					name_eksternal LIKE "%' . $params['searchtxt'] . '%" 
				)
				AND kode_eksternal != ""
			';

		$query2 = '
				SELECT z.*, rank() over ( ORDER BY kode_eksternal ASC) AS Rangking from ( 
					SELECT * 
					FROM t_eksternal 
					WHERE 
						type_eksternal = 1  AND ( 
							kode_eksternal LIKE "%' . $params['searchtxt'] . '%" OR
							name_eksternal LIKE "%' . $params['searchtxt'] . '%" 
						)
						AND kode_eksternal != ""
					ORDER BY kode_eksternal) z
				ORDER BY kode_eksternal ASC
				
				LIMIT ' . $params['limit'] . ' 
				OFFSET ' . $params['offset'] . ' 
			';

		//echo $query2;die;

		$querys		= $this->db->query($query);
		$result = $querys->row();

		$total_filtered = $result->jumlah;
		$total 			= $result->jumlah;

		if (($params['offset'] + 10) > $total_filtered) {
			$limit_data = $total_filtered - $params['offset'];
		} else {
			$limit_data = $params['limit'];
		}

		//echo $query;die;
		//echo $query;die;
		$query2s		= $this->db->query($query2);
		$result2 = $query2s->result_array();
		$return = array(
			'data' => $result2,
			'total_filtered' => $total_filtered,
			'total' => $total,
		);
		return $return;
	}

	public function get_principal($params = array())
	{
		$query = $this->db->get_where('t_eksternal', array('id' => $params['id']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function check_principal($params = array())
	{
		$query = $this->db->get_where('t_eksternal', array('kode_eksternal' => $params['kode']));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_principal($data)
	{
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'kode_eksternal' => $data['kode'],
			'name_eksternal' => $data['name'],
			'eksternal_address' => $data['alamat'],
			'phone_1' => $data['telp'],
			'fax' => $data['fax'],
			'bank1' => $data['bank1'],
			'email' => $data['email'],
			'rek1' => $data['norek1'],
			'bank2' => $data['bank2'],
			'rek2' => $data['norek2'],
			'bank3' => $data['bank3'],
			'rek3' => $data['norek3'],
			'type_eksternal' => $data['type_eksternal']
		);

		//	print_r($datas);die;

		$this->db->insert('t_eksternal', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function update_principal($data)
	{
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'kode_eksternal' => $data['kode'],
			'name_eksternal' => $data['name'],
			'eksternal_address' => $data['alamat'],
			'phone_1' => $data['telp'],
			'fax' => $data['fax'],
			'email' => $data['email'],
			'bank1' => $data['bank1'],
			'rek1' => $data['norek1'],
			'bank2' => $data['bank2'],
			'rek2' => $data['norek2'],
			'bank3' => $data['bank3'],
			'rek3' => $data['norek3'],
			'type_eksternal' => $data['type_eksternal'],
			'sts_show' => $data['sts_show']
		);

		//	print_r($datas);die;
		$this->db->where('id', $data['id']);
		$this->db->update('t_eksternal', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	function delete_principal($id)
	{

		//print_r($id);die;

		$this->db->where('id', $id);
		$this->db->delete('t_eksternal');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}
