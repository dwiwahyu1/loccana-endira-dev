<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_persediaan extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('report_persediaan/return_model');
		$this->load->library('log_activity');
		
		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong');
		$this->form_validation->set_message('min_length', '%s Minimal 4 Karakter');
		$this->form_validation->set_message('max_length', '%s Maksimal 10 Karakter');
	}

	/**
	 * anti sql injection
	 * @return string
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	  * This function is redirect to index distributor page
	  * @return Void
	  */
	public function index() {
		
		$params['region'] = 4;
		
		$result['result'] = $this->return_model->get_principal($params);
		
		//print();die;
		
		$this->template->load('maintemplate', 'report_persediaan/views/index', $result);
	}

public function reports(){
		
		$data = array(
			'principal' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
			'tgl_awal' => $this->Anti_sql_injection($this->input->post('tgl_awal', TRUE)),
			'tgl_akhir' => $this->Anti_sql_injection($this->input->post('tgl_akhir', TRUE))
			
		);
		
				
				
				//print_r($params_awalss);die;
		
		$kuantiti_a = 0;
		$harga_satuan_a = 0;
		$nilai_a = 0;
		$pembelian_a = 0;
		$kuantiti_sol_a = 0;
		$kuantiti_titip_a = 0;
		$kuantiti_bonus_a = 0;
		$kuantiti_diskon_a = 0;
		$harga_sat_pemb_a = 0;
		$nilai_pemb_a = 0;
		$harga_pokok_a = 0;
		$penjualan_a = 0;
		$pengeluaran_a = 0;
		$saldo_akhir_a = 0;
		$nilai_akhir_a = 0;
		$retur1_a = 0;
		$retur2_a = 0;
		
		if($data['principal'] == 0){
			
			$dist_a = $this->return_model->get_distribution($data);
			$html = '';
			
			foreach($dist_a as $dist_as){
			
				$datat = array(
					'principal' => $dist_as['dist_id'],
					'tgl_awal' => $this->Anti_sql_injection($this->input->post('tgl_awal', TRUE)),
					'tgl_akhir' => $this->Anti_sql_injection($this->input->post('tgl_akhir', TRUE))
					
				);
			
				$params_awalss['principal']		= $dist_as['dist_id'];
				$params_awalss['tgl_akhir']	= date("Y-m-d", strtotime($data['tgl_awal'] ."-1 days"));
				$params_awalss['tgl_awal']	= date_format(date_create($params_awalss['tgl_akhir']),"Y-m-01");
			
			if($datat['tgl_awal'] == '2021-01-01' ){
				$result = $this->return_model->get_report_persediaan($datat);
				$result_awal = $this->return_model->get_report_persediaan($datat);
			}else{
				$result_awal = $this->return_model->get_report_persediaan_newest($params_awalss);
				$result = $this->return_model->get_report_persediaan_newest($datat);
			}
				// echo "<pre>";
			//print_r($result_awal);die; 
				
				
				$kuantiti = 0;
				$harga_satuan = 0;
				$nilai = 0;
				$pembelian = 0;
				$kuantiti_sol = 0;
				$kuantiti_titip = 0;
				$kuantiti_bonus = 0;
				$kuantiti_diskon = 0;
				$harga_sat_pemb = 0;
				$nilai_pemb = 0;
				$harga_pokok = 0;
				$penjualan = 0;
				$pengeluaran = 0;
				$saldo_akhir = 0;
				$nilai_akhir = 0;
				$prin = '';
				$retur1 = 0;
				$retur2 = 0;
				$kode_prin = '';
				
				$int_iuy = 0;
				foreach( $result as $datas ){
					
					if($datat['tgl_awal'] == '2021-01-01' ){
						$datas['harga_satuan'] = $datas['harga_satuan'];
					}else{
						$datas['harga_satuan'] = $result_awal[$int_iuy]['harga_pokok'];
					}
					
					$html .= '<tr>
								<th >'.$datas['stock_code'].'</th>
								<th>'.$datas['stock_name'].'</th>
								<th>'.$datas['base_qty'].' '.$datas['uom_symbol'].' x '.$datas['unit_box'].'</th>
								<th>'.number_format($datas['saldo_awals'],2,',','.').'</th>
								<th>'.number_format($datas['harga_satuan'],2,',','.').'</th>
								<th>'.number_format($datas['saldo_awals']*$datas['harga_satuan'],2,',','.').'</th>
								<th>'.number_format($datas['pembelian'],2,',','.').'</th>
								<th>'.number_format($datas['kuantiti_diskon'],2,',','.').'</th>
								<th>'.number_format($datas['kuantiti_titip'],2,',','.').'</th>
								<th>'.number_format($datas['kuantiti_bonus'],2,',','.').'</th>
								<th>'.number_format($datas['harga_sat_pemb'],2,',','.').'</th>
								<th>'.number_format($datas['nilai_pemb'],2,',','.').'</th>
								<th>'.number_format($datas['qty_retur_jual'],2,',','.').'</th>
								<th>'.$datas['ket'].'</th>
								<th>'.number_format($datas['harga_pokok'],2,',','.').'</th>
								<th>'.number_format($datas['penjualan'],2,',','.').'</th>
								<th>'.number_format($datas['pengeluaran'],2,',','.').'</th>
								<th>'.number_format(0,2,',','.').'</th>
								<th>'.number_format($datas['saldo_akhir'],2,',','.').'</th>
								<th>'.number_format(($datas['saldo_akhir'])*$datas['harga_pokok'],2,',','.').'</th>
							</tr>';
					
					$prin = $datas['name_eksternal'];
					$kode_prin = substr($datas['stock_code'],0,3);
					$kuantiti = $kuantiti + $datas['saldo_awals'];
					$harga_satuan = $harga_satuan+ $datas['harga_satuan'];
					$nilai = $nilai+ $datas['saldo_awals']*$datas['harga_satuan'];
					$pembelian = $pembelian+ $datas['pembelian'];
					$kuantiti_sol = $kuantiti_sol+ $datas['kuantiti_sol'];
					$kuantiti_diskon = $kuantiti_diskon+ $datas['kuantiti_diskon'];
					$kuantiti_titip = $kuantiti_titip+ $datas['kuantiti_titip'];
					$kuantiti_bonus = $kuantiti_bonus+ $datas['kuantiti_bonus'];
					$harga_sat_pemb = $harga_sat_pemb+ $datas['harga_sat_pemb'];
					$nilai_pemb = $nilai_pemb+ $datas['nilai_pemb'];
					$harga_pokok =$harga_pokok+ $datas['harga_pokok'];
					$penjualan = $penjualan+ $datas['penjualan'];
					$pengeluaran = $pengeluaran+ $datas['pengeluaran'];
					$saldo_akhir = $saldo_akhir+ $datas['saldo_akhir'];
					$retur1 = $retur1 + $datas['qty_retur_jual'];
					$retur2 = $retur1 + 0;
					$nilai_akhir = $nilai_akhir+ $datas['saldo_akhir']*$datas['harga_pokok'];
					
					$kuantiti_a = $kuantiti_a + $datas['saldo_awals'];
					$harga_satuan_a = $harga_satuan_a +  $datas['harga_satuan'];
					$nilai_a = $nilai_a + $datas['saldo_awals']*$datas['harga_satuan'];
					$pembelian_a = $pembelian_a +  $datas['pembelian'];
					$kuantiti_sol_a = $kuantiti_sol_a +  $datas['kuantiti_sol'];
					$kuantiti_titip_a = $kuantiti_titip_a +  $datas['kuantiti_titip'];
					$kuantiti_diskon_a = $kuantiti_diskon_a +  $datas['kuantiti_diskon'];
					$kuantiti_bonus_a = $kuantiti_bonus_a +  $datas['kuantiti_bonus'];
					$harga_sat_pemb_a = $harga_sat_pemb_a +  $datas['harga_sat_pemb'];
					$nilai_pemb_a = $nilai_pemb_a +  $datas['nilai_pemb'];
					$harga_pokok_a = $harga_pokok_a + $datas['harga_pokok'];
					$penjualan_a = $penjualan_a +  $datas['penjualan'];
					$pengeluaran_a = $pengeluaran_a +  $datas['pengeluaran'];
					$saldo_akhir_a = $saldo_akhir_a +  $datas['saldo_akhir'];
					$nilai_akhir_a = $nilai_akhir_a +  $datas['saldo_akhir']*$datas['harga_pokok'];
					$retur1_a = $retur1 + $datas['qty_retur_jual'];
					$retur2_a = $retur1 + 0;
				
					$int_iuy++;
				}
			
				$html .= '<tr style="background:yellow">
							<th>'.$kode_prin.'</th>
							<th>'.$prin.'</th>
							<th></th>
							<th>'.number_format($kuantiti,2,',','.').'</th>
							<th>'.number_format($harga_satuan,2,',','.').'</th>
							<th>'.number_format($nilai,2,',','.').'</th>
							<th>'.number_format($pembelian,2,',','.').'</th>
							<th>'.number_format($kuantiti_diskon,2,',','.').'</th>
							<th>'.number_format($kuantiti_titip,2,',','.').'</th>
							<th>'.number_format($kuantiti_bonus,2,',','.').'</th>
							<th>'.number_format($harga_sat_pemb,2,',','.').'</th>
							<th>'.number_format($nilai_pemb,2,',','.').'</th>
							<th>'.number_format($retur1,2,',','.').'</th>
							<th></th>
							<th>'.number_format($harga_pokok,2,',','.').'</th>
							<th>'.number_format($penjualan,2,',','.').'</th>
							<th>'.number_format($pengeluaran,2,',','.').'</th>
							<th>'.number_format($retur2,2,',','.').'</th>
							<th>'.number_format($saldo_akhir,2,',','.').'</th>
							<th>'.number_format($nilai_akhir,2,',','.').'</th>
						</tr>';
			}
		}else{
		
			$datat = array(
					'principal' => $data['principal'],
					'tgl_awal' => $this->Anti_sql_injection($this->input->post('tgl_awal', TRUE)),
					'tgl_akhir' => $this->Anti_sql_injection($this->input->post('tgl_akhir', TRUE))
					
				);
				
			$params_awalss['principal']		= $data['principal'];
				$params_awalss['tgl_akhir']	= date("Y-m-d", strtotime($data['tgl_awal'] ."-1 days"));
				$params_awalss['tgl_awal']	= date_format(date_create($params_awalss['tgl_akhir']),"Y-m-01");
		
		
			if($datat['tgl_awal'] == '2021-01-01' ){
				$result = $this->return_model->get_report_persediaan($datat);
				$result_awal = $this->return_model->get_report_persediaan($params_awalss);
			}else{
				$result = $this->return_model->get_report_persediaan_newest($datat);
				$result_awal = $this->return_model->get_report_persediaan_newest($params_awalss);
			}
		
		//print_r($result);die;
		
		$html = '';
		
		$kuantiti = 0;
		$harga_satuan = 0;
		$nilai = 0;
		$pembelian = 0;
		$kuantiti_sol = 0;
		$kuantiti_titip = 0;
		$kuantiti_bonus = 0;
		$kuantiti_diskon = 0;
		$harga_sat_pemb = 0;
		$nilai_pemb = 0;
		$harga_pokok = 0;
		$penjualan = 0;
		$pengeluaran = 0;
		$saldo_akhir = 0;
		$nilai_akhir = 0;
		$retur1 = 0;
				$retur2 = 0;
		$prin = '';
		$kode_prin = '';
		
		$int_iuy = 0;
		foreach( $result as $datas ){
			
			if($datat['tgl_awal'] == '2021-01-01' ){
				$datas['harga_satuan'] = $datas['harga_satuan'];
			}else{
				$datas['harga_satuan'] = $result_awal[$int_iuy]['harga_pokok'];
			}
			
			$html .= '<tr>
						<th>'.$datas['stock_code'].'</th>
						<th>'.$datas['stock_name'].'</th>
						<th>'.$datas['base_qty'].' '.$datas['uom_symbol'].' x '.$datas['unit_box'].'</th>
						<th>'.number_format($datas['saldo_awals'],2,',','.').'</th>
						<th>'.number_format($datas['harga_satuan'],2,',','.').'</th>
						<th>'.number_format($datas['saldo_awals']*$datas['harga_satuan'],2,',','.').'</th>
						<th>'.number_format($datas['pembelian'],2,',','.').'</th>
						<th>'.number_format($datas['kuantiti_diskon'],2,',','.').'</th>
						<th>'.number_format($datas['kuantiti_titip'],2,',','.').'</th>
						<th>'.number_format($datas['kuantiti_bonus'],2,',','.').'</th>
						<th>'.number_format($datas['harga_sat_pemb'],2,',','.').'</th>
						<th>'.number_format($datas['nilai_pemb'],2,',','.').'</th>
						<th>'.number_format($datas['qty_retur_jual'],2,',','.').'</th>
						<th>'.$datas['ket'].'</th>
						<th>'.number_format($datas['harga_pokok'],2,',','.').'</th>
						<th>'.number_format($datas['penjualan'],2,',','.').'</th>
						<th>'.number_format($datas['pengeluaran'],2,',','.').'</th>
						<th>'.number_format(0,2,',','.').'</th>
						<th>'.number_format($datas['saldo_akhir'],2,',','.').'</th>
						<th>'.number_format($datas['saldo_akhir']*$datas['harga_pokok'],2,',','.').'</th>
					</tr>';
			
			$prin = $datas['name_eksternal'];
			$kode_prin = substr($datas['stock_code'],0,3);
			$kuantiti = $kuantiti + $datas['saldo_awals'];
			$harga_satuan = $harga_satuan+ $datas['harga_satuan'];
			$nilai = $nilai+ $datas['saldo_awals']*$datas['harga_satuan'];
			$pembelian = $pembelian+ $datas['pembelian'];
			$kuantiti_sol = $kuantiti_sol+ $datas['kuantiti_sol'];
			$kuantiti_diskon = $kuantiti_diskon+ $datas['kuantiti_diskon'];
			$kuantiti_titip = $kuantiti_titip+ $datas['kuantiti_titip'];
			$kuantiti_bonus = $kuantiti_bonus+ $datas['kuantiti_bonus'];
			$harga_sat_pemb = $harga_sat_pemb+ $datas['harga_sat_pemb'];
			$nilai_pemb = $nilai_pemb+ $datas['nilai_pemb'];
			$harga_pokok =$harga_pokok+ $datas['harga_pokok'];
			$penjualan = $penjualan+ $datas['penjualan'];
			$pengeluaran = $pengeluaran+ $datas['pengeluaran'];
			$saldo_akhir = $saldo_akhir+ $datas['saldo_akhir'];
			$retur1 = $retur1 + $datas['qty_retur_jual'];
					$retur2 = $retur1 + 0;
			$nilai_akhir = $nilai_akhir+ $datas['saldo_akhir']*$datas['harga_pokok'];
			
			$kuantiti_a = $kuantiti_a + $datas['saldo_awals'];
					$harga_satuan_a = $harga_satuan_a +  $datas['harga_satuan'];
					$nilai_a = $nilai_a + $datas['saldo_awals']*$datas['harga_satuan'];
					$pembelian_a = $pembelian_a +  $datas['pembelian'];
					$kuantiti_sol_a = $kuantiti_sol_a +  $datas['kuantiti_sol'];
					$kuantiti_titip_a = $kuantiti_titip_a +  $datas['kuantiti_titip'];
					$kuantiti_diskon_a = $kuantiti_diskon_a +  $datas['kuantiti_diskon'];
					$kuantiti_bonus_a = $kuantiti_bonus_a +  $datas['kuantiti_bonus'];
					$harga_sat_pemb_a = $harga_sat_pemb_a +  $datas['harga_sat_pemb'];
					$nilai_pemb_a = $nilai_pemb_a +  $datas['nilai_pemb'];
					$harga_pokok_a = $harga_pokok_a + $datas['harga_pokok'];
					$penjualan_a = $penjualan_a +  $datas['penjualan'];
					$pengeluaran_a = $pengeluaran_a +  $datas['pengeluaran'];
					$saldo_akhir_a = $saldo_akhir_a +  $datas['saldo_akhir'];
					$nilai_akhir_a = $nilai_akhir_a +  $datas['saldo_akhir']*$datas['harga_pokok'];
					$retur1_a = $retur1 + $datas['qty_retur_jual'];
					$retur2_a = $retur1 + 0;
			
			$int_iuy++;
		}
		
			$html .= '<tr style="background:yellow">
						<th>'.$kode_prin.'</th>
						<th colspan="2">'.$prin.'</th>
						<th>'.number_format($kuantiti,2,',','.').'</th>
						<th>'.number_format($harga_satuan,2,',','.').'</th>
						<th>'.number_format($nilai,2,',','.').'</th>
						<th>'.number_format($pembelian,2,',','.').'</th>
						<th>'.number_format($kuantiti_diskon,2,',','.').'</th>
						<th>'.number_format($kuantiti_titip,2,',','.').'</th>
						<th>'.number_format($kuantiti_bonus,2,',','.').'</th>
						<th>'.number_format($harga_sat_pemb,2,',','.').'</th>
						<th>'.number_format($nilai_pemb,2,',','.').'</th>
						<th>'.number_format($retur1,2,',','.').'</th>
						<th></th>
						<th>'.number_format($harga_pokok,2,',','.').'</th>
						<th>'.number_format($penjualan,2,',','.').'</th>
						<th>'.number_format($pengeluaran,2,',','.').'</th>
						<th>'.number_format($retur2,2,',','.').'</th>
						<th>'.number_format($saldo_akhir,2,',','.').'</th>
						<th>'.number_format($nilai_akhir,2,',','.').'</th>
					</tr>';
					
					
					
		
		
		}
		
		$html .= '<tr style="background:green">
						<th colspan="3">Total </th>
						<th>'.number_format($kuantiti_a,2,',','.').'</th>
						<th>'.number_format($harga_satuan_a,2,',','.').'</th>
						<th>'.number_format($nilai_a,2,',','.').'</th>
						<th>'.number_format($pembelian_a,2,',','.').'</th>
						<th>'.number_format($kuantiti_diskon_a,2,',','.').'</th>
						<th>'.number_format($kuantiti_titip_a,2,',','.').'</th>
						<th>'.number_format($kuantiti_bonus_a,2,',','.').'</th>
						<th>'.number_format($harga_sat_pemb_a,2,',','.').'</th>
						<th>'.number_format($nilai_pemb_a,2,',','.').'</th> 
						<th>'.number_format($retur1_a,2,',','.').'</th>
						<th></th>
						<th>'.number_format($harga_pokok_a,2,',','.').'</th>
						<th>'.number_format($penjualan_a,2,',','.').'</th>
						<th>'.number_format($pengeluaran_a,2,',','.').'</th>
						<th>'.number_format($retur2_a,2,',','.').'</th>
						<th>'.number_format($saldo_akhir_a,2,',','.').'</th>
						<th>'.number_format($nilai_akhir_a,2,',','.').'</th>
					</tr>';
		
		$results['html'] = $html;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($results));
		//print_r($result);die;
		
		
	}
	
	
	
	public function export_excel(){
		
		
		
		$data = array(
			'principal' => $this->Anti_sql_injection($this->input->post('name')),
			'tgl_awal' => $this->Anti_sql_injection($this->input->post('tgl_awal')),
			'tgl_akhir' => $this->Anti_sql_injection($this->input->post('tgl_akhir')),
			
		);
		
		$params_awalss['principal']		= $data['principal'];
				$params_awalss['tgl_akhir']	= date("Y-m-d", strtotime($data['tgl_awal'] ."-1 days"));
				$params_awalss['tgl_awal']	= date_format(date_create($params_awalss['tgl_akhir']),"Y-m-01");
		
		
		//	$result = $this->return_model->get_report_pembelian_export($data);
			//$result = $this->return_model->get_report_persediaan($data);
		
		
		
		
		//print_r($result);die;
		
		$this->load->library('excel');
	   
		$objPHPExcel = new PHPExcel();
		
		$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'PT.ENDIRA ALDA')
            ->setCellValue('A2', 'JL.SANGKURIANG NO.38-A')
            ->setCellValue('A3', 'BANDUNG');
			
			
		$objPHPExcel->setActiveSheetIndex(0)
            ->mergeCells('A5:A6')
            ->setCellValue('A5', 'KODE PRODUK')
             ->mergeCells('B5:B6')
            ->setCellValue('B5', 'NAMA BARANG')
            ->mergeCells('C5:I5')
            ->setCellValue('C5', 'SALDO AWAL')
            ->setCellValue('C6', 'KEMASAN')
            ->setCellValue('D6', 'SATUAN')
            ->setCellValue('E6', 'x')
            ->setCellValue('F6', 'ISI')
            ->setCellValue('G6', 'KUANT.')
            ->setCellValue('H6', 'HRG. SAT.')
            ->setCellValue('I6', 'NILAI');
			
			$objPHPExcel->setActiveSheetIndex(0)
            ->mergeCells('J5:P5')
            ->setCellValue('J5', 'PENERIMAAN')
            ->setCellValue('J6', 'PEMBELIAN')
            ->setCellValue('K6', 'DISC.PROD')
            ->setCellValue('L6', 'LAIN2')
            ->setCellValue('M6', 'BONUS')
            ->setCellValue('N6', 'HRG. SAT.')
            ->setCellValue('O6', 'NILAI')
            ->setCellValue('P6', 'RETUR')
            ->setCellValue('Q6', 'KETERANGAN')
			->mergeCells('R5:R6')
            ->setCellValue('S5', 'HARGA POKOK')
			->mergeCells('S5:T5')
            ->setCellValue('S5', 'PENGELUARAN')
            ->setCellValue('S6', 'PENJUALAN')
            ->setCellValue('T6', 'LAIN2')
            ->setCellValue('U6', 'RETUR')
			->mergeCells('V5:W6')
            ->setCellValue('V5', 'SALDO AKHIR')
			->setCellValue('V6', 'KUANT.')
            ->setCellValue('W6', 'NILAI');
		
$kuantiti_a = 0;
		$harga_satuan_a = 0;
		$nilai_a = 0;
		$pembelian_a = 0;
		$kuantiti_sol_a = 0;
		$kuantiti_titip_a = 0;
		$kuantiti_bonus_a = 0;
		$kuantiti_diskon_a = 0;
		$harga_sat_pemb_a = 0;
		$nilai_pemb_a = 0;
		$harga_pokok_a = 0;
		$penjualan_a = 0;
		$pengeluaran_a = 0;
		$saldo_akhir_a = 0;
		$nilai_akhir_a = 0;
		$retur1_a = 0;
		$retur2_a = 0;
		$rt = 7;
		
		if($data['principal'] == 0){
			
			$dist_a = $this->return_model->get_distribution($data);
			$html = '';
			
			
			foreach($dist_a as $dist_as){
			
				$datat = array(
					'principal' => $dist_as['dist_id'],
					'tgl_awal' => $this->Anti_sql_injection($this->input->post('tgl_awal', TRUE)),
					'tgl_akhir' => $this->Anti_sql_injection($this->input->post('tgl_akhir', TRUE))
					
				);
				
				
			
			$params_awalss['principal']		= $dist_as['dist_id'];
				$params_awalss['tgl_akhir']	= date("Y-m-d", strtotime($data['tgl_awal'] ."-1 days"));
				$params_awalss['tgl_awal']	= date_format(date_create($params_awalss['tgl_akhir']),"Y-m-01");
				
			//	print_r($params_awalss );die;
				
			if($datat['tgl_awal'] == '2021-01-01' ){
				$result = $this->return_model->get_report_persediaan($datat);
				$result_awal = $this->return_model->get_report_persediaan($datat);
			}else{
				$result_awal = $this->return_model->get_report_persediaan_newest($params_awalss);
				$result = $this->return_model->get_report_persediaan_newest($datat);
			}
			
				
				
				$kuantiti = 0;
				$harga_satuan = 0;
				$nilai = 0;
				$pembelian = 0;
				$kuantiti_sol = 0;
				$kuantiti_titip = 0;
				$kuantiti_bonus = 0;
				$kuantiti_diskon = 0;
				$harga_sat_pemb = 0;
				$nilai_pemb = 0;
				$harga_pokok = 0;
				$penjualan = 0;
				$pengeluaran = 0;
				$saldo_akhir = 0;
				$nilai_akhir = 0;
				$prin = '';
				$retur1 = 0;
				$retur2 = 0;
				$kode_prin = '';
				$int_iuy = 0;
				foreach( $result as $datas ){
					
					if($datat['tgl_awal'] == '2021-01-01' ){
						$datas['harga_satuan'] = $datas['harga_satuan'];
					}else{
						$datas['harga_satuan'] = $result_awal[$int_iuy]['harga_pokok'];
					}
					
							$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$rt, $datas['stock_code'])
					->setCellValue('B'.$rt, $datas['stock_name'])
					->setCellValue('C'.$rt, $datas['base_qty'])
					->setCellValue('D'.$rt, $datas['uom_symbol'])
					->setCellValue('E'.$rt, ' x ')
					->setCellValue('F'.$rt, $datas['unit_box'])
					->setCellValue('G'.$rt, number_format($datas['saldo_awals'],2,'.',''))
					->setCellValue('H'.$rt, number_format($datas['harga_satuan'],2,'.',''))
					->setCellValue('I'.$rt, number_format($datas['saldo_awals']*$datas['harga_satuan'],2,'.',''))
					->setCellValue('J'.$rt, number_format($datas['pembelian'],2,'.',''))
					->setCellValue('K'.$rt, number_format($datas['kuantiti_diskon'],2,'.',''))
					->setCellValue('L'.$rt, number_format($datas['kuantiti_titip'],2,'.',''))
					->setCellValue('M'.$rt, number_format($datas['kuantiti_bonus'],2,'.',''))
					->setCellValue('N'.$rt, number_format($datas['harga_sat_pemb'],2,'.',''))
					->setCellValue('O'.$rt, number_format($datas['nilai_pemb'],2,'.',''))
					->setCellValue('P'.$rt, number_format($datas['qty_retur_jual'],2,'.',''))
					->setCellValue('Q'.$rt, $datas['ket'])
					->setCellValue('R'.$rt, number_format($datas['harga_pokok'],2,'.',''))
					->setCellValue('S'.$rt, number_format($datas['penjualan'],2,'.',''))
					->setCellValue('T'.$rt, number_format($datas['pengeluaran'],2,'.',''))
					->setCellValue('U'.$rt, number_format(0,2,'.',''))
					->setCellValue('V'.$rt, number_format($datas['saldo_akhir'],2,'.',''))
					->setCellValue('W'.$rt, number_format($datas['saldo_akhir']*$datas['harga_pokok'],2,'.',''));
					
					
					$html .= '<tr>
								<th >'.$datas['stock_code'].'</th>
								<th>'.$datas['stock_name'].'</th>
								<th>'.$datas['base_qty'].' '.$datas['uom_symbol'].' x '.$datas['unit_box'].'</th>
								<th>'.number_format($datas['saldo_awals'],2,',','.').'</th>
								<th>'.number_format($datas['harga_satuan'],2,',','.').'</th>
								<th>'.number_format($datas['saldo_awals']*$datas['harga_satuan'],2,',','.').'</th>
								<th>'.number_format($datas['pembelian'],2,',','.').'</th>
								<th>'.number_format($datas['kuantiti_diskon'],2,',','.').'</th>
								<th>'.number_format($datas['kuantiti_titip'],2,',','.').'</th>
								<th>'.number_format($datas['kuantiti_bonus'],2,',','.').'</th>
								<th>'.number_format($datas['harga_sat_pemb'],2,',','.').'</th>
								<th>'.number_format($datas['nilai_pemb'],2,',','.').'</th>
								<th>'.$datas['ket'].'</th>
								<th>'.number_format($datas['harga_pokok'],2,',','.').'</th>
								<th>'.number_format($datas['penjualan'],2,',','.').'</th>
								<th>'.number_format($datas['pengeluaran'],2,',','.').'</th>
								<th>'.number_format($datas['saldo_akhir'],2,',','.').'</th>
								<th>'.number_format($datas['saldo_akhir']*$datas['harga_pokok'],2,',','.').'</th>
							</tr>';
					
					$prin = $datas['name_eksternal'];
					$kode_prin = substr($datas['stock_code'],0,3);
					$kuantiti = $kuantiti + $datas['saldo_awals'];
					$harga_satuan = $harga_satuan+ $datas['harga_satuan'];
					$nilai = $nilai+ $datas['saldo_awals']*$datas['harga_satuan'];
					$pembelian = $pembelian+ $datas['pembelian'];
					$kuantiti_sol = $kuantiti_sol+ $datas['kuantiti_sol'];
					$kuantiti_titip = $kuantiti_titip+ $datas['kuantiti_titip'];
					$kuantiti_bonus = $kuantiti_bonus+ $datas['kuantiti_bonus'];
					$kuantiti_diskon = $kuantiti_diskon+ $datas['kuantiti_diskon'];
					$harga_sat_pemb = $harga_sat_pemb+ $datas['harga_sat_pemb'];
					$nilai_pemb = $nilai_pemb+ $datas['nilai_pemb'];
					$harga_pokok =$harga_pokok+ $datas['harga_pokok'];
					$penjualan = $penjualan+ $datas['penjualan'];
					$pengeluaran = $pengeluaran+ $datas['pengeluaran'];
					$saldo_akhir = $saldo_akhir+ $datas['saldo_akhir'];
					$retur1 = $retur1 + $datas['qty_retur_jual'];
					$retur2 = $retur1 + 0;
					$nilai_akhir = $nilai_akhir+ $datas['saldo_akhir']*$datas['harga_pokok'];
					
					$kuantiti_a = $kuantiti_a + $datas['saldo_awals'];
					$harga_satuan_a = $harga_satuan_a +  $datas['harga_satuan'];
					$nilai_a = $nilai_a + $datas['saldo_awals']*$datas['harga_satuan'];
					$pembelian_a = $pembelian_a +  $datas['pembelian'];
					$kuantiti_sol_a = $kuantiti_sol_a +  $datas['kuantiti_sol'];
					$kuantiti_titip_a = $kuantiti_titip_a +  $datas['kuantiti_titip'];
					$kuantiti_bonus_a = $kuantiti_bonus_a +  $datas['kuantiti_bonus'];
					$kuantiti_diskon_a = $kuantiti_diskon_a +  $datas['kuantiti_diskon'];
					$harga_sat_pemb_a = $harga_sat_pemb_a +  $datas['harga_sat_pemb'];
					$nilai_pemb_a = $nilai_pemb_a +  $datas['nilai_pemb'];
					$harga_pokok_a = $harga_pokok_a + $datas['harga_pokok'];
					$penjualan_a = $penjualan_a +  $datas['penjualan'];
					$pengeluaran_a = $pengeluaran_a +  $datas['pengeluaran'];
					$saldo_akhir_a = $saldo_akhir_a +  $datas['saldo_akhir'];
					$nilai_akhir_a = $nilai_akhir_a +  $datas['saldo_akhir']*$datas['harga_pokok'];
					$retur1_a = $retur1 + $datas['qty_retur_jual'];
					$retur2_a = $retur1 + 0;
					$rt++;
					
					$int_iuy++;
				}
			
				$html .= '<tr style="background:yellow">
							<th>'.$kode_prin.'</th>
							<th>'.$prin.'</th>
							<th></th>
							<th>'.number_format($kuantiti,2,',','.').'</th>
							<th>'.number_format($harga_satuan,2,',','.').'</th>
							<th>'.number_format($nilai,2,',','.').'</th>
							<th>'.number_format($pembelian,2,',','.').'</th>
							<th>'.number_format($kuantiti_diskon,2,',','.').'</th>
							<th>'.number_format($kuantiti_titip,2,',','.').'</th>
							<th>'.number_format($kuantiti_bonus,2,',','.').'</th>
							<th>'.number_format($harga_sat_pemb,2,',','.').'</th>
							<th>'.number_format($nilai_pemb,2,',','.').'</th>
							<th>'.number_format($retur1,2,',','.').'</th>
							<th></th>
							<th>'.number_format($harga_pokok,2,',','.').'</th>
							<th>'.number_format($penjualan,2,',','.').'</th>
							<th>'.number_format($pengeluaran,2,',','.').'</th>
							<th>'.number_format(0,2,',','.').'</th>
							<th>'.number_format($saldo_akhir,2,',','.').'</th>
							<th>'.number_format($nilai_akhir,2,',','.').'</th>
						</tr>';
						
						$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$rt, $kode_prin)
					->mergeCells('B'.$rt.':G'.$rt)
					->setCellValue('B'.$rt, $prin)
					->setCellValue('D'.$rt, $datas['uom_symbol'])
					->setCellValue('E'.$rt, ' x ')
					->setCellValue('F'.$rt, $datas['unit_box'])
					->setCellValue('G'.$rt, number_format($kuantiti,2,'.',''))
					->setCellValue('H'.$rt, number_format($harga_satuan,2,'.',''))
					->setCellValue('I'.$rt, number_format($nilai,2,'.',''))
					->setCellValue('J'.$rt, number_format($pembelian,2,'.',''))
					->setCellValue('K'.$rt, number_format($kuantiti_diskon,2,'.',''))
					->setCellValue('L'.$rt, number_format($kuantiti_titip,2,'.',''))
					->setCellValue('M'.$rt, number_format($kuantiti_bonus,2,'.',''))
					->setCellValue('N'.$rt, number_format($harga_sat_pemb,2,'.',''))
					->setCellValue('O'.$rt, number_format($nilai_pemb,2,'.',''))
					->setCellValue('P'.$rt, number_format($retur1,2,'.',''))
					->setCellValue('Q'.$rt, '')
					->setCellValue('R'.$rt, number_format($harga_pokok,2,'.',''))
					->setCellValue('S'.$rt, number_format($penjualan,2,'.',''))
					->setCellValue('T'.$rt, number_format($pengeluaran,2,'.',''))
					->setCellValue('U'.$rt, number_format($retur2,2,'.',''))
					->setCellValue('V'.$rt, number_format($saldo_akhir,2,'.',''))
					->setCellValue('W'.$rt, number_format($nilai_akhir,2,'.',''));
					
					$rt++;
			}
				}else{
				
				$datat = array(
					'principal' => $data['principal'],
					'tgl_awal' => $this->Anti_sql_injection($this->input->post('tgl_awal', TRUE)),
					'tgl_akhir' => $this->Anti_sql_injection($this->input->post('tgl_akhir', TRUE))
					
				);
		
		
			if($datat['tgl_awal'] == '2021-01-01' ){
				$result = $this->return_model->get_report_persediaan($datat);
				$result_awal = $this->return_model->get_report_persediaan($datat);
			}else{
				$result_awal = $this->return_model->get_report_persediaan($datat);
				//$result_awal = $this->return_model->get_report_persediaan_newest($params_awalss);
				$result = $this->return_model->get_report_persediaan_newest($datat);
			}
				
				$html = '';
				
				$kuantiti = 0;
				$harga_satuan = 0;
				$nilai = 0;
				$pembelian = 0;
				$kuantiti_sol = 0;
				$kuantiti_titip = 0;
				$kuantiti_bonus = 0;
				$kuantiti_diskon = 0;
				$harga_sat_pemb = 0;
				$nilai_pemb = 0;
				$harga_pokok = 0;
				$penjualan = 0;
				$pengeluaran = 0;
				$saldo_akhir = 0;
				$nilai_akhir = 0;
				$prin = '';
					$retur1 = 0;
				$retur2 = 0;
				$kode_prin = '';
				
				$int_iuy = 0;
				foreach( $result as $datas ){
					
					if($datat['tgl_awal'] == '2021-01-01' ){
						$datas['harga_satuan'] = $datas['harga_satuan'];
					}else{
						$datas['harga_satuan'] = $result_awal[$int_iuy]['harga_pokok'];
					}
					
							$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$rt, $datas['stock_code'])
					->setCellValue('B'.$rt, $datas['stock_name'])
					->setCellValue('C'.$rt, $datas['base_qty'])
					->setCellValue('D'.$rt, $datas['uom_symbol'])
					->setCellValue('E'.$rt, ' x ')
					->setCellValue('F'.$rt, $datas['unit_box'])
					->setCellValue('G'.$rt, number_format($datas['saldo_awals'],2,'.',''))
					->setCellValue('H'.$rt, number_format($datas['harga_satuan'],2,'.',''))
					->setCellValue('I'.$rt, number_format($datas['saldo_awals']*$datas['harga_satuan'],2,'.',''))
					->setCellValue('J'.$rt, number_format($datas['pembelian'],2,'.',''))
					->setCellValue('K'.$rt, number_format($datas['kuantiti_diskon'],2,'.',''))
					->setCellValue('L'.$rt, number_format($datas['kuantiti_titip'],2,'.',''))
					->setCellValue('M'.$rt, number_format($datas['kuantiti_bonus'],2,'.',''))
					->setCellValue('N'.$rt, number_format($datas['harga_sat_pemb'],2,'.',''))
					->setCellValue('O'.$rt, number_format($datas['nilai_pemb'],2,'.',''))
					->setCellValue('P'.$rt, number_format($datas['qty_retur_jual'],2,'.',''))
					->setCellValue('Q'.$rt, $datas['ket'])
					->setCellValue('R'.$rt, number_format($datas['harga_pokok'],2,'.',''))
					->setCellValue('S'.$rt, number_format($datas['penjualan'],2,'.',''))
					->setCellValue('T'.$rt, number_format($datas['pengeluaran'],2,'.',''))
					->setCellValue('U'.$rt, number_format(0,2,'.',''))
					->setCellValue('V'.$rt, number_format($datas['saldo_akhir'],2,'.',''))
					->setCellValue('W'.$rt, number_format($datas['saldo_akhir']*$datas['harga_pokok'],2,'.',''));
					
					$html .= '<tr>
								<th>'.$datas['stock_code'].'</th>
								<th>'.$datas['stock_name'].'</th>
								<th>'.$datas['base_qty'].' '.$datas['uom_symbol'].' x '.$datas['unit_box'].'</th>
								<th>'.number_format($datas['saldo_awals'],2,',','.').'</th>
								<th>'.number_format($datas['harga_satuan'],2,',','.').'</th>
								<th>'.number_format($datas['saldo_awals']*$datas['harga_satuan'],2,',','.').'</th>
								<th>'.number_format($datas['pembelian'],2,',','.').'</th>
								<th>'.number_format($datas['kuantiti_diskon'],2,',','.').'</th>
								<th>'.number_format($datas['kuantiti_titip'],2,',','.').'</th>
								<th>'.number_format($datas['kuantiti_bonus'],2,',','.').'</th>
								<th>'.number_format($datas['harga_sat_pemb'],2,',','.').'</th>
								<th>'.number_format($datas['nilai_pemb'],2,',','.').'</th>
								<th>'.$datas['ket'].'</th>
								<th>'.number_format($datas['harga_pokok'],2,',','.').'</th>
								<th>'.number_format($datas['penjualan'],2,',','.').'</th>
								<th>'.number_format($datas['pengeluaran'],2,',','.').'</th>
								<th>'.number_format($datas['saldo_akhir'],2,',','.').'</th>
								<th>'.number_format($datas['saldo_akhir']*$datas['harga_pokok'],2,',','.').'</th>
							</tr>';
					
					$prin = $datas['name_eksternal'];
					$kode_prin = substr($datas['stock_code'],0,3);
					$kuantiti = $kuantiti + $datas['saldo_awals'];
					$harga_satuan = $harga_satuan+ $datas['harga_satuan'];
					$nilai = $nilai+ $datas['saldo_awals']*$datas['harga_satuan'];
					$pembelian = $pembelian+ $datas['pembelian'];
					$kuantiti_sol = $kuantiti_sol+ $datas['kuantiti_sol'];
					$kuantiti_titip = $kuantiti_titip+ $datas['kuantiti_titip'];
					$kuantiti_bonus = $kuantiti_bonus+ $datas['kuantiti_bonus'];
					$kuantiti_diskon = $kuantiti_diskon+ $datas['kuantiti_diskon'];
					$harga_sat_pemb = $harga_sat_pemb+ $datas['harga_sat_pemb'];
					$nilai_pemb = $nilai_pemb+ $datas['nilai_pemb'];
					$harga_pokok =$harga_pokok+ $datas['harga_pokok'];
					$penjualan = $penjualan+ $datas['penjualan'];
					$pengeluaran = $pengeluaran+ $datas['pengeluaran'];
					$saldo_akhir = $saldo_akhir+ $datas['saldo_akhir'];
						$retur1 = $retur1 + $datas['qty_retur_jual'];
					$retur2 = $retur1 + 0;
					$nilai_akhir = $nilai_akhir+ $datas['saldo_akhir']*$datas['harga_pokok'];
					
					$kuantiti_a = $kuantiti_a + $datas['saldo_awals'];
					$harga_satuan_a = $harga_satuan_a +  $datas['harga_satuan'];
					$nilai_a = $nilai_a +  $datas['nilai'];
					$pembelian_a = $pembelian_a +  $datas['pembelian'];
					$kuantiti_sol_a = $kuantiti_sol_a +  $datas['kuantiti_sol'];
					$kuantiti_titip_a = $kuantiti_titip_a +  $datas['kuantiti_titip'];
					$kuantiti_bonus_a = $kuantiti_bonus_a +  $datas['kuantiti_bonus'];
					$kuantiti_diskon_a = $kuantiti_diskon_a +  $datas['kuantiti_diskon'];
					$harga_sat_pemb_a = $harga_sat_pemb_a +  $datas['harga_sat_pemb'];
					$nilai_pemb_a = $nilai_pemb_a +  $datas['nilai_pemb'];
					$harga_pokok_a = $harga_pokok_a + $datas['harga_pokok'];
					$penjualan_a = $penjualan_a +  $datas['penjualan'];
					$pengeluaran_a = $pengeluaran_a +  $datas['pengeluaran'];
					$saldo_akhir_a = $saldo_akhir_a +  $datas['saldo_akhir'];
					$nilai_akhir_a = $nilai_akhir_a +  $datas['saldo_akhir']*$datas['harga_pokok'];
					$retur1_a = $retur1 + $datas['qty_retur_jual'];
					$retur2_a = $retur1 + 0;
					$rt++;
					
					$int_iuy++;
				}
				
						$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$rt, $kode_prin)
					->mergeCells('B'.$rt.':G'.$rt)
					->setCellValue('B'.$rt, $prin)
					->setCellValue('D'.$rt, $datas['uom_symbol'])
					->setCellValue('E'.$rt, ' x ')
					->setCellValue('F'.$rt, $datas['unit_box'])
					->setCellValue('G'.$rt, number_format($kuantiti,2,'.',''))
					->setCellValue('H'.$rt, number_format($harga_satuan,2,'.',''))
					->setCellValue('I'.$rt, number_format($nilai,2,'.',''))
					->setCellValue('J'.$rt, number_format($pembelian,2,'.',''))
					->setCellValue('K'.$rt, number_format($kuantiti_diskon,2,'.',''))
					->setCellValue('L'.$rt, number_format($kuantiti_titip,2,'.',''))
					->setCellValue('M'.$rt, number_format($kuantiti_bonus,2,'.',''))
					->setCellValue('N'.$rt, number_format($harga_sat_pemb,2,'.',''))
					->setCellValue('O'.$rt, number_format($nilai_pemb,2,'.',''))
					->setCellValue('P'.$rt, number_format($retur1,2,'.',''))
					->setCellValue('Q'.$rt, '')
					->setCellValue('R'.$rt, number_format($harga_pokok,2,'.',''))
					->setCellValue('S'.$rt, number_format($penjualan,2,'.',''))
					->setCellValue('T'.$rt, number_format($pengeluaran,2,'.',''))
					->setCellValue('U'.$rt, number_format($retur2,2,'.',''))
					->setCellValue('V'.$rt, number_format($saldo_akhir,2,'.',''))
					->setCellValue('W'.$rt, number_format($nilai_akhir,2,'.',''));
					//$rt++;
					
					$html .= '<tr style="background:yellow">
								<th>'.$kode_prin.'</th>
								<th colspan="2">'.$prin.'</th>
								<th>'.number_format($kuantiti,2,',','.').'</th>
								<th>'.number_format($harga_satuan,2,',','.').'</th>
								<th>'.number_format($nilai,2,',','.').'</th>
								<th>'.number_format($pembelian,2,',','.').'</th>
								<th>'.number_format($kuantiti_diskon,2,',','.').'</th>
								<th>'.number_format($kuantiti_titip,2,',','.').'</th>
								<th>'.number_format($kuantiti_bonus,2,',','.').'</th>
								<th>'.number_format($harga_sat_pemb,2,',','.').'</th>
								<th>'.number_format($nilai_pemb,2,',','.').'</th>
								<th></th>
								<th>'.number_format($harga_pokok,2,',','.').'</th>
								<th>'.number_format($penjualan,2,',','.').'</th>
								<th>'.number_format($pengeluaran,2,',','.').'</th>
								<th>'.number_format($saldo_akhir,2,',','.').'</th>
								<th>'.number_format($nilai_akhir,2,',','.').'</th>
							</tr>';
							
					$rt++;		
							
				
				
				}
		
		$html .= '<tr style="background:green">
						<th colspan="3">Total </th>
						<th>'.number_format($kuantiti_a,2,',','.').'</th>
						<th>'.number_format($harga_satuan_a,2,',','.').'</th>
						<th>'.number_format($nilai_a,2,',','.').'</th>
						<th>'.number_format($pembelian_a,2,',','.').'</th>
						<th>'.number_format($kuantiti_diskon_a,2,',','.').'</th>
						<th>'.number_format($kuantiti_titip_a,2,',','.').'</th>
						<th>'.number_format($kuantiti_bonus_a,2,',','.').'</th>
						<th>'.number_format($harga_sat_pemb_a,2,',','.').'</th>
						<th>'.number_format($nilai_pemb_a,2,',','.').'</th>
						<th></th>
						<th>'.number_format($harga_pokok_a,2,',','.').'</th>
						<th>'.number_format($penjualan_a,2,',','.').'</th>
						<th>'.number_format($pengeluaran_a,2,',','.').'</th>
						<th>'.number_format($saldo_akhir_a,2,',','.').'</th>
						<th>'.number_format($nilai_akhir_a,2,',','.').'</th>
					</tr>';
					
					
					$objPHPExcel->setActiveSheetIndex(0)
					->mergeCells('A'.$rt.':G'.$rt)
					->setCellValue('A'.$rt, ' Total ')
					->setCellValue('G'.$rt, number_format($kuantiti_a,2,'.',''))
					->setCellValue('H'.$rt, number_format($harga_satuan_a,2,'.',''))
					->setCellValue('I'.$rt, number_format($nilai_a,2,'.',''))
					->setCellValue('J'.$rt, number_format($pembelian_a,2,'.',''))
					->setCellValue('K'.$rt, number_format($kuantiti_diskon_a,2,'.',''))
					->setCellValue('L'.$rt, number_format($kuantiti_titip_a,2,'.',''))
					->setCellValue('M'.$rt, number_format($kuantiti_bonus_a,2,'.',''))
					->setCellValue('N'.$rt, number_format($harga_sat_pemb_a,2,'.',''))
					->setCellValue('O'.$rt, number_format($nilai_pemb_a,2,'.',''))
					->setCellValue('P'.$rt, number_format($retur1_a,2,'.',''))
					->setCellValue('Q'.$rt, '')
					->setCellValue('R'.$rt, number_format($harga_pokok_a,2,'.',''))
					->setCellValue('S'.$rt, number_format($penjualan_a,2,'.',''))
					->setCellValue('T'.$rt, number_format($pengeluaran_a,2,'.',''))
					->setCellValue('U'.$rt, number_format($retur2_a,2,'.',''))
					->setCellValue('V'.$rt, number_format($saldo_akhir_a,2,'.',''))
					->setCellValue('W'.$rt, number_format($nilai_akhir_a,2,'.',''));
		
		$results['html'] = $html;
		
		// $rt = 9;
		// $ints = 1;
		// foreach($result as $results){
			
			// $objPHPExcel->setActiveSheetIndex(0)
            // ->setCellValue('A'.$rt, $ints)
            // ->setCellValue('B'.$rt, $results['Tanggal'])
            // ->setCellValue('C'.$rt, $results['Kode'])
            // ->setCellValue('D'.$rt, $results['Nama_barang'])
            // ->setCellValue('E'.$rt, number_format(floatval($results['kemasan']),2,',','.'))
            // ->setCellValue('F'.$rt, number_format(floatval($results['qty']),2,',','.'))
            // ->setCellValue('G'.$rt, number_format(floatval($results['harga']),0,',','.'))
            // ->setCellValue('H'.$rt, number_format(floatval($results['jumlah']),0,',','.'))
            // ->setCellValue('I'.$rt, number_format(floatval($results['PPN']),2,',','.'))
            // ->setCellValue('J'.$rt,number_format(floatval( $results['Rp_PPN']),0,',','.'))
            // ->setCellValue('K'.$rt, $results['No_trans'])
            // ->setCellValue('L'.$rt, $results['no_invoice'])
            // ->setCellValue('M'.$rt, $results['jatuh_tempo'])
            // ->setCellValue('N'.$rt, $results['lama_hari'])
            // ->setCellValue('O'.$rt, $results['faktur_pajak'])
            // ->setCellValue('P'.$rt, $results['tanggal_faktur']);
			
			// $rt++;
			// $ints++;
		// }
           
			 	 					 								  		

			;
// Redirect output to a client’s web browser (Excel2007)
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename=Report_Persediaan.xls');
			header('Cache-Control: max-age=0');
			// If you’re serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');
			// If you’re serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			unset($objPHPExcel);
		
	}
	/**
	  * This function is used for showing distributor list
	  * @return Array
	  */
	function lists() {
		
		 if( !empty($_GET['sess_user_id']) ) {
			  $sess_user_id = $_GET['sess_user_id'];
		  } else {
			  $sess_user_id = NULL;
		  }
		  
		   if( !empty($_GET['sess_token']) ) {
			  $sess_token = $_GET['sess_token'];
		  } else {
			  $sess_token = NULL;
		  }
		  
		    if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		  if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		  if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 				
		  $order_fields = array('kode_eksternal'); // , 'COST'
		  $order = $this->input->get_post('order');
		  if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'desc';}; 
		  if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 	
		  
		    $params['limit'] 		= (int) $length;
			$params['offset'] 		= (int) $start;
			$params['order_column'] = $order_fields[$order_column];
			$params['order_dir'] 	= $order_dir;
			$params['sess_user_id'] = $sess_user_id;
			$params['sess_token'] 	= $sess_token;
			$params['searchtxt'] 	= $_GET['search']['value'];
		  
		 // print_r($params);die;
		  
			$list = $this->return_model->list_penjualan($params); 
			//print_r($list['data']);die;
			  $data = array();	
				   foreach ( $list['data'] as $k => $v ) {

					if($v['status_retur']==0){
						$sts = '<button type="button" class="btn btn-custon-rounded-two btn-danger" onClick="konfirmasi('. $v['id_retur'].')"  > Konfirmasi </button>';
						$action = '<button type="button" class="btn btn-custon-rounded-two btn-default" onClick="updatepo('. $v['id_retur'].')" > Edit </button>
						<button type="button" class="btn btn-custon-rounded-two btn-danger" onClick="deletepo('. $v['id_retur'].')" > Hapus </button>
						<button type="button" class="btn btn-custon-rounded-two btn-primary" onClick="detail('. $v['id_retur'].')" > Detail </button>
						';
					}else{
						$sts = '<button type="button" class="btn btn-custon-rounded-two btn-primary" disabled > Disetujui </button>';
						$action = '<button type="button" class="btn btn-custon-rounded-two btn-primary" onClick="detail('. $v['id_retur'].')" > Detail </button>
						';
					}
					
					if($v['term_of_payment'] == '0'){
						$hhh = 'Cash';
					}else{
						$hhh =  $v['term_of_payment'].' Hari';
					}
						
						$date=date_create($v['date_penjualan']);
						$date_due = date_add($date,date_interval_create_from_date_string($v['term_of_payment']." days"));
						$due_date = date_format($date_due,"Y-m-d");;
						
						  
						  array_push($data, 
							  array(
								  number_format($v['Rangking'],0,',','.'),
								  $v['no_penjualan'],
								  $v['cust_name'],
								  $v['date_penjualan'],
								  $v['pic'],
								  $due_date,
								  $sts,
								  $action
							  )
							);
							//$idx++;
				   }
		   
			 $result["data"] = $data;
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		//$result["data"] = $list['data'];
	  
		echo json_encode($result);
	  
				//$this->json_result($result);
		  
	}

	/**
	  * This function is redirect to add distributor page
	  * @return Void
	  */
	  
	 public function get_customer(){ 
		 
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);
		
		$result['customer'] = $this->return_model->get_customer_byid($params);
		
		//print_r($result['customer']);die;
		$item = $this->return_model->get_item_byprin_all();
		
		//print_r($item);die;
		
		$list = '<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		foreach($item as $items){
			
			
			$list .= '<option value="'.$items['id_mat'].'|'.$items['unit_box'].'|'.$items['bottom_price'].'|'.$items['qty'].'" >'.$items['stock_code'].' - '.$items['stock_name'].' - '.$items['base_qty'].' '.$items['uom_symbol'].'</option>';
			
		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';
		
		$result['list'] = $list;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		 
	 }		

	 public function get_penjualan_detail(){ 
		 
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);
		
		$result['penjualan'] = $this->return_model->get_penjualan_byid($params);
		
		//print_r($result['penjualan']);die;
		
		//print_r($result['customer']);die;
		$item = $this->return_model->get_penjualan_detail($params);
		
		
		
		$htl = '';
		$ii = 0;
		foreach($item as $items){
			
			$htl .=	'<div id="row_'.$ii.'" >';
			$htl .=	'									<div class="row" style="border-top-style:solid;">';
			$htl .=	'									<div style="margin-top:10px">';
			$htl .=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
			$htl .=	'												<input name="kode_v'.$ii.'" id="kode_v'.$ii.'" type="text" class="form-control " placeholder="Qty" value="'.$items['stock_name'].' '.$items['base_qty'].' '.$items['uom_symbol'].'"  readOnly >';
			$htl .=	'												<input name="kode_'.$ii.'" id="kode_'.$ii.'" type="hidden" class="form-control " placeholder="Qty" value="'.$items['id_material'].'|'.$items['box_ammount'].'|'.$items['unit_price'].'|'.$items['qty'].'" readOnly  >';
			$htl .=	'												<input name="id_d_'.$ii.'" id="id_d_'.$ii.'" type="hidden" class="form-control " placeholder="Qty" value="'.$items['id_dp'].'" readOnly  >';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_'.$ii.'" id="qty_'.$ii.'" type="text" onKeyup="change_sum('.$ii.')" class="form-control rupiah" placeholder="Qty" value=0  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<label id="iteml_'.$ii.'">Box @ '.$items['box_ammount'].'</label>';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qtys_'.$ii.'" id="qtys_'.$ii.'" type="text" onKeyup="change_sum('.$ii.')" class="form-control rupiah" placeholder="Qty" value='.number_format($items['qty_satuan'],0,',','.').'  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_retur_'.$ii.'" id="qty_retur_'.$ii.'" type="text" onKeyup="change_sum('.$ii.')" class="form-control rupiah" placeholder="Qty" value='.number_format($items['qty_satuan'],0,',','.').' readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="totalqty_'.$ii.'" id="totalqty_'.$ii.'" type="number"  class="form-control" placeholder="Diskon" value='.number_format($items['qty'],0,',','.').' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="harga_'.$ii.'" id="harga_'.$ii.'" type="text" onKeyup="change_sum('.$ii.')" class="form-control rupiah" placeholder="Harga" value='.number_format($items['unit_price'],0,',','.').' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="total_'.$ii.'" id="total_'.$ii.'" type="text" class="form-control rupiah" placeholder="Total" value=0 readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'									</div>';
			$htl .=	'									</div>';
			$htl .=	'								</div>';
			
			//$list .= '<option value="'.$items['id_mat'].'|'.$items['unit_box'].'|'.$items['bottom_price'].'|'.$items['qty'].'" >'.$items['stock_code'].' - '.$items['stock_name'].' - '.$items['base_qty'].' '.$items['uom_symbol'].'</option>';
			$ii++;
		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';
		
		$result['list'] = $htl;
		$result['int'] = $ii;
		//$result['total_amount'] = number_format(floatval($result['penjualan'][0]['total_amount']),0,',','.');
		//$result['ppn'] = number_format(floatval($result['penjualan'][0]['total_amount'])/11,2,',','.');
		//$result['dpp'] = number_format(floatval($result['penjualan'][0]['total_amount'])-floatval($result['ppn']),2,',','.');
		
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		 
	 }	

	 public function get_all_item(){
		 
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);
		
		$item = $this->return_model->get_item_byprin($params);
		
		$list = '<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		foreach($item as $items){
			
			$list .= '<option value="'.$items['id_mat'].'" >'.$items['stock_code'].' - '.$items['stock_name'].' - '.$items['base_qty'].' '.$items['uom_symbol'].'</option>';
			
		}
		$list .= '<option value="0" selected="selected" disabled>-- Pilih Item Lainnya --</option>';
		
		$item2 = $this->return_model->get_item_byprin_all($params);
		
		foreach($item2 as $items){
			
			$list .= '<option value="'.$items['id_mat'].'" >'.$items['stock_code'].' - '.$items['stock_name'].' - '.$items['base_qty'].' '.$items['uom_symbol'].'</option>';
			
		}
	//	$list .= '------------------';
		
		$result['list'] = $list;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		 
	 }	 
	 
	 
	 public function get_price_mat(){
		 
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);
		
		//print_r($params);die;
		
		$ddd = $this->return_model->get_price_mat($params);
		//$item = $this->return_model->get_item_byprin($params);
		
		if(count($ddd) == 0){
			$list = 0;
		}else{
			$list = number_format($ddd[0]['unit_price'],0,',','.');
		}
		//print_r($ddd);die;
		
		$result['list'] = $list;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		 
	 }
	 
	public function add() {
		
		$params['region'] = 4;
		
		$result = $this->return_model->get_customer($params);
		$result_p = $this->return_model->get_penjualan_all();
		
		$result_g = $this->return_model->get_kode();
		if($result_g[0]['max']+1 < 1000){
			if($result_g[0]['max']+1 < 100){
				if($result_g[0]['max']+1 < 10){
					$kode = ''.date('Y').'000'.($result_g[0]['max']+1);
				}else{
					$kode = ''.date('Y').'00'.($result_g[0]['max']+1);
				}
			}else{
				$kode = ''.date('Y').'0'.($result_g[0]['max']+1);
			}
		}else{
			$kode = ''.date('Y').''.($result_g[0]['max']+1);
		}
		//print_r($kode);die;

		$data = array(
			'principal' => $result,
			'kode' => $kode,
			'item' => $result_g,
			'result_p' => $result_p
		);

		$this->template->load('maintemplate', 'report_persediaan/views/addPo',$data);
		//$this->load->view('addPrinciple',$data);
	}	


	public function updatepo() {
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			'id_penjualan' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			
		);
		
		$params['region'] = 4;
		
		$result = $this->return_model->get_customer($params);
		$result_p = $this->return_model->get_penjualan_all();
		$result_retur = $this->return_model->get_retur_id($data);
		
		//print_r($result_retur);die;
		
		$result_g = $this->return_model->get_kode();
		if($result_g[0]['max']+1 < 1000){
			if($result_g[0]['max']+1 < 100){
				if($result_g[0]['max']+1 < 10){
					$kode = ''.date('Y').'000'.($result_g[0]['max']+1);
				}else{
					$kode = ''.date('Y').'00'.($result_g[0]['max']+1);
				}
			}else{
				$kode = ''.date('Y').'0'.($result_g[0]['max']+1);
			}
		}else{
			$kode = ''.date('Y').''.($result_g[0]['max']+1);
		}
		//print_r($kode);die;

		$htl = '';
		$ii = 0;
		
		$item = $this->return_model->get_penjualan_detail_update($data);
		
		//print_r($item);die;
		$total_retur = 0;
		foreach($item as $items){
			
			$htl .=	'<div id="row_'.$ii.'" >';
			$htl .=	'									<div class="row" style="border-top-style:solid;">';
			$htl .=	'									<div style="margin-top:10px">';
			$htl .=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
			$htl .=	'												<input name="kode_v'.$ii.'" id="kode_v'.$ii.'" type="text" class="form-control " placeholder="Qty" value="'.$items['stock_name'].' '.$items['base_qty'].' '.$items['uom_symbol'].'"  readOnly >';
			$htl .=	'												<input name="kode_'.$ii.'" id="kode_'.$ii.'" type="hidden" class="form-control " placeholder="Qty" value="'.$items['id_material'].'|'.$items['box_ammount'].'|'.$items['unit_price'].'|'.$items['qty'].'" readOnly  >';
			$htl .=	'												<input name="id_d_'.$ii.'" id="id_d_'.$ii.'" type="hidden" class="form-control " placeholder="Qty" value="'.$items['id_dp'].'" readOnly  >';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_'.$ii.'" id="qty_'.$ii.'" type="text" onKeyup="change_sum('.$ii.')" class="form-control rupiah" placeholder="Qty" value='.$items['qty_box_retur'].'  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<label id="iteml_'.$ii.'">Box @ '.$items['box_ammount'].'</label>';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qtys_'.$ii.'" id="qtys_'.$ii.'" type="text" onKeyup="change_sum('.$ii.')" class="form-control rupiah" placeholder="Qty" value='.number_format($items['qty_retur_sat'],0,',','.').'  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_retur_'.$ii.'" id="qty_retur_'.$ii.'" type="text" onKeyup="change_sum('.$ii.')" class="form-control rupiah" placeholder="Qty" value='.number_format($items['qty_retur'],0,',','.').' readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="totalqty_'.$ii.'" id="totalqty_'.$ii.'" type="number"  class="form-control" placeholder="Diskon" value='.number_format($items['qty'],0,',','.').' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="harga_'.$ii.'" id="harga_'.$ii.'" type="text" onKeyup="change_sum('.$ii.')" class="form-control rupiah" placeholder="Harga" value='.number_format($items['unit_price'],0,',','.').' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="total_'.$ii.'" id="total_'.$ii.'" type="text" class="form-control rupiah" placeholder="Total" value='.number_format($items['amount'],0,',','.').' readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'									</div>';
			$htl .=	'									</div>';
			$htl .=	'								</div>';
			
			$total_retur += $items['amount'];
			//$list .= '<option value="'.$items['id_mat'].'|'.$items['unit_box'].'|'.$items['bottom_price'].'|'.$items['qty'].'" >'.$items['stock_code'].' - '.$items['stock_name'].' - '.$items['base_qty'].' '.$items['uom_symbol'].'</option>';
			$ii++;
		}

		$data = array(
			'principal' => $result,
			'kode' => $kode,
			'item' => $result_g,
			'retur' => $result_retur,
			'int_flo' => $ii,
			'result_p' => $result_p,
			'total_retur' => number_format($total_retur,0,',','.'),
			'item_form' => $htl,
			'id_retur' => $data['id']
		);

		$this->template->load('maintemplate', 'report_persediaan/views/updatePo',$data);
		//$this->load->view('addPrinciple',$data);
	}		
	
	public function detail() {
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			'id_penjualan' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			
		);
		
		$params['region'] = 4;
		
		$result = $this->return_model->get_customer($params);
		$result_p = $this->return_model->get_penjualan_all();
		$result_retur = $this->return_model->get_retur_id($data);
		
		//print_r($result_retur);die;
		
		$result_g = $this->return_model->get_kode();
		if($result_g[0]['max']+1 < 1000){
			if($result_g[0]['max']+1 < 100){
				if($result_g[0]['max']+1 < 10){
					$kode = ''.date('Y').'000'.($result_g[0]['max']+1);
				}else{
					$kode = ''.date('Y').'00'.($result_g[0]['max']+1);
				}
			}else{
				$kode = ''.date('Y').'0'.($result_g[0]['max']+1);
			}
		}else{
			$kode = ''.date('Y').''.($result_g[0]['max']+1);
		}
		//print_r($kode);die;

		$htl = '';
		$ii = 0;
		
		$item = $this->return_model->get_penjualan_detail_update($data);
		
		//print_r($item);die;
		$total_retur = 0;
		foreach($item as $items){
			
			$htl .=	'<div id="row_'.$ii.'" >';
			$htl .=	'									<div class="row" style="border-top-style:solid;">';
			$htl .=	'									<div style="margin-top:10px">';
			$htl .=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
			$htl .=	'												<input name="kode_v'.$ii.'" id="kode_v'.$ii.'" type="text" class="form-control " placeholder="Qty" value="'.$items['stock_name'].' '.$items['base_qty'].' '.$items['uom_symbol'].'"  readOnly >';
			$htl .=	'												<input name="kode_'.$ii.'" id="kode_'.$ii.'" type="hidden" class="form-control " placeholder="Qty" value="'.$items['id_material'].'|'.$items['box_ammount'].'|'.$items['unit_price'].'|'.$items['qty'].'" readOnly  >';
			$htl .=	'												<input name="id_d_'.$ii.'" id="id_d_'.$ii.'" type="hidden" class="form-control " placeholder="Qty" value="'.$items['id_dp'].'" readOnly  >';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_'.$ii.'" id="qty_'.$ii.'" type="text" onKeyup="change_sum('.$ii.')" class="form-control rupiah" placeholder="Qty" value='.$items['qty_box_retur'].' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<label id="iteml_'.$ii.'">Box @ '.$items['box_ammount'].'</label>';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qtys_'.$ii.'" id="qtys_'.$ii.'" type="text" onKeyup="change_sum('.$ii.')" class="form-control rupiah" placeholder="Qty" value='.number_format($items['qty_retur_sat'],0,',','.').' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_retur_'.$ii.'" id="qty_retur_'.$ii.'" type="text" onKeyup="change_sum('.$ii.')" class="form-control rupiah" placeholder="Qty" value='.number_format($items['qty_retur'],0,',','.').' readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="totalqty_'.$ii.'" id="totalqty_'.$ii.'" type="number"  class="form-control" placeholder="Diskon" value='.number_format($items['qty'],0,',','.').' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="harga_'.$ii.'" id="harga_'.$ii.'" type="text" onKeyup="change_sum('.$ii.')" class="form-control rupiah" placeholder="Harga" value='.number_format($items['unit_price'],0,',','.').' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="total_'.$ii.'" id="total_'.$ii.'" type="text" class="form-control rupiah" placeholder="Total" value='.number_format($items['amount'],0,',','.').' readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'									</div>';
			$htl .=	'									</div>';
			$htl .=	'								</div>';
			
			$total_retur += $items['amount'];
			//$list .= '<option value="'.$items['id_mat'].'|'.$items['unit_box'].'|'.$items['bottom_price'].'|'.$items['qty'].'" >'.$items['stock_code'].' - '.$items['stock_name'].' - '.$items['base_qty'].' '.$items['uom_symbol'].'</option>';
			$ii++;
		}

		$data = array(
			'principal' => $result,
			'kode' => $kode,
			'item' => $result_g,
			'retur' => $result_retur,
			'int_flo' => $ii,
			'result_p' => $result_p,
			'total_retur' => number_format($total_retur,0,',','.'),
			'item_form' => $htl,
			'id_retur' => $data['id']
		);

		$this->template->load('maintemplate', 'report_persediaan/views/detailPo',$data);
		//$this->load->view('addPrinciple',$data);
	}	
	
	public function konfirmasi() {
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			'id_penjualan' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			
		);
		
		$params['region'] = 4;
		
		$result = $this->return_model->get_customer($params);
		$result_p = $this->return_model->get_penjualan_all();
		$result_retur = $this->return_model->get_retur_id($data);
		
		//print_r($result_retur);die;
		
		$result_g = $this->return_model->get_kode();
		if($result_g[0]['max']+1 < 1000){
			if($result_g[0]['max']+1 < 100){
				if($result_g[0]['max']+1 < 10){
					$kode = ''.date('Y').'000'.($result_g[0]['max']+1);
				}else{
					$kode = ''.date('Y').'00'.($result_g[0]['max']+1);
				}
			}else{
				$kode = ''.date('Y').'0'.($result_g[0]['max']+1);
			}
		}else{
			$kode = ''.date('Y').''.($result_g[0]['max']+1);
		}
		//print_r($kode);die;

		$htl = '';
		$ii = 0;
		
		$item = $this->return_model->get_penjualan_detail_update($data);
		
		//print_r($item);die;
		$total_retur = 0;
		foreach($item as $items){
			
			$htl .=	'<div id="row_'.$ii.'" >';
			$htl .=	'									<div class="row" style="border-top-style:solid;">';
			$htl .=	'									<div style="margin-top:10px">';
			$htl .=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
			$htl .=	'												<input name="kode_v'.$ii.'" id="kode_v'.$ii.'" type="text" class="form-control " placeholder="Qty" value="'.$items['stock_name'].' '.$items['base_qty'].' '.$items['uom_symbol'].'"  readOnly >';
			$htl .=	'												<input name="kode_'.$ii.'" id="kode_'.$ii.'" type="hidden" class="form-control " placeholder="Qty" value="'.$items['id_material'].'|'.$items['box_ammount'].'|'.$items['unit_price'].'|'.$items['qty'].'" readOnly  >';
			$htl .=	'												<input name="id_d_'.$ii.'" id="id_d_'.$ii.'" type="hidden" class="form-control " placeholder="Qty" value="'.$items['id_dp'].'" readOnly  >';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_'.$ii.'" id="qty_'.$ii.'" type="text" onKeyup="change_sum('.$ii.')" class="form-control rupiah" placeholder="Qty" value='.$items['qty_box_retur'].' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<label id="iteml_'.$ii.'">Box @ '.$items['box_ammount'].'</label>';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qtys_'.$ii.'" id="qtys_'.$ii.'" type="text" onKeyup="change_sum('.$ii.')" class="form-control rupiah" placeholder="Qty" value='.number_format($items['qty_retur_sat'],0,',','.').' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_retur_'.$ii.'" id="qty_retur_'.$ii.'" type="text" onKeyup="change_sum('.$ii.')" class="form-control rupiah" placeholder="Qty" value='.number_format($items['qty_retur'],0,',','.').' readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="totalqty_'.$ii.'" id="totalqty_'.$ii.'" type="number"  class="form-control" placeholder="Diskon" value='.number_format($items['qty'],0,',','.').' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="harga_'.$ii.'" id="harga_'.$ii.'" type="text" onKeyup="change_sum('.$ii.')" class="form-control rupiah" placeholder="Harga" value='.number_format($items['unit_price'],0,',','.').' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="total_'.$ii.'" id="total_'.$ii.'" type="text" class="form-control rupiah" placeholder="Total" value='.number_format($items['amount'],0,',','.').' readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'									</div>';
			$htl .=	'									</div>';
			$htl .=	'								</div>';
			
			$total_retur += $items['amount'];
			//$list .= '<option value="'.$items['id_mat'].'|'.$items['unit_box'].'|'.$items['bottom_price'].'|'.$items['qty'].'" >'.$items['stock_code'].' - '.$items['stock_name'].' - '.$items['base_qty'].' '.$items['uom_symbol'].'</option>';
			$ii++;
		}

		$data = array(
			'principal' => $result,
			'kode' => $kode,
			'item' => $result_g,
			'retur' => $result_retur,
			'int_flo' => $ii,
			'result_p' => $result_p,
			'total_retur' => number_format($total_retur,0,',','.'),
			'item_form' => $htl,
			'id_retur' => $data['id']
		);

		$this->template->load('maintemplate', 'report_persediaan/views/konfirmasiPo',$data);
		//$this->load->view('addPrinciple',$data);
	
	}		
	
	public function approve_retur(){
		
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);
		
		$data = array(
			'id_po' => $params['id'],
			'id_penjualan' => $params['id'],
			'sts' => $params['sts']
			
		);
		
		//print_r($data);die;
		
		$add_prin_result = $this->return_model->edit_retur_apr($data);
		
		if($params['sts'] == 1){
			//$this->return_model->history_retur_apr($data);
			
			$item = $this->return_model->get_penjualan_detail_update($data);
			
			foreach($item as $items){
				
				//print_r($items);die;
				
				$data_mutasi = array(
					'id_material'  => $items['id_material'],
					'qty'  => $items['qty_retur']
					
				);
				
				$add_mutasi_out = $this->return_model->add_mutasi_out($data_mutasi);
				
				$data_mutasi = array(
					'id_material'  => $items['id_material'],
					'amount_mutasi'  => $items['qty_retur'],
					'id_dp'  => $items['id_dp'],
					'qty_retur'  => floatval($items['qty'])-floatval($items['qty_retur']),
					'qty_box_retur'  => floatval($items['qty_box'])-floatval($items['qty_box_retur']),
					'qty_retur_sat'  => floatval($items['qty_satuan'])-floatval($items['qty_retur_sat']),
					'amount'  => floatval($items['price'])-floatval($items['amount']),
					'mutasi_id'  => $add_mutasi_out['lastid']
					
				);
				
				$this->return_model->edit_detail_penjualan($data_mutasi);
				
				
			}
			
			
		}
		
		if ($add_prin_result['result'] > 0) {

			$msg = 'Berhasil Menambah data Retur';

			$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ');
			$result = array('success' => true, 'message' => $msg);
					
		}else{
			$msg = 'Gagal Menambah data Retur';

			$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ');
			$result = array('success' => false, 'message' => $msg);
					
		}
		
	}
	
	public function add_po(){
		
		$this->form_validation->set_rules('name', 'nama Principal', 'required|min_length[1]|max_length[10]');
		// $this->form_validation->set_rules('alamat', 'Alamat Principal', 'required');
		//$this->form_validation->set_rules('tgl', 'Tanggal Tidak Boleh Kosong', 'required');
		
		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			
			//echo $pesan;die;
			
			//$msg = strip_tags(str_replace("\r", '', $pesan));
			$msg = $pesan;

			$result = array('success' => false, 'message' => $msg);
		}else {
			
			$int_val = $this->Anti_sql_injection($this->input->post('int_flo', TRUE));
			
			$array_items = [];
			
			$total_amount = 0;
			for($i=0;$i<=$int_val;$i++){
				
				
				$kode_split = explode('|',$this->Anti_sql_injection($this->input->post('kode_'.$i, TRUE)));
				if($kode_split[0]){
					$array_items[$i]['kode'] = $kode_split[0];
					$array_items[$i]['qty_stock'] = $kode_split[3];
					//$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE));
					$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE));
					$array_items[$i]['qty'] = $this->Anti_sql_injection($this->input->post('qty_'.$i, TRUE));
					$array_items[$i]['qtys'] = $this->Anti_sql_injection($this->input->post('qtys_'.$i, TRUE));
					$array_items[$i]['id_d'] = $this->Anti_sql_injection($this->input->post('id_d_'.$i, TRUE));
					$array_items[$i]['qty_box'] = $kode_split[1];
					$array_items[$i]['total'] = ( (intval(str_replace('.','',$array_items[$i]['qty'])) + intval(str_replace('.','',$array_items[$i]['qtys'])) ) * intval($kode_split[1]) ) * floatval(str_replace(',','.',str_replace('.','',$this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE))))) ;
				//	$array_items[$i]['remark'] = $this->Anti_sql_injection($this->input->post('remark_'.$i, TRUE));
					$array_items[$i]['totalqty'] = $this->Anti_sql_injection($this->input->post('totalqty_'.$i, TRUE));
					$total_amount = $total_amount + ( ((intval(str_replace('.','',$array_items[$i]['qty'])) + intval(str_replace('.','',$array_items[$i]['qtys']))) * intval($kode_split[1]) ) * floatval(str_replace(',','.',str_replace('.','',$this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE)))))  );
				
				}
				
				
			}
			
			//print_r($array_items);die;
			
			//$total_amounts = $total_amount + ($total_amount * (floatval($this->Anti_sql_injection($this->input->post('ppn', TRUE)))/100));
			
			$data = array(
				'id_penjualan' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
				'keterangan' => $this->Anti_sql_injection($this->input->post('ket_retur', TRUE)),
				'pic'          => $this->session->userdata['logged_in']['user_id'],
				'date' => date('Y-m-d')
				
			);
			
			//print_r($data);die;
		
				
				$add_prin_result = $this->return_model->add_returm($data);
				//$this->return_model->edit_credit($data);
				
				foreach($array_items as $array_itemss){
					
					if($array_itemss['kode'] == ""){
						
					}else{
					
						$datas = array(
						
							'id_retur' => $add_prin_result['lastid'],
							'id_detail_mat' => $array_itemss['id_d'],
							'harga_satuan' => str_replace(',','.',str_replace('.','',$array_itemss['harga'])),
							'amount' => intval(str_replace('.','',$array_itemss['harga']))* ( (intval(str_replace('.','',$array_itemss['qty'])) + intval(str_replace('.','',$array_itemss['qtys'])) ) * intval($array_itemss['qty_box'])  ),
							'qty' => (intval(str_replace('.','',$array_itemss['qty'])) + intval(str_replace('.','',$array_itemss['qtys'])) ) * intval($array_itemss['qty_box']), 
							'qty_per_box' => intval($array_itemss['qty_box']),
							'qty_box' => intval(str_replace('.','',$array_itemss['qty'])),
							'qty_satuan' => intval(str_replace('.','',$array_itemss['qtys']))
							
							//'remark' => $array_itemss['remark'],
						
						);
						
						//$id_mutasi = $this->return_model->add_mutasi($datas);
						//$this->return_model->edit_qty($datas);
						
						
						
						$prin_result = $this->return_model->add_detail_retur($datas);
					
					}
					
				}
				
				
				
				//print_r($add_prin_result);die;
				
				if ($prin_result['result'] > 0) {
					
					
					
					
					
					$msg = 'Berhasil Menambah data Retur';

					$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ');
					$result = array('success' => true, 'message' => $msg);
					
				}else{
					$msg = 'Gagal Menambah data Retur';

					$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ');
					$result = array('success' => false, 'message' => $msg);
					
				}
		
			
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);
		
		//echo $kode;die;
		
	}	
	
	public function edit_po(){
		
		// $this->form_validation->set_rules('kode', 'Kode Principal', 'required|min_length[4]|max_length[10]');
		// $this->form_validation->set_rules('alamat', 'Alamat Principal', 'required');
		$this->form_validation->set_rules('name', 'nama Principal', 'required|min_length[1]|max_length[10]');
		
		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			
			//echo "aasasasas";die;
			
			//$msg = strip_tags(str_replace("\r", '', $pesan));
			$msg = $pesan;

			$result = array('success' => false, 'message' => $msg);
		}else {
			
			$int_val = $this->Anti_sql_injection($this->input->post('int_flo', TRUE));
			$id_retur = $this->Anti_sql_injection($this->input->post('id_retur', TRUE));
			
			$array_items = [];
			
			$total_amount = 0;
			for($i=0;$i<=$int_val;$i++){
				
				$kode_split = explode('|',$this->Anti_sql_injection($this->input->post('kode_'.$i, TRUE)));
				if($kode_split[0]){
					$array_items[$i]['kode'] = $kode_split[0];
					$array_items[$i]['qty_stock'] = $kode_split[3];
					//$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE));
					$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE));
					$array_items[$i]['qty'] = $this->Anti_sql_injection($this->input->post('qty_'.$i, TRUE));
					$array_items[$i]['qtys'] = $this->Anti_sql_injection($this->input->post('qtys_'.$i, TRUE));
					$array_items[$i]['id_d'] = $this->Anti_sql_injection($this->input->post('id_d_'.$i, TRUE));
					$array_items[$i]['qty_box'] = $kode_split[1];
					$array_items[$i]['total'] = ( (intval(str_replace('.','',$array_items[$i]['qty'])) + intval(str_replace('.','',$array_items[$i]['qtys'])) ) * intval($kode_split[1]) ) * floatval(str_replace(',','.',str_replace('.','',$this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE))))) ;
				//	$array_items[$i]['remark'] = $this->Anti_sql_injection($this->input->post('remark_'.$i, TRUE));
					$array_items[$i]['totalqty'] = $this->Anti_sql_injection($this->input->post('totalqty_'.$i, TRUE));
					$total_amount = $total_amount + ( ((intval(str_replace('.','',$array_items[$i]['qty'])) + intval(str_replace('.','',$array_items[$i]['qtys']))) * intval($kode_split[1]) ) * floatval(str_replace(',','.',str_replace('.','',$this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE)))))  );
				
				}
			}
			

			$data = array(
				'id_penjualan' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
				'keterangan' => $this->Anti_sql_injection($this->input->post('ket_retur', TRUE)),
				'pic'          => $this->session->userdata['logged_in']['user_id'],
				'date' => date('Y-m-d'),
				'id_retur' => $id_retur
				
			);
		
				$add_prin_result = $this->return_model->edit_retur($data);
				
				$this->return_model->delete_detail_retur($id_retur);
				
				
				
				foreach($array_items as $array_itemss){
					
					if($array_itemss['kode'] == ""){
						
					}else{
					
						$datas = array(
						
							'id_retur' => $id_retur,
							'id_detail_mat' => $array_itemss['id_d'],
							'harga_satuan' => str_replace(',','.',str_replace('.','',$array_itemss['harga'])),
							'amount' => intval(str_replace('.','',$array_itemss['harga']))* ( (intval(str_replace('.','',$array_itemss['qty'])) + intval(str_replace('.','',$array_itemss['qtys'])) ) * intval($array_itemss['qty_box'])  ),
							'qty' => (intval(str_replace('.','',$array_itemss['qty'])) + intval(str_replace('.','',$array_itemss['qtys'])) ) * intval($array_itemss['qty_box']), 
							'qty_per_box' => intval($array_itemss['qty_box']),
							'qty_box' => intval(str_replace('.','',$array_itemss['qty'])),
							'qty_satuan' => intval(str_replace('.','',$array_itemss['qtys']))
							
							//'remark' => $array_itemss['remark'],
						
						);

						$prin_result = $this->return_model->add_detail_retur($datas);
						//$this->return_model->update_qty_m($data_q);
					
					}
					
				}
				
				
				
				//print_r($add_prin_result);die;
				
				if ($add_prin_result['result'] > 0) {
					
					
					
					
					
					$msg = 'Berhasil Merubah data Retur';

					$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ');
					$result = array('success' => true, 'message' => $msg);
					
				}else{
					$msg = 'Gagal Menambah data Retur';

					$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ');
					$result = array('success' => false, 'message' => $msg);
					
				}
		
			
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);
		
		//echo $kode;die;
		
	}

	public function delete_po() {
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);
		
		$this->return_model->delete_detail_retur($params['id']);
		$this->return_model->delete_retur($params['id']);


		//$result_dist 		= $this->return_model->delete_po($params['id']);
		//$result_dist2 		= $this->return_model->delete_po_mat($params['id']);

		$msg = 'Berhasil menghapus data Retur.';
		$result = array('success' => true, 'message' => $msg);
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function print_pdf(){
		
		$data   	= file_get_contents("php://input");
		//$params     = json_decode($data,true);
		
		
		//echo "aaa";
		$params = (explode('=',$data));
		
		$data = array(
			'id' => $params[1],
			
		);
		
		//print_r($data);die;
		
		$po_result = $this->return_model->get_po($data);
		$po_detail_result = $this->return_model->get_po_detail_full($data);
		$result = $this->return_model->get_principal();
		
		
		
		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);
		
		$array_items = [];
			
		$total_sub = 0;
		$total_sub_all = 0;
		$total_disk = 0;
		$grand_total = 0;
		
		$htmls = '';
		foreach($po_detail_result as $po_detail_results){
		
			$amn = number_format(($po_detail_results['price']-($po_detail_results['price']*($po_detail_results['diskon']/100))),0,',',''); 
			$htmls = $htmls.'
			<tr style="border-top: 1px solid black;" >
								<td  align="left" style="border-top: 1px solid black;"><p>'.$po_detail_results['stock_code'].'</p></td>
								<td  align="left" style="border-top: 1px solid black;"><p>'.$po_detail_results['stock_name'].'</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>'.number_format($po_detail_results['base_qty'],'2',',','.').' '.$po_detail_results['uom_symbol'].'</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>'.number_format($po_detail_results['qty_order'],'2',',','.').'</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>'.number_format($po_detail_results['unit_price'],'0',',','.').'</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>'.number_format($po_detail_results['diskon'],'2',',','.').'</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>'.number_format($amn,'0',',','.').'</p></td>

							</tr>
			';
			
			//$total_sub = $total_sub + number_format(($po_detail_results['price']-($po_detail_results['price']*($po_detail_results['diskon']/100))),0,',',''); 
			$total_sub = $total_sub + number_format(($po_detail_results['price']-($po_detail_results['price']*($po_detail_results['diskon']/100))),0,',',''); 
			$total_sub_all = $total_sub_all + number_format(($po_detail_results['price']),0,',',''); 
			$total_disk = $total_disk + ($po_detail_results['price']*($po_detail_results['diskon']/100)); 
			//$grand_total = $grand_total +  number_format(($po_detail_results['price']),0,',',''); 

		}
		
		$total_ppn = $total_sub * (floatval($po_result[0]['ppn']))/100;
		$grand_total = $total_ppn + $total_sub;
		
		//print_r($htmls);die;
		
		$item = $this->return_model->get_item_byprin($params);
		
		$rtext = '<br><p style="font-size: 10px;">PT. ENDRIA ALDA</p> ';
		
		$ctext = '<br><p style="font-size: 12px"> PURCHASE ORDER</p> ';
		
		$this->load->library('Pdf');
		
		$pdf = new Pdf('P', 'mm', 'A4', false, 'UTF-8', false);
		$pdf->SetTitle('Purchase Order');
		$pdf->SetHeaderMargin(25);
		$pdf->setPrintHeader(false);
		$pdf->SetTopMargin(6);
		$pdf->setFooterMargin(5);
		$pdf->SetAutoPageBreak(true);
		$pdf->SetAuthor('Author');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->SetPrintFooter(false);
		
		$pdf->AddPage();


		$html = '
		
		<style>
		
		  .floatedTable {
            float:left;
        }
        .inlineTable {
            display: inline-block;
        }
		p {
							font-size: 8px;
							
						}
		p {
							font-size: 8px;
							
						}
		</style>
  <table style=" float:left;border: none;" >
						<tr style="border: none;">
							<th  align="left" ><h5>PT.ENDIRA ALDA</h5></th>
							<th  align="RIGHT"></th>
						</tr>
						<tr style="border: none;">
							<th  align="left" ><h4>PURCHASE ORDER</h4></th>
							<th  align="RIGHT"><p>NOMOR, TGL : '.$po_result[0]['no_po'].'   '.$po_result[0]['date_po'].'</p></th>
						</tr>
					  </table><br><br><br>
					  
					<table>
					  <tr>
						<td>

						  <table cellspacing="0" cellpadding="0" border="0" style="float:right;width:100%.">
							<tr>
							  <td style="width:18%;"><p>Kepada</p></td>
							  <td><p>: '.$po_result[0]['name_eksternal'].'</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Alamat</p></td>
							  <td><p>: '.$po_result[0]['eksternal_address'].'</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Att</p></td>
							  <td><p>: '.$po_result[0]['pic'].'</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Telp</p></td>
							  <td><p>: '.$po_result[0]['phone_1'].'</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Fax</p></td>
							  <td><p>: '.$po_result[0]['fax'].'</p></td>
							</tr>

						  </table>
						</td>
						<td>
						  <table cellspacing="0" cellpadding="0" border="0" style="float:right;width:100%">
							<tr>
							  <td style="width:18%;"><p>Ship To</p></td>
							  <td><p>:JL.SANGKURIANG NO 38-A<BR>NPWP:01.555.161.7.428.000</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Email</p></td>
							  <td><p>: </p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Telp/Fax</p></td>
							  <td><p>: (022)6626-946</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Delivery</p></td>
							  <td><p>: </p></td>
							</tr>
						

						  </table>
						</td>
					  </tr>
					</table>
					<br><br><br>

					
					  <table style=" float:left;border-top: 1px solid black;border-bottom: 1px solid black;" >
						<tr style=" border-top: 1px solid black;font-size: 10px;">
							<th  align="left" >Code</th>
							<th  align="RIGHT">Description</th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Quantity<br>Lt/Kg</th>
							<th  align="RIGHT">Unit Price</th>
							<th  align="RIGHT">Disc</th>
							<th  align="RIGHT">Amount</th>
						</tr>
						'.$htmls.'
					  </table>
					  	<br><br><br>
						<br><br><br>
					  
					    <table style=" float:left;border-top: 1px solid black;" >
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Sub Total :</th>
							<th  align="RIGHT">'.number_format($total_sub_all,0,',','.').'</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Discount :</th>
							<th  align="RIGHT">'.number_format($total_disk,0,',','.').'</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"> Taxable :</th>
							<th  align="RIGHT">'.number_format($total_sub,0,',','.').'</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Vat/PPN :</th>
							<th  align="RIGHT">'.number_format($total_ppn,0,',','.').'</th>
						</tr>
					  </table>
					  
					  <table style=" float:left;border-top: 1px solid black;" >
						<tr style=" border-top: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Total :</th>
							<th  align="RIGHT">'.number_format($grand_total,0,',','.').'</th>
						</tr>
					  </table>
					<br><br><br><br><br> 
					<br><br><br><br><br> 
					
					 <table style="font-size: 10px;" >
						<tr style="">
							<th  align="CENTER" style="width:25%" ></th>
							<th  align="CENTER" style="width:12%" ></th>
							<th  align="CENTER" style="width:25%"></th>
							<th  align="CENTER" style="width:13%" ></th>
							<th  align="CENTER" style="width:25%">Cimahi, '.date('m/d/Y').'</th>
						</tr>
						<tr style="">
							<th  align="CENTER" style="width:25%" >Disetujui Oleh,</th>
							<th  align="CENTER" style="width:12%" ></th>
							<th  align="CENTER" style="width:25%">Diperiksa Oleh,</th>
							<th  align="CENTER" style="width:13%" ></th>
							<th  align="CENTER" style="width:25%">Dipesan Oleh,</th>
						</tr>
					  </table>
					  <br><br><br><br><br> 
					  <table style="font-size: 10px;" >
						<tr style="">
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Ratna S. Iskandar Dinata</span></th>
							<th  align="CENTER" style="width:12%"></th>
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Rienaldy Aryanto</span></th>
							<th  align="CENTER" style="width:13%"></th>
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Rangga Dean</span></th>
						</tr>
					  </table>
';

//echo $html;die;

		// $html = <<<EOD
// <h5>PT.ENDIRA ALDA</h5>
  // <table style=" float:left;border: none;" >
						// <tr style="border: none;">
							// <th  align="left" ><h4>PURCHASE ORDER</h4></th>
							// <th  align="RIGHT">NOMOR, TGL : ".."</th>
						// </tr>
					  // </table>

// EOD;

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
		
		
		
		$pdf->SetPrintFooter(false);
		
		$pdf->lastPage();
		
		// $pdf->writeHTMLCell(85, 5, '<p>'.$kategoriby.' : '.$kategori.'<br>Periode : '.$start_date.' / '.$end_date.'</p> ', 1, 'L', 0, 0, '', '', true);
		// $pdf->writeHTMLCell(85, 5, $img, 1, 'R', 0, 1, '', '', true);
		// $pdf->writeHTMLCell(180, 5, '[CENTER] ', 1, 'C', 0, 1, '', '', true);
		// $pdf->writeHTMLCell(85, 5, '[LEFT] ', 1, 'L', 0, 0, '', '', true);
		// $pdf->writeHTMLCell(85, 5, '[RIGHT] ', 1, 'L', 0, 1, '', '', true);

		// $pdf->Output('/var/www/html/tmp_doc/Report_Postbuy.pdf', 'F');

		// $this->output->set_content_type('application/json')->set_output(json_encode('aaa'));
		
		$pdf->Output('Purchase_order.pdf', 'I');

		
		// if ( $list ) {			
			  // $this->output->set_content_type('application/json')->set_output(json_encode($list));
		// } else {
			  // $result = array( 'Value not found!' );
			  // $this->output->set_content_type('application/json')->set_output(json_encode($list));
		// }
		
	}
	
}