<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report_dasar_pembelian extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('report_dasar_pembelian_model', 'reportDasarPembelian');
		$this->load->library('log_activity');
	}

	public function index()
	{
		$result['principles'] = $this->reportDasarPembelian->get_principles();

		$this->template->load('maintemplate', 'report_dasar_pembelian/views/index', $result);
	}

	public function list()
	{
		$principle = !empty($_GET['principle_id']) ? $_GET['principle_id'] : NULL;
		$tgl_awal = !empty($_GET['tgl_awal']) ? $_GET['tgl_awal'] : NULL;
		$tgl_akhir = !empty($_GET['tgl_akhir']) ? $_GET['tgl_akhir'] : NULL;

		// $sess_user_id = !empty($_GET['sess_user_id']) ? $_GET['sess_user_id'] : NULL;
		// $sess_token = !empty($_GET['sess_token']) ? $_GET['sess_token'] : NULL;
		$draw = $this->input->get('draw') != FALSE ? $this->input->get('draw') : 1;
		$length = $this->input->get('length') != FALSE ? $this->input->get('length') : 10;
		$start = $this->input->get('start') != FALSE ? $this->input->get('start') : 0;

		$order_fields = array('date_po');
		$order = $this->input->get('order');
		$order_dir = !empty($order[0]['dir']) ? $order[0]['dir'] : 'desc';
		$order_column = !empty($order[0]['column']) ? $order[0]['column'] : 0;

		$search = $this->input->get_post('search');
		$search_val = (!empty($search['value'])) ? $search['value'] : null;
		$search_value = anti_sql_injection($search_val);

		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['principle_id'] 	= $principle;
		$params['tgl_awal'] 	= $tgl_awal;
		$params['tgl_akhir'] 	= $tgl_akhir;
		// $params['sess_user_id'] = $sess_user_id;
		// $params['sess_token'] 	= $sess_token;
		$params['searchtxt'] 	= $search_value;

		$list = $this->reportDasarPembelian->list_pembelian($params);

		$newList = [];
		foreach ($list['data'] as $row) {
			array_push($newList, [
				$row['tanggal_bpb'],
				$row['stock_code'],
				$row['stock_name'],
				$row['name_eksternal'],
				number_format($row['qty'], 2, '.', ','),
				//number_format($row['unit_price'], 2, '.', ','),
				number_format($row['jumlah']/$row['qty'], 2, '.', ','),
				number_format($row['jumlah'], 2, '.', ','),
				number_format($row['ppn'], 2, '.', ','),
				number_format($row['rp_ppn'], 2, '.', ','),
			]);
		}

		$result["data"] = $newList;
		// $result["recordsTotal"] = $list['total'];
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function export_excel()
	{
		$params = array(
			'principle_id' => anti_sql_injection($this->input->post('principle_id')),
			'tgl_awal' => anti_sql_injection($this->input->post('tgl_awal')),
			'tgl_akhir' => anti_sql_injection($this->input->post('tgl_akhir')),
		);

		// var_dump($params);
		// die;

		// if (empty($params['principle_id']) || $params['principle_id'] == '0') {
		// 	$result = $this->reportDasarPembelian->get_report_penjualan_export($params);
		// 	$prin = "Semua Principle";
		// } else {
		// 	$result = $this->reportDasarPembelian->get_report_penjualan_prin($params);
		// 	$prin = "";
		// }

		$result = $this->reportDasarPembelian->list_pembelian($params);

		// var_dump($result['data'][0]);
		// die;

		$this->load->library('excel');

		$objPHPExcel = new PHPExcel();

		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'Tanggal')
			->setCellValue('B1', 'Bulan')
			->setCellValue('C1', 'Kode')
			->setCellValue('D1', 'Nama Barang')
			->setCellValue('E1', 'Kemasan')
			->setCellValue('F1', 'Principle')
			->setCellValue('G1', 'Qty')
			->setCellValue('H1', 'Harga')
			->setCellValue('I1', 'Jumlah')
			->setCellValue('J1', 'PPN')
			->setCellValue('K1', 'Rp + PPN')
			->setCellValue('L1', 'No Trans')
			->setCellValue('M1', 'No Invoice')
			->setCellValue('N1', 'Lama Hari')
			->setCellValue('O1', 'Jatuh Tempo')
			->setCellValue('P1', 'No Faktur Pajak')
			->setCellValue('Q1', 'Tgl Faktur Pajak')
			->setCellValue('R1', 'Jenis')
			->setCellValue('S1', 'Keterangan')
			->setCellValue('T1', 'Tahun');

		$rt = 2;
		$ints = 1;
		foreach ($result['data'] as $row) {

			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A' . $rt, $row['tanggal_bpb'])
				->setCellValue('B' . $rt, date('F', strtotime($row['tanggal_bpb'])))
				->setCellValue('C' . $rt, $row['stock_code'])
				->setCellValue('D' . $rt, $row['stock_name'])
				//->setCellValue('E' . $rt, implode(' ', [$row['base_qty'], $row['uom_name']]))
				->setCellValue('E' . $rt, $row['kemasan'])
				->setCellValue('F' . $rt, $row['name_eksternal'])
				->setCellValue('G' . $rt, $row['qty'])
				//->setCellValue('H' . $rt, number_format($row['unit_price'], 2, '.', ''))
				->setCellValue('H' . $rt, number_format($row['jumlah']/$row['qty'], 2, '.', ''))
				->setCellValue('I' . $rt, number_format($row['jumlah'], 2, '.', ''))
				->setCellValue('J' . $rt, number_format($row['ppn'], 2, '.', ''))
				->setCellValue('K' . $rt, number_format($row['ppn']+$row['jumlah'], 2, '.', ''))
				->setCellValue('L' . $rt, $row['no_po'])
				->setCellValue('M' . $rt, $row['no_invoice'])
				->setCellValue('N' . $rt, $row['lama_hari'])
				->setCellValue('O' . $rt, $row['due_date'])
				->setCellValue('P' . $rt, $row['faktur_pajak'])
				->setCellValue('Q' . $rt, $row['tanggal_faktur'])
				->setCellValue('R' . $rt, $row['type_material_name'])
				->setCellValue('S' . $rt, $row['nnnt'])
				->setCellValue('T' . $rt, date('Y', strtotime($row['date_po'])));

			$rt++;
			$ints++;
		};
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename=Report_dasar_pembelian.xls');
		header('Cache-Control: max-age=0');
		// If you’re serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you’re serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		unset($objPHPExcel);
	}
}
