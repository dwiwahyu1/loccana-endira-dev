<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Return_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}


	public function list_penjualan($params = array()){
		
		if($params['filter'] == ''){
			
			$query_s = ' AND a.status = 0 ';
			
		}else if($params['filter'] == 2 ){
			$query_s = '  ';
			
		}else{
			$query_s = ' AND a.status = '.$params['filter'];
		}
		
		if($params['filter_month'] == 'ALL'){
			$query_m = ' ';
		}else{
			$query_m = ' AND DATE_FORMAT(a.tanggal_invoice,"%M") = "'.$params['filter_month'].'"';
		}
		
		if($params['filter_year'] == 'ALL'){
			$query_y = ' ';
		}else{
			$query_y = 'AND DATE_FORMAT(a.tanggal_invoice,"%Y") = "'.$params['filter_year'].'" ';
		}
		
		$where_sts = $query_s.' '.$query_m.' '.$query_y;
		
			$query = 	'
				SELECT COUNT(*) AS jumlah 
				 FROM `t_invoice_penjualan` a
					JOIN `t_penjualan` b ON a.id_penjualan = b.`id_penjualan`
					LEFT JOIN `t_customer` e ON b.id_customer = e.`id_t_cust`
					where 1 = 1 '.$where_sts.'  AND ( 
						a.no_invoice LIKE "%'.$params['searchtxt'].'%"  OR
						e.cust_name LIKE "%'.$params['searchtxt'].'%"  OR
						a.tanggal_invoice LIKE "%'.$params['searchtxt'].'%"  OR
						b.no_penjualan LIKE "%'.$params['searchtxt'].'%"
					)
			';
						
			// $query2 = 	'
				// SELECT z.*,total_invoice - bayar AS sisa, RANK() OVER ( ORDER BY tanggal_invoice,total_invoice DESC) AS Rangking FROM ( 
					// SELECT SUM(f.price) AS total_invoice,a.id_invoice,a.no_invoice,a.tanggal_invoice,a.due_date,attention,a.faktur_pajak,note,SIGN,a.status AS status_invoice,b.*, e.`cust_name` ,
					// IF(g.bayar IS NULL,0,g.bayar) AS bayar
					// FROM `t_invoice_penjualan` a
					// JOIN `t_penjualan` b ON a.id_penjualan = b.`id_penjualan`
					// LEFT JOIN `t_customer` e ON b.id_customer = e.`id_t_cust`
					// LEFT JOIN d_invoice_penjualan f ON a.id_invoice = f.id_inv_penjualan
					// LEFT JOIN (
						// SELECT b.`id_invoice`,SUM(b.ammount) AS bayar FROM  `t_payment_hp` a
						// JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp` WHERE a.payment_type = 1 AND a.payment_status = 1 GROUP BY id_invoice
					// ) g ON g.`id_invoice` = f.`id_inv_penjualan`
					// WHERE 1 = 1 '.$where_sts.'  AND ( 
						// a.no_invoice LIKE "%'.$params['searchtxt'].'%"  OR
						// e.cust_name LIKE "%'.$params['searchtxt'].'%"  OR
						// a.tanggal_invoice LIKE "%'.$params['searchtxt'].'%"  OR
						// b.no_penjualan LIKE "%'.$params['searchtxt'].'%"
					// ) GROUP BY id_invoice ORDER BY tanggal_invoice DESC) z
				// ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
				
				
				
				// LIMIT '.$params['limit'].' 
				// OFFSET '.$params['offset'].' 
			// ';
			
			$query2 = 	'
				SELECT z.*,total_invoice - bayar AS sisa, RANK() OVER ( ORDER BY tanggal_invoice,total_invoice DESC) AS Rangking FROM ( 
					SELECT SUM(f.price) AS total_invoice,a.id_invoice,a.no_invoice,a.tanggal_invoice,a.due_date,attention,a.faktur_pajak,note,SIGN,a.status AS status_invoice,b.*, e.`cust_name` ,
					IF(g.bayar IS NULL,0,g.bayar) AS bayar
					FROM `t_invoice_penjualan` a
					JOIN `t_penjualan` b ON a.id_penjualan = b.`id_penjualan`
					LEFT JOIN `t_customer` e ON b.id_customer = e.`id_t_cust`
					LEFT JOIN d_invoice_penjualan f ON a.id_invoice = f.id_inv_penjualan
					LEFT JOIN (
						SELECT b.`id_invoice`,SUM(b.ammount) AS bayar FROM  `t_payment_hp` a
						JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp` WHERE a.payment_type = 1 GROUP BY id_invoice
					) g ON g.`id_invoice` = f.`id_inv_penjualan`
					WHERE 1 = 1 '.$where_sts.'  AND ( 
						a.no_invoice LIKE "%'.$params['searchtxt'].'%"  OR
						e.cust_name LIKE "%'.$params['searchtxt'].'%"  OR
						a.tanggal_invoice LIKE "%'.$params['searchtxt'].'%"  OR
						b.no_penjualan LIKE "%'.$params['searchtxt'].'%"
					) GROUP BY id_invoice ORDER BY tanggal_invoice DESC) z
				ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
				
				
				
				LIMIT '.$params['limit'].' 
				OFFSET '.$params['offset'].' 
			';
		
			//echo $query2;die;
		
			 $out		= array();
			  $querys		= $this->db->query($query);
			  $result = $querys->row();
			  
			  $total_filtered = $result->jumlah;
			  $total 			= $result->jumlah;
		  
				if(($params['offset']+10) > $total_filtered){
				$limit_data = $total_filtered - $params['offset'];
			  }else{
				$limit_data = $params['limit'] ;
			  }
		  
		
		  
		  //echo $query;die;
			//echo $query;die;
			 $query2s		= $this->db->query($query2);
		  $result2 = $query2s->result_array();						
		  $return = array(
			  'data' => $result2,
			  'total_filtered' => $total_filtered,
			  'total' => $total,
		  );
		  return $return;
		
		
	}
	
	public function get_list_bk_p($params)
	{
		//$sql 	= 'CALL uom_search_id(?)';


		$sql 	= '
			
			SELECT z.*, rank() over ( ORDER BY tanggal_invoice,total_invoice DESC) AS Rangking from ( 
					SELECT uom_symbol as symbol,(SUM(f.price) - (if(sum(tp.retur_qty) is null,0,sum(tp.retur_qty))*g.unit_price) + ((SUM(f.price)- (if(sum(tp.retur_qty) is null,0,sum(tp.retur_qty))*g.unit_price)) * (b.ppn/100)))  as total_invoice,j.bayar total_bayar, bb.no_do,a.id_invoice,a.no_invoice,a.tanggal_invoice,a.due_date,attention,a.faktur_pajak,a.note,sign,a.status as status_invoice,b.*, e.`name_eksternal` 
					FROM `t_invoice_pembelian` a
					JOIN t_bpb bb on a.id_bpb  = bb.id_bpb
					JOIN `t_purchase_order` b ON bb.id_po = b.`id_po`
					LEFT JOIN `t_eksternal` e ON b.id_distributor = e.`id`
					LEFT JOIN d_invoice_pembelian f on a.id_invoice = f.id_inv_pembelian
					LEFT JOIN t_bpb_detail fa on fa.id_bpb_detail = f.id_bpb_detail
					LEFT JOIN t_po_mat g on g.id_t_ps = fa.id_po_mat
					LEFT JOIN t_detail_retur_pembelian tp on g.id_t_ps = tp.id_po_mat
					LEFT JOIN m_material h on h.id_mat = g.id_material
					LEFT JOIN m_uom i on h.unit_terkecil = i.id_uom
					LEFT JOIN (
						SELECT b.`id_invoice`,SUM(b.ammount) AS bayar FROM  `t_payment_hp` a
						JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp` GROUP BY id_invoice
					) j on j.id_invoice = a.id_invoice
					where 1 = 1  group by a.id_invoice order by tanggal_invoice DESC) z
				ORDER BY tanggal_invoice,total_invoice DESC

		';

		//print_r($sql);die;

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function list_pembayaran($params = array()){
		
			// if($params['filter'] == 2 || $params['filter'] == ''){
			
			$query_s = ' ';
			
		// }else{
			
			// $query_s = ' AND a.payment_status = '.$params['filter'];
		// }
		
		if($params['filter_month'] == 'ALL'){
			$query_m = ' ';
		}else{
			$query_m = ' AND DATE_FORMAT(a.date,"%M") = "'.$params['filter_month'].'"';
		}
		
		if($params['filter_year'] == 'ALL'){
			$query_y = ' ';
		}else{
			$query_y = 'AND DATE_FORMAT(a.date,"%Y") = "'.$params['filter_year'].'" ';
		}
		
		$where_sts = $query_s.' '.$query_m.' '.$query_y;
		
			$query = 	'
				SELECT COUNT(*) AS jumlah 
				 FROM ( select a.* from `t_payment_hp` a
						JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp`
						JOIN `t_invoice_penjualan` c ON b.id_invoice = c.`id_invoice`
						JOIN `t_penjualan` d ON d.`id_penjualan` = c.`id_penjualan`
						JOIN `t_customer` e ON d.`id_customer` = e.`id_t_cust`
						where 1 = 1 '.$where_sts.' AND ( 
							a.no_payment LIKE "%'.$params['searchtxt'].'%" OR
							e.cust_name LIKE "%'.$params['searchtxt'].'%" OR
							a.tgl_terbit LIKE "%'.$params['searchtxt'].'%"
						)
						GROUP BY a.`id_hp`
					) a 
			';
						
			$query2 = 	'
					SELECT z.*, RANK() OVER ( ORDER BY id_hp DESC) AS Rangking FROM ( 

					SELECT a.*,SUM(b.ammount) AS total,`cust_name`, eg.keterangan FROM  `t_payment_hp` a
					JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp`
					JOIN `t_invoice_penjualan` c ON b.id_invoice = c.`id_invoice`
					JOIN `t_penjualan` d ON d.`id_penjualan` = c.`id_penjualan`
					JOIN `t_customer` e ON d.`id_customer` = e.`id_t_cust`
					JOIN `t_coa` eg ON a.`cash_account` = eg.`id_coa`
					WHERE a.`payment_type` = 1 '.$where_sts.' AND (  
						a.no_payment LIKE "%'.$params['searchtxt'].'%" OR
						e.cust_name LIKE "%'.$params['searchtxt'].'%" OR
						a.tgl_terbit LIKE "%'.$params['searchtxt'].'%"
					)
					GROUP BY a.`id_hp`

					) z
				ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
				
				
				
				LIMIT '.$params['limit'].' 
				OFFSET '.$params['offset'].' 
			';
		
			//echo $query2;die;
		
			 $out		= array();
			  $querys		= $this->db->query($query);
			  $result = $querys->row();
			  
			  $total_filtered = $result->jumlah;
			  $total 			= $result->jumlah;
		  
				if(($params['offset']+10) > $total_filtered){
				$limit_data = $total_filtered - $params['offset'];
			  }else{
				$limit_data = $params['limit'] ;
			  }
		  
		
		  
		  //echo $query;die;
			//echo $query;die;
			 $query2s		= $this->db->query($query2);
		  $result2 = $query2s->result_array();						
		  $return = array(
			  'data' => $result2,
			  'total_filtered' => $total_filtered,
			  'total' => $total,
		  );
		  return $return;
		
		
	}
	
	public function get_cash_account($params = array()){
		
		// $this->db->select('t_purchase_order.*,t_eksternal.name_eksternal,no_do,t_bpb.tanggal as tanggal_bpb,id_bpb ')
		// #$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        // ->from('t_purchase_order')
        // ->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id')
		// ->join('t_bpb', 't_bpb.id_po = t_purchase_order.id_po')
		// ->where(array('t_purchase_order.status' => 1));
		// $query = $this->db->get();
		
		// $query = "
		// SELECT * FROM t_coa
		// WHERE id_coa BETWEEN 4 AND 21 
		// ORDER BY id_coa 
		// ";
		
		$query = "
		SELECT * FROM t_coa
		WHERE id_parent = 3
		ORDER BY id_coa 
		";
		
		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}				
		
		
		public function get_invoice_s($params = array()){
		
		
		
		$query = "
		SELECT b.* FROM `t_penjualan` a
				JOIN `t_invoice_penjualan` b ON a.id_penjualan = b.`id_penjualan`
				WHERE `id_customer` = ".$params['id_customer']."
				ORDER BY id_invoice
		";
		
		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}				
	
	
	public function get_cash_account_pay($params = array()){
		
		// $this->db->select('t_purchase_order.*,t_eksternal.name_eksternal,no_do,t_bpb.tanggal as tanggal_bpb,id_bpb ')
		// #$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        // ->from('t_purchase_order')
        // ->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id')
		// ->join('t_bpb', 't_bpb.id_po = t_purchase_order.id_po')
		// ->where(array('t_purchase_order.status' => 1));
		// $query = $this->db->get();
		
		$query = "
		SELECT * FROM t_coa
		WHERE id_coa BETWEEN 140 AND 153 
		ORDER BY id_coa 
		";
		
		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}				
	
		public function update_konfirmas($id) {
	
		$datas = array(
			'payment_status' => 1
		);

	//	print_r($datas);die;
		$this->db->where('id_hp',$id);
		$this->db->update('t_payment_hp',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}	
	
		public function update_invoice_sts($id) {
	
		$datas = array(
			'status' => 1
		);

	//	print_r($datas);die;
		$this->db->where('id_invoice',$id);
		$this->db->update('t_invoice_penjualan',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
		public function add_uom($data)
	{
		$datas = array(
			'id_coa' => $data['coa'],
			'date' => $data['tgl'],
			'date_insert' => date('Y-m-d H:i:s'),
			'value' => $data['jumlah'],
			'value_real' => $data['jumlah'],
			'type_cash' => $data['type_cash'],
			'bukti' => $data['bukti'],
			'id_coa_temp' => $data['id_coa_temp'],
			'id_parent' => $data['id_parent'],
			'id_valas' => 1,
			'note' => $data['ket']
		);

	//	print_r($datas);die;

		$this->db->insert('t_coa_value',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}	
	
	public function get_invoice_byprin($params = array()){
		
		// $query = "			
				// SELECT A.*,SUM(price) AS amount,C.*, IF(bayar IS NULL,0,bayar) AS bayar, (SUM(price) - IF(bayar IS NULL,0,bayar)) AS sisa FROM `t_invoice_penjualan` A
				// JOIN `d_invoice_penjualan` B ON A.`id_invoice` = B.`id_inv_penjualan`
				// JOIN `t_penjualan` C ON A.`id_penjualan` = C.`id_penjualan`
				// LEFT JOIN (SELECT a.*,b.id_invoice AS inv ,SUM(b.`ammount`) AS bayar FROM `t_payment_hp` a 
				// JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp`				
				// WHERE a.payment_type = 1 AND a.payment_status = 1  GROUP BY b.id_invoice) g ON g.`inv` = A.`id_invoice`
				// WHERE `id_customer` =  ".$params['id_prin']."
				// AND A.status = 0
				// GROUP BY A.`id_invoice`
		// ";
		
		$query = "			
		SELECT * FROM (
				SELECT A.*,SUM(price) AS amount, IF(bayar IS NULL,0,bayar) AS bayar, (SUM(price) - IF(bayar IS NULL,0,bayar)) AS sisa, IF(bayar_giro IS NULL,0,bayar_giro) AS bayar_giro ,
				C.`id_sales`,C.`no_penjualan`,C.`date_penjualan`,C.`delivery_date`,C.`id_customer`,C.`term_of_payment`,C.`total_amount`,C.`id_coa`,C.`payment_coa`,C.`id_valas`,C.`rate`,C.`ppn`,C.`seq_n`,C.`keterangan`
				FROM `t_invoice_penjualan` A
				JOIN `d_invoice_penjualan` B ON A.`id_invoice` = B.`id_inv_penjualan`
				JOIN `t_penjualan` C ON A.`id_penjualan` = C.`id_penjualan`
				LEFT JOIN (SELECT a.*,b.id_invoice AS inv ,SUM(b.`ammount`) AS bayar FROM `t_payment_hp` a 
				JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp`				
				WHERE a.payment_type = 1 AND ( a.account_type = 0 or a.payment_status = 1 ) GROUP BY b.id_invoice) g ON g.`inv` = A.`id_invoice`
				LEFT JOIN (SELECT a.*,b.id_invoice AS inv ,SUM(b.`ammount`) AS bayar_giro FROM `t_payment_hp` a 
				JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp`				
				WHERE a.payment_type = 1 AND a.account_type = 1 AND a.payment_status = 0 GROUP BY b.id_invoice) h ON h.`inv` = A.`id_invoice`
				WHERE `id_customer` =  ".$params['id_prin']."
				AND A.status = 0
				GROUP BY A.`id_invoice`
				) A WHERE sisa > 0
		";
		
		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}			
	
	public function get_cust_by_hp($params = array()){
		
		$query = "
			SELECT a.*, b.ammount,b.id_invoice, c.no_invoice, f.* 
				FROM `t_payment_hp` a
				JOIN `d_payment_hp` b ON a.id_hp = b.id_hp
				JOIN `t_invoice_penjualan` c ON b.id_invoice = c.id_invoice
				JOIN `t_penjualan` f ON c.`id_penjualan` = f.`id_penjualan`
				WHERE a.id_hp = ".$params['id']."
				GROUP BY c.id_invoice
		";
		
		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}		
	
	public function get_piutang_detail($params = array()){
		
		$query = "
			SELECT a.*, b.ammount,b.id_invoice, SUM(d.price) AS piutang, d.price total_amount,IF(bayar IS NULL,0,bayar) AS bayar,c.no_invoice FROM `t_payment_hp` a
				JOIN `d_payment_hp` b ON a.id_hp = b.id_hp
				JOIN `t_invoice_penjualan` c ON b.id_invoice = c.id_invoice
				JOIN (SELECT `id_d_penjualan`,`id_inv_penjualan`,SUM(d.price) AS price FROM `d_invoice_penjualan` d GROUP BY `id_inv_penjualan` ) d ON c.id_invoice = d.`id_inv_penjualan`
				LEFT JOIN (SELECT a.*,b.id_invoice AS inv ,SUM(b.`ammount`) AS bayar FROM `t_payment_hp` a 
				JOIN `d_payment_hp` b ON a.`id_hp` = b.`id_hp`				
				WHERE a.payment_type = 1 AND a.payment_status = 1  GROUP BY b.id_invoice) g ON g.`inv` = b.`id_invoice`
				JOIN `d_penjualan` e ON d.`id_d_penjualan` = e.`id_dp`
				WHERE a.id_hp = ".$params['id']."
				GROUP BY c.id_invoice
				
		";
		
		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function get_customer($params = array()){
		
		//$query = $this->db->get_where('t_customer', array('region' => $params['region']));
		
		$this->db->select('*')
		 ->from('t_customer')
		# ->where(array('region' => $params['region']))
		 ->order_by('cust_name', 'asc');
		 $query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}			
	
	public function get_price_mat($params = array()){
		
		// $query = $this->db->get_where('t_po_mat', array('id_po' => $params['kode']));
		// $query = $this->db->order_by('id_t_ps', 'desc');
		
		$this->db->select('*')
		 ->from('t_po_mat')
		 ->where(array('id_material' => $params['kode']))
		 ->order_by('id_t_ps', 'desc');
		 $query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}		
	
	public function get_penjualan_byid($params = array()){
		
		$this->db->select('t_penjualan.*, t_customer.*,t_invoice_penjualan.no_invoice, tanggal_invoice, due_date, note ')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('t_penjualan')
        ->join('t_customer', 't_penjualan.id_customer = t_customer.id_t_cust')
		->join('t_invoice_penjualan', 't_penjualan.id_penjualan = t_invoice_penjualan.id_penjualan','left')
		->where(array('t_penjualan.id_penjualan' => $params['id_penjualan']));
		$query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}		
	
	public function get_penjualan_byid_inv($params = array()){
		
		$this->db->select('t_penjualan.*, t_customer.*,t_invoice_penjualan.no_invoice, tanggal_invoice, due_date, note ')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('t_penjualan')
        ->join('t_customer', 't_penjualan.id_customer = t_customer.id_t_cust')
		->join('t_invoice_penjualan', 't_penjualan.id_penjualan = t_invoice_penjualan.id_penjualan','left')
		->where(array('t_invoice_penjualan.id_invoice' => $params['id_penjualan']));
		$query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
		
		
	public function get_item_byprin($params = array()){
		$this->db->select('m_material.*, m_uom.uom_symbol')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('m_material')
        ->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
		->where(array('dist_id' => $params['id_prin']));
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}		
	
	public function get_retur_id($params = array()){
		$this->db->select('t_retur.date,t_retur.id_penjualan,t_retur.keterangan, t_detail_retur.*')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('t_retur')
        ->join('t_detail_retur', 't_retur.id_retur = t_detail_retur.id_retur')
		->where(array('t_retur.id_retur' => $params['id']));
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function get_item_byprin_all(){
		$this->db->select('m_material.*, m_uom.uom_symbol')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('m_material')
		->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
		->where(array('status' => 0))
		->order_by('stock_name','asc');
		//->where(array('dist_id' => $params['id_prin']));
		// ->where('m_material.dist_id != ',$params['id_prin'],FALSE);
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}		
	
	public function get_penjualan_all(){ 
		$this->db->select('t_penjualan.*, t_customer.cust_name')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('t_penjualan')
		->join('t_customer', 't_penjualan.id_customer = t_customer.id_t_cust')
		->where(array('status' => 0))
		->order_by('date_penjualan','desc');
		//->where(array('dist_id' => $params['id_prin']));
		// ->where('m_material.dist_id != ',$params['id_prin'],FALSE);
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function update_seq($data) {
		$sql 	= 'update t_ordering set seq_max = seq_max+1 where nama_menu = "'.$data.'" ';

			
		$query 	= $this->db->query($sql);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
		public function get_kode($params = array()){
		
		$query =  $this->db->select('seq_max as max')
		->from('t_ordering')
		->where('nama_menu','pembayaran_k');
		;
		
		$query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function get_penjualan($params = array()){
		
		// $query = $this->db->get_where('t_purchase_order', array('id_po' => $params['id']));
		$this->db->select('t_penjualan.*, t_customer.*')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('t_penjualan')
        ->join('t_customer', 't_penjualan.id_customer = t_customer.id_t_cust')
		->where(array('id_penjualan' => $params['id']));
		$query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_penjualan_detail_update($params = array()){
		
		//$query = $this->db->get_where('d_penjualan', array('id_penjualan' => $params['id_penjualan']));
		$this->db->select('d_penjualan.*, m_material.stock_name, m_material.base_qty, m_material.stock_name, m_uom.uom_symbol,d_invoice_penjualan.qty as qty_retur,d_invoice_penjualan.qty_satuan as qty_retur_sat,d_invoice_penjualan.qty_box as qty_box_retur,d_invoice_penjualan.price ')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('d_penjualan')
        ->join('m_material', 'd_penjualan.id_material = m_material.id_mat')
        ->join('d_invoice_penjualan', 'd_penjualan.id_dp = d_invoice_penjualan.id_d_penjualan')
        ->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
		->where(array('d_invoice_penjualan.id_inv_penjualan' => $params['id_penjualan']));
		$query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_penjualan_detail($params = array()){
		
		//$query = $this->db->get_where('d_penjualan', array('id_penjualan' => $params['id_penjualan']));
		$this->db->select('d_penjualan.*, m_material.stock_name, m_material.base_qty, m_material.stock_name, m_uom.uom_symbol')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('d_penjualan')
        ->join('m_material', 'd_penjualan.id_material = m_material.id_mat')
        ->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
		->where(array('id_penjualan' => $params['id_penjualan']));
		$query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
		
	public function get_po_detail_full($params = array()){
		
		//$query = $this->db->get_where('t_po_mat', array('id_po' => $params['id']));
		$this->db->select('t_po_mat.*,t_po_mat.qty as qty_order, m_material.*, m_uom.*')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('t_po_mat')
        ->join('m_material', 't_po_mat.id_material = m_material.id_mat')
        ->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
		->where(array('id_po' => $params['id']));
		$query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function check_principal($params = array()){
		
		$query = $this->db->get_where('t_eksternal', array('kode_eksternal' => $params['kode']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_invoice($data) {

		$this->db->insert('t_invoice_penjualan',$data);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}	
	
	public function add_mutasi($data) {
	
	  $data_insert_mutasi = array(
        'id_stock_awal'  => $data['id_material'],
        'id_stock_akhir' => $data['id_material'],
        'date_mutasi'    => date('Y-m-d H:i:s'),
        'amount_mutasi'  => $data['qty'],
        'type_mutasi'    => 0,
        'user_id'    => $this->session->userdata['logged_in']['user_id'],
      );

	//	print_r($datas);die;

		$this->db->insert('t_mutasi',$data_insert_mutasi);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}	
		
		
	public function add_payment($data) {
	
	  $data_insert_mutasi = array(
        'no_payment'  => $data['kode'],
        'payment_type' => 1,
        'date'    		=> $data['tgl'],
        'tgl_terbit'  => $data['tglterbit'],
        'tgl_aktif'  => $data['tglaktif'],
        'tgl_jatuh_tempo'  => $data['tgljatuhtempo'],
        'cash_account'    => $data['cash_acc'],
		'id_invoice'    => $data['inv'],
        'account_type'    => $data['tipe_bayar']
      );

	//	print_r($datas);die;

		$this->db->insert('t_payment_hp',$data_insert_mutasi);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function add_detail_payment($data) {
	
	 

		$this->db->insert('d_payment_hp',$data);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}	
	
	public function add_mutasi_out($data) {
	
	  $data_insert_mutasi = array(
        'id_stock_awal'  => $data['id_material'],
        'id_stock_akhir' => $data['id_material'],
        'date_mutasi'    => date('Y-m-d H:i:s'),
        'amount_mutasi'  => $data['qty'],
        'type_mutasi'    => 1,
        'user_id'    => $this->session->userdata['logged_in']['user_id'],
      );

	//	print_r($datas);die;

		$this->db->insert('t_mutasi',$data_insert_mutasi);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function add_detail_invoice($data) {
		
	//	print_r($datas);die;

		$this->db->insert('d_invoice_penjualan',$data);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function edit_qty($data) {
		$sql 	= 'update m_material set qty = qty - '.$data['qty'].' 
		where id_mat = '.$data['id_material'].' ';

		// $datas = array(
			// 'qty' => $data['qty_stock']
		// );

	// //	print_r($datas);die;
		// $this->db->where('id_mat',$data['id_material'], false);
		// $this->db->update('m_material',$datas, false);

			
		$query 	= $this->db->query($sql);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}	
	
	public function update_qty_m($data) {
		$sql 	= 'update m_material set qty = qty + '.$data['qty'].' 
		where id_mat = '.$data['id_mat'].' ';
		
		//echo $sql;die;
		// $datas = array(
			// 'qty' => $data['qty_stock']
		// );

	//	print_r($datas);die;
		// $this->db->where('id_mat',$data['id_material'], false);
		// $this->db->update('m_material',$datas, false);

			
		$query 	= $this->db->query($sql);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function edit_credit($data) {
		$sql 	= "
		update t_customer set sisa_credit = sisa_credit - ".$data['total_amount']." where id_t_cust = ".$data['id_customer']."
		";
		
		// $total = $data['sisa'] - $data['total_amount'];
		// $datas = array(
			// 'sisa_credit' => $total
		// );

	//	print_r($datas);die;
		// $this->db->where('id_t_cust',$data['id_customer'], false);
		// $this->db->update('t_customer',$datas, false);

			
		$query 	= $this->db->query($sql);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}	
	
	public function edit_credit2($data,$selling_data) {
		$sql 	= 'update t_customer set sisa_credit = sisa_credit + '.floatval($selling_data['total_amount']).'
		where id_t_cust = '.$data['id_customer'];
		$query 	= $this->db->query($sql);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function edit_retur_apr($data){
		
		$datas = array(
			
			'status' => $data['sts']
			
		);
		
		$this->db->where('id_retur',$data['id_po']);
		$this->db->update('t_retur',$datas);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}	
	
	public function history_retur_apr($data){
		
		$sql 	= "
		INSERT INTO `history_d_penjualan`
		SELECT NULL AS jo,a.* FROM `d_penjualan` a
		JOIN  `t_detail_retur` b ON a.id_dp = b.`id_detail_mat` 
		WHERE b.id_retur  = ".$data['id_po']."
		
		";
		
		// $total = $data['sisa'] - $data['total_amount'];
		// $datas = array(
			// 'sisa_credit' => $total
		// );

	//	print_r($datas);die;
		// $this->db->where('id_t_cust',$data['id_customer'], false);
		// $this->db->update('t_customer',$datas, false);

			
		$query 	= $this->db->query($sql);
		
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function edit_invoice($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			
			'no_invoice' => $data['no_invoice'],
			'due_date' => $data['due_date'],
			'attention' => $data['attention'],
			'tanggal_invoice' => $data['tanggal_invoice'],
			'id_penjualan' => $data['id_penjualan'],
			'note' => $data['note']
			
		);

	//	print_r($datas);die;
		$this->db->where('id_invoice',$data['id_invoice']);
		$this->db->update('t_invoice_penjualan',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
		
		
		public function edit_invoice_sts($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			
			'status' => 1
			
		);

	//	print_r($datas);die;
		$this->db->where('id_invoice',$data['id_po']);
		$this->db->update('t_invoice_penjualan',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function edit_detail_penjualan($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'qty' => $data['qty_retur'],
			'qty_box' => $data['qty_box_retur'],
			'qty_satuan' => $data['qty_retur_sat'],
			'price' => $data['amount'],
			'mutasi_id' => $data['mutasi_id']
		);

	//	print_r($datas);die;
		$this->db->where('id_dp',$data['id_dp']);
		$this->db->update('d_penjualan',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	function delete_hp($id) {

		$this->db->where('id_hp', $id);
		$this->db->delete('t_payment_hp'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		

		return $result;
	}
	
	function delete_hp_detail($id) {

		$this->db->where('id_hp', $id);
		$this->db->delete('d_payment_hp'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		

		return $result;
	}

	function delete_retur($id) {

		$this->db->where('id_invoice', $id);
		$this->db->delete('t_invoice_penjualan'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		

		return $result;
	}	
	
	function delete_detail_invoice($id) {

		$this->db->where('id_inv_penjualan', $id);
		$this->db->delete('d_invoice_penjualan'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		

		return $result;
	}	
	
	function delete_mutasi($data_q) {

		$this->db->where('id_mutasi', $data_q['mutasi_id']);
		$this->db->delete('t_mutasi'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		

		return $result;
	}

}