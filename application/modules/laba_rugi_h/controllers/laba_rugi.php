<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Laba_rugi extends MX_Controller
{

  public function __construct()
  {
      parent::__construct();
      $this->load->model('laba_rugi/rl_model');
      $this->load->library('log_activity');
      $this->load->library('priv');
  }

  /**
   * anti sql injection
   */
  public function Anti_sql_injection($string)
  {
    $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
    return $string;
  }

  public function index()
  {
	  $priv = $this->priv->get_priv();

	// sales per montg
	  
	  $year = date('Y');
	  
	    $html_sb = '';
			  $html_sbs = '';
			  $html_all = '';
			  $cur_month = '';
			  
	$params  = array(
		'start_time' => date('Y-m-d'),
		'end_time' => date('Y-m-d'),
		'report' => "laba_rugi"
	);
	
	$params_p  = array(
		'start_time' => date('Y-m-d'),
		'end_time' => date('Y-m-d'),
		'report' => "passiva"
	);


	$data['get_aktiva'] = $this->rl_model->get_report_keu($params);
	$data['get_pasiva'] = $this->rl_model->get_report_keu($params_p);
	$datas['get_summ'] = $this->rl_model->get_report_keu_summ($params);
	
	$html_s = '
		<tr><th>PENJUALAN BERSIH</th><th>'.number_format($datas['get_summ'][0]['nilai'],2,',','.').'</th></tr>
				<tr><th>BONUS PRINCIPLE</th><th>'.number_format($datas['get_summ'][1]['nilai'],2,',','.').'</th></tr>
				<tr><th>HARGA POKOK PENJUALAN</th><th>'.number_format($datas['get_summ'][2]['nilai'],2,',','.').'</th></tr>
				<tr><th><STRONG>LABA KOTOR</STRONG></th><th>'.number_format($datas['get_summ'][0]['nilai'] + $datas['get_summ'][1]['nilai'] - $datas['get_summ'][2]['nilai'],2,',','.').'</th></tr>
				<tr><th>BIAYA PENJUALAN</th><th>'.number_format($datas['get_summ'][3]['nilai'],2,',','.').'</th></tr>
				<tr><th>BIAYA ADMINISTRASI dan UMUM</th><th>'.number_format($datas['get_summ'][4]['nilai'],2,',','.').'</th></tr>
				<tr><th>BIAYA PENYUSUTAN</th><th>'.number_format($datas['get_summ'][5]['nilai'],2,',','.').'</th></tr>
				<tr><th><STRONG>BIAYA OPERASIONAL</STRONG></th><th>'.number_format($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai'],2,',','.').'</th></tr>
				<tr><th><STRONG>LABA (RUGI) OPERASI</STRONG></th><th>'.number_format(($datas['get_summ'][0]['nilai'] + $datas['get_summ'][1]['nilai'] - $datas['get_summ'][2]['nilai'])-($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai']),2,',','.').'</th></tr>
				<tr><th>PENDAPATAN NON OPERASI</th><th>'.number_format($datas['get_summ'][6]['nilai'],2,',','.').'</th></tr>
				<tr><th>BIAYA NON OPERASI</th><th>'.number_format($datas['get_summ'][7]['nilai'],2,',','.').'</th></tr>
				<tr><th><STRONG>LABA / RUGI BERSIH SEBELUM PAJAK</STRONG></th><th>'.number_format(($datas['get_summ'][0]['nilai'] + $datas['get_summ'][1]['nilai'] - $datas['get_summ'][2]['nilai'])-($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai'])+$datas['get_summ'][6]['nilai']-$datas['get_summ'][7]['nilai'],2,',','.').'</th></tr>
		';
	
	$data['priv'] = $priv;
	$data['t_pa'] = $html_s;
	
	//print_r($get_aktiva);die;

    $this->template->load('maintemplate', 'laba_rugi/views/index',$data);
  }


	public function reports_persediaan($params){
		
		$data = array(
			'principal' => 0,
			'tgl_awal' => $params['start_time'],
			'tgl_akhir' => $params['end_time']
			
		);
		
				
				
				//print_r($params_awalss);die;
		
		$kuantiti_a = 0;
		$harga_satuan_a = 0;
		$nilai_a = 0;
		$pembelian_a = 0;
		$kuantiti_sol_a = 0;
		$kuantiti_titip_a = 0;
		$kuantiti_bonus_a = 0;
		$kuantiti_diskon_a = 0;
		$harga_sat_pemb_a = 0;
		$nilai_pemb_a = 0;
		$harga_pokok_a = 0;
		$penjualan_a = 0;
		$pengeluaran_a = 0;
		$saldo_akhir_a = 0;
		$nilai_akhir_a = 0;
		
			
			$dist_a = $this->rl_model->get_distribution($data);
			$html = '';
			
			foreach($dist_a as $dist_as){
			
				$datat = array(
					'principal' => $dist_as['dist_id'],
					'tgl_awal' => $params['start_time'],
					'tgl_akhir' => $params['end_time']
					
				);
			
				$params_awalss['principal']		= $dist_as['dist_id'];
				$params_awalss['tgl_akhir']	= date("Y-m-d", strtotime($datat['tgl_awal'] ."-1 days"));
				$params_awalss['tgl_awal']	= date_format(date_create($params_awalss['tgl_akhir']),"Y-m-01");
			
			if($datat['tgl_awal'] == '2021-01-01' ){
				$result = $this->rl_model->get_report_persediaan($datat);
				$result_awal = $this->rl_model->get_report_persediaan($datat);
			}else{
				$result_awal = $this->rl_model->get_report_persediaan_newest($params_awalss);
				$result = $this->rl_model->get_report_persediaan_newest($datat);
			}
				// echo "<pre>";
			//print_r($result_awal);die; 
				
				
				$kuantiti = 0;
				$harga_satuan = 0;
				$nilai = 0;
				$pembelian = 0;
				$kuantiti_sol = 0;
				$kuantiti_titip = 0;
				$kuantiti_bonus = 0;
				$kuantiti_diskon = 0;
				$harga_sat_pemb = 0;
				$nilai_pemb = 0;
				$harga_pokok = 0;
				$penjualan = 0;
				$pengeluaran = 0;
				$saldo_akhir = 0;
				$nilai_akhir = 0;
				$prin = '';
				$kode_prin = '';
				
				$int_iuy = 0;
				foreach( $result as $datas ){
					
					if($datat['tgl_awal'] == '2021-01-01' ){
						$datas['harga_satuan'] = $datas['harga_satuan'];
					}else{
						$datas['harga_satuan'] = $result_awal[$int_iuy]['harga_pokok'];
					}
					
					$html .= '<tr>
								<th >'.$datas['stock_code'].'</th>
								<th>'.$datas['stock_name'].'</th>
								<th>'.$datas['base_qty'].' '.$datas['uom_symbol'].' x '.$datas['unit_box'].'</th>
								<th>'.number_format($datas['saldo_awals'],2,',','.').'</th>
								<th>'.number_format($datas['harga_satuan'],2,',','.').'</th>
								<th>'.number_format($datas['saldo_awals']*$datas['harga_satuan'],2,',','.').'</th>
								<th>'.number_format($datas['pembelian'],2,',','.').'</th>
								<th>'.number_format($datas['kuantiti_diskon'],2,',','.').'</th>
								<th>'.number_format($datas['kuantiti_titip'],2,',','.').'</th>
								<th>'.number_format($datas['kuantiti_bonus'],2,',','.').'</th>
								<th>'.number_format($datas['harga_sat_pemb'],2,',','.').'</th>
								<th>'.number_format($datas['nilai_pemb'],2,',','.').'</th>
								<th>'.$datas['ket'].'</th>
								<th>'.number_format($datas['harga_pokok'],2,',','.').'</th>
								<th>'.number_format($datas['penjualan'],2,',','.').'</th>
								<th>'.number_format($datas['pengeluaran'],2,',','.').'</th>
								<th>'.number_format($datas['saldo_akhir'],2,',','.').'</th>
								<th>'.number_format($datas['saldo_akhir']*$datas['harga_pokok'],2,',','.').'</th>
							</tr>';
					
					$prin = $datas['name_eksternal'];
					$kode_prin = substr($datas['stock_code'],0,3);
					$kuantiti = $kuantiti + $datas['saldo_awals'];
					$harga_satuan = $harga_satuan+ $datas['harga_satuan'];
					$nilai = $nilai+ $datas['saldo_awals']*$datas['harga_satuan'];
					$pembelian = $pembelian+ $datas['pembelian'];
					$kuantiti_sol = $kuantiti_sol+ $datas['kuantiti_sol'];
					$kuantiti_diskon = $kuantiti_diskon+ $datas['kuantiti_diskon'];
					$kuantiti_titip = $kuantiti_titip+ $datas['kuantiti_titip'];
					$kuantiti_bonus = $kuantiti_bonus+ $datas['kuantiti_bonus'];
					$harga_sat_pemb = $harga_sat_pemb+ $datas['harga_sat_pemb'];
					$nilai_pemb = $nilai_pemb+ $datas['nilai_pemb'];
					$harga_pokok =$harga_pokok+ $datas['harga_pokok'];
					$penjualan = $penjualan+ $datas['penjualan'];
					$pengeluaran = $pengeluaran+ $datas['pengeluaran'];
					$saldo_akhir = $saldo_akhir+ $datas['saldo_akhir'];
					$nilai_akhir = $nilai_akhir+ $datas['saldo_akhir']*$datas['harga_pokok'];
					
					$kuantiti_a = $kuantiti_a + $datas['saldo_awals'];
					$harga_satuan_a = $harga_satuan_a +  $datas['harga_satuan'];
					$nilai_a = $nilai_a + $datas['saldo_awals']*$datas['harga_satuan'];
					$pembelian_a = $pembelian_a +  $datas['pembelian'];
					$kuantiti_sol_a = $kuantiti_sol_a +  $datas['kuantiti_sol'];
					$kuantiti_titip_a = $kuantiti_titip_a +  $datas['kuantiti_titip'];
					$kuantiti_diskon_a = $kuantiti_diskon_a +  $datas['kuantiti_diskon'];
					$kuantiti_bonus_a = $kuantiti_bonus_a +  $datas['kuantiti_bonus'];
					$harga_sat_pemb_a = $harga_sat_pemb_a +  $datas['harga_sat_pemb'];
					$nilai_pemb_a = $nilai_pemb_a +  $datas['nilai_pemb'];
					$harga_pokok_a = $harga_pokok_a + $datas['harga_pokok'];
					$penjualan_a = $penjualan_a +  $datas['penjualan'];
					$pengeluaran_a = $pengeluaran_a +  $datas['pengeluaran'];
					$saldo_akhir_a = $saldo_akhir_a +  $datas['saldo_akhir'];
					$nilai_akhir_a = $nilai_akhir_a +  $datas['saldo_akhir']*$datas['harga_pokok'];
				
					$int_iuy++;
				}
			
				$html .= '<tr style="background:yellow">
							<th>'.$kode_prin.'</th>
							<th>'.$prin.'</th>
							<th></th>
							<th>'.number_format($kuantiti,2,',','.').'</th>
							<th>'.number_format($harga_satuan,2,',','.').'</th>
							<th>'.number_format($nilai,2,',','.').'</th>
							<th>'.number_format($pembelian,2,',','.').'</th>
							<th>'.number_format($kuantiti_diskon,2,',','.').'</th>
							<th>'.number_format($kuantiti_titip,2,',','.').'</th>
							<th>'.number_format($kuantiti_bonus,2,',','.').'</th>
							<th>'.number_format($harga_sat_pemb,2,',','.').'</th>
							<th>'.number_format($nilai_pemb,2,',','.').'</th>
							<th></th>
							<th>'.number_format($harga_pokok,2,',','.').'</th>
							<th>'.number_format($penjualan,2,',','.').'</th>
							<th>'.number_format($pengeluaran,2,',','.').'</th>
							<th>'.number_format($saldo_akhir,2,',','.').'</th>
							<th>'.number_format($nilai_akhir,2,',','.').'</th>
						</tr>';
			}
		
		$html .= '<tr style="background:green">
						<th colspan="3">Total </th>
						<th>'.number_format($kuantiti_a,2,',','.').'</th>
						<th>'.number_format($harga_satuan_a,2,',','.').'</th>
						<th>'.number_format($nilai_a,2,',','.').'</th>
						<th>'.number_format($pembelian_a,2,',','.').'</th>
						<th>'.number_format($kuantiti_diskon_a,2,',','.').'</th>
						<th>'.number_format($kuantiti_titip_a,2,',','.').'</th>
						<th>'.number_format($kuantiti_bonus_a,2,',','.').'</th>
						<th>'.number_format($harga_sat_pemb_a,2,',','.').'</th>
						<th>'.number_format($nilai_pemb_a,2,',','.').'</th> 
						<th></th>
						<th>'.number_format($harga_pokok_a,2,',','.').'</th>
						<th>'.number_format($penjualan_a,2,',','.').'</th>
						<th>'.number_format($pengeluaran_a,2,',','.').'</th>
						<th>'.number_format($saldo_akhir_a,2,',','.').'</th>
						<th>'.number_format($nilai_akhir_a,2,',','.').'</th>
					</tr>';
		
		$results['html'] = $html;
		
		$datae['persediaan_awal'] = $nilai_a;
		$datae['persediaan_akhir'] = $nilai_akhir_a;
		$datae['pembelian'] = $nilai_pemb_a;
		
		return $datae;
		//$this->output->set_content_type('application/json')->set_output(json_encode($results));
		//print_r($result);die;
		
		
	}

	function filter(){
		
		$data   = file_get_contents("php://input");
		$param   = json_decode($data, true);
		
		$params  = array(
			'start_time' => $param['start_time'],
			'end_time' => $param['end_time'],
			'report' => "laba_rugi"
		);
		
		$params_p  = array(
			'start_time' => $param['start_time'],
			'end_time' => $param['end_time'],
			'report' => "laba_rugi"
		);


		$datas['get_aktiva'] = $this->rl_model->get_report_keu($params);
		$datas['get_summ'] = $this->rl_model->get_report_keu_summ($params_p);
		$datas['get_piutang'] = $this->rl_model->get_piutang($params);
		//$datas['get_persediaan'] = $this->rl_model->get_persediaan($params);
		$datas['get_persediaan'] = $this->reports_persediaan($params);
		$datas['get_hutang'] = $this->rl_model->get_hutang($params);
		$datas['get_penjualan'] = $this->rl_model->get_penjualan($params);
		
		//print_r($datas['get_persediaan']);die;
		
		$html_a = '';
		
		$tot_a = 0;
		$tot_b = 0;
		
		$hp1 = 0;
		$hp2 = 0;
		
		$penj = 0;
		
		foreach($datas['get_aktiva'] as $get_aktivas){
			
			if($get_aktivas['type_data'] == 0){
				
				
				
				if($get_aktivas['coa'] == 50100){
					
					$html_a .= ' <tr>
						  <th >'.$get_aktivas['coa'].'</th>
						  <th>'.$get_aktivas['keterangan'].'</th>
						  <th>'.number_format($datas['get_persediaan']['persediaan_awal'],2,',','.').'</th>
						  <th>0.00</th>
						</tr>';
					$tot_a +=$datas['get_persediaan']['persediaan_awal'];
					
					$nn = $datas['get_persediaan']['persediaan_awal'];

					
				}elseif($get_aktivas['coa'] == 50201){
					
					$html_a .= ' <tr>
						  <th >'.$get_aktivas['coa'].'</th>
						  <th>'.$get_aktivas['keterangan'].'</th>
						  <th>'.number_format($datas['get_persediaan']['pembelian'],2,',','.').'</th>
						  <th>0.00</th>
						</tr>';
					$tot_a +=$datas['get_persediaan']['pembelian'];
					
					$nn = $datas['get_persediaan']['pembelian'];

					
				}else{
				
					$html_a .= ' <tr>
						  <th >'.$get_aktivas['coa'].'</th>
						  <th>'.$get_aktivas['keterangan'].'</th>
						  <th>'.number_format($get_aktivas['nilai'],2,',','.').'</th>
						  <th>0.00</th>
						</tr>';
					$tot_a +=$get_aktivas['nilai'];
					
					$nn = $get_aktivas['nilai'];
				
				}
				
				if($get_aktivas['coa'] == 50100 | $get_aktivas['coa'] == 50201 | $get_aktivas['coa'] == 50202 | $get_aktivas['coa'] == 50500 | $get_aktivas['coa'] == 50600){
					$hp1 += $nn;
				}
			
			}else{
				
				if($get_aktivas['coa'] == 50700){
				
				$html_a .= ' <tr>
                  <th >'.$get_aktivas['coa'].'</th>
                  <th>'.$get_aktivas['keterangan'].'</th>
				  <th>0.00</th>
                  <th>'.number_format($datas['get_persediaan']['persediaan_akhir'],2,',','.').'</th>
                </tr>';
			$tot_b +=$datas['get_persediaan']['persediaan_akhir'];
			
				$nn = $datas['get_persediaan']['persediaan_akhir'];
				
				}elseif($get_aktivas['coa'] == 41102){
					
					$html_a .= ' <tr>
						  <th >'.$get_aktivas['coa'].'</th>
						  <th>'.$get_aktivas['keterangan'].'</th>
						  <th>0.00</th>
						  <th>'.number_format($datas['get_penjualan'][0]['pestisida'],2,',','.').'</th>
						</tr>';
					$tot_b +=$datas['get_penjualan'][0]['pestisida'];
					
					$nn = $datas['get_penjualan'][0]['pestisida'];
					$penj += $datas['get_penjualan'][0]['pestisida'];

					
				}elseif($get_aktivas['coa'] == 41103){
					
					$html_a .= ' <tr>
						  <th >'.$get_aktivas['coa'].'</th>
						 <th>'.$get_aktivas['keterangan'].'</th>
						  <th>0.00</th>
						   <th>'.number_format($datas['get_penjualan'][0]['non_pestisida'],2,',','.').'</th>
						</tr>';
					$tot_b +=$datas['get_penjualan'][0]['non_pestisida'];
					
					$nn = $datas['get_penjualan'][0]['non_pestisida'];
					$penj += $datas['get_penjualan'][0]['non_pestisida'];
					
				}else{
					
				$html_a .= ' <tr>
                  <th >'.$get_aktivas['coa'].'</th>
                  <th>'.$get_aktivas['keterangan'].'</th>
				  <th>0.00</th>
                  <th>'.number_format($get_aktivas['nilai'],2,',','.').'</th>
                </tr>';
			$tot_b +=$get_aktivas['nilai'];
			
				$nn = $get_aktivas['nilai'];
			
				}
				
				if($get_aktivas['coa'] == 50300 | $get_aktivas['coa'] == 50400 | $get_aktivas['coa'] == 50700){
					$hp2 += $nn;
				}
				
			}
		}
		
			$html_a .= ' <tr>
					<th colspan=2>Total</th>
					<th>'.number_format($tot_a,2,'.',',').'</th>
					<th>'.number_format($tot_b,2,'.',',').'</th>
				</tr> <tr>
					<th colspan=3>Laba / Rugi </th>
					<th>'.number_format($tot_b - $tot_a,2,'.',',').'</th>
				</tr>';
		
		$html_p = '';
		
		
		$tot_p = 0;
		// foreach($datas['get_pasiva'] as $get_aktivas){
			
			// $html_p .= ' <tr>
                  // <th >'.$get_aktivas['coa'].'</th>
                  // <th>'.$get_aktivas['keterangan'].'</th>
                  // <th>'.number_format($get_aktivas['nilai'],2,'.',',').'</th>
                // </tr>';
			// $tot_p +=$get_aktivas['nilai'];
		// }
		
		// $html_p .= ' <tr>
					// <th colspan=2>Total</th>
					// <th>'.number_format($tot_p,2,'.',',').'</th>
				// </tr>';
		//print_r($datas['get_summ']);die;
		
		$html_s = '
		<tr><th>PENJUALAN BERSIH</th><th>'.number_format($penj,2,',','.').'</th></tr>
				<tr><th>BONUS PRINCIPLE</th><th>'.number_format($datas['get_summ'][1]['nilai'],2,',','.').'</th></tr>
				<tr><th>HARGA POKOK PENJUALAN</th><th>'.number_format(($hp1 - $hp2),2,',','.').'</th></tr>
				<tr><th><STRONG>LABA KOTOR</STRONG></th><th>'.number_format($penj + $datas['get_summ'][1]['nilai'] - ($hp1 - $hp2),2,',','.').'</th></tr>
				<tr><th>BIAYA PENJUALAN</th><th>'.number_format($datas['get_summ'][3]['nilai'],2,',','.').'</th></tr>
				<tr><th>BIAYA ADMINISTRASI dan UMUM</th><th>'.number_format($datas['get_summ'][4]['nilai'],2,',','.').'</th></tr>
				<tr><th>BIAYA PENYUSUTAN</th><th>'.number_format($datas['get_summ'][5]['nilai'],2,',','.').'</th></tr>
				<tr><th><STRONG>BIAYA OPERASIONAL</STRONG></th><th>'.number_format($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai'],2,',','.').'</th></tr>
				<tr><th><STRONG>LABA (RUGI) OPERASI</STRONG></th><th>'.number_format(($datas['get_summ'][0]['nilai'] + $datas['get_summ'][1]['nilai'] - $datas['get_summ'][2]['nilai'])-($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai']),2,',','.').'</th></tr>
				<tr><th>PENDAPATAN NON OPERASI</th><th>'.number_format($datas['get_summ'][6]['nilai'],2,',','.').'</th></tr>
				<tr><th>BIAYA NON OPERASI</th><th>'.number_format($datas['get_summ'][7]['nilai'],2,',','.').'</th></tr>
				<tr><th><STRONG>LABA / RUGI BERSIH SEBELUM PAJAK</STRONG></th><th>'.number_format(($penj + $datas['get_summ'][1]['nilai'] - ($hp1 - $hp2))-($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai'])+$datas['get_summ'][6]['nilai']-$datas['get_summ'][7]['nilai'],2,',','.').'</th></tr>
		';
		
		$datas['t_ac'] = $html_a;
		//$datas['t_pa'] = $html_p;
		$datas['t_pa'] = $html_s;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($datas));
		//print_r($datas['get_aktiva']);die;
		
	}

  function lists()
  {

    if( !empty($_GET['sess_user_id']) ) {
			  $sess_user_id = $_GET['sess_user_id'];
		  } else {
			  $sess_user_id = NULL;
		  }
		  
		   if( !empty($_GET['sess_token']) ) {
			  $sess_token = $_GET['sess_token'];
		  } else {
			  $sess_token = NULL;
		  }
		  
		    if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		  if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		  if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 				
		  $order_fields = array('coa'); // , 'COST'
		  $order = $this->input->get_post('order');
		  if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'desc';}; 
		  if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 	
		  
		    $params['limit'] 		= (int) $length;
			$params['offset'] 		= (int) $start;
			$params['order_column'] = $order_fields[$order_column];
			$params['order_dir'] 	= $order_dir;
			$params['sess_user_id'] = $sess_user_id;
			$params['sess_token'] 	= $sess_token;
			$params['searchtxt'] 	= $_GET['search']['value'];
		  

    //print_r($params);die;

    $list = $this->rl_model->lists($params);
    // echo '<pre>'; print_r($list);
	$priv = $this->priv->get_priv();
    //print_r($list);die;

    $result["recordsTotal"] = $list['total'];
    $result["recordsFiltered"] = $list['total_filtered'];
    $result["draw"] = $draw;

    //print_r($result);die;

    $data = array();
    $i = $params['offset'];
    $username = $this->session->userdata['logged_in']['username'];
    foreach ($list['data'] as $k => $v) {
      $i = $i + 1;


      $status_akses = '
                <div class="btn-group" style="display:'.$priv['update'].'"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edituom(\'' . $v['id'] . '\')"><i class="fa fa-edit"></i></button></div>
				  <div class="btn-group" style="display:'.$priv['delete'].'"><button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteuom(\'' . $v['id'] . '\')"><i class="fa fa-trash"></i></button></div>';
				  
				  if($v['type_cash'] == 0){
					  $sss = 'Pemasukan';
				  }else{
					  $sss = 'Pengeluaran';
				  }

      array_push($data, array(
        $i,
        $v['coa'].'-'.$v['keterangan'],
        $sss,
        number_format($v['value_real'],2,',','.'),
        $v['note'],
        $status_akses
      ));
    }

    $result["data"] = $data;

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function add()
  {
	  
	  
    $coa = $this->rl_model->get_coa();



    // $this->load->view('add_modal_view', $data);

    // $result = $this->distributor_model->location();

    $data = array(
      'group' => '',
      'coa' => $coa
    );

    $this->template->load('maintemplate', 'laba_rugi/views/add_modal_view', $data);
  }

  public function edit()
  {
    //$id = $this->Anti_sql_injection($this->input->post('sid', TRUE));
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('iduom', TRUE)),
		);
    $result = $this->rl_model->get_uom_data($this->Anti_sql_injection($data['id']));
	 $coa = $this->rl_model->get_coa();

    //print_r($result);die;
    // $roles = $this->rl_model->roles($id);

    $data = array(
      'uom' => $result[0],
      'id' => $this->Anti_sql_injection($this->input->post('iduom', TRUE)),
	  'coa' => $coa
    );

    $this->template->load('maintemplate', 'laba_rugi/views/edit_modal_view', $data);
  }

  public function deletes()
  {

    $data   = file_get_contents("php://input");
    $params   = json_decode($data, true);

    $list = $this->rl_model->deletes($this->Anti_sql_injection($params['id']));

    $res = array(
      'status' => 'success',
      'message' => 'Data telah di hapus'
    );

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }

  public function edit_uom()
  {
	$this->form_validation->set_rules('coa', 'coa', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('tgl', 'tgl', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('cash', 'cash', 'trim|required|max_length[255]');
    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));
      $result = array(
        'success' => false,
        'message' => $msg
      );
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      // die;
      $message = "";
      $data = array(
       'user_id' => $this->session->userdata['logged_in']['user_id'],
        'coa'     => $this->Anti_sql_injection($this->input->post('coa', TRUE)),
        'id'     => $this->Anti_sql_injection($this->input->post('id', TRUE)),
        'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
        'cash' => $this->Anti_sql_injection($this->input->post('cash', TRUE)),
        'jumlah' => $this->Anti_sql_injection($this->input->post('jumlah', TRUE)),
        'ket' => $this->Anti_sql_injection($this->input->post('ket', TRUE))
      );
      $result = $this->rl_model->edit_uom($data);
      if ($result > 0) {
        $msg = 'Berhasil merubah uom.';

        $result = array(
          'success' => true,
          'message' => $msg
        );
      } else {
        $msg = 'Gagal merubah uom.';

        $result = array(
          'success' => false,
          'message' => $msg
        );
      }

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
  }

  public function add_uom()
  {
    $this->form_validation->set_rules('coa', 'coa', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('tgl', 'tgl', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('cash', 'cash', 'trim|required|max_length[255]');
    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));
      $result = array(
        'success' => false,
        'message' => $msg
      );

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      // die;
      $message = "";
      $data = array(
        'user_id' => $this->session->userdata['logged_in']['user_id'],
        'coa'     => $this->Anti_sql_injection($this->input->post('coa', TRUE)),
        'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
        'cash' => $this->Anti_sql_injection($this->input->post('cash', TRUE)),
        'jumlah' => $this->Anti_sql_injection($this->input->post('jumlah', TRUE)),
        'ket' => $this->Anti_sql_injection($this->input->post('ket', TRUE))
      );

		//print_r($data);die;

      $result = $this->rl_model->add_uom($data);

      if ($result > 0) {
        $msg = 'Berhasil menambahkan uom.';

        $result = array(
          'success' => true,
          'message' => $msg
        );
      } else {
        $msg = 'Gagal menambahkan uom ke database.';
        $result = array(
          'success' => false,
          'message' => $msg
        );
      }
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
  }

    public function export()
    {
        $this->load->library('excel');
        
        $objPHPExcel = new PHPExcel();
        
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A1', 'COA')
        ->setCellValue('B1', 'Keterangan')
        ->setCellValue('C1', 'Debit')
        ->setCellValue('D1', 'Credit');
      
        // $end_date = anti_sql_injection($this->input->post('date_filter'));
        // $end_date = Carbon::parse($end_date)->isValid() ? $end_date : null;
		
		$start_time = anti_sql_injection($this->input->post('start_time'));
		$end_date = anti_sql_injection($this->input->post('end_time'));

        $params  = array(
            'start_time' => $start_time,
            'end_time' => $end_date ?? date('Y-m-d'),
            'report' => "laba_rugi" 
        );
		
	//	print_r( $params);die;

        # FIRST SHEET
        $objPHPExcel->getActiveSheet()->setTitle('Laba Rugi');
        $firstData = $this->rl_model->get_report_keu($params);
		$datas['get_piutang'] = $this->rl_model->get_piutang($params);
		$datas['get_persediaan'] = $this->rl_model->get_persediaan($params);
		$datas['get_hutang'] = $this->rl_model->get_hutang($params);
		$datas['get_penjualan'] = $this->rl_model->get_penjualan($params);

        $row = 2;
        $totalDebit = 0;
        $totalKredit = 0;
		
		$hp1 = 0;
		$hp2 = 0;
		
		$penj = 0;
		
        foreach ($firstData as $item) {
			
			if($item['coa'] == 50100){
			
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A' . $row, $item['coa'] ?? '')
				->setCellValue('B' . $row, $item['keterangan'] ?? '')
				->setCellValue('C' . $row, $item['type_data'] == 0 ? $datas['get_persediaan'][0]['persediaan_awal'] : 0)
				->setCellValue('D' . $row, $item['type_data'] == 0 ? 0 : $item['nilai']);
				$row++;
				
				$nn = $datas['get_persediaan'][0]['persediaan_awal'];

			}elseif($item['coa'] == 50201){
			
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A' . $row, $item['coa'] ?? '')
				->setCellValue('B' . $row, $item['keterangan'] ?? '')
				->setCellValue('C' . $row, $item['type_data'] == 0 ? $datas['get_persediaan'][0]['pembelian'] : 0)
				->setCellValue('D' . $row, $item['type_data'] == 0 ? 0 : $item['nilai']);
				$row++;
				
				$nn = $datas['get_persediaan'][0]['pembelian'];

			}elseif($item['coa'] == 50700){
			
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A' . $row, $item['coa'] ?? '')
				->setCellValue('B' . $row, $item['keterangan'] ?? '')
				->setCellValue('C' . $row, $item['type_data'] == 0 ? $item['nilai'] : 0)
				->setCellValue('D' . $row, $item['type_data'] == 0 ? 0 : $datas['get_persediaan'][0]['persediaan']);
				$row++;
				
				$nn = $datas['get_persediaan'][0]['persediaan'];

			}elseif($item['coa'] == 41102){
			
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A' . $row, $item['coa'] ?? '')
				->setCellValue('B' . $row, $item['keterangan'] ?? '')
				->setCellValue('C' . $row, $item['type_data'] == 0 ? $item['nilai'] : 0)
				->setCellValue('D' . $row, $item['type_data'] == 0 ? 0 : $datas['get_penjualan'][0]['pestisida']);
				$row++;
				
				$nn = $datas['get_penjualan'][0]['pestisida'];
				$penj += $datas['get_penjualan'][0]['pestisida'];

			}elseif($item['coa'] == 41103){
			
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A' . $row, $item['coa'] ?? '')
				->setCellValue('B' . $row, $item['keterangan'] ?? '')
				->setCellValue('C' . $row, $item['type_data'] == 0 ? $item['nilai'] : 0)
				->setCellValue('D' . $row, $item['type_data'] == 0 ? 0 : $datas['get_penjualan'][0]['non_pestisida']);
				$row++;
				
				$nn = $datas['get_penjualan'][0]['non_pestisida'];
				$penj += $datas['get_penjualan'][0]['non_pestisida'];

			}else{
				
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A' . $row, $item['coa'] ?? '')
				->setCellValue('B' . $row, $item['keterangan'] ?? '')
				->setCellValue('C' . $row, $item['type_data'] == 0 ? $item['nilai'] : 0)
				->setCellValue('D' . $row, $item['type_data'] == 0 ? 0 : $item['nilai']);
				$row++;
				
				$nn = $item['nilai'];
				
			}
			
			if($item['coa'] == 50100 | $item['coa'] == 50201 | $item['coa'] == 50202 | $item['coa'] == 50500 | $item['coa'] == 50600){
					$hp1 += $nn;
			}
			
			if($item['coa'] == 50300 | $item['coa'] == 50400 | $item['coa'] == 50700){
					$hp2 += $nn;
				}

            if ($item['type_data'] == 0) {
              $totalDebit += $nn;
            } else {
              $totalKredit += $nn;
            }
        }

        $objPHPExcel->setActiveSheetIndex(0)
            ->mergeCells('A' . $row . ':' . 'B' . $row)
            ->setCellValue('A' . $row, 'Total')
            ->setCellValue('C' . $row, $totalDebit)
            ->setCellValue('D' . $row, $totalKredit)
            ->mergeCells('A' . ($row+1) . ':' . 'C' . ($row+1))
            ->setCellValue('A' . ($row+1), 'Laba/Rugi')
            ->setCellValue('D' . ($row+1), $totalKredit - $totalDebit);


        # SECOND SHEET
        $secondData = $this->rl_model->get_report_keu_summ($params);
        // echo '<pre>'; print_r($secondData); die;

       // $objPHPExcel->createSheet(1)->setTitle('Summary');
        $objPHPExcel->setActiveSheetIndex(0)
            // ->setTitle('LABA/RUGI SUMMARY')
            ->setCellValue('G1', 'PENJUALAN BERSIH')
            ->setCellValue('H1', $penj ?? '')
            ->setCellValue('G2', 'HONUS PRINCIPLE')
            ->setCellValue('H2', $secondData[1]['nilai'] ?? '')
            ->setCellValue('G3', 'HARGA POKOK PENJUALAN')
            ->setCellValue('H3', ($hp1-$hp2) ?? '')
            ->setCellValue('G4', 'LABA KOTOR')
            ->setCellValue('H4', $penj + $secondData[1]['nilai'] - ($hp1-$hp2))
            ->setCellValue('G5', 'HIAYA PENJUALAN')
            ->setCellValue('H5', $secondData[3]['nilai'] ?? '')
            ->setCellValue('G6', 'HIAYA ADMINISTRASI dan UMUM')
            ->setCellValue('H6', $secondData[4]['nilai'] ?? '')
            ->setCellValue('G7', 'HIAYA PENYUSUTAN')
            ->setCellValue('H7', $secondData[5]['nilai'] ?? '')
            ->setCellValue('G8', 'HIAYA OPERASIONAL')
            ->setCellValue('H8', $secondData[3]['nilai'] + $secondData[4]['nilai'] + $secondData[5]['nilai'])
            ->setCellValue('G9', 'LABA (RUGI) OPERASI')
            ->setCellValue('H9', ($penj + $secondData[1]['nilai'] - ($hp1-$hp2)) - ($secondData[3]['nilai'] + $secondData[4]['nilai'] + $secondData[5]['nilai']))
            ->setCellValue('G10', 'PENDAPATAN NON OPERASI')
            ->setCellValue('H10', $secondData[6]['nilai'] ?? '')
            ->setCellValue('G11', 'HIAYA NON OPERASI')
            ->setCellValue('H11', $secondData[7]['nilai'] ?? '')
            ->setCellValue('G12', 'LABA / RUGI BERSIH SEBELUM PAJAK')
            ->setCellValue('H12', ($penj + $secondData[1]['nilai'] -($hp1-$hp2)) - ($secondData[3]['nilai'] + $secondData[4]['nilai'] + $secondData[5]['nilai']) + $secondData[6]['nilai'] - $secondData[7]['nilai'])
            ;

        $objPHPExcel->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename=laba_rugi.xls');
        header('Cache-Control: max-age=0');
        // If you’re serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you’re serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        unset($objPHPExcel);
    }
}
