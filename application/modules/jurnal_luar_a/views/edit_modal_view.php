<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto
  }

  .scroll-overflow {
    min-height: 650px
  }

  #modal-distributor::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="product-payment-inner-st">
          <ul id="myTabedu1" class="tab-review-design">
            <li class="active"><a href="#description">Update Jurnal</a></li>
          </ul>
          <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="review-content-section">
                    <form class="form-horizontal form-label-left" id="edit_uom" role="form" action="<?php echo base_url('jurnal/edit_uom'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>

                      <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Coa <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                            <select name="coa" id="coa" class="form-control" placeholder="Coa">
																	<option value="0" selected="selected" disabled>-- Pilih Coa --</option>
																	<?php
																		foreach($coa as $principals){
																			
																			if($principals['id_coa'] == $uom['id_coa']){
																				echo '<option value="'.$principals['id_coa'].'" selected="selected" >'.$principals['coa'].'-'.$principals['keterangan'].'</option>';
																			}else{
																				echo '<option value="'.$principals['id_coa'].'" >'.$principals['coa'].'-'.$principals['keterangan'].'</option>';
																			}
																			
																			
																		}
																	?>
																</select>
                        </div>
                      </div>


                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Tanggal <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input data-parsley-maxlength="255" type="text" id="tgl" name="tgl" class="form-control col-md-7 col-xs-12" placeholder="Tanggal" required="required" value="<?php echo $uom['date']; ?>">
                        </div>
                      </div>
					  
					  
					     <div class="item form-group" id="prin_div" <?php if($uom['id_external'] == '' ){ echo 'style="display:none"'; } ?> >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Principle <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <select name="prin" id="prin" class="form-control" placeholder="principle">
							<option ></option>
							<?php foreach($principle as $principles){
								
								if($principles['id_t_cust'] == $uom['id_external']){
									echo "<option value='".$principles['id_t_cust']."' selected >".$principles['cust_name']."</option>";
								}else{
									echo "<option value='".$principles['id_t_cust']."' >".$principles['cust_name']."</option>";
								}
								
							} ?>
						  </select>
						  
                        </div>
                      </div>
					  
					  <!--  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Tipe Cash <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <select name="cash" id="cash" class="form-control" placeholder="Pengeluaran">
							<option value='' disabled selected="selected">-- Tipe Cash --</option>
							<option value='0' <?php if($uom['type_cash'] == '0'){ echo "selected='selected'"; }  ?> >Pemasukan</option>
							<option value='1' <?php if($uom['type_cash'] == '1'){ echo "selected='selected'"; }  ?> >Pengeluaran</option>
						  </select>
						  
                        </div>
                      </div> -->
					  
					    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Jumlah <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input data-parsley-maxlength="255" type="text" id="jumlah" name="jumlah" class="form-control col-md-7 col-xs-12" placeholder="Jumlah" required="required" value="<?php echo number_format($uom['value_real'],2,',',''); ?>">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Keterangan <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <textarea data-parsley-maxlength="255" type="text" id="ket" name="ket" class="form-control col-md-7 col-xs-12" placeholder="Keterangan" required="required"> <?php echo $uom['note']; ?> </textarea>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-lg-12">
                          <div class="payment-adress">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                            <button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()">Batal</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="PrimaryModalalert" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-close-area modal-close-df">
        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
      </div>
      <div class="modal-body">
        <i class="educate-icon educate-checked modal-check-pro"></i>
        <h2>Data Berhasil Dirubah</h2>
        <p></p>
      </div>
      <div class="modal-footer">
        <a data-dismiss="modal" href="#" onClick="back()">OK</a>
      </div>
    </div>
  </div>
</div>

<script>
  function clearform() {

    $('#edit_uom').trigger("reset");

  }

  function back() {

    window.location.href = "<?php echo base_url() . 'jurnal'; ?>";

  }

  function listdist() {
    var user_id = '0001';
    var token = '093940349';

  }

  $(document).ready(function() {

$('#coa').select2();
$('#prin').select2();

$('#tgl').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		});

    listdist();

    $('#edit_uom').on('submit', function(e) {
      // validation code here
      //if(!valid) {
      e.preventDefault();

 swal({
      title: 'Yakin akan Rubah Data ?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak'
    }).then(function() {


      var formData = new FormData();
	formData.append('id', <?php echo $id; ?>);
	formData.append('coa', $('#coa').val());
			formData.append('tgl', $('#tgl').val());
			formData.append('cash', $('#cash').val());
			formData.append('jumlah', $('#jumlah').val());
			formData.append('ket', $('#ket').val());

      $.ajax({
        type: 'POST',
        url: '<?php echo base_url('jurnal/edit_uom'); ?>',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
          if (response.success == true) {
            // $('#PrimaryModalalert').modal('show');
            swal({
              title: 'Success!',
              text: response.message,
              type: 'success',
              showCancelButton: false,
              confirmButtonText: 'Ok'
            }).then(function() {
              back();
            })
          } else {
            $('#msg_err').html(response.message);
            $('#WarningModalftblack').modal('show');
          }
        }
      });
	  
	  })
	  
	  
    });
  });
</script>
