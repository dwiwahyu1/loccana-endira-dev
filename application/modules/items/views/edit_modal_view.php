<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto
  }

  .scroll-overflow {
    min-height: 650px
  }

  .parsley-errors-list {
    margin-top: 10px;
    color: red;
  }

  #modal-distributor::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="product-payment-inner-st">
          <ul id="myTabedu1" class="tab-review-design">
            <li class="active"><a href="#description">Update Item</a></li>
          </ul>
          <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="review-content-section">

                    <form class="form-horizontal form-label-left" id="edit_material" role="form" action="<?php echo base_url('items/edit_material'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
                      <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
                      <input data-parsley-maxlength="255" type="hidden" id="id_mat" name="id_mat" class="form-control" placeholder="Kode Stok" value="<?= $stok[0]['id_mat'] ?? ''; ?>" autocomplete="off" required="required">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Kode Item <span class="required"><sup>*</sup></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input data-parsley-maxlength="255" type="text" id="kode_stok" name="kode_stok" class="form-control" placeholder="Kode Item" value="<?= $stok[0]['stock_code'] ?? ''; ?>" autocomplete="off" required="required">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Item <span class="required"><sup>*</sup></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input data-parsley-maxlength="255" type="text" id="nama_stok" name="nama_stok" class="form-control" placeholder="Nama Item" value="<?= $stok[0]['stock_name'] ?? ''; ?>" autocomplete="off" required="required">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Deskripsi Item <span class="required"><sup>*</sup></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <textarea data-parsley-maxlength="255" id="desk_stok" name="desk_stok" class="form-control" autocomplete="off"><?= $stok[0]['stock_description'] ?? ''; ?></textarea>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Ukuran<span class="required"><sup>*</sup></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input type="number" id="satuan_kecil" name="satuan_kecil" class="form-control" placeholder="Quantity" value="<?= $stok[0]['base_qty'] ?? ''; ?>" autocomplete="off" required step="0.01">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_material">Unit <span class="required"><sup>*</sup></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <select class="form-control" name="unit" id="unit" style="width: 100%" required autocomplete="off">
                            <option value="">-- Pilih Unit --</option>
                            <?php foreach ($unit as $unit) { ?>
                              <option value="<?php echo $unit['id_uom']; ?>" <?php if (isset($stok[0]['unit_terkecil'])) {
                                                                                if ($stok[0]['unit_terkecil'] == $unit['id_uom']) echo 'selected="selected"';
                                                                              } ?>><?php echo $unit['uom_name']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Unit per Box <span class="required"><sup>*</sup></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input type="number" id="qty" name="qty" class="form-control" placeholder="Quantity" value="<?php if (isset($stok[0]['unit_box'])) {
                                                                                                                        echo $stok[0]['unit_box'];
                                                                                                                      } ?>" autocomplete="off" required step="0.01">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Status Item<span class="required"><sup>*</sup></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <select class="form-control" name="status_i" id="status_i" style="width: 100%" required autocomplete="off">
                            <option value="0" <?php if ($stok[0]['status'] == 0) {
                                                echo "selected='selected'";
                                              } ?>> Dijual </option>
                            <option value="1" <?php if ($stok[0]['status'] == 1) {
                                                echo "selected='selected'";
                                              } ?>> Tidak Dijual </option>
                            <option value="2" <?php if ($stok[0]['status'] == 2) {
                                                echo "selected='selected'";
                                              } ?>> Limit </option>
                          </select>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Status Show<span class="required"><sup>*</sup></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <select class="form-control" name="sts_show" id="sts_show" style="width: 100%" required autocomplete="off">
                            <option value="0" <?php if ($stok[0]['sts_show'] == 0) {
                                                echo "selected='selected'";
                                              } ?>> Hide </option>
                            <option value="1" <?php if ($stok[0]['sts_show'] == 1) {
                                                echo "selected='selected'";
                                              } ?>> Show </option>
                            <?php print_r($stok);?>
                          </select>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="limit"></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input type="number" id="limit" name="limit" class="form-control" placeholder="Quantity" value="" autocomplete="off" step="0.01" readOnly='readOnly' value='0'>
                        </div>
                      </div>

					<div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gudang">Tipe Barang <span class="required"><sup>*</sup></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <select class="form-control" name="tipe" id="tipe" style="width: 100%" required autocomplete="off">
                            <option value="" selected='selected' disabled>-- Pilih Tipe --</option>
                            <?php foreach ($tipe as $tipe) { 
								if ($stok[0]['type'] == $tipe['id_type_material']){ ?>
									
									<option value="<?php echo $tipe['id_type_material']; ?>" Selected="selected"><?php echo $tipe['type_material_name']; ?></option>
									
							<?php	}else{ ?>
									
									<option value="<?php echo $tipe['id_type_material']; ?>"><?php echo $tipe['type_material_name']; ?></option>
									
							<?php	}
								?>
								
                              
                            <?php } ?>
                          </select>
                        </div>
                      </div>
					  
					  <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gudang">Pajak <span class="required"><sup>*</sup></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <select class="form-control" name="pajak" id="pajak" style="width: 100%" required autocomplete="off">
                            <option value="" selected='selected' disabled>-- Pilih Pajak --</option>
							  <?php 
								if ($stok[0]['pajak'] == 10){ ?>
									
									 <option value="10" selected="selected">10 %</option>
									 <option value="0">0 %</option>
									
							<?php	}else{ ?>
									
									<option value="10">10 %</option>
									<option value="0" selected="selected">0 %</option>
									
							<?php	}
								?>
							
                             
                          </select>
                        </div>
                      </div>

                      <input type="hidden" value="<?php if (isset($stok[0]['weight'])) {
                                                    echo (int) $stok[0]['weight'];
                                                  } ?>" id="weight" name="weight" class="form-control" placeholder="Weight" autocomplete="off">

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gudang">Principal <span class="required"><sup>*</sup></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <select class="form-control" name="gudang" id="gudang" style="width: 100%" required autocomplete="off">
                            <option value="">-- Pilih Principal --</option>
                            <?php foreach ($Gudang as $Gudang) { ?>
                              <option value="<?php echo $Gudang['id']; ?>" <?php if (isset($stok[0]['dist_id'])) {
                                                                              if ($stok[0]['dist_id'] == $Gudang['id']) echo 'selected="selected"';
                                                                            } ?>><?php echo $Gudang['name_eksternal']; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Submit</button>
                          <button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()">Batal</button>
                        </div>
                      </div>
                    </form><!-- /page content -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="PrimaryModalalert" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-close-area modal-close-df">
        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
      </div>
      <div class="modal-body">
        <i class="educate-icon educate-checked modal-check-pro"></i>
        <h2>Data Berhasil Dirubah</h2>
        <p></p>
      </div>
      <!-- <div class="modal-footer">
                                        <a data-dismiss="modal" href="#" onClick="back()">Ya</a>
                                    </div>-->
    </div>
  </div>
</div>


<script>
  function clearform() {

    $('#edit_material').trigger("reset");

  }

  function back() {

    window.location.href = "<?php echo base_url() . 'items'; ?>";

  }

  function listdist() {
    var user_id = '0001';
    var token = '093940349';

  }

  $(document).ready(function() {

    $('#status_i').on('change', function() {
      var status_i = this.value;

      if (status_i == 2) {
        $('#limit').prop('readonly', false);
      } else {
        $('#limit').prop('readonly', true);
      }

    });

    listdist();

    $('#edit_material').on('submit', function(e) {
      e.preventDefault();

      var formData = new FormData(this);
      var urls = $(this).attr('action');

      swal({
        title: 'Yakin akan Merubah Data ?',
        text: '',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak'
      }).then(function() {

        $.ajax({
          type: 'POST',
          url: urls,
          data: formData,
          cache: false,
          contentType: false,
          processData: false,
          success: function(response) {
            if (response.success == true) {
              // $('#PrimaryModalalert').modal('show');
              swal({
                title: 'Success!',
                text: response.message,
                type: 'success',
                showCancelButton: false,
                confirmButtonText: 'Ok'
              }).then(function() {
                back();
              })
            } else {
              $('#msg_err').html(response.message);
              $('#WarningModalftblack').modal('show');

              swal({
                title: 'Data Inputan Tidak Seusai',
                html: response.message,
                type: 'error',
                showCancelButton: true,
                confirmButtonText: 'Ok'
              })
            }
          }
        });

      });

    });
  });
</script>