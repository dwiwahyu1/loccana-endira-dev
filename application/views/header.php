<?php
    $image_base = base_url() . 'assets/ocr/images/';
    $image_back = base_url() . 'assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/gallery/7.jpg';
    // $api   = base_url() . 'api/';
    // $path = base_url() . 'assets/ocr/material/base/';
    // $path2 = base_url() . 'assets/ocr/material/';
?>

<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Distributor & Sales System</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- favicon
		============================================ -->
  <!--<link rel="shortcut icon" type="image/x-icon" href="<?php echo $path; ?>img/favicon.ico">-->
  <link rel="shortcut icon" type="image/x-icon" href="<?php echo $image_base; ?>Logo_Gram_White.png">
  <!-- Google Fonts
		============================================ -->
  <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
  <!-- Bootstrap CSS
		============================================ -->
  <link rel="stylesheet" href="<?php echo $path; ?>css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo $path; ?>css/styleLines.css">
  <!-- Bootstrap CSS
		============================================ -->
  <link rel="stylesheet" href="<?php echo $path; ?>css/font-awesome.min.css">
  <!-- owl.carousel CSS
		============================================ -->
  <link rel="stylesheet" href="<?php echo $path; ?>css/owl.carousel.css">
  <link rel="stylesheet" href="<?php echo $path; ?>css/owl.theme.css">
  <link rel="stylesheet" href="<?php echo $path; ?>css/owl.transitions.css">
  <!-- animate CSS
		============================================ -->
  <link rel="stylesheet" href="<?php echo $path; ?>css/animate.css">
  <!-- normalize CSS
		============================================ -->
  <link rel="stylesheet" href="<?php echo $path; ?>css/normalize.css">
  <!-- meanmenu icon CSS
		============================================ -->
  <link rel="stylesheet" href="<?php echo $path; ?>css/meanmenu.min.css">
  <!-- main CSS
		============================================ -->
  <link rel="stylesheet" href="<?php echo $path; ?>css/main.css">
  <!-- educate icon CSS
		============================================ -->
  <link rel="stylesheet" href="<?php echo $path; ?>css/educate-custon-icon.css">
  <!-- morrisjs CSS
		============================================ -->
  <link rel="stylesheet" href="<?php echo $path; ?>css/morrisjs/morris.css">
  <!-- mCustomScrollbar CSS
		============================================ -->
  <link rel="stylesheet" href="<?php echo $path; ?>css/scrollbar/jquery.mCustomScrollbar.min.css">

  <link rel="stylesheet" href="<?php echo $path; ?>css/datapicker/datepicker3.css">

  <!-- metisMenu CSS
		============================================ -->
  <link rel="stylesheet" href="<?php echo $path; ?>css/metisMenu/metisMenu.min.css">
  <link rel="stylesheet" href="<?php echo $path; ?>css/metisMenu/metisMenu-vertical.css">
  <!-- calendar CSS
		============================================ -->
  <link rel="stylesheet" href="<?php echo $path; ?>css/calendar/fullcalendar.min.css">
  <link rel="stylesheet" href="<?php echo $path; ?>css/calendar/fullcalendar.print.min.css">

  <link rel="stylesheet" href="<?php echo $path; ?>css/modals.css">
  <!-- style CSS
		============================================ -->
  <link rel="stylesheet" href="<?php echo $path; ?>style.css">
  <!-- responsive CSS
		============================================ -->
  <link rel="stylesheet" href="<?php echo $path; ?>css/responsive.css">
   <!-- DataTables -->
  <link href="<?php echo $path2; ?>assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo $path2; ?>assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo $path2; ?>assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo $path2; ?>assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo $path2; ?>assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />
  <!-- Plugins css-->
  <link href="<?php echo $path2; ?>assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css?jsr=<?php echo jsversionstring(); ?>" rel="stylesheet" />
  <link href="<?php echo $path2; ?>assets/plugins/multiselect/css/multi-select.css?jsr=<?php echo jsversionstring(); ?>" rel="stylesheet" type="text/css" />
  <link href="<?php echo $path2; ?>assets/plugins/select2/dist/css/select2.css?jsr=<?php echo jsversionstring(); ?>" rel="stylesheet" type="text/css">
  <link href="<?php echo $path2; ?>assets/plugins/select2/dist/css/select2-bootstrap.css?jsr=<?php echo jsversionstring(); ?>" rel="stylesheet" type="text/css">
  <link href="<?php echo $path2; ?>assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css?jsr=<?php echo jsversionstring(); ?>" rel="stylesheet" />
  <link href="<?php echo $path2; ?>assets/plugins/switchery/switchery.min.css?jsr=<?php echo jsversionstring(); ?>" rel="stylesheet" />
  <link href="<?php echo $path2; ?>assets/plugins/timepicker/bootstrap-timepicker.min.css?jsr=<?php echo jsversionstring(); ?>" rel="stylesheet">
  <link href="<?php echo $path2; ?>assets/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css?jsr=<?php echo jsversionstring(); ?>" rel="stylesheet">
  <link href="<?php echo $path2; ?>assets/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css?jsr=<?php echo jsversionstring(); ?>" rel="stylesheet">
  <link href="<?php echo $path2; ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.css?jsr=<?php echo jsversionstring(); ?>" rel="stylesheet">

  <link href="<?php echo base_url(); ?>assets/sweetalert/sweetalert2.min.css" rel="stylesheet" type="text/css" />
  
  <!-- curency -->

    <script src="<?php echo $path; ?>js/vendor/jquery-1.12.4.min.js" type="text/javascript"></script>
    <!-- bootstrap JS
    ============================================ -->
    <script src="<?php echo $path; ?>js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo $path2;?>assets/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>
    <script src="<?php echo $path;?>Jquery-Price-Format/jquery.priceformat.min.js" type="text/javascript"></script>

    <script type="text/javascript">
    var x = 0;
    var jumlah = 0;
    var data = [];

    $(document).ready(function() {
            $('#pulses-green').addClass('x-hidden');
            $('#pulses-green').removeClass('pulses-green');
            $('#pulses-warning').addClass('x-hidden');
            $('#pulses-warning').removeClass('pulses-warning');
            $('#pulses-red').addClass('x-hidden');
            $('#pulses-red').removeClass('pulses-red');

            setInterval(MeasureConnectionSpeed, 5000);
        });

        var imageAddr = "<?= $image_base; ?>Landscape_Blue.png"; 
        var downloadSize = 11800; //bytes

        function ShowProgressMessage(msg) {
            if (console) {
                if (typeof msg == "string") {
                    // console.log(msg);
                } else {
                    for (var i = 0; i < msg.length; i++) {
                        // console.log(msg[i]);
                    }
                }
            }
            
            var oProgress = document.getElementById("progress");
            if (oProgress) {
                var actualHTML = (typeof msg == "string") ? msg : msg.join("<br />");
                oProgress.innerHTML = actualHTML;
            }
        }

        function InitiateSpeedDetection() {
            ShowProgressMessage("Loading the image, please wait...");
            window.setTimeout(MeasureConnectionSpeed, 1);
        };    

        if (window.addEventListener) {
            window.addEventListener('load', InitiateSpeedDetection, false);
        } else if (window.attachEvent) {
            window.attachEvent('onload', InitiateSpeedDetection);
        }

        function MeasureConnectionSpeed() {
            var startTime, endTime;
            var download = new Image();
            download.onload = function () {
                endTime = (new Date()).getTime();
                showResults();
            }
            
            download.onerror = function (err, msg) {
                ShowProgressMessage("Invalid image, or error downloading");
            }
            
            startTime = (new Date()).getTime();
            var cacheBuster = "?nnn=" + startTime;
            download.src = imageAddr + cacheBuster;
            
            function showResults() {
                var duration = (endTime - startTime) / 1000;
                var bitsLoaded = downloadSize * 8;
                var speedBps = (bitsLoaded / duration).toFixed(2);
                var speedKbps = (speedBps / 1024).toFixed(2);
                var speedMbps = (speedKbps / 1024).toFixed(2);
                ShowProgressMessage([
                    "Your connection speed is:", 
                    speedBps + " bps", 
                    speedKbps + " kbps", 
                    speedMbps + " Mbps"
                ]);
                
                if(speedMbps < 1) {
                $('#pulses-meter').removeClass('pulses-warning');
                $('#pulses-meter').removeClass('pulses-green');
                $('#pulses-meter').removeClass('x-hidden');
                $('#pulses-meter').addClass('pulses-red');

                jumlah = jumlah + 1;
                if(jumlah == 1) {
                    swal('Information', 'Mohon untuk tidak malakukan penginputan transaksi sementara, jika koneksi belum stabil','info');
                }
                }else if(speedMbps < 3) {
                jumlah = 0;
                $('#pulses-meter').addClass('pulses-warning');
                $('#pulses-meter').removeClass('pulses-red');
                $('#pulses-meter').removeClass('pulses-green');
                $('#pulses-meter').removeClass('x-hidden');
                }else{
                jumlah = 0;
                $('#pulses-meter').addClass('pulses-green');
                $('#pulses-meter').removeClass('pulses-red');
                $('#pulses-meter').removeClass('pulses-warning');
                $('#pulses-meter').removeClass('x-hidden');
                }

                $('#meter-connection').text(speedMbps + " Mbps");
            }
        }
    </script>

    <style>
        th {
            text-align: center
        }
        .not{
            float: left;
            /* margin: 2px; */
        }
        .meter{
            float: right;
            margin-top: 2px;
            /* margin-bottom: 2px; */
            margin-left: 2px;
            margin-right: 2px;
            color: white;
            font-size: initial;
        }
        .pulses-warning {
            margin-top: 2px;
            /* margin-bottom: 2px; */
            margin-left: 2px;
            margin-right: 2px;
            display: block;
            width: 22px;
            height: 22px;
            border-radius: 50%;
            background: #cca92c;
            cursor: pointer;
            box-shadow: 0 0 0 rgba(204,169,44, 0.4);
            animation: pulses-warning 2s infinite;
        }
        .pulses-warning:hover {
            animation: none;
        }

        @-webkit-keyframes pulses-warning {
            0% {
                -webkit-box-shadow: 0 0 0 0 rgba(204,169,44, 0.4);
            }
            70% {
                -webkit-box-shadow: 0 0 0 10px rgba(204,169,44, 0);
            }
            100% {
                -webkit-box-shadow: 0 0 0 0 rgba(204,169,44, 0);
            }
        }
        @keyframes pulses-warning {
            0% {
                -moz-box-shadow: 0 0 0 0 rgba(204,169,44, 0.4);
                box-shadow: 0 0 0 0 rgba(204,169,44, 0.4);
            }
            70% {
                -moz-box-shadow: 0 0 0 10px rgba(204,169,44, 0);
                box-shadow: 0 0 0 10px rgba(204,169,44, 0);
            }
            100% {
                -moz-box-shadow: 0 0 0 0 rgba(204,169,44, 0);
                box-shadow: 0 0 0 0 rgba(204,169,44, 0);
            }
        }
        .pulses-red {
            margin-top: 2px;
            /* margin-bottom: 2px; */
            margin-left: 2px;
            margin-right: 2px;
            display: block;
            width: 22px;
            height: 22px;
            /* border-radius: 50%; */
            background: #f11909;
            cursor: pointer;
            box-shadow: 0 0 0 rgba(241,25,9, 0.4);
            animation: pulses-red 2s infinite;
        }
        .pulses-red:hover {
            animation: none;
        }

        @-webkit-keyframes pulses-red {
            0% {
                -webkit-box-shadow: 0 0 0 0 rgba(241,25,9, 0.4);
            }
            70% {
                -webkit-box-shadow: 0 0 0 10px rgba(241,25,9, 0);
            }
            100% {
                -webkit-box-shadow: 0 0 0 0 rgba(241,25,9, 0);
            }
        }
        @keyframes pulses-red {
            0% {
                -moz-box-shadow: 0 0 0 0 rgba(241,25,9, 0.4);
                box-shadow: 0 0 0 0 rgba(241,25,9, 0.4);
            }
            70% {
                -moz-box-shadow: 0 0 0 10px rgba(241,25,9, 0);
                box-shadow: 0 0 0 10px rgba(241,25,9, 0);
            }
            100% {
                -moz-box-shadow: 0 0 0 0 rgba(241,25,9, 0);
                box-shadow: 0 0 0 0 rgba(241,25,9, 0);
            }
        }
        .pulses-green {
            margin-top: 2px;
            /* margin-bottom: 2px; */
            margin-left: 2px;
            margin-right: 2px;
            display: block;
            width: 22px;
            height: 22px;
            /* border-radius: 50%; */
            background: #09ed70;
            cursor: pointer;
            box-shadow: 0 0 0 rgba(9,237,112, 0.4);
            animation: pulses-green 2s infinite;
        }
        .pulses-green:hover {
            animation: none;
        }

        @-webkit-keyframes pulses-green {
            0% {
                -webkit-box-shadow: 0 0 0 0 rgba(9,237,112, 0.4);
            }
            70% {
                -webkit-box-shadow: 0 0 0 10px rgba(9,237,112, 0);
            }
            100% {
                -webkit-box-shadow: 0 0 0 0 rgba(9,237,112, 0);
            }
        }
        @keyframes pulses-green {
            0% {
                -moz-box-shadow: 0 0 0 0 rgba(9,237,112, 0.4);
                box-shadow: 0 0 0 0 rgba(9,237,112, 0.4);
            }
            70% {
                -moz-box-shadow: 0 0 0 10px rgba(9,237,112, 0);
                box-shadow: 0 0 0 10px rgba(9,237,112, 0);
            }
            100% {
                -moz-box-shadow: 0 0 0 0 rgba(9,237,112, 0);
                box-shadow: 0 0 0 0 rgba(9,237,112, 0);
            }
        }
        .meter-conn {
            margin  : 0px 0px 0px 0px;
        }
    </style>
	<!-- modernizr JS
		============================================ -->
    <script src="<?php echo $path; ?>js/vendor/modernizr-2.8.3.min.js" type="text/javascript"></script>
</head>


<?php
$user_id = $this->session->userdata('user_id');
$id_role = $this->session->userdata('id_role');
$image = $this->session->userdata('image');

//print_r($this->session->userdata['logged_in']);
?>

<body>

  <!-- Begin page -->
  <div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">

      <!-- LOGO -->
      <!-- <div class="topbar-left">
        <a href="<?php echo base_url(); ?>"><img class="main-logo" src="<?php echo $path; ?>img/logo/logo.png" alt="" /></a>
        <strong><a href="<?php echo base_url(); ?>"><img src="<?php echo $path; ?>img/logo/logosn.png" alt="" /></a></strong>
      </div> -->

      <!-- Button mobile view to collapse sidebar menu -->
    </div>
    <!-- Top Bar End -->

    <?php include(APPPATH . 'views/menu.php'); ?>

    <div class="all-content-wrapper">
      <div class="container-fluid" style="margin-bottom:80px;">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <!--  <div class="logo-pro">
                       <a href="index.html"><img class="main-logo" src="img/logo/logo.png" alt="" /><h4 id="namamenu"></h4></a>
                    </div> -->
          </div>
        </div>
      </div>
      <div class="header-advance-area">
        <div class="header-top-area">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="header-top-wraper">
                  <div class="row">
                    <div class="col-lg-1 col-md-0 col-sm-1 col-xs-12">
                      <div class="menu-switcher-pro">
                        <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
                          <i class="educate-icon educate-nav"></i>
                        </button>
                      </div>
                    </div>
                    <div class="col-lg-6 col-md-7 col-sm-6 col-xs-12">
                      <div class="header-top-menu tabl-d-n">
                        <ul class="nav navbar-nav mai-top-nav">
                        <!--  <li class="nav-item"><a href="#" class="nav-link">Home</a>
                          </li>
                          <li class="nav-item"><a href="#" class="nav-link">About</a>
                          </li>
                          <li class="nav-item"><a href="#" class="nav-link">Services</a>
                          </li>
                          <li class="nav-item dropdown res-dis-nn">
                            <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">Project <span class="angle-down-topmenu"><i class="fa fa-angle-down"></i></span></a>
                            <div role="menu" class="dropdown-menu animated zoomIn">
                              <a href="#" class="dropdown-item">Documentation</a>
                              <a href="#" class="dropdown-item">Expert Backend</a>
                              <a href="#" class="dropdown-item">Expert FrontEnd</a>
                              <a href="#" class="dropdown-item">Contact Support</a>
                            </div>
                          </li>
                          <li class="nav-item"><a href="#" class="nav-link">Support</a>
                          </li> -->
                        </ul>
                      </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                      <div class="header-right-info">
                        <ul class="nav navbar-nav mai-top-nav header-right-menu">
                          <!-- <li class="nav-item dropdown">
                                                    <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="educate-icon educate-message edu-chat-pro" aria-hidden="true"></i><span class="indicator-ms"></span></a>
                                                    <div role="menu" class="author-message-top dropdown-menu animated zoomIn">
                                                        <div class="message-single-top">
                                                            <h1>Message</h1>
                                                        </div>
                                                        <ul class="message-menu">
                                                            <li>
                                                                <a href="#">
                                                                    <div class="message-img">
                                                                        <img src="img/contact/1.jpg" alt="">
                                                                    </div>
                                                                    <div class="message-content">
                                                                        <span class="message-date">16 Sept</span>
                                                                        <h2>Advanda Cro</h2>
                                                                        <p>Please done this project as soon possible.</p>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <div class="message-img">
                                                                        <img src="img/contact/4.jpg" alt="">
                                                                    </div>
                                                                    <div class="message-content">
                                                                        <span class="message-date">16 Sept</span>
                                                                        <h2>Sulaiman din</h2>
                                                                        <p>Please done this project as soon possible.</p>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <div class="message-img">
                                                                        <img src="img/contact/3.jpg" alt="">
                                                                    </div>
                                                                    <div class="message-content">
                                                                        <span class="message-date">16 Sept</span>
                                                                        <h2>Victor Jara</h2>
                                                                        <p>Please done this project as soon possible.</p>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <div class="message-img">
                                                                        <img src="img/contact/2.jpg" alt="">
                                                                    </div>
                                                                    <div class="message-content">
                                                                        <span class="message-date">16 Sept</span>
                                                                        <h2>Victor Jara</h2>
                                                                        <p>Please done this project as soon possible.</p>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <div class="message-view">
                                                            <a href="#">View All Messages</a>
                                                        </div>
                                                    </div>
                                                </li> -->
                          <!-- <li class="nav-item"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="educate-icon educate-bell" aria-hidden="true"></i><span class="indicator-nt"></span></a>
                                                    <div role="menu" class="notification-author dropdown-menu animated zoomIn">
                                                        <div class="notification-single-top">
                                                            <h1>Notifications</h1>
                                                        </div>
                                                        <ul class="notification-menu">
                                                            <li>
                                                                <a href="#">
                                                                    <div class="notification-icon">
                                                                        <i class="educate-icon educate-checked edu-checked-pro admin-check-pro" aria-hidden="true"></i>
                                                                    </div>
                                                                    <div class="notification-content">
                                                                        <span class="notification-date">16 Sept</span>
                                                                        <h2>Advanda Cro</h2>
                                                                        <p>Please done this project as soon possible.</p>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <div class="notification-icon">
                                                                        <i class="fa fa-cloud edu-cloud-computing-down" aria-hidden="true"></i>
                                                                    </div>
                                                                    <div class="notification-content">
                                                                        <span class="notification-date">16 Sept</span>
                                                                        <h2>Sulaiman din</h2>
                                                                        <p>Please done this project as soon possible.</p>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <div class="notification-icon">
                                                                        <i class="fa fa-eraser edu-shield" aria-hidden="true"></i>
                                                                    </div>
                                                                    <div class="notification-content">
                                                                        <span class="notification-date">16 Sept</span>
                                                                        <h2>Victor Jara</h2>
                                                                        <p>Please done this project as soon possible.</p>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a href="#">
                                                                    <div class="notification-icon">
                                                                        <i class="fa fa-line-chart edu-analytics-arrow" aria-hidden="true"></i>
                                                                    </div>
                                                                    <div class="notification-content">
                                                                        <span class="notification-date">16 Sept</span>
                                                                        <h2>Victor Jara</h2>
                                                                        <p>Please done this project as soon possible.</p>
                                                                    </div>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <div class="notification-view">
                                                            <a href="#">View All Notification</a>
                                                        </div>
                                                    </div>
                                                </li> -->
                                                <li class="nav-item">
                                                    <div class="form-group">
                                                        <div class="not">
                                                            <span id="pulses-meter"></span>
                                                        </div>
                                                        <div class="meter">
                                                            <p id="meter-connection" class="meter-conn"></p>
                                                        </div>
                                                    </div>
                                                    <!-- <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                                        <i class="educate-icon educate-bell" aria-hidden="true"></i>
                                                        <span class="indicator-nt"></span>
                                                    </a>
                                                    <div role="menu" class="notification-author dropdown-menu animated zoomIn">
                                                        <div class="notification-single-top">
                                                            <h1>Notifications</h1>
                                                        </div>
                                                    </div> -->
                                                </li>
                          <li class="nav-item">
                            <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                              <img src="<?php echo  $this->session->userdata['logged_in']['profile_picture']; ?>" alt="" />
                              <span class="admin-name"><?php echo  $this->session->userdata['logged_in']['nama']; ?></span>
                              <i class="fa fa-angle-down edu-icon edu-down-arrow"></i>
                            </a><br>
                            <!--<p><?php echo  $this->session->userdata['logged_in']['name_role']; ?></b>-->
                            <ul role="menu" class="dropdown-header-top author-log dropdown-menu animated zoomIn">

                              <li><a href="<?= base_url('profile'); ?>"><span class="edu-icon edu-user-rounded author-log-ic"></span>My Profile</a>
                              </li>
                              <li><a href="<?php echo base_url(); ?>logout"><span class="edu-icon edu-locked author-log-ic"></span>Log Out</a>
                              </li>
                            </ul>
                          </li>
                          <!--<li class="nav-item nav-setting-open"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="educate-icon educate-menu"></i></a>

                                                    <div role="menu" class="admintab-wrap menu-setting-wrap menu-setting-wrap-bg dropdown-menu animated zoomIn">
                                                        <ul class="nav nav-tabs custon-set-tab">
                                                            <li class="active"><a data-toggle="tab" href="#Notes">Notes</a>
                                                            </li>
                                                            <li><a data-toggle="tab" href="#Projects">Projects</a>
                                                            </li>
                                                            <li><a data-toggle="tab" href="#Settings">Settings</a>
                                                            </li>
                                                        </ul>

                                                        <div class="tab-content custom-bdr-nt">
                                                            <div id="Notes" class="tab-pane fade in active">
                                                                <div class="notes-area-wrap">
                                                                    <div class="note-heading-indicate">
                                                                        <h2><i class="fa fa-comments-o"></i> Latest Notes</h2>
                                                                        <p>You have 10 new message.</p>
                                                                    </div>
                                                                    <div class="notes-list-area notes-menu-scrollbar">
                                                                        <ul class="notes-menu-list">
                                                                            <li>
                                                                                <a href="#">
                                                                                    <div class="notes-list-flow">
                                                                                        <div class="notes-img">
                                                                                            <img src="img/contact/4.jpg" alt="" />
                                                                                        </div>
                                                                                        <div class="notes-content">
                                                                                            <p> The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                                                            <span>Yesterday 2:45 pm</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    <div class="notes-list-flow">
                                                                                        <div class="notes-img">
                                                                                            <img src="img/contact/1.jpg" alt="" />
                                                                                        </div>
                                                                                        <div class="notes-content">
                                                                                            <p> The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                                                            <span>Yesterday 2:45 pm</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    <div class="notes-list-flow">
                                                                                        <div class="notes-img">
                                                                                            <img src="img/contact/2.jpg" alt="" />
                                                                                        </div>
                                                                                        <div class="notes-content">
                                                                                            <p> The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                                                            <span>Yesterday 2:45 pm</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    <div class="notes-list-flow">
                                                                                        <div class="notes-img">
                                                                                            <img src="img/contact/3.jpg" alt="" />
                                                                                        </div>
                                                                                        <div class="notes-content">
                                                                                            <p> The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                                                            <span>Yesterday 2:45 pm</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    <div class="notes-list-flow">
                                                                                        <div class="notes-img">
                                                                                            <img src="img/contact/4.jpg" alt="" />
                                                                                        </div>
                                                                                        <div class="notes-content">
                                                                                            <p> The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                                                            <span>Yesterday 2:45 pm</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    <div class="notes-list-flow">
                                                                                        <div class="notes-img">
                                                                                            <img src="img/contact/1.jpg" alt="" />
                                                                                        </div>
                                                                                        <div class="notes-content">
                                                                                            <p> The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                                                            <span>Yesterday 2:45 pm</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    <div class="notes-list-flow">
                                                                                        <div class="notes-img">
                                                                                            <img src="img/contact/2.jpg" alt="" />
                                                                                        </div>
                                                                                        <div class="notes-content">
                                                                                            <p> The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                                                            <span>Yesterday 2:45 pm</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    <div class="notes-list-flow">
                                                                                        <div class="notes-img">
                                                                                            <img src="img/contact/1.jpg" alt="" />
                                                                                        </div>
                                                                                        <div class="notes-content">
                                                                                            <p> The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                                                            <span>Yesterday 2:45 pm</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    <div class="notes-list-flow">
                                                                                        <div class="notes-img">
                                                                                            <img src="img/contact/2.jpg" alt="" />
                                                                                        </div>
                                                                                        <div class="notes-content">
                                                                                            <p> The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                                                            <span>Yesterday 2:45 pm</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    <div class="notes-list-flow">
                                                                                        <div class="notes-img">
                                                                                            <img src="img/contact/3.jpg" alt="" />
                                                                                        </div>
                                                                                        <div class="notes-content">
                                                                                            <p> The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                                                                                            <span>Yesterday 2:45 pm</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="Projects" class="tab-pane fade">
                                                                <div class="projects-settings-wrap">
                                                                    <div class="note-heading-indicate">
                                                                        <h2><i class="fa fa-cube"></i> Latest projects</h2>
                                                                        <p> You have 20 projects. 5 not completed.</p>
                                                                    </div>
                                                                    <div class="project-st-list-area project-st-menu-scrollbar">
                                                                        <ul class="projects-st-menu-list">
                                                                            <li>
                                                                                <a href="#">
                                                                                    <div class="project-list-flow">
                                                                                        <div class="projects-st-heading">
                                                                                            <h2>Web Development</h2>
                                                                                            <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                                                                            <span class="project-st-time">1 hours ago</span>
                                                                                        </div>
                                                                                        <div class="projects-st-content">
                                                                                            <p>Completion with: 28%</p>
                                                                                            <div class="progress progress-mini">
                                                                                                <div style="width: 28%;" class="progress-bar progress-bar-danger hd-tp-1"></div>
                                                                                            </div>
                                                                                            <p>Project end: 4:00 pm - 12.06.2014</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    <div class="project-list-flow">
                                                                                        <div class="projects-st-heading">
                                                                                            <h2>Software Development</h2>
                                                                                            <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                                                                            <span class="project-st-time">2 hours ago</span>
                                                                                        </div>
                                                                                        <div class="projects-st-content project-rating-cl">
                                                                                            <p>Completion with: 68%</p>
                                                                                            <div class="progress progress-mini">
                                                                                                <div style="width: 68%;" class="progress-bar hd-tp-2"></div>
                                                                                            </div>
                                                                                            <p>Project end: 4:00 pm - 12.06.2014</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    <div class="project-list-flow">
                                                                                        <div class="projects-st-heading">
                                                                                            <h2>Graphic Design</h2>
                                                                                            <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                                                                            <span class="project-st-time">3 hours ago</span>
                                                                                        </div>
                                                                                        <div class="projects-st-content">
                                                                                            <p>Completion with: 78%</p>
                                                                                            <div class="progress progress-mini">
                                                                                                <div style="width: 78%;" class="progress-bar hd-tp-3"></div>
                                                                                            </div>
                                                                                            <p>Project end: 4:00 pm - 12.06.2014</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    <div class="project-list-flow">
                                                                                        <div class="projects-st-heading">
                                                                                            <h2>Web Design</h2>
                                                                                            <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                                                                            <span class="project-st-time">4 hours ago</span>
                                                                                        </div>
                                                                                        <div class="projects-st-content project-rating-cl2">
                                                                                            <p>Completion with: 38%</p>
                                                                                            <div class="progress progress-mini">
                                                                                                <div style="width: 38%;" class="progress-bar progress-bar-danger hd-tp-4"></div>
                                                                                            </div>
                                                                                            <p>Project end: 4:00 pm - 12.06.2014</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    <div class="project-list-flow">
                                                                                        <div class="projects-st-heading">
                                                                                            <h2>Business Card</h2>
                                                                                            <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                                                                            <span class="project-st-time">5 hours ago</span>
                                                                                        </div>
                                                                                        <div class="projects-st-content">
                                                                                            <p>Completion with: 28%</p>
                                                                                            <div class="progress progress-mini">
                                                                                                <div style="width: 28%;" class="progress-bar progress-bar-danger hd-tp-5"></div>
                                                                                            </div>
                                                                                            <p>Project end: 4:00 pm - 12.06.2014</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    <div class="project-list-flow">
                                                                                        <div class="projects-st-heading">
                                                                                            <h2>Ecommerce Business</h2>
                                                                                            <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                                                                            <span class="project-st-time">6 hours ago</span>
                                                                                        </div>
                                                                                        <div class="projects-st-content project-rating-cl">
                                                                                            <p>Completion with: 68%</p>
                                                                                            <div class="progress progress-mini">
                                                                                                <div style="width: 68%;" class="progress-bar hd-tp-6"></div>
                                                                                            </div>
                                                                                            <p>Project end: 4:00 pm - 12.06.2014</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    <div class="project-list-flow">
                                                                                        <div class="projects-st-heading">
                                                                                            <h2>Woocommerce Plugin</h2>
                                                                                            <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                                                                            <span class="project-st-time">7 hours ago</span>
                                                                                        </div>
                                                                                        <div class="projects-st-content">
                                                                                            <p>Completion with: 78%</p>
                                                                                            <div class="progress progress-mini">
                                                                                                <div style="width: 78%;" class="progress-bar"></div>
                                                                                            </div>
                                                                                            <p>Project end: 4:00 pm - 12.06.2014</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="#">
                                                                                    <div class="project-list-flow">
                                                                                        <div class="projects-st-heading">
                                                                                            <h2>Wordpress Theme</h2>
                                                                                            <p> The point of using Lorem Ipsum is that it has a more or less normal.</p>
                                                                                            <span class="project-st-time">9 hours ago</span>
                                                                                        </div>
                                                                                        <div class="projects-st-content project-rating-cl2">
                                                                                            <p>Completion with: 38%</p>
                                                                                            <div class="progress progress-mini">
                                                                                                <div style="width: 38%;" class="progress-bar progress-bar-danger"></div>
                                                                                            </div>
                                                                                            <p>Project end: 4:00 pm - 12.06.2014</p>
                                                                                        </div>
                                                                                    </div>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div id="Settings" class="tab-pane fade">
                                                                <div class="setting-panel-area">
                                                                    <div class="note-heading-indicate">
                                                                        <h2><i class="fa fa-gears"></i> Settings Panel</h2>
                                                                        <p> You have 20 Settings. 5 not completed.</p>
                                                                    </div>
                                                                    <ul class="setting-panel-list">
                                                                        <li>
                                                                            <div class="checkbox-setting-pro">
                                                                                <div class="checkbox-title-pro">
                                                                                    <h2>Show notifications</h2>
                                                                                    <div class="ts-custom-check">
                                                                                        <div class="onoffswitch">
                                                                                            <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="example">
                                                                                            <label class="onoffswitch-label" for="example">
																									<span class="onoffswitch-inner"></span>
																									<span class="onoffswitch-switch"></span>
																								</label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="checkbox-setting-pro">
                                                                                <div class="checkbox-title-pro">
                                                                                    <h2>Disable Chat</h2>
                                                                                    <div class="ts-custom-check">
                                                                                        <div class="onoffswitch">
                                                                                            <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="example3">
                                                                                            <label class="onoffswitch-label" for="example3">
																									<span class="onoffswitch-inner"></span>
																									<span class="onoffswitch-switch"></span>
																								</label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="checkbox-setting-pro">
                                                                                <div class="checkbox-title-pro">
                                                                                    <h2>Enable history</h2>
                                                                                    <div class="ts-custom-check">
                                                                                        <div class="onoffswitch">
                                                                                            <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="example4">
                                                                                            <label class="onoffswitch-label" for="example4">
																									<span class="onoffswitch-inner"></span>
																									<span class="onoffswitch-switch"></span>
																								</label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="checkbox-setting-pro">
                                                                                <div class="checkbox-title-pro">
                                                                                    <h2>Show charts</h2>
                                                                                    <div class="ts-custom-check">
                                                                                        <div class="onoffswitch">
                                                                                            <input type="checkbox" name="collapsemenu" class="onoffswitch-checkbox" id="example7">
                                                                                            <label class="onoffswitch-label" for="example7">
																									<span class="onoffswitch-inner"></span>
																									<span class="onoffswitch-switch"></span>
																								</label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="checkbox-setting-pro">
                                                                                <div class="checkbox-title-pro">
                                                                                    <h2>Update everyday</h2>
                                                                                    <div class="ts-custom-check">
                                                                                        <div class="onoffswitch">
                                                                                            <input type="checkbox" name="collapsemenu" checked="" class="onoffswitch-checkbox" id="example2">
                                                                                            <label class="onoffswitch-label" for="example2">
																									<span class="onoffswitch-inner"></span>
																									<span class="onoffswitch-switch"></span>
																								</label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="checkbox-setting-pro">
                                                                                <div class="checkbox-title-pro">
                                                                                    <h2>Global search</h2>
                                                                                    <div class="ts-custom-check">
                                                                                        <div class="onoffswitch">
                                                                                            <input type="checkbox" name="collapsemenu" checked="" class="onoffswitch-checkbox" id="example6">
                                                                                            <label class="onoffswitch-label" for="example6">
																									<span class="onoffswitch-inner"></span>
																									<span class="onoffswitch-switch"></span>
																								</label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                        <li>
                                                                            <div class="checkbox-setting-pro">
                                                                                <div class="checkbox-title-pro">
                                                                                    <h2>Offline users</h2>
                                                                                    <div class="ts-custom-check">
                                                                                        <div class="onoffswitch">
                                                                                            <input type="checkbox" name="collapsemenu" checked="" class="onoffswitch-checkbox" id="example5">
                                                                                            <label class="onoffswitch-label" for="example5">
																									<span class="onoffswitch-inner"></span>
																									<span class="onoffswitch-switch"></span>
																								</label>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    </ul>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>-->
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> 
		  <?php include(APPPATH . 'views/menu_mobile.php'); ?>
        <!-- Mobile Menu start
		<div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">  
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul class="mobile-menu-nav">
                                        <li><a data-toggle="collapse" data-target="#Charts" href="#">Home <span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul class="collapse dropdown-header-top">
                                                <li><a href="index.html">Dashboard v.1</a></li>
                                                <li><a href="index-1.html">Dashboard v.2</a></li>
                                                <li><a href="index-3.html">Dashboard v.3</a></li>
                                                <li><a href="analytics.html">Analytics</a></li>
                                                <li><a href="widgets.html">Widgets</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="events.html">Event</a></li>
                                        <li><a data-toggle="collapse" data-target="#demoevent" href="#">Professors <span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul id="demoevent" class="collapse dropdown-header-top">
                                                <li><a href="all-professors.html">All Professors</a>
                                                </li>
                                                <li><a href="add-professor.html">Add Professor</a>
                                                </li>
                                                <li><a href="edit-professor.html">Edit Professor</a>
                                                </li>
                                                <li><a href="professor-profile.html">Professor Profile</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a data-toggle="collapse" data-target="#demopro" href="#">Students <span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul id="demopro" class="collapse dropdown-header-top">
                                                <li><a href="all-students.html">All Students</a>
                                                </li>
                                                <li><a href="add-student.html">Add Student</a>
                                                </li>
                                                <li><a href="edit-student.html">Edit Student</a>
                                                </li>
                                                <li><a href="student-profile.html">Student Profile</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a data-toggle="collapse" data-target="#democrou" href="#">Courses <span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul id="democrou" class="collapse dropdown-header-top">
                                                <li><a href="all-courses.html">All Courses</a>
                                                </li>
                                                <li><a href="add-course.html">Add Course</a>
                                                </li>
                                                <li><a href="edit-course.html">Edit Course</a>
                                                </li>
                                                <li><a href="course-profile.html">Courses Info</a>
                                                </li>
                                                <li><a href="course-payment.html">Courses Payment</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a data-toggle="collapse" data-target="#demolibra" href="#">Library <span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul id="demolibra" class="collapse dropdown-header-top">
                                                <li><a href="library-assets.html">Library Assets</a>
                                                </li>
                                                <li><a href="add-library-assets.html">Add Library Asset</a>
                                                </li>
                                                <li><a href="edit-library-assets.html">Edit Library Asset</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a data-toggle="collapse" data-target="#demodepart" href="#">Departments <span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul id="demodepart" class="collapse dropdown-header-top">
                                                <li><a href="departments.html">Departments List</a>
                                                </li>
                                                <li><a href="add-department.html">Add Departments</a>
                                                </li>
                                                <li><a href="edit-department.html">Edit Departments</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a data-toggle="collapse" data-target="#demomi" href="#">Mailbox <span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul id="demomi" class="collapse dropdown-header-top">
                                                <li><a href="mailbox.html">Inbox</a>
                                                </li>
                                                <li><a href="mailbox-view.html">View Mail</a>
                                                </li>
                                                <li><a href="mailbox-compose.html">Compose Mail</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a data-toggle="collapse" data-target="#Miscellaneousmob" href="#">Interface <span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul id="Miscellaneousmob" class="collapse dropdown-header-top">
                                                <li><a href="google-map.html">Google Map</a>
                                                </li>
                                                <li><a href="data-maps.html">Data Maps</a>
                                                </li>
                                                <li><a href="pdf-viewer.html">Pdf Viewer</a>
                                                </li>
                                                <li><a href="x-editable.html">X-Editable</a>
                                                </li>
                                                <li><a href="code-editor.html">Code Editor</a>
                                                </li>
                                                <li><a href="tree-view.html">Tree View</a>
                                                </li>
                                                <li><a href="preloader.html">Preloader</a>
                                                </li>
                                                <li><a href="images-cropper.html">Images Cropper</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a data-toggle="collapse" data-target="#Chartsmob" href="#">Charts <span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul id="Chartsmob" class="collapse dropdown-header-top">
                                                <li><a href="bar-charts.html">Bar Charts</a>
                                                </li>
                                                <li><a href="line-charts.html">Line Charts</a>
                                                </li>
                                                <li><a href="area-charts.html">Area Charts</a>
                                                </li>
                                                <li><a href="rounded-chart.html">Rounded Charts</a>
                                                </li>
                                                <li><a href="c3.html">C3 Charts</a>
                                                </li>
                                                <li><a href="sparkline.html">Sparkline Charts</a>
                                                </li>
                                                <li><a href="peity.html">Peity Charts</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a data-toggle="collapse" data-target="#Tablesmob" href="#">Tables <span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul id="Tablesmob" class="collapse dropdown-header-top">
                                                <li><a href="static-table.html">Static Table</a>
                                                </li>
                                                <li><a href="data-table.html">Data Table</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a data-toggle="collapse" data-target="#formsmob" href="#">Forms <span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul id="formsmob" class="collapse dropdown-header-top">
                                                <li><a href="basic-form-element.html">Basic Form Elements</a>
                                                </li>
                                                <li><a href="advance-form-element.html">Advanced Form Elements</a>
                                                </li>
                                                <li><a href="password-meter.html">Password Meter</a>
                                                </li>
                                                <li><a href="multi-upload.html">Multi Upload</a>
                                                </li>
                                                <li><a href="tinymc.html">Text Editor</a>
                                                </li>
                                                <li><a href="dual-list-box.html">Dual List Box</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a data-toggle="collapse" data-target="#Appviewsmob" href="#">App views <span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul id="Appviewsmob" class="collapse dropdown-header-top">
                                                <li><a href="basic-form-element.html">Basic Form Elements</a>
                                                </li>
                                                <li><a href="advance-form-element.html">Advanced Form Elements</a>
                                                </li>
                                                <li><a href="password-meter.html">Password Meter</a>
                                                </li>
                                                <li><a href="multi-upload.html">Multi Upload</a>
                                                </li>
                                                <li><a href="tinymc.html">Text Editor</a>
                                                </li>
                                                <li><a href="dual-list-box.html">Dual List Box</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a data-toggle="collapse" data-target="#Pagemob" href="#">Pages <span class="admin-project-icon edu-icon edu-down-arrow"></span></a>
                                            <ul id="Pagemob" class="collapse dropdown-header-top">
                                                <li><a href="login.html">Login</a>
                                                </li>
                                                <li><a href="register.html">Register</a>
                                                </li>
                                                <li><a href="lock.html">Lock</a>
                                                </li>
                                                <li><a href="password-recovery.html">Password Recovery</a>
                                                </li>
                                                <li><a href="404.html">404 Page</a></li>
                                                <li><a href="500.html">500 Page</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        Mobile Menu end -->

      </div>
	    
 <!-- wow JS
		============================================ -->
  <script src="<?php echo $path; ?>js/wow.min.js" type="text/javascript"></script>
  <!-- price-slider JS
		============================================ -->
  <script src="<?php echo $path; ?>js/jquery-price-slider.js" type="text/javascript"></script>
  <!-- meanmenu JS
		============================================ -->
  <script src="<?php echo $path; ?>js/jquery.meanmenu.js" type="text/javascript"></script>
  <!-- owl.carousel JS
		============================================ -->
  <script src="<?php echo $path; ?>js/owl.carousel.min.js" type="text/javascript"></script>
  <!-- sticky JS
		============================================ -->
  <script src="<?php echo $path; ?>js/jquery.sticky.js" type="text/javascript"></script>
  <!-- scrollUp JS
		============================================ -->
  <script src="<?php echo $path; ?>js/jquery.scrollUp.min.js" type="text/javascript"></script>
  <!-- counterup JS
		============================================ -->
  <script src="<?php echo $path; ?>js/counterup/jquery.counterup.min.js" type="text/javascript"></script>
  <script src="<?php echo $path; ?>js/counterup/waypoints.min.js" type="text/javascript"></script>
  <script src="<?php echo $path; ?>js/counterup/counterup-active.js" type="text/javascript"></script>
  <!-- mCustomScrollbar JS
		============================================ -->
  <script src="<?php echo $path; ?>js/scrollbar/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
  <script src="<?php echo $path; ?>js/scrollbar/mCustomScrollbar-active.js" type="text/javascript"></script>
  <!-- metisMenu JS
		============================================ -->
  <script src="<?php echo $path; ?>js/metisMenu/metisMenu.min.js" type="text/javascript"></script>
  <script src="<?php echo $path; ?>js/metisMenu/metisMenu-active.js" type="text/javascript"></script>
  <!-- morrisjs JS
		============================================ -->
  <script src="<?php echo $path; ?>js/morrisjs/raphael-min.js" type="text/javascript"></script>
  <script src="<?php echo $path; ?>js/morrisjs/morris.js" type="text/javascript"></script>
  <script src="<?php echo $path; ?>js/morrisjs/morris-active.js" type="text/javascript"></script>
  <!-- morrisjs JS
		============================================ -->
  <script src="<?php echo $path; ?>js/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
  <script src="<?php echo $path; ?>js/sparkline/jquery.charts-sparkline.js" type="text/javascript"></script>
  <script src="<?php echo $path; ?>js/sparkline/sparkline-active.js" type="text/javascript"></script>
  <!-- calendar JS
		============================================ -->
  <script src="<?php echo $path; ?>js/calendar/moment.min.js" type="text/javascript"></script>
  <script src="<?php echo $path; ?>js/calendar/fullcalendar.min.js" type="text/javascript"></script>
  <script src="<?php echo $path; ?>js/calendar/fullcalendar-active.js" type="text/javascript"></script>
  <!-- plugins JS
		============================================ -->
  <script src="<?php echo $path; ?>js/plugins.js" type="text/javascript"></script>
  <!-- main JS
		============================================ -->
  <script src="<?php echo $path; ?>js/main.js" type="text/javascript"></script>
  <!-- tawk chat JS
		============================================
    <script src="<?php echo $path; ?>js/tawk-chat.js" type="text/javascript"></script> -->
  <!-- datapicker JS
		============================================ -->
  <script src="<?php echo $path; ?>js/datapicker/bootstrap-datepicker.js" type="text/javascript"></script>
  <script src="<?php echo $path; ?>js/datapicker/datepicker-active.js" type="text/javascript"></script>

  <script src="<?php echo $path2; ?>assets/plugins/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
  <script src="<?php echo $path2; ?>assets/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
  <script src="<?php echo $path2; ?>assets/plugins/datatables/dataTables.buttons.min.js"></script>
  <script src="<?php echo $path2; ?>assets/plugins/datatables/buttons.bootstrap.min.js"></script>
  <script src="<?php echo $path2; ?>assets/plugins/datatables/jszip.min.js"></script>
  <script src="<?php echo $path2; ?>assets/plugins/datatables/pdfmake.min.js"></script>
  <script src="<?php echo $path2; ?>assets/plugins/datatables/vfs_fonts.js"></script>
  <script src="<?php echo $path2; ?>assets/plugins/datatables/buttons.html5.min.js"></script>
  <script src="<?php echo $path2; ?>assets/plugins/datatables/buttons.print.min.js"></script>
  <script src="<?php echo $path2; ?>assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
  <script src="<?php echo $path2; ?>assets/plugins/datatables/dataTables.fixedColumns.min.js"></script>
  <script src="<?php echo $path2; ?>assets/plugins/datatables/dataTables.keyTable.min.js"></script>
  <script src="<?php echo $path2; ?>assets/plugins/datatables/dataTables.responsive.min.js"></script>
  <script src="<?php echo $path2; ?>assets/plugins/datatables/responsive.bootstrap.min.js"></script>
  <script src="<?php echo $path2; ?>assets/plugins/datatables/dataTables.scroller.min.js"></script>

  <!-- Sweet Alert 2 -->
  <script src="<?php echo base_url(); ?>assets/sweetalert/sweetalert2.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/interact.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/typeahead.bundle.js"></script>
  
  
