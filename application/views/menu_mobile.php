<?php

class Menu_tree_mobile {

	private $menu_data;

	function __construct() {
        $this->ci = & get_instance();
        $id_role    =  $this->ci->session->userdata['logged_in']['id_role'];
		$url  = base_url('menu_view/get_menu')."/".$id_role;
         $ch   = curl_init(); 
        
        curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output  = curl_exec($ch);
        curl_close($ch); 
        $this->menu_data = json_decode($output, TRUE);
	}

	function generate_html() {
       
		return $this->_build_menu(0, $this->menu_data['data']);
	}
//class="active"
	private function _build_menu($parent, $menu)
	{
	   $html = '';
	   if (isset($menu['parents'][$parent]))
	   {

		  foreach ($menu['parents'][$parent] as $itemId)
		  {
			 if( ! isset($menu['parents'][$itemId]))
			 {
				$html .=
				//'<li><a href="javascript:void(0);" onClick="openMenu(&#34;'.base_url().$menu['items'][$itemId]['url'].'&#34;,&#34;'.$menu['items'][$itemId]['label'].'&#34;);"><i class="'.base_url().'/img/'.$menu['items'][$itemId]['icon'].'"></i> <span> '.$menu['items'][$itemId]['label'].' </span> </a></li>';
				'<li><a title="'.base_url().$menu['items'][$itemId]['label'].'" href="javascript:void(0);" onClick="openMenu(&#34;'.base_url().$menu['items'][$itemId]['url'].'&#34;,&#34;'.$menu['items'][$itemId]['label'].'&#34;);"><span class="mini-sub-pro">'.$menu['items'][$itemId]['label'].'</span></a></li>';
			 }
			 if( isset($menu['parents'][$itemId]) )
			 {

				// $html .=
				// '<li class="has_sub">
					// <a href="javascript:void(0);" class="waves-effect"><i class="'.base_url().'/img/'.$menu['items'][$itemId]['icon'].'"></i> <span> '.$menu['items'][$itemId]['label'].' </span> <span class="menu-arrow"></span></a>
                  // <ul class="list-unstyled" >';
				  
				 $html .=
				 '
				 <li><a data-toggle="collapse"  href="#"><span class="admin-project-icon edu-icon edu-down-arrow"></span>'.$menu['items'][$itemId]['label'].'</span></a>
                            <ul class="collapse dropdown-header-top">
				 ';

				$html .= $this->_build_menu($itemId, $menu);

				$html .= '</ul></li>';
			 }
		  }
	   }
	   return $html;
	}
}
?>
<script>
	function openMenu(link, valname){
		localStorage.setItem("namamenu", valname);
        window.location.href = link;
	}
	
	
	$( document ).ready(function() {
		
		  $.fn.inputFilter = function(inputFilter) {
			return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
			  if (inputFilter(this.value)) {
				this.oldValue = this.value;
				this.oldSelectionStart = this.selectionStart;
				this.oldSelectionEnd = this.selectionEnd;
			  } else if (this.hasOwnProperty("oldValue")) {
				this.value = this.oldValue;
				this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
			  } else {
				this.value = "";
			  }
			});
		  };
		
		$(".rupiahs").inputFilter(function(value) {
			return /^-?\d*[,]?\d*$/.test(value); 
		});
		
		$(".rupiahs").click(function(value){
			var harga = value.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
	
		  return harga; 
		});
		
		var nn = localStorage.getItem("namamenu");
		if(!nn){
			$('#namamenu').html("Dashboard");		
		}else{
			$('#namamenu').html(nn);
		}
		
		
	});
	
</script>

<div class="mobile-menu-area">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">  
				<div class="mobile-menu">
					<nav id="dropdown">
						<ul class="mobile-menu-nav">
                       
							<?php
									$menu_tree = new Menu_tree_mobile();
									echo $menu_tree->generate_html();
							?>
						</ul>
				</nav>
			</div>
		</div>
	</div>
</div>
