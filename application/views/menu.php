<?php

class Menu_tree {

	private $menu_data;

	function __construct() {
        $this->ci = & get_instance();
        $id_role    =  $this->ci->session->userdata['logged_in']['id_role'];
		$url  = base_url('menu_view/get_menu')."/".$id_role;
         $ch   = curl_init(); 
        
        curl_setopt($ch, CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_URL, $url); 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output  = curl_exec($ch);
        curl_close($ch); 
        $this->menu_data = json_decode($output, TRUE);
	}

	function generate_html() {
       
		return $this->_build_menu(0, $this->menu_data['data']);
	}
//class="active"
	private function _build_menu($parent, $menu)
	{
	   $html = '';
	   if (isset($menu['parents'][$parent]))
	   {

		  foreach ($menu['parents'][$parent] as $itemId)
		  {
			 if( ! isset($menu['parents'][$itemId]))
			 {
				$html .=
				//'<li><a href="javascript:void(0);" onClick="openMenu(&#34;'.base_url().$menu['items'][$itemId]['url'].'&#34;,&#34;'.$menu['items'][$itemId]['label'].'&#34;);"><i class="'.base_url().'/img/'.$menu['items'][$itemId]['icon'].'"></i> <span> '.$menu['items'][$itemId]['label'].' </span> </a></li>';
				'<li><a id="'.$menu['items'][$itemId]['url'].'" title="'.base_url().$menu['items'][$itemId]['label'].'" href="'.base_url().$menu['items'][$itemId]['url'].'" onClick="openMenu(&#34;'.base_url().$menu['items'][$itemId]['url'].'&#34;,&#34;'.$menu['items'][$itemId]['label'].'&#34;);"><span class="mini-sub-pro">'.$menu['items'][$itemId]['label'].'</span></a></li>';
			 }
			 if( isset($menu['parents'][$itemId]) )
			 {

				// $html .=
				// '<li class="has_sub">
					// <a href="javascript:void(0);" class="waves-effect"><i class="'.base_url().'/img/'.$menu['items'][$itemId]['icon'].'"></i> <span> '.$menu['items'][$itemId]['label'].' </span> <span class="menu-arrow"></span></a>
                  // <ul class="list-unstyled" >';
				  
				 $html .=
				 '
				 <li><a class="has-arrow" href="#" aria-expanded="false"><span class="educate-icon educate-apps icon-wrap"></span> <span class="mini-click-non">'.$menu['items'][$itemId]['label'].'</span></a>
                            <ul class="submenu-angle app-mini-nb-dp" aria-expanded="false">
				 ';

				$html .= $this->_build_menu($itemId, $menu);

				$html .= '</ul></li>';
			 }
		  }
	   }
	   return $html;
	}
}
?>
<script>

	function openMenu(link, valname){
		localStorage.setItem("namamenu", valname);
        window.location.href = link;
	}
	
	
	$( document ).ready(function() {
		  $.fn.inputFilter = function(inputFilter) {
			return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
			  if (inputFilter(this.value)) {
				this.oldValue = this.value;
				this.oldSelectionStart = this.selectionStart;
				this.oldSelectionEnd = this.selectionEnd;
			  } else if (this.hasOwnProperty("oldValue")) {
				this.value = this.oldValue;
				this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
			  } else {
				this.value = "";
			  }
			});
		  };
		
		$(".rupiahs").inputFilter(function(value) {
			return /^-?\d*[,]?\d*$/.test(value); 
		});
		
		// $(".rupiahs").click(function(value){
			// var harga = value.replace('.','');
			// var harga = harga.replace('.','');
			// var harga = harga.replace('.','');
			// var harga = harga.replace('.','');
	
		  // return harga; 
		// });
		
		var nn = localStorage.getItem("namamenu");
		if(!nn){
			$('#namamenu').html("Dashboard");		
		}else{
			$('#namamenu').html(nn);
		}
		
		//$('#items').attr(class,"active");
		
		
		
	});
	
</script>

    <div class="left-sidebar-pro">
        <nav id="sidebar" class="">
            <div class="sidebar-header">
                <a href="<?= base_url();?>"><img class="main-logo" src="<?php echo $image_base; ?>Landscape_Blue.png" alt="Endira Alda"  style="width: 69%;margin: 17px -790px 9px -790px;"/></a>
                <strong><a href="<?= base_url();?>"><img src="<?php echo $image_base; ?>Logo_Gram_Blue.png" alt="Endira Alda" style="width:55%;"/></a></strong>
            </div>
            <div class="left-custom-menu-adp-wrap comment-scrollbar">
                <nav class="sidebar-nav left-sidebar-menu-pro">
                    <ul class="metismenu" id="menu1">
                       
							<?php
									$menu_tree = new Menu_tree();
									echo $menu_tree->generate_html();
							?>
						<!--<li>
                            <a class="has-arrow" href="mailbox.html" aria-expanded="false"><span class="educate-icon educate-apps icon-wrap"></span> <span class="mini-click-non">App views</span></a>
                            <ul class="submenu-angle app-mini-nb-dp" aria-expanded="false">
                                <li><a title="Notifications" href="notifications.html"><span class="mini-sub-pro">Notifications</span></a></li>
                                <li><a title="Alerts" href="alerts.html"><span class="mini-sub-pro">Alerts</span></a></li>
                                <li><a title="Modals" href="modals.html"><span class="mini-sub-pro">Modals</span></a></li>
                                <li><a title="Buttons" href="buttons.html"><span class="mini-sub-pro">Buttons</span></a></li>
                                <li><a title="Tabs" href="tabs.html"><span class="mini-sub-pro">Tabs</span></a></li>
                                <li><a title="Accordion" href="accordion.html"><span class="mini-sub-pro">Accordion</span></a></li>
                            </ul>
                        </li>-->

                    </ul>
                </nav>
            </div>
        </nav>
    </div>
						
                 <!--- Sidemenu 
                    <div id="sidebar-menu">
                        <ul>
						
                        </ul>
                        <div class="clearfix"></div>
                    </div>-->
