<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stock_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
    
	
	public function get_price($params = array()){
		
		$query = $this->db->get_where('m_material', array('id_mat' => $params['id_mat']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	
	
    // public function get_material($id_mat) {
    	// $query = $this->db->get_where('m_material', array('id_mat' => $id_mat));
		
		// $return = $query->result_array();

		// $this->db->close();
		// $this->db->initialize();

		// return $return;
	// }
	
   public function get_material($id_mat) {
		$sql 	= 'SELECT a.*, b.uom_symbol, c.name_eksternal FROM m_material a
					JOIN m_uom b ON a.unit_terkecil=b.id_uom
					JOIN t_eksternal c ON a.dist_id=c.id
					where a.id_mat = "'.$id_mat.'" ';

		$query_all 	=  $this->db->query($sql);

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}

	public function lists($params = array()) {
		$query = 	'
				SELECT COUNT(*) AS jumlah FROM m_material a
					JOIN m_uom b ON a.unit_terkecil=b.id_uom
					JOIN t_eksternal c ON a.dist_id=c.id where 1 = 1
				AND ( 
					stock_code LIKE "%'.$params['searchtxt'].'%" OR
					stock_name LIKE "%'.$params['searchtxt'].'%" 
				)
			';
						
			$query2 = 	'
				SELECT z.*, rank() over ( ORDER BY stock_name ASC) AS Rangking from ( 
					
					SELECT a.id_mat,a.stock_code,a.qty,a.qty_big,a.stock_name,a.top_price,a.bottom_price,a.base_price,
					a.stock_description,a.base_qty,b.uom_name,b.uom_symbol,a.unit_box,c.name_eksternal,c.kode_eksternal 
					FROM m_material a
					JOIN m_uom b ON a.unit_terkecil=b.id_uom
					JOIN t_eksternal c ON a.dist_id=c.id
					WHERE  ( 
					stock_code LIKE "%'.$params['searchtxt'].'%" OR
					stock_name LIKE "%'.$params['searchtxt'].'%" 
				)  order by stock_name) z
				ORDER BY stock_name ASC
				
				LIMIT '.$params['limit'].' 
				OFFSET '.$params['offset'].' 
			';
		
			//echo $query2;die;
		
			 $out		= array();
			  $querys		= $this->db->query($query);
			  $result = $querys->row();
			  
			  $total_filtered = $result->jumlah;
			  $total 			= $result->jumlah;
		  
				if(($params['offset']+10) > $total_filtered){
				$limit_data = $total_filtered - $params['offset'];
			  }else{
				$limit_data = $params['limit'] ;
			  }
		  
		
		  
		  //echo $query;die;
			//echo $query;die;
			 $query2s		= $this->db->query($query2);
		  $result2 = $query2s->result_array();						
		  $return = array(
			  'data' => $result2,
			  'total_filtered' => $total_filtered,
			  'total' => $total,
		  );
		  return $return;
	}

	public function list_mutasi($params = array()){
		
		$query = 	'
				SELECT COUNT(*) AS jumlah 
				FROM t_mutasi a
				JOIN m_material b ON a.id_stock_awal=b.id_mat
				JOIN m_uom c ON b.unit_terkecil=c.id_uom
				JOIN t_eksternal d ON b.dist_id=d.id 
				where a.id_stock_awal = '.$params['id_mat'].'
				AND ( 
					stock_code LIKE "%'.$params['searchtxt'].'%" OR
					stock_name LIKE "%'.$params['searchtxt'].'%" 
				)
			';
					
		$query2 = 	'
			SELECT z.*, rank() over ( ORDER BY stock_code ASC) AS Rangking from ( 
				SELECT b.id_mat,b.stock_code,b.qty,b.stock_name,
        b.stock_description,c.uom_name,d.name_eksternal,
        a.amount_mutasi,a.date_mutasi,a.type_mutasi,a.note
				FROM t_mutasi a
				JOIN m_material b ON a.id_stock_awal=b.id_mat
				JOIN m_uom c ON b.unit_terkecil=c.id_uom
				JOIN t_eksternal d ON b.dist_id=d.id  
				where a.id_stock_awal = '.$params['id_mat'].'
				AND ( 
					stock_code LIKE "%'.$params['searchtxt'].'%" OR
					stock_name LIKE "%'.$params['searchtxt'].'%"
			)  order by stock_code) z
			ORDER BY stock_code ASC
			
			LIMIT '.$params['limit'].' 
			OFFSET '.$params['offset'].' 
		';
	
		//echo $query2;die;
	
		 $out		= array();
		  $querys		= $this->db->query($query);
		  $result = $querys->row();
		  
		  $total_filtered = $result->jumlah;
		  $total 			= $result->jumlah;
	  
			if(($params['offset']+10) > $total_filtered){
			$limit_data = $total_filtered - $params['offset'];
		  }else{
			$limit_data = $params['limit'] ;
		  }
	  
	
	  
	  //echo $query;die;
		//echo $query;die;
		 $query2s		= $this->db->query($query2);
	  $result2 = $query2s->result_array();						
	  $return = array(
		  'data' => $result2,
		  'total_filtered' => $total_filtered,
		  'total' => $total,
	  );
	  return $return;
	
	
	}


	public function edit_price($data) {
		$datas = array(
			'top_price' => $data['top_price'],
			'bottom_price' => $data['bottom_price']
		);

	//	print_r($datas);die;
		$this->db->where('id_mat',$data['id_mat']);
		$this->db->update('m_material',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	
	public function add_mutasi($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'id_stock_awal' => $data['id_stock_awal'],
			'id_stock_akhir' => $data['id_stock_akhir'],
			'date_mutasi' => $data['date_mutasi'],
			'amount_mutasi' => $data['amount_mutasi'],
			'type_mutasi' => $data['type_mutasi'],
			'user_id' => $data['user_id']
		);

	//	print_r($datas);die;

		$this->db->insert('t_mutasi',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}	
	
	public function update_qty_material($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'id_mat' => $data['id_mat'],
			'qty' => $data['qty']
		);

	//	print_r($datas);die;
		$this->db->where('id_mat',$data['id_mat']);
		$this->db->update('m_material',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function get_price_history($params = array()){
		
		$query=$this->db->order_by('id_ph','desc')
						->get_where('t_price_history', array('id_mat' => $params['id_mat']), 1,0);
	
		// $this->db->order_by('id_hl','asc');
		// $query=$this->db->get();
		$return = $query->result_array();

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $return;
	}

	public function update_price_app($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			
			'id_approval' => $data['id_approval'],
			'approval_date' => $data['approval_date']
		);

	//	print_r($datas);die;
		$this->db->where('id_ph',$data['id_ph']);
		$this->db->update('t_price_history',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function update_status($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			
			'status_harga' => $data['status_harga']
		);

	//	print_r($datas);die;
		$this->db->where('id_mat',$data['id_mat']);
		$this->db->update('m_material',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function deletes($id_mat) {
		$this->db->where('id_mat', $id_mat);
		$this->db->delete('m_material'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}


}
