<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Persediaan extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('persediaan/stock_model');
    $this->load->library('log_activity');
    $this->load->library('formatnumbering');
  }

  /**
   * anti sql injection
   */
  public function Anti_sql_injection($string)
  {
    $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
    return $string;
  }

  public function index()
  {
    $this->template->load('maintemplate', 'persediaan/views/index');
  }

  function lists()
  {
    $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
    $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
    $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
    $order = $this->input->get_post('order');
    $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
    $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

    $order_fields = array('', 'stock_code', 'stock_name');

    $search = $this->input->get_post('search');

    $search_val = (!empty($search['value'])) ? $search['value'] : null;

    $search_value = $this->Anti_sql_injection($search_val);

    // Build params for calling model
    $params['limit'] = (int) $length;
    $params['offset'] = (int) $start;
    $params['order_column'] = $order_fields[$order_column];
    $params['order_dir'] = $order_dir;
    $params['searchtxt'] = $search_value;

    $list = $this->stock_model->lists($params);

    $result["recordsTotal"] = $list['total'];
    $result["recordsFiltered"] = $list['total_filtered'];
    $result["draw"] = $draw;

    $data = array();
    //$i = 0;
    $i = $params['offset'];
    $username = $this->session->userdata['logged_in']['username'];
    foreach ($list['data'] as $k => $v) {
      $i = $i + 1;
      $button_mutasi = '<div class="btn-group"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Mutasi" onClick="editmaterial(\'' . $v['id_mat'] . '\')"><i class="fa fa-history"></i></button></div>';
      // $button_approve = '<div class="btn-group"><button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Approve" onClick="approvematerial(\'' . $v['id_mat'] . '\')"><i class="fa fa-check"></i></button></div>';
      $button_detail = '<div class="btn-group"><button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Detail" onClick="detailmaterial(\'' . $v['id_mat'] . '\')"><i class="fa fa-search"></i></button></div>';
      // if($v['status_harga']==0){
      //   $status = 'Konfirmasi';
      $button = $button_detail . $button_mutasi;
      // }
      // else {
      //   $status = 'Setuju';
      //   $button = $button_edit;
      // }
      array_push($data, array(
        $i,
        $v['stock_code'],
        $v['stock_name'].' '.$v['base_qty'].' '.$v['uom_symbol'],
        $v['stock_name'].' '.$v['base_qty'].' '.$v['uom_symbol'],
        $v['stock_name'].' '.$v['base_qty'].' '.$v['uom_symbol'],
        $v['stock_name'].' '.$v['base_qty'].' '.$v['uom_symbol'],
        $v['stock_name'].' '.$v['base_qty'].' '.$v['uom_symbol'],
        $v['stock_name'].' '.$v['base_qty'].' '.$v['uom_symbol'],
        $v['stock_name'].' '.$v['base_qty'].' '.$v['uom_symbol'],
        $v['stock_name'].' '.$v['base_qty'].' '.$v['uom_symbol'],
        $v['stock_name'].' '.$v['base_qty'].' '.$v['uom_symbol'],
        $v['stock_name'].' '.$v['base_qty'].' '.$v['uom_symbol'],
        $v['stock_name'].' '.$v['base_qty'].' '.$v['uom_symbol'],
        $v['stock_name'].' '.$v['base_qty'].' '.$v['uom_symbol'],
        $v['stock_name'].' '.$v['base_qty'].' '.$v['uom_symbol'],
        number_format($v['unit_box'],0,',','.').' per Box',
        //number_format($v['qty'],0,',','.').' Pcs',
        number_format($v['qty']/$v['unit_box'],2,',','.').' Box',
        $this->formatnumbering->qty($v['qty_big']),
        // $status,
        $button
      ));
    }

    $result["data"] = $data;

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }
  function lists_mutasi()
  {
    $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
    $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
    $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
    $order = $this->input->get_post('order');
    $id_mat = (int)$this->Anti_sql_injection($this->uri->segment(3));
    $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
    $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

    $order_fields = array('', 'stock_code', 'stock_name');

    $search = $this->input->get_post('search');

    $search_val = (!empty($search['value'])) ? $search['value'] : null;

    $search_value = $this->Anti_sql_injection($search_val);

    // Build params for calling model
    $params['limit'] = (int) $length;
    $params['offset'] = (int) $start;
    $params['order_column'] = $order_fields[$order_column];
    $params['order_dir'] = $order_dir;
    $params['searchtxt'] = $search_value;
    $params['id_mat'] = $id_mat;
    // var_dump($params);die;
    $list = $this->stock_model->list_mutasi($params);

    $result["recordsTotal"] = $list['total'];
    $result["recordsFiltered"] = $list['total_filtered'];
    $result["draw"] = $draw;

    $data = array();
    //$i = 0;
    $i = $params['offset'];
    $username = $this->session->userdata['logged_in']['username'];
    foreach ($list['data'] as $k => $v) {
      $i = $i + 1;
      $button_mutasi = '<div class="btn-group"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Mutasi" onClick="editmaterial(\'' . $v['id_mat'] . '\')"><i class="fa fa-history"></i></button></div>';
      $button_approve = '<div class="btn-group"><button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Approve" onClick="approvematerial(\'' . $v['id_mat'] . '\')"><i class="fa fa-check"></i></button></div>';
      $button_detail = '<div class="btn-group"><button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Detail" onClick="approvematerial(\'' . $v['id_mat'] . '\')"><i class="fa fa-search"></i></button></div>';
      // if($v['status_harga']==0){
      //   $status = 'Konfirmasi';
      $button = $button_detail . $button_mutasi . $button_approve;
      // }
      // else {
      //   $status = 'Setuju';
      //   $button = $button_edit;
      // }
	  
	  if( $v['type_mutasi'] == 0){
		 // $tp_mutasi = "Pemasukan";
		  $tp_mutasi = "Penjualan";
	  }else{
		 // $tp_mutasi = "Pengeluaran";
		  $tp_mutasi = "Pembelian";
	  }
	  
      array_push($data, array(
        $i,
        $v['stock_code'],
        $v['stock_name'],
        $v['date_mutasi'],
         $tp_mutasi,
        $this->formatnumbering->qty($v['amount_mutasi']),
        $v['name_eksternal'],
        $v['note'],
        // $status,
        // $button
      ));
    }

    $result["data"] = $data;

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }
  public function edit()
  {
    $data = array(
      'id' => $this->Anti_sql_injection($this->input->post('iditems', TRUE)),
    );
    $result = $this->stock_model->get_material($data['id']);

    // $unit = $this->stock_model->get_unit();?
    // $Gudang = $this->stock_model->get_distributor();

    $data = array(
      'stok'         => $result,
      // 'unit' 				=> $unit,
      // 'Gudang' 			=> $Gudang
    );

    $this->template->load('maintemplate', 'persediaan/views/edit_modal_view', $data);
  }
  public function detail()
  {
    $data = array(
      'id' => $this->Anti_sql_injection($this->input->post('iditems', TRUE)),
    );
    $result = $this->stock_model->get_material($data['id']);

	//print_r($result);die;

    // $unit = $this->stock_model->get_unit();?
    // $Gudang = $this->stock_model->get_distributor();

    $data = array(
      'stok'         => $result,
      // 'unit' 				=> $unit,
      // 'Gudang' 			=> $Gudang
    );

    $this->template->load('maintemplate', 'persediaan/views/mutasi_view', $data);
  }

  public function edit_material()
  {
    $this->form_validation->set_rules('id_mat', 'Unit', 'trim|required');
    $this->form_validation->set_rules('type_mutasi', 'Type Mutasi', 'trim|required');
    $this->form_validation->set_rules('amount_mutasi', 'Jumlah Mutasi', 'trim|required');
    $this->form_validation->set_rules('date_mutasi', 'Jumlah Mutasi', 'trim|required');
    $this->form_validation->set_rules('qty', 'Qty Awal Barang', 'trim|required');


    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));

      $result = array('success' => false, 'message' => $msg);
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      $id_mat = $this->Anti_sql_injection($this->input->post('id_mat', TRUE));
      $type_mutasi = $this->Anti_sql_injection($this->input->post('type_mutasi', TRUE));
      $amount_mutasi = $this->Anti_sql_injection($this->input->post('amount_mutasi', TRUE));
      $qty = (float) $this->Anti_sql_injection($this->input->post('qty', TRUE));
      if ($type_mutasi == 0)
        $qty = $qty + $amount_mutasi;
      else
        $qty = $qty - $amount_mutasi;
      $data = array(
        'id_mat'          => $id_mat,
        'id_stock_awal'   => $id_mat,
        'id_stock_akhir'  => $id_mat,
        'type_mutasi'     => $type_mutasi,
        'amount_mutasi'   => $amount_mutasi,
        'qty'             => $qty,
        'date_mutasi'     =>  date('Y-m-d H:i:s', strtotime($this->Anti_sql_injection($this->input->post('date_mutasi', TRUE)) . ' ' . date('H:i:s'))),
        'user_id'         => $this->session->userdata['logged_in']['user_id'],
      );
      $result = $this->stock_model->add_mutasi($data);
      $result = $result + $this->stock_model->update_qty_material($data);
      if (count($result) > 1) {
        $msg = 'Berhasil mengubah data stock ke database';

        $this->log_activity->insert_activity('insert', 'Berhasil Update Stock dan Mutasi dengan id Material ' . $id_mat);
        $results = array(
          'success' => true,
          'message' => $msg
        );
      } else {
        $msg = 'Gagal mengubah data stock ke database';

        $this->log_activity->insert_activity('insert', 'Gagal Update Stock dan Mutasi dengan id material = ' . $id_mat);
        $results = array(
          'success' => false,
          'message' => $msg
        );
      }

      $this->output->set_content_type('application/json')->set_output(json_encode($results), true);
    }
  }

}
