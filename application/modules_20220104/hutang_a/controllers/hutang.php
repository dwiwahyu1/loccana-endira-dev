<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hutang extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('hutang/purchase_order_model');
		$this->load->model('selling/selling_model');
		$this->load->model('invoice_selling/return_model');
		$this->load->library('log_activity');
		$this->load->library('priv');

		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong');
		$this->form_validation->set_message('min_length', '%s Minimal 4 Karakter');
		$this->form_validation->set_message('max_length', '%s Maksimal 10 Karakter');
	}

	/**
	 * anti sql injection
	 * @return string
	 */
	public function Anti_sql_injection($string)
	{
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	 * This function is redirect to index distributor page
	 * @return Void
	 */
	public function index()
	{
		$priv = $this->priv->get_priv();

		$year_count = date('Y') - 2019;

		$filter_year = '<select class="form-control" id="filter_year" onChange="changeFilter()"><option value="ALL" >ALL</option>';

		for ($i = $year_count; $i >= 0; $i--) {

			$y = 2019 + $i;

			if ($y == date('Y')) {
				$filter_year .= '<option value="' . $y . '" selected="selected" >' . $y . '</option>';
			} else {
				$filter_year .= '<option value="' . $y . '" >' . $y . '</option>';
			}
		}
		$filter_year .= '</select>';

		$array_month = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

		$filter_month = '<select class="form-control" id="filter_month" onChange="changeFilter()"><option value="ALL" >ALL</option>';
		foreach ($array_month as $array_months) {
			if ($array_months == date('F')) {
				$filter_month .= '<option value="' . $array_months . '" selected="selected">' . $array_months . '</option>';
			} else {
				$filter_month .= '<option value="' . $array_months . '" >' . $array_months . '</option>';
			}
		}

		$filter_month .= '</select>';
		$today_month = date('F');
		$today_year = date('Y');

		$data = array(
			'priv' => $priv,
			'filter_month' => $filter_month,
			'filter_year' => $filter_year,
			'today_month' => $today_month,
			'today_year' => $today_year
		);

		$this->template->load('maintemplate', 'hutang/views/index', $data);
	}

	/**
	 * This function is used for showing distributor list
	 * @return Array
	 */
	function lists_bayar()
	{

		if (!empty($_GET['sess_user_id'])) {
			$sess_user_id = $_GET['sess_user_id'];
		} else {
			$sess_user_id = NULL;
		}

		if (!empty($_GET['sess_token'])) {
			$sess_token = $_GET['sess_token'];
		} else {
			$sess_token = NULL;
		}

		if (!empty($_GET['filter'])) {
			$filter = $_GET['filter'];
		} else {
			$filter = NULL;
		}

		if (!empty($_GET['filter_month'])) {
			$filter_month = $_GET['filter_month'];
		} else {
			$filter_month = NULL;
		}

		if (!empty($_GET['filter_year'])) {
			$filter_year = $_GET['filter_year'];
		} else {
			$filter_year = NULL;
		}

		if ($this->input->get_post('draw') != FALSE) {
			$draw   = $this->input->get_post('draw');
		} else {
			$draw   = 1;
		};
		if ($this->input->get_post('length') != FALSE) {
			$length = $this->input->get_post('length');
		} else {
			$length = 10;
		};
		if ($this->input->get_post('start') != FALSE) {
			$start  = $this->input->get_post('start');
		} else {
			$start  = 0;
		};
		$order_fields = array('id_hp', 'no_payment', 'total', 'tgl_terbit', 'keterangan', 'id_hp', 'id_hp'); // , 'COST'
		$order = $this->input->get_post('order');
		if (!empty($order[0]['dir'])) {
			$order_dir    = $order[0]['dir'];
		} else {
			$order_dir    = 'desc';
		};
		if (!empty($order[0]['column'])) {
			$order_column = $order[0]['column'];
		} else {
			$order_column = 0;
		};

		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['sess_user_id'] = $sess_user_id;
		$params['sess_token'] 	= $sess_token;
		$params['filter'] 		= $filter;
		$params['filter_month'] 		= $_GET['filter_month'];
		$params['filter_year'] 		= $_GET['filter_year'];
		$params['searchtxt'] 	= $_GET['search']['value'];

		// print_r($params);die;
		$priv = $this->priv->get_priv();

		$list = $this->purchase_order_model->list_pembayaran($params);
		//print_r($list['data']);die;
		$data = array();
		foreach ($list['data'] as $k => $v) {

			if ($v['payment_status'] == 0) {
				$sts = '<button type="button" class="btn btn-custon-rounded-two btn-danger" style="display:' . $priv['update'] . '" onClick="konfirmasi(' . $v['id_hp'] . ')" > Konfirmasi </button>';
				// $action = '
				// <button type="button" class="btn btn-custon-rounded-two btn-primary" onClick="detail('. $v['id_invoice'].')" > Detail </button>
				// ';
				$action = '
						<div class="btn-group" style="display:' . $priv['update'] . '"><button type="button" class="btn btn-custon-rounded-two btn-warning" data-toggle="tooltip" data-placement="top" title="Edit" onClick="updatebayar(' . $v['id_hp'] . ')" > <i class="fa fa-edit"></i> </button></div>
						<div class="btn-group" style="display:' . $priv['update'] . '"><button type="button" class="btn btn-custon-rounded-two btn-danger" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deletehp(' . $v['id_hp'] . ')" > <i class="fa fa-trash"></i> </button></div>
						<div class="btn-group" style="display:' . $priv['detail'] . '"><button type="button" class="btn btn-custon-rounded-two btn-info" data-toggle="tooltip" data-placement="top" title="Detail" onClick="detail(' . $v['id_hp'] . ')" > <i class="fa fa-search-plus"></i> </button></div>
						<div class="btn-group" style="display:' . $priv['detail'] . '"><button type="button" class="btn btn-custon-rounded-two btn-secondary" data-toggle="tooltip" data-placement="top" title="Print"  onClick="hp(' . $v['id_hp'] . ')" > <i class="fa fa-download"></i>  </button></div>
						';
			} else {
				$sts = '<button type="button" class="btn btn-custon-rounded-two btn-primary" disabled > Sudah Dibayar </button>';
				//$action = '<button type="button" class="btn btn-custon-rounded-two btn-primary" onClick="detail('. $v['id_invoice'].')" > Detail </button>
				$action = '
						<div class="btn-group" style="display:' . $priv['detail'] . '"><button type="button" class="btn btn-custon-rounded-two btn-primary" onClick="detail(' . $v['id_hp'] . ')" > Detail </button></div>
						<div class="btn-group" style="display:' . $priv['detail'] . '"><button type="button" class="btn btn-custon-rounded-two btn-secondary" data-toggle="tooltip" data-placement="top" title="Print"  onClick="slip_pdf(' . $v['id_hp'] . ')" > <i class="fa fa-download"></i>  </button></div>
						';
			}
			
			$array_trans = ['Cash / Transfer', 'Giro','Bonus','Komisi','Piutang'];
			
			if ($v['account_type'] == 1 || $v['account_type'] == 0) {
				$ls = $array_trans[$v['account_type']].'<br>'.$v['keterangan'];
			}else{
				$ls = $array_trans[$v['account_type']];
			}

			// if($v['term_of_payment'] == '0'){
			// $date=date_create($v['date_penjualan']);
			// $date_due = date_add($date,date_interval_create_from_date_string($v['term_of_payment']." days"));
			// $due_date = date_format($date_due,"Y-m-d");
			// }else{
			// $date=date_create($v['date_penjualan']);
			// $date_due = date_add($date,date_interval_create_from_date_string($v['term_of_payment']." days"));
			// $due_date = date_format($date_due,"Y-m-d");
			// }


			// if($v['term_of_payment'] == '0'){
			// $hhh = 'Cash';
			// }else{
			// $hhh =  $v['term_of_payment'].' Hari';
			// }

			// if($v['sisa'] == 0){
			// $sts = '<button type="button" class="btn btn-custon-rounded-two btn-primary" > Sudah Lunas </button>';

			// }else{

			// $sts = '<button type="button" class="btn btn-custon-rounded-two btn-danger" > Belum Lunas </button>';

			// }




			array_push(
				$data,
				array(
					number_format($v['Rangking'], 0, ',', '.'),
					$v['no_payment'],
					$v['name_eksternal'],
					number_format(floatval($v['total']), 2, ',', '.'),
					$v['tgl_terbit'],
					// $v['tgl_aktif'],
					// $v['tgl_cair'],
					$ls,
					$sts,
					$action
				)
			);
			//$idx++;
		}

		$result["data"] = $data;
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		//$result["data"] = $list['data'];

		echo json_encode($result);

		//$this->json_result($result);

	}

	public function invoice()
	{
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
		);

		//print_r($data);die;

		$po_result = $this->purchase_order_model->get_po_by_payment($data);
		$cash_account = $this->purchase_order_model->get_cash_account();
		$po_detail_result = $this->purchase_order_model->get_po_detail($data);
		$detail_bayar = $this->purchase_order_model->get_bayar_inv($data);
		$result = $this->purchase_order_model->get_principal();
		//$result = $this->purchase_order_model->get_po();
		$resultby = $this->purchase_order_model->get_bayar($data);

		$pms = array(
			'id_prin' => $resultby[0]['id_distributor']
		);

		$item2 = $this->purchase_order_model->get_invoice_byprin($pms);
		$item3 = $this->purchase_order_model->get_bayar_detail($data);

		//print_r($po_result);die;

		$total_amnt = 0;
		foreach ($resultby as $resultbys) {

			$total_amnt += $resultbys['ammount'];
		}

		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);

		$item = $this->purchase_order_model->get_item_byprin($params);

		//print_r($data);die;
		$session = $this->session->userdata;
		$params['region'] = $session['logged_in']['idlokasi'];
		//$params['region'] = 4;

		$date = date_create($resultby[0]['date']);
		$date_ss = date_format($date, "d F Y");
		//$page_size = [241,279];
		//$page_size = array(241, 279);
		$page_size = array('auto', 20);
		$pdf = new Pdf('P', 'mm', 'a4', true, 'UTF-8', false);
		$pdf->SetTitle('Invoice');
		$pdf->AddPage();

		// $pdf->AddPage();

		$bayar_tipe = ['Cash/Transfer', 'Giro', 'Bonus', 'Komisi'];

		$pdf->SetFont('', 'B', 10);

		//Cell(width , height , text , border , end line , [align] )

		$pdf->Cell(130, 5, 'PT. ENDIRA ALDA', 0, 0);
		$pdf->Cell(59, 5, 'SLIP KAS/ BANK KELUAR', 0, 1); //end of line


		//set font to arial, regular, 12pt
		$pdf->SetFont('', '', 8);

		// $pdf->Cell(130 ,5,'',0,0);
		// $pdf->Cell(59 ,5,'',0,1);//end of line

		$pdf->Cell(130, 5, 'JL. Sangkuriang NO.38-A', 0, 0);
		$pdf->Cell(25, 5, 'Tanggal ', 0, 0);
		$pdf->Cell(34, 5, ': ' . $date_ss, 0, 1); //end of line

		$pdf->Cell(130, 5, 'Cimahi - 40511', 0, 0);
		$pdf->Cell(25, 5, 'Transaksi', 0, 0);
		$pdf->Cell(34, 5, ': R' . $resultby[0]['no_payment'], 0, 1); //end of line

		$pdf->Cell(130, 5, 'NPWP: 01.555.161.7.428.000', 0, 0);
		$pdf->Cell(25, 5, 'Ref', 0, 0);
		$pdf->Cell(34, 5, ': ' . $resultby[0]['no_payment'], 0, 1); //end of line

		$pdf->Cell(130, 5, '', 0, 0);
		$pdf->Cell(25, 5, 'Supplier', 0, 0);
		$pdf->Cell(34, 5, ': ' . $po_result[0]['name_eksternal'], 0, 1); //end of line

		$pdf->Cell(130, 5, '', 0, 0);
		$pdf->Cell(25, 5, 'Currency', 0, 0);
		$pdf->Cell(34, 5, ': Rp.   : Kurs : 1', 0, 1); //end of line

		//make a dummy empty cell as a vertical spacer

		$pdf->Cell(189, 10, '', 0, 1); //end of line

		//invoice contents
		// $pdf->SetFont('roboto','B',12);
		$pdf->Cell(130, 5, 'Pembayaran', 0, 0);
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->Cell(34, 5, '', 0, 1); //end of li

		$pdf->SetFont('', 'B', 8);

		$pdf->Cell(30, 5, 'CH/BG/CASH/NC', 1, 0);
		$pdf->Cell(30, 5, 'Nama Bank', 1, 0);
		$pdf->Cell(30, 5, 'Jatuh Tempo', 1, 0);
		$pdf->Cell(40, 5, 'Nilai', 1, 0);
		$pdf->Cell(40, 5, 'Total', 1, 0);
		$pdf->Cell(25, 5, 'L/R Kurs', 1, 1);

		$pdf->SetFont('', '', 8);

		$pdf->Cell(30, 5, $bayar_tipe[$resultby[0]['account_type']], 1, 0);
		$pdf->Cell(30, 5, $resultby[0]['coa_name'], 1, 0);
		$pdf->Cell(30, 5, $resultby[0]['tgl_jatuh_tempo'], 1, 0);
		$pdf->Cell(40, 5, number_format($total_amnt, 2, ',', '.'), 1, 0, 'R');
		$pdf->Cell(40, 5, '', 1, 0);
		$pdf->Cell(25, 5, '', 1, 1);

		$pdf->Cell(90, 5, 'Total Pembayaran', 1, 0, 'R');
		$pdf->Cell(40, 5, number_format($total_amnt, 2, ',', '.'), 1, 0, 'R');
		$pdf->Cell(40, 5, number_format($total_amnt, 2, ',', '.'), 1, 0, 'R');
		$pdf->Cell(25, 5, '', 1, 1);

		$pdf->Cell(189, 10, '', 0, 1); //end of line

		$pdf->SetFont('', 'B', 8);

		$pdf->Cell(130, 5, 'Pembelian', 0, 0);
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->Cell(34, 5, '', 0, 1); //end of li

		$pdf->SetFont('', 'B', 8);

		$pdf->Cell(30, 5, 'Invoice', 1, 0);
		$pdf->Cell(30, 5, 'Currency', 1, 0);
		$pdf->Cell(30, 5, 'Kurs', 1, 0);
		$pdf->Cell(40, 5, 'Jatuh Tempo', 1, 0);
		$pdf->Cell(40, 5, 'Nilai Kewajiban', 1, 0);
		$pdf->Cell(25, 5, 'Nilai Terbayar', 1, 1);

		$pdf->SetFont('', '', 8);

		foreach ($item3 as $item3s) {

			$pdf->Cell(30, 5, $item3s['no_invoice'], 1, 0);
			$pdf->Cell(30, 5, 'Rp.', 1, 0);
			$pdf->Cell(30, 5, '1', 1, 0);
			$pdf->Cell(40, 5, $item3s['due_date'], 1, 0);
			$pdf->Cell(40, 5, number_format($po_result[0]['total_amount'], 2, ',', '.'), 1, 0, 'R');
			$pdf->Cell(25, 5, number_format($item3s['ammount'], 2, ',', '.'), 1, 1, 'R');
		}

		$pdf->SetFont('', 'B', 8);

		$pdf->Cell(130, 5, '', 0, 0);
		$pdf->Cell(65, 5, 'Total  :  ' . number_format($total_amnt, 2, ',', '.'), 0, 1, 'R');
		// $pdf->SetFont('roboto','',12);
		$pdf->Cell(189, 10, '', 0, 1); //end of line

		$pdf->SetFont('', '', 8);

		$pdf->Cell(50, 5, 'Mengetahui,', 0, 0, 'C');
		$pdf->Cell(50, 5, 'Membayar,', 0, 0, 'C');
		$pdf->Cell(50, 5, 'Menerima,', 0, 0, 'C');
		$pdf->Cell(50, 5, 'Verifikasi', 0, 1, 'C');

		$pdf->Cell(189, 10, '', 0, 1); //end of line
		$pdf->Cell(189, 10, '', 0, 1); //end of line

		$pdf->Cell(7, 5, '', 0, 0, 'C');
		$pdf->Cell(40, 5, '___________________________', 0, 0, 'C');
		$pdf->Cell(10, 5, '', 0, 0, 'C');
		$pdf->Cell(40, 5, '___________________________', 0, 0, 'C');
		$pdf->Cell(10, 5, '', 0, 0, 'C');
		$pdf->Cell(40, 5, '___________________________', 0, 0, 'C');
		$pdf->Cell(10, 5, '', 0, 0, 'C');
		$pdf->Cell(40, 5, '___________________________', 0, 1, 'C');

		$pdf->Cell(7, 5, '', 0, 0, 'C');
		$pdf->Cell(40, 5, 'Tanggal Cetak :' . date('d F Y'), 0, 0, 'C');
		//	$pdf->Cell(189 ,10,'',0,1);//end of line


		//Numbers are right-aligned so we give 'R' after new line parameter
		$ii = 1;
		// foreach($item2 as $items){
		// $pdf->Cell(30 ,5,$items['stock_code'],1,0);
		// $pdf->Cell(50 ,5,$items['stock_name'].' '.$items['base_qty'].' '.$items['uom_symbol'],1,0);
		// $pdf->Cell(20 ,5,$items['qty_box_retur'],1,0,'L');
		// $pdf->Cell(25 ,5,'Box @ '.$items['box_ammount'],1,0,'R');
		// $pdf->Cell(25 ,5,number_format($items['unit_price'],2,',','.'),1,0,'R');
		// $pdf->Cell(34 ,5,number_format($items['price'],2,',','.'),1,1,'R');//end of line
		// $ii++;
		// $total_retur += $items['price'];
		// }

		// //summary
		// $pdf->Cell(130 ,5,'MENGETAHUI,',0,0);
		// $pdf->Cell(25 ,5,'Total',0,0);
		// $pdf->Cell(6 ,5,'Rp',1,0);
		// $pdf->Cell(28 ,5,number_format($total_retur,2,',','.'),1,1,'R');//end of line

		// $pdf->Cell(130 ,5,'',0,0);
		// $pdf->Cell(25 ,5,'DPP',0,0);
		// $pdf->Cell(6 ,5,'Rp',1,0);
		// $pdf->Cell(28 ,5,number_format($total_retur-($total_retur/11),2,',','.'),1,1,'R');//end of line

		// $pdf->Cell(130 ,5,'_____________',0,0);
		// $pdf->Cell(25 ,5,'PPN',0,0);
		// $pdf->Cell(6 ,5,'Rp',1,0);
		// $pdf->Cell(28 ,5,number_format(($total_retur/11),2,',','.'),1,1,'R');//end of line

		// $pdf->Cell(130 ,5,'',0,0);
		// $pdf->Cell(25 ,5,'',0,0);
		// $pdf->Cell(6 ,5,'',0,0);
		// $pdf->Cell(28 ,5,'',0,1,'R');//end of line		
		// $last_row = $pdf->GetY();
		// $line = 130;
		// $margin = $line - $last_row;
		// // $pdf->AddPage();
		// $pdf->Line(10,$line,190,$line);

		//	$pdf->Cell(189 ,$margin,'',0,1);//end of line
		// print_r($pdf->GetY());
		$pdf->SetFont('', 'B', 10);

		//Cell(width , height , text , border , end line , [align] )

		// $pdf->Cell(28 ,5,'',0,1,'R');//end of line


		$pdf->SetPrintFooter(false);

		$pdf->lastPage();


		$pdf->Output('Kas_Keluar.pdf', 'i');






		// $this->template->load('maintemplate', 'invoice_selling/views/invoice',$data);
	}

	function lists()
	{

		if (!empty($_GET['sess_user_id'])) {
			$sess_user_id = $_GET['sess_user_id'];
		} else {
			$sess_user_id = NULL;
		}

		if (!empty($_GET['sess_token'])) {
			$sess_token = $_GET['sess_token'];
		} else {
			$sess_token = NULL;
		}

		if (!empty($_GET['filter'])) {
			$filter = $_GET['filter'];
		} else {
			$filter = NULL;
		}

		if (!empty($_GET['filter_month'])) {
			$filter_month = $_GET['filter_month'];
		} else {
			$filter_month = NULL;
		}

		if (!empty($_GET['filter_year'])) {
			$filter_year = $_GET['filter_year'];
		} else {
			$filter_year = NULL;
		}

		// echo $filter;die;

		if ($this->input->get_post('draw') != FALSE) {
			$draw   = $this->input->get_post('draw');
		} else {
			$draw   = 1;
		};
		if ($this->input->get_post('length') != FALSE) {
			$length = $this->input->get_post('length');
		} else {
			$length = 10;
		};
		if ($this->input->get_post('start') != FALSE) {
			$start  = $this->input->get_post('start');
		} else {
			$start  = 0;
		};
		$order_fields = array('id_invoice', 'no_do', 'name_eksternal', 'date_po', 'total_amount', 'total_invoice-total_bayar', 'id_invoice', 'id_invoice', 'id_invoice'); // , 'COST'
		$order = $this->input->get_post('order');
		if (!empty($order[0]['dir'])) {
			$order_dir    = $order[0]['dir'];
		} else {
			$order_dir    = 'desc';
		};
		if (!empty($order[0]['column'])) {
			$order_column = $order[0]['column'];
		} else {
			$order_column = 0;
		};

		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['sess_user_id'] = $sess_user_id;
		$params['sess_token'] 	= $sess_token;
		$params['filter_month'] 		= $_GET['filter_month'];
		$params['filter_year'] 		= $_GET['filter_year'];
		$params['filter'] 		= $_GET['filter'];
		$params['searchtxt'] 	= $_GET['search']['value'];

		//print_r($params);die;

		$list = $this->purchase_order_model->list_po($params);
		//print_r($list['data']);die;
		$data = array();
		foreach ($list['data'] as $k => $v) {

			if ($v['status_invoice'] == 0) {
				//<button type="button" class="btn btn-custon-rounded-two btn-primary" onClick="bayar('. $v['id_invoice'].')" > Bayar </button>
				$sts = '<span type="button" class="btn btn-custon-rounded-two btn-danger"  > Konfirmasi </button>';
				// $action = '
				// <button type="button" class="btn btn-custon-rounded-two btn-default" onClick="updatepo('. $v['id_invoice'].')" > Edit </button>
				// <button type="button" class="btn btn-custon-rounded-two btn-danger" onClick="deletepo('. $v['id_invoice'].')" > Hapus </button>
				// <button type="button" class="btn btn-custon-rounded-two btn-primary" onClick="detail('. $v['id_invoice'].')" > Detail </button>

				// ';
				$action = '
						<button type="button" class="btn btn-custon-rounded-two btn-info" onClick="detail(' . $v['id_invoice'] . ')" data-toggle="tooltip" data-placement="top" title="Detail" >  <i class="fa fa-search-plus"></i> </button>
						
						';
			} else {
				$sts = '<button type="button" class="btn btn-custon-rounded-two btn-primary" disabled > Disetujui </button>';
				$action = '<button type="button" class="btn btn-custon-rounded-two btn-info" onClick="detail(' . $v['id_invoice'] . ')" data-toggle="tooltip" data-placement="top" title="Detail" >  <i class="fa fa-search-plus"></i> </button>
						';
			}

			if ($v['term_of_payment'] == '0' || $v['term_of_payment'] == 'Cash') {
				$hhh = 'Cash';
			} else {
				$hhh =  $v['term_of_payment'] . ' Hari';
			}


			if ($v['total_bayar'] == 0) {
				$sts = '<button type="button" class="btn btn-custon-rounded-two btn-danger" > Belum Lunas </button>';
			} else {
				if ($v['total_invoice'] - $v['total_bayar'] == 0) {
					$sts = '<button type="button" class="btn btn-custon-rounded-two btn-primary"  > Sudah Lunas </button>';
				} else {
					$sts = '<button type="button" class="btn btn-custon-rounded-two btn-primary" > Belum Lunas </button>';
				}
			}




			array_push(
				$data,
				array(
					number_format($v['Rangking'], 0, ',', '.'),
					$v['no_do'] . ' / ' . $v['no_po'],
					$v['name_eksternal'],
					$v['date_po'],
					number_format($v['total_invoice'], 0, ',', '.'),
					number_format($v['total_invoice'] - $v['total_bayar'], 0, ',', '.'),
					$hhh,
					$sts,
					$action
				)
			);
			//$idx++;
		}

		$result["data"] = $data;
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		//$result["data"] = $list['data'];

		echo json_encode($result);

		//$this->json_result($result);

	}

	/**
	 * This function is redirect to add distributor page
	 * @return Void
	 */

	public function get_principal()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$result['principal'] = $this->purchase_order_model->get_principal_byid($params);
		$item = $this->purchase_order_model->get_item_byprin($params);

		$list = '<option value="0" selected="selected" disabled>-- Pilih Invoice --</option>';
		foreach ($item as $items) {

			$list .= '<option value="' . $items['id_mat'] . '" >' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '</option>';
		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}


	public function get_list_bk()
	{

		$params = array(
			'name' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
			'tipeb' => $this->Anti_sql_injection($this->input->post('tipeb', TRUE))
		);



		$item = $this->purchase_order_model->get_list_bk($params);

		//print_r($item);die;

		$list = '';
		foreach ($item as $items) {

			$list .= '<option value="' . $items['id'] . '" >' . $items['date'] . ' - ' . $items['note'] . ' - ' . number_format($items['value_real'], '2', ',', '.') . '</option>';
		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function get_list_bk_p()
	{

		$params = array(
			'name' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
			'tipeb' => $this->Anti_sql_injection($this->input->post('tipeb', TRUE))
		);



		$item = $this->purchase_order_model->get_list_bk_p($params);

		//print_r($item);die;

		$list = '';
		foreach ($item as $items) {

			$list .= '<option value="' . $items['id_invoice'] . '" >' . $items['no_invoice'] . ' - ' . $items['cust_name'] . ' - Rp. ' . number_format($items['sisa'], '2', ',', '.') . '</option>';
		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function get_invoice()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$result['principal'] = $this->purchase_order_model->get_principal_byid($params);
		//$item = $this->purchase_order_model->get_item_byprin($params);
		$item = $this->purchase_order_model->get_invoice_byprin($params);

// echo "aaaa";die;
		//print_r($item);die;

		$list = '<option></option>';
		//$list = '';
		foreach ($item as $items) {

			$list .= '<option value="' . $items['id_invoice'] . '|' . $items['bayar']. '|' . $items['sisa'] . '|' . $items['amount'] . '|' . $items['due_date']. '|'. $items['bayar_giro'] . '" >' . $items['no_invoice']. '</option>';
		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function get_po_detail()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		//print_r($params);die;

		$result['invoice'] = $this->purchase_order_model->get_po_byid($params);
		$result['id_pos'] = $params['id'];
		$Date = $result['invoice'][0]['date_po'];
		$result['due_date'] = date('Y-m-d', strtotime($Date . ' + ' . $result['invoice'][0]['term_of_payment'] . ' days'));
		//print_r($result['invoice']);die;

		$item = $this->purchase_order_model->get_po_detail_full($params);
		//print_r($item);die;

		$htl = '';
		$new_fl = 0;
		$total_amount = 0;
		$sub_total_amount = 0;
		$sub_diskon = 0;
		foreach ($item as $items) {

			$htl .=	'<div id="row_' . $new_fl . '" >';
			$htl .=	'									<div class="row" style="border-top-style:solid;">';
			$htl .=	'									<div style="margin-top:10px">';
			$htl .=	'										<div class="col-lg-5 col-md-5 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="kode_' . $new_fl . '" id="kode_' . $new_fl . '" type="text" class="form-control" placeholder="Qty" value="' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '" readOnly  >';
			$htl .=	'												<input name="po_mat_' . $new_fl . '" id="po_mat_' . $new_fl . '" type="hidden" class="form-control" placeholder="Qty" value="' . $items['id_bpb_detail'] . '" readOnly  >';
			$htl .=	'												<input name="idpo_mat_' . $new_fl . '" id="idpo_mat_' . $new_fl . '" type="hidden" class="form-control" placeholder="Qty" value="' . $items['id_t_ps'] . '" readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_' . $new_fl . '" id="qty_' . $new_fl . '" type="text" class="form-control rupiah" onKeydown="change_sum(' . $new_fl . ')" placeholder="Qty" value="' . number_format($items['qty_order'], 2, ',', '.') . '" ReadOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="harga_' . $new_fl . '" id="harga_' . $new_fl . '" type="text" class="form-control rupiah" onKeyup="change_sum(' . $new_fl . ')" placeholder="Harga" value="' . number_format($items['unit_price'], 2, ',', '.') . '" >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="diskon_' . $new_fl . '" id="diskon_' . $new_fl . '" type="text" class="form-control rupiah" onKeyup="change_sum(' . $new_fl . ')" placeholder="Diskon" value="' . number_format($items['diskon'], 2, ',', '.') . '" >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="total_' . $new_fl . '" id="total_' . $new_fl . '" type="text" class="form-control rupiah" placeholder="Total" value="' . number_format(((floatval($items['unit_price']) * floatval($items['qty_order'])) - ((floatval($items['unit_price']) * floatval($items['qty_order'])) * (floatval($items['diskon']) / 100))), 2, ',', '.') . '" readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'									</div>';
			$htl .=	'									</div>';
			$htl .=	'								</div>';

			$new_fl++;

			$total_amount = $total_amount + ((floatval($items['unit_price']) * floatval($items['qty_order'])) - ((floatval($items['unit_price']) * floatval($items['qty_order'])) * (floatval($items['diskon']) / 100)));
			$sub_total_amount = $sub_total_amount + ((floatval($items['unit_price']) * floatval($items['qty_order'])));
			$sub_diskon = $sub_diskon + ((floatval($items['unit_price']) * floatval($items['qty_order'])) * (floatval($items['diskon']) / 100));
			//$sub_diskon = $sub_diskon + ((intval($items['unit_price'])*intval($items['qty_order']))*(floatval($items['diskon'])/100));

			//$total_amount = $total_amount + $items['price'];

		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';

		$result['list'] = $htl;
		$result['int_val'] = $new_fl;
		$result['total_amount'] = number_format($total_amount, 2, ',', '.');
		$result['sub_total_amount'] = number_format($sub_total_amount, 2, ',', '.');
		$result['sub_diskon'] = number_format($sub_diskon, 2, ',', '.');
		$result['sub_ppn'] = number_format($total_amount * (floatval($result['invoice'][0]['ppn']) / 100), 2, ',', '.');

		$result['grand_total'] = number_format(($total_amount * (floatval($result['invoice'][0]['ppn']) / 100)) + $total_amount, 2, ',', '.');

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function get_po_detail_detail()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		//print_r($params);die;

		$result['invoice'] = $this->purchase_order_model->get_po_by_inv($params);
		$result['id_pos'] = $params['id'];
		$Date = $result['invoice'][0]['date_po'];
		$result['due_date'] = date('Y-m-d', strtotime($Date . ' + ' . $result['invoice'][0]['term_of_payment'] . ' days'));
		//print_r($result['invoice']);die;

		$item = $this->purchase_order_model->get_po_detail_full_edit($params);
		//print_r($item);die;
 
		$htl = '';
		$new_fl = 0;
		$total_amount = 0;
		$sub_total_amount = 0;
		$sub_diskon = 0;
		foreach ($item as $items) {

			$htl .=	'<div id="row_' . $new_fl . '" >';
			$htl .=	'									<div class="row" style="border-top-style:solid;">';
			$htl .=	'									<div style="margin-top:10px">';
			$htl .=	'										<div class="col-lg-5 col-md-5 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="kode_' . $new_fl . '" id="kode_' . $new_fl . '" type="text" class="form-control" placeholder="Qty" value="' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '" readOnly  >';
			$htl .=	'												<input name="po_mat_' . $new_fl . '" id="po_mat_' . $new_fl . '" type="hidden" class="form-control" placeholder="Qty" value="' . $items['id_bpb_detail'] . '" readOnly  >';
			$htl .=	'												<input name="idpo_mat_' . $new_fl . '" id="idpo_mat_' . $new_fl . '" type="hidden" class="form-control" placeholder="Qty" value="' . $items['id_t_ps'] . '" readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_' . $new_fl . '" id="qty_' . $new_fl . '" type="text" class="form-control rupiah" onKeydown="change_sum(' . $new_fl . ')" placeholder="Qty" value="' . number_format($items['qty_order'], 2, ',', '.') . '" ReadOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="harga_' . $new_fl . '" id="harga_' . $new_fl . '" type="text" class="form-control rupiah" onKeyup="change_sum(' . $new_fl . ')" placeholder="Harga" value="' . number_format($items['unit_price'], 2, ',', '.') . '" readOnly>';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="diskon_' . $new_fl . '" id="diskon_' . $new_fl . '" type="text" class="form-control rupiah" onKeyup="change_sum(' . $new_fl . ')" placeholder="Diskon" value="' . number_format($items['diskon'], 2, ',', '.') . '" readOnly>';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="total_' . $new_fl . '" id="total_' . $new_fl . '" type="text" class="form-control rupiah" placeholder="Total" value="' . number_format(((floatval($items['unit_price']) * floatval($items['qty_order'])) - ((floatval($items['unit_price']) * floatval($items['qty_order'])) * (floatval($items['diskon']) / 100))), 2, ',', '.') . '" readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'									</div>';
			$htl .=	'									</div>';
			$htl .=	'								</div>';

			$new_fl++;

			$total_amount = $total_amount + ((floatval($items['unit_price']) * floatval($items['qty_order'])) - ((floatval($items['unit_price']) * floatval($items['qty_order'])) * (floatval($items['diskon']) / 100)));
			$sub_total_amount = $sub_total_amount + ((floatval($items['unit_price']) * floatval($items['qty_order'])));
			$sub_diskon = $sub_diskon + ((floatval($items['unit_price']) * floatval($items['qty_order'])) * (floatval($items['diskon']) / 100));
			//$sub_diskon = $sub_diskon + ((intval($items['unit_price'])*intval($items['qty_order']))*(floatval($items['diskon'])/100));

			//$total_amount = $total_amount + $items['price'];

		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';

		$result['list'] = $htl;
		$result['int_val'] = $new_fl;
		$result['total_amount'] = number_format($total_amount, 2, ',', '.');
		$result['sub_total_amount'] = number_format($sub_total_amount, 2, ',', '.');
		$result['sub_diskon'] = number_format($sub_diskon, 2, ',', '.');
		$result['sub_ppn'] = number_format($total_amount * (floatval($result['invoice'][0]['ppn']) / 100), 2, ',', '.');

		$result['grand_total'] = number_format(($total_amount * (floatval($result['invoice'][0]['ppn']) / 100)) + $total_amount, 2, ',', '.');

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function get_po_detail_edit()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		//print_r($params);die;

		$result['invoice'] = $this->purchase_order_model->get_po_by_inv($params);
		$result['id_pos'] = $params['id'];
		$Date = $result['invoice'][0]['date_po'];
		$result['due_date'] = date('Y-m-d', strtotime($Date . ' + ' . $result['invoice'][0]['term_of_payment'] . ' days'));
		//print_r($result['invoice']);die;

		$item = $this->purchase_order_model->get_po_detail_full_edit($params);
		//print_r($item);die;

		$htl = '';
		$new_fl = 0;
		$total_amount = 0;
		$sub_total_amount = 0;
		$sub_diskon = 0;
		foreach ($item as $items) {

			$htl .=	'<div id="row_' . $new_fl . '" >';
			$htl .=	'									<div class="row" style="border-top-style:solid;">';
			$htl .=	'									<div style="margin-top:10px">';
			$htl .=	'										<div class="col-lg-5 col-md-5 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="kode_' . $new_fl . '" id="kode_' . $new_fl . '" type="text" class="form-control" placeholder="Qty" value="' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '" readOnly  >';
			$htl .=	'												<input name="po_mat_' . $new_fl . '" id="po_mat_' . $new_fl . '" type="hidden" class="form-control" placeholder="Qty" value="' . $items['id_bpb_detail'] . '" readOnly  >';
			$htl .=	'												<input name="idpo_mat_' . $new_fl . '" id="idpo_mat_' . $new_fl . '" type="hidden" class="form-control" placeholder="Qty" value="' . $items['id_t_ps'] . '" readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_' . $new_fl . '" id="qty_' . $new_fl . '" type="text" class="form-control rupiah" onKeydown="change_sum(' . $new_fl . ')" placeholder="Qty" value="' . number_format($items['qty_order'], 2, ',', '.') . '" ReadOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="harga_' . $new_fl . '" id="harga_' . $new_fl . '" type="text" class="form-control rupiah" onKeyup="change_sum(' . $new_fl . ')" placeholder="Harga" value="' . number_format($items['unit_price'], 2, ',', '.') . '" >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="diskon_' . $new_fl . '" id="diskon_' . $new_fl . '" type="text" class="form-control rupiah" onKeyup="change_sum(' . $new_fl . ')" placeholder="Diskon" value="' . number_format($items['diskon'], 2, ',', '.') . '" >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="total_' . $new_fl . '" id="total_' . $new_fl . '" type="text" class="form-control rupiah" placeholder="Total" value="' . number_format(((floatval($items['unit_price']) * floatval($items['qty_order'])) - ((floatval($items['unit_price']) * floatval($items['qty_order'])) * (floatval($items['diskon']) / 100))), 2, ',', '.') . '" readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'									</div>';
			$htl .=	'									</div>';
			$htl .=	'								</div>';

			$new_fl++;

			$total_amount = $total_amount + ((floatval($items['unit_price']) * floatval($items['qty_order'])) - ((floatval($items['unit_price']) * floatval($items['qty_order'])) * (floatval($items['diskon']) / 100)));
			$sub_total_amount = $sub_total_amount + ((floatval($items['unit_price']) * floatval($items['qty_order'])));
			$sub_diskon = $sub_diskon + ((floatval($items['unit_price']) * floatval($items['qty_order'])) * (floatval($items['diskon']) / 100));
			//$sub_diskon = $sub_diskon + ((intval($items['unit_price'])*intval($items['qty_order']))*(floatval($items['diskon'])/100));

			//$total_amount = $total_amount + $items['price'];

		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';

		$result['list'] = $htl;
		$result['int_val'] = $new_fl;
		$result['total_amount'] = number_format($total_amount, 2, ',', '.');
		$result['sub_total_amount'] = number_format($sub_total_amount, 2, ',', '.');
		$result['sub_diskon'] = number_format($sub_diskon, 2, ',', '.');
		$result['sub_ppn'] = number_format($total_amount * (floatval($result['invoice'][0]['ppn']) / 100), 2, ',', '.');

		$result['grand_total'] = number_format(($total_amount * (floatval($result['invoice'][0]['ppn']) / 100)) + $total_amount, 2, ',', '.');

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_bayar_save()
	{
		$this->form_validation->set_rules('name', 'Nama Principal', 'required|min_length[1]|max_length[10]');
		$this->form_validation->set_rules('tgl', 'Tanggal Pembayaran Hutang', 'required');
		$this->form_validation->set_rules('casha', 'Cash Account', 'required');
		$this->form_validation->set_rules('kode_0', 'Invoice', 'required', ['required' => 'Invoice harus dipilih minimal satu']);
		// $this->form_validation->set_rules('tglterbit', 'Tanggal Terbit', 'required');

		if ($this->form_validation->run() == FALSE) {
			$msg = validation_errors();
			$result = array('success' => false, 'message' => $msg);
		} else {

			$int_val = $this->Anti_sql_injection($this->input->post('int_flo', TRUE));

			$array_items = [];

			$total_amount = 0;
			for ($i = 0; $i <= $int_val; $i++) {
				$kode_split = explode('|', $this->Anti_sql_injection($this->input->post('kode_' . $i, TRUE)));
				if ($kode_split[0]) {
					$array_items[$i]['kode'] = $kode_split[0];
					//$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE));
					$array_items[$i]['bayar'] = $this->Anti_sql_injection($this->input->post('bayar_' . $i, TRUE));
				}
			}

			//print_r($array_items);die;

			//$total_amounts = $total_amount + ($total_amount * (floatval($this->Anti_sql_injection($this->input->post('ppn', TRUE)))/100));
			
			if($this->Anti_sql_injection($this->input->post('term', TRUE)) == 4 ){
				$inv_id = $this->Anti_sql_injection($this->input->post('casha2', TRUE));
			}else{
				$inv_id = 0;
			}

			$data = array(
				'kode' => $this->Anti_sql_injection($this->input->post('kode', TRUE)),
				'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
				'customer' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
				'tipe_bayar' => $this->Anti_sql_injection($this->input->post('term', TRUE)),
				//'tipe_bayar' => 0,
				'cash_acc' => $this->Anti_sql_injection($this->input->post('casha', TRUE)),
				'inv' => $inv_id,
				'tglterbit' => $this->Anti_sql_injection($this->input->post('tglterbit', TRUE)),
				'tglaktif' => "",
				'tgljatuhtempo' => $this->Anti_sql_injection($this->input->post('tgljatuhtempo', TRUE)),
				'keterangan' => $this->Anti_sql_injection($this->input->post('keterangan', TRUE))
			);

			//print_r($data);die;


			$add_prin_result = $this->purchase_order_model->add_payment($data);
			//$this->return_model->edit_credit($data);

			// var_dump($array_items);
			// die;

			foreach ($array_items as $array_itemss) {

				if ($array_itemss['kode'] == "") {
				} else {

					$datas = array(

						'id_hp' => $add_prin_result['lastid'],
						'id_invoice' => $array_itemss['kode'],
						//'ammount' => $array_itemss['bayar'],
						'ammount' => floatval(str_replace(',', '.', str_replace('.', '', $array_itemss['bayar'])))


						//'remark' => $array_itemss['remark'],

					);

					//print_r($datas);die;

					//$id_mutasi = $this->return_model->add_mutasi($datas);
					//$this->return_model->edit_qty($datas);



					$prin_result = $this->purchase_order_model->add_detail_payment($datas);
				}
			}



			//print_r($add_prin_result);die;

			if (isset($prin_result['result']) && $prin_result['result'] > 0) {


				$result_g = $this->purchase_order_model->update_seq('pembayaran');


				$msg = 'Berhasil Menambah Pembayaran hutang';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => true, 'message' => $msg);
			} else {
				$msg = 'Gagal Menambah Pembayaran hutang';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => false, 'message' => $msg);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);

		//echo $kode;die;

	}

	public function edit_bayar_save()
	{

		$this->form_validation->set_rules('name', 'nama Principal', 'required|min_length[1]|max_length[10]');
		// $this->form_validation->set_rules('alamat', 'Alamat Principal', 'required');
		//$this->form_validation->set_rules('tgl', 'Tanggal Tidak Boleh Kosong', 'required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();

			//echo $pesan;die;

			//$msg = strip_tags(str_replace("\r", '', $pesan));
			$msg = $pesan;

			$result = array('success' => false, 'message' => $msg);
		} else {

			$int_val = $this->Anti_sql_injection($this->input->post('int_flo', TRUE));
			$id_hp = $this->Anti_sql_injection($this->input->post('id', TRUE));

			$result_dist2 		= $this->purchase_order_model->delete_hp_detail($id_hp);
			$result_dist 		= $this->purchase_order_model->delete_hp($id_hp);

			$array_items = [];

			$total_amount = 0;
			for ($i = 0; $i <= $int_val; $i++) {


				$kode_split = explode('|', $this->Anti_sql_injection($this->input->post('kode_' . $i, TRUE)));
				if ($kode_split[0]) {
					$array_items[$i]['kode'] = $kode_split[0];
					//$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE));
					$array_items[$i]['bayar'] = $this->Anti_sql_injection($this->input->post('bayar_' . $i, TRUE));
				}
			}

			//print_r($array_items);die;

			//$total_amounts = $total_amount + ($total_amount * (floatval($this->Anti_sql_injection($this->input->post('ppn', TRUE)))/100));

			$data = array(
				'kode' => $this->Anti_sql_injection($this->input->post('kode', TRUE)),
				'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
				'customer' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
				'tipe_bayar' => $this->Anti_sql_injection($this->input->post('term', TRUE)),
				'inv' => 0,
				'cash_acc' => $this->Anti_sql_injection($this->input->post('casha', TRUE)),
				'tglterbit' => $this->Anti_sql_injection($this->input->post('tglterbit', TRUE)),
				'tglaktif' => "",
				'tgljatuhtempo' => $this->Anti_sql_injection($this->input->post('tgljatuhtempo', TRUE)),
				'keterangan' => $this->Anti_sql_injection($this->input->post('keterangan', TRUE))
			);

			//print_r($data);die;


			$add_prin_result = $this->purchase_order_model->add_payment($data);
			//$this->return_model->edit_credit($data);

			foreach ($array_items as $array_itemss) {

				if ($array_itemss['kode'] == "") {
				} else {

					$datas = array(

						'id_hp' => $add_prin_result['lastid'],
						'id_invoice' => $array_itemss['kode'],
						//'ammount' => $array_itemss['bayar'],
						'ammount' => floatval(str_replace(',', '.', str_replace('.', '', $array_itemss['bayar'])))


						//'remark' => $array_itemss['remark'],

					);

					//print_r($datas);die;

					//$id_mutasi = $this->return_model->add_mutasi($datas);
					//$this->return_model->edit_qty($datas);



					$prin_result = $this->purchase_order_model->add_detail_payment($datas);
				}
			}



			//print_r($add_prin_result);die;

			if ($prin_result['result'] > 0) {


				$result_g = $this->purchase_order_model->update_seq('pembayaran');


				$msg = 'Berhasil Merubah Pembayaran hutang';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => true, 'message' => $msg);
			} else {
				$msg = 'Gagal Merubah Pembayaran hutang';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => false, 'message' => $msg);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);

		//echo $kode;die;

	}

	public function get_all_item()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$item = $this->purchase_order_model->get_item_byprin($params);

		$list = '<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		foreach ($item as $items) {

			$list .= '<option value="' . $items['id_mat'] . '" >' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '</option>';
		}
		$list .= '<option value="0" selected="selected" disabled>-- Pilih Item Lainnya --</option>';

		$item2 = $this->purchase_order_model->get_item_byprin_all($params);

		foreach ($item2 as $items) {

			$list .= '<option value="' . $items['id_mat'] . '" >' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '</option>';
		}
		//	$list .= '------------------';

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}


	public function get_price_mat()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		//print_r($params);die;

		$ddd = $this->purchase_order_model->get_price_mat($params);
		//$item = $this->purchase_order_model->get_item_byprin($params);

		if (count($ddd) == 0) {
			$list = 0;
		} else {
			$list = number_format($ddd[0]['unit_price'], 0, ',', '.');
		}
		//print_r($ddd);die;

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add()
	{
		//$result = $this->purchase_order_model->get_principal();
		$result = $this->purchase_order_model->get_principal();
		$cash_account = $this->purchase_order_model->get_cash_account();

		$year_count = date('Y') - 2019;

		$filter_year = '<select class="form-control" id="filter_year" onChange="changeFilter()"><option value="ALL" >ALL</option>';

		for ($i = $year_count; $i >= 0; $i--) {

			$y = 2019 + $i;

			if ($y == date('Y')) {
				$filter_year .= '<option value="' . $y . '" selected="selected" >' . $y . '</option>';
			} else {
				$filter_year .= '<option value="' . $y . '" >' . $y . '</option>';
			}
		}
		$filter_year .= '</select>';

		$array_month = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

		$filter_month = '<select class="form-control" id="filter_month" onChange="changeFilter()"><option value="ALL" >ALL</option>';
		foreach ($array_month as $array_months) {
			if ($array_months == date('F')) {
				$filter_month .= '<option value="' . $array_months . '" selected="selected">' . $array_months . '</option>';
			} else {
				$filter_month .= '<option value="' . $array_months . '" >' . $array_months . '</option>';
			}
		}

		$filter_month .= '</select>';
		$today_month = date('F');
		$today_year = date('Y');

		$result_g = $this->purchase_order_model->get_kode();
		if ($result_g[0]['max'] + 1 < 1000) {
			if ($result_g[0]['max'] + 1 < 100) {
				if ($result_g[0]['max'] + 1 < 10) {
					$kode = 'K' . date('Y') . '000' . ($result_g[0]['max'] + 1);
				} else {
					$kode = 'K' . date('Y') . '00' . ($result_g[0]['max'] + 1);
				}
			} else {
				$kode = 'K' . date('Y') . '0' . ($result_g[0]['max'] + 1);
			}
		} else {
			$kode = 'K' . date('Y') . '' . ($result_g[0]['max'] + 1);
		}
		//print_r($kode);die;
		$priv = $this->priv->get_priv();


		$data = array(
			'principal' => $result,
			'cash_account' => $cash_account,
			'kode' => $kode,
			'priv' => $priv,
			'filter_month' => $filter_month,
			'filter_year' => $filter_year,
			'today_month' => $today_month,
			'today_year' => $today_year
		);
		$this->template->load('maintemplate', 'hutang/views/addIndex', $data);
		//$this->load->view('addPrinciple',$data);
	}

	public function add_bayar()
	{
		//$result = $this->purchase_order_model->get_principal();
		$result = $this->purchase_order_model->get_principal();
		$cash_account = $this->purchase_order_model->get_cash_account();

		$result_g = $this->purchase_order_model->get_kode();
		if ($result_g[0]['max'] + 1 < 1000) {
			if ($result_g[0]['max'] + 1 < 100) {
				if ($result_g[0]['max'] + 1 < 10) {
					$kode = 'K' . date('Y') . '000' . ($result_g[0]['max'] + 1);
				} else {
					$kode = 'K' . date('Y') . '00' . ($result_g[0]['max'] + 1);
				}
			} else {
				$kode = 'K' . date('Y') . '0' . ($result_g[0]['max'] + 1);
			}
		} else {
			$kode = 'K' . date('Y') . '' . ($result_g[0]['max'] + 1);
		}
		//print_r($kode);die;

		$data = array(
			'principal' => $result,
			'cash_account' => $cash_account,
			'kode' => $kode
		);
		$this->template->load('maintemplate', 'hutang/views/add_bayar', $data);
		//$this->load->view('addPrinciple',$data);
	}


	public function updatepo()
	{
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			//	'id_invoice' => $this->Anti_sql_injection($this->input->post('id_invoice', TRUE)),

		);

		//print_r($data);die;

		$po_result = $this->purchase_order_model->get_po_by_inv($data);
		$po_detail_result = $this->purchase_order_model->get_po_detail($data);
		//$result = $this->purchase_order_model->get_principal();
		$result = $this->purchase_order_model->get_po();

		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);

		$item = $this->purchase_order_model->get_item_byprin($params);
		//print_r($principal_result);die;

		// $result = $this->distributor_model->location();

		$data = array(
			'po' => $po_result[0],
			'principal' => $result,
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			'po_mat' => $po_detail_result,
			'item' => $item
		);

		$this->template->load('maintemplate', 'hutang/views/updatePo', $data);
		//$this->load->view('addPrinciple',$data);
	}

	public function detail()
	{
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			//	'id_invoice' => $this->Anti_sql_injection($this->input->post('id_invoice', TRUE)),

		);

		//print_r($data);die;

		$po_result = $this->purchase_order_model->get_po_by_inv($data);
		$po_detail_result = $this->purchase_order_model->get_po_detail($data);
		//$result = $this->purchase_order_model->get_principal();
		$result = $this->purchase_order_model->get_po();

		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);

		$item = $this->purchase_order_model->get_item_byprin($params);
		//print_r($principal_result);die;

		// $result = $this->distributor_model->location();

		$data = array(
			'po' => $po_result[0],
			'principal' => $result,
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			'po_mat' => $po_detail_result,
			'item' => $item
		);

		$this->template->load('maintemplate', 'hutang/views/detailPo', $data);
		//$this->load->view('addPrinciple',$data);
	}

	public function detail_bayar()
	{
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			//	'id_invoice' => $this->Anti_sql_injection($this->input->post('id_invoice', TRUE)),

		);

		//print_r($data);die;

		$po_result = $this->purchase_order_model->get_po_by_payment($data);
		$cash_account = $this->purchase_order_model->get_cash_account();
		$po_detail_result = $this->purchase_order_model->get_po_detail($data);
		$detail_bayar = $this->purchase_order_model->get_bayar_inv($data);
		$result = $this->purchase_order_model->get_principal();
		//$result = $this->purchase_order_model->get_po();
		$resultby = $this->purchase_order_model->get_bayar($data);

		$pms = array(
			'id_prin' => $resultby[0]['id_distributor']
		);

		$item2 = $this->purchase_order_model->get_invoice_byprin($pms);

		//print_r($resultby);die;


		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);

		$item = $this->purchase_order_model->get_item_byprin($params);
		//print_r($principal_result);die;

		// $result = $this->distributor_model->location();

		$data = array(
			'po' => $po_result[0],
			'bayar' => $resultby,
			'principal' => $result,
			'detail_bayar' => $detail_bayar,
			'cash_account' => $cash_account,
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			'po_mat' => $po_detail_result,
			//'list' => $list,
			'item2' => $item2,
			'item' => $item
		);


		$this->template->load('maintemplate', 'hutang/views/detail_bayar', $data);
		//$this->load->view('addPrinciple',$data);
	}

	public function bayar()
	{
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			//	'id_invoice' => $this->Anti_sql_injection($this->input->post('id_invoice', TRUE)),

		);

		//print_r($data);die;

		$po_result = $this->purchase_order_model->get_po_by_inv($data);
		$cash_account = $this->purchase_order_model->get_cash_account();
		$result = $this->purchase_order_model->get_principal();
		//$cash_account = $this->purchase_order_model->get_cash_account();
		$po_detail_result = $this->purchase_order_model->get_po_detail($data);
		$detail_bayar = $this->purchase_order_model->get_bayar_inv($data);
		//$result = $this->purchase_order_model->get_principal();
		//$result = $this->purchase_order_model->get_po();

		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);

		$item = $this->purchase_order_model->get_item_byprin($params);
		//print_r($principal_result);die;

		// $result = $this->distributor_model->location();

		$data = array(
			'po' => $po_result[0],
			'principal' => $result,
			'detail_bayar' => $detail_bayar,
			'cash_account' => $cash_account,
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			'po_mat' => $po_detail_result,
			'item' => $item
		);

		$this->template->load('maintemplate', 'hutang/views/bayarPo', $data);
		//$this->load->view('addPrinciple',$data);
	}


	public function edit_bayar()
	{
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			//	'id_invoice' => $this->Anti_sql_injection($this->input->post('id_invoice', TRUE)),

		);

		//print_r($data);die;

		$po_result = $this->purchase_order_model->get_po_by_payment($data);
		$cash_account = $this->purchase_order_model->get_cash_account();
		$po_detail_result = $this->purchase_order_model->get_po_detail($data);
		$detail_bayar = $this->purchase_order_model->get_bayar_inv($data);
		$result = $this->purchase_order_model->get_principal();
		//$result = $this->purchase_order_model->get_po();
		$resultby = $this->purchase_order_model->get_bayar($data);

		$pms = array(
			'id_prin' => $resultby[0]['id_distributor']
		);

		$item2 = $this->purchase_order_model->get_invoice_byprin($pms);

		//print_r($item);die;


		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);

		$item = $this->purchase_order_model->get_item_byprin($params);
		//print_r($principal_result);die;

		// $result = $this->distributor_model->location();

		$data = array(
			'po' => $po_result[0],
			'bayar' => $resultby,
			'principal' => $result,
			'detail_bayar' => $detail_bayar,
			'cash_account' => $cash_account,
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			'po_mat' => $po_detail_result,
			//'list' => $list,
			'item2' => $item2,
			'item' => $item
		);



		$this->template->load('maintemplate', 'hutang/views/edit_bayar', $data);
		//$this->load->view('addPrinciple',$data);
	}

	public function konfirmasi()
	{
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			//	'id_invoice' => $this->Anti_sql_injection($this->input->post('id_invoice', TRUE)),

		);

		//print_r($data);die;

		$po_result = $this->purchase_order_model->get_po_by_payment($data);
		$cash_account = $this->purchase_order_model->get_cash_account();
		$po_detail_result = $this->purchase_order_model->get_po_detail($data);
		$detail_bayar = $this->purchase_order_model->get_bayar_inv($data);
		$result = $this->purchase_order_model->get_principal();
		//$result = $this->purchase_order_model->get_po();
		$resultby = $this->purchase_order_model->get_bayar($data);

		$pms = array(
			'id_prin' => $resultby[0]['id_distributor']
		);

		$item2 = $this->purchase_order_model->get_invoice_byprin($pms);

		//print_r($resultby);die;


		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);

		$item = $this->purchase_order_model->get_item_byprin($params);
		//print_r($item);die;

		// $result = $this->distributor_model->location();

		$data = array(
			'po' => $po_result[0],
			'bayar' => $resultby,
			'principal' => $result,
			'detail_bayar' => $detail_bayar,
			'cash_account' => $cash_account,
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			'po_mat' => $po_detail_result,
			//'list' => $list,
			'item2' => $item2,
			'item' => $item
		);

		$this->template->load('maintemplate', 'hutang/views/approvallPo', $data);
		//$this->load->view('addPrinciple',$data);
	}

	public function approve_po()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$data = array(
			'id_po' => $params['id'],
			'sts' => $params['sts']

		);

		//print_r($data);die;

		$add_prin_result = $this->purchase_order_model->edit_po_apr($data);
	}

	public function konfirmasi_bayar()
	{



		$id = $this->Anti_sql_injection($this->input->post('id', TRUE));

		$result = $this->purchase_order_model->get_payment_detail($id);
		$result_now  = $this->purchase_order_model->get_payment_detail_now($id);

		//print_r($result);die;
		$u = 0;
		foreach ($result as $resultss) {

			//print_r($result);die;
			
			if($resultss['account_type'] == 0 || $resultss['account_type'] == 1){

				$data = array(
					'user_id' => $this->session->userdata['logged_in']['user_id'],
					'coa'     => $resultss['cash_account'],
					'tgl' => date('Y-m-d'),
					'date_insert' => date('Y-m-d H:i:s'),
					'jumlah' => $resultss['bayar'],
					'bukti' => "Bayar Hutang",
					'id_coa_temp' => $id,
					'ket' => '',
					'id_parent' => 0,
					'type_cash' => 1
				);
				
				$results = $this->purchase_order_model->add_uom($data);

			}
			
			

			if ($result[0]['due_date'] <> '1970-01-01') {
				$data = array(
					'user_id' => $this->session->userdata['logged_in']['user_id'],
					'coa'     => 58,
					'tgl' => date('Y-m-d'),
					'date_insert' => date('Y-m-d H:i:s'),
					'jumlah' => $resultss['bayar'],
					'bukti' => "Bayar Hutang",
					'id_coa_temp' => $id,
					'ket' => '',
					'id_parent' => $results['lastid'],
					'type_cash' => 1
				);

				$prin_result = $this->purchase_order_model->add_uom($data);
			}



			if ($resultss['sisa'] - $resultss['bayar'] <= 0) {
				$this->purchase_order_model->update_invoice_sts($result[0]['id_invoice']);
			}

			$this->purchase_order_model->update_konfirmas($id);

			$u++;
		}

		// if (isset($prin_result['result']) && $prin_result['result'] > 0) {
		if (isset($prin_result['result'])) {

			$msg = 'Berhasil Konfirmasi Pembayaran Hutang';

			$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
			$result = array('success' => true, 'message' => $msg);
		} else {
			$msg = 'Gagal Konfirmasi Pembayaran Hutang';

			$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
			$result = array('success' => false, 'message' => $msg);
		}




		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);

		//echo $kode;die;

	}

	public function add_po()
	{

		// $this->form_validation->set_rules('kode', 'Kode Principal', 'required|min_length[4]|max_length[10]');
		// $this->form_validation->set_rules('alamat', 'Alamat Principal', 'required');
		$this->form_validation->set_rules('tgl', 'Nama Principal', 'required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();

			//echo $pesan;die;

			//$msg = strip_tags(str_replace("\r", '', $pesan));
			$msg = $pesan;

			$result = array('success' => false, 'message' => $msg);
		} else {

			$int_val = $this->Anti_sql_injection($this->input->post('int_flo', TRUE));

			$array_items = [];

			$total_amount = 0;
			for ($i = 0; $i <= $int_val; $i++) {

				//if(isset($this->input->post('kode_'.$i, TRUE))){
				$array_items[$i]['po_mat'] = $this->Anti_sql_injection($this->input->post('po_mat_' . $i, TRUE));
				$array_items[$i]['idpo_mat'] = $this->Anti_sql_injection($this->input->post('idpo_mat_' . $i, TRUE));
				$array_items[$i]['kode'] = $this->Anti_sql_injection($this->input->post('kode_' . $i, TRUE));
				$array_items[$i]['harga'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE)))));
				$array_items[$i]['diskon'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('diskon_' . $i, TRUE)))));
				$array_items[$i]['total'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('total_' . $i, TRUE)))));
				//$array_items[$i]['remark'] = $this->Anti_sql_injection($this->input->post('remark_'.$i, TRUE));
				$array_items[$i]['remark'] = "";
				$array_items[$i]['qty'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('qty_' . $i, TRUE)))));
				$array_items[$i]['total_amount'] = (($array_items[$i]['harga'] * $array_items[$i]['qty']) - (($array_items[$i]['harga'] * ($array_items[$i]['diskon'] / 100)) * $array_items[$i]['qty']));

				//}

				$total_amount = $total_amount + (($array_items[$i]['harga'] * $array_items[$i]['qty']) - (($array_items[$i]['harga'] * ($array_items[$i]['diskon'] / 100)) * $array_items[$i]['qty']));
			}

			$total_amounts = $total_amount + ($total_amount * (floatval($this->Anti_sql_injection($this->input->post('ppn', TRUE))) / 100));

			//echo $total_amounts;die;

			$result_g = $this->purchase_order_model->get_kode();
			$new_seq = $result_g[0]['max'] + 1;

			if ($this->Anti_sql_injection($this->input->post('term', TRUE)) == 'other') {
				$terms = $this->Anti_sql_injection($this->input->post('term_o', TRUE));
			} else {
				$terms = $this->Anti_sql_injection($this->input->post('term', TRUE));
			}


			$data = array(
				'id_bpb' => $this->Anti_sql_injection($this->input->post('no_pos', TRUE)),
				'note' => $this->Anti_sql_injection($this->input->post('ket_inv', TRUE)),
				'tanggal_invoice' => $this->Anti_sql_injection($this->input->post('tgl_inv', TRUE)),
				'due_date' => $this->Anti_sql_injection($this->input->post('tgl_due', TRUE)),
				'no_invoice' => $this->Anti_sql_injection($this->input->post('no_inv', TRUE)),
				'faktur_pajak' => $this->Anti_sql_injection($this->input->post('fak_paj', TRUE)),
				'tanggal_faktur' => $this->Anti_sql_injection($this->input->post('tgl_fak', TRUE)),
				'attention' => $this->Anti_sql_injection($this->input->post('att', TRUE))
			);

			$data2 = array(
				'id_po' => $this->Anti_sql_injection($this->input->post('no_pos', TRUE)),
				'total_amount' => $total_amounts
			);

			$this->purchase_order_model->edit_po($data2);
			//print_r($data);die;


			$add_prin_result = $this->purchase_order_model->add_invoice($data);

			foreach ($array_items as $array_itemss) {

				if ($array_itemss['kode'] == "") {
				} else {

					$datas = array(

						'id_inv_pembelian' => $add_prin_result['lastid'],
						'id_bpb_detail' => $array_itemss['po_mat'],
						'unit_price' => $array_itemss['harga'],
						'price' =>  $array_itemss['total_amount'],
						'qty' => $array_itemss['qty'],
						//'remark' => $array_itemss['remark'],
						'diskon' => $array_itemss['diskon']

					);


					$datas_mat = array(

						'id_inv_pembelian' => $add_prin_result['lastid'],
						'id_bpb_detail' => $array_itemss['po_mat'],
						'id_po_mat' => $array_itemss['idpo_mat'],
						'unit_price' => $array_itemss['harga'],
						'price' =>  $array_itemss['total_amount'],
						'qty' => $array_itemss['qty'],
						//'remark' => $array_itemss['remark'],
						'diskon' => $array_itemss['diskon']

					);

					$prin_result = $this->purchase_order_model->add_detail_invoice($datas);
					$this->purchase_order_model->edit_po_mat($datas_mat);
				}
			}



			//print_r($add_prin_result);die;

			if ($prin_result['result'] > 0) {





				$msg = 'Berhasil Menambah data Pembayaran Hutang';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => true, 'message' => $msg);
			} else {
				$msg = 'Gagal Menambah data Pembayaran Hutang';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => false, 'message' => $msg);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);

		//echo $kode;die;

	}

	public function insert_invoice_am()
	{

		$int_val = $this->Anti_sql_injection($this->input->post('int_flo2', TRUE));
		$id_invoice = $this->Anti_sql_injection($this->input->post('id_invoice', TRUE));

		$array_items = [];

		$total_amount = 0;
		for ($i = 0; $i <= $int_val; $i++) {

			//if(isset($this->input->post('cash_account'.$i, TRUE))){
			$array_items[$i]['cash_account'] = $this->Anti_sql_injection($this->input->post('casha_' . $i, TRUE));
			$array_items[$i]['tgl_terbit'] = $this->Anti_sql_injection($this->input->post('tglterbit_' . $i, TRUE));
			$array_items[$i]['tgl_aktif'] = $this->Anti_sql_injection($this->input->post('tglaktif_' . $i, TRUE));
			$array_items[$i]['tgl_jatuh_tempo'] = $this->Anti_sql_injection($this->input->post('tgltempo_' . $i, TRUE));
			$array_items[$i]['amount'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('total_inv_' . $i, TRUE)))));
			$array_items[$i]['id_invoice'] = $id_invoice;
			//$array_items[$i]['int_val'] = $int_val;



			//}

		}

		foreach ($array_items as $array_itemss) {

			if ($array_itemss['cash_account'] == "") {
			} else {

				$prin_result = $this->purchase_order_model->add_pay_invoice($array_itemss);
			}
		}



		if ($prin_result['result'] > 0) {

			$msg = 'Berhasil Menambah Pembayaran hutang';

			$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
			$result = array('success' => true, 'message' => $msg);
		} else {
			$msg = 'Gagal Menambah Pembayaran hutang';

			$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
			$result = array('success' => false, 'message' => $msg);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));



		//print_r($array_items);die;


	}

	public function update_po()
	{

		// $this->form_validation->set_rules('kode', 'Kode Principal', 'required|min_length[4]|max_length[10]');
		// $this->form_validation->set_rules('alamat', 'Alamat Principal', 'required');
		$this->form_validation->set_rules('tgl', 'Nama Principal', 'required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();

			//echo $pesan;die;

			//$msg = strip_tags(str_replace("\r", '', $pesan));
			$msg = $pesan;

			$result = array('success' => false, 'message' => $msg);
		} else {

			$int_val = $this->Anti_sql_injection($this->input->post('int_flo', TRUE));

			$array_items = [];

			$total_amount = 0;
			for ($i = 0; $i <= $int_val; $i++) {

				//if(isset($this->input->post('kode_'.$i, TRUE))){
				$array_items[$i]['kode'] = $this->Anti_sql_injection($this->input->post('kode_' . $i, TRUE));
				$array_items[$i]['po_mat'] = $this->Anti_sql_injection($this->input->post('po_mat_' . $i, TRUE));
				$array_items[$i]['idpo_mat'] = $this->Anti_sql_injection($this->input->post('idpo_mat_' . $i, TRUE));
				$array_items[$i]['harga'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE)))));
				$array_items[$i]['diskon'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('diskon_' . $i, TRUE)))));
				$array_items[$i]['total'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('total_' . $i, TRUE)))));
				//$array_items[$i]['remark'] = $this->Anti_sql_injection($this->input->post('remark_'.$i, TRUE));
				$array_items[$i]['remark'] = "";
				$array_items[$i]['qty'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('qty_' . $i, TRUE)))));
				$array_items[$i]['total_amount'] = (($array_items[$i]['harga'] * $array_items[$i]['qty']) - (($array_items[$i]['harga'] * ($array_items[$i]['diskon'] / 100)) * $array_items[$i]['qty']));
				//}

				$total_amount = $total_amount + (($array_items[$i]['harga'] * $array_items[$i]['qty']) - (($array_items[$i]['harga'] * ($array_items[$i]['diskon'] / 100)) * $array_items[$i]['qty']));
			}

			$total_amounts = $total_amount + ($total_amount * (floatval($this->Anti_sql_injection($this->input->post('ppn', TRUE))) / 100));

			$result_g = $this->purchase_order_model->get_kode();
			$new_seq = $result_g[0]['max'] + 1;

			if ($this->Anti_sql_injection($this->input->post('term_o', TRUE)) == 'other') {
				$terms = $this->Anti_sql_injection($this->input->post('term_o', TRUE));
			} else {
				$terms = $this->Anti_sql_injection($this->input->post('term_o', TRUE));
			}

			$data = array(
				'id_invoice' => $this->Anti_sql_injection($this->input->post('id_invoice', TRUE)),
				'note' => $this->Anti_sql_injection($this->input->post('ket_inv', TRUE)),
				'tanggal_invoice' => $this->Anti_sql_injection($this->input->post('tgl_inv', TRUE)),
				'due_date' => $this->Anti_sql_injection($this->input->post('tgl_due', TRUE)),
				'no_invoice' => $this->Anti_sql_injection($this->input->post('no_inv', TRUE)),
				'faktur_pajak' => $this->Anti_sql_injection($this->input->post('fak_paj', TRUE)),
				'tanggal_faktur' => $this->Anti_sql_injection($this->input->post('tgl_fak', TRUE)),
				'attention' => $this->Anti_sql_injection($this->input->post('att', TRUE))
			);

			$data2 = array(
				'id_po' => $this->Anti_sql_injection($this->input->post('no_pos', TRUE)),
				'total_amount' => $total_amounts
			);

			$this->purchase_order_model->edit_po($data2);

			$add_prin_result = $this->purchase_order_model->edit_inv($data);

			$this->purchase_order_model->delete_po_mat($this->Anti_sql_injection($this->input->post('id_invoice', TRUE)));

			foreach ($array_items as $array_itemss) {

				if ($array_itemss['kode'] == "") {
				} else {

					$datas = array(

						'id_inv_pembelian' => $this->Anti_sql_injection($this->input->post('id_invoice', TRUE)),
						'id_bpb_detail' => $array_itemss['po_mat'],
						'unit_price' => $array_itemss['harga'],
						'price' =>  $array_itemss['total_amount'],
						'qty' => $array_itemss['qty'],
						//'remark' => $array_itemss['remark'],
						'diskon' => $array_itemss['diskon']

					);

					$datas_mat = array(

						'id_inv_pembelian' => $add_prin_result['lastid'],
						'id_bpb_detail' => $array_itemss['po_mat'],
						'id_po_mat' => $array_itemss['idpo_mat'],
						'unit_price' => $array_itemss['harga'],
						'price' =>  $array_itemss['total_amount'],
						'qty' => $array_itemss['qty'],
						//'remark' => $array_itemss['remark'],
						'diskon' => $array_itemss['diskon']

					);

					$prin_result = $this->purchase_order_model->add_detail_invoice($datas);
					$this->purchase_order_model->edit_po_mat($datas_mat);
				}
			}



			//print_r($add_prin_result);die;

			if ($add_prin_result['result'] > 0) {





				$msg = 'Berhasil Merubah data Pembayaran Hutang';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => true, 'message' => $msg);
			} else {
				$msg = 'Gagal Menambah data Pembayaran Hutang';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => false, 'message' => $msg);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);

		//echo $kode;die;

	}

	public function delete_po()
	{
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		//print_r($params);die;

		$result_dist 		= $this->purchase_order_model->delete_po($params['id']);
		$result_dist2 		= $this->purchase_order_model->delete_po_mat($params['id']);

		$msg = 'Berhasil menghapus data Pembayaran.';
		$result = array('success' => true, 'message' => $msg);
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function delete_hp()
	{
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		//print_r($params);die;

		$result_dist2 		= $this->purchase_order_model->delete_hp_detail($params['id']);
		$result_dist 		= $this->purchase_order_model->delete_hp($params['id']);


		$msg = 'Berhasil menghapus data Pembayara.';
		$result = array('success' => true, 'message' => $msg);
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function print_pdf()
	{

		$data   	= file_get_contents("php://input");
		//$params     = json_decode($data,true);


		//echo "aaa";
		$params = (explode('=', $data));

		$data = array(
			'id' => $params[1],

		);

		//print_r($data);die;

		$po_result = $this->purchase_order_model->get_po($data);
		$po_detail_result = $this->purchase_order_model->get_po_detail_full($data);
		$result = $this->purchase_order_model->get_principal();



		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);

		$array_items = [];

		$total_sub = 0;
		$total_sub_all = 0;
		$total_disk = 0;
		$grand_total = 0;

		$htmls = '';
		foreach ($po_detail_result as $po_detail_results) {

			$amn = number_format(($po_detail_results['price'] - ($po_detail_results['price'] * ($po_detail_results['diskon'] / 100))), 0, ',', '');
			$htmls = $htmls . '
			<tr style="border-top: 1px solid black;" >
								<td  align="left" style="border-top: 1px solid black;"><p>' . $po_detail_results['stock_code'] . '</p></td>
								<td  align="left" style="border-top: 1px solid black;"><p>' . $po_detail_results['stock_name'] . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['base_qty'], '2', ',', '.') . ' ' . $po_detail_results['uom_symbol'] . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['qty_order'], '2', ',', '.') . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['unit_price'], '0', ',', '.') . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['diskon'], '2', ',', '.') . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($amn, '0', ',', '.') . '</p></td>

							</tr>
			';

			//$total_sub = $total_sub + number_format(($po_detail_results['price']-($po_detail_results['price']*($po_detail_results['diskon']/100))),0,',',''); 
			$total_sub = $total_sub + number_format(($po_detail_results['price'] - ($po_detail_results['price'] * ($po_detail_results['diskon'] / 100))), 0, ',', '');
			$total_sub_all = $total_sub_all + number_format(($po_detail_results['price']), 0, ',', '');
			$total_disk = $total_disk + ($po_detail_results['price'] * ($po_detail_results['diskon'] / 100));
			//$grand_total = $grand_total +  number_format(($po_detail_results['price']),0,',',''); 

		}

		$total_ppn = $total_sub * (floatval($po_result[0]['ppn'])) / 100;
		$grand_total = $total_ppn + $total_sub;

		//print_r($htmls);die;

		$item = $this->purchase_order_model->get_item_byprin($params);

		$rtext = '<br><p style="font-size: 10px;">PT. ENDRIA ALDA</p> ';

		$ctext = '<br><p style="font-size: 12px"> PURCHASE ORDER</p> ';

		$this->load->library('Pdf');

		$pdf = new Pdf('P', 'mm', 'A4', false, 'UTF-8', false);
		$pdf->SetTitle('Purchase Order');
		$pdf->SetHeaderMargin(25);
		$pdf->setPrintHeader(false);
		$pdf->SetTopMargin(6);
		$pdf->setFooterMargin(5);
		$pdf->SetAutoPageBreak(true);
		$pdf->SetAuthor('Author');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->SetPrintFooter(false);

		$pdf->AddPage();


		$html = '
		
		<style>
		
		  .floatedTable {
            float:left;
        }
        .inlineTable {
            display: inline-block;
        }
		p {
							font-size: 8px;
							
						}
		p {
							font-size: 8px;
							
						}
		</style>
  <table style=" float:left;border: none;" >
						<tr style="border: none;">
							<th  align="left" ><h5>PT.ENDIRA ALDA</h5></th>
							<th  align="RIGHT"></th>
						</tr>
						<tr style="border: none;">
							<th  align="left" ><h4>PURCHASE ORDER</h4></th>
							<th  align="RIGHT"><p>NOMOR, TGL : ' . $po_result[0]['no_po'] . '   ' . $po_result[0]['date_po'] . '</p></th>
						</tr>
					  </table><br><br><br>
					  
					<table>
					  <tr>
						<td>

						  <table cellspacing="0" cellpadding="0" border="0" style="float:right;width:100%.">
							<tr>
							  <td style="width:18%;"><p>Kepada</p></td>
							  <td><p>: ' . $po_result[0]['name_eksternal'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Alamat</p></td>
							  <td><p>: ' . $po_result[0]['eksternal_address'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Att</p></td>
							  <td><p>: ' . $po_result[0]['pic'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Telp</p></td>
							  <td><p>: ' . $po_result[0]['phone_1'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Fax</p></td>
							  <td><p>: ' . $po_result[0]['fax'] . '</p></td>
							</tr>

						  </table>
						</td>
						<td>
						  <table cellspacing="0" cellpadding="0" border="0" style="float:right;width:100%">
							<tr>
							  <td style="width:18%;"><p>Ship To</p></td>
							  <td><p>:JL.SANGKURIANG NO 38-A<BR>NPWP:01.555.161.7.428.000</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Email</p></td>
							  <td><p>: </p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Telp/Fax</p></td>
							  <td><p>: (022)6626-946</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Delivery</p></td>
							  <td><p>: </p></td>
							</tr>
						

						  </table>
						</td>
					  </tr>
					</table>
					<br><br><br>

					
					  <table style=" float:left;border-top: 1px solid black;border-bottom: 1px solid black;" >
						<tr style=" border-top: 1px solid black;font-size: 10px;">
							<th  align="left" >Code</th>
							<th  align="RIGHT">Description</th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Quantity<br>Lt/Kg</th>
							<th  align="RIGHT">Unit Price</th>
							<th  align="RIGHT">Disc</th>
							<th  align="RIGHT">Amount</th>
						</tr>
						' . $htmls . '
					  </table>
					  	<br><br><br>
						<br><br><br>
					  
					    <table style=" float:left;border-top: 1px solid black;" >
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Sub Total :</th>
							<th  align="RIGHT">' . number_format($total_sub_all, 0, ',', '.') . '</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Discount :</th>
							<th  align="RIGHT">' . number_format($total_disk, 0, ',', '.') . '</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"> Taxable :</th>
							<th  align="RIGHT">' . number_format($total_sub, 0, ',', '.') . '</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Vat/PPN :</th>
							<th  align="RIGHT">' . number_format($total_ppn, 0, ',', '.') . '</th>
						</tr>
					  </table>
					  
					  <table style=" float:left;border-top: 1px solid black;" >
						<tr style=" border-top: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Total :</th>
							<th  align="RIGHT">' . number_format($grand_total, 0, ',', '.') . '</th>
						</tr>
					  </table>
					<br><br><br><br><br> 
					<br><br><br><br><br> 
					
					 <table style="font-size: 10px;" >
						<tr style="">
							<th  align="CENTER" style="width:25%" ></th>
							<th  align="CENTER" style="width:12%" ></th>
							<th  align="CENTER" style="width:25%"></th>
							<th  align="CENTER" style="width:13%" ></th>
							<th  align="CENTER" style="width:25%">Cimahi, ' . date('m/d/Y') . '</th>
						</tr>
						<tr style="">
							<th  align="CENTER" style="width:25%" >Disetujui Oleh,</th>
							<th  align="CENTER" style="width:12%" ></th>
							<th  align="CENTER" style="width:25%">Diperiksa Oleh,</th>
							<th  align="CENTER" style="width:13%" ></th>
							<th  align="CENTER" style="width:25%">Dipesan Oleh,</th>
						</tr>
					  </table>
					  <br><br><br><br><br> 
					  <table style="font-size: 10px;" >
						<tr style="">
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Ratna S. Iskandar Dinata</span></th>
							<th  align="CENTER" style="width:12%"></th>
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Rienaldy Aryanto</span></th>
							<th  align="CENTER" style="width:13%"></th>
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Rangga Dean</span></th>
						</tr>
					  </table>
';

		//echo $html;die;

		// $html = <<<EOD
		// <h5>PT.ENDIRA ALDA</h5>
		// <table style=" float:left;border: none;" >
		// <tr style="border: none;">
		// <th  align="left" ><h4>PURCHASE ORDER</h4></th>
		// <th  align="RIGHT">NOMOR, TGL : ".."</th>
		// </tr>
		// </table>

		// EOD;

		// Print text using writeHTMLCell()
		$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);



		$pdf->SetPrintFooter(false);

		$pdf->lastPage();

		// $pdf->writeHTMLCell(85, 5, '<p>'.$kategoriby.' : '.$kategori.'<br>Periode : '.$start_date.' / '.$end_date.'</p> ', 1, 'L', 0, 0, '', '', true);
		// $pdf->writeHTMLCell(85, 5, $img, 1, 'R', 0, 1, '', '', true);
		// $pdf->writeHTMLCell(180, 5, '[CENTER] ', 1, 'C', 0, 1, '', '', true);
		// $pdf->writeHTMLCell(85, 5, '[LEFT] ', 1, 'L', 0, 0, '', '', true);
		// $pdf->writeHTMLCell(85, 5, '[RIGHT] ', 1, 'L', 0, 1, '', '', true);

		// $pdf->Output('/var/www/html/tmp_doc/Report_Postbuy.pdf', 'F');

		// $this->output->set_content_type('application/json')->set_output(json_encode('aaa'));

		$pdf->Output('Purchase_order.pdf', 'I');


		// if ( $list ) {			
		// $this->output->set_content_type('application/json')->set_output(json_encode($list));
		// } else {
		// $result = array( 'Value not found!' );
		// $this->output->set_content_type('application/json')->set_output(json_encode($list));
		// }
	}

	public function slip_pdf()
	{
		$data = array(
			'id' => anti_sql_injection($this->input->post('idpo', TRUE)),
		);

		// print_r($data);die;

		$po_result = $this->purchase_order_model->get_po_by_payment($data);
		$cash_account = $this->purchase_order_model->get_cash_account();
		$po_detail_result = $this->purchase_order_model->get_po_detail($data);
		$detail_bayar = $this->purchase_order_model->get_bayar_inv($data);
		$result = $this->purchase_order_model->get_principal();
		//$result = $this->purchase_order_model->get_po();
		$resultby = $this->purchase_order_model->get_bayar($data);

		$pms = array(
			'id_prin' => $resultby[0]['id_distributor']
		);

		$item2 = $this->purchase_order_model->get_invoice_byprin($pms);
		$item3 = $this->purchase_order_model->get_bayar_detail($data);

		//print_r($po_result);die;

		$total_amnt = 0;
		foreach ($resultby as $resultbys) {

			$total_amnt += $resultbys['ammount'];
		}

		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);

		$item = $this->purchase_order_model->get_item_byprin($params);

		//print_r($data);die;
		$session = $this->session->userdata;
		$params['region'] = $session['logged_in']['idlokasi'];
		//$params['region'] = 4;

		$date = date_create($resultby[0]['date']);
		$date_ss = date_format($date, "d F Y");
		$bayar_tipe = ['Cash/Transfer', 'Giro', 'Bonus', 'Komisi'];

		$data = compact(
			'date_ss',
			'resultby',
			'total_amnt',
			'bayar_tipe',
			'po_result',
			'item3'
		);

		$html = $this->load->view('../modules/hutang/views/slip_pdf', $data, true);

		// echo $html; die; // for debug to browser

		$this->load->library('dompdfwrapper');

		$this->dompdfwrapper->load_html($html);
		// $customPaper = array(0,0,793.00,567.00);
		$customPaper = array(0, 0, 893.00, 529.00); // width ditambah 200px
		$this->dompdfwrapper->setPaper($customPaper, 'potrait');
		// Render the PDF
		$this->dompdfwrapper->render();

		// Output the generated PDF to Browser
		$this->dompdfwrapper->stream('kas_keluar.pdf', array("Attachment" => false));
	}
}
