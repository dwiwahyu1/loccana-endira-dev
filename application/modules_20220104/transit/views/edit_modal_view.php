<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto
  }

  .scroll-overflow {
    min-height: 650px
  }

  #modal-distributor::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="product-payment-inner-st">
          <ul id="myTabedu1" class="tab-review-design">
            <li class="active"><a href="#description">Add Mutasi</a></li>
          </ul>
          <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="review-content-section">

                    <form class="form-horizontal form-label-left" id="edit_material" role="form" action="<?php echo base_url('transit/edit_material'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
                      <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
                      <input data-parsley-maxlength="255" type="hidden" id="id_mat" name="id_mat" class="form-control" placeholder="Kode Stok" value="<?php if (isset($stok[0]['id_mat'])) {
                                                                                                                                                        echo $stok[0]['id_mat'];
                                                                                                                                                      } ?>" autocomplete="off" required="required">
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Kode Item <span class="required"><sup>*</sup></span></label>
                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="nama"><?php if (isset($stok[0]['stock_code'])) {
                                                                                              echo $stok[0]['stock_code'];
                                                                                            } ?></sup></span></label>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Item <span class="required"><sup>*</sup></span></label>

                        <label class="control-label col-md-2 col-sm-3 col-xs-12" for="nama"><?php if (isset($stok[0]['stock_name'])) {
                                                                                              echo $stok[0]['stock_name'].' '.$stok[0]['base_qty'].' '.$stok[0]['uom_symbol'];
                                                                                            } ?></sup></span></label>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Quantity <span class="required"><sup>*</sup></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input type="number" id="qty" name="qty" class="form-control" placeholder="Quantity" value="<?php if (isset($stok[0]['qty_big'])) {
                                                                                                                        echo number_format($stok[0]['qty_big'],2,'.',',');
                                                                                                                      } ?>" autocomplete="off" step="0.0001" readonly>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Tanggal Transit <span class="required"><sup>*</sup></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input type="text" id="date_mutasi" name="date_mutasi" class="form-control" placeholder="" <?php
                                                                                                                      echo date('d-M-Y'); ?>" value="<?php echo date('d-M-Y'); ?>" value="<?php echo date('d-M-Y'); ?>" autocomplete="off" required>
                          <!-- <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                          </div> -->
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gudang">Jenis Transit <span class="required"><sup>*</sup></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <select class="form-control" name="type_mutasi" id="type_mutasi" style="width: 100%" required autocomplete="off">
                            <option value="" selected='selected'>-- Pilih Jenis Transit --</option>
                            <option value="1">Transit Masuk</option>
                            <option value="0">Transit Keluar</option>
                          </select>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Jumlah Transit (Lt / Kg)<span class="required"><sup>*</sup></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input type="number" id="amount_mutasi" name="amount_mutasi" class="form-control" placeholder="Jumlah Mutasi" value="" autocomplete="off" required step="0.01">
                        </div>
                      </div>
					  
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Keterangan Transit <span class="required"></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <textarea id="note" name="note" class="form-control" placeholder="Keterangan" value="" autocomplete="off" required step="0.01"></textarea>
                        </div>
                      </div>
					  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Submit</button>
                          <button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()">Batal</button>
                        </div>
                      </div>
                    </form><!-- /page content -->
                  </div>
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="PrimaryModalalert" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-close-area modal-close-df">
        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
      </div>
      <div class="modal-body">
        <i class="educate-icon educate-checked modal-check-pro"></i>
        <h2>Data Berhasil Dirubah</h2>
        <p></p>
      </div>
      <div class="modal-footer">
        <a data-dismiss="modal" href="#" onClick="back()">OK</a>
      </div>
    </div>
  </div>
</div>


<script>
  function clearform() {

    $('#edit_material').trigger("reset");

  }

  function back() {

    window.location.href = "<?php echo base_url() . 'transit'; ?>";

  }


  $(document).ready(function() {
    $('#date_mutasi').datepicker({
      isRTL: true,
      format: "dd-M-yyyy",
      autoclose: true,
      todayHighlight: true,
      changeYear: true,
      minDate: '-3M',
      maxDate: '+30D',
    });
    $('#edit_material').on('submit', function(e) {
      // validation code here
      //if(!valid) {
      e.preventDefault();

      var formData = new FormData(this);
var urls = $(this).attr('action');

swal({
      title: 'Yakin akan Simpan Data ?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak'
    }).then(function() {

      $.ajax({
        type: 'POST',
        url: urls,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
          if (response.success == true) {
            // $('#PrimaryModalalert').modal('show');
            swal({
              title: 'Success!',
              text: response.message,
              type: 'success',
              showCancelButton: false,
              confirmButtonText: 'Ok'
            }).then(function() {
              back();
            })
          } else {
            $('#msg_err').html(response.message);
            $('#WarningModalftblack').modal('show');
          }
        }
      });

 });	
      //alert(kode);


      //}
    });
  });
</script>