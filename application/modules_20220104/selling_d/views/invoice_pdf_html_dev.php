<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style>
        body {
            margin-right: 100px;
			font-family:"Times New Roman", Times, serif;
        }

        .paper {
            width: 100%;
            page-break-after: always;
            /* border: 1px solid blue; */
        }

        .paper:last-child {
            page-break-after: avoid;
        }

       

        .subtitle {
            font-size: 24px;
        }
		
		 .subtitle2 {
            font-size: 21px;
        }
		
		
    </style>
</head>

<body>
    <div class="paper" style="width: 100%;margin-top:-10px">
        <div class="row" style="height: 200px;">
            <table border="0.0px" style="width: 55%; float: left; border-collapse:collapse;">
                <tr>
                    <td colspan="3"><strong><span class="subtitle">PT. ENDIRA ALDA</span></strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr class="subtitle2">
                    <td>JL. Sangkuriang No.38-A</td>
                </tr >
                <tr class="subtitle2">
                    <td>01.555.161.7.428.000</td>
                </tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr class="subtitle2">
					<td><strong>Pembeli</strong></td>
				</tr>
				<tr class="subtitle2">
					<td><?= $result_penjualan[0]['cust_name'] ?? ''; ?></td>
				</tr>
				<tr class="subtitle2">
					<td><?= $result_penjualan[0]['cust_address'] ?? ''; ?></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				
            </table  >

            <table border="0.0px" style="width: 45%; float: left; border-collapse:collapse;">
                <tr>
                    <td colspan="3"><strong><span class="subtitle">FAKTUR</span></strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr class="subtitle2">
                    <td><strong>No.Faktur</strong></td>
                    <td>:</td>
                    <td><strong><?= $result_penjualan[0]['no_penjualan'] ?? ''; ?></strong></td>
                </tr>
                <tr class="subtitle2">
                    <td><strong>Tanggal</strong></td>
                    <td>:</td>
                    <td><strong><?= $result_penjualan[0]['date_penjualan'] ?? ''; ?></strong></td>
                </tr>
                <tr class="subtitle2">
                    <td>Alamat pengiriman</td>
                    <td>:</td>
                    <td><?= $result_penjualan[0]['cust_address'] ?? ''; ?></td>
                </tr>
                <tr class="subtitle2" >
                    <td>Phone</td>
                    <td>:</td>
                    <td><?= $result_penjualan[0]['phone'] ?? ''; ?></td>
                </tr>
            </table>
        </div><br>
      
        <div class="row" style="margin-top:20px">
            <table style="width: 98%; border-collapse:collapse; font-size: 20px; ">
                <thead>
                    <tr>
                        <td style="border: 1px solid black; width: 5px;text-align: center;">No.</td>
                        <td style="border: 1px solid black; width: 15px;text-align: center;">Kode</td>
                        <td style="border: 1px solid black; padding-left: 4px; width: 365px;text-align: center;">Nama Barang</td>
                        <td style="border: 1px solid black; padding-left: 4px; width: 65px;text-align: center;">Qty Box</td>
                        <td style="border: 1px solid black; padding-left: 4px; width: 65px;text-align: center;">Qty Pcs</td>
                        <td style="border: 1px solid black; padding-left: 4px; width: 75px;text-align: center;">Isi per Box</td>
                        <td style="border: 1px solid black; padding-left: 4px; width: 75px;text-align: center;">Total Pcs</td>
                        <td style="border: 1px solid black; text-align: left; padding-left: 4px;text-align: center;">Harga</td>
                        <td colspan="2" style="border: 1px solid black; text-align: left; padding-left: 4px;text-align: center;">Jumlah Rp</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $rowNumber = 1;
                    $total_retur = 0;
                    $dpp = 0;
                    foreach ($items as $item) : ?>

                        <?php
                        $qtyBox = (float)($item['qty_box_retur'] ?? 0);
                        $qtyPerBox = (float)($item['box_ammount'] ?? 0);
                        $qtyPcs = (float)($item['qty_retur_sat'] ?? 0);
                        $totalPcs = ($qtyBox * $qtyPerBox) + $qtyPcs;
                        ?>

                        <tr>
                            <td style="padding: 5px 4px; border: none; padding-left: 4px; width: 5px;"><?= $rowNumber; ?></td>
                            <td style="padding: 5px 4px; border: none; padding-left: 4px; width: 15px;"><?= $item['stock_code'] ?? ''; ?></td>
                            <td style="padding: 5px 4px; border: none; padding-left: 4px; width: 365px;"><?= implode(' ', [$item['stock_name'], $item['base_qty'], $item['uom_symbol']]) ?></td>
                            <td style="padding: 5px 4px; border: none; text-align: center;padding-left: 4px; width: 65px;"><?= number_format($item['qty_box_retur'], 0, ',', '.') ?? ''; ?></td>
                            <td style="padding: 5px 4px; border: none; text-align: center;padding-left: 4px; width: 65px;"><?= number_format($item['qty_retur_sat'], 0, ',', '.') ?? ''; ?></td>
                            <td style="padding: 5px 4px; border: none; text-align: left;padding-left: 4px; width: 75px;"><?= '@ ' . (number_format($item['box_ammount'], 0, ',', '.') ?? ''); ?></td>
                            <td style="padding: 5px 4px; border: none; text-align: right;padding-left: 4px; width: 75px;"><?= indonesia_currency_format($totalPcs); ?></td>
                            <td style="padding: 5px 4px; border: none; text-align: right; padding-right: 4px;"><?= indonesia_currency_format($item['unit_price']); ?></td>
                            <td colspan="2" style="padding: 5px 4px; border: none; text-align: right; padding-right: 4px;"><?= indonesia_currency_format($item['price']); ?></td>
                        </tr>
                    <?php
                        $rowNumber++;
                        $total_retur += $item['price'];
                        $dpp += (($item['price'] / (10 + ($item['pajak'] / 10))) * 10);
                    endforeach; ?>
                    <tr >
                        <td style="border-top:1px solid black;"></td>
                        <td style="border:none;border-top:1px solid black; text-align:center; padding: 5px 4px;">Mengetahui,</td>
						<td style="border:none;border-top:1px solid black; text-align:center; padding: 5px 4px;"><?= $result_penjualan[0]['cust_name'] ?? ''; ?>,</td>
                        <td colspan="4" style="border:none;border-top:1px solid black; padding: 5px 4px;"></td>
                        <td style="border:none;border-top:1px solid black; padding: 5px 4px;font-size:22px;"><b>Total</b></td>
                        <td style="border: none;border-top:1px solid black; padding-left: 4px; width: 6px; padding: 5px 4px;font-size:22px;"><b>Rp</b></td>
                        <td style="border: none;border-top:1px solid black; padding-right: 4px; text-align: right; padding: 5px 4px; font-size:22px; font-weight: bold;" ><b><?= indonesia_currency_format($total_retur); ?></b></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="6" style="border:none; padding: 5px 4px;"></td>
                        <td style="border:none; padding: 5px 4px;">DPP</td>
                        <td style="border: none; padding-left: 4px; width: 6px; padding: 5px 4px;">Rp</td>
                        <td style="border: none; padding-right: 4px; text-align: right; padding: 5px 4px;"><?= indonesia_currency_format($dpp); ?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td colspan="5" style="border:none; padding: 5px 4px;"></td>
                        <td style="border:none; padding: 5px 4px;">PPN</td>
                        <td style="border: none; padding-left: 4px; width: 6px; padding: 5px 4px;">Rp</td>
                        <td style="border: none; padding-right: 4px; text-align: right; padding: 5px 4px;"><?= indonesia_currency_format($total_retur - $dpp); ?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="border:none; text-align:center; padding: 5px 4px;">________</td>
						 <td style="border:none; text-align:center; padding: 5px 4px;">________</td>
                        <td colspan="4" style="border:none; padding: 5px 4px;"></td>
                        <td style="border:none; padding: 5px 4px;"></td>
                        <td style="border: none; padding-left: 4px; width: 10px; padding: 5px 4px;"></td>
                        <td style="border: none; padding-right: 4px; text-align: right; padding: 5px 4px;"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div><!-- paper -->



    <div class="paper">
        <div class="row" style="height: 200px;">
            <table border="0.0px" style="width: 55%; float: left; border-collapse:collapse;">
                <tr>
                    <td colspan="3"><strong><span class="subtitle">PT. ENDIRA ALDA</span></strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr class="subtitle2">
                    <td>JL. Sangkuriang No.38-A</td>
                </tr >
                <tr class="subtitle2">
                    <td>01.555.161.7.428.000</td>
                </tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr class="subtitle2">
					<td><strong>Pembeli</strong></td>
				</tr>
				<tr class="subtitle2">
					<td><?= $result_penjualan[0]['cust_name'] ?? ''; ?></td>
				</tr>
				<tr class="subtitle2">
					<td><?= $result_penjualan[0]['cust_address'] ?? ''; ?></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				
            </table  >

            <table border="0.0px" style="width: 45%; float: left; border-collapse:collapse;">
                <tr>
                    <td colspan="3"><strong><span class="subtitle">DO</span></strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr class="subtitle2">
                    <td><strong>No.Faktur</strong></td>
                    <td>:</td>
                    <td><strong><?= $result_penjualan[0]['no_penjualan'] ?? ''; ?></strong></td>
                </tr>
                <tr class="subtitle2">
                    <td><strong>Tanggal</strong></td>
                    <td>:</td>
                    <td><strong><?= $result_penjualan[0]['date_penjualan'] ?? ''; ?></strong></td>
                </tr>
                <tr class="subtitle2">
                    <td>Alamat pengiriman</td>
                    <td>:</td>
                    <td><?= $result_penjualan[0]['cust_address'] ?? ''; ?></td>
                </tr>
                <tr class="subtitle2" >
                    <td>Phone</td>
                    <td>:</td>
                    <td><?= $result_penjualan[0]['phone'] ?? ''; ?></td>
                </tr>
            </table>
        </div>

        <div class="row" style="margin-top:35px; ">
            <table style="width: 100%; border-collapse:collapse; font-size: 20px;">
                <thead>
                    <tr>
                        <td style="border: 1px solid black; padding-left: 4px; width: 5px;">No.</td>
                        <td style="border: 1px solid black; padding-left: 4px; text-align: center;text-align: center;">Kode</td>
                        <td style="border: 1px solid black; padding-left: 4px; text-align: center;">Nama Barang</td>
                        <td style="border: 1px solid black; padding-left: 4px; width: 65px;text-align: center;">Qty Box</td>
                        <td style="border: 1px solid black; padding-left: 4px; width: 65px;text-align: center;">Qty Pcs</td>
                        <td style="border: 1px solid black; padding-left: 4px; width: 75px;text-align: center;">Isi per Box</td>
                        <td style="border: 1px solid black; text-align: left; padding-left: 4px;text-align: center;">Total Pcs</td>
                        <td style="border: 1px solid black; text-align: left; padding-left: 4px;text-align: center;">Liter/Kg</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $rowNumber = 1;
                    $qty = 0;
                    $qtyS = 0;
                    $price = 0;
                    $qty_retur = 0;
                    $ltkg = 0;
                    foreach ($items as $item) : ?>
                        <tr>
                            <td style="padding: 5px 4px; border: none; padding-left: 4px; width: 5px;"><?= $rowNumber; ?></td>
                            <td style="padding: 5px 4px; border: none; padding-left: 4px; width: 15px;"><?= $item['stock_code'] ?? ''; ?></td>
                            <td style="padding: 5px 4px; border: none; padding-left: 4px; width: 365px;"><?= implode(' ', [$item['stock_name'], $item['base_qty'], $item['uom_symbol']]) ?></td>
                            <td style="padding: 5px 4px; border: none; padding-left: 4px; text-align: center;width: 65px;"><?= number_format($item['qty_box_retur'], 0, ',', '.') ?? ''; ?></td>
                            <td style="padding: 5px 4px; border: none; padding-left: 4px; text-align: center;width: 65px;"><?= number_format($item['qty_retur_sat'], 0, ',', '.') ?? ''; ?></td>
                            <td style="padding: 5px 4px; border: none; padding-left: 4px; text-align: left;width: 75px;"><?= '@ ' . (number_format($item['box_ammount'], 0, ',', '.') ?? ''); ?></td>
                            <td style="padding: 5px 4px; border: none; text-align: right; padding-right: 4px;"><?= indonesia_currency_format($item['qty_retur']); ?></td>
                            <td style="padding: 5px 4px; border: none; text-align: right; padding-right: 4px;"><?= indonesia_currency_format($item['bpl'] * $item['qty_box_retur']); ?></td>
                        </tr>
                    <?php
                        $rowNumber++;
                        $qty += $item['qty_box_retur'];
                        $qtyS += $item['qty_retur_sat'];
                        $qty_retur += $item['qty_retur'];
                        $price += $item['unit_price'];
                        $ltkg += $item['bpl'] * $item['qty_box_retur'];
                    endforeach; ?>
                    <tr>
                        <td style="padding: 5px 4px; border-top:1px solid black;"></td>
                        <td style="padding: 5px 4px; border-top:1px solid black;"></td>
                        <td style="padding: 5px 4px; border-top:1px solid black;">Jumlah</td>
                        <td style="padding: 5px 4px; border-top:1px solid black; text-align:center;"><?= number_format($qty, 0, ',', '.'); ?></td>
                        <td style="padding: 5px 4px; border-top:1px solid black; text-align:center;"><?= number_format($qtyS, 0, ',', '.'); ?></td>
                        <td style="padding: 5px 4px; border-top:1px solid black;"></td>
                        <td style="padding: 5px 4px; border-top:1px solid black; text-align:right; padding-right: 4px;"><?= indonesia_currency_format($qty_retur); ?></td>
                        <td style="padding: 5px 4px; border-top:1px solid black; text-align:right; padding-right: 4px;"><?= indonesia_currency_format($ltkg); ?></td>
                    </tr>
                </tbody>
            </table>
            <table style="width: 100%; border-collapse:collapse;">
                <tbody>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;font-size: 20px;">Penerima</td>
                        <td style="text-align:center;font-size: 20px;">Menyetujui</td>
                        <td style="text-align:center;font-size: 20px;">Pengirim</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="text-align:center;">__________</td>
                        <td style="text-align:center;">__________</td>
                        <td style="text-align:center;">__________</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="paper">
          <!--  <h5><span class="subtitle">Permintaan Barang <?= date('Y-M-d'); ?></span></h5> -->
        <h5><span class="subtitle">Permintaan Barang <?= $result_penjualan[0]['date_penjualan'] ?? ''; ?></span></h5>
        <table style="width: 100%; border-collapse:collapse; font-size: 20px;">
            <thead>
                <tr>
                    <td style="border: 1px solid black; padding-left: 4px;">No.</td>
                    <td style="border: 1px solid black; padding-left: 4px;">Customer</td>
                    <td style="border: 1px solid black; padding-left: 4px;">Kota</td>
                    <td style="border: 1px solid black; padding-left: 4px;">Produk</td>
                    <td style="border: 1px solid black; padding-right: 4px; text-align:right;">Pack</td>
                    <td style="border: 1px solid black; padding-right: 4px; text-align:right;">Box</td>
                    <td style="border: 1px solid black; padding-right: 4px; text-align:right;">Pcs</td>
                    <td style="border: 1px solid black; padding-right: 4px; text-align:right;">Total Pcs</td>
                    <td style="border: 1px solid black; padding-right: 4px; text-align:right;">Liter/Kg</td>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                $qty = 0;
                $qtyS = 0;
                $price = 0;
                $qty_retur = 0;
                $ltkg = 0;
                foreach ($items as $item) : ?>
                    <tr>
                        <td style="padding: 5px 4px; border: none; padding-left: 4px;"><?= $i; ?></td>
                        <td style="padding: 5px 4px; border: none; padding-left: 4px;"><?= $item['cust_name'] ?? ''; ?></td>
                        <td style="padding: 5px 4px; border: none; padding-left: 4px;"><?= $item['city'] ?? ''; ?></td>
                        <td style="padding: 5px 4px; border: none; padding-left: 4px;"><?= $item['stock_name'] ?? ''; ?></td>
                        <td style="padding: 5px 4px; border: none; padding-right: 4px; text-align: right;"><?= $item['base_qty'] . ' ' . $item['uom_symbol'] ?? ''; ?></td>
                        <td style="padding: 5px 4px; border: none; padding-right: 4px; text-align: right;"><?= indonesia_currency_format($item['qty_box_retur']); ?></td>
                        <td style="padding: 5px 4px; border: none; padding-right: 4px; text-align: right;"><?= indonesia_currency_format($item['qty_retur_sat']); ?></td>
                        <td style="padding: 5px 4px; border: none; padding-right: 4px; text-align: right;"><?= indonesia_currency_format($item['qty_retur']); ?></td>
                        <td style="padding: 5px 4px; border: none; padding-right: 4px; text-align: right;"><?= indonesia_currency_format($item['bpl'] * $item['qty_box_retur']); ?></td>
                    </tr>
                <?php
                    $i++;
                    $qty += $item['qty_box_retur'];
                    $qtyS += $item['qty_retur_sat'];
                    $qty_retur += $item['qty_retur'];
                    $price += $item['unit_price'];
                    $ltkg += $item['bpl'] * $item['qty_box_retur'];
                endforeach; ?>
                <tr>
                    <td colspan="5" style="border-top: 1px solid black;"></td>
                    <td style="padding: 5px 4px; border-top: 1px solid black; padding-right: 4px; text-align: right;"><?= indonesia_currency_format($qty); ?></td>
                    <td style="padding: 5px 4px; border-top: 1px solid black; padding-right: 4px; text-align: right;"><?= indonesia_currency_format($qtyS); ?></td>
                    <td style="padding: 5px 4px; border-top: 1px solid black; padding-right: 4px; text-align: right;"><?= indonesia_currency_format($qty_retur); ?></td>
                    <td style="padding: 5px 4px; border-top: 1px solid black; padding-right: 4px; text-align: right;"><?= indonesia_currency_format($ltkg); ?></td>
                </tr>
            </tbody>
        </table>
        <table style="width: 100%; border-collapse:collapse;">
            <tbody>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:center;font-size: 20px;">Diminta oleh</td>
                    <td style="text-align:center;font-size: 20px;">Pemeriksa</td>
                    <td style="text-align:center;font-size: 20px;">Pengirim</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:center;">__________</td>
                    <td style="text-align:center;">__________</td>
                    <td style="text-align:center;">__________</td>
                </tr>
            </tbody>
        </table>
    </div><!-- paper -->
</body>

</html>