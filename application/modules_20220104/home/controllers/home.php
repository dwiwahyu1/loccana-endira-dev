<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('home_model');
		$this->load->model('penerimaan_barang/purchase_order_model');
	}

	public function index() {
		$result['total_penjualan'] 	= $this->home_model->total_penjualan();
		$result['total_pembelian'] 	=$this->purchase_order_model->mtd_pembelian("ALL", date('Y'));
		$result['mtd_penjualan'] 	= $this->home_model->mtd_penjualan();
		$result['mtd_pembelian'] 	= $this->purchase_order_model->mtd_pembelian(date('F'), date('Y'));
		$result['informasi'] 		= $this->home_model->informasi();
		$this->template->load('maintemplate', 'home/views/home', $result);
	}
}
