<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Asset extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('asset/stock_model');
		$this->load->model('piutang/return_model');
		$this->load->library('log_activity');
		$this->load->library('formatnumbering');
		$this->load->library('priv');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$params = array(
			'end_time'=>date('Y-m-d'),
			'id_coa'=>0
		);
		// echo"<pre>";print_r($params);die;
		
		$array_month = array('48', '51', '54','56','49','52','55');
		$array_coa = array('Nilai Perolehan Inventaris', 'Nilai Perolehan Kendaraan', 'Nilai Perolehan Bangunan','Tanah'
		,'Depresiasi Penyusutan Inventaris','Depresiasi Penyusutan Kendaraan','Depresiasi Penyusutan Bangunan');

		$filter_month = '<select class="form-control" id="filter_coa" onChange="filter()"><option value="0" >ALL</option>';
		$i=0;
		foreach ($array_month as $array_months) {
			$filter_month .= '<option value="' . $array_months . '" >' . $array_coa[$i] . '</option>';
			$i++;
		}
		$filter_month.='</select>';
		$total_amount= number_format((float)$this->stock_model->mtd_penjualan($params)[0]['mtd_penjualan'], 2, ',', '.');
		$total_month = '
			<div class="analytics-content pull-right">
				<h5>Total Per <label id="end_time_label" >'
				.$params['end_time']
				.'</label> </h5>
				<h3>Rp 
					<span class="counter">
						<label id="mtd_penjualan" >'.
							$total_amount.
						'</label>
					</span> 
					<span class="tuition-fees">
					</span>
				</h2>
			</div>';
			
		$data = array(
			'total_month'=>$total_month,
			'filter_coa'=>$filter_month
		);
		$this->template->load('maintemplate', 'asset/views/index', $data);
	}

	function lists() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

		$order_fields = array('', 'stock_code', 'stock_name');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['searchtxt'] = $search_value;

		$list = $this->stock_model->lists($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		//$i = 0;
		$i = $params['offset'];
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$button_mutasi = '<div class="btn-group"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Mutasi" onClick="editmaterial(\'' . $v['id_mat'] . '\')"><i class="fa fa-history"></i></button></div>';
			// $button_approve = '<div class="btn-group"><button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Approve" onClick="approvematerial(\'' . $v['id_mat'] . '\')"><i class="fa fa-check"></i></button></div>';
			$button_detail = '<div class="btn-group"><button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Detail" onClick="detailmaterial(\'' . $v['id_mat'] . '\')"><i class="fa fa-search"></i></button></div>';
			// if($v['status_harga']==0){
			//   $status = 'Konfirmasi';
			$button = $button_detail . $button_mutasi;
			// }
			// else {
			//   $status = 'Setuju';
			//   $button = $button_edit;
			// }
			array_push($data, array(
				$v['stock_code'],
				$v['stock_name'],
				$v['base_qty'] . ' ' . $v['uom_symbol'],
				$v['name_eksternal'],
				number_format($v['box_per_lt'], 2, ',', '.'),
				//number_format($v['qty'],0,',','.').' Pcs',
				number_format($v['saldo_awal_big'], 2, ',', '.') . '',
				number_format($v['saldo_awal'] / $v['unit_box'], 2, ',', '.') . '',
				number_format($v['pemasukan'] / $v['unit_box'], 2, ',', '.') . '',
				number_format($v['pengeluaran_01'] / $v['unit_box'], 2, ',', '.') . '',
				// number_format($v['pengeluaran_02']/$v['unit_box'],2,',','.').'',
				// number_format($v['pengeluaran_03']/$v['unit_box'],2,',','.').'',
				// number_format($v['pengeluaran_04']/$v['unit_box'],2,',','.').'',
				number_format($v['saldo_akhir_big'], 2, ',', '.') . '',
				number_format($v['saldo_akhir'] / $v['unit_box'], 2, ',', '.') . '',
				// $this->formatnumbering->qty($v['qty_big']),
				// $status,
				$button
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	function lists_mutasi() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$id_mat = (int)$this->Anti_sql_injection($this->uri->segment(3));
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

		$order_fields = array('', 'stock_code', 'stock_name');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['searchtxt'] = $search_value;
		$params['id_mat'] = $id_mat;
		// var_dump($params);die;
		$list = $this->stock_model->list_mutasi($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		//$i = 0;
		$i = $params['offset'];
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$button_mutasi = '<div class="btn-group"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Mutasi" onClick="editmaterial(\'' . $v['id_mat'] . '\')"><i class="fa fa-history"></i></button></div>';
			$button_approve = '<div class="btn-group"><button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Approve" onClick="approvematerial(\'' . $v['id_mat'] . '\')"><i class="fa fa-check"></i></button></div>';
			$button_detail = '<div class="btn-group"><button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Detail" onClick="approvematerial(\'' . $v['id_mat'] . '\')"><i class="fa fa-search"></i></button></div>';
			// if($v['status_harga']==0){
			//   $status = 'Konfirmasi';
			$button = $button_detail . $button_mutasi . $button_approve;
			// }
			// else {
			//   $status = 'Setuju';
			//   $button = $button_edit;
			// }

			if ($v['type_mutasi'] == 0) {
				// $tp_mutasi = "Pemasukan";
				$tp_mutasi = "Pembelian";
			} else {
				// $tp_mutasi = "Pengeluaran";
				$tp_mutasi = "Penjualan";
			}

			array_push($data, array(
				$i,
				$v['stock_code'],
				$v['stock_name'],
				$v['date_mutasi'],
				$tp_mutasi,
				$this->formatnumbering->qty($v['mutasi_big']),
				$v['name_eksternal'],
				$v['note'],
				// $status,
				// $button
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	function getDatesFromRange($start, $end, $format = 'Y-m-d') {
		$array = array();
		$interval = new DateInterval('P1D');

		$realEnd = new DateTime($end);
		$realEnd->add($interval);

		$period = new DatePeriod(new DateTime($start), $interval, $realEnd);

		foreach ($period as $date) {
			$array[] = $date->format($format);
		}

		return $array;
	}


	public function getNameFromNumber($num) {
		$numeric = $num % 26;
		$letter = chr(65 + $numeric);
		$num2 = intval($num / 26);
		if ($num2 > 0) {
			return $this->getNameFromNumber($num2 - 1) . $letter;
		} else {
			return $letter;
		}
	}


	public function filter() {

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		// $data = array(
		// 'start_time' => $this->Anti_sql_injection($this->input->post('start_time', TRUE)),
		// 'end_time' => $this->Anti_sql_injection($this->input->post('end_time', TRUE))
		// );

		$array_date = $this->getDatesFromRange($params['start_time'], $params['end_time']);



		$data_r = $this->stock_model->get_stock_data_range($params)['data'];

		// print_r($data_r);die;

		$html = "<tr>
                   <th>Asset</th>
                  <th width='100'>Keterangan</th>
                  <th >Tanggal Pembelian</th>
                  <th >Tahun</th>
                  <th >Bulan </th>
                  <th >Tanggal Akhir </th>
                  <th >Harga</th>
                  <th >Depresiasi Perbulan </th>
                  <th >Akumulasi Depresiasi " . (date('Y') - 1) . " </th>
                  <th >Depresiasi " . (date('Y')) . " </th>
                  <th >Total Depresiasi</th>
                  <th >Book Value</th>
                  <th >Action</th>
                </tr>";

		$html_body = '';

		$priv = $this->priv->get_priv();
		$rtr = 0;
		foreach ($data_r as $v) {

			$mth = $v['bln'] * 12;

			if ($mth == 0) {
				$mthv = 1;
			} else {
				$mthv = $mth;
			}


			$mth_day = $mth * 30;
			// echo"<pre>";print_r($v);die;
			if($v['bukti']==NULL)
				$v['bukti']="";
			if($v['tanggal_terjual']==NULL||$v['tanggal_terjual']=='')
				$v['tanggal_terjual'] = date('Y-m-d', strtotime($v['date'] . ' + ' . $mth_day . ' days'));
			if (!str_contains($v['bukti'],"Terjual"))
      	$button_edit = '<div class="btn-group" style="display:'.$priv['update'].'"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="editmaterial(\'' . $v['id'] . '\')"><i class="fa fa-edit"></i></button></div>';
			else
				$button_edit = '<div class="btn-group" style="display:'.$priv['update'].'"><button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Edit" "><i class="fas fa-dollar-sign"></i></button></div>';
			$html_body .= "<tr>
                  <td>" . $v['keterangan'] . "</td>
                  <td>" . $v['note']. "</td>
                  <td>" . $v['date'] . "</td>
                  <td>" . $v['bln'] . "</th>
                  <td>" . $mth . "</th>
									<td>" . $v['tanggal_terjual'] . "</th>
									<td>" . number_format($v['value_real'], 2, ',', '.') . "</td>
									<td>" . number_format($v['value_real'] / $mthv, 2, ',', '.') . "</td>
									<td>" . number_format($v['valuea'], 2, ',', '.') . "</td>
									<td>" . number_format($v['valuea2'], 2, ',', '.') . "</td>
									<td>" . number_format($v['valuea'] + $v['valuea2'], 2, ',', '.') . "</td>
									<td>";
									if($v['value_jual']==0)
									$html_body.=
									number_format(0, 2, ',', '.') . "</td>
									<td>" . $button_edit . "</td></tr>";
									else
										$html_body.=
										number_format($v['value_real'] - $v['value_jual'], 2, ',', '.') . "</td>
										<td>" . $button_edit . "</td></tr>";

			$rtr++;
		}

		// print_r($array_date);die;
		$result['table'] = $html;
		$result['body'] = $html_body;
		$result['data'] = $data_r;
		$result['mtd_penjualan'] = number_format((float)$this->stock_model->mtd_penjualan($params)[0]['mtd_penjualan'], 2, ',', '.');

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}


	public function edit() {
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('iditems', TRUE)),
		);
		$data_r = $this->stock_model->get_stock_data_range_edit($data['id']);
		$cash_account = $this->return_model->get_cash_account();
		// $unit = $this->stock_model->get_unit();?
		// $Gudang = $this->stock_model->get_distributor();

		$data = array(
			'stok'         => $data_r,
			'cash_account' => $cash_account,
			// 'unit' 				=> $unit,
			// 'Gudang' 			=> $Gudang
		);

		$this->template->load('maintemplate', 'asset/views/edit_modal_view', $data);
	}
	public function detail() {
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('iditems', TRUE)),
		);
		$result = $this->stock_model->get_material($data['id']);

		//print_r($result);die;

		// $unit = $this->stock_model->get_unit();?
		// $Gudang = $this->stock_model->get_distributor();

		$data = array(
			'stok'         => $result,
			// 'unit' 				=> $unit,
			// 'Gudang' 			=> $Gudang
		);

		$this->template->load('maintemplate', 'asset/views/mutasi_view', $data);
	}

	public function edit_asset() {
		$this->form_validation->set_rules('id', 'Unit', 'trim|required');
		$this->form_validation->set_rules('tgl_terjual', 'Tanggal terjual', 'trim|required');


		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			$id = $this->Anti_sql_injection($this->input->post('id', TRUE));
			$tgl_terjual = $this->Anti_sql_injection($this->input->post('tgl_terjual', TRUE));
			$casha = $this->Anti_sql_injection($this->input->post('casha', TRUE));
			$price = $this->Anti_sql_injection($this->input->post('price', TRUE));
			$data = array(
				'id'         		=> $id,
				'tgl_terjual' 	=> $tgl_terjual,
				'casha' 				=> $casha,
				'price' 				=> $price,
				// 'Gudang' 			=> $Gudang
			);
			$result = $this->stock_model->update_depresiasi($data);
			if (count($result) > 0) {
				$msg = 'Berhasil mengubah data Asset ke database';

				$this->log_activity->insert_activity('update', 'Berhasil Update Asset dengan id  coa value ' . $id);
				$results = array(
					'success' => true,
					'message' => $msg
				);
			} else {
				$msg = 'Gagal mengubah data Asset ke database';

				$this->log_activity->insert_activity('update', 'Gagal Update Asset dengan id coa value = ' . $id);
				$results = array(
					'success' => false,
					'message' => $msg
				);
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($results), true);
		}
	}


	public function excel() {
		
		$params = array(
			'start_time' => $this->Anti_sql_injection($this->input->post('start_time', TRUE)),
			'end_time' => $this->Anti_sql_injection($this->input->post('end_time', TRUE))
		);
		$data = $this->stock_model->get_stock_data_range($params);
		// echo '<pre>'; print_r($data); die;

		$this->load->library('excel');
		$objPHPExcel = new PHPExcel();


		$currencyFormat = '_(Rp* #,##0.00_);_(Rp* (#,##0.00);_(Rp* "-"??_);_(@_)';
		$line = 1;
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A' . $line, 'Asset')
			->setCellValue('B' . $line, 'Keterangan')
			->setCellValue('C' . $line, 'Tanggal Pembelian')
			->setCellValue('D' . $line, 'Tahun')
			->setCellValue('E' . $line, 'Bulan')
			->setCellValue('F' . $line, 'Tanggal Akhir')
			->setCellValue('G' . $line, 'Harga')
			->setCellValue('H' . $line, 'Depresiasi Perbulan')
			->setCellValue('I' . $line, 'Akumulasi Depresiasi 2020')
			->setCellValue('J' . $line, 'Depresiasi 2021')
			->setCellValue('K' . $line, 'Total')
			->setCellValue('L' . $line, 'Value');

		$line++;
		foreach ($data['data'] as $row) {
			// echo '<pre>'; print_r($row['keterangan']); die;

			$mth = $row['bln'] * 12;

			if ($mth == 0) {
				$mthv = 1;
			} else {
				$mthv = $mth;
			}

			$mth_day = $mth * 30;
			$end_data = date('Y-m-d', strtotime($row['date'] . ' + ' . $mth_day . ' days'));

			$objPHPExcel->getActiveSheet()->getStyle('G' . $line)->getNumberFormat()->setFormatCode($currencyFormat);
			$objPHPExcel->getActiveSheet()->getStyle('H' . $line)->getNumberFormat()->setFormatCode($currencyFormat);
			$objPHPExcel->getActiveSheet()->getStyle('I' . $line)->getNumberFormat()->setFormatCode($currencyFormat);
			$objPHPExcel->getActiveSheet()->getStyle('J' . $line)->getNumberFormat()->setFormatCode($currencyFormat);
			$objPHPExcel->getActiveSheet()->getStyle('K' . $line)->getNumberFormat()->setFormatCode($currencyFormat);
			$objPHPExcel->getActiveSheet()->getStyle('L' . $line)->getNumberFormat()->setFormatCode($currencyFormat);

			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A' . $line, $row['keterangan'] ?? '')
				->setCellValue('B' . $line, $row['note'] ?? '')
				->setCellValue('C' . $line, $row['date'] ?? '')
				->setCellValue('D' . $line, $row['bln'] ?? '')
				->setCellValue('E' . $line, $mth ?? '')
				->setCellValue('F' . $line, $end_data ?? '')
				->setCellValue('G' . $line, $row['value_real'] ?? 0)
				->setCellValue('H' . $line, $row['value_real'] / $mthv)
				->setCellValue('I' . $line, $row['valuea'] ?? 0)
				->setCellValue('J' . $line, $row['valuea2'] ?? 0)
				->setCellValue('K' . $line, $row['valuea'] + $row['valuea2'])
				->setCellValue('L' . $line, $row['value_real'] - ($row['valuea'] + $row['valuea2']));

			$line++;
		}

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename=asset.xls');
		header('Cache-Control: max-age=0');
		// If you’re serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you’re serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		unset($objPHPExcel);
	}
}
