<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto
  }

  .scroll-overflow {
    min-height: 650px
  }

  #modal-distributor::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="product-payment-inner-st">
          <ul id="myTabedu1" class="tab-review-design">
            <li class="active"><a href="#description">Add Penerimaan Barang</a></li>
          </ul>
          <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="review-content-section">

                    <form class="form-horizontal form-label-left" id="edit_material" role="form" action="<?php echo base_url('penerimaan_barang/add_bpb'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
                      <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">No DO <span class="required"><sup>*</sup></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input type="text" id="no_do" name="no_do" class="form-control" placeholder="No Do" value="" autocomplete="off">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Angkutan <span class="required"><sup>*</sup></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input type="text" id="angkutan" name="angkutan" class="form-control" placeholder="Angkutan" value="" autocomplete="off">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">No Polisi <span class="required"><sup>*</sup></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input type="text" id="no_polisi" name="no_polisi" class="form-control" placeholder="No Polisi" value="" autocomplete="off">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Tanggal Penerimaan Barang <span class="required"><sup>*</sup></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input type="text" id="tanggal_btb" name="tanggal_btb" class="form-control" placeholder="<?php echo date('Y-m-d'); ?>" autocomplete="off" required>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="gudang">Nomor PO <span class="required"><sup>*</sup></span></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <select class="form-control" name="no_po" id="no_po" style="width: 100%" required autocomplete="off" onchange="getPO();">
                            <option value="" selected='selected'>-- Pilih Nomor PO --</option>
                            <?php
                            foreach ($po as $po => $p) {
                            ?>
                              <option value="<?php echo $p->id_po; ?>"><?php echo $p->no_po . ' - ' . $p->name_eksternal; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <table id="listpo" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                          <tr>
                            <th class="custom-tables align-text">No PO</th>
                            <th class="custom-tables align-text">Kode Barang</th>
                            <th class="custom-tables align-text">Nama Barang</th>
                            <th class="custom-tables align-text">Satuan Barang</th>
                            <th class="custom-tables align-text">Qty Order</th>
                            <th class="custom-tables align-text">Qty Diterima</th>
                            <!-- <th class="custom-tables align-text">Status</th> -->
                            <th class="custom-tables align-text">Harga Unit</th>
                            <!-- <th class="custom-tables align-text">Ket</th> -->
                            <!-- <th class="custom-tables align-text">Actions</th> -->
                          </tr>
                        </thead>
                        <tbody></tbody>
                      </table>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Submit</button>
                          <button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()">Batal</button>
                        </div>
                      </div>
                    </form><!-- /page content -->
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="PrimaryModalalert" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-close-area modal-close-df">
        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
      </div>
      <div class="modal-body">
        <i class="educate-icon educate-checked modal-check-pro"></i>
        <h2>Data Berhasil Disimpan</h2>
        <p>Apakah Anda Ingin Menambah Data Item Lagi ?</p>
        <p></p>
      </div>
      <div class="modal-footer">
        <a data-dismiss="modal" href="#" onClick="back()">Tidak</a>
        <a data-dismiss="modal" href="#" onClick="clearform()">Ya</a>
      </div>
    </div>
  </div>
</div>


<script>
  function valQty(row) {
    var rowData = table_po.row(row).data();
    var qty = rowData[7];
    var qty_diterima = $('#qty_diterima' + row).val();
    var hargaUnit = qty_diterima * rowData[10];
  }

  function clearform() {
    $('#edit_material').trigger("reset");
  }

  function back() {
    window.location.href = "<?php echo base_url() . 'penerimaan_barang'; ?>";
  }
  var table_po;

  function resetDt() {
    table_po = $('#listpo').DataTable({
      "processing": true,
      "searching": false,
      "responsive": true,
      "lengthChange": false,
      "info": false,
      "bSort": false,
      "paging": false,
      "columnDefs": [{
        "targets": [4, 5, 6],
        "className": 'dt-body-right'
      }]
    });
  }

  function getPO() {
    var value_no = $("#no_po").val();

    if (value_no != '') {
      sendNo($("#no_po option:selected").text().replace(/,/g, '') + ',' + value_no);
    } else {
      table_po.clear().draw();
      resetDt();
    }
  }

  function sendNo(po) {
    var tempPO = [];


    var datapost = {
      "po": po,
      "tempPO": tempPO
    };

    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>penerimaan_barang/get_detail_po",
      data: JSON.stringify(datapost),
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      success: function(r) {
        if (r.success == true) {
          table_po.clear().draw();
          for (var i = 0; i < r.data.length; i++) table_po.row.add(r.data[i]).draw();
        } else {
          table_po.clear().draw();
          for (var i = 0; i < r.data.length; i++) table_po.row.add(r.data[i]).draw();
          swal("Perhatian!", r.message, "info");
        }
      }
    });
  }
  $(document).ready(function() {

    resetDt();
    $("#no_po").select2();
    $('#tanggal_btb').datepicker({
      isRTL: true,
      format: "dd-M-yyyy",
      autoclose: true,
      todayHighlight: true,
      changeYear: true,
      minDate: '-3M',
      maxDate: '+30D',
    });
    $('#edit_material').on('submit', (function(e) {
      if ($('#type_btb').val() !== '') {
        $('#btn-submit').attr('disabled', 'disabled');
        $('#btn-submit').text("Memasukkan data...");
        e.preventDefault();
        var formData = new FormData(this);
        $.ajax({
          type: 'POST',
          url: $(this).attr('action'),
          data: formData,
          cache: false,
          contentType: false,
          processData: false,
          success: function(response) {
            if (response.success == true) {
              var tanggal_btb = response.tanggal_btb;
              // save_PO(response.lastid, tanggal_btb);
              $('#PrimaryModalalert').modal('show');
              $('#btn-submit').removeAttr('disabled');
              $('#btn-submit').text("Simpan");
            } else {
              $('#btn-submit').removeAttr('disabled');
              $('#btn-submit').text("Simpan");
              swal("Failed!", response.message, "error");
            }
          }
        }).fail(function(xhr, status, message) {
          $('#btn-submit').removeAttr('disabled');
          $('#btn-submit').text("Simpan");
          swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
        });
      } else {
        swal("Failed!", "Maaf kolom Keterangan harus diisi", "error");
      }
      return false;
    }));

  });
</script>