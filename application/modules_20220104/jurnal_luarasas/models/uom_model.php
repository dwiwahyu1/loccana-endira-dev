<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Uom_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
    
    public function get_uom_data($id)
	{
		//$sql 	= 'CALL uom_search_id(?)';
		$sql 	= '
		select a.*,b.coa,b.keterangan,c.id_external FROM t_coa_value a 
		join t_coa b on a.id_coa = b.id_coa
		left join t_jurnal_trans c on a.id = c.coa_value
		where a.id = ?';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$id
			));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}      
	
	public function get_detail($data)
	{
		//$sql 	= 'CALL uom_search_id(?)';
		$sql 	= '
		SELECT a.*,b.`id_coa` AS coa_kr, b.`value_real` AS value_real_kr, b.`note` AS note_kr, b.`id_coa_temp` AS id_dist FROM `t_coa_value` a
		LEFT JOIN `t_coa_value` b ON a.`id` = b.`id_parent`
		WHERE a.`id` = ?';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$data['id']
			));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}    
	
			public function get_detail_2($data)
	{
		//$sql 	= 'CALL uom_search_id(?)';
		$sql 	= '
		SELECT b.id_coa id_head, b.date date_head, b.value_real value_real, b.note note_head, a.*, c.coa as head_coa, c.keterangan as head_ket ,
		d.coa as v_coa, d.keterangan as v_ket FROM t_coa_value a
		JOIN t_coa_value b ON a.id_parent = b.id
		JOIN t_coa c ON b.id_coa = c.id_coa
		JOIN t_coa d ON a.id_coa = d.id_coa
		WHERE b.id  = ?';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$data['id']
			));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}    
	
    public function get_principle()
	{
		//$sql 	= 'CALL uom_search_id(?)';
		$sql 	= 'select *,id_t_cust as id,cust_name as name_eksternal from t_customer order by code_cust';

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_cash_acc(){
		
		//$sql 	= 'select * from t_coa where type_coa in (0,1,3) order by coa';
		$sql 	= 'select * from t_coa where type_coa in (3) order by coa';

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
		
	}
	
	public function get_cash_acc_cr(){
		
		//$sql 	= 'select * from t_coa where type_coa = 1 order by coa';
		$sql 	= 'select * from t_coa order by coa';

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
		
	}

	public function get_coa(){
		
		$sql 	= 'select * from t_coa where type_coa in (0,1,3) order by coa';

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
		
	}

	public function get_id_trans(){
		
		$sql 	= 'select * from t_coa where type_coa in (1,0) order by coa';

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
		
	}
	
	public function lists($params = array())
	{
		
			$query_s = ' ';
			
		// }else{
			
			// $query_s = ' AND a.payment_status = '.$params['filter'];
		// }
		
		if($params['filter_month'] == 'ALL'){
			$query_m = ' ';
		}else{
			$query_m = ' AND DATE_FORMAT(a.date,"%M") = "'.$params['filter_month'].'"';
		}
		
		if($params['filter_year'] == 'ALL'){
			$query_y = ' ';
		}else{
			$query_y = 'AND DATE_FORMAT(a.date,"%Y") = "'.$params['filter_year'].'" ';
		}
		
		$where_sts = $query_s.' '.$query_m.' '.$query_y;
		
			// $query = 	'
				// SELECT COUNT(*) AS jumlah FROM t_coa_value a join t_coa b on a.id_coa = b.id_coa where a.id_parent = 0 '.$where_sts.' AND a.type_cash = 1 AND  (id_coa_temp is null or id_coa_temp = 0) AND bukti  IS NULL
				// '.$where_sts.'
				// AND ( 
					// coa LIKE "%'.$params['searchtxt'].'%" OR
					// keterangan LIKE "%'.$params['searchtxt'].'%" 
				// )
			// ';
			
			$query = 	'
				SELECT COUNT(*) AS jumlah FROM (
					SELECT a.*,b.coa,b.keterangan,b.id_coa AS sss,d.coa AS coa2,d.keterangan ket2,d.id_coa AS sss2
					FROM t_coa_value a 
					JOIN t_coa b ON a.id_coa = b.id_coa
					JOIN t_coa_value c ON  a.id_parent = c.id
					JOIN t_coa d ON c.id_coa = d.id_coa
					WHERE a.id_parent <> 0 '.$where_sts.'  AND c.type_cash = 1 AND  (c.id_coa_temp IS NULL OR c.id_coa_temp = 0) 
					AND a.bukti IS NULL  AND ( 
						b.coa LIKE "%'.$params['searchtxt'].'%" OR
						d.coa LIKE "%'.$params['searchtxt'].'%" OR
						b.keterangan LIKE "%'.$params['searchtxt'].'%" OR
						d.keterangan LIKE "%'.$params['searchtxt'].'%" 
					) AND d.id_parent = 3 group by a.id_parent
				) s
			';
						
			// $query2 = 	'
				// SELECT z.*, rank() over ( ORDER BY sss ) AS Rangking from ( 
					// select a.*,b.coa,b.keterangan,b.id_coa as sss 
					// FROM t_coa_value a join t_coa b on a.id_coa = b.id_coa
					// where a.id_parent = 0 '.$where_sts.' AND a.type_cash = 1 AND  (id_coa_temp is null or id_coa_temp = 0) AND bukti  IS NULL AND ( 
					// coa LIKE "%'.$params['searchtxt'].'%" OR
					// keterangan LIKE "%'.$params['searchtxt'].'%" 
				// )  order by sss) z
				// ORDER BY `sss` ASC
				
				// LIMIT '.$params['limit'].' 
				// OFFSET '.$params['offset'].' 
			// ';
			
			$query2 = 	'
				SELECT z.*, RANK() OVER ( ORDER BY sss ) AS Rangking FROM ( 
					SELECT a.*,sum(a.value) sum_amnt,b.coa,b.keterangan,b.id_coa AS sss,d.coa AS coa2,d.keterangan ket2,d.id_coa AS sss2,
					GROUP_CONCAT(distinct concat(b.coa," ",b.keterangan)) AS kr_note,GROUP_CONCAT(" ",a.note) AS k_note
					FROM t_coa_value a 
					JOIN t_coa b ON a.id_coa = b.id_coa
					JOIN t_coa_value c ON  a.id_parent = c.id
					JOIN t_coa d ON c.id_coa = d.id_coa
					WHERE a.id_parent <> 0 '.$where_sts.'  AND c.type_cash = 1 AND  (c.id_coa_temp IS NULL OR c.id_coa_temp = 0) 
					AND a.bukti IS NULL  AND ( 
						b.coa LIKE "%'.$params['searchtxt'].'%" OR
						d.coa LIKE "%'.$params['searchtxt'].'%" OR
						b.keterangan LIKE "%'.$params['searchtxt'].'%" OR
						d.keterangan LIKE "%'.$params['searchtxt'].'%" 
					) AND d.id_parent = 3 group by a.id_parent  ORDER BY sss) z
				ORDER BY `sss` ASC
				
				LIMIT '.$params['limit'].' 
				OFFSET '.$params['offset'].' 
			';
		
			//echo $query2;die;
		
			 $out		= array();
			  $querys		= $this->db->query($query);
			  $result = $querys->row();
			  
			  $total_filtered = $result->jumlah;
			  $total 			= $result->jumlah;
		  
				if(($params['offset']+10) > $total_filtered){
				$limit_data = $total_filtered - $params['offset'];
			  }else{
				$limit_data = $params['limit'] ;
			  }
		  
		
		  
		  //echo $query;die;
			//echo $query;die;
			 $query2s		= $this->db->query($query2);
		  $result2 = $query2s->result_array();						
		  $return = array(
			  'data' => $result2,
			  'total_filtered' => $total_filtered,
			  'total' => $total,
		  );
		  return $return;
	}

	public function list_for_excel($params = [])
	{
		$queryBuilder = $this->db
			->select('*')
			->from('t_coa_value')
			->join('t_coa', 't_coa_value.id_coa = t_coa.id_coa')
			->where('t_coa_value.id_parent', 0)
			->where('t_coa_value.type_cash', 1)
			->where('(t_coa_value.id_coa_temp IS NULL OR t_coa_value.id_coa_temp = 0)')
			->where('t_coa_value.bukti IS NULL')
			;

		if (isset($params['filter_year']) && !empty($params['filter_year']) && $params['filter_year'] != 'ALL') {
			$queryBuilder->where('( DATE_FORMAT(t_coa_value.date, "%Y") = "' . $params['filter_year'] . '")');
		}

		if (isset($params['filter_month']) && !empty($params['filter_month']) && $params['filter_month'] != 'ALL') {
			$queryBuilder->where('( DATE_FORMAT(t_coa_value.date, "%M") = "' . $params['filter_month'] . '")');
		}

		return $queryBuilder->get()->result_array();
	}

	public function edit_uom_trans($data)
	{
		$datas = array(
			'id_external' => $data['prin'],
			'tipe_trans' => $data['cash'],
			'ammount' => $data['jumlah'],
			'tgl_terbit' => $data['tgl']
		);

	//	print_r($datas);die;
		$this->db->where('id',$data['id']);
		$this->db->update('coa_value',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function edit_uom($data)
	{
		$datas = array(
			'id_coa' => $data['coa'],
			'date' => $data['tgl'],
			'date_insert' => date('Y-m-d H:i:s'),
			'value' => $data['jumlah'],
			'value_real' => $data['jumlah'],
			'type_cash' => 0,
			'id_valas' => 1,
			'note' => $data['ket']
		);

	//	print_r($datas);die;
		$this->db->where('id',$data['id']);
		$this->db->update('t_coa_value',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_uom($data)
	{
		$datas = array(
			'id_coa' => $data['coa'],
			'date' => $data['tgl'],
			'date_insert' => date('Y-m-d H:i:s'),
			'value' => $data['jumlah'],
			'value_real' => $data['jumlah'],
			'type_cash' => $data['type_cash'],
			'id_parent' => $data['parent'],
			'id_coa_temp' => $data['id_coa_temp'],
			'id_valas' => 1,
			'note' => $data['ket']
		);

	//	print_r($datas);die;

		$this->db->insert('t_coa_value',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}	
	
	public function add_trans($data,$id_coa_val)
	{
		$datas = array(
			'id_external' => $data['prin'],
			'tipe_trans' => 1,
			'ammount' => $data['jumlah'],
			'pph' => $data['pph'],
			'tgl_terbit' => $data['tgl'],
			'coa_value' => $id_coa_val
		);

	//	print_r($datas);die;

		$this->db->insert('t_jurnal_trans',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	
	public function deletes($id_uom)
	{
		$this->db->where('id', $id_uom);
		$this->db->delete('t_coa_value'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function delete_uom_trans($data)
	{
		$this->db->where('coa_value', $data['id']);
		$this->db->delete('t_jurnal_trans'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function delete_detail($id_uom)
	{
		$this->db->where('id_parent', $id_uom);
		$this->db->delete('t_coa_value'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}	



}
