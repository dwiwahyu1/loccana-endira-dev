<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Jurnal_penyesuaian extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('jurnal_penyesuaian/uom_model');
		$this->load->library('log_activity');
		$this->load->library('priv');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$year_count = date('Y') - 2019;

		$filter_year = '<select class="form-control" id="filter_year" onChange="changeFilter()"><option value="ALL" >ALL</option>';

		for ($i = $year_count; $i >= 0; $i--) {

			$y = 2019 + $i;

			if ($y == date('Y')) {
				$filter_year .= '<option value="' . $y . '" selected="selected" >' . $y . '</option>';
			} else {
				$filter_year .= '<option value="' . $y . '" >' . $y . '</option>';
			}
		}
		$filter_year .= '</select>';

		$array_month = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

		$filter_month = '<select class="form-control" id="filter_month" onChange="changeFilter()"><option value="ALL" >ALL</option>';
		foreach ($array_month as $array_months) {
			if ($array_months == date('F')) {
				$filter_month .= '<option value="' . $array_months . '" selected="selected">' . $array_months . '</option>';
			} else {
				$filter_month .= '<option value="' . $array_months . '" >' . $array_months . '</option>';
			}
		}

		$filter_month .= '</select>';
		$today_month = date('F');
		$today_year = date('Y');

		$priv = $this->priv->get_priv();

		$data = array(
			'priv' => $priv,
			'filter_month' => $filter_month,
			'filter_year' => $filter_year,
			'today_month' => $today_month,
			'today_year' => $today_year
		);

		$this->template->load('maintemplate', 'jurnal_penyesuaian/views/index', $data);
	}

	function lists() {
		$sess_user_id = !empty($_GET['sess_user_id']) ? $_GET['sess_user_id'] : NULL;
		$sess_token = !empty($_GET['sess_token']) ? $_GET['sess_token'] : NULL;
		$filter_month = !empty($_GET['filter_month']) ? $_GET['filter_month'] : NULL;
		$filter_year = !empty($_GET['filter_year']) ? $_GET['filter_year'] : NULL;
		$draw = $this->input->get_post('draw') != FALSE ? $this->input->get_post('draw') : 1;
		$length = $this->input->get_post('length') != FALSE ? $this->input->get_post('length') : 10;
		$start = $this->input->get_post('start') != FALSE ? $this->input->get_post('start') : 0;

		$order_fields = array('coa'); // , 'COST'

		$order = $this->input->get_post('order');
		$order_dir = !empty($order[0]['dir']) ? $order[0]['dir'] : 'desc';
		$order_column = !empty($order[0]['column']) ? $order[0]['column'] : 0;

		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['sess_user_id'] = $sess_user_id;
		$params['sess_token'] 	= $sess_token;
		$params['filter_month']	= $filter_month;
		$params['filter_year']	= $filter_year;
		$params['searchtxt']	= $_GET['search']['value'];

		$list = $this->uom_model->lists($params);
		$priv = $this->priv->get_priv();
		//print_r($list);die;

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		//print_r($result);die;

		$data = array();
		$i = $params['offset'];
		foreach ($list['data'] as $row) {
			$status_akses = '
				<div class="btn-group" style="display:' . $priv['update'] . '"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edituom(\'' . $row['id'] . '\')"><i class="fa fa-edit"></i></button></div>
				<div class="btn-group" style="display:' . $priv['delete'] . '"><button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteuom(\'' . $row['id'] . '\')"><i class="fa fa-trash"></i></button></div>
				<div class="btn-group" style="display:' . $priv['detail'] . '"><button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="detail(\'' . $row['id'] . '\')"><i class="fa fa-search-plus"></i></button></div>
			';

			array_push($data, [
				++$i,
				$row['coa'] . '-' . $row['keterangan'],
				$row['coa_k'],
				$row['date'],
				number_format($row['value_real'], 2, ',', '.'),
				$row['note'],
				$status_akses
			]);
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add() {
		$coa = $this->uom_model->get_cash_acc();
		$coa_p = $this->uom_model->get_cash_acc_cr();
		$principle = $this->uom_model->get_principle();

		// $this->load->view('add_modal_view', $data);
		// $result = $this->distributor_model->location();

		$data = array(
			'group' => '',
			'coa' => $coa,
			'coa_p' => $coa_p,
			'principle' => $principle
		);

		$this->template->load('maintemplate', 'jurnal_penyesuaian/views/add_modal_view', $data);
	}

	public function edit() {
		//$id = $this->Anti_sql_injection($this->input->post('sid', TRUE));
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('iduom', TRUE)),
		);

		$coa = $this->uom_model->get_cash_acc();
		$coa_p = $this->uom_model->get_cash_acc_cr();
		$principle = $this->uom_model->get_principle();

		$detail = $this->uom_model->get_detail($data);

		// print_r($detail);die;
		// $roles = $this->uom_model->roles($id);

		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('iduom', TRUE)),
			'group' => '',
			'coa' => $coa,
			'coa_p' => $coa_p,
			'detail' => $detail,
			'principle' => $principle
		);


		$this->template->load('maintemplate', 'jurnal_penyesuaian/views/edit_modal_view', $data);
	}

	public function detail() {
		//$id = $this->Anti_sql_injection($this->input->post('sid', TRUE));
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('iduom', TRUE)),
		);

		$coa = $this->uom_model->get_cash_acc();
		$coa_p = $this->uom_model->get_cash_acc_cr();
		$principle = $this->uom_model->get_principle();

		$detail = $this->uom_model->get_detail($data);


		// $roles = $this->uom_model->roles($id);

		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('iduom', TRUE)),
			'group' => '',
			'coa' => $coa,
			'coa_p' => $coa_p,
			'detail' => $detail,
			'principle' => $principle
		);


		$this->template->load('maintemplate', 'jurnal_penyesuaian/views/detailPo', $data);
	}

	public function deletes() {

		$data   = file_get_contents("php://input");
		$params   = json_decode($data, true);

		$list = $this->uom_model->delete_detail($this->Anti_sql_injection($params['id']));
		$list = $this->uom_model->deletes($this->Anti_sql_injection($params['id']));

		$res = array(
			'status' => 'success',
			'message' => 'Data telah di hapus'
		);

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function edit_uom() {
		$this->form_validation->set_rules('coa', 'coa', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('tgl', 'tgl', 'trim|required|max_length[255]');
		//$this->form_validation->set_rules('cash', 'cash', 'trim|required|max_length[255]');
		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));
			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			// die;
			$message = "";


			$int_val = $this->Anti_sql_injection($this->input->post('int_val', TRUE));
			$id = $this->Anti_sql_injection($this->input->post('id', TRUE));

			$array_items = [];

			$total_amount = 0;
			for ($i = 0; $i <= $int_val; $i++) {

				//if(isset($this->input->post('kode_'.$i, TRUE))){
				$array_items[$i]['coa_p'] = $this->Anti_sql_injection($this->input->post('coa_p_' . $i, TRUE));
				$array_items[$i]['jumlahp'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('jumlahp_' . $i, TRUE)))));
				$array_items[$i]['ket_p'] = $this->Anti_sql_injection($this->input->post('ket_p_' . $i, TRUE));
				$array_items[$i]['dist_p'] = $this->Anti_sql_injection($this->input->post('dist_p_' . $i, TRUE));
			}

			//print_r($array_items);die; 

			// if($this->Anti_sql_injection($this->input->post('coa', TRUE)) <> '122'  ){

			$this->uom_model->delete_detail($id);
			$this->uom_model->deletes($id);
			//untuk coa atas
			$array_itemss['coa_p']=$this->Anti_sql_injection($this->input->post('coa', TRUE));
			if (
				$array_itemss['coa_p'] == 99 || $array_itemss['coa_p'] == 99
			) {
				$tc = 1;
			} else {
				$tc = 0;
			}

			$data = array(
				'user_id' => $this->session->userdata['logged_in']['user_id'],
				'coa'     => $this->Anti_sql_injection($this->input->post('coa', TRUE)),
				'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
				//'prin' => $this->Anti_sql_injection($this->input->post('prin', TRUE)),
				'jumlah' => floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('jumlah', TRUE))))),
				'pph' => 0,
				'type_cash' => $tc,
				// 'type_cash' => 1,
				'parent' => 0,
				'id_coa_temp' => 0,
				'ket' => $this->Anti_sql_injection($this->input->post('ket', TRUE))
			);

			// print_r($data);die;
			$resultsss = $this->uom_model->add_uom($data);

			foreach ($array_items as $array_itemss) {

				if ($array_itemss['coa_p'] == "") {
				} else {

					if ($array_itemss['dist_p'] == '') {
						$sss = 0;
					} else {
						$sss = $array_itemss['dist_p'];
					}


					if (
						$array_itemss['coa_p'] == 28|| $array_itemss['coa_p'] == 58 || $array_itemss['coa_p'] == 87 || 
						$array_itemss['coa_p'] == 89 || $array_itemss['coa_p'] == 90 || $array_itemss['coa_p'] == 93 ||
						$array_itemss['coa_p'] == 95 || $array_itemss['coa_p'] == 96 || $array_itemss['coa_p'] == 99 || 
						$array_itemss['coa_p'] == 102 || $array_itemss['coa_p'] == 103 || $array_itemss['coa_p'] == 105 || 
						$array_itemss['coa_p'] == 106 || $array_itemss['coa_p'] == 111 || $array_itemss['coa_p'] == 158 ||
						$array_itemss['coa_p'] == 226 
					) {
						$tc = 1;
					} else {
						$tc = 0;
					}

					$datas = array(

						'user_id' => $this->session->userdata['logged_in']['user_id'],
						'coa'     => $array_itemss['coa_p'],
						'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
						//'prin' => $this->Anti_sql_injection($this->input->post('prin', TRUE)),
						'jumlah' => $array_itemss['jumlahp'],
						'pph' => 0,
						'type_cash' => $tc,
						'parent' =>  $resultsss['lastid'],
						'ket' => $array_itemss['ket_p'],
						'id_coa_temp' => $sss

					);

					$prin_result = $this->uom_model->add_uom($datas);
				}
			}

			// }else{
			// $data = array(
			// 'user_id' => $this->session->userdata['logged_in']['user_id'],
			// 'coa'     => $this->Anti_sql_injection($this->input->post('coa', TRUE)),
			// 'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
			// 'prin' => $this->Anti_sql_injection($this->input->post('prin', TRUE)),
			// 'jumlah' => $this->Anti_sql_injection($this->input->post('jumlah', TRUE)),
			// //'jumlah' => $this->Anti_sql_injection($this->input->post('jumlah', TRUE))-($this->Anti_sql_injection($this->input->post('jumlah', TRUE))*0.1),
			// 'pph' => 10,
			// 'ppn' => ($this->Anti_sql_injection($this->input->post('jumlah', TRUE))*0.1),
			// 'ket' => $this->Anti_sql_injection($this->input->post('ket', TRUE))
			// );
			// $result = $this->uom_model->add_uom($data);
			// $result2 = $this->uom_model->add_trans($data,$result['lastid']);
			// }
			//print_r($data);die;

			//$result = $this->uom_model->add_uom($data);



			if ($resultsss > 0) {
				$msg = 'Berhasil Merubah cash .';

				$result = array(
					'success' => true,
					'message' => $msg
				);
			} else {
				$msg = 'Gagal Merubah cash.';
				$result = array(
					'success' => false,
					'message' => $msg
				);
			}
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function add_uom() {
		$this->form_validation->set_rules('coa', 'coa', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('tgl', 'tgl', 'trim|required|max_length[255]');
		//$this->form_validation->set_rules('cash', 'cash', 'trim|required|max_length[255]');
		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));
			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			// die;
			$message = "";


			$int_val = $this->Anti_sql_injection($this->input->post('int_val', TRUE));

			$array_items = [];

			$total_amount = 0;
			for ($i = 0; $i <= $int_val; $i++) {

				//if(isset($this->input->post('kode_'.$i, TRUE))){
				$array_items[$i]['coa_p'] = $this->Anti_sql_injection($this->input->post('coa_p_' . $i, TRUE));
				$array_items[$i]['jumlahp'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('jumlahp_' . $i, TRUE)))));
				$array_items[$i]['ket_p'] = $this->Anti_sql_injection($this->input->post('ket_p_' . $i, TRUE));
				$array_items[$i]['dist_p'] = $this->Anti_sql_injection($this->input->post('dist_p_' . $i, TRUE));
			}

			// print_r($array_items);die;

			// if($this->Anti_sql_injection($this->input->post('coa', TRUE)) <> '122'  ){
				$array_itemss['coa_p']=$this->Anti_sql_injection($this->input->post('coa', TRUE));
				if (
					$array_itemss['coa_p'] == 99 || $array_itemss['coa_p'] == 99
				) {
					$tc = 1;
				} else {
					$tc = 0;
				}
	
				$data = array(
					'user_id' => $this->session->userdata['logged_in']['user_id'],
					'coa'     => $this->Anti_sql_injection($this->input->post('coa', TRUE)),
					'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
					//'prin' => $this->Anti_sql_injection($this->input->post('prin', TRUE)),
					'jumlah' => floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('jumlah', TRUE))))),
					'pph' => 0,
					'type_cash' => $tc,
					// 'type_cash' => 1,
					'parent' => 0,
					'id_coa_temp' => 0,
					'ket' => $this->Anti_sql_injection($this->input->post('ket', TRUE))
				);
	
				// print_r($data);die;

			$resultsss = $this->uom_model->add_uom($data);

			foreach ($array_items as $array_itemss) {

				if ($array_itemss['coa_p'] == "") {
				} else {

					if ($array_itemss['dist_p'] == '') {
						$sss = 0;
					} else {
						$sss = $array_itemss['dist_p'];
					}


					if (
						$array_itemss['coa_p'] == 28|| $array_itemss['coa_p'] == 58 || $array_itemss['coa_p'] == 87 || 
						$array_itemss['coa_p'] == 89 || $array_itemss['coa_p'] == 90 || $array_itemss['coa_p'] == 93 ||
						$array_itemss['coa_p'] == 95 || $array_itemss['coa_p'] == 96 || $array_itemss['coa_p'] == 99 || 
						$array_itemss['coa_p'] == 102 || $array_itemss['coa_p'] == 103 || $array_itemss['coa_p'] == 105 || 
						$array_itemss['coa_p'] == 106 || $array_itemss['coa_p'] == 111 || $array_itemss['coa_p'] == 158 ||
						$array_itemss['coa_p'] == 226 
						 ) {
						$tc = 1;
					} else {
						$tc = 0;
					}
					$datas = array(

						'user_id' => $this->session->userdata['logged_in']['user_id'],
						'coa'     => $array_itemss['coa_p'],
						'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
						//'prin' => $this->Anti_sql_injection($this->input->post('prin', TRUE)),
						'jumlah' => $array_itemss['jumlahp'],
						'pph' => 0,
						'type_cash' => $tc,
						'parent' =>  $resultsss['lastid'],
						'ket' => $array_itemss['ket_p'],
						'id_coa_temp' => $sss

					);

					$prin_result = $this->uom_model->add_uom($datas);
				}
			}

			// }else{
			// $data = array(
			// 'user_id' => $this->session->userdata['logged_in']['user_id'],
			// 'coa'     => $this->Anti_sql_injection($this->input->post('coa', TRUE)),
			// 'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
			// 'prin' => $this->Anti_sql_injection($this->input->post('prin', TRUE)),
			// 'jumlah' => $this->Anti_sql_injection($this->input->post('jumlah', TRUE)),
			// //'jumlah' => $this->Anti_sql_injection($this->input->post('jumlah', TRUE))-($this->Anti_sql_injection($this->input->post('jumlah', TRUE))*0.1),
			// 'pph' => 10,
			// 'ppn' => ($this->Anti_sql_injection($this->input->post('jumlah', TRUE))*0.1),
			// 'ket' => $this->Anti_sql_injection($this->input->post('ket', TRUE))
			// );
			// $result = $this->uom_model->add_uom($data);
			// $result2 = $this->uom_model->add_trans($data,$result['lastid']);
			// }
			//print_r($data);die;

			//$result = $this->uom_model->add_uom($data);



			if ($resultsss > 0) {
				$msg = 'Berhasil menambahkan cash .';

				$result = array(
					'success' => true,
					'message' => $msg
				);
			} else {
				$msg = 'Gagal menambahkan cash ke database.';
				$result = array(
					'success' => false,
					'message' => $msg
				);
			}
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}
}
