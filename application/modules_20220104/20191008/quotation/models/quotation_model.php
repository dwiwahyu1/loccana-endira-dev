<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Quotation_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	/**
      * This function is get the list data in po_quotation table
      * @param : $params is where condition for select query
      */

	public function lists($params = array())
	{
		$sql_all 	= 'CALL po_list_all(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				$params['filter'],
				0
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL po_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total_row,
			'total' => $total_row,
		);

		return $return;
	}

	/**
      * This function is get the list data in valas table
      */
	public function valas()
	{
		$this->db->select('valas_id,nama_valas');
		$this->db->from('m_valas');

		$query 	= $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is get the list data in eksternal table
      */
	public function customer()
	{
		$this->db->select('id,name_eksternal');
		$this->db->from('t_eksternal');
		$this->db->where('type_eksternal', 1);

		$query 	= $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is get the list data in material table
      */
	public function item()
	{
		$this->db->select('id,stock_name');
		$this->db->from('m_material');
		$this->db->where('status', 1);
		$this->db->where('type', 4);

		$query 	= $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is used to Insert Record in po_quotation and order table
      * @param : $data - record array 
      */

	public function add_quotation($data)
	{
		$sql 	= 'CALL po_add(?,?,?,?,?,?,?,?,?)';

		$this->db->query($sql,
			array(
				$data['cust_id'],
				$data['issue_date'],
				NULL,
				NULL,
				$data['term_of_pay'],
				NULL,
				NULL,
				$data['id_valas'],
				3
			));
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$id_po_quotation = $row['LAST_INSERT_ID()'];

		$id_produk = $data['id_produk'];
		$id_produklen = count($data['id_produk']);
		$price = $data['price'];
		$diskon = $data['diskon'];

		$bom_no = strtoupper(randomString());

		for($i=0;$i<$id_produklen;$i++){
			$sql 	= 'INSERT INTO m_material(			
												no_bc,
												stock_code,
												stock_name,
												stock_description,
												unit,
												type,
												qty,
												treshold,
												id_properties,
												id_gudang,
												status
								  )
								  SELECT 
								  		no_bc,
										stock_code,
										stock_name,
										stock_description,
										unit,
										type,
										qty,
										treshold,
										id_properties,
										id_gudang,
										3
								  FROM
								  		m_material
								  WHERE
								  		id=?';

			$query 	= $this->db->query($sql,array($id_produk[$i]));

			$id_productnew	= $this->db->insert_id();

			$sql 	= 'CALL order_add(?,?,?,?,?,?)';

			$this->db->query($sql,
				array(
					$id_po_quotation,
					$id_productnew,
					$id_produk[$i],
					NULL,
					$price[$i],
					$diskon[$i]
				));

			$sql 	= 'CALL bom_add(?,?,?,?,?,?)';

			$this->db->query($sql,
				array(
					$bom_no,
					$id_po_quotation,
					$id_produk[$i],
					0,
					0,
					0
				));
		}

		$this->db->close();
		$this->db->initialize();
	}

	/**
     * This function is used to delete po_quotation and t_order
     * @param: $id - id of po_quotation table
     */
	function delete_quotation($id) {
		$this->db->where('id', $id);  
		$this->db->delete('t_po_quotation');
		
		$this->db->where('id_po_quotation', $id);  
		$this->db->delete('t_order');

		$this->db->where('id_po_quot', $id);  
		$this->db->delete('t_bom');  

		$this->db->close();
		$this->db->initialize();
	}

	/**
      * This function is get data in po_quotation table by id
      * @param : $id is where condition for select query
      */

	public function po_detail($id)
	{
		$sql 	= 'CALL po_search_id(?)';

		$query 	= $this->db->query($sql,array(
				$id
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is get data in t_order table by id
      * @param : $id is where condition for select query
      */

	public function order_detail($id)
	{
		$this->db->select('id_produk,id_produkold,price,diskon');
		$this->db->from('t_order');
		$this->db->where('id_po_quotation', $id);

		$query 	= $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is used to Insert Record in po_quotation and order table
      * @param : $data - record array 
      */

	public function edit_quotation($data)
	{
		$array = array(
		        'cust_id' => $data['cust_id'],
		        'issue_date' => $data['issue_date'],
		        'price_term' => $data['term_of_pay'],
		        'valas_id' => $data['id_valas']
		);

		$this->db->where('id', $data['id_quotation']);
		$this->db->update('t_po_quotation',$array);

		$this->db->where('id_po_quotation', $data['id_quotation']);  
		$this->db->delete('t_order'); 

		$this->db->where('id_po_quot', $data['id_quotation']);  
		$this->db->delete('t_bom');

		$id_produk = $data['id_produk'];
		$id_produklen = count($data['id_produk']);
		$price = $data['price'];
		$diskon = $data['diskon'];

		$bom_no = strtoupper(randomString());

		for($i=0;$i<$id_produklen;$i++){
			$sql 	= 'INSERT INTO m_material(			
												no_bc,
												stock_code,
												stock_name,
												stock_description,
												unit,
												type,
												qty,
												treshold,
												id_properties,
												id_gudang,
												status
								  )
								  SELECT 
								  		no_bc,
										stock_code,
										stock_name,
										stock_description,
										unit,
										type,
										qty,
										treshold,
										id_properties,
										id_gudang,
										3
								  FROM
								  		m_material
								  WHERE
								  		id=?';

			$query 	= $this->db->query($sql,array($id_produk[$i]));

			$id_productnew	= $this->db->insert_id();

			$sql 	= 'CALL order_add(?,?,?,?,?,?)';

			$this->db->query($sql,
				array(
					$data['id_quotation'],
					$id_productnew,
					$id_produk[$i],
					NULL,
					$price[$i],
					$diskon[$i]
				));

			$sql 	= 'CALL bom_add(?,?,?,?,?,?)';

			$this->db->query($sql,
				array(
					$bom_no,
					$data['id_quotation'],
					$id_produk[$i],
					0,
					0,
					0
				));
		}

		$this->db->close();
		$this->db->initialize();
	}

	/**
      * This function is get data in po_quotation table by id
      * @param : $id is where condition for select query
      */

	public function detail($id)
	{
		$this->db->select("name_eksternal,
							order_no,
							order_date,
							transport,
							packing,
							issue_date,
							`price_term`,
							GROUP_CONCAT(t_order.id_order SEPARATOR '~') AS id_order,
							GROUP_CONCAT(id_produk SEPARATOR '~') AS id_produk,
							GROUP_CONCAT(price SEPARATOR '~') AS price,
							GROUP_CONCAT(diskon SEPARATOR '~') AS diskon,
							GROUP_CONCAT(disc_price SEPARATOR '~') AS disc_price,
							nama_valas,
							GROUP_CONCAT(stock_name SEPARATOR '~') AS stock_name,
							GROUP_CONCAT(t_order.qty SEPARATOR '~') AS qty,
							GROUP_CONCAT(amount_price SEPARATOR '~') AS amount_price,
							t_status_g.`status`,
	                        approval_date,
	                        notes");
		$this->db->from('t_po_quotation');
		$this->db->join('t_eksternal', 't_po_quotation.cust_id=t_eksternal.id');
		$this->db->join('t_order', 't_po_quotation.id=t_order.id_po_quotation');
		$this->db->join('m_valas', 't_po_quotation.valas_id=m_valas.valas_id');
		$this->db->join('m_material', 't_order.id_produk=m_material.id');
		$this->db->join('t_status_g', 't_po_quotation.status=t_status_g.id_status');
		$this->db->where('t_po_quotation.id', $id);

		$query 	= $this->db->get();

		$return = $query->result_array();

		$orderArray = explode('~',$return[0]['id_order']);
		$idArray = explode('~',$return[0]['id_produk']);
		$stockArray = explode('~',$return[0]['stock_name']);
		$priceArray = explode('~',$return[0]['price']);
		$diskonArray = explode('~',$return[0]['diskon']);
		$disc_priceArray = explode('~',$return[0]['disc_price']);
		$qtyArray = explode('~',$return[0]['qty']);
		$amount_priceArray = explode('~',$return[0]['amount_price']);

		$itemCount = count($stockArray);

		$data = array();
		for($i=0;$i<$itemCount;$i++){
			$this->db->select('type_shipping,date_shipping_plan');
			$this->db->from('t_schedule_shipping');
			$this->db->where('id_order', $orderArray[$i]);

			$query_shipping = $this->db->get();

			$shipping = $query_shipping->result_array();

			if(count($shipping)){
				if($shipping[0]["type_shipping"] == '0'){
					$delivery = 'ASAP';
				}else if($shipping[0]["type_shipping"] == '1'){
					$delivery = 'Menunggu info';
				}else{
					$delivery = '';
					$j = 0;
					foreach($shipping as $key) {
						if($j==0){
							$delivery = $key['date_shipping_plan'];
						}else{
							$delivery .= ','.$key['date_shipping_plan'];
						}
						$j++;
					}
				}
			}else{
				$delivery = '-';
			}

			$qty = (!isset($qtyArray[$i]) || $qtyArray[$i]=='')?'0':$qtyArray[$i];
			$amount_price = (!isset($amount_priceArray[$i]) || $amount_priceArray[$i]=='')?'0':$amount_priceArray[$i];

			$data[] = array(
						"id_order" => $orderArray[$i],
						"id_produk" => $idArray[$i],
						"stock_name" => $stockArray[$i],
						"price" => $priceArray[$i],
						"diskon" => $diskonArray[$i],
						"disc_price" => $disc_priceArray[$i],
						"qty" => $qty,
						"amount_price" => $amount_price,
						"delivery" => $delivery
					  );
		}

		$approval_date = ($return[0]['approval_date']=='')?'-':date_format(date_create($return[0]['approval_date']),'d F Y');
		$notes = ($return[0]['notes']=='')?'-':$return[0]['notes'];

		$result = array(
					"id" => $id,
					"name_eksternal" => $return[0]['name_eksternal'],
					"issue_date" => date_format(date_create($return[0]['issue_date']),'d F Y'),
					"nama_valas" => $return[0]['nama_valas'],
					"price_term" => $return[0]['price_term'],
					"status" => $return[0]['status'],
					"approval_date" => $approval_date,
					"notes" => $notes,
					"order_no" => $return[0]['order_no'],
					"order_date" => $return[0]['order_date'],
					"transport" => $return[0]['transport'],
					"packing" => $return[0]['packing'],
					"item" => $data
				  );

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	/**
      * This function is used to Update Record in po_quotation table
      * @param : $data - updated array 
      */

	public function change_status($data)
	{
		$array = array(
		        'status' => $data['status'],
		        'notes' => $data['notes'],
		        'approval_date' => date('Y-m-d')
		);

		$this->db->where('id', $data['id_quotation']);
		$this->db->update('t_po_quotation',$array);

		$array = array(
		        'status' => 5
		);
		$id_produk = explode(',',$data['id_produk']);
		$this->db->where_in('id', $id_produk);
		$this->db->update('m_material',$array);

		$this->db->close();
		$this->db->initialize();
	}
}