<?php

defined('BASEPATH') or exit('No direct script access allowed');

use Carbon\Carbon;

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Neraca extends MX_Controller
{

  public function __construct()
  {
      parent::__construct();
      $this->load->model('neraca/uom_model');
      $this->load->library('log_activity');
      $this->load->library('priv');
  }

  /**
   * anti sql injection
   */
  public function Anti_sql_injection($string)
  {
    $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
    return $string;
  }

  public function index()
  {
	  $priv = $this->priv->get_priv();

	// sales per montg
	  
	  $year = date('Y');
	  
	    $html_sb = '';
			  $html_sbs = '';
			  $html_all = '';
			  $cur_month = '';
			  
	$params  = array(
		'start_time' => date('Y-m-d'),
		'end_time' => date('Y-m-d'),
		'report' => "aktiva"
	);
	
	$params_p  = array(
		'start_time' => date('Y-m-d'),
		'end_time' => date('Y-m-d'),
		'report' => "passiva"
	);


		$datas['get_piutang'] = $this->uom_model->get_piutang($params);
		$datas['get_persediaan'] = $this->uom_model->get_persediaan($params);
		$datas['get_hutang'] = $this->get_hutang($params);
		
	//	print_r($datas['get_piutang']);die;
		
		$data['get_aktiva'] = $this->uom_model->get_report_keu($params);
		$data['get_pasiva'] = $this->uom_model->get_report_keu($params_p);
		
		
		
	$data['priv'] = $priv;
	
	$html_a = '';
		
		$tot_a = 0;
		foreach($data['get_aktiva'] as $get_aktivas){
			
			if($get_aktivas['coa'] == 11301){
				$html_a .= ' <tr>
                  <th >'.$get_aktivas['coa'].'</th>
                  <th>'.$get_aktivas['keterangan'].'</th>
                  <th>'.number_format($datas['get_piutang'][0]['piutang'],2,'.',',').'</th>
                </tr>';
			$tot_a +=$datas['get_piutang'][0]['piutang'];
			}elseif($get_aktivas['coa'] == 11600){
				$html_a .= ' <tr>
                  <th >'.$get_aktivas['coa'].'</th>
                  <th>'.$get_aktivas['keterangan'].'</th>
                  <th>'.number_format($datas['get_persediaan'][0]['persediaan'],2,'.',',').'</th>
                </tr>';
			$tot_a +=$datas['get_persediaan'][0]['persediaan'];
			}else{
				$html_a .= ' <tr>
                  <th >'.$get_aktivas['coa'].'</th>
                  <th>'.$get_aktivas['keterangan'].'</th>
                  <th>'.number_format($get_aktivas['nilai'],2,'.',',').'</th>
                </tr>';
			$tot_a +=$get_aktivas['nilai'];
			}
			
			
		}
		
			$html_a .= ' <tr>
					<th colspan=2>Total</th>
					<th>'.number_format($tot_a,2,'.',',').'</th>
				</tr>';
		
		$html_p = '';
		
		
		$tot_p = 0;
		foreach($data['get_pasiva'] as $get_aktivas){
			
			if($get_aktivas['coa'] == 20100){
				$html_p .= ' <tr>
                  <th >'.$get_aktivas['coa'].'</th>
                  <th>'.$get_aktivas['keterangan'].'</th>
                  <th>'.number_format($datas['get_hutang'][0]['hutang'],2,'.',',').'</th>
                </tr>';
			$tot_p +=$datas['get_hutang'][0]['hutang'];
			}else{
			$html_p .= ' <tr>
                  <th >'.$get_aktivas['coa'].'</th>
                  <th>'.$get_aktivas['keterangan'].'</th>
                  <th>'.number_format($get_aktivas['nilai'],2,'.',',').'</th>
                </tr>';
			$tot_p +=$get_aktivas['nilai'];
			}
		}
		
		$html_p .= ' <tr>
					<th colspan=2>Total</th>
					<th>'.number_format($tot_p,2,'.',',').'</th>
				</tr>';
		
		$data['t_ac'] = $html_a;
		$data['t_pa'] = $html_p;
	
	
	//print_r($get_aktiva);die;

    $this->template->load('maintemplate', 'neraca/views/index',$data);
  }


function laba_rugi($param){
		
		$s_date = explode('-',$param['end_time']);		
		$start_date = $s_date[0].'-01-01';

		//print_r($param);die;
		// $data   = file_get_contents("php://input");
		// $param   = json_decode($data, true);
		
		$params  = array(
			'start_time' => $start_date,
			'end_time' => $param['end_time'],
			'report' => "laba_rugi"
		);
		
		$params_p  = array(
			'start_time' => $start_date,
			'end_time' => $param['end_time'],
			'report' => "laba_rugi"
		);


		$datas['get_aktiva'] = $this->uom_model->get_report_keu_rl($params);
		$datas['get_summ'] = $this->uom_model->get_report_keu_summ_rl($params_p);
		$datas['get_piutang'] = $this->uom_model->get_piutang_rl($params);
		$datas['get_persediaan'] = $this->uom_model->get_persediaan_rl($params);
		$datas['get_hutang'] = $this->uom_model->get_hutang_rl($params);
		$datas['get_penjualan'] = $this->uom_model->get_penjualan_rl($params);
		
		$html_a = '';
		
		$tot_a = 0;
		$tot_b = 0;
		
		$hp1 = 0;
		$hp2 = 0;
		
		$penj = 0;
		$pemb = 0;
		
		foreach($datas['get_aktiva'] as $get_aktivas){
			
			if($get_aktivas['type_data'] == 0){
				
				
				
				if($get_aktivas['coa'] == 50100){
					
					$html_a .= ' <tr>
						  <th >'.$get_aktivas['coa'].'</th>
						  <th>'.$get_aktivas['keterangan'].'</th>
						  <th>'.number_format($datas['get_persediaan'][0]['persediaan_awal'],2,',','.').'</th>
						  <th>0.00</th>
						</tr>';
					$tot_a +=$datas['get_persediaan'][0]['persediaan_awal'];
					
					$nn = $datas['get_persediaan'][0]['persediaan_awal'];

					
				}elseif($get_aktivas['coa'] == 50201){
					
					$html_a .= ' <tr>
						  <th >'.$get_aktivas['coa'].'</th>
						  <th>'.$get_aktivas['keterangan'].'</th>
						  <th>'.number_format($datas['get_persediaan'][0]['pembelian'],2,',','.').'</th>
						  <th>0.00</th>
						</tr>';
					$tot_a +=$datas['get_persediaan'][0]['pembelian'];
					
					$nn = $datas['get_persediaan'][0]['pembelian'];
					$pemb = $datas['get_persediaan'][0]['pembelian'];

					
				}else{
				
					$html_a .= ' <tr>
						  <th >'.$get_aktivas['coa'].'</th>
						  <th>'.$get_aktivas['keterangan'].'</th>
						  <th>'.number_format($get_aktivas['nilai'],2,',','.').'</th>
						  <th>0.00</th>
						</tr>';
					$tot_a +=$get_aktivas['nilai'];
					
					$nn = $get_aktivas['nilai'];
				
				}
				
				if($get_aktivas['coa'] == 50100 | $get_aktivas['coa'] == 50201 | $get_aktivas['coa'] == 50202 | $get_aktivas['coa'] == 50500 | $get_aktivas['coa'] == 50600){
					$hp1 += $nn;
				}
			
			}else{
				
				if($get_aktivas['coa'] == 50700){
				
				$html_a .= ' <tr>
                  <th >'.$get_aktivas['coa'].'</th>
                  <th>'.$get_aktivas['keterangan'].'</th>
				  <th>0.00</th>
                  <th>'.number_format($datas['get_persediaan'][0]['persediaan'],2,',','.').'</th>
                </tr>';
			$tot_b +=$datas['get_persediaan'][0]['persediaan'];
			
				$nn = $datas['get_persediaan'][0]['persediaan'];
				
				}elseif($get_aktivas['coa'] == 41102){
					
					$html_a .= ' <tr>
						  <th >'.$get_aktivas['coa'].'</th>
						  <th>'.$get_aktivas['keterangan'].'</th>
						  <th>0.00</th>
						  <th>'.number_format($datas['get_penjualan'][0]['pestisida'],2,',','.').'</th>
						</tr>';
					$tot_b +=$datas['get_penjualan'][0]['pestisida'];
					
					$nn = $datas['get_penjualan'][0]['pestisida'];
					$penj += $datas['get_penjualan'][0]['pestisida'];

					
				}elseif($get_aktivas['coa'] == 41103){
					
					$html_a .= ' <tr>
						  <th >'.$get_aktivas['coa'].'</th>
						 <th>'.$get_aktivas['keterangan'].'</th>
						  <th>0.00</th>
						   <th>'.number_format($datas['get_penjualan'][0]['non_pestisida'],2,',','.').'</th>
						</tr>';
					$tot_b +=$datas['get_penjualan'][0]['non_pestisida'];
					
					$nn = $datas['get_penjualan'][0]['non_pestisida'];
					$penj += $datas['get_penjualan'][0]['non_pestisida'];
					
				}else{
					
				$html_a .= ' <tr>
                  <th >'.$get_aktivas['coa'].'</th>
                  <th>'.$get_aktivas['keterangan'].'</th>
				  <th>0.00</th>
                  <th>'.number_format($get_aktivas['nilai'],2,',','.').'</th>
                </tr>';
			$tot_b +=$get_aktivas['nilai'];
			
				$nn = $get_aktivas['nilai'];
			
				}
				
				if($get_aktivas['coa'] == 50300 | $get_aktivas['coa'] == 50400 | $get_aktivas['coa'] == 50700){
					$hp2 += $nn;
				}
				
			}
		}
		
			$html_a .= ' <tr>
					<th colspan=2>Total</th>
					<th>'.number_format($tot_a,2,'.',',').'</th>
					<th>'.number_format($tot_b,2,'.',',').'</th>
				</tr> <tr>
					<th colspan=3>Laba / Rugi </th>
					<th>'.number_format($tot_b - $tot_a,2,'.',',').'</th>
				</tr>';
		
		$html_p = '';
		
		
		$tot_p = 0;
		// foreach($datas['get_pasiva'] as $get_aktivas){
			
			// $html_p .= ' <tr>
                  // <th >'.$get_aktivas['coa'].'</th>
                  // <th>'.$get_aktivas['keterangan'].'</th>
                  // <th>'.number_format($get_aktivas['nilai'],2,'.',',').'</th>
                // </tr>';
			// $tot_p +=$get_aktivas['nilai'];
		// }
		
		// $html_p .= ' <tr>
					// <th colspan=2>Total</th>
					// <th>'.number_format($tot_p,2,'.',',').'</th>
				// </tr>';
		//print_r($datas['get_summ']);die;
		
		$html_s = '
		<tr><th>PENJUALAN BERSIH</th><th>'.number_format($penj,2,',','.').'</th></tr>
				<tr><th>BONUS PRINCIPLE</th><th>'.number_format($datas['get_summ'][1]['nilai'],2,',','.').'</th></tr>
				<tr><th>HARGA POKOK PENJUALAN</th><th>'.number_format(($hp1 - $hp2),2,',','.').'</th></tr>
				<tr><th><STRONG>LABA KOTOR</STRONG></th><th>'.number_format($penj + $datas['get_summ'][1]['nilai'] - ($hp1 - $hp2),2,',','.').'</th></tr>
				<tr><th>BIAYA PENJUALAN</th><th>'.number_format($datas['get_summ'][3]['nilai'],2,',','.').'</th></tr>
				<tr><th>BIAYA ADMINISTRASI dan UMUM</th><th>'.number_format($datas['get_summ'][4]['nilai'],2,',','.').'</th></tr>
				<tr><th>BIAYA PENYUSUTAN</th><th>'.number_format($datas['get_summ'][5]['nilai'],2,',','.').'</th></tr>
				<tr><th><STRONG>BIAYA OPERASIONAL</STRONG></th><th>'.number_format($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai'],2,',','.').'</th></tr>
				<tr><th><STRONG>LABA (RUGI) OPERASI</STRONG></th><th>'.number_format(($datas['get_summ'][0]['nilai'] + $datas['get_summ'][1]['nilai'] - $datas['get_summ'][2]['nilai'])-($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai']),2,',','.').'</th></tr>
				<tr><th>PENDAPATAN NON OPERASI</th><th>'.number_format($datas['get_summ'][6]['nilai'],2,',','.').'</th></tr>
				<tr><th>BIAYA NON OPERASI</th><th>'.number_format($datas['get_summ'][7]['nilai'],2,',','.').'</th></tr>
				<tr><th><STRONG>LABA / RUGI BERSIH SEBELUM PAJAK</STRONG></th><th>'.number_format(($penj + $datas['get_summ'][1]['nilai'] - ($hp1 - $hp2))-($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai'])+$datas['get_summ'][6]['nilai']-$datas['get_summ'][7]['nilai'],2,',','.').'</th></tr>
		';
		
		$datas['t_ac'] = $html_a;
		//$datas['t_pa'] = $html_p;
		$datas['t_pa'] = $html_s;
		
		$data_ret['penjualan'] = $penj;
		$data_ret['pembelian'] = $pemb;
		$data_ret['total_rl'] = ($penj + $datas['get_summ'][1]['nilai'] - ($hp1 - $hp2))-($datas['get_summ'][3]['nilai'] + $datas['get_summ'][4]['nilai'] + $datas['get_summ'][5]['nilai'])+$datas['get_summ'][6]['nilai']-$datas['get_summ'][7]['nilai'];
		
		
		$data_ret['ppn'] = ($pemb*0.1) - ($penj*0.1);
		
		
		return $data_ret;
		//$this->output->set_content_type('application/json')->set_output(json_encode($datas));
		//print_r($datas['get_aktiva']);die;
		
	}


	function filter(){
		
		$data   = file_get_contents("php://input");
		$param   = json_decode($data, true);
		
		
		$params  = array(
			'start_time' => $param['start_time'],
			'end_time' => $param['end_time'],
			'report' => "aktiva"
		);
		
		$params_p  = array(
			'start_time' => $param['start_time'],
			'end_time' => $param['end_time'],
			'report' => "passiva"
		);

		$datas['get_piutang'] = $this->uom_model->get_piutang($params);
		$datas['get_persediaan'] = $this->uom_model->get_persediaan($params);
		$datas['get_hutang'] = $this->get_hutang($params);
		
		$datas['get_laba_rugi'] = $this->laba_rugi($params);
		
	//print_r($datas['get_persediaan']);die;
		
		
		if($param['end_time'] > '2021-01-01'){

		$datas['get_aktiva'] = $this->uom_model->get_report_keu($params);
		$datas['get_pasiva'] = $this->uom_model->get_report_keu($params_p);
		
		$html_a = '';
		
		$tot_a = 0;
		foreach($datas['get_aktiva'] as $get_aktivas){
			
		
			
			if($get_aktivas['coa'] == 11301){
				$html_a .= ' <tr>
                  <th >'.$get_aktivas['coa'].'</th>
                  <th>'.$get_aktivas['keterangan'].'</th>
                  <th>'.number_format($datas['get_piutang'][0]['piutang'],2,'.',',').'</th>
                </tr>';
			$tot_a +=$datas['get_piutang'][0]['piutang'];
			}elseif($get_aktivas['coa'] == 11600){
				$html_a .= ' <tr>
                  <th >'.$get_aktivas['coa'].'</th>
                  <th>'.$get_aktivas['keterangan'].'</th>
                  <th>'.number_format($datas['get_persediaan'][0]['persediaan'],2,'.',',').'</th>
                </tr>';
			$tot_a +=$datas['get_persediaan'][0]['persediaan'];
			}elseif($get_aktivas['coa'] == 70100){
				$html_a .= ' <tr>
                  <th >'.$get_aktivas['coa'].'</th>
                  <th>'.$get_aktivas['keterangan'].'</th>
                  <th>'.number_format(($get_aktivas['nilai']+($datas['get_laba_rugi']['ppn'])),2,'.',',').'</th>
                </tr>';
			$tot_a +=($get_aktivas['nilai']+($datas['get_laba_rugi']['ppn']));
			}else{
				
				if($get_aktivas['id_parent'] == 3){
					
					$params_c  = array(
						'start_date' => $param['start_time'],
						'end_date' => $param['end_time'],
						'coa_id' => $get_aktivas['id_coa']
					);
					
					$get_cash = $this->get_cash($params_c);
					
					$html_a .= ' <tr>
					<th >'.$get_aktivas['coa'].'</th>
					<th>'.$get_aktivas['keterangan'].'</th>
					<th>'.number_format($get_cash,2,'.',',').'</th>
					</tr>';
					$tot_a +=$get_cash;
					
				}else{
					$html_a .= ' <tr>
					<th >'.$get_aktivas['coa'].'</th>
					<th>'.$get_aktivas['keterangan'].'</th>
					<th>'.number_format($get_aktivas['nilai'],2,'.',',').'</th>
					</tr>';
					$tot_a +=$get_aktivas['nilai'];
				}
			
			}
			
		}
		
			$html_a .= ' <tr>
					<th colspan=2>Total</th>
					<th>'.number_format($tot_a,2,'.',',').'</th>
				</tr>';
		
		$html_p = '';
		
		
		$tot_p = 0;
		foreach($datas['get_pasiva'] as $get_aktivas){
			
			if($get_aktivas['coa'] == 20100){
				$html_p .= ' <tr>
                  <th >'.$get_aktivas['coa'].'</th>
                  <th>'.$get_aktivas['keterangan'].'</th>
                  <th>'.number_format($datas['get_hutang'],2,'.',',').'</th>
                </tr>';
			$tot_p +=$datas['get_hutang'];
			}elseif($get_aktivas['coa'] == 30400){
				$html_p .= ' <tr>
                  <th >'.$get_aktivas['coa'].'</th>
                  <th>'.$get_aktivas['keterangan'].'</th>
                  <th>'.number_format(($get_aktivas['nilai']+($datas['get_laba_rugi']['total_rl'])),2,'.',',').'</th>
                </tr>';
			$tot_p +=($get_aktivas['nilai']+($datas['get_laba_rugi']['total_rl']));
			}else{
			$html_p .= ' <tr>
                  <th >'.$get_aktivas['coa'].'</th>
                  <th>'.$get_aktivas['keterangan'].'</th>
                  <th>'.number_format($get_aktivas['nilai'],2,'.',',').'</th>
                </tr>';
			$tot_p +=$get_aktivas['nilai'];
			}
		}
		
		$html_p .= ' <tr>
					<th colspan=2>Total</th>
					<th>'.number_format($tot_p,2,'.',',').'</th>
				</tr>';
		
		$datas['t_ac'] = $html_a;
		$datas['t_pa'] = $html_p;
		
		
		}else{
			
				$params  = array(
					'start_time' => date('Y-m-d'),
					'end_time' => date('Y-m-d'),
					'report' => "aktiva_saldo"
				);
			
			$datas['get_aktiva'] = $this->uom_model->get_report_keu_awal($params);
			$datas['get_pasiva'] = $this->uom_model->get_report_keu($params_p);
			
			
			$html_a = '';
			
			$tot_a = 0;
		
			foreach($datas['get_aktiva'] as $get_aktivas){
			
			$html_a .= ' <tr>
					<th >'.$get_aktivas['coa'].'</th>
					<th>'.$get_aktivas['keterangan'].'</th>
					<th>'.number_format($get_aktivas['nilai'],2,'.',',').'</th>
					</tr>';
					$tot_a +=$get_aktivas['nilai'];
			
			}
			
			$html_a .= ' <tr>
					<th colspan=2>Total</th>
					<th>'.number_format($tot_a,2,'.',',').'</th>
				</tr>';
				
				 
			$html_p = '';
			$tot_p = 0;
			
			foreach($datas['get_pasiva'] as $get_aktivas){
			
				$html_p .= ' <tr>
					  <th >'.$get_aktivas['coa'].'</th>
					  <th>'.$get_aktivas['keterangan'].'</th>
					  <th>'.number_format($get_aktivas['nilai'],2,'.',',').'</th>
					</tr>';
				$tot_p +=$get_aktivas['nilai'];
			}
			
			
			$html_p .= ' <tr>
					<th colspan=2>Total</th>
					<th>'.number_format($tot_p,2,'.',',').'</th>
				</tr>';
		
			$datas['t_ac'] = $html_a;
			$datas['t_pa'] = $html_p;
			
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($datas));
		//print_r($datas['get_aktiva']);die;
		
	}


public function get_cash($param){
	
		$params['coa_id']		= $param['coa_id'];
		$params['start_date']	= $param['start_date'];
		$params['end_date']		= $param['end_date'];
	
		$list_saldo = $this->uom_model->list_report_cash_saldo($params);

		$list = $this->uom_model->list_report_cash($params);
		$typeCoa = ['Debit', 'Kredit'];
		$data = [];
		$balance = 0.00;

		foreach ($list_saldo['data'] as $row) {
			
			array_push($data, [
				'', // tanggal
				'', // tanggal
				// $typeCoa[$row['type_cash']] ?? '-', // transaksi
				'', 
				'Saldo Awal', // transaksi
				// kode akun
				'', // akun
				'', // uraian
				'-', // debit
				'-', // kredit
				indonesia_currency_format($row['saldo_awal']), // saldo
				// '-', // action
			]);
			
			
			$balance = $row['saldo_awal'];
			
		}

		foreach ($list['data'] as $row) {

			if ($row['type_cash'] == 0) {
				$balance += $row['sum_amnt'];
			} else if ($row['type_cash'] == 1) {
				$balance -= $row['sum_amnt'];
			}
			
			// if($row['kr_note'] == 'Utang Dagang' || $row['kr_note'] == 'Piutang Dagang'){ 
				// $row['banks'] = $row['keterangan']; 
			// }
			
			// if ($row['bukti'] == "Bayar Piutang") {
				// $faktur = $row['jual'];
				// $note_ac = $row['cust_name'];
			// }elseif ($row['bukti'] == "Bayar Hutang") {
				// $faktur = $row['beli'];
				// $note_ac = $row['name_eksternal'];
			// }else{
				$faktur = '';
				$note_ac = $row['k_note'].''.$row['users'];
			// }
			
			array_push($data, [
				$row['date'], // tanggal
				$row['banks'], // tanggal
				// $typeCoa[$row['type_cash']] ?? '-', // transaksi
				$row['kr_coa'], 
				$row['kr_note'], // transaksi
				// kode akun 
				$note_ac, // akun
				$row['invoice'], // uraian
				$row['type_cash'] == 0 ? indonesia_currency_format($row['sum_amnt']) : '-', // debit
				$row['type_cash'] == 1 ? indonesia_currency_format($row['sum_amnt']) : '-', // kredit
				indonesia_currency_format($balance), // saldo
				// '-', // action
			]);
		}
		
		// foreach(){
			
				
			
		// }
		
		return $balance;
		
		//print_r($data);
	
}

public function get_hutang($param){
		
		$kuantiti_a = 0;
		$harga_satuan_a = 0;
		$nilai_a = 0;
		$pembelian_a = 0;
		$kuantiti_sol_a = 0;
		$kuantiti_titip_a = 0;
		$kuantiti_bonus_a = 0;
		$harga_sat_pemb_a = 0;
		$nilai_pemb_a = 0;
		$harga_pokok_a = 0;
		$penjualan_a = 0;
		$pengeluaran_a = 0;
		$saldo_akhir_a = 0;
		$nilai_akhir_a = 0;
		$invoice_saldo_a = 0;
		$d0_a = 0;
		$d30_a = 0;
		$d60_a = 0;
		$d90_a = 0;
		$d120_a = 0;
		$total_t_a = 0;
		
			
			$dist_a = $this->uom_model->get_distribution([]);
			$html = '';
			
			foreach($dist_a as $dist_as){
			
				$datat = array(
					'principal' => $dist_as['dist_id'],
					'start_time' => $this->Anti_sql_injection($param['start_time']),
					'end_time' => $this->Anti_sql_injection($param['end_time'])
					
				);
			
				$result = $this->uom_model->get_hutang($datat);
				// echo "<pre>";
			// print_r($result);die;
				
				
				$kuantiti = 0;
				$harga_satuan = 0;
				$nilai = 0;
				$pembelian = 0;
				$kuantiti_sol = 0;
				$kuantiti_titip = 0;
				$kuantiti_bonus = 0;
				$harga_sat_pemb = 0;
				$nilai_pemb = 0;
				$harga_pokok = 0;
				$penjualan = 0;
				$pengeluaran = 0;
				$saldo_akhir = 0;
				$nilai_akhir = 0;
				$invoice_saldo = 0;
				$prin = '';
				$kode_prin = '';
				
				$d0 = 0;
				$d30 = 0;
				$d60 = 0;
				$d90 = 0;
				$d120 = 0;
				$total_t = 0;
		
		$curr_invoice = '';
				foreach( $result as $datas ){
					
					if($curr_invoice <> $datas['no_invoice']){
						
							$bayar_r_te =  number_format($datas['bayar'],2,',','.');
						$bayar_r =  $datas['bayar'];
						$bb =  $datas['bayar'];
						$curr_invoice = $datas['no_invoice'];
						$sisa_r_te =  number_format(($datas['prices']-( $datas['prices']*($datas['diskons']/100)) + ( ($datas['prices']-( $datas['prices']*($datas['diskons']/100) ))*($datas['pajak']/100) ))-$datas['bayar'],2,',','.');
						$sisa_r =  ($datas['prices']-( $datas['prices']*($datas['diskons']/100)) + ( ($datas['prices']-( $datas['prices']*($datas['diskons']/100) ))*($datas['pajak']/100) ))-$datas['bayar'];
						$curr_invoice = $datas['no_invoice'];
						
					}else{
							$bayar_r_te = null;
						$sisa_r_te = null;
						$bayar_r = 0;
						$sisa_r = 0;
						$bb = 0;
					}
					
						$ttl = $datas['min_30']-( $datas['min_30']*($datas['diskons']/100)) + ( ($datas['min_30']-( $datas['min_30']*($datas['diskons']/100) ))*($datas['pajak']/100)) + $datas['d30']-( $datas['d30']*($datas['diskons']/100)) + ( ($datas['d30']-( $datas['d30']*($datas['diskons']/100) ))*($datas['pajak']/100) ) + $datas['d60']-( $datas['d60']*($datas['diskons']/100)) + ( ($datas['d60']-( $datas['d60']*($datas['diskons']/100) ))*($datas['pajak']/100) ) + $datas['d90']-( $datas['d90']*($datas['diskons']/100)) + ( ($datas['d90']-( $datas['d90']*($datas['diskons']/100) ))*($datas['pajak']/100) ) + $datas['d120']-( $datas['d120']*($datas['diskons']/100)) + ( ($datas['d120']-( $datas['d120']*($datas['diskons']/100) ))*($datas['pajak']/100) );
			
				$prcs = $datas['prices']-( $datas['prices']*($datas['diskons']/100)) + ( ($datas['prices']-( $datas['prices']*($datas['diskons']/100) ))*($datas['pajak']/100) );


				$bb = $bb - $prcs ;
				
				if($bb > 0){
					$ss_sisa = 0;
				}else{
					$ss_sisa = abs($bb);
				}
				
				if($datas['umur'] < 31){
					$m1 = $ss_sisa;
					$m2 = 0;
					$m3 = 0;
					$m4 = 0;
					$m5 = 0;
				}elseif($datas['umur'] > 30 && $datas['umur'] < 61){
					$m1 = 0;
					$m2 = $ss_sisa;
					$m3 = 0;
					$m4 = 0;
					$m5 = 0;
				}elseif($datas['umur'] > 60 && $datas['umur'] < 91){
					$m1 = 0;
					$m2 = 0;
					$m3 = $ss_sisa;
					$m4 = 0;
					$m5 = 0;
				}elseif($datas['umur'] > 90 && $datas['umur'] < 121){
					$m1 = 0;
					$m2 = 0;
					$m3 = 0;
					$m4 = $ss_sisa;
					$m5 = 0;
				}else{
					$m1 = 0;
					$m2 = 0;
					$m3 = 0;
					$m4 = 0;
					$m5 = $ss_sisa;
				}
				
				$ttl = $m1 + $m2 +$m3 +$m4 +$m5 ;
					
				
							
							$html .= '<tr>
									<th >'.$datas['name_eksternal'].'</th>
								<th>'.$datas['date_po'].'</th>
								<th>'.$datas['no_po'].'</th>
								<th>'.$datas['no_invoice'].'</th>
								<th>'.$datas['jatuh_tempo'].'</th>
								<th>'.$datas['umur'].'</th>
								<th>'.number_format($datas['saldo']-( $datas['saldo']*($datas['diskons']/100)) + ( ($datas['saldo']-( $datas['saldo']*($datas['diskons']/100) ))*($datas['pajak']/100) ),2,',','.').'</th>
								<th>'.number_format($datas['bulan_berjalan']-( $datas['bulan_berjalan']*($datas['diskons']/100)) + ( ($datas['bulan_berjalan']-( $datas['bulan_berjalan']*($datas['diskons']/100) ))*($datas['pajak']/100) ),2,',','.').'</th>
								<th>'.$datas['tgl_bayar'].'</th>
								<th>'.$bayar_r_te.'</th>
								<th>'.number_format($ss_sisa,2,',','.').'</th>
								<th>'.number_format($datas['invoice_saldo'],2,',','.').'</th>
								<th>'.number_format($m1,2,',','.').'</th>
								<th>'.number_format($m2,2,',','.').'</th>
								<th>'.number_format($m3,2,',','.').'</th>
								<th>'.number_format($m4,2,',','.').'</th>
								<th>'.number_format($m5,2,',','.').'</th>
								<th>'.number_format($ttl,2,',','.').'</th>
							</tr>';
					
						$prin = $datas['name_eksternal'];
					

					$curr_invoice = $datas['no_invoice'];
					
					$penjualan = $penjualan+ ($datas['saldo']-( $datas['saldo']*($datas['diskons']/100)) + ( ($datas['saldo']-( $datas['saldo']*($datas['diskons']/100) ))*($datas['pajak']/100) ));
					$pengeluaran = $pengeluaran+ ($datas['bulan_berjalan']-( $datas['bulan_berjalan']*($datas['diskons']/100)) + ( ($datas['bulan_berjalan']-( $datas['bulan_berjalan']*($datas['diskons']/100) ))*($datas['pajak']/100) ));
					$saldo_akhir = $saldo_akhir+ $bayar_r;
					$nilai_akhir = $nilai_akhir+ $datas['bayar'];
					$invoice_saldo = $invoice_saldo+ $datas['invoice_saldo'];
					
					// $d0 = $d0 + $datas['min_30']-( $datas['min_30']*($datas['diskons']/100)) + ( ($datas['min_30']-( $datas['min_30']*($datas['diskons']/100) ))*($datas['pajak']/100));
					// $d30 = $d30 + $datas['d30']-( $datas['d30']*($datas['diskons']/100)) + ( ($datas['d30']-( $datas['d30']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// $d60 = $d60 + $datas['d60']-( $datas['d60']*($datas['diskons']/100)) + ( ($datas['d60']-( $datas['d60']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// $d90 = $d90 + $datas['d90']-( $datas['d90']*($datas['diskons']/100)) + ( ($datas['d90']-( $datas['d90']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// $d120 = $d120 + $datas['d120']-( $datas['d120']*($datas['diskons']/100)) + ( ($datas['d120']-( $datas['d120']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// $total_t = $total_t + $ttl;
					
					$d0 = $d0 + $m1;
					$d30 = $d30 + $m2;
					$d60 = $d60 + $m3;
					$d90 = $d90 + $m4;
					$d120 = $d120 + $m5;
					$total_t = $total_t + $ttl;
					
						$penjualan_a = $penjualan_a +  ($datas['saldo']-( $datas['saldo']*($datas['diskons']/100)) + ( ($datas['saldo']-( $datas['saldo']*($datas['diskons']/100) ))*($datas['pajak']/100) ));
					$pengeluaran_a = $pengeluaran_a +  ($datas['bulan_berjalan']-( $datas['bulan_berjalan']*($datas['diskons']/100)) + ( ($datas['bulan_berjalan']-( $datas['bulan_berjalan']*($datas['diskons']/100) ))*($datas['pajak']/100) ));
					$saldo_akhir_a = $saldo_akhir_a +  $bayar_r;
					$nilai_akhir_a = $nilai_akhir_a +  $datas['bayar'];
					$invoice_saldo_a = $invoice_saldo_a +  $datas['invoice_saldo'];
					
					// $d0_a = $d0_a + $datas['min_30']-( $datas['min_30']*($datas['diskons']/100)) + ( ($datas['min_30']-( $datas['min_30']*($datas['diskons']/100) ))*($datas['pajak']/100));
					// $d30_a = $d30_a + $datas['d30']-( $datas['d30']*($datas['diskons']/100)) + ( ($datas['d30']-( $datas['d30']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// $d60_a = $d60_a + $datas['d60']-( $datas['d60']*($datas['diskons']/100)) + ( ($datas['d60']-( $datas['d60']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// $d90_a = $d90_a + $datas['d90']-( $datas['d90']*($datas['diskons']/100)) + ( ($datas['d90']-( $datas['d90']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// $d120_a = $d120_a + $datas['d120']-( $datas['d120']*($datas['diskons']/100)) + ( ($datas['d120']-( $datas['d120']*($datas['diskons']/100) ))*($datas['pajak']/100) );
					// $total_t_a = $total_t_a + $ttl;
					
					$d0_a = $d0_a + $m1;
					$d30_a = $d30_a + $m2;
					$d60_a = $d60_a + $m3;
					$d90_a = $d90_a + $m4;
					$d120_a = $d120_a + $m5;
					$total_t_a = $total_t_a + $ttl;
					
				}
			
				$html .= '<tr style="background:yellow">
							<th>'.$prin.'</th>
							<th></th>
							<th ></th>
							<th ></th>	
							<th></th>
							<th></th>
							<th>'.number_format($penjualan,2,',','.').'</th>
							<th>'.number_format($pengeluaran,2,',','.').'</th>
							<th></th>
							<th>'.number_format($saldo_akhir,2,',','.').'</th>
							<th>'.number_format($penjualan+$pengeluaran-$saldo_akhir,2,',','.').'</th>
							<th>'.number_format($invoice_saldo,2,',','.').'</th>
							<th>'.number_format($d0,2,',','.').'</th>
							<th>'.number_format($d30,2,',','.').'</th>
							<th>'.number_format($d60,2,',','.').'</th>
							<th>'.number_format($d90,2,',','.').'</th>
							<th>'.number_format($d120,2,',','.').'</th>
							<th>'.number_format($total_t,2,',','.').'</th>
						</tr>';
			}
		
		
		$html .= '<tr style="background:green">
						<th colspan="3">Total </th>
						<th></th>
							<th ></th>
							<th></th>
						<th>'.number_format($penjualan_a,2,',','.').'</th>
						<th>'.number_format($pengeluaran_a,2,',','.').'</th>
						<th></th>
						<th>'.number_format($saldo_akhir_a,2,',','.').'</th>
						<th>'.number_format($penjualan_a+$pengeluaran_a-$saldo_akhir_a,2,',','.').'</th>
						<th>'.number_format($invoice_saldo_a,2,',','.').'</th>
							<th>'.number_format($d0_a,2,',','.').'</th>
							<th>'.number_format($d30_a,2,',','.').'</th>
							<th>'.number_format($d60_a,2,',','.').'</th>
							<th>'.number_format($d90_a,2,',','.').'</th>
							<th>'.number_format($d120_a,2,',','.').'</th>
							<th>'.number_format($total_t_a,2,',','.').'</th>
					</tr>';
		
		$results['html'] = $html;
		
		return $total_t_a;
		
		//$this->output->set_content_type('application/json')->set_output(json_encode($results));
		//print_r($result);die;
		
		
	}


  function lists()
  {

    if( !empty($_GET['sess_user_id']) ) {
			  $sess_user_id = $_GET['sess_user_id'];
		  } else {
			  $sess_user_id = NULL;
		  }
		  
		   if( !empty($_GET['sess_token']) ) {
			  $sess_token = $_GET['sess_token'];
		  } else {
			  $sess_token = NULL;
		  }
		  
		    if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		  if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		  if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 				
		  $order_fields = array('coa'); // , 'COST'
		  $order = $this->input->get_post('order');
		  if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'desc';}; 
		  if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 	
		  
		    $params['limit'] 		= (int) $length;
			$params['offset'] 		= (int) $start;
			$params['order_column'] = $order_fields[$order_column];
			$params['order_dir'] 	= $order_dir;
			$params['sess_user_id'] = $sess_user_id;
			$params['sess_token'] 	= $sess_token;
			$params['searchtxt'] 	= $_GET['search']['value'];
		  

    //print_r($params);die;

    $list = $this->uom_model->lists($params);
	$priv = $this->priv->get_priv();
    //print_r($list);die;

    $result["recordsTotal"] = $list['total'];
    $result["recordsFiltered"] = $list['total_filtered'];
    $result["draw"] = $draw;

    //print_r($result);die;

    $data = array();
    $i = $params['offset'];
    $username = $this->session->userdata['logged_in']['username'];
    foreach ($list['data'] as $k => $v) {
      $i = $i + 1;


      $status_akses = '
                <div class="btn-group" style="display:'.$priv['update'].'"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edituom(\'' . $v['id'] . '\')"><i class="fa fa-edit"></i></button></div>
				  <div class="btn-group" style="display:'.$priv['delete'].'"><button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteuom(\'' . $v['id'] . '\')"><i class="fa fa-trash"></i></button></div>';
				  
				  if($v['type_cash'] == 0){
					  $sss = 'Pemasukan';
				  }else{
					  $sss = 'Pengeluaran';
				  }

      array_push($data, array(
        $i,
        $v['coa'].'-'.$v['keterangan'],
        $sss,
        number_format($v['value_real'],2,',','.'),
        $v['note'],
        $status_akses
      ));
    }

    $result["data"] = $data;

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function add()
  {
	  
	  
    $coa = $this->uom_model->get_coa();



    // $this->load->view('add_modal_view', $data);

    // $result = $this->distributor_model->location();

    $data = array(
      'group' => '',
      'coa' => $coa
    );

    $this->template->load('maintemplate', 'neraca/views/add_modal_view', $data);
  }

  public function edit()
  {
    //$id = $this->Anti_sql_injection($this->input->post('sid', TRUE));
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('iduom', TRUE)),
		);
    $result = $this->uom_model->get_uom_data($this->Anti_sql_injection($data['id']));
	 $coa = $this->uom_model->get_coa();

    //print_r($result);die;
    // $roles = $this->uom_model->roles($id);

    $data = array(
      'uom' => $result[0],
      'id' => $this->Anti_sql_injection($this->input->post('iduom', TRUE)),
	  'coa' => $coa
    );

    $this->template->load('maintemplate', 'neraca/views/edit_modal_view', $data);
  }

  public function deletes()
  {

    $data   = file_get_contents("php://input");
    $params   = json_decode($data, true);

    $list = $this->uom_model->deletes($this->Anti_sql_injection($params['id']));

    $res = array(
      'status' => 'success',
      'message' => 'Data telah di hapus'
    );

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }

  public function edit_uom()
  {
	$this->form_validation->set_rules('coa', 'coa', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('tgl', 'tgl', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('cash', 'cash', 'trim|required|max_length[255]');
    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));
      $result = array(
        'success' => false,
        'message' => $msg
      );
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      // die;
      $message = "";
      $data = array(
       'user_id' => $this->session->userdata['logged_in']['user_id'],
        'coa'     => $this->Anti_sql_injection($this->input->post('coa', TRUE)),
        'id'     => $this->Anti_sql_injection($this->input->post('id', TRUE)),
        'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
        'cash' => $this->Anti_sql_injection($this->input->post('cash', TRUE)),
        'jumlah' => $this->Anti_sql_injection($this->input->post('jumlah', TRUE)),
        'ket' => $this->Anti_sql_injection($this->input->post('ket', TRUE))
      );
      $result = $this->uom_model->edit_uom($data);
      if ($result > 0) {
        $msg = 'Berhasil merubah uom.';

        $result = array(
          'success' => true,
          'message' => $msg
        );
      } else {
        $msg = 'Gagal merubah uom.';

        $result = array(
          'success' => false,
          'message' => $msg
        );
      }

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
  }

  public function add_uom()
  {
    $this->form_validation->set_rules('coa', 'coa', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('tgl', 'tgl', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('cash', 'cash', 'trim|required|max_length[255]');
    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));
      $result = array(
        'success' => false,
        'message' => $msg
      );

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      // die;
      $message = "";
      $data = array(
        'user_id' => $this->session->userdata['logged_in']['user_id'],
        'coa'     => $this->Anti_sql_injection($this->input->post('coa', TRUE)),
        'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
        'cash' => $this->Anti_sql_injection($this->input->post('cash', TRUE)),
        'jumlah' => $this->Anti_sql_injection($this->input->post('jumlah', TRUE)),
        'ket' => $this->Anti_sql_injection($this->input->post('ket', TRUE))
      );

		//print_r($data);die;

      $result = $this->uom_model->add_uom($data);

      if ($result > 0) {
        $msg = 'Berhasil menambahkan uom.';

        $result = array(
          'success' => true,
          'message' => $msg
        );
      } else {
        $msg = 'Gagal menambahkan uom ke database.';
        $result = array(
          'success' => false,
          'message' => $msg
        );
      }
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
  }

  public function export()
  {
    $this->load->library('excel');

    $objPHPExcel = new PHPExcel();

    $end_date = anti_sql_injection($this->input->post('date_filter'));
    $end_date = Carbon::parse($end_date)->isValid() ? $end_date : null;

    $params  = array(
      'start_time' => date('Y-m-d'),
      'end_time' => $end_date ?? date('Y-m-d'),
      'report' => "aktiva"
    );

		$datas['get_piutang'] = $this->uom_model->get_piutang($params);
		$datas['get_persediaan'] = $this->uom_model->get_persediaan($params);
		$datas['get_hutang'] = $this->uom_model->get_hutang($params);

    # FIRST SHEET
    $aktivas = $this->uom_model->get_report_keu($params);
    // echo '<pre>'; print_r($aktivas); die;
	
	 $objPHPExcel->setActiveSheetIndex(0)
      ->setCellValue('A1', 'Aktiva');

    $row = 3;
    $totalNilai = 0;
    $objPHPExcel->getActiveSheet()->setTitle('Neraca');
    $objPHPExcel->setActiveSheetIndex(0)
      ->setCellValue('A2', 'COA')
      ->setCellValue('B2', 'Keterangan')
      ->setCellValue('C2', 'Nilai');
    foreach ($aktivas as $aktiva) {
		
		if($aktiva['coa'] == 11301){
			
			 $objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A' . $row, $aktiva['coa'] ?? '')
			->setCellValue('B' . $row, $aktiva['keterangan'] ?? '')
			->setCellValue('C' . $row, $datas['get_piutang'][0]['piutang'] ?? '');

			  $row++;
			  $totalNilai += $datas['get_piutang'][0]['piutang'];
		
		}elseif($aktiva['coa'] == 11600){
			
			 $objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A' . $row, $aktiva['coa'] ?? '')
			->setCellValue('B' . $row, $aktiva['keterangan'] ?? '')
			->setCellValue('C' . $row, $datas['get_persediaan'][0]['persediaan'] ?? '');

			  $row++;
			  $totalNilai += $datas['get_persediaan'][0]['persediaan'];
		
		}else{
			
		  $objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A' . $row, $aktiva['coa'] ?? '')
			->setCellValue('B' . $row, $aktiva['keterangan'] ?? '')
			->setCellValue('C' . $row, $aktiva['nilai'] ?? '');

		  $row++;
		  $totalNilai += $aktiva['nilai'];
		  
		}
	
    }

    $objPHPExcel->setActiveSheetIndex(0)
      ->mergeCells('A' . $row . ':' . 'B' . $row)
      ->setCellValue('A' . $row, 'Total')
      ->setCellValue('C' . $row, $totalNilai);

    # SECOND SHEET
    $params_passiva  = array(
      'start_time' => date('Y-m-d'),
      'end_time' => $end_date ?? date('Y-m-d'),
      'report' => "passiva"
    );
    $passivas = $this->uom_model->get_report_keu($params_passiva);

 //   $objPHPExcel->createSheet(1)->setTitle('Passiva');
    $objPHPExcel->setActiveSheetIndex(0);

 $objPHPExcel->setActiveSheetIndex(0)
      ->setCellValue('E1', 'Passiva');
    $row = 3;
    $totalNilai = 0;
    $objPHPExcel->setActiveSheetIndex(0)
      ->setCellValue('E2', 'COA')
      ->setCellValue('F2', 'Keterangan')
      ->setCellValue('G2', 'Nilai');
    foreach ($passivas as $passiva) {
		
		if($passiva == 20100){
		
			  $objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('E' . $row, $passiva['coa'] ?? '')
				->setCellValue('F' . $row, $passiva['keterangan'] ?? '')
				->setCellValue('G' . $row, $datas['get_hutang'][0]['hutang']?? '');

			  $row++;
			  $totalNilai += $datas['get_hutang'][0]['hutang'];
		
		}else{
			  $objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('E' . $row, $passiva['coa'] ?? '')
				->setCellValue('F' . $row, $passiva['keterangan'] ?? '')
				->setCellValue('G' . $row, $passiva['nilai'] ?? '');

			  $row++;
			  $totalNilai += $passiva['nilai'];
	  
		}
	  
	  
    }

    $objPHPExcel->setActiveSheetIndex(0)
      ->mergeCells('E' . $row . ':' . 'F' . $row)
      ->setCellValue('E' . $row, 'Total')
      ->setCellValue('G' . $row, $totalNilai);

    $objPHPExcel->setActiveSheetIndex(0);

    // Redirect output to a client’s web browser (Excel2007)
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename=neraca.xls');
    header('Cache-Control: max-age=0');
    // If you’re serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');
    // If you’re serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
    unset($objPHPExcel);
  }
}
