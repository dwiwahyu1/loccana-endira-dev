<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Report_hutang_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param array $params
	 * @return mixed
	 */
	public function list_report_hutang($params = [])
	{
		// get total
		$total = $this->db
			->select('t_purchase_order.id_po')
			->from('t_purchase_order')
			->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id','LEFT')
			->join('t_po_mat', 't_purchase_order.id_po = t_po_mat.id_po','LEFT')
			->join('m_material', 't_po_mat.id_material = m_material.id_mat','LEFT')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom','LEFT')
			->join('t_bpb_detail', 't_po_mat.id_t_ps = t_bpb_detail.id_po_mat','LEFT')
			->join('t_invoice_pembelian', 't_bpb_detail.id_bpb = t_invoice_pembelian.id_bpb','LEFT')
			->join('d_payment_hp', 't_invoice_pembelian.id_invoice = d_payment_hp.id_invoice','LEFT')
			->join('t_payment_hp', 'd_payment_hp.id_hp = t_payment_hp.id_hp','LEFT')
			->get()
			->result_array();

		// get filtered data
		$filteredQueryBuilder = $this->db
			->select('*, t_payment_hp.date AS `t_payment_hp.date`')
			->from('t_purchase_order')
			->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id','LEFT')
			->join('t_po_mat', 't_purchase_order.id_po = t_po_mat.id_po','LEFT')
			->join('m_material', 't_po_mat.id_material = m_material.id_mat','LEFT')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom','LEFT')
			->join('t_bpb_detail', 't_po_mat.id_t_ps = t_bpb_detail.id_po_mat','LEFT')
			->join('t_invoice_pembelian', 't_bpb_detail.id_bpb = t_invoice_pembelian.id_bpb','LEFT')
			->join('d_payment_hp', 't_invoice_pembelian.id_invoice = d_payment_hp.id_invoice','LEFT')
			->join('t_payment_hp', 'd_payment_hp.id_hp = t_payment_hp.id_hp','LEFT')
			//
		;

		if (isset($params['principle_id']) && !empty($params['principle_id'])) {
			$filteredQueryBuilder = $filteredQueryBuilder->where(['t_purchase_order.id_distributor' => $params['principle_id']]);
		}

		if (isset($params['searchtxt']) && !empty($params['searchtxt'])) {
			$filteredQueryBuilder = $filteredQueryBuilder->like('t_eksternal.name_eksternal', $params['searchtxt']);
			$filteredQueryBuilder = $filteredQueryBuilder->or_like('t_eksternal.kode_eksternal', $params['searchtxt']);
			$filteredQueryBuilder = $filteredQueryBuilder->or_like('m_material.stock_name', $params['searchtxt']);
			$filteredQueryBuilder = $filteredQueryBuilder->or_like('t_purchase_order.no_po', $params['searchtxt']);
			$filteredQueryBuilder = $filteredQueryBuilder->or_like('t_purchase_order.date_po', $params['searchtxt']);
			$filteredQueryBuilder = $filteredQueryBuilder->or_like('t_invoice_pembelian.no_invoice', $params['searchtxt']);
			$filteredQueryBuilder = $filteredQueryBuilder->or_like('t_payment_hp.date', $params['searchtxt']);
		}

		$filteredResult = $filteredQueryBuilder->get()->result_array();

		// paginated result
		$paginatedQueryBuilder = $this->db
			->select('*, t_payment_hp.date AS `t_payment_hp.date`')
			->from('t_purchase_order')
			->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id','LEFT')
			->join('t_po_mat', 't_purchase_order.id_po = t_po_mat.id_po','LEFT')
			->join('m_material', 't_po_mat.id_material = m_material.id_mat','LEFT')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom','LEFT')
			->join('t_bpb_detail', 't_po_mat.id_t_ps = t_bpb_detail.id_po_mat','LEFT')
			->join('t_invoice_pembelian', 't_bpb_detail.id_bpb = t_invoice_pembelian.id_bpb','LEFT')
			->join('d_payment_hp', 't_invoice_pembelian.id_invoice = d_payment_hp.id_invoice','LEFT')
			->join('t_payment_hp', 'd_payment_hp.id_hp = t_payment_hp.id_hp','LEFT')
			// 
		;

		if (isset($params['principle_id']) && !empty($params['principle_id'])) {
			$paginatedQueryBuilder = $paginatedQueryBuilder->where(['t_purchase_order.id_distributor' => $params['principle_id']]);
		}

		if (isset($params['searchtxt']) && !empty($params['searchtxt'])) {
			$paginatedQueryBuilder = $paginatedQueryBuilder->like('t_eksternal.name_eksternal', $params['searchtxt']);
			$paginatedQueryBuilder = $paginatedQueryBuilder->or_like('t_eksternal.kode_eksternal', $params['searchtxt']);
			$paginatedQueryBuilder = $paginatedQueryBuilder->or_like('m_material.stock_name', $params['searchtxt']);
			$paginatedQueryBuilder = $paginatedQueryBuilder->or_like('t_purchase_order.no_po', $params['searchtxt']);
			$paginatedQueryBuilder = $paginatedQueryBuilder->or_like('t_purchase_order.date_po', $params['searchtxt']);
			$paginatedQueryBuilder = $paginatedQueryBuilder->or_like('t_invoice_pembelian.no_invoice', $params['searchtxt']);
			$paginatedQueryBuilder = $paginatedQueryBuilder->or_like('t_payment_hp.date', $params['searchtxt']);
		}
		if (isset($params['limit']) && $params['limit'] >= 0 && isset($params['offset'])) {
			$paginatedQueryBuilder = $paginatedQueryBuilder->limit($params['limit'], $params['offset']);
		}

		$paginatedResult = $paginatedQueryBuilder->get()->result_array();

		return [
			'data' => $paginatedResult,
			'total_filtered' => count($filteredResult),
			'total' => count($total),
		];
	}

	/**
	 * get list of principle
	 * 
	 * @return array
	 */
	public function get_principle_list()
	{
		$queryBuilder = $this->db
			->select('*')
			->from('t_eksternal');

		$result = $queryBuilder->get()->result_array();

		return $result;
	}
}
