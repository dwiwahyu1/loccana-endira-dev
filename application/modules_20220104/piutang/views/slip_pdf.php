<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style>
        body {
            margin-right: 100px;
            font-family: "Times New Roman", Times, serif;
        }

        .paper {
            width: 100%;
            page-break-after: always;
            /* border: 1px solid blue; */
        }

        .paper:last-child {
            page-break-after: avoid;
        }

        .subtitle {
            font-size: 24px;
        }

        .subtitle2 {
            font-size: 21px;
        }

        .content-table tr>td {
            padding-left: 5px;
            padding-right: 5px;
            padding-top: 5px;
            padding-bottom: 5px;
            font-size: smaller;
        }

        thead>tr>td {
            font-weight: bolder;
        }

        .header-table tr td {
            font-size: smaller;
        }

        .sign-placeholder {
            font-size: smaller;
        }

    </style>
</head>

<body>
    <div class="paper" style="width: 100%;margin-top:-10px">
        <div class="row" style="height: 170px;">
            <table class="header-table" table border="0.0px" style="width: 55%; float: left; border-collapse:collapse;">
                <tr>
                    <td colspan="3"><strong><span class="subtitle">PT. ENDIRA ALDA</span></strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr class="subtitle2">
                    <td>JL. Sangkuriang No.38-A</td>
                </tr>
                <tr class="subtitle2">
                    <td>Cimahi - 40511</td>
                </tr>
                <tr class="subtitle2">
                    <td>NPWP: 01.555.161.7.428.000</td>
                </tr>
            </table>

            <table class="header-table" border="0.0px" style="width: 45%; float: left; border-collapse:collapse;">
                <tr>
                    <td colspan="3"><strong><span class="subtitle">SLIP KAS / BANK MASUK</span></strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr class="subtitle2">
                    <td>Tanggal</td>
                    <td>:</td>
                    <td><?= date('d F Y', strtotime($date_detail['tgl_terbit'])) ?? ''; ?></td>
                </tr>
                <tr class="subtitle2">
                    <td>Transaksi</td>
                    <td>:</td>
                    <td><?= 'R' . ($date_detail['no_payment'] ?? ''); ?></td>
                </tr>
                <tr class="subtitle2">
                    <td>Ref</td>
                    <td>:</td>
                    <td><?= $date_detail['no_payment'] ?? ''; ?></td>
                </tr>
                <tr class="subtitle2">
                    <td>Customer</td>
                    <td>:</td>
                    <td><?= $cust_by_key[$cust['id_customer']]['cust_name'] ?? ''; ?></td>
                </tr>
                <tr class="subtitle2">
                    <td>Currency</td>
                    <td>:</td>
                    <td><?= 'Rp.   : Kurs : 1'; ?></td>
                </tr>
            </table>
        </div><br>

        <div class="row" style="margin-top:0">
            <span style="font-weight:bolder; font-size:smaller;">Pembayaran</span>
            <table class="content-table" style="width: 98%; border-collapse:collapse; font-size: 20px; ">
                <thead>
                    <tr>
                        <td style="border: 1px solid black; border-width: 1px 0px 1px 1px;">CH/BG/CASH/NC</td>
                        <td style="border: 1px solid black; border-width: 1px 0px;">Nama Bank</td>
                        <td style="border: 1px solid black; border-width: 1px 0px;">Jatuh Tempo</td>
                        <td style="border: 1px solid black; border-width: 1px 0px;">Nilai</td>
                        <td style="border: 1px solid black; border-width: 1px 0px;">Total</td>
                        <td style="border: 1px solid black; border-width: 1px 1px 1px 0px;">L/R Kurs</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="border: 1px solid black; border-width: 1px 0px 1px 1px;"><?= $date_detail['account_type'] ?? ''; ?></td>
                        <td style="border: 1px solid black; border-width: 1px 0px;"><?= $cash_account_by_key[$date_detail['cash_account']]['keterangan'] ?? ''; ?></td>
                        <td style="border: 1px solid black; border-width: 1px 0px; text-align:center;"><?= $date_detail['tgl_jatuh_tempo'] ?? ''; ?></td>
                        <td style="border: 1px solid black; border-width: 1px 0px; text-align:right;"><?= number_format($date_detail['total_amount'] ?? 0, 2, ',', '.') ?? ''; ?></td>
                        <td style="border: 1px solid black; border-width: 1px 0px; text-align:right;"><?= number_format($date_detail['total_amount'] ?? 0, 2, ',', '.') ?? ''; ?></td>
                        <td style="border: 1px solid black; border-width: 1px 1px 1px 0px;"><?= ''; ?></td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid black; border-width: 1px 0px 1px 1px; text-align:right; font-weight:bolder;" colspan="3"><?= 'Total Pembayaran'; ?></td>
                        <td style="border: 1px solid black; border-width: 1px 0px; text-align:right; font-weight:bolder;"><?= number_format($date_detail['total_amount'] ?? 0, 2, ',', '.') ?? ''; ?></td>
                        <td style="border: 1px solid black; border-width: 1px 0px; text-align:right; font-weight:bolder;"><?= number_format($date_detail['total_amount'] ?? 0, 2, ',', '.') ?? ''; ?></td>
                        <td style="border: 1px solid black; border-width: 1px 1px 1px 0px;"><?= ''; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="row" style="margin-top:20px">
            <span style="font-weight:bolder; font-size:smaller;">Penjualan</span>
            <table class="content-table" style="width: 98%; border-collapse:collapse; font-size: 20px; ">
                <thead>
                    <tr>
                        <td style="border: 1px solid black; border-width: 1px 0px 1px 1px;">Invoice</td>
                        <td style="border: 1px solid black; border-width: 1px 0px;">Currency</td>
                        <td style="border: 1px solid black; border-width: 1px 0px;">Kurs</td>
                        <td style="border: 1px solid black; border-width: 1px 0px;">Jatuh Tempo</td>
                        <td style="border: 1px solid black; border-width: 1px 0px;">Nilai Kewajiban</td>
                        <td style="border: 1px solid black; border-width: 1px 1px 1px 0px;">Nilai Terbayar</td>
                    </tr>
                </thead>
                <tbody>
                    <?php $total = 0; ?>
                    <?php foreach (($date_detail_item ?? []) as $the_item) : ?>
                        <tr>
                            <td style="border: 1px solid black; border-width: 1px 0px 1px 1px;"><?= $the_item['no_invoice'] ?? ''; ?></td>
                            <td style="border: 1px solid black; border-width: 1px 0px; text-align:center;"><?= 'Rp.'; ?></td>
                            <td style="border: 1px solid black; border-width: 1px 0px; text-align:center;"><?= '1'; ?></td>
                            <td style="border: 1px solid black; border-width: 1px 0px; text-align:center;"><?= $the_item['tgl_jatuh_tempo'] ?? ''; ?></td>
                            <td style="border: 1px solid black; border-width: 1px 0px; text-align:right;"><?= number_format($the_item['ammount'], 2, ',', '.') ?? ''; ?></td>
                            <td style="border: 1px solid black; border-width: 1px 1px 1px 0px; text-align:right;"><?= number_format($the_item['ammount'], 2, ',', '.') ?? ''; ?></td>
                        </tr>
                        <?php $total += $the_item['ammount']; ?>
                    <?php endforeach; ?>
                    <tr>
                        <td style="text-align:right; font-weight:bolder;" colspan="5">Total:</td>
                        <td style="text-align:right; font-weight:bolder;"><?= number_format($total, 2, ',', '.'); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="row" style="margin-top:10px">
            <table style="width: 98%; border-collapse:collapse; font-size: 20px;">
                <tr>
                    <td class="sign-placeholder" style="text-align:center;"><span>Mengetahui,</span><br><br><br><span>__________</span></td>
                    <td class="sign-placeholder" style="text-align:center;"><span>Membayar,</span><br><br><br><span>__________</span></td>
                    <td class="sign-placeholder" style="text-align:center;"><span>Menerima,</span><br><br><br><span>__________</span></td>
                    <td class="sign-placeholder" style="text-align:center;"><span>Verifikasi,</span><br><br><br><span>__________</span></td>
                </tr>
            </table>
        </div>

        <div class="row" style="margin-top: 15px;">
            <span style="font-size: smaller;">Tanggal Cetak: <?= date('d F Y'); ?></span>
        </div>
    </div><!-- paper -->
</body>

</html>