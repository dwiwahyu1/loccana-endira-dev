<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto;
  }

  .scroll-overflow {
    min-height: 650px;
  }

  #modal-distributor::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }

  table thead {
    position: sticky;
  }

</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
          <div class="card-box table-responsive">
            <div style="display: flex; justify-content:space-between; align-items:center; margin-bottom: 12px;">
              <h4 class="header-title" style="margin:0;">Report Cash</h4>
            </div>
            <form id="report-cash-filter-form" action="<?= base_url('report_pembelian/export_excel'); ?>" class="add-department" method="post" target="_blank" autocomplete="off">
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label>Akun</label>
                    <select name="coa_id" id="coa_id" class="form-control">
                      <option value="0" selected>Semua Akun</option>
                      <?php foreach ($coa_list as $coa) : ?>
                        <option value="<?= $coa['id_coa']; ?>"><?= implode(' - ', [$coa['coa'], $coa['keterangan']]); ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="start_date">Start Date</label>
                    <input class="form-control" type="text" name="start_date" id="start_date">
                  </div>
                  <div class="form-group">
                    <label for="end_date">End Date</label>
                    <input class="form-control" type="text" name="end_date" id="end_date">
                  </div>
                </div>
              </div>
              <div class="text-center row" style="margin-top: 24px; margin-bottom: 24px;">
                <button class="btn btn-primary submit-button" type="submit">Submit</button>
                <button class="btn btn-primary m-t-4" onclick="print_excel()">Export</button>
              </div>
            </form>
            <table id="datatable_pricipal" class="table table-striped table-bordered m-t-4">
              <thead>
                <tr>
                  <th width="15%">Tanggal</th>
                  <th>Kode</th>
                  <th>Akun</th>
                  <th>Uraian</th>
                  <th>No.Faktur</th>
                  <th>Debit</th>
                  <th>Kredit</th>
                  <th>Saldo</th>
                  <!-- <th>Action</th> -->
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div><!-- end col -->
      </div>
    </div>
  </div>
  <span id="laod"></span>
</div>

<script>
  // $('table#datatable_pricipal').floatThead({
  //   top: 100
  // });

  $('select').select2();

  $('#start_date, #end_date').datepicker({
    format: "yyyy-mm-dd",
    autoclose: true,
    todayHighlight: true
  });

  function load_list() {
    var user_id = '0001';
    var token = '093940349';

    var params = [
      'sess_user_id=' + user_id,
      'sess_token=' + token,
      'coa_id=' + $('#coa_id').val() || '',
      'start_date=' + $('#start_date').val() || '',
      'end_date=' + $('#end_date').val() || '',
    ];

    var params_string = '?' + params.join('&');

    $('#datatable_pricipal').DataTable({
      //"dom": 'rtip',
      "bFilter": false,
      "aaSorting": [],
      "bLengthChange": true,
      'iDisplayLength': 10,
      "sPaginationType": "simple_numbers",
      "info": false,
      "processing": true,
      "serverSide": true,
      "destroy": true,
      "ajax": "<?= base_url('report_cash/list'); ?>" + params_string,
      "searching": false,
      "language": {
        "decimal": ",",
        "thousands": "."
      },
      "dom": 'l<"toolbar">frtip',
      "columnDefs": [{
          className: 'text-center',
          targets: [0, 2, 4, 5]
        },
        {
          className: 'text-right',
          targets: [-1, -2, -3]
        }
      ],
      "paging": false,
      "ordering": false,
      "scrollY": "300px",
      "initComplete": function() {
        $('.submit-button').attr('disabled', false);
      }
    });
  }

  function print_excel() {
    var url = '<?= base_url('report_cash/excel'); ?>';
    var coa_id = $('#coa_id').val();
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var form = $("<form action='" + url + "' method='post' target='_blank'><input type='hidden' name='coa_id' value='" + coa_id + "' /><input type='hidden' name='start_date' value='" + start_date + "' /><input type='hidden' name='end_date' value='" + end_date + "' /></form>");
    $('body').append(form);
    form.submit();
  }

  $(document).ready(function() {
    $('#report-cash-filter-form').on('submit', function(e) {
      e.preventDefault();
      $('.submit-button').attr('disabled', true);
      load_list();
    });
  });
</script>