<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Uom_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function get_uom_data($id) {
		//$sql 	= 'CALL uom_search_id(?)';
		$sql 	= '
		select a.*,b.coa,b.keterangan,c.id_external FROM t_coa_value a 
		join t_coa b on a.id_coa = b.id_coa
		left join t_jurnal_trans c on a.id = c.coa_value
		where a.id = ?';

		$out = array();
		$query 	=  $this->db->query(
			$sql,
			array(
				$id
			)
		);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_detail($data) {
		//$sql 	= 'CALL uom_search_id(?)';
		$sql 	= '
		SELECT a.*,b.`id_coa` AS coa_kr, b.`value_real` AS value_real_kr, b.`note` AS note_kr, b.`id_coa_temp` AS id_dist,c.type_jurnal
		FROM `t_coa_value` a
		LEFT JOIN `t_coa_value` b ON a.`id` = b.`id_parent`
		join t_coa c on a.id_coa = c.id_coa
		join t_coa d on b.id_coa =d.id_coa
		WHERE a.`id` = ?
		and d.keterangan not like "%Penyusutan%"';

		$out = array();
		$query 	=  $this->db->query(
			$sql,
			array(
				$data['id']
			)
		);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_principle() {
		//$sql 	= 'CALL uom_search_id(?)';
		$sql 	= 'select * from t_eksternal order by kode_eksternal';

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_cash_acc(){
		
		//$sql 	= 'select * from t_coa where type_coa in (0,1,3) order by coa';
		$sql 	= 'select * from t_coa #where type_coa in (3) and sts_show = 1 order by coa';

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
		
	}
	
	public function get_cash_acc_cr(){
		
		//$sql 	= 'select * from t_coa where type_coa = 1 order by coa';
		$sql 	= 'select * from t_coa where sts_show = 1  order by coa';

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
		
	}


	public function get_coa() {

		//$sql 	= 'select * from t_coa where type_coa in (0,2,3) order by coa';
		$sql 	= 'select * from t_coa  order by coa';

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_id_trans() {

		$sql 	= 'select * from t_coa where type_coa in (2,0) order by coa';

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function lists($params = array()) {

		$query_s = ' ';

		// }else{

		// $query_s = ' AND a.payment_status = '.$params['filter'];
		// }

		if ($params['filter_month'] == 'ALL') {
			$query_m = ' ';
		} else {
			$query_m = ' AND DATE_FORMAT(a.date,"%M") = "' . $params['filter_month'] . '"';
		}

		if ($params['filter_year'] == 'ALL') {
			$query_y = ' ';
		} else {
			$query_y = 'AND DATE_FORMAT(a.date,"%Y") = "' . $params['filter_year'] . '" ';
		}

		$where_sts = $query_s . ' ' . $query_m . ' ' . $query_y;

		$query = 	'
				SELECT COUNT(*) AS jumlah FROM 
				(
					select a.*,group_concat(concat(b.coa," ",b.keterangan) separator "<br>") as coa_k from 
						(
							select a.*,b.coa, b.keterangan from `t_coa_value` a
							join t_coa b on a.id_coa = b.id_coa
							where `adjusment` = 1 and a.id_parent = 0 ' . $where_sts . '
							and ( 
								b.coa LIKE "%' . $params['searchtxt'] . '%" OR
								a.note LIKE "%' . $params['searchtxt'] . '%" OR
								b.keterangan LIKE "%' . $params['searchtxt'] . '%" 
								)
						) a join
						(
							select a.*,b.coa, b.keterangan 
							from `t_coa_value` a
							join t_coa b on a.id_coa = b.id_coa
							where `adjusment` = 1 and a.id_parent <> 0 ' . $where_sts . '
							and ( 
								b.coa LIKE "%' . $params['searchtxt'] . '%" OR
								a.note LIKE "%' . $params['searchtxt'] . '%" OR
								b.keterangan LIKE "%' . $params['searchtxt'] . '%" )
							and b.keterangan not like "%Penyusutan%"
						) b on a.id = b.id_parent
						group by a.id
				) d
			';

		// $query2 = 	'
		// SELECT z.*, rank() over ( ORDER BY sss ) AS Rangking from ( 
		// select a.*,b.coa,b.keterangan,b.id_coa as sss 
		// FROM t_coa_value a join t_coa b on a.id_coa = b.id_coa
		// where a.id_parent = 0 '.$where_sts.' AND a.id_coa and adjustment = 1
		// AND a.type_cash = 0 AND  (id_coa_temp is null or id_coa_temp = 0) AND bukti  IS NULL AND ( 
		// coa LIKE "%'.$params['searchtxt'].'%" OR
		// keterangan LIKE "%'.$params['searchtxt'].'%" 
		// )  order by sss) z
		// ORDER BY `sss` ASC

		// LIMIT '.$params['limit'].' 
		// OFFSET '.$params['offset'].' 
		// ';

		$query2 = 	'
					select a.*,group_concat(concat(b.coa," ",b.keterangan) separator "<br>") as coa_k from 
					(
						select a.*,b.coa, b.keterangan from `t_coa_value` a
						join t_coa b on a.id_coa = b.id_coa
						where `adjusment` = 1 and a.id_parent = 0 ' . $where_sts . '
						and (
							b.coa LIKE "%' . $params['searchtxt'] . '%" OR
							a.note LIKE "%' . $params['searchtxt'] . '%" OR
							b.keterangan LIKE "%' . $params['searchtxt'] . '%" 
						)
					) a join
					(
						select a.*,b.coa, b.keterangan from `t_coa_value` a
						join t_coa b on a.id_coa = b.id_coa
						where `adjusment` = 1 and a.id_parent <> 0 ' . $where_sts . '
						and (
							b.coa LIKE "%' . $params['searchtxt'] . '%" OR
							a.note LIKE "%' . $params['searchtxt'] . '%" OR
							b.keterangan LIKE "%' . $params['searchtxt'] . '%" 
							)
						and b.keterangan not like "%Penyusutan%"
					) b on a.id = b.id_parent
					group by a.id
				ORDER BY a.`date` desc
				
				LIMIT ' . $params['limit'] . ' 
				OFFSET ' . $params['offset'] . ' 
			';

		//echo $query2;die;

		$out		= array();
		$querys		= $this->db->query($query);
		$result = $querys->row();

		$total_filtered = $result->jumlah;
		$total 			= $result->jumlah;

		if (($params['offset'] + 10) > $total_filtered) {
			$limit_data = $total_filtered - $params['offset'];
		} else {
			$limit_data = $params['limit'];
		}



		//echo $query;die;
		//echo $query;die;
		$query2s		= $this->db->query($query2);
		$result2 = $query2s->result_array();
		$return = array(
			'data' => $result2,
			'total_filtered' => $total_filtered,
			'total' => $total,
		);
		return $return;
	}

	public function edit_uom_trans($data) {
		$datas = array(
			'id_external' => $data['prin'],
			'tipe_trans' => $data['cash'],
			'ammount' => $data['jumlah'],
			'tgl_terbit' => $data['tgl']
		);

		//	print_r($datas);die;
		$this->db->where('id', $data['id']);
		$this->db->update('coa_value', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_uom($data) {
		$datas = array(
			'id_coa' => $data['coa'],
			'date' => $data['tgl'],
			'date_insert' => date('Y-m-d H:i:s'),
			'value' => $data['jumlah'],
			'value_real' => $data['jumlah'],
			'type_cash' => 0,
			'id_valas' => 1,
			'note' => $data['ket']
		);

		//	print_r($datas);die;
		$this->db->where('id', $data['id']);
		$this->db->update('t_coa_value', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_uom($data) {
		$datas = array(
			'id_coa' => $data['coa'],
			'date' => $data['tgl'],
			'date_insert' => date('Y-m-d H:i:s'),
			'value' => $data['jumlah'],
			'value_real' => $data['jumlah'],
			'type_cash' => $data['type_cash'],
			'id_parent' => $data['parent'],
			'adjusment' => 1,
			'id_coa_temp' => $data['id_coa_temp'],
			'id_valas' => 1,
			'note' => $data['ket']
		);

		// echo"<pre>";print_r($datas);die;

		$this->db->insert('t_coa_value', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_trans($data, $id_coa_val) {
		$datas = array(
			'id_external' => $data['prin'],
			'tipe_trans' => 1,
			'ammount' => $data['jumlah'],
			'pph' => $data['pph'],
			'tgl_terbit' => $data['tgl'],
			'coa_value' => $id_coa_val
		);

		//	print_r($datas);die;

		$this->db->insert('t_jurnal_trans', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}


	public function deletes($id_uom) {
		$this->db->where('id', $id_uom);
		$this->db->delete('t_coa_value');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function delete_uom_trans($data) {
		$this->db->where('coa_value', $data['id']);
		$this->db->delete('t_jurnal_trans');

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function delete_detail($id_uom) {
		
		$sql 	= 'DELETE a,b
		from  t_coa_value a
		left join t_coa_value b on a.id = b.id_parent
		WHERE a.`id_parent` = '.$id_uom.'';

		$query 	= $this->db->query($sql);
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}
