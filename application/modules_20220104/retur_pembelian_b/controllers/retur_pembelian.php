<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Retur_pembelian extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('retur_pembelian/retur_pembelian_model', 'returPembelianModel');
		$this->load->library('log_activity');
		$this->load->library('priv');

		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong');
		$this->form_validation->set_message('min_length', '%s Minimal 4 Karakter');
		$this->form_validation->set_message('max_length', '%s Maksimal 10 Karakter');
	}

	public function index()
	{
		$this->template->load('maintemplate', 'retur_pembelian/views/index', [
			'priv' => $this->priv->get_priv()
		]);
	}

	public function lists()
	{
		$sess_user_id = !empty($_GET['sess_user_id']) ? $_GET['sess_user_id'] : NULL;
		$sess_token = !empty($_GET['sess_token']) ? $_GET['sess_token'] : NULL;

		$draw = ($this->input->get_post('draw') != FALSE) ? $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;

		$order = $this->input->get_post('order');
		$order_dir = !empty($order[0]['dir']) ? $order[0]['dir'] : 'desc';
		$order_column = !empty($order[0]['column']) ? $order[0]['column'] : 0;
		$order_fields = array('kode_eksternal'); // , 'COST'

		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['sess_user_id'] = $sess_user_id;
		$params['sess_token'] 	= $sess_token;
		$params['searchtxt'] 	= $_GET['search']['value'];

		// print_r($params);die;
		$priv = $this->priv->get_priv();

		$list = $this->returPembelianModel->get_list_retur_pembelian($params);

		$data = array();
		foreach ($list['data'] as $row) {

			$action = $this->load->view('../modules/retur_pembelian/views/partials/table_action', ['returPembelian' => $row], true);
			$status = $this->load->view('../modules/retur_pembelian/views/partials/table_status', ['returPembelian' => $row], true);

			array_push($data, [
				$row['no_invoice'],
				$row['principle'],
				$row['date'],
				$row['pic_name'],
				$status,
				$action,
			]);
		}

		$result["data"] = $data;
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add()
	{
		// $params['region'] = 4;
		
		$principles = $this->returPembelianModel->get_principles();
		// $list_pembelian = $this->returPembelianModel->get_list_pembelian();
		$list_invoice_pembelian = $this->returPembelianModel->get_list_invoice_pembelian();
		// echo '<pre>'; print_r($list_invoice_pembelian); die;

		$data = array(
			'principles' => $principles,
			// 'list_pembelian' => $list_pembelian,
			'list_invoice_pembelian' => $list_invoice_pembelian,
		);

		$this->template->load('maintemplate', 'retur_pembelian/views/addReturPembelian', $data);
	}

	public function add_retur_pembelian()
	{
		$this->form_validation->set_rules('id_invoice', 'Invoice', 'required');
		// $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

		if ($this->form_validation->run() == false) {
			$msg = validation_errors();
			$result = array('success' => false, 'message' => $msg);
		} else {

			try {
				// $this->db->trans_begin();

				$idInvoice = $this->input->post('id_invoice');

				$params = [
					'id_invoice' => $idInvoice,
					'keterangan_retur' => anti_sql_injection($this->input->post('keterangan_retur')),
					'retur_date' => date('Y-m-d'),
					'pic' => $this->session->userdata['logged_in']['user_id'] ?? null,
				];

				// var_dump($params);
				// die;

				$insertResult = $this->returPembelianModel->add_retur_pembelian($params);

				$totalItems = $this->input->post('total_items') ?? 0;

				for ($idx = 0; $idx < $totalItems; $idx++) {
					$idPoMat = $this->input->post('id_t_ps_' . $idx);
					$qtyRetur = $this->input->post('qty_retur_' . $idx);

					$detailParams = [
						'id_retur_pembelian' => $insertResult['lastid'] ?? null,//v
						'id_pembelian' => $idInvoice,//x
						'id_po_mat' => $idPoMat,//v
						'retur_qty' => $qtyRetur,//v
					];

					$this->returPembelianModel->add_detail_retur_pembelian($detailParams);
				}

				$msg = 'Berhasil Menambah Data Retur Pembelian';
				// $this->db->trans_commit();
				$success = true;
			} catch (\Throwable $th) {
				// $this->db->trans_rollback();
				$msg = 'Gagal Menambah Data Retur Pembelian';
				$success = false;
			}

			$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
			$result = array('success' => $success, 'message' => $msg);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function get_detail_pembelian()
	{
		$params = json_decode(file_get_contents("php://input"), true);
		$pembelian = ($params['id_invoice'] ?? false) ? $this->returPembelianModel->get_detail_pembelian_by_invoice($params['id_invoice']) : [];
		$pembelian_items = ($params['id_invoice'] ?? false) ? $this->returPembelianModel->get_detail_pembelian_items_by_invoice($params['id_invoice']) : [];

		$items = '';
		foreach ($pembelian_items as $key => $item) {
			// var_dump($item, $key);
			// die;
			$template = $this->load->view('../modules/retur_pembelian/views/partials/items_row', ['item' => $item, 'index' => $key], true);
			$items .= $template;
		}

		// var_dump($items);
		// die;

		$this->output->set_content_type('application/json')->set_output(json_encode([
			'data' => [
				'pembelian' => $pembelian,
				'items' => $items,
				'total_items' => count($pembelian_items),
			],
		]));
	}

	public function detail()
	{
		$idReturPembelian = anti_sql_injection($this->input->post('id_retur_pembelian', TRUE));

		$returPembelian = $this->returPembelianModel->get_retur_pembelian($idReturPembelian);
		$detailReturPembelian = $this->returPembelianModel->get_detail_retur_pembelian($idReturPembelian);

		// echo '<pre>'; print_r($detailReturPembelian); die;

		$this->template->load('maintemplate', 'retur_pembelian/views/detailReturPembelian', [
			'returPembelian' => $returPembelian,
			'detailReturPembelian' => $detailReturPembelian,
		]);
	}

	public function edit()
	{
		// $params['region'] = 4;
		$idReturPembelian = anti_sql_injection($this->input->post('id_retur_pembelian', TRUE));
		
		$returPembelian = $this->returPembelianModel->get_retur_pembelian($idReturPembelian);
		// echo '<pre>'; print_r($returPembelian); die;
		$list_invoice_pembelian = $this->returPembelianModel->get_list_invoice_pembelian($returPembelian['id_invoice']);

		$detailReturPembelian = $this->returPembelianModel->get_detail_retur_pembelian($idReturPembelian);

		$data = array(
			'list_invoice_pembelian' => $list_invoice_pembelian,
			'returPembelian' => $returPembelian,
			'detailReturPembelian' => $detailReturPembelian,
		);

		$this->template->load('maintemplate', 'retur_pembelian/views/editReturPembelian', $data);
	}

	public function update_retur_pembelian()
	{
		$this->form_validation->set_rules('id_retur_pembelian', 'Retur Pembelian', 'required');
		$this->form_validation->set_rules('id_invoice', 'Invoice', 'required');
		// $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');

		if ($this->form_validation->run() == false) {
			$msg = validation_errors();
			$result = array('success' => false, 'message' => $msg);
		} else {

			try {
				// $this->db->trans_begin();

				$idReturPembelian = $this->input->post('id_retur_pembelian');
				$idInvoice = $this->input->post('id_invoice');

				$params = [
					'id_invoice' => $idInvoice,
					'keterangan_retur' => anti_sql_injection($this->input->post('keterangan_retur')),
					'retur_date' => date('Y-m-d'),
					'status' => 0,
					'pic' => $this->session->userdata['logged_in']['user_id'] ?? null,
				];

				// var_dump($params);
				// die;

				$this->returPembelianModel->update_retur_pembelian($idReturPembelian, $params);

				$totalItems = $this->input->post('total_items') ?? 0;

				if ($totalItems > 0) {
					// clear old detail retur pembelian
					$this->returPembelianModel->clear_detail_retur_pembelian($idReturPembelian);
				}

				for ($idx = 0; $idx < $totalItems; $idx++) {
					$idPoMat = $this->input->post('id_t_ps_' . $idx);
					$qtyRetur = $this->input->post('qty_retur_' . $idx);

					$detailParams = [
						'id_retur_pembelian' => $idReturPembelian,
						'id_pembelian' => $idInvoice,
						'id_po_mat' => $idPoMat,
						'retur_qty' => $qtyRetur,
					];

					$this->returPembelianModel->add_detail_retur_pembelian($detailParams);
				}


				$msg = 'Berhasil Menambah Data Retur Pembelian';
				// $this->db->trans_commit();
				$success = true;
			} catch (\Throwable $th) {
				// $this->db->trans_rollback();
				$msg = 'Gagal Menambah Data Retur Pembelian';
				$success = false;
			}

			$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
			$result = array('success' => $success, 'message' => $msg);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function konfirmasi()
	{
		$idReturPembelian = anti_sql_injection($this->input->post('id_retur_pembelian', TRUE));
		$returPembelian = $this->returPembelianModel->get_retur_pembelian($idReturPembelian);
		$detailReturPembelian = $this->returPembelianModel->get_detail_retur_pembelian($idReturPembelian);
		// $pembelian = ($returPembelian['id_pembelian'] ?? false) ? $this->returPembelianModel->get_detail_pembelian($returPembelian['id_pembelian']) : [];
		// $pembelian_items = ($returPembelian['id_pembelian'] ?? false) ? $this->returPembelianModel->get_detail_pembelian_items($returPembelian['id_pembelian']) : [];

		$this->template->load('maintemplate', 'retur_pembelian/views/konfirmasiReturPembelian', [
			'returPembelian' => $returPembelian,
			'detailReturPembelian' => $detailReturPembelian,
			// 'pembelian' => $pembelian,
			// 'pembelian_items' => $pembelian_items,
		]);
	}

	public function approve_retur_pembelian()
	{
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$this->returPembelianModel->update_retur_pembelian($params['id'], [
			'status' => $params['status'],
			'approval_date' => $params['status'] == 1 ? date('Y-m-d') : null,
		]);
		
		if($params['status'] == 1){
			
			//$sss = $this->returPembelianModel->update_pomat($params['id']);
			
			
			$sss = $this->returPembelianModel->update_qty($params['id']);
			$sss = $this->returPembelianModel->insert_mutasi($params['id']);
			//$sss = $this->returPembelianModel->insert_coa_value($params['id']);
			
		}else{
			
			
			
		}

		$msg = $params['status'] == 1 ? 'Berhasil Menyetujui data Retur' : 'Berhasil Menolak data Retur';
		$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
		$result = array('success' => true, 'message' => $msg);
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	// public function test()
	// {
	// 	echo '<pre>'; print_r($this->returPembelianModel->get_list_retur_pembelian_with_invoice()); die;
	// }
}
