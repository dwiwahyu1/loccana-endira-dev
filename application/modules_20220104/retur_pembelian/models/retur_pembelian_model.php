<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Retur_pembelian_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function get_principles($params = array()) {
		$this->db->select('*')
			->from('t_eksternal')
			->order_by('name_eksternal', 'asc');

		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_list_pembelian($idPembelianForUpdate = null) {
		$queryBuilder = $this->db
			->select('
				purchase_orders.no_po as no_pembelian,
				purchase_orders.id_po as id_pembelian,
				principles.name_eksternal as principle
			')
			->from('t_purchase_order AS purchase_orders')
			->join('t_eksternal AS principles', 'purchase_orders.id_distributor = principles.id');

		if ($idPembelianForUpdate) {
			$subQuery = "SELECT DISTINCT id_pembelian FROM t_retur_pembelian WHERE t_retur_pembelian.id_pembelian != " . $idPembelianForUpdate;
		} else {
			$subQuery = "SELECT DISTINCT id_pembelian FROM t_retur_pembelian";
		}

		$queryBuilder = $queryBuilder->where('purchase_orders.id_po NOT IN (' . $subQuery . ')',  NULL, FALSE);

		$result = $queryBuilder->get()->result_array();
		return $result;
	}

	public function get_list_invoice_pembelian($idInvoiceForUpdate = null) {
		$queryBuilder = $this->db
			->select('*')
			->from('t_invoice_pembelian')
			->join('t_bpb', 't_bpb.id_bpb = t_invoice_pembelian.id_bpb')
			->join('t_purchase_order', 't_bpb.id_po = t_purchase_order.id_po')
			->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id')
			->order_by('t_invoice_pembelian.no_invoice', 'ASC');

		$result = $queryBuilder->get()->result_array();

		if ($idInvoiceForUpdate) {
			$subQuery = "SELECT DISTINCT id_invoice FROM t_retur_pembelian WHERE t_retur_pembelian.id_invoice != " . $idInvoiceForUpdate;
		} else {
			$subQuery = "SELECT DISTINCT id_invoice FROM t_retur_pembelian";
		}

		$queryBuilder = $queryBuilder->where('t_invoice_pembelian.id_invoice NOT IN (' . $subQuery . ')',  NULL, FALSE);

		// echo '<pre>'; $this->db->last_query(); die; // for dumping the sql query string

		return $result;
	}

	public function get_list_retur_pembelian($params = array()) {
		$queryBuilder = $this->db
			->select('
				t_retur_pembelian.id_retur_pembelian AS id_retur_pembelian,
				purchase_orders.no_po AS no_po,
				t_invoice_pembelian.no_invoice AS no_invoice,
				principles.name_eksternal AS principle,
				t_retur_pembelian.retur_date AS date,
				t_retur_pembelian.status AS status,
				users.nama AS pic_name
			')
			->from('t_retur_pembelian')
			->join('t_invoice_pembelian', 't_retur_pembelian.id_invoice = t_invoice_pembelian.id_invoice')
			->join('t_bpb', 't_invoice_pembelian.id_bpb = t_bpb.id_bpb')
			// ->join('t_bpb_detail', 't_bpb.id_bpb = t_bpb_detail.id_bpb')
			->join('t_purchase_order AS purchase_orders', 't_bpb.id_po = purchase_orders.id_po')
			->join('t_eksternal AS principles', 'purchase_orders.id_distributor = principles.id')
			->join('u_user AS users', 't_retur_pembelian.pic = users.id', 'left');

		if (isset($params['searchtxt']) && !empty($params['searchtxt'])) {
			$queryBuilder = $queryBuilder->like('purchase_orders.no_po', $params['searchtxt']);
			$queryBuilder = $queryBuilder->or_like('principles.name_eksternal', $params['searchtxt']);
			$queryBuilder = $queryBuilder->or_like('retur_pembelian.retur_date', $params['searchtxt']);
			$queryBuilder = $queryBuilder->or_like('users.nama', $params['searchtxt']);
		}

		$result = $queryBuilder->get()->result_array();

		return [
			'data' => $result,
			'total' => count($result),
			'total_filtered' => count($result),
		];
	}

	public function get_retur_pembelian($id_retur_pembelian) {
		$queryBuilder = $this->db
			->select('*')
			->from('t_retur_pembelian')
			->join('t_invoice_pembelian', 't_retur_pembelian.id_invoice = t_invoice_pembelian.id_invoice')
			->join('t_bpb', 't_invoice_pembelian.id_bpb = t_bpb.id_bpb')
			->join('t_purchase_order', 't_purchase_order.id_po = t_bpb.id_po')
			->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id')
			->where([
				't_retur_pembelian.id_retur_pembelian' => $id_retur_pembelian,
			]);

		return $queryBuilder->get()->row_array();
	}

	public function get_detail_retur_pembelian($id_retur_pembelian) {
		$queryBuilder = $this->db->select('
				*,
				d_invoice_pembelian.qty as qty,
				d_invoice_pembelian.unit_price as unit_price
			')
			->from('t_detail_retur_pembelian')
			->join('t_invoice_pembelian', 't_detail_retur_pembelian.id_pembelian = t_invoice_pembelian.id_invoice')
			->join('t_po_mat', 't_detail_retur_pembelian.id_po_mat = t_po_mat.id_t_ps')
			->join('t_bpb_detail', 't_po_mat.id_t_ps = t_bpb_detail.id_po_mat')
			->join('d_invoice_pembelian', 't_bpb_detail.id_bpb_detail = d_invoice_pembelian.id_bpb_detail')
			->join('m_material', 't_po_mat.id_material = m_material.id_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->group_by('t_detail_retur_pembelian.id_detail_retur_pembelian')
			->where('t_detail_retur_pembelian.id_retur_pembelian', $id_retur_pembelian);

		$result = $queryBuilder->get()->result_array();
		// echo '<pre>'; print_r( $this->db->last_query()); die; // for dumping the sql query string

		return $result;
	}

	public function get_detail_pembelian_by_invoice($id_invoice) {
		$queryBuilder = $this->db
			->select('
				purchase_orders.id_po AS purchase_id,
				purchase_orders.no_po AS purchase_number,
				purchase_orders.date_po AS purchase_date,
				purchase_orders.term_of_payment AS purchase_term,
				purchase_orders.keterangan AS purchase_note,
				purchase_orders.term_of_payment AS term_of_payment,
				principles.name_eksternal AS principle,
				principles.eksternal_address AS principle_address,
				principles.phone_1 AS principle_phone,
				principles.email AS principle_email
			')
			->from('t_purchase_order AS purchase_orders')
			->join('t_eksternal AS principles', 'purchase_orders.id_distributor = principles.id')
			->join('t_bpb', 'purchase_orders.id_po = t_bpb.id_po')
			->join('t_invoice_pembelian', 't_bpb.id_bpb = t_invoice_pembelian.id_bpb')
			->where(['t_invoice_pembelian.id_invoice' => $id_invoice]);

		$result = $queryBuilder->get()->row_array();

		// echo '<pre>'; print_r($result); die;

		return $result;
	}

	public function get_detail_pembelian($id_pembelian) {
		$queryBuilder = $this->db
			->select('
				purchase_orders.id_po AS purchase_id,
				purchase_orders.no_po AS purchase_number,
				purchase_orders.date_po AS purchase_date,
				purchase_orders.term_of_payment AS purchase_term,
				purchase_orders.keterangan AS purchase_note,
				purchase_orders.term_of_payment AS term_of_payment,
				principles.name_eksternal AS principle,
				principles.eksternal_address AS principle_address,
				principles.phone_1 AS principle_phone,
				principles.email AS principle_email
			')
			->from('t_purchase_order AS purchase_orders')
			->join('t_eksternal AS principles', 'purchase_orders.id_distributor = principles.id')
			->where(['purchase_orders.id_po' => $id_pembelian]);

		$result = $queryBuilder->get()->row_array();

		return $result;
	}

	public function get_detail_pembelian_items_by_invoice($id_invoice) {
		$result = $this->db
			->select('
				*,
				d_invoice_pembelian.qty as qty
			')
			->from('d_invoice_pembelian')
			->join('t_bpb_detail', 'd_invoice_pembelian.id_bpb_detail = t_bpb_detail.id_bpb_detail')
			->join('t_bpb', 't_bpb_detail.id_bpb = t_bpb.id_bpb')
			->join('t_invoice_pembelian', 't_bpb.id_bpb = t_invoice_pembelian.id_bpb')
			->join('t_po_mat', 't_bpb_detail.id_po_mat = t_po_mat.id_t_ps')
			->join('m_material', 't_po_mat.id_material = m_material.id_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where(['t_invoice_pembelian.id_invoice' => $id_invoice])
			->get()
			->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_detail_pembelian_items($id_pembelian) {
		$result = $this->db
			->select('
				po_mat.id_t_ps AS id_t_ps,
				po_mat.qty AS qty,
				po_mat.unit_price AS unit_price,
				m_material.stock_name AS stock_name,
				m_material.stock_code AS stock_code,
				m_material.base_qty AS base_qty,
				m_material.id_mat AS id_material,
				m_uom.uom_symbol AS uom_symbol
			')
			->from('t_po_mat AS po_mat')
			->join('m_material', 'po_mat.id_material = m_material.id_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where([
				'po_mat.id_po' => $id_pembelian
			])
			->get()
			->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function add_retur_pembelian($params) {
		$this->db->insert('t_retur_pembelian', $params);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}


	public function update_pomat($params) {
		$this->db->insert('t_retur_pembelian', $params);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_detail_retur_pembelian($params) {
		$this->db->insert('t_detail_retur_pembelian', $params);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function update_retur_pembelian($id_retur_pembelian, $params) {
		$queryBuilder = $this->db;

		foreach ($params as $key => $value) {
			$queryBuilder = $queryBuilder->set($key, $value);
		}

		$queryBuilder->where('id_retur_pembelian', $id_retur_pembelian)
			->update('t_retur_pembelian');

		return $this->get_retur_pembelian($id_retur_pembelian);
	}

	public function clear_detail_retur_pembelian($id_retur_pembelian) {
		$this->db->where('id_retur_pembelian', $id_retur_pembelian)
			->delete('t_detail_retur_pembelian');

		return true;
	}

	function update_qty($id) {

		$sql 	= '
			UPDATE m_material a JOIN (
			SELECT c.`id_material`,b.retur_qty AS retur_big,IF(convertion IS NULL,1,convertion) satuan_b, (IF(convertion IS NULL,1,convertion)) / d.`base_qty` AS mult ,
			 (b.retur_qty * ((IF(convertion IS NULL,1,convertion)) / d.`base_qty`) ) AS retur
			FROM `t_retur_pembelian` a
			JOIN `t_detail_retur_pembelian` b ON a.`id_retur_pembelian` = b.`id_retur_pembelian`
			JOIN `t_po_mat` c ON b.`id_po_mat` = c.`id_t_ps`
			JOIN `m_material` d ON c.`id_material` = d.`id_mat`
			JOIN `m_uom` e ON d.unit_terkecil = e.`id_uom`
			LEFT JOIN `t_uom_convert` f ON e.id_uom = f.`id_uom`
			WHERE a.id_retur_pembelian = ' . $id . '
			) b ON a.id_mat = id_material
			SET qty = qty - retur, `qty_sol` = `qty_sol` - retur, `qty_big` = `qty_big` - retur_big, `qty_sol_big` = `qty_sol_big` - retur_big;

		';


		$query 	= $this->db->query($sql);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	function insert_mutasi($id) {

		$sql 	= '
			INSERT INTO `t_mutasi`
			SELECT NULL,c.`id_material` AS awal, c.`id_material` akhir, retur_date, (b.retur_qty * ((IF(convertion IS NULL,1,convertion)) / d.`base_qty`) ) retur,
			1 tm, pic, "Retur Pembelian" note,retur_qty,(b.retur_qty * ((IF(convertion IS NULL,1,convertion)) / d.`base_qty`) ),retur_qty,0,0,0,0,3,0,0 
			FROM `t_retur_pembelian` a
			JOIN `t_detail_retur_pembelian` b ON a.`id_retur_pembelian` = b.`id_retur_pembelian`
			JOIN `t_po_mat` c ON b.`id_po_mat` = c.`id_t_ps`
			JOIN `m_material` d ON c.`id_material` = d.`id_mat`
			JOIN `m_uom` e ON d.unit_terkecil = e.`id_uom`
			LEFT JOIN `t_uom_convert` f ON e.id_uom = f.`id_uom`
			WHERE a.id_retur_pembelian = ' . $id . '

		';


		$query 	= $this->db->query($sql);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_value($data) {
		$datas = array(
			'id_coa' => $data['coa'],
			'date' => $data['tgl'],
			'date_insert' => date('Y-m-d H:i:s'),
			'value' => $data['jumlah'],
			'value_real' => $data['jumlah'],
			'type_cash' => $data['type_cash'],
			'bukti' => $data['bukti'],
			'id_coa_temp' => $data['id_coa_temp'],
			'id_valas' => 1,
			'note' => $data['ket']
		);

		//	print_r($datas);die;

		$this->db->insert('t_coa_value', $datas);


		//$query 	= $this->db->query($sql,$data);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
}
