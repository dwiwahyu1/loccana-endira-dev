<style>
	.dt-body-left {
		text-align: left;
	}

	.dt-body-right {
		text-align: right;
	}

	.dt-body-center {
		text-align: center;
		vertical-align: middle;
	}

	.force-overflow {
		height: 650px;
		overflow-y: auto;
		overflow-x: auto;
	}

	.scroll-overflow {
		min-height: 650px;
	}

	#modal-distributor::-webkit-scrollbar-track {
		box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
		-webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar {
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar-thumb {
		background-image: -webkit-gradient(linear,
				left bottom,
				left top,
				color-stop(0.44, rgb(122, 153, 217)),
				color-stop(0.72, rgb(73, 125, 189)),
				color-stop(0.86, rgb(28, 58, 148)));
	}

	.select2-container .select2-choice {
		height: 35px !important;
	}
</style>

<div class="product-sales-area mg-tb-30">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="product-payment-inner-st">
					<ul id="myTabedu1" class="tab-review-design">
						<li class="active"><a href="#description">Tambah Retur Pembelian</a></li>
					</ul>
					<form id="add_retur_pembelian" action="<?= base_url('retur_pembelian/add_retur_pembelian'); ?>" class="add-department" autocomplete="off">
						<div id="myTabContent" class="tab-content custom-product-edit">
							<div class="product-tab-list tab-pane fade active in" id="description">
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="review-content-section">
											<div class="row">
												<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
													<div class="form-group">
														<label>Nomor Invoice</label>
														<select name="id_invoice" id="id_invoice" onChange="populate_detail_pembelian()" class="form-control" placeholder="Nama Principle">
															<option value="" selected="selected" disabled>-- Pilih Invoice --</option>
															<?php foreach (($list_invoice_pembelian ?? []) as $inv_pembelian) : ?>
																<option value="<?= $inv_pembelian['id_invoice']; ?>"><?= implode(' - ', [$inv_pembelian['no_invoice'], $inv_pembelian['name_eksternal']]); ?></option>
															<?php endforeach; ?>
															<!-- <?php foreach (($list_pembelian ?? []) as $pembelian) : ?>
																<option value="<?= $pembelian['id_pembelian']; ?>"><?= implode(' - ', [$pembelian['no_pembelian'], $pembelian['principle']]); ?></option>
															<?php endforeach; ?> -->
														</select>
													</div>
													<div class="form-group date">
														<label>Tanggal Pembelian</label>
														<input name="tgl" id="tgl" type="text" class="form-control" placeholder="Tanggal Penjualan" readOnly>
													</div>
													<div class="form-group">
														<label>Principle</label>
														<input name="principle" id="principle" type="text" class="form-control" placeholder="Principle" readOnly>
													</div>
													<div class="form-group">
														<label>Alamat</label>
														<textarea name="alamat" id="alamat" type="text" class="form-control" placeholder="Alamat Principal" readOnly></textarea>
													</div>
													<!-- <div class="form-group">
														<label>Att</label>
														<input name="att" id="att" type="text" class="form-control" placeholder="Att" readOnly>
													</div> -->
													<div class="form-group">
														<label>No. Telp</label>
														<input name="telp" id="telp" type="text" class="form-control" placeholder="Telephone" readOnly>
													</div>
													<div class="form-group">
														<label>Email</label>
														<input name="email" id="email" type="text" class="form-control" placeholder="Email" readOnly>
													</div>
													<!-- <div class="form-group">
														<label>Limit Kredit</label>
														<input name="limit" id="limit" type="text" class="form-control" placeholder="Limit Kredit" readOnly>
													</div>
													<div class="form-group">
														<label>Sisa Kredit</label>
														<input name="sisa" id="sisa" type="text" class="form-control" placeholder="Sisa Kredit" readOnly>
													</div> -->
												</div>
												<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
													<div class="form-group">
														<label>Ship From:</label>
														<textarea name="alamat_kirim" id="alamat_kirim" type="text" class="form-control" placeholder="Alamat Principal" readOnly>JL. Sangkuriang NO.38-A NPWP: 01.555.161.7.428.000</textarea>
													</div>
													<div class="form-group date">
														<label>Email</label>
														<input name="emaila" id="emaila" type="email" class="form-control" placeholder="Email" readOnly>
													</div>
													<div class="form-group date">
														<label>Telp/Fax</label>
														<input name="telpfax" id="telpfax" type="text" class="form-control" placeholder="Telp/Fax" value="(022) 6626-946" readOnly>
													</div>

													<input name="ppn" id="ppn" type="hidden" class="form-control" placeholder="VAT/PPN" onKeyup="change_ppn()" value="10">

													<div class="form-group date">
														<label>Term Pembayaran</label>
														<input name="term" id="term" type="hidden" class="form-control" placeholder="Term (Hari)" value="0" style="" readonly>
														<input name="term_o" id="term_o" type="number" class="form-control" placeholder="Term (Hari)" value="0" style="" readonly>
													</div>

													<div class="form-group date">
														<label>Keterangan Beli</label>
														<textarea name="keterangan_beli" id="ket" class="form-control" placeholder="keterangan" readOnly></textarea>
													</div>

													<div class="form-group date">
														<label>Tanggal Retur</label>
														<input name="tgl_retur" id="tgl_retur" class="form-control" placeholder="Tanggal Retur" value="" required/>
													</div>

													<div class="form-group date">
														<label>Keterangan Retur</label>
														<textarea name="keterangan_retur" id="keterangan_retur" class="form-control" placeholder="keterangan"></textarea>
													</div>
												</div>
											</div>

										</div>
									</div>
								</div>
								<br><br>
								<div class="row">
									<h3>Items</h3>
								</div>

								<div class="row">
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
										<div class="form-group">
											<label>Kode</label>
										</div>
									</div>
									<div class="col-lg-1 col-md-2 col-sm-3 col-xs-12">
										<div class="form-group">
											<label>Retur Satuan</label>
										</div>
									</div>
									<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
										<div class="form-group">
											<label>Qty Retur</label>
										</div>
									</div>
									<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
										<div class="form-group">
											<label>Qty Order</label>
										</div>
									</div>

									<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
										<div class="form-group">
											<label>Harga</label>
										</div>
									</div>

									<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
										<div class="form-group">
											<label>Total</label>
										</div>
									</div>
								</div>
							</div>
							<input name="total_items" id="total_items" type="hidden" value="0" />
							<input name="int_flo" id="int_flo" type="hidden" class="form-control" placeholder="Qty" value=0;>
							<div id="table_items"></div>
							<br>

							<div class="row" style="border-top-style:solid;">
								<div style="margin-top:10px">
									<div class="col-lg-6 col-md-3 col-sm-3 col-xs-12">

									</div>
									<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
										<label>Total Harga Retur</label>
									</div>
									<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
										<label id="grand_total" style="float:right">0</label>
									</div>
									<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-12">
									<div class="payment-adress">
										<button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
										<button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()"> Batal</button>
									</div>
								</div>
							</div>
						</div>
				</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>

<script>
	function indonesia_currency_format(value, printSymbol = false) {
		var options = {};

		if (printSymbol) {
			options = {
				style: 'currency',
				currency: 'IDR'
			}
		}

		value = value.toFixed(2);
		remain = value % 1;

		value = new Intl.NumberFormat('id-ID', options).format(value);

		// console.log(value);

		if (remain <= 0) {
			value = value + ',00';
		}

		return value;
	}

	function populate_detail_pembelian() {

		var prin = $('#ints').val();
		var prin2 = $('#name').val();

		$('#ints2').val(prin);
		//	alert(prin2);
		$('#ints').val(prin2);


		var datapost = {
			"id_invoice": $('#id_invoice').val()
		};

		$.ajax({
			type: 'POST',
			url: "<?php echo base_url() . 'retur_pembelian/get_detail_pembelian'; ?>",
			data: JSON.stringify(datapost),
			cache: false,
			contentType: false,
			processData: false,
			success: function(response) {
				//var obj = JSON.parse(response);
				$('#tgl').val(response['pembelian'].purchase_date);
				$('#principle').val(response['pembelian'].principle);
				$('#alamat').val(response['pembelian'].principle_address);
				$('#att').val(response['pembelian'].purchase_note);
				$('#telp').val(response['pembelian'].principle_phone);
				$('#email').val(response['pembelian'].principle_email);
				$('#term').val(response['pembelian'].term_of_payment);
				$('#term_o').val(response['pembelian'].term_of_payment);
				$('#ket').val(response['pembelian'].purchase_note);
				//console.log(response.phone_1);


				$('#table_items').html(response.list)

				$('#vatppn').html(response.ppn);
				$('#subttl2').html(response.dpp);
				$('#grand_total').html(response.total_amount);
				$('#int_flo').val(response.int)
				$('#total_items').val(response.int)
				// $('#table_items').append(htl);
				// $('#kode_0').html(response.list);

				// $('#kode_0').select2();

				//$($("#kode_0").select2("container")).addClass("form-control");
				//$("#kode_0").select2({ height: '300px' });	

				$('.rupiah').priceFormat({
					prefix: '',
					centsSeparator: ',',
					centsLimit: 0,
					thousandsSeparator: '.'
				});
			}
		});


		//$('#ints').val('1');

	}


	function change_ppn() {

		var sub_total = 0;
		var sub_total_bd = 0;
		var sub_disk = 0;
		var int_val = parseInt($('#int_flo').val());

		for (var yo = 0; yo <= int_val; yo++) {

			if ($('#kode_' + yo).val() !== undefined) {

				var tots = $('#total_' + yo).val();
				var tots = tots.replace('.', '');
				var tots = tots.replace('.', '');
				var tots = tots.replace('.', '');
				var tots = tots.replace('.', '');

				var prcs = $('#harga_' + yo).val();
				var prcs = prcs.replace('.', '');
				var prcs = prcs.replace('.', '');
				var prcs = prcs.replace('.', '');
				var prcs = prcs.replace('.', '');

				var qtys = $('#qty_' + yo).val();
				var qtys = qtys.replace('.', '');
				var qtys = qtys.replace('.', '');
				var qtys = qtys.replace('.', '');
				var qtys = qtys.replace('.', '');



				sub_total = sub_total + parseFloat(tots);
				sub_total_bd = sub_total_bd + (parseFloat(prcs) * parseFloat(qtys));
				sub_disk = sub_disk + ((parseFloat(prcs) * parseFloat(qtys)) * (parseFloat($('#diskon_' + yo).val()) / 100));
			}

		}

		if ($('#ppn').val() == '') {
			var ppns = 0;
		} else {
			var ppns = $('#ppn').val();
		}

		var total_ppn = (sub_total * (parseFloat(ppns) / 100));
		var grand_total = sub_total + (sub_total * (parseFloat(ppns) / 100));

		var gt = new Intl.NumberFormat('de-DE', {}).format(grand_total);
		var sub_totals = new Intl.NumberFormat('de-DE', {}).format(sub_total);
		var total_ppns = new Intl.NumberFormat('de-DE', {}).format(total_ppn);
		var total_bds = new Intl.NumberFormat('de-DE', {}).format(sub_total_bd);
		var total_sub_disk = new Intl.NumberFormat('de-DE', {}).format(sub_disk);

		// $('#vatppn').html(total_ppns);
		console.log(gt)
		$('#grand_total').html(gt);

	}

	function change_sum(fl) {

		var qty = $('#qty_' + fl).val();
		var kode = $('#kode_' + fl).val();
		// var qtys = $('#qtys_' + fl).val();
		var harga = $('#harga_' + fl).val();
		// console.log(qty,kode,qtys,harga)
		var harga = harga.replace('.', '');
		var harga = harga.replace('.', '');
		var harga = harga.replace('.', '');
		var harga = harga.replace('.', '');
		var harga = harga.replace('.', '');
		var harga = harga.replace(',', '.');

		var qty = qty.replace('.', '');
		var qty = qty.replace('.', '');
		var qty = qty.replace('.', '');
		var qty = qty.replace('.', '');
		var qty = qty.replace('.', '');

		// var qtys = qtys.replace('.', '');
		// var qtys = qtys.replace('.', '');
		// var qtys = qtys.replace('.', '');
		// var qtys = qtys.replace('.', '');
		// var qtys = qtys.replace('.', '');

		//var val = $('#kode_'+rs+'').val();

		var sss = kode.split('|');

		//alert(qtys);
		// if (qtys == "") {
		// 	var qtys = 0;
		// } else {
		// 	var qtys = qtys;
		// }

		if (qty == "") {
			var qty = 0;
		} else {
			var qty = qty;
		}

		var total_qty = parseInt(qty);

		var total_harga = total_qty * parseFloat(harga);
		var total_harga = new Intl.NumberFormat('de-DE', {}).format(total_harga);

		$('#qty_retur_' + fl + '').val(total_qty);
		$('#total_' + fl + '').val(total_harga);

		// if(total_qty > parseInt(sss[3]) ){



		// }

		// var diskon = diskon.replace('.','');
		// var diskon = diskon.replace('.','');
		// var diskon = diskon.replace('.','');
		// var diskon = diskon.replace('.','');

		// var qty = qty.replace('.','');
		// var qty = qty.replace('.','');
		// var qty = qty.replace('.','');
		// var qty = qty.replace('.','');
		// //alert(harga);


		//var total = (parseFloat(qty) * parseFloat(harga) )- ((parseFloat(qty) * parseFloat(harga))*(parseFloat(diskon)/100)); 
		// var totals = new Intl.NumberFormat('de-DE', { }).format(total);


		// if( isNaN(total)){
		// //var total_num = Number((total).toFixed(1)).toLocaleString();
		// //$('#subttl').html('0');
		// $('#total_'+fl).val('0');
		// }else{
		// //var total_num = Number((total).toFixed(1)).toLocaleString();
		// //$('#subttl').html(total);
		// $('#total_'+fl).val(totals);
		// }

		// var sub_total = 0;
		// var sub_total_bd = 0;
		// var sub_disk = 0;
		var int_val = parseInt($('#int_flo').val());

		var total_all = 0;

		for (var yo = 0; yo <= int_val; yo++) {

			if ($('#kode_' + yo).val() !== undefined) {

				var qty = $('#qty_' + yo).val();
				var kode = $('#kode_' + yo).val();
				// var qtys = $('#qtys_' + yo).val();
				var harga = $('#harga_' + yo).val();

				var harga = harga.replace('.', '');
				var harga = harga.replace('.', '');
				var harga = harga.replace('.', '');
				var harga = harga.replace('.', '');
				var harga = harga.replace('.', '');
				var harga = harga.replace(',', '.');

				var qty = qty.replace('.', '');
				var qty = qty.replace('.', '');
				var qty = qty.replace('.', '');
				var qty = qty.replace('.', '');
				var qty = qty.replace('.', '');

				// var qtys = qtys.replace('.', '');
				// var qtys = qtys.replace('.', '');
				// var qtys = qtys.replace('.', '');
				// var qtys = qtys.replace('.', '');
				// var qtys = qtys.replace('.', '');

				//var val = $('#kode_'+rs+'').val();

				var sss = kode.split('|');

				//alert(qtys);
				// if (qtys == "") {
				// 	var qtys = 0;
				// } else {
				// 	var qtys = qtys;
				// }

				// if (qty == "") {
				// 	var qty = 0;
				// } else {
				// 	var qty = qty;
				// }

				var total_qty = (parseInt(qty) * parseInt(sss[1]));

				var total_harga = total_qty * parseFloat(harga);
				//var total_harga = new Intl.NumberFormat('de-DE', {}).format(total_harga);

				total_all = total_all + total_harga;

			}

		}

		//alert(total_all);

		var gt = new Intl.NumberFormat('de-DE', {}).format(total_all.toFixed(2));
		console.log(int_val,total_all,gt);
		$('#grand_total').html(gt);

		var ppn = total_all / 11;
		var ppns = new Intl.NumberFormat('de-DE', {}).format(ppn.toFixed(2));
		//$('#vatppn').html(ppns);

		var dpp = total_all - ppn;
		var dpps = new Intl.NumberFormat('de-DE', {}).format(dpp.toFixed(2));
		//$('#subttl2').html(dpps);

		// var total_ppn = (sub_total * (parseFloat($('#ppn').val())/100 ));
		// var grand_total = sub_total + (sub_total * (parseFloat($('#ppn').val())/100 ));



		// var gt = new Intl.NumberFormat('de-DE', { }).format(grand_total);
		// var sub_totals = new Intl.NumberFormat('de-DE', { }).format(sub_total);
		// var total_ppns = new Intl.NumberFormat('de-DE', { }).format(total_ppn);
		// var total_bds = new Intl.NumberFormat('de-DE', { }).format(sub_total_bd);
		// var total_sub_disk = new Intl.NumberFormat('de-DE', { }).format(sub_disk);

		// $('#subttl').html(total_bds);
		// $('#subttl2').html(sub_totals);
		// $('#subttl3').html(total_sub_disk);
		// $('#vatppn').html(total_ppns);
		// $('#grand_total').html(gt);




		//alert(qty+' '+harga+' '+diskon);

	}

	function back() {
		window.location.href = "<?= base_url('retur_pembelian'); ?>";
	}

	$(document).ready(function() {
		$('#id_invoice').select2();

		$('.rupiah').priceFormat({
			prefix: '',
			centsSeparator: ',',
			centsLimit: 0,
			thousandsSeparator: '.'
		});

		$('#tgl').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		});
		$('#tgl_retur').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		});

		$('#add_retur_pembelian').on('submit', function(e) {

			e.preventDefault();

			var formUrl = $(this).attr('action');
			var formData = new FormData();
			var totalItems = parseInt($('#total_items').val() || 0);

			formData.append('id_invoice', $('#id_invoice').val() || '');
			formData.append('keterangan_retur', $('#keterangan_retur').val() || '');
			formData.append('tgl_retur', $('#tgl_retur').val() || '');
			formData.append('total_items', totalItems);

			for (var idx = 0; idx < totalItems; idx++) {
				formData.append('id_t_ps_' + idx, $('#id_t_ps_' + idx).val());
				formData.append('qty_retur_' + idx, $('#qty_retur_' + idx).val());
			}

			swal({
				title: 'Yakin akan Simpan Data ?',
				text: '',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak'
			}).then(function() {

				$.ajax({
					type: 'POST',
					url: formUrl,
					data: formData,
					cache: false,
					contentType: false,
					processData: false,
					success: function(response) {

						if (response.success == true) {

							swal({
								title: 'Success!',
								text: "Apakah Anda Akan Input Data Retur Lagi ?",
								type: 'success',
								showCancelButton: true,
								confirmButtonText: 'Ya',
								cancelButtonText: 'Tidak'
							}).then(function() {
								$('#add_retur_pembelian').trigger("reset");
							}, function(dismiss) {
								back();
							})

						} else {
							swal({
								title: 'Data Inputan Tidak Sesuai !! ',
								html: response.message,
								type: 'error',
								showCancelButton: true,
								confirmButtonText: 'Ok'
							});
						}
					}
				});

			});
		});
	});
</script>