<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Range_Price extends MX_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('range_price/range_price_model');
    $this->load->library('log_activity');
    $this->load->library('formatnumbering');
	  $this->load->library('priv');
  }

  /**
   * anti sql injection
   */
  public function Anti_sql_injection($string) {
    $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
    return $string;
  }

  public function index() {
	  
		$priv = $this->priv->get_priv();

		$data = array(
			'priv' => $priv
		);
	  
    $this->template->load('maintemplate', 'range_price/views/index', $data);
  }

  function lists() {
    $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
    $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
    $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
    $order = $this->input->get_post('order');
    $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
    $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

    $order_fields = array('', 'stock_code', 'stock_name');

    $search = $this->input->get_post('search');

    $search_val = (!empty($search['value'])) ? $search['value'] : null;

    $search_value = $this->Anti_sql_injection($search_val);

    // Build params for calling model
    $params['limit'] = (int) $length;
    $params['offset'] = (int) $start;
    $params['order_column'] = $order_fields[$order_column];
    $params['order_dir'] = $order_dir;
    $params['searchtxt'] = $search_value;

    $list = $this->range_price_model->lists($params);
	
	  $priv = $this->priv->get_priv();

    $result["recordsTotal"] = $list['total'];
    $result["recordsFiltered"] = $list['total_filtered'];
    $result["draw"] = $draw;

    $data = array();
    //$i = 0;
    $i = $params['offset'];
    $username = $this->session->userdata['logged_in']['username'];
    foreach ($list['data'] as $k => $v) {
      $i = $i + 1;
      $button_edit = '<div class="btn-group" style="display:'.$priv['update'].'"><button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="save_price(\'' . $v['id_mat'] . '\')"><i class="fa fa-save"></i></button></div>';
      if ($v['status_harga'] == 0) {
        $status = 'Konfirmasi';
        $button = $button_edit 
        // . $button_appr
        ;
      } else {
        $status = 'Setuju';
        $button = $button_edit;
      }
      if($priv['update']=='block')
      array_push($data, array(
        $i,
        $v['stock_code'],
        $v['stock_name'].' '.$v['base_qty'].' '.$v['uom_symbol'],
        $v['name_eksternal'],
        '<input type="number"  id="top_price' . $v['id_mat'] . '" name="top_price" class="form-control" placeholder="Top Price" value="' . $v['top_price'] . '" autocomplete="off" required step="0.0001">',     
        '<input type="number"  id="bottom_price' . $v['id_mat'] . '" name="top_price" class="form-control" placeholder="Top Price" value="' . $v['bottom_price'] . '" autocomplete="off" required step="0.0001">',
        $button
      ));
      else
      
      array_push($data, array(
        $i,
        $v['stock_code'],
        $v['stock_name'].' '.$v['base_qty'].' '.$v['uom_symbol'],
        $v['name_eksternal'],
        '<input type="number" readonly id="top_price' . $v['id_mat'] . '" name="top_price" class="form-control" placeholder="Top Price" value="' . $v['top_price'] . '" autocomplete="off" required step="0.0001">',     
        '<input type="number" readonly id="bottom_price' . $v['id_mat'] . '" name="top_price" class="form-control" placeholder="Top Price" value="' . $v['bottom_price'] . '" autocomplete="off" required step="0.0001">',
        '<div class="btn-group" ><button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Block" style="disabled:true"><i class="fa fa-ban"></i></button></div>'
      ));
    }

    $result["data"] = $data;

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }


  public function edit_material() {
    
    $data   = file_get_contents("php://input");
    $params   = json_decode($data, true);
    $id_mat = $this->Anti_sql_injection($params['id_mat']);
    $top_price = $this->Anti_sql_injection($params['top_price']);
    $bottom_price = $this->Anti_sql_injection($params['bottom_price']);
    $data = array(
      'id_mat'         => $id_mat,
      'top_price'     => $top_price,
      'bottom_price'   => $bottom_price
    );
    $result = $this->range_price_model->edit_price($data);
    // $result = $result + $this->range_price_model->add_price_history($data);
    if (count($result) >= 1) {
      $msg = 'Berhasil mengubah data price ke database';

      $this->log_activity->insert_activity('insert', 'Berhasil Update Price dengan id Material ' . $id_mat);
      $results = array(
        'status' => 'success',
        'success' => true,
        'message' => $msg
      );
    } else {
      $msg = 'Gagal mengubah data price ke database';

      $this->log_activity->insert_activity('insert', 'Gagal Update Price dengan id material = ' . $id_mat);
      $results = array(
        'status' => 'failed',
        'success' => false,
        'message' => $msg
      );
    }

    $this->output->set_content_type('application/json')->set_output(json_encode($results), true);
  
  }

  public function get_properties() {
    $id = $this->Anti_sql_injection($this->input->post('id', TRUE));
    $detail_prop = $this->range_price_model->detail_prop_full($id);

    $result = array(
      'success' => true,
      'message' => '',
      'data' => $detail_prop
    );

    $this->output->set_content_type('application/json')->set_output(json_encode($result, true));
  }

}
