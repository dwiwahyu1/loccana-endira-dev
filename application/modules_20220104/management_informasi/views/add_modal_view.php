<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto
  }

  .scroll-overflow {
    min-height: 650px
  }

  #modal-distributor::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="product-payment-inner-st">
          <ul id="myTabedu1" class="tab-review-design">
            <li class="active"><a href="#description">Tambah Informasi</a></li>
          </ul>
          <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="review-content-section">
                    <form class="form-horizontal form-label-left" id="add_info" role="form" action="<?= base_url('management_informasi/add_informasi'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>

                      <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Judul <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input data-parsley-maxlength="255" type="text" id="judul_informasi" name="judul_informasi" class="form-control col-md-7 col-xs-12" placeholder="Judul" required="required">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Keterangan<span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <textarea data-parsley-maxlength="255" type="text" id="informasi_ket" name="informasi_ket" class="form-control col-md-7 col-xs-12" placeholder="Keterangan" required="required"> </textarea>
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Gambar <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input data-parsley-maxlength="255" type="file" id="gambar_informasi" name="gambar_informasi" class="form-control col-md-7 col-xs-12" placeholder="gambar_informasi" required="required">
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Submit</button>
                          <button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()">Batal</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="PrimaryModalalert" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-close-area modal-close-df">
        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
      </div>
      <div class="modal-body">
        <i class="educate-icon educate-checked modal-check-pro"></i>
        <h2>Data Berhasil Disimpan</h2>
        <p>Apakah Anda Ingin Menambah Data Informasi Lagi ?</p>
      </div>
      <div class="modal-footer">
        <a data-dismiss="modal" href="#" onClick="back()">Tidak</a>
        <a data-dismiss="modal" href="#" onClick="clearform()">Ya</a>
      </div>
    </div>
  </div>
</div>

<div id="WarningModalftblack" class="modal modal-edu-general Customwidth-popup-WarningModal PrimaryModal-bgcolor fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-close-area modal-close-df">
        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
      </div>
      <div class="modal-body">
        <span class="educate-icon educate-warning modal-check-pro information-icon-pro"></span>
        <h2>Warning!</h2>
        <p id="msg_err">The Modal plugin is a dialog box/popup window that is displayed on top of the current page</p>
      </div>
      <div class="modal-footer footer-modal-admin warning-md">
        <a data-dismiss="modal" href="#">Ok</a>
      </div>
    </div>
  </div>
</div>

<script>
  function clearform() {

    $('#add_info').trigger("reset");

  }

  function back() {

    window.location.href = "<?php echo base_url() . 'management_informasi'; ?>";

  }

  function listdist() {
    var user_id = '0001';
    var token = '093940349';


    $('#datatable_pricipal').DataTable({
      //"dom": 'rtip',
      "bFilter": false,
      "aaSorting": [],
      "bLengthChange": true,
      'iDisplayLength': 10,
      "sPaginationType": "simple_numbers",
      "Info": false,
      "processing": true,
      "serverSide": true,
      "destroy": true,
      "ajax": "<?php echo base_url() . 'management_informasi/lists' ?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token,
      "searching": true,
      "language": {
        "decimal": ",",
        "thousands": "."
      },
      "dom": 'l<"toolbar">frtip',
      "initComplete": function() {
        $("div.toolbar").prepend('<div class="btn-group pull-left"><button type="button" class="btn btn-custon-rounded-two btn-primary" > Tambah </button></div>');
      }
    });
  }

  $(document).ready(function() {

    listdist();

    $('#add_info').on('submit', function(e) {
      e.preventDefault();

      var formData = new FormData(this);
      var urls = $(this).attr('action');

      swal({
        title: 'Yakin akan Simpan Data ?',
        text: '',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak'
      }).then(function() {

        $.ajax({
          type: 'POST',
          url: urls,
          data: formData,
          cache: false,
          contentType: false,
          processData: false,
          success: function(response) {
            if (response.success == true) {
              swal({
                title: 'Success!',
                text: "Apakah Anda Akan Input Data Informasi Lagi ?",
                type: 'success',
                showCancelButton: true,
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak'
              }).then(function() {
                $('#add_info').trigger("reset");
              }, function(dismiss) {
                back();
              })

            } else {
              swal({
                title: 'Data Inputan Tidak Sesuai',
                html: response.message,
                type: 'error',
                confirmButtonText: 'Ok'
              })
            }
          }
        });

      });
    });
  });
</script>


<!-- /page content -->