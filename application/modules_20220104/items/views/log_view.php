<script src="<?php echo base_url('/assets/gentelella-master/vendors/waitme/waitMe.min.js') ?>"></script>
<link rel="stylesheet" href="<?php echo base_url('/assets/gentelella-master/vendors/waitme/waitMe.min.css') ?>">

<link href="<?php echo base_url('assets/addons'); ?>/lightbox2/css/lightbox.min.css" rel="stylesheet">
<script src="<?php echo base_url('assets/addons'); ?>/lightbox2/js/lightbox.min.js" type="text/javascript"></script>

<style>
    tfoot {
        background: #5d6369;
        color: #ECF0F1;
    }
</style>

<h4>Log Activity</h4>
<div class="x_panel">
    <div class="x_content">
        <table id="listdokumen" class="table table-bordered table-striped jambo_table bulk-action">
            <thead>
            <th width="10">No</th>
            <th>User ID</th>
            <th>Nama</th>
            <th>Role</th>
            <th>Tipe Aktifitas</th>
            <th>Deskripsi Aktifitas</th>
            <th>Nama Objek</th>
            <th>Url Objek</th>
            <th>Tanggal</th>
            </thead>
            <tbody>              
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        $("#listdokumen").dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo base_url().'users/lists_log/'; ?>",
            "searchDelay": 700
        });
         
    });
    
</script>
