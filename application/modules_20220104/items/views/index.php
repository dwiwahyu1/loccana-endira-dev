<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto
  }

  .scroll-overflow {
    min-height: 650px
  }

  #modal-distributor::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
          <div class="card-box table-responsive">
            <h4 class="header-title m-t-0 m-b-30">Items</h4>
            <table id="listitems" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kode Item</th>
                  <th>Nama Item</th>
                  <th>Deskripsi</th>
                  <th>UoM</th>
                  <th>Unit Box</th>
                  <th>Principal</th>
                  <th>Option</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div><!-- end col -->
      </div>
    </div>
  </div>
  <span id="laod"></span>
</div>


<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog" style="width:800px;">
    <div class="modal-content p-0 b-0">
      <div class="panel panel-color panel-primary panel-filled">
        <div class="panel-heading">
          <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 class="panel-title"></h3>
        </div>
        <div class="panel-body force-overflow" id="modal-material">
          <div class="scroll-overflow">
            <p></p>
          </div>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
  function add_material() {
    $('#panel-modal').removeData('bs.modal');
    $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    $('#panel-modal  .panel-body').load('<?php echo base_url('items/add'); ?>');
    $('#panel-modal  .panel-title').html('<i class="fa fa-plus"></i> Tambah Stok');
    $('#panel-modal').modal({
      backdrop: 'static',
      keyboard: false
    }, 'show');
  }

  function editmaterial(id) {
    var url = '<?php echo base_url(); ?>items/edit';

    $("#laod").append(' <img id="loading" src="<?php echo base_url(); ?>assets/urate-frontend-master/assets/images/icon_loader.gif">');
    var form = $("<form action='" + url + "' method='post'>" +
      "<input type='hidden' name='iditems' value='" + id + "' />" +
      "</form>");
    $('body').append(form);
    form.submit();
    // $('#panel-modal').removeData('bs.modal');
    // $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    // $('#panel-modal  .panel-body').load('<?php echo base_url('items/edit/'); ?>' + "/" + id);
    // $('#panel-modal  .panel-title').html('<i class="fa fa-pencil"></i> Edit Stok');
    // $('#panel-modal').modal({
    //   backdrop: 'static',
    //   keyboard: false
    // }, 'show');
  }

  function deletematerial(id) {
    swal({
      title: 'Yakin akan Menghapus ?',
      text: 'data tidak dapat dikembalikan bila sudah dihapus !',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak'
    }).then(function() {
      var datapost = {
        "id": id
      };
      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>items/deletes",
        data: JSON.stringify(datapost),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function(response) {
          if (response.status == "success") {
            swal({
              title: 'Success!',
              text: response.message,
              type: 'success',
              showCancelButton: false,
              confirmButtonText: 'Ok'
            }).then(function() {
              window.location.href = "<?php echo base_url('items'); ?>";
            })
          } else {
            swal("Failed!", response.message, "error");
          }
        }
      });
    })
  }

  $(document).ready(function() {

    $("#listitems").dataTable({
      "processing": true,
      "serverSide": true,
      "ajax": "<?php echo base_url() . 'items/lists/'; ?>",
      "searchDelay": 700,
      "responsive": true,
      "lengthChange": false,
      "info": false,
      "bSort": false,
      "dom": 'l<"toolbar">frtip',
      "initComplete": function() {
        $("div.toolbar").prepend('<div class="btn-group pull-left"><a href="<?php echo base_url() . 'items/add'; ?>" type="button" class="btn btn-custon-rounded-two btn-primary" style="display: <?php echo $priv['insert'] ?>;" data-toggle="tooltip" title="Insert" > <i class="fa fa-plus"></i> </a></div>');
      }
    });
  });
</script>