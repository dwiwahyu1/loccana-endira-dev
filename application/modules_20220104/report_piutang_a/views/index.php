<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto;
  }

  .scroll-overflow {
    min-height: 650px
  }

  #modal-distributor::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }

  table.dataTable thead>tr>th {
    padding-right: 10px;
  }

  table.dataTable thead .sorting_asc:after,
  table.dataTable thead .sorting_desc:after {
    content: "";
  }

  /* tbody {
    display: block;
    max-height: 500px;
    overflow: auto;
  }

  thead,
  tbody tr {
    display: table;
    width: 100%;
    table-layout: fixed;
  }

  thead {
    width: calc(100% - 1em)
  }

  table {
    width: 400px;
  } */

  table tbody {
    display: block;
    max-height: 300px;
    overflow-y: scroll;
    overflow-x: hidden;
  }

  table thead,
  table tbody tr {
    display: table;
    width: 100%;
    table-layout: fixed;
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
          <div class="card-box table-responsive">
            <div style="display: flex; justify-content:space-between; align-items:center; margin-bottom: 12px;">
              <h4 class="header-title" style="margin:0;">Report Piutang</h4>
            </div>
            <form id="report-cash-filter-form" action="<?= base_url('report_piutang/list'); ?>" class="add-department" method="post" target="_blank" autocomplete="off">
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group">
                    <label>Customer</label>
                    <select name="customer_id" id="customer_id" class="form-control">
                      <option value="" selected>Semua Customer</option>
                      <?php foreach ($customer_list as $customer) : ?>
                        <option value="<?= $customer['id_t_cust']; ?>"><?= $customer['cust_name']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Bulan & Tahun</label><br>
                    <select class="" name="filter_month" id="filter_month">
                      <option value="" selected>Semua Bulan</option>
                      <option value="01">Januari</option>
                      <option value="02">Februari</option>
                      <option value="03">Maret</option>
                      <option value="04">April</option>
                      <option value="05">Mei</option>
                      <option value="06">Juni</option>
                      <option value="07">Juli</option>
                      <option value="08">Agustus</option>
                      <option value="09">September</option>
                      <option value="10">Oktober</option>
                      <option value="11">November</option>
                      <option value="12">Desember</option>
                    </select>
                    <select class="" name="filter_year" id="filter_year">
                      <option value="" selected>Semua Tahun</option>
                      <?php for ($tahun = (int) date('Y'); $tahun >= 1990; $tahun--) : ?>
                        <option value="<?= $tahun; ?>"><?= $tahun; ?></option>
                      <?php endfor; ?>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Wilayah</label>
                    <select class="form-control" name="region_id" id="region_id">
                      <option value="" selected>Semua Wilayah</option>
                      <?php foreach ($region_list as $region) : ?>
                        <option value="<?= $region['id_t_region']; ?>"><?= $region['region']; ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="text-center row" style="margin-top: 24px; margin-bottom: 24px;">
                <button class="btn btn-primary submit-button" type="button">Submit</button>
                <button class="btn btn-primary m-t-4" type="button" onclick="print_excel()">Export</button>
              </div>
            </form>
            <table id="datatable-report-piutang" class="table table-striped table-bordered m-t-4">
              <thead>
                <tr>
                  <th style="width: 150px;">Customer</th>
                  <th style="width: 100px;">Tanggal Pesan</th>
                  <th style="width: 100px;">Faktur</th>
                  <th style="width: 150px;">Produk</th>
                  <th style="width: 100px;">Kemasan</th>
                  <th style="width: 100px;">Qty</th>
                  <th style="width: 150px;">Saldo</th>
                  <th style="width: 150px;">Bln Berjalan</th>
                  <th style="width: 100px;">Umur</th>
                  <th style="width: 17px;"></th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div><!-- end col -->
      </div>
    </div>
  </div>
  <span id="laod"></span>
</div>

<script>
  $('select').select2();

  function load_list() {
    var user_id = '0001';
    var token = '093940349';

    var params = [
      'sess_user_id=' + user_id,
      'sess_token=' + token,
      'customer_id=' + $('#customer_id').val() || '',
      'region_id=' + $('#region_id').val() || '',
      'filter_month=' + $('#filter_month').val() || '',
      'filter_year=' + $('#filter_year').val() || '',
    ];

    var params_string = '?' + params.join('&');

    var config = {
      "ordering": false,
      "lengthChange": false,
      "bPaginate": false,
      "bInfo": false,
      "bSort": false,
      "bFilter": false,
      "bLengthChange": false,
      "order": [],
      "aaSorting": [],
      'iDisplayLength': 10,
      "info": true,
      "processing": true,
      "serverSide": true,
      "destroy": true,
      "ajax": "<?= base_url('report_piutang/list'); ?>" + params_string,
      "searching": true,
      "language": {
        "decimal": ",",
        "thousands": "."
      },
      "dom": 'l<"toolbar">frtip',
      "columnDefs": [{
          className: 'text-center',
          targets: [1, 2]
        },
        {
          className: 'text-right',
          targets: [-1, -2, -3, -4]
        },
        {
          width: '10%',
          targets: [1, 2, 4]
        },
        {
          orderable: false,
          targets: 0,
        }
      ],
      "initComplete": function() {
        $('.submit-button').attr('disabled', false);
      }
    };

    $('#datatable-report-piutang').DataTable(config);
    // if (!$.fn.DataTable.isDataTable('#datatable-report-piutang')) {
    // } else {
    //   // $('#datatable-report-piutang').dataTable().draw();
    // }
  }

  function print_excel() {
    var form = document.createElement("form");
    form.method = "POST";
    form.action = '<?= base_url('report_piutang/excel'); ?>';
    form.target = '_blank';

    var customer_id = document.createElement("input");
    customer_id.value = $('#customer_id').val() || '';
    customer_id.name = 'customer_id';
    customer_id.type = 'hidden';
    form.appendChild(customer_id);

    var region_id = document.createElement("input");
    region_id.value = $('#region_id').val() || '';
    region_id.name = 'region_id';
    region_id.type = 'hidden';
    form.appendChild(region_id);

    var filter_month = document.createElement("input");
    filter_month.value = $('#filter_month').val() || '';
    filter_month.name = 'filter_month';
    filter_month.type = 'hidden';
    form.appendChild(filter_month);

    var filter_year = document.createElement("input");
    filter_year.value = $('#filter_year').val() || '';
    filter_year.name = 'filter_year';
    filter_year.type = 'hidden';
    form.appendChild(filter_year);

    // var url = '<?= base_url('report_piutang/excel'); ?>';
    // var customer_id = $('#customer_id').val() || '';
    // var region_id = $('#region_id').val() || '';
    // var filter_month = $('#filter_month').val() || '';
    // var filter_year = $('#filter_year').val() || '';
    // var form = $("<form action='" + url + "' method='post' target='_blank'><input type='hidden' name='customer_id' value='" + customer_id + "' /></form>");
    $('body').append(form);
    form.submit();
  }

  $(document).ready(function() {
    $('.submit-button').on('click', function(e) {
      // e.preventDefault();
      $('.submit-button').attr('disabled', true);
      // $('#datatable-report-piutang').dataTable().fnClearTable();
      // $('#datatable-report-piutang').dataTable().fnDestroy();
      // load_list();


      swal({
        text: 'Loading Data',
        showCancelButton: false,
        showConfirmButton: false,
        imageUrl: '<?= base_url('assets/images/ajax-loader.gif'); ?>',
        confirmButtonText: 'Ok',
        allowOutsideClick: false
      });

      $.ajax({
        method: 'POST',
        data: {
          customer_id: $('#customer_id').val() || '',
          region_id: $('#region_id').val() || '',
          filter_month: $('#filter_month').val() || '',
          filter_year: $('#filter_year').val() || ''
        },
        url: '<?= base_url('report_piutang/rows'); ?>'
      }).done(function(response) {
        // console.log(response);
        $('tbody').html(response.data.rows);
        $('.submit-button').attr('disabled', false);
        swal.close();
      }).fail(function() {
        $('.submit-button').attr('disabled', false);
        swal.close();
      });

    });
  });
</script>