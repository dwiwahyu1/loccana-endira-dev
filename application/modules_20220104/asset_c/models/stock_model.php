<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Stock_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
    
	
	public function get_price($params = array()){
		
		$query = $this->db->get_where('m_material', array('id_mat' => $params['id_mat']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	
	
    // public function get_material($id_mat) {
    	// $query = $this->db->get_where('m_material', array('id_mat' => $id_mat));
		
		// $return = $query->result_array();

		// $this->db->close();
		// $this->db->initialize();

		// return $return;
	// }
	
   public function get_material($id_mat) {
		$sql 	= 'SELECT a.*, b.uom_symbol, c.name_eksternal FROM m_material a
					JOIN m_uom b ON a.unit_terkecil=b.id_uom
					JOIN t_eksternal c ON a.dist_id=c.id
					where a.id_mat = "'.$id_mat.'" ';

		$query_all 	=  $this->db->query($sql);

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
	   public function get_stock_data($query_full, $qry) {
		$sql 	= $query_full.' '.$qry;

		$query_all 	=  $this->db->query($sql);

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
	public function get_stock_data_range($params) {
		$sql = 	"
	
				
				SELECT a.*,b.coa,b.keterangan, if(a.id_coa = 47,4,if(a.id_coa = 50,8,if(a.id_coa = 53,20,0))) bln,valuea,valuea2 
				FROM t_coa_value a JOIN
				t_coa b ON a.id_coa = b.id_coa 
				LEFT JOIN(  
				SELECT SUM(value_real) valuea, a.id_parent FROM t_coa_value a JOIN
				t_coa b ON a.id_coa = b.id_coa WHERE b.id_parent = 46 AND a.id_parent <> 0 AND a.date <= '".(date('Y')-1)."-12-31' 
				GROUP BY id_parent
				) c ON a.id = c.id_parent
				LEFT JOIN(  
				SELECT SUM(value_real) valuea2, a.id_parent FROM t_coa_value a JOIN
				t_coa b ON a.id_coa = b.id_coa WHERE b.id_parent = 46 AND a.id_parent <> 0 AND a.date BETWEEN '".date('Y')."-01-01' AND NOW() 
				GROUP BY id_parent
				) d ON a.id = d.id_parent
				LEFT JOIN t_coa_value e ON a.id_parent = e.id
				WHERE b.id_parent = 46 AND (a.id_parent = 0 OR e.id_coa NOT BETWEEN 46 AND 56 )
				ORDER BY a.date,b.id_coa,a.note
				
				
			";

		//  echo"<pre>"; print_r($sql);die;
		 

		$query_all 	=  $this->db->query($sql);

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}

	public function get_stock_data_range_edit($params) {
		$sql = 	"
	
				
				SELECT a.*,b.coa,b.keterangan, if(a.id_coa = 47,4,if(a.id_coa = 50,8,if(a.id_coa = 53,20,0))) bln,valuea,valuea2 
				FROM t_coa_value a 
				JOIN t_coa b ON a.id_coa = b.id_coa 
				LEFT JOIN(  
				SELECT SUM(value_real) valuea, a.id_parent 
				FROM t_coa_value a 
				JOIN t_coa b ON a.id_coa = b.id_coa 
				WHERE b.id_parent = 46 
				AND a.id_parent <> 0 
				AND a.date <= '".(date('Y')-1)."-12-31' 
				and a.id = ".$params."
				GROUP BY id_parent
				) c ON a.id = c.id_parent
				LEFT JOIN(  
				SELECT SUM(value_real) valuea2, a.id_parent 
				FROM t_coa_value a 
				JOIN t_coa b ON a.id_coa = b.id_coa 
				WHERE b.id_parent = 46 
				AND a.id_parent <> 0 
				AND a.date BETWEEN '".date('Y')."-01-01' AND NOW() 
				and a.id = ".$params."
				GROUP BY id_parent
				) d ON a.id = d.id_parent
				LEFT JOIN t_coa_value e ON a.id_parent = e.id
				WHERE b.id_parent = 46 
				AND (a.id_parent = 0 OR e.id_coa NOT BETWEEN 46 AND 56 )
				and a.id = ".$params."
				ORDER BY a.date,b.id_coa,a.note
				
				
			";

		//  echo"<pre>"; print_r($sql);die;
		 

		$query_all 	=  $this->db->query($sql);

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	public function lists($params = array()) {
		$query = 	'
				SELECT COUNT(*) AS jumlah FROM m_material a
					JOIN m_uom b ON a.unit_terkecil=b.id_uom
					JOIN t_eksternal c ON a.dist_id=c.id where 1 = 1
				AND ( 
					stock_code LIKE "%'.$params['searchtxt'].'%" OR
					stock_name LIKE "%'.$params['searchtxt'].'%" 
				)
			';
						
			// $query2 = 	'
				// SELECT z.*, rank() over ( ORDER BY stock_name ASC) AS Rangking from ( 
					
					// SELECT a.id_mat,a.stock_code,a.qty,a.qty_big,a.stock_name,a.top_price,a.bottom_price,a.base_price,
					// a.stock_description,a.base_qty,b.uom_name,b.uom_symbol,a.unit_box,c.name_eksternal,c.kode_eksternal ,
					// (a.base_qty*a.unit_box)/convertion box_per_lt 
					// FROM m_material a
					// JOIN m_uom b ON a.unit_terkecil=b.id_uom
					// JOIN t_eksternal c ON a.dist_id=c.id
					// LEFT JOIN `t_uom_convert` d ON b.id_uom = d.`id_uom`
					// WHERE  ( 
					// stock_code LIKE "%'.$params['searchtxt'].'%" OR
					// stock_name LIKE "%'.$params['searchtxt'].'%" 
				// )  order by stock_name) z
				// ORDER BY name_eksternal,stock_name ASC
				
				// LIMIT '.$params['limit'].' 
				// OFFSET '.$params['offset'].' 
			// ';
			
			
			$query2 = 	"
				SELECT z.*, rank() over ( ORDER BY stock_name ASC) AS Rangking from ( 
				
					SELECT a.*,`kode_eksternal`,`name_eksternal`,(a.base_qty*a.unit_box)/convertion box_per_lt ,uom.*,b.amount_mutasi_fix AS saldo_awal,b.mutasi_big_fix AS saldo_awal_big,
					 IF(c.amount_mutasi_fix IS NULL,0,c.amount_mutasi_fix)AS pemasukan,IF(c.mutasi_big_fix IS NULL,0,c.mutasi_big_fix) AS pemasukan_big,
					 IF(d.amount_mutasi_fix IS NULL,0,d.amount_mutasi_fix)AS pengeluaran_01,IF(d.mutasi_big_fix IS NULL,0,d.mutasi_big_fix) AS pengeluaran_big_01,
					 IF(e.amount_mutasi_fix IS NULL,0,e.amount_mutasi_fix)AS pengeluaran_02,IF(e.mutasi_big_fix IS NULL,0,e.mutasi_big_fix) AS pengeluaran_big_02,
					 IF(f.amount_mutasi_fix IS NULL,0,f.amount_mutasi_fix)AS pengeluaran_03,IF(f.mutasi_big_fix IS NULL,0,f.mutasi_big_fix) AS pengeluaran_big_03,
					 IF(g.amount_mutasi_fix IS NULL,0,g.amount_mutasi_fix)AS pengeluaran_04,IF(g.mutasi_big_fix IS NULL,0,g.mutasi_big_fix) AS pengeluaran_big_04,
					 b.amount_mutasi_fix + IF(c.amount_mutasi_fix IS NULL,0,c.amount_mutasi_fix)- IF(d.amount_mutasi_fix IS NULL,0,d.amount_mutasi_fix)AS saldo_akhir,
					 b.mutasi_big_fix + IF(c.mutasi_big_fix IS NULL,0,c.mutasi_big_fix)- IF(d.mutasi_big_fix IS NULL,0,d.mutasi_big_fix)AS saldo_akhir_big
					 FROM m_material a
					 JOIN `m_uom` uom ON a.`unit_terkecil` = uom.`id_uom`
					 JOIN `t_eksternal` ek ON a.`dist_id` = ek.`id`
					 LEFT JOIN `t_uom_convert` uc ON uom.id_uom = uc.`id_uom`
					LEFT JOIN (
					SELECT a.id_stock_awal,
					IF(a.amount_mutasi IS NULL,0,a.amount_mutasi)-IF(b.amount_mutasi IS NULL,0,b.amount_mutasi) AS amount_mutasi_fix,
					IF(a.mutasi_big IS NULL,0,a.mutasi_big)-IF(b.mutasi_big IS NULL,0,b.mutasi_big) AS mutasi_big_fix,
					IF(a.qty_sol IS NULL,0,a.qty_sol)-IF(b.qty_sol IS NULL,0,b.qty_sol) AS qty_sol_fix,
					IF(a.qty_sol_big IS NULL,0,a.qty_sol_big)-IF(b.qty_sol_big IS NULL,0,b.qty_sol_big) AS qty_sol_big_fix,
					IF(a.qty_bonus IS NULL,0,a.qty_bonus)-IF(b.qty_bonus IS NULL,0,b.qty_bonus) AS qty_bonus_fix,
					IF(a.qty_bonus_big IS NULL,0,a.qty_bonus_big)-IF(b.qty_bonus_big IS NULL,0,b.qty_bonus_big) AS qty_bonus_big_fix,
					IF(a.qty_titip IS NULL,0,a.qty_titip)-IF(b.qty_titip IS NULL,0,b.qty_titip) AS qty_titip_fix,
					IF(a.qty_titip_big IS NULL,0,a.qty_titip_big)-IF(b.qty_titip_big IS NULL,0,b.qty_titip_big) AS qty_titip_big_fix
					FROM ( SELECT id_stock_awal,
					SUM(a.`amount_mutasi`) amount_mutasi, 
					SUM(a.`mutasi_big`) mutasi_big,
					SUM(a.`qty_sol`) qty_sol,
					SUM(a.`qty_sol_big`) qty_sol_big,
					SUM(a.`qty_bonus`) qty_bonus,
					SUM(a.`qty_bonus_big`) qty_bonus_big,
					SUM(a.`qty_titip`) qty_titip,
					SUM(a.`qty_titip_big`) qty_titip_big
					FROM t_mutasi a 
					WHERE a.`type_mutasi`=0 AND a.`date_mutasi` <'".date('Y-m-d')."'
					GROUP BY a.`id_stock_awal`) a LEFT JOIN ( SELECT id_stock_awal,
					SUM(a.`amount_mutasi`) amount_mutasi, 
					SUM(a.`mutasi_big`) mutasi_big,
					SUM(a.`qty_sol`) qty_sol,
					SUM(a.`qty_sol_big`) qty_sol_big,
					SUM(a.`qty_bonus`) qty_bonus,
					SUM(a.`qty_bonus_big`) qty_bonus_big,
					SUM(a.`qty_titip`) qty_titip,
					SUM(a.`qty_titip_big`) qty_titip_big
					FROM t_mutasi a
					WHERE a.`type_mutasi`=1 AND a.`date_mutasi` < '".date('Y-m-d')."'
					GROUP BY a.`id_stock_awal`) b ON a.id_stock_awal=b.id_stock_awal) b ON a.id_mat=b.id_stock_awal
					LEFT JOIN 
					(
					SELECT a.id_stock_awal,
					IF(a.amount_mutasi IS NULL,0,a.amount_mutasi) AS amount_mutasi_fix,
					IF(a.mutasi_big IS NULL,0,a.mutasi_big) AS mutasi_big_fix,
					IF(a.qty_sol IS NULL,0,a.qty_sol) AS qty_sol_fix,
					IF(a.qty_sol_big IS NULL,0,a.qty_sol_big)AS qty_sol_big_fix,
					IF(a.qty_bonus IS NULL,0,a.qty_bonus)AS qty_bonus_fix,
					IF(a.qty_bonus_big IS NULL,0,a.qty_bonus_big) AS qty_bonus_big_fix,
					IF(a.qty_titip IS NULL,0,a.qty_titip) AS qty_titip_fix,
					IF(a.qty_titip_big IS NULL,0,a.qty_titip_big) AS qty_titip_big_fix
					FROM ( SELECT id_stock_awal,
					IF(SUM(a.`amount_mutasi`) IS NULL,0,SUM(a.`amount_mutasi`)) amount_mutasi, 
					IF(SUM(a.`mutasi_big`) IS NULL,0,SUM(a.`mutasi_big`)) mutasi_big,
					IF(SUM(a.`qty_sol`) IS NULL,0,SUM(a.`qty_sol`)) qty_sol,
					IF(SUM(a.`qty_sol_big`) IS NULL,0,SUM(a.`qty_sol_big`)) qty_sol_big,
					IF(SUM(a.`qty_bonus`) IS NULL,0,SUM(a.`qty_bonus`)) qty_bonus,
					IF(SUM(a.`qty_bonus_big`) IS NULL,0,SUM(a.`qty_bonus_big`)) qty_bonus_big,
					IF(SUM(a.`qty_titip`) IS NULL,0,SUM(a.`qty_titip`)) qty_titip,
					IF(SUM(a.`qty_titip_big`) IS NULL,0,SUM(a.`qty_titip_big`)) qty_titip_big
					FROM t_mutasi a 
					WHERE a.`type_mutasi`=0 AND a.`date_mutasi` BETWEEN '".date('Y-m-d')."' AND '".date('Y-m-d')."'
					GROUP BY a.`id_stock_awal`) a )c ON a.id_mat=c.id_stock_awal
					LEFT JOIN 
					(
					SELECT a.id_stock_awal,
					IF(a.amount_mutasi IS NULL,0,a.amount_mutasi) AS amount_mutasi_fix,
					IF(a.mutasi_big IS NULL,0,a.mutasi_big) AS mutasi_big_fix,
					IF(a.qty_sol IS NULL,0,a.qty_sol) AS qty_sol_fix,
					IF(a.qty_sol_big IS NULL,0,a.qty_sol_big)AS qty_sol_big_fix,
					IF(a.qty_bonus IS NULL,0,a.qty_bonus)AS qty_bonus_fix,
					IF(a.qty_bonus_big IS NULL,0,a.qty_bonus_big) AS qty_bonus_big_fix,
					IF(a.qty_titip IS NULL,0,a.qty_titip) AS qty_titip_fix,
					IF(a.qty_titip_big IS NULL,0,a.qty_titip_big) AS qty_titip_big_fix
					FROM ( SELECT id_stock_awal,
					IF(SUM(a.`amount_mutasi`) IS NULL,0,SUM(a.`amount_mutasi`)) amount_mutasi, 
					IF(SUM(a.`mutasi_big`) IS NULL,0,SUM(a.`mutasi_big`)) mutasi_big,
					IF(SUM(a.`qty_sol`) IS NULL,0,SUM(a.`qty_sol`)) qty_sol,
					IF(SUM(a.`qty_sol_big`) IS NULL,0,SUM(a.`qty_sol_big`)) qty_sol_big,
					IF(SUM(a.`qty_bonus`) IS NULL,0,SUM(a.`qty_bonus`)) qty_bonus,
					IF(SUM(a.`qty_bonus_big`) IS NULL,0,SUM(a.`qty_bonus_big`)) qty_bonus_big,
					IF(SUM(a.`qty_titip`) IS NULL,0,SUM(a.`qty_titip`)) qty_titip,
					IF(SUM(a.`qty_titip_big`) IS NULL,0,SUM(a.`qty_titip_big`)) qty_titip_big
					FROM t_mutasi a 
					JOIN d_penjualan b ON a.id_mutasi=b.mutasi_id
					JOIN t_penjualan c ON b.id_penjualan=c.id_penjualan
					JOIN t_customer d ON c.`id_customer`=d.id_t_cust
					WHERE a.`type_mutasi`=1 AND a.`date_mutasi` BETWEEN '".date('Y-m-d')."' AND '".date('Y-m-d')."' AND d.region=1
					GROUP BY a.`id_stock_awal`) a )d ON a.id_mat=d.id_stock_awal
					LEFT JOIN 
					(
					SELECT a.id_stock_awal,
					IF(a.amount_mutasi IS NULL,0,a.amount_mutasi) AS amount_mutasi_fix,
					IF(a.mutasi_big IS NULL,0,a.mutasi_big) AS mutasi_big_fix,
					IF(a.qty_sol IS NULL,0,a.qty_sol) AS qty_sol_fix,
					IF(a.qty_sol_big IS NULL,0,a.qty_sol_big)AS qty_sol_big_fix,
					IF(a.qty_bonus IS NULL,0,a.qty_bonus)AS qty_bonus_fix,
					IF(a.qty_bonus_big IS NULL,0,a.qty_bonus_big) AS qty_bonus_big_fix,
					IF(a.qty_titip IS NULL,0,a.qty_titip) AS qty_titip_fix,
					IF(a.qty_titip_big IS NULL,0,a.qty_titip_big) AS qty_titip_big_fix
					FROM ( SELECT id_stock_awal,
					IF(SUM(a.`amount_mutasi`) IS NULL,0,SUM(a.`amount_mutasi`)) amount_mutasi, 
					IF(SUM(a.`mutasi_big`) IS NULL,0,SUM(a.`mutasi_big`)) mutasi_big,
					IF(SUM(a.`qty_sol`) IS NULL,0,SUM(a.`qty_sol`)) qty_sol,
					IF(SUM(a.`qty_sol_big`) IS NULL,0,SUM(a.`qty_sol_big`)) qty_sol_big,
					IF(SUM(a.`qty_bonus`) IS NULL,0,SUM(a.`qty_bonus`)) qty_bonus,
					IF(SUM(a.`qty_bonus_big`) IS NULL,0,SUM(a.`qty_bonus_big`)) qty_bonus_big,
					IF(SUM(a.`qty_titip`) IS NULL,0,SUM(a.`qty_titip`)) qty_titip,
					IF(SUM(a.`qty_titip_big`) IS NULL,0,SUM(a.`qty_titip_big`)) qty_titip_big
					FROM t_mutasi a 
					JOIN d_penjualan b ON a.id_mutasi=b.mutasi_id
					JOIN t_penjualan c ON b.id_penjualan=c.id_penjualan
					JOIN t_customer d ON c.`id_customer`=d.id_t_cust
					WHERE a.`type_mutasi`=1 AND a.`date_mutasi` BETWEEN '".date('Y-m-d')."' AND '".date('Y-m-d')."' AND d.region=2
					GROUP BY a.`id_stock_awal`) a )e ON a.id_mat=e.id_stock_awal
					LEFT JOIN 
					(
					SELECT a.id_stock_awal,
					IF(a.amount_mutasi IS NULL,0,a.amount_mutasi) AS amount_mutasi_fix,
					IF(a.mutasi_big IS NULL,0,a.mutasi_big) AS mutasi_big_fix,
					IF(a.qty_sol IS NULL,0,a.qty_sol) AS qty_sol_fix,
					IF(a.qty_sol_big IS NULL,0,a.qty_sol_big)AS qty_sol_big_fix,
					IF(a.qty_bonus IS NULL,0,a.qty_bonus)AS qty_bonus_fix,
					IF(a.qty_bonus_big IS NULL,0,a.qty_bonus_big) AS qty_bonus_big_fix,
					IF(a.qty_titip IS NULL,0,a.qty_titip) AS qty_titip_fix,
					IF(a.qty_titip_big IS NULL,0,a.qty_titip_big) AS qty_titip_big_fix
					FROM ( SELECT id_stock_awal,
					IF(SUM(a.`amount_mutasi`) IS NULL,0,SUM(a.`amount_mutasi`)) amount_mutasi, 
					IF(SUM(a.`mutasi_big`) IS NULL,0,SUM(a.`mutasi_big`)) mutasi_big,
					IF(SUM(a.`qty_sol`) IS NULL,0,SUM(a.`qty_sol`)) qty_sol,
					IF(SUM(a.`qty_sol_big`) IS NULL,0,SUM(a.`qty_sol_big`)) qty_sol_big,
					IF(SUM(a.`qty_bonus`) IS NULL,0,SUM(a.`qty_bonus`)) qty_bonus,
					IF(SUM(a.`qty_bonus_big`) IS NULL,0,SUM(a.`qty_bonus_big`)) qty_bonus_big,
					IF(SUM(a.`qty_titip`) IS NULL,0,SUM(a.`qty_titip`)) qty_titip,
					IF(SUM(a.`qty_titip_big`) IS NULL,0,SUM(a.`qty_titip_big`)) qty_titip_big
					FROM t_mutasi a 
					JOIN d_penjualan b ON a.id_mutasi=b.mutasi_id
					JOIN t_penjualan c ON b.id_penjualan=c.id_penjualan
					JOIN t_customer d ON c.`id_customer`=d.id_t_cust
					WHERE a.`type_mutasi`=1 AND a.`date_mutasi` BETWEEN '".date('Y-m-d')."' AND '".date('Y-m-d')."' AND d.region=3
					GROUP BY a.`id_stock_awal`) a )f ON a.id_mat=f.id_stock_awal
					LEFT JOIN 
					(
					SELECT a.id_stock_awal,
					IF(a.amount_mutasi IS NULL,0,a.amount_mutasi) AS amount_mutasi_fix,
					IF(a.mutasi_big IS NULL,0,a.mutasi_big) AS mutasi_big_fix,
					IF(a.qty_sol IS NULL,0,a.qty_sol) AS qty_sol_fix,
					IF(a.qty_sol_big IS NULL,0,a.qty_sol_big)AS qty_sol_big_fix,
					IF(a.qty_bonus IS NULL,0,a.qty_bonus)AS qty_bonus_fix,
					IF(a.qty_bonus_big IS NULL,0,a.qty_bonus_big) AS qty_bonus_big_fix,
					IF(a.qty_titip IS NULL,0,a.qty_titip) AS qty_titip_fix,
					IF(a.qty_titip_big IS NULL,0,a.qty_titip_big) AS qty_titip_big_fix
					FROM ( SELECT id_stock_awal,
					IF(SUM(a.`amount_mutasi`) IS NULL,0,SUM(a.`amount_mutasi`)) amount_mutasi, 
					IF(SUM(a.`mutasi_big`) IS NULL,0,SUM(a.`mutasi_big`)) mutasi_big,
					IF(SUM(a.`qty_sol`) IS NULL,0,SUM(a.`qty_sol`)) qty_sol,
					IF(SUM(a.`qty_sol_big`) IS NULL,0,SUM(a.`qty_sol_big`)) qty_sol_big,
					IF(SUM(a.`qty_bonus`) IS NULL,0,SUM(a.`qty_bonus`)) qty_bonus,
					IF(SUM(a.`qty_bonus_big`) IS NULL,0,SUM(a.`qty_bonus_big`)) qty_bonus_big,
					IF(SUM(a.`qty_titip`) IS NULL,0,SUM(a.`qty_titip`)) qty_titip,
					IF(SUM(a.`qty_titip_big`) IS NULL,0,SUM(a.`qty_titip_big`)) qty_titip_big
					FROM t_mutasi a 
					JOIN d_penjualan b ON a.id_mutasi=b.mutasi_id
					JOIN t_penjualan c ON b.id_penjualan=c.id_penjualan
					JOIN t_customer d ON c.`id_customer`=d.id_t_cust
					WHERE a.`type_mutasi`=1 AND a.`date_mutasi` BETWEEN '".date('Y-m-d')."' AND '".date('Y-m-d')."' AND d.region=4
					GROUP BY a.`id_stock_awal`) a )g ON a.id_mat=g.id_stock_awal

				
				) z 
				ORDER BY name_eksternal,stock_name ASC
				
				LIMIT ".$params['limit']." 
				OFFSET ".$params['offset']." 
			";
		
		
			 $out		= array();
			  $querys		= $this->db->query($query);
			  $result = $querys->row();
			  
			  $total_filtered = $result->jumlah;
			  $total 			= $result->jumlah;
		  
				if(($params['offset']+10) > $total_filtered){
				$limit_data = $total_filtered - $params['offset'];
			  }else{
				$limit_data = $params['limit'] ;
			  }
		  
		
		  
		  //echo $query;die;
			//echo $query;die;
			 $query2s		= $this->db->query($query2);
		  $result2 = $query2s->result_array();						
		  $return = array(
			  'data' => $result2,
			  'total_filtered' => $total_filtered,
			  'total' => $total,
		  );
		  return $return;
	}

	public function list_mutasi($params = array()){
		
		$query = 	'
				SELECT COUNT(*) AS jumlah 
				FROM t_mutasi a
				JOIN m_material b ON a.id_stock_awal=b.id_mat
				JOIN m_uom c ON b.unit_terkecil=c.id_uom
				JOIN t_eksternal d ON b.dist_id=d.id 
				where a.id_stock_awal = '.$params['id_mat'].'
				AND ( 
					stock_code LIKE "%'.$params['searchtxt'].'%" OR
					stock_name LIKE "%'.$params['searchtxt'].'%" 
				)
			';
					
		$query2 = 	'
			SELECT z.*, rank() over ( ORDER BY stock_code ASC) AS Rangking from ( 
				SELECT b.id_mat,b.stock_code,b.qty,b.stock_name,
        b.stock_description,c.uom_name,d.name_eksternal,
        a.amount_mutasi,a.date_mutasi,a.type_mutasi,a.note, a.mutasi_big
				FROM t_mutasi a
				JOIN m_material b ON a.id_stock_awal=b.id_mat
				JOIN m_uom c ON b.unit_terkecil=c.id_uom
				JOIN t_eksternal d ON b.dist_id=d.id  
				where a.id_stock_awal = '.$params['id_mat'].'
				AND ( 
					stock_code LIKE "%'.$params['searchtxt'].'%" OR
					stock_name LIKE "%'.$params['searchtxt'].'%"
			)  order by stock_code) z
			ORDER BY stock_code ASC
			
			LIMIT '.$params['limit'].' 
			OFFSET '.$params['offset'].' 
		';
	
		//echo $query2;die;
	
		 $out		= array();
		  $querys		= $this->db->query($query);
		  $result = $querys->row();
		  
		  $total_filtered = $result->jumlah;
		  $total 			= $result->jumlah;
	  
			if(($params['offset']+10) > $total_filtered){
			$limit_data = $total_filtered - $params['offset'];
		  }else{
			$limit_data = $params['limit'] ;
		  }
	  
	
	  
	  //echo $query;die;
		//echo $query;die;
		 $query2s		= $this->db->query($query2);
	  $result2 = $query2s->result_array();						
	  $return = array(
		  'data' => $result2,
		  'total_filtered' => $total_filtered,
		  'total' => $total,
	  );
	  return $return;
	
	
	}


	public function edit_price($data) {
		$datas = array(
			'top_price' => $data['top_price'],
			'bottom_price' => $data['bottom_price']
		);

		//	print_r($datas);die;
		$this->db->where('id_mat',$data['id_mat']);
		$this->db->update('m_material',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function get_convertion($params){
		
		$query = "
		
		SELECT a.*,IF(convertion IS NULL,1,convertion) satuan_b, (IF(convertion IS NULL,1,convertion)) / c.`base_qty` AS mult 
		FROM `m_uom` a
		LEFT JOIN `t_uom_convert` b ON a.id_uom = b.`id_uom`
		LEFT JOIN `m_material` c ON c.`unit_terkecil` = a.`id_uom`
		WHERE c.`id_mat` = '".$params."'
		
		";
		$query = $this->db->query($query);
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function add_mutasi($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'id_stock_awal' => $data['id_stock_awal'],
			'id_stock_akhir' => $data['id_stock_akhir'],
			'date_mutasi' => $data['date_mutasi'],
			'amount_mutasi' => $data['amount_mutasi_sm'],
			'mutasi_big' => $data['amount_mutasi'],
			'qty_sol_big' => $data['amount_mutasi'],
			'qty_sol' => $data['amount_mutasi_sm'],
			'note' => $data['note'],
			'type_mutasi' => $data['type_mutasi'],
			'user_id' => $data['user_id']
		);

		//	print_r($datas);die;

		$this->db->insert('t_mutasi',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}	
	
	public function update_depresiasi($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'value_real' => 0
		);

		//	print_r($datas);die;
		$array = array('id_parent =' => $data['id'], 'date >=' => $data['tgl_terjual']);
		$this->db->where($array);
		$this->db->update('t_coa_value',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function get_price_history($params = array()){
		
		$query=$this->db->order_by('id_ph','desc')
						->get_where('t_price_history', array('id_mat' => $params['id_mat']), 1,0);
	
		// $this->db->order_by('id_hl','asc');
		// $query=$this->db->get();
		$return = $query->result_array();

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $return;
	}

	public function update_price_app($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			
			'id_approval' => $data['id_approval'],
			'approval_date' => $data['approval_date']
		);

		//	print_r($datas);die;
		$this->db->where('id_ph',$data['id_ph']);
		$this->db->update('t_price_history',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function update_status($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			
			'status_harga' => $data['status_harga']
		);

		//	print_r($datas);die;
		$this->db->where('id_mat',$data['id_mat']);
		$this->db->update('m_material',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function deletes($id_mat) {
		$this->db->where('id_mat', $id_mat);
		$this->db->delete('m_material'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}
