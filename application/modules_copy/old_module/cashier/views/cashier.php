  <link href="<?php echo $path;?>assets/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
        <link href="<?php echo $path;?>assets/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">
		 <script src="<?php echo $path;?>assets/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>
       
       
<script>    
    
var total = 0;
var subtotals = 0;
var idbarcode = 0;		
var order  = 0;
var kembalian  = 0;
var sub  = 0;
var totalorder  = 0;
var namabarang = '';    
var databelanja = [];   
var isibelanja = [];
var sstotal = 0;
var saldo = 0;
var idcust;
var idbarang;
var dd;	
	
	jQuery(document).ready(function() {
		$(".select2").select2();   
    });
	
    function KeyPress(e) {
          var evtobj = window.event? event : e
          if (evtobj.keyCode == 77 && evtobj.ctrlKey){
               $("#idcard").removeAttr('disabled');
			
			     $('#idcard').focus();
          }
    }

    document.onkeydown = KeyPress;
    
	
    function bayarkan(val){
		var bayars = val-sstotal;
		  $("#kembalian").empty();
		 $('#kembalian').append(convertToRupiah(bayars));
	}
    function qtyv(val){
        
        console.log(val);
            sub = parseFloat(dd[1])*val;
            total = sub;
            $("#isibelanja").empty();
            $("#daftarbelanja").empty();
            $("#jmlorder").empty();
            $("#subtotal").empty();
            $('#subtotal').append(sub);
            
            $("#total").empty();
            $("#qty").val('');
            $('#idbarcodenya').empty();
            $("#namabarang").empty();
            $("#harga").empty();
        
		      			
        var data = {
            id : dd[0],
            namabarang : dd[2],
            qty : val,
            harga : dd[1],
            idbarcode : dd[3],
            total : total
          
        }	
        
        
        
        
        databelanja.push(data);
        totalorder = databelanja.length;
        $('#jmlorder').append(totalorder);

       sstotal = 0;
        databelanja.forEach(function(entry) {
            var i = 1;
            
             $('#daftarbelanja').append('<tr id="daftarbelanja">'
									+'<th>'+entry.namabarang+'  <input type="hidden" class="form-control" id="idbarang" name="idbarang[]" placeholder="payment" value="'+entry.id+'"><input type="hidden" class="form-control" id="namabarang" name="namabarang[]" placeholder="payment" value="'+entry.namabarang+'"></th>'
									+'<th>'+entry.idbarcode+'<input type="hidden" value="'+entry.idbarcode+'" id="idbarcode" name="idbarcode[]"></th>'
									+'<th>'+entry.qty+'<input type="hidden" value="'+entry.qty+'" id="qty" name="qty[]"></th>'
									+'<th>Rp. '+convertToRupiah(entry.harga)+'<input type="hidden" value="'+entry.harga+'" id="harga" name="harga[]"></th>'
									+'<th>Rp. '+convertToRupiah(entry.total)+'<input type="hidden" value="'+entry.total+'" id="total" name="total[]"></th>'
								+'</tr>');
            
            i++;
            sstotal = entry.total + sstotal;
            console.log(entry);
        });
        	  $('#total').append(convertToRupiah(sstotal));
        	  $('#totals').html('<input type="hidden" class="form-control" id="totals" name="totals" placeholder="payment" value="'+sstotal+'">');
        
            		
    }
	
	
	
	function cashback(){
		var bayar = $("#bayar").val();
		kembalian = 0;
		kembalian = bayar - sstotal;
		
		
		
		var datapost={
			"total"  	:   sstotal,
			// "subtotal"  :   sstotal,
			"databelanja"  :   databelanja
			};
			// console.log(datapost); return false;
		  $.ajax({
			type: "POST",
			url: "<?php echo base_url();?>cashier/save",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
			  if (response.status == "success") {
						swal({
							title: 'Kembalian Anda',
							text: kembalian,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function () {
							$('#daftarbelanja').empty();
							$('#total').empty();
							$("#daftarbelanja").empty();
							$("#jmlorder").empty();
							$("#subtotal").empty();
							$("#total").empty();
							$('#idbarcodenya').empty();
							$("#namabarang").empty();
							$("#harga").empty();
							  $("#kembalian").empty();
							  isibelanja =[]; 
							  databelanja = [];
							  sstotal = 0;
							  sub = 0;
							  total = 0;
						})
			  } else{
				swal("Failed!", response.message, "error");
				$("#loader").hide();
			  }
			}
		  });
		
		
		
		
	}
	
	
	function solution1(base64Data) {

		var arrBuffer = base64ToArrayBuffer(base64Data);

		// It is necessary to create a new blob object with mime-type explicitly set
		// otherwise only Chrome works like it should
		var newBlob = new Blob([arrBuffer], { type: "application/pdf" });

		// IE doesn't allow using a blob object directly as link href
		// instead it is necessary to use msSaveOrOpenBlob
		if (window.navigator && window.navigator.msSaveOrOpenBlob) {
			window.navigator.msSaveOrOpenBlob(newBlob);
			return;
		}

		// For other browsers: 
		// Create a link pointing to the ObjectURL containing the blob.
		var data = window.URL.createObjectURL(newBlob);

		var link = document.createElement('a');
		document.body.appendChild(link); //required in FF, optional for Chrome
		link.href = data;
		link.download = "file.pdf";
		link.click();
		window.URL.revokeObjectURL(data);
		link.remove();
	}

	function base64ToArrayBuffer(data) {
		var binaryString = window.atob(data);
		var binaryLen = binaryString.length;
		var bytes = new Uint8Array(binaryLen);
		for (var i = 0; i < binaryLen; i++) {
			var ascii = binaryString.charCodeAt(i);
			bytes[i] = ascii;
		}
		return bytes;
	};
	
	function cashbackprint(){
		var bayar = $("#bayar").val();
		kembalian = 0;
		kembalian = bayar - sstotal;
		
		
		
		
		var datapost={
			"total"  	:   sstotal,
			// "subtotal"  :   sstotal,
			"databelanja"  :   databelanja
			};
			// console.log(datapost); return false;
			  $.ajax({
				type: "POST",
				url: "<?php echo base_url();?>cashier/downloadPdf2",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				processData: false, 
				success: function(response) {
				
							swal({
								title: 'Kembalian Anda',
								text: kembalian,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () {
								$('#daftarbelanja').empty();
								$('#total').empty();
								$("#daftarbelanja").empty();
								$("#jmlorder").empty();
								$("#subtotal").empty();
								$("#total").empty();
								$('#idbarcodenya').empty();
								$("#namabarang").empty();
								$("#harga").empty();
								  $("#kembalian").empty();
								  isibelanja =[]; 
								  databelanja = [];
								  sstotal = 0;
								  sub = 0;
								  total = 0;
							})
				  
				},
				  error: function (xhr, ajaxOptions, thrownError) {
					// console.log(xhr.responseText);
					
					window.open(xhr.responseText, '_target');
					// alert(123);
				  }
			  });
		
		
		
	}
    
	function opengroup(val){
		 var datapost={
			"id"  :   val
		  };
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>cashier/opengroup",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				console.log(response);
			}
		  });
	}
	
	function convertToRupiah(angka){
		var rupiah = '';
		var angkarev = angka.toString().split('').reverse().join('');
		for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
		return rupiah.split('',rupiah.length-1).reverse().join('');
	}
	function masukBarang(val){
		dd = val.split("_");
		
		
		
		isibelanja.push(dd);
		isibelanja.forEach(function(entry) {
             $('#isibelanja').append('<tr id="isibelanja">'
									+'<th>'+entry[2]+'</th>'
									+'<th>'+entry[3]+'</th>'
								+'</tr>');
            
           
        });
	}
	
	
</script>
<style>
.tt-query, /* UPDATE: newer versions use tt-input instead of tt-query */
.tt-hint {
    width: 396px;
    height: 30px;
    padding: 8px 12px;
    font-size: 24px;
    line-height: 30px;
    border: 2px solid #ccc;
    border-radius: 8px;
    outline: none;
}

.tt-query { /* UPDATE: newer versions use tt-input instead of tt-query */
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
}

.tt-hint {
    color: #999;
}

.tt-menu { /* UPDATE: newer versions use tt-menu instead of tt-dropdown-menu */
    width: 422px;
    margin-top: 12px;
    padding: 8px 0;
    background-color: #fff;
    border: 1px solid #ccc;
    border: 1px solid rgba(0, 0, 0, 0.2);
    border-radius: 8px;
    box-shadow: 0 5px 10px rgba(0,0,0,.2);
}

.tt-suggestion {
    padding: 3px 20px;
    font-size: 18px;
    line-height: 24px;
}

.tt-suggestion.tt-is-under-cursor { /* UPDATE: newer versions use .tt-suggestion.tt-cursor */
    color: #fff;
    background-color: #0097cf;

}

.tt-suggestion p {
    margin: 0;
}
</style>
</head>	
<body>
	<div class="container">
	
		<div class="row" style="margin-top:0px">
		
			<div class="col-md-6">
			<div class="panel panel-default">
				  <div class="panel-body">
							  
							  <a id='dwnldLnk' download='struk.pdf' style="display:none;" ></a> 
							  <!--div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">Paket</label>
								<div class="col-sm-10">
                                   <select class="form-control" id="sel1" onchange="opengroup(this.value)">
									<option selected disabled>Pilih Paket</option>
									<?php 
									// foreach($paket as $pk){
										// echo '<option value="'.$pk['id'].'">'.$pk['nama'].'</option>';
									// }
									?>
								  </select>
								</div>

							  </div-->  
							  <br/>
							  <br/>
							  <div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">Barang</label>
								<div class="col-sm-10">
                                    
									<select class="form-control select2" onchange="masukBarang(this.value)">
									<option selected disabled>Pilih Barang</option>
										<optgroup label="Kaca">
										<?php foreach($items as $it){
											echo '<option value="'.$it['id'].'_'.$it['harga'].'_'.$it['nama_barang'].'_'.$it['kode_barang'].'">'.$it['nama_barang'].'</option>';
										}?>
										
										</optgroup>
									</select>
								</div>

							  </div>  
							  <br/>
			
						<div class="row" style="margin-top: 40px;">
					
					<br/>
							<div class="col-md-6">
                                <table class="table table-hover">
								<thead>
									<tr>
										<th>Barang</th>
										<th>Code</th>
									</tr>
									
								</thead>
								<tbody id="isibelanja">
								
								</tbody>
								</table>
													
							</div>
							<div class="col-md-6">
                                
				<span id="idbarcodenya"></span>
				<span id="namabarang"></span>
				<span id="harga"></span>
				
							  <div class="form-group">
								<label for="inputEmail3" class="col-sm-2 control-label">QTY</label>
								<div class="col-sm-10">
								  <input type="text" class="form-control" id="qty" placeholder="ex.(5)" onchange="return qtyv(this.value)"   style="width: 170px; height: 150px; font-size:50px; text-align:center">
								</div>
							  </div>  									
							</div>
						</div>
						
				  </div>
				</div>
			
				
				<div class="panel panel-default">
				  <div class="panel-body">
						
						<h4>
							Discount : <span id="diskon"></span>
						</h4>
						<h4>
							Tax : <span id="tax"></span>
						</h4>
						<h4>
							Sub Total : <span id="subtotal"></span>
						</h4>
						<br/>
						<h1>
							Grand Total : <span id="total"></span>
						</h1>
						
						
						<h4>
							Kembalian : <span id="kembalian"></span>
						</h4>
						
				  </div>
				</div>
				
				
			</div>
			<div class="col-md-6" >
				
				<div class="panel panel-default" >
				  <div class="panel-body">
					  
					  	<form role="form" method="POST" action="<?php echo base_url();?>cashier/downloadPdf">
					<div class="col-md-12" style="border: 1px solid #ccc; margin-top: 30px; border-radius: 20px; background: #eee">
						  <div style="height: 350px; overflow: auto; overflow-y: true;">
							<table class="table table-hover">
							<thead>
								<tr>
									<th>Barang</th>
									<th>Code</th>
									<th>Qty</th>
									<th>Price</th>
									<th>Total</th>
								</tr>
								
							</thead>
							<tbody id="daftarbelanja">
							
							</tbody>
							</table>
							</div>
							
							
							
					  </div>
				  </div>
				</div>
			
				<div class="panel panel-default">
				  <div class="panel-body">
						
							  <div class="form-group">
								<h3><label for="inputEmail3" class="col-sm-2 control-label">Payment:</label></h3>
								<div class="col-sm-8" style="margin-left: 30px; margin-top:-3px" >
								  <input type="text" class="form-control" id="bayar" name="bayar" placeholder="payment" onchange="bayarkan(this.value)">
								   <span id="totals">
								</div>
							  </div>  
						
						
				  </div>
				</div>
				<div class="panel panel-default"> 
				  <div class="panel-body">
						
							  <div class="form-group">
							  
								<button type="submit" class="btn btn-primary col-sm-6">Pay & Print</button>
					</form>
								<button class="btn btn-danger col-sm-6"  onClick="cashback()" >Pay</button>
						
						
				  </div>
				</div>
				
			</div>
		</div>
	</div>
	