<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cashier_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		
	}
	
	
	
	public function save2($params = array()) {		

		
		for($i = 0; $i < count($params['idbarang']); $i++){
			
			$sql 	= 'INSERT INTO  t_transaksi (
												id_master,
												jumlah,
												total,
												no_transaksi,
												tanggal,
												pelanggan,
												id_kasir
											)
								VALUES
											(
												?,
												?,
												?,
												?,
												NOW(),
												?,
												?
											)';
											
			$hasil = $this->db->query($sql,array(
													$params['idbarang'][$i],
													$params['qty'][$i],
													"'".$params['total'][$i]."'",
													'TRX/'.date("Ymd-His"),
													
													0,
													$this->session->userdata['logged_in']['user_id'],
														
													)
										   );
		}
	
		
		if ($hasil) {
			return  $hasil;
		} 
		else {
			return false;
		}
	}
	
	public function save($params = array()) {		

		foreach($params['databelanja'] as $tah){
			$sql 	= 'INSERT INTO  t_transaksi (
												id_master,
												jumlah,
												total,
												no_transaksi,
												tanggal,
												pelanggan,
												id_kasir
											)
								VALUES
											(
												?,
												?,
												?,
												?,
												NOW(),
												?,
												?
											)';
											
			$hasil = $this->db->query($sql,array(
													$tah['id'],
													$tah['qty'],
													"'".$tah['total']."'",
													'TRX/'.date("Ymd-His"),
													
													0,
													$this->session->userdata['logged_in']['user_id'],
														
													)
										   );
		}
	
		
        
		if ($hasil) {
			return  $hasil;
		} 
		else {
			return false;
		}
	}
	
	public function search($params) {					
		$sql 	= "SELECT id, nama_barang as value, kode_barang FROM t_masterdata	WHERE (id LIKE '%".$params."%' OR nama_barang LIKE '%".$params."%' OR kode_barang LIKE '%".$params."%')";
        $query  = $this->db->query($sql);
		$result = $query->result_array();
		
		$this->db->close();
		$this->db->initialize();
        
        return $result;
	}	
			
				
	public function paket() {					
		$sql 	= "SELECT id, nama, category_id FROM t_inventorygroup";
        $query  = $this->db->query($sql);
		$result = $query->result_array();
		
		$this->db->close();
		$this->db->initialize();
        
        return $result;
	}		
	public function items() {					
		$sql 	= "SELECT id, nama_barang, harga, kode_barang FROM t_masterdata";
        $query  = $this->db->query($sql);
		$result = $query->result_array();
		
		$this->db->close();
		$this->db->initialize();
        
        return $result;
	}		
	public function opengroup() {					
		$sql 	= "SELECT b.* 
					FROM t_items a
					LEFT JOIN t_masterdata b ON a.`items_id` = b.`id`
					WHERE a.`inventory_id` = 1";
        $query  = $this->db->query($sql);
		$result = $query->result_array();
		
		$this->db->close();
		$this->db->initialize();
        
        return $result;
	}	
			
				
	
}	