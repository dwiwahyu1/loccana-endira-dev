<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cashier extends MX_Controller {

    public function __construct()
	{
		parent::__construct();	
		$this->load->model('Cashier_model');
	
	}
	
	public function index()
	{
			$data['items'] = $this->Cashier_model->items();
			$data['paket'] = $this->Cashier_model->paket();
			$this->template->load('maintemplate', 'cashier/views/cashier', $data);
		
	}
	
	public function test()
	{
		
			$this->template->load('maintemplate', 'cashier/views/testcashier');
		
	}
	
	public function downloadPdf2()
	{
		print_r($_POST); die;
		// $result = $this->Cashier_model->save($request);

		// if ( $result ) {			
		
			
					
					
					$html = '<html><head><title>Struck</title>';
					$html .= '<link href="'.base_url().'assets/adminto-14/adminto-14/Admin/Horizontal/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />';
							
					$html .= '</head><body>';
					

					
					
					
					

					$html .= '
								
								<table class="table">
										<tr>
											<th>
												<h4>Faktur Penjualan</h4>
												<h6>No Telepon : 022-200000</h6>
											</th>
											<th>
												<table class="table" border="0">
														<tr>
															<th>
																No Transaksi
															</th>
															<th>
																:
															</th>
															<th>
																TRX/'.date("Ymd-his").'
															</th>
														</tr>
														
														<tr>
															<th>
																Tanggal
															</th>
															<th>
																:
															</th>
															<th>
																'.date("Y/m/d").'
															</th>
														</tr>
														
														<tr>
															<th>
																Kasir
															</th>
															<th>
																:
															</th>
															<th>
																'.$this->session->userdata['logged_in']['nama'].'
															</th>
														</tr>
														
														<tr>
															<th>
																Alamat
															</th>
															<th>
																:
															</th>
															<th>
																Jln. Kiaracondong No 54 Bandung 
															</th>
														</tr>
														
												 </table>
											</th>
										</tr>
										
								 </table>';
								
								
						$html .= '		<table class="table table-bordered" border="1" style="text-align: right">
										<tr>
										  <th ><center>No</center></th>
										  <th ><center>Kode Barang</center></th>
										  <th ><center>Nama Barang</center></th>
										  <th ><center>Jumlah</center></th>
										  <th ><center>Harga</center></th>
										  <th ><center>Diskon</center></th>
										  <th ><center>Total</center></th>
										</tr>';
									
									$html .= '<tr>
										  <th ><center>1</center></th>
										  <th ><center>TRX/'.date("Ymd-his").'</center></th>
										  <th ><center>Kaca Belakang</center></th>
										  <th ><center>1</center></th>
										  <th ><center>Rp. 20.000.000</center></th>
										  <th ><center>0</center></th>
										  <th ><center>Rp. 20.000.000</center></th>
										</tr>';
										
										
										
							$html .= '			<tr>
										  <td colspan="6"><h4>Sub Total</h4></td>
										  <td ><h4>Rp. 20.000.000</h4></td>
										</tr>
										<tr>
										  <td colspan="6"><h3>Grand Total</h3></td>
										  <td ><h3>Rp. 20.000.000</h3></td>
										</tr>
										
								 </table>
								<table class="table">
										<tr>
											<th ></th>
										  <th ></th>
										  <th ></th>
										  <th ></th>
										  <th ></th>
										  <th ></th>
										  <th ></th>
										  <td colspan="6" style="margin-right: 10px"><center>
													<h5><i>Hormat Kami</i></h5>
													<br/>
													<br/>
													<br/>
													<br/>
													<h5>(...........................)</h5>
												</center></th>
										  <th ><center>
													<h5><i>Penerima</i></h5>
													<br/>
													<br/>
													<br/>
													<br/>
													<h5>(...........................)</h5>
												</center></th>
										</tr>
										
								 </table>
									
							  ';

					
					
					
					$html .= ' </table>';
					
					$html .= '</body></html>';
				// echo $html; die;
					require_once("dompdf/dompdf_config.inc.php");
					spl_autoload_register('DOMPDF_autoload');
					// $html = file_get_contents(APPPATH.'modules/invoice/views/invoice_view.php');

					$dompdf = new DOMPDF();
					$dompdf->set_paper('letter', 'landscape');
					$dompdf->load_html($html);
					$dompdf->render();

					$pdf = $dompdf->output();
					$destination = dirname($_SERVER["SCRIPT_FILENAME"]).'/uploads/invoice_pdf/';
					$filenamepdf = "data_pdf"; 
					$filepdf = $filenamepdf.".pdf"; 		

					$datafile = $destination.$filepdf;
					$dompdf->stream('Struck.pdf');
		
		// } else {
			// $result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			// $this->output->set_content_type('application/json')->set_output(json_encode($result));
		// }
		
		
	}
	public function downloadPdf()
	{
		$request 	= $_POST;
		// print_r($request); die;
		$result = $this->Cashier_model->save2($request);

		if ( $result ) {			
		
			
					
					
					$html = '<html><head><title>Struck</title>';
					$html .= '<link href="'.base_url().'assets/adminto-14/adminto-14/Admin/Horizontal/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />';
							
					$html .= '</head><body>';
					

					
					
					
					

					$html .= '
								
								<table class="table">
										<tr>
											<th>
												<h4>Faktur Penjualan</h4>
												<h6>No Telepon : 022-200000</h6>
											</th>
											<th>
												<table class="table" border="0">
														<tr>
															<th>
																No Transaksi
															</th>
															<th>
																:
															</th>
															<th>
																TRX/'.date("Ymd-his").'
															</th>
														</tr>
														
														<tr>
															<th>
																Tanggal
															</th>
															<th>
																:
															</th>
															<th>
																'.date("Y/m/d").'
															</th>
														</tr>
														
														<tr>
															<th>
																Kasir
															</th>
															<th>
																:
															</th>
															<th>
																'.$this->session->userdata['logged_in']['nama'].'
															</th>
														</tr>
														
														<tr>
															<th>
																Alamat
															</th>
															<th>
																:
															</th>
															<th>
																Jln. Kiaracondong No 54 Bandung 
															</th>
														</tr>
														
												 </table>
											</th>
										</tr>
										
								 </table>';
								
							$totals = 0;	
						$html .= '		<table class="table table-bordered" border="1" style="text-align: right">
										<tr>
										  <th ><center>No</center></th>
										  <th ><center>Kode Barang</center></th>
										  <th ><center>Nama Barang</center></th>
										  <th ><center>Jumlah</center></th>
										  <th ><center>Harga</center></th>
										  <th ><center>Diskon</center></th>
										  <th ><center>Total</center></th>
										</tr>';
							for($i = 0; $i < count($request['namabarang']); $i++){
								$html .= '<tr>
										  <th ><center>'.$i.'</center></th>
										  <th ><center>'.$request['idbarcode'][$i].'</center></th>
										  <th ><center>'.$request['namabarang'][$i].'</center></th>
										  <th ><center>'.$request['qty'][$i].'</center></th>
										  <th ><center>'.$request['harga'][$i].'</center></th>
										  <th ><center>0</center></th>
										  <th ><center>'.$request['total'][$i].'</center></th>
										</tr>';
								$totals += $request['total'][$i];
							}			
									
										
										
										
							$html .= '			<tr>
										  <td colspan="6"><h4>Sub Total</h4></td>
										  <td ><h4>Rp. '.$totals.'</h4></td>
										</tr>
										<tr>
										  <td colspan="6"><h3>Grand Total</h3></td>
										  <td ><h3>Rp. '.$totals.'</h3></td>
										</tr>
										
								 </table>
								<table class="table">
										<tr>
											<th ></th>
										  <th ></th>
										  <th ></th>
										  <th ></th>
										  <th ></th>
										  <th ></th>
										  <th ></th>
										  <td colspan="6" style="margin-right: 10px"><center>
													<h5><i>Hormat Kami</i></h5>
													<br/>
													<br/>
													<br/>
													<br/>
													<h5>(...........................)</h5>
												</center></th>
										  <th ><center>
													<h5><i>Penerima</i></h5>
													<br/>
													<br/>
													<br/>
													<br/>
													<h5>(...........................)</h5>
												</center></th>
										</tr>
										
								 </table>
									
							  ';

					
					
					
					$html .= ' </table>';
					
					$html .= '</body></html>';
				// echo $html; die;
					require_once("dompdf/dompdf_config.inc.php");
					spl_autoload_register('DOMPDF_autoload');
					// $html = file_get_contents(APPPATH.'modules/invoice/views/invoice_view.php');

					$dompdf = new DOMPDF();
					$dompdf->set_paper('letter', 'landscape');
					$dompdf->load_html($html);
					$dompdf->render();

					$pdf = $dompdf->output();
					$destination = dirname($_SERVER["SCRIPT_FILENAME"]).'/uploads/invoice_pdf/';
					$filenamepdf = "data_pdf"; 
					$filepdf = $filenamepdf.".pdf"; 		

					$datafile = $destination.$filepdf;
					$dompdf->stream('Struck.pdf');
		
		
			$result = array( 'success' => true, 'message' => 'Formula Sukses Tersimpan');
			
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			$result = array( 'success' => false, 'message' => 'Error Ketika Memasukan Ke Database' );
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
		
		
	}
	
	function search(){
		
		$params 	= $_POST['query'];
		
		$list = $this->Cashier_model->search($params);
		$arr = array();
		foreach($list as $ls){
			
			array_push($arr,$ls['value'].'-'.$ls['kode_barang']);
		}
        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
        echo json_encode($arr);
	}

	
	 function opengroup(){
		$data 	= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		$list = $this->Cashier_model->opengroup($params);
		
		if($list){
			$res = array(
				'status' => 'success',
				'message' => 'Data driver has been load',
				'data' => $list
			);
		}else{
			$res = array(
				'status' => 'error',
				'message' => 'Data driver error when deleted'
			);
		}
        
		
        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
        echo json_encode($res);
	}
	 
	 
	 function save(){
		$data 	= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		$list = $this->Cashier_model->save($params);
		
		if($list){
			$res = array(
				'status' => 'success',
				'message' => 'Data driver has been load',
				'data' => $list
			);
		}else{
			$res = array(
				'status' => 'error',
				'message' => 'Data driver error when deleted'
			);
		}
        
		
        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
        echo json_encode($res);
	}
	
}
