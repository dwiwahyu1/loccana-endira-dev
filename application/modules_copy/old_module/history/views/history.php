

<script src="<?php echo base_url();?>assets/highchart/code/highcharts.js"></script>
<script src="<?php echo base_url();?>assets/highchart/code/modules/exporting.js"></script>
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<br/>
				<div class="card-box table-responsive">
					<div class="row">
					
					<div class="col-lg-12">
							
							<div id="container" style="min-width: 100%; height: 400px; margin: 0 auto"></div>
						</div>
						<br/>
						<br/>
						<div class="col-lg-12">
							<h4 class="header-title m-t-0 m-b-30">Histori Transaksi</h4>	
							<table id="example" class="table table-striped table-bordered">
								<thead>
									<tr>
									
										<th>No Transaksi</th>
										<th>Kode Barang</th>
										<th>Nama Barang</th>
										<th>Harga</th>
										<th>Jumlah</th>
										<th>Total</th>
										<th>Tanggal</th>
										<th>Pelanggan</th>
										<th>Kasir</th>
									</tr>
								</thead>
							</table>
						</div>
					
						
					</div><!-- end col -->
				
				
				
				</div><!-- end row -->

			
		</div><!-- end card -->


	</div>
	<!-- end container -->

</div> <!-- content -->



<script type="text/javascript">
var bulan = ['Januari', 'Febuari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
var ss =[]
var tl =[]
<?php foreach($tahun as $th){
	?>
	ss.push(bulan[<?php echo $th['tanggal'];?>]);
	
<?php
}?>

<?php foreach($total as $tl){
	?>
	tl.push(<?php echo $tl['total'];?>);
	
<?php
}?>


console.log(tl);
Highcharts.chart('container', {
    chart: {
        type: 'area'
    },
    title: {
        text: 'Data Penjualan Perbulan'
    },
    subtitle: {
        text: ''
    },
    xAxis: {
        categories: ss,
        tickmarkPlacement: 'on',
        title: {
            enabled: false
        }
    },
    yAxis: {
        title: {
            text: 'Total'
        },
        labels: {
            formatter: function () {
                return this.value ;
            }
        }
    },
    tooltip: {
        split: true,
        valueSuffix: ' '
    },
    plotOptions: {
        area: {
            stacking: 'normal',
            lineColor: '#666666',
            lineWidth: 1,
            marker: {
                lineWidth: 1,
                lineColor: '#666666'
            }
        }
    },
    series: [{
        name: 'Pendapatan',
        data: tl
    }]
});
		</script>


<script>
    $( document ).ready(function() {
        $("#example").DataTable({
			 dom: 'Bfrtip',
			buttons: [
				{ extend:'copy', attr: { id: 'allan' } }, 'csv', 'excel', 'pdf', 'print'
			],
            "processing": true,
            "serverSide": true,
            destroy: true,
            "ajax": "<?php echo base_url().'history/lists'?>",
            "searchDelay": 700,
            responsive: true,
            "bFilter" : false,
            "bInfo" : false,
            "bLengthChange": false
        });

        
    });
	
</script>