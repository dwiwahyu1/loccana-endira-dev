<script>
	function savekategory(){
		
		var category   = $('#category').val();
		if(!category){
			
			$('#errortext').html('<span style="color:red"><i class="zmdi zmdi-assignment-alert"></i> You must fill the field</span>');
			setTimeout(function(){ $('#errortext').html(''); }, 3000);
		}else{
			$('#errortext').html('');
			var datapost={
			"category"  :   category
			};
			// console.log(datapost); return false;
			  $.ajax({
				type: "POST",
				url: "<?php echo base_url();?>kategori/save",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
				  if (response.status == "success") {
								swal({
                                        title: 'Success!',
                                        text: response.message,
                                        type: 'success',
                                        showCancelButton: false,
                                        confirmButtonText: 'Ok'
                                    }).then(function () {
                                       window.location.href = "<?php echo base_url('kategori');?>";
                                    })
				  } else{
					swal("Failed!", response.message, "error");
					$("#loader").hide();
				  }
				}
			  });
		}
		
	}
</script>

<div class="row">
	<div class="col-lg-12">
			<div class="form-group">
				<input type="text" class="form-control" id="category" name="category" placeholder="Enter category ..." required>
				<span id="errortext"></span>
			</div>
			<button class="btn btn-purple waves-effect waves-light col-lg-offset-9 col-sm-offset-9" onClick="savekategory()">Save</button>
		
	</div>
</div><!-- end col -->	

