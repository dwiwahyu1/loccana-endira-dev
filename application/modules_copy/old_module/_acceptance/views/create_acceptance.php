<style>
	.control-label{
		text-align: left !important;
	}
</style>
<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
		<br/>
				<div class="card-box table-responsive">
					<h3 class=" m-t-0 m-b-30">Create Acceptance</h3>	
					<div class="row">
						<div class="col-md-10">
							<form class="form-horizontal" role="form" id="submitacceptance">
								<div class="form-group">
									<label class="col-md-2 control-label">SMU</label>
									<div class="col-md-8">
										<input type="text" class="form-control" id="smu" name="smu" placeholder="Masukan kode SMU" required>
									</div>
									<div class="col-md-2">
										
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">Berat</label>
									<div class="col-md-10">
										<input type="text" class="form-control" id="berat" name="berat" placeholder="Masukan Berat barang" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">Volume</label>
									<div class="col-md-10">
										<input type="text" class="form-control" id="volume" name="volume" placeholder="Masukan Volume barang" required>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">Warehouse</label>
									<div class="col-md-10">
										<select class="form-control" id="warehouse" name="warehouse" required >
											<option disabled selected>Select Warehouse</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">Flight</label>
									<div class="col-md-10">
										<select class="form-control" id="flight" name="flight" required>
											<option disabled selected>Select Flight</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">Commundity</label>
									<div class="col-md-10">
										<select class="form-control" id="commundity" name="commundity" required>
											<option disabled selected>Select Commundity</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">Agent</label>
									<div class="col-md-10">
										<select class="form-control" id="agent" name="agent" required>
											<option disabled selected>Select Agent</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">Shipper</label>
									<div class="col-md-10">
										<select class="form-control" id="shipper" name="shipper" required>
											<option disabled selected>Select Shipper</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label">Destination</label>
									<div class="col-md-10">
										<select class="form-control" id="destination" name="destination" required>
											<option disabled selected>Select Destination</option>
										</select>
									</div>
								</div>
								
								<div class="row">
									<div class="col-sm-8">
									</div>
									<div class="col-sm-2">
										<button type="submit" class="btn btn-custom waves-effect w-md waves-light m-b-5">Submit</button>
									</div>
									<div class="col-sm-2">
										<a href="<?php echo base_url()?>acceptance" style="background: #dce0e5; border: 1px solid #dce0e5" class="btn btn-custom waves-effect w-md waves-light m-b-5">Cancel</a>
									</div>
								</div>
							</form>
							
						</div><!-- end col -->
					</div><!-- end row -->
				</div><!-- end row -->
		</div><!-- end card -->
	</div>
	<!-- end container -->
</div> <!-- content -->


<script>
    $( document ).ready(function() {
		
		$("#submitacceptance").on("submit", function (e) {
			e.preventDefault();
        
          $("#loader").show();
          var vsmu  			 = $('#smu').val();
          var vberat   	 		 = $('#berat ').val();
          var vvolume   		 = $('#volume').val();
          var vwarehouse   	 	 = $('#warehouse').val();
          var vflight   	 	 = $('#flight').val();
          var vcommundity   	 = $('#commundity').val();
          var vagent		   	 = $('#agent').val();
          var vshipper   	 	 = $('#shipper').val();
          var vdestination   	 = $('#destination').val();

          var datapost={
            "smu"  			:   vsmu,
            "berat" 		:   vberat,
            "volume"  		:   vvolume,
            "warehouse"  	:   vwarehouse,
            "flight"  		:   vflight,
            "commundity"  	:   vcommundity,
            "agent"  		:   vagent,
            "shipper"  		:   vshipper,
            "destination"   :   vdestination
          };
		  
		  // console.log(datapost); return false;

          $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>api/api_acceptance/saveacceptance",
            data : JSON.stringify(datapost),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function(response) {
              if (response.status == "success") {
								swal({
                                        title: 'Success!',
                                        text: response.message,
                                        type: 'success',
                                        showCancelButton: false,
                                        confirmButtonText: 'Ok'
                                    }).then(function () {
                                       window.location.href = "<?php echo base_url('acceptance');?>";
                                    })
				  } else{
					swal("Failed!", response.message, "error");
					$("#loader").hide();
				  }
            }
          });
        });
		
		
		
        $.ajax({
			type: "GET",
			url: "<?php echo base_url();?>api/api_acceptance/listwarehouse",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				console.log(response);
			  if (response.status == "success") {
				response.data.forEach(function(entry) {
					$('#warehouse').append("<option value='"+entry['id']+"'>"+entry['name']+"</option>");
				});
							
			  }
			}
		});
        $.ajax({
			type: "GET",
			url: "<?php echo base_url();?>api/api_acceptance/listflight",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				console.log(response);
			  if (response.status == "success") {
				response.data.forEach(function(entry) {
					$('#flight').append("<option value='"+entry['id']+"'>"+entry['name']+"</option>");
				});
							
			  }
			}
		});
        $.ajax({
			type: "GET",
			url: "<?php echo base_url();?>api/api_acceptance/listcommundity",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				console.log(response);
			  if (response.status == "success") {
				response.data.forEach(function(entry) {
					$('#commundity').append("<option value='"+entry['id']+"'>"+entry['name']+"</option>");
				});
							
			  }
			}
		});
        $.ajax({
			type: "GET",
			url: "<?php echo base_url();?>api/api_acceptance/listagent",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				console.log(response);
			  if (response.status == "success") {
				response.data.forEach(function(entry) {
					$('#agent').append("<option value='"+entry['id']+"'>"+entry['name']+"</option>");
				});
							
			  }
			}
		});
        $.ajax({
			type: "GET",
			url: "<?php echo base_url();?>api/api_acceptance/listshipper",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				console.log(response);
			  if (response.status == "success") {
				response.data.forEach(function(entry) {
					$('#shipper').append("<option value='"+entry['id']+"'>"+entry['name']+"</option>");
				});
							
			  }
			}
		});
        $.ajax({
			type: "GET",
			url: "<?php echo base_url();?>api/api_acceptance/listdestination",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				console.log(response);
			  if (response.status == "success") {
				response.data.forEach(function(entry) {
					$('#destination').append("<option value='"+entry['id']+"'>"+entry['name']+"</option>");
				});
							
			  }
			}
		});
        
    });
	
	
</script>