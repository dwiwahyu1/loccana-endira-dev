<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
		<br/>
			<div class="row">
				<div class="col-lg-12">
					<img src="<?php echo base_url();?>img/kuning tae1.png" style="width: 100%" />
				</div>
			</div>
		<br/>
		<br/>
					<div class="row">
						<div class="col-lg-4">
							<h4 class="header-title m-t-0 m-b-30">List Acceptance</h4>	
						</div>
						<div class="col-lg-8">
							<div class="form-group has-feedback">
								<div class="col-sm-6">
									<input type="text" class="form-control" placeholder="">
									<i class="fa fa-search form-control-feedback l-h-34" style="width: 64px"></i>
								</div>
								<div class="col-sm-1">
										 
								</div>
								<div class="col-sm-2">
										<div class="btn-group">
											<button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Filter By <span class="caret"></span></button>
											<ul class="dropdown-menu" role="menu">
												<li><a href="#">By All</a></li>
												<li><a href="#">By Per Day</a></li>
												<li><a href="#">By Per Week</a></li>
												<li><a href="#">By Per Month</a></li>
											</ul>
										</div>
								</div>
								<div class="col-sm-2">
								 <a href="<?php echo base_url()?>acceptance/create_acceptance" class="btn btn-success waves-effect w-md waves-light m-b-5"><i class="zmdi zmdi-plus"></i> Create Acceptance</a>
								</div>
							</div>
						</div>
						
					</div><!-- end col -->
				<div class="card-box table-responsive">
					<div class="row">
					
					
						<div class="col-lg-12">
							<table id="datatable" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>ID</th>
										<th>SMU</th>
										<th>Weight</th>
										<th>Volume</th>
										<th>Warehouse</th>
										<th>Flight</th>
										<th>Commundity</th>
										<th>Action</th>
										<th>Re-Print</th>
									</tr>
								</thead>
							</table>
						</div>
					</div><!-- end col -->
				
				
				
				</div><!-- end row -->

			
		</div><!-- end card -->


	</div>
	<!-- end container -->

</div> <!-- content -->


<script>
    $( document ).ready(function() {
         $("#datatable").DataTable({
			dom: 'Bfrtip',
			buttons: [
				'csv', 'excel', 'pdf', 'print'
			],
            "processing": true,
            "serverSide": true,
            destroy: true,
            "ajax": "<?php echo base_url().'api/api_acceptance/listacceptance'?>",
            "searchDelay": 700,
            responsive: true,
            "bFilter" : false,
            "bInfo" : false,
            "bLengthChange": false,
             "columnDefs": [
                            {"render": function ( data, type, row ) {
                                            return '<div class="dropdown" style="text-align:center">'
											+'<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">'
												+'<i class="zmdi zmdi-more"></i>'
											+'</a>'
											+'<ul class="dropdown-menu" role="menu">'
												+'<li><a href="javascript:void(0)" onClick="deletes('+row[0]+')">Delete</a></li>'
												+'<li><a href="javascript:void(0)" onClick="edites('+row[0]+')">Edit</a></li>'
												+'<li><a href="#">Design Report</a></li>'
											+'</ul>'
											+'</div>';
											


                                },
                                "targets": 7
                            },{"render": function ( data, type, row ) {
                                            return '<div class="btn-group">'
                                                +'<a href="javascript:void(0)" ><img class="tengah" src="<?php echo base_url();?>img/print.png"></i></a>'
                                                +'</div> ';


                                },
                                "targets": 8
                            }
                        ]
        });

        
    });
	
	function edites(id){
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load('<?php echo base_url('acceptance/editacceptance');?>'+'/'+id);
        $('#panel-modal  .modal-title').html('Edit Agent');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
	
    function deletes(id){
		swal({
            title: 'Are You Sure?',
			text: 'You will not be able to recover this data acceptance!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ok'
        }).then(function () {
            var datapost={
                "id"  :   id
              };
              $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>api/api_acceptance/deleteacceptance",
                data : JSON.stringify(datapost),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function(response) {
					
					
                  if (response.status == "success") {

                      swal({
                            title: 'Success!',
                            text: response.message,
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        }).then(function () {
                            window.location.href = "<?php echo base_url('acceptance');?>";
                        })



                  } else{
                    swal("Failed!", response.message, "error");
                  }
                }
              });
        })
		
    }
</script>