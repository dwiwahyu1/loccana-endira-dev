<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Acceptance extends MX_Controller {

    public function __construct()
	{
		parent::__construct();	
		//$this->load->model('Login_model');
	
	}
	
	public function index()
	{
		
			$this->template->load('maintemplate', 'acceptance/views/acceptance');
		
	}
	public function create_acceptance()
	{
		
			$this->template->load('maintemplate', 'acceptance/views/create_acceptance');
		
	}

	
}
