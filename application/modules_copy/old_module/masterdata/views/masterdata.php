<div class="content-page">
	<!-- Start content -->
	<div class="content">
		<div class="container">
			<br/>
				<div class="card-box table-responsive">
					<div class="row">
					
					
						<div class="col-lg-12">
							<h4 class="header-title m-t-0 m-b-30">List Masterdata</h4>	
							<button type="button" class="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-5 col-lg-offset-10 col-sm-offset-10" onClick="adds()"><i class="zmdi zmdi-plus-circle-o"></i> New Masterdata </button>
							<table id="example" class="table table-striped table-bordered">
								<thead>
									<tr>
										<th>ID</th>
										<th>Kode Barang</th>
										<th>Nama Barang</th>
										<th width="5">Discount</th>
										<th>Stok</th>
										<th>Harga</th>
										<th>Kategori</th>
										<th>Action</th>
									</tr>
								</thead>
							</table>
						</div>
					</div><!-- end col -->
				
				
				
				</div><!-- end row -->

			
		</div><!-- end card -->


	</div>
	<!-- end container -->

</div> <!-- content -->



<div id="panel-modal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="mySmallModalLabel"></h4>
			</div>
			<div class="modal-body">
			  
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<script>
    $( document ).ready(function() {
        $("#example").DataTable({
            "processing": true,
            "serverSide": true,
            destroy: true,
            "ajax": "<?php echo base_url().'masterdata/lists'?>",
            "searchDelay": 700,
            responsive: true,
            "bFilter" : false,
            "bInfo" : false,
            "bLengthChange": false,
            "columnDefs":	[
								{"render": function ( data, type, row ) {
												return '<div class="dropdown" style="text-align:center">'
												+'<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">'
													+'<i class="zmdi zmdi-more"></i>'
												+'</a>'
												+'<ul class="dropdown-menu" role="menu">'
													+'<li><a href="javascript:void(0)" onClick="deletes('+row[0]+')">Delete</a></li>'
													+'<li><a href="javascript:void(0)" onClick="edites('+row[0]+')">Edit</a></li>'
												+'</ul>'
												+'</div>';
												


									},
									"targets": 7
								}
							]
        });

        
    });
	
	
    function adds(){
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load('<?php echo base_url('masterdata/add');?>');
        $('#panel-modal  .modal-title').html('Add Masterdata');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
    function edites(id){
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .modal-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .modal-body').load('<?php echo base_url('masterdata/edit');?>'+'/'+id);
        $('#panel-modal  .modal-title').html('Edit Masterdata');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
    }
	
    function deletes(id){
		swal({
            title: 'Are You Sure!',
			text: 'You will not be able to recover this data agent!',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ok'
        }).then(function () {
            var datapost={
                "id"  :   id
              };
              $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>masterdata/deletes",
                data : JSON.stringify(datapost),
                dataType: 'json',
                contentType: 'application/json; charset=utf-8',
                success: function(response) {
					
					
                  if (response.status == "success") {

                      swal({
                            title: 'Success!',
                            text: response.message,
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'Ok'
                        }).then(function () {
                            window.location.href = "<?php echo base_url('masterdata');?>";
                        })



                  } else{
                    swal("Failed!", response.message, "error");
                  }
                }
              });
        })
		
    }
</script>