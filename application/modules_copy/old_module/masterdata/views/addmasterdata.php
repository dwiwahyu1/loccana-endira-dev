<script>
	function save(){
		
		var kodebarang   = $('#kodebarang').val();
		var namabarang   = $('#namabarang').val();
		var stokbarang   = $('#stokbarang').val();
		var diskon   = $('#diskon').val();
		var kategoribarang   = $('#kategoribarang').val();
		var harga   = $('#harga').val();
		if(!kodebarang){
			
			$('#errortext').html('<span style="color:red"><i class="zmdi zmdi-assignment-alert"></i> You must fill the field</span>');
			setTimeout(function(){ $('#errortext').html(''); }, 3000);
		}else{
			$('#errortext').html('');
			var datapost={
				"kodebarang"  :   kodebarang,
				"namabarang"  :   namabarang,
				"stokbarang"  :   stokbarang,
				"diskon"  :   diskon,
				"kategoribarang"  :   kategoribarang,
				"harga"  :   harga
			};
			// console.log(datapost); return false;
			  $.ajax({
				type: "POST",
				url: "<?php echo base_url();?>masterdata/save",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
				  if (response.status == "success") {
								swal({
                                        title: 'Success!',
                                        text: response.message,
                                        type: 'success',
                                        showCancelButton: false,
                                        confirmButtonText: 'Ok'
                                    }).then(function () {
                                       window.location.href = "<?php echo base_url('masterdata');?>";
                                    })
				  } else{
					swal("Failed!", response.message, "error");
					$("#loader").hide();
				  }
				}
			  });
		}
		
	}
	
	
	
	$(function () {
	  $("#stokbarang").keydown(function () {
		// Save old value.
		$(this).data("old", $(this).val());
	  });
	  $("#stokbarang").keyup(function () {
		// Check correct, else revert back to old value.
		if ( parseInt($(this).val()) >= 0)
		  ;
		else
		  $(this).val($(this).data("old"));
	  }); 
	  
	  $("#harga").keydown(function () {
		// Save old value.
		$(this).data("old", $(this).val());
	  });
	  $("#harga").keyup(function () {
		// Check correct, else revert back to old value.
		if ( parseInt($(this).val()) >= 0)
		  ;
		else
		  $(this).val($(this).data("old"));
	  });
	});
</script>

<div class="row">
	<div class="col-lg-12">
			<div class="form-horizontal">
				<div  class="form-group">
					<label for="kodebarang" class="col-sm-2 control-label">Kode Barang</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="kodebarang" name="kodebarang" placeholder="Kode Barang ..." required>
					</div>
				</div>
				<div  class="form-group">
					<label for="namabarang" class="col-sm-2 control-label">Nama Barang</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="namabarang" name="namabarang" placeholder="Nama Barang ..." required>
					</div>
				</div>
				<div  class="form-group">
					<label for="stokbarang" class="col-sm-2 control-label">Stok Barang</label>
					<div class="col-sm-10">
						<input type="number" class="form-control" id="stokbarang" name="stokbarang"  placeholder="Stok Barang ..." required>
					</div>
				</div>
				<div  class="form-group">
					<label for="diskon" class="col-sm-2 control-label">Diskon</label>
					<div class="col-sm-10">
						<input type="number" class="form-control" id="diskon" name="diskon"  placeholder="Diskon ..." required>
					</div>
				</div>
				<div  class="form-group">
					<label for="kategoribarang" class="col-sm-2 control-label">Kategori Barang</label>
					<div class="col-sm-10">
						<select class="form-control" id="kategoribarang">
							<option selected disabled>Pilih Kategori</option>
							<?php if(isset($kategori)){
								foreach($kategori as $kat){
									echo "<option value='".$kat['id']."'>".$kat['nama']."</option>";
								}
							}?>
						  </select>
					</div>
				</div>
				<div class="form-group">
					<label for="harga" class="col-sm-2 control-label">Harga</label>
					<div class="col-sm-10">
						<input type="number" class="form-control" id="harga" name="harga"   placeholder="Harga ..." required>
					</div>
				</div>
				<span id="errortext"></span>
				<div class="form-group"> 
					<div class="col-sm-offset-2 col-sm-10">
					  <button class="btn btn-purple waves-effect waves-light col-lg-offset-11 col-sm-offset-11" onClick="save()">Save</button>
					</div>
				  </div>
			</div>
			
		
	</div>
</div><!-- end col -->	

