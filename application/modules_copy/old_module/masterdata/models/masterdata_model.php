<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Masterdata_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		
	}
	
	
	
	public function list_datatable_report($params = array()) {					
		$sql		= "call masterdata_list( ?, ?, ?, ?, ?, @total_filtered, @total)";
		$out		= array();
		$query		= $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));
		//var_dump($this->db->last_query());
		$result = $query->result_array();
		
		while(mysqli_more_results($this->db->conn_id) && mysqli_next_result($this->db->conn_id)){
		if($l_result = mysqli_store_result($this->db->conn_id)){
			  mysqli_free_result($l_result);
			}
		}
		$total_filtered = $this->db->query('select @total_filtered')->row_array();
		$total 			= $this->db->query('select @total')->row_array();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total_filtered['@total_filtered'],
			'total' => $total['@total'],
		);
		
		return $return;
	}
	
	public function save($params = array()) {
		// print_r($params); die;
		$sqlstock 	= 'INSERT INTO  t_stock
						(	
							stock
						)
					VALUES
						(
							?
						);';
		$this->db->query($sqlstock,array(
														$params['stokbarang']
													)
										   );
										   
		$insert_id = $this->db->insert_id();
		
		$this->db->close();
		$this->db->initialize();
		
		$sql 	= 'INSERT INTO  t_masterdata
						(	
							kode_barang, 
							nama_barang, 
							disc,
							id_stok,
							id_kategory,
							harga
						)
					VALUES
						(
							?,?,?,?,?,?
						);';
        
		if ($sql) {
			return  $this->db->query($sql,array(
														$params['kodebarang'],
														$params['namabarang'],
														$params['diskon'],
														$insert_id,
														$params['kategoribarang'],
														$params['harga']
													)
										   );
		} 
		else {
			return false;
		}
	}
	public function edit($params = array()) {					
		$sql 	= '';
        
		if ($sql) {
			return  $this->db->query($sql,array(
														$params['id'],
														$params['kodebarang'],
														$params['namabarang'],
														$params['diskon'],
														$insert_id,
														$params['kategoribarang'],
														$params['harga']
													)
										   );
		} 
		else {
			return false;
		}
	}
	
	public function deletes($params = array()) {					
		$sql 	= 'DELETE FROM t_masterdata
					WHERE id = ?';
        
		if ($sql) {
			return  $this->db->query($sql,array(
														$params['id']
													)
										   );
		} 
		else {
			return false;
		}
	}
	public function detail($params = array()) {					
		$sql 	= 'SELECT a.id, a.kode_barang, a.nama_barang, a.disc,
						b.stock, a.harga, d.`id` AS kategori
					FROM t_masterdata a
					LEFT JOIN t_stock b ON a.id = b.id
					LEFT JOIN t_kategory d ON a.`id_kategory` = d.`id`
					WHERE a.id = ?';
        $query  = $this->db->query($sql,array(
									$params['id']
								)
					   );
		$result = $query->result_array();
		
		$this->db->close();
		$this->db->initialize();
        
        return $result;
	}	
	public function kategori() {					
		$sql 	= 'SELECT * FROM t_kategory';
        $query  = $this->db->query($sql);
		$result = $query->result_array();
		
		$this->db->close();
		$this->db->initialize();
        
        return $result;
	}		
				
	
}	