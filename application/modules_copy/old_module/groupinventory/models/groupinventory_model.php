<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Groupinventory_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		
	}
	
	
	
	public function list_datatable_report($params = array()) {					
		$sql		= "call group_list( ?, ?, ?, ?, ?, @total_filtered, @total)";
		$out		= array();
		$query		= $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));
		//var_dump($this->db->last_query());
		$result = $query->result_array();
		
		while(mysqli_more_results($this->db->conn_id) && mysqli_next_result($this->db->conn_id)){
		if($l_result = mysqli_store_result($this->db->conn_id)){
			  mysqli_free_result($l_result);
			}
		}
		$total_filtered = $this->db->query('select @total_filtered')->row_array();
		$total 			= $this->db->query('select @total')->row_array();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total_filtered['@total_filtered'],
			'total' => $total['@total'],
		);
		
		return $return;
	}
	
	public function save($params = array()) {		
			
	
		$sql 	= 'INSERT INTO  t_inventorygroup 	(
												nama,
												category_id
											)
								VALUES
											(
												?,
												?
											)';
											
		$hasil = $this->db->query($sql,array(
									$params['name'],
									$params['category']
								)
					   );									
        $insert_id = $this->db->insert_id();
		$this->db->close();
		$this->db->initialize();
		
		foreach($params['items'] as $ts){
			$sql2 	= 'INSERT INTO  t_items 	(
											inventory_id,
											items_id
										)
							VALUES
										(
											?,
											?
										)';
										
			$hasil2 = $this->db->query($sql2,array(
									$insert_id,
									$ts
								)
					   );
			$this->db->close();
			$this->db->initialize();
			
		} 						
		
		if ($hasil2) {
			
			
			return $hasil2;
			
			
		} 
		else {
			return false;
		}
	}
	public function edit($params = array()) {					
		$sql 	= 'UPDATE t_inventorygroup
					   SET nama = ? , category_id = ? 
					 WHERE id = ? ';
					 
		$hasil =	$this->db->query($sql,array(
													$params['name'],
													$params['category'],
													$params['id']
														
													)
										   );		 
      	$this->db->close();
		$this->db->initialize();
		
		foreach($params['items'] as $ts){
			$sql2 	= 'UPDATE  t_items  SET items_id = ? WHERE inventory_id = ?';
										
			$hasil2 = $this->db->query($sql2,array(
									$ts,
									$params['id']
								)
					   );
			$this->db->close();
			$this->db->initialize();
			
		} 						
		if ($hasil2) {
			
			
			return $hasil2;
			
			
		} 
		else {
			return false;
		}
	}
	
	public function deletes($params = array()) {					
		$sql 	= 'DELETE FROM t_kategory
WHERE id = ?';
        
		if ($sql) {
			return  $this->db->query($sql,array(
														$params['id']
													)
										   );
		} 
		else {
			return false;
		}
	}
	public function detail($id) {					
		$sql 	= 'SELECT * FROM t_inventorygroup where id = ?';
        $query  = $this->db->query($sql,array(
									$id
								)
					   );
		$result = $query->result_array();
		
		$this->db->close();
		$this->db->initialize();
        
        return $result;
	}
	public function list_items() {					
		$sql 	= 'SELECT id, nama_barang FROM t_masterdata	';
        $query  = $this->db->query($sql );
		$result = $query->result_array();
		
		$this->db->close();
		$this->db->initialize();
        
        return $result;
	}		
	public function list_car() {					
		$sql 	= 'SELECT id, nama FROM t_kategory	';
        $query  = $this->db->query($sql );
		$result = $query->result_array();
		
		$this->db->close();
		$this->db->initialize();
        
        return $result;
	}		
				
	
}	