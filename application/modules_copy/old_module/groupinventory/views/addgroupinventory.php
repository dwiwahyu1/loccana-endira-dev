<script>
	function savegroupinventory(){
		
		var name   = $('#name').val();
		var category   = $('#category').val();
		if(!name){
			
			$('#errortext').html('<span style="color:red"><i class="zmdi zmdi-assignment-alert"></i> You must fill the field</span>');
			setTimeout(function(){ $('#errortext').html(''); }, 3000);
		}else{
			$('#errortext').html('');
			var datapost={
			"name"  :   name,
			"category"  :   category,
			"items"  :   $('#my_multi_select3').val()
			};
			// console.log(datapost); return false;
			  $.ajax({
				type: "POST",
				url: "<?php echo base_url();?>groupinventory/save",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
				  if (response.status == "success") {
								swal({
                                        title: 'Success!',
                                        text: response.message,
                                        type: 'success',
                                        showCancelButton: false,
                                        confirmButtonText: 'Ok'
                                    }).then(function () {
                                       window.location.href = "<?php echo base_url('groupinventory');?>";
                                    })
				  } else{
					swal("Failed!", response.message, "error");
					$("#loader").hide();
				  }
				}
			  });
		}
		
	}
	
	 jQuery(document).ready(function() {

                //advance multiselect start
                $('#my_multi_select3').multiSelect({
                    selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
                    selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
                    afterInit: function (ms) {
                        var that = this,
                            $selectableSearch = that.$selectableUl.prev(),
                            $selectionSearch = that.$selectionUl.prev(),
                            selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                            selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                        that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                            .on('keydown', function (e) {
                                if (e.which === 40) {
                                    that.$selectableUl.focus();
                                    return false;
                                }
                            });

                        that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                            .on('keydown', function (e) {
                                if (e.which == 40) {
                                    that.$selectionUl.focus();
                                    return false;
                                }
                            });
                    },
                    afterSelect: function () {
                        this.qs1.cache();
                        this.qs2.cache();
                    },
                    afterDeselect: function () {
                        this.qs1.cache();
                        this.qs2.cache();
                    }
                });
				
	});
</script>

<link href="<?php echo base_url() . 'assets/adminto-14/adminto-14/Admin/Horizontal/';?>assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url() . 'assets/adminto-14/adminto-14/Admin/Horizontal/';?>assets/plugins/multiselect/js/jquery.multi-select.js"></script>
<div class="row">
	<div class="col-lg-12">
			<form>
			  <div class="form-group">
				<label for="name">Nama Group Inventory </label>
				<input type="text" class="form-control" id="name" placeholder="Enter Name">
			  </div>
			  <div class="form-group">
				<label for="name">Category</label>
				<select name="category" class="form-control" id="category" >
					<option selected disabled>Pilih Category</option>
					 <?php if(isset($car[0])){
						   foreach($car as $cr){
							   echo ' <option value="'.$cr['id'].'">'.$cr['nama'].'</option>';
							   
						   }
					   }?>
				</select>
			  </div>
			  <div class="form-group">
				<label for="name">Select Items</label>
				
					<select name="my_multi_select3" class="multi-select" multiple="" id="my_multi_select3" >
					   <?php if(isset($items[0])){
						   foreach($items as $it){
							   echo ' <option value="'.$it['id'].'">'.$it['nama_barang'].'</option>';
							   
						   }
					   }?>
					</select>
			  </div>
			</form>
			<span id="errortext"></span>
			<button class="btn btn-purple waves-effect waves-light col-lg-offset-9 col-sm-offset-9" onClick="savegroupinventory()">Save</button>
		
	</div>
</div><!-- end col -->	

