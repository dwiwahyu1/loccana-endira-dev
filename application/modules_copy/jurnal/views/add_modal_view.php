<style>
	#loading{
		position: absolute;
		background: #FFFFFF;
		opacity: 0.5;
		width: 93%;
		height: 85%;
		z-index: 3;
		text-align: center;
		display:none;
	}
</style>
<div id="loading"><img src="<?php echo base_url();?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/preloader.gif"/></div>
<form class="form-horizontal form-label-left" id="add_coa" role="form" action="<?php echo base_url('jurnal/add_jurnal');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id">Coa <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control select-control" id="level1" name="level1" style="width: 100%" required>
				<?php foreach($level1 as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>"><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<!-- <div class="item form-group" id="level2_temp">
		<div class="col-md-8 col-sm-6 col-xs-12" style="margin-left: 145px;">
			<select class="form-control select-control" id="level2" name="level2" style="width: 100%">
				<?php foreach($level2 as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>"><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group" id="level3_temp">
		<div class="col-md-8 col-sm-6 col-xs-12" style="margin-left: 145px;">
			<select class="form-control select-control" id="level3" name="level3" style="width: 100%">
				<?php foreach($level3 as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>"><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group" id="level4_temp">
		<div class="col-md-8 col-sm-6 col-xs-12" style="margin-left: 145px;">
			<select class="form-control" id="level4" name="level4" style="width: 100%">
				<?php foreach($level4 as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>"><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div> -->

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Tanggal
			<span class="required">
				<sup>*</sup>
			</span>
		</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			  <div class="input-group date">
				   <input placeholder="Tanggal" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="date" name="date" required="required" value="<?php echo date('Y-m-d');?>">
				   <div class="input-group-addon">
						<span class="glyphicon glyphicon-th"></span>
				   </div>
			  </div>
		 </div>
	</div>

	<div class="item form-group" id="subdetail_temp">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id">Valas <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="id_valas" name="id_valas" style="width: 100%" required>
				<?php foreach($valas as $key) { ?>
					<option value="<?php echo $key['valas_id']; ?>" ><?php echo $key['nama_valas']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Rate <span class="required"><sup>*</sup></span>
		</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="number" id="rate" name="rate" min="1" class="form-control col-md-7 col-xs-12" placeholder="Rate" value="1" required="required">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nilai <span class="required"><sup>*</sup></span>
		</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="number" id="value" name="value" min="0" class="form-control col-md-7 col-xs-12" placeholder="Nilai" value="0" required="required">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id">Type <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="type_cash" name="type_cash" style="width: 100%" required>
				<option value="0">Pemasukan</option>
				<option value="1">Pengeluaran</option>
			</select>
		</div>
	</div>

	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="keterangan">Catatan</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<textarea data-parsley-maxlength="255" type="text" id="note" name="note" class="form-control col-md-7 col-xs-12" placeholder="Catatan"></textarea>
		</div>
	</div>

	<div class="item form-group has-feedback">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="file_jurnal">File Bukti</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="file" class="form-control" id="file_jurnal" name="file_jurnal" data-height="110" accept=".pdf, .txt, .doc, .docx"/>
			<span id="file_bc_tick">Hanya format file pdf,txt,doc,docx dengan besaran max 10Mb yang diterima.</span>
		</div>
	</div>
		
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Jurnal</button>
		</div>
	</div>

</form>
<!-- /page content -->

<script type="text/javascript">
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();

		$("#date").datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});

		$('.select-control').on('change',(function(e) {
			var id 	  = $(this).children('option:selected').data('coa'),
				level = $(this).attr('id');
				
				coa(level,id);
		}));
		
		$('#level1').select2();
		$('#level2').select2();
		$('#level3').select2();
		$('#level4').select2();
	});

	$('#add_coa').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		var formData = new FormData();
		var id_coa		= '0',
			id_parent	= $('#level1').val();

		if($('#level4_temp').is(':visible')) id_coa	= $('#level4').val();
		else if($('#level3_temp').is(':visible')) id_coa	= $('#level3').val();
		else if($('#level2_temp').is(':visible')) id_coa	= $('#level2').val();
		else id_coa	= id_parent;

		formData.set('id_coa',		id_coa);
		formData.set('id_parent',	id_parent);
		formData.set('date',		$('#date').val());
		formData.set('id_valas',	$('#id_valas').val());
		formData.set('value',		$('#value').val());
		formData.set('rate',		$('#rate').val());
		formData.set('type_cash',	$('#type_cash').val());
		formData.set('note',		$('#note').val());

		if($('#file_jurnal')[0].files.length > 0) {
			if($('#file_jurnal')[0].files[0].size <= 9437184) {
				formData.set('file_jurnal', $('#file_jurnal')[0].files[0], $('#file_jurnal')[0].files[0].name);
				save_Form(formData, $(this).attr('action'));
			}else {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Tambah Jurnal");
				swal("Failed!", "Ukuran File Terlalu besar, silahkan cek kembali", "error");
			}
		}else {
			// console.log($('#file_jurnal')[0].files);
			// formData.set('file_jurnal', $('#file_jurnal')[0].files[0], '');
			save_Form(formData, $(this).attr('action'));
		}
	}));

	function save_Form(formData, url) {
		$.ajax({
			type:'POST',
			url: url,
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					$('.panel-heading button').trigger('click');
					listjurnal();
					<?php echo base_url('approve_jurnal');?>;
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					});
					
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Tambah Jurnal");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah Jurnal");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}
</script>
