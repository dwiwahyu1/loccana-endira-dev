<form class="form-horizontal form-label-left" id="edit_jurnal" role="form" action="<?php echo base_url('jurnal/edit_jurnal');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	
	<div class="item form-group" id="level1_temp">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id">Coa <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control select-control" id="level1" name="level1" style="width: 100%" required>
				<?php foreach($level1 as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>" <?php if(isset($level1_id)){ if( $level1_id == $key['coa']){ echo "selected"; } }?>><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
		</div>
	<!-- <div class="item form-group" id="level2_temp" <?php if(isset($level2)){ if( count($level2)==0){ echo 'style="display:none"'; } }?>>
		<div class="col-md-8 col-sm-6 col-xs-12" style="margin-left: 145px;">
			<select class="form-control select-control" id="level2" name="level2" style="width: 100%">
				<?php foreach($level2 as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>" <?php if(isset($level2_id)){ if( $level2_id == $key['coa']){ echo "selected"; } }?>><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group" id="level3_temp" <?php if(isset($level3)){ if( count($level3)==0){ echo 'style="display:none"'; } }?>>
		<div class="col-md-8 col-sm-6 col-xs-12" style="margin-left: 145px;">
			<select class="form-control" id="level3" name="level3" style="width: 100%">
				<?php foreach($level3 as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>" <?php if(isset($level3_id)){ if( $level3_id == $key['coa']){ echo "selected"; } }?>><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group" id="level4_temp" <?php if(isset($level4)){ if( count($level4)==0){ echo 'style="display:none"'; } }?>>
		<div class="col-md-8 col-sm-6 col-xs-12" style="margin-left: 145px;">
			<select class="form-control" id="level4" name="level4" style="width: 100%">
				<?php foreach($level4 as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>" <?php if(isset($level4_id)){ if( $level4_id == $key['coa']){ echo "selected"; } }?>><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div> -->
	
	<input type="hidden" class="form-control col-md-7 col-xs-12 coa" id="coa_val" name="coa_val" value="<?php if(isset($detail[0]['date'])){ echo $detail[0]['date']; }?>">
	 
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Tanggal
			<span class="required">
				<sup>*</sup>
			</span>
		</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			  <div class="input-group date">
				   <input placeholder="Tanggal" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="date" name="date" required="required" value="<?php if(isset($detail[0]['date'])){ echo $detail[0]['date']; }?>">
				   <div class="input-group-addon">
						<span class="glyphicon glyphicon-th"></span>
				   </div>
			  </div>
		 </div>
	</div>

	<div class="item form-group" id="subdetail_temp">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id">Valas <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="id_valas" name="id_valas" style="width: 100%" required>
				<?php foreach($valas as $key) { ?>
					<option value="<?php echo $key['valas_id']; ?>" <?php if(isset($detail[0]['id_valas'])){ if( $detail[0]['id_valas'] == $key['valas_id'] ){ echo "selected"; } }?>><?php echo $key['nama_valas']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="rate">Rate <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="number" id="rate" name="rate" min="0" class="form-control col-md-7 col-xs-12" placeholder="Rate" value="<?php if(isset($detail[0]['rate'])){ echo round($detail[0]['rate']); }?>" required="required">
		</div>
	</div>

	<div class="item form-group">
		  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nilai <span class="required"><sup>*</sup></span>
		  </label>
		  <div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="number" id="value" name="value" min="0" class="form-control col-md-7 col-xs-12" placeholder="Nilai" value="<?php if(isset($detail[0]['value'])){ echo round($detail[0]['value']); }?>" required="required">
		  </div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id">Type <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="type_cash" name="type_cash" style="width: 100%" required>
				<option value="0" <?php if(isset($detail[0]['type_cash'])){ if( $detail[0]['type_cash'] == '0' ){ echo "selected"; } }?>>Pemasukan</option>
				<option value="1" <?php if(isset($detail[0]['type_cash'])){ if( $detail[0]['type_cash'] == '1' ){ echo "selected"; } }?>>Pengeluaran</option>
			</select>
		</div>
	</div>

	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="keterangan">Catatan</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<textarea data-parsley-maxlength="255" type="text" id="note" name="note" class="form-control col-md-7 col-xs-12" placeholder="Catatan"><?php if(isset($detail[0]['note'])){ echo $detail[0]['note']; }?></textarea>
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="file_jurnal">File </label>
		<div style="margin-left: 5px;">
			<?php if(isset($detail[0]['bukti']) && $detail[0]['bukti'] != '') {
				if($filetype == 'pdf') { ?>
				<a href="<?php echo base_url().'uploads/bc/'.$detail[0]['bukti'];?>" title="Download File" target="_blank" download>
					<!-- Gambar hanya contoh, path tergantung proyek masing2 -->
					<img src="<?php echo base_url() ."uploads/file-icon/pdf96px.png";?>" id="panel_file_temp"/>
				</a>
			<?php }elseif($filetype == 'docx' || $filetype == 'doc'){?>
				<a href="<?php echo base_url().'uploads/bc/'.$detail[0]['bukti'];?>" target="_blank" title="Download File" target="_blank" download>
					<!-- Gambar hanya contoh, path tergantung proyek masing2 -->
					<img src="<?php echo base_url() ."uploads/file-icon/word2019-96px.png";?>" id="panel_file_temp"/>
				</a>
			<?php }elseif($filetype == 'xlsx' || $filetype == 'xls') { ?>
				<a href="<?php echo base_url().'uploads/bc/'.$detail[0]['bukti'];?>" target="_blank" title="Download File" target="_blank" download>
					<!-- Gambar hanya contoh, path tergantung proyek masing2 -->
					<img src="<?php echo base_url() ."uploads/file-icon/excel2019-96px.png";?>" id="panel_file_temp"/>
				</a>
			<?php }else{ ?>
				<a href="<?php echo base_url().'uploads/bc/'.$detail[0]['bukti'];?>" target="_blank" title="Download File" target="_blank" download>
					<!-- Gambar hanya contoh, path tergantung proyek masing2 -->
					<img src="<?php echo base_url() ."uploads/file-icon/txt-160px.png";?>" id="panel_file_temp"/>
				</a>
			<?php } } ?>
			<label class="control-label text-center"><?php echo $filename; ?></label>
		</div>
	</div>
		
	<div class="item form-group">
		<div class="col-md-8 col-sm-6 col-xs-12" style="padding-top: 10px;margin-left: 145px;">
			<input type="file" class="form-control" id="file_jurnal" name="file_jurnal" data-height="110" accept=".pdf, .txt, .doc, .docx"/>
			<span id="file_bc_tick">Hanya format file pdf,txt,doc,docx dengan besaran max 10Mb yang diterima.</span>
			<input type="hidden" id="old_file" name="old_file" value="<?php if(isset($detail[0]['bukti'])){ echo $detail[0]['bukti']; }?>">
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Jurnal</button>
		</div>
	</div>
	<input type="hidden" id="id" name="id" value="<?php if(isset($detail[0]['id'])){ echo $detail[0]['id']; }?>">
</form>
<!-- /page content -->

<script type="text/javascript">
	var selectflag = '';

	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();

		$("#date").datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});
		
		$('#file_jurnal').bind('change', function() {
			if(this.files[0].size >= 9437184) {
				$('#file_jurnal').css('border', '3px #C33 solid');
				$('#file_bc_tick').empty();
				$("#file_bc_tick").append('<span class="fa fa-close"> Ukuran File Lebih dari 9Mb</span>');
				$('#file_bc_tick').show();
			}else {
				$('#file_jurnal').removeAttr("style");
				$('#file_bc_tick').empty();
				$("#file_bc_tick").append('Hanya format file pdf,txt,doc,docx dengan besaran max 10Mb yang diterima.');
				$('#file_bc_tick').show();
			}
		});
		
		$('#level1').select2();
		$('#level2').select2();
		$('#level3').select2();
		$('#level4').select2();
		
	});

	$('#edit_jurnal').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Mengubah data...");
		e.preventDefault();
		var formData = new FormData(this);
		
		/*var id_coa		= '0',
			id_parent	= $('#level1').val();
			if($('#level4_temp').is(':visible')) id_coa	= $('#level4').val();
			else if($('#level3_temp').is(':visible')) id_coa	= $('#level3').val();
			else if($('#level2_temp').is(':visible')) id_coa	= $('#level2').val();
			else id_coa	= id_parent;
		
		jQuery.each(jQuery('#file_jurnal')[0].files, function(i, file) {
			formData.append('file_jurnal-'+i, file);
		});

		formData.set('id',			$('#id').val());
		formData.set('id_coa',		id_coa);
		formData.set('id_parent',	id_parent);
		formData.set('date',		$('#date').val());
		formData.set('id_valas',	$('#id_valas').val());
		formData.set('value',		$('#value').val());
		formData.set('rate',		$('#rate').val());
		formData.set('type_cash',	$('#type_cash').val());
		formData.set('note',		$('#note').val());
		formData.set('old_file',	$('#old_file').val());*/
		
		
		if($('#file_jurnal')[0].files.length > 0) {
			if($('#file_jurnal')[0].files[0].size <= 9437184) {
				save_Form(formData, $(this).attr('action'));
			}else {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Update Data");
				swal("Failed!", "Ukuran File Terlalu besar, silahkan cek kembali", "error");
			}
		}else {
			save_Form(formData, $(this).attr('action'));
		}
	}));

	function save_Form(formData, url) {
		$.ajax({
			type:'POST',
			url: url,
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					$('.panel-heading button').trigger('click');
					listjurnal();
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					});
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Edit Jurnal");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Edit Jurnal");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}
</script>
