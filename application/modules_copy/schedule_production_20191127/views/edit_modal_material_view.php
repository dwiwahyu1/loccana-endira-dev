    <!--Parsley-->
    <script type="text/javascript" src="<?php echo base_url('assets'); ?>/gentelella-master/vendors/parsleyjs/dist/parsley.min.js"></script>

	<style>
	#loading-us{display:none}
	#tick{display:none}

	#loading-mail{display:none}
	#cross{display:none}
	</style>

<form class="form-horizontal form-label-left" role="form" action="" method="post" enctype="multipart/form-data" data-parsley-validate>

    <div class="item form-group" id="subdetail_temp">
        <label class="col-md-3 col-sm-3 col-xs-12" for="type_id">Kode</label>
        <div class="col-md-8 col-sm-6 col-xs-12">
            : <?php if(isset($uom[0]['stock_code'])){ echo $uom[0]['stock_code']; }?>
        </div>
    </div>

    <div class="item form-group" id="subdetail_temp">
        <label class="col-md-3 col-sm-3 col-xs-12" for="type_id">Nama</label>
        <div class="col-md-8 col-sm-6 col-xs-12">
            : <?php if(isset($uom[0]['stock_name'])){ echo $uom[0]['stock_name']; }?>
        </div>
    </div>

    <div class="item form-group" id="subdetail_temp">
        <label class="col-md-3 col-sm-3 col-xs-12" for="type_id">Deskripsi</label>
        <div class="col-md-8 col-sm-6 col-xs-12">
            : <?php if(isset($uom[0]['stock_description'])){ echo $uom[0]['stock_description']; }?>
        </div>
    </div>

    <div class="item form-group" id="subdetail_temp">
        <label class="col-md-3 col-sm-3 col-xs-12" for="type_id">Type</label>
        <div class="col-md-8 col-sm-6 col-xs-12">
            : <?php if(isset($uom[0]['type_material_name'])){ echo $uom[0]['type_material_name']; }?>
        </div>
    </div>

    <div class="item form-group" id="subdetail_temp">
        <label class="col-md-3 col-sm-3 col-xs-12" for="type_id">Nama Gudang</label>
        <div class="col-md-8 col-sm-6 col-xs-12">
            : <?php if(isset($uom[0]['nama_gudang'])){ echo $uom[0]['nama_gudang']; }?>
        </div>
    </div>

    <div class="item form-group" id="subdetail_temp">
        <label class="col-md-3 col-sm-3 col-xs-12" for="type_id">Valas</label>
        <div class="col-md-8 col-sm-6 col-xs-12">
            : <?php if(isset($uom[0]['nama_valas'])){ echo $uom[0]['nama_valas']; }?>
        </div>
    </div>

    <div class="item form-group" id="subdetail_temp">
        <label class="col-md-3 col-sm-3 col-xs-12" for="type_id">Harga</label>
        <div class="col-md-8 col-sm-6 col-xs-12">
            : <?php if(isset($uom[0]['base_price'])){ echo $uom[0]['base_price']; }?>
        </div>
    </div>

    <div class="item form-group" id="subdetail_temp">
        <label class="col-md-3 col-sm-3 col-xs-12" for="type_id">Qty</label>
        <div class="col-md-8 col-sm-6 col-xs-12">
            : <?php if(isset($uom[0]['base_qty'])){ echo $uom[0]['base_qty']; }?>
        </div>
    </div>

</form>

        <!-- /page content -->

<script type="text/javascript">
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
	});
</script>
