<!--Parsley-->
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/gentelella-master/vendors/parsleyjs/dist/parsley.min.js"></script>
<style>
	#loading-us{display:none}
	#tick{display:none}

	#loading-mail{display:none}
	#cross{display:none}
</style>
	
<form class="form-horizontal form-label-left" id="add_form" role="form" action="<?php echo base_url('type_gudang/save_typegudang');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="typegudang_name">Nama Type Gudang <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-minlength="4" data-parsley-maxlength="100" type="text" id="typegudang_name" name="typegudang_name" class="form-control col-md-7 col-xs-12" placeholder="nama properties minimal 4 karakter" required="required">
			<span id="loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking type gudang...</span>
			<span id="tick"></span>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="deskripsi">Deskripsi <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<textarea data-parsley-maxlength="255" id="deskripsi" name="deskripsi" class="form-control col-md-7 col-xs-12" placeholder="Deskripsi"></textarea>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Type Gudang</button>
		</div>
	</div>

</form>
<!-- /page content -->

<script type="text/javascript">
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
	});

	var last_typegudang_name = $('#typegudang_name').val();
	$('#typegudang_name').on('input',function(event) {
		if($('#typegudang_name').val() != last_typegudang_name) {
			typegudang_name_check();
		}
	});

	function typegudang_name_check() {
		var typegudang_name = $('#typegudang_name').val();
		if(typegudang_name.length > 3) {
			var post_data = {
				'typegudang_name': typegudang_name
			};

			$('#tick').empty();
			$('#tick').hide();
			$('#loading-us').show();
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url('type_gudang/check_typegudang_name');?>",
				data: post_data,
				cache: false,
				success: function(response){
					if(response.success == true){
						$('#typegudang_name').css('border', '3px #090 solid');
						$('#loading-us').hide();
						$('#tick').empty();
						$("#tick").append('<span class="fa fa-check"> '+response.message+'</span>');
						$('#tick').show();
					}else {
						$('#typegudang_name').css('border', '3px #C33 solid');
						$('#loading-us').hide();
						$('#tick').empty();
						$("#tick").append('<span class="fa fa-close"> '+response.message+'</span>');
						$('#tick').show();
					}
				}
			});
		}else {
			$('#typegudang_name').css('border', '3px #C33 solid');
			$('#loading-us').hide();
			$('#tick').empty();
			$("#tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
			$('#tick').show();
		}
	}

	$('#add_form').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		var formData = new FormData(this);

		$.ajax({
			type:'POST',
			url: $(this).attr('action'),
			data:formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						window.location.href = "<?php echo base_url('type_gudang');?>";
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Tambah Type Gudang");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah Type Gudang");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}));
</script>
