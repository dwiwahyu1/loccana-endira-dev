<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Type_Gudang extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('type_gudang/type_gudang_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'type_gudang/views/index');
	}

	function list_type() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;
		$order_fields = array('', 'nama_type_gudang', 'deskripsi');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->type_gudang_model->list_type($params);
		// print_r($list);die;

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$status_akses =
				'<div class="btn-group">'.
					'<button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
						onClick="edit_typegudang(\'' . $v['id'] . '\')">'.
						'<i class="fa fa-edit"></i>'.
					'</button>'.
				'</div>'.
				'<div class="btn-group">'.
					'<button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete"
						onClick="delete_typegudang(\'' . $v['id'] . '\')">'.
						'<i class="fa fa-trash"></i>'.
					'</button>'.
				'</div>';

			array_push($data, array(
				$i,
				ucwords($v['nama_type_gudang']),
				ucwords($v['deskripsi']),
				$status_akses
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_typegudang() {
		$this->load->view('add_modal_view');
	}

	public function check_typegudang_name(){
		$this->form_validation->set_rules('typegudang_name', 'Type Gudang Name', 'trim|required|min_length[4]|max_length[100]|is_unique[t_type_gudang.nama_type_gudang]');
		$this->form_validation->set_message('is_unique', 'Detail Type Gudang Already Exists.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'Nama Type Gudang Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}

	public function save_typegudang() {
		$this->form_validation->set_rules('typegudang_name', 'Type Gudang Name', 'trim|required|min_length[4]|max_length[100]|is_unique[t_type_gudang.nama_type_gudang]',array('is_unique' => 'This %s already exists.'));
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|max_length[255]');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$typegudang_name = ucwords($this->Anti_sql_injection($this->input->post('typegudang_name', TRUE)));
			$deskripsi = ucwords($this->Anti_sql_injection($this->input->post('deskripsi', TRUE)));

			$data = array(
				'typegudang_name' => $typegudang_name,
				'deskripsi' => $deskripsi
			);

			$result = $this->type_gudang_model->add_typegudang($data);

			if ($result > 0) {
				$this->log_activity->insert_activity('insert', 'Insert type Gudang');
				$result = array('success' => true, 'message' => 'Berhasil menambahkan Type Gudang ke database');
			} else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert Type Gudang');
				$result = array('success' => false, 'message' => 'Gagal menambahkan Type Gudang ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function edit_typegudang($id) {
		$result = $this->type_gudang_model->edit_typegudang($id);

		$data = array(
			'typegudang' => $result,
		);

		$this->load->view('edit_modal_view', $data);
	}

	public function save_edit_typegudang() {
		$this->form_validation->set_rules('typegudang_name', 'Type Gudang Name', 'trim|required|min_length[4]|max_length[100]|is_unique[t_type_gudang.nama_type_gudang]',array('is_unique' => 'This %s already exists.'));
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|max_length[255]');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$typegudang_id = $this->Anti_sql_injection($this->input->post('typegudang_id', TRUE));
			$typegudang_name = ucwords($this->Anti_sql_injection($this->input->post('typegudang_name', TRUE)));
			$deskripsi = ucwords($this->Anti_sql_injection($this->input->post('deskripsi', TRUE)));

			$data = array(
				'typegudang_id' => $typegudang_id,
				'typegudang_name' => $typegudang_name,
				'deskripsi' => $deskripsi
			);

			$result = $this->type_gudang_model->save_edit_typegudang($data);

			if ($result > 0) {
				$this->log_activity->insert_activity('update', 'Update Type Gudang id : '.$typegudang_id);
				$result = array('success' => true, 'message' => 'Berhasil mengubah Type Gudang ke database');
			} else {
				$this->log_activity->insert_activity('update', 'Gagal Update Type Gudang id : '.$typegudang_id);
				$result = array('success' => false, 'message' => 'Gagal mengubah Type Gudang ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function delete_typegudang() {
		$data 	= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		$result = $this->type_gudang_model->delete_typegudang($params['id']);
		if($result > 0) {
			$this->log_activity->insert_activity('delete', 'Delete Type Gudang id : '.$params['id']);
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}else {
			$this->log_activity->insert_activity('delete', 'Gagal Delete Type Gudang id : '.$params['id']);
			$res = array('status' => 'success', 'message' => 'Data gagal di hapus');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}
}