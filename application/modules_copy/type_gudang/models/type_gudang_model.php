<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Type_Gudang_Model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function check_userid($params = array()) {
		$sql 	= 'SELECT COUNT(id) AS jumlah FROM u_user WHERE id = ? AND status_akses = 1;';
		$query 	= $this->db->query($sql,
			array($params['user_id'])
		);
		$return = $query->row_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function list_type($params = array()) {
		$sql 	= 'CALL typegudang_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function add_typegudang($data) {
		$sql 	= 'CALL typegudang_add(?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['typegudang_name'],
				$data['deskripsi']
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_typegudang($id) {
		$sql 	= 'CALL typegudang_search_id(?)';

		$query 	=  $this->db->query($sql,
			array($id)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function save_edit_typegudang($data) {
		$sql 	= 'CALL typegudang_update(?,?,?)';
		
		$query 	=  $this->db->query($sql,
			array(
				$data['typegudang_id'],
				$data['typegudang_name'],
				$data['deskripsi']
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function delete_typegudang($data) {
		$sql 	= 'CALL typegudang_delete(?)';
		$query 	=  $this->db->query($sql,
			array($data)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}