<style>
	.dt-body-left {text-align:left;}
	.dt-body-right {text-align:right;}
	.dt-body-center {text-align:center; vertical-align: middle;}
	.force-overflow {height: 650px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow {min-height: 650px}
	#modal-distributor::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar
	{
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar-thumb
	{
		background-image: -webkit-gradient(linear,
		   left bottom,
		   left top,
		   color-stop(0.44, rgb(122,153,217)),
		   color-stop(0.72, rgb(73,125,189)),
		   color-stop(0.86, rgb(28,58,148)));
	}

</style>

 <div class="product-sales-area mg-tb-30">
            <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card-box table-responsive">
                                    <div class="dropdown pull-right">
                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                            <i class="zmdi zmdi-more-vert"></i>
                                        </a>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>

                        			<h4 class="header-title m-t-0 m-b-30">List Purchase Order</h4>

                                    <table id="datatable_pricipal" class="table table-striped table-bordered" >
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Kode Pricipal</th>
                                                <th>Nama Pricipal</th>
                                                <th>Alamat</th>
                                                <th>No. Telp</th>
                                                <th>No. Fax</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>

                                        <tbody style="">
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div><!-- end col -->
                        </div>
</div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body force-overflow" id="modal-distributor">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  function add_distributor(){
      $('#panel-modal').removeData('bs.modal');
      $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modal  .panel-body').load('<?php echo base_url('distributor/add');?>');
      $('#panel-modal  .panel-title').html('<i class="fa fa-user-plus"></i> Add Distributor');
      $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
  }

  function editdistributor(id){
      $('#panel-modal').removeData('bs.modal');
      $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modal  .panel-body').load('<?php echo base_url('distributor/edit/');?>'+"/"+id);
      $('#panel-modal  .panel-title').html('<i class="fa fa-user"></i> Edit Distributor');
      $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
  }

  function deletedistributor(id){
    swal({
        title: 'Yakin akan Menghapus ?',
        text: 'data tidak dapat dikembalikan bila sudah dihapus !',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak'
      }).then(function () {
          var datapost={
            "id"  :   id
          };

          $.ajax({
                type:'POST',
                url: "<?php echo base_url().'distributor/delete_distributor';?>",
                data:JSON.stringify(datapost),
                cache:false,
                contentType: false,
                processData: false,
                success: function(response) {
                    $('.panel-heading button').trigger('click');
                    listdist();
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                    })
                }
            });
      });
  }

  function listdist(){
	  var user_id = '0001';
	  var token = '093940349';
	  
	  
   $('#datatable_pricipal').DataTable({
	   //"dom": 'rtip',
		"bFilter": false,
		"aaSorting": [],
		"bLengthChange": true,
		'iDisplayLength': 10,
		"sPaginationType": "simple_numbers",
		"Info" : false,
		"processing": true,
        "serverSide": true,
        "destroy": true,
		"ajax": "<?php echo base_url().'principal_management/lists'?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token,
		"searching": true,
		"language": {
            "decimal": ",",
            "thousands": "."
        },
		"dom": 'l<"toolbar">frtip',
		"initComplete": function(){
                           $("div.toolbar").prepend('<div class="btn-group pull-left"><a href="<?php echo base_url().'principal_management/add'; ?>" type="button" class="btn btn-custon-rounded-two btn-primary" > Tambah </a></div>');
        }
	});	
  }

 // $(document).ready(function() {
                // $('#datatable_pricipal').dataTable();
                // $('#datatable-keytable').DataTable( { keys: true } );
                // $('#datatable-responsive').DataTable();
                // $('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
                // var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
            // } );

	$(document).ready(function(){
      listdist();
  });
</script>