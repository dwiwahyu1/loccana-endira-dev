<style>
	.dt-body-left {text-align:left;}
	.dt-body-right {text-align:right;}
	.dt-body-center {text-align:center; vertical-align: middle;}
	.force-overflow {height: 650px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow {min-height: 650px}
	#modal-distributor::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar
	{
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar-thumb
	{
		background-image: -webkit-gradient(linear,
		   left bottom,
		   left top,
		   color-stop(0.44, rgb(122,153,217)),
		   color-stop(0.72, rgb(73,125,189)),
		   color-stop(0.86, rgb(28,58,148)));
	}

</style>

 <div class="product-sales-area mg-tb-30">
            <div class="container-fluid">
                        <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            <ul id="myTabedu1" class="tab-review-design">
                                <li class="active"><a href="#description">Tambah Principal</a></li>
                            </ul>
                            <div id="myTabContent" class="tab-content custom-product-edit">
                                <div class="product-tab-list tab-pane fade active in" id="description">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="review-content-section">
                                                <form id="add-department" action="#" class="add-department">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
														 <div class="form-group">
                                                                <input name="kode" type="text" class="form-control" placeholder="Kode Principal">
                                                            </div>
                                                            <div class="form-group">
                                                                <input name="name" type="text" class="form-control" placeholder="Nama Principal">
                                                            </div>
                                                            <div class="form-group">
                                                                <input name="alamat" type="text" class="form-control" placeholder="Alamat Principal">
                                                            </div>
                                                            <div class="form-group">
                                                                <input name="telp" type="email" class="form-control" placeholder="Telephone">
                                                            </div>
															 <div class="form-group">
                                                                <input name="fax" type="email" class="form-control" placeholder="Fax">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="payment-adress">
                                                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               </div>
                        </div>
                    </div>
                </div>
</div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body force-overflow" id="modal-distributor">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>


  function deletedistributor(id){
    swal({
        title: 'Yakin akan Menghapus ?',
        text: 'data tidak dapat dikembalikan bila sudah dihapus !',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak'
      }).then(function () {
          var datapost={
            "id"  :   id
          };

          $.ajax({
                type:'POST',
                url: "<?php echo base_url().'distributor/delete_distributor';?>",
                data:JSON.stringify(datapost),
                cache:false,
                contentType: false,
                processData: false,
                success: function(response) {
                    $('.panel-heading button').trigger('click');
                    listdist();
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                    })
                }
            });
      });
  }

  function listdist(){
	  var user_id = '0001';
	  var token = '093940349';
	  
	  
   $('#datatable_pricipal').DataTable({
	   //"dom": 'rtip',
		"bFilter": false,
		"aaSorting": [],
		"bLengthChange": true,
		'iDisplayLength': 10,
		"sPaginationType": "simple_numbers",
		"Info" : false,
		"processing": true,
        "serverSide": true,
        "destroy": true,
		"ajax": "<?php echo base_url().'principal_management/lists'?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token,
		"searching": true,
		"language": {
            "decimal": ",",
            "thousands": "."
        },
		"dom": 'l<"toolbar">frtip',
		"initComplete": function(){
                           $("div.toolbar").prepend('<div class="btn-group pull-left"><button type="button" class="btn btn-custon-rounded-two btn-primary" > Tambah </button></div>');
        }
	});	
  }

 // $(document).ready(function() {
                // $('#datatable_pricipal').dataTable();
                // $('#datatable-keytable').DataTable( { keys: true } );
                // $('#datatable-responsive').DataTable();
                // $('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
                // var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
            // } );

	$(document).ready(function(){
      listdist();
  });
</script>