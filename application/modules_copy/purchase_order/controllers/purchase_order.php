<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Purchase_order extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('purchase_order/purchase_order_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 * @return string
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	  * This function is redirect to index distributor page
	  * @return Void
	  */
	public function index() {
		$this->template->load('maintemplate', 'purchase_order/views/index');
	}

	/**
	  * This function is used for showing distributor list
	  * @return Array
	  */
	function lists() {
		
		 if( !empty($_GET['sess_user_id']) ) {
			  $sess_user_id = $_GET['sess_user_id'];
		  } else {
			  $sess_user_id = NULL;
		  }
		  
		   if( !empty($_GET['sess_token']) ) {
			  $sess_token = $_GET['sess_token'];
		  } else {
			  $sess_token = NULL;
		  }
		  
		    if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		  if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		  if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 				
		  $order_fields = array('kode_eksternal'); // , 'COST'
		  $order = $this->input->get_post('order');
		  if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'desc';}; 
		  if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 	
		  
		    $params['limit'] 		= (int) $length;
			$params['offset'] 		= (int) $start;
			$params['order_column'] = $order_fields[$order_column];
			$params['order_dir'] 	= $order_dir;
			$params['sess_user_id'] = $sess_user_id;
			$params['sess_token'] 	= $sess_token;
			$params['searchtxt'] 	= $_GET['search']['value'];
		  
			$list = $this->principal_management_model->list_principal($params); 
			//print_r($list['data']);die;
			  $data = array();	
				   foreach ( $list['data'] as $k => $v ) {

						$action = '<button type="button" class="btn btn-custon-rounded-two btn-default" > Edit </button><button type="button" class="btn btn-custon-rounded-two btn-danger" onClick="deletedistributor('. $v['id'].')" > Hapus </button>';
						   array_push($data, 
							  array(
								  number_format($v['Rangking'],0,',','.'),
								  $v['kode_eksternal'],
								  $v['name_eksternal'],
								  $v['eksternal_address'],
								  $v['phone_1'],
								  $v['fax'],
								  $action
							  )
							);
							//$idx++;
				   }
		   
			 $result["data"] = $data;
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		//$result["data"] = $list['data'];
	  
		echo json_encode($result);
	  
				//$this->json_result($result);
		  
	}

	/**
	  * This function is redirect to add distributor page
	  * @return Void
	  */
	public function add() {
		// $result = $this->distributor_model->location();

		$data = array(
			'location' => ''
		);

		$this->template->load('maintemplate', 'purchase_order/views/addPrinciple',$data);
		//$this->load->view('addPrinciple',$data);
	}	
	
	public function add_principle() {
		echo "Asasasa";
	}

	/**
	  * This function is used to add distributor data
	  * @return Array
	  */
	public function add_distributor() {
		$this->form_validation->set_rules('kode_distributor', 'Kode Distributor', 'trim|required|min_length[4]|max_length[100]');
		$this->form_validation->set_rules('dist_name', 'Nama Distributor', 'trim|required|min_length[4]|max_length[50]');
		$this->form_validation->set_rules('dist_address', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('phone_1', 'Telepon', 'trim|required|min_length[4]|max_length[15]');
		$this->form_validation->set_rules('email', 'Email Distributor', 'trim|required');
		$this->form_validation->set_rules('pic', 'PIC', 'trim|required');
		$this->form_validation->set_rules('location', 'Lokasi', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$kode_distributor = $this->Anti_sql_injection($this->input->post('kode_distributor', TRUE));
			$dist_name = $this->Anti_sql_injection($this->input->post('dist_name', TRUE));
			$dist_address = $this->Anti_sql_injection($this->input->post('dist_address', TRUE));
			$ship_address = $this->Anti_sql_injection($this->input->post('ship_address', TRUE));
			$phone_1 = $this->Anti_sql_injection($this->input->post('phone_1', TRUE));
			$phone_2 = $this->Anti_sql_injection($this->input->post('phone_2', TRUE));
			$fax = $this->Anti_sql_injection($this->input->post('fax', TRUE));
			$email = $this->Anti_sql_injection($this->input->post('email', TRUE));
			$pic = $this->Anti_sql_injection($this->input->post('pic', TRUE));
			$location = $this->Anti_sql_injection($this->input->post('location', TRUE));

			$data = array(
				'kode_distributor' 	=> $kode_distributor,
				'name_eksternal' 	=> $dist_name,
				'eksternal_address' => $dist_address,
				'ship_address' 		=> $ship_address,
				'phone_1' 			=> $phone_1,
				'phone_2' 			=> $phone_2,
				'fax' 				=> $fax,
				'email' 			=> $email,
				'pic' 				=> $pic,
				'eksternal_loc' 	=> $location,
				'type_eksternal' 	=> 2
			);

			$add_dist_result = $this->distributor_model->add_distributor($data);
			if ($add_dist_result['result'] > 0) {
				$dataDistributorCoa = array(
					'id_parent'		=> '50100',
					'keterangan'	=> $dist_name,
					'type_coa'		=> 1,
					'id_eksternal'	=> $add_dist_result['lastid']
				);
				$add_dist_coa_result = $this->distributor_model->add_distributor_coa($dataDistributorCoa);

				if($add_dist_coa_result > 0) {
					$msg = 'Berhasil menambahkan distributor';

					$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ' .$kode_distributor);
					$result = array('success' => true, 'message' => $msg);
				}
			}else {
				$msg = 'Gagal menambahkan customer ke database';

				$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ' .$kode_distributor);
				$result = array('success' => false, 'message' => $msg);
			}
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function edit($id) {
		$result = $this->distributor_model->detail($id);
		$location = $this->distributor_model->location();

		$data = array(
			'detail' => $result,
			'location' => $location
		);

		$this->load->view('edit_modal_view', $data);
	}

	public function edit_distributor() {
		$this->form_validation->set_rules('kode_distributor', 'Kode Distributor', 'trim|required|min_length[4]|max_length[100]');
		$this->form_validation->set_rules('dist_name', 'Nama Distributor', 'trim|required|min_length[4]|max_length[50]');
		$this->form_validation->set_rules('dist_address', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('phone_1', 'Telepon', 'trim|required|min_length[4]|max_length[15]');
		$this->form_validation->set_rules('email', 'Email Distributor', 'trim|required');
		$this->form_validation->set_rules('pic', 'PIC', 'trim|required');
		$this->form_validation->set_rules('location', 'Lokasi', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$id = $this->Anti_sql_injection($this->input->post('id', TRUE));
			$kode_distributor = $this->Anti_sql_injection($this->input->post('kode_distributor', TRUE));
			$dist_name = $this->Anti_sql_injection($this->input->post('dist_name', TRUE));
			$dist_address = $this->Anti_sql_injection($this->input->post('dist_address', TRUE));
			$ship_address = $this->Anti_sql_injection($this->input->post('ship_address', TRUE));
			$phone_1 = $this->Anti_sql_injection($this->input->post('phone_1', TRUE));
			$phone_2 = $this->Anti_sql_injection($this->input->post('phone_2', TRUE));
			$fax = $this->Anti_sql_injection($this->input->post('fax', TRUE));
			$email = $this->Anti_sql_injection($this->input->post('email', TRUE));
			$pic = $this->Anti_sql_injection($this->input->post('pic', TRUE));
			$location = $this->Anti_sql_injection($this->input->post('location', TRUE));

			$data = array(
				'id' => $id,
				'kode_distributor' => $kode_distributor,
				'name_eksternal' => $dist_name,
				'eksternal_address' => $dist_address,
				'ship_address' => $ship_address,
				'phone_1' => $phone_1,
				'phone_2' => $phone_2,
				'fax' => $fax,
				'email' => $email,
				'pic' => $pic,
				'eksternal_loc' => $location
			);

			$dataCoa = array(
				'id_eksternal'	=> $id,
				'keterangan'	=> $dist_name
			);
			
			$result_dist 		= $this->distributor_model->edit_distributor($data);
			$result_dist_coa	= $this->distributor_model->edit_distributor_coa($dataCoa);

			if($result_dist['status'] > 0 && $result_dist_coa['status'] > 0) {
				$msg = 'Berhasil merubah data distributor';

				$this->log_activity->insert_activity('update', $msg. ' dengan kode distributor ' .$kode_distributor);
				$result = array('success' => true, 'message' => $msg);
			}else{
				$msg = 'gagal merubah data distributor di database';

				$this->log_activity->insert_activity('update', $msg. ' dengan kode distributor ' .$kode_distributor);
				$result = array('success' => true, 'message' => $msg);
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function delete_distributor() {
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);

		$result_dist 		= $this->distributor_model->delete_distributor($params['id']);
		$result_dist_coa	= $this->distributor_model->delete_distributor_coa($params['id']);

		$msg = 'Berhasil menghapus data distributor.';
		$result = array('success' => true, 'message' => $msg);
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function check_kode(){
		$this->form_validation->set_rules('kode_distributor', 'Kode_Distributor', 'trim|required|min_length[4]|max_length[100]|is_unique[t_eksternal.kode_eksternal]');
		$this->form_validation->set_message('is_unique', 'Kode Distributor Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'Kode Distributor Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}
}