<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Payment_Voucher extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('payment_voucher/payment_voucher_model');
		$this->load->library('log_activity');
		$this->load->library('sequence');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'payment_voucher/views/index');
	}

	function list_payment() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

		$order_fields = array('', 'a.no_bpv', 'a.date_bpv');
		$search = $this->input->get_post('search');
		$search_val = (!empty($search['value'])) ? $search['value'] : null;
		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		if($params['limit'] < 0) $params['limit'] = $this->payment_voucher_model->count_payment($params);
		
		$list = $this->payment_voucher_model->list_payment($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = $params['offset']+1;
		foreach ($list['data'] as $k => $v) {
			$strBtn =
				'<div class="btn-group">'.
					'<a class="btn btn-danger btn-sm" title="Delete" onClick="delete_payment(\''.$v['id_vq'].'\')">'.
						'<i class="fa fa-trash"></i>'.
					'</a>'.
				'</div>';
			array_push($data, array(
				$i++,
				$v['name_eksternal'],
				$v['no_bpv'],
				date('d-M-Y', strtotime($v['date_bpv'])),
				$v['symbol_valas'].' '.number_format(round($v['total_amount']),0,',','.'),
				$strBtn
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_payment() {
		$result = $this->payment_voucher_model->list_bank();
		
		$data = array(
			'list_bank' => $result
		);
		
		$this->load->view('add_modal_view',$data);
	}

	public function get_tujuanCoa() {
		$result_coa = $this->payment_voucher_model->get_data_coa();

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result_coa);
	}
	
	public function get_PurchaseOrder() {
		$result_po = $this->payment_voucher_model->get_purchase_order();

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result_po);
	}

	public function set_PurchaseOrder() {
		$data 			= file_get_contents("php://input");
		$params 		= json_decode($data,true);
		
		$cek_kontrabon 	= $this->payment_voucher_model->cek_kontra_bon($params['id_kontrabon']);
		
		if($cek_kontrabon[0]['id_distributor'] == 0) {
			$result_get 	= $this->payment_voucher_model->get_purchase_order_by_id_kontrabon($params['id_kontrabon']);
		}else{
			$result_get 	= $this->payment_voucher_model->get_purchase_order_by_id_kontrabon_baru($params['id_kontrabon']);
		}
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result_get);
	}

	public function set_idCoa() {
		$data 			= file_get_contents("php://input");
		$params 		= json_decode($data,true);
		
		$result_get 	= $this->payment_voucher_model->get_no_rek($params['id_coa']);
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result_get);
	}
	
	public function get_list_barang() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

		$order_fields = array('', 'no_spb', 'tanggal_spb');
		$search = $this->input->get_post('search');
		$search_val = (!empty($search['value'])) ? $search['value'] : null;
		$search_value = $this->Anti_sql_injection($search_val);

		$id_kontrabon = ($this->input->get_post('id_kontrabon') != FALSE) ? $this->input->get_post('id_kontrabon') : 0;
		$params['id_kontrabon'] = $id_kontrabon;

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;
		
		$list = $this->payment_voucher_model->get_po_spb_kontrabon($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		$username = $this->session->userdata['logged_in']['username'];
		
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;

			array_push($data, array(
				$i,
				$v['stock_code'],
				$v['stock_name'],
				$v['symbol_valas']." ".number_format($v['price'],0,",",".")
			));
		}
		
		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function save_payment() {
		$this->form_validation->set_rules('tgl_bpv', 'Tanggal Payment Voucher', 'trim|required');
		$this->form_validation->set_rules('no_po', 'No Faktur', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$no_voucher 	= $this->sequence->get_no('voucher_bank');
			$tgl_bpv 		= $this->Anti_sql_injection($this->input->post('tgl_bpv', TRUE));
			$no_po 			= $this->Anti_sql_injection($this->input->post('no_po', TRUE));
			$no_faktur 		= $this->Anti_sql_injection($this->input->post('no_faktur', TRUE));
			$id_coa			= $this->Anti_sql_injection($this->input->post('id_coa', TRUE));
			$coa_eksternal	= $this->Anti_sql_injection($this->input->post('coa_eksternal', TRUE));
			$rate_po		= $this->Anti_sql_injection($this->input->post('rate_po', TRUE));
			$tempcoa 		= explode(',', $id_coa);
			$id_valas		= $this->Anti_sql_injection($this->input->post('id_valas', TRUE));
			$jumlah 		= $this->Anti_sql_injection($this->input->post('jumlah', TRUE));
			$arrBank 		= $this->Anti_sql_injection($this->input->post('arrBank', TRUE));
			$tempbank 		= explode(',', $arrBank);
			$arrRek 		= $this->Anti_sql_injection($this->input->post('arrRek', TRUE));
			$temprek 		= explode(',', $arrRek);
			$arrRate 		= $this->Anti_sql_injection($this->input->post('arrRate', TRUE));
			$temprate 		= explode(',', $arrRate);
			$arrNominal 	= $this->Anti_sql_injection($this->input->post('arrNominal', TRUE));
			$tempnominal 	= explode(',', $arrNominal);
			$data = array(
				'no_voucher'	=> $no_voucher,
				'tgl_bpv'		=> date('Y-m-d', strtotime($tgl_bpv)),
				'no_po' 		=> $no_po,
				'jumlah'		=> preg_replace('/\D/', '', floatval($jumlah))
			);

			/*echo "<pre>";
			print_r($this->input->post());
			print_r($data);
			echo "</pre>";
			die;*/

			$result_payment = $this->payment_voucher_model->add_payment($data);
			if ($result_payment['result'] > 0) {
				$this->log_activity->insert_activity('insert', 'Insert Bank Payment Voucher No Voucher : '.$no_voucher);

				$get_id_hp = $this->payment_voucher_model->get_id_hp_by_idkontrabon($no_po);

				$dataCoaEksternal = array(
					'id_coa'		=> $coa_eksternal,
					'id_parent'		=> 0,
					'date'			=> date('Y-m-d'),
					'id_valas'		=> $id_valas,
					'value'			=> preg_replace('/\D/', '', floatval($jumlah)),
					'adjustment'	=> 0,
					'type_cash'		=> 1,
					'note'			=> 'Pembayaran faktur dengan no faktur '.$get_id_hp[0]['no_faktur'],
					'rate'			=> $rate_po,
					'bukti'			=> NULL,
					'id_coa_temp'	=> NULL
				);
				$result_coavalue_eksternal = $this->payment_voucher_model->add_coa_values($dataCoaEksternal);

				$dataKontraBonCoa = array(
					'id_kontra_bon' => $no_po,
					'id_coa_value' => $result_coavalue_eksternal['lastid']
				);
				$insert_kontra_bon_coa = $this->payment_voucher_model->add_kontra_bon_coa($dataKontraBonCoa); 
				for ($i=0; $i < sizeof($tempbank); $i++) { 
					$get_coa_values 	= $this->payment_voucher_model->get_data_coa_id($tempcoa[$i]);
					$hutang_coa 		= $this->payment_voucher_model->get_hutang_coa($no_faktur);
					
					//print_r($hutang_coa[0]['id_coa']);die;
					
					$dataCoa = array(
						'id_coa'		=> $tempcoa[$i],
						'id_parent'		=> 0,
						'date'			=> date('Y-m-d'),
						'id_valas'		=> $id_valas,
						'value'			=> $tempnominal[$i],
						'adjustment'	=> 0,
						'type_cash'		=> 1,
						'note'			=> 'Pembayaran faktur dengan no faktur '.$get_id_hp[0]['no_faktur'],
						'rate'			=> $temprate[$i],
						'bukti'			=> NULL,
						'id_coa_temp'	=> NULL
					);
					$result_coa_values = $this->payment_voucher_model->add_coa_values($dataCoa);
					
					/*$dataCoa2 = array(
						'id_coa'		=> $hutang_coa[0]['id_coa'],
						'id_parent'		=> 0,
						'date'			=> date('Y-m-d'),
						'id_valas'		=> $id_valas,
						'value'			=> $tempnominal[$i],
						'adjustment'	=> 0,
						'type_cash'		=> 0,
						'note'			=> 'Pembayaran faktur dengan no faktur '.$get_id_hp[0]['no_faktur'],
						'rate'			=> $temprate[$i],
						'bukti'			=> NULL,
						'id_coa_temp'	=> NULL
					);
					$result_coa_values2 = $this->payment_voucher_model->add_coa_values($dataCoa2);*/
					
					$dataKartuHP = array(
						'ref'				=> NULL,
						'source'			=> NULL,
						'ket'				=> 'Pembayaran hutang dengan no faktur '.$no_faktur,
						'status'			=> 0,
						'jml_byr'			=> $tempnominal[$i],
						'saldo_akhir'		=> 0,
						'id_valas'			=> $id_valas,
						'type_kartu'		=> 1,
						//'id_master'			=> $get_id_hp[0]['id_master'],
						// 'id_master'			=> $result_coa_values2['lastid'],
						'id_master'			=> $result_coavalue_eksternal['lastid'],
						'type_master'		=> 0,
						'type_status_hp'	=> 0
					);
					$insert_kartu_hp = $this->payment_voucher_model->add_kartu_hp($dataKartuHP);
					$dataHp = array(
						'id_hp'		=> $insert_kartu_hp['lastid'],
						'id_pay_hp'	=> $get_id_hp[0]['id_hp'],
						'amount'	=> $tempnominal[$i],
						'note'		=> 'Pembayaran hutang dengan no faktur '.$no_faktur,
						'rate'		=> $temprate[$i],
						'konversi'	=> $tempnominal[$i] * $temprate[$i]
					);
					$result_payment_hp = $this->payment_voucher_model->add_payment_hp($dataHp);
					
					$dataPayCoa = array(
						'id_pv'			=> $result_payment['lastid'],
						'id_coa'		=> $result_coa_values['lastid']
					);
					$result_coa_values = $this->payment_voucher_model->add_payment_coa($dataPayCoa);

					$dataBank = array(
						'no_rek'	=> $temprek[$i],
						'nama_bank'	=> strtoupper($tempbank[$i]),
						'id_vq' 	=> $result_payment['lastid'],
						'nominal'	=> $tempnominal[$i]
					);
					$result_bank = $this->payment_voucher_model->add_payment_bank($dataBank);

					if($result_bank > 0) $this->log_activity->insert_activity('insert', 'Insert List Bank Payment Voucher Id : '.$tempbank[$i]);
					else $this->log_activity->insert_activity('insert', 'Gagal Insert List Bank Payment Voucher Id = '.$tempbank[$i]);
				}

				if($result_bank > 0) $result = array('success' => true, 'message' => 'Berhasil menambahkan Bank Payment Voucher ke database');
				else $result = array('success' => false, 'message' => 'Gagal menambahkan Bank Payment Voucher ke database');
			}else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert Bank Payment Voucher No Voucher = '.$no_voucher);
				$result = array('success' => false, 'message' => 'Gagal menambahkan Bank Payment Voucher ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}
	
	public function edit_payment($id) {
		$result_payment		= $this->payment_voucher_model->edit_payment($id);
		$result_bank		= $this->payment_voucher_model->edit_payment_bank($id);

		$data = array(
			'pv'		=> $result_payment,
			'pvd'		=> $result_bank,
			'lastSize'	=> sizeof($result_bank)
		);

		$this->load->view('edit_modal_view', $data);
	}

	public function save_edit_payment() {
		$this->form_validation->set_rules('no_voucher', 'No Voucher', 'trim|required');
		$this->form_validation->set_rules('tgl_bpv', 'Tanggal Payment Voucher', 'trim|required');
		$this->form_validation->set_rules('no_po', 'No Purchase Order', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$id_payment 	= $this->Anti_sql_injection($this->input->post('id_payment', TRUE));
			$no_voucher 	= $this->Anti_sql_injection($this->input->post('no_voucher', TRUE));
			$tgl_bpv 		= $this->Anti_sql_injection($this->input->post('tgl_bpv', TRUE));
			$no_po 			= $this->Anti_sql_injection($this->input->post('no_po', TRUE));
			$jumlah 		= $this->Anti_sql_injection($this->input->post('jumlah', TRUE));
			$arrBank 		= $this->Anti_sql_injection($this->input->post('arrBank', TRUE));
			$tempbank 		= explode(',', $arrBank);
			$arrRek 		= $this->Anti_sql_injection($this->input->post('arrRek', TRUE));
			$temprek 		= explode(',', $arrRek);
			$arrNominal 	= $this->Anti_sql_injection($this->input->post('arrNominal', TRUE));
			$tempnominal 	= explode(',', $arrNominal);
			$arrIdBank 		= $this->Anti_sql_injection($this->input->post('arrIdBank', TRUE));
			$tempIdBank 	= explode(',', $arrIdBank);
			
			$data = array(
				'id_payment'	=> $id_payment,
				'no_voucher'	=> $no_voucher,
				'tgl_bpv'		=> date('Y-m-d', strtotime($tgl_bpv)),
				'no_po' 		=> $no_po,
				'jumlah'		=> preg_replace('/\D/', '', $jumlah)
			);

			$result_payment = $this->payment_voucher_model->save_edit_payment($data);
			if ($result_payment > 0) {
				$this->log_activity->insert_activity('insert', 'Update Bank Payment Voucher No Voucher : '.$no_voucher);

				for ($i=0; $i < sizeof($temprek); $i++) {
					if($tempIdBank[$i] != 0) {
						$dataBank = array(
							'id_detail' 	=> $tempIdBank[$i],
							'no_rek' 		=> $temprek[$i],
							'nama_bank' 	=> strtoupper($tempbank[$i]),
							'nominal'		=> $tempnominal[$i]
						);
						$result_bank = $this->payment_voucher_model->save_edit_payment_bank($dataBank);

						if($result_bank > 0) $this->log_activity->insert_activity('insert', 'Update List Bank Payment Voucher Id : '.$tempIdBank[$i]);
						else $this->log_activity->insert_activity('insert', 'Gagal Update List Bank Payment Voucher Id = '.$tempIdBank[$i]);
					}else {
						$dataBank = array(
							'no_rek' 		=> $temprek[$i],
							'nama_bank' 	=> strtoupper($tempbank[$i]),
							'id_vq' 		=> $id_payment,
							'nominal' 		=> $tempnominal[$i]
						);
						$result_bank = $this->payment_voucher_model->add_payment_bank($dataBank);

						if($result_bank > 0) $this->log_activity->insert_activity('insert', 'Insert List Bank Payment Voucher Id : '.$tempbank[$i]);
						else $this->log_activity->insert_activity('insert', 'Gagal Insert List Bank Payment Voucher Id = '.$tempbank[$i]);
					}
				}

				if($result_bank > 0) $result = array('success' => true, 'message' => 'Berhasil menambahkan Bank Payment Voucher ke database');
				else $result = array('success' => false, 'message' => 'Gagal menambahkan Bank Payment Voucher ke database');
			}else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert Bank Payment Voucher No Voucher = '.$no_voucher);
				$result = array('success' => false, 'message' => 'Gagal menambahkan Bank Payment Voucher ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}
	
	public function delete_payment() {
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		
		$result = $this->payment_voucher_model->payment_delete($params);
		
		if($params['id_vq'] != '') {
			$this->log_activity->insert_activity('delete', 'Delete Bank Payment Voucher id : '.$params['id_vq']);
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}else {
			$this->log_activity->insert_activity('delete', 'Gagal Delete Bank Payment Voucher id : '.$params['id_vq']);
			$res = array('status' => 'failed', 'message' => 'Data gagal di hapus');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
}