<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment_Voucher_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function count_payment($params) {
		$sql 	= 'CALL pv_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,array(NULL, NULL, NULL, NULL, NULL));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		return $total['@total'];
	}

	public function list_payment($params) {
		$sql 	= 'CALL pv_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function get_data_coa_id($id_coa) {
		$sql 	= 'CALL coa_search_id(?)';

		$query 	= $this->db->query($sql,array(
			$id_coa
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function get_hutang_coa($id_coa) {
		$sql 	= 'SELECT a.id_kontra_bon,a.no_faktur,e.id_hp,e.id_master,c.id_coa
					 FROM t_kontra_bon a
					LEFT JOIN t_kontra_bon_coa b ON a.id_kontra_bon=b.id_kontra_bon
					LEFT JOIN t_coa_value c ON b.coa_value=c.id
					LEFT JOIN t_kartu_hp e ON c.id=e.id_master
					WHERE a.no_faktur = "'.$id_coa.'" GROUP BY a.id_kontra_bon ';

		$query 	= $this->db->query($sql);
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_coa_values($data) {
		$sql 	= 'CALL coavalue_add4(?,?,?,?,?,?,?,?,?,?,?)';
		
		$query 	= $this->db->query($sql,array(
			$data['id_coa'],
			$data['id_parent'],
			$data['date'],
			$data['id_valas'],
			$data['value'],
			$data['adjustment'],
			$data['type_cash'],
			$data['note'],
			$data['rate'],
			$data['bukti'],
			$data['id_coa_temp']
		));
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$result['code']	= $this->db->affected_rows();
		$result['list']	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['code'] = $result['code'];
		$arr_result['results'] = $result['list'];
		
		return $arr_result;
	}
	
	public function list_bank($id_coa=null){
		// $sql 	= 'SELECT a.*, b.*, c.* 
							// FROM `t_bank` a
							// LEFT JOIN  `t_coa` b ON a.`id_coa` = b.`id_coa`
							// LEFT JOIN `m_valas` c ON a.`id_valas` = c.`valas_id`';
		// if($id_coa) $sql = $sql.'WHERE a.id_coa ='.$id_coa;	
		
		$sql 	= 'SELECT * FROM t_coa order by coa';
		if($id_coa) $sql = $sql.'WHERE a.id_coa ='.$id_coa;
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function get_no_rek($id_coa){
		$sql 	= 'SELECT a.*, b.*, c.* 
						FROM `t_bank` a
						LEFT JOIN  `t_coa` b ON a.`id_coa` = b.`id_coa`
						LEFT JOIN `m_valas` c ON a.`id_valas` = c.`valas_id`
						WHERE a.id_coa ='.$id_coa;
						
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function get_purchase_order() {
		$sql 	= "SELECT a.id_kontra_bon, a.no_faktur FROM t_kontra_bon a
					 LEFT JOIN t_payment_voucher b ON a.id_kontra_bon= b.id_kontra_bon
					 WHERE b.`id_kontra_bon`IS NULL";
					 
		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function cek_kontra_bon($id_kontrabon) {
		$sql 	= 
			'SELECT
				a.*,
				b.`rate` AS rate_po, b.`id_coa` AS coa_po
			FROM `t_kontra_bon` a
			LEFT JOIN `t_purchase_order` b ON b.`id_po` = a.`id_purchase_order`
			WHERE a.`id_kontra_bon` = ?';

		$query 	= $this->db->query($sql, array(
			$id_kontrabon
		));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_purchase_order_by_id_kontrabon($id_kontrabon) {
		$sql 	=
			'SELECT
				a.*,
				a.rate AS rate_po,
				b.*,
				SUM(c.qty_diterima*(c.unit_price-c.diskon)) AS total_amount_fix,
				d.name_eksternal
			FROM t_purchase_order a 
			LEFT JOIN t_kontra_bon b ON a.id_po=b.id_purchase_order
			LEFT JOIN t_po_spb c ON a.id_po=c.id_po
			LEFT JOIN t_eksternal d ON a.id_distributor=d.id
			WHERE b.id_kontra_bon = ?
			GROUP BY b.id_kontra_bon';

		$query 	= $this->db->query($sql, array(
			$id_kontrabon
		));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_purchase_order_by_id_kontrabon_baru($id_kontrabon) {
		$sql 	=
			'SELECT
				a.*, b.name_eksternal,
				d.`rate` AS rate_eksternal
			FROM t_kontra_bon a
			LEFT JOIN t_eksternal b ON a.id_distributor = b.id
			LEFT JOIN t_coa c ON b.id = c.id_eksternal
			LEFT JOIN `t_purchase_order` d ON a.`id_purchase_order` = d.`id_po`
			WHERE a.id_kontra_bon = ?';

		$query 	= $this->db->query($sql, array(
			$id_kontrabon
		));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_purchase_order_by_id_kontrabon_copy($id_kontrabon) {
		$sql 	= 'CALL kb_search_id(?)';

		$query 	= $this->db->query($sql,array(
			$id_kontrabon
		));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_id_hp_by_idkontrabon($id_kontrabon) {
		$sql 	= 'CALL pospb_list_search_idhp_by_idkontrabon(?)';

		$query 	= $this->db->query($sql,array(
			$id_kontrabon
		));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_po_spb_kontrabon($params) {
		$sql 	= 'CALL pospb_list_search_id_kontrabon(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['id_kontrabon'],
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();
		
		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);
		
		return $return;
	}
	
	public function get_po_spb($params) {
		$sql 	= 'CALL pospb_list_search_id(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['id_po'],
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function add_kartu_hp($data)
	{
		$sql 	= 'CALL hp_add_pays(?,?,?,?,?,?,?,?,?,?,?)';
		
		$query 	= $this->db->query($sql,array(
			$data['ref'],
			$data['source'],
			$data['ket'],
			$data['status'],
			$data['jml_byr'],
			$data['saldo_akhir'],
			$data['id_valas'],
			$data['type_kartu'],
			$data['id_master'],
			$data['type_master'],
			$data['type_status_hp']
		));
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$result['code']	= $this->db->affected_rows();
		$result['list']	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['code'] = $result['code'];
		$arr_result['results'] = $result['list'];
		
		return $arr_result;
	}
	public function add_kontra_bon_coa($data)
	{
		$sql 	= 'CALL kb_add_coa(?,?)';
		
		$query 	= $this->db->query($sql,array(
			$data['id_kontra_bon'],
			$data['id_coa_value'],
		));
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$result['code']	= $this->db->affected_rows();
		$result['list']	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['code'] = $result['code'];
		$arr_result['results'] = $result['list'];
		
		return $arr_result;
	}
	public function add_payment($data) {
		$sql 	= 'CALL pv_add(?, ?, ?, ?)';

		$query 	=  $this->db->query($sql, array(
				$data['no_voucher'],
				$data['tgl_bpv'],
				$data['no_po'],
				$data['jumlah']
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_payment_hp($data) {
		$sql 	= 'CALL hp_add_payment(?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql, array(
				$data['id_hp'],
				$data['id_pay_hp'],
				$data['amount'],
				$data['note'],
				$data['rate'],
				$data['konversi']
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function add_payment_coa($data) {
		$sql 	= 'CALL pv_coa_add(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['id_pv'],
			$data['id_coa']
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function add_payment_bank($data) {
		$sql 	= 'CALL pv_d_add(?,?,?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['id_vq'],
			$data['no_rek'],
			$data['nama_bank'],
			$data['nominal']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_payment($id) {
		$sql 	= 'CALL pv_search_id(?)';

		$out = array();
		$query 	=  $this->db->query($sql, array(
			$id
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_payment_bank($id) {
		$sql 	= 'CALL pv_d_search_id(?)';

		$out = array();
		$query 	=  $this->db->query($sql, array(
			$id
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function save_edit_payment($data) {
		$sql 	= 'CALL pv_update(?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id_payment'],
				$data['no_voucher'],
				$data['tgl_bpv'],
				$data['no_po'],
				$data['jumlah']
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function save_edit_payment_bank($data) {
		$sql 	= 'CALL pv_d_update(?,?,?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['id_detail'],
			$data['no_rek'],
			$data['nama_bank'],
			$data['nominal']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function payment_delete($data) 
	{
		$sql 	= 'CALL pv_delete(?)';
		
		$query 	=  $this->db->query($sql,array(
			$data['id_vq']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}