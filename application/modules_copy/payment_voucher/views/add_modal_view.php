<form class="form-horizontal form-label-left" id="add_form" role="form" action="<?php echo base_url('payment_voucher/save_payment');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tgl_bpv">Tanggal Terbit <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="tgl_bpv" name="tgl_bpv" class="form-control" placeholder="Tanggal Payment Voucher" autocomplete="off" required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_po">No Faktur <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="no_po" name="no_po" style="width: 100%" required>
				<option value="" selected>... Pilih No Faktur ...</option>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<div class="col-md-8 col-md-offset-3">
			<table id="list_barang" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th style="text-align: center; width: 5%;">No</th>
						<th style="width: 20%;">Kode Stok</th>
						<th>Nama Stok</th>
						<th style="width: 10%;">Amount</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jumlah">Jumlah</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="jumlah" name="jumlah" class="form-control" placeholder="Jumlah" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="paid_to">Paid To</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="paid_to" name="paid_to" class="form-control" placeholder="Nama Distributor" autocomplete="off" readonly>
			<input type="hidden" id="coa_eksternal" name="coa_eksternal">
			<input type="hidden" id="rate_po" name="rate_po">
		</div>
	</div>

	<div class="item form-group" id="div_list_bank">
		<div class="col-md-8 col-md-offset-3">
			<table id="list_bank" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th style="width: 35%;">Bank</th>
						<th>No Rekening</th>
						<th>Rate</th>
						<th>Nominal</th>
						<td style="width: 5%;"><a id="btn_bank_add" class="btn btn-primary"><i class="fa fa-plus"></i></a></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<select class="list_coa form-control" id="tujuan1"  name="namaBank[]" style="width: 100%" required>
								<option value="" selected>COA</option>
								<?php foreach($list_bank as $bank => $b) { ?>
									<option value="<?php echo $b['id_coa'];?>"><?php echo $b['coa'].' - '.$b['keterangan']; ?></option>
								<?php } ?>
							</select>
						</td>
						<td>
							<input type="text" class="form-control" id="no_rek1" name="no_rek[]" placeholder="No Rekening" autocomplete="off">
						</td>
						<td>
							<input type="text" class="form-control" id="rate1" name="rate[]" placeholder="Rate" value="1" autocomplete="off" required>
						</td>
						<td>
							<input type="number" class="form-control" id="nominal_bank1" name="nominal_bank[]" value="0" placeholder="Nominal" autocomplete="off" required>
						</td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	
	<div class="item form-group">
		<input type="hidden" id="id_valas" name="id_valas" class="form-control" value="<?php if(isset($list_bank[0]['id_valas'])) { echo $list_bank[0]['id_valas']; } ?>" readonly>
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Payment Voucher</button>
		</div>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	var idRowBank 	= 1;
	var arrBank 	= [];
	var arrRek 		= [];
	var arrRate		= [];
	var arrNominal 	= [];
	
	$(document).ready(function() {
		$('#tgl_bpv').datepicker({
			format: "d-M-yyyy",
			autoclose: true,
			todayHighlight: true
		});

		$('.list_coa').select2();
		
		$('#no_po').select2();
		$('#no_po').on('change', function() {
			if(this.value != '') 
				set_PurchaseOrder(this.value);
			else {
				$('#jumlah').val('');
				$('#paid_to').val('');
			}
		});
		
		$('#tujuan1').on('change', function() {
			if(this.value != '') setRek(this.value);
			else {
				$('#no_rek1').val('');
			}
		});
		
		//get_Coa();
		get_PurchaseOrder();
		setTable(0);

		$('#btn_bank_add').on('click', function() {
			idRowBank++;
			$('#list_bank tbody').append(
				'<tr id="trRowBank'+idRowBank+'">'+
					'<td>'+
						'<select class="list_coa form-control" id="tujuan'+idRowBank+'" name="namaBank[]" style="width: 100%" required>'+
							'<option value="" selected>COA</option>'+
							<?php if (is_array($list_bank) || is_object($list_bank)) {
									foreach ($list_bank as $bank) { ?>
										'<option value="'+'<?php echo $bank['id_coa'];?>'+'">'+'<?php echo $bank['coa'].' - '.$bank['keterangan']; ?>'+'</option>'+
							<?php } } ?>
						'</select>'+
					'</td>'+
					'<td>'+
						'<input type="text" class="form-control" id="no_rek'+idRowBank+'" name="no_rek[]" placeholder="No Rekening" autocomplete="off">'+
					'</td>'+
					'<td>'+
						'<input type="text" class="form-control" id="rate'+idRowBank+'" name="rate[]" placeholder="Rate" value="1" autocomplete="off" required>'+
					'</td>'+
					'<td>'+
						'<input type="number" class="form-control" id="nominal_bank'+idRowBank+'" name="nominal_bank[]" value="0" placeholder="Nominal" autocomplete="off">'+
					'</td>'+
					'<td id="td_btn">'+
						'<a class="btn btn-danger" onclick="removeRow('+idRowBank+')"><i class="fa fa-minus"></i></a>'+
					'</td>'+
				'</tr>'
			);
			$('#tujuan'+idRowBank).select2();
			$('#tujuan'+idRowBank).on('change', function() {
			if(this.value != '') setRekening(this.value,idRowBank);
				else {
					$('#no_rek'+idRowBank).val('');
				}
			});
			
		});
	});

	function setRek(id_coa) {
		var datapost = {"id_coa" : id_coa};
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('payment_voucher/set_idCoa');?>",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					// $('#no_rek1').val(r[0].no_rek);
				}else {
					// $('#no_rek1').val('');
				}
			}
		});
	}
	
	function setRekening(id_coa,idRowBank) {
		var datapost = {"id_coa" : id_coa};
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('payment_voucher/set_idCoa');?>",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					// $('#no_rek'+idRowBank).val(r[0].no_rek);
				}else {
					// $('#no_rek'+idRowBank).val('');
				}
			}
		});
	}
	
	function get_PurchaseOrder() {
		var html = '';
		var html = '';
		$.ajax({
			type: "GET",
			url: "<?php echo base_url('payment_voucher/get_PurchaseOrder');?>",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					for (var i = 0; i < r.length; i++) {
						var newOption = new Option(r[i].no_faktur, r[i].id_kontra_bon, false, false);
						$('#no_po').append(newOption);
					}
					$('#no_po').removeAttr('disabled');
				}else{
					html +='<option value="" selected>Tidak ada No Faktur</option>';
					$('#no_po').append(html);
				}
			}
		});
	}
			
	function set_PurchaseOrder(id_kontrabon) {
		var datapost = {"id_kontrabon" : id_kontrabon};
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('payment_voucher/set_PurchaseOrder');?>",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				console.log(r);
				if(r[0].id_distributor == 0) {
					$('#id_valas').val(r[0].id_valas);
					$('#jumlah').val(r[0].amount);
					$('#paid_to').val(r[0].name_eksternal);
					$('#coa_eksternal').val(r[0].id_coa);
					$('#rate_po').val(r[0].rate_po);
					setTable(id_kontrabon);
				}else if (r[0].id_distributor != 0) {
					$('#id_valas').val(r[0].id_valas);
					$('#jumlah').val(r[0].amount);
					$('#paid_to').val(r[0].name_eksternal);
					$('#coa_eksternal').val(r[0].id_coa);
					$('#rate_po').val(r[0].id_coa);
					setTable(id_kontrabon);
				}else{
					$('#id_valas').val('');
					$('#jumlah').val('');
					$('#paid_to').val('');
				}
			}
		});
	}

	function setTable(id_kontrabon) {
		$("#list_barang").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'payment_voucher/get_list_barang?id_kontrabon="+id_kontrabon+"';?>",
			"searching": false,
			"responsive": true,
			"lengthChange": false,
			"destroy": true,
			"info": false,
			"bSort": false,
			"dom": 'lfrtip',
			"columnDefs": [{
				targets: [0],
				className: 'dt-body-center'
			},{
				targets: [3],
				className: 'dt-body-right'
			}]
		});
	}

	function removeRow(rowBank) {
		$('#trRowBank'+rowBank).remove();
	}

	$('#add_form').on('submit',(function(e) {
		arrBank 			= [];
		arrNameBank 		= [];
		arrRek 				= [];
		arrRate				= [];
		arrNominal 			= [];
		var jumlahNominal 	= 0;
		var jumlahSaldo 	= parseFloat($('#jumlah').val());
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();

		$('select[name="namaBank[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') 
					arrBank.push(this.value);
					arrNameBank.push($(this).find("option:selected").text());
			}
		})

		$('input[name="no_rek[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arrRek.push(this.value);
				else arrRek.push('');
			}
			else arrRek.push('');
		})
		
		$('input[name="rate[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arrRate.push(this.value);
				else arrRate.push('');
			}
			else arrRate.push('');
		})

		$('input[name="nominal_bank[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '' && this.value > 0) arrNominal.push(this.value);
				else arrNominal.push(0);
				jumlahNominal = jumlahNominal + parseFloat(this.value);
			}
		})
		
		//if(arrBank.length > 0 && arrRek.length > 0 && arrNominal.length > 0) {
		if(arrBank.length > 0 && arrNominal.length > 0) {
			if(jumlahSaldo == jumlahNominal) {
				var formData = new FormData();
				formData.set('no_voucher',		$('#no_voucher').val());
				formData.set('tgl_bpv',			$('#tgl_bpv').val());
				formData.set('no_po',			$('#no_po').val());
				formData.set('no_faktur',		$('#no_po option:selected').text());
				formData.set('id_coa',			arrBank);
				formData.set('id_valas',		$('#id_valas').val());
				formData.set('jumlah',			$('#jumlah').val());
				formData.set('coa_eksternal',	$('#coa_eksternal').val());
				formData.set('rate_po',			$('#rate_po').val());
				formData.set('jumlahSaldo',		jumlahSaldo);
				formData.set('jumlahNominal',	jumlahNominal);
				formData.set('arrBank',			arrNameBank);
				formData.set('arrRek',			arrRek);
				formData.set('arrRate',			arrRate);
				formData.set('arrNominal',		arrNominal);

				$.ajax({
					type:'POST',
					url: $(this).attr('action'),
					data: formData,
					cache:false,
					contentType: false,
					processData: false,
					success: function(response) {
						if (response.success == true) {
							swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () {
								window.location.href = "<?php echo base_url('payment_voucher');?>";
							})
						}else {
							$('#btn-submit').removeAttr('disabled');
							$('#btn-submit').text("Tambah Payment Voucher");
							swal("Failed!", response.message, "error");
						}
					}
				}).fail(function(xhr, status, message) {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Tambah Payment Voucher");
					swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
				});
			}else {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Tambah Payment Voucher");
				swal("Failed!", "Nominal Bank tidak sama dengan Jumlah yang ada", "error");
			}
		}else {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah Payment Voucher");
			swal("Failed!", "Invalid Inputan Bank, silahkan cek kembali.", "error");
		}
	}));
</script>