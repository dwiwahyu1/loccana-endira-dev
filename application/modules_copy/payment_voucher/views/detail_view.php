<div class="btn-group pull-left">
	<a class="btn btn-danger" onClick="back_list()"><i class="fa fa-arrow-left"></i> Kembali</a>
</div>
<table id="list_stokOpname" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th style="text-align: center;">No</th>
			<th>Kode BC</th>
			<th>Kode Stok</th>
			<th>Nama Stok</th>
			<th>Unit</th>
			<th style="text-align: center; width: 5%;">Type</th>
			<th style="text-align: center; width: 5%;">Stok Awal</th>
			<th style="text-align: center; width: 6%;">Stok Perubahan</th>
			<th style="text-align: center; width: 5%;">Stok Akhir</th>
			<th>Ket</th>
			<th>Tanggal Perubahan</th>
			<th></th>
		</tr>
	</thead>
	<tbody></tbody>
</table>

<script type="text/javascript">
	$(document).ready(function(){
		get_list_opname('<?php echo $periode; ?>');
	});

	function back_list() {
		$('#div_list_opname').html('');
		$('#div_list_opname').hide();
		$('#div_opname_master').show();
		$('#title_menu').html('Stok Opname');
	}

	function get_list_opname(periode) {
		var jsPeriode 				= new Date(periode);
		var titlePeriode 			= jsPeriode.getDate() +'-'+ months[jsPeriode.getMonth()] +'-'+ jsPeriode.getFullYear();
		var lengthRecordDefault 	= 10;
		var lengthRecordAll 		= -1;

		tableOpname = $("#list_stokOpname").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'stok_opname/list_opname?periode="+periode+"';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false,
			"destroy": true,
			"info": false,
			"bSort": false,
			"dom": 'Blrtip',
			"buttons" : [{
				extend: 'excelHtml5',
				text: '<i class="fa fa-file-excel-o" title="Excel"></i>',
				filename : 'Stok_Opname_'+titlePeriode,
				header : true,
				action: function (e, dt, button, node, config) {
					$('.buttons-excel').attr('disabled', 'disabled');
					var excelButtonConfig 				= $.fn.DataTable.ext.buttons.excelHtml5;
					excelButtonConfig.filename 			= 'Stok_Opname_'+titlePeriode;
					excelButtonConfig.exportOptions 	= {
						columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
					};
					
					$.ajax({
						type: "POST",
						url: "<?php echo base_url().'stok_opname/list_opname?periode="+periode+"&length=-1';?>",
						dataType: 'json',
						contentType: 'application/json; charset=utf-8',
						success: function(response) {
							tableOpname.api().rows().remove();
							tableOpname.api().rows.add(response.data);
							excelButtonConfig.action(e, dt, button, excelButtonConfig);
							$('.buttons-excel').removeAttr('disabled');
						}
					});
				}
			}, {
				extend 			: 'pdfHtml5',
				text 			: '<i class="fa fa-file-pdf-o" title="Pdf"></i>',
				action : function(e, dt, button, node, config) {
					$('.buttons-pdf').attr('disabled', 'disabled');
					var pdfButtonConfig 			= $.fn.DataTable.ext.buttons.pdfHtml5;
					pdfButtonConfig.filename 		= 'Stok_Opname_'+titlePeriode;
					pdfButtonConfig.title 			= 'Stok Opname Periode '+titlePeriode;
					pdfButtonConfig.orientation 	= 'landscape';
					pdfButtonConfig.pageSize 		= 'A4';
					// pdfButtonConfig.customize 		= function(doc) {
					// 	if(doc.content[1].table.body) {
					// 		for (var i = 0; i < doc.content[1].table.body.length; i++) {
					// 			doc.content[1].table.body[i][0].alignment = 'center';
					// 			doc.content[1].table.body[i][2].alignment = 'center';
					// 			doc.content[1].table.body[i][6].alignment = 'center';
					// 			doc.content[1].table.body[i][7].alignment = 'center';
					// 			doc.content[1].table.body[i][8].alignment = 'center';
					// 		}
					// 	}
					// };
					pdfButtonConfig.exportOptions 	= {
						columns: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
					};
					$.ajax({
						type: "POST",
						url: "<?php echo base_url().'stok_opname/list_opname?periode="+periode+"&length=-1';?>",
						dataType: 'json',
						contentType: 'application/json; charset=utf-8',
						success: function(response) {
							tableOpname.api().rows().remove();
							tableOpname.api().rows.add(response.data);
							pdfButtonConfig.action(e, dt, button, pdfButtonConfig);
							$('.buttons-pdf').removeAttr('disabled');
						}
					});
				}
			}],
			"columnDefs": [{
				targets: [0,1,2],
				className: 'dt-body-center'
			},{
				targets: [6],
				className: 'dt-body-right'
			}]
		});
	}

	function getAllData(periode) {
		$.ajax({
			type: "POST",
			url: "<?php echo base_url().'stok_opname/list_opname?periode="+periode+"&length=-1';?>",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				if (response.status != "success") swal("Failed!", response.message, "error");
				else return response;
			}
		});
	}
</script>