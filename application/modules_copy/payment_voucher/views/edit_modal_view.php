<form class="form-horizontal form-label-left" id="edit_form" role="form" action="<?php echo base_url('payment_voucher/save_edit_payment');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_voucher">No Voucher <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="no_voucher" name="no_voucher" class="form-control" placeholder="No Voucher" autocomplete="off"
			value="<?php if(isset($pv[0]['no_bpv'])){ echo $pv[0]['no_bpv']; }?>" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tgl_bpv">Tanggal <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="tgl_bpv" name="tgl_bpv" class="form-control" placeholder="Tanggal Payment Voucher" autocomplete="off"
			value="<?php if(isset($pv[0]['date_bpv'])){ echo date('d-M-Y', strtotime($pv[0]['date_bpv'])); }?>" required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_po">No Purchase Order <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="no_po" name="no_po" style="width: 100%" required>
				<option value="" selected>... Pilih No PO ...</option>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_btb">No BTB</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="no_btb" name="no_btb" class="form-control" placeholder="No BTB" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group">
		<div class="col-md-12" style="padding-left: 50px; padding-right: 50px;">
			<table id="list_barang" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th style="text-align: center; width: 5%;">No</th>
						<th style="width: 20%;">Kode Stok</th>
						<th>Nama Stok</th>
						<th style="width: 10%;">Amount</th>
					</tr>
				</thead>
				<tbody></tbody>
				<!-- <tfoot>
					<tr>
						<th colspan="3" style="text-align: right;">Total</th>
						<td id="tdFootTotal">0</td>
					</tr>
				</tfoot> -->
			</table>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jumlah">Jumlah</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="jumlah" name="jumlah" class="form-control" placeholder="Jumlah" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="paid_to">Paid To</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="paid_to" name="paid_to" class="form-control" placeholder="Nama Distributir" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group" id="div_list_bank">
		<table id="list_bank" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th style="width: 35%;">Bank</th>
					<th>No Rekening</th>
					<th>Nominal</th>
					<td style="width: 5%;"><a id="btn_bank_add" class="btn btn-primary"><i class="fa fa-plus"></i></a></td>
				</tr>
			</thead>
			<tbody>
		<?php
			$no_kpvd = 0;
			foreach ($pvd as $kPvd => $vPvd) {
				$no_kpvd++; ?>
				<tr>
					<td>
						<input type="text" class="form-control" id="bank<?php echo $no_kpvd; ?>" name="bank[]" placeholder="Nama Bank"
						value="<?php echo strtoupper($vPvd['nama_bank']); ?>" autocomplete="off" required>
					</td>
					<td>
						<input type="number" class="form-control" id="no_rek<?php echo $no_kpvd; ?>" name="no_rek[]" placeholder="No Rekening"
						value="<?php echo strtoupper($vPvd['no_rekening']); ?>" autocomplete="off" required>
					</td>
					<td>
						<input type="number" class="form-control" id="nominal_bank<?php echo $no_kpvd; ?>" name="nominal_bank[]" value="<?php
							echo $vPvd['nominal'];
						?>" placeholder="Nominal" autocomplete="off" required>
					</td>
					<td>
						<input type="hidden" id="id_bank<?php echo $no_kpvd; ?>" name="id_bank[]" value="<?php echo $vPvd['id_detail_vq']; ?>">
					</td>
				</tr>
		<?php
			} ?>
			</tbody>
		</table>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Payment Voucher</button>
			<input type="hidden" id="id_payment" name="id_payment" value="<?php if(isset($pv[0]['id_vq'])){ echo $pv[0]['id_vq']; }?>">
		</div>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	var idRowBank 	= <?php if(isset($lastSize)) echo $lastSize; ?>;
	var arrBank 	= [];
	var arrRek 		= [];
	var arrNominal 	= [];
	var arrIdBank 	= [];
	$(document).ready(function() {
		$('#tgl_bpv').datepicker({
			format: "d-M-yyyy",
			autoclose: true,
			todayHighlight: true
		});

		$('#no_po').select2();
		$('#no_po').on('change', function() {
			if(this.value != '') set_PurchaseOrder(this.value);
			else {
				$('#no_btb').val('');
				$('#jumlah').val('');
				$('#paid_to').val('');
			}
		});
		get_PurchaseOrder();
		setTable(0);

		$('#btn_bank_add').on('click', function() {
			idRowBank++;
			$('#list_bank tbody').append(
				'<tr id="trRowBank'+idRowBank+'">'+
					'<td>'+
						'<input type="text" class="form-control" id="bank'+idRowBank+'" name="bank[]" placeholder="Nama Bank" autocomplete="off" required>'+
					'</td>'+
					'<td>'+
						'<input type="number" class="form-control" id="no_rek'+idRowBank+'" name="no_rek[]" placeholder="No Rekening" autocomplete="off" required>'+
					'</td>'+
					'<td>'+
						'<input type="number" class="form-control" id="nominal_bank'+idRowBank+'" name="nominal_bank[]" value="0" placeholder="Nominal" autocomplete="off">'+
					'</td>'+
					'<td id="td_btn">'+
						'<a class="btn btn-danger" onclick="removeRow('+idRowBank+')"><i class="fa fa-minus"></i></a>'+
						'<input type="hidden" id="id_bank'+idRowBank+'" name="id_bank[]" value="0">'+
					'</td>'+
				'</tr>'
			);
		});
	});

	function get_PurchaseOrder() {
		$('#no_po').attr('disabled', 'disabled');
		var poAwal = "<?php if(isset($pv[0]['id_purchase_order'])){ echo $pv[0]['id_purchase_order']; }?>";
		$.ajax({
			type: "GET",
			url: "<?php echo base_url('payment_voucher/get_PurchaseOrder');?>",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					for (var i = 0; i < r.length; i++) {
						if(poAwal == r[i].id_po) {
							var newOption = new Option(r[i].no_po, r[i].id_kontrabon, true, true);
							$('#no_po').append(newOption).trigger('change');
						}else {
							var newOption = new Option(r[i].no_po, r[i].id_kontrabon, false, false);
							$('#no_po').append(newOption);
						}
					}
					$('#no_po').removeAttr('disabled');
				}
			}
		});
	}

	function set_PurchaseOrder(id_kontrabon) {
		var datapost = {"id_kontrabon" : id_kontrabon};
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('payment_voucher/set_PurchaseOrder');?>",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r[0].id_distributor == 0) {
					$('#id_valas').val(r[0].id_valas);
					$('#jumlah').val(r[0].total_amount_fix);
					$('#paid_to').val(r[0].name_eksternal);
					setTable(id_kontrabon);
				}else if (r[0].id_distributor != 0) {
					$('#id_valas').val(r[0].id_valas);
					$('#jumlah').val(r[0].amount);
					$('#paid_to').val(r[0].name_eksternal);
					setTable(id_kontrabon);
				}else{
					$('#id_valas').val('');
					$('#jumlah').val('');
					$('#paid_to').val('');
				}
			}
		});
	}

	function setTable(id_po) {
		$("#list_barang").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'payment_voucher/get_list_barang?id_po="+id_po+"';?>",
			"searching": false,
			"responsive": true,
			"lengthChange": false,
			"destroy": true,
			"info": false,
			"bSort": false,
			"dom": 'lfrtip',
			"columnDefs": [{
				targets: [0],
				className: 'dt-body-center'
			},{
				targets: [3],
				className: 'dt-body-right'
			}]
		});
	}

	function removeRow(rowBank) {
		$('#trRowBank'+rowBank).remove();
	}

	$('#edit_form').on('submit',(function(e) {
		arrBank 	= [];
		arrRek 		= [];
		arrNominal 	= [];
		arrIdBank	= [];
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();

		$('input[name="bank[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arrBank.push(this.value);
			}
		})

		$('input[name="no_rek[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arrRek.push(this.value);
			}
		})

		$('input[name="nominal_bank[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '' && this.value > 0) arrNominal.push(this.value);
			}
		})

		$('input[name="id_bank[]"]').each(function() {
			if(this.value) {
				arrIdBank.push(this.value);
			}
		})

		if(arrBank.length > 0 && arrRek.length > 0 && arrNominal.length > 0 && arrIdBank.length > 0) {
			var formData = new FormData();
			formData.set('id_payment',	$('#id_payment').val());
			formData.set('no_voucher',	$('#no_voucher').val());
			formData.set('tgl_bpv',		$('#tgl_bpv').val());
			formData.set('no_po',		$('#no_po').val());
			formData.set('jumlah',		$('#jumlah').val());
			formData.set('arrBank',		arrBank);
			formData.set('arrRek',		arrRek);
			formData.set('arrNominal',	arrNominal);
			formData.set('arrIdBank',	arrIdBank);

			$.ajax({
				type:'POST',
				url: $(this).attr('action'),
				data: formData,
				cache:false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function () {
							window.location.href = "<?php echo base_url('payment_voucher');?>";
						})
					}else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Tambah Payment Voucher");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Tambah Payment Voucher");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}else {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah Payment Voucher");
			swal("Failed!", "Invalid Inputan Bank, silahkan cek kembali.", "error");
		}
	}));
</script>