<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Uom extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('uom/uom_model');
    }

    /**
     * anti sql injection
     */
    public function Anti_sql_injection($string) {
        $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
        return $string;
    }

    public function mail_service($result) {
        $name = 'Cashier App';
        $from = 'mail@google.com';
        $to = $result['mail_to']; //change this
        $subject = $result['mail_subject']; //change this

        $header = $result['mail_header']; //change this
        $body = $result['mail_body']; //change this
        $footer = $result['mail_footer']; //change this
        // Timpa isi email dengan data
        $a = array('xxheaderxx', 'xxbodyxx', 'xxfooterxx');
        $b = array($header, $body, $footer);

        $template = APPPATH . 'modules/template/email_default.html';
        $fd = fopen($template, "r");
        $message = fread($fd, filesize($template));
        fclose($fd);
        $message = str_replace($a, $b, $message);


        $this->load->library('email'); // load email library
        $this->email->from($from, $name);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        if ($this->email->send()) {
            return 1;
        } else {
            return 0;
        }
    }

    public function index() {
        $this->template->load('maintemplate', 'uom/views/index');
    }

    function lists() {

        $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
        $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
        $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
        $order = $this->input->get_post('order');
        $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
        $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

        $order_fields = array('', 'uom_name', 'uom_symbol');

        $search = $this->input->get_post('search');

        $search_val = (!empty($search['value'])) ? $search['value'] : null;

        $search_value = $this->Anti_sql_injection($search_val);

        // Build params for calling model
        $params['limit'] = (int) $length;
        $params['offset'] = (int) $start;
        $params['order_column'] = $order_fields[$order_column];
        $params['order_dir'] = $order_dir;
        $params['filter'] = $search_value;

		//print_r($params);die;

        $list = $this->uom_model->lists($params);
        //print_r($list);die;

        $result["recordsTotal"] = $list['total'];
        $result["recordsFiltered"] = $list['total_filtered'];
        $result["draw"] = $draw;

		//print_r($result);die;

        $data = array();
        $i = $params['offset'];
        $username = $this->session->userdata['logged_in']['username'];
        foreach ($list['data'] as $k => $v) {
            $i = $i + 1;

            
                $status_akses = '
                <div class="btn-group"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edituom(\'' . $v['id_uom'] . '\')"><i class="fa fa-edit"></i></button></div>
				  <div class="btn-group"><button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteuom(\'' . $v['id_uom'] . '\')"><i class="fa fa-trash"></i></button></div>';
            
            array_push($data, array(
                $i,
                $v['uom_name'],
                $v['uom_symbol'],
                $v['description'],
                $status_akses
                    )
            );
        }

        $result["data"] = $data;

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function add() {
        //$result = $this->uom_model->group();

        $data = array(
            'group' => ''
        );

        $this->load->view('add_modal_view', $data);
    }
	
    public function edit($id) {
		//$id = $this->Anti_sql_injection($this->input->post('sid', TRUE));
		 
        $result = $this->uom_model->edit($id);
		
		//print_r($result);die;
       // $roles = $this->uom_model->roles($id);

        $data = array(
            'uom' => $result,
        );

        $this->load->view('edit_modal_view', $data);
    }

    public function deletes() {
		
		$data 	= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		
		$list = $this->uom_model->deletes($params['id']);
		
		$res = array(
				'status' => 'success',
				'message' => 'Data telah di hapus'
			);
		
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
        echo json_encode($res);
        

    }

  public function edit_uom() {
        $this->form_validation->set_rules('uom', 'uom', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('uom_sym', 'uom_sym', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('uom_ket', 'uom_ket', 'trim|required|max_length[255]');
      

        if ($this->form_validation->run() == FALSE) {

            $pesan = validation_errors();
            $msg = strip_tags(str_replace("\n", '', $pesan));

            $result = array(
                'success' => false,
                'message' => $msg
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        } else {


                $id_uom = $this->Anti_sql_injection($this->input->post('id_uom', TRUE));
                $uom = $this->Anti_sql_injection($this->input->post('uom', TRUE));
                $uom_sym = $this->Anti_sql_injection($this->input->post('uom_sym', TRUE));
                $uom_ket = $this->Anti_sql_injection($this->input->post('uom_ket', TRUE));
               
           
				 // die;
                $message = "";

                $data = array(
                    'id_uom' => $id_uom,
                    'uom' => $uom,
                    'uom_sym' => $uom_sym,
                    'uom_ket' => $uom_ket
                );

                // print_r($data);die;
                // $data = $this->security->xss_clean($data);

                $result = $this->uom_model->edit_uom($data);

                if ($result > 0) {
                    $msg = 'Berhasil merubah uom.';

                    $result = array(
                        'success' => true,
                        'message' => $msg
                    );
                } else {
                    $msg = 'Gagal merubah uom.';

                    $result = array(
                        'success' => false,
                        'message' => $msg
                    );
                }

            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
    }

    public function add_uom() {
        $this->form_validation->set_rules('uom', 'uom', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('uom_sym', 'uom_sym', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('uom_ket', 'uom_ket', 'trim|required|max_length[255]');
      

        if ($this->form_validation->run() == FALSE) {

            $pesan = validation_errors();
            $msg = strip_tags(str_replace("\n", '', $pesan));

            $result = array(
                'success' => false,
                'message' => $msg
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        } else {


                $user_id = $this->session->userdata['logged_in']['user_id'];
                $uom = $this->Anti_sql_injection($this->input->post('uom', TRUE));
                $uom_sym = $this->Anti_sql_injection($this->input->post('uom_sym', TRUE));
                $uom_ket = $this->Anti_sql_injection($this->input->post('uom_ket', TRUE));
               
           
				 // die;
                $message = "";

                $data = array(
                    'user_id' => $user_id,
                    'uom' => $uom,
                    'uom_sym' => $uom_sym,
                    'uom_ket' => $uom_ket
                );

                // print_r($data);die;
                // $data = $this->security->xss_clean($data);

                $result = $this->uom_model->add_uom($data);

                if ($result > 0) {
                    $msg = 'Berhasil menambahkan uom.';

                    $result = array(
                        'success' => true,
                        'message' => $msg
                    );
                } else {
                    $msg = 'Gagal menambahkan uom ke database.';

                    $result = array(
                        'success' => false,
                        'message' => $msg
                    );
                }

            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
    }

 

    public function log() {
        $this->template->load('maintemplate', 'users/views/log_view');
    }
    
    public function eskalasi() {
        $this->template->load('maintemplate', 'users/views/eskalasi');
    }

    function lists_log() {

        $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
        $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
        $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
        $order = $this->input->get_post('order');
        $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'DESC';
        $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 8;

        $order_fields = array('log_id', 'user_id', 'nama', 'group', 'activity_type', 'activity_desc', 'object_name',
            'object_url', 'activity_date');

        $search = $this->input->get_post('search');

        $search_val = (!empty($search['value'])) ? $search['value'] : NULL;
 
        $search_value = $this->Anti_sql_injection($search_val);

        // Build params for calling model
        $params['limit'] = (int) $length;
        $params['offset'] = (int) $start;
        $params['order_column'] = $order_fields[$order_column];
        $params['order_dir'] = $order_dir;
        $params['filter'] = $search_value;

        $list = $this->uom_model->lists_log($params);
//        print_r($list);die;

        $result["recordsTotal"] = $list['total'];
        $result["recordsFiltered"] = $list['total_filtered'];
        $result["draw"] = $draw;

        $data = array();
        foreach ($list['data'] as $k => $v) {
            array_push($data, array(
                $v['log_id'],
                $v['user_id'],
                $v['nama'],
                $v['group'],
                $v['activity_type'],
                $v['activity_desc'],
                $v['object_name'],
                $v['object_url'],
                $v['activity_date']
                    )
            );
        }

        $result["data"] = $data;

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
    
   

    public function add_ekalasi_view() {
        $result = $this->uom_model->lokasi();

        $data = array(
            'lokasi' => $result
        );

        $this->load->view('add_modal_eskalasi', $data);
    }

    public function add_eskalasi() {
        $lokasi_id = $this->Anti_sql_injection($this->input->post('lokasi_id', TRUE));
        $level = $this->Anti_sql_injection($this->input->post('level', TRUE));
        $data = array(
            'lokasi_id' => $lokasi_id,
            'level' => $level
        );
        $level = $this->uom_model->cek_level($data);

        if ($level) {
            $return = array('success' => false, 'message' => 'Level sudah digunakan di lokasi ini');
            $this->output->set_content_type('application/json')->set_output(json_encode($return));
        } else {
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
            $this->form_validation->set_rules('lokasi_id', 'Lokasi', 'required');
            $this->form_validation->set_rules('level', 'level', 'required');

            if ($this->form_validation->run() == FALSE) {

                $pesan = validation_errors();
                $msg = strip_tags(str_replace("\n", '', $pesan));

                $result = array(
                    'success' => false,
                    'message' => $msg
                );

                $this->output->set_content_type('application/json')->set_output(json_encode($result));
            } else {

                $user_id = $this->session->userdata['logged_in']['user_id'];
                $title = $this->Anti_sql_injection($this->input->post('title', TRUE));
                $keterangan = $this->Anti_sql_injection($this->input->post('keterangan', TRUE));
                $lokasi_id = $this->Anti_sql_injection($this->input->post('lokasi_id', TRUE));
                $level = $this->Anti_sql_injection($this->input->post('level', TRUE));


                $data = array(
                    'user_id' => $user_id,
                    'title' => $title,
                    'keterangan' => $keterangan,
                    'lokasi_id' => $lokasi_id,
                    'level' => $level
                );


                $data = $this->security->xss_clean($data);

                $result = $this->uom_model->create_ekalasi($data);

                if ($result > 0) {
                    $msg = 'Berhasil menambahkan eskalasi user.';

                    $result = array(
                        'success' => true,
                        'message' => $msg
                    );
                } else {
                    $msg = 'Gagal menambahkan eskalasi user ke database.';

                    $result = array(
                        'success' => false,
                        'message' => $msg
                    );
                }


                $this->output->set_content_type('application/json')->set_output(json_encode($result));
            }
        }
    }

    public function edit_ekalasi_view($sid) {
        if ($sid) {
            $data = array(
                'sid' => $sid
            );
            $result = $this->uom_model->detail_eskalasi($data);

            if ($result) {
                $data['sid'] = $sid;
                $data['detail_eskalasi'] = $result;

                $data['lokasi'] = $this->uom_model->lokasi();
                $this->load->view('editeskalasi_modal_view', $data);
            } else {
                echo '<h2 style="text-align:center;">Parameter tidak ditemukan!</h2>';
            }
        } else {
            echo '<h2 style="text-align:center;">Tidak ada parameter!</h2>';
        }
    }

    public function edit_eskalasi() {

        $lokasi_id = $this->Anti_sql_injection($this->input->post('lokasi_id', TRUE));
        $level = $this->Anti_sql_injection($this->input->post('level', TRUE));
        $data = array(
            'lokasi_id' => $lokasi_id,
            'level' => $level
        );
        $level = $this->uom_model->cek_level($data);

        if ($level) {
            $return = array('success' => false, 'message' => 'Level sudah digunakan di lokasi ini');
            $this->output->set_content_type('application/json')->set_output(json_encode($return));
        } else {
            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
            $this->form_validation->set_rules('lokasi_id', 'Lokasi', 'required');
            $this->form_validation->set_rules('level', 'level', 'required');

            if ($this->form_validation->run() == FALSE) {

                $pesan = validation_errors();
                $msg = strip_tags(str_replace("\n", '', $pesan));

                $result = array(
                    'success' => false,
                    'message' => $msg
                );

                $this->output->set_content_type('application/json')->set_output(json_encode($result));
            } else {

                $user_id = $this->session->userdata['logged_in']['user_id'];
                $title = $this->Anti_sql_injection($this->input->post('title', TRUE));
                $id = $this->Anti_sql_injection($this->input->post('sid', TRUE));
                $keterangan = $this->Anti_sql_injection($this->input->post('keterangan', TRUE));
                $lokasi_id = $this->Anti_sql_injection($this->input->post('lokasi_id', TRUE));
                $level = $this->Anti_sql_injection($this->input->post('level', TRUE));


                $data = array(
                    'id' => $id,
                    'user_id' => $user_id,
                    'title' => $title,
                    'keterangan' => $keterangan,
                    'lokasi_id' => $lokasi_id,
                    'level' => $level
                );

                // print_r($data);die;

                $data = $this->security->xss_clean($data);

                $result = $this->uom_model->edit_ekalasi($data);

                if ($result > 0) {
                    $msg = 'Berhasil merubah eskalasi user.';

                    $result = array(
                        'success' => true,
                        'message' => $msg
                    );
                } else {
                    $msg = 'Gagal merubah eskalasi user ke database.';

                    $result = array(
                        'success' => false,
                        'message' => $msg
                    );
                }


                $this->output->set_content_type('application/json')->set_output(json_encode($result));
            }
        }
    }

    public function deleted_eskalasi() {
        $_POST = json_decode(file_get_contents("php://input"), true);
        $sid = $this->Anti_sql_injection($this->input->get_post('sid'));

        if ($sid) {
            $data = array(
                'sid' => $sid
            );

            $result = $this->uom_model->deleted_eskalasi($data);

            if ($result > 0) {
                $msg = 'Berhasil menghapus eskalasi.';
                $return = array('success' => true, 'message' => $msg);
                $this->output->set_content_type('application/json')->set_output(json_encode($return));
            } else {
                $msg = 'Gagal menghapus ekalasi!';
                $return = array('success' => false, 'message' => $msg);
                $this->output->set_content_type('application/json')->set_output(json_encode($return));
            }
        } else {
            $msg = 'ekalasi is empty.';
            $return = array('success' => false, 'message' => $msg);
            $this->output->set_content_type('application/json')->set_output(json_encode($return));
        }
    }

}
