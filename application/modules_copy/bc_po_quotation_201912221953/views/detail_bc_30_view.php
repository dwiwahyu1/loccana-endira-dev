<style type="text/css">
	.tombol-kembali{
		margin-top: -5px;
	}
	.text-kembali{
		margin-top: -5px;
		margin-left: 50px;
	}
	.label-header{
		color: #505458;
		font-size: 20px;
		font-weight: bold;
	}
</style>

<div class="card-box" style="height:60px;">
	<div class="row">
		<div class="btn-group pull-left tombol-kembali">
			<a class="btn btn-info btn-sm" id="back_bc" name="back_bc" data-toggle="tooltip" data-placement="top" title="Back">
				<i class="fa fa-arrow-left"></i>
			</a> 
		</div> 
		<h3 class="text-kembali"><label> Kembali </label></h3>
	</div>
</div>

<!-- PEMBERITAHUAN IMPOR -->
<div class="card-box" style="margin-bottom:30px;margin-top:20px;">
	<div class="row">
		<div class="col-md-12">
			<div class="text-center">
				<label class="label-header">PEMBERITAHUAN BARANG IMPOR DARI TEMPAT PENIMBUNAN BERIKAT</label>
			</div>
			<div class="pull-right">
				<label class=""><?php if(isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']); ?></label>
			</div>				
		</div>
	</div>
	
	<div class="row margin-top: 20px;">
		<div class="col-md-12">
			<table class="table table-bordered" cellspacing="0" width="100%">
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">Kantor Pabean</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="3" style="border-bottom-style: hidden;">KPPBC BANDUNG</td>
					<td class="text-center" colspan="1" style="">050500</td>
					<td class="text-right" colspan="4" style="border-bottom-style: hidden;">halaman ke-1 dari 5</td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">Nomor Pengajuan</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;"><?php if(isset($no_pengajuan)) echo strtoupper($no_pengajuan); ?></td>
					<td colspan="5" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="1" style="border-bottom-style: hidden;;border-right-style: hidden;">A.</td>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">Jenis TPB</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="1" style="">1</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">1. Kawasan Berikat 2. Gudang Berikat 3. TPPB 4. TBB 5. TLB 6. KDUB 7. Lainnya</td>
					<td colspan="6" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="12" style=""></td>
				</tr>
				<tr>
					<td colspan="6" style="border-bottom-style: hidden;"><label>B. DATA PEMBERITAHUAN</label></td>
					<td colspan="6" rowspan="2" style="border-bottom-style: hidden;"><label>D. DIISI OLEH BEA DAN CUKAI</label></td>
				</tr>
				<tr>
					<td colspan="6" style="border-bottom-style: hidden;"><label>PENYELENGGARA/ PENGUSAHA TPB</label></td>
				</tr>
				<tr>
					<td colspan="4" style="border-bottom-style: hidden;border-right-style: hidden;">1. NPWP</td>
					<td colspan="2" style="border-bottom-style: hidden;">:</td>
					<td colspan="4" style="border-bottom-style: hidden;border-right-style: hidden;">No Pendaftaran</td>
					<td colspan="2" style="border-bottom-style: hidden;">: <?php if(isset($bc[0]['no_pendaftaran'])) echo strtoupper($bc[0]['no_pendaftaran']); ?> </td>
				</tr>
				<tr>
					<td colspan="4" style="border-bottom-style: hidden;border-right-style: hidden;">2. Nama</td>
					<td colspan="2" style="border-bottom-style: hidden;">:</td>
					<td colspan="4" style="border-bottom-style: hidden;border-right-style: hidden;">Tanggal</td>
					<td colspan="2" style="border-bottom-style: hidden;">: <?php if(isset($bc[0]['tanggal_pengajuan'])) echo strtoupper($bc[0]['tanggal_pengajuan']); ?> </td>
				</tr>
				<tr>
					<td colspan="4" rowspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">3. Alamat</td>
					<td colspan="2" style="border-bottom-style: hidden;">:</td>
					<td colspan="6" rowspan="2" style=""></td>
				</tr>
				<tr>
					<td colspan="2" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="4" style="border-bottom-style: hidden;border-right-style: hidden;">4. No Izin TPB</td>
					<td colspan="2" style="border-bottom-style: hidden;">:</td>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">14. Invoice</td>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="2" style="border-bottom-style: hidden;">Tgl</td>
				</tr>
				<tr>
					<td colspan="4" style="border-right-style: hidden;">5. API</td>
					<td colspan="2" style="">:</td>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">15. Packing List</td>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="2" style="border-bottom-style: hidden;">Tgl</td>
				</tr>
				<tr>
					<td colspan="6" style="border-bottom-style: hidden;"><h5><label>PEMILIK BARANG</label></h5></td>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">16. Kontrak</td>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="2" style="">Tgl</td>
				</tr>
				<tr>
					<td colspan="4" style="border-bottom-style: hidden;border-right-style: hidden;">6. NPWP</td>
					<td colspan="2" style="border-bottom-style: hidden;">:</td>
					<td colspan="2" rowspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">17. Fasilitas Impor</td>
					<td colspan="2" style="border-bottom-style: hidden;">:</td>
					<td colspan="2" style=""></td>
				</tr>
				<tr>
					<td colspan="4" style="border-bottom-style: hidden;border-right-style: hidden;">7. Nama</td>
					<td colspan="2" style="border-bottom-style: hidden;">: CELEBIT CIRCUIT TECHNOLOGY INDONESIA</td>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;"></td>
					<td colspan="2" style="border-bottom-style: hidden;">Tgl</td>
				</tr>
				<tr>
					<td colspan="4" rowspan="3" style="border-right-style: hidden;">8. Alamat</td>
					<td colspan="2" rowspan="3" style="">JALAN BUAH DUA, RT. 01, RW. 04, DESA RANCAEKEK, KECAMATAN RANCAEKEK, BANDUNG, JAWA BARAT</td>
				</tr>
				<tr>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">18. Surat Keputusan</td>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;"></td>
					<td colspan="2" style="border-bottom-style: hidden;">Tgl</td>
				</tr>
				<tr>
					<td colspan="6" style="">/Dokuman Lainnya</td>
				</tr>
				
				<tr>
					<td colspan="6" style="border-bottom-style: hidden;"><label>PENERIMAAN BARANG</label></td>
					<td colspan="2" style="">19. Valuta</td>
					<td colspan="1" style="">USD</td>
					<td colspan="3" style="">20. NDPBM</td>
				</tr>
				<tr>
					<td colspan="4" style="border-bottom-style: hidden;border-right-style: hidden;">9. NPWP</td>
					<td colspan="2" style="border-bottom-style: hidden;">:</td>
					<td colspan="1" rowspan="2" style="">USD DOLLAR</td>
					<td colspan="2" rowspan="2" style="">USD DOLLAR</td>
					<td class="text-right" colspan="3" rowspan="2" style="">14.052,0000</td>
				</tr>
				<tr>
					<td colspan="4" style="border-bottom-style: hidden;border-right-style: hidden;">10. Nama</td>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">: CELEBIT CIRCUIT TECHNOLOGY INDONESIA</td>
				</tr>
				<tr>
					<td colspan="4" rowspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">11. Alamat</td>
					<td colspan="2" style="border-bottom-style: hidden;">: THE MANNOR OFFICE LT.7 UNIT NOMOR LT7 E SURYA CIPTA</td>
					<td colspan="4" style="border-bottom-style: hidden;border-right-style: hidden;">21. Nilai CIF</td>
					<td colspan="2" style="border-bottom-style: hidden;">: 423,64</td>
				</tr>
				<tr>
					<td colspan="2" style="border-bottom-style: hidden;">SQUARE KARAWANG JAWA BARAT</td>
					<td colspan="4" style="border-right-style: hidden;">22. Harga Penyerahan</td>
					<td colspan="2" style="">: Rp. 8.655.600,00</td>
				</tr>
				<tr>
					<td colspan="4" style="border-bottom-style: hidden;border-right-style: hidden;">12. NIPER</td>
					<td colspan="2" style="border-bottom-style: hidden;">:</td>
					<td colspan="4" style="border-right-style: hidden;">23. Jenis Sarana Pengangkut</td>
					<td colspan="2" style="border-bottom-style: hidden;">:</td>
				</tr>
				<tr>
					<td colspan="4" style="border-right-style: hidden;">13. API</td>
					<td colspan="2" style="">:</td>
					<td colspan="6" style="">DARAT</td>
				</tr>
				<tr>
					<td colspan="4" style="border-bottom-style: hidden;border-right-style: hidden;">24. Nomor, Ukuran, Tipe Peti Kemas</td>
					<td colspan="3" style="border-bottom-style: hidden;">25. Jumlah, Jenis, Merek, Kemasan</td>
					<td colspan="2" style="">1</td>
					<td colspan="3" style="">26. Berat Kotor (Kg)  : 88,7400</td>
				</tr>
				<tr>
					<td colspan="4" style="border-right-style: hidden;"></td>
					<td colspan="3" style="border-right-style: hidden;">6. Carton, CELEBIT</td>
					<td colspan="2" style=""></td>
					<td colspan="3" style="">27. Berat Bersih (Kg) : 86,6400</td>
				</tr>
				<tr>
					<td colspan="1" style="border-bottom-style: hidden;">28</td>
					<td colspan="3" style="border-bottom-style: hidden;">29. - Pos Tarid/HS</td>
					<td colspan="2" style="border-bottom-style: hidden;">30. - Kategori Barang</td>
					<td colspan="2" style="border-bottom-style: hidden;">31. - Tarif dan Fasilitas</td>
					<td colspan="2" style="border-bottom-style: hidden;">32. - Jumlah dan Jenis Satuan</td>
					<td colspan="2" style="border-bottom-style: hidden;">33. - Nilai CIF</td>
				</tr>
				<tr>
					<td colspan="1" style="border-bottom-style: hidden;">No</td>
					<td colspan="3" style="border-bottom-style: hidden;">- Kode Barang</td>
					<td colspan="2" style="border-bottom-style: hidden;">- Kondisi Barang</td>
					<td colspan="2" style="border-bottom-style: hidden;">- BM -BMT</td>
					<td colspan="2" style="border-bottom-style: hidden;">- Berat Bersih (Kg)</td>
					<td colspan="2" style="border-bottom-style: hidden;">- Harga Penyerahan</td>
				</tr>
				<tr>
					<td colspan="1" style="border-bottom-style: hidden;"></td>
					<td colspan="3" style="border-bottom-style: hidden;">- Uraian barang secara lengkap, merk, tipe, ukuran, spesifikasi lain</td>
					<td colspan="2" style="border-bottom-style: hidden;"></td>
					<td colspan="2" style="border-bottom-style: hidden;">- Cukai</td>
					<td colspan="2" style="border-bottom-style: hidden;">- Jumlah dan Jenis Kemasan</td>
					<td colspan="2" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="1" style="border-bottom-style: hidden;"></td>
					<td colspan="3" style="border-bottom-style: hidden;">- Fasilitas Impor</td>
					<td colspan="2" style="border-bottom-style: hidden;"></td>
					<td colspan="2" style="border-bottom-style: hidden;">- PPn</td>
					<td colspan="2" style="border-bottom-style: hidden;">- Jumlah dan Jenis Kemasan</td>
					<td colspan="2" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="1" style=""></td>
					<td colspan="3" style="">- Surat Keputusan/ Dokumen Lainnya</td>
					<td colspan="2" style=""></td>
					<td colspan="2" style="">- PPh</td>
					<td colspan="2" style=""></td>
					<td colspan="2" style=""></td>
				</tr>
				<tr>
					<td class="text-center" colspan="12" style="height:100px;vertical-align:middle">................................................... 2. Jenis Barang Lihat Lembar Lanjutan ...................................................</td>
				</tr>
				<tr>
					<td class="text-center" colspan="4" style="">
						Jenis Pungutan
					</td>
					<td class="text-center" colspan="2" style="">
						Di Bayar<br>
						(Rp)
					</td>
					<td class="text-center" colspan="2" style="">
						Di Bebaskan<br>
						(Rp)
					</td>
					<td class="text-center" colspan="2" style="">
						Ditanggung Pemerintah<br>
						(Rp)
					</td>
					<td class="text-center"colspan="2" style="">
						Sudah Dilunasi<br>
						(Rp)
					</td>
				</tr>
				<tr>
					<td colspan="1" style="">34</td>
					<td colspan="3" style="">BM</td>
					<td class="text-right" colspan="2" style="">0</td>
					<td class="text-right" colspan="2" style="">0</td>
					<td class="text-right" colspan="2" style="">0</td>
					<td class="text-right" colspan="2" style="">0</td>
				</tr>
				<tr>
					<td colspan="1" style="">35</td>
					<td colspan="3" style="">BMT</td>
					<td class="text-right" colspan="2" style="">0</td>
					<td class="text-right" colspan="2" style="">0</td>
					<td class="text-right" colspan="2" style="">0</td>
					<td class="text-right" colspan="2" style="">0</td>
				</tr>
				<tr>
					<td colspan="1" style="">36</td>
					<td colspan="3" style="">Cukai</td>
					<td class="text-right" colspan="2" style="">0</td>
					<td class="text-right" colspan="2" style="">0</td>
					<td class="text-right" colspan="2" style="">0</td>
					<td class="text-right" colspan="2" style="">0</td>
				</tr>
				<tr>
					<td colspan="1" style="">37</td>
					<td colspan="3" style="">PPN</td>
					<td class="text-right" colspan="2" style="">596.000</td>
					<td class="text-right" colspan="2" style="">0</td>
					<td class="text-right" colspan="2" style="">0</td>
					<td class="text-right" colspan="2" style="">0</td>
				</tr>
				<tr>
					<td colspan="1" style="">38</td>
					<td colspan="3" style="">PPnBM</td>
					<td class="text-right" colspan="2" style="">0</td>
					<td class="text-right" colspan="2" style="">0</td>
					<td class="text-right" colspan="2" style="">0</td>
					<td class="text-right" colspan="2" style="">0</td>
				</tr>
				<tr>
					<td colspan="1" style="">39</td>
					<td colspan="3" style="">PPh</td>
					<td class="text-right" colspan="2" style="">149.000</td>
					<td class="text-right" colspan="2" style="">0</td>
					<td class="text-right" colspan="2" style="">0</td>
					<td class="text-right" colspan="2" style="">0</td>
				</tr>
				<tr>
					<td colspan="1" style="">40</td>
					<td colspan="3" style="">TOTAL</td>
					<td class="text-right" colspan="2" style="">745.000</td>
					<td class="text-right" colspan="2" style="">0</td>
					<td class="text-right" colspan="2" style="">0</td>
					<td class="text-right" colspan="2" style="">0</td>
				</tr>
				<tr>
					<td colspan="6" style=""><label>C. PENGESAHAN PENGUSAHA TPB</label></td>
					<td colspan="6" style=""><label><u>E. UNTUK PEMBAYARAN</u></label></td>
				</tr>
				<tr>
					<td colspan="6" style="border-bottom-style: hidden;"></td>
					<td colspan="2" style="border-bottom-style: hidden;"></td>
					<td colspan="1" style="border-bottom-style: hidden;"></td>
					<td colspan="3" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="6" style="border-bottom-style: hidden;">Dengan ini saya menyatakan bertanggung jawab atas</td>
					<td colspan="2" style="border-bottom-style: hidden;">Pembayaran</td>
					<td colspan="1" style="">1</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">1. Bank</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden">2. POS</td>
					<td colspan="1" style="border-bottom-style: hidden;">3. Kantor Pabean</td>
				</tr>
				<tr>
					<td colspan="6" rowspan="1" style="border-bottom-style: hidden;">kebenaran hal-hal yang diberitahukan dalam pemberitahuan pabean ini</td>
					<td colspan="2" rowspan="1" style="border-bottom-style: hidden;">Wajib Bayar</td>
					<td colspan="1" rowspan="1" style="">1</td>
					<td colspan="2" rowspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">1. Pengusaha TPB</td>
					<td colspan="1" rowspan="1" style="border-bottom-style: hidden;">2. Penerima</td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">Tempat, Tanggal</td>
					<td colspan="3" style="border-bottom-style: hidden;">: BANDUNG, 29APRIL 2019</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;"></td>
					<td colspan="3" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">Nama Lengkap</td>
					<td colspan="3" style="border-bottom-style: hidden;">: LO JU JIE</td>
					<td class="text-center" colspan="6" style="">Tanggal :</td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">Jabatan</td>
					<td colspan="3" style="border-bottom-style: hidden;">: Direktur Utama</td>
					<td colspan="6" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="6" rowspan="" style="border-bottom-style: hidden;height:200px;">Tanda Tangan dan Stempel Perusahaan :</td>
					<td class="text-center" colspan="6" rowspan="" style="border-bottom-style: hidden;height:200px;">Nama/Stempel Instansi</td>
				</tr>
				<tr>
					<td class="text-center" colspan="6" style=""></td>
					<td class="text-center" colspan="6" style="">Nama/Stempel</td>
				</tr>
			</table>
			<div class="pull-left"><label>Rangkap ke-1/2/3: Pengusaha TPB / KPPBC Pengawas / Penerima Barang</label><div>
		</div>
	</div>
</div>

<!-- PEMBERITAHUAN IMPOR BARANG DARI TEMPAT PENIMBUNAN BERIKAT -->
<div class="card-box" style="margin-bottom:30px;margin-top:20px;">
	<div class="row">
		<div class="col-md-12">
			<div class="text-center">
				<label class="label-header">LEMBAR LANJUTAN DATA BARANG</label>
			</div>					
			<div class="text-center">
				<label class="label-header">PEMBERITAHUAN IMPOR BARANG DARI TEMPAT PENIMBUNAN BERIKAT</label>
			</div>
			<div class="pull-right">
				<label class=""><?php if(isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']); ?></label>
			</div>		
		</div>
		
		<div class="col-md-12">
			<table class="table table-bordered" cellspacing="0" width="100%">
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">Kantor Pabean</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="3" style="border-bottom-style: hidden;">KPPBC BANDUNG</td>
					<td class="text-center" colspan="1" style="">050500</td>
					<td class="text-right" colspan="4" style="border-bottom-style: hidden;">halaman ke-2 dari 5</td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">Nomor Pengajuan</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;"><?php if(isset($no_pengajuan)) echo strtoupper($no_pengajuan); ?></td>
					<td colspan="5" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="3" style="border-right-style: hidden;">Nomor Pendaftaran</td>
					<td colspan="1" style="border-right-style: hidden;">:</td>
					<td colspan="3" style="border-right-style: hidden"><?php if(isset($bc[0]['no_pendaftaran'])) echo strtoupper($bc[0]['no_pendaftaran']); ?></td>
					<td colspan="3" style="border-right-style: hidden">Tanggal Pendaftaran</td>
					<td colspan="2" style="">: <?php if(isset($bc[0]['tanggal_pengajuan'])) $date = date_create($bc[0]['tanggal_pengajuan']); echo date_format($date, 'd-m-Y'); ?></td>
				</tr>
				<tr>
					<td colspan="1" rowspan="" style="border-bottom-style: hidden;">29.</td>
					<td colspan="3" rowspan="" style="border-bottom-style: hidden;">30.</td>
					<td colspan="2" rowspan="" style="border-bottom-style: hidden;">31.</td>
					<td colspan="2" rowspan="" style="border-bottom-style: hidden;">32.</td>
					<td colspan="2" rowspan="" style="border-bottom-style: hidden;">33.</td>
					<td colspan="2" rowspan="" style="border-bottom-style: hidden;">34.</td>
				</tr>
				<tr>
					<td colspan="1" rowspan="7" style="">No</td>
					<td colspan="3" rowspan="" style="border-bottom-style: hidden;">- Pos Tarif/HS</td>
					<td colspan="2" rowspan="" style="border-bottom-style: hidden;">- Kategori Barang</td>
					<td colspan="2" rowspan="" style="border-bottom-style: hidden;">- Tarif dan Fasilitass</td>
					<td colspan="2" rowspan="" style="border-bottom-style: hidden;">- Jumlah dan Jenis Satuan</td>
					<td colspan="2" rowspan="" style="border-bottom-style: hidden;">- Nilai CIF</td>
				</tr>
				<tr>
					<td colspan="3" rowspan="" style="border-bottom-style: hidden;">- Kode Barang</td>
					<td colspan="2" rowspan="6" style="">- Kondisi Barang</td>
					<td colspan="2" rowspan="" style="border-bottom-style: hidden;">- BM</td>
					<td colspan="2" rowspan="" style="border-bottom-style: hidden;">- Berat Bersih (Kg)</td>
					<td colspan="2" rowspan="6" style="">- Harga Penyerahan</td>
				</tr>
				<tr>
					<td colspan="3" rowspan="" style="border-bottom-style: hidden;">- Uraian barang secara lengkap, merk, tipe, ukuran, spesifikasi lain</td>
					<td colspan="2" rowspan="" style="border-bottom-style: hidden;">- BMT</td>
					<td colspan="2" rowspan="5" style="">- Jumlah dan Jenis Kemasan</td>
				</tr>
				<tr>
					<td colspan="3" rowspan="" style="border-bottom-style: hidden;">- Fasilitas Impor</td>
					<td colspan="2" rowspan="" style="border-bottom-style: hidden;">- Cukai</td>
				</tr>
				<tr>
					<td colspan="3" rowspan="3" style="">- Surat Keputusan/Dokumen Lainnya</td>
					<td colspan="2" rowspan="" style="border-bottom-style: hidden;">- PPN</td>
				</tr>
				<tr>
					<td colspan="2" rowspan="" style="border-bottom-style: hidden;">- PPnBM</td>
				</tr>
				<tr>
					<td colspan="2" rowspan="" style="">- PPh</td>
				</tr>
				<tr>
					<td colspan="1" rowspan="" style="">1</td>
					<td colspan="3" rowspan="" style="">
						Pos Tarif/HS : 85340010<br>
						Kode Brg : 012NSS-PSS-P-06<br>
						PRINTED CIRCUIT BOARD, Merk : CELEBIT, Tipe : DM.500-01 (B251547-101), Ukuran: - <br>
						Lain-lain : -<br>
						Fasilitas : -<br>
						Dokumen :
					</td>
					<td colspan="2" rowspan="" style="">
						Kategori : 1<br>
						Hasil Produksi<br><br>
						Kondisi : 1<br>
						TIDAK RUSAK
					</td>
					<td colspan="2" rowspan="" style="">
						<div class="pull-left" style="font-size: 10px;">BM</div><div class="pull-right" style="font-size: 10px;">0,00% 100,00% BY</div><br>
						<div class="pull-left" style="font-size: 10px;">PPH</div><div class="pull-right" style="font-size: 10px;">2,50% 100,00% BY</div><br>
						<div class="pull-left" style="font-size: 10px;">PPN</div><div class="pull-right" style="font-size: 10px;">10,00% 100,00% BY</div><br>
						<div class="pull-left" style="font-size: 10px;">PPNBM</div><div class="pull-right" style="font-size: 10px;">0,00% 100,00% BY</div><br>
					</td>
					<td colspan="2" rowspan="" style="">
						<div class="pull-left" style="">Satuan :</div><div class="pull-right" style="">2.000,0000</div><br>
						<div class="pull-right" style="">PCE (PIECE)</div><br>
						<div class="pull-left" style="">Berat Bersih :</div><div class="pull-right" style="">77,6400</div><br>
						<div class="pull-left" style="">Kemasan :</div><div class="pull-right" style="">77,6400</div><br>
						<div class="pull-right" style="">CT (Carton)</div>
					</td>
					<td colspan="2" rowspan="" style="">
						<div class="pull-left" style="">CIF :</div><div class="pull-right" style="">380,27</div><br>
						<div class="pull-left" style="">Harga Penyerahan :</div><div class="pull-right" style="">Rp. 7.738.000,00</div>
					</td>
				</tr>
				<tr>
					<td colspan="1" rowspan="" style="">2</td>
					<td colspan="3" rowspan="" style="">
						Pos Tarif/HS : 85340010<br>
						Kode Brg : 012NSS-PSS-P-06<br>
						PRINTED CIRCUIT BOARD, Merk : CELEBIT, Tipe : DM.500-01 (B251547-101), Ukuran: - <br>
						Lain-lain : -<br>
						Fasilitas : -<br>
						Dokumen :
					</td>
					<td colspan="2" rowspan="" style="border-bottom-style: hidden;">
						Kategori : 1<br>
						Hasil Produksi<br><br>
						Kondisi : 1<br>
						TIDAK RUSAK
					</td>
					<td colspan="2" rowspan="" style="">
						<div class="pull-left" style="font-size: 10px;">BM</div><div class="pull-right" style="font-size: 10px;">0,00% 100,00% BY</div><br>
						<div class="pull-left" style="font-size: 10px;">PPH</div><div class="pull-right" style="font-size: 10px;">2,50% 100,00% BY</div><br>
						<div class="pull-left" style="font-size: 10px;">PPN</div><div class="pull-right" style="font-size: 10px;">10,00% 100,00% BY</div><br>
						<div class="pull-left" style="font-size: 10px;">PPNBM</div><div class="pull-right" style="font-size: 10px;">0,00% 100,00% BY</div><br>
					</td>
					<td colspan="2" rowspan="" style="">
						<div class="pull-left" style="">Satuan :</div><div class="pull-right" style="">2.000,0000</div><br>
						<div class="pull-right" style="">PCE (PIECE)</div><br>
						<div class="pull-left" style="">Berat Bersih :</div><div class="pull-right" style="">77,6400</div><br>
						<div class="pull-left" style="">Kemasan :</div><div class="pull-right" style="">77,6400</div><br>
						<div class="pull-right" style="">CT (Carton)</div>
					</td>
					<td colspan="2" rowspan="" style="">
						<div class="pull-left" style="">CIF :</div><div class="pull-right" style="">380,27</div><br>
						<div class="pull-left" style="">Harga Penyerahan :</div><div class="pull-right" style="">Rp. 7.738.000,00</div>
					</td>
				</tr>
				<tr>
					<td colspan="12" style="border-bottom-style: hidden"><label>C. PENGESAHAN PENGUSAHA TPB</label></td>
				</tr>
				<tr>
					<td colspan="12" style="border-bottom-style: hidden;">
						Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberitahukan dalam pemberitahuan pabean ini dan keabsahan dokumen
					</td>
				</tr>
				<tr>
					<td colspan="12" style="border-bottom-style: hidden;">Pelengkap pabean yang menjadi dasar pembuatan dokumen ini.</td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden">Tempat, Tanggal</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden">:</td>
					<td colspan="8" style="border-bottom-style: hidden;"><?php echo date('d F Y'); ?></td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden">Nama Lengkap</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="8" style="border-bottom-style: hidden">Nama Pengusaha TPB
					</td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden">Jabatan</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="8" style="border-bottom-style: hidden;">Direktur Utama</td>
				</tr>
				<tr>
					<td colspan="12" style="height: 200px;">Tanda Tangan dan Stempel Perusahaan :</td>
				</tr>
			</table>
			<div class="pull-left"><label>Rangkap ke-1/2/3: Pengusaha TPB / KPPBC Pengawas / Penerima Barang</label></div>
		</div>
	</div>
</div>

<!-- PEMBERITAHUAN IMPOR BARANG -->
<div class="card-box" style="margin-bottom:30px;margin-top:20px;">
	<div class="row">
		<div class="col-md-12">
			<div class="text-center">
				<label class="label-header">LEMBAR LANJUTAN DOKUMEN PELENGKAP PABEAN</label>
			</div>					
			<div class="text-center">
				<label class="label-header">PEMBERITAHUAN IMPOR BARANG DARI TEMPAT PENIMBUNAN BERIKAT</label>
			</div>
			<div class="pull-right">
				<label class=""><?php if(isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']); ?></label>
			</div>		
		</div>
		
		<div class="col-md-12">
			<table class="table table-bordered" cellspacing="0" width="100%">
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">Kantor Pabean</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="3" style="border-bottom-style: hidden;">KPPBC BANDUNG</td>
					<td class="text-center" colspan="1" style="">050500</td>
					<td class="text-right" colspan="4" style="border-bottom-style: hidden;">halaman ke-3 dari 5</td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">Nomor Pengajuan</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;"><?php if(isset($no_pengajuan)) echo strtoupper($no_pengajuan); ?></td>
					<td colspan="5" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="3" style="border-right-style: hidden;">Nomor Pendaftaran</td>
					<td colspan="1" style="border-right-style: hidden;">:</td>
					<td colspan="3" style="border-right-style: hidden"><?php if(isset($bc[0]['no_pendaftaran'])) echo strtoupper($bc[0]['no_pendaftaran']); ?></td>
					<td colspan="3" style="border-right-style: hidden">Tanggal Pendaftaran</td>
					<td colspan="2" style="">: <?php if(isset($bc[0]['tanggal_pengajuan'])) $date = date_create($bc[0]['tanggal_pengajuan']); echo date_format($date, 'd-m-Y'); ?></td>
				</tr>
				<tr>
					<td class="text-center" colspan="2" style="">No</td>
					<td class="text-center" colspan="3" style="">Jenis Dokumen</td>
					<td class="text-center" colspan="3" style="">Nomor Dokumen</td>
					<td class="text-center" colspan="3" style="">Tanggal Dokumen</td>
				</tr>
				<tr>
					<td class="text-center" colspan="2" style="border-bottom-style: hidden;">1</td>
					<td colspan="3" style="border-bottom-style: hidden;">PACKING LIST</td>
					<td colspan="3" style="border-bottom-style: hidden;">BDG 190283</td>
					<td class="text-center" colspan="3" style="border-bottom-style: hidden;">29-04-2019</td>
				</tr>
				<tr>
					<td class="text-center" colspan="2" style="">2</td>
					<td colspan="3" style="">INVOICE</td>
					<td colspan="3" style="">BDG 190283</td>
					<td class="text-center" colspan="3" style="">29-04-2019</td>
				</tr>
				<tr>
					<td colspan="12" style="border-bottom-style: hidden"><label>C. PENGESAHAN PENGUSAHA TPB</label></td>
				</tr>
				<tr>
					<td colspan="12" style="border-bottom-style: hidden">
						Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberitahukan dalam pemberitahuan pabean ini dan keabsahan dokumen
					</td>
				</tr>
				<tr>
					<td colspan="12" style="border-bottom-style: hidden">Pelengkap pabean yang menjadi dasar pembuatan dokumen ini.</td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden">Tempat, Tanggal</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="8" style="border-bottom-style: hidden;"><?php echo date('d F Y'); ?></td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden">Nama Lengkap</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="8" style="border-bottom-style: hidden">Nama Pengusaha TPB
					</td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden">Jabatan</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="8" style="border-bottom-style: hidden;">Direktur Utama</td>
				</tr>
				<tr>
					<td colspan="12" style="height: 200px;">
						Tanda Tangan dan Stempel Perusahaan :
					</td>
				</tr>
			</table>
			<div class="pull-left"><label>Rangkap ke-1/2/3: Pengusaha TPB / KPPBC Pengawas / Penerima Barang</label></div>
		</div>
	</div>
</div>

<!-- DOKUMENT PELENGKAP PABEAN -->
<div class="card-box" style="margin-bottom:30px;margin-top:20px;">
	<div class="row">
		<div class="col-md-12">
			<div class="text-center">
				<label class="label-header">LEMBAR LAMPIRAN</label>
			</div>					
			<div class="text-center">
				<label class="label-header">DATA PENGGUNAAN DAN/ATAU BAHAN IMPOR</label>
			</div>
			<div class="text-center">
				<label class="label-header">PEMBERITAHUAN IMPOR BARANG DARI TEMPAT PENIMBUNAN BERIKAT</label>
			</div>
			<div class="pull-right">
				<label class=""><?php if(isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']); ?></label>
			</div>		
		</div>
		
		<div class="col-md-12">
			<table class="table table-bordered" cellspacing="0" width="100%">
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">Kantor Pabean</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="3" style="border-bottom-style: hidden;">KPPBC BANDUNG</td>
					<td class="text-center" colspan="1" style="">050500</td>
					<td class="text-right" colspan="4" style="border-bottom-style: hidden;">halaman ke-4 dari 5</td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">Nomor Pengajuan</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;"><?php if(isset($no_pengajuan)) echo strtoupper($no_pengajuan); ?></td>
					<td colspan="5" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="3" style="border-right-style: hidden;">Nomor Pendaftaran</td>
					<td colspan="1" style="border-right-style: hidden;">:</td>
					<td colspan="3" style="border-right-style: hidden"><?php if(isset($bc[0]['no_pendaftaran'])) echo strtoupper($bc[0]['no_pendaftaran']); ?></td>
					<td colspan="3" style="border-right-style: hidden">Tanggal Pendaftaran</td>
					<td colspan="2" style="">: <?php if(isset($bc[0]['tanggal_pengajuan'])) $date = date_create($bc[0]['tanggal_pengajuan']); echo date_format($date, 'd-m-Y'); ?></td>
				</tr>
				<tr>
					<td class="text-center" colspan="1" style="">
						No<br>
						Urut<br>
						Barang
					</td>
					<td class="text-center" colspan="2" style="">
						- Kode Barang<br>
						- No/Tgl Daftar BC 2.3,<br>
						2.7, Lainnya *)
					</td>
					<td class="text-center" colspan="1" style="">
						Nomor Urut Dalam<br>
						- BC 2.3<br>
						- BC 2.7<br>
						- Lainnya *)
					</td>
					<td class="text-center" colspan="3" style="">
						Pos Tarif/HS, uraian jumlah<br>
						dan jenis barang secara<br>
						lengkap, kode barang merk,<br>
						tipe, ukuran dan spesifikasi lain<br><br>
						- Perijinan/Fasilitas
					</td>
					<td class="text-center" colspan="2" style="">
						- Jumlah<br>
						- Satuan
					</td>
					<td class="text-center" colspan="1" style="">
						Nilai<br>
						- CIF<br>
						- Harga Penyerahan (Rp)
					</td>
					<td class="text-center" colspan="1" style="">
						Nilai (Rp)<br>
						BM, BMT, Cukai<br>
						PPn, PPnBM,<br>
						PPh 22
					</td>
				</tr>
				<tr>
					<td class="text-center" colspan="1" style="">(1)</td>
					<td class="text-center" colspan="2" style="">(2)</td>
					<td class="text-center" colspan="1" style="">(3)</td>
					<td class="text-center" colspan="3" style="">(4)</td>
					<td class="text-center" colspan="2" style="">(5)</td>
					<td class="text-center" colspan="1" style="">(6)</td>
					<td class="text-center" colspan="1" style="">(7)</td>
				</tr>
				<tr>
					<td class="text-center" colspan="1" style="">1.1</td>
					<td colspan="2" style="">
						Kantor : &nbsp; 050500<br>
						KPPBC BANDUNG<br><br>
						Dok Asal BC 2.3<br>
						No : &nbsp; 040609<br>
						Tgl. &nbsp; 11-10-2018
					</td>
					<td class="text-center" colspan="1" style="">Seri barang ke-1</td>
					<td class="text-center" colspan="3" style="">
						Pos Tarif/HS : &nbsp; 74102110<br>
						Kode Brg : &nbsp; CCLF17<br>
						COPPER CLAD LAMINATE,<br>
						Merk : &nbsp; ETLXPC, Tipe : &nbsp; 204N1 /0,<br>
						Ukuran : &nbsp; 1030 x 1230,<br>
						Lain-lain: <br>
						Dokumen
					</td>
					<td class="text-center" colspan="2" style="">
						- 29,3000<br>
						- ST
					</td>
					<td class="text-center" colspan="1" style="">
						CIF :<br>
						379,73<br><br>
						Harga Penyerahan : 0,00
					</td>
					<td class="text-center" colspan="1" style="">
						<div class="pull-left" style="font-size: 10px;">BM</div><div class="pull-right" style="font-size: 10px;">0,00</div><br>
						<div class="pull-left" style="font-size: 10px;">BMT</div><div class="pull-right" style="font-size: 10px;">0,00</div><br>
						<div class="pull-left" style="font-size: 10px;">Cukai</div><div class="pull-right" style="font-size: 10px;">0,00</div><br>
						<div class="pull-left" style="font-size: 10px;">PPn</div><div class="pull-right" style="font-size: 10px;">533.596,60</div><br>
						<div class="pull-left" style="font-size: 10px;">PPnBM</div><div class="pull-right" style="font-size: 10px;">0,00</div><br>
						<div class="pull-left" style="font-size: 10px;">PPh</div><div class="pull-right" style="font-size: 10px;">133.399,15</div>
					</td>
				</tr>
				<tr>
					<td class="text-center" colspan="1" style="">1.2</td>
					<td colspan="2" style="">
						Kantor : &nbsp; 050500<br>
						KPPBC BANDUNG<br><br>
						Dok Asal BC 2.3<br>
						No : &nbsp; 040609<br>
						Tgl. &nbsp; 11-10-2018
					</td>
					<td class="text-center" colspan="1" style="">Seri barang ke-1</td>
					<td class="text-center" colspan="3" style="">
						Pos Tarif/HS : &nbsp; 74102110<br>
						Kode Brg : &nbsp; CCLF17<br>
						COPPER CLAD LAMINATE,<br>
						Merk : &nbsp; ETLXPC, Tipe : &nbsp; 204N1 /0,<br>
						Ukuran : &nbsp; 1030 x 1230,<br>
						Lain-lain: <br>
						Dokumen
					</td>
					<td class="text-center" colspan="2" style="">
						- 29,3000<br>
						- ST
					</td>
					<td class="text-center" colspan="1" style="">
						CIF :<br>
						379,73<br><br>
						Harga Penyerahan : 0,00
					</td>
					<td class="text-center" colspan="1" style="">
						<div class="pull-left" style="font-size: 10px;">BM</div><div class="pull-right" style="font-size: 10px;">0,00</div><br>
						<div class="pull-left" style="font-size: 10px;">BMT</div><div class="pull-right" style="font-size: 10px;">0,00</div><br>
						<div class="pull-left" style="font-size: 10px;">Cukai</div><div class="pull-right" style="font-size: 10px;">0,00</div><br>
						<div class="pull-left" style="font-size: 10px;">PPn</div><div class="pull-right" style="font-size: 10px;">758,81</div><br>
						<div class="pull-left" style="font-size: 10px;">PPnBM</div><div class="pull-right" style="font-size: 10px;">0,00</div><br>
						<div class="pull-left" style="font-size: 10px;">PPh</div><div class="pull-right" style="font-size: 10px;">189,70</div>
					</td>
				</tr>
				<tr>
					<td class="text-center" colspan="1" style="">2.1</td>
					<td colspan="2" style="">
						Kantor : &nbsp; 050500<br>
						KPPBC BANDUNG<br><br>
						Dok Asal BC 2.3<br>
						No : &nbsp; 040609<br>
						Tgl. &nbsp; 11-10-2018
					</td>
					<td class="text-center" colspan="1" style="">Seri barang ke-1</td>
					<td class="text-center" colspan="3" style="">
						Pos Tarif/HS : &nbsp; 74102110<br>
						Kode Brg : &nbsp; CCLF17<br>
						COPPER CLAD LAMINATE,<br>
						Merk : &nbsp; ETLXPC, Tipe : &nbsp; 204N1 /0,<br>
						Ukuran : &nbsp; 1030 x 1230,<br>
						Lain-lain: <br>
						Dokumen
					</td>
					<td class="text-center" colspan="2" style="">
						- 29,3000<br>
						- ST
					</td>
					<td class="text-center" colspan="1" style="">
						CIF :<br>
						379,73<br><br>
						Harga Penyerahan : 0,00
					</td>
					<td class="text-center" colspan="1" style="">
						<div class="pull-left" style="font-size: 10px;">BM</div><div class="pull-right" style="font-size: 10px;">0,00</div><br>
						<div class="pull-left" style="font-size: 10px;">BMT</div><div class="pull-right" style="font-size: 10px;">0,00</div><br>
						<div class="pull-left" style="font-size: 10px;">Cukai</div><div class="pull-right" style="font-size: 10px;">0,00</div><br>
						<div class="pull-left" style="font-size: 10px;">PPn</div><div class="pull-right" style="font-size: 10px;">60.859,21</div><br>
						<div class="pull-left" style="font-size: 10px;">PPnBM</div><div class="pull-right" style="font-size: 10px;">0,00</div><br>
						<div class="pull-left" style="font-size: 10px;">PPh</div><div class="pull-right" style="font-size: 10px;">15.214,80</div>
					</td>
				</tr>
				<tr>
					<td class="text-center" colspan="1" style="">2.2</td>
					<td colspan="2" style="">
						Kantor : &nbsp; 050500<br>
						KPPBC BANDUNG<br><br>
						Dok Asal BC 2.3<br>
						No : &nbsp; 040609<br>
						Tgl. &nbsp; 11-10-2018
					</td>
					<td class="text-center" colspan="1" style="">Seri barang ke-1</td>
					<td class="text-center" colspan="3" style="">
						Pos Tarif/HS : &nbsp; 74102110<br>
						Kode Brg : &nbsp; CCLF17<br>
						COPPER CLAD LAMINATE,<br>
						Merk : &nbsp; ETLXPC, Tipe : &nbsp; 204N1 /0,<br>
						Ukuran : &nbsp; 1030 x 1230,<br>
						Lain-lain: <br>
						Dokumen
					</td>
					<td class="text-center" colspan="2" style="">
						- 29,3000<br>
						- ST
					</td>
					<td class="text-center" colspan="1" style="">
						CIF :<br>
						379,73<br><br>
						Harga Penyerahan : 0,00
					</td>
					<td class="text-center" colspan="1" style="">
						<div class="pull-left" style="font-size: 10px;">BM</div><div class="pull-right" style="font-size: 10px;">0,00</div><br>
						<div class="pull-left" style="font-size: 10px;">BMT</div><div class="pull-right" style="font-size: 10px;">0,00</div><br>
						<div class="pull-left" style="font-size: 10px;">Cukai</div><div class="pull-right" style="font-size: 10px;">0,00</div><br>
						<div class="pull-left" style="font-size: 10px;">PPn</div><div class="pull-right" style="font-size: 10px;">84,31</div><br>
						<div class="pull-left" style="font-size: 10px;">PPnBM</div><div class="pull-right" style="font-size: 10px;">0,00</div><br>
						<div class="pull-left" style="font-size: 10px;">PPh</div><div class="pull-right" style="font-size: 10px;">21,08</div>
					</td>
				</tr>
				<tr>
					<td colspan="12" style="border-bottom-style: hidden"><label>C. PENGESAHAN PENGUSAHA TPB</label></td>
				</tr>
				<tr>
					<td colspan="12" style="border-bottom-style: hidden">
						Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberitahukan dalam pemberitahuan pabean ini dan keabsahan dokumen
					</td>
				</tr>
				<tr>
					<td colspan="12" style="border-bottom-style: hidden">Pelengkap pabean yang menjadi dasar pembuatan dokumen ini.</td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden">Tempat, Tanggal</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="8" style="border-bottom-style: hidden;"><?php echo date('d F Y'); ?></td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden">Nama Lengkap</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="8" style="border-bottom-style: hidden">Nama Pengusaha TPB
					</td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden">Jabatan</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="8" style="border-bottom-style: hidden;">Direktur Utama</td>
				</tr>
				<tr>
					<td colspan="12" style="height: 200px;">
						Tanda Tangan dan Stempel Perusahaan :
					</td>
				</tr>
			</table>
			<div class="pull-left"><label>Rangkap ke-1/2/3: Pengusaha TPB / KPPBC Pengawas / Penerima Barang</label></div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var id_bc = "<?php if(isset($bc[0])){ echo $bc[0]['id']; } ?>";

	$(document).ready(function() {
		$('#back_bc').on('click', function() {
			$('#title_Menu').html('Bea Cukai');
			$('#div_list_bc').show();
			$('#div_detail_bc').hide();
			$('#div_detail_bc').html('');
		})
	});
</script>