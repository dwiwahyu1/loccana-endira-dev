<style type="text/css">
	.separator {
		display: flex;
		align-items: center;
		text-align: center;
	}
	
	.separator::before, .separator::after {
		content: '';
		flex: 1;
		border-bottom: 1px solid #000;
		border-style: dashed;
	}

	.separator::before {
		margin-right: .25em;
	}

	.separator::after {
		margin-left: .25em;
	}

	h4 { line-height: 20px !important; }
	.table {border-color: black !important}
	.table thead tr td, th {border-color: black !important}
	.table tbody tr td, th {border-color: black !important}
	.table tfoot tr td, th {border-color: black !important}
</style>

<!-- Start Lembar 1 -->
<div class="card-box" style="margin-top: 20px;">
	<div class="btn-group pull-left" style="padding-bottom: 20px;">
		<a class="btn btn-info btn-sm" id="back_bc" name="back_bc" data-toggle="tooltip" data-placement="top" title="Back">
			<i class="fa fa-arrow-left"></i>
		</a>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<tr>
					<th style="font-size: 30px; width: 15%; vertical-align: middle;" class="dt-body-center">
						<?php if (isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']); ?>
					</th>
					<th style="font-size: 20px;" class="dt-body-center">
						<?php if(isset($bc[0]['desc_bc'])) echo wordwrap(strtoupper($bc[0]['desc_bc']), 70, "<br>\n"); ?>
					</th>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<th colspan="2">HEADER</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="border: 0px; padding-left: 15px; width: 50%;" rowspan="4">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<thead>
									<tr>
										<td style="width: 30%;">Nomor Pengajuan</td>
										<td>: &nbsp; <?php if(isset($bc[0]['no_pengajuan'])) echo $bc[0]['no_pengajuan']; ?></td>
									</tr>
									<tr>
										<td style="width: 30%;">A. KANTOR PABEAN</td>
										<td>: &nbsp; <?php
											if(isset($header[0]['KPPBC'])) {
												$kppbc = $header[0]['KPPBC'];
												foreach ($ref_kppbc as $rkk => $rkv) {
													if($rkv['KODE_KANTOR'] == $kppbc) echo $rkv['URAIAN_KANTOR'];
												}
											}
										?></td>
									</tr>
									<tr>
										<td style="width: 30%;">B. JENIS TPB</td>
										<td>: &nbsp; KAWASAN BERIKAT</td>
									</tr>
									<tr>
										<td style="width: 30%;">C. TUJUAN PENGIRIMAN</td>
										<td>: &nbsp; LAINNYA</td>
									</tr>
								</thead>
							</table>
						</td>
						<th class="dt-body-right" style="border-left: 0px; width: 50%; padding-right: 60px;">Halaman Ke-1 dari 3</th>
					</tr>
					<tr>
						<td style="border-left: 1px solid;" rowspan="3">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<thead>
									<tr>
										<td colspan="2">F. KOLOM KHUSUS BEA DAN CUKAI</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="padding-left: 20px; width: 30%; border: 0px;">Nomor Pendaftaran</td>
										<td style="border: 0px;">: &nbsp; <?php if(isset($bc[0]['no_pendaftaran'])) echo $bc[0]['no_pendaftaran']; ?></td>
									</tr>
									<tr>
										<td style="padding-left: 20px; width: 30%; border: 0px;">Tanggal</td>
										<td style="border: 0px;">: &nbsp; <?php if(isset($bc[0]['tanggal_pengajuan'])) echo date('d F Y', strtotime($bc[0]['tanggal_pengajuan'])); ?></td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<th colspan="2">D. DATA PEMBERITAHUAN</th>
					</tr>
					<tr>
						<td class="dt-body-center">PENGUSAHA TPB</td>
						<td class="dt-body-center">PENERIMA BARANG</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="width: 50%;">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<tr>
									<td style="border: 0px; width: 20%;">1. NPWP</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['KODE_ID_PENGUSAHA'])) echo $header[0]['KODE_ID_PENGUSAHA'];
									?></td>
								</tr>
								<tr>
									<td style="border: 0px; width: 20%;">2. NAMA</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['PERUSAHAAN'])) echo $header[0]['PERUSAHAAN'];
									?></td>
								</tr>
								<tr>
									<td style="border: 0px; width: 20%;">3. ALAMAT</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['ALAMAT_PENGUSAHA'])) echo $header[0]['ALAMAT_PENGUSAHA'];
									?></td>
								</tr>
								<tr>
									<td style="border: 0px; width: 20%;">4. No Izin TPB</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['NOMOR_IJIN_TPB'])) echo $header[0]['NOMOR_IJIN_TPB'];
									?></td>
								</tr>
							</table>
						</td>
						<td style="width: 50%;">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<tr>
									<td style="border: 0px; width: 20%;">5. NPWP</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['ID_PENERIMA_BARANG'])) echo $header[0]['ID_PENERIMA_BARANG'];
									?></td>
								</tr>
								<tr>
									<td style="border: 0px; width: 20%;">6. NAMA</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['NAMA_PENERIMA_BARANG'])) echo $header[0]['NAMA_PENERIMA_BARANG'];
									?></td>
								</tr>
								<tr>
									<td style="border: 0px; width: 20%;">7. ALAMAT</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['ALAMAT_PENERIMA_BARANG'])) echo $header[0]['ALAMAT_PENERIMA_BARANG'];
									?></td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<th colspan="2">DOKUMEN PELENGKAP PABEAN</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="width: 50%;">
							<?php
								$strPackList 		= '';
								$strKontrak 		= '';
								$strFakturPajak		= '';
								$strKeputPer		= '';
								$strLainnya			= '';

								$strTglPackList		= '';
								$strTglKontrak		= '';
								$strTglFakturPajak	= '';
								$strTglKeputPer		= '';
								$strTglLainnya		= '';
								if(isset($dokumen) && (sizeof($dokumen) > 0)) {
									for ($i=0; $i < sizeof($dokumen); $i++) {
										foreach ($ref_dok as $rdk => $rdv) {
											if (strpos(strtoupper($rdv['URAIAN_DOKUMEN']), strtoupper('Packing List')) !== false) {
												if((int)$rdv['KODE_DOKUMEN'] == (int)$dokumen[$i]['KODE_JENIS_DOKUMEN']) {
													$strPackList 		= $dokumen[$i]['NOMOR_DOKUMEN'];
													$strTglPackList		= $dokumen[$i]['TANGGAL_DOKUMEN'];
												}
											}else if(strpos(strtoupper($rdv['URAIAN_DOKUMEN']), strtoupper('Kontrak')) !== false) {
												if((int)$rdv['KODE_DOKUMEN'] == (int)$dokumen[$i]['KODE_JENIS_DOKUMEN']) {
													$strKontrak 		= $dokumen[$i]['NOMOR_DOKUMEN'];
													$strTglKontrak		= $dokumen[$i]['TANGGAL_DOKUMEN'];
												}
											}else if(strpos(strtoupper($rdv['URAIAN_DOKUMEN']), strtoupper('Faktur Pajak')) !== false) {
												if((int)$rdv['KODE_DOKUMEN'] == (int)$dokumen[$i]['KODE_JENIS_DOKUMEN']) {
													$strFakturPajak 	= $dokumen[$i]['NOMOR_DOKUMEN'];
													$strTglFakturPajak	= $dokumen[$i]['TANGGAL_DOKUMEN'];
												}
											}else if((strpos(strtoupper($rdv['URAIAN_DOKUMEN']), strtoupper('Keputusan')) !== false) || (strpos(strtoupper($rdv['URAIAN_DOKUMEN']), strtoupper('Persetujuan')) !== false)) {
												if((int)$rdv['KODE_DOKUMEN'] == (int)$dokumen[$i]['KODE_JENIS_DOKUMEN']) {
													$strKeputPer 		= $dokumen[$i]['NOMOR_DOKUMEN'];
													$strTglKeputPer		= $dokumen[$i]['TANGGAL_DOKUMEN'];
												}
											}else if(strpos(strtoupper($rdv['URAIAN_DOKUMEN']), strtoupper('Lainnya')) !== false) {
												if((int)$rdv['KODE_DOKUMEN'] == (int)$dokumen[$i]['KODE_JENIS_DOKUMEN']) {
													$strLainnya 		= $dokumen[$i]['NOMOR_DOKUMEN'];
													$strTglLainnya		= $dokumen[$i]['TANGGAL_DOKUMEN'];
												}
											}
										}
									}
								}
							?>
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<tr>
									<td style="border: 0px; width: 30%;">8. Packing List</td>
									<td style="border: 0px; width: 35%;">: &nbsp; <?php echo $strPackList; ?></td>
									<td style="border: 0px;">tgl. <?php if($strTglPackList != '') echo date('d-m-Y', strtotime($strTglPackList)); ?></td>
								</tr>
								<tr>
									<td style="border: 0px; width: 30%;">9. Kontrak</td>
									<td style="border: 0px; width: 35%;">: &nbsp; <?php echo $strKontrak; ?></td>
									<td style="border: 0px;">tgl. <?php if($strTglKontrak != '') echo date('d-m-Y', strtotime($strTglKontrak)); ?></td>
								</tr>
								<tr>
									<td style="border: 0px; width: 30%;">10. Faktur Pajak</td>
									<td style="border: 0px; width: 35%;">: &nbsp; <?php echo $strFakturPajak; ?></td>
									<td style="border: 0px;">tgl. <?php if($strTglFakturPajak != '') echo date('d-m-Y', strtotime($strTglFakturPajak)); ?></td>
								</tr>
							</table>
						</td>
						<td style="width: 50%;">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<tr>
									<td style="border: 0px; width: 35%;">11. Surat Keputusan/Persetujuan</td>
									<td style="border: 0px; width: 40%;">: &nbsp; <?php echo $strKeputPer; ?></td>
									<td style="border: 0px;">tgl. <?php if($strTglKeputPer != '') echo date('d-m-Y', strtotime($strTglKeputPer)); ?></td>
								</tr>
								<tr>
									<td style="border: 0px; width: 35%;">12. Jenis / Nomor / Tanggal Dokumen Lainnya</td>
									<td style="border: 0px; width: 40%;">: &nbsp; <?php echo $strLainnya; ?></td>
									<td style="border: 0px;">tgl. <?php if($strTglLainnya != '') echo date('d-m-Y', strtotime($strTglLainnya)); ?></td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<th>RIWAYAT BARANG</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<tr>
									<td style="border: 0px; width: 20%;">13. Nomor dan tanggal BC 4.0 Asal</td>
									<td style="border: 0px; width: 40%;">: &nbsp; <?php
										if(isset($header[0]['NOMOR_DAFTAR'])) echo $header[0]['NOMOR_DAFTAR'];
									?></td>
									<td style="border: 0px;">Tgl. <?php
										if(isset($header[0]['TANGGAL_DAFTAR'])) echo date('d-m-Y', strtotime($header[0]['TANGGAL_DAFTAR']));
									?></td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<th colspan="2">DATA PENGANGKUTAN</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="width: 50%;">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<tr>
									<td style="border: 0px; width: 40%;">14. Jenis Sarana Pengangkut Darat</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['NAMA_PENGANGKUT'])) echo $header[0]['NAMA_PENGANGKUT'];
									?></td>
								</tr>
							</table>
						</td>
						<td style="width: 50%;">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<tr>
									<td style="border: 0px; width: 20%;">15. No Polisi</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['NOMOR_POLISI'])) echo $header[0]['NOMOR_POLISI'];
									?></td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<th>DATA PERDAGANGAN</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<tr>
									<td style="border: 0px; width: 15%;">16. Harga Penyerahan</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['HARGA_PENYERAHAN'])) echo number_format($header[0]['HARGA_PENYERAHAN'], 4, ',', '.');
									?></td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<th colspan="2">DATA PENGEMAS</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="width: 50%;">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<tr>
									<td style="border: 0px; width: 30%;">17. Jenis Kemasan</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($kemasan[0]['KODE_JENIS_KEMASAN'])) echo $kemasan[0]['KODE_JENIS_KEMASAN'];
									?></td>
								</tr>
								<tr>
									<td style="border: 0px; width: 30%;">18. Merek Kemasan</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($kemasan[0]['MEREK_KEMASAN'])) echo $kemasan[0]['MEREK_KEMASAN'];
									?></td>
								</tr>
							</table>
						</td>
						<td style="width: 50%;">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<tr>
									<td style="border: 0px; width: 30%;">19. Jumlah Kemasan</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($kemasan[0]['JUMLAH_KEMASAN'])) echo $kemasan[0]['JUMLAH_KEMASAN'];
									?></td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<th colspan="3">DATA BARANG</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="border:0px; width: 33%;">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<tr>
									<td style="border: 0px; width: 35%;">20. Volume (m3)</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['VOLUME'])) echo $header[0]['VOLUME'];
									?></td>
								</tr>
							</table>
						</td>
						<td style="border:0px; width: 33%;">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<tr>
									<td style="border: 0px; width: 40%;">21. Berat Kotor (Kg)</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['BRUTO'])) echo $header[0]['BRUTO'];
									?></td>
								</tr>
							</table>
						</td>
						<td style="border:0px; width: 33%;">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<tr>
									<td style="border: 0px; width: 40%;">22. Berat Bersih (Kg)</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['NETTO'])) echo $header[0]['NETTO'];
									?></td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<td style="width: 5%;">23. No</td>
						<td style="width: 33%;">24. Uraian jumlah dan jenis barang secara lengkap, kode barang, merk, tipe, ukuran, dan spesifikasi lain</td>
						<td style="width: 33%;">25. <ul style="margin-top: -20px;">
								<li>Jumlah & Jenis Satuan</li>
								<li>Berat Bersih (Kg)</li>
								<li>Volume (m3)</li>
						</ul></td>
						<td style="width: 33%;">26. <ul style="margin-top: -20px;">
							<li>Harga penyerahan (Rp)</li>
						</ul></td>
					</tr>
				</thead>
				<tbody>
		<?php
			if(isset($barang) && (sizeof($barang) > 0)) {
				for ($i = 0; $i < sizeof($barang); $i++) { ?>
					<tr>
						<td style="border-top: 0px; border-bottom: 0px;"><?php echo ($i+1); ?></td>
						<td style="border-top: 0px; border-bottom: 0px;"><ul>
							<li><?php echo $barang[$i]['URAIAN']; ?></li>
							<li>Kode Barang : <?php echo $barang[$i]['KODE_BARANG']; ?></li>
							<li>Merk : <?php echo $barang[$i]['MERK']; ?></li>
							<li>Tipe : <?php echo $barang[$i]['TIPE']; ?></li>
							<li>Ukuran : <?php echo $barang[$i]['UKURAN']; ?></li>
							<li>Spesifikasi Lain : <?php echo $barang[$i]['SPESIFIKASI_LAIN']; ?></li>
						</ul></td>
						<td style="border-top: 0px; border-bottom: 0px;"><ul>
							<li><?php
								echo number_format($barang[$i]['JUMLAH_SATUAN'], 4).' ';
								foreach ($ref_satuan as $rfk => $rfv) {
									if((int)$rfv['ID'] == (int)$barang[$i]['KODE_SATUAN']) echo $rfv['KODE_SATUAN'];
								}
							?></li>
							<li><?php echo number_format($barang[$i]['NETTO'], 4); ?></li>
							<li><?php echo number_format($barang[$i]['VOLUME'], 4); ?></li>
						</ul></td>
						<td style="border-top: 0px; border-bottom: 0px;"><?php echo number_format($barang[$i]['HARGA_PENYERAHAN'], 4, ',', '.'); ?></td>
					</tr>
		<?php
				}
			}else { ?>
				<tr>
					<td style="border-top: 0px; border-bottom: 0px;"></td>
					<td style="border-top: 0px; border-bottom: 0px;"></td>
					<td style="border-top: 0px; border-bottom: 0px;"></td>
					<td style="border-top: 0px; border-bottom: 0px;"></td>
				</tr>
		<?php
			}
		?>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
				<thead>
					<tr>
						<th class="dt-body-center" style="width: 50%;">
							G. UNTUK PEJABAT BEA DAN CUKAI
						</th>
						<th class="dt-body-center" style="width: 50%;">
							E. TANDA TANGAN PENGUSAHA TPB
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="width: 50%;">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<tr>
									<td style="border: 0px; height: 157px;" colspan="2"></td>
								</tr>
								<tr>
									<td style="border: 0px; width: 5%;">Nama</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['NAMA_TTD'])) echo strtoupper($header[0]['NAMA_TTD']);
									?></td>
								</tr>
								<tr>
									<td style="border: 0px; width: 5%;">NIP</td>
									<td style="border: 0px;">: &nbsp;</td>
								</tr>
							</table>
						</td>
						<td style="width: 50%;">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<tr>
									<td class="dt-body-center" style="border: 0px;">Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberikan dalam pemberitahuan pabean ini</td>
								</tr>
								<tr>
									<td class="dt-body-center" style="border: 0px;"><?php
										if(isset($header[0]['KOTA_TTD'])) echo strtoupper($header[0]['KOTA_TTD']);
										echo ", ";
										if(isset($header[0]['TANGGAL_TTD'])) echo date('d F Y', strtotime($header[0]['TANGGAL_TTD']));
									?></td>
								</tr>
								<tr>
									<td style="border: 0px; height: 100px;"></td>
								</tr>
								<tr>
									<td class="dt-body-center" style="border: 0px;"><?php
										if(isset($header[0]['NAMA_TTD'])) echo strtoupper($header[0]['NAMA_TTD']);
									?></td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- End Lembar 1 -->

<!-- Start Lembar 2 -->
<div class="card-box" style="margin-top: 20px;">
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<tr>
					<th style="font-size: 30px; width: 15%; vertical-align: middle;" class="dt-body-center">
						<?php if (isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']); ?>
					</th>
					<th style="font-size: 20px;" class="dt-body-center">
						<?php if(isset($bc[0]['desc_bc'])) echo wordwrap(strtoupper($bc[0]['desc_bc']), 70, "<br>\n"); ?>
					</th>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<th colspan="2">HEADER</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="border: 0px; padding-left: 15px; width: 50%;" rowspan="4">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<thead>
									<tr>
										<td style="width: 30%;">Nomor Pengajuan</td>
										<td>: &nbsp; <?php if(isset($bc[0]['no_pengajuan'])) echo $bc[0]['no_pengajuan']; ?></td>
									</tr>
									<tr>
										<td style="width: 30%;">A. KANTOR PABEAN</td>
										<td>: &nbsp; KPPBC BANDUNG</td>
									</tr>
									<tr>
										<td style="width: 30%;">B. JENIS TPB</td>
										<td>: &nbsp; KAWASAN BERIKAT</td>
									</tr>
									<tr>
										<td style="width: 30%;">C. TUJUAN PENGIRIMAN</td>
										<td>: &nbsp; LAINNYA</td>
									</tr>
								</thead>
							</table>
						</td>
						<th class="dt-body-right" style="border-left: 0px; width: 50%; padding-right: 60px;">Halaman Ke-2 dari 3</th>
					</tr>
					<tr>
						<td style="border-left: 1px solid;" rowspan="3">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<thead>
									<tr>
										<td colspan="2">F. KOLOM KHUSUS BEA DAN CUKAI</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="padding-left: 20px; width: 30%; border: 0px;">Nomor Pendaftaran</td>
										<td style="border: 0px;">: &nbsp; <?php if(isset($bc[0]['no_pendaftaran'])) echo $bc[0]['no_pendaftaran']; ?></td>
									</tr>
									<tr>
										<td style="padding-left: 20px; width: 30%; border: 0px;">Tanggal</td>
										<td style="border: 0px;">: &nbsp; <?php if(isset($bc[0]['tanggal_pengajuan'])) echo date('d F Y', strtotime($bc[0]['tanggal_pengajuan'])); ?></td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<td class="dt-body-center" style="width: 5%;">No</td>
						<td class="dt-body-center" style="width: 44.99%;">JENIS DOKUMEN</td>
						<td class="dt-body-center" style="width: 25%;">NOMOR</td>
						<td class="dt-body-center" style="width: 25%;">TANGGAL</td>
					</tr>
				</thead>
				<tbody>
			<?php
				if(isset($dokumen)) {
					if(sizeof($dokumen) > 0) {
						for ($i = 0; $i < sizeof($dokumen); $i++) { ?>
							<tr>
								<td style="border-top: 0px; border-bottom: 0px;" class="dt-body-center"><?php echo ($i+1); ?></td>
								<td style="border-top: 0px; border-bottom: 0px;"><?php
									$dok = (int)$dokumen[$i]['KODE_JENIS_DOKUMEN'];
									if($dok != 640) {
										foreach ($ref_dok as $rdk => $rdv) {
											if($rdv['KODE_DOKUMEN'] == $dok) echo $rdv['URAIAN_DOKUMEN'];
										}
									}else echo "SURAT JALAN";
								?></td>
								<td style="border-top: 0px; border-bottom: 0px;"><?php echo $dokumen[$i]['NOMOR_DOKUMEN']; ?></td>
								<td style="border-top: 0px; border-bottom: 0px;" class="dt-body-center"><?php
									echo date('d-m-Y', strtotime($dokumen[$i]['TANGGAL_DOKUMEN']));
								?></td>
							</tr>
			<?php
						}
					}else { ?>
						<tr>
							<td style="border-top: 0px; border-bottom: 0px;" class="dt-body-center"></td>
							<td style="border-top: 0px; border-bottom: 0px;"></td>
							<td style="border-top: 0px; border-bottom: 0px;"></td>
							<td style="border-top: 0px; border-bottom: 0px;" class="dt-body-center"></td>
						</tr>
			<?php
					}
				}
			?>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
				<thead>
					<tr>
						<th class="dt-body-center" style="width: 50%; border-right: 0px;"></th>
						<th class="dt-body-left" style="width: 50%;">
							E. TANDA TANGAN PENGUSAHA TPB
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="width: 50%; border-right: 0px;">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<tr>
									<td style="border: 0px;" colspan="2"></td>
								</tr>
							</table>
						</td>
						<td style="width: 50%;">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<tr>
									<td class="dt-body-center" style="border: 0px;">Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberikan dalam pemberitahuan pabean ini</td>
								</tr>
								<tr>
									<td class="dt-body-center" style="border: 0px;"><?php
										if(isset($header[0]['KOTA_TTD'])) echo strtoupper($header[0]['KOTA_TTD']);
										echo ", ";
										if(isset($header[0]['TANGGAL_TTD'])) echo date('d F Y', strtotime($header[0]['TANGGAL_TTD']));
									?></td>
								</tr>
								<tr>
									<td style="border: 0px; height: 100px;"></td>
								</tr>
								<tr>
									<td class="dt-body-center" style="border: 0px;"><?php
										if(isset($header[0]['NAMA_TTD'])) echo strtoupper($header[0]['NAMA_TTD']);
									?></td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- End Lembar 2 -->

<!-- Start Lembar 3 -->
<div class="card-box" style="margin-top: 20px;">
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<tr>
					<th style="font-size: 30px; width: 15%; vertical-align: middle;" class="dt-body-center">
						<?php if (isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']); ?>
					</th>
					<th style="font-size: 20px;" class="dt-body-center">
						<?php if(isset($bc[0]['desc_bc'])) echo wordwrap(strtoupper($bc[0]['desc_bc']), 70, "<br>\n"); ?>
					</th>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<th colspan="2">HEADER</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="border: 0px; padding-left: 15px; width: 50%;" rowspan="4">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<thead>
									<tr>
										<td style="width: 30%;">Nomor Pengajuan</td>
										<td>: &nbsp; <?php if(isset($bc[0]['no_pengajuan'])) echo $bc[0]['no_pengajuan']; ?></td>
									</tr>
									<tr>
										<td style="width: 30%;">A. KANTOR PABEAN</td>
										<td>: &nbsp; KPPBC BANDUNG</td>
									</tr>
									<tr>
										<td style="width: 30%;">B. JENIS TPB</td>
										<td>: &nbsp; KAWASAN BERIKAT</td>
									</tr>
									<tr>
										<td style="width: 30%;">C. TUJUAN PENGIRIMAN</td>
										<td>: &nbsp; LAINNYA</td>
									</tr>
								</thead>
							</table>
						</td>
						<th class="dt-body-right" style="border-left: 0px; width: 50%; padding-right: 60px;">Halaman Ke-3 dari 3</th>
					</tr>
					<tr>
						<td style="border-left: 1px solid;" rowspan="3">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<thead>
									<tr>
										<td colspan="2">F. KOLOM KHUSUS BEA DAN CUKAI</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td style="padding-left: 20px; width: 30%; border: 0px;">Nomor Pendaftaran</td>
										<td style="border: 0px;">: &nbsp; <?php if(isset($bc[0]['no_pendaftaran'])) echo $bc[0]['no_pendaftaran']; ?></td>
									</tr>
									<tr>
										<td style="padding-left: 20px; width: 30%; border: 0px;">Tanggal</td>
										<td style="border: 0px;">: &nbsp; <?php if(isset($bc[0]['tanggal_pengajuan'])) echo date('d F Y', strtotime($bc[0]['tanggal_pengajuan'])); ?></td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<tr>
					<td class="dt-body-center" style="width: 50%;">Barang Jadi</td>
					<td class="dt-body-center" style="width: 50%">Bahan baku yang digunakan</td>
				</tr>
			</table>
			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<td class="dt-body-center" style="width: 5%;">NO</td>
						<td style="width: 37.5%;">Uraian dan jenis barang secara lengkap, kode barang, merk, tipe, ukuran, dan spesifikasi lain</td>
						<td class="dt-body-center" style="width: 5%;">Jumlah</td>
						<td class="dt-body-center" style="width: 5%;">Satuan</td>
						<td style="width: 37.5%;">Uraian dan jenis barang secara lengkap, kode barang, merk, tipe, ukuran, dan spesifikasi lain</td>
						<td class="dt-body-center" style="width: 5%;">Jumlah</td>
						<td class="dt-body-center" style="width: 5%;">Satuan</td>
					</tr>
				</thead>
				<tbody>
		<?php
			if(isset($barang) && (sizeof($barang) > 0)) {
				for ($i = 0; $i < sizeof($barang); $i++) { ?>
					<tr>
						<td style="border-top: 0px; border-bottom: 0px;" class="dt-body-center"><?php echo ($i+1); ?></td>
						<td style="border-top: 0px; border-bottom: 0px;"><ul>
							<li><?php echo $barang[$i]['URAIAN']; ?></li>
							<li>Kode Barang : <?php echo $barang[$i]['KODE_BARANG']; ?></li>
							<li>Merk : <?php echo $barang[$i]['MERK']; ?></li>
							<li>Tipe : <?php echo $barang[$i]['TIPE']; ?></li>
							<li>Ukuran : <?php echo $barang[$i]['UKURAN']; ?></li>
							<li>Spesifikasi Lain : <?php echo $barang[$i]['SPESIFIKASI_LAIN']; ?></li>
						</ul></td>
						<td style="border-top: 0px; border-bottom: 0px;" class="dt-body-center"><?php
							echo number_format($barang[$i]['JUMLAH_SATUAN'], 4);
						?></td>
						<td style="border-top: 0px; border-bottom: 0px;" class="dt-body-center"><?php
							foreach ($ref_satuan as $rfk => $rfv) {
								if((int)$rfv['ID'] == (int)$barang[$i]['KODE_SATUAN']) echo $rfv['KODE_SATUAN'];
							}
						?></td>
						<td style="border-top: 0px; border-bottom: 0px;"></td>
						<td style="border-top: 0px; border-bottom: 0px;" class="dt-body-center"></td>
						<td style="border-top: 0px; border-bottom: 0px;" class="dt-body-center"></td>
					</tr>
		<?php
				}
			}else { ?>
				<tr>
					<td style="border-top: 0px; border-bottom: 0px;" class="dt-body-center"></td>
					<td style="border-top: 0px; border-bottom: 0px;"></td>
					<td style="border-top: 0px; border-bottom: 0px;" class="dt-body-center"></td>
					<td style="border-top: 0px; border-bottom: 0px;" class="dt-body-center"></td>
					<td style="border-top: 0px; border-bottom: 0px;"></td>
					<td style="border-top: 0px; border-bottom: 0px;" class="dt-body-center"></td>
					<td style="border-top: 0px; border-bottom: 0px;" class="dt-body-center"></td>
				</tr>
		<?php
			} ?>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
				<thead>
					<tr>
						<th class="dt-body-center" style="width: 50%; border-right: 0px;"></th>
						<th class="dt-body-left" style="width: 50%;">
							E. TANDA TANGAN PENGUSAHA TPB
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="width: 50%; border-right: 0px;">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<tr>
									<td style="border: 0px;" colspan="2"></td>
								</tr>
							</table>
						</td>
						<td style="width: 50%;">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<tr>
									<td class="dt-body-center" style="border: 0px;">Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberikan dalam pemberitahuan pabean ini</td>
								</tr>
								<tr>
									<td class="dt-body-center" style="border: 0px;"><?php
										if(isset($header[0]['KOTA_TTD'])) echo strtoupper($header[0]['KOTA_TTD']);
										echo ", ";
										if(isset($header[0]['TANGGAL_TTD'])) echo date('d F Y', strtotime($header[0]['TANGGAL_TTD']));
									?></td>
								</tr>
								<tr>
									<td style="border: 0px; height: 100px;"></td>
								</tr>
								<tr>
									<td class="dt-body-center" style="border: 0px;"><?php
										if(isset($header[0]['NAMA_TTD'])) echo strtoupper($header[0]['NAMA_TTD']);
									?></td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- End Lembar 3 -->

<script type="text/javascript">
	var id_bc = "<?php if(isset($bc[0])){ echo $bc[0]['id']; } ?>";

	$(document).ready(function() {
		$('#back_bc').on('click', function() {
			$('#title_Menu').html('Bea Cukai');
			$('#div_list_bc').show();
			$('#div_detail_bc').hide();
			$('#div_detail_bc').html('');
		})

		$('#downloadDetail').on('click', function() {

		})
	});
</script>