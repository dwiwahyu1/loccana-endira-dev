<style type="text/css">
	.control-label-detail {text-align: right;}

	.item-detail {
		margin-top: 10px;
		margin-bottom: 10px;
	}
</style>

<div class="row">
	<div class="col-md-12 item-detail">
		<div class="item form-group">
			<label class="control-label-detail col-md-3" for="nama_limbah">Nama Limbah <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8">
				<input type="text" class="form-control" id="nama_limbah" name="nama_limbah" placeholder="Nama Limbah" autocomplete="off" required>
			</div>
		</div>
	</div>

	<div class="col-md-12 item-detail">
		<div class="item form-group">
			<label class="control-label-detail col-md-3" for="id_uom">UOM <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8">
				<select class="form-control" id="id_uom" name="id_uom" required>
					<option value="">Select UOM</option>
		<?php
			if(isset($uom) && sizeof($uom) > 0) {
				foreach ($uom as $k) { ?>
					<option value="<?php echo $k['id_uom']; ?>"><?php echo $k['uom_name'].' ('.$k['uom_symbol'].')'; ?></option>
		<?php
				}
			} ?>
				</select>
			</div>
		</div>
	</div>

	<div class="col-md-12 item-detail">
		<div class="item form-group">
			<label class="control-label-detail col-md-3" for="qty">Qty <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8">
				<input type="number" class="form-control" id="qty" name="qty" placeholder="Qty" autocomplete="off" required>
			</div>
		</div>
	</div>

	<div class="col-md-12 item-detail">
		<div class="item form-group">
			<label class="control-label-detail col-md-3" for="ket">Keterangan</label>
			<div class="col-md-8">
				<textarea class="form-control" id="ket" name="ket" placeholder="Keterangan" rows="4"></textarea>
			</div>
		</div>
	</div>

	<div class="col-md-12 item-detail">
		<div class="item form-group">
			<label class="control-label-detail col-md-3" for="btn_submit_detail"></label>
			<div class="col-md-8">
				<a class="btn btn-primary" id="btn_submit_detail">Simpan</a>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->

<script type="text/javascript">
	$(document).ready(function() {
		$('#id_uom').select2();

		$('#btn_submit_detail').on('click', function() {
			var nama_limbah		= $('#nama_limbah').val();
			var id_uom			= $('#id_uom').val();
			var text_id_uom		= $('#id_uom').find('option:selected').text();
			var qty 			= $('#qty').val();
			var ket 			= $('#ket').val();

			if((nama_limbah != '' && nama_limbah != null) && (id_uom != '' && id_uom != null) && (qty != '' && qty != null && qty > 0)) {
				dt_list_limbah.row.add([nama_limbah, qty, text_id_uom, ket, id_uom]).draw();

				$('#panel-modal-detail').toggle();
			}else swal("Failed!", "Invalid inputan, silahkan cek kembali!.", "error");
		})
	});
</script>
