<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Btb_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	/**
      * This function is get the list data in spb table
      * @param : $params is where condition for select query
      */
	
	public function deletes($id) {
		$sql 	= 'CALL btb_deletes(?)';

		$query 	=  $this->db->query($sql, array(
			$id
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function add_coa_values_cash($data)
	{
		$sql 	= 'CALL coavalue_add2(?,?,?,?,?,?,?,?)';
		
		$query 	= $this->db->query($sql,array(
			$data['payment_coa'],
			0,
			$data['delivery_date'],
			$data['id_valas'],
			$data['saldo'],
			0,
			1,
			$data['keterangan'],
		));

		$this->db->close();
		$this->db->initialize();
	}

	public function lists($params = array()) {
		$sql_all 	= 'CALL btb_list_new_all(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				$params['filter']
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL btb_list_new(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);
		
		return $return;
	}
	
	public function lists_detail($id_btb)
	{
		$sql 	= 'CALL btb_detail_list_new2(?)';
		
		$query 	= $this->db->query($sql,array(
			$id_btb
		));
		
		$return = $query->result_array();
		
		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function btb_list_po()
	{
		$sql 	= 'CALL btb_list_no_po_2()';

		$query 	= $this->db->query($sql);
		
		$return = $query->result_array();
		
		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function btb_search_by_idpo($data)
	{
		$sql 	= 'CALL btb_search_by_idpo(?)';

		$query 	= $this->db->query($sql,array(
			$data['id_po']
		));
		
		$return = $query->result_array();
		
		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function btb_search_po($data)
	{
		$sql 	= 'CALL btb_search_po(?)';

		$query 	= $this->db->query($sql,array(
			$data['id_po']
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	public function btb_detail_delete($data)
	{
		$sql 	= 'CALL btb_po_delete(?)';

		$query 	= $this->db->query($sql,array(
			$data['id_btb']
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
		public function update_status_spb($id) {
		$sql 	= 'update t_spb_status set status_spb = 3, status_name = "BTB" where id_spb = '.$id['id_spb'];

		//echo $sql;die;
		$query 	=  $this->db->query($sql);

		//$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		//return $result;
	}
	
	public function btb_insert_po($data)
	{
		$sql 	= 'CALL btb_add(?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['no_btb'],
			$data['tanggal_btb'],
			$data['type_btb'],
			$data['no_pol'],
			$data['surat_jalan']
		));
		
		$return['result'] = $query->result_array();
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$return['lastid'] = $lastid;

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function insertCoaValue($data) {
		$sql 	= 'CALL coavalue_add4(?,?,?,?,?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id_coa'],
				$data['id_parent'],
				$data['btb_date'],
				$data['id_valas'],
				$data['btb_value'],
				$data['adjusment'],
				$data['type_cash'],
				$data['note'],
				$data['rate'],
				$data['bukti'],
				$data['id_coa_temp']
			)
		);
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	public function btb_update($data)
	{
		$sql 	= 'CALL btb_update(?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_btb'],
			$data['no_btb'],
			$data['tanggal_btb'],
			$data['type_btb'],
			$data['no_pol'],
			$data['surat_jalan']
		));
		
		$return['code'] = $this->db->affected_rows();
		$return['status'] = 1;
		
		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	public function btb_update_qty_material($data,$total_harga)
	{
		$sql 	= 'CALL btb_update_qty_material(?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_material'],
			$data['unit_price'],
			$data['qty_diterima']
		));
		
		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function btb_update_qty($data)
	{
		$sql 	= 'CALL btb_update_qty(?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_spb'],
			$data['qty_diterima']
		));
		
		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function btb_add_detail_po($data)
	{
		$sql 	= 'CALL btb_add_detail_po(?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_spb'],
			$data['id_btb'],
			$data['qty_diterima']
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function btb_view_report($data)
	{
		$sql 	= 'CALL btb_view_report(?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['jenis_report'],
			$data['tanggal_awal'],
			$data['tanggal_akhir']
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function approve_btb($data,$id_material,$qty,$base_price,$status_appr)
	{
		$sql 	= 'CALL btb_update_status(?,?,?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_po_spb'],
			$id_material,
			$status_appr,
			$data['status_material'],
			$data['keterangan'],
			$qty,
			$data['no_btb'],
			$data['no_bc'],
			$base_price
		));
		
		$return['code'] = $this->db->affected_rows();
		$return['array'] = $query->result_array();
		
		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function insert_no_btb($data,$qty)
	{
		$sql 	= ' UPDATE `t_spb`
			SET	`t_spb`.`no_btb` = "'.$data['no_btb'].'"
			WHERE  `t_spb`.`id` = (SELECT id_spb FROM t_po_spb WHERE `t_po_spb`.`id` = '.$data['id_po_spb'].' ) ';

		$query 	= $this->db->query($sql);
		
		$this->db->close();
		$this->db->initialize();
	}

	public function btb_mutasi_add($data)
	{
		$sql 	= 'CALL mutasi_add(?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_material'],
			$data['id_material'],
			$data['tanggal_btb'],
			$data['qty_diterima'],
			0
		));
		
		$return['code'] = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function update_qty_material($data)
	{
		$sql 	= 'CALL update_qty_material(?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_material'],
			$data['qty_diterima']
		));
		
		$return['code'] = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function btb_mutasi_add_hutang($data)
	{
		$sql 	= 'CALL mutasi_add(?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_material'],
			$data['id_material'],
			$data['qty_diterima'],
			0
		));
		
		$return['code'] = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function approve_detail($idpospb)
	{
		$sql 	= 'CALL btb_search_idpo(?)';

		$query 	= $this->db->query($sql,array(
			$idpospb
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function btb_detail($idbtb)
	{
		$sql 	= 'CALL btb_search_idbtb(?)';

		$query 	= $this->db->query($sql,array(
			$idbtb
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_data($params)
	{
		$sql 	= 'CALL btb_get_data_fix(?)';

		$query 	= $this->db->query($sql,array(
			$params['id_btb']
		));
		
		$return['code'] = $this->db->affected_rows();
		$return['results'] = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function check_po($params)
	{
		$sql 	= 'select count(*) as cnt_po from `t_purchase_order` a
		left join `t_po_spb` b on a.`id_po` = b.`id_po`
		where a.`id_po` = ? and b.`status` <> 2';

		$query 	= $this->db->query($sql,array(
			$params
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_id_eksternal($params)
	{
		$sql 	= 'CALL btb_get_id_eksternal(?)';

		$query 	= $this->db->query($sql,array(
			$params['id_distributor']
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function type_bc($type_bc)
	{
		$sql 	= 'SELECT a.*,b.`jenis_bc` AS jb FROM `t_bc` a
					LEFT JOIN `m_type_bc` b ON
					a.`jenis_bc` = b.`id`
					WHERE b.`type_bc` = ?
					GROUP BY b.`jenis_bc`, a.no_pendaftaran';

		$query 	= $this->db->query($sql,array(
			$type_bc
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function list_bc_all($type_bc)
	{
		$sql 	= 'SELECT * FROM `m_type_bc` b
					WHERE b.`type_bc` = ?
					';

		$query 	= $this->db->query($sql,array(
			$type_bc
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_id_coa($data)
	{
		$sql 	= 'CALL btb_get_id_coa(?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_po'],
			$data['id_distributor']
		));
		 
		$return= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_btb_rate($id_btb) {
		$sql 	= 'CALL btb_search_rate(?)';

		$query 	= $this->db->query($sql, array(
			$id_btb
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_kartu_hp($data)
	{
		$sql 	= 'CALL hp_add(?,?,?,?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['ref'],
			$data['source'],
			$data['keterangan'],
			$data['status'],
			$data['saldo'],
			$data['saldo_akhir'],
			$data['id_valas'],
			$data['type_kartu'],
			$data['id_master'],
			$data['type_master']
		));
		
		$return['code'] = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function mutasi_add($data,$date_mutasi)
	{
		$sql 	= 'CALL mutasi_add(?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_material'],
			$data['id_material'],
			$date_mutasi,
			$data['qty_diterima'],
			0
		));
		
		$return['code'] = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_po_spb_coa($id_coa_value,$id_po_spb)
	{
		$sql 	= 'CALL hp_po_spb_coa_add(?,?)';

		$query 	= $this->db->query($sql,array(
			$id_coa_value,
			$id_po_spb
		));
		
		$return['code'] = $this->db->affected_rows();
		$return['array'] = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_coa_values($data)
	{
		$sql 	= 'CALL coavalue_add(?,?,?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_coa'],
			$data['id_parent'],
			$data['id_valas'],
			$data['btb_value'],
			$data['adjusment'],
			$data['type_cash'],
			$data['note'],
			$data['rate'],
			$data['id_coa_temp']
		));
		
		$return['code'] = $this->db->affected_rows();
		$return['result'] = $query->result_array();
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$return['lastid'] = $lastid;

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_btb_coa($data) {
		$sql 	= 'CALL btb_add_btb_coa(?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_btb'],
			$data['id_coa_value']
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function update_status_btb($data) {
		$sql 	= 'UPDATE t_btb SET status = ? WHERE id_btb = ?';

		$query 	= $this->db->query($sql,array(
			$data['status'],
			$data['id_btb']
		));
		
		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
}