	<style>
		#loading-us{display:none}
		#tick{display:none}

		#loading-mail{display:none}
		#cross{display:none}
		.add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
		.add_item:hover{color:#ff8c00}
		.right-text{text-align:right}
		.form-item{margin-top: 15px;overflow: auto;}
		.dt-body-center{text-align: center;}
		.dt-body-left{text-align: left;}
		.dt-body-right{text-align: right;}
	</style>

	
  	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<br>
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_po">Nomor PO<span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="no_po" id="no_po" style="width: 100%" onchange="getPO();" required>
				<option value="">Select Nomor PO</option>
				<?php foreach($po as $po => $p) { ?>
					<option value="<?php echo $p['id_po']; ?>" ><?php echo $p['no_po'].' - '.$p['name_eksternal']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
		
	<div class="item form-group form-item">
		<div style="overflow-x:auto;">
			<table id="listpo" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>ID SPB</th>
						<th>ID PO</th>
						<th>ID Material</th>
						<th class="custom-tables align-text">No PO</th>
						<th class="custom-tables align-text">Kode Barang</th>
						<th class="custom-tables align-text">Nama Barang</th>
						<th class="custom-tables align-text">Satuan Barang</th>
						<th class="custom-tables align-text">Qty</th>
						<th class="custom-tables align-text">Qty Diterima</th>
						<th class="custom-tables align-text">Status</th>
						<th class="custom-tables align-text">Harga Unit</th>
						<th class="custom-tables align-text">Ket</th>
						<th class="custom-tables align-text">Actions</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>

	<hr>
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="btn_submit_simpan"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn_submit_simpan" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Simpan</button>
		</div>
	</div>
	
	<script type="text/javascript">
	var table_po;
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
	
		resetDt();
		$("#no_po").select2();
	});
	
	function resetDt() {
		table_po = $('#listpo').DataTable( {
			"processing": true,
			"searching": false,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0, 1, 2],
				"visible": false,
				"searchable": false
			}]
		});
	}

	function getPO() {
		var value_no = $("#no_po").val();
		
		if(value_no != '') {
			sendNo($("#no_po option:selected").text().replace(/,/g, '')+','+value_no);
		}else {
			table_po.clear().draw();
			resetDt();
		}
	}
	
	function sendNo(po) {
		var tempPO = [];
		if(t_addPO.rows().data().length > 0) {
			for (var i = 0; i < t_addPO.rows().data().length; i++) {
				tempPO.push(t_addPO.rows().data()[i]);
			}
		}
	
		var datapost = {
			"po" : po,
			"tempPO" : tempPO
		};
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>btb/btb_search_po",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.success == true) {
					table_po.clear().draw();
					for (var i = 0; i < r.data.length; i++) table_po.row.add(r.data[i]).draw();
				}else {
					table_po.clear().draw();
					for (var i = 0; i < r.data.length; i++) table_po.row.add(r.data[i]).draw();
					swal("Perhatian!", r.message, "info");
				}
			}
		});
	}
	
	function valQty(row) {
		var rowData = table_po.row(row).data();
		var qty = rowData[7];
		var qty_diterima = $('#qty_diterima'+row).val();
		var hargaUnit = qty_diterima * rowData[10];
		
		/*if(qty_diterima > qty) {
			swal('Warning', 'Data qty diterima tidak boleh lebih besar dari qty yang telah ditentukan', 'info');
		}*/
	}
	
	$('#btn_submit_simpan').click(function() {
		var tempArr = [];
		var statusBtn = 0;
		
		$('input[type=checkbox]').each(function () {
			if(this.checked == true) {
				var row = parseInt($(this).val());
				var rowData = table_po.row(row).data();
				var qtyDiterima = $('#qty_diterima' + row).val();
				var qtyAsal = rowData[7];
				var option =
					'<button class="btn btn-icon waves-effect waves-light btn-danger">'+
						'<i class="fa fa-trash"></i>'+
					'</button>';
					
				if(qtyDiterima != '' && qtyDiterima != 0 || (qtyDiterima > 0 || qtyDiterima == qtyAsal || qtyDiterima > qtyAsal)) {
					var arrRow = [rowData[0], rowData[1], rowData[2], rowData[3], rowData[4], rowData[5], rowData[6], rowData[7], parseFloat(qtyDiterima), rowData[9], rowData[10], rowData[11], option];
					tempArr.push(arrRow);
					arrRow = [];
					statusBtn = 1;
				}else if(qtyDiterima == 0) {
					swal('Warning', 'Data qty diterima kolom ke-'+ (row+8) +' tidak boleh kosong');
					statusBtn = 0;
				}else {
					swal('Warning', 'Data qty diterima ke-'+ (row+8) +' harus diisi');
					statusBtn = 0;
				}
			}
		});
	
	
		if(statusBtn == 1) {
			for (var i = 0; i < tempArr.length; i++) {
				tempArr[i][8] = (tempArr[i][8]);
				t_addPO.row.add(tempArr[i]).draw();
			}
			$('#panel-modal-detail').modal('toggle');
		}

		for (var i = 0; i < t_addPO.rows().data().length; i++) {
			var rowData = t_addPO.row(i).data();
		}
	})
	</script>