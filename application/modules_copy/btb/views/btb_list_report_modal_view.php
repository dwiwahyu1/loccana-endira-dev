<style>
	.changed_status{cursor:pointer;text-decoration: underline;color:#96b6e8;}
	.changed_status:hover{color:#ff8c00}
	.custom-tables, th{text-align:center;vertical-align:middle;}
	.custom-tables.align-text, th{vertical-align:middle;}
	.dt-body-right{text-align:right;}
	#titleTanggal{margin-bottom:10px;}
	.center_numb{text-align:center;}
	.left_numb{text-align:left;}
	.right_numb{text-align:right;font-weight:bold;}
	.tableReport {
		box-sizing: border-box;
		text-align: left;
		width: 90%;
		margin: 10px auto;
		padding-bottom:5px;
		page-break-before: always;
		z-index:99;
	}
	.table-scroll {
		position:relative;
		max-width:100%;
		margin:auto;
		overflow:hidden;
		border:1px solid #f0fffd;
	}
	.table-wrap {
		width:100%;
		overflow:auto;
	}
	.table-scroll table {
		width:100%;
		margin:auto;
		border-spacing:0;
	}
	.table-scroll th, .table-scroll td {
		padding:5px 10px;
		border:1px solid #000;
		background:#fff;
		white-space:nowrap;
		vertical-align:top;
	}
</style>

<div class="card-box" id="view_report_btb">
	<div class="row">
		<div class="col-md-12">
			<div class="pull-left">
				<a class="btn btn-icon waves-effect waves-light btn-danger m-b-5" data-toggle="tooltip" data-placement="top" data-dismiss="modal" title="Keluar" id="keluar">
					<i class="fa fa-times"></i>
				</a>
			</div>
			<div class="pull-right">
				<!--<a class="btn btn-icon waves-effect waves-light btn-success m-b-5" data-toggle="tooltip" data-placement="top" title="Export to PDF" id="export_pdf">
					<i class="fa fa-file-pdf-o"></i>
				</a>-->
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download PDF" id="btn_download">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>

	<div class="col-md-12 text-center titleReport">
		<h2 id="titlePerusahaan">LAPORAN <?php echo strtoupper($report); ?></h2>
		<h2 id="namaPerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
		<h3 id="titleTanggal">PERIODE <?php echo $tanggal_awal.' S.D '.$tanggal_akhir; ?></h3>
	</div>
	<hr>
	<div id="table-scroll" class="table-scroll">
		<div class="table-wrap">
			<table id="listbtbreport" class="table table-striped table-bordered table-responsive dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th class="custom-tables text-center">No</th>
							<th class="custom-tables text-center">No BTB</th>
							<th class="custom-tables text-center">Tanggal BTB</th>
							<th class="custom-tables text-center">Type BTB</th>
							<th class="custom-tables text-center">Kode Barang</th>
							<th class="custom-tables text-center">Nama Barang</th>
							<th class="custom-tables text-center">Qty</th>
							<th class="custom-tables text-center">Qty Diterima</th>
							<th class="custom-tables text-center">Keterangan</th>
					</tr>
				</thead>
				<tbody>
					<?php $no = 1;
					foreach($hasil as $h) { ?>
						<tr>
							<td class="center_numb"><?php echo $no; ?></td>
							<td class="left_numb"><?php echo $h['no_btb']; ?></td>
							<td class="left_numb"><?php echo $h['tanggal_btb']; ?></td>
							<td class="left_numb"><?php echo $h['type_btb']; ?></td>
							<td class="left_numb"><?php echo $h['stock_code']; ?></td>
							<td class="left_numb"><?php echo $h['stock_name']; ?></td>
							<td class="right_numb"><?php echo $h['qty']; ?></td>
							<td class="right_numb"><?php echo $h['qty_diterima']; ?></td>
							<td class="left_numb"><?php echo $h['notes']; ?></td>
						</tr>
					<?php $no++; } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script>

$(document).ready(function() {
	var pesan = '<?php echo $msg['message']; ?>';
	var tgl_awal = '<?php echo $tanggal_awal; ?>';
	var tgl_akhir = '<?php echo $tanggal_akhir; ?>';
	$('#btn_print').on('click', function() {
		var divToPrint=document.getElementById("listbtbreport");
		newWin= window.open('', '', 'height=700,width=700');
		newWin.document.write(divToPrint.outerHTML);
		newWin.print();
		newWin.close();
	});
	
	$('#btn_download').click(function () {
		var doc = new jsPDF("l", "mm", "a4");
		var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
		var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
		
		// FOOTER
		doc.setTextColor(50);
		doc.setFontSize(12); 
		doc.setFontStyle('helvetica','arial','sans-serif','bold');
		doc.text($('#titlePerusahaan').html(), pageWidth / 2, 20, 'center');
		doc.text($('#namaPerusahaan').html(), pageWidth / 2, 25, 'center');
		doc.text($('#titleTanggal').html(), pageWidth / 2, 30, 'center');
		
		doc.autoTable({
			html 			: '#listbtbreport',
			headStyles		: {
				fontSize 	: 7,
				valign		: 'middle', 
				halign		: 'center',
			},
			bodyStyles		: {
				fontSize 	: 7,
			},
			theme			: 'plain',
			styles			: {
				fontSize : 6, 
				lineColor: [0, 0, 0],
				lineWidth: 0.15,
				cellWidth : 'auto',
				
			},
			margin 			: 4,
			rowPageBreak	: 'auto',
			showHead 		: 'firstPage',
			showFoot		: 'lastPage',
			startY			: 45
		});
		doc.save('Report <?php echo $report; ?>.pdf');
	});
	
	$('#export_pdf').click(function () {
		
		var url = '<?php echo base_url(); ?>btb/export_pdf'; 
		var tanggal_awal = '<?php echo $tanggal_awal; ?>';
		var tanggal_akhir = '<?php echo $tanggal_akhir; ?>';
		var report = '<?php echo $report ?>';
		  
		var form = $("<form action='" + url + "' method='post' target='_blank'>" +
			"<input type='hidden' name='tanggal_awal' value='" + tanggal_awal + "' />" +
			"<input type='hidden' name='tanggal_akhir' value='" + tanggal_akhir + "' />" +
			"<input type='hidden' name='report' value='" + report + "' />" +
			"</form>");
		$('body').append(form);
		form.submit();
	});
	
	function objectSize(obj) {
		var size = 0;
		for (key in obj) {
			if (obj.hasOwnProperty(key)) size++;
		}
		return size;
	}
	
	if(pesan == 1){
		swal("Maaf!", "Laporan periode "+tgl_awal+" S.D "+tgl_akhir+" tidak ditemukan...!!!", "info");
	}
});
</script>