	<style>
		#loading-us{display:none}
		#tick{display:none}

		#loading-mail{display:none}
		#cross{display:none}
		.add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
		.add_item:hover{color:#ff8c00}
	</style>
	
	<form class="form-horizontal form-label-left" id="reject_btb" role="form" action="<?php echo base_url('btb/reject_btb');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

  		<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

        <div class="item form-group">
        	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_po">No PO <span class="required"><sup>*</sup></span></label>
            <div class="col-md-8 col-sm-6 col-xs-12">
               	<input type="text" id="no_po" name="no_po" class="form-control col-md-7 col-xs-12" placeholder="No PO"  value="<?php echo $reject[0]['no_po'];?>" readonly>
               	<input type="hidden" id="id_po_spb" name="id_po_spb" class="form-control col-md-7 col-xs-12" placeholder="ID PO SPB"  value="<?php echo $reject[0]['id_po_spb'];?>" readonly>
               	<input type="hidden" id="id_material" name="id_material" class="form-control col-md-7 col-xs-12" placeholder="ID Material"  value="<?php echo $reject[0]['id_material'];?>" readonly>
            </div>
        </div>
            
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_spb">No SPB <span class="required"><sup>*</sup></span></label>
            <div class="col-md-8 col-sm-6 col-xs-12">
				  <div class="input-group date">
					<input placeholder="No SPB" type="text" class="form-control col-md-7 col-xs-12" id="no_spb" name="no_spb" required="required" value="<?php echo $reject[0]['no_spb'];?>" readonly>
					<input placeholder="ID SPB" type="hidden" class="form-control col-md-7 col-xs-12" id="id_spb" name="id_spb" required="required" value="<?php echo $reject[0]['id_spb'];?>" readonly>
					<input placeholder="Status" type="hidden" class="form-control col-md-7 col-xs-12" id="status" name="status" required="required" value="0" readonly>
					<input placeholder="Status Material" type="hidden" class="form-control col-md-7 col-xs-12" id="status_reject" name="status_reject" required="required" value="2" readonly>
				</div>
			</div>
       	</div>

        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="keterangan">Keterangan <span class="required"><sup>*</sup></span></label>
            <div class="col-md-8 col-sm-6 col-xs-12">
                <textarea data-parsley-maxlength="255" type="text" id="keterangan" name="keterangan" class="form-control col-md-7 col-xs-12" placeholder="Keterangan"></textarea>     
            </div>
        </div>
		
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
            <div class="col-md-8 col-sm-6 col-xs-12">
            	<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Reject BTB</button>
            </div>
        </div>
  </form><!-- /page content -->

<script type="text/javascript">
$(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();
});

  $('#reject_btb').on('submit',(function(e) {
    if($('#keterangan').val() !== ''){
        $('#btn-submit').attr('disabled','disabled');
        $('#btn-submit').text("Memasukkan data...");
        e.preventDefault();

        var id_po_spb = $('#id_po_spb').val(),
            id_material = $('#id_material').val(),
            id_spb = $('#id_spb').val(),
            status = $('#status').val();
            status_material = $('#status_material').val();
            keterangan = $('#keterangan').val();

        var datapost={
			"id_po_spb"			:   id_po_spb,
			"id_material"		:   id_material,
			"id_spb"			:   id_spb,
			"status"			:   status,
			"status_material"	:   status_material,
			"keterangan"		:   keterangan
		};

        $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:JSON.stringify(datapost),
            cache:false,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response.success == true) {
                    swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
                    }).then(function () {
						$('.panel-heading button').trigger('click');
						listbtb();
                    })
                } else{
                    $('#btn-submit').removeAttr('disabled');
                    $('#btn-submit').text("Reject BTB");
                    swal("Failed!", response.message, "error");
                }
            }
        }).fail(function(xhr, status, message) {
            $('#btn-submit').removeAttr('disabled');
            $('#btn-submit').text("Reject BTB");
            swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
        });
    }else{
		swal("Failed!", "Maaf kolom Keterangan harus diisi", "error");
    }
    return false;
  }));
</script>
