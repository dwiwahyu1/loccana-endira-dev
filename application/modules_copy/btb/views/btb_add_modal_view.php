	<style>
		#loading-us{display:none}
		#tick{display:none}

		#loading-mail{display:none}
		#cross{display:none}
		.add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
		.add_item:hover{color:#ff8c00}
		.right-text{text-align:right}
		.dt-body-center{text-align: center;}
		.dt-body-left{text-align: left;}
		.dt-body-right{text-align: right;}
	</style>

	<form class="form-horizontal form-label-left" id="btb_add" role="form" action="<?php echo base_url('btb/btb_insert_po');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

  		<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
		<br>
        <div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_btb">Type BTB <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<select class="form-control" name="type_btb" id="type_btb" style="width: 100%" required>
					<option value="">Select Type BTB</option>
					<option value="lokal">Lokal</option>
					<option value="import">Import</option>
					<option value="antar_kb">Antar KB</option>
				</select>
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_btb">Tanggal BTB <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<div class="input-group date">
					<input placeholder="<?php echo date('Y-m-d'); ?>" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="tanggal_btb" name="tanggal_btb" autocomplete="off" placeholder="" required>
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-th"></span>
					</div>
				</div>
			</div>
		</div>
		
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_btb">No Polisi <span class="required"><sup></sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
					<input placeholder="" type="text" class="form-control" id="no_pol" name="no_pol" style="width: 100%" autocomplete="off" placeholder="">
			</div>
		</div>
		
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_btb">Surat Jalan <span class="required"><sup></sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
					<input placeholder="" type="text" class="form-control" id="surat_jalan" name="surat_jalan" style="width: 100%" autocomplete="off" placeholder="" >
			</div>
		</div>
	
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar PO : </label>
			<div class="col-md-8 col-sm-6 col-xs-12 add_item">
				<a class="btn btn-primary" onclick="btb_add_item()">
					<i class="fa fa-plus"></i> Tambah PO
				</a> 
				<input type="hidden" id="tambah_barang" name="tambah_barang" value="0">
			</div>
		</div>
		
		<div class="item form-group">
			<div style="overflow-x:auto;">
				<table id="listaddPO" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>ID SPB</th>
							<th>ID PO</th>
							<th>ID Material</th>
							<th class="custom-tables align-text">No PO</th>
							<th class="custom-tables align-text">Kode Barang</th>
							<th class="custom-tables align-text">Nama Barang</th>
							<th class="custom-tables align-text">Satuan Barang</th>
							<th class="custom-tables align-text">Qty</th>
							<th class="custom-tables align-text">Qty Diterima</th>
							<th class="custom-tables align-text">Status</th>
							<th class="custom-tables align-text">Harga Unit</th>
							<th class="custom-tables align-text">Ket</th>
							<th class="custom-tables align-text">Actions</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	
		<hr>
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Simpan</button>
			</div>
		</div>
	</form>
	
	<script type="text/javascript">
	var dataPO = [];
	var t_addPO;
	
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
		
		$('#tanggal_btb').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true,
			changeYear: true,
			minDate: '-3M',
			maxDate: '+30D',
		});
		
		$("#type_btb").select2();
		dtPO();
	});
	
	function dtPO() {
		t_addPO = $('#listaddPO').DataTable( {
			"processing": true,
			"searching": false,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0],
				"visible": false,
				"searchable": false
			},{
				"targets": [1],
				"visible": false,
				"searchable": false
			},{
				"targets": [2],
				"visible": false,
				"searchable": false
			},{
				"targets": [6,8,9],
				"className": 'dt-body-right'
			}]
		});
	}
	
	function btb_add_item(){
		$('#panel-modal-detail').removeData('bs.modal');
		$('#panel-modal-detail  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal-detail  .panel-body').load('<?php echo base_url('btb/btb_add_item');?>');
		$('#panel-modal-detail  .panel-title').html('<i class="fa fa-plus"></i> Tambah PO');
		$('#panel-modal-detail').modal({backdrop:'static',keyboard:false},'show');
	}

	$('#listaddPO').on("click", "button", function(){
		t_addPO.row($(this).parents('tr')).remove().draw(false);
		for (var i = 0; i < t_addPO.rows().data().length; i++) {
			var rowData = t_addPO.row(i).data();
		}
	});
	
	function redrawTable(row) {
		var rowData = t_addPO.row(row).data();
		rowData[8] = $('#qty_diterima'+row).val();
		t_addPO.draw();
	}
	
	$('#btb_add').on('submit',(function(e) {
		if($('#type_btb').val() !== ''){
			$('#btn-submit').attr('disabled','disabled');
			$('#btn-submit').text("Memasukkan data...");
			e.preventDefault();
			var formData = new FormData(this);
			
			$.ajax({
				type:'POST',
				url: $(this).attr('action'),
				data:formData,
				cache:false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						var tanggal_btb = response.tanggal_btb;
						save_PO(response.lastid,tanggal_btb);
					} else{
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Simpan");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Simpan");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}else{
			swal("Failed!", "Maaf kolom Keterangan harus diisi", "error");
		}
		return false;
	}));
	
	function save_PO(lastid,tanggal_btb) {
		
		var arrTemp = [];
		for (var i = 0; i < t_addPO.rows().data().length; i++) {
			var rowData = t_addPO.row(i).data();
			arrTemp.push(rowData); 
		}

		var datapost = {
			"id_btb"	: lastid,
			"tanggal_btb"	: tanggal_btb,
			'listpo'	: arrTemp
		};
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>btb/btb_insert_detail_po",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						$('#panel-modal').modal('toggle');
						listbtb();
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Simpan");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Simpan");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}
	
	function isNumber(evt) {
		var iKeyCode = (evt.which) ? evt.which : evt.keyCode
		if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
			return false;

		return true;
	}
	</script>