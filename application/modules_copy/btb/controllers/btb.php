<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Btb extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('btb/btb_model');
		$this->load->library('log_activity');
		$this->load->library('sequence');
		$this->load->library('excel');
	}

	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	  * This function is redirect to index btb page
	  * @return Void
	  */
	public function index() {
		$this->template->load('maintemplate', 'btb/views/index');
	}

	/**
	  * This function is used for showing btb list
	  * @return Array
	  */
	function lists() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'DESC';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;
		
		$order_fields = array('a.tanggal_btb', 'a.no_btb', 'h.name_eksternal', 'a.type_btb');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->btb_model->lists($params);
		
		/*echo "<pre>";
		print_r($list);
		echo "</pre>";
		die;*/
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();
		
		$no = $start;
		foreach ($list['data'] as $k => $v) {
			$no++;
			$tanggal = date('d F Y', strtotime($v['tanggal_btb']));

			/*$date = date_create($v['tanggal_btb']);
			$tanggal = date_format($date,'d F Y');*/
			
			if($v['status'] == 0) {
				$status = '<span class="label label-warning">Pending</span>';
				
				$actions =
					'<div class="text-center">'.
						'<div class="btn-group">'.
							'<a class="btn btn-info" title="Detail BTB" onclick="btb_detail(\''.$v['id_btb'].'\')">'.
								'<i class="fa fa-search"></i>'.
							'</a>'.
						'</div>'.
						'<div class="btn-group">'.
							'<a class="btn btn-warning" title="Edit BTB" onclick="btb_edit(\''.$v['id_btb'].'\')">'.
								'<i class="fa fa-edit"></i>'.
							'</a>'.
						'</div>'.
						'<div class="btn-group">'.
							'<a class="btn btn-danger" title="Hapus BTB" onclick="btb_delete(\''.$v['id_btb'].'\')">'.
								'<i class="fa fa-trash"></i>'.
							'</a>'.
						'</div>'.
						'<div class="btn-group">'.
							'<a class="btn btn-success" title="Approve BTB" onclick="btb_approve(\''.$v['id_btb'].'\',\''.$v['id_po_spb'].'\')">'.
								'<i class="fa fa-check"></i>'.
							'</a>'.
						'</div>'.
					'</div>';
			}elseif($v['status'] == 1){
				$status = '<span class="label label-success">Approve</span>';
				
				$actions =
					'<div class="text-center">'.
						'<div class="btn-group">'.
							'<a class="btn btn-info" title="Detail BTB" onclick="btb_detail(\''.$v['id_btb'].'\')">'.
								'<i class="fa fa-search"></i>'.
							'</a>'.
						'</div>'.
					'</div>';
			}else{
				$status = '<span class="label label-danger">Reject</span>';
				
				$actions =
					'<div class="text-center">'.
						'<div class="btn-group">'.
							'<a class="btn btn-info" title="Detail BTB" onclick="btb_detail(\''.$v['id_btb'].'\')">'.
								'<i class="fa fa-search"></i>'.
							'</a>'.
						'</div>'.
						'<div class="btn-group">'.
							'<a class="btn btn-warning" title="Edit BTB" onclick="btb_edit(\''.$v['id_btb'].'\')">'.
								'<i class="fa fa-edit"></i>'.
							'</a>'.
						'</div>'.
					'</div>';
			}
			
			array_push($data, 
				array(
					$no,
					$v['no_btb'],
					$v['name_eksternal'],
					$tanggal,
					$v['type_btb'],
					$status,
					$actions
				)
			);
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function get_lists_detail($id_btb) {
		$list = $this->btb_model->lists_detail($id_btb);

		// echo "<pre>";print_r($list);echo "</pre>";die;
	
		$data = array();
		
		$i = 1;
		$total_amount = 0;
		foreach ($list as $k => $v) {
			$amount = floatval($v['total_amount_ppn']);
			
			$total_amount = $total_amount + $amount;
			array_push($data, 
				array(
					$i++,
					$v['stock_name'],
					number_format($v['qty_diterima'],2,',','.').' ('.$v['uom_symbol'].')',
					number_format($v['unit_price'],2,',','.'),
					//number_format($v['amount_ppn'],4,',','.'),
					'',
					number_format($v['total_amount_ppn'],2,',','.'),
					$v['no_spb'],
					$v['no_po'],
					$v['stock_code'],
					$v['notes']
				)
			);
		}

		$result["data"] = $data;
		$result["total"] = $total_amount;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function delete_btb() {
		$data 	= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		
		$list = $this->btb_model->deletes($params['id']);
		
		if($list > 0) {
			$this->log_activity->insert_activity('insert', 'Berhasil Delete');
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}else {
			$this->log_activity->insert_activity('insert', 'Berhasil Delete');
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
		

	}
	
	public function get_lists_edit($id_btb) 
	{
		$list = $this->btb_model->lists_detail($id_btb);
	
		$data = array();
		
		$i = 0;
		$total_amount = 0;
		foreach ($list as $k => $v) {
			$amount = floatval($v['total_amount']);
			
			$total_amount = $total_amount + $amount;
			array_push($data, 
				array(
					$i+1,
					$v['stock_name'],
					number_format($v['qty_diterima'],2,',','.'),
					number_format($v['unit_price'],4,',','.'),
					'',
					number_format($v['total_amount'],4,',','.'),
					$v['no_spb'],
					$v['no_po'],
					$v['stock_code'],
					$v['notes']
				)
			);
		}

		$result["data"] = $data;
		$result["total"] = $total_amount;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function btb_add() {
		// $no_btb = $this->sequence->get_no('btb_lokal');
		$data = array(
			// 'no_btb' => $no_btb,
		);
		
		$this->load->view('btb_add_modal_view',$data);
	}
	
	public function btb_report() {
		$data = array(
			'msg' => '',
			'tanggal_awal' => '',
			'tanggal_akhir' => ''
		);
		
		$this->load->view('btb_report_modal_view',$data);
	}
	
	public function btb_view_report() 
	{
		$jenis_report = $this->Anti_sql_injection($this->input->post('jenis_report', TRUE));
		$tanggalAwal = $this->Anti_sql_injection($this->input->post('tanggal_awal', TRUE));
		$tanggalAkhir = $this->Anti_sql_injection($this->input->post('tanggal_akhir', TRUE));
		
		$tglAwl = explode("/", $tanggalAwal);
		$tglAkhr = explode("/", $tanggalAkhir);
		$tanggal_awal = date('Y-m-d', strtotime($tglAwl[2].'-'.$tglAwl[1].'-'.$tglAwl[0]));
		$tanggal_akhir = date('Y-m-d', strtotime($tglAkhr[2].'-'.$tglAkhr[1].'-'.$tglAkhr[0]));
		
		if($jenis_report == 'lokal') {
			//LOKAL
			$lokal = array('jenis_report' => $jenis_report, 'tanggal_awal' => $tanggal_awal, 'tanggal_akhir' => $tanggal_akhir);
			$result_lokal = $this->btb_model->btb_view_report($lokal);
			
			$data = array(
				'report'			=> 'Bukti Terima Barang Lokal',
				'tanggal_awal'		=> date('d-F-Y', strtotime($tglAwl[2].'-'.$tglAwl[1].'-'.$tglAwl[0])),
				'tanggal_akhir'		=> date('d-F-Y', strtotime($tglAkhr[2].'-'.$tglAkhr[1].'-'.$tglAkhr[0])),
				'hasil'				=> $result_lokal
			);
			
			if(count($result_lokal) > 0){
				$msg = 'Cetak report';
				
				$data['msg'] = array(
					'message'	=> 0,
					'success'	=> true,
					'status'	=> 'success'
				);
				
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}else{
				$msg = 'Gagal mencetak report bukti terima barang';
				
				$data['msg'] = array(
					'message'	=> 1,
					'success'	=> false,
					'status'	=> 'error'
				);
				
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}
			
			$this->load->view('btb_list_report_modal_view', $data);
		}else if($jenis_report == 'import') {
			//IMPORT
			$import = array('jenis_report' => $jenis_report,'tanggal_awal' => $tanggal_awal, 'tanggal_akhir' => $tanggal_akhir);
			$result_import = $this->btb_model->btb_view_report($import);
			
			$data = array(
				'report'			=> 'Bukti Terima Barang Import',
				'tanggal_awal'		=> date('d-F-Y', strtotime($tglAwl[2].'-'.$tglAwl[1].'-'.$tglAwl[0])),
				'tanggal_akhir'		=> date('d-F-Y', strtotime($tglAkhr[2].'-'.$tglAkhr[1].'-'.$tglAkhr[0])),
				'hasil'				=> $result_import
			);
			
			if(count($result_import) > 0){
				$msg = 'Cetak report';
				
				$data['msg'] = array(
					'message'	=> 0,
					'success'	=> true,
					'status'	=> 'success'
				);
				
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}else{
				$msg = 'Gagal mencetak report bukti terima barang';
				
				$data['msg'] = array(
					'message'	=> 1,
					'success'	=> false,
					'status'	=> 'error'
				);
				
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}
			
			$this->load->view('btb_list_report_modal_view', $data);
		}else if($jenis_report == 'antar_kb') {
			//ANTAR KB
			$kb = array('jenis_report' => $jenis_report, 'tanggal_awal' => $tanggal_awal, 'tanggal_akhir' => $tanggal_akhir);
			$result_kb = $this->btb_model->btb_view_report($kb);
			
			$data = array(
				'report'			=> 'Bukti Terima Barang Antar KB',
				'tanggal_awal'		=> date('d-F-Y', strtotime($tglAwl[2].'-'.$tglAwl[1].'-'.$tglAwl[0])),
				'tanggal_akhir'		=> date('d-F-Y', strtotime($tglAkhr[2].'-'.$tglAkhr[1].'-'.$tglAkhr[0])),
				'hasil'				=> $result_kb
			);
			
			if(count($result_kb) > 0){
				$msg = 'Cetak report bukti terima barang';
				
				$data['msg'] = array(
					'message'	=> 0,
					'success'	=> true,
					'status'	=> 'success'
				);
				
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}else{
				$msg = 'Gagal mencetak report bukti terima barang';
				
				$data['msg'] = array(
					'message'	=> 1,
					'success'	=> false,
					'status'	=> 'error'
				);
				
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}
			
			$this->load->view('btb_list_report_modal_view', $data);
		}else {
			$msg = 'Gagal melihat report bukti terima barang';
			
			$result = array(
				'success' => false,
			);
			
			$this->log_activity->insert_activity('view', $msg);
			
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}
	
	public function btb_add_item() {
		$results = $this->btb_model->btb_list_po();
		
		$data = array(
			'po' => $results,
		);
		
		$this->load->view('btb_add_item_modal_view',$data);
	}
	
	public function btb_search_po() {
		$data   = file_get_contents("php://input");
		$params = json_decode($data,true);
		
		$explode = explode(',',$params['po']);
		
		$var = array(
			'id_po' => $explode[1],
		);

		// print_r($var);die;
		
		$list = $this->btb_model->btb_search_po($var);
		
		if(empty($list)){
			$data = array();
			$res = array(
				'success'	=> false,
				'message'	=> 'Data PO tidak ditemukan',
				'data'		=> $data
			);
		}else{
			$data = array();
			$tempObj = new stdClass();

			if(sizeof($params['tempPO']) > 0) {
				foreach ($params['tempPO'] as $p => $pk) {
					for ($i=0; $i <= sizeof($list); $i++) {
						if(!empty($list[$i])) {
							if($list[$i]['no_po'] == $pk[1] && $list[$i]['no_po'] == $pk[2]) {
								unset($list[$i]);
							}
						}
					}
				}
			}
			
			$i = 0;
			foreach ($list as $k => $v) {
				if($v['diskon'] != 0 || $v['diskon'] != '') { $diskon = $v['diskon']; }else{ $diskon = 0; }
				if($v['no_po'] != 0 || $v['no_po'] != '') { $po = $v['no_po']; }else{ $po = '-'; }
				
				$status = $v['status_po'];
				if($status == 0){
					$stat_btb	= 'Processed';
					$status = '		<label class="label label-success">';
					$status .=    		$stat_btb;
					$status .= '	</label>';
				}else{
					$stat_btb	= 'Approved';
					$status = '		<label class="label label-success">';
					$status .=    		$stat_btb;
					$status .= '	</label>';
				}
				
				$strQty =
				'<input type="number" class="form-control" min="0" id="qty_diterima'.$i.'" name="qty_diterima'.$i.'" onInput="valQty('.$i.')" style="height:25px; width: 100px;" value="'.$v['qty_diterima'].'">';
				
				$strOption =
					'<div class="checkbox">'.
						'<input id="option['.$i.']" type="checkbox" value="'.$i.'">'.
						'<label for="option['.$i.']"></label>'.
					'</div>';
				$harga_unit = $v['unit_price'] * $v['qty'];

				array_push($data, array(
					$v['id_spb'],
					$v['id_po'],
					$v['id_material'],
					$v['no_po'].' - '.$v['name_eksternal'],
					$v['stock_code'],
					$v['stock_name'],
					'('.$v['uom_symbol'].') - '.$v['uom_name'],
					number_format($v['qty'],2,'.',''),
					//number_format($v['qty_btb'],0,'.','.'),
					$strQty, 
					$status,
					$v['symbol_valas'].'. '.number_format($v['unit_price'],0,'.','.'),
					$v['notes_po'],
					$strOption
				));
				$i++;
			}
			
			$res = array(
				'success'	=> true,
				'status'	=> 'success',
				'data'		=> $data
			);
		}
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	public function btb_insert_po() {
		$type_btb		= $this->Anti_sql_injection($this->input->post('type_btb', TRUE));
		$tanggal_btb	= $this->Anti_sql_injection($this->input->post('tanggal_btb', TRUE));
		$po_number	= $this->Anti_sql_injection($this->input->post('tambah_barang', TRUE));
		$no_pol	= $this->Anti_sql_injection($this->input->post('no_pol', TRUE));
		$surat_jalan	= $this->Anti_sql_injection($this->input->post('surat_jalan', TRUE));
		
		if($type_btb == 'lokal') $no_btb = $this->sequence->get_max_no('btb_lokal');
		else if($type_btb == 'import') $no_btb = $this->sequence->get_max_no('btb_impor');
		else $no_btb = $this->sequence->get_max_no('btb_kb');
		
		$data = array(
			'no_btb'		=> $no_btb,
			'tanggal_btb'	=> $tanggal_btb,
			'type_btb'		=> $type_btb,
			'surat_jalan'	=> $surat_jalan,
			'no_pol'		=> $no_pol,
			'po_number'		=> $po_number
		);
			
		$results = $this->btb_model->btb_insert_po($data);
		if($results > 0) {
			if($type_btb == 'lokal') $this->sequence->save_max_no('btb_lokal');
			else if($type_btb == 'import') $this->sequence->save_max_no('btb_impor');
			else $this->sequence->save_max_no('btb_kb');

			$msg = 'Berhasil menambahkan data BTB';

			$results = array(
				'success' 		=> true,
				'message' 		=> $msg,
				'lastid' 		=> $results['lastid'],
				'tanggal_btb'	=> $tanggal_btb
			);
			
			$this->log_activity->insert_activity('insert', $msg. ' dengan No BTB ' .$no_btb);
		}else {
			$msg = 'Gagal menambahkan data BTB ke database';

			$results = array('success' => false, 'message' => $msg);
			$this->log_activity->insert_activity('insert', $msg. ' dengan No BTB ' .$no_btb);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}
	
	public function btb_edit_po() {
		$id_btb			= $this->Anti_sql_injection($this->input->post('id_btb', TRUE));
		$no_btb			= $this->Anti_sql_injection($this->input->post('no_btb', TRUE));
		$type_btb		= $this->Anti_sql_injection($this->input->post('type_btb', TRUE));
		$tanggal_btb	= $this->Anti_sql_injection($this->input->post('tanggal_btb', TRUE));
		$po_number		= $this->Anti_sql_injection($this->input->post('tambah_barang', TRUE));
		$no_pol			= $this->Anti_sql_injection($this->input->post('no_pol', TRUE));
		$surat_jalan	= $this->Anti_sql_injection($this->input->post('surat_jalan', TRUE));
		
		$data = array(
			'id_btb'		=> $id_btb,
			'no_btb'		=> $no_btb,
			'tanggal_btb'	=> $tanggal_btb,
			'type_btb'		=> $type_btb,
			'no_pol'		=> $no_pol,
			'surat_jalan'	=> $surat_jalan
		);
			
		$results = $this->btb_model->btb_update($data);
		
		if ($results['status'] == 1) {
			$msg = 'Berhasil update data BTB';

			$results = array(
				'success' 		=> true,
				'message' 		=> $msg,
				'lastid' 		=> $id_btb,
				'tanggal_btb'	=> $tanggal_btb
			);
			
			$this->log_activity->insert_activity('insert', $msg. ' dengan No BTB ' .$no_btb);
		}else {
			$msg = 'Gagal menambahkan data BTB ke database';

			$results = array('success' => false, 'message' => $msg);
			$this->log_activity->insert_activity('insert', $msg. ' dengan No BTB ' .$no_btb);
		}

		
		$this->btb_model->btb_detail_delete($data);
		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}
	
	/**
	  * This function is redirect to edit customer page
	  * @return Void
	  */
	public function btb_insert_detail_po() {
		$data   = file_get_contents("php://input");
		$params = json_decode($data, true);
		
		$total_amnt = 0;
		if (is_array($params['listpo']) || is_object($params['listpo'])) {
			foreach($params['listpo'] as $k => $v) {
				// $unit_p = explode(' ', $v[10]);
				$unit_p = preg_replace("/[^0-9\,]/", "", $v[10]);
				$total_amnt = $total_amnt + (floatval($v[8]) * floatval($unit_p));
				
				$arrTemp = array(
					'id_btb'		=> $params['id_btb'],
					'tanggal_btb'	=> $params['tanggal_btb'],
					'id_spb'		=> $v[0],
					'id_po'			=> $v[1],
					'id_material'	=> $v[2],
					'qty_diterima'	=> $v[8],
					'unit_price'	=> is_numeric($unit_p[1]),
					'total_amount'	=> is_numeric($v[8])*is_numeric($unit_p[1]),
					'ttl'			=> $total_amnt + (is_numeric($v[8])*is_numeric($unit_p[1]))
				);
				
				$results = $this->btb_model->btb_add_detail_po($arrTemp);
				$this->btb_model->update_status_spb($arrTemp);
				/*$upPoSpb = $this->btb_model->btb_update_qty($arrTemp);
				$material = $this->btb_model->btb_update_qty_material($arrTemp);
				$mutasi = $this->btb_model->btb_mutasi_add($arrTemp);*/
			}

			//Insert Coa Values
			$getBtbRate = $this->btb_model->get_btb_rate($params['id_btb']);
			foreach ($getBtbRate as $brk => $brv) {
				/*$dataCoaValueHutang = array(
					'id_coa'		=> $brv['id_hutang'],
					'id_parent'		=> 0,
					'btb_date'		=> date('Y-m-d', strtotime($params['tanggal_btb'])),
					'id_valas'		=> $brv['id_valas'],
					'btb_value'		=> $brv['value'],
					'adjusment'		=> 0,
					'type_cash'		=> 0,
					'note'			=> 'Hutang PO '.$brv['no_po'],
					'rate'			=> $brv['rate'],
					'bukti'			=> NULL,
					'id_coa_temp'	=> NULL
				);

				$dataCoaValuePembelian = array(
					'id_coa'		=> 120,
					'id_parent'		=> 0,
					'btb_date'		=> date('Y-m-d', strtotime($params['tanggal_btb'])),
					'id_valas'		=> $brv['id_valas'],
					'btb_value'		=> $brv['value'],
					'adjusment'		=> 0,
					'type_cash'		=> 0,
					'note'			=> 'Pembelian PO '.$brv['no_po'],
					'rate'			=> $brv['rate'],
					'bukti'			=> NULL,
					'id_coa_temp'	=> NULL
				);

				$coaValueHutang 	= $this->btb_model->add_coa_values($dataCoaValueHutang);
				$coaValuePembelian 	= $this->btb_model->add_coa_values($dataCoaValuePembelian);

				if($coaValuePembelian['result'] > 0) {
					$dataBtbCoa = array(
						'id_btb'		=> $params['id_btb'],
						'id_coa_value'	=> $coaValuePembelian['lastid']
					);

					$addBtbCoa = $this->btb_model->add_btb_coa($dataBtbCoa);
				}*/
			}
			// $this->btb_model->btb_mutasi_add_hutang($arrTemp);
		}
		
		
		if ($results > 0) {
			$msg = 'Berhasil menambahkan data PO';

			$results = array('success' => true, 'message' => $msg);
			$this->log_activity->insert_activity('insert', $msg. ' dengan ID BTB ' .$params['id_btb']);
		}else {
			$msg = 'Gagal menambahkan data detail PO ke database';

			$results = array('success' => false, 'message' => $msg);
			$this->log_activity->insert_activity('insert', $msg. ' dengan ID BTB ' .$params['id_btb']);
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($results);
	}
	
	/**
	  * This function is redirect to edit customer page
	  * @return Void
	  */
	public function approve($idpospb) {
		
		$results = $this->btb_model->approve_detail($idpospb);
		$result_type = $this->btb_model->type_bc('0');
		$result_type2 = $this->btb_model->list_bc_all('0');
		
		$data = array(
			'approve' => $results,
			'jenis_bc' => $result_type,
			'list_bc_all' => $result_type2
		);
		
		$this->load->view('approve_btb_modal_view',$data);
	}
	
	/**
	  * This function is redirect to edit customer page
	  * @return Void
	  */
	public function approve_btb() {
		$data   = file_get_contents("php://input");
		$params = json_decode($data,true);
		$title 	= 'Akan mengubah status BTB';
		$date_mutasi = date('Y-m-d H:i:s');
		
		if(count($params) > 0) {
			if($params['status'] == 1) {
				$get_key = $this->btb_model->get_data($params);
				// echo "<pre>";print_r($get_key); echo "</pre>";die;
				if($get_key['code'] <> '0') {
					$data_coa_id = array(
						'id_po'				=> $get_key['results'][0]['id_po'],
						'id_distributor'	=> $get_key['results'][0]['id_distributor'],
					);
					
					$get_data_id = $this->btb_model->get_id_coa($data_coa_id);
					$total_harga = 0;
					$it = 0;
					
					foreach($get_key['results'] as $ods){
						
						$harga = 0;
				
						$data_coa = array (
							'id_coa'		=> $ods['coa_barang'],
							'id_parent'		=> 0,
							'id_valas'		=> $ods['valas_id'],
							'btb_value'		=> $ods['total_amount_ppn'],
							'adjusment'		=> 0,
							'type_cash'		=> 0,
							'note'			=> 'Pembelian No PO : ' .$ods['no_po']. ' dengan Part Number : ' .$ods['stock_name'],
							'rate'			=> $ods['rate'],
							'id_coa_temp'	=> NULL		
						);
						
						$add_coa = $this->btb_model->add_coa_values($data_coa);
						
						$data_coa = array (
							'id_coa'		=> $ods['coa_material'],
							'id_parent'		=> 0,
							'id_valas'		=> $ods['valas_id'],
							'btb_value'		=> $ods['total_amount_ppn'],
							'adjusment'		=> 0,
							'type_cash'		=> 0,
							'note'			=> 'Pembelian No PO : ' .$ods['no_po']. ' dengan Part Number : ' .$ods['stock_name'],
							'rate'			=> $ods['rate'],
							'id_coa_temp'	=> NULL		
						);
						
						$add_coa = $this->btb_model->add_coa_values($data_coa);
						
						$total_ppn = $ods['total_amount_ppn_real'] * $ods['ppn'];
						$total_harga = $total_harga + $ods['total_amount_ppn'];
				
						$this->btb_model->mutasi_add($ods,$date_mutasi);
						$this->btb_model->btb_update_qty_material($ods,$ods['total_amount_ppn']);
					}
					
					if($get_data_id > 0) {
						$data_coa1 = array (
							'id_coa'		=> $get_data_id[0]['id_coa'],
							'id_parent'		=> 0,
							'id_valas'		=> $get_key['results'][0]['valas_id'],
							'btb_value'		=> $total_harga,
							'adjusment'		=> 0,
							'type_cash'		=> 0,
							'note'			=> 'Pembelian dengan No PO : ' .$get_key['results'][0]['no_po'],
							'rate'			=> $get_key['results'][0]['rate'],
							'id_coa_temp'	=> NULL		
						);
						
						$add_coa = $this->btb_model->add_coa_values($data_coa1);
						
						$data_kartu_hp = array(
							'ref' 			=> NULL, 
							'source' 		=> NUll,
							'keterangan' 	=> 'Pembelian PO '.$get_key['results'][0]['no_po'], 
							'status' 		=> 0,
							'saldo' 		=> $total_harga,
							'saldo_akhir' 	=> $total_harga, 
							'id_valas'		=> $get_key['results'][0]['valas_id'], 
							'type_kartu'	=> 1,
							'id_master'		=> $add_coa['lastid'],
							'type_master'	=> 0,
							'delivery_date'	=> $get_key['results'][0]['delivery_date'],
							'payment_coa'	=> $get_key['results'][0]['payment_coa']
						);
						
						$this->btb_model->add_kartu_hp($data_kartu_hp);
						
						if($total_ppn != 0) {
							$data_coa_ppn = array (
								'id_coa'		=> 450,
								'id_parent'		=> 0,
								'id_valas'		=> $get_key['results'][0]['valas_id'],
								'btb_value'		=> $total_ppn,
								'adjusment'		=> 0,
								'type_cash'		=> 0,
								'note'			=> 'Pembelian dengan No PO : ' .$get_key['results'][0]['no_po'],
								'rate'			=> $get_key['results'][0]['rate'],
								'id_coa_temp'	=> NULL		
							);
						
							$add_coa = $this->btb_model->add_coa_values($data_coa_ppn);
						}
						
						$data_status = array (
							'id_btb'	=> $params['id_btb'],
							'status'	=> $params['status']
						);
						
						$update_status_btb = $this->btb_model->update_status_btb($data_status);
						
						if($add_coa['result'] > 0) {
							$dataBtbCoa = array(
								'id_btb'		=> $params['id_btb'],
								'id_coa_value'	=> $add_coa['lastid']
							);

							$addBtbCoa = $this->btb_model->add_btb_coa($dataBtbCoa);
						}
					} else {
						$msg = 'Maaf data distributor dengan No PO : ' .$get_key['results'][0]['no_po']. ' ini tidak lengkap silahkan hubungi admin untuk pengajuan approval';

						$result = array(
							'success' 	=> false,
							'message' 	=> $msg,
							'status' 	=> $params['status'],
							'cek' 		=> 2,
							'title' 	=> $title
						);
					}
					// $insert_val_d = $this->btb_model->add_po_spb_coa($add_coa['lastid'],$get_key[0]['id_po']);
					
					$msg = 'Barang berhasil di approve dengan No PO : ' .$get_key['results'][0]['no_po'];

					$result = array(
						'success' 	=> true,
						'message' 	=> $msg,
						'status' 	=> $params['status'],
						'cek' 		=> 1,
						'title' 	=> $title
					);
				} 
				
			} else {
				
				$msg = 'Barang berhasil di reject dengan No PO : ' .$get_key['results'][0]['no_po'];

				$result = array(
					'success' 	=> true,
					'message' 	=> $msg,
					'status' 	=> $params['status'],
					'cek' 		=> 3,
					'title' 	=> $title
				);
			}
			
		} else {
			
			$msg = 'Barang Gagal di approve karena, Data belum lengkap silahkan cek kembali.';
			
			$result = array(
				'success' 	=> false,
				'message' 	=> $msg,
				'status' 	=> $params['status'],
				'cek' 		=> 4,
				'title' 	=> $title
			);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	/**
	  * This function is redirect to edit customer page
	  * @return Void
	  */
	  
	public function btb_detail($idbtb) 
	{
		$results = $this->btb_model->btb_detail($idbtb);
		$tanggal = $results[0]['tanggal_btb'];
		
		$tglbtb = explode("-", $tanggal);
		$tanggal_btb = date('d-F-Y', strtotime($tglbtb[2].'-'.$tglbtb[1].'-'.$tglbtb[0]));
		
		$data = array(
			'btb' => $results,
			'tanggal_btb' => $tanggal_btb
		);

		$this->load->view('btb_detail_modal_view',$data);
	}
	
	public function btb_edit($idbtb) 
	{
		$results = $this->btb_model->btb_detail($idbtb);
		$tanggal = $results[0]['tanggal_btb'];
		
		$tglbtb = explode("-", $tanggal);
		$tanggal_btb = date('d-F-Y', strtotime($tglbtb[2].'-'.$tglbtb[1].'-'.$tglbtb[0]));
		$listTypeBTB = array();
		$typeBTB1 = array(
			'text' => 'Lokal',
			'id' => 'lokal'
		);
		$typeBTB2 = array(
			'text' => 'Import',
			'id' => 'import'
		);
		$typeBTB3 = array(
			'text' => 'Antar KB',
			'id' => 'antar_kb'
		);
		array_push($listTypeBTB,$typeBTB1,$typeBTB2,$typeBTB3);
		$data = array(
			'btb' => $results,
			'tanggal_btb' => $tanggal_btb,
			'listTypeBTB'=>$listTypeBTB
		);
		// echo "<pre>";print_r($data); echo "</pre>";die;
		$this->load->view('btb_edit_modal_view',$data);
	}

	public function export_excel($idbtb) 
	{
		$btb_list = array();
		$result_btb = $this->btb_model->btb_detail($idbtb);
		$list = $this->btb_model->btb_detail($idbtb);

		$temptgl_btb = explode('-', $result_btb[0]['tanggal_btb']);
		$date_btb = date('d/M/Y', strtotime($temptgl_btb[0].'-'.$temptgl_btb[1].'-'.$temptgl_btb[2]));

		$data = array();
		foreach ($list as $k => $v) {
			array_push($data, array(
				'',
				$v['no_po'],
				$v['stock_code'],
				$v['stock_name'],
				$v['qty'],
				$v['qty_diterima'],
				$v['notes']
			));
		}

		$result["btb"]			= $result_btb[0];
		$result["itemPO"]		= $data;
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getActiveSheet()->setTitle("Bukti Terima Barang");
		$objPHPExcel->getProperties()->setCreator("System")
			->setLastModifiedBy("System")
			->setTitle("Bukti Terima Barang")
			->setSubject("Laporan Bukti Terima Barang")
			->setDescription("Report Bukti Terima Barang")
			->setKeywords("Bukti Terima Barang")
			->setCategory("Report");
						 
		$objPHPExcel->setActiveSheetIndex(0)
			// ->setCellValue('A1', 'Purchase Order')
			->setCellValue('A2', 'PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA')
			->setCellValue('A3', '');
							
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A4', 'No BTB')
			->setCellValue('B4', ': '.$result['btb']['no_btb'])

			->setCellValue('A5', 'Tanggal BTB')
			->setCellValue('B5', ': '.$result['btb']['tanggal_btb'])

			->setCellValue('A6', 'Type BTB')
			->setCellValue('B6', ': '.$result['btb']['type_btb'])

			->setCellValue('F7', 'No PO')
			->setCellValue('F8', 'Tanggal Diterima')

			->setCellValue('A10', 'No')
			->setCellValue('B10', 'No PO')
			->setCellValue('C10', 'Kode Material')
			->setCellValue('D10', 'Nama Material')
			->setCellValue('E10', 'QTY')
			->setCellValue('F10', 'QTY Diterima')
			->setCellValue('G10', 'Keterangan');
		
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('G7', ': '.$result['btb']['no_po'])->getStyle('G7')->getAlignment();
		
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('G8', ': '.date('d-M-Y', strtotime($result['btb']['tanggal_btb'])))->getStyle('G8')->getAlignment();

		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);

		$i = 11;
		$no = 1;
		foreach($result['itemPO'] as $itemPO) {
			// $objPHPExcel->setActiveSheetIndex()->getStyle('A5:W'.$i)->applyFromArray($styleArray);
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$i, $no)
				->setCellValue('B'.$i, $itemPO[1])
				->setCellValue('C'.$i, $itemPO[2])
				->setCellValue('D'.$i, $itemPO[3])
				->setCellValue('E'.$i, $itemPO[4])
				->setCellValue('F'.$i, $itemPO[5])
				->setCellValue('G'.$i, $itemPO[6]);
			
			//CUSTOM COLUMN -->
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$i, $no)->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
			
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('B'.$i, $itemPO[1])->getStyle('B'.$i)->getAlignment();
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
			
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('C'.$i, $itemPO[2])->getStyle('C'.$i)->getAlignment();
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
			
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('D'.$i, $itemPO[3])->getStyle('D'.$i)->getAlignment();
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
			
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('E'.$i, $itemPO[4])->getStyle('E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
			
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('F'.$i, $itemPO[5])->getStyle('F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
			
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('G'.$i, $itemPO[6])->getStyle('G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
			
			$i++;
			$no++;
		}

		$styleArray = array ();
		$styleArray['borders'] = array();
		$styleArray['borders']['allborders'] = array();
		$styleArray['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN;
		$styleArray['borders']['allborders']['color'] = array('argb' => '000000');

		$objPHPExcel->setActiveSheetIndex(0)->getStyle('A10:G'.($i-1))->applyFromArray($styleArray);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:G1');
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:W1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:G2');
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:W2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:G3');
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:W3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B4:G4');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:G5');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B6:G6');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B7:E7');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B8:E8');

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A9:G9');

		$objPHPExcel->setActiveSheetIndex(0)->getStyle('A10:G10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		// Footer
		$i = $i+2;
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A'.$i, 'Received by.');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$i.':B'.$i);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$i.':B'.$i)->applyFromArray($styleArray);

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('E'.$i, 'Prepared by.');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('F'.$i, 'Approved by.');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$i, 'Approved by.');
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('E'.$i.':G'.$i)->applyFromArray($styleArray);

		$i++;
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$i.':B'.$i);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A'.$i.':A'.($i+2));
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$i.':B'.($i+2))->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E'.$i.':E'.($i+2));
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('E'.$i.':E'.($i+2))->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F'.$i.':F'.($i+2));
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('F'.$i.':F'.($i+2))->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G'.$i.':G'.($i+2));
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('G'.$i.':G'.($i+2))->applyFromArray($styleArray);
		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="BTB Report_'.$result['btb']['no_btb'].'.xls"');
		
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;
	}
}