<style type="text/css">
  #loading-us {
    display: none
  }

  #tick {
    display: none
  }

  #loading-mail {
    display: none
  }

  #cross {
    display: none
  }

  .x-hidden {
    display: none
  }

  .add_item {
    cursor: pointer;
    text-decoration: underline;
    color: #96b6e8;
    padding-top: 6px;
  }

  .add_item:hover {
    color: #ff8c00
  }

  .right-text {
    text-align: right
  }
</style>

<form class="form-horizontal form-label-left" id="packing_edit" role="form" action="<?php echo base_url('packing/packing_insert'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
  <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
  <br>

  <input type="hidden" class="form-control" id="id_packing" name="id_packing" value='<?php echo $id_order; ?>'>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date_packing">Tanggal Packing <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <div class="input-group date">
        <input type="text" class="form-control col-md-7 col-xs-12 datepicker" id="date_packing" name="date_packing" required autocomplete="off">
        <div class="input-group-addon">
          <span class="glyphicon glyphicon-th"></span>
        </div>
      </div>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tipe_stok">Asal Qty<span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <select type="text" class="form-control right-text" id="tipe_stok" name="tipe_stok" autocomplete="off">
        <option value=0>Produksi</option>
        <option value=1>Stock Gudang</option>
      </select>
    </div>
  </div>

  <div class="item form-group x-hidden" id="hidden_field">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_material">Part Number<span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <select type="text" class="form-control" id="type_material" name="type_material" autocomplete="off">
        <option value="">Silahkan Pilih</option>
        <?php foreach ($type_material as $tm) { ?>
          <option value="<?php echo $tm['id_material']; ?>"><?php echo $tm['stock_name']; ?></option>
        <?php } ?>
      </select>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty_box">Qty Box <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="number" class="form-control right-text" id="qty_box" name="qty_box" min="1" value="1" autocomplete="off" required>
      <!-- <input type="text" class="form-control right-text" id="qty_box" name="qty_box" autocomplete="off" required onkeypress="javascript:return isNumber(event)"> -->
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty_array">Qty Array <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="number" class="form-control right-text" id="qty_array" name="qty_array" min="1" value="1" autocomplete="off" required>
      <!-- <input type="text" class="form-control right-text" id="qty_array" name="qty_array" autocomplete="off" required onkeypress="javascript:return isNumber(event)"> -->
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="netto">Netto <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="number" class="form-control right-text" id="netto" name="netto" autocomplete="off" min="0" value="<?php
                                                                                                                      if (isset($results_esf) && sizeof($results_esf) > 0) echo $results_esf['pcb_size'];
                                                                                                                      else echo "0";
                                                                                                                      ?>" step=".0001" readonly required>
      <!-- <input type="text" class="form-control right-text" id="netto" name="netto" autocomplete="off" required value="<?php
                                                                                                                          if (isset($results_esf) && sizeof($results_esf) > 0) echo $results_esf['pcb_size'];
                                                                                                                          else echo "0";
                                                                                                                          ?>"> -->
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="bruto">Bruto <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="number" class="form-control right-text" id="bruto" name="bruto" min="0" value="0" step=".0001" autocomplete="off" readonly required>
      <!-- <input type="text" class="form-control right-text" id="bruto" name="bruto" autocomplete="off" required> -->
    </div>
  </div>

  <hr>
  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Simpan</button>
    </div>
  </div>
</form>
<?php
/*echo "<pre>";
	print_r($results_esf);
	echo "</pre>";*/
?>
<script type="text/javascript">
  var nettoAwal = '<?php if (isset($results_esf) && sizeof($results_esf) > 0) echo $results_esf['pcb_size'];
                    else echo "0"; ?>';
  var pcs_array = '<?php echo $results_esf['pcs_array'] ?>';
  var id_eks = '<?php echo $results_esf['id_eks'] ?>';

  $(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();

    jQuery('#date_packing').datepicker({
      format: "yyyy-mm-dd",
      autoclose: true,
      todayHighlight: true,
      changeYear: true,
      minDate: '-3M',
      maxDate: '+30D',
    });

    // nettoAwal = parseFloat($('#netto').val());
    calculateNettoBrutto();
  });

  function calculateNettoBrutto() {
    var box = parseFloat($('#qty_box').val());
    var array = parseFloat($('#qty_array').val());

    /*if(id_eks == 15 || id_eks == 136) var netto = box * array * nettoAwal;
    else var netto = box * array * nettoAwal / pcs_array;*/

    var netto = box * array * nettoAwal;
    var bruto = netto + (box * 0.35);

    $('#netto').val(netto);
    $('#bruto').val(bruto);
  }

  $("#qty_box").on('keyup', function(e) {
    calculateNettoBrutto();

    /*$("#bruto").val(
    	parseFloat($("#netto").val()) + parseFloat(
    		$("#qty_box").val() * 0.35
    	)
    );*/
  });

  $('#qty_array').on('keyup', function(e) {
    calculateNettoBrutto();
  });

  $('#type_material').select2({
    allowClear: true,
    placeholder: {
      "id": "",
      "text": "Cari Part Number"
    },
  });

  $('#tipe_stok').on('change', function(e) {
    var type_stock = $('#tipe_stok').val();

    if (type_stock == 1) {
      $("#hidden_field").fadeIn();
      $("#hidden_field").animate(300);
      $('#hidden_field').removeClass('x-hidden');
    } else {
      $("#hidden_field").fadeOut();
      $("#hidden_field").animate(200);
      $('#hidden_field').addClass('x-hidden');

    }
  });

  $('#packing_edit').on('submit', (function(e) {
    if ($('#no_packing').val() !== '') {
      $('#btn-submit').attr('disabled', 'disabled');
      $('#btn-submit').text("Memasukkan data...");
      e.preventDefault();

      var date_packing = $('#date_packing').val(),
        qty_box = $('#qty_box').val(),
        qty_array = $('#qty_array').val(),
        netto = $('#netto').val(),
        bruto = $('#bruto').val(),
        id_packing = $('#id_packing').val(),
        tipe_stok = $('#tipe_stok').val()

      if (tipe_stok == 1) var id_material = $('#type_material').val();
      else var id_material = '';

      var datapost = {
        "id_packing": id_packing,
        "date_packing": date_packing,
        "qty_box": qty_box,
        "qty_array": qty_array,
        "netto": netto,
        "bruto": bruto,
        "tipe_stok": tipe_stok,
        "id_material": id_material
      };

      $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
          if (response.success == true) {
            swal({
              title: 'Success!',
              text: response.message,
              type: 'success',
              showCancelButton: false,
              confirmButtonText: 'Ok'
            }).then(function() {
              $('.panel-heading button').trigger('click');
              listpacking(id_packing);
            })
          } else {
            $('#btn-submit').removeAttr('disabled');
            $('#btn-submit').text("Simpan");
            swal("Failed!", response.message, "error");
          }
        }
      }).fail(function(xhr, status, message) {
        $('#btn-submit').removeAttr('disabled');
        $('#btn-submit').text("Simpan");
        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
      });
    } else {
      swal("Failed!", "Maaf kolom Keterangan harus diisi", "error");
    }
    return false;
  }));

  function isNumber(evt) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
      return false;

    return true;
  }
</script>