<style>
	.dt-body-left {text-align:left;}
	.dt-body-right {text-align:right;}
	.dt-body-center {text-align:center;}
</style>

<div class="container" style="height: auto;">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Packing</h4>
		</div>
	</div>

	<div class="row">
        <div class="col-sm-12">
            <div class="card-box">
				<table id="listpacking" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
                        <tr>
							<th>No</th>
							<th>Nomor PO</th>
							<th>Packing Date</th>
							<th>Customer</th>
							<th>Part Number</th>
							<th>Qty Order</th>
							<th>Qty Terkirim</th>
							<th>Qty Sisa</th>
							<th>Qty Box Terkirim</th>
							<th>Net Weight</th>
							<th>Gross Weight</th>
							
							
							<th>Option</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
            </div>
        </div><!-- end col -->
    </div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:800px;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal-detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:70%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$(document).ready(function(){
		listpacking();
	});
// function packing_edit(id){
	// $('#panel-modal').removeData('bs.modal');
	// $('#panel-modal .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	// $('#panel-modal .panel-body').load('<?php echo base_url('packing/packing_edit');?>'+'/'+id);
	// $('#panel-modal .panel-title').html('<i class="fa fa-pencil-o"></i> Edit Packing');
	// $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
// }

function packing_detail(id){
	$('#panel-modal-detail').removeData('bs.modal');
	$('#panel-modal-detail .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal-detail .panel-body').load('<?php echo base_url('packing/packing_detail');?>'+'/'+id);
	$('#panel-modal-detail .panel-title').html('<i class="fa fa-search-o"></i> Detail Packing');
	$('#panel-modal-detail').modal({backdrop:'static',keyboard:false},'show');
}

	function packing_edit(id){
		
		window.location = "<?php echo base_url('packing/packing_detail/');?>/"+id;
		
	}

function packing_delete(id){
	swal({
		title: 'Yakin akan Menghapus ?',
		text: 'data tidak dapat dikembalikan bila sudah dihapus !',
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Ya',
		cancelButtonText: 'Tidak'
	}).then(function () {
		var datapost={
			"id"  :   id
		};
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('packing/packing_delete');?>",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				
			   swal({
					title: 'Success!',
					text: response.message,
					type: 'success',
					showCancelButton: false,
					confirmButtonText: 'Ok'
				}).then(function () {
					window.location.href = "<?php echo base_url('packing');?>";
				})
				if (response.status == "success") {
				} else{
                    swal("Failed!", response.message, "error");
				}
			}
		});
	})
}

function listpacking(){
	$("#listpacking").dataTable({
		"processing": true,
		"serverSide": true,
		"ajax": "<?php echo base_url().'packing/lists';?>",
		"searchDelay": 700,
		"responsive": true,
		"lengthChange": false,
		"destroy": true,
		"info": false,
		"bSort": false,
		"dom": 'l<"toolbar">frtip',
		/*"initComplete": function(result){
			var element = '<div class="btn-group pull-left">';
				element += '  <label>Filter</label>';
				element += '  <label class="filter_separator">|</label>';
				element += '  <label>Customer:';
				element += '    <select class="form-control select_customer" onChange="listpacking()" style="height:30px;width: 340px;">';
				element += '      <option value="0">-- Semua Customer --</option>';
				$.each( result.json.customer, function( key, val ) {
				  var selectcust = (val.id===cust_id)?'selected':'';
						element += '    <option value="'+val.id+'" '+selectcust+'>'+val.name_eksternal+'</option>';
					});
				element += '    </select>';
				element += '  </label>';
					
		   $("#listpo_filter").prepend(element);
		}*/
		"columnDefs": [{
			"targets": [0],
			"searchable": false,
			"className": 'dt-body-center'
		}, {
			"targets": [1, 2, 3, 4],
			"searchable": false,
			"className": 'dt-body-left'
		}, {
			"targets": [4],
			"searchable": false,
			"className": 'dt-body-left'
		}, {
			"targets": [5, 6, 7, 8],
			"searchable": false,
			"className": 'dt-body-right'
		}, {
			"targets": [9],
			"searchable": false,
			"className": 'dt-body-center'
		}]
    });
}
  
function delete_packing(id){
	swal({
		title: 'Yakin akan Menghapus ?',
		text: 'data tidak dapat dikembalikan bila sudah dihapus !',
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Ya',
		cancelButtonText: 'Tidak'
    }).then(function () {
		var datapost={
			"id" : id
		};

      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>packing/delete_packing",
        data : JSON.stringify(datapost),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function(response) {
           $('.panel-heading button').trigger('click');
				listpacking();
				swal({
					title: 'Success!',
					text: response.message,
					type: 'success',
					showCancelButton: false,
					confirmButtonText: 'Ok'
				}).then(function () {
				})
			}
		});
    })
}
</script>