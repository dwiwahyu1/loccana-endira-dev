<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Packing extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('packing/packing_model');
		$this->load->library('log_activity');
		$this->load->library('sequence');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string)
	{
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index()
	{
		$this->template->load('maintemplate', 'packing/views/index');
	}

	function lists() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'asc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

		$order_fields = array('A.id_order', 'A.no_po', 'A.customer', 'A.stock_name');
		// $order_fields = array('order_no', 'stock_name');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->packing_model->lists($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		// echo "<pre>";print_r($list);die;
		$data = array();
		$no = (int)$start;
		foreach ($list['data'] as $k => $v) {
			$no++;
			// $actions = '<div class="btn-group">';
			// $actions .= '   <button class="btn btn-icon waves-effect waves-light btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Detail Packing" onClick="packing_detail(\'' . $v['id_order'] . '\')">';
			// $actions .= '       <i class="fa fa-search"></i>';
			// $actions .= '   </button>';
			// $actions .= '</div>';

			$actions = '<div class="btn-group">';
			$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Detail Packing" onClick="packing_edit(\'' . $v['id_order'] . '\')">';
			$actions .= '       <i class="fa fa-search"></i>';
			$actions .= '   </button>';
			$actions .= '</div>';

			array_push(
				$data,
				array(
					$no,
					$v['no_po'],
					$v['date_packing'],
					$v['customer'],
					$v['part_number'],
					number_format($v['qty_order'], 2, ',', '.'),
					number_format($v['qty_array_terkirim'], 2, ',', '.'),
					number_format($v['qty_sisa'], 2, ',', '.'),
					number_format($v['qty_box_terkirim'], 2, ',', '.'),
					number_format($v['net_weight'], 2, ',', '.'),
					number_format($v['gross_weight'], 2, ',', '.'),
					$actions
				)
			);
		}


		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	function lists_detail()
	{
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'asc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

		$order_fields = array('order_no', 'stock_name');

		$order_id = $this->input->get_post('order_id');

		//print_r($order_id);die;

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;
		$params['order_id'] = $order_id;

		$list = $this->packing_model->lists_detail($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		//echo "<pre>";print_r($list['data']);die;
		$data = array();
		$no = $start;
		foreach ($list['data'] as $k => $v) {
			$no++;
			$actions = '';

			if ($v['status_do'] == 'NOT') {

				$actions .= '<div class="btn-group">';
				$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edit_detail(\'' . $v['id_packing'] . '\')">';
				$actions .= '       <i class="fa fa-edit"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';

				$actions .= '<div class="btn-group">';
				$actions .= '   <button class="btn btn-danger waves-effect waves-light btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="delete_detail(\'' . $v['id_packing'] . '\')">';
				$actions .= '       <i class="fa fa-trash"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
			} else {
				$actions = '<span class="label label-success text-center">Send</span>';
			}

			array_push(
				$data,
				array(
          $no,
          $v['no_packing'],
					number_format($v['qty_box'], 2, ',', '.'),
					number_format($v['qty_array'], 2, ',', '.'),
					number_format($v['net_weight'], 2, ',', '.'),
					number_format($v['gross_weight'], 2, ',', '.'),
					$v['date_packing'],
					$actions
				)
			);
		}


		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function packing_detail($id)
	{


		//echo $id;die;
		$results = $this->packing_model->detail_packing_list($id);
		$customer = $this->packing_model->packing_customer();

		$data = array(
			'id_order' => $id,
			'packing' => $results,
			'customer' => $customer
		);

		//$this->load->view('index_detail',$data);

		$this->template->load('maintemplate', 'packing/views/index_detail', $data);
	}

	public function packing_edit_a()
	{

		$id = $this->input->get_post('dd');
		$results = $this->packing_model->detail_packing_list($id);
		$results_esf = $this->packing_model->detail_esf($id);
		
		if(sizeof($results_esf) > 0) $esf = $results_esf[0];
		else $esf = array();
		
		$customer = $this->packing_model->packing_customer();
		$type_material = $this->packing_model->get_type_material();
		
		$data = array(
			'id_order' => $id,
			'packing' => $results,
			'results_esf' => $esf,
			'customer' => $customer,
			'type_material' => $type_material
		);
		
		$this->load->view('packing_edit_view', $data);
	}

	public function packing_edit_update()
	{

		$id = $this->input->get_post('dd');
		// $id = $this->input->get_post('dd');
		//echo $id;die;
		$results = $this->packing_model->detail_packing_list_d($id);
		$customer = $this->packing_model->packing_customer();

		//print_r($results);die;

		$data = array(
			'id_order' => $id,
			'packing' => $results,
			'customer' => $customer
		);

		$this->load->view('packing_edit_view_update', $data);

		//$this->template->load('maintemplate', 'packing/views/index_detail',$data);
	}

	public function get_detail_packing($id)
	{
		$list = $this->packing_model->detail_packing_list($id);

		$data = array();
		$no = 1;
		$i = 0;
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list as $k => $v) {
			$no = $k + 1;

			$actions = '<div class="btn-group">';
			$actions .=		'<button class="btn btn-icon waves-effect waves-light btn-danger" data-toggle="tooltip" data-placement="top" title="Delete Packing">';
			$actions .=			'<i class="fa fa-trash"></i>';
			$actions .=		'</button>';
			$actions .=	'</div>';

			$strNoPack =
				'<input type="text" class="form-control" id="edit_no_pack' . $i . '" name="edit_no_pack' . $i . '" onInput="cal_qty_edit(' . $i . ')" onChange="redrawTable(' . $i . ')" value="' . $v['no_packing'] . '" style="width:100%;">';
			$strQtyBox =
				'<input type="text" class="form-control" min="0" id="edit_qty_box' . $i . '" name="edit_qty_box' . $i . '" onInput="cal_qty_edit(' . $i . ')" onChange="redrawTable(' . $i . ')" value="' . $v['qty_box'] . '" onkeypress="javascript:return isNumber(event)" style="width:100%;">';
			$strQtyArray =
				'<input type="text" class="form-control" min="0" id="edit_qty_array' . $i . '" name="edit_qty_array' . $i . '" onInput="cal_qty_edit(' . $i . ')" onChange="redrawTable(' . $i . ')" value="' . $v['qty_array'] . '" onkeypress="javascript:return isNumber(event)" style="width:100%;">';
			$strDatePack =
				'<input type="text" class="form-control datepicker" id="edit_date_pack' . $i . '" name="edit_date_pack' . $i . '" onInput="cal_qty_edit(' . $i . ')" onChange="redrawTable(' . $i . ')" value="' . $v['date_packing'] . '" style="width:100%;">';

			array_push($data, array(
				$no,
				$strNoPack,
				$strQtyBox,
				$strQtyArray,
				$strDatePack,
				$actions
			));
			$i++;
		}

		$results['data'] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}

	public function packing_edit()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$add_packing = $this->packing_model->insert_packing($params);

		$msg = 'Berhasil merubah data packing';

		$result = array(
			'success' => true,
			'message' => $msg
		);

		//$this->log_activity->insert_activity('update', $msg. ' dengan No Packing' .$params['no_pack']);
		$this->log_activity->insert_activity('update', $msg . ' dengan No Packing');

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function packing_update()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$add_packing = $this->packing_model->update_packing($params);

		$msg = 'Berhasil merubah data packing';

		$result = array(
			'success' => true,
			'message' => $msg
		);

		//$this->log_activity->insert_activity('update', $msg. ' dengan No Packing' .$params['no_pack']);
		$this->log_activity->insert_activity('update', $msg . ' dengan No Packing');

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function packing_insert()
	{
		$data   = file_get_contents("php://input");
		$params     = json_decode($data, true);

		$no_packing = $this->sequence->get_no('packing');

		$insert = $this->packing_model->insert_packing($no_packing, $params);
		$ddd = $this->packing_model->get_id_material($insert['lastid']);
		
		if($params['tipe_stok'] == 1){
			
			$this->packing_model->stock_insert_packing($no_packing, $params, $ddd[0]['id_material']);
			
		}
		
		$msg = 'Data packing berhasil disimpan';

		$results = array(
			'success' => true,
			'message' => $msg
		);

		$this->log_activity->insert_activity('insert', $msg);
		
		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}

	public function delete_packing()
	{

		$data 	= file_get_contents("php://input");
		$params = json_decode($data, true);

		$list = $this->packing_model->packing_delete($params['id']);

		$msg = 'Data packing berhasil di hapus';

		$results = array(
			'status' => 'success',
			'message' => $msg
		);

		$this->log_activity->insert_activity('delete', $msg . ' dengan ID Packing ' . $params['id']);

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($results);
	}
}
