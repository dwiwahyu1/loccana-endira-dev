<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Packing_model extends CI_Model {
	
	public function __construct() {
		
		parent::__construct();
		
	}

	public function lists($params = array())
	{
		$sql_all 	= 'CALL packing_all(?, ?, ?, ?, ?, @total_filtered, @total)';
		
		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				$params['filter']
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL packing(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array(); 
	
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();
		//var_dump($total_row);die;
		$return = array(
			'data' => $result,
			//'total_filtered' => $total['@total_filtered'],
			'total_filtered' => intval($total_row),
			//'total' => $total['@total']
			'total' => intval($total_row)
		);
		
		return $return;
	}	
	
	public function lists_detail($params = array())
	{
		$sql_all 	= 'CALL packing_all_detail(?, ?, ?, ?, ?, ?, @total_filtered, @total)';
		
		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				$params['order_id'],
				NULL,
				NULL,
				NULL,
				NULL,
				''
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL packing_detail(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['order_id'],
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();
	
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);
		
		return $return;
	}
	
	public function get_packing($id)
	{
		$sql 	= 'CALL packing_search_id(?)';

		$query 	= $this->db->query($sql,array(
			$id
		));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function detail_packing_list($id)
	{
		$sql 	= 'CALL packing_list(?)';

		$query 	= $this->db->query($sql,array(
			$id
		));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	public function detail_esf($id_order)
	{
		$sql 	= 'CALL esf_search_by_id_material(?)';

		$query 	= $this->db->query($sql,array(
			$id_order
		));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	public function detail_packing_list_d($id)
	{
		$sql 	= 'CALL packing_list_detail(?)';

		$query 	= $this->db->query($sql,array(
			$id
		));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_type_material() {
		$sql 	= 'SELECT b.id AS id_material,b.stock_name FROM m_type_material a JOIN m_material b ON a.id_type_material = b.type WHERE a.id_type_material IN (4,5)';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_id_material($id)
	{
		$sql 	= 'SELECT e.id AS id_material FROM t_packing a
					LEFT JOIN t_order b ON a.id_po_quot=b.id_order
					LEFT JOIN t_po_quotation c ON b.id_po_quotation=c.id
					LEFT JOIN m_material e ON b.id_produk=e.id
					WHERE a.id_packing= ? ';

		$query 	= $this->db->query($sql,array(
			$id
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function insert_packing($no,$data)
	{
		//$jml = sizeof($data['no_pack']);
		//$this->db->trans_start();
		// $this->db->delete('t_packing', array('id_po_quot' => $data['id_order']));
		// $this->db->delete('t_po_packing', array('id_po_quotation' => $data['id_order']));

		//for($i=0;$i<$jml;$i++) {
			$sql 	= 'CALL packing_add(?,?,?,?,?,?,?,?)';

			$query 	= $this->db->query($sql,array(
				$no,
				$data['qty_box'],
				$data['qty_array'],
				$data['date_packing'],
				$data['id_packing'],
				$data['netto'],
				$data['bruto'],
				$data['tipe_stok']
			));
		
			$query = $this->db->query('SELECT LAST_INSERT_ID()');
			$row = $query->row_array();
			$lastid = $row['LAST_INSERT_ID()'];
			
			// $sql 	= 'CALL po_packing_add(?,?,?,?)';

			// $query 	= $this->db->query($sql,array(
				// $data['id_order'],
				// 0,
				// $lastid,
				// $data['date_pack'][$i]
			// ));
		
		//}
		
		$this->db->close();
		$this->db->initialize();
		//$this->db->trans_complete()
		
		$arr_result['lastid'] = $lastid;

		return $arr_result;
	}	
	
	public function stock_insert_packing($no,$data)
	{
		$sql 	= 'update m_material set qty = qty - ? where id =  ? ';

		$query 	= $this->db->query($sql,array(
			$data['qty_box'] * $data['qty_array'],$data['id_material']
		));

		$this->db->close();
		$this->db->initialize();

		//return $return;
	}
	
	public function insert_po_packing($data)
	{
		$sql 	= 'CALL po_packing_add(?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_po_quot'],
			$data['id_schedule'],
			$data['id_packing'],
			$data['date_packing']
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function update_packing($data) {
		$sql 	= 'update  t_packing set date_packing = ?,qty_box = ?,qty_array = ?,net_weight = ?,gross_weight = ? where id_packing = ?';

		$query 	= $this->db->query($sql,array(
			$data['date_packing'],
			$data['qty_box'],
				$data['qty_array'],
				$data['netto'],
				$data['bruto'],
				$data['id_packing']
		));
		
		//$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		//return $return;
	}
	
	public function packing_delete($data)
	{
		$sql 	= 'CALL packing_delete(?)';

		$query 	= $this->db->query($sql,array(
			$data
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function packing_customer()
	{
		$this->db->select('id,name_eksternal');
		$this->db->from('t_eksternal');
		$this->db->where('type_eksternal', 1);

		$query 	= $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
}