<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class BC_Po_Quotation extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('bc_po_quotation/bc_po_quotation_model');
		$this->load->library('log_activity');
		$this->load->library('sequence');
		$this->load->library('excel');
		$this->codeData = array(
            'codeBC'		=> '0505',
            'codeCelebit'	=> '005347',
            'codeTgl'		=> date('Ymd')
        );
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'bc_po_quotation/views/index');
	}
	function list_bc() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 2;
		$order_fields = array('', 'jenis_bc', 'no_pendaftaran', 'no_pengajuan', 'tgl_pengajuan');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->bc_po_quotation_model->list_bc($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$btnstr = '';

			if($v['file_loc'] != '' || $v['file_loc'] != NULL) {
				$btnstr =
					'<div class="btn-group">'.
						'<a href="'.base_url().''. $v['file_loc'] .'" class="btn btn-info btn-sm" target="_blank" title="Download File" download>'.
							'<i class="fa fa-file"></i>'.
						'</a>'.
					'</div>';
			}

			$status_akses =
				/*'<div class="btn-group">'.
					'<button class="btn btn-warning btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
						onClick="edit_bc(\'' . $v['id'] . '\')">'.
						'<i class="fa fa-edit"></i>'.
					'</button>'.
				'</div>'.
				'<div class="btn-group">'.
					'<button class="btn btn-danger btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Delete"
						onClick="delete_bc(\'' . $v['id'] . '\')">'.
						'<i class="fa fa-trash"></i>'.
					'</button>'.
				'</div>'.*/
				// '<div class="btn-group">'.
				// 	'<button class="btn btn-info btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Details"
				// 		onClick="details_bc(\'' . $v['id'] . '\')">'.
				// 		'<i class="fa fa-list-alt"></i>'.
				// 	'</button>'.
				// '</div>'.
				// '<div class="btn-group">'.
				// 	'<button class="btn btn-success btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Download"
				// 		onClick="download_bc(\'' . $v['id'] . '\')">'.
				// 		'<i class="fa fa-file-excel-o"></i>'.
				// 	'</button>'.
				// '</div>'.
				'<div class="btn-group">'.
					'<button class="btn btn-info btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
						onClick="edit_bc(\'' . $v['id'] . '\')">'.
						'<i class="fa fa-edit"></i>'.
					'</button>'.
				'</div>';

			array_push($data, array(
				$i,
				$v['jenis_bc'],
				ucwords($v['no_pendaftaran']),
				ucwords($v['no_do']),
				$v['name_eksternal'],
				$v['detail_box'],
				date('d M Y', strtotime($v['tanggal_pengajuan'])), 
				//$btnstr,
				$status_akses
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	//list bc jadul
	// function list_bc() {
	// 	$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 10;
	// 	$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
	// 	$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
	// 	$order = $this->input->get_post('order');
	// 	$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
	// 	$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 2;
	// 	$order_fields = array('', 'jenis_bc', 'no_pendaftaran', 'name_eksternal', 'stock_name', 'stock_description', 'no_btb');

	// 	$search = $this->input->get_post('search');

	// 	$search_val = (!empty($search['value'])) ? $search['value'] : null;

	// 	$search_value = $this->Anti_sql_injection($search_val);

	// 	// Build params for calling model
	// 	$params['limit'] = (int) $length;
	// 	$params['offset'] = (int) $start;
	// 	$params['order_column'] = $order_fields[$order_column];
	// 	$params['order_dir'] = $order_dir;
	// 	$params['filter'] = $search_value;

	// 	$list = $this->bc_po_quotation_model->list_bc($params);
		
	// 	// echo"<pre>";print_r($list);die;
	// 	$result["recordsTotal"] = $list['total'];
	// 	$result["recordsFiltered"] = $list['total_filtered'];
	// 	$result["draw"] = $draw;

	// 	$data = array();
	// 	$i = 0;
	// 	$username = $this->session->userdata['logged_in']['username'];
	// 	foreach ($list['data'] as $k => $v) {
	// 		$i = $i + 1;
	// 		$btnstr = '';

	// 		if($v['file_loc'] != '' || $v['file_loc'] != NULL) {
	// 			$btnstr =
	// 				'<div class="btn-group">'.
	// 					'<a href="'. $v['file_loc'] .'" class="btn btn-info btn-sm" target="_blank" title="Download File" download>'.
	// 						'<i class="fa fa-file"></i>'.
	// 					'</a>'.
	// 				'</div>';
	// 		}

	// 		$status_akses =
	// 			/*'<div class="btn-group">'.
	// 				'<button class="btn btn-warning btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
	// 					onClick="edit_bc(\'' . $v['id'] . '\')">'.
	// 					'<i class="fa fa-edit"></i>'.
	// 				'</button>'.
	// 			'</div>'.
	// 			'<div class="btn-group">'.
	// 				'<button class="btn btn-danger btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Delete"
	// 					onClick="delete_bc(\'' . $v['id'] . '\')">'.
	// 					'<i class="fa fa-trash"></i>'.
	// 				'</button>'.
	// 			'</div>'.*/
	// 			'<div class="btn-group">'.
	// 				'<button class="btn btn-info btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Details"
	// 					onClick="details_bc(\'' . $v['id'] . '\')">'.
	// 					'<i class="fa fa-list-alt"></i>'.
	// 				'</button>'.
	// 			'</div>'.
	// 			'<div class="btn-group">'.
	// 				'<button class="btn btn-success btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Download"
	// 					onClick="download_bc(\'' . $v['id'] . '\')">'.
	// 					'<i class="fa fa-file-excel-o"></i>'.
	// 				'</button>'.
	// 			'</div>';

	// 		array_push($data, array(
	// 			$i,
	// 			$v['jenis_bc'],
	// 			ucwords($v['no_pendaftaran']),
	// 			date('d M Y', strtotime($v['tanggal_pengajuan'])),
	// 			$v['order_no'],
	// 			date('d M Y', strtotime($v['tanggal'])),
	// 			$v['name_eksternal'],
	// 			$v['stock_code'],
	// 			$v['stock_name'],
	// 			$v['uom_name'],
	// 			number_format($v['jumlah'],2),
	// 			$v['nama_valas'],
	// 			number_format($v['Nilai_barang'],2),
	// 			$status_akses
	// 		));
	// 	}

	// 	$result["data"] = $data;

	// 	$this->output->set_content_type('application/json')->set_output(json_encode($result));
	// }

	public function add_bc() 
	{
		$result_type = $this->bc_po_quotation_model->type_bc();
		$result_do = $this->bc_po_quotation_model->list_delivery_order();
		
		$data = array(
			'jenis_bc' => $result_type,
			'delivery_order' => $result_do
		);

		$this->load->view('add_modal_view', $data);
	}

	public function check_nopendaftaran() {
		$this->form_validation->set_rules('nopendaftaran', 'No Pendaftaran', 'trim|required|min_length[4]|max_length[100]|is_unique[t_bc.no_pendaftaran]');
		$this->form_validation->set_message('is_unique', 'No Pendaftaran Already Exists.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'No Pendaftaran Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}

	public function save_bc() {
		$this->form_validation->set_rules('jenis_bc', 'Jenis BC', 'trim|required');
		//$this->form_validation->set_rules('nopengajuan', 'No Pengajuan', 'trim|required|min_length[4]|max_length[100]|is_unique[t_bc.no_pengajuan]');
		$this->form_validation->set_rules('tglpengajuan', 'Tanggal Pengajuan', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('no_do', 'No DO', 'trim|required|max_length[100]');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$jenis_bc = $this->Anti_sql_injection($this->input->post('jenis_bc', TRUE));
			$nopendaftaran = $this->Anti_sql_injection($this->input->post('nopendaftaran', TRUE));
			$nopengajuan = ucwords($this->Anti_sql_injection($this->input->post('nopengajuan', TRUE)));
			$no_do = $this->Anti_sql_injection($this->input->post('no_do', TRUE));
			$tglinput = $this->Anti_sql_injection($this->input->post('tglpengajuan', TRUE));
			$temptgl = explode("/", $tglinput);
			$tglpengajuan = date('Y-m-d', strtotime($temptgl[2].'-'.$temptgl[1].'-'.$temptgl[0]));
			$type_text = ucwords($this->Anti_sql_injection($this->input->post('type_text', TRUE)));
			$upload_error = NULL;
			$file_bc = NULL;

			// if ($_FILES['file_bc']['name']) {
				// $this->load->library('upload');
				// $new_filename =
					// preg_replace('/\s/', '', $type_text) .' '.
					// $nopendaftaran .' '.
					// date('d-m-Y', strtotime($temptgl[2].'-'.$temptgl[1].'-'.$temptgl[0])).
					// '.'.pathinfo($_FILES['file_bc']['name'], PATHINFO_EXTENSION);

				// $config = array(
					// 'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/bc",
					// 'upload_url' => base_url() . "uploads/bc",
					// 'encrypt_name' => FALSE,
					// 'max_filename' => 100,
					// 'file_name' => $new_filename,
					// 'overwrite' => FALSE,
					// 'allowed_types' => 'pdf|txt|doc|docx',
					// 'max_size' => '10000'
				// );
				// $this->upload->initialize($config);

				// if ($this->upload->do_upload("file_bc")) {
					// // General result data
					// $result = $this->upload->data();

					// // Add our stuff
					// $file_bc = 'uploads/bc/'.$result['file_name'];
				// }else {
					// $pesan = $this->upload->display_errors();
					// $upload_error = strip_tags(str_replace("\n", '', $pesan));

					// $result = array(
						// 'success' => false,
						// 'message' => $upload_error
					// );
				// }
			// }

			// if (!isset($upload_error)) {
			$data = array(
				'jenis_bc' => $jenis_bc,
				'no_pendaftaran' => $nopendaftaran,
				'no_pengajuan' => $nopengajuan,
				'tanggal_pengajuan' => $tglpengajuan,
				'file_loc' => $file_bc
			);

			$result = $this->bc_po_quotation_model->add_bc($data);
			
			$data_bc_po = array(
				'id_bc' => $result['lastid'],
				'id_do' => $no_do
			);
			
			$data_header = array(
				'no_pengajuan' 		=> $nopengajuan,
				'id_bc' 			=> $result['lastid'],
				'tanggal_daftar'	=> $tglpengajuan,
				'id_do' 			=> $no_do
			);
			
			$result_bc_po =  $this->bc_po_quotation_model->add_bc_po_quotation($data_bc_po);
			//$result_add_header =  $this->bc_po_quotation_model->add_report_header($data_bc_po);
			
			if ($result > 0) {
				$this->log_activity->insert_activity('insert', 'Berhasil Menambahkan BC Keluar dengan No Pendaftaran : '.$nopendaftaran);
				$result = array('success' => true, 'message' => 'Berhasil menambahkan BC Keluar dengan No Pendaftaran : '.$nopendaftaran);
			}else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert BC Keluar');
				$result = array('success' => false, 'message' => 'Gagal menambahkan BC ke database.');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function edit_bc($id) {
		$result = $this->bc_po_quotation_model->edit_bc($id);
		$result_do = $this->bc_po_quotation_model->list_delivery_order_edit();
		$result_type = $this->bc_po_quotation_model->type_bc();
		$temptgl = explode('-', $result[0]['tanggal_pengajuan']);
		$result[0]['tanggal_pengajuan'] = date('d/M/Y', strtotime($temptgl[0].'-'.$temptgl[1].'-'.$temptgl[2]));
		if($result[0]['file_loc'] != '' || $result[0]['file_loc'] != NULL) $result[0]['file_loc'] = explode('uploads/bc/', $result[0]['file_loc'])[1];

		$data = array(
			'bc' => $result,
			'delivery_order' => $result_do,
			'jenis_bc' => $result_type,
			'folder' => 'uploads/bc/',
			'url' => base_url()
		);
		
		$this->load->view('edit_modal_view', $data);
	}

	public function save_edit_bc() {
		$this->form_validation->set_rules('jenis_bc', 'Jenis BC', 'trim|required');
		//$this->form_validation->set_rules('nopengajuan', 'No Pengajuan', 'trim|required|min_length[4]|max_length[100]');
		$this->form_validation->set_rules('tglpengajuan', 'Tanggal Pengajuan', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('no_do', 'No DO', 'trim|required|max_length[100]');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$status = 0;
			$bc_id = $this->Anti_sql_injection($this->input->post('bc_id', TRUE));
			$old_file = $this->Anti_sql_injection($this->input->post('old_file', TRUE));
			$jenis_bc = $this->Anti_sql_injection($this->input->post('jenis_bc', TRUE));
			$nopendaftaran = $this->Anti_sql_injection($this->input->post('nopendaftaran', TRUE));
			$no_do = $this->Anti_sql_injection($this->input->post('no_do', TRUE));
			$old_no_pengajuan = ucwords($this->Anti_sql_injection($this->input->post('old_no_pengajuan', TRUE)));
			//$nopengajuan = ucwords($this->Anti_sql_injection($this->input->post('nopengajuan', TRUE)));
			$tglinput = $this->Anti_sql_injection($this->input->post('tglpengajuan', TRUE));
			$temptgl = explode("/", $tglinput);
			$tglpengajuan = date('Y-m-d', strtotime($temptgl[2].'-'.$temptgl[1].'-'.$temptgl[0]));
			$type_text = ucwords($this->Anti_sql_injection($this->input->post('type_text', TRUE)));

			$upload_error = NULL;
			$file_bc = NULL;
			// if ($_FILES['file_bc']['name']) {
				// $this->load->library('upload');
				// $new_filename =
					// preg_replace('/\s/', '', $type_text) .' '.
					// $nopendaftaran .' '.
					// date('d-m-Y', strtotime($temptgl[2].'-'.$temptgl[1].'-'.$temptgl[0])).
					// '.'.pathinfo($_FILES['file_bc']['name'], PATHINFO_EXTENSION);

				// $config = array(
					// 'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "uploads/bc",
					// 'upload_url' => base_url() . "uploads/bc",
					// 'encrypt_name' => FALSE,
					// 'max_filename' => 100,
					// 'file_name' => $new_filename,
					// 'overwrite' => FALSE,
					// 'allowed_types' => 'pdf|txt|doc|docx',
					// 'max_size' => '10000'
				// );
				// $this->upload->initialize($config);

				// if ($this->upload->do_upload("file_bc")) {
					// // General result data
					// $result = $this->upload->data();

					// // Add our stuff
					// $file_bc = 'uploads/bc/'.$result['file_name'];
				// }else {
					// $pesan = $this->upload->display_errors();
					// $upload_error = strip_tags(str_replace("\n", '', $pesan));

					// $result = array(
						// 'success' => false,
						// 'message' => $upload_error
					// );
				// }
			// }

			// if (!isset($upload_error)) {
				// if($file_bc == '' || $file_bc == NULL) {
					// $file_bc = 'uploads/bc/'.$old_file;
				// }else {
					// $filepath = dirname($_SERVER["SCRIPT_FILENAME"]) . "uploads/bc/" . $old_file;
					// if (is_file($filepath)) {
						// unlink($filepath);
					// }
				// }
			$data = array(
				'bc_id' => $bc_id,
				'jenis_bc' => $jenis_bc,
				'no_pendaftaran' => $nopendaftaran,
				//'no_pengajuan' => $nopengajuan,
				'tanggal_pengajuan' => $tglpengajuan,
				'file_loc' => $file_bc
			);
			
			if($type_text != '' || $type_text != null) $this->sequence->get_no($type_text);
			
			if($data != '') {
				$result = $this->bc_po_quotation_model->save_edit_bc($data);
			
				$data_bc_po = array(
					'id_bc' => $bc_id,
					'id_do' => $no_do
				);
				
				$result_bc_po =  $this->bc_po_quotation_model->save_edit_detail_bc($data_bc_po);
				
				$status = 1;
			}else{
				$status = 0;
			}
			
			if ($status == 1) {
				$this->log_activity->insert_activity('insert', 'Berhasil merubah data BC Keluar dengan No Pendaftaran : '.$nopendaftaran);
				$result = array('success' => true, 'message' => 'Berhasil merubah data BC Keluar dengan No Pendaftaran : '.$nopendaftaran);
			} 
			else {
				$this->log_activity->insert_activity('insert', 'Gagal merubah data BC Keluar dengan No Pendaftaran : '.$nopendaftaran);
				$result = array('success' => false, 'message' => 'Gagal merubah data BC Keluar dengan No Pendaftaran : '.$nopendaftaran);
				
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function delete_bc() 
	{
		$data 	= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		$result = $this->bc_po_quotation_model->edit_bc($params['id']);

		//Delete File if exists
		$filepath = dirname($_SERVER["SCRIPT_FILENAME"]) ."/". $result[0]['file_loc'];
		if (is_file($filepath)) {
			unlink($filepath);
		}
		
		$result 		= $this->bc_po_quotation_model->delete_bc($params['id']);
		$resulttbcpo 	= $this->bc_po_quotation_model->delete_t_bc_po($params['id']);

		if($result > 0 && $resulttbcpo > 0) {
			$this->log_activity->insert_activity('insert', 'Berhasil hapus data BC Keluar PO Quotation id : '.$params['id']);
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}else {
			$this->log_activity->insert_activity('insert', 'Gagal hapus data BC Keluar PO Quotation id : '.$params['id']);
			$res = array('status' => 'failed', 'message' => 'Data telah di hapus');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function detail_bc($id) 
	{	
		$bc 			= $this->bc_po_quotation_model->edit_bc($id);
		$type_bc 		= $this->bc_po_quotation_model->type_bc();
		$result_do 		= $this->bc_po_quotation_model->list_delivery_order_edit();
		$ref_dok		= $this->bc_po_quotation_model->get_ref_dokumen();
		$ref_kppbc		= $this->bc_po_quotation_model->get_ref_kantor_pabean();
		$ref_satuan		= $this->bc_po_quotation_model->get_ref_satuan();
		$exportList		= array();
		$data_key 		= array();
		
		/*$data = array(
			'bc_id' => $id,
			'bc' 	=> $bc
		);*/
	
		if($bc[0]) {
			foreach ($type_bc as $tbk => $tbv) {
				if($tbv['id'] == $bc[0]['jenis_bc']) {
					$bc[0]['jenis_bc'] = $tbv['jenis_bc'];
					$bc[0]['desc_bc'] = trim($tbv['desc_bc']);
				}
			}
		}
		
		$jenis_bc = preg_replace('/\D/', '', $bc[0]['jenis_bc']);
		
		switch ($jenis_bc) {
			case 30:
				$exportList = array('Header', 'BahanBaku', 'BahanBakuTarif', 'BahanBakuDokumen', 'Barang', 'BarangTarif', 'BarangDokumen', 'Dokumen', 'Kemasan', 'Kontainer', 'Respon', 'Status', 'Billing', 'Pungutan');
				break;
			case 25:
				$exportList = array('Header', 'BahanBaku', 'BahanBakuTarif', 'BahanBakuDokumen', 'Barang', 'BarangTarif', 'BarangDokumen', 'Dokumen', 'Kemasan', 'Kontainer', 'Respon', 'Status', 'Billing', 'Pungutan');
				break;
			case 27:
				$exportList = array('Header', 'BahanBaku', 'BahanBakuTarif', 'BahanBakuDokumen', 'Barang', 'BarangTarif', 'BarangDokumen', 'Dokumen', 'Kemasan', 'Kontainer', 'Respon', 'Status');
				break;
			case 41:
				$exportList = array('Header', 'BahanBaku', 'BahanBakuTarif', 'BahanBakuDokumen', 'Barang', 'BarangTarif', 'BarangDokumen', 'Dokumen', 'Kemasan', 'Kontainer', 'Respon', 'Status');
				break;
		}

		$data = array(
			'bc' 			=> $bc,
			'ref_dok'		=> $ref_dok,
			'ref_kppbc'		=> $ref_kppbc,
			'ref_satuan'	=> $ref_satuan
		);
		
		$no_aju = preg_replace('/\D/', '', $bc[0]['no_pengajuan']);
		
		for ($is = 0; $is < sizeof($exportList); $is++) {
			$data_key = array();
			$tempTitle  = strtolower($exportList[$is]);

			switch ($exportList[$is]) {
				case 'Header':
					$data[$tempTitle] = $this->bc_po_quotation_model->get_report_header($no_aju);
					break;

				case 'BahanBaku':
					$data[$tempTitle] = $this->bc_po_quotation_model->get_report_bahanbaku($no_aju);
					break;

				case 'BahanBakuTarif':
					$data[$tempTitle] = $this->bc_po_quotation_model->get_report_bahanbakutarif($no_aju);
					break;

				case 'BahanBakuDokumen':
					$data[$tempTitle] = $this->bc_po_quotation_model->get_report_bahanbakudokumen($no_aju);
					break;

				case 'Barang':
					$data[$tempTitle] = $this->bc_po_quotation_model->get_report_barang($no_aju);
					break;

				case 'BarangTarif':
					$data[$tempTitle] = $this->bc_po_quotation_model->get_report_barangtarif($no_aju);
					break;

				case 'BarangDokumen':
					$data[$tempTitle] = $this->bc_po_quotation_model->get_report_barangdokumen($no_aju);
					break;

				case 'Dokumen':
					$data[$tempTitle] = $this->bc_po_quotation_model->get_report_dokumen($no_aju);
					break;

				case 'Kemasan':
					$data[$tempTitle] = $this->bc_po_quotation_model->get_report_kemasan($no_aju);
					break;

				case 'Kontainer':
					$data[$tempTitle] = $this->bc_po_quotation_model->get_report_kontainer($no_aju);
					break;

				case 'Respon':
					$data[$tempTitle] = $this->bc_po_quotation_model->get_report_respons($no_aju);
					break;

				case 'Status':
					$data[$tempTitle] = $this->bc_po_quotation_model->get_report_status($no_aju);
					break;

				case 'Billing':
					$data[$tempTitle] = $this->bc_po_quotation_model->get_report_Billing($no_aju);
					break;

				case 'Pungutan':
					$data[$tempTitle] = $this->bc_po_quotation_model->get_report_pungutan($no_aju);
					break;
			}
		}
		
		if($jenis_bc == 25) $fileView = 'detail_bc_25_view';
		else if($jenis_bc == 27) $fileView = 'detail_bc_27_view';
		else if($jenis_bc == 30) $fileView = 'detail_bc_30_view';
		else if($jenis_bc == 41) $fileView = 'detail_bc_41_view';
		else $fileView = 'detail_bc_not_found_view';

		//echo "<pre>";print_r($data);
		
		$this->load->view($fileView, $data);
	}

	public function list_detail_bc() {
		$id_bc = ($this->input->get_post('id_bc') != FALSE) ? $this->input->get_post('id_bc') : 0;
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 2;
		$order_fields = array('', 'b.id_bc_po_quot', 'c.issue_date', 'c.order_date', 'c.order_no', 'c.price_term', 'c.approval_date', 'c.req_no', 'd.kode_eksternal', 'd.name_eksternal');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['id_bc'] = (int) $id_bc;
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->bc_po_quotation_model->detail_bc($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			if($v['id_bc_po_quot'] != '' || $v['id_bc_po_quot'] != NULL) {
				$status_akses =
					/*
					'<div class="btn-group">'.
						'<button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
							onClick="edit_detail_bc(\'' . $v['id'] . '\')">'.
							'<i class="fa fa-edit"></i>'.
						'</button>'.
					'</div>'.
					*/
					'<div class="btn-group">'.
						'<button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete"
							onClick="delete_detail_bc(\'' . $v['id_bc_po_quot'] . '\')">'.
							'<i class="fa fa-trash"></i>'.
						'</button>'.
					'</div>';
	
				array_push($data, array(
					$i,
					$v['order_no'],
					date('d-m-Y', strtotime($v['order_date'])),
					ucwords($v['name_eksternal']),
					$v['price_term'],
					ucfirst($v['notes']),
					$status_akses
				));
			}
		}

		$result['data'] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_detail_bc() {
		$this->load->view('add_modal_d_bc_view');
	}

	public function get_PoQuotation() {
		$result_po = $this->bc_po_quotation_model->get_PoQuotation();

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result_po);
	}

	public function check_nopengajuan(){
		$jenis_bc = $this->Anti_sql_injection($this->input->post('jenis_bc', TRUE));
		
		$tempJenis 	= 'bc_keluar_'.$jenis_bc;
		$no_aju 	= $this->sequence->get_max_no($tempJenis);
		
		$no_pengajuan = $this->codeData['codeBC'].$jenis_bc.$this->codeData['codeCelebit'].$this->codeData['codeTgl'].$no_aju;

		if($no_pengajuan != '') {
			$res = array('success' => true, 'message' => 'No Pengajuan ditemukan', 'data' => $no_pengajuan, 'type_data' => $jenis_bc);
		}else {
			$res = array('status' => false, 'message' => 'No Pengajuan tidak ditemukan', 'data' => 'No Pengajuan tidak ditemukan', 'type_data' => $jenis_bc);
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	public function check_kd_brg_bc(){
		$this->form_validation->set_rules('kd_brg_bc', 'KodeBarangBC', 'trim|required|min_length[4]|max_length[20]|is_unique[d_bc_list.kode_barang_bc]');
		$this->form_validation->set_message('is_unique', 'Kode barang BC Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'Kode Barang BC Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}
	
	public function check_kd_brg(){
		$this->form_validation->set_rules('kd_brg', 'KodeBarang', 'trim|required|min_length[4]|max_length[20]|is_unique[d_bc_list.kode_barang]');
		$this->form_validation->set_message('is_unique', 'Kode barang Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'Kode Barang Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}
	
	public function save_detail() {
		$this->form_validation->set_rules('id_po_quotation', 'PO Quotation', 'trim|required');
		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$bc_id = $this->Anti_sql_injection($this->input->post('bc_id', TRUE));
			$id_po_quotation = $this->Anti_sql_injection($this->input->post('id_po_quotation', TRUE));

			$data = array(
				'id_bc' 			=> $bc_id,
				'id_po_quotation' 	=> $id_po_quotation
			);
			
			$result = $this->bc_po_quotation_model->add_detail_bc($data);
			
			if($result > 0) {
				$this->log_activity->insert_activity('insert', 'Insert Detail BC Keluar PO Quotation id : '.$bc_id);
				$result = array('success' => true, 'message' => 'Berhasil menambahkan detail bea cukai ke database');
			}else{
				$this->log_activity->insert_activity('insert', 'Gagal Insert Detail BC Keluar PO Quotation id : '.$bc_id);
				$result = array('success' => false, 'message' => 'Gagal menambahkan detail bea cukai ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function edit_detail_bc($id) {
		$result = $this->bc_po_quotation_model->edit_detail_bc($id);
		$result_uom = $this->bc_po_quotation_model->search_uom();
		$result_valas = $this->bc_po_quotation_model->search_valas();
		//$result_stock = $this->bc_po_quotation_model->stock();
		
		$data = array(
			'detail' => $result,
			'uom' => $result_uom,
			'valas' => $result_valas
			//'stock' => $result_stock
		);
		// print_r($data);die;

		$this->load->view('edit_modal_d_bc_view', $data);
	}

	public function save_edit_detail() {
		$this->form_validation->set_rules('kd_brg_bc', 'KodeBarangBC', 'trim|required|min_length[4]|max_length[20]',array('is_unique' => 'This %s Kode Barang BC already exists.'));
		$this->form_validation->set_rules('kd_brg', 'KodeBarang', 'trim|required|min_length[4]|max_length[20]',array('is_unique' => 'This %s Kode Barang already exists.'));
		//$this->form_validation->set_rules('stock_id', 'Stock', 'trim|required');
		//var_dump($this->form_validation->run());die;
		
		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$dbc_id = $this->Anti_sql_injection($this->input->post('dbc_id', TRUE));
			$bc_id = $this->Anti_sql_injection($this->input->post('bc_id', TRUE));
			$kd_brg_bc = strtoupper($this->Anti_sql_injection($this->input->post('kd_brg_bc', TRUE)));
			$kd_brg = strtoupper($this->Anti_sql_injection($this->input->post('kd_brg', TRUE)));
			$uom_id = $this->Anti_sql_injection($this->input->post('uom_id', TRUE));
			$valas_id = $this->Anti_sql_injection($this->input->post('valas_id', TRUE));
			$price = $this->Anti_sql_injection($this->input->post('price', TRUE));
			$weight = $this->Anti_sql_injection($this->input->post('weight', TRUE));
			$qty = $this->Anti_sql_injection($this->input->post('qty', TRUE));

			$data = array(
				'id' 				=> $dbc_id,
				'id_bc' 			=> $bc_id,
				'kode_barang_bc' 	=> $kd_brg_bc,
				'kode_barang' 		=> $kd_brg,
				'uom'		 		=> $uom_id,
				'valas' 			=> $valas_id,
				'price' 			=> $price,
				'weight' 			=> $weight,
				'qty' 				=> $qty
			);

			$result = $this->bc_po_quotation_model->save_edit_detail($data);

			if ($result > 0) {
				$this->log_activity->insert_activity('update', 'Update Detail BC Keluar id : '.$dbc_id);
				$result = array('success' => true, 'message' => 'Berhasil mengubah detail bea cukai');
			}else {
				$this->log_activity->insert_activity('update', 'Gagal Update Detail BC Keluar id : '.$dbc_id);
				$result = array('success' => false, 'message' => 'Gagal mengubah detail bea cukai ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function delete_detail_bc() {
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		$result 	= $this->bc_po_quotation_model->delete_detail_bc_po_quotation($params['id']);

		if($result > 0) {
			$this->log_activity->insert_activity('delete', 'Delete Detail BC Keluar PO Quotation id : '.$params['id']);
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}else {
			$this->log_activity->insert_activity('delete', 'Gagal Delete Detail BC Keluar PO Quotation id : '.$params['id']);
			$res = array('status' => 'failed', 'message' => 'Data gagal di hapus');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	public function download_bc($id_bc) {
		$data_bc 		= $this->bc_po_quotation_model->edit_bc($id_bc)[0];
		$exportList		= array();
		$data 			= array();
		$data_key 		= array();

		$no_aju = preg_replace('/\D/', '', $data_bc['no_pengajuan']);
		
		switch (preg_replace('/\D/', '', $data_bc['type_jenis_bc'])) {
			case 30:
				$exportList = array('Header', 'BahanBaku', 'BahanBakuTarif', 'BahanBakuDokumen', 'Barang', 'BarangTarif', 'BarangDokumen', 'Dokumen', 'Kemasan', 'Kontainer', 'Respon', 'Status', 'Billing', 'Pungutan');
				break;
			case 25:
				$exportList = array('Header', 'BahanBaku', 'BahanBakuTarif', 'BahanBakuDokumen', 'Barang', 'BarangTarif', 'BarangDokumen', 'Dokumen', 'Kemasan', 'Kontainer', 'Respon', 'Status', 'Billing', 'Pungutan');
				break;
			case 27:
				$exportList = array('Header', 'BahanBaku', 'BahanBakuTarif', 'BahanBakuDokumen', 'Barang', 'BarangTarif', 'BarangDokumen', 'Dokumen', 'Kemasan', 'Kontainer', 'Respon', 'Status');
				break;
			case 41:
				$exportList = array('Header', 'BahanBaku', 'BahanBakuTarif', 'BahanBakuDokumen', 'Barang', 'BarangTarif', 'BarangDokumen', 'Dokumen', 'Kemasan', 'Kontainer', 'Respon', 'Status');
				break;
		}

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("System")
			->setLastModifiedBy("System")
			->setTitle($data_bc['type_bc'])
			->setSubject("Laporan Bukti Keluar Barang")
			->setDescription($data_bc['desc_bc'])
			->setKeywords("Bukti Keluar Barang")
			->setCategory("Report");

		for ($is = 0; $is < sizeof($exportList); $is++) {
			$data_key = array();

			switch ($exportList[$is]) {
				case 'Header':
					$data = $this->bc_po_quotation_model->get_report_header($no_aju);
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_header']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}					
					break;

				case 'BahanBaku':
					$data = $this->bc_po_quotation_model->get_report_bahanbaku($no_aju);
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_bahanbaku']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;

				case 'BahanBakuTarif':
					$data = $this->bc_po_quotation_model->get_report_bahanbakutarif($no_aju);
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_bahanbakutarif']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;

				case 'BahanBakuDokumen':
					$data = $this->bc_po_quotation_model->get_report_bahanbakudokumen($no_aju);
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_bahanbakudokumen']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;

				case 'Barang':
					$data = $this->bc_po_quotation_model->get_report_barang($no_aju);
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_barang']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;

				case 'BarangTarif':
					$data = $this->bc_po_quotation_model->get_report_barangtarif($no_aju);
					if(sizeof($data) > 0){
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_barangtarif']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;

				case 'BarangDokumen':
					$data = $this->bc_po_quotation_model->get_report_barangdokumen($no_aju);
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_barangdokumen']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}

					$objPHPExcel->createSheet($is);
					break;

				case 'Dokumen':
					$data = $this->bc_po_quotation_model->get_report_dokumen($no_aju);
					if(sizeof($data)) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_dokumen']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}

					$objPHPExcel->createSheet($is);
					break;

				case 'Kemasan':
					$data = $this->bc_po_quotation_model->get_report_kemasan($no_aju);
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_kemasan']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;

				case 'Kontainer':
					$data = $this->bc_po_quotation_model->get_report_kontainer($no_aju);
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_kontainer']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;

				case 'Respon':
					$data = $this->bc_po_quotation_model->get_report_respons($no_aju);
					if(sizeof($data)) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_respons']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;

				case 'Status':
					$data = $this->bc_po_quotation_model->get_report_status($no_aju);
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_status']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;

				case 'Billing':
					$data = $this->bc_po_quotation_model->get_report_Billing($no_aju);
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_billing']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;

				case 'Pungutan':
					$data = $this->bc_po_quotation_model->get_report_pungutan($no_aju);
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_pungutan']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;
			}
			$objPHPExcel->setActiveSheetIndex($is)->setTitle($exportList[$is]);
			$objPHPExcel->getActiveSheet()->fromArray($data_key, null, 'A1');
			$no = 2;
			for ($i = 0; $i < sizeof($data); $i++) { 
				$objPHPExcel->getActiveSheet()->fromArray($data[$i], null, 'A'.$no);
				$no++;
			}
		}

		$objPHPExcel->setActiveSheetIndex(0);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$data_bc['type_jenis_bc'].'.xls"');
		
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;
	}
	
}