<style type="text/css">
	.tombol-kembali{
		margin-top: -5px;
	}
	.text-kembali{
		margin-top: -5px;
		margin-left: 50px;
	}
	.header-jenis-bc{
		vertical-align: middle;
		width:	10%;
	}
	.header-deskripsi-bc{
		vertical-align: middle;
		width:	90%;
	}
	.label-header{
		color: #505458;
		font-size: 20px;
		font-weight: bold;
	}
</style>

<div class="card-box" style="height:60px;">
	<div class="row">
		<div class="btn-group pull-left tombol-kembali">
			<a class="btn btn-info btn-sm" id="back_bc" name="back_bc" data-toggle="tooltip" data-placement="top" title="Back">
				<i class="fa fa-arrow-left"></i>
			</a> 
		</div> 
		<h3 class="text-kembali"><label> Kembali </label></h3>
	</div>
</div>
	
<div class="card-box" style="margin-bottom:30px;margin-top:20px;">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-bordered" cellspacing="0" width="100%">
				<tr>
					<td class="text-center header-jenis-bc" colspan="2" style="vertical-align: middle;"><h3><label><?php if(isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']); ?></label></h3></td>
					<td class="text-center header-deskripsi-bc" colspan="10" style="vertical-align: middle;"><h3><label><?php if(isset($bc[0]['desc_bc'])) echo strtoupper($bc[0]['desc_bc']); ?></label></h3></td>
				</tr>
				<tr>
					<td colspan="12" style=""><h4><label>HEADER</label></h4></td>
				</tr>
				<tr class="title-header2">
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">NO PENGAJUAN</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">: <?php if(isset($no_pengajuan)) echo strtoupper($no_pengajuan); ?></td>
					<td class="text-right" colspan="6" style="border-bottom-style: hidden;">halaman ke 1 dari 6</td>
				</tr>
				<tr>
					<td colspan="6" style="border-bottom-style: hidden;border-right-style: hidden;">A. KANTOR PABEAN</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">D. TUJUAN PENGIRIMAN</td>
					<td colspan="3" style="border-bottom-style: hidden;">: DIJUAL</td>
				</tr>
				<tr>
					<td colspan="3" style="margin-left:20px;border-bottom-style: hidden;border-right-style: hidden;">1. Kantor Asal</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">: 
						<?php
							if(isset($header[0]['KPPBC'])) {
								$kppbc = $header[0]['KPPBC'];
								foreach ($ref_kppbc as $rkk => $rkv) {
									if($rkv['KODE_KANTOR'] == $kppbc) echo $rkv['URAIAN_KANTOR'];
								}
							}
						?>
					</td>
					<td colspan="6" style=""></td>
				</tr>
				<tr>
					<td colspan="3" style="margin-left:20px;border-bottom-style: hidden;border-right-style: hidden; width: 13%;">2. Kantor Tujuan</td>
					<td colspan="3" style="border-bottom-style: hidden;">:</td>
					<td colspan="6" style="border-bottom-style: hidden;width: 50%;">G. KOLOM KHUSUS BEA DAN CUKAI</td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">B. JENIS TPB ASAL</td>
					<td colspan="3" style="border-bottom-style: hidden;">:</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden; width: 15%;">Nomor Pendaftaran</td>
					<td colspan="3" style="border-bottom-style: hidden;">: <?php if(isset($bc[0]['no_pendaftaran'])) echo strtoupper($bc[0]['no_pendaftaran']); ?></td>
				</tr>
				<tr>
					<td colspan="3" style="border-right-style: hidden;">C. JENIS TPB TUJUAN</td>
					<td colspan="3" style="border-bottom-style: hidden;">:</td>
					<td colspan="3" style="border-right-style: hidden;">Tanggal</td>
					<td colspan="3" style="">: <?php if(isset($bc[0]['tanggal_pengajuan'])) $date = date_create($bc[0]['tanggal_pengajuan']); echo date_format($date, 'd-m-Y'); ?></td>
				</tr>
				<tr>
					<td colspan="12" style=""><h5><label>E. DATA PEMBERITAHUAN :</label></h5></td>
				</tr>
				<tr>
					<td colspan="6" style="">TPB ASAL BARANG</td>
					<td colspan="6" style="">TPB TUJUAN BARANG</td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">1. NPWP</td>
					<td colspan="3" style="border-bottom-style: hidden;">:</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">5. NPWP</td>
					<td colspan="3" style="border-bottom-style: hidden;">:</td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">2. Nama</td>
					<td colspan="3" style="border-bottom-style: hidden;">:</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">6. Nama</td>
					<td colspan="3" style="border-bottom-style: hidden;">:</td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">3. Alamat</td>
					<td colspan="3" style="border-bottom-style: hidden;">:</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">7. Alamat</td>
					<td colspan="3" style="border-bottom-style: hidden;">:</td>
				</tr>
				<tr>
					<td colspan="3" style="border-right-style: hidden;">4. No Izin TPB</td>
					<td colspan="3" style="border-bottom-style: hidden;">:</td>
					<td colspan="3" style="border-right-style: hidden;">8. No Izin TPB</td>
					<td colspan="3" style="border-bottom-style: hidden;">:</td>
				</tr>
				<tr>
					<td colspan="12" style=""><h5><label>DOKUMEN PELENGKAP PABEAN</label></h5></td>
				</tr>
				<tr>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">9. Invoice</td>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">tgl</td>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">12. Surat Jalan</td>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="2" style="border-bottom-style: hidden;">tgl</td>
				</tr>
				<tr>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">10. Packing List</td>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">tgl</td>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">13. Surat Keputusan/Persetujuan</td>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="2" style="border-bottom-style: hidden;">tgl</td>
				</tr>
				<tr>
					<td colspan="2" style="border-right-style: hidden;">11. Kontrak</td>
					<td colspan="2" style="border-right-style: hidden;">:</td>
					<td colspan="2" style="border-right-style: hidden;">tgl</td>
					<td colspan="2" style="border-right-style: hidden;">14. Lainnya</td>
					<td colspan="2" style="border-right-style: hidden;">:</td>
					<td colspan="2" style="border-bottom-style: hidden;">tgl</td>
				</tr>
				<tr>
					<td colspan="12" style=""><h5><label>RIWAYAT BARANG</label></h5></td>
				</tr>
				<tr>
					<td colspan="4" style="border-bottom-style: hidden;border-right-style: hidden;">15. a. Nomor dan tanggal BC 2.7 Asal</td>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="6" style="border-bottom-style: hidden;">tgl</td>
				</tr>
				<tr>
					<td colspan="4" style="border-bottom-style: hidden;border-right-style: hidden;">b. Nomor dan tanggal BC 2.3</td>
					<td colspan="2" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="6" style="border-bottom-style: hidden;">tgl</td>
				</tr>
				<tr>
					<td colspan="12" style=""><h5><label>DATA PERDAGANGAN</label></h5></td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">16. Jenis Valuta Asing</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="6" rowspan="2" style="">18. Harga Penyerahan</td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">17. CIF</td>
					<td colspan="3" style="border-right-style: hidden;">:</td>
				</tr>
				<tr>
					<td colspan="8" style=""><h5><label>DATA PENGANGKUTAN</label></h5></td>
					<td class="text-center" colspan="4" style=""><h5><label>SEGEL (DIISI OLEH BEA DAN CUKAI)</label></h5></td>
				</tr>
				<tr>
					<td class="text-center" colspan="4" style="border-bottom-style: hidden;border-right-style: hidden;">9. Jenis Sarana Pengangkut Darat</td>
					<td class="text-center" colspan="4" style="border-bottom-style: hidden;">20. No Polisi</td>
					<td class="text-center"colspan="2" style="">BC Asal</td>
					<td class="text-center"colspan="2" rowspan="2" style="vertical-align:middle">23. Catatan BC Tujuan</td>
				</tr>
				<tr>
					<td colspan="4" style="border-right-style: hidden;">MOBIL BOX</td>
					<td colspan="4" style="">-</td>
					<td class="text-center" colspan="1" style="">21. No Segel</td>
					<td class="text-center" colspan="1" style="">22. Jenis</td>
				</tr>
				<tr>
					<td colspan="8" style=""><h5><label>DATA PETI KEMAS DAN PENGEMAS</label></h5></td>
					<td colspan="1" rowspan="3" style=""></td>
					<td colspan="1" rowspan="3" style=""></td>
					<td colspan="2" rowspan="3" style=""></td>
				</tr>
				<tr>
					<td colspan="4" style="border-bottom-style: hidden;border-right-style: hidden;">24. Merk dan No Kemasan/Peti Kemas dan Jumlah</td>
					<td colspan="4" style="border-bottom-style: hidden;">25. Jumlah dan Jenis Kemasan</td>
				</tr>
				<tr>
					<td colspan="4" style="border-right-style: hidden;"></td>
					<td colspan="4" style="">7 Carton, Celebit</td>
				</tr>
				<tr>
					<td colspan="4" style="border-right-style: hidden;">26. Volume (m3)</td>
					<td colspan="4" style="border-right-style: hidden;">27. Berat Kotor (Kg)</td>
					<td colspan="4" style="">28. Berat Bersih (Kg)</td>
				</tr>
				<tr>
					<td colspan="1" style="border-bottom-style: hidden;">29.</td>
					<td colspan="4" style="">30.</td>
					<td colspan="4" style="">31.</td>
					<td colspan="3" style="">32.</td>
				</tr>
				<tr>
					<td colspan="1" rowspan="3" style="">No</td>
					<td colspan="4" rowspan="1" style="border-bottom-style: hidden;">Pos Tarif/HS, uraian jumlah dan jenis barang secara lengkap,</td>
					<td colspan="4" rowspan="1" style="border-bottom-style: hidden;">- Jumlah & Jenis Satuan</td>
					<td colspan="3" rowspan="1" style="border-bottom-style: hidden;">- Nilai CIF</td>
				</tr>
				<tr>
					<td colspan="4" rowspan="2" style="">kode barang, merk, tipe, ukuran dan spesifikasi lain</td>
					<td colspan="4" rowspan="1" style="border-bottom-style: hidden;">- Berat Bersih (Kg)</td>
					<td colspan="2" rowspan="2" style="">- Harga Penyerahan</td>
				</tr>
				<tr>
					<td colspan="4" rowspan="1" style="">- Volume (m3)</td>
				</tr>
				<tr>
					<td class="text-center" colspan="12" style="height:100px;vertical-align:middle">................................................... 2. Jenis Barang Lihat Lembar Lanjutan ...................................................</td>
				</tr>
				<tr>
					<td colspan="6" style=""><label>F. TANDA TANGAN PENGUSAHA TPB</label></td>
					<td colspan="6" style=""><label>H. UNTUK PEJABAT BEA DAN CUKAI</label></td>
				</tr>
				<tr>
					<td colspan="6" style="border-bottom-style: hidden;">Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal</td>
					<td class="text-center" colspan="3" style="">Kantor Pabean Asal</td>
					<td class="text-center" colspan="3" style="">Kantor Pabean Tujuan</td>
				</tr>
				<tr>
					<td colspan="6" style="">yang diberitahukan dalam pemberitahuan pabean ini.</td>
					<td colspan="3" rowspan="" style="border-bottom-style:hidden;"></td>
					<td colspan="3" rowspan="" style="border-bottom-style:hidden;"></td>
				</tr>
				<tr>
					<td class="text-center" colspan="6" rowspan="" style="">BANDUNG, <?php echo date('d F Y'); ?></td>
					<td colspan="3" style="border-bottom-style:hidden;"></td>
					<td colspan="3" style="border-bottom-style:hidden;"></td>
				</tr>
				<tr>
					<td class="text-center" colspan="6" rowspan="" style="border-bottom-style:hidden;"></td>
					<td colspan="3" style="border-bottom-style:hidden;"></td>
					<td colspan="3" style="border-bottom-style:hidden;"></td>
				</tr>
				<tr>
					<td class="text-center" colspan="6" rowspan="" style="border-bottom-style:hidden;"></td>
					<td colspan="3" style="border-bottom-style:hidden;"></td>
					<td colspan="3" style="border-bottom-style:hidden;"></td>
				</tr>
				<tr>
					<td class="text-center" colspan="6" rowspan="" style="border-bottom-style:hidden;"></td>
					<td colspan="3" style="border-bottom-style:hidden;">Nama :</td>
					<td colspan="3" style="border-bottom-style:hidden;">Nama :</td>
				</tr>
				<tr>
					<td class="text-center" colspan="6" rowspan="" style="">NAMA PENGUSAHA TPB</td>
					<td colspan="3" style="">NIP :</td>
					<td colspan="3" style="">NIP :</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<!-- DATA BARANG -->
<div class="card-box" style="margin-bottom:30px;margin-top:20px;">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-bordered" cellspacing="0" width="100%">
				<tr>
					<td class="text-center header-jenis-bc" colspan="2" style=""><h3><label><?php if(isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']); ?></label></h3></td>
					<td class="text-center header-deskripsi-bc" colspan="10" style=""><h3><label>LEMBAR LANJUTAN DATA BARANG</label></h3></td>
				</tr>
				<tr>
					<td colspan="12" style=""><h4><label>HEADER</label></h4></td>
				</tr>
				<tr class="title-header2">
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">NO PENGAJUAN</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">: <?php if(isset($no_pengajuan)) echo strtoupper($no_pengajuan); ?></td>
					<td class="text-right" colspan="6" style="border-bottom-style: hidden;">halaman ke 2 dari 6</td>
				</tr>
				<tr>
					<td colspan="6" style="border-bottom-style: hidden;border-right-style: hidden;">A. KANTOR PABEAN</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">D. TUJUAN PENGIRIMAN</td>
					<td colspan="3" style="border-bottom-style: hidden;">: DIJUAL</td>
				</tr>
				<tr>
					<td colspan="3" style="margin-left:20px;border-bottom-style: hidden;border-right-style: hidden;">1. Kantor Asal</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="6" style=""></td>
				</tr>
				<tr>
					<td colspan="3" style="margin-left:20px;border-bottom-style: hidden;border-right-style: hidden;">2. Kantor Tujuan</td>
					<td colspan="3" style="border-bottom-style: hidden;">:</td>
					<td colspan="6" style="border-bottom-style: hidden;">G. KOLOM KHUSUS BEA DAN CUKAI</td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">B. JENIS TPB ASAL</td>
					<td colspan="3" style="border-bottom-style: hidden;">:</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">Nomor Pendaftaran</td>
					<td colspan="3" style="border-bottom-style: hidden;">: <?php if(isset($bc[0]['no_pendaftaran'])) echo strtoupper($bc[0]['no_pendaftaran']); ?></td>
				</tr>
				<tr>
					<td colspan="3" style="border-right-style: hidden;">C. JENIS TPB TUJUAN</td>
					<td colspan="3" style="">:</td>
					<td colspan="3" style="border-right-style: hidden;">Tanggal</td>
					<td colspan="3" style="">: <?php if(isset($bc[0]['tanggal_pengajuan'])) $date = date_create($bc[0]['tanggal_pengajuan']); echo date_format($date, 'd-m-Y'); ?></td>
				</tr>
				<tr>
					<td colspan="1" style="">29.</td>
					<td colspan="4" style="">30.</td>
					<td colspan="4" style="">31.</td>
					<td colspan="3" style="">32.</td>
				</tr>
				<tr>
					<td colspan="1" rowspan="3" style="">No</td>
					<td colspan="4" rowspan="1" style="border-bottom-style: hidden;">Pos Tarif/HS, uraian jumlah dan jenis barang secara lengkap,</td>
					<td colspan="4" rowspan="1" style="border-bottom-style: hidden;">- Jumlah & Jenis Satuan</td>
					<td colspan="3" rowspan="1" style="border-bottom-style: hidden;">- Nilai CIF</td>
				</tr>
				<tr>
					<td colspan="4" rowspan="2" style="">kode barang, merk, tipe, ukuran dan spesifikasi lain</td>
					<td colspan="4" rowspan="1" style="border-bottom-style: hidden;">- Berat Bersih (Kg)</td>
					<td colspan="2" rowspan="2" style="">- Harga Penyerahan</td>
				</tr>
				<tr>
					<td colspan="4" rowspan="1" style="">- Volume (m3)</td>
				</tr>
				<tr>
					<td colspan="1" rowspan="5" style="">1</td>
					<td colspan="4" style="border-bottom-style: hidden;">- Pos Tarif/HS : 85340010</td>
					<td colspan="4" style="border-bottom-style: hidden;">- 1,200,0000</td>
					<td colspan="3" style="border-bottom-style: hidden;">- 675,00</td>
				</tr>
				<tr>
					<td colspan="4" style="border-bottom-style: hidden;">- Kode Barang : </td>
					<td colspan="4" style="border-bottom-style: hidden;">PIECE (PCE)</td>
					<td colspan="3" style="border-bottom-style: hidden;">- Rp. 9.485.100,00</td>
				</tr>
				<tr>
					<td colspan="4" style="border-bottom-style: hidden;">- PRINTED CIRCUIT BOARD : Merk : CELEBIT Type : DM-500-01 </td>
					<td colspan="4" style="border-bottom-style: hidden;">- 45,1600</td>
					<td colspan="3" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="4" rowspan="" style="border-bottom-style: hidden;">(B251547-101), Ukuran : , Spesifikasi lain : </td>
					<td colspan="4" rowspan="" style="border-bottom-style: hidden;">-</td>
					<td colspan="3" rowspan="" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="4" style=""></td>
					<td colspan="4" style=""></td>
					<td colspan="3" style=""></td>
				</tr>
				<tr>
					<td colspan="1" rowspan="5" style="">2</td>
					<td colspan="4" style="border-bottom-style: hidden;">- Pos Tarif/HS : 85340010</td>
					<td colspan="4" style="border-bottom-style: hidden;">- 900,0000</td>
					<td colspan="3" style="border-bottom-style: hidden;">- 176, 94</td>
				</tr>
				<tr>
					<td colspan="4" style="border-bottom-style: hidden;">- Kode Barang : </td>
					<td colspan="4" style="border-bottom-style: hidden;">PIECE (PCE)</td>
					<td colspan="3" style="border-bottom-style: hidden;">Rp. 2.486.360,88</td>
				</tr>
				<tr>
					<td colspan="4" style="border-bottom-style: hidden;">- PRINTED CIRCUIT BOARD : Merk : CELEBIT Type : DM-500-01 </td>
					<td colspan="4" style="border-bottom-style: hidden;">- 19,6100</td>
					<td colspan="3" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="4" style="border-bottom-style: hidden;">(B251334-101), Ukuran : , Spesifikasi lain : </td>
					<td colspan="4" style="border-bottom-style: hidden;">-</td>
					<td colspan="3" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="4" style=""></td>
					<td colspan="4" style=""></td>
					<td colspan="3" style=""></td>
				</tr>
				<tr>
					<td colspan="6" style=""><label>F. TANDA TANGAN PENGUSAHA TPB</label></td>
					<td colspan="6" style=""><label>H. UNTUK PEJABAT BEA DAN CUKAI</label></td>
				</tr>
				<tr>
					<td colspan="6" style="border-bottom-style: hidden;">Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal</td>
					<td class="text-center" colspan="3" style="">Kantor Pabean Asal</td>
					<td class="text-center" colspan="3" style="">Kantor Pabean Tujuan</td>
				</tr>
				<tr>
					<td colspan="6" style="">yang diberitahukan dalam pemberitahuan pabean ini.</td>
					<td colspan="3" rowspan="" style="border-bottom-style:hidden;"></td>
					<td colspan="3" rowspan="" style="border-bottom-style:hidden;"></td>
				</tr>
				<tr>
					<td class="text-center" colspan="6" rowspan="" style="">BANDUNG, <?php echo date('d F Y'); ?></td>
					<td colspan="3" style="border-bottom-style:hidden;"></td>
					<td colspan="3" style="border-bottom-style:hidden;"></td>
				</tr>
				<tr>
					<td class="text-center" colspan="6" rowspan="" style="border-bottom-style:hidden;"></td>
					<td colspan="3" style="border-bottom-style:hidden;"></td>
					<td colspan="3" style="border-bottom-style:hidden;"></td>
				</tr>
				<tr>
					<td class="text-center" colspan="6" rowspan="" style="border-bottom-style:hidden;"></td>
					<td colspan="3" style="border-bottom-style:hidden;">Nama :</td>
					<td colspan="3" style="border-bottom-style:hidden;">Nama :</td>
				</tr>
				<tr>
					<td class="text-center" colspan="6" rowspan="" style="">NAMA PENGUSAHA TPB</td>
					<td colspan="3" style="">NIP :</td>
					<td colspan="3" style="">NIP :</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<!-- DOKUMENT PELENGKAP PABEAN -->
<div class="card-box" style="margin-bottom:30px;margin-top:20px;">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-bordered" cellspacing="0" width="100%">
				<tr>
					<td class="text-center header-jenis-bc" colspan="2" style=""><h3><label><?php if(isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']); ?></label></h3></td>
					<td class="text-center header-deskripsi-bc" colspan="10" style=""><h3><label>LEMBAR LANJUTAN DOKUMEN PELENGKAP PABEAN</label></h3></td>
				</tr>
				<tr>
					<td colspan="12" style=""><h4><label>HEADER</label></h4></td>
				</tr>
				<tr class="title-header2">
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">NO PENGAJUAN</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">: <?php if(isset($no_pengajuan)) echo strtoupper($no_pengajuan); ?></td>
					<td class="text-right" colspan="6" style="border-bottom-style: hidden;">halaman ke 3 dari 6</td>
				</tr>
				<tr>
					<td colspan="6" style="border-bottom-style: hidden;border-right-style: hidden;">A. KANTOR PABEAN</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">D. TUJUAN PENGIRIMAN</td>
					<td colspan="3" style="border-bottom-style: hidden;">: DIJUAL</td>
				</tr>
				<tr>
					<td colspan="3" style="margin-left:20px;border-bottom-style: hidden;border-right-style: hidden;">1. Kantor Asal</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="6" style=""></td>
				</tr>
				<tr>
					<td colspan="3" style="margin-left:20px;border-bottom-style: hidden;border-right-style: hidden;">2. Kantor Tujuan</td>
					<td colspan="3" style="border-bottom-style: hidden;">:</td>
					<td colspan="6" style="border-bottom-style: hidden;">G. KOLOM KHUSUS BEA DAN CUKAI</td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">B. JENIS TPB ASAL</td>
					<td colspan="3" style="border-bottom-style: hidden;">:</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">Nomor Pendaftaran</td>
					<td colspan="3" style="border-bottom-style: hidden;">: <?php if(isset($bc[0]['no_pendaftaran'])) echo strtoupper($bc[0]['no_pendaftaran']); ?></td>
				</tr>
				<tr>
					<td colspan="3" style="border-right-style: hidden;">C. JENIS TPB TUJUAN</td>
					<td colspan="3" style="">:</td>
					<td colspan="3" style="border-right-style: hidden;">Tanggal</td>
					<td colspan="3" style="">: <?php if(isset($bc[0]['tanggal_pengajuan'])) $date = date_create($bc[0]['tanggal_pengajuan']); echo date_format($date, 'd-m-Y'); ?></td>
				</tr>
				<tr>
					<td colspan="1" style="">No.</td>
					<td colspan="4" style="">Jenis Dokumen</td>
					<td colspan="4" style="">Nomor</td>
					<td colspan="3" style="">Tanggal</td>
				</tr>
				<tr>
					<td colspan="1" rowspan="" style="border-bottom-style: hidden;">1</td>
					<td colspan="4" rowspan="" style="border-bottom-style: hidden;">INVOICE</td>
					<td colspan="4" rowspan="" style="border-bottom-style: hidden;">090329382</td>
					<td colspan="3" rowspan="" style="border-bottom-style: hidden;">20-11-2016</td>
				</tr>
				<tr>
					<td colspan="1" rowspan="" style="border-bottom-style: hidden;">2</td>
					<td colspan="4" rowspan="" style="border-bottom-style: hidden;">INVOICE</td>
					<td colspan="4" rowspan="" style="border-bottom-style: hidden;">090329382</td>
					<td colspan="3" rowspan="" style="border-bottom-style: hidden;">20-11-2016</td>
				</tr>
				<tr>
					<td colspan="1" rowspan="" style="border-bottom-style: hidden;">3</td>
					<td colspan="4" rowspan="" style="border-bottom-style: hidden;">INVOICE</td>
					<td colspan="4" rowspan="" style="border-bottom-style: hidden;">090329382</td>
					<td colspan="3" rowspan="" style="border-bottom-style: hidden;">20-11-2016</td>
				</tr>
				<tr>
					<td colspan="1" rowspan="" style="">4</td>
					<td colspan="4" rowspan="" style="">INVOICE</td>
					<td colspan="4" rowspan="" style="">090329382</td>
					<td colspan="3" rowspan="" style="">20-11-2016</td>
				</tr>
				<tr>
					<td colspan="6" style=""><label></label></td>
					<td colspan="6" style=""><label>E. TANDA TANGAN PENGUSAHA TPB</label></td>
				</tr>
				<tr>
					<td colspan="6" style="border-bottom-style: hidden;border-right-style: hidden;"></td>
					<td class="text-center" colspan="6" style="border-bottom-style: hidden;">Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal</td>
				</tr>
				<tr>
					<td colspan="6" style="border-bottom-style: hidden;border-right-style: hidden;"></td>
					<td class="text-center" colspan="6" style="border-bottom-style: hidden;">yang diberitahukan dalam pemberitahuan pabean ini.</td>
				</tr>
				<tr>
					<td colspan="6" style="border-bottom-style: hidden;border-right-style: hidden;"></td>
					<td class="text-center" colspan="6" style="border-bottom-style: hidden;">BANDUNG, <?php echo date('d F Y'); ?></td>
				</tr>
				<tr>
					<td colspan="12" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="12" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="12" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="12" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="6" rowspan="" style="border-right-style: hidden;"></td>
					<td class="text-center" colspan="6" rowspan="" style="">NAMA PENGUSAHA TPB</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<!-- KONVERSI PEMAKAIAN BAHAN (DIJUAL) -->
<div class="card-box" style="margin-bottom:30px;margin-top:20px;">
	<div class="row">
		<div class="col-md-12">
			<table class="table table-bordered" cellspacing="0" width="100%">
				<tr>
					<td class="text-center header-jenis-bc" colspan="2" style=""><h3><label><?php if(isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']); ?></label></h3></td>
					<td class="text-center header-deskripsi-bc" colspan="10" style=""><h3><label>LEMBAR LANJUTAN KONVERSI PEMAKAIAN BAHAN (DIJUAL)</label></h3></td>
				</tr>
				<tr>
					<td colspan="12" style=""><h4><label>HEADER</label></h4></td>
				</tr>
				<tr class="title-header2">
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">NO PENGAJUAN</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">: <?php if(isset($no_pengajuan)) echo strtoupper($no_pengajuan); ?></td>
					<td class="text-right" colspan="6" style="border-bottom-style: hidden;">halaman ke 4 dari 6</td>
				</tr>
				<tr>
					<td colspan="6" style="border-bottom-style: hidden;border-right-style: hidden;">A. KANTOR PABEAN</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">D. TUJUAN PENGIRIMAN</td>
					<td colspan="3" style="border-bottom-style: hidden;">: DIJUAL</td>
				</tr>
				<tr>
					<td colspan="3" style="margin-left:20px;border-bottom-style: hidden;border-right-style: hidden;">1. Kantor Asal</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="6" style=""></td>
				</tr>
				<tr>
					<td colspan="3" style="margin-left:20px;border-bottom-style: hidden;border-right-style: hidden;">2. Kantor Tujuan</td>
					<td colspan="3" style="border-bottom-style: hidden;">:</td>
					<td colspan="6" style="border-bottom-style: hidden;">G. KOLOM KHUSUS BEA DAN CUKAI</td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">B. JENIS TPB ASAL</td>
					<td colspan="3" style="border-bottom-style: hidden;">:</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">Nomor Pendaftaran</td>
					<td colspan="3" style="border-bottom-style: hidden;">: <?php if(isset($bc[0]['no_pendaftaran'])) echo strtoupper($bc[0]['no_pendaftaran']); ?></td>
				</tr>
				<tr>
					<td colspan="3" style="border-right-style: hidden;">C. JENIS TPB TUJUAN</td>
					<td colspan="3" style="">:</td>
					<td colspan="3" style="border-right-style: hidden;">Tanggal</td>
					<td colspan="3" style="">: <?php if(isset($bc[0]['tanggal_pengajuan'])) $date = date_create($bc[0]['tanggal_pengajuan']); echo date_format($date, 'd-m-Y'); ?></td>
				</tr>
				<tr>
					<td class="text-center" colspan="6" style=""><h5><label>Barang Jadi</label></h5></td>
					<td class="text-center" colspan="6" style=""><h5><label>Bahan Baku yang Digunakan</label></h5></td>
				</tr>
				<tr>
					<td colspan="1" rowspan="" style="">No.</td>
					<td colspan="3" rowspan="" style="">Pos Tarif/HS, uraian jumlah dan jenis barang secara lengkap, kode barang, merk tipe, ukuran dan spesifikasi lain</td>
					<td colspan="1" rowspan="" style="">Jumlah</td>
					<td colspan="1" rowspan="" style="">Satuan</td>
					<td colspan="3" rowspan="" style="">Pos Tarif/HS, uraian jumlah dan jenis barang secara lengkap, kode barang, merk tipe, ukuran dan spesifikasi lain</td>
					<td colspan="1" rowspan="" style="">Jumlah</td>
					<td colspan="2" rowspan="" style="">Satuan</td>
				</tr>
				<tr>
					<td colspan="1" rowspan="" style="border-bottom-style: hidden;">1</td>
					<td colspan="3" rowspan="" style="border-bottom-style: hidden;">
						Pos Tarif/HS : 85340010
						- Kode Barang : 012NSS-PSS-P-06
						- PRINTED CIRCUIT BOARD, Merk : CELEBIT, Tipe : DM.500-01 (B251547-101), Ukuran dan Spesifikasi lain :
					</td>
					<td colspan="1" rowspan="" style="border-bottom-style: hidden;">1.200,0000</td>
					<td colspan="1" rowspan="" style="border-bottom-style: hidden;">PIECE (PCE)</td>
					<td colspan="3" rowspan="" style="border-bottom-style: hidden;">
						Pos Tarif/HS : 74102110
						- Kode Barang : CCLC36
						- PRINTED CIRCUIT BOARD, Merk : CCP3400, Tipe : CEM-1 (B251547-101), Ukuran dan Spesifikasi lain :
					</td>
					<td colspan="1" rowspan="" style="border-bottom-style: hidden;">1240 x 1080</td>
					<td colspan="2" rowspan="" style="border-bottom-style: hidden;">ST</td>
				</tr>
				<tr>
					<td colspan="1" rowspan="" style="">2</td>
					<td colspan="3" rowspan="" style="">
						Pos Tarif/HS : 85340010
						- Kode Barang : 012NSS-PSS-P-06
						- PRINTED CIRCUIT BOARD, Merk : CELEBIT, Tipe : DM.500-01 (B251547-101), Ukuran dan Spesifikasi lain :
					</td>
					<td colspan="1" rowspan="" style="">1.200,0000</td>
					<td colspan="1" rowspan="" style="">PIECE (PCE)</td>
					<td colspan="3" rowspan="" style="">
						Pos Tarif/HS : 74102110
						- Kode Barang : CCLC36
						- PRINTED CIRCUIT BOARD, Merk : CCP3400, Tipe : CEM-1 (B251547-101), Ukuran dan Spesifikasi lain :
					</td>
					<td colspan="1" rowspan="" style="">1240 x 1080</td>
					<td colspan="2" rowspan="" style="">ST</td>
				</tr>
				<tr>
					<td colspan="6" style=""><label></label></td>
					<td colspan="6" style=""><label>E. TANDA TANGAN PENGUSAHA TPB</label></td>
				</tr>
				<tr>
					<td colspan="6" style="border-bottom-style: hidden;"></td>
					<td class="text-center" colspan="6" style="border-bottom-style: hidden;">Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal</td>
				</tr>
				<tr>
					<td colspan="6" style="border-bottom-style: hidden;"></td>
					<td class="text-center" colspan="6" style="border-bottom-style: hidden;">yang diberitahukan dalam pemberitahuan pabean ini.</td>
				</tr>
				<tr>
					<td colspan="6" rowspan="" style="border-bottom-style: hidden;"></td>
					<td class="text-center" colspan="6" rowspan="" style="border-bottom-style: hidden;">BANDUNG, <?php echo date('d F Y'); ?></td>
				</tr>
				<tr>
					<td colspan="6" rowspan="" style="border-bottom-style: hidden;"></td>
					<td colspan="6" rowspan="" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="6" style="border-bottom-style: hidden;"></td>
					<td colspan="6" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="6" style="border-bottom-style: hidden;"></td>
					<td colspan="6" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="6" style="border-bottom-style: hidden;"></td>
					<td colspan="6" style="border-bottom-style: hidden;"></td>
				<tr>
					<td colspan="6" rowspan="" style=""></td>
					<td class="text-center" colspan="6" rowspan="" style="">NAMA PENGUSAHA TPB</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<!-- DATA BARANG DAN/ATAU BAHAN IMPOR -->
<div class="card-box" style="margin-bottom:30px;margin-top:20px;">
	<div class="row">
		<div class="col-md-12">
			<div class="text-center">
				<label class="label-header">LEMBAR LAMPIRAN</label>
			</div>					
			<div class="text-center">
				<label class="label-header">DATA BARANG DAN/ATAU BAHAN IMPOR</label>
			</div>					
			<div class="text-center">
				<label class="label-header">UNTUK DIANGKUT DARI TEMPAT PENIMBUNAN BERIKAT KE TEMPAT PENIMBUNAN BERIKAT LAINNYA</label>
			</div>
			<div class="text-right">
				<?php if(isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']); ?>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<table class="table table-bordered" cellspacing="0" width="100%">
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">Kantor Pabean</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="3" style="border-bottom-style: hidden;">KPPBC BANDUNG</td>
					<td class="text-center" colspan="1" style="">050500</td>
					<td class="text-right" colspan="4" style="border-bottom-style: hidden;">halaman ke 5 dari 6</td>
				</tr>
				<tr>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;">Nomor Pengajuan</td>
					<td colspan="1" style="border-bottom-style: hidden;border-right-style: hidden;">:</td>
					<td colspan="3" style="border-bottom-style: hidden;border-right-style: hidden;"><?php if(isset($no_pengajuan)) echo strtoupper($no_pengajuan); ?></td>
					<td colspan="5" style="border-bottom-style: hidden;"></td>
				</tr>
				<tr>
					<td colspan="3" style="border-right-style: hidden;">Nomor Pendaftaran</td>
					<td colspan="1" style="border-right-style: hidden;">:</td>
					<td colspan="3" style="border-right-style: hidden"><?php if(isset($bc[0]['no_pendaftaran'])) echo strtoupper($bc[0]['no_pendaftaran']); ?></td>
					<td colspan="3" style="border-right-style: hidden">Tanggal Pendaftaran</td>
					<td colspan="2" style="">: <?php if(isset($bc[0]['tanggal_pengajuan'])) $date = date_create($bc[0]['tanggal_pengajuan']); echo date_format($date, 'd-m-Y'); ?></td>
				</tr>
				<tr>
					<td colspan="1" style="">No. Urut Barang</td>
					<td colspan="2" style="">
						- Kode Kantor
						- No/Tgl Daftar BC
						- 2.3, BC 2.7.
					</td>
					<td colspan="1" style="">
						No Urut Dalam
						- BC 2.3
						- BC 2.7
					</td>
					<td colspan="3" style="">Pos Tarif/HS, uraian jumlah dan jenis barang secara lengkap, kode barang, merk tipe, ukuran dan spesifikasi lain</td>
					<td colspan="1" style="">
						- Jumlah
						- Satuan
					</td>
					<td colspan="2" style="">
						Nilai
						- CIF
						- Harga Penyerahan (Rp)
					</td>
					<td colspan="2" style="">
						Nilai (Rp)
						BM, BMT, Cukai, PPN
						PPnBM,
						PPh 22
					</td>
				</tr>
				<tr>
					<td class="text-center" colspan="1" style="">1</td>
					<td class="text-center" colspan="2" style="">2</td>
					<td class="text-center" colspan="1" style="">3</td>
					<td class="text-center" colspan="3" style="">4</td>
					<td class="text-center" colspan="1" style="">5</td>
					<td class="text-center" colspan="2" style="">6</td>
					<td class="text-center" colspan="2" style="">7</td>
				</tr>
				<tr>
					<td colspan="1" style="">1.1</td>
					<td colspan="2" style="">
						- 050500
						- BC 23 / 034422
						  09-11-2017
					</td>
					<td colspan="1" style="">
						Seri barang ke - 2
					</td>
					<td colspan="3" style="">
						- Pos Tarif/HS : 74102110
						- Kode Barang : CCLC36
						- COPPER CLAD LAMINATED, Merk : CCP3400,
						  Tipe : 1240 x 1080 1.6MM Ukuran dan Spesifikasi lain :
					</td>
					<td colspan="1" style="">
						18,2000 ST
					</td>
					<td colspan="2" style="">
						- 489,22
						- 6.874.519,44
					</td>
					<td colspan="2" style="">
						BM		0,00
						BMT		0,00
						Cukai	0,00
						PPN		0,00
						PPnBM	0,00
						PPh		0,00
					</td>
				</tr>
				<tr>
					<td colspan="1" style="">2.1</td>
					<td colspan="2" style="border-bottom-style: hidden;">
						- 050500
						- BC 23 / 040609
						  11-10-2018
					</td>
					<td colspan="1" style="border-bottom-style: hidden;">
						Seri barang ke - 2
					</td>
					<td colspan="3" style="">
						- Pos Tarif/HS : 74102110
						- Kode Barang : CCLF17
						- COPPER CLAD LAMINATED, Merk : ETL 204,
						  Tipe : FR-1 Ukuran : 1225 x 1025 1.6 MM dan Spesifikasi lain :
					</td>
					<td colspan="1" style="border-bottom-style: hidden">
						18,2000 ST
					</td>
					<td colspan="2" style="border-bottom-style: hidden">
						- 489,22
						- 6.874.519,44
					</td>
					<td colspan="2" style="border-bottom-style: hidden">
						BM		0,00
						BMT		0,00
						Cukai	0,00
						PPN		0,00
						PPnBM	0,00
						PPh		0,00
					</td>
				</tr>
				<tr>
					<td colspan="12" style="border-bottom-style: hidden"><h5><label>C. PENGESAHAN PENGUSAHA TPB</label></h5></td>
				</tr>
				<tr>
					<td colspan="12" style="border-bottom-style: hidden">
						Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberitahukan dalam pemberitahuan pabean ini dan keabsahan dokumen
					</td>
				</tr>
				<tr>
					<td colspan="12" style="border-bottom-style: hidden">
						pelengkap pabean yang menjadi dasar pembuatan dokumen ini.
					</td>
				</tr>
				<tr>
					<td colspan="12" style="border-bottom-style: hidden">
						Tempat, Tanggal		: <?php echo date('d F Y'); ?>
					</td>
				</tr>
				<tr>
					<td colspan="12" style="border-bottom-style: hidden">
						Nama Lengkap		: Nama Pengusaha TPB
					</td>
				</tr>
				<tr>
					<td colspan="12" style="border-bottom-style: hidden">
						Jabatan				:
					</td>
				</tr>
				<tr>
					<td colspan="12" style="">
						Tanda Tangan dan Stempel Perusahaan :
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	var id_bc = "<?php if(isset($bc[0])){ echo $bc[0]['id']; } ?>";

	$(document).ready(function() {
		$('#back_bc').on('click', function() {
			$('#title_Menu').html('Bea Cukai');
			$('#div_list_bc').show();
			$('#div_detail_bc').hide();
			$('#div_detail_bc').html('');
		})
	});
</script>