<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Report_Penjualan extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('report_penjualan/Report_penjualan_model');
    $this->load->library('excel');
    $this->load->library('log_activity');
  }

  /**
   * anti sql injection
   */
  public function Anti_sql_injection($string)
  {
    $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
    return $string;
  }

  public function index()
  {
    $this->template->load('maintemplate', 'report_penjualan/views/index');
  }

  public function report_detail()
  {
    $this->load->view('report_detail_view');
  }

  public function view_report()
  {
    // $jenis_report = $this->Anti_sql_injection($this->input->post('jenis_report', TRUE));
    $tanggalMasuk = $this->Anti_sql_injection($this->input->post('tanggal_masuk', TRUE));
    $tanggalKeluar = $this->Anti_sql_injection($this->input->post('tanggal_keluar', TRUE));

    $tglMsk = explode("/", $tanggalMasuk);
    $tglKel = explode("/", $tanggalKeluar);
    $tanggal_masuk = date('Y-m-d', strtotime($tglMsk[2] . '-' . $tglMsk[1] . '-' . $tglMsk[0]));
    $tanggal_keluar = date('Y-m-d', strtotime($tglKel[2] . '-' . $tglKel[1] . '-' . $tglKel[0]));

    $dataReport = array(
      'tanggal_masuk' => $tanggal_masuk,
      'tanggal_keluar' => $tanggal_keluar
    );

    $result_report = $this->Report_penjualan_model->get_report_penjualan($dataReport);
    if (sizeof($result_report) > 0) {
      $result = array(
        'report'      => 'Penjualan',
        'data'        => $result_report,
        'tanggal_masuk'    => $tanggal_masuk,
        'tanggal_keluar'  => $tanggal_keluar,
        'success'      => true
      );
    } else {
      $result = array(
        'report'      => 'Penjualan',
        'data'        => array(),
        'tanggal_masuk'    => $tanggal_masuk,
        'tanggal_keluar'  => $tanggal_keluar,
        'success'      => false
      );
    }
    $this->load->view('report_penjualan_view', $result);

    /*echo "<pre>";
		print_r($result_report);
		echo "</pre>";
		die;*/

    /*if($jenis_report == 'lokal') {
			//LOKAL
			$lokal = array('tanggal_masuk' => $tanggal_masuk, 'tanggal_keluar' => $tanggal_keluar);
			$result_lokal = $this->Report_penjualan_model->get_report_penjualan_lokal($lokal);
			
			$data = array(
				'report'			=> 'Penjualan Lokal',
				'tanggal_masuk'		=> date('d-F-Y', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0])),
				'tanggal_keluar'	=> date('d-F-Y', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0])),
				'hasil'				=> $result_lokal
			);
				
			if(count($result_lokal) > 0){
				$msg = 'Lihat report';
				
				$data['msg'] = array('message' => 0);
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}else{
				$msg = 'Gagal melihat report';
				
				$data['msg'] = array('message' => 1);
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}
			
			$this->load->view('report_penjualan_view', $data);
		}else if($jenis_report == 'import') {
			//IMPORT
			$import = array('tanggal_masuk' => $tanggal_masuk, 'tanggal_keluar' => $tanggal_keluar);
			$result_import = $this->Report_penjualan_model->get_report_penjualan_import($import);
			
			$data = array(
				'report'			=> 'Penjualan Import',
				'tanggal_masuk'		=> date('d-F-Y', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0])),
				'tanggal_keluar'	=> date('d-F-Y', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0])),
				'hasil'				=> $result_import
			);
				
			if(count($result_import) > 0){
				$msg = 'Lihat report';
				
				$data['msg'] = array('message' => 0);
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}else{
				$msg = 'Gagal melihat report';
				
				$data['msg'] = array('message' => 1);
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}
			
			$this->load->view('report_penjualan_view', $data);
		}else if($jenis_report == 'service') {
			//SERVICE
			$service = array('tanggal_masuk' => $tanggal_masuk, 'tanggal_keluar' => $tanggal_keluar);
			$result_service = $this->Report_penjualan_model->get_report_penjualan_service($service);
			
			$data = array(
				'report'			=> 'Penjualan Service',
				'tanggal_masuk'		=> date('d-F-Y', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0])),
				'tanggal_keluar'	=> date('d-F-Y', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0])),
				'hasil'				=> $result_service
			);
			
			if(count($result_mutasi_barjad) > 0) {
				$msg = 'Lihat report';
				
				$data['msg'] = array('message' => 0);
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}else {
				$msg = 'Gagal melihat report';
				
				$data['msg'] = array('message' => 1);
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}
			
			$this->load->view('report_penjualan_view', $data);
		}else {
			$msg = 'Gagal melihat report Penjualan';
			
			$result = array('success' => false);
			$this->log_activity->insert_activity('view', $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}*/
  }

  public function export_excel()
  {
    $jenis_report = $this->Anti_sql_injection($this->input->post('report', TRUE));
    $tanggalMasuk = $this->Anti_sql_injection($this->input->post('tanggal_awal', TRUE));
    $tanggalKeluar = $this->Anti_sql_injection($this->input->post('tanggal_akhir', TRUE));

    $datemasuk = date_create($tanggalMasuk);
    $date_masuk_fix = date_format($datemasuk, "Y-m-d");

    $datekeluar = date_create($tanggalKeluar);
    $date_keluar_fix = date_format($datekeluar, "Y-m-d");

    // if($jenis_report == 'Pembelian Lokal') {

    $lokal = array('tanggal_masuk' => $date_masuk_fix, 'tanggal_keluar' => $date_keluar_fix);
    $result_lokal = $this->Report_penjualan_model->get_report_penjualan($lokal);

    $objPHPExcel = new PHPExcel();

    $objPHPExcel->getActiveSheet()->setTitle("Report Penjualan Lokal");
    $objPHPExcel->getProperties()->setCreator("System")
      ->setLastModifiedBy("System")
      ->setTitle("Report Penjualan Lokal")
      ->setSubject("Report Bea Cukai")
      ->setDescription("Report Penjualan Lokal")
      ->setKeywords("Penjualan Lokal")
      ->setCategory("Report");

    $objPHPExcel->setActiveSheetIndex(0)
      ->setCellValue('A1', 'LAPORAN ' . strtoupper($jenis_report))
      ->setCellValue('A2', 'PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA')
      ->setCellValue('A3', 'PERIODE ' . strtoupper($tanggalMasuk) . ' S.D ' . strtoupper($tanggalKeluar) . '');

    $objPHPExcel->setActiveSheetIndex(0)
      ->setCellValue('A5', 'No')
      ->setCellValue('B5', 'Nama')
      // ->setCellValue('C5', 'NPWP')
      ->setCellValue('C5', 'Faktur Pajak')
      ->setCellValue('D5', 'Tanggal')
      ->setCellValue('E5', 'No. Invoice')
      ->setCellValue('F5', 'USD')
      ->setCellValue('G5', 'Rate')
      ->setCellValue('H5', 'DPP')
      ->setCellValue('I5', 'PPN')
      ->setCellValue('J5', 'TOTAL');

    $styleArray = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN
        )
      )
    );


    $i = 6;
    // echo "<pre>";
    // print_r($result_lokal);
    // die;

    $no = 1;
    $total_harga_dpp = 0;
    $total_harga_ppn = 0;
    $total_harga_total_perbaris = 0;
    foreach ($result_lokal as $lokal) {

      $faktur_pajak = strtolower($lokal['note']);
      $faktur_pajak = str_replace("faktur pajak", "", $faktur_pajak);
      $faktur_pajak = preg_replace('/\D/', '', $faktur_pajak);

      $harga_dpp = $lokal['invoice_price'] * $lokal['rate'];
      if (substr($faktur_pajak, 0, 2) == '01')
        $harga_ppn = (float) $harga_dpp * ((float) $lokal['ppn'] / 100);
      else
        $harga_ppn = 0;
      $harga_total_per_baris = $harga_ppn + $harga_dpp;
      $total_harga_dpp += $harga_dpp;
      $total_harga_ppn += $harga_ppn;
      $total_harga_total_perbaris += $harga_total_per_baris;

      $objPHPExcel->setActiveSheetIndex()->getStyle('A5:J' . $i)->applyFromArray($styleArray);
      $objPHPExcel->setActiveSheetIndex(0)
        // ->setCellValue('A'.$i, $no)
        ->setCellValue('B' . $i, $lokal['name_eksternal'])
        ->setCellValue('C' . $i, $faktur_pajak)
        // ->setCellValue('D'.$i, $lokal['tanggal_invoice'])
        ->setCellValue('E' . $i, $lokal['no_invoice'])
        ->setCellValue('F' . $i, $lokal['symbol'] . number_format($lokal['invoice_price'], 2, ',', '.'))
        ->setCellValue('G' . $i, $lokal['rate']);
        // ->setCellValue('H' . $i, 'Rp' . number_format($harga_dpp, 2, ',', '.'))
        // ->setCellValue('I' . $i, 'Rp' . number_format($harga_ppn, 2, ',', '.'))
        // ->setCellValue('J' . $i, 'Rp' . number_format($harga_total_per_baris, 2, ',', '.'));

      //CUSTOM COLUMN -->
      $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A' . $i, $no)->getStyle('A' . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

      $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('D' . $i, $lokal['tanggal_invoice'])->getStyle('D' . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

      $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValueExplicit('H' . $i, number_format($harga_dpp, 2, ',', '.'), PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('H' . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

      $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValueExplicit('I' . $i,number_format($harga_ppn, 2, ',', '.'), PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('I' . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
      
      $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValueExplicit('J' . $i,number_format($harga_total_per_baris, 2, ',', '.'), PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('J' . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

      $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
      $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
      $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
      $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
      $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
      $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
      $objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
      $objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
      $objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
      $objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);

      $i++;
      $no++;
    }
    
    $objPHPExcel->setActiveSheetIndex()->getStyle('A5:J' . $i)->applyFromArray($styleArray);
    
    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B'.$i.':G'.$i);
    $objPHPExcel->setActiveSheetIndex(0)
    // ->setCellValue('A'.$i, $no)
    ->setCellValue('B' . $i, 'Total Sales : '. strtoupper($tanggalMasuk) . ' S.D ' . strtoupper($tanggalKeluar));
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValueExplicit('H' . $i, number_format($total_harga_dpp, 4, ',', '.'), PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('H' . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

  $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValueExplicit('I' . $i, number_format($total_harga_ppn, 4, ',', '.'), PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('I' . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
  
  $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValueExplicit('J' . $i, number_format($total_harga_total_perbaris, 4, ',', '.'), PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('J' . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

    // ->setCellValue('G' . $i, $lokal['rate']);

    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:J1');
    $objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:J1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:J2');
    $objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:J2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:J3');
    $objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:J3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:J4');
    // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:A6');
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:E5');
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:E5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('B6:B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('C6:C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('D6:D6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('E6:E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('F6:F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('G6:G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('H6:H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('L6:L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('M6:M6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('N6:N6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('O6:O6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('P6:P6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('Q6:Q6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('R6:R6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('S6:S6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('T6:T6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('U6:U6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('V6:V6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('W6:W6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('F5:H5');
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('F5:H5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('L5:N5');
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:N5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('O5:Q5');
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('O5:Q5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('R5:U5');
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('R5:U5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('V5:W5');
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('V5:W5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('I5:I6');
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('J5:J6');
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->mergeCells('K5:K6');
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // $objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Penjualan ' . $tanggalMasuk . ' S.D ' . $tanggalKeluar . '.xls"');

    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
    exit;

    // } elseif($jenis_report == 'Pembelian Import') {

    // 	$import = array('tanggal_masuk' => $date_masuk_fix, 'tanggal_keluar' => $date_keluar_fix);
    // 	$result_import = $this->Report_penjualan_model->get_report_pembelian_import($import);

    // 	$objPHPExcel = new PHPExcel();

    // 	$objPHPExcel->getActiveSheet()->setTitle("Report Pembelian Import");
    // 	$objPHPExcel->getProperties()->setCreator("System")
    // 		->setLastModifiedBy("System")
    // 		->setTitle("Report Pembelian Import")
    // 		->setSubject("Report Bea Cukai")
    // 		->setDescription("Report Pembelian Import")
    // 		->setKeywords("Pembelian Import")
    // 		->setCategory("Report");

    // 	$objPHPExcel->setActiveSheetIndex(0)
    // 		->setCellValue('A1', 'LAPORAN '.strtoupper($jenis_report))
    // 		->setCellValue('A2', 'PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA')
    // 		->setCellValue('A3', 'PERIODE '.strtoupper($tanggalMasuk).' S.D '.strtoupper($tanggalKeluar).'');

    // 	$objPHPExcel->setActiveSheetIndex(0)
    // 		->setCellValue('A5', 'No')
    // 		->setCellValue('B5', 'PR')
    // 		->setCellValue('B6', 'No')
    // 		->setCellValue('C6', 'Kode')
    // 		->setCellValue('D6', 'Qty')
    // 		->setCellValue('E6', 'Unit')
    // 		->setCellValue('F5', 'PO')
    // 		->setCellValue('F6', 'No')
    // 		->setCellValue('G6', 'PO')
    // 		->setCellValue('H6', 'TOP')
    // 		->setCellValue('I5', 'Deskripsi')
    // 		->setCellValue('J5', 'Qty')
    // 		->setCellValue('K5', 'Unit')
    // 		->setCellValue('L5', 'UOM')
    // 		->setCellValue('L6', 'IDR')
    // 		->setCellValue('M6', 'SGD')
    // 		->setCellValue('N6', 'USD')
    // 		->setCellValue('O5', 'Amount')
    // 		->setCellValue('O6', 'IDR')
    // 		->setCellValue('P6', 'SGD')
    // 		->setCellValue('Q6', 'USD')
    // 		->setCellValue('R5', 'BTB')
    // 		->setCellValue('R6', 'No')
    // 		->setCellValue('S6', 'Tanggal')
    // 		->setCellValue('T6', 'Qty')
    // 		->setCellValue('U6', 'Unit')
    // 		->setCellValue('V5', 'No BC')
    // 		->setCellValue('V6', 'No')
    // 		->setCellValue('W6', 'Tanggal');

    // 	$styleArray = array(
    // 		'borders' => array(
    // 			'allborders' => array(
    // 				'style' => PHPExcel_Style_Border::BORDER_THIN
    // 			)
    // 		)
    // 	);


    // 	$i = 7;
    // 	$no = 1;
    // 	foreach($result_import as $import){

    // 		$objPHPExcel->setActiveSheetIndex()->getStyle('A5:W'.$i)->applyFromArray($styleArray);
    // 		$objPHPExcel->setActiveSheetIndex(0)
    // 			->setCellValue('B'.$i, $import['no_spb'])
    // 			->setCellValue('C'.$i, $import['stock_code'])
    // 			->setCellValue('D'.$i, $import['base_qty'])
    // 			->setCellValue('F'.$i, $import['no_po'])
    // 			->setCellValue('I'.$i, $import['stock_description'])
    // 			->setCellValue('J'.$i, $import['base_qty'])
    // 			->setCellValue('M'.$i, $import['SGD'])
    // 			->setCellValue('N'.$i, $import['USD'])
    // 			->setCellValue('P'.$i, $import['SGD'])
    // 			->setCellValue('Q'.$i, $import['USD'])
    // 			->setCellValue('R'.$i, $import['no_btb'])
    // 			->setCellValue('T'.$i, $import['base_qty'])
    // 			->setCellValue('V'.$i, $import['no_pendaftaran']);

    // 		//CUSTOM COLUMN -->
    // 		$objPHPExcel->setActiveSheetIndex(0)
    // 			->setCellValue('A'.$i, $no)->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // 		$objPHPExcel->setActiveSheetIndex(0)
    // 			->setCellValue('E'.$i, $import['uom_name'])->getStyle('E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // 		$objPHPExcel->setActiveSheetIndex(0)
    // 			->setCellValue('G'.$i, $import['date_po'])->getStyle('G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // 		$objPHPExcel->setActiveSheetIndex(0)		
    // 			->setCellValue('H'.$i, '-')->getStyle('H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // 		$objPHPExcel->setActiveSheetIndex(0)
    // 			->setCellValue('K'.$i, $import['uom_name'])->getStyle('K'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // 		$objPHPExcel->setActiveSheetIndex(0)
    // 			->setCellValue('S'.$i, $import['approval_date'])->getStyle('S'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // 		$objPHPExcel->setActiveSheetIndex(0)
    // 			->setCellValue('U'.$i, $import['uom_name'])->getStyle('U'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // 		$objPHPExcel->setActiveSheetIndex(0)
    // 			->setCellValue('W'.$i, $import['tanggal_pengajuan'])->getStyle('U'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // 		$objPHPExcel->setActiveSheetIndex(0)
    // 			->setCellValueExplicit('L'.$i, str_replace('.','',number_format($import['Rp'],4,',','.')),PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('L'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

    // 		$objPHPExcel->setActiveSheetIndex(0)
    // 			->setCellValueExplicit('O'.$i, str_replace('.','',number_format($import['Rp'],4,',','.')),PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('O'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(35);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(12);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(12);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(12);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(12);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(13);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(15);

    // 		$i++;
    // 		$no++;


    // 	}

    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:W1');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:W1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:W2');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:W2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:W3');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:W3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:W4');
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:A6');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:E5');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:E5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('B6:B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('C6:C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('D6:D6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('E6:E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('F6:F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('G6:G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('H6:H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('L6:L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('M6:M6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('N6:N6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('O6:O6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('P6:P6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('Q6:Q6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('R6:R6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('S6:S6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('T6:T6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('U6:U6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('V6:V6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('W6:W6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F5:H5');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('F5:H5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L5:N5');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:N5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('O5:Q5');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('O5:Q5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('R5:U5');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('R5:U5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('V5:W5');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('V5:W5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I5:I6');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J5:J6');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('K5:K6');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

    // 	header('Content-Type: application/vnd.ms-excel');
    // 	header('Content-Disposition: attachment;filename="Pembelian Import '.$tanggalMasuk.' S.D '.$tanggalKeluar.'.xls"');

    // 	header('Cache-Control: max-age=0');
    // 	// If you're serving to IE 9, then the following may be needed
    // 	header('Cache-Control: max-age=1');

    // 	// If you're serving to IE over SSL, then the following may be needed
    // 	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    // 	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    // 	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    // 	header ('Pragma: public'); // HTTP/1.0

    // 	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    // 	$objWriter->save('php://output');
    // 	exit; 

    // } elseif($jenis_report == 'Pembelian Service') {

    // 	$service = array('tanggal_masuk' => $date_masuk_fix, 'tanggal_keluar' => $date_keluar_fix);
    // 	$result_service = $this->Report_penjualan_model->get_report_pembelian_service($service);

    // 	$objPHPExcel = new PHPExcel();

    // 	$objPHPExcel->getActiveSheet()->setTitle("Report Pembelian Service");
    // 	$objPHPExcel->getProperties()->setCreator("System")
    // 		->setLastModifiedBy("System")
    // 		->setTitle("Report Pembelian Service")
    // 		->setSubject("Report Bea Cukai")
    // 		->setDescription("Report Pembelian Service")
    // 		->setKeywords("Pembelian Service")
    // 		->setCategory("Report");

    // 	$objPHPExcel->setActiveSheetIndex(0)
    // 		->setCellValue('A1', 'LAPORAN '.strtoupper($jenis_report))
    // 		->setCellValue('A2', 'PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA')
    // 		->setCellValue('A3', 'PERIODE '.strtoupper($tanggalMasuk).' S.D '.strtoupper($tanggalKeluar).'');

    // 	$objPHPExcel->setActiveSheetIndex(0)
    // 		->setCellValue('A5', 'No')
    // 		->setCellValue('B5', 'PR')
    // 		->setCellValue('B6', 'No')
    // 		->setCellValue('C6', 'Kode')
    // 		->setCellValue('D6', 'Qty')
    // 		->setCellValue('E6', 'Unit')
    // 		->setCellValue('F5', 'PO')
    // 		->setCellValue('F6', 'No')
    // 		->setCellValue('G6', 'PO')
    // 		->setCellValue('H6', 'TOP')
    // 		->setCellValue('I5', 'Deskripsi')
    // 		->setCellValue('J5', 'Qty')
    // 		->setCellValue('K5', 'Unit')
    // 		->setCellValue('L5', 'UOM')
    // 		->setCellValue('L6', 'IDR')
    // 		->setCellValue('M6', 'SGD')
    // 		->setCellValue('N6', 'USD')
    // 		->setCellValue('O5', 'Amount')
    // 		->setCellValue('O6', 'IDR')
    // 		->setCellValue('P6', 'SGD')
    // 		->setCellValue('Q6', 'USD')
    // 		->setCellValue('R5', 'BTB')
    // 		->setCellValue('R6', 'No')
    // 		->setCellValue('S6', 'Tanggal')
    // 		->setCellValue('T6', 'Qty')
    // 		->setCellValue('U6', 'Unit')
    // 		->setCellValue('V5', 'No BC')
    // 		->setCellValue('V6', 'No')
    // 		->setCellValue('W6', 'Tanggal');

    // 	$styleArray = array(
    // 		'borders' => array(
    // 			'allborders' => array(
    // 				'style' => PHPExcel_Style_Border::BORDER_THIN
    // 			)
    // 		)
    // 	);


    // 	$i = 7;
    // 	$no = 1;
    // 	foreach($result_service as $service){

    // 		$objPHPExcel->setActiveSheetIndex()->getStyle('A5:W'.$i)->applyFromArray($styleArray);
    // 		$objPHPExcel->setActiveSheetIndex(0)
    // 			->setCellValue('B'.$i, $service['no_spb'])
    // 			->setCellValue('C'.$i, $service['stock_code'])
    // 			->setCellValue('D'.$i, $service['base_qty'])
    // 			->setCellValue('F'.$i, $service['no_po'])
    // 			->setCellValue('I'.$i, $service['stock_description'])
    // 			->setCellValue('J'.$i, $service['base_qty'])
    // 			->setCellValue('M'.$i, $service['SGD'])
    // 			->setCellValue('N'.$i, $service['USD'])
    // 			->setCellValue('P'.$i, $service['SGD'])
    // 			->setCellValue('Q'.$i, $service['USD'])
    // 			->setCellValue('R'.$i, $service['no_btb'])
    // 			->setCellValue('T'.$i, $service['base_qty'])
    // 			->setCellValue('V'.$i, $service['no_pendaftaran']);

    // 		//CUSTOM COLUMN -->
    // 		$objPHPExcel->setActiveSheetIndex(0)
    // 			->setCellValue('A'.$i, $no)->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // 		$objPHPExcel->setActiveSheetIndex(0)
    // 			->setCellValue('E'.$i, $service['uom_name'])->getStyle('E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // 		$objPHPExcel->setActiveSheetIndex(0)
    // 			->setCellValue('G'.$i, $service['date_po'])->getStyle('G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // 		$objPHPExcel->setActiveSheetIndex(0)		
    // 			->setCellValue('H'.$i, '-')->getStyle('H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // 		$objPHPExcel->setActiveSheetIndex(0)
    // 			->setCellValue('K'.$i, $service['uom_name'])->getStyle('K'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // 		$objPHPExcel->setActiveSheetIndex(0)
    // 			->setCellValue('S'.$i, $service['approval_date'])->getStyle('S'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // 		$objPHPExcel->setActiveSheetIndex(0)
    // 			->setCellValue('U'.$i, $service['uom_name'])->getStyle('U'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // 		$objPHPExcel->setActiveSheetIndex(0)
    // 			->setCellValue('W'.$i, $service['tanggal_pengajuan'])->getStyle('U'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

    // 		$objPHPExcel->setActiveSheetIndex(0)
    // 			->setCellValueExplicit('L'.$i, str_replace('.','',number_format($service['Rp'],4,',','.')),PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('L'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

    // 		$objPHPExcel->setActiveSheetIndex(0)
    // 			->setCellValueExplicit('O'.$i, str_replace('.','',number_format($service['Rp'],4,',','.')),PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('O'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(35);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(12);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(12);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(12);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(12);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(13);
    // 		$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(15);

    // 		$i++;
    // 		$no++;


    // 	}

    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:W1');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:W1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:W2');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:W2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:W3');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:W3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:W4');
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:A6');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:E5');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:E5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('B6:B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('C6:C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('D6:D6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('E6:E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('F6:F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('G6:G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('H6:H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('L6:L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('M6:M6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('N6:N6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('O6:O6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('P6:P6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('Q6:Q6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('R6:R6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('S6:S6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('T6:T6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('U6:U6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('V6:V6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('W6:W6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F5:H5');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('F5:H5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L5:N5');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:N5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('O5:Q5');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('O5:Q5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('R5:U5');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('R5:U5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('V5:W5');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('V5:W5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I5:I6');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J5:J6');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->mergeCells('K5:K6');
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    // 	$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

    // 	header('Content-Type: application/vnd.ms-excel');
    // 	header('Content-Disposition: attachment;filename="Pembelian Service '.$tanggalMasuk.' S.D '.$tanggalKeluar.'.xls"');

    // 	header('Cache-Control: max-age=0');
    // 	// If you're serving to IE 9, then the following may be needed
    // 	header('Cache-Control: max-age=1');

    // 	// If you're serving to IE over SSL, then the following may be needed
    // 	header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    // 	header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    // 	header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    // 	header ('Pragma: public'); // HTTP/1.0

    // 	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    // 	$objWriter->save('php://output');
    // 	exit; 

    // }

  }
}
