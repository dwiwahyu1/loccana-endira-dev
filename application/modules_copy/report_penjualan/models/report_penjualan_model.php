<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Report_Penjualan_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}
	
	public function get_report_penjualan($data) {
		$sql 	= 'CALL report_penjualan_do(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['tanggal_masuk'],
			$data['tanggal_keluar']
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function get_report_penjualan_lokal($data) {
		$sql 	= 'CALL report_lokal(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['tanggal_masuk'],
			$data['tanggal_keluar']
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function get_report_penjualan_import($data) {
		$sql 	= 'CALL report_import(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['tanggal_masuk'],
			$data['tanggal_keluar']
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function get_report_penjualan_service($data) {
		$sql 	= 'CALL report_service(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['tanggal_masuk'],
			$data['tanggal_keluar']
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
}