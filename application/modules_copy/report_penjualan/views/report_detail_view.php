<style type="text/css">
	.row-search{
		padding: 5px;
	}
</style>

<!-- <div class="row row-search">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jenis_report">Jenis Report</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<select class="form-control" id="jenis_report" name="jenis_report" required>
			<option value="">-- Select Jenis --</option>
			<option value="lokal">Lokal</option>
			<option value="import">Import</option>
			<option value="service">Service</option>
		</select>
	</div>
</div> -->

<div class="row row-search">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="date-range">Tanggal</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<div class="input-daterange input-group" id="date-range">
			<input type="text" class="form-control" id="tanggal_masuk" name="tanggal_masuk" autocomplete="off" required>
			<span class="input-group-addon bg-primary b-0 text-white">to</span>
			<input type="text" class="form-control" id="tanggal_keluar" name="tanggal_keluar" autocomplete="off">
		</div>
	</div>
</div>
<hr>
<div class="row row-search">
	<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<button id="btn-search" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Cari</button>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		jQuery('#date-range').datepicker({
			format: "dd/M/yyyy",
			toggleActive: true
		}).focus(function() {
			$(this).prop("autocomplete", "off");
		});
		
		$('#btn-search').on('click', function () {
			$('#btn-search').attr('disabled','disabled');
			$('#btn-search').text("Loading...");
			$('#div_report').removeData('');
			$('#div_report').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');

			// if($('#tanggal_masuk').val() != '' && $('#tanggal_keluar').val() != '' && $('#jenis_report').val() != '') {
			if($('#tanggal_masuk').val() != '' && $('#tanggal_keluar').val() != '') {
				var data = {
					// 'jenis_report'		: $('#jenis_report').val(),
					'tanggal_masuk'		: $('#tanggal_masuk').val(),
					'tanggal_keluar'	: $('#tanggal_keluar').val()
				};

				$('#div_report').load('<?php echo base_url('report_penjualan/view_report/');?>', data, function(response, status, xhr) {
					$('#btn-search').removeAttr('disabled');
					$('#btn-search').text("Cari");
					$('#panel-modal-detail').modal('toggle');
				});
			}else {
				$('#btn-search').removeAttr('disabled');
				$('#btn-search').text("Cari");
				$('#div_report').removeData('');
				$('#div_report').html('');
				swal("Failed!", "Invalid inputan, silahkan cek kembali.", "error");
			}
		})
	});
</script>