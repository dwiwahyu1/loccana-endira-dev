<style>
	.dt-body-left thead tr td {
		text-align: left;
		vertical-align: middle;
	}

	.dt-body-center thead tr td {
		text-align: center;
		vertical-align: middle;
	}

	.dt-body-right thead tr td {
		text-align: right;
		vertical-align: middle;
	}

	.dt-body-left thead tr th {
		text-align: left;
		vertical-align: middle;
	}

	.dt-body-center thead tr th {
		text-align: center;
		vertical-align: middle;
	}

	.dt-body-right thead tr th {
		text-align: right;
		vertical-align: middle;
	}

	.dt-body-left tbody tr td {
		text-align: left;
		vertical-align: middle;
	}

	.dt-body-center tbody tr td {
		text-align: center;
		vertical-align: middle;
	}

	.dt-body-right tbody tr td {
		text-align: right;
		vertical-align: middle;
	}

	.dt-body-left tbody tr th {
		text-align: left;
		vertical-align: middle;
	}

	.dt-body-center tbody tr th {
		text-align: center;
		vertical-align: middle;
	}

	.dt-body-right tbody tr th {
		text-align: right;
		vertical-align: middle;
	}

	.changed_status {
		cursor: pointer;
		text-decoration: underline;
		color: #96b6e8;
	}

	.changed_status:hover {
		color: #ff8c00
	}

	.custom-tables,
	th {
		text-align: center;
		vertical-align: middle;
	}

	.custom-tables.align-text,
	th {
		vertical-align: middle;
	}

	#titleTanggal {
		margin-bottom: 10px;
	}

	.center_numb {
		text-align: center;
	}

	.left_numb {
		text-align: left;
	}

	.right_numb {
		text-align: right;
		font-weight: bold;
	}

	.tableReport {
		box-sizing: border-box;
		text-align: left;
		width: 90%;
		margin: 10px auto;
		padding-bottom: 5px;
		page-break-before: always;
		z-index: 99;
		font-size: 6px;
	}

	.table-scroll {
		position: relative;
		max-width: 100%;
		margin: auto;
		overflow: hidden;
		border: 1px solid #f0fffd;
	}

	.table-wrap {
		width: 100%;
		overflow: auto;
	}

	.table-scroll table {
		width: 100%;
		margin: auto;
		border-spacing: 0;
	}

	.table-scroll th,
	.table-scroll td {
		padding: 5px 10px;
		border: 1px solid #000;
		background: #fff;
		white-space: nowrap;
		vertical-align: top;
	}
</style>
<?php
// echo "<pre>";
// print_r($data);
// echo "</pre>";
?>
<div class="card-box">
	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<a class="btn btn-icon waves-effect waves-light btn-success m-b-5" title="Refresh Report" id="refresh_report">
					<i class="fa fa-refresh"></i>
				</a>
				<a class="btn btn-icon waves-effect waves-light btn-success m-b-5" data-toggle="tooltip" data-placement="top" title="Export to Excel" id="export_excel">
					<i class="fa fa-file-excel-o"></i>
				</a>
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download PDF" id="btn_download">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>

	<div class="col-md-12 text-center titleReport">
		<h2 id="titlePerusahaan">LAPORAN <?php echo strtoupper($report); ?></h2>
		<h2 id="namaPerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
		<h3 id="titleTanggal">PERIODE <?php echo $tanggal_masuk . ' S.D ' . $tanggal_keluar; ?></h3>
	</div>
	<hr>
	<div id="table-scroll" class="table-scroll">
		<div class="table-wrap">
			<table id="listreportpembelian" class="table table-striped table-bordered table-responsive dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<!-- <th class="dt-body-center">No</th>
						<th class="dt-body-center">No Delivery Order</th>
						<th class="dt-body-center">No Invoice</th>
						<th class="dt-body-center">Remark</th>
						<th class="dt-body-center">Price</th>
						<th class="dt-body-center">Customer</th> -->
						<th class="dt-body-center">No</th>
						<th class="dt-body-center">No. DO</th>
						<th class="dt-body-center">Nama</th>
						<th class="dt-body-center">Faktur Pajak</th>
						<th class="dt-body-center">Tanggal</th>
						<th class="dt-body-center">No. Invoice</th>
						<th class="dt-body-center">USD</th>
						<th class="dt-body-center">Rate</th>
						<th class="dt-body-center">DPP</th>
						<th class="dt-body-center">PPN</th>
						<th class="dt-body-center">Total</th>
					</tr>
					<!-- <tr>
						<th rowspan="2" class="custom-tables align-text">No</th>
						<th colspan="4" class="custom-tables align-text">PR</th>
						<th colspan="3" class="custom-tables align-text">PO</th>
						<th rowspan="2" class="custom-tables align-text">Deskripsi</th>
						<th rowspan="2" class="custom-tables align-text">Qty</th>
						<th rowspan="2" class="custom-tables align-text">Unit</th>
						<th colspan="3" class="custom-tables align-text">UOM</th>
						<th colspan="3" class="custom-tables align-text">Amount</th>
						<th colspan="4" class="custom-tables align-text">BTB</th>
						<th colspan="2" class="custom-tables align-text">No BC</th>
					</tr>
					<tr>
						<th class="custom-tables">No</th>
						<th class="custom-tables">Kode</th>
						<th class="custom-tables">Qty</th>
						<th class="custom-tables">Unit</th>
						<th class="custom-tables">No</th>
						<th class="custom-tables">PO</th>
						<th class="custom-tables">TOP</th>
						<th class="custom-tables">IDR</th>
						<th class="custom-tables">SGD</th>
						<th class="custom-tables">USD</th>
						<th class="custom-tables">IDR</th>
						<th class="custom-tables">SGD</th>
						<th class="custom-tables">USD</th>
						<th class="custom-tables">No</th>
						<th class="custom-tables">Tgl</th>
						<th class="custom-tables">Qty</th>
						<th class="custom-tables">Unit</th>
						<th class="custom-tables">No</th>
						<th class="custom-tables">Tgl</th>
					</tr> -->
				</thead>
				<tbody>
					<?php
					if (isset($data) && sizeof($data) > 0) {
						$no = 1;
						$total_harga_dpp = 0;
						$total_harga_ppn = 0;
						$total_harga_total_perbaris = 0;
						foreach ($data as $h) {
							$faktur_pajak = strtolower($h['note']);
							$faktur_pajak = str_replace("faktur pajak", "", $faktur_pajak);
							$faktur_pajak = preg_replace('/\D/', '', $faktur_pajak);
							$harga_dpp = $h['invoice_price'] * $h['rate'];
							if(substr($faktur_pajak,0,2)=='01')
								$harga_ppn = (float)$harga_dpp * ((float)$h['ppn']/100);
							else
								$harga_ppn = 0;
							$harga_total_per_baris = $harga_ppn + $harga_dpp;
							$total_harga_dpp += $harga_dpp;
							$total_harga_ppn += $harga_ppn;
							$total_harga_total_perbaris += $harga_total_per_baris;
					?>
							<tr>
								<td class="center_numb"><?php echo $no; ?></td>
								<td class="left_numb"><?php echo $h['no_do']; ?></td>
								<td class="left_numb"><?php echo $h['name_eksternal']; ?></td>
								<td class="left_numb"><?php echo $faktur_pajak; ?></td>
								<td class="left_numb"><?php echo $h['tanggal_invoice']; ?></td>
								<td class="left_numb"><?php echo $h['no_invoice']; ?></td>
								<td class="right_numb"><?php echo $h['symbol'] . number_format($h['invoice_price'], 2, ',', '.'); ?></td>
								<td class="right_numb"><?php echo $h['rate']; ?></td>
								<td class="right_numb"><?php echo 'Rp' . number_format($harga_dpp, 2, ',', '.'); ?></td>
								<td class="right_numb"><?php echo 'Rp' . number_format($harga_ppn, 2, ',', '.'); ?></td>
								<td class="right_numb"><?php echo 'Rp' . number_format($harga_total_per_baris, 2, ',', '.'); ?></td>
							</tr>
					<?php

							$no++;
						}
					} ?>
					<tr>
						<td class="left_numb" colspan="6">
							<b>Total :
						</td>
						<td></td>
						<td></td>
						<td class="right_numb"><?php echo 'Rp' . number_format($total_harga_dpp, 2, ',', '.'); ?></td>
						<td class="right_numb"><?php echo 'Rp' . number_format($total_harga_ppn, 2, ',', '.'); ?></td>
						<td class="right_numb"><?php echo 'Rp' . number_format($total_harga_total_perbaris, 2, ',', '.'); ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript" src="https://oss.sheetjs.com/sheetjs/xlsx.full.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		var pesan = '<?php echo $success; ?>';
		var tgl_masuk = '<?php echo $tanggal_masuk; ?>';
		var tgl_keluar = '<?php echo $tanggal_keluar; ?>';

		$('#btn_print').on('click', function() {
			var divToPrint = document.getElementById("listreportpembelian");
			newWin = window.open('', '', 'height=700,width=700');
			newWin.document.write(divToPrint.outerHTML);
			newWin.print();
			newWin.close();
		});

		$('#btn_download').click(function() {
			var doc = new jsPDF("l", "mm", "a4");
			var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
			var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

			// FOOTER
			doc.setTextColor(50);
			doc.setFontSize(12);
			doc.setFontStyle('helvetica', 'arial', 'sans-serif', 'bold');
			doc.text($('#titlePerusahaan').html(), pageWidth / 2, 20, 'center');
			doc.text($('#namaPerusahaan').html(), pageWidth / 2, 25, 'center');
			doc.text($('#titleTanggal').html(), pageWidth / 2, 30, 'center');

			doc.autoTable({
				html: '#listreportpembelian',
				headStyles: {
					fontSize: 7,
					valign: 'middle',
					halign: 'center',
				},
				bodyStyles: {
					fontSize: 6,
				},
				theme: 'plain',
				styles: {
					fontSize: 6,
					lineColor: [0, 0, 0],
					lineWidth: 0.35,
					cellWidth: 'auto',

				},
				margin: 4,
				/*columnStyles	: {
					0: {halign: 'center'},
					1: {halign: 'left'},
					2: {halign: 'left'},
					3: {halign: 'left'},
					4: {halign: 'left'},
					5: {halign: 'right'},
					6: {halign: 'left'},
					7: {halign: 'left'},
					8: {halign: 'left'},
					9: {halign: 'center'},
					10: {halign: 'right'},
					11: {halign: 'center'},
					12: {halign: 'right'}
				},*/
				rowPageBreak: 'auto',
				showHead: 'firstPage',
				showFoot: 'lastPage',
				startY: 40
			});
			doc.save('Report <?php echo $report; ?>.pdf');
		});
		$('#refresh_report').on('click', function() {
			$('#div_report').removeData('');
			$('#div_report').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');

			var data = {
				'tanggal_masuk': '<?php echo date('Y/m/d', strtotime($tanggal_masuk)); ?>',
				'tanggal_keluar': '<?php echo date('Y/m/d', strtotime($tanggal_keluar)); ?>',
				// 'jenis_report': jenis_report
			};

			$('#div_report').load('<?php echo base_url('report_penjualan/view_report/'); ?>', data, function(response, status, xhr) {
				if (status != "error") {} else {
					swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
				}
			});
		});

		$('#export_excel').click(function() {
			var url = '<?php echo base_url(); ?>report_penjualan/export_excel';
			var tanggal_awal = '<?php echo $tanggal_masuk; ?>';
			var tanggal_akhir = '<?php echo $tanggal_keluar; ?>';
			var report = '<?php echo $report ?>';

			var form = $("<form action='" + url + "' method='post' target='_blank'>" +
				"<input type='hidden' name='tanggal_awal' value='" + tanggal_awal + "' />" +
				"<input type='hidden' name='tanggal_akhir' value='" + tanggal_akhir + "' />" +
				"<input type='hidden' name='report' value='" + report + "' />" +
				"</form>");
			$('body').append(form);
			form.submit();
		});		
    
		function xls(table, title) {
			if (Array.isArray(table)) {
				var uri = 'data:application/vnd.ms-excel;base64,',
					template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>',
					base64 = function(s) {
						return window.btoa(unescape(encodeURIComponent(s)))
					},
					format = function(s, c) {
						return s.replace(/{(\w+)}/g, function(m, p) {
							return c[p];
						})
					}
				ctx = [];
				// console.log(name);
				// console.log(template);
				table.forEach(function(entry) {
					tableObj = document.getElementById(entry)
					ctx.push({
						worksheet: entry,
						table: tableObj.innerHTML
					});
				});
				console.log(ctx);
				// window.download = "filename.xls";
				// window.location.href = uri + base64(format(template, ctx));

				var link = document.createElement("a");
				link.download = title + '.xls';
				newctx='';
				ctx.forEach(function(ctxEl){
					newctx = newctx + base64(format(template, ctxEl))
				})
				link.href = uri + base64(format(template, newctx));
				link.click();


			} else {
				var uri = 'data:application/vnd.ms-excel;base64,',
					template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>',
					base64 = function(s) {
						return window.btoa(unescape(encodeURIComponent(s)))
					},
					format = function(s, c) {
						return s.replace(/{(\w+)}/g, function(m, p) {
							return c[p];
						})
					}



				// console.log(name);
				// console.log(template);
				if (!table.nodeType) table = document.getElementById(table)
				var ctx = {
					worksheet: 'Worksheet',
					table: table.innerHTML
				}
				// window.download = "filename.xls";
				// window.location.href = uri + base64(format(template, ctx));

				var link = document.createElement("a");
				link.download = title + '.xls';
				link.href = uri + base64(format(template, ctx));
				link.click();
			}

		}

		function xlsx(table, title) {
			console.log(Array.isArray(table));
			var workbook = XLSX.utils.book_new()
			if (Array.isArray(table)) {
				let a = [];
				table.forEach(function(entry) {
					var config = {
						raw: true,
						type: 'string'
					};
					var tbl = document.getElementById(entry);
					var wb = XLSX.utils.table_to_sheet(tbl, config);
					XLSX.utils.book_append_sheet(workbook, wb, entry);
				})
        
				XLSX.writeFile(workbook, title + '.xls');
			} else {
				// let a = [];
				var config = {
					raw: true,
					type: 'string',
					cellStyles: true
				};
				// var tbl = document.getElementById(table);
				// var wb = XLSX.utils.table_to_sheet(tbl, config);
				// console.log(wb)
				// a = a.concat(XLSX.utils.sheet_to_json(wb, { header: 1, raw: false })).concat(['']);


				// let worksheet = XLSX.utils.json_to_sheet(a, { skipHeader: true, cellStyles : true });

				// const new_workbook = XLSX.utils.book_new();
				// XLSX.utils.book_append_sheet(new_workbook, worksheet, "worksheet");
				// XLSX.writeFile(new_workbook, title+'.xlsx');
				// var canvas = document.querySelector('#barChart2');
				//creates image
				// var canvasImg = canvas.toDataURL("image/png", 1.0);
				// console.log(canvasImg);
				var tbl = document.getElementById(table);
				console.log(tbl);
				var wb = XLSX.utils.table_to_book(tbl, config);
				// wb.Sheets['my-sheet']['!images'] = [
				// {
				// name: 'image1.jpg',
				// data: canvasImg,
				// opts: { base64: true },
				// position: {
				// type: 'twoCellAnchor',
				// attrs: { editAs: 'oneCell' },
				// from: { col: 2, row : 2 },
				// to: { col: 6, row: 5 }
				// }
				// }
				// ];


				var read_opts = {
					cellStyles: true
				}

				// XLSX.readFile(wb, read_opts);
				XLSX.writeFile(wb, title + '.xlsx', read_opts);
			}
		}
    
		function htmlToExcel(tableName, fName, title) {
			$(':button[id="download_excel"]').prop('disabled', true);
			$(':button[id="download_excel"]').html("Sedang Proses...");
			arrTable = [];
			$('table[name="' + tableName + '"]').each(function() {
				if (this.id) {
					if (this.id != undefined && this.value != '') arrTable.push(this.id);
				}
			})
			setTimeout(function() {
				if (fName == "xls") {
					if (arrTable.length < 2)
						xls(tableName, title);
					else
						xls(arrTable, title);
				}
				if (fName == "xlsx") {
					if (arrTable.length < 2)
						xlsx(tableName, title);
					else
						xlsx(arrTable, title);

				}


				// $(':button[id="download_excel"]').prop('disabled', false);
				// $(':button[id="download_excel"]').html("<i class='fa fa-file-excel-o '></i> Excel");
				// $('#myModal').modal('hide');
				// toastr.info("Download sukses!");
			}, 1000);

		}
    // $('#export_excel').click(function() {
    //   htmlToExcel('listreportpembelian', 'xlsx', 'REPORT PENJUALAN '+tgl_masuk+" - "+tgl_keluar)
		// });

		function objectSize(obj) {
			var size = 0;
			for (key in obj) {
				if (obj.hasOwnProperty(key)) size++;
			}
			return size;
		}

		if (pesan == 0) {
			swal("Maaf!", "Laporan periode " + tgl_masuk + " S.D " + tgl_keluar + " tidak ditemukan...!!!", "info");
		}
	});
</script>