<style>
	.changed_status{cursor:pointer;text-decoration: underline;color:#96b6e8;}
	.changed_status:hover{color:#ff8c00}
	.custom-tables, th{text-align:center;vertical-align:middle;}
	.custom-tables.align-text, th{vertical-align:middle;}
	.dt-body-right{text-align:right;}
	#titleTanggal{margin-bottom:10px;}
	.center_numb{text-align:center;}
	.left_numb{text-align:left;}
	.right_numb{text-align:right;}
	.tableReport {
		box-sizing: border-box;
		text-align: left;
		width: 90%;
		margin: 10px auto;
		padding-bottom:5px;
		page-break-before: always;
		z-index:99;
	}
	.table-scroll {
		position:relative;
		max-width:100%;
		margin:auto;
		overflow:hidden;
		border:1px solid #f0fffd;
	}
	.table-wrap {
		width:100%;
		overflow:auto;
	}
	.table-scroll table {
		width:100%;
		margin:auto;
		border-spacing:0;
	}
	.table-scroll th, .table-scroll td {
		padding:5px 10px;
		border:1px solid #000;
		background:#fff;
		white-space:nowrap;
		vertical-align:top;
	}
</style>

<div class="card-box">
	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<a class="btn btn-icon waves-effect waves-light btn-success m-b-5" data-toggle="tooltip" data-placement="top" title="Export to Excel" id="export_excel">
					<i class="fa fa-file-excel-o"></i>
				</a>
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download PDF" id="btn_download">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>
	
	<div class="col-md-12 text-center titleReport">
		<h2 id="titlePerusahaan">LAPORAN <?php echo strtoupper($report); ?></h2>
		<h2 id="namaPerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
		<h3 id="titleTanggal">Untuk Tgl : <?php echo $tanggal_masuk; ?></h3>
	</div>
	<hr>
	<div id="table-scroll" class="table-scroll">
		<div class="table-wrap">
			<table id="listreportpembelian" class="table table-striped table-bordered table-responsive dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th class="custom-tables align-text">Customer</th>
						<th class="custom-tables align-text">Part Number</th>
						<th class="custom-tables align-text">UOM</th>
						<th class="custom-tables align-text">Qty Pcs</th>
						<th class="custom-tables align-text">No PO</th>
						<th class="custom-tables align-text">Qty Per PO</th>
						<th class="custom-tables align-text">Qty Box</th>
						<th class="custom-tables align-text">Detail Box</th>
						<th class="custom-tables align-text">No DO</th>
						<th class="custom-tables align-text">No. PI</th>
						<th class="custom-tables align-text">Remark</th>
					</tr>
				</thead>
				<tbody>
			<?php $no = 1;
				foreach($data as $kdata) {?>
					<tr>
						<td class="left_numb"><?php echo $kdata['customer']; ?></td>
						<td class="left_numb"><?php echo $kdata['part_no']; ?></td>
						<td class="center_numb"><?php echo $kdata['uom']; ?></td>
						<td class="center_numb"><?php echo number_format($kdata['qty_pcs'],0,',','.'); ?></td>
						<td class="left_numb"><?php echo $kdata['no_po']; ?></td>
						<td class="center_numb"><?php echo number_format($kdata['qtyperpo'],0,',','.'); ?></td>
						<td class="center_numb"><?php echo number_format($kdata['qty_box'],0,',','.'); ?></td>
						<td class="center_numb"><?php echo number_format($kdata['detail_box'],0,',','.'); ?></td>
						<td class="center_numb"><?php echo $kdata['no_do']; ?></td>
						<td class="center_numb"><?php echo $kdata['no_pi']; ?></td>
						<td class="left_numb"><?php echo $kdata['remark']; ?></td>
					</tr>
			<?php
				} ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script>

$(document).ready(function() {
	var pesan = '<?php echo $msg['message']; ?>';
	var tgl_masuk = '<?php echo $tanggal_masuk; ?>';
	var report = '<?php echo $reports; ?>';
	
	$('#btn_download').click(function () {
		var doc = new jsPDF("l", "mm", "a4");
		var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
		var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
		
		// FOOTER
		doc.setTextColor(50);
		doc.setFontSize(12); 
		doc.setFontStyle('helvetica','arial','sans-serif','bold');
		doc.text($('#titlePerusahaan').html(), pageWidth / 2, 20, 'center');
		doc.text($('#namaPerusahaan').html(), pageWidth / 2, 25, 'center');
		doc.text($('#titleTanggal').html(), pageWidth / 2, 30, 'center');
		
		doc.autoTable({
			html 			: '#listreportpembelian',
			headStyles		: {
				fontSize 	: 7,
				valign		: 'middle', 
				halign		: 'center',
			},
			bodyStyles		: {
				fontSize 	: 6,
			},
			theme			: 'plain',
			styles			: {
				fontSize : 6, 
				lineColor: [0, 0, 0],
				lineWidth: 0.35,
				cellWidth : 'auto'
			},
			margin 			: 4,
			columnStyles	: {
				0: {halign: 'left'},
				1: {halign: 'left'},
				2: {halign: 'center'},
				3: {halign: 'center'},
				4: {halign: 'left'},
				5: {halign: 'center'},
				6: {halign: 'center'},
				7: {halign: 'center'},
				8: {halign: 'left'},
				9: {halign: 'left'},
				10: {halign: 'left'}
			},
			rowPageBreak	: 'auto',
			showHead 		: 'firstPage',
			showFoot		: 'lastPage',
			startY			: 40
		});
		doc.save('PENGAJUAN <?php echo $report; ?>.pdf');
	});
	
	$('#export_excel').click(function () {
		
		var url = '<?php echo base_url(); ?>report_pengiriman/export_excel'; 
		  
		var form = $("<form action='" + url + "' method='post' target='_blank'>" +
			"<input type='hidden' name='tanggal_awal' value='" + tgl_masuk + "' />" +
			"<input type='hidden' name='report' value='" + report + "' />" +
			"</form>");
		$('body').append(form);
		form.submit();
	});
	
	function objectSize(obj) {
		var size = 0;
		for (key in obj) {
			if (obj.hasOwnProperty(key)) size++;
		}
		return size;
	}
	
	if(pesan == 1){
	 	swal("Maaf!", "Laporan Tanggal "+tgl_masuk+" tidak ditemukan...!!!", "info");
	}
});
</script>