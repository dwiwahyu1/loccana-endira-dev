<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Report_Pengiriman_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function get_report_pengiriman($data) {
		$sql 	= 'CALL report_pengiriman(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['tanggal_masuk'],
			$data['jenis_report']
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}