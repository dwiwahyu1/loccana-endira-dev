<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Report_Pengiriman extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('report_pengiriman/report_pengiriman_model');
		$this->load->library('excel');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'report_pengiriman/views/index');
	}

	public function report_detail() {
		$this->load->view('report_detail_view');
	}

	public function view_report() {
		$jenis_report = $this->Anti_sql_injection($this->input->post('jenis_report', TRUE));
		$tanggalMasuk = $this->Anti_sql_injection($this->input->post('tanggal_masuk', TRUE));

		if($jenis_report) {
			$tglMsk = explode("/", $tanggalMasuk);
			$tanggal_masuk = date('Y-m-d', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0]));

			if ($jenis_report == 'lokal') {
				$sendData = array(
					'jenis_report'		=> 2,
					'tanggal_masuk'		=> $tanggal_masuk
				);
			}else if ($jenis_report == 'import') {
				$sendData = array(
					'jenis_report'		=> 1,
					'tanggal_masuk'		=> $tanggal_masuk
				);
			}
			$result = $this->report_pengiriman_model->get_report_pengiriman($sendData);
			$data = array(
				'data'				=> $result,
				'report'			=> 'PENGIRIMAN '.strtoupper($jenis_report),
				'reports'			=> 'Pengiriman '. $jenis_report,
				'tanggal_masuk'		=> date('d-F-Y', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0]))
			);
			
			if(count($result) > 0){
				$msg = 'Lihat report';
				
				$data['msg'] = array(
					'message'	=> 0
				);
				
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}else{
				$msg = 'Gagal melihat report';
				
				$data['msg'] = array(
					'message'	=> 1
				);
				
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}
			
			$this->load->view('report_pengiriman_view', $data);
		}else {
			$result = array('success' => false);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}
	
	public function export_excel()
    {
		$jenis_report = $this->Anti_sql_injection($this->input->post('report', TRUE));
		$tanggalMasuk = $this->Anti_sql_injection($this->input->post('tanggal_awal', TRUE));
		
		$datemasuk=date_create($tanggalMasuk);
		$date_masuk_fix = date_format($datemasuk,"Y-m-d");
		
		if($jenis_report == 'Pengiriman lokal') {
			$val_report = 1;
			$lokal = array('tanggal_masuk' => $date_masuk_fix, 'jenis_report' => $val_report);
			$result_lokal = $this->report_pengiriman_model->get_report_pengiriman($lokal);
			
			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getActiveSheet()->setTitle("Report Pengiriman Lokal");
			$objPHPExcel->getProperties()->setCreator("System")
				->setLastModifiedBy("System")
				->setTitle("Report Pengiriman Lokal")
				->setSubject("Report Bea Cukai")
				->setDescription("Report Pengiriman Lokal")
				->setKeywords("Pengiriman Lokal")
				->setCategory("Report");
							 
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'LAPORAN '.strtoupper($jenis_report))
				->setCellValue('A2', 'PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA')
				->setCellValue('A3', 'PERIODE '.strtoupper($tanggalMasuk));
								
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A5', 'No')
				->setCellValue('B5', 'Customer')
				->setCellValue('C5', 'Part Number')
				->setCellValue('D5', 'UOM')
				->setCellValue('E5', 'Qty Pcs')
				->setCellValue('F5', 'No PO')
				->setCellValue('G5', 'Qty Per PO')
				->setCellValue('H5', 'Qty Box')
				->setCellValue('I5', 'Detail Box')
				->setCellValue('J5', 'No DO')
				->setCellValue('K5', 'No. PI')
				->setCellValue('L5', 'Remark');
			
			$styleArray = array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			
						
			$i = 7;
			$no = 1;
			foreach($result_lokal as $lokal){
				
				$objPHPExcel->setActiveSheetIndex()->getStyle('A5:L'.$i)->applyFromArray($styleArray);
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B'.$i, $lokal['customer'])
					->setCellValue('C'.$i, $lokal['part_no'])
					->setCellValue('F'.$i, $lokal['stock_description'])
					->setCellValue('G'.$i, $lokal['no_po'])
					->setCellValue('J'.$i, $lokal['detail_box'])
					->setCellValue('K'.$i, $lokal['no_do'])
					->setCellValue('L'.$i, $lokal['no_pi']);
				
				// //CUSTOM COLUMN -->
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$i, $no)->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('D'.$i, $lokal['uom'])->getStyle('D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('E'.$i, $lokal['qty_pcs'])->getStyle('E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('H'.$i, $lokal['qtyperpo'])->getStyle('H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				
				$objPHPExcel->setActiveSheetIndex(0)		
					->setCellValue('I'.$i, $lokal['qty_box'])->getStyle('I'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(17);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(17);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(17);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
				
				$i++;
				$no++;
				
				
			}
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:L1');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:L2');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:L2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:L3');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:L3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:L4');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:A6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:B6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:B6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C5:C6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('C5:C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('C5:C6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D5:D6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('D5:D6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('D5:D6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E5:E6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('E5:E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('E5:E6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F5:F6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('F5:F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('F5:F6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G5:G6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('G5:G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('G5:G6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H5:H6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('H5:H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('H5:H6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I5:I6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J5:J6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('K5:K6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L5:L6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:L6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Pengiriman Lokal '.$tanggalMasuk.'.xls"');
			
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit; 

		}else{
			
			$val_report = 2;
			$import = array('tanggal_masuk' => $date_masuk_fix, 'jenis_report' => $val_report);
			$result_import = $this->report_pengiriman_model->get_report_pengiriman($import);
			
			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getActiveSheet()->setTitle("Report Pengiriman Import");
			$objPHPExcel->getProperties()->setCreator("System")
				->setLastModifiedBy("System")
				->setTitle("Report Pengiriman Import")
				->setSubject("Report Bea Cukai")
				->setDescription("Report Pengiriman Import")
				->setKeywords("Pengiriman Import")
				->setCategory("Report");
							 
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'LAPORAN '.strtoupper($jenis_report))
				->setCellValue('A2', 'PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA')
				->setCellValue('A3', 'PERIODE '.strtoupper($tanggalMasuk));
								
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A5', 'No')
				->setCellValue('B5', 'Customer')
				->setCellValue('C5', 'Part Number')
				->setCellValue('D5', 'UOM')
				->setCellValue('E5', 'Qty Pcs')
				->setCellValue('F5', 'No PO')
				->setCellValue('G5', 'Qty Per PO')
				->setCellValue('H5', 'Qty Box')
				->setCellValue('I5', 'Detail Box')
				->setCellValue('J5', 'No DO')
				->setCellValue('K5', 'No. PI')
				->setCellValue('L5', 'Remark');
			
			$styleArray = array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			
						
			$i = 7;
			$no = 1;
			foreach($result_import as $import){
				
				$objPHPExcel->setActiveSheetIndex()->getStyle('A5:L'.$i)->applyFromArray($styleArray);
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B'.$i, $import['customer'])
					->setCellValue('C'.$i, $import['part_no'])
					->setCellValue('F'.$i, $import['stock_description'])
					->setCellValue('G'.$i, $import['no_po'])
					->setCellValue('J'.$i, $import['detail_box'])
					->setCellValue('K'.$i, $import['no_do'])
					->setCellValue('L'.$i, $import['no_pi']);
				
				// //CUSTOM COLUMN -->
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$i, $no)->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('D'.$i, $import['uom'])->getStyle('D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('E'.$i, $import['qty_pcs'])->getStyle('E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('H'.$i, $import['qtyperpo'])->getStyle('H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				
				$objPHPExcel->setActiveSheetIndex(0)		
					->setCellValue('I'.$i, $import['qty_box'])->getStyle('I'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(17);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(17);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(17);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
				
				$i++;
				$no++;
				
				
			}
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:L1');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:L2');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:L2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:L3');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:L3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:L4');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:A6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:B6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:B6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C5:C6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('C5:C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('C5:C6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D5:D6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('D5:D6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('D5:D6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E5:E6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('E5:E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('E5:E6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F5:F6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('F5:F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('F5:F6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G5:G6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('G5:G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('G5:G6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H5:H6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('H5:H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('H5:H6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I5:I6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J5:J6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('K5:K6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L5:L6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:L6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Pengiriman Import '.$tanggalMasuk.'.xls"');
			
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit; 
			
		}
	}
}