<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Customer_Specification_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	/**
      * This function is get the list data in cust_spec table
      * @param : $params is where condition for select query
      */

	public function lists($params = array())
	{
		$sql_all 	= 'CALL custspec_list_all(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				$params['filter']
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL custspec_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total_row,
			'total' => $total_row,
		);

		return $return;
	}

	/**
      * This function is get data in cust_spec table by id
      * @param : $id is where condition for select query
      */

	public function detail($id)
	{
		$sql 	= 'CALL custspec_search_id(?)';

		$query 	= $this->db->query($sql,array(
				$id
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is used to Insert Record in cust_spec table
      * @param : $data - record array 
      */

	public function add_customer_specification($data)
	{
		$sql 	= 'CALL custspec_add(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id_produk'],
				$data['id_eksternal'],
				$data['approved_laminate'],
				$data['laminate_grade'],
				$data['laminate'],
				$data['thickness'],
				$data['base_cooper'],
				$data['co_logo'],
				$data['type_logo'],
				$data['img_logo'],
				$data['ul_recognition'],
				$data['date_code'],
				$data['date_type'],
				$data['date_img'],
				$data['solder_resist_type'],
				$data['solder_resist'],
				$data['legend_circuit'],
				$data['legend_component'],
				$data['carbon'],
				$data['carbon_type'],
				$data['carbon_spec'],
				$data['peelable'],
				$data['peelable_type'],
				$data['peelable_spec'],
				$data['plating'],
				$data['plating_spec'],
				$data['process_molding'],
				$data['process_molding_remarks'],
				$data['v_cut'],
				$data['v_cut_spec'],
				$data['v_cut_img'],
				$data['tin_roll'],
				$data['tin_roll_spec'],
				$data['finishing'],
				$data['packing_standard'],
				$data['others_req']
			));

		$this->db->close();
		$this->db->initialize();
	}

	/**
      * This function is used to Update Record in cust_spec table
      * @param : $data - updated array 
      */

	public function edit_customer_specification($data)
	{
		$sql 	= 'CALL custspec_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id'],
				$data['id_produk'],
				$data['id_eksternal'],
				$data['approved_laminate'],
				$data['laminate_grade'],
				$data['laminate'],
				$data['thickness'],
				$data['base_cooper'],
				$data['co_logo'],
				$data['type_logo'],
				$data['img_logo'],
				$data['ul_recognition'],
				$data['date_code'],
				$data['date_type'],
				$data['date_img'],
				$data['solder_resist_type'],
				$data['solder_resist'],
				$data['legend_circuit'],
				$data['legend_component'],
				$data['carbon'],
				$data['carbon_type'],
				$data['carbon_spec'],
				$data['peelable'],
				$data['peelable_type'],
				$data['peelable_spec'],
				$data['plating'],
				$data['plating_spec'],
				$data['process_molding'],
				$data['process_molding_remarks'],
				$data['v_cut'],
				$data['v_cut_spec'],
				$data['v_cut_img'],
				$data['tin_roll'],
				$data['tin_roll_spec'],
				$data['finishing'],
				$data['packing_standard'],
				$data['others_req'],
				$data['uc1'],
				$data['uc2'],
				$data['shearing_cut']
			));

		$this->db->close();
		$this->db->initialize();
	}

	/**
     * This function is used to delete customer specification
     * @param: $id - id of cust_spec table
     */
	function delete_customer_specification($id) {
		$this->db->where('id', $id);  
		$this->db->delete('t_cust_spec'); 

		$this->db->close();
		$this->db->initialize();
	}

	/**
      * This function is used to Update Record in cust_spec table
      * @param : $data - updated array 
      */

	public function approval_customer_specification($data)
	{
		$user_id = $this->session->userdata['logged_in']['user_id'];

		$sql 	= 'UPDATE t_cust_spec SET status=?, notes=?, approval_date=NOW() WHERE id=?;';

		$this->db->query($sql,
			array(
				$data['status'],
				$data['notes'],
				$data['id']
			));

		$this->db->close();
		$this->db->initialize();
	}

	/**
      * This function is get data in gerber_data table by id
      * @param : $id is where condition for select query
      */

	public function gerber_data($id)
	{
		$sql 	= 'SELECT id,title,doc FROM t_gerber_data WHERE id_custspec=?';

		$query 	= $this->db->query($sql,array(
				$id
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is used to Update Record in cust_spec table
      * @param : $data - updated array 
      */

	public function upload_gerberdata($data)
	{
		if($data['deletedItems'] != ''){
			$array_deleted = explode(',',$data['deletedItems']);
			$this->db->where_in('id', $array_deleted);  
			$this->db->delete('t_gerber_data');
		} 

		if($data['title'] != ''){
			$array_title = explode(',',$data['title']);

			$datagerberLen = count($array_title);

			for($j=0;$j<$datagerberLen;$j++){
				$doc = isset($data['doc'][$j])?$data['doc'][$j]:NULL;

				$sql 	= 'CALL gerberdata_add(?, ?, ?)';

				$this->db->query($sql,array(
					$data['id'],
					$array_title[$j],
					$doc
				));
			}
		}

		$this->db->close();
		$this->db->initialize();
	}
}