<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_Specification extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('quotation/quotation_model');
        $this->load->model('customer_specification/customer_specification_model');
		$this->load->library('log_activity');
    }

    /**
     * anti sql injection
     * @return string
     */
    public function Anti_sql_injection($string) {
        $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
        return $string;
    }

    /**
     * upload file
     * @return string
     */
    public function upload($data) {
        $this->load->library('upload');
        $config = array(
            'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/specification",
            'upload_url' => base_url() . "uploads/specification",
            'encrypt_name' => TRUE,
            'overwrite' => FALSE,
            'allowed_types' => 'jpg|jpeg|png',
            'max_size' => '10000'
        );

        $this->upload->initialize($config);

        $this->upload->do_upload($data);
        // General result data
        $result = $this->upload->data();

        // Load resize library
        $this->load->library('image_lib');

        // Resizing parameters large
        $resize = array
            (
            'source_image' => $result['full_path'],
            'new_image' => $result['full_path'],
            'maintain_ratio' => TRUE,
            'width' => 300,
            'height' => 300
        );

        // Do resize
        $this->image_lib->initialize($resize);
        $this->image_lib->resize();
        $this->image_lib->clear();

        // Add our stuff
        $img = $result['file_name'];
            
        return $img;
    }

    public function upload_gerber($data) {
        $this->load->library('upload');
        $config = array(
            'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/gerber_data",
            'upload_url' => base_url() . "uploads/gerber_data",
            'encrypt_name' => TRUE,
            'overwrite' => FALSE,
            'allowed_types' => 'pdf|txt|doc|docx',
            'max_size' => '10000'
        );

        $this->upload->initialize($config);

        $this->upload->do_upload($data);
        // General result data
        $result = $this->upload->data();

        // Load resize library
        $this->load->library('image_lib');

        // Resizing parameters large
        $resize = array
            (
            'source_image' => $result['full_path'],
            'new_image' => $result['full_path'],
            'maintain_ratio' => TRUE,
            'width' => 300,
            'height' => 300
        );

        // Do resize
        $this->image_lib->initialize($resize);
        $this->image_lib->resize();
        $this->image_lib->clear();

        // Add our stuff
        $file = $result['file_name'];
            
        return $file;
    }

    /**
      * This function is redirect to index customer specification page
      * @return Void
      */
    public function index() {
        $this->template->load('maintemplate', 'customer_specification/views/index');
    }

    /**
      * This function is used for showing customer specifications list
      * @return Array
      */
    function lists() {
        $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
        $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
        $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
        $order = $this->input->get_post('order');
        $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
        $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

        $order_fields = array('', 'id', 'name_eksternal', 'stock_name');

        $search = $this->input->get_post('search');

        $search_val = (!empty($search['value'])) ? $search['value'] : null;

        $search_value = $this->Anti_sql_injection($search_val);

        // Build params for calling model
        $params['limit'] = (int) $length;
        $params['offset'] = (int) $start;
        $params['order_column'] = $order_fields[$order_column];
        $params['order_dir'] = $order_dir;
        $params['filter'] = $search_value;

        $list = $this->customer_specification_model->lists($params);

        $result["recordsTotal"] = $list['total'];
        $result["recordsFiltered"] = $list['total_filtered'];
        $result["draw"] = $draw;

        $data = array();

        $i = $params['offset'];
        foreach ($list['data'] as $k => $v) {
            $i++;

            $status = $v['status'];
            if($v['status'] != "Preparing"){
                $status = '<div class="changed_status" onClick="detail_status(\'' . $v['id'] . '\')">';
                $status .=      $v['status'];
                $status .= '</div>';
            }

            $gerber = '<div class="btn-group">';
            $gerber .= '   <button class="btn btn-inverse" type="button" data-toggle="tooltip" data-placement="top" title="Gerber Data" onClick="uploadgerber_data(\'' . $v['id'] . '\')">';
            $gerber .= '       <i class="fa fa-file-text-o"></i>';
            $gerber .= '   </button>';
            $gerber .= '</div>';

            $actions = '';
            if($v['status'] == "Preparing"){
                $actions .= '<div class="btn-group">';
                $actions .= '   <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="editcustomer_specification(\'' . $v['id'] . '\')">';
                $actions .= '       <i class="fa fa-edit"></i>';
                $actions .= '   </button>';
                $actions .= '</div>';
            }

            if($v['status'] == "Preparing"){
                $actions .= '<div class="btn-group">';
                $actions .= '   <button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Approval" onClick="approval_customer_specification(\'' . $v['id'] . '\')">';
                $actions .= '       <i class="fa fa-check"></i>';
                $actions .= '   </button>';
                $actions .= '</div>';
            }

            $actions .= '<div class="btn-group">';
            $actions .= '   <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deletecustomer_specification(\'' . $v['id'] . '\')">';
            $actions .= '       <i class="fa fa-trash"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';
            $actions .= '<div class="btn-group">';
            $actions .= '   <button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Details"
                        onClick="details_customer_specification(\'' . $v['id'] . '\')">';
            $actions .= '       <i class="fa fa-book"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';
            
            array_push($data, array(
                $i,
                $v['name_eksternal'],
                $v['stock_name'],
                $v['approved_laminate'],
                $v['laminate_grade'],
                $v['lam_thk_bc'],
                $status,
                $gerber,
                $actions
                    )
            );
        }

        $result["data"] = $data;

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is redirect to add customer specification page
      * @return Void
      */
    public function add() {
        $customer = $this->quotation_model->customer();
        $item = $this->quotation_model->item();

        $data = array(
            'customer' => $customer,
            'item' => $item
        );

        $this->load->view('add_modal_view',$data);
    }

    /**
      * This function is used to add customer specification data
      * @return Array
      */
    public function add_customer_specification() {
        $id_eksternal = $this->Anti_sql_injection($this->input->post('id_eksternal', TRUE));
        $id_produk = $this->Anti_sql_injection($this->input->post('id_produk', TRUE));
        $approved_laminate = $this->Anti_sql_injection($this->input->post('approved_laminate', TRUE));
        $laminate_grade = $this->Anti_sql_injection($this->input->post('laminate_grade', TRUE));
        $laminate = $this->Anti_sql_injection($this->input->post('laminate', TRUE));
        $thickness = $this->Anti_sql_injection($this->input->post('thickness', TRUE));
        $base_cooper = $this->Anti_sql_injection($this->input->post('base_cooper', TRUE));
        $co_logo = $this->Anti_sql_injection($this->input->post('co_logo', TRUE));
        $type_logo = $this->Anti_sql_injection($this->input->post('type_logo', TRUE));
        $img_logo = NULL;
        $ul_recognition = $this->Anti_sql_injection($this->input->post('ul_recognition', TRUE));
        $date_code = $this->Anti_sql_injection($this->input->post('date_code', TRUE));
        $date_type = $this->Anti_sql_injection($this->input->post('date_type', TRUE));
        $date_img = NULL;
        $solder_resist_type = $this->Anti_sql_injection($this->input->post('solder_resist_type', TRUE));
        $solder_resist = $this->Anti_sql_injection($this->input->post('solder_resist', TRUE));
        $solder_resist_others = $this->Anti_sql_injection($this->input->post('solder_resist_others', TRUE));
        $legend_circuit = $this->Anti_sql_injection($this->input->post('legend_circuit', TRUE));
        $legend_circuit_others = $this->Anti_sql_injection($this->input->post('legend_circuit_others', TRUE));
        $legend_component = $this->Anti_sql_injection($this->input->post('legend_component', TRUE));
        $legend_component_others = $this->Anti_sql_injection($this->input->post('legend_component_others', TRUE));
        $carbon = $this->Anti_sql_injection($this->input->post('carbon', TRUE));
        $carbon_type = $this->Anti_sql_injection($this->input->post('carbon_type', TRUE));
        $carbon_spec = $this->Anti_sql_injection($this->input->post('carbon_spec', TRUE));
        $peelable = $this->Anti_sql_injection($this->input->post('peelable', TRUE));
        $peelable_type = $this->Anti_sql_injection($this->input->post('peelable_type', TRUE));
        $peelable_spec = $this->Anti_sql_injection($this->input->post('peelable_spec', TRUE));
        $plating = $this->Anti_sql_injection($this->input->post('plating', TRUE));
        $plating_spec = $this->Anti_sql_injection($this->input->post('plating_spec', TRUE));
        $process_molding = $this->Anti_sql_injection($this->input->post('process_molding', TRUE));
        $process_molding_remarks = $this->Anti_sql_injection($this->input->post('process_molding_remarks', TRUE));
        $v_cut = $this->Anti_sql_injection($this->input->post('v_cut', TRUE));
        $v_cut_spec = $this->Anti_sql_injection($this->input->post('v_cut_spec', TRUE));
        $v_cut_img = NULL;
        $tin_roll = $this->Anti_sql_injection($this->input->post('tin_roll', TRUE));
        $tin_roll_spec = $this->Anti_sql_injection($this->input->post('tin_roll_spec', TRUE));
        $finishing = $this->Anti_sql_injection($this->input->post('finishing', TRUE));
        $packing_standard = $this->Anti_sql_injection($this->input->post('packing_standard', TRUE));
        $packing_standard_others = $this->Anti_sql_injection($this->input->post('packing_standard_others', TRUE));
        $others_req = $this->Anti_sql_injection($this->input->post('others_req', TRUE));

        if ($_FILES['img_logo']['name']) {
            $img_logo = $this->upload('img_logo');
        }

        if ($_FILES['date_img']['name']) {
            $date_img = $this->upload('date_img');
        }

        if ($_FILES['v_cut_img']['name']) {
            $v_cut_img = $this->upload('v_cut_img');
        }

        $solder_resist = ($solder_resist == 'others') ? $solder_resist_others : $solder_resist;
        $legend_circuit = ($legend_circuit == 'others') ? $legend_circuit_others : $legend_circuit;
        $legend_component = ($legend_component == 'others') ? $legend_component_others : $legend_component;
        $packing_standard = ($packing_standard == 'others') ? $packing_standard_others : $packing_standard;

        $data = array(
            'id_eksternal' => $id_eksternal,
            'id_produk' => $id_produk,
            'approved_laminate' => $approved_laminate,
            'laminate_grade' => $laminate_grade,
            'laminate' => $laminate,
            'thickness' => $thickness,
            'base_cooper' => $base_cooper,
            'co_logo' => $co_logo,
            'type_logo' => $type_logo,
            'img_logo' => $img_logo,
            'ul_recognition' => $ul_recognition,
            'date_code' => $date_code,
            'date_type' => $date_type,
            'date_img' => $date_img,
            'solder_resist_type' => $solder_resist_type,
            'solder_resist' => $solder_resist,
            'legend_circuit' => $legend_circuit,
            'legend_component' => $legend_component,
            'carbon' => $carbon,
            'carbon_type' => $carbon_type,
            'carbon_spec' => $carbon_spec,
            'peelable' => $peelable,
            'peelable_type' => $peelable_type,
            'peelable_spec' => $peelable_spec,
            'plating' => $plating,
            'plating_spec' => $plating_spec,
            'process_molding' => $process_molding,
            'process_molding_remarks' => $process_molding_remarks,
            'v_cut' => $v_cut,
            'v_cut_spec' => $v_cut_spec,
            'v_cut_img' => $v_cut_img,
            'tin_roll' => $tin_roll,
            'tin_roll_spec' => $tin_roll_spec,
            'finishing' => $finishing,
            'packing_standard' => $packing_standard,
            'others_req' => $others_req
        );

        $this->customer_specification_model->add_customer_specification($data);

        $msg = 'Berhasil menambahkan customer specification';

        $result = array(
            'success' => true,
            'message' => $msg
        );
		
		$this->log_activity->insert_activity('insert', $msg. ' dengan ID Customer ' .$data['id_eksternal']);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is redirect to edit customer specification page
      * @return Void
      */
    public function edit($id) {
        $customer = $this->quotation_model->customer();
        $item = $this->quotation_model->item();
        $detail = $this->customer_specification_model->detail($id);

        $data = array(
            'customer' => $customer,
            'item' => $item,
            'detail' => $detail
        );

        $this->load->view('edit_modal_view', $data);
    }

    /**
      * This function is used to edit customer specification data
      * @return Array
      */
    public function edit_customer_specification() {
        $id = $this->Anti_sql_injection($this->input->post('id', TRUE));
        $id_eksternal = $this->Anti_sql_injection($this->input->post('id_eksternal', TRUE));
        $id_produk = $this->Anti_sql_injection($this->input->post('id_produk', TRUE));
        $approved_laminate = $this->Anti_sql_injection($this->input->post('approved_laminate', TRUE));
        $laminate_grade = $this->Anti_sql_injection($this->input->post('laminate_grade', TRUE));
        $laminate = $this->Anti_sql_injection($this->input->post('laminate', TRUE));
        $thickness = $this->Anti_sql_injection($this->input->post('thickness', TRUE));
        $base_cooper = $this->Anti_sql_injection($this->input->post('base_cooper', TRUE));
        $co_logo = $this->Anti_sql_injection($this->input->post('co_logo', TRUE));
        $type_logo = $this->Anti_sql_injection($this->input->post('type_logo', TRUE));
        $img_logo = NULL;
        $ul_recognition = $this->Anti_sql_injection($this->input->post('ul_recognition', TRUE));
        $date_code = $this->Anti_sql_injection($this->input->post('date_code', TRUE));
        $date_type = $this->Anti_sql_injection($this->input->post('date_type', TRUE));
        $date_img = NULL;
        $solder_resist_type = $this->Anti_sql_injection($this->input->post('solder_resist_type', TRUE));
        $solder_resist = $this->Anti_sql_injection($this->input->post('solder_resist', TRUE));
        $solder_resist_others = $this->Anti_sql_injection($this->input->post('solder_resist_others', TRUE));
        $legend_circuit = $this->Anti_sql_injection($this->input->post('legend_circuit', TRUE));
        $legend_circuit_others = $this->Anti_sql_injection($this->input->post('legend_circuit_others', TRUE));
        $legend_component = $this->Anti_sql_injection($this->input->post('legend_component', TRUE));
        $legend_component_others = $this->Anti_sql_injection($this->input->post('legend_component_others', TRUE));
        $carbon = $this->Anti_sql_injection($this->input->post('carbon', TRUE));
        $carbon_type = $this->Anti_sql_injection($this->input->post('carbon_type', TRUE));
        $carbon_spec = $this->Anti_sql_injection($this->input->post('carbon_spec', TRUE));
        $peelable = $this->Anti_sql_injection($this->input->post('peelable', TRUE));
        $peelable_type = $this->Anti_sql_injection($this->input->post('peelable_type', TRUE));
        $peelable_spec = $this->Anti_sql_injection($this->input->post('peelable_spec', TRUE));
        $plating = $this->Anti_sql_injection($this->input->post('plating', TRUE));
        $plating_spec = $this->Anti_sql_injection($this->input->post('plating_spec', TRUE));
        $process_molding = $this->Anti_sql_injection($this->input->post('process_molding', TRUE));
        $process_molding_remarks = $this->Anti_sql_injection($this->input->post('process_molding_remarks', TRUE));
        $v_cut = $this->Anti_sql_injection($this->input->post('v_cut', TRUE));
        $v_cut_spec = $this->Anti_sql_injection($this->input->post('v_cut_spec', TRUE));
        $v_cut_img = NULL;
        $tin_roll = $this->Anti_sql_injection($this->input->post('tin_roll', TRUE));
        $tin_roll_spec = $this->Anti_sql_injection($this->input->post('tin_roll_spec', TRUE));
        $finishing = $this->Anti_sql_injection($this->input->post('finishing', TRUE));
        $packing_standard = $this->Anti_sql_injection($this->input->post('packing_standard', TRUE));
        $packing_standard_others = $this->Anti_sql_injection($this->input->post('packing_standard_others', TRUE));
        $others_req = $this->Anti_sql_injection($this->input->post('others_req', TRUE));
        $uc1 = $this->Anti_sql_injection($this->input->post('uc1', TRUE));
        $uc2 = $this->Anti_sql_injection($this->input->post('uc2', TRUE));
        $shearing_cut = $this->Anti_sql_injection($this->input->post('scut', TRUE));

        if ($_FILES['img_logo']['name']) {
            $img_logo = $this->upload('img_logo');
        }

        if ($_FILES['date_img']['name']) {
            $date_img = $this->upload('date_img');
        }

        if ($_FILES['v_cut_img']['name']) {
            $v_cut_img = $this->upload('v_cut_img');
        }

        $solder_resist = ($solder_resist == 'others') ? $solder_resist_others : $solder_resist;
        $legend_circuit = ($legend_circuit == 'others') ? $legend_circuit_others : $legend_circuit;
        $legend_component = ($legend_component == 'others') ? $legend_component_others : $legend_component;
        $packing_standard = ($packing_standard == 'others') ? $packing_standard_others : $packing_standard;

        $data = array(
            'id' => $id,
            'id_eksternal' => $id_eksternal,
            'id_produk' => $id_produk,
            'approved_laminate' => $approved_laminate,
            'laminate_grade' => $laminate_grade,
            'laminate' => $laminate,
            'thickness' => $thickness,
            'base_cooper' => $base_cooper,
            'co_logo' => $co_logo,
            'type_logo' => $type_logo,
            'img_logo' => $img_logo,
            'ul_recognition' => $ul_recognition,
            'date_code' => $date_code,
            'date_type' => $date_type,
            'date_img' => $date_img,
            'solder_resist_type' => $solder_resist_type,
            'solder_resist' => $solder_resist,
            'legend_circuit' => $legend_circuit,
            'legend_component' => $legend_component,
            'carbon' => $carbon,
            'carbon_type' => $carbon_type,
            'carbon_spec' => $carbon_spec,
            'peelable' => $peelable,
            'peelable_type' => $peelable_type,
            'peelable_spec' => $peelable_spec,
            'plating' => $plating,
            'plating_spec' => $plating_spec,
            'process_molding' => $process_molding,
            'process_molding_remarks' => $process_molding_remarks,
            'v_cut' => $v_cut,
            'v_cut_spec' => $v_cut_spec,
            'v_cut_img' => $v_cut_img,
            'tin_roll' => $tin_roll,
            'tin_roll_spec' => $tin_roll_spec,
            'finishing' => $finishing,
            'packing_standard' => $packing_standard,
            'uc1' => $uc1,
            'uc2' => $uc2,
            'shearing_cut' => $shearing_cut,
            'others_req' => $others_req
        );

        $this->customer_specification_model->edit_customer_specification($data);

        $msg = 'Berhasil merubah data customer specification';

        $result = array(
            'success' => true,
            'message' => $msg
        );

		$this->log_activity->insert_activity('update', $msg. ' dengan ID ' .$id);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is used to delete customer specification data
      * @return Array
      */
    public function delete_customer_specification() {
        $data   = file_get_contents("php://input");
        $params = json_decode($data,true);

        $this->customer_specification_model->delete_customer_specification($params['id']);

        $msg = 'Berhasil menghapus data customer specification';

        $result = array(
            'success' => true,
            'message' => $msg
        );
		
		$this->log_activity->insert_activity('update', $msg. ' dengan ID Customer ' .$params['id']);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is redirect to detail customer specification page
      * @return Void
      */
    public function detail($id) {
        $customer = $this->quotation_model->customer();
        $item = $this->quotation_model->item();
        $detail = $this->customer_specification_model->detail($id);

        $data = array(
            'customer' => $customer,
            'item' => $item,
            'detail' => $detail
        );

        $this->load->view('detail_modal_view', $data);
    }

    /**
      * This function is redirect to approval customer specification page
      * @return Void
      */
    public function approval($id) {
        $customer = $this->quotation_model->customer();
        $item = $this->quotation_model->item();
        $detail = $this->customer_specification_model->detail($id);

        $data = array(
            'customer' => $customer,
            'item' => $item,
            'detail' => $detail
        );

        $this->load->view('approval_modal_view', $data);
    }

    /**
      * This function is used to change status customer specification data
      * @return Array
      */
    public function approval_customer_specification() {
        $data   = file_get_contents("php://input");
        $params = json_decode($data,true);

        $this->customer_specification_model->approval_customer_specification($params);

        $msg = 'Berhasil mengubah status customer specification';

        $result = array(
            'success' => true,
            'message' => $msg
        );

		$this->log_activity->insert_activity('update', $msg. ' dengan status ' .$params['status']);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is redirect to detail approval customer specification page
      * @return Void
      */
    public function detail_status($id) {
        $detail = $this->customer_specification_model->detail($id);

        $data = array(
            'detail' => $detail
        );

        $this->load->view('approval_detail_modal_view', $data);
    }

    /**
      * This function is redirect to edit gerber data page
      * @return Void
      */
    public function gerber_data($id) {
        $gerber_data = $this->customer_specification_model->gerber_data($id);

        $data = array(
            'id' => $id,
            'gerber_data' => $gerber_data
        );

        $this->load->view('gerber_modal_view', $data);
    }

    /**
      * This function is used to upload gerber data
      * @return Array
      */
    public function upload_gerberdata() {
        $id = $this->Anti_sql_injection($this->input->post('id', TRUE));
        $deletedItems = $this->Anti_sql_injection($this->input->post('deletedItems', TRUE));
        $datagerber = $this->Anti_sql_injection($this->input->post('datagerber', TRUE));

        $files = array();

        if($datagerber != ''){
            for($i=0;$i<count(explode(',',$datagerber));$i++){
                if (isset($_FILES['doc'.$i]) && $_FILES['doc'.$i]['name']) {
                    $file = $this->upload_gerber('doc'.$i);
                    array_push($files,$file);
                }
            }
        }

        $data = array(
            'id' => $id,
            'deletedItems' => $deletedItems,
            'title' => $datagerber,
            'doc' => $files
        );

        $this->customer_specification_model->upload_gerberdata($data);

        $msg = 'Berhasil menyimpan gerber data.';

        $result = array(
            'success' => true,
            'message' => $msg
        );

		$this->log_activity->insert_activity('insert', $msg. ' dengan judul ' .$data['title']);
		
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
}