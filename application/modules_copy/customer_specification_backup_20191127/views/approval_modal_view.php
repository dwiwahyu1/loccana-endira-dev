  <style>
    #loading-us{display:none}
    #tick{display:none}

    #loading-mail{display:none}
    #cross{display:none}
    .multiple-input{float:left;font-weight: normal;}
    .multiple-span{line-height: 36px;vertical-align: sub;margin: 0px 5px;}
    .laminate-inpt{width:120px;margin-bottom: 0px;}
    .img-spec{margin-left: 190px;}
  </style>

  <?php
    switch($detail[0]['solder_resist']){
      case "red" :
      case "blue" :
      case "green" :
      case "black" :
      case "yellow" : $solder_resist = $detail[0]['solder_resist'];break;
      default : $solder_resist = "others";
    }

    switch($detail[0]['legend_circuit']){
      case "red" :
      case "blue" :
      case "green" :
      case "black" :
      case "yellow" : $legend_circuit = $detail[0]['legend_circuit'];break;
      default : $legend_circuit = "others";
    }

    switch($detail[0]['legend_component']){
      case "red" :
      case "blue" :
      case "green" :
      case "black" :
      case "yellow" : $legend_component = $detail[0]['legend_component'];break;
      default : $legend_component = "others";
    }

    switch($detail[0]['packing_standard']){
      case "local" :
      case "over sea" : $packing_standard = $detail[0]['packing_standard'];break;
      default : $packing_standard = "others";
    }
  ?>
  <form class="form-horizontal form-label-left" id="approval_customer_specification" role="form" action="<?php echo base_url('customer_specification/approval_customer_specification');?>" method="post" enctype="multipart/form-data" >

    <p style="text-align: center;">Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

 <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Customer Name<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
		<input data-parsley-maxlength="255" type="text" id="id_eksternal_n" name="id_eksternal_n" class="form-control col-md-7 col-xs-12" placeholder="Approved Laminate" " value="<?php if(isset($detail[0]['name_eksternal'])){ echo $detail[0]['name_eksternal']; }?>" readOnly>
		<input data-parsley-maxlength="255" type="hidden" id="id_eksternal" name="id_eksternal" class="form-control col-md-7 col-xs-12" placeholder="Approved Laminate" " value="0" readOnly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Part Number <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
		 <input data-parsley-maxlength="255" type="text" id="id_produk_n" name="id_produk_n" class="form-control col-md-7 col-xs-12" placeholder="Approved Laminate" " value="<?php if(isset($detail[0]['stock_name'])){ echo $detail[0]['stock_name']; }?>" readOnly>
		 <input data-parsley-maxlength="255" type="hidden" id="id_produk" name="id_produk" class="form-control col-md-7 col-xs-12" placeholder="Approved Laminate" " value="0" readOnly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Approved Laminate <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="approved_laminate" name="approved_laminate" class="form-control col-md-7 col-xs-12" placeholder="Approved Laminate" " value="<?php if(isset($detail[0]['approved_laminate'])){ echo $detail[0]['approved_laminate']; }?>" disabled>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Laminate Grade <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="laminate_grade" name="laminate_grade" class="form-control col-md-7 col-xs-12" placeholder="Approved Laminate" " value="<?php if(isset($detail[0]['laminate_grade'])){ echo $detail[0]['laminate_grade']; }?>" disabled>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Lam Thk Base Cooper <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="number" id="laminate" name="laminate" class="form-control laminate-inpt col-md-7 col-xs-12"  min="0" " value="<?php if(isset($detail[0]['laminate'])){ echo $detail[0]['laminate']; }?>" disabled> 
          <span class="multiple-span">mm</span>
        </label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="number" id="thickness" name="thickness" class="form-control laminate-inpt col-md-7 col-xs-12"  min="0" " value="<?php if(isset($detail[0]['thickness'])){ echo $detail[0]['thickness']; }?>" disabled> 
          <span class="multiple-span">micron</span>
        </label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="number" id="base_cooper" name="base_cooper" class="form-control laminate-inpt col-md-7 col-xs-12"  min="0" " value="<?php if(isset($detail[0]['base_cooper'])){ echo $detail[0]['base_cooper']; }?>" disabled> 
          <span class="multiple-span">OZ</span>
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">CO Logo / Type / UL Logo <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="co_logo" name="co_logo" style="width: 120px"  disabled>
            <option value="yes" <?php if(isset($detail[0]['co_logo'])){ if( $detail[0]['co_logo'] == 'yes' ){ echo "selected"; } }?>>YES</option>
            <option value="no" <?php if(isset($detail[0]['co_logo'])){ if( $detail[0]['co_logo'] == 'no' ){ echo "selected"; } }?>>NO</option>
          </select>
        </label>
        <label class="multiple-input multiple-span" style="width: 43px;">type</label>
        <label class="multiple-input" style="font-weight: normal;">
          <input data-parsley-maxlength="255" type="text" id="type_logo" name="type_logo" class="form-control col-md-7 col-xs-12" placeholder="Type" <?php if(isset($detail[0]['co_logo'])){ if( $detail[0]['co_logo'] == 'yes' ){ echo "required"; } }?> style="width: 310px;" value="<?php if(isset($detail[0]['type_logo'])){ echo $detail[0]['type_logo']; }?>" disabled>
        </label>
      </div>
    </div>

    <?php
      if(isset($detail[0]['img_logo']) && $detail[0]['img_logo'] != ''){ 
    ?>
      <div class="item form-group has-feedback">
        <div class="col-md-8 col-sm-6 col-xs-12 img-spec">
            <img src="<?php echo base_url() . "uploads/specification/".$detail[0]['img_logo']; ?>" width="100" height="100" />
        </div>
      </div>
    <?php
      } 
    ?>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">UL Recognition <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="ul_recognition" name="ul_recognition" class="form-control col-md-7 col-xs-12" placeholder="UL Recognition" " value="<?php if(isset($detail[0]['ul_recognition'])){ echo $detail[0]['ul_recognition']; }?>" disabled>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Date Code <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="date_code" name="date_code" style="width: 120px"  disabled>
            <option value="yes" <?php if(isset($detail[0]['date_code'])){ if( $detail[0]['date_code'] == 'yes' ){ echo "selected"; } }?>>YES</option>
            <option value="no" <?php if(isset($detail[0]['date_code'])){ if( $detail[0]['date_code'] == 'no' ){ echo "selected"; } }?>>NO</option>
          </select>
        </label>
        <label class="multiple-input multiple-span" style="width: 43px;">type</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="date_type" name="date_type" class="form-control col-md-7 col-xs-12" placeholder="Type" <?php if(isset($detail[0]['date_code'])){ if( $detail[0]['date_code'] == 'yes' ){ echo "required"; } }?> style="width: 310px;" value="<?php if(isset($detail[0]['date_type'])){ echo $detail[0]['date_type']; }?>" disabled>
        </label>
      </div>
    </div>

    <?php
      if(isset($detail[0]['date_img']) && $detail[0]['date_img'] != ''){ 
    ?>
      <div class="item form-group has-feedback">
        <div class="col-md-8 col-sm-6 col-xs-12 img-spec">
            <img src="<?php echo base_url() ."uploads/specification/". $detail[0]['date_img']; ?>" width="100" height="100" />
        </div>
      </div>
    <?php
      } 
    ?>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Solder Resist type <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="solder_resist_type" name="solder_resist_type" class="form-control col-md-7 col-xs-12" placeholder="UL Recognition" " value="<?php if(isset($detail[0]['solder_resist_type'])){ echo $detail[0]['solder_resist_type']; }?>" disabled>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Solder Resist <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="solder_resist" name="solder_resist" style="width: 120px"  disabled>
            <option value="red" <?php if( $solder_resist == 'red' ){ echo "selected"; } ?>>Red</option>
            <option value="blue" <?php if( $solder_resist == 'blue' ){ echo "selected"; } ?>>Blue</option>
            <option value="green" <?php if( $solder_resist == 'green' ){ echo "selected"; } ?>>Green</option>
            <option value="black" <?php if( $solder_resist == 'black' ){ echo "selected"; } ?>>Black</option>
            <option value="white" <?php if( $solder_resist == 'white' ){ echo "selected"; } ?>>White</option>
            <option value="yellow" <?php if( $solder_resist == 'yellow' ){ echo "selected"; } ?>>Yellow</option>
            <option value="others" <?php if( $solder_resist == 'others' ){ echo "selected"; } ?>>Others</option>
          </select>
        </label>
        <label class="multiple-input" style="margin-left: 5px;">
          <input data-parsley-maxlength="255" type="text" id="solder_resist_others" name="solder_resist_others" class="form-control col-md-7 col-xs-12" placeholder="Others" value="<?php if($solder_resist == 'others'){ echo $detail[0]['solder_resist']; }?>" style="width: 358px;" <?php if( $solder_resist == 'others' ){ echo "required"; } ?> disabled>
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Legend Circuit <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="legend_circuit" name="legend_circuit" style="width: 120px"  disabled>
            <option value="red" <?php if( $legend_circuit == 'red' ){ echo "selected"; } ?>>Red</option>
            <option value="blue" <?php if( $legend_circuit == 'blue' ){ echo "selected"; } ?>>Blue</option>
            <option value="green" <?php if( $legend_circuit == 'green' ){ echo "selected"; } ?>>Green</option>
            <option value="black" <?php if( $legend_circuit == 'black' ){ echo "selected"; } ?>>Black</option>
            <option value="white" <?php if( $legend_circuit == 'white' ){ echo "selected"; } ?>>White</option>
            <option value="yellow" <?php if( $legend_circuit == 'yellow' ){ echo "selected"; } ?>>Yellow</option>
            <option value="others" <?php if( $legend_circuit == 'others' ){ echo "selected"; } ?>>Others</option>
          </select>
        </label>
        <label class="multiple-input" style="margin-left: 5px;">
          <input data-parsley-maxlength="255" type="text" id="legend_circuit_others" name="legend_circuit_others" class="form-control col-md-7 col-xs-12" placeholder="Others" value="<?php if($legend_circuit == 'others'){ echo $detail[0]['legend_circuit']; }?>" style="width: 358px;" <?php if( $legend_circuit == 'others' ){ echo "required"; } ?> disabled>
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Legend Component <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="legend_component" name="legend_component" style="width: 120px"  disabled>
            <option value="red" <?php if( $legend_component == 'red' ){ echo "selected"; } ?>>Red</option>
            <option value="blue" <?php if( $legend_component == 'blue' ){ echo "selected"; } ?>>Blue</option>
            <option value="green" <?php if( $legend_component == 'green' ){ echo "selected"; } ?>>Green</option>
            <option value="black" <?php if( $legend_component == 'black' ){ echo "selected"; } ?>>Black</option>
            <option value="white" <?php if( $legend_component == 'white' ){ echo "selected"; } ?>>White</option>
            <option value="yellow" <?php if( $legend_component == 'yellow' ){ echo "selected"; } ?>>Yellow</option>
            <option value="others" <?php if( $legend_component == 'others' ){ echo "selected"; } ?>>Others</option>
          </select>
        </label>
        <label class="multiple-input" style="margin-left: 5px;">
          <input data-parsley-maxlength="255" type="text" id="legend_component_others" name="legend_component_others" class="form-control col-md-7 col-xs-12" placeholder="Others" value="<?php if($legend_component == 'others'){ echo $detail[0]['legend_component']; }?>" style="width: 358px;" <?php if( $legend_component == 'others' ){ echo "required"; } ?> disabled>
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Carbon <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="carbon" name="carbon" class="form-control col-md-7 col-xs-12" placeholder="Carbon" " value="<?php if(isset($detail[0]['carbon'])){ echo $detail[0]['carbon']; }?>" style="width: 120px;" disabled>
        </label>
        <label class="multiple-input multiple-span" style="width: 51px;">type</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="carbon_type" name="carbon_type" class="form-control col-md-7 col-xs-12" placeholder="Type" value="<?php if(isset($detail[0]['carbon_type'])){ echo $detail[0]['carbon_type']; }?>" style="width: 120px;" disabled>
        </label>
        <label class="multiple-input multiple-span" style="width: 51px;">spec</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="carbon_spec" name="carbon_spec" class="form-control col-md-7 col-xs-12" placeholder="Spec" value="<?php if(isset($detail[0]['carbon_spec'])){ echo $detail[0]['carbon_spec']; }?>" style="width: 120px;" disabled>
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Peelable <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="peelable" name="peelable" class="form-control col-md-7 col-xs-12" placeholder="Peelable" " value="<?php if(isset($detail[0]['peelable'])){ echo $detail[0]['peelable']; }?>" style="width: 120px;" disabled>
        </label>
        <label class="multiple-input multiple-span" style="width: 51px;">type</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="peelable_type" name="peelable_type" class="form-control col-md-7 col-xs-12" placeholder="Type" value="<?php if(isset($detail[0]['peelable_type'])){ echo $detail[0]['peelable_type']; }?>" style="width: 120px;" disabled>
        </label>
        <label class="multiple-input multiple-span" style="width: 51px;">spec</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="peelable_spec" name="peelable_spec" class="form-control col-md-7 col-xs-12" placeholder="Spec" value="<?php if(isset($detail[0]['peelable_spec'])){ echo $detail[0]['peelable_spec']; }?>" style="width: 120px;" disabled>
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Plating <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="plating" name="plating" class="form-control col-md-7 col-xs-12" placeholder="Plating" " disabled value="<?php if(isset($detail[0]['plating'])){ echo $detail[0]['plating']; }?>" style="width: 120px;">
        </label>
        <label class="multiple-input" style="margin-left: 5px;">
          <input data-parsley-maxlength="255" type="text" id="plating_spec" name="plating_spec" class="form-control col-md-7 col-xs-12" placeholder="Spec" disabled value="<?php if(isset($detail[0]['plating_spec'])){ echo $detail[0]['plating_spec']; }?>" style="width: 358px;">
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Process Molding <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="process_molding" name="process_molding" style="width: 120px"  disabled>
            <option value="cnc" <?php if(isset($detail[0]['process_molding'])){ if( $detail[0]['process_molding'] == 'cnc' ){ echo "selected"; } }?>>CNC</option>
            <option value="punching" <?php if(isset($detail[0]['process_molding'])){ if( $detail[0]['process_molding'] == 'punching' ){ echo "selected"; } }?>>Punching</option>
          </select>
        </label>
        <label class="multiple-input multiple-span" style="width: 53px;">remarks</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="process_molding_remarks" name="process_molding_remarks" class="form-control col-md-7 col-xs-12" placeholder="Remarks" value="<?php if(isset($detail[0]['process_molding_remarks'])){ echo $detail[0]['process_molding_remarks']; }?>" style="width: 300px;" disabled>
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">V-Cut <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="v_cut" name="v_cut" style="width: 120px"  disabled>
            <option value="yes" <?php if(isset($detail[0]['v_cut'])){ if( $detail[0]['v_cut'] == 'yes' ){ echo "selected"; } }?>>YES</option>
            <option value="no" <?php if(isset($detail[0]['v_cut'])){ if( $detail[0]['v_cut'] == 'no' ){ echo "selected"; } }?>>NO</option>
          </select>
        </label>
        <label class="multiple-input multiple-span" style="width: 53px;">spec</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="v_cut_spec" name="v_cut_spec" class="form-control col-md-7 col-xs-12" placeholder="Spec" <?php if(isset($detail[0]['v_cut'])){ if( $detail[0]['v_cut'] == 'yes' ){ echo "required"; } }?> style="width: 300px;" value="<?php if(isset($detail[0]['v_cut_spec'])){ echo $detail[0]['v_cut_spec']; }?>" disabled>
        </label>
      </div>
    </div>

    <?php
      if(isset($detail[0]['v_cut_img']) && $detail[0]['v_cut_img'] != ''){ 
    ?>
      <div class="item form-group has-feedback">
        <div class="col-md-8 col-sm-6 col-xs-12 img-spec">
            <img src="<?php echo base_url() ."uploads/specification/". $detail[0]['v_cut_img']; ?>" width="100" height="100" />
        </div>
      </div>
    <?php
      } 
    ?>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Tin Roll / Hal <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="tin_roll" name="tin_roll" style="width: 120px" disabled>
            <option value="yes" <?php if(isset($detail[0]['tin_roll'])){ if( $detail[0]['tin_roll'] == 'yes' ){ echo "selected"; } }?>>YES</option>
            <option value="no" <?php if(isset($detail[0]['tin_roll'])){ if( $detail[0]['tin_roll'] == 'no' ){ echo "selected"; } }?>>NO</option>
          </select>
        </label>
        <label class="multiple-input multiple-span" style="width: 53px;">spec</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="tin_roll_spec" name="tin_roll_spec" class="form-control col-md-7 col-xs-12" placeholder="Spec" <?php if(isset($detail[0]['tin_roll'])){ if( $detail[0]['tin_roll'] == 'yes' ){ echo "required"; } }?> style="width: 300px;" value="<?php if(isset($detail[0]['tin_roll_spec'])){ echo $detail[0]['tin_roll_spec']; }?>" disabled>
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Finishing <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input" style="width: 100%;">
          <select class="form-control" id="finishing" name="finishing" disabled>
            <option value="osp" <?php if(isset($detail[0]['finishing'])){ if( $detail[0]['finishing'] == 'osp' ){ echo "selected"; } }?>>OSP</option>
            <option value="normal flux" <?php if(isset($detail[0]['finishing'])){ if( $detail[0]['finishing'] == 'normal flux' ){ echo "selected"; } }?>>Normal Flux</option>
          </select>
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Packing Standard <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="packing_standard" name="packing_standard" style="width: 120px" disabled>
            <option value="local" <?php if( $packing_standard == 'local' ){ echo "selected"; } ?>>Local</option>
            <option value="over sea" <?php if( $packing_standard == 'over sea' ){ echo "selected"; } ?>>Over Sea</option>
            <option value="others" <?php if( $packing_standard == 'others' ){ echo "selected"; } ?>>Others</option>
          </select>
        </label>
        <label class="multiple-input multiple-span" style="width: 53px;">others</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="packing_standard_others" name="packing_standard_others" class="form-control col-md-7 col-xs-12" placeholder="Others" style="width: 300px;" value="<?php if($packing_standard == 'others'){ echo $detail[0]['packing_standard']; }?>" <?php if( $packing_standard == 'others' ){ echo "required"; } ?> disabled>
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Others Requirement</span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="others_req" name="others_req" class="form-control col-md-7 col-xs-12" placeholder="Others Requirement" value="<?php if(isset($detail[0]['others_req'])){ echo $detail[0]['others_req']; }?>" disabled>
      </div>
    </div>

    <div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Status 
          <span class="required">
            <sup>*</sup>
          </span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
            <select class="form-control" name="status" id="status" style="width: 100%" required>
                <option value="1">Accept</option>
                <option value="2">Reject</option>
            </select>
        </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Notes <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <textarea data-parsley-maxlength="255" type="text" id="notes" name="notes" class="form-control col-md-7 col-xs-12" placeholder="Notes"></textarea>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Change Status Customer Specification</button>
      </div>
    </div>

    <input type="hidden" id="id" name="id" value="<?php if(isset($detail[0]['id'])){ echo $detail[0]['id']; }?>">
  </form><!-- /page content -->

<script type="text/javascript">
  $(document).ready(function() {
    // $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();
  });

  $('#approval_customer_specification').on('submit',(function(e) {
    $('#btn-submit').attr('disabled','disabled');
    $('#btn-submit').text("Mengubah status data...");
    e.preventDefault();

    var id     = $('#id').val(),
        status = $('#status').val(),
        notes  = $('#notes').val();

    var datapost={
          "id"      :   id,
          "status"  :   status,
          "notes"   :   notes
        };

    $.ajax({
        type:'POST',
        url: $(this).attr('action'),
        data:JSON.stringify(datapost),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function(response) {
            if (response.success == true) {
              $('.panel-heading button').trigger('click');
                listcustspec();
                swal({
                  title: 'Success!',
                  text: response.message,
                  type: 'success',
                  showCancelButton: false,
                  confirmButtonText: 'Ok'
                }).then(function () {
                });
            } else{
                $('#btn-submit').removeAttr('disabled');
                $('#btn-submit').text("Change Status Customer Specification");
                swal("Failed!", response.message, "error");
            }
        }
    }).fail(function(xhr, status, message) {
        $('#btn-submit').removeAttr('disabled');
        $('#btn-submit').text("Change Status Customer Specification");
        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
    });
  }));
</script>