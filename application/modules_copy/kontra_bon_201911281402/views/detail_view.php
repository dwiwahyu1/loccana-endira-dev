<form class="form-horizontal form-label-left" role="form" method="post" enctype="multipart/form-data" data-parsley-validate>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_kontra_bon">No Kontra Bon <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="no_kontra_bon" name="no_kontra_bon" class="form-control" placeholder="No Kontra Bon" value="<?php if(isset($kontra_bon[0]['no_kb'])){ echo $kontra_bon[0]['no_kb']; }?>" autocomplete="off" readonly>
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_faktur">No Faktur <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="no_faktur" name="no_faktur" class="form-control" placeholder="No Faktur" value="<?php if(isset($kontra_bon[0]['no_faktur'])){ echo $kontra_bon[0]['no_faktur']; }?>" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tgl_kontra_bon">Tanggal Kontra Bon <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="tgl_kontra_bon" name="tgl_kontra_bon" class="form-control" placeholder="Tanggal Kontra Bon" value="<?php if(isset($kontra_bon[0]['date_kb'])){
				echo date('d-M-Y', strtotime($kontra_bon[0]['date_kb']));
			}?>" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_invoice">No Invoice <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="no_invoice" name="no_invoice" style="width: 100%" readonly>
				<option value="">... Pilih Invoice ...</option>
				<option value="<?php if(isset($kontra_bon[0]['id_po'])){ echo $kontra_bon[0]['id_po']; }?>" selected><?php if(isset($kontra_bon[0]['no_po'])){ echo $kontra_bon[0]['no_po']; }?></option>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_supplier">Nama Supplier</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="nama_supplier" name="nama_supplier" class="form-control" placeholder="Nama Supplier" value="<?php if(isset($kontra_bon[0]['name_eksternal'])){ echo $kontra_bon[0]['name_eksternal']; }?>" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jatuh_tempo">Jatuh Tempo</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="jatuh_tempo" name="jatuh_tempo" class="form-control" placeholder="Jatuh Tempo" value="<?php if(isset($kontra_bon[0]['date_po'])) {
				$kontra_bon[0]['date_po']			= date('d-M-Y', strtotime($kontra_bon[0]['date_po']));
				$kontra_bon[0]['term_of_payment']	= preg_replace('/\D/', '', $kontra_bon[0]['term_of_payment']);
				echo date('d-M-Y', strtotime($kontra_bon[0]['date_po'] . ' +'.$kontra_bon[0]['term_of_payment'].' day'));
			}?>" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jumlah">Jumlah</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="jumlah" name="jumlah" class="form-control" placeholder="Jumlah" value="<?php if(isset($kontra_bon[0]['total_amount'])){ echo $kontra_bon[0]['symbol'].' '.number_format(round((int)$kontra_bon[0]['total_amount']),0,',','.'); }?>" autocomplete="off" readonly>
		</div>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	var inVoice = '<?php if(isset($kontra_bon[0]['id_po'])){ echo $kontra_bon[0]['id_po']; }?>';
	$(document).ready(function() {
		$('#tgl_kontra_bon').datepicker({
			format: "d-M-yyyy",
			autoclose: true,
			todayHighlight: true
		});

		$('#no_invoice').select2();
		$('#no_invoice').on('change', function() {
			if(this.value != '') setInvoice(this.value);
			else {
				$('#nama_supplier').val('');
				$('#jatuh_tempo').val('');
				$('#jumlah').val('');
			}
		});
		getInvoice();
	});

	function getInvoice() {
		$('#no_invoice').attr('disabled', 'disabled');
		$('#tgl_kontra_bon').attr('disabled', 'disabled');
		$.ajax({
			type: "GET",
			url: "<?php echo base_url('kontra_bon/get_invoice');?>",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					for (var i = 0; i < r.length; i++) {
						if(inVoice == r[i].id_po) {
							var newOption = new Option(r[i].no_po, r[i].id_po, true, true);
							$('#no_invoice').append(newOption).trigger('change');
						}else {
							var newOption = new Option(r[i].no_po, r[i].id_po, false, false);
							$('#no_invoice').append(newOption);
						}
					}
				}
			}
		});
	}

	function setInvoice(id_po) {
		var datapost = {"id_po" : id_po};
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('kontra_bon/set_invoice_edit_detail');?>",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					$('#nama_supplier').val(r[0].stock_name);
					$('#jatuh_tempo').val(r[0].jatuh_tempo);
					$('#jumlah').val(r[0].total_amount);
				}else {
					$('#nama_supplier').val();
					$('#jatuh_tempo').val();
					$('#jumlah').val();
				}
			}
		});
	}
</script>