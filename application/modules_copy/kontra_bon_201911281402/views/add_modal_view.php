<form class="form-horizontal form-label-left" id="add_form" role="form" action="<?php echo base_url('kontra_bon/save_kontra_bon');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<!--<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_kontra_bon">No Kontra Bon <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="no_kontra_bon" name="no_kontra_bon" class="form-control" placeholder="No Kontra Bon"
			value="<?php if(isset($no_kontra_bon)){ echo $no_kontra_bon; }?>" autocomplete="off" readonly>
		</div>
	</div>-->
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_faktur">No Faktur <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="no_faktur" name="no_faktur" class="form-control" placeholder="No Faktur" value="" autocomplete="off">
		</div>
	</div>
	
	<div class="item form-group x-hidden" id="loaders-check">
		<div class="col-md-8 col-sm-6 col-xs-12 col-offset-md-3">
			<div class="loading-container">
				<div class="loading"></div>
				<div id="loading-text">Checking No Faktur ...</div>
			</div>
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tgl_kontra_bon">Tanggal Kontra Bon <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="tgl_kontra_bon" name="tgl_kontra_bon" class="form-control" placeholder="Tanggal Kontra Bon" autocomplete="off" required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_invoice">No PO <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="no_invoice" name="no_invoice" style="width: 100%" required>
				<option value="">... Pilih PO ...</option>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_supplier">Nama Supplier</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="nama_supplier" name="nama_supplier" class="form-control" placeholder="Nama Supplier" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jatuh_tempo">Jatuh Tempo</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="jatuh_tempo" name="jatuh_tempo" class="form-control" placeholder="Jatuh Tempo" autocomplete="off">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jumlah">Jumlah</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="jumlah" name="jumlah" class="form-control" placeholder="Jumlah" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Kontra Bon</button>
		</div>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	var last_no_faktur = $('#no_faktur').val();
	$(document).ready(function() {
		$('#tgl_kontra_bon').datepicker({
			format: "d-M-yyyy",
			autoclose: true,
			todayHighlight: true
		});

		$('#no_invoice').select2();
		
	
		$('#no_invoice').on('change', function() {
			if(this.value != '')
				setInvoice(this.value);
			else {
				$('#nama_supplier').val('');
				//$('#jatuh_tempo').val('');
				$('#jumlah').val('');
			}
		});
		getInvoice();
	});
	
	$('#no_faktur').on('input',function(event) {
		if($('#no_faktur').val() != last_no_faktur) {
			$('#loaders-check').removeClass('x-hidden');
			no_faktur_check();
		}
	});

	function no_faktur_check() {
		var no_faktur = $('#no_faktur').val();
		if(no_faktur.length > 3) {
			var post_data = {
				'no_faktur': no_faktur
			};
			
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url('kontra_bon/check_no_faktur');?>",
				data: post_data,
				cache: false,
				success: function(response){
					if(response.success == true){
						$('#no_faktur').css('border', '3px #090 solid');
						$('#loaders-check').addClass('x-hidden');
					}else {
						$('#no_faktur').css('border', '3px #C33 solid');
						$('#loaders-check').removeClass('x-hidden');
					}
				}
			});
		}else {
			$('#no_faktur').css('border', '3px #C33 solid');
			$('#loaders-check').removeClass('x-hidden');
		}
	}
	
	function getInvoice() {
		var html = '';
		//$('#no_invoice').attr('disabled', 'disabled');
		$.ajax({
			type: "GET",
			url: "<?php echo base_url('kontra_bon/get_invoice');?>",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					for (var i = 0; i < r.length; i++) {
						var name_eksternal ="";
						if (r[i].name_eksternal !== undefined)
							name_eksternal = r[i].name_eksternal;
						var newOption = new Option(r[i].no_po + ' - ' + name_eksternal, r[i].id_po, false, false);
						$('#no_invoice').append(newOption);
					}
					$('#no_invoice').removeAttr('disabled');
				}else{
					html +='<option value="" selected>Data PO tidak ada</option>';
					$('#no_invoice').append(html);
				}
			}
		});
	}

	function setInvoice(id_po) {
		var datapost = {"id_po" : id_po};
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('kontra_bon/set_invoice');?>",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				console.log(r);
				if(r.length > 0) {
					$('#nama_supplier').val(r[0].nama_supplier);
					//$('#jatuh_tempo').val(r[0].jatuh_tempo);
					$('#jumlah').val(r[0].total_amount);
				}else {
					$('#nama_supplier').val();
					//$('#jatuh_tempo').val();
					$('#jumlah').val();
				}
			}
		});
	}

	$('#add_form').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		var periode_exists = $('#existPeriode').val();
		var url = $(this).attr('action');
		e.preventDefault();

		var formData = new FormData(this);

		$.ajax({
			type:'POST',
			url: $(this).attr('action'),
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						window.location.href = "<?php echo base_url('kontra_bon');?>";
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Tambah Kontra Bon");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah Kontra Bon");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}));
</script>