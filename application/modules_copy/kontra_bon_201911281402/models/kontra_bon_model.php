<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kontra_Bon_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
	}

	public function count_kontra_bon($params) {
		$sql 	= 'CALL kb_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql, array(NULL, NULL, NULL, NULL, NULL));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		return $total['@total'];
	}

	public function list_kontra_bon($params) {
		$sql 	= 'CALL kb_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function get_purchase_order() {
		$sql 	= "SELECT c.name_eksternal,a.id_po,a.no_po 
							FROM t_purchase_order a
							LEFT JOIN t_kontra_bon b ON a.id_po = b.id_purchase_order
							LEFT JOIN t_eksternal c ON a.id_distributor = c.id
							WHERE b.id_kontra_bon IS NULL";

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_purchase_order_by_id($id_po) {
		$sql 	= '
		SELECT * FROM t_purchase_order a 
    LEFT JOIN t_po_spb d ON a.id_po=d.id_po
    LEFT JOIN t_btb_po e ON d.id_spb=e.id_spb
    JOIN t_spb zz ON d.id_spb=zz.id
    JOIN m_valas c ON a.id_valas = c.valas_id 
    JOIN t_eksternal f ON a.id_distributor = f.id 
    WHERE a.id_po = '.$id_po.'
    GROUP BY a.id_po';
			// $sql 	= 'CALL get_po_by_id(?)';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function kb_get_po($id_kontrabon) {
		$sql 	= "SELECT a.*,b.*,SUM(c.qty_diterima*(c.unit_price-c.diskon)) AS total_amount_fix
					 FROM t_purchase_order a 
							  LEFT JOIN t_kontra_bon b ON a.id_po=b.id_purchase_order
							  LEFT JOIN t_po_spb c ON a.id_po=c.id_po
							  WHERE b.id_kontra_bon = ".$id_kontrabon."
							  GROUP BY b.id_kontra_bon";

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_kartu_hp($data)
	{
		$sql 	= 'CALL hp_add(?,?,?,?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['ref'],
			$data['source'],
			$data['keterangan'],
			$data['status'],
			$data['saldo'],
			$data['saldo_akhir'],
			$data['id_valas'],
			$data['type_kartu'],
			$data['id_master'],
			$data['type_master']
		));
		
		$return['code'] = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function get_id_coa($params)
	{
		$sql 	= 'CALL btb_get_id_coa(?,?)';

		$query 	= $this->db->query($sql,array(
			$params[0]['id_po'],
			$params[0]['id_distributor']
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_coa_values($data)
	{
		$sql 	= 'CALL coavalue_add(?,?,?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_coa'],
			$data['id_parent'],
			$data['id_valas'],
			$data['value'],
			$data['adjusment'],
			$data['type_cash'],
			$data['note'],
			$data['rate'],
			''
		));
		
		$return['result'] = $query->result_array();
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$return['lastid'] = $lastid;

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_coa_values_cash($data)
	{
		$sql 	= 'CALL coavalue_add2(?,?,?,?,?,?,?,?)';
		
		$query 	= $this->db->query($sql,array(
			$data['payment_coa'],
			0,
			$data['delivery_date'],
			$data['id_valas'],
			$data['saldo'],
			0,
			1,
			$data['keterangan'],
		));

		$this->db->close();
		$this->db->initialize();
	}
	
	public function add_po_spb_coa($id_coa_value,$id_po_spb)
	{
		$sql 	= 'CALL hp_po_spb_coa_add(?,?)';

		$query 	= $this->db->query($sql,array(
			$id_coa_value,
			$id_po_spb
		));
		
		$return['code'] = $this->db->affected_rows();
		$return['array'] = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function kb_add($data) {
		$sql 	= 'CALL kb_add(?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['id_po'],
			$data['no_kb'],
			$data['no_faktur'],
			$data['tgl_kb'],
			$data['id_coa_masuk'],
			$data['amount'],
			$data['jatuh_tempo']
		));

		$return['result'] = $query->result_array();
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$return['lastid'] = $lastid;

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function edit_kontra_bon($id) {
		$sql 	= 'CALL kb_search_id(?)';

		$query 	=  $this->db->query($sql,
			array($id)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function kb_update($data) {
		$sql 	= 'CALL kb_update(?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id_kontra_bon'],
				$data['id_po'],
				$data['no_kb'],
				$data['no_faktur'],
				$data['tgl_kb']
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function kb_delete($data) {
		$sql 	= 'CALL kb_delete(?)';
		$query 	=  $this->db->query($sql,array(
			$data['id']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}