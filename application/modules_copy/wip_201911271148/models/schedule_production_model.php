<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Schedule_Production_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	/**
      * This function is get the list data in production schedule table
      * @param : $params is where condition for select query
      */

	public function lists($params = array())
	{
		$sql_all 	= 'CALL wipprod_list_all(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				$params['filter']
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL wipprod_list(?, ?, ?, ?, ?,  @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total_row,
			'total' => $total_row,
		);

		return $return;
	}

	/**
      * This function is get data in production_schedule table by id
      * @param : $id is where condition for select query
      */

		public function app_schd($i)
	{
			//$split_data = explode("|",$data);

				$sql 	= 'update t_production_schedule set status = "On Production"
				where id_prod = ? ';

					$this->db->query($sql,array(
						$i
					));

				$this->db->close();
				$this->db->initialize();
			
			
	}	
	
			public function done_sch($i)
	{
			//$split_data = explode("|",$data);

				$sql 	= 'update t_production_schedule set status = "Done Production"
				where id_prod = ? ';

					$this->db->query($sql,array(
						$i
					));	
					
					$sql2 	= 'INSERT INTO t_mutasi
								SELECT NULL as asd, a.`id_produk` AS id_awal, a.`id_produk` AS id_akhir, NOW() AS dits, b.`qty_out`, 0 AS tpd FROM `t_production_schedule` a
								LEFT JOIN t_wip b ON a.`id_prod` = b.`id_po_quotation`
								LEFT JOIN `t_process_mat` c ON b.`id_process_flow` = c.`id_proc_mat`
								LEFT JOIN `t_process_flow` d ON c.`t_process_flow` = d.`id`
								WHERE id_prod = 76 AND d.`id` = 22';

					$this->db->query($sql2);
					
					$sql3 	= 'update m_material set qty = qty + ( SELECT  b.`qty_out` FROM `t_production_schedule` a
								LEFT JOIN t_wip b ON a.`id_prod` = b.`id_po_quotation`
								LEFT JOIN `t_process_mat` c ON b.`id_process_flow` = c.`id_proc_mat`
								LEFT JOIN `t_process_flow` d ON c.`t_process_flow` = d.`id`
								WHERE id_prod = '.$i.' AND d.`id` = 22 ) ,
								base_price = (SELECT c.`disc_price` FROM `t_production_schedule` a
								LEFT JOIN `t_po_quotation` b ON a.`id_po_quot` = b.`id`
								LEFT JOIN `t_order` c ON b.`id` = c.`id_po_quotation` AND a.`id_produk` = c.`id_produk`
								WHERE a.`id_prod` = '.$i.')
								
								where `id` = (select id_produk from t_production_schedule where id_prod = ? );
								
								';

					$this->db->query($sql3,array(
						$i
					));	

				$this->db->close();
				$this->db->initialize();
			
			
	}	


	public function detail($id)
	{
		$sql 	= 'SELECT 
					      order_no,
					      id_produk,
					      stock_name,
					      t_order.qty AS order_qty,
					      IFNULL(( SELECT SUM(qty) FROM t_production_schedule WHERE t_po_quotation.id=t_production_schedule.id_po_quot AND t_production_schedule.id_produk=t_order.id_produk),0) AS jumlah_run_down_qty
					FROM 
					      t_po_quotation 
					       INNER JOIN t_order ON t_po_quotation.id=t_order.id_po_quotation 
					       INNER JOIN m_material ON t_order.id_produk=m_material.id
					WHERE 
					      t_po_quotation.id=?';

		$query 	=  $this->db->query($sql,
			array(
				$id
			));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function detail_flows($id)
	{
		// $sql 	= 'SELECT 
					     // a.*,p.name from t_wip a LEFT join t_process_flow p on a.id_process_flow = p.id
					// WHERE 
					      // a.id=?';
						  
		$sql 	= '
		SELECT a.*,f.`id_reject`,h.*,g.`value`,g.`id_flow_rej`,e.name FROM t_wip a
		LEFT JOIN t_process_mat b ON a.`id_process_flow` = b.`id_proc_mat`
		LEFT JOIN t_esf c ON a.`id_produk` = c.`id_produk`
		LEFT JOIN `t_prod_layout` d ON d.`id_material` = c.`id_produk`
		LEFT JOIN `t_process_flow` e ON b.`t_process_flow` = e.`id`
		LEFT JOIN `t_flow_reject` f ON e.`id` = f.`id_process_flow`
		LEFT JOIN `t_flow_reject_val` g ON f.`id_reject` = g.`id_reject` AND g.`id_wip` = a.`id`
		LEFT JOIN `t_reject_prod` h ON g.`id_reject` = h.`id_reject_prod`
		WHERE a.`id` = ?
		GROUP BY f.`id_reject` 
		ORDER BY f.`id_reject`
		';

		$query 	=  $this->db->query($sql,
			array(
				$id
			));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function detail_schedule_l($id)
	{
		$sql 	= "
		SELECT a.*,b.`instruction`,b.`lot_no_v`,b.`ink_type`,b.`status`,c.`rec_sheet_long`,c.`rec_sheet_wide`,c.`rec_img`,
		c.`rec_m2`,c.`rec_nopanel`,c.`rec_pcb`,e.`name`,e.`tipe_ins`,e.`element`,d.* 
		FROM t_wip a
		LEFT JOIN `t_production_schedule` op ON a.`id_po_quotation` = op.`id_prod` 
		LEFT JOIN t_process_mat b ON a.`id_process_flow` = b.`id_proc_mat` AND op.`id_esf` = b.`id_esf`
		LEFT JOIN `t_esf_layout` ef ON op.`id_esf` = ef.`id_esf_layout`
		LEFT JOIN `t_esf` c ON ef.`id_esf` = c.`id`
		LEFT JOIN `t_prod_layout` d ON d.`id_material` = c.`id_produk` AND ef.`sheet_panel` = d.`sheet_panel`
		LEFT JOIN `t_process_flow` e ON b.`t_process_flow` = e.`id`
		WHERE a.`id_po_quotation` = ? ORDER BY e.level";

		$query 	=  $this->db->query($sql,
			array(
				$id
			));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function flow($id)
	{
		// $sql 	= "
		// SELECT *,CONCAT('TOTAL ',ttl_schedule,' SPLIT, SPLIT ',split,'/',ttl_schedule,' ',panel_pcs_array,' PNL') AS ticket FROM (

// SELECT *,
// ROW_NUMBER() OVER (PARTITION BY id_po_quot ORDER BY `id_prod` ASC ) AS split FROM (

// SELECT a.*,d.*,b.order_no,b.order_date,g.name_eksternal,c.stock_name,
// p.name,es.pcb_type,es.pcs_array,es.panel_m2,es.panel_pcs_array,panel_pcs_m2,rec_nopanel,rec_pcb,
// `approved_laminate`,`laminate_grade`,`laminate`,`thickness`,`base_cooper`,`co_logo`,`img_logo`,type_logo,
// bmc.stock_code AS ccl, bc.no_pendaftaran AS bc,
// CONCAT(a.qty,' PCS/ ',panel_pcs_array,' PNL/ ',qty_sheet,' SHT') AS rqd,
// ttl_schedule,od.qty AS jumlah_order, price, diskon,
// ROW_NUMBER() OVER (PARTITION BY a.`id_prod` ORDER BY `id_process_flow` DESC ) AS rng FROM t_production_schedule a
// LEFT JOIN t_po_quotation b ON a.`id_po_quot` = b.`id`
// LEFT JOIN m_material c ON a.`id_produk` = c.`id`
// LEFT JOIN t_eksternal g ON b.cust_id = g.id
// LEFT JOIN (SELECT * FROM t_wip WHERE `status` = 1) d ON a.`id_prod` = d.`id_production_sch`
// LEFT JOIN t_process_flow p ON d.id_process_flow = p.id
// LEFT JOIN t_esf es ON b.id = es.id_po_quotation
// LEFT JOIN t_cust_spec cs ON a.id_produk = cs.id_produk
// LEFT JOIN ( SELECT id_po_quot,id_mainproduk FROM t_bom GROUP BY id_po_quot ) bm ON b.`id` = bm.id_po_quot 
// LEFT JOIN m_material bmc ON bm.`id_mainproduk` = bmc.`id`
// LEFT JOIN t_bc bc ON bmc.no_bc = bc.id
// LEFT JOIN ( SELECT `id_po_quot`,COUNT(*) AS ttl_schedule FROM `t_production_schedule` GROUP BY `id_po_quot` ) ttl_lot ON b.`id` = ttl_lot.id_po_quot  
// LEFT JOIN `t_order` od ON b.`id` = od.id_po_quotation AND a.id_produk = od.id_produk
 

// ) po
// WHERE ticket = 1
// AND id_prod = ?

// ) op";


$sql = "
SELECT a.*,b.`order_no`,b.`req_date`,b.`status`,b.`req_no`,c.*,
			d.`qty` AS qty_order, f.`name_eksternal`,e.`stock_name`,k.`sheet_panel`,
			`approved_laminate`,`laminate_grade`,`laminate`,`thickness`,`base_cooper`,`co_logo`,`img_logo`,type_logo
			  FROM `t_production_schedule` a
			LEFT JOIN t_po_quotation b ON a.`id_po_quot` = b.`id`
			LEFT JOIN `t_esf` c ON a.`id_produk` = c.`id_produk`
			LEFT JOIN `t_esf_layout` k ON k.`id_esf` = c.`id`
			LEFT JOIN t_cust_spec h ON h.`id_produk` = a.`id_produk`
			LEFT JOIN t_order d ON d.`id_produk` = a.`id_produk` AND d.`id_po_quotation` = a.`id_po_quot`
			LEFT JOIN m_material e ON d.`id_produk` = e.`id`
			LEFT JOIN t_eksternal f ON e.`cust_id` = f.`id` 
			   WHERE id_prod = ? 
";

		$query 	=  $this->db->query($sql,
			array(
				$id
			));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}


	public function update_status_new($id){
		
		$sql 	= '
		
				UPDATE `t_production_schedule` SET `status_prod` = 1
				WHERE `id_prod`  = '.$id.'
			
			';

		$query 	= $this->db->query($sql);

		$this->db->close();
		$this->db->initialize();
		
		
		$sql 	= '
		
				UPDATE `t_wip` SET `status` = 1, qty_in = (SELECT qty FROM `t_production_schedule` WHERE `id_prod` = '.$id.')
				WHERE `id_production_sch` = '.$id.'
				AND seqeuence = (SELECT seqeuence FROM `t_wip` WHERE `id_production_sch` = '.$id.' ORDER BY id DESC LIMIT 1)
			
			';

		$query 	= $this->db->query($sql);

		$this->db->close();
		$this->db->initialize();
		
	}
	
	public function update_status_done($id){
		
		$sql 	= '
		
				UPDATE `t_production_schedule` SET `status_prod` = 2
				WHERE `id_prod`  = '.$id.'
			
			';

		$query 	= $this->db->query($sql);

		$this->db->close();
		$this->db->initialize();
		
	}
	/**
      * This function is get data in production_schedule table by id
      * @param : $id is where condition for select query
      */

	public function edit_schedule_production($data)
	{
		$schedulDeletedList = $data['schedulDeletedList'];
		$deletedItemsLen = count($schedulDeletedList);

		$schedulList = $data['schedulList'];
		$itemsLen = count($schedulList);

		$produkTemp = '';

		if($itemsLen>0){
			for($i=0;$i<$deletedItemsLen;$i++){
				if($schedulDeletedList[$i]['id_produk'] != $produkTemp){
					$this->db->where('id_po_quot', $schedulDeletedList[$i]['id_po_quot']);
					$this->db->where('id_produk', $schedulDeletedList[$i]['id_produk']);
					  
					$this->db->delete('t_production_schedule');

					$produkTemp = $schedulDeletedList[$i]['id_produk'];
				}
			}

			for($j=0;$j<$itemsLen;$j++){
				$sql 	= 'CALL prodsche_add(?, ?, ?, ?, ?, ?, ?)';

				$this->db->query($sql,array(
					$schedulList[$j]['id_po_quot'],
					$schedulList[$j]['id_produk'],
					$schedulList[$j]['date_issue'],
					$schedulList[$j]['lot_number'],
					$schedulList[$j]['qty'],
					$schedulList[$j]['qty_sheet'],
					$schedulList[$j]['due_date']
				));
			}
		}

		$this->db->close();
		$this->db->initialize();
	}

	public function edit_flow_production($data,$date_now_f,$user_id)
	{
		
		//print_r($data);die;
		
		$sql 	= '
		
				UPDATE `t_wip` SET `status` = 2, qty_in = '.$data['in'].',qty_out = '.$data['out'].', note = "'.$data['note'].'", date_insert = "'.$date_now_f.'", pic = '.$user_id.'
				WHERE `id` = '.$data['id'].'
				
			
			';

		$query 	= $this->db->query($sql);
		

		$this->db->close();
		$this->db->initialize();
	}	
	
	public function edit_flow_reject($reject,$reject_id)
	{
		
		//print_r($data);die;
		
		$sql 	= '
		
				UPDATE `t_flow_reject_val` SET `value` = '.$reject.'
				WHERE `id_flow_rej` = '.$reject_id.'
				
			
			';

		$query 	= $this->db->query($sql);
		

		$this->db->close();
		$this->db->initialize();
	}

	/**
      * This function is get data in production_schedule table by id
      * @param : $id is where condition for select query
      */

	public function detail_schedule($id,$id_produk)
	{
		$sql 	= 'CALL prodsche_search_id(?, ?)';

		$query 	=  $this->db->query($sql,
			array(
				$id,
				$id_produk
			));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is get data in esf table by id_po_quotation and id_produk
      * @param : $id is where condition for select query
      */

	public function detail_esf($id_po_quotation,$id_produk)
	{
		
		//echo $id_po_quotation.' '.$id_produk;die;
		$sql 	= 'SELECT 
					      rec_nopanel,
					      rec_pcb
					FROM 
					      t_esf 
					WHERE 
					      id_po_quotation = ? AND id_produk=?';

		//echo $sql;die;

		$query 	=  $this->db->query($sql,
			array(
				$id_po_quotation,
				$id_produk
			));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
}