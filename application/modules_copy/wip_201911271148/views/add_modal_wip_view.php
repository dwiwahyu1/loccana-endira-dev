  <style>
	  #loading-us{display:none}
	  #tick{display:none}

	  #loading-mail{display:none}
	  #cross{display:none}
	  .form-item{margin-top: 14px;overflow: auto;}
  </style>

	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
    <div class="item form-group form-item">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Process Flow <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
            <select class="form-control" name="id_process_flow" id="id_process_flow" style="width: 100%" required>
				<?php foreach($process_flow as $key) { ?>
					<option value="<?php echo $key['id']; ?>" ><?php echo $key['name']; ?></option>
				<?php } ?>
            </select>
        </div>
    </div>

    <div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_diperlukan">Date </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
            <div class="input-group">
				<input placeholder="Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="date" name="date" value="<?php echo date('Y-m-d');?>">
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
        </div>
    </div>
	
    <div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qtyin">Qty In<span class="required"><sup>*</sup></span></label>
        <div class="col-md-8 col-sm-6 col-xs-12">
            <input data-parsley-maxlength="255" type="number" id="qtyin" name="qtyin" class="form-control col-md-7 col-xs-12" placeholder="Qty" min="1" maxlength="7" value="0" required="required">
        </div>
    </div>

    <div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qtyin">Qty Out<span class="required"><sup>*</sup></span></label>
        <div class="col-md-8 col-sm-6 col-xs-12">
            <input data-parsley-maxlength="255" type="number" id="qtyout" name="qtyout" class="form-control col-md-7 col-xs-12" placeholder="Qty" min="1" maxlength="7" value="0" required="required">
        </div>
    </div>
	
    <div class="item form-group form-item">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="m2">M&sup2</label>
        <div class="col-md-8 col-sm-6 col-xs-12">
            <input data-parsley-maxlength="255" type="text" id="m2" name="m2" class="form-control col-md-7 col-xs-12" placeholder="M&sup2" min="1" maxlength="7" value="0.0" required="required" readonly>
        </div>
    </div>
	
    <div class="item form-group form-item">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
        <div class="col-md-8 col-sm-6 col-xs-12">
        	<button id="btn-submitwip" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Wip</button>
        </div>
    </div>

<script type="text/javascript">
$(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();

    $("#date").datepicker({
		  format: 'yyyy-mm-dd',
		  autoclose: true,
		  todayHighlight: true,
	});
});

$('#qtyin,#qtyout').on('change',(function(e) {
	var qty = ($('#qtyin').val()*$('#qtyout').val())/2;
	$('#m2').val(parseFloat(qty*parseFloat($('#panel_m2').val())));
}));

$('#btn-submitwip').on('click',(function(e) {
    if($('#item_name').val() === ''){
        swal("Warning", "Nama Barang harus diisi.", "error");
    }else{
        $(this).attr('disabled','disabled');
        $(this).text("Memasukkan data...");

        var id_po_quotation = $('#id_po_quotation').val(),
        	id_produk = $('#id_produk').val(),
        	id_process_flow = $('#id_process_flow').val(),
            date = $('#date').val(),
            qtyin = $('#qtyin').val(),
            qtyout = $('#qtyout').val(),
            m2 = $('#m2').val(),
            text_process_flow = $('#id_process_flow option:selected').text();
		
        var datawip = {
        	id_po_quotation,
        	id_produk,
            id_process_flow,
            date,
            qtyin,
            qtyout,
            m2,
            text_process_flow
        };
        
        addWip.push(datawip);

        $('#panel-modalwip .panel-heading button').trigger('click');

        itemsElem();
    }
  
}));
</script>
