<style>
   .form-item{margin-top: 15px;overflow: auto;}
</style>

<p style="text-align:center;">Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Issue Date
		<span class="required">
			<sup>*</sup>
		</span>
	</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<div class="input-group date">
		   <input placeholder="Issue Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="date_issue" name="date_issue" required="required" value="<?php echo date('Y-m-d');?>">
			<div class="input-group-addon">
				<span class="glyphicon glyphicon-th"></span>
			</div>
		</div>
	</div>
</div>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Lot No</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
	   	<input data-parsley-maxlength="255" type="text" id="lot_number" name="lot_number" class="form-control col-md-7 col-xs-12" placeholder="Lot No" value="">
	</div>
</div>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty">Order Qty
		<span class="required">
			<sup>*</sup>
		</span>
	</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
	   	<input type="number" min="0" id="order_qty" name="order_qty" class="form-control col-md-7 col-xs-12" placeholder="Order Qty" value="0" required="required" readonly>
	</div>
</div>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty">Run Down Qty(sheet)
		<span class="required">
			<sup>*</sup>
		</span>
	</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
	   	<input type="number" min="0" id="qty" name="qty" class="form-control col-md-7 col-xs-12" placeholder="Run Down Qty" value="0" required="required">
	</div>
</div>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="schedule">Due Date</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
	   	<label style="width:70px;font-weight: inherit;">
	   		<input type="radio" id="asap_schedule" name="type" value="ASAP" checked> <span>ASAP</span>
	   	</label>
	   	<label style="font-weight: inherit;">
	   		<input type="radio" id="date_schedule" name="type" value="<?php echo date('Y-m-d');?>">
	   		<input type="text" id="date" class="form-control datepicker" style="width: 357px;display: inline-block;" value="<?php echo date('Y-m-d');?>">
	   		<div class="input-group-addon" style="width: 40px;float: right;height: 36.99px;line-height: 26.99px;">
				<span class="glyphicon glyphicon-th"></span>
			</div>
	   	</label>
	</div>
</div>

<div class="item form-group form-item" id="btn-addtemp" style="margin-bottom: 0px;">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama" style="float: right;">
		<button id="btn-add" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">
			<i class="fa fa-plus"></i> Add
		</button>
	</label>
</div>

<div class="item form-group form-item" id="scheduletext" style="margin-top: 0px;">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Schedule List : </label>
</div>

<div class="item form-group form-item" id="schedulelist">
	<table id="listschedule" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Issue Date</th>
				<th>Lot No</th>
				<th>Run Down Qty(array)</th>
				<th>Moving Ticket No</th>
				<th>Splits Qty</th>
				<th>Due Date</th>
				<th>Option</th>
			</tr>
		</thead>
		<tbody>
			<?php
				$i=0; 
				foreach($detail as $key) {
					$schedule_temp = $i+1;
			?>
				<tr id="schedule_temp<?php echo $i;?>">
					<td><?php if(isset($key['date_issue'])){ echo $key['date_issue']; }?></td>
					<td><?php if(isset($key['lot_number'])){ echo $key['lot_number']; }?></td>
					<td><?php if(isset($key['qty'])){ echo number_format($key['qty'],2,",","."); }?></td>
					<td><?php echo $schedule_temp.'/'.count($detail);?></td>
					<td><?php echo count($detail); ?></td>
					<td><?php if(isset($key['due_date'])){ echo $key['due_date']; }?></td>
					<td>
						<button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteSchedule('<?php echo $i;?>')">
							<i class="fa fa-trash"></i>
						</button>
					</td>
				</tr>
			<?php $i++;} ?>
		</tbody>
	</table>
</div>

<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<button id="btn-submitschedule" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Schedule</button>
	</div>
</div>

<input type="hidden" id="id_produk" value="<?php if(isset($id_produk)){ echo $id_produk; }?>">
<input type="hidden" id="id_po_quot" value="<?php if(isset($id)){ echo $id; }?>">
<input type="hidden" id="nopanel" value="<?php if(isset($detail_esf[0]['rec_nopanel'])){ echo $detail_esf[0]['rec_nopanel']; }?>">
<input type="hidden" id="pcb" value="<?php if(isset($detail_esf[0]['rec_pcb'])){ echo $detail_esf[0]['rec_pcb']; }?>">
<!-- /page content -->

<script type="text/javascript">
	var scheduleArray = [],
		qtyTemp = 0;

	var order_qty = parseInt($('#order_qty<?php echo $id_produk;?>').text());
	$('#order_qty').val(order_qty);

    "<?php
    foreach($detail as $key) {
      $date_issue = $key['date_issue'];
      $lot_number = $key['lot_number'];
      $qty = $key['qty'];
      $qty_sheet = $key['qty_sheet'];
      $due_date = $key['due_date'];
  ?>"
      var id_po_quot = "<?php echo $id; ?>";
  	  var id_produk = "<?php echo $id_produk; ?>";
      var date_issue = "<?php echo $date_issue; ?>";
      var lot_number = "<?php echo $lot_number; ?>";
      var qty = "<?php echo $qty; ?>";
      var qty_sheet = "<?php echo $qty_sheet; ?>";
      var due_date = "<?php echo $due_date; ?>";

      var scheduleData = {
      	        "id_po_quot":id_po_quot,
      	        "id_produk":id_produk,
				"date_issue":date_issue,
				"lot_number":lot_number,
				"qty":qty,
				"qty_sheet":qty_sheet,
				"due_date":due_date
			};

	  scheduleArray.push(scheduleData);

	  qtyTemp = qtyTemp + parseInt(qty_sheet);
  "<?php    
    }
  ?>"

  schedulDeletedList.push({"id_po_quot":$('#id_po_quot').val(),
      	        				 "id_produk":$('#id_produk').val()});

  var qtyMax = order_qty - qtyTemp;

	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();

		$("#issue_date").datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});

		$("#date").datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});

		if(qtyTemp === parseInt($('#order_qty<?php echo $id_produk;?>').text())){
			$('#btn-addtemp').hide();
		}

		$('#qty').attr('max',qtyMax);
	});

	$('#qty').on('change',(function(e) {
		if(parseInt($(this).val()) > order_qty){
			$(this).val(0);
		}
	}));

	$('#btn-add').on('click',(function(e) {
		var date_issue = $('#date_issue').val(),
			qty = (parseInt($('#qty').val())*parseInt($('#nopanel').val()))*parseInt($('#pcb').val()),
			lot_number = $('#lot_number').val(),
			due_date = $('input[name="type"]:checked').val(),
			qty_sheet = parseInt($('#qty').val());
					
		if(due_date!=='ASAP'){due_date = $('#date').val();}

		var scheduleData = {
			    "id_po_quot":$('#id_po_quot').val(),
      	        "id_produk":$('#id_produk').val(),
				"date_issue":date_issue,
				"lot_number":lot_number,
				"qty":qty,
				"qty_sheet":qty_sheet,
				"due_date":due_date
			};

	  	scheduleArray.push(scheduleData);
	  	
	  	qtyMax = qtyMax - parseInt(qty_sheet);
	  	
	  	if(qtyMax === 0){
			$('#btn-addtemp').hide();
		}

	  	$('#qty').attr('max',qtyMax);
	  	$('#qty').val(0);

	  	listSchedule();
	}));

	$('#btn-submitschedule').on('click',(function(e) {
		for(var i=0;i<schedulList.length;i++){
			if(schedulList[i]['id_produk'] === "<?php echo $id_produk;?>")schedulList.splice(i,1);;
		}

		var countRunQty = 0;
		for(var j=0;j<scheduleArray.length;j++){
			countRunQty = countRunQty + parseInt(scheduleArray[j]['qty']);
			schedulList.push(scheduleArray[j]);	
		}

		$('#produk<?php echo $id_produk;?>').html(currencyFormatDE(countRunQty));

		$('#panel-modalchild .panel-heading button').trigger('click');
	}));

	function deleteSchedule(id,max){
		$('#schedule_temp'+id).remove();
	    scheduleArray.splice(id,1);

	    qtyMax = qtyMax + max;
		$('#qty').attr('max',qtyMax);

	    listSchedule();

	    $('#btn-addtemp').show();
	}

	function listSchedule(){
		var scheduleLen = scheduleArray.length;

		var element ='';
	    for(var i=0;i<scheduleLen;i++){
	    	var ticket = (i+1)+'/'+scheduleLen;
	        element +='   <tr>';
	        element +='     <td>'+scheduleArray[i]["date_issue"]+'</td>';
	        element +='     <td>'+scheduleArray[i]["lot_number"]+'</td>';
	        element +='     <td>'+currencyFormatDE(scheduleArray[i]["qty"])+'</td>';
	        element +='     <td>'+ticket+'</td>';
	        element +='     <td>'+scheduleLen+'</td>';
	        element +='     <td>'+scheduleArray[i]["due_date"]+'</td>';
	        element +='     <td>';
	        element +='       <div class="btn-group">';
	        element +='         <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteSchedule('+i+','+scheduleArray[i]["qty"]+')">';
	        element +='         <i class="fa fa-trash"></i>';
	        element +='       </div>';
	        element +='     </td>';
	        element +='   </tr>';
	    }

	    $('#listschedule tbody').html(element);
	}

	function currencyFormatDE(num) {
		var num = parseFloat(num);
	  return (
	    num
	      .toFixed(2) // always two decimal digits
	      .replace('.', ',') // replace decimal point character with ,
	      .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
	  ) // use . as a separator
	}
</script>
