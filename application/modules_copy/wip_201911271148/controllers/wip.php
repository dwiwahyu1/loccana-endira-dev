<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Wip extends MX_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('quotation/quotation_model');
        $this->load->model('wip/schedule_production_model');
        $this->load->library('log_activity');
        $this->load->helper('url');
    }

    /**
     * anti sql injection
     */
    public function Anti_sql_injection($string)
    {
        $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
        return $string;
    }

    public function index()
    {
        $this->template->load('maintemplate', 'wip/views/index');
    }


    public function prosess()
    {

        $id = $this->uri->segment(3);
        // $split = $this->uri->segment(4);
        // $prosess = $this->uri->segment(5);


        // if($prosess == 3){

        // $this->schedule_production_model->update_status_new($id);

        // }

        $detail = $this->schedule_production_model->flow($id);
        $flow = $this->schedule_production_model->detail_schedule_l($id);
        // var_dump($id,$split,$prosess);die; 

        $data = array(
            'detail' => $detail[0],
            'flow' => $flow,
            'ido' => $id,
            // 'split' => $split 
        );

        //$this->load->view('proses',$data);
        $this->template->load('maintemplate', 'wip/views/proses', $data);
    }

    public function done()
    {

        $id = $this->uri->segment(3);

        // if($prosess == 3){

        $this->schedule_production_model->update_status_done($id);

        // }

        // $detail = $this->schedule_production_model->flow($id);
        // $flow = $this->schedule_production_model->detail_schedule_l($id);
        // //print_r($detail);die;

        // $data = array(
        // 'detail'=> $detail[0],
        // 'flow'=> $flow,
        // 'ido'=>$id,
        // 'split' => $split 
        // );

        // //$this->load->view('proses',$data);

        // $this->template->load('maintemplate', 'wip/views/proses',$data);
        redirect('/wip', 'refresh');
    }

    /**
     * This function is used for showing schedule production list
     * @return Array
     */
    function lists()
    {
        $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
        $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
        $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
        $order = $this->input->get_post('order');
        $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'asc';
        $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

        $order_fields = array('', 'id_prod', 'name_eksternal', 'order_no');

        $search = $this->input->get_post('search');

        $search_val = (!empty($search['value'])) ? $search['value'] : null;

        $search = $this->input->get_post('search');
        $cust_id = $this->input->get_post('cust_id');

        $search_value = $this->Anti_sql_injection($search_val);

        // Build params for calling model
        $params['limit'] = (int) $length;
        $params['offset'] = (int) $start;
        $params['order_column'] = $order_fields[$order_column];
        $params['order_dir'] = $order_dir;
        $params['filter'] = $search_value;
        $params['cust_id'] = $cust_id;

        $list = $this->schedule_production_model->lists($params);

        //print_r($list);die;

        $result["recordsTotal"] = $list['total'];
        $result["recordsFiltered"] = $list['total_filtered'];
        $result["customer"] = $this->quotation_model->customer();
        $result["draw"] = $draw;

        $data = array();

        $i = $params['offset'];
        foreach ($list['data'] as $k => $v) {
            $i++;
            $actions = '';
            // if($v['status_prod'] == '2'){

            // $status_flow = $v['name'];

            // $actions = 'Done';

            // }else{

            // $status_flow = $v['name'];

            // $actions = '<div class="btn-group">';
            // $actions .= '   <a href="wip/prosess/'. $v['id_prod'].'/'. $v['split'].'/1" class="btn btn-warning"  data-toggle="tooltip" data-placement="top" title="Proses Schedule"  />';
            // $actions .= '       <i class="fa fa-edit"></i>';
            // $actions .= '   <a href="wip/done/'. $v['id_prod'].'" class="btn btn-success"  data-toggle="tooltip" data-placement="top" title="Proses Selesai" />';
            // $actions .= '       <i class="fa fa-check"></i>';
            // $actions .= '</div>';

            // }

            // if($v['status_prod'] == '1'){
            // $status_flow = $v['name'];
            if ($v['status'] == 'Approve') {
                $actions .= '<div class="btn-group">';
                $actions .= '   <button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="On Production" onClick="app_sch(\'' . $v['id_prod'] . '\')">';
                $actions .= '       <i class="fa fa-check"></i>';
                $actions .= '   </button>';
                $actions .= '</div>';
            } elseif ($v['status'] == 'On Production') {

                $actions = '<div class="btn-group">';
                $actions .= '   <a href="' . base_url('wip/prosess/') . '/' . $v['id_prod'] . '" class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Proses Schedule">';
                $actions .= '       <i class="fa fa-edit"></i>';
                $actions .= '   </a>';
                $actions .= '</div>';

                $actions .= '<div class="btn-group">';
                $actions .= '   <button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Done Production" onClick="done_sch(\'' . $v['id_prod'] . '\')">';
                $actions .= '       <i class="fa fa-check"></i>';
                $actions .= '   </button>';
                $actions .= '</div>';
            }
            // }elseif($v['status_prod'] == '2'){
            // $status_flow = "Done";

            // $actions = '<div class="btn-group">';
            // $actions .= '   <a href="wip/prosess/'. $v['id_prod'].'/'. $v['split'].'/2" class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Proses Schedule">';
            // $actions .= '       <i class="fa fa-edit"></i>';
            // $actions .= '   </button>';
            // $actions .= '</div>';

            // }else{
            // $status_flow = "Waiting";

            // $actions = '<div class="btn-group">';
            // $actions .= '   <a href="wip/prosess/'. $v['id_prod'].'/'. $v['split'].'/3" class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Proses Schedule">';
            // $actions .= '       <i class="fa fa-plus"></i>';
            // $actions .= '   </button>';
            // $actions .= '</div>';
            // }

            array_push(
                $data,
                array(
                    $i,
                    $v['order_no'],
                    $v['name_eksternal'],
                    $v['qty_order'],
                    $v['date_issue'],
                    $v['tipe'],
                    $v['qty'] . " PCS / " . $v['sheet_panel'] * $v['qty_sheet'] . " PNL (" . $v['pcs_ary_panel'] . " UP) / " . $v['qty_sheet'] . " SHT",
                    $v['qty'],
                    $v['status'],
                    $actions
                )
            );
        }

        $result["data"] = $data;

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
     * This function is redirect to edit schedule production page
     * @return Void
     */
    public function app_sch()
    {
        $data       = file_get_contents("php://input");
        $params     = json_decode($data, true);

        $this->schedule_production_model->app_schd($params['id']);

        $msg = 'Berhasil merubah Schedule Production';

        $result = array(
            'success' => true,
            'message' => $msg
        );

        $this->log_activity->insert_activity('rubah', $msg . ' dengan id ' . $params['id']);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function done_sch()
    {
        $data       = file_get_contents("php://input");
        $params     = json_decode($data, true);

        $this->schedule_production_model->done_sch($params['id']);

        $msg = 'Berhasil merubah Schedule Production';

        $result = array(
            'success' => true,
            'message' => $msg
        );

        $this->log_activity->insert_activity('rubah', $msg . ' dengan id ' . $params['id']);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function edit_flow($id)
    {
        $detail = $this->schedule_production_model->detail_flows($id);

        //print_r($detail);die;

        $data = array(
            'id'    => $id,
            'detail' => $detail[0],
            'detail_full' => $detail
        );

        $this->load->view('edit_flow_view', $data);
    }

    public function edit($id)
    {
        $detail = $this->schedule_production_model->detail($id);

        $data = array(
            'id'    => $id,
            'detail' => $detail
        );

        $this->load->view('edit_modal_view', $data);
    }

    /**
     * This function is used to edit schedule of schedule production data
     * @return Array
     */
    public function edit_schedule_production()
    {
        $data   = file_get_contents("php://input");
        $params = json_decode($data, true);

        $schedule = $this->schedule_production_model->edit_schedule_production($params);

        $msg = 'Berhasil mengubah schedule production';

        $result = array(
            'success' => true,
            'message' => $msg
        );

        $this->log_activity->insert_activity('update', $msg);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }


    public function edit_prosess_flow()
    {
        $data   = file_get_contents("php://input");
        $params = json_decode($data, true);


        //print_r($params['reject_id'][0]);die;
        $user_id = $this->session->userdata['logged_in']['user_id'];



        $tz = 'Asia/Jakarta';
        $timestamp = time();
        $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
        $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
        $date_now =  $dt->format('Y_d_m_H_i_s');
        $date_now_f =  $dt->format('Y-m-d H:i:s');
        //print_r($params);die;

        $schedule = $this->schedule_production_model->edit_flow_production($params, $date_now_f, $user_id);

        $arr_id = [];
        foreach ($params['reject_id'] as $rejects_id) {

            $arr_id[] = $rejects_id;
        }

        $ir = 0;
        foreach ($params['reject'] as $rejects) {

            $this->schedule_production_model->edit_flow_reject($rejects, $arr_id[$ir]);
            $ir++;
        }

        $msg = 'Berhasil mengubah Flow ';

        $result = array(
            'success' => true,
            'message' => $msg
        );

        $this->log_activity->insert_activity('update', $msg);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
     * This function is redirect to edit schedule production page
     * @return Void
     */
    public function edit_schedule($id, $id_produk)
    {
        $detail = $this->schedule_production_model->detail_schedule($id, $id_produk);
        $detail_esf = $this->schedule_production_model->detail_esf($id, $id_produk);

        //print_r($detail_esf);die;

        $data = array(
            'id' => $id,
            'id_produk' => $id_produk,
            'detail' => $detail,
            'detail_esf' => $detail_esf
        );

        $this->load->view('edit_modal_schedule_view', $data);
    }
}
