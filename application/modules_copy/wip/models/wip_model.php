<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Wip_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	/**
      * This function is get the list data in wip table
      * @param : $params is where condition for select query
      */

	public function lists($params = array())
	{
		$sql_all 	= 'CALL wip_list_all(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				$params['filter']
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL wip_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total_row,
			'total' => $total_row,
		);

		return $return;
	}

	public function get_po($params) {
		$sql 	= 'CALL po_search_wip(?,?,?,?)';

		if($params['datefrom'] != ''){
			$datefrom=date_create($params['datefrom']);
			$datefrom=date_format($datefrom,"Y-m-d");
		}else{
			$datefrom='';
		}

		if($params['dateto'] != ''){
			$dateto=date_create($params['dateto']);
			$dateto=date_format($dateto,"Y-m-d");
		}else{
			$dateto='';
		}

		$query 	=  $this->db->query($sql,
			array(
				$datefrom,
				$dateto,
				$params['searchby'],
				$params['searchby_text']
			)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	/**
      * This function is get data in wip table by id
      * @param : $id is where condition for select query
      */
	public function detail($id)
	{
		$idarray = explode('~', $id);
		$id_po_quot = $idarray[0];
		$id_produk = $idarray[1];

		$sql 	= 'SELECT 
						id_po_quotation,
						t_esf.id_produk,
						order_no,
						stock_name,
						name_eksternal,
						t_esf.unit,                        
                        pcs_array,                        
                        panel_m2
					FROM 
						t_po_quotation 
						LEFT JOIN t_eksternal ON t_po_quotation.cust_id = t_eksternal.id
						LEFT JOIN t_esf ON t_esf.id_po_quotation = t_po_quotation.id 
						LEFT JOIN m_material ON m_material.id = t_esf.id_produk 
					WHERE 
						t_esf.id_po_quotation='.$id_po_quot.' AND t_esf.id_produk='.$id_produk;

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is get data in process_flow table
      */
	public function process_flow() {
		$sql 	= 'SELECT 
						*
					FROM 
						t_process_flow
					ORDER BY 
						level ASC';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is used to Insert Record in wip table
      * @param : $data - record array 
      */

	public function add_wip($data)
	{
		$this->db->set('status', 6, FALSE);
		$this->db->where('id', $data[0]['id_po_quotation']);
		$this->db->update('t_po_quotation');

		$this->db->set('status', 6, FALSE);
		$this->db->where('id', $data[0]['id_produk']);
		$this->db->update('m_material');

		$dataLen = count($data);
		for($i=0;$i<$dataLen;$i++){
			$sql 	= 'CALL wip_add(?,?,?,?,?,?,?)';

			$query 	=  $this->db->query($sql,
				array(
					$data[$i]['id_po_quotation'],
					$data[$i]['id_produk'],
					$data[$i]['id_process_flow'],
					$data[$i]['date'],
					$data[$i]['qtyin'],
					$data[$i]['qtyout'],
					$data[$i]['m2']
				));
		}

		$this->db->close();
		$this->db->initialize();
	}

	/**
      * This function is used to Update Record in wip table
      * @param : $data - record array 
      */

	public function edit_wip($data)
	{
		$this->db->where('id_po_quotation', $data[0]['id_po_quotation']);
		$this->db->where('id_produk', $data[0]['id_produk']);

		$this->db->delete('t_wip'); 

		$dataLen = count($data);
		for($i=0;$i<$dataLen;$i++){
			$sql 	= 'CALL wip_add(?,?,?,?,?,?,?)';

			$query 	=  $this->db->query($sql,
				array(
					$data[$i]['id_po_quotation'],
					$data[$i]['id_produk'],
					$data[$i]['id_process_flow'],
					$data[$i]['date'],
					$data[$i]['qtyin'],
					$data[$i]['qtyout'],
					$data[$i]['m2']
				));
		}

		$this->db->close();
		$this->db->initialize();
	}

	/**
     * This function is used to delete wip
     * @param: $id - id of wip table
     */
	function delete_wip($id_po_quotation,$id_produk) {
		$this->db->where('id_po_quotation', $id_po_quotation);
		$this->db->where('id_produk', $id_produk);

		$this->db->delete('t_wip'); 

		$this->db->close();
		$this->db->initialize();
	}

	/**
      * This function is get data in wip table by id
      * @param : $id is where condition for select query
      */
	public function listwip($id)
	{
		$idarray = explode('~', $id);
		$id_po_quot = $idarray[0];
		$id_produk = $idarray[1];

		$sql 	= 'SELECT 
						id_process_flow,
						`date`,
						qty_in,
						qty_out,
						m2,
						name
					FROM 
						t_wip 
						LEFT JOIN t_process_flow ON t_wip.id_process_flow = t_process_flow.id
					WHERE 
						t_wip.id_po_quotation='.$id_po_quot.' AND t_wip.id_produk='.$id_produk;

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
}