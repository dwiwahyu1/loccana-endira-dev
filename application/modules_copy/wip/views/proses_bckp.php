<style>
  .filter_separator {
    padding: 0px 10px;
  }
</style>
<div class="container" id="printArea">
  <div class="row">
    <div class="col-sm-2">
      <h4 class="page-title">Work in Progress <a class="btn btn-icon waves-effect waves-light btn-primary m-b-5 btn-sm" data-toggle="tooltip" data-placement="bottom" title="Download Work In Progress Report" id="btn_download">
          <i class="fa fa-download"></i>
        </a></h4>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-6">
      <div class="card-box">
        <table id="ppp" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>Customer</th>
              <th><?php echo $detail['name_eksternal']; ?></th>
            <tr>
            <tr>
              <th>Part No</th>
              <th><?php echo $detail['stock_name']; ?></th>
            <tr>
            <tr>
              <th>Order Qty</th>
              <th><?php echo $detail['qty_order']; ?></th>
            <tr>
            <tr>
              <th>Pcb Type</th>
              <th><?php echo $detail['pcb_type']; ?></th>
            <tr>
            <tr>
              <th>PCS / ARY/ M2</th>
              <th><?php echo $detail['panel_pcs_m2']; ?></th>
            <tr>
            <tr>
              <th>Po Number</th>
              <th><?php echo $detail['order_no']; ?></th>
            <tr>
            <tr>
              <th>Panel / M2</th>
              <th><?php echo $detail['panel_m2']; ?></th>
            <tr>
            <tr>
              <th></th>
              <th></th>
            <tr>
            <tr>
              <th>Material & Thickness</th>
              <th><?php echo $detail['approved_laminate']; ?> <?php echo $detail['thickness']; ?> mc &nbsp &nbsp<?php echo $detail['laminate']; ?> mm</th>
            <tr>
            <tr>
              <th>Grade / UL Type</th>
              <th><?php echo $detail['laminate_grade']; ?> &nbsp <?php echo $detail['type_logo']; ?></th>
            <tr>
            <tr>
              <th>Base Copper</th>
              <th><?php echo $detail['base_cooper']; ?> /OZ</th>
            <tr>
            </tr>
          </thead>
        </table>


      </div>
    </div><!-- end col -->

    <div class="col-sm-6">
      <div class="card-box">

        <table id="ppps" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>Date Issue</th>
              <th><?php echo $detail['date_issue']; ?></th>
            <tr>
            <tr>
              <th>Lot No</th>
              <th></th>
            <tr>
            <tr>
              <th>Code CCL & No BC</th>
              <th></th>
            <tr>
            <tr>
              <th>Rundown Qty</th>
			  <!--$v['qty']." PCS / ".$v['sheet_panel']*$v['qty_sheet']." PNL (".$v['pcs_ary_panel']." UP) / ".$v['qty_sheet']." SHT", -->
              <th><?php echo $detail['qty_pcb'] . "PCS / " . $detail['rec_nopanel'] . " PNL (" . $detail['panel_pcs_array'] . " UP) / " . $detail['qty_sheet'] . " SHT" ?></th>
            <tr>
            <tr>
              <th>Moving Ticket</th>
              <th><?php echo 'TOTAL 1 SPLIT, SPLIT 1/1 ', $detail['panel_pcs_array'], ' PNL' ?></th>
            <tr>
            <tr>
              <th>Split Qty</th>
              <th><?php echo 1; ?></th>
            <tr>
            <tr>
              <th>Due Date</th>
              <th><?php echo $detail['due_date']; ?></th>
            <tr>
            <tr>
              <th></th>
              <th></th>
            <tr>
            <tr>
              <th>UL Logo</th>
              <th><?php echo strtoupper($detail['co_logo']); ?> </th>
            <tr>
            <tr>
              <th>Company Logo</th>
              <th><?php echo strtoupper($detail['co_logo']); ?> &nbsp <?php $ddd = explode(' ', $detail['type_logo']);
                                                                      echo $ddd[0]; ?></th>
            <tr>
            <tr>
              <th>Week Code</th>
              <th></th>
            <tr>
            </tr>
          </thead>
        </table>

      </div>
    </div><!-- end col -->

    <div class="col-sm-9">
      <div class="card-box">

        <table id="ppps2" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>Open<br>Seq</th>
              <th>Process Flow</th>
              <th>Instruction</th>
              <th>Qty In</th>
              <th>Qty Reject</th>
              <th>Qty Out</th>
              <th>Date</th>
              <th>By</th>
              <th>Option</th>
            <tr>
          </thead>
          <tbody id="body_tbl">

            <?php $no = 1;
            foreach ($flow as $flows) { ?>

              <tr>
                <td><?php echo $no; ?></td>
                <td><?php echo $flows['element'] ?></td>
                <td><?php if ($flows['tipe_ins'] == 1) {
                        echo $flows['instruction'];
                      } else {
                        echo ' <img src="' . base_url() . 'uploads/layout/' . $flows['instruction'] . '"  style="height: 50px;"/>';
                      } ?></td>
                <td><?php echo $flows['qty_in'] ?></td>
                <td><?php if ($flows['qty_out'] == '') {
                        echo '0';
                      } else {
                        echo $flows['qty_in'] - $flows['qty_out'];
                      } ?></td>
                <td><?php echo $flows['qty_out'] ?></td>
                <td><?php echo $flows['date_insert'] ?></td>
                <td><?php echo $flows['pic'] ?></td>
                <!--<?php if ($flows['status'] == 1) { ?><div class="btn-group"> <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Proses Schedule" onClick="edit_flow(<?php echo $flows['id'] ?>)"><i class="fa fa-plus"></i></button></div><?php } elseif ($flows['status'] == 2) {
                                                                                                                                                                                                                                                                                                echo "Done";
                                                                                                                                                                                                                                                                                              } else {
                                                                                                                                                                                                                                                                                                echo "Waiting";
                                                                                                                                                                                                                                                                                              } ?>< -->
                <td>
                  <div class="btn-group"> <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Proses Schedule" onClick="edit_flow(<?php echo $flows['id'] ?>)"><i class="fa fa-plus"></i></button></div>
                </td>
              <tr>

              <?php $no++;
              } ?>
          </tbody>
        </table>
        <table id="ppps2-hidden" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="display:none">
          <thead>
            <tr>
              <th>Open<br>Seq</th>
              <th>Process Flow</th>
              <th>Instruction</th>
              <th>Qty In</th>
              <th>Qty Reject</th>
              <th>Qty Out</th>
              <th>Date</th>
              <th>By</th>
            </tr>
          </thead>
          <tbody id="body_tbl">

            <?php $no = 1;
            foreach ($flow as $flows) { ?>

              <tr>
                <td><?php echo $no; ?></td>
                <td><?php echo $flows['element'] ?></td>
                <td><?php if ($flows['tipe_ins'] == 1) {
                        echo $flows['instruction'];
                      } else {
                        $url = base_url() . 'uploads/layout/' . $flows['instruction'];
                        echo '<br><br><br> <img src="data:image/jpeg;base64,' . base64_encode(file_get_contents($url)) . '" id="vcut_temp" style="height: 50px;"/>';
                      } ?></td>
                <td><?php echo $flows['qty_in'] ?></td>
                <td><?php if ($flows['qty_out'] == '') {
                        echo '0';
                      } else {
                        echo $flows['qty_in'] - $flows['qty_out'];
                      } ?></td>
                <td><?php echo $flows['qty_out'] ?></td>
                <td><?php echo $flows['date_insert'] ?></td>
                <td><?php echo $flows['pic'] ?></td>
              </tr>

            <?php $no++;
            } ?>
            <tr rowspan="3">
              <td colspan="8">
                <br>
                Prepared BY : ____________________________________________________________________________________________
                Date : ___________________________
                <br><br>
                Confirmed BY : ___________________________________________________________________________________________
                Date : ___________________________
                <br><br>
                Approved BY : ____________________________________________________________________________________________
                Date : ___________________________
                <br>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div><!-- end col -->

    <div class="col-md-3 col-sm-3 col-xs-12">
      <div class="card-box">
        <h4 style="text-align: center;" id="title-cutting-map">CUTTING MAP</h4>
        <hr>
        </hr>
        <h5 id="lv1-cutting-map">SHEET SIZE: <?php echo $flow[0]['sheet_size'] ?></h5>
        <h5 id="lv2-cutting-map">PANEL SIZE: <?php echo $flow[0]['panel_size'] ?></h5>
        <h5 id="lv3-cutting-map">PANEL/SHEET: <?php echo $flow[0]['sheet_panel'] ?></h5>
        <img src="<?php echo base_url() ?>uploads/esf/layout/<?php echo $flow[0]['cutting_map'] ?>" id="cutting_map_temp" style="width:250px; height: 250px;" />
      </div>
      <div class="card-box">
        <h4 style="text-align: center;" id="title-boarding-layout">BOARD LAYOUT</h4>
        <hr>
        </hr>
        <h5 id="lv1-boarding-layout">PCS /ARY /PNL: <?php echo $flow[0]['pcs_ary_pnl'] ?></h5>
        <h5 id="lv2-boarding-layout">BDS /ARY /SIZE: <?php echo $flow[0]['bds_ary_pnl'] ?></h5>
        <h5>:</h5>

        <img src="<?php echo base_url() ?>uploads/esf/layout/<?php echo $flow[0]['board_layout'] ?>" id="board_layout_temp" style="width:250px; height: 250px;" />


      </div>
      <div class="card-box">
        <h4 style="text-align: center;" id="title-remarks">REMARKS</h4>
        <hr>
        </hr>
        <h5><?php echo $flow[0]['remark'] ?></h5>

      </div>
    </div><!-- end col -->
  </div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content p-0 b-0">
      <div class="panel panel-color panel-primary panel-filled">
        <div class="panel-heading">
          <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 class="panel-title"></h3>
        </div>
        <div class="panel-body">
          <p></p>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;">
  <div class="modal-dialog" style="width:800px;">
    <div class="modal-content p-0 b-0">
      <div class="panel panel-color panel-primary panel-filled">
        <div class="panel-heading">
          <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 class="panel-title"></h3>
        </div>
        <div class="panel-body">
          <p></p>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  function log_img() {
    var img = td.getElementsByTagName('img');
    console.log(img);
  }
  $(document).ready(function() {
    // listsp();

    toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
      //console.log('RESULT:', dataUrl)
      dataImage = dataUrl;
    })
    toDataURL('<?php echo base_url() ?>uploads/layout/<?php echo $flow[0]['cutting_map'] ?>', function(dataUrl) {
      if ('<?php echo base_url() ?>uploads/layout/<?php echo $flow[0]['cutting_map'] ?>' != '<?php echo base_url() ?>uploads/layout/')
        dataImageCuttingMap = dataUrl;
      else
        dataImageBoardLayout = null;
    })
    toDataURL('<?php echo base_url() ?>uploads/layout/<?php echo $flow[0]['board_layout'] ?>', function(dataUrl) {
      if ('<?php echo base_url() ?>uploads/layout/<?php echo $flow[0]['board_layout'] ?>' != '<?php echo base_url() ?>uploads/layout/')
        dataImageBoardLayout = dataUrl;
      else
        dataImageBoardLayout = null;
    })
    $('#btn_download').click(function() {
      var doc = new jsPDF('p', 'mm', 'letter');
      var imgData = dataImage;
      var imgDataCuttingMap = dataImageCuttingMap;
      var imgDataImageBoardLayout = dataImageBoardLayout;
      var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
      var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

      // FOOTER
      doc.setTextColor(100);
      doc.addImage(imgData, 'JPEG', 5, 5, 25, 25)
      doc.setFontSize(36);
      doc.text("PRODUCTION TRAVELLER", 33, 23, 'left');
      doc.setFontSize(20);
      doc.autoTable({
        html: '#ppp',
        theme: 'plain',
        styles: {
          fontSize: 6,
          lineColor: [116, 119, 122],
          lineWidth: 0.1,
          cellWidth: 'auto',

        },
        margin: 4.5,
        tableWidth: (pageWidth / 2),
        headStyles: {
          valign: 'middle',
          halign: 'center',
        },
        columnStyles: {
          0: {
            halign: 'left'
          },
          1: {
            halign: 'left'
          },
        },
        rowPageBreak: 'auto',
        showHead: 'firstPage',
        showFoot: 'lastPage',
        startY: 30,
        startX: 'left'
      });
      doc.autoTable({
        html: '#ppps',
        theme: 'plain',
        styles: {
          fontSize: 6,
          lineColor: [116, 119, 122],
          lineWidth: 0.1,
          cellWidth: 'auto',

        },
        margin: (pageWidth / 2) + 4.5,
        tableWidth: (pageWidth / 2) - 9,
        headStyles: {
          valign: 'middle',
          halign: 'center',
        },
        columnStyles: {
          0: {
            halign: 'left'
          },
          1: {
            halign: 'left'
          },
        },
        rowPageBreak: 'auto',
        showHead: 'firstPage',
        showFoot: 'lastPage',
        startY: 30,
      });
      doc.autoTable({
        html: '#ppps2-hidden',
        theme: 'plain',
        styles: {
          fontSize: 5.5,
          lineColor: [116, 119, 122],
          lineWidth: 0.1,
          cellWidth: 'auto',

        },
        margin: 4.5,
        tableWidth: (pageWidth * 80 / 100) - 9,
        headStyles: {
          valign: 'middle',
          halign: 'center',
        },
        columnStyles: {
          0: {
            halign: 'left'
          },
          1: {
            halign: 'left',
            cellWidth: 20
          },
          2: {
            cellWidth: 30
          },
        },
        rowPageBreak: 'auto',
        showHead: 'firstPage',
        showFoot: 'lastPage',
        startY: 96,
        didDrawCell: function(data) {
          if ((data.column.index == 2) && (data.cell.section == 'body')) {
            var td = data.cell.raw;
            var img = td.getElementsByTagName('img')[0];
            var dim = data.cell.height;
            var textpos = data.cell.textPos;
            if (img != null)
              doc.addImage(img.src, textpos.x - 0.5, textpos.y - 0.5, data.cell.width - 2, data.cell.height - 2);
          }
        }
      });

      // doc.line(4, 30, 500, 30);

      doc.setDrawColor(116, 119, 122);
      doc.setLineWidth(0.5);
      var imgRectWidth = 20;
      var heightRectArea = 68;
      var widthRectArea = (pageWidth * 20 / 100);
      var startingXPointCuttingArea = (pageWidth * 80 / 100) - 4.5;
      var startingYPointCuttingArea = 96;
      var titleXPointCuttingArea = startingXPointCuttingArea + (pageWidth * 10 / 100);

      var startingXPointBoardingLayoutArea = startingXPointCuttingArea;
      var startingYPointBoardingLayoutArea = startingYPointCuttingArea + heightRectArea;


      var startingXPointRemarkArea = startingXPointBoardingLayoutArea;
      var startingYPointRemarkArea = startingYPointBoardingLayoutArea + heightRectArea;
      //Cutting Map Area
      doc.rect(startingXPointCuttingArea, startingYPointCuttingArea, widthRectArea, heightRectArea);
      doc.setFontSize(11);
      doc.text($('#title-cutting-map').html(), startingXPointCuttingArea + widthRectArea / 2, startingYPointCuttingArea + 6, 'center');
      doc.setFontSize(6);
      doc.text($('#lv1-cutting-map').html(), startingXPointCuttingArea + widthRectArea / 20, startingYPointCuttingArea + 11, 'left');
      doc.setFontSize(6);
      doc.text($('#lv2-cutting-map').html(), startingXPointCuttingArea + widthRectArea / 20, startingYPointCuttingArea + 16, 'left');
      doc.setFontSize(6);
      doc.text($('#lv3-cutting-map').html(), startingXPointCuttingArea + widthRectArea / 20, startingYPointCuttingArea + 21, 'left');
      doc.addImage(imgDataCuttingMap, 'JPEG', startingXPointCuttingArea + 1, startingYPointCuttingArea + 22, widthRectArea - 1, widthRectArea - 1);

      //Board Layout Area
      doc.rect(startingXPointBoardingLayoutArea, startingYPointBoardingLayoutArea, widthRectArea, heightRectArea);

      doc.setFontSize(11);
      doc.text($('#title-boarding-layout').html(), startingXPointCuttingArea + widthRectArea / 2, startingYPointBoardingLayoutArea + 6, 'center');
      doc.setFontSize(6);
      doc.text($('#lv1-boarding-layout').html(), startingXPointCuttingArea + widthRectArea / 20, startingYPointBoardingLayoutArea + 11, 'left');
      doc.setFontSize(6);
      doc.text($('#lv2-boarding-layout').html(), startingXPointCuttingArea + widthRectArea / 20, startingYPointBoardingLayoutArea + 16, 'left');
      if (dataImageBoardLayout != null)
        doc.addImage(dataImageBoardLayout, 'JPEG', startingXPointCuttingArea + 1, startingYPointBoardingLayoutArea + 17, widthRectArea - 1, widthRectArea - 1);


      //Remarks
      doc.rect(startingXPointRemarkArea, startingYPointRemarkArea, widthRectArea, widthRectArea);

      doc.setFontSize(11);
      doc.text($('#title-remarks').html(), startingXPointRemarkArea + widthRectArea / 2, startingYPointRemarkArea + 6, 'center');

      //Tempat TTD
      // doc.rect(4.5,250,(pageWidth * 80 / 100) - 9,25);

      doc.save('INVOICE <?php echo $detail['date_issue']; ?>.pdf');
    });
  });

  function listsp() {

    window.location.href = "<?php echo base_url('wip/prosess/' . $ido); ?>";
    //alert('sss');

    // $('#body_tbl').html('');

    // var datapost={
    // "ido"   : ido
    // };

  }

  function edit_flow(id) {
    $('#panel-modal').removeData('bs.modal');
    $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    $('#panel-modal  .panel-body').load('<?php echo base_url('wip/edit_flow/'); ?>' + "/" + id);
    $('#panel-modal  .panel-title').html('<i class="fa fa-pencil"></i> Proses Flow');
    $('#panel-modal').modal({
      backdrop: 'static',
      keyboard: false
    }, 'show');
  }

  function edit_schedule(name, id, id_produk) {
    $('#panel-modalchild').removeData('bs.modal');
    $('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    $('#panel-modalchild  .panel-body').load('<?php echo base_url('schedule_production/edit_schedule/'); ?>' + "/" + id + "/" + id_produk);
    $('#panel-modalchild  .panel-title').html('<i class="fa fa-pencil"></i> Edit ' + name);
    $('#panel-modalchild').modal({
      backdrop: 'static',
      keyboard: false
    }, 'show');
  }

  function toDataURL(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function() {
      var reader = new FileReader();
      reader.onloadend = function() {
        callback(reader.result);
      }
      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
  }
</script>