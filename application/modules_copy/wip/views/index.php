<style>
  .filter_separator{padding:0px 10px;}
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Work in Progress</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">

                    <table id="listsp" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>PO</th>
                          <th>Part Number</th>
                          <th>Customer</th> 
                          <th>Order Qty</th>
                          <th>Date Issue</th>
                          <th>Type</th>
                          <th>Run Down</th>
                          <th>Qty Proses</th>
                          <!-- <th>Qty Success</th>-->
                          <th>Qty Reject</th> 
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <th>No</th>
                          <th>PO</th>
						    <th>Part Number</th>
                          <th>Customer</th>
                          <th>Order Qty</th>
                          <th>Date Issue</th>
						    <th>Type</th>
                          <th>Run Down</th>
						   <th>Qty Proses</th>
						    <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </tbody>
                    </table>

            </div>
        </div><!-- end col -->
    </div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content p-0 b-0">
      <div class="panel panel-color panel-primary panel-filled">
        <div class="panel-heading">
          <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 class="panel-title"></h3>
        </div>
        <div class="panel-body">
          <p></p>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <p></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
	$(document).ready(function(){
      listsp();
  });

  function listsp(){
      var cust_id = (typeof $('.select_customer').val() !== "undefined")?$('.select_customer').val():'0';

      $("#listsp").dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo base_url().'wip/lists?cust_id="+cust_id+"';?>",
            "searchDelay": 700,
            "responsive": true,
            "lengthChange": false,
            "destroy": true,
            "info": false,
            "bSort": false,
            "dom": 'l<"toolbar">frtip',
            "initComplete": function(result){
                var element = '<div class="btn-group pull-left">';
                    element += '  <label>Filter</label>';
                    element += '  <label class="filter_separator">|</label>';
                    element += '  <label>Customer:';
                    element += '    <select class="form-control select_customer" onChange="listsp()" style="height:30px;width: 340px;">';
                    element += '      <option value="0">-- Semua Customer --</option>';
                    $.each( result.json.customer, function( key, val ) {
                      var selectcust = (val.id===cust_id)?'selected':'';
                            element += '    <option value="'+val.id+'" '+selectcust+'>'+val.name_eksternal+'</option>';
                        });
                    element += '    </select>';
                    element += '  </label>';
                        
               $("#listsp_filter").prepend(element);
            }
      });
  }
  
  function edit_sp(id){
    $('#panel-modal').removeData('bs.modal');
    $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    $('#panel-modal  .panel-body').load('<?php echo base_url('schedule_production/edit/');?>'+"/"+id);
    $('#panel-modal  .panel-title').html('<i class="fa fa-pencil"></i> Proses Schedule');
    $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
  }

  function edit_schedule(name,id,id_produk){
      $('#panel-modalchild').removeData('bs.modal');
      $('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modalchild  .panel-body').load('<?php echo base_url('schedule_production/edit_schedule/');?>'+"/"+id+"/"+id_produk);
      $('#panel-modalchild  .panel-title').html('<i class="fa fa-pencil"></i> Edit '+name);
      $('#panel-modalchild').modal({backdrop:'static',keyboard:false},'show');
  }
  
  function app_sch(id){
			swal({
			title: 'Yakin akan Melanjutkan ke Proses Produksi ?',
			text: 'data tidak dapat dirubah bila sudah disetujui !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost={
				"id" : id
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>wip/app_sch",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
				   $('.panel-heading button').trigger('click');
                    listsp();
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                    })
				}
			});
		})
	}	  
	
	
  function done_sch(id){
			swal({
			title: 'Yakin akan Menyelesaikan Proses Produksi ?',
			text: 'data tidak dapat dirubah bila sudah disetujui !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost={
				"id" : id
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>wip/done_sch",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
				   $('.panel-heading button').trigger('click');
                    listsp();
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                    })
				}
			});
		})
	}		
  
</script>