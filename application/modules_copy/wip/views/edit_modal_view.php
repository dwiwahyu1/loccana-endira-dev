<form class="form-horizontal form-label-left" id="edit_schedule_production" role="form" action="<?php echo base_url('schedule_production/edit_schedule_production');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p style="text-align:center;">Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	
	<div class="item form-group">
    	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">PO No
    		<span class="required">
    			<sup>*</sup>
    		</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
           	<input data-parsley-maxlength="255" type="text" id="id" name="id" class="form-control col-md-7 col-xs-12" placeholder="PO No"  value="<?php if(isset($detail[0]['order_no'])){ echo $detail[0]['order_no']; }?>" readonly>
        </div>
    </div>

	<div class="item form-group">
		<table id="listedititem" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>No</th>
					<th>Items</th>
					<th>Qty</th>
					<th>Jumlah Run Down Qty(array)</th>
					<th>Option</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$i=1; 
				foreach($detail as $orderkey) {?>
					<tr>
						<td style="padding-top: 17px;"><?php echo $i; ?></td>
	                  	<td>
	                  		<?php if(isset($orderkey['stock_name'])){ echo $orderkey['stock_name']; }?>
	                  	</td>
		                <td id="order_qty<?php echo $orderkey['id_produk'];?>">
		                	<?php if(isset($orderkey['order_qty'])){ echo $orderkey['order_qty']; }?>
		                </td>
		                <td id="produk<?php echo $orderkey['id_produk'];?>">
		                	<?php if(isset($orderkey['jumlah_run_down_qty'])){ echo number_format($orderkey['jumlah_run_down_qty'],2,",","."); }?>
		                </td>
		                <td>
		                	<div class="btn-group">
		                		<button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit Schedule" onClick="edit_schedule('<?php echo $orderkey['stock_name'];?>','<?php echo $id;?>','<?php echo $orderkey['id_produk'];?>')">
		                			<i class="fa fa-pencil"></i>
		                		</button>
		                	</div>
		                </td>
		            </tr>
	            <?php $i++;} ?>
			</tbody>
		</table>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Schedule</button>
		</div>
	</div>

	<input type="hidden" id="id_po_quot" value="<?php if(isset($id)){ echo $id; }?>">
</form>
<!-- /page content -->

<script type="text/javascript">
	var schedulList = [],
		schedulDeletedList = [];
	 
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
	});

	$('#edit_schedule_production').on('submit',(function(e) {
		var schedulData = {"schedulList":schedulList,"schedulDeletedList":schedulDeletedList};

		$('#btn-submit').attr('disabled','disabled');
	    $('#btn-submit').text("Mengubah data...");
	    e.preventDefault();

		$.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:JSON.stringify(schedulData),
            cache:false,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response.success == true) {
                	$('.panel-heading button').trigger('click');
                	listsp();
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                    })
                } else{
                    $('#btn-submit').removeAttr('disabled');
                    $('#btn-submit').text("Edit Schedule");
                    swal("Failed!", response.message, "error");
                }
            }
        }).fail(function(xhr, status, message) {
            $('#btn-submit').removeAttr('disabled');
            $('#btn-submit').text("Edit Schedule");
            swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
        });
	}));
</script>
