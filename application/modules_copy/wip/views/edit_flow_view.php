<form class="form-horizontal form-label-left" id="edit_prosess_flow" role="form" action="<?php echo base_url('wip/edit_prosess_flow');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p style="text-align:center;">Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	
	<div class="item form-group">
    	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Process Flow
    		<span class="required">
    			<sup>*</sup>
    		</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
           	<input data-parsley-maxlength="255" type="text" id="flow" name="flow" class="form-control col-md-7 col-xs-12" placeholder="Process Flow"  value="<?php if(isset($detail['name'])){ echo $detail['name']; }?>" readonly>
        </div>
    </div>
	
	<div class="item form-group">
    	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Qty In 
    		<span class="required">
    			<sup>*</sup>
    		</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
           	<input data-parsley-maxlength="255" type="text" id="in" name="in" class="form-control col-md-7 col-xs-12" placeholder="PO No"  value="<?php if(isset($detail['qty_in'])){ echo $detail['qty_in']; }?>" autocomplete="off" >
        </div>
    </div>
	
	<?php foreach($detail_full as $def){ ?>
		
		<div class="item form-group">
    	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama"><?php echo $def['reject_name'] ?>
    		<span class="required">
    			
    		</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
           	<input data-parsley-maxlength="255" type="text" id="rej_<?php echo $def['id_flow_rej'] ?>" name="rej_<?php echo $def['id_flow_rej'] ?>" class="form-control col-md-7 col-xs-12" placeholder="PO No"  value="<?php if(isset($def['value'])){ echo $def['value']; }?>" onKeyup="out_set()" autocomplete="off" >
        </div>
    </div>
		
	<?php } ?>
	
	<div class="item form-group">
    	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Qty Out 
    		<span class="required">
    			<sup>*</sup>
    		</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
           	<input data-parsley-maxlength="255" type="text" id="out" name="out" class="form-control col-md-7 col-xs-12" placeholder="Qty Out"  value="<?php if(isset($detail['qty_out'])){ echo $detail['qty_out']; }?>"  autocomplete="off" readonly >
        </div>
    </div>
	
	<div class="item form-group">
    	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Note
    		<span class="required">
    			<sup>*</sup>
    		</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
           	<textarea data-parsley-maxlength="255" type="text" id="remark" name="remark" class="form-control col-md-7 col-xs-12" placeholder="Note"  value="<?php if(isset($detail['note'])){ echo $detail['note']; }?>" ></textarea>
        </div>
    </div>

	<div class="item form-group">
		
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Save Flow</button>
		</div>
	</div>

	<input type="hidden" id="id_po_quot" value="<?php if(isset($id)){ echo $id; }?>">
</form>
<!-- /page content -->

<script type="text/javascript">
	var schedulList = [],
		schedulDeletedList = [];
	 
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
	});

	function out_set(){
		
		var outs = 0;
		
		<?php foreach($detail_full as $def){ ?>
		
			outs = outs + parseInt($('#rej_<?php echo $def['id_flow_rej'] ?>').val());
		
		<?php } ?>
		//alert('sss');
		
		$('#out').val(parseInt($('#in').val()) - outs);
		
	}

	$('#edit_prosess_flow').on('submit',(function(e) {
		
		var out = $('#out').val();
		var ins = $('#in').val();
		
		var reject_val = 0;
		
		<?php foreach($detail_full as $def){ ?>
		
			reject_val = reject_val + parseInt($('#rej_<?php echo $def['id_flow_rej'] ?>').val());
			var rej_<?php echo $def['id_flow_rej'] ?> = $('#rej_<?php echo $def['id_flow_rej'] ?>').val();
		
		<?php } ?>
		
		
		var note = $('#remark').val();
		
		var reject = {
			
			<?php foreach($detail_full as $def){ ?>
					"rej_<?php echo $def['id_flow_rej'] ?>":rej_<?php echo $def['id_flow_rej'] ?>,
			<?php } ?>
			
		}
		
		var ir = 0;
		var reject_id = {
			
			<?php foreach($detail_full as $def){ ?>
					"rej_<?php echo $def['id_flow_rej'] ?>":<?php echo $def['id_flow_rej'] ?>,
			<?php } ?>
			
		}
		
		var out_val = ins-reject_val;
		
		//alert(out_val);
		 
		var schedulData = {
					"note":note,
					"out":out_val,
					"in":ins,
					"reject":reject,
					"reject_id":reject_id,
					"id":<?php echo $id; ?>  
			};

		$('#btn-submit').attr('disabled','disabled');
	    $('#btn-submit').text("Mengubah data...");
	    e.preventDefault();

		$.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:JSON.stringify(schedulData),
            cache:false,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response.success == true) {
                	$('.panel-heading button').trigger('click');
                	
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
						listsp();
                    })
                } else{
                    $('#btn-submit').removeAttr('disabled');
                    $('#btn-submit').text("Edit Schedule");
                    swal("Failed!", response.message, "error");
                }
            }
        }).fail(function(xhr, status, message) {
            $('#btn-submit').removeAttr('disabled');
            $('#btn-submit').text("Edit Schedule");
            swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
        });
	}));
</script>
