<style>
	.dt-body-left {text-align: left;}
	.dt-body-center {text-align: center;}
	.dt-body-right {text-align: right;}
	.dt-body-left th {text-align: left;}
	.dt-body-center th {text-align: center;}
	.dt-body-right th {text-align: right;}

	/*#listItemWO {
		counter-reset: rowNumber;
	}

	#listItemWO tbody tr > td:first-child {
		counter-increment: rowNumber;
		text-align: center;
	}

	#listItemWO tbody tr td:first-child::before {
		content: counter(rowNumber);
		min-width: 1em;
		margin-right: 0.5em;
	}*/
</style>
<form class="form-horizontal form-label-left" id="edit_btb" role="form" action="<?php echo base_url('btb_work_order/edit_btb');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nomor_btb">Nomor BTB</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input class="form-control" type="text" id="nomor_btb" name="nomor_btb" value="<?php
				if(isset($btbwo[0]['no_btbwo'])) echo $btbwo[0]['no_btbwo'];
			?>" placeholder="Nomor BTB" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_btb">Type BTB <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="type_btb" id="type_btb" style="width: 100%" required>
				<option value="" selected>Select Type BTB</option>
				<option value="lokal" <?php if($btbwo[0]['type_btbwo'] == 'lokal') echo "selected"; ?>>Lokal</option>
				<option value="import" <?php if($btbwo[0]['type_btbwo'] == 'import') echo "selected"; ?>>Import</option>
				<option value="antar_kb" <?php if($btbwo[0]['type_btbwo'] == 'antar_kb') echo "selected"; ?>>Antar KB</option>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_btb">Tanggal BTB <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<div class="input-group date">
				<input type="text" class="form-control" id="tanggal_btb" name="tanggal_btb" placeholder="<?php
					echo date('d-M-Y');
				?>" value="<?php
					if(isset($btbwo[0]['tanggal_btbwo'])) echo date('d-M-Y', strtotime($btbwo[0]['tanggal_btbwo']));
				?>" autocomplete="off" required>
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Work Order : </label>
		<div class="col-md-8 col-sm-6 col-xs-12 add_item">
			<a class="btn btn-primary" id="btn_add_wo">
				<i class="fa fa-plus"></i> Tambah Work Order
			</a>
		</div>
	</div>

	<div class="item form-group">
		<div>
			<table id="listItemWO" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th class="dt-body-center" style="width: 8%;">WO</th>
						<th class="dt-body-center" style="width: 8%;">WR</th>
						<th class="dt-body-center">Nama Pekerjaan</th>
						<th class="dt-body-center">Nama Supplier</th>
						<th class="dt-body-center" style="width: 5%;">Qty</th>
						<th class="dt-body-center" style="width: 125px;">Qty Yang Sudah Ada</th>
						<th class="dt-body-center" style="width: 8%;">Qty Diterima</th>
						<th class="dt-body-center" style="width: 8%;">Qty Sisa</th>
						<th class="dt-body-center" style="width: 5%;">UOM</th>
						<th class="dt-body-center">Remark</th>
						<td class="dt-body-center" style="width: 5%;">Action</td>
						<td></td>
						<td></td>
					</tr>
				</thead>
				<tbody>
		<?php
			if(isset($d_btbwo) && sizeof($d_btbwo) > 0) {
				$i = 0;
				foreach ($d_btbwo as $dk => $dv) {
					$qtyAda = $dv['qty_awal'] - $dv['qty_akhir']; ?>
					<tr>
						<td class="dt-body-center"><?php echo $dv['no_work_order']; ?></td>
						<td class="dt-body-center"><?php echo $dv['no_pekerjaan']; ?></td>
						<td><?php echo $dv['nama_pekerjaan']; ?></td>
						<td><?php echo $dv['name_eksternal']; ?></td>
						<td class="dt-body-center"><?php echo $dv['qty']; ?></td>
						<td class="dt-body-center"><?php echo number_format($qtyAda, 4); ?></td>
						<td class="dt-body-center">
						<input class="form-control" type="number" id="edit_qty_diterima<?php echo $i; ?>";" name="edit_qty_diterima[]" step=".0001" value="<?php
							echo $dv['qty_diterima'];
						?>">
						</td>
						<td class="dt-body-center"><div id="div_qty_akhir<?php echo $i; ?>"><?php echo $dv['qty_akhir']; ?></div></td>
						<td class="dt-body-center"><?php echo $dv['uom_name']; ?></td>
						<td><?php echo $dv['deskripsi']; ?></td>
						<td>
							<a class="btn btn-danger btn-sm" id="btn_del<?php echo $i; ?>">
								<i class="fa fa-trash"></i>
							</a>
							<input type="hidden" id="edit_id_btbwo_dpp<?php echo $i; ?>" name="edit_id_btbwo_dpp[]" value="<?php echo $dv['id_btbwo_dpp']; ?>">
							<input type="hidden" id="edit_id_wo<?php echo $i; ?>" name="edit_id_wo[]" value="<?php echo $dv['id_wo']; ?>">
							<input type="hidden" id="edit_id_dpp<?php echo $i; ?>" name="edit_id_dpp[]" value="<?php echo $dv['id_dpp']; ?>">
							<input type="hidden" id="edit_qty_awal<?php echo $i; ?>" name="edit_qty_awal[]" value="<?php
								echo $dv['qty_awal'];
							?>">
							<input type="hidden" id="edit_qty_akhir<?php echo $i; ?>" name="edit_qty_akhir[]" value="<?php
								echo $dv['qty_akhir'];
							?>">
						</td>
						<td><?php echo $dv['id_wo']; ?></td>
						<td><?php echo $dv['id_dpp']; ?></td>
					</tr>
		<?php
					$i++;
				}
			}
		?>
				</tbody>
			</table>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Edit BTB</button>
			<input type="hidden" id="id_btbwo" name="id_btbwo" value="<?php
				if($btbwo[0]['id_btbwo']) echo $btbwo[0]['id_btbwo'];
			?>">
		</div>
	</div>
</form>
	
<script type="text/javascript">
	var arrBtbWO_dpp	= [];
	var arrWO			= [];
	var arrDPP			= [];
	var arrQtyAwal		= [];
	var arrQtyTerima	= [];
	var arrQtyAkhir 	= [];
	var lastItem		= "<?php echo $i; ?>";
	var dt_itemWO;

	$(document).ready(function() {
		$('#tanggal_btb').datepicker({
			format: "dd-M-yyyy",
			autoclose: true,
			todayHighlight: true,
			changeYear: true,
			minDate: '-3M',
			maxDate: '+30D',
		});
		
		$("#type_btb").select2();

		dtPO();

		$('input[name="edit_qty_diterima[]"]').on('keyup', function() {
			var idRow = this.id.replace(/\D/g,'');
			var qty 	= parseFloat($('#edit_qty_awal'+idRow).val());
			if(this.value != '' && this.value != null && this.value != 'null') {
				var qtydtr 	= parseFloat(this.value);
				var qtysis 	= (qty - qtydtr);
				$('#edit_qty_akhir'+idRow).val(qtysis);
				$('#div_qty_akhir'+idRow).html(qtysis);
			}else {
				$('#edit_qty_akhir'+idRow).val(0);
				$('#div_qty_akhir'+idRow).html(0);
			}
		})
	});

	function dtPO() {
		dt_itemWO = $('#listItemWO').DataTable( {
			"processing": true,
			"searching": false,
			"paging": false,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0, 1, 6, 7],
				"className": 'dt-body-center',
				"width": "55px"
			}, {
				"targets": [5],
				"className": 'dt-body-center',
				"width": "125px"
			}, {
				"targets": [4, 10],
				"className": 'dt-body-center',
				"width": "10px"
			}, {
				"targets": [8],
				"className": 'dt-body-center',
				"width": "15px"
			}, {
				"targets": [11, 12],
				"visible": false
			}]
		});
	}

	function btn_delItem(val) {
		dt_itemWO.row($(val).parents('tr')).remove().draw(false);
	}

	$('#listItemWO').on("click", "a", function() {
		var dataWO = dt_itemWO.row($(this).parents('tr')).data();
		var rowParent = $(this).parents('tr');
		var idRow = this.id.replace(/\D/g,'');

		if(dataWO[12] != "") {
			swal({
				title: 'Yakin akan Menghapus Barang yang sudah ada ?',
				text: 'data tidak dapat dikembalikan bila sudah dihapus !',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak'
			}).then(function () {
				var datapost = {
					"id_btbwo_dpp" : parseInt($('#edit_id_btbwo_dpp'+idRow).val())
				};

				$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>btb_work_order/delete_btbwo_dpp",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
						if (response.success == true) {
							dt_itemWO.row(rowParent).remove().draw(false);
							swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () { })
						}else swal("Failed!", response.message, "error");
					}
				});
			});
		}
	})
	
	$('#btn_add_wo').on('click', function() {
		$('#panel-modalchild').removeData('bs.modal');
		$('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modalchild  .panel-body').load('<?php echo base_url('btb_work_order/edit_add_item');?>');
		$('#panel-modalchild  .panel-title').html('<i class="fa fa-plus"></i> Tambah Work Order');
		$('#panel-modalchild').modal({backdrop:'static',keyboard:false},'show');
	})

	$('#edit_btb').on('submit',(function(e) {
		arrBtbWO_dpp	= [];
		arrWO 			= [];
		arrDPP 			= [];
		arrQtyAwal		= [];
		arrQtyTerima	= [];
		arrQtyAkhir 	= [];

		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();

		/*
		
		<input type="hidden" id="id_btbwo" name="id_btbwo" value="<?php if($btbwo[0]['id_btbwo']) echo $btbwo[0]['id_btbwo']; ?>">
		*/

		$('input[name="edit_id_btbwo_dpp[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arrBtbWO_dpp.push(this.value);
			}
		})

		$('input[name="edit_id_wo[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arrWO.push(this.value);
			}
		})

		$('input[name="edit_id_dpp[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arrDPP.push(this.value);
			}
		})

		$('input[name="edit_qty_awal[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arrQtyAwal.push(this.value);
			}
		})

		$('input[name="edit_qty_diterima[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arrQtyTerima.push(this.value);
			}
		})

		$('input[name="edit_qty_akhir[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arrQtyAkhir.push(this.value);
			}
		})

		if(arrBtbWO_dpp.length > 0 && arrWO.length > 0 && arrDPP.length > 0 && arrQtyAwal.length > 0 && arrQtyTerima.length > 0 && arrQtyAkhir.length > 0) {
			var formData = new FormData();
			formData.set('nomor_btb',		$('#nomor_btb').val());
			formData.set('type_btb',		$('#type_btb').val());
			formData.set('tanggal_btb',		$('#tanggal_btb').val());
			formData.set('id_btbwo',		$('#id_btbwo').val());
			formData.set('arrBtbWO_dpp',	arrBtbWO_dpp);
			formData.set('arrWO',			arrWO);
			formData.set('arrDPP',			arrDPP);
			formData.set('arrQtyAwal',		arrQtyAwal);
			formData.set('arrQtyTerima',	arrQtyTerima);
			formData.set('arrQtyAkhir',		arrQtyAkhir);

			$.ajax({
				type:'POST',
				url: $(this).attr('action'),
				data: formData,
				cache:false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function () {
							$('#panel-modal').modal('toggle');
							listbtb();
						})
					}else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Edit BTB");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Edit BTB");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}else {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Edit BTB");
			swal("Failed!", 'List Work Order belum ada!', "error");
		}
	}))
</script>