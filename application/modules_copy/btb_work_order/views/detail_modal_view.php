<style>
	.dt-body-left {text-align: left;}
	.dt-body-center {text-align: center;}
	.dt-body-right {text-align: right;}
	.dt-body-left th {text-align: left;}
	.dt-body-center th {text-align: center;}
	.dt-body-right th {text-align: right;}

	#listItemWO {
		counter-reset: rowNumber;
	}

	#listItemWO tbody tr > td:first-child {
		counter-increment: rowNumber;
		text-align: center;
	}

	#listItemWO tbody tr td:first-child::before {
		content: counter(rowNumber);
		min-width: 1em;
		margin-right: 0.5em;
	}
</style>
<form class="form-horizontal form-label-left" id="add_btb" role="form" action="<?php echo base_url('btb_work_order/add_btb');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_btb">Type BTB <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text-align" class="form-control" id="type_btb" name="type_btb" value="<?php
				if(isset($btbwo[0]['type_btbwo'])) echo ucwords($btbwo[0]['type_btbwo']);
			?>" disabled>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_btb">Tanggal BTB <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="tanggal_btb" name="tanggal_btb" value="<?php
				if(isset($btbwo[0]['tanggal_btbwo'])) echo date('d-M-Y', strtotime($btbwo[0]['tanggal_btbwo']));
			?>" disabled>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Work Order : </label>
		<div class="col-md-8 col-sm-6 col-xs-12 add_item"></div>
	</div>
	
	<div class="item form-group">
		<div>
			<table id="listItemWO" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th class="dt-body-center">No</th>
						<th class="dt-body-center">WO</th>
						<th class="dt-body-center">WR</th>
						<th class="dt-body-center">Nama Pekerjaan</th>
						<th class="dt-body-center">Nama Supplier</th>
						<th class="dt-body-center">Qty</th>
						<th class="dt-body-center">Qty Diterima</th>
						<th class="dt-body-center">Qty Sisa</th>
						<th class="dt-body-center">UOM</th>
						<th class="dt-body-center">Remark</th>
					</tr>
				</thead>
				<tbody>
		<?php
			if(isset($d_btbwo)) {
				if(sizeof($d_btbwo) > 0) {
					foreach ($d_btbwo as $dk => $dv) { ?>
						<tr>
							<td></td>
							<td class="dt-body-center"><?php echo $dv['no_work_order']; ?></td>
							<td class="dt-body-center"><?php echo $dv['no_pekerjaan']; ?></td>
							<td><?php echo $dv['nama_pekerjaan']; ?></td>
							<td><?php echo $dv['name_eksternal']; ?></td>
							<td class="dt-body-center"><?php echo $dv['qty_awal']; ?></td>
							<td class="dt-body-center"><?php echo $dv['qty_diterima']; ?></td>
							<td class="dt-body-center"><?php echo $dv['qty_akhir']; ?></td>
							<td class="dt-body-center"><?php echo $dv['uom_name']; ?></td>
							<td><?php echo $dv['deskripsi']; ?></td>
						</tr>
		<?php
					}
				}
			}
		?>
				</tbody>
			</table>
		</div>
	</div>
</form>
	
<script type="text/javascript">
	var dt_itemWO;

	$(document).ready(function() {
		dtPO();
	});

	function dtPO() {
		dt_itemWO = $('#listItemWO').DataTable( {
			"processing": true,
			"searching": false,
			"paging": false,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0],
				"className": "dt-body-right",
				"width": "10px"
			}, {
				"targets": [1],
				"className": "dt-body-right",
				"width": "45px"
			}, {
				"targets": [2],
				"className": "dt-body-right",
				"width": "45px"
			}, {
				"targets": [5, 6],
				"className": 'dt-body-center'
			}, {
				"targets": [7, 8],
				"className": 'dt-body-right'
			}]
		});
	}
</script>