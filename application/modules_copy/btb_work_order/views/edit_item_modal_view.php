<style type="text/css">
	.form-item{margin-top: 15px;overflow: auto;}
	#listWo {
		counter-reset: rowNumber;
	}

	#listWo tbody tr > td:first-child {
		counter-increment: rowNumber;
		text-align: center;
	}

	#listWo tbody tr td:first-child::before {
		content: counter(rowNumber);
		min-width: 1em;
		margin-right: 0.5em;
	}
</style>

<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
<br>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_wo">Nomor Work Order<span class="required"><sup>*</sup></span></label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<select class="form-control" name="no_wo" id="no_wo" style="width: 100%" required>
			<option value="" selected>-- Pilih Nomor Work Order --</option>
		<!-- <?php foreach($workOrder as $wod => $wov) { ?>
			<option value="<?php echo $wov['id_wo']; ?>"><?php echo $wov['no_work_order']; ?></option>
		<?php } ?> -->
		</select>
	</div>
</div>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_supplier">Nama Supplier</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<input type="text" class="form-control" id="nama_supplier" name="nama_supplier" placeholder="Nama Supplier" autocomplete="off" readonly>
	</div>
</div>

<div class="item form-group form-item">
	<table id="listWo" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class="dt-body-center" style="width: 5%;">No</th>
				<th>No Pekerjaan</th>
				<th>Nama Pekerjaan</th>
				<th class="dt-body-center">Qty</th>
				<th class="dt-body-center" style="width: 15%;">Qty Yang Sudah Ada</th>
				<th style="text-align: center; width: 10%;">Qty Diterima</th>
				<th style="text-align: center; width: 10%;">Qty Sisa</th>
				<th class="dt-body-center">UOM</th>
				<th>Deskripsi</th>
				<th style="width: 5%;">Pilih</th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>
</div>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<a class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" id="btn_simpan_item">Simpan</a>
	</div>
</div>
	
<script type="text/javascript">
	$(document).ready(function() {
		$("#no_wo").select2();

		getListWO();
	});

	$('#no_wo').on('change', function() {
		if(this.value != '') listDtWo(this.value);
		else listDtWo(0);
	})

	function getListWO() {
		$('#no_wo').attr('disabled', 'disabled')
		var dtLength 	= dt_itemWO.rows().data().length;
		var arrTemp 	= [];
		if(dtLength > 0) {
			for (var i = 0; i < dtLength; i++) {
				var dtData = dt_itemWO.row(i).data();
				arrTemp.push(dtData[0]);
			}
		}

		var datapost = {
			'listWorkOrder' : arrTemp
		};
			
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('btb_work_order/get_workOrder');?>",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					for (var i = 0; i < r.length; i++) {
						var newOption = new Option(r[i].no_work_order, r[i].id_wo, false, false);
						$('#no_wo').append(newOption);
					}
					$('#no_wo').removeAttr('disabled');
				}else $('#no_wo').removeAttr('disabled');
			}
		});
	}
	
	function listDtWo(id_wo) {
		var dtLength 	= dt_itemWO.rows().data().length;
		var arrTemp 	= [];
		if(dtLength > 0) {
			for (var i = 0; i < dtLength; i++) {
				var dtData = dt_itemWO.row(i).data();
				arrTemp.push(dtData[12]);
			}
		}

		var datapost = {
			'id_wo'		: id_wo,
			'listExist' : arrTemp
		};

		$.ajax({
			type: "POST",
			url: "<?php echo base_url('btb_work_order/get_workOrder_id');?>",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					$('#listWo tbody').empty();
					var strAppend		= '';
					var strRemark		= '';
					for (var i = 0; i < r.length; i++) {
						if(parseFloat(r[i].qty_akhir) > 0 || r[i].qty_akhir == null || r[i].qty_akhir == 'null') {
							var strQtyAwal		= r[i].qty;
							if(r[i].qty_awal != null && r[i].qty_awal != 'null') var strQtyAda = r[i].qty_awal;
							else var strQtyAda = 0;

							if(r[i].remark != null && r[i].remark != 'null') strRemark = r[i].remark;
							if(r[i].qty_akhir != null && r[i].qty_akhir != 'null') strQtyAwal = r[i].qty_akhir;
							strAppend =
								'<tr>'+
									'<td class="dt-body-center"></td>'+
									'<td>'+r[i].no_pekerjaan+'</td>'+
									'<td>'+r[i].nama_pekerjaan+'</td>'+
									'<td class="dt-body-center">'+r[i].qty+'</td>'+
									'<td class="dt-body-center">'+strQtyAda+'</td>'+
									'<td>'+
										'<input type="hidden" id="qty_awal'+lastItem+'" name="qty_awal[]" value="'+strQtyAwal+'">'+
										'<input class="form-control" type="number" id="qty_diterima'+lastItem+'" name="qty_diterima[]" step=".0001" value="0">'+
										'<input type="hidden" id="qty_akhir'+lastItem+'" name="qty_akhir[]" value="0">'+
									'</td>'+
									'<td class="dt-body-center">'+
										'<div id="div_qty_akhir'+lastItem+'">0</div>'+
									'</td>'+
									'<td class="dt-body-center">'+r[i].uom_name+'</td>'+
									'<td>'+
										strRemark+
										'<input type="hidden" id="checkNoDpp'+lastItem+'" name="checkNoDpp[]" value="'+r[i].id_dpp+'">'+
									'</td>'+
									'<td>'+
										'<div class="checkbox">'+
											'<input id="checkDpp'+lastItem+'" name="checkDpp[]" type="checkbox" value="'+r[i].id_dpp+'">'+
											'<label for="checkDpp'+lastItem+'"></label>'+
										'</div>'+
									'</td>'+
								'</tr>';

							$('#listWo tbody').append(strAppend);

							$('#qty_diterima'+lastItem).on('keyup', function() {
								var idRow 	= this.id.replace(/\D/g,'');
								var qty 	= parseFloat($('#qty_awal'+idRow).val());
								if(this.value != '' && this.value != null && this.value != 'null') {
									var qtydtr 	= parseFloat(this.value);
									var qtysis 	= (qty - qtydtr);
									$('#qty_akhir'+idRow).val(qtysis);
									$('#div_qty_akhir'+idRow).html(qtysis);
								}else {
									$('#qty_akhir'+idRow).val(0);
									$('#div_qty_akhir'+idRow).html(0);
								}
							})
						}
					}
					$('#nama_supplier').val(r[0].name_eksternal);
				}else {
					$('#nama_supplier').val('');
					$('#listWo tbody').empty();
				}
			}
		});
	}

	$('#btn_simpan_item').on('click', function() {
		$(this).attr('disabled','disabled');
		$(this).text("Memasukkan data...");
		var tempArr 			= [];
		var tempArrQtyAwal		= [];
		var tempArrQtyterima 	= [];
		var tempArrQtyAkhir 	= [];

		$('input[name="checkDpp[]"]').each(function() {
			if(this.checked == true) {
				if(this.value != undefined && this.value != '') tempArr.push(this.value);
				var parentId = this.id.replace(/\D/g,'');
				tempArrQtyAwal.push($('#qty_awal'+parentId).val());
				tempArrQtyterima.push($('#qty_diterima'+parentId).val());
				tempArrQtyAkhir.push($('#qty_akhir'+parentId).val());
			}
		})

		if(tempArr.length > 0) {
			var datapost = {
				"lastItem"			: lastItem,
				"arrNoDpp"			: tempArr,
				'arrQtyAwal'		: tempArrQtyAwal,
				"arrQtyDiterima"	: tempArrQtyterima,
				"arrQtyAkhir"		: tempArrQtyAkhir
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url('btb_work_order/edit_get_workOrder_idDpp');?>",
				data:JSON.stringify(datapost),
				cache:false,
				contentType: false,
				processData: false,
				contentType: 'application/json; charset=utf-8',
				success: function(r) {
					if(r.data.length > 0) {
						lastItem = r.lastItem;
						dt_itemWO.rows.add(r.data).draw();
						$(this).removeAttr('disabled');
						$(this).text("Simpan");
						$('#panel-modalchild').modal('toggle');
					}
				}
			});
		}else {
			$(this).removeAttr('disabled');
			$(this).text("Simpan");
			swal("Failed!", 'List Pekerjaan belum ada yang dipilih!', "error");
		}
	})
</script>