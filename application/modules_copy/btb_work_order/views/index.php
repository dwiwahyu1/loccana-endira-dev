<style>
	.changed_status{cursor:pointer;text-decoration: underline;color:#96b6e8;}
	.changed_status:hover{color:#ff8c00}
	.dt-body-left {text-align:left;}
	.dt-body-right {text-align:right;}
	.dt-body-center {text-align:center; vertical-align: middle;}
	.force-overflow {height: 450px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow {min-height: 350px;}
	#panel-modalchild::-webkit-scrollbar-track {
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	#panel-modal-modalchild::-webkit-scrollbar {
		width: 10px;
		background-color: #F5F5F5;
	}

	#panel-modal-modalchild::-webkit-scrollbar-thumb {
		background-image: -webkit-gradient(linear,
		left bottom,
		left top,
		color-stop(0.44, rgb(122,153,217)),
		color-stop(0.72, rgb(73,125,189)),
		color-stop(0.86, rgb(28,58,148)));
	}

	.x-hidden {
		display: none;
	}
</style>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">BTB Work Order</h4>
        </div>
    </div>
	
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">

				<table id="listbtb" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th class="custom-tables text-center" style="width: 5%;">No</th>
							<th class="custom-tables text-center" style="width: 10%;">No BTB</th>
							<th class="custom-tables text-center">Nama Supplier</th>
							<th class="custom-tables text-center" style="width: 15%;">Tanggal BTB</th>
							<th class="custom-tables text-center" style="width: 15%;">Type BTB</th>
							<th class="custom-tables text-center" style="width: 8%;">Actions</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
            </div>
        </div><!-- end col -->
    </div>
	
	<div class="row x-hidden">
		<div class="col-sm-12">
			<div class="card-box">
				<table id="listbtbwoprint" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th class="custom-tables text-center" style="width: 5%;">No</th>
							<th class="custom-tables text-center">Nama Pekerjaan</th>
							<th class="custom-tables text-center" style="width: 5%;">Qty</th>
							<th class="custom-tables text-center" style="width: 8%;">Harga Satuan</th>
							<th class="custom-tables text-center" style="width: 5%;">PPN / Disc</th>
							<th class="custom-tables text-center" style="width: 10%;">Harga Total</th>
							<th class="custom-tables text-center" style="width: 8%;">No WR</th>
							<th class="custom-tables text-center">Keterangan</th>
						</tr>
					</thead>
					<tbody></tbody>
					<tfoot>
						<tr>
							<th class="custom-tables text-center" colspan="5">TOTAL</th>
							<th class="custom-tables text-right" id="footTotal"></th>
							<td class="custom-tables text-center" colspan="3"></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div><!-- end col -->
	</div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:90%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-modal">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width: 70%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal-report" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width: 700px;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-report-overflow">
					<div class="scroll-report-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal-viewReport" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width: 700px;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-report-overflow">
					<div class="scroll-report-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	var t_btbwo_detail;
	var dataImage = null;
	
	$(document).ready(function(){
		listbtb();
	});

	function btb_add(){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('btb_work_order/add/');?>');
		$('#panel-modal  .panel-title').html('<i class="fa fa-plus"></i> Add BTB');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function btb_report(){
		$('#panel-modal-report').removeData('bs.modal');
		$('#panel-modal-report  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal-report  .panel-body').load('<?php echo base_url('btb_work_order/btb_report/');?>');
		$('#panel-modal-report  .panel-title').html('<i class="fa fa-search"></i> Filter Report BTB');
		$('#panel-modal-report').modal({backdrop:'static',keyboard:false},'show');
	}

	function btb_view_report(){
		$('#panel-modal-view-report').removeData('bs.modal');
		$('#panel-modal-view-report  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal-view-report  .panel-body').load('<?php echo base_url('btb_work_order/btb_view_report/');?>');
		$('#panel-modal-view-report  .panel-title').html('<i class="fa fa-file-pdf-o"></i> View Report BTB');
		$('#panel-modal-view-report').modal({backdrop:'static',keyboard:false},'show');
	}

	function btbwo_detail(id_btbwo){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('btb_work_order/btbwo_detail');?>'+"/"+id_btbwo);
		$('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Detail BTB');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}
	
	function btbwo_edit(id_btbwo){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('btb_work_order/btbwo_edit');?>'+"/"+id_btbwo);
		$('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Edit BTB');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}
	
	function toDataURL(url, callback) {
		var xhr = new XMLHttpRequest();
		xhr.onload = function() {
			var reader = new FileReader();
			reader.onloadend = function() {
				callback(reader.result);
			}
			reader.readAsDataURL(xhr.response);
		};
		xhr.open('GET', url);
		xhr.responseType = 'blob';
		xhr.send();
	}
	
	toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
		dataImage = dataUrl;
	})
	
	function btbwo_detail_download(id_btbwo) {
		var html = '';
		var datapost = {
			'id_btbwo' : id_btbwo
		};
			
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('btb_work_order/btbwo_detail_download');?>",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.success == true) {
					var btbwo = r.data;
					var tgl_btb;
					var total = 0;
					var strTotal = '';
					var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
					$('#listbtbwoprint tbody').html('');
					
					for(var i =0;i < btbwo.length; i++) {
						console.log(btbwo[i]);
						var no_btbwo = btbwo[i]['no_btbwo'];
						var tanggal_btbwo = btbwo[i]['tanggal_btbwo'];
						tgl_btb = new Date(btbwo[i]['tanggal_btbwo']);
						
						var day = tgl_btb.getDate();
						if (day.toString().length == 1) day = "0"+day;

						var type_btbwo = btbwo[i]['type_btbwo'];
						var no_wo = btbwo[i]['no_work_order'];
						var name_supplier = btbwo[i]['name_eksternal'];
						var diskon = btbwo[i]['diskon'];
						var pembayaran = btbwo[i]['term_of_payment'];

						if(parseInt(btbwo[i]['diskon']) <= 100) {
							diskon = diskon+"%";
						}
						var totalRow = (parseFloat(btbwo[i]['qty_diterima']) * parseFloat(btbwo[i]['unit_price']));
						total = total + totalRow;
						if(btbwo[i]['remark'] != null && btbwo[i]['remark'] != 'null') var keterangan = btbwo[i]['remark'];
						else var keterangan = '';

						html +=
							'<tr>'+
								'<td style="text-align: center;">'+(i+1)+'</td>'+
								'<td>'+btbwo[i]['nama_pekerjaan']+'</td>'+
								'<td style="text-align: center;">'+btbwo[i]['qty_diterima']+'</td>'+
								'<td style="text-align: right;">'+btbwo[i]['symbol_valas']+' '+formatCurrencyRupiah(parseFloat(btbwo[i]['unit_price']))+'</td>'+
								'<td style="text-align: center;">'+diskon+'</td>'+
								'<td style="text-align: right;">'+btbwo[i]['symbol_valas']+' '+formatCurrencyRupiah(totalRow)+'</td>'+
								'<td style="text-align: center;">'+btbwo[i]['no_pekerjaan']+'</td>'+
								'<td>'+keterangan+'</td>'+
							'</tr>';
						strTotal = btbwo[i]['symbol_valas']+' '+formatCurrencyRupiah(total);
					}
					
					$('#listbtbwoprint tbody').append(html);
					$('#footTotal').html(strTotal);
					var doc = new jsPDF("l", "mm", "a4");
					var imgData = dataImage;
					var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
					var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
					
					doc.setTextColor(50);
					doc.setFontSize(12);
					doc.setFontStyle('helvetica','arial','sans-serif','bold');

					doc.line(pageWidth - 5, 10, 5, 10);
					doc.text('BUKTI TERIMA PEKERJAAN (SERVICE / MODIFIKASI)', pageWidth / 2, 15, 'center');
					doc.text(type_btbwo.toUpperCase(), pageWidth / 2, 20, 'center');
					doc.line(5, 10, 5, 23);
					doc.line(pageWidth - 5, 10, pageWidth - 5, 23);
					doc.line(pageWidth - 5, 23, 5, 23);

					doc.setTextColor(50);
					doc.setFontSize(11);
					doc.text('Telah di terima barang dengan baik dari(supplier) : '+name_supplier.toUpperCase(), 8, 28, 'left');
					/*doc.text('Melalui (Via) : ', 8, 33, 'left');
					doc.text('No Surat Jalan : ', 8, 39, 'left');*/

					doc.text('No BTB', pageWidth - 48, 28, 'left');
					doc.text(': '+no_btbwo, pageWidth - 34, 28, 'left');
					doc.text('Tanggal', pageWidth - 48, 34, 'left');
					doc.text(': '+day+'-'+tgl_btb.getMonth()+'-'+tgl_btb.getFullYear(), pageWidth - 31, 33, 'left');
					doc.text('NO WO', pageWidth - 48, 40, 'left');
					doc.text(': '+no_wo, pageWidth - 31, 40, 'left');
					doc.line(5, 23, 5, 43);
					doc.line(pageWidth - 5, 23, pageWidth - 5, 43);
					
					doc.autoTable({
						html 			: '#listbtbwoprint',
						headStyles		: {fontSize : 7, valign : 'middle', halign : 'center'},
						bodyStyles		: {fontSize : 7},
						footStyles		: {fontSize : 7},

						columnStyles	: {
							0: {halign: 'center'},
							1: {halign: 'left'},
							2: {halign: 'center'},
							3: {halign: 'right'},
							4: {halign: 'center'},
							5: {halign: 'right'},
							6: {halign: 'center'},
							7: {halign: 'left'}
						},
						theme			: 'plain',
						styles			: {
							fontSize : 6, 
							lineColor: [0, 0, 0],
							lineWidth: 0.15,
							cellWidth : 'auto',
							
						},
						didParseCell	: function (data) {
							if(data.table.foot[0]) {
								if(data.table.foot[0].cells[0]) data.table.foot[0].cells[0].styles.halign = 'center';
								if(data.table.foot[0].cells[5]) data.table.foot[0].cells[5].styles.halign = 'right';
							}
						},
						margin			: 5,
						rowPageBreak	: 'auto',
						showHead		: 'firstPage',
						showFoot		: 'lastPage',
						startY			: 43
					});

					var lastTable = doc.autoTable.previous.finalY;

					doc.setDrawColor(0, 0, 0)
					/*doc.line(8, (lastTable + 5), 8, (lastTable + 10));
					doc.line(8, (lastTable + 5), 16, (lastTable + 5));
					doc.line(8, (lastTable + 10), 16, (lastTable + 10));
					doc.line(16, (lastTable + 5), 16, (lastTable + 10));
					doc.text('CASH', 18, (lastTable + 8), 'left');

					doc.line(8, (lastTable + 15), 8, (lastTable + 20));
					doc.line(8, (lastTable + 15), 16, (lastTable + 15));
					doc.line(8, (lastTable + 20), 16, (lastTable + 20));
					doc.line(16, (lastTable + 15), 16, (lastTable + 20));
					doc.text('CREDIT', 18, (lastTable + 18), 'left');

					doc.text('No. BBK', 8, (lastTable + 31), 'left');
					doc.text(': ----', 30, (lastTable + 31), 'left');

					doc.text('Bank', 8, (lastTable + 36), 'left');
					doc.text(': ----', 30, (lastTable + 36), 'left');

					doc.text('No. CH/GB', 8, (lastTable + 41), 'left');
					doc.text(': ----', 30, (lastTable + 41), 'left');

					doc.text('Jml', 8, (lastTable + 46), 'left');
					doc.text(': ----', 30, (lastTable + 46), 'left');*/

					doc.text('Adm. Gudang', (pageWidth - 38), (lastTable + 23), 'left');
					doc.text('Tgl. ', (pageWidth - 30), (lastTable + 28), 'left');
					doc.text('(', (pageWidth - 46), (lastTable + 45), 'left');
					doc.line((pageWidth - 45), (lastTable + 46), (pageWidth - 10), (lastTable + 46));
					doc.text(')', (pageWidth - 10), (lastTable + 45), 'left');

					doc.text('Penerima', (pageWidth - 72), (lastTable + 23), 'left');
					doc.text('Tgl. ', (pageWidth - 68), (lastTable + 28), 'left');
					doc.text('(', (pageWidth - 81), (lastTable + 45), 'left');
					doc.line((pageWidth - 80), (lastTable + 46), (pageWidth - 50), (lastTable + 46));
					doc.text(')', (pageWidth - 50), (lastTable + 45), 'left');

					doc.text('Ka. Gudang', (pageWidth - 109), (lastTable + 23), 'left');
					doc.text('Tgl. ', (pageWidth - 103), (lastTable + 28), 'left');
					doc.text('(', (pageWidth - 116), (lastTable + 45), 'left');
					doc.line((pageWidth - 115), (lastTable + 46), (pageWidth - 85), (lastTable + 46));
					doc.text(')', (pageWidth - 85), (lastTable + 45), 'left');

					doc.text('Pembelian', (pageWidth - 142), (lastTable + 23), 'left');
					doc.text('Tgl. ', (pageWidth - 137), (lastTable + 28), 'left');
					doc.text('(', (pageWidth - 151), (lastTable + 45), 'left');
					doc.line((pageWidth - 150), (lastTable + 46), (pageWidth - 120), (lastTable + 46));
					doc.text(')', (pageWidth - 120), (lastTable + 45), 'left');

					// doc.line(50, 55, 60, 80);
					doc.line(5, lastTable, 5, (lastTable + 51));
					doc.line((pageWidth - 5), lastTable, (pageWidth - 5), (lastTable + 51));
					doc.line(5, (lastTable + 51), (pageWidth - 5), (lastTable + 51));

					doc.save('Report '+no_btbwo+'-'+type_btbwo.toUpperCase()+' .pdf');
				}else swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			}
		});
	}

	function listbtb(){
		$("#listbtb").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'btb_work_order/lists/';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false,
			"destroy": true,
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip',
			"initComplete": function(){
				var element =
					'<div class="btn-group pull-left">'+
						'<a class="btn btn-primary" onClick="btb_add()">'+
							'<i class="fa fa-plus"></i> Add BTB'+
						'</a>'+
					'</div>'+
					'<div class="btn-group pull-left">'+
						'<a class="btn btn-success" onClick="btb_report()">'+
							'<i class="fa fa-file-pdf-o"></i> Cetak Laporan BTB'+
						'</a>'+
					'</div>';
				$("div.toolbar").prepend(element);
			},
			"columnDefs": [{
				"targets": [0, 1, 3, 4],
				"className": 'dt-body-center'
			},{
				"targets": [5],
				"width": 100,
				"className": 'dt-body-center'
			}]
		});
	}
</script>