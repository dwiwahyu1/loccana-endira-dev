<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Kontra_Bon extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('kontra_bon/kontra_bon_model');
		$this->load->model('jurnal_np/jurnal_np_model');
		$this->load->model('jurnal/jurnal_model');
		$this->load->library('log_activity');
		$this->load->library('sequence');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'kontra_bon/views/index');
	}

	function list_kontra_bon() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

		$order_fields = array('', 'id_kontra_bon', 'no_kb', 'date_kb');
		$search = $this->input->get_post('search');
		$search_val = (!empty($search['value'])) ? $search['value'] : null;
		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		if($params['limit'] < 0) $params['limit'] = $this->kontra_bon_model->count_kontra_bon($params);

		$list = $this->kontra_bon_model->list_kontra_bon($params);
		// $list2 = $this->kontra_bon_model->list_kontra_bon_non_po($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		$data = array();
		$i = $params['offset']+1;
		foreach ($list['data'] as $k => $v) {
			$strBtn =
				'<div class="btn-group">'.
					'<a class="btn btn-primary btn-sm" title="Detail" onClick="detail_kontra_bon(\''.$v['id_kontra_bon'].'\')">'.
						'<i class="fa fa-search"></i>'.
					'</a>'.
				'</div>'.
				'<div class="btn-group">'.
					'<a class="btn btn-warning btn-sm" title="Edit" onClick="edit_kontra_bon(\''.$v['id_kontra_bon'].'\')">'.
						'<i class="fa fa-edit"></i>'.
					'</a>'.
				'</div>'.
				'<div class="btn-group">'.
					'<a class="btn btn-danger btn-sm" title="Delete" onClick="delete_kontra_bon(\''.$v['id_kontra_bon'].'\')">'.
						'<i class="fa fa-trash"></i>'.
					'</a>'.
				'</div>';
			
			if($v['type'] == 1) $v['name_eksternal'] = $v['name_eksternal_dist'];

			if(is_numeric($v['term_of_payment']) == true) $jatuh_tempo = $v['term_of_payment'].' Hari';
			else $jatuh_tempo = date('d-M-Y', strtotime(preg_replace('|/|', '-', $v['term_of_payment'])));

			array_push($data, array(
				$i++,
				$v['no_kb'],
				$v['no_faktur'],
				date('d-M-Y', strtotime($v['date_kb'])),
				$v['name_eksternal'],
				$jatuh_tempo,
				number_format(round($v['total_amount']),0,',','.'),
				$strBtn
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function detail_stok_opname($periode) {
		// echo "<pre>";
		// print_r($periode);die;
		// $result_opname = $this->kontra_bon_model->edit_perubahan($id_opname);

		$data = array(
			'periode' => (string)$periode
		);

		$this->load->view('detail_view', $data);
	}

	public function add_kontra_bon() {
		$coa_all	= $this->jurnal_np_model->coa_list();
		/*$level1		= $this->jurnal_model->coa("id_parent", 0);
		$level2		= $this->jurnal_model->coa("id_parent", $level1[0]["coa"]);
		$level3		= $this->jurnal_model->coa("id_parent", $level2[0]["coa"]);
		$level4		= $this->jurnal_model->coa("id_parent", $level3[0]["coa"]);*/

		$valas 			= $this->jurnal_model->valas();
		$distributor 	= $this->kontra_bon_model->get_distributor();

		$data = array(
			'coa_all'		=> $coa_all,
			/*'level1'		=> $level1,
			'level2'		=> $level2,
			'level3'		=> $level3,
			'level4'		=> $level4,*/
			'valas'			=> $valas,
			'distributor'	=> $distributor
		);

		$this->load->view('add_modal_view',$data);
	}

	public function get_invoice() {
		$result_po = $this->kontra_bon_model->get_purchase_order();

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result_po);
	}

	public function set_invoice() {
		$data 			= file_get_contents("php://input");
		$params 		= json_decode($data,true);
		
		$result_get_po 	= $this->kontra_bon_model->get_purchase_order_by_id2($params['id_po'], $params['id_btb']);
		
		// echo "<pre>";print_r($result_get_po);die;
		
		if(sizeof($result_get_po) > 0) {
			$result_get_po[0]['nama_supplier'] 		= $result_get_po[0]['name_eksternal'];
			$result_get_po[0]['date_po'] 			= date('d-M-Y', strtotime($result_get_po[0]['date_po']));
			//$result_get_po[0]['total_amount'] 		= $result_get_po[0]['symbol'].' '.number_format(round((int)$result_get_po[0]['total_amount']),0,',','.');
			$result_get_po[0]['total_amount'] 		= $result_get_po[0]['symbol'].' '.number_format(round((int)$result_get_po[0]['btb_amount']),0,',','.');
			$result_get_po[0]['term_of_payment'] 	= preg_replace('/\D/', '', $result_get_po[0]['term_of_payment']);
			$result_get_po[0]['jatuh_tempo'] 		= date('d-M-Y', strtotime($result_get_po[0]['date_po'] . ' +'.$result_get_po[0]['term_of_payment'].' day'));
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result_get_po);
	}

	public function set_invoice_edit() {
		$data 			= file_get_contents("php://input");
		$params 		= json_decode($data,true);

		$result_get_po 	= $this->kontra_bon_model->get_purchase_order_by_id2_edit($params['id_po']);
		
		// echo "<pre>";print_r($result_get_po);die;
		
		if(sizeof($result_get_po) > 0) {
			$result_get_po[0]['nama_supplier'] 		= $result_get_po[0]['name_eksternal'];
			$result_get_po[0]['date_po'] 			= date('d-M-Y', strtotime($result_get_po[0]['date_po']));
			//$result_get_po[0]['total_amount'] 		= $result_get_po[0]['symbol'].' '.number_format(round((int)$result_get_po[0]['total_amount']),0,',','.');
			$result_get_po[0]['total_amount'] 		= $result_get_po[0]['symbol'].' '.number_format(round((int)$result_get_po[0]['btb_amount']),0,',','.');
			$result_get_po[0]['term_of_payment'] 	= preg_replace('/\D/', '', $result_get_po[0]['term_of_payment']);
			$result_get_po[0]['jatuh_tempo'] 		= date('d-M-Y', strtotime($result_get_po[0]['date_po'] . ' +'.$result_get_po[0]['term_of_payment'].' day'));
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result_get_po);
	}
	
	public function set_btb_po() {
		$data 			= file_get_contents("php://input");
		$params 		= json_decode($data,true);
		
		$result 		= $this->kontra_bon_model->set_btb_po($params['id_po']);
		
		if(sizeof($result) > 0){
			$result_get_po = array(
				'status' => true,
				'message' => 'Data BTB ditemukan',
				'data' => $result
			);
		}else{
			$result_get_po = array(
				'status' => false,
				'message' => 'Maaf data BTB tidak ditemukan, silahkan cek kembali Data PO yang berisi BTB',
				'data' => $result
			);
		}
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result_get_po);
	}

	public function save_kontra_bon() {
		$this->form_validation->set_rules('no_faktur', 'Nomor Faktur', 'trim|required');
		$this->form_validation->set_rules('tgl_kontra_bon', 'Tanggal Kontra Bon', 'trim|required');
		$this->form_validation->set_rules('no_invoice', 'Nomor Invoice', 'trim|required');
		//$this->form_validation->set_rules('id_coa_masuk', 'Nomor id_coa_masuk', 'trim');
		
		// $this->form_validation->set_rules('level1', 'Nomor Invoice', 'trim');
		// $this->form_validation->set_rules('level2', 'Nomor Invoice', 'trim');
		// $this->form_validation->set_rules('level3', 'Nomor Invoice', 'trim');
		// $this->form_validation->set_rules('level4', 'Nomor Invoice', 'trim');
		
		$id_coa_masuk = $this->Anti_sql_injection($this->input->post('id_coa_masuk', TRUE));

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$no_kontra_bon 		= $this->sequence->get_no('kontra_bon');
			$no_faktur 			= $this->Anti_sql_injection($this->input->post('no_faktur', TRUE));
			$tgl_kontra_bon 	= $this->Anti_sql_injection($this->input->post('tgl_kontra_bon', TRUE));
			$no_invoice 		= $this->Anti_sql_injection($this->input->post('no_invoice', TRUE));
			$jumlah 			= $this->Anti_sql_injection($this->input->post('jumlah', TRUE));
			$jatuh_tempo 		= $this->Anti_sql_injection($this->input->post('jatuh_tempo', TRUE));
			$btb 				= $this->Anti_sql_injection($this->input->post('btb', TRUE));
			// var_dump($btb);die;
			
			$jumlah = preg_replace('#[^0-9\.,]#', '', $jumlah);
			$jumlah = str_replace('.','',$jumlah);

			if($no_invoice != 'non_po') {
				$data = array(
					'id_po'				=> $no_invoice,
					'no_kb'				=> $no_kontra_bon,
					'no_faktur'			=> $no_faktur,
					'tgl_kb'			=> date('Y-m-d', strtotime($tgl_kontra_bon)),
					'id_coa_masuk'		=> $id_coa_masuk,
					'amount' 			=> $jumlah,
					'jatuh_tempo'		=> $jatuh_tempo,
					'id_btb'			=> $btb
				);

				$result_add = $this->kontra_bon_model->kb_add($data); 
			
				$get_key = $this->kontra_bon_model->kb_get_po($result_add['lastid']);
				if($get_key[0]['term_of_payment'] == 'CASH') {
					$this->kontra_bon_model->add_coa_values_cash($data_kartu_hp);
				}else {
					$get_data_id = $this->kontra_bon_model->get_id_coa($get_key);
					
					$data_coa = array(
						'id_coa'		=> $get_key[0]['dist_coa'],
						'id_parent'		=> 0,
						'date_kb'		=> date('Y-m-d', strtotime($tgl_kontra_bon)),
						'id_valas'		=> $get_key[0]['id_valas'],
						'value'			=> $jumlah,
						'adjusment'		=> 0,
						'type_cash'		=> 1,
						'note'			=> 'Penerbitan Hutang Faktur '.$get_key[0]['no_faktur'], 
						'rate'			=> $get_key[0]['rate'],
						'bukti'			=> NULL,
						'id_coa_temp'	=> NULL
					);
					//$add_coa = $this->kontra_bon_model->add_coa_values($data_coa);
					$coa_btb = $this->kontra_bon_model->get_coa_btb($btb);

						

					$data_coa = array(
						'id_kontra_bon'		=> $result_add['lastid'],
						'coa_value'			=> $coa_btb[0]['id_coa_value']
					);
					$kb_add_coa = $this->kontra_bon_model->kb_add_coa($data_coa);

					$insert_val_d = $this->kontra_bon_model->add_po_spb_coa($coa_btb[0]['id_coa_value'], $get_key[0]['id_po']);
					
					// $data_kartu_hp = array(
						// 'ref' 			=> NULL,
						// 'source' 		=> NUll,
						// 'keterangan' 	=> 'Penerbitan Hutang Faktur '.$get_key[0]['no_faktur'],
						// 'status' 		=> 0,
						// 'saldo' 		=> $jumlah,
						// 'saldo_akhir' 	=> $jumlah,
						// 'id_valas'		=> $get_key[0]['id_valas'],
						// 'type_kartu'	=> 0,
						// 'id_master'		=> $add_coa['lastid'],
						// 'delivery_date'	=> $get_key[0]['delivery_date'],
						// 'type_master'	=> 0,
						// 'payment_coa'	=> $get_key[0]['payment_coa'],
						// 'jatuh_tempo' 	=> $jatuh_tempo
					// );
					//$add_kartu_hp = $this->kontra_bon_model->add_kartu_hp2($data_kartu_hp);
					
					$data_coa2 = array (
						'id_coa'		=> $id_coa_masuk,
						'id_parent'		=> 0,
						'date_kb'		=> date('Y-m-d', strtotime($tgl_kontra_bon)),
						'id_valas'		=> $get_key[0]['id_valas'],
						'value'			=> $jumlah, 
						'adjusment'		=> 0,
						'type_cash'		=> 0,
						'note'			=> 'Pembelian Faktur '.$get_key[0]['no_faktur'], 
						'rate'			=> $get_key[0]['rate'],
						'bukti'			=> NULL,
						'id_coa_temp'	=> NULL
					);
					//$add_coa2 = $this->kontra_bon_model->add_coa_values($data_coa2);

					// $data_coa2 = array(
						// 'id_kontra_bon'		=> $result_add['lastid'],
						// 'coa_value'			=> $add_coa2['lastid']
					// );
					//$kb_add_coa2 = $this->kontra_bon_model->kb_add_coa($data_coa2);

					// $data_kartu_hp2 = array(
						// 'ref' 			=> NULL, 
						// 'source' 		=> NUll,
						// 'keterangan' 	=> 'Pembelian Hutang Faktur '.$get_key[0]['no_faktur'], 
						// 'status' 		=> 0,  
						// 'saldo' 		=> $jumlah,
						// 'saldo_akhir' 	=> $jumlah,
						// 'id_valas'		=> $get_key[0]['id_valas'],
						// 'type_kartu'	=> 0,
						// 'id_master'		=> $add_coa2['lastid'],
						// 'delivery_date'	=> $get_key[0]['delivery_date'],
						// 'type_master'	=> 0,
						// 'payment_coa'	=> $get_key[0]['payment_coa'],
						// 'jatuh_tempo' 	=> $jatuh_tempo
					// );
					//$add_kartu_hp2 = $this->kontra_bon_model->add_kartu_hp2($data_kartu_hp2);
					
					$data_coa3 = array (
						'id_coa'		=> 120,
						'id_parent'		=> 0,
						'date_kb'		=> date('Y-m-d', strtotime($tgl_kontra_bon)),
						'id_valas'		=> $get_key[0]['id_valas'],
						'value'			=> $jumlah, 
						'adjusment'		=> 0,
						'type_cash'		=> 0,
						'note'			=> 'Pembelian Faktur '.$get_key[0]['no_faktur'], 
						'rate'			=> $get_key[0]['rate'],
						'bukti'			=> NULL,
						'id_coa_temp'	=> NULL
					);
					//$add_coa3 = $this->kontra_bon_model->add_coa_values($data_coa3);

					// $data_coa3 = array(
						// 'id_kontra_bon'		=> $result_add['lastid'],
						// 'coa_value'			=> $add_coa3['lastid']
					// );
					//$kb_add_coa3 = $this->kontra_bon_model->kb_add_coa($data_coa3);
				}
			}else {
				$id_distributor	= $this->Anti_sql_injection($this->input->post('nama_supplier', TRUE));
				$valas 			= $this->Anti_sql_injection($this->input->post('valas', TRUE));
				$rate 			= $this->Anti_sql_injection($this->input->post('rate', TRUE));
				$get_coa 		= $this->kontra_bon_model->get_coa($id_distributor);

				$data = array(
					'id_po'				=> 0,
					'no_kb'				=> $no_kontra_bon,
					'no_faktur'			=> $no_faktur,
					'tgl_kb'			=> date('Y-m-d', strtotime($tgl_kontra_bon)),
					'id_coa_masuk'		=> $id_coa_masuk,
					'amount' 			=> $jumlah,
					'jatuh_tempo'		=> $jatuh_tempo,
					'rate'				=> $rate,
					'valas'				=> $valas,
					'id_distributor'	=> $id_distributor,
					'type'				=> 1
				);

				$result_add = $this->kontra_bon_model->kb_add_non_po($data);
				if($result_add['result'] > 0) {
					//ADD COA SUPPLIER
					$data_coa = array (
						'id_coa'		=> $get_coa[0]['id_coa'],
						'id_parent'		=> 0,
						'date_kb'		=> date('Y-m-d', strtotime($tgl_kontra_bon)),
						'id_valas'		=> $valas,
						'value'			=> $jumlah, 
						'adjusment'		=> 0,
						'type_cash'		=> 1,
						'note'			=> 'Penerbitan Hutang Faktur '.$no_faktur,
						'rate'			=> $rate,
						'bukti'			=> NULL,
						'id_coa_temp'	=> NULL
					);
					$add_coa = $this->kontra_bon_model->add_coa_values($data_coa);

					$data_coa = array(
						'id_kontra_bon'		=> $result_add['lastid'],
						'coa_value'			=> $add_coa['lastid']
					);

					$kb_add_coa = $this->kontra_bon_model->kb_add_coa($data_coa);

					$data_kartu_hp = array(
						'ref'			=> NULL,
						'source'		=> NULL,
						'keterangan'	=> 'Penerbitan Hutang Faktur '.$no_faktur,
						'status'		=> 0,
						'saldo'			=> $jumlah,
						'saldo_akhir'	=> $jumlah,
						'id_valas'		=> $valas,
						'type_kartu'	=> 0,
						'id_master'		=> $add_coa['lastid'],
						'type_master'	=> 0,
						'jatuh_tempo'	=> $jatuh_tempo
					);
					$add_kartu_hp = $this->kontra_bon_model->add_kartu_hp2($data_kartu_hp);

					//ADD COA
					$data_coa2 = array (
						'id_coa'		=> $id_coa_masuk,
						'id_parent'		=> 0,
						'date_kb'		=> date('Y-m-d', strtotime($tgl_kontra_bon)),
						'id_valas'		=> $valas,
						'value'			=> $jumlah, 
						'adjusment'		=> 0,
						'type_cash'		=> 0,
						'note'			=> 'Pembelian Faktur '.$no_faktur,
						'rate'			=> $rate,
						'bukti'			=> NULL,
						'id_coa_temp'	=> NULL
					);
					$add_coa2 = $this->kontra_bon_model->add_coa_values($data_coa2);

					$data_coa = array(
						'id_kontra_bon'		=> $result_add['lastid'],
						'coa_value'			=> $add_coa2['lastid']
					);

					$kb_add_coa = $this->kontra_bon_model->kb_add_coa($data_coa);

					$data_kartu_hp2 = array(
						'ref'			=> NULL,
						'source'		=> NULL,
						'keterangan'	=> 'Pembelian Hutang Faktur '.$no_faktur,
						'status'		=> 0,
						'saldo'			=> $jumlah,
						'saldo_akhir'	=> $jumlah,
						'id_valas'		=> $valas,
						'type_kartu'	=> 0,
						'id_master'		=> $add_coa2['lastid'],
						'type_master'	=> 0,
						'jatuh_tempo'	=> $jatuh_tempo
					);
					//$add_kartu_hp2 = $this->kontra_bon_model->add_kartu_hp2($data_kartu_hp2);
				}
			}
				
			if($result_add['result'] > 0) {
				$this->log_activity->insert_activity('insert', 'Insert Kontra Bon No Kontra Bon = '.$no_kontra_bon);
				$result = array('success' => true, 'message' => 'Berhasil menambahkan Kontra Bon ke database');
			}else {
				$this->log_activity->insert_activity('insert', 'Gagal Kontra Bon No Kontra Bon = '.$no_kontra_bon);
				$result = array('success' => false, 'message' => 'Gagal menambahkan Kontra Bon ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function edit_kontra_bon($id) {
		$get_id		= $this->kontra_bon_model->get_id_kontrabon($id);
		$coa_all	= $this->jurnal_np_model->coa_list();
		
		if($get_id[0]['id_btb'] != '' && $get_id[0]['id_btb'] != NULL && $get_id[0]['id_btb'] != 0) {
			$type_kb	= $this->kontra_bon_model->get_type_kb1($id,$get_id[0]['id_btb']);
		}else{
			$type_kb	= $this->kontra_bon_model->get_type_kb2($id);
		}

		$valas 			= $this->jurnal_model->valas();
		$distributor 	= $this->kontra_bon_model->get_distributor();
		$result_po 		= $this->kontra_bon_model->get_purchase_order();

		if($type_kb[0]['type'] != 1) {
			$result_kb = $this->kontra_bon_model->edit_kontra_bon2($id,$get_id[0]['id_btb']);
		}else{
			$result_kb = $this->kontra_bon_model->edit_kontra_bon_non_po2($id,$get_id[0]['id_btb']);
		}

		$data = array(
			'coa_all'		=> $coa_all,
			'valas'			=> $valas,
			'distributor'	=> $distributor,
			'type_kb'		=> $type_kb,
			'po'			=> $result_po,
			'kontra_bon'	=> $result_kb,
			'id_btb'		=> $get_id[0]['id_btb']
		);

		$this->load->view('edit_modal_view', $data);
	}

	public function save_edit_kontra_bon() {
		$this->form_validation->set_rules('no_kontra_bon', 'No Kontra Bon', 'trim|required');
		$this->form_validation->set_rules('no_faktur', 'No Faktur', 'trim|required');
		$this->form_validation->set_rules('tgl_kontra_bon', 'Tanggal Kontra Bon', 'trim|required');
		// $this->form_validation->set_rules('no_invoice', 'Nomor Invoice', 'trim|required');
		$this->form_validation->set_rules('id_coa_masuk', 'COA', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$type_kb = $this->Anti_sql_injection($this->input->post('type_kb', TRUE));

			if($type_kb != 1) {
				$id_kontra_bon 		= $this->Anti_sql_injection($this->input->post('id_kontra_bon', TRUE));
				$no_kontra_bon 		= $this->Anti_sql_injection($this->input->post('no_kontra_bon', TRUE));
				$no_faktur 			= $this->Anti_sql_injection($this->input->post('no_faktur', TRUE));
				$tgl_kontra_bon 	= $this->Anti_sql_injection($this->input->post('tgl_kontra_bon', TRUE));
				$no_invoice 		= $this->Anti_sql_injection($this->input->post('no_invoice', TRUE));
				$id_coa_masuk 		= $this->Anti_sql_injection($this->input->post('id_coa_masuk', TRUE));
				$jatuh_tempo 		= $this->Anti_sql_injection($this->input->post('jatuh_tempo', TRUE));
				$jumlah 			= $this->Anti_sql_injection($this->input->post('jumlah', TRUE));
				$id_distributor 	= $this->Anti_sql_injection($this->input->post('nama_supplier', TRUE));
				$btb			 	= $this->Anti_sql_injection($this->input->post('btb', TRUE));
				
				$jumlah = preg_replace('#[^0-9\.,]#', '', $jumlah);
				$jumlah = str_replace('.','',$jumlah);

				$get_kb_coavalue	= $this->kontra_bon_model->get_kb_coavalue($id_kontra_bon);
				
				$data = array(
					'id_kontra_bon'		=> $id_kontra_bon,
					'id_purchase_order'	=> $no_invoice,
					'no_kb'				=> $no_kontra_bon,
					'no_faktur'			=> $no_faktur,
					'date_kb'			=> date('Y-m-d', strtotime($tgl_kontra_bon)),
					'coa'				=> $id_coa_masuk,
					'amount'			=> $jumlah,
					'jatuh_tempo'		=> $jatuh_tempo,
					'rate'				=> NULL,
					'valas'				=> NULL,
					'id_distributor'	=> 0,
					'type'				=> 0,
					'id_btb'			=> $btb
				);

				$result_update = $this->kontra_bon_model->kb_update($data);

				if(sizeof($get_kb_coavalue) > 1) {
					$get_key 			= $this->kontra_bon_model->kb_get_po($id_kontra_bon);
					$get_data_id 		= $this->kontra_bon_model->get_id_coa($get_key);

					$data_coavalue = array(
						'id'			=> $get_kb_coavalue[0]['coa_value'],
						'id_coa'		=> $get_key[0]['dist_coa'],
						'id_parent'		=> 0,
						'date_kb'		=> date('Y-m-d', strtotime($tgl_kontra_bon)),
						'id_valas'		=> $get_key[0]['id_valas'],
						'value'			=> $jumlah,
						'value_real'	=> floatval($jumlah)*$get_key[0]['rate'],
						'rate'			=> $get_key[0]['rate'],
						'bukti'			=> NULL,
						'id_coa_temp'	=> NULL
					);
					$this->kontra_bon_model->coavalue_update($data_coavalue);

					$data_kartu_hp = array(
						'tanggal'		=> date('Y-m-d', strtotime($tgl_kontra_bon)),
						'saldo'			=> $jumlah,
						'saldo_akhir'	=> $jumlah,
						'id_valas'		=> $get_key[0]['id_valas'],
						'id_master'		=> $get_kb_coavalue[0]['coa_value'],
						'durasi'		=> $jatuh_tempo
					);
					$this->kontra_bon_model->hp_update_non_po($data_kartu_hp);

					$update_val_d = $this->kontra_bon_model->hp_po_spb_coa_update($get_kb_coavalue[0]['coa_value'], $get_key[0]['id_po']);

					$data_coavalue2 = array(
						'id'			=> $get_kb_coavalue[1]['coa_value'],
						'id_coa'		=> $id_coa_masuk,
						'id_parent'		=> 0,
						'date_kb'		=> date('Y-m-d', strtotime($tgl_kontra_bon)),
						'id_valas'		=> $get_key[0]['id_valas'],
						'value'			=> $jumlah,
						'value_real'	=> floatval($jumlah)*$get_key[0]['rate'],
						'rate'			=> $get_key[0]['rate'],
						'bukti'			=> NULL,
						'id_coa_temp'	=> NULL
					);
					$this->kontra_bon_model->coavalue_update_non_po($data_coavalue2);

					$data_kartu_hp2 = array(
						'tanggal'		=> date('Y-m-d', strtotime($tgl_kontra_bon)),
						'saldo'			=> $jumlah,
						'saldo_akhir'	=> $jumlah,
						'id_valas'		=> $get_key[0]['id_valas'],
						'id_master'		=> $get_kb_coavalue[1]['coa_value'],
						'durasi'		=> $jatuh_tempo
					);
					$this->kontra_bon_model->hp_update_non_po($data_kartu_hp2);

					$data_coavalue3 = array(
						'id'			=> $get_kb_coavalue[2]['coa_value'],
						'id_coa'		=> 120,
						'id_parent'		=> 0,
						'date_kb'		=> date('Y-m-d', strtotime($tgl_kontra_bon)),
						'id_valas'		=> $get_key[0]['id_valas'],
						'value'			=> $jumlah,
						'value_real'	=> floatval($jumlah)*$get_key[0]['rate'],
						'rate'			=> $get_key[0]['rate'],
						'bukti'			=> NULL,
						'id_coa_temp'	=> NULL
					);
					$this->kontra_bon_model->coavalue_update_non_po($data_coavalue3);
				}
			}else {
				$id_kontra_bon 		= $this->Anti_sql_injection($this->input->post('id_kontra_bon', TRUE));
				$no_kontra_bon 		= $this->Anti_sql_injection($this->input->post('no_kontra_bon', TRUE));
				$no_faktur 			= $this->Anti_sql_injection($this->input->post('no_faktur', TRUE));
				$tgl_kontra_bon 	= $this->Anti_sql_injection($this->input->post('tgl_kontra_bon', TRUE));
				$id_distributor 	= $this->Anti_sql_injection($this->input->post('nama_supplier', TRUE));
				$valas				= $this->Anti_sql_injection($this->input->post('valas', TRUE));
				$rate				= $this->Anti_sql_injection($this->input->post('rate', TRUE));
				$jatuh_tempo		= $this->Anti_sql_injection($this->input->post('jatuh_tempo', TRUE));
				$jumlah				= $this->Anti_sql_injection($this->input->post('jumlah', TRUE));
				$id_coa_masuk 		= $this->Anti_sql_injection($this->input->post('id_coa_masuk', TRUE));

				$jumlah = preg_replace('#[^0-9\.,]#', '', $jumlah);
				$jumlah = str_replace('.','',$jumlah);

				$get_coa 			= $this->kontra_bon_model->get_coa($id_distributor);
				$get_kb_coavalue	= $this->kontra_bon_model->get_kb_coavalue($id_kontra_bon);

				$data = array(
					'id_kontra_bon'		=> $id_kontra_bon,
					'id_purchase_order'	=> 0,
					'no_kb'				=> $no_kontra_bon,
					'no_faktur'			=> $no_faktur,
					'date_kb'			=> date('Y-m-d', strtotime($tgl_kontra_bon)),
					'coa'				=> $id_coa_masuk,
					'amount'			=> $jumlah,
					'jatuh_tempo'		=> $jatuh_tempo,
					'rate'				=> $rate,
					'valas'				=> $valas,
					'id_distributor'	=> $id_distributor,
					'type'				=> 1,
					'id_btb'			=> $btb
				);
				
				$result_update = $this->kontra_bon_model->kb_update_non_po($data);

				if(sizeof($get_coa) > 0 && sizeof($get_kb_coavalue) > 1) {
					$data_coavalue = array (
						'id'			=> $get_kb_coavalue[0]['coa_value'],
						'id_coa'		=> $get_coa[0]['id_coa'],
						'id_valas'		=> $valas,
						'value'			=> $jumlah,
						'value_real'	=> floatval($jumlah)*$rate,
						'rate'			=> $rate
					);
					$this->kontra_bon_model->coavalue_update_non_po($data_coavalue);

					$data_kartu_hp = array(
						'tanggal'		=> date('Y-m-d', strtotime($tgl_kontra_bon)),
						'saldo'			=> $jumlah,
						'saldo_akhir'	=> $jumlah,
						'id_valas'		=> $valas,
						'id_master'		=> $get_kb_coavalue[0]['coa_value'],
						'durasi'		=> $jatuh_tempo
					);
					$this->kontra_bon_model->hp_update_non_po($data_kartu_hp);

					$data_coavalue2 = array (
						'id'			=> $get_kb_coavalue[1]['coa_value'],
						'id_coa'		=> $id_coa_masuk,
						'id_valas'		=> $valas,
						'value'			=> $jumlah,
						'value_real'	=> floatval($jumlah)*$rate,
						'rate'			=> $rate
					);
					$this->kontra_bon_model->coavalue_update_non_po($data_coavalue2);

					$data_kartu_hp2 = array(
						'tanggal'		=> date('Y-m-d', strtotime($tgl_kontra_bon)),
						'saldo'			=> $jumlah,
						'saldo_akhir'	=> $jumlah,
						'id_valas'		=> $valas,
						'id_master'		=> $get_kb_coavalue[1]['coa_value'],
						'durasi'		=> $jatuh_tempo
					);
					$this->kontra_bon_model->hp_update_non_po($data_kartu_hp2);
				}
			}

			if($result_update['status'] > 0) {
				$this->log_activity->insert_activity('update', 'Update Kontra Bon id : '.$id_kontra_bon);
				$result = array('success' => true, 'message' => 'Berhasil mengubah Kontra Bon ke database');
			}else {
				$this->log_activity->insert_activity('update', 'Gagal Update Kontra Bon id : '.$id_kontra_bon);
				$result = array('success' => false, 'message' => 'Gagal mengubah Kontra Bon ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function delete_kontra_bon() {
		$data 	= file_get_contents("php://input");
		$params 	= json_decode($data,true);

		$get_id		= $this->kontra_bon_model->get_id_kontrabon($params['id']);
		$type_kb	= $this->kontra_bon_model->get_type_kb($params['id']);

		if($type_kb[0]['type'] != 1) {
			$result_kb = $this->kontra_bon_model->edit_kontra_bon2($params['id'],$get_id[0]['id_btb']);
			$get_kb_coavalue	= $this->kontra_bon_model->get_kb_coavalue($params['id']);

			$result_kb_coa 		= $this->kontra_bon_model->kb_coa_delete($params['id']);
			foreach ($get_kb_coavalue as $gk) {
				$result_po_spb_coa 	= $this->kontra_bon_model->delete_po_spb_coa($gk['coa_value']);
				$result_coavalue 	= $this->kontra_bon_model->coavalue_delete($gk['coa_value']);
				$result_kartu_hp 	= $this->kontra_bon_model->kartu_hp_delete($gk['coa_value']);
			}
		}else {
			$result_kb = $this->kontra_bon_model->edit_kontra_bon_non_po2($params['id'],$get_id[0]['id_btb']);

			if(sizeof($result_kb) > 0) {
				$result_kb_coa 		= $this->kontra_bon_model->kb_coa_delete($params['id']);
				foreach ($result_kb as $rk) {
					$result_coavalue 	= $this->kontra_bon_model->coavalue_delete($rk['coa_value']);
					$result_kartu_hp 	= $this->kontra_bon_model->kartu_hp_delete($rk['coa_value']);
				}
			}
		}

		$result = $this->kontra_bon_model->kb_delete($params);

		if($result > 0) {
			$this->log_activity->insert_activity('delete', 'Delete Kontra Bon id : '.$params['id']);
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}else {
			$this->log_activity->insert_activity('delete', 'Gagal Delete Kontra Bon id : '.$params['id']);
			$res = array('status' => 'failed', 'message' => 'Data gagal di hapus');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function detail_kontra_bon($id) {
		$get_id		= $this->kontra_bon_model->get_id_kontrabon($id);
		$coa_all	= $this->jurnal_np_model->coa_list();
		
		if($get_id[0]['id_btb'] != '' || $get_id[0]['id_btb'] != NULL || $get_id[0]['id_btb'] != 0) {
			$type_kb	= $this->kontra_bon_model->get_type_kb1($id,$get_id[0]['id_btb']);
		}else{
			$type_kb	= $this->kontra_bon_model->get_type_kb2($id);
		}
		
		$valas 			= $this->jurnal_model->valas();
		$distributor 	= $this->kontra_bon_model->get_distributor();
		$result_po 		= $this->kontra_bon_model->get_purchase_order();

		if($type_kb[0]['type'] != 1) $result_kb = $this->kontra_bon_model->edit_kontra_bon2($id,$get_id[0]['id_btb']);
		else $result_kb = $this->kontra_bon_model->edit_kontra_bon_non_po2($id,$get_id[0]['id_btb']);

		$data = array(
			'coa_all'		=> $coa_all,
			'valas'			=> $valas,
			'distributor'	=> $distributor,
			'type_kb'		=> $type_kb,
			'po'			=> $result_po,
			'kontra_bon'	=> $result_kb
		);

		$this->load->view('detail_view', $data);
	}
	
	public function check_no_faktur(){
		$this->form_validation->set_rules('no_faktur', 'NoFaktur', 'trim|required|min_length[4]|max_length[20]|is_unique[t_kontra_bon.no_faktur]');
		$this->form_validation->set_message('is_unique', 'No Faktur Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'No Faktur Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}
}