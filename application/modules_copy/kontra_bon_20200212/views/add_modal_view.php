<form class="form-horizontal form-label-left" id="add_form" role="form" action="<?php echo base_url('kontra_bon/save_kontra_bon');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<!--<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_kontra_bon">No Kontra Bon <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="no_kontra_bon" name="no_kontra_bon" class="form-control" placeholder="No Kontra Bon"
			value="<?php if(isset($no_kontra_bon)){ echo $no_kontra_bon; }?>" autocomplete="off" readonly>
		</div>
	</div>-->
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_faktur">No Faktur <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="no_faktur" name="no_faktur" class="form-control" placeholder="No Faktur" value="" autocomplete="off">
		</div>
	</div>
	
	<div class="item form-group x-hidden" id="loaders-check">
		<div class="col-md-8 col-sm-6 col-xs-12 col-offset-md-3">
			<div class="loading-container">
				<div class="loading"></div>
				<div id="loading-text">Checking No Faktur ...</div>
			</div>
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tgl_kontra_bon">Tanggal Kontra Bon <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="tgl_kontra_bon" name="tgl_kontra_bon" class="form-control" placeholder="Tanggal Kontra Bon" autocomplete="off" required>
			<!-- <input type="hidden" id="id_coa_masuk" name="id_coa_masuk" class="form-control" placeholder="Tanggal Kontra Bon" autocomplete="off" required> -->
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_invoice">No PO <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="no_invoice" name="no_invoice" style="width: 100%" required>
				<option value="">... Pilih PO ...</option>
				<option value="non_po">Non PO</option>
			</select>
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="btb">BTB</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="btb" name="btb" style="width: 100%">
				<option value="">... Pilih BTB ...</option>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_coa_masuk">Coa <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control select-control" id="id_coa_masuk" name="id_coa_masuk" style="width: 100%" required>
		<?php foreach ($coa_all as $key) { ?>
				<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>"><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
		<?php } ?>
			</select>
		</div>
	</div>

	<!-- <div class="item form-group" id="level2_temp">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control select-control" id="level2" name="level2" style="width: 100%">
				<?php foreach ($level2 as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>"><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group" id="level3_temp">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control select-control" id="level3" name="level3" style="width: 100%">
				<?php foreach ($level3 as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>"><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group" id="level4_temp">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="level4" name="level4" style="width: 100%">
				<?php foreach ($level4 as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>"><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div> -->

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_supplier">Nama Supplier</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="nama_supplier" name="nama_supplier" readonly>
				<option value="">... Pilih Supplier ...</option>
		<?php if(isset($distributor)) {
			foreach ($distributor as $dk) { ?>
				<option value="<?php echo $dk['id']; ?>"><?php echo strtoupper($dk['name_eksternal']); ?></option>
		<?php
			}
		} ?>
			</select>
			<input type="hidden" id="str_nama_supplier" name="str_nama_supplier" autocomplete="off">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="valas">Valas</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="valas" name="valas" readonly>
				<option value="" selected>... Pilih Valas ...</option>
		<?php if(isset($valas)) {
			foreach ($valas as $vk) { ?>
				<option value="<?php echo $vk['valas_id']; ?>"><?php echo $vk['nama_valas']; ?></option>
		<?php
			}
		} ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="rate">Rate</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="number" id="rate" name="rate" min="1" class="form-control" step="1" placeholder="Rate" value="1" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jatuh_tempo">Jatuh Tempo</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="jatuh_tempo" name="jatuh_tempo" class="form-control" placeholder="Jatuh Tempo" autocomplete="off">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jumlah">Jumlah</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="jumlah" name="jumlah" class="form-control" placeholder="Jumlah" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Kontra Bon</button>
		</div>
	</div>
</form> 
<!-- /page content -->

<script type="text/javascript">
	var last_no_faktur = $('#no_faktur').val();
	$(document).ready(function() {
		$('#tgl_kontra_bon').datepicker({
			format: "d-M-yyyy",
			autoclose: true,
			todayHighlight: true
		});
		
		$('#jatuh_tempo').datepicker({
			format: "d-M-yyyy",
			autoclose: true,
			todayHighlight: true
		});

		$('#no_invoice').select2();		
		$('#id_coa_masuk').select2();
		$('#valas').select2();
		$('#nama_supplier').select2();
		$('#nama_supplier').select2('readonly', true);
		$('#valas').select2('readonly', true);
		/*
		$('#level2').select2();
		$('#level3').select2();
		$('#level4').select2();

		$('.select-control').on('change', (function(e) {
			var id = $(this).children('option:selected').data('coa'),
				level = $(this).attr('id');

			coa(level, id);
		}));
		*/
	
		$('#no_invoice').on('change', function() {
			$('#nama_supplier').select2('readonly', true);
			$('#valas').select2('readonly', true);
			$('#rate').prop('readonly', true);
			$('#jumlah').prop('readonly', true);

			if(this.value != '' && this.value != 'non_po') {
				//setInvoice(this.value);
				setBTBPO(this.value);
			}else if(this.value == 'non_po') {
				$('#btb').find('option').not(':first').remove();
				
				$('#nama_supplier').val('').trigger('change');
				$('#nama_supplier').select2('readonly', false);

				$('#valas').val('').trigger('change');
				$('#valas').select2('readonly', false);
				
				$('#rate').val('1');
				$('#rate').prop('readonly', false);

				$('#jumlah').val('');
				$('#jumlah').prop('readonly', false);
			}else {
				$('#btb').find('option').not(':first').remove();

				$('#nama_supplier').val('').trigger('change');
				//$('#jatuh_tempo').val('');
				$('#valas').val('').trigger('change');
				$('#rate').val('1');
				$('#jumlah').val('');
			}
		});
		
		$('#btb').on('change', function() {
			var id_po = $('#no_invoice').val();
			
			if(this.value != '' && this.value != 'non_po') {
				setInvoice(id_po,this.value);
				//setBTBPO(this.value);
			}else {
				$('#nama_supplier').val('').trigger('change');
				//$('#jatuh_tempo').val('');
				$('#valas').val('').trigger('change');
				$('#rate').val('1');
				$('#jumlah').val('');
			}
		});

		$('#nama_supplier').on('change', function() {
			if(this.value != '' && this.value != null) $('#str_nama_supplier').val($(this).find('option:selected').text());
			else $('#str_nama_supplier').val('');
		});
		
		getInvoice();
	});
	
	function coa(level, id) {
		$('#loading').show();
		$.ajax({
			type: 'GET',
			url: "<?php echo base_url(); ?>jurnal_np/change_coa",
			data: {
				id: id
			},
			success: function(response) {
				if (response.length) {
					var element = '<option value="" data-coa="" selected="selected">Pilih COA</option>';
					$.each(response, function(key, val) {
						element += '<option value="' + val.id_coa + '" data-coa="' + val.coa + '">' + val.keterangan + '</option>';
					});

					if (level === 'level1') {
						$('#level2').html(element);
						$('#level2_temp').show();
						//$('#level2').val("");


						coa('level2', response[0].coa); 
					} else if (level === 'level2') {
						$('#level3').html(element);
						$('#level3_temp').show();

						coa('level3', response[0].coa);
					} else {
						$('#level4').html(element);
						$('#level4_temp').show();
						$('#loading').hide();
					}
				} else {
					if (level === 'level1') {
						$('#level2_temp,#level3_temp,#level4_temp').hide();
					} else if (level === 'level2') {
						$('#level3_temp,#level4_temp').hide();
					} else {
						$('#level4_temp').hide();
					}
					$('#loading').hide();
				}
				
				var id_coa_masuk = '0',
				id_parent_masuk = $('#level1').val(),
				coa_number_masuk = $('#level1 option:selected').data("coa"),
				text_coa_masuk = $('#level1 option:selected').text();

			if ($('#level4_temp').is(':visible')) {
				id_coa_masuk = $('#level4').val();
				text_coa_masuk = $('#level4 option:selected').text();
				coa_number_masuk = $('#level4 option:selected').data("coa");
			} else if ($('#level3_temp').is(':visible')) {
				id_coa_masuk = $('#level3').val();
				text_coa_masuk = $('#level3 option:selected').text();
				coa_number_masuk = $('#level3 option:selected').data("coa");
			} else if ($('#level2_temp').is(':visible')) {
				id_coa_masuk = $('#level2').val();
				text_coa_masuk = $('#level2 option:selected').text();
				coa_number_masuk = $('#level2 option:selected').data("coa");
			} else id_coa_masuk = id_parent_masuk;
			
			$('#id_coa_masuk').val(id_coa_masuk);
				
			}
		});
	}
	
	$('#no_faktur').on('input',function(event) {
		if($('#no_faktur').val() != last_no_faktur) {
			$('#loaders-check').removeClass('x-hidden');
			no_faktur_check();
		}
	});

	function no_faktur_check() {
		var no_faktur = $('#no_faktur').val();
		if(no_faktur.length > 3) {
			var post_data = {
				'no_faktur': no_faktur
			};
			
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url('kontra_bon/check_no_faktur');?>",
				data: post_data,
				cache: false,
				success: function(response){
					if(response.success == true){
						$('#no_faktur').css('border', '3px #090 solid');
						$('#loaders-check').addClass('x-hidden');
					}else {
						$('#no_faktur').css('border', '3px #C33 solid');
						$('#loaders-check').removeClass('x-hidden');
					}
				}
			});
		}else {
			$('#no_faktur').css('border', '3px #C33 solid');
			$('#loaders-check').removeClass('x-hidden');
		}
	}
	
	function getInvoice() {
		var html = '';
		//$('#no_invoice').attr('disabled', 'disabled');
		$.ajax({
			type: "GET",
			url: "<?php echo base_url('kontra_bon/get_invoice');?>",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					for (var i = 0; i < r.length; i++) {
						var newOption = new Option(r[i].no_po + ' - ' + r[i].name_eksternal, r[i].id_po, false, false);
						$('#no_invoice').append(newOption);
					}
					$('#no_invoice').removeAttr('disabled');
				}else{
					html +='<option value="" selected>Data PO tidak ada</option>';
					$('#no_invoice').append(html);
				}
			}
		});
	}

	function setInvoice(id_po,id_btb) {
		var datapost = {"id_po" : id_po,"id_btb" : id_btb};

		$.ajax({
			type: "POST",
			url: "<?php echo base_url('kontra_bon/set_invoice');?>", 
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					$('#nama_supplier').val(r[0].id_distributor).trigger('change');
					//$('#jatuh_tempo').val(r[0].jatuh_tempo);
					$('#valas').val(r[0].valas_id).trigger('change');
					$('#rate').val(r[0].rate);
					$('#jumlah').val(r[0].total_amount);
				}else {
					$('#nama_supplier').val('').trigger('change');
					$('#valas').val('').trigger('change');
					$('#rate').val('1');
					//$('#jatuh_tempo').val();
					//$('#jumlah').val();
				}
			}
		});
	}
	
	function setBTBPO(id_po) {
		var datapost = {"id_po" : id_po};
		//var html = "<option value='0'>Pilih BTB</option>";
		var html = "";
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('kontra_bon/set_btb_po');?>",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				var data_btb = response.data;
				$('#btb').find('option').not(':first').remove();

				if(response.status == true) {
					for(var i=0; i < data_btb.length;i++)	{
						var id_btb 		= data_btb[i]['id_btb'];
						var no_btb 		= data_btb[i]['no_btb'];
						var tanggal_btb = data_btb[i]['tanggal_btb'];
						var type_btb 	= data_btb[i]['type_btb'];
					
						html += "<option value='"+id_btb+"'>"+no_btb+"</option>"; 

						//var newOption = new Option(no_btb, id_btb, false, false);
					}

					$('#btb').append(html);
					/*$('#btb').val(null).trigger('change');
					$('#btb').html(html);*/

					//$('#nama_btb').removeClass('x-hidden');
					//$('#loaders').addClass('x-hidden');	
				}else {
					//$('#btb').val();
					/*swal("Perhatian!", response.message, "info");
					$('#btb').html( "<option value=''>... Pilih BTB ... </option>");*/
				}
			}
		});
	}

	$('#add_form').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		var periode_exists = $('#existPeriode').val();
		var url = $(this).attr('action');
		e.preventDefault();

		var formData = new FormData(this);

		$.ajax({
			type:'POST',
			url: $(this).attr('action'),
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						window.location.href = "<?php echo base_url('kontra_bon');?>";
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Tambah Kontra Bon");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah Kontra Bon");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}));
</script>