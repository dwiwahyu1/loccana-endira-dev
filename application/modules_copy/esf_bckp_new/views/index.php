<style>
  .changed_status{cursor:pointer;text-decoration: underline;color:#96b6e8;}
  .changed_status:hover{color:#ff8c00}
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Engineering Specification</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">

                    <table id="listesf" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No</th>
                        <!--  <th>PO No</th>
                          <th>ESF No</th>
                          <th>ESF Date</th> -->
                          <th>Customer</th>
                          <th>Item</th>
                          <th>Pcs/arrays</th>
                          <th>PCB Type</th>
                          <th>PCB Size</th>
                          <th>Unit</th>
                          <th>Option</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>No</td>
                       <!--   <td>PO No</td>
                          <td>ESF No</td>
                          <td>ESF Date</td> -->
                          <td>Customer</td>
                          <td>Item</td>
                          <td>Pcs/arrays</td>
                          <td>PCB Type</td>
                          <td>PCB Size</td>
                          <td>Unit</td>
                          <td>Option</td>
                        </tr>
                      </tbody>
                    </table>

            </div>
        </div><!-- end col -->
    </div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <p></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <p></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  function add_esf(){
      $('#panel-modal').removeData('bs.modal');
      $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modal  .panel-body').load('<?php echo base_url('esf/add');?>');
      $('#panel-modal  .panel-title').html('<i class="fa fa-signal"></i> Choose PO');
      $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
  }

  function editesf(id){
      $('#panel-modalchild').removeData('bs.modal');
      $('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modalchild  .panel-body').load('<?php echo base_url('esf/edit/');?>'+"/"+id);
      $('#panel-modalchild  .panel-title').html('<i class="fa fa-edit"></i> Edit ESF');
      $('#panel-modalchild').modal({backdrop:'static',keyboard:false},'show');
  }

  function deleteesf(id){
    swal({
        title: 'Yakin akan Menghapus ?',
        text: 'data tidak dapat dikembalikan bila sudah dihapus !',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak'
      }).then(function () {
          var datapost={
            "id"  :   id
          };

          $.ajax({
                type:'POST',
                url: "<?php echo base_url().'esf/delete_esf';?>",
                data:JSON.stringify(datapost),
                cache:false,
                contentType: false,
                processData: false,
                success: function(response) {
                    listesf();
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                    })
                }
            });
      });
  }

  function listesf(){
      $("#listesf").dataTable({
          "processing": true,
          "serverSide": true,
          "ajax": "<?php echo base_url().'esf/lists';?>",
          "searchDelay": 700,
          "responsive": true,
          "lengthChange": false,
          "destroy": true,
          "info": false,
          "bSort": false,
          "dom": 'l<"toolbar">frtip',
          "initComplete": function(){
			  var element = '';
              // var element = '<div class="btn-group pull-left">';
                  // element += '  <a class="btn btn-primary" onClick="add_esf()">';
                  // element += '    <i class="fa fa-plus"></i> Add ESF';
                  // element += '  </a>';
                  // element += '</div>';
              $("div.toolbar").prepend(element);
          }
      });
  }

	$(document).ready(function(){
      listesf();
  });
</script>