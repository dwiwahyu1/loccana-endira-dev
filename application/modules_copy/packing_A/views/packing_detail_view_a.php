<style>
	.dt-body-left {text-align:left;}
	.dt-body-right {text-align:right;}
	.dt-body-center {text-align:center;width:10%;}
</style>

<form class="form-horizontal form-label-left" id="packing_edit" role="form" action="<?php echo base_url('packing/packing_edit');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	
	<?php /*<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<br>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_packing">PO Number <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" min="0" id="no_packing" name="no_packing" value="<?php if(isset($packing[0]['no_po'])){ echo $packing[0]['no_po']; }?>" readonly>
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="date_packing">Packing Date<span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<div class="input-group date">
			   <input placeholder="<?php echo date('Y-m-d'); ?>" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="date_packing" name="date_packing" required="required" value="<?php if(isset($packing[0]['date_packing'])){ echo $packing[0]['date_packing']; }?>">
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="cust_id">Customer <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="cust_id" name="cust_id" style="width: 100%" required>
				<option value=""> Select Customer </option>
				<?php foreach($customer as $dk) { ?>
					<option value="<?php echo $dk['id']; ?>" <?php if(isset($packing[0]['id_customer'])){ if( $packing[0]['id_customer'] == $dk['id'] ){ echo "selected"; } }?>><?php echo $dk['name_eksternal']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="part_no">Part Number <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="part_no" name="part_no" value="<?php if(isset($packing[0]['part_number'])){ echo $packing[0]['part_number']; }?>">
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty_order">Qty Order <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="qty_order" name="qty_order" value="<?php if(isset($packing[0]['qty_order'])){ echo $packing[0]['qty_order']; }?>" onkeypress="javascript:return isNumber(event)">
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty_box">Qty Box <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="qty_box" name="qty_box" value="<?php if(isset($packing[0]['qty_box'])){ echo $packing[0]['qty_box']; }?>" onkeypress="javascript:return isNumber(event)">
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id">Qty Array <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="qty_array" name="qty_array" value="<?php if(isset($packing[0]['qty_array'])){ echo $packing[0]['qty_array']; }?>" onkeypress="javascript:return isNumber(event)">
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty_ready">Qty Ready <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="qty_ready" name="qty_ready" value="<?php if(isset($packing[0]['qty_ready'])){ echo $packing[0]['qty_ready']; }?>" onkeypress="javascript:return isNumber(event)">
		</div>
	</div>
	
	<hr>*/ ?>
	
	<div class="item form-group">
		<table id="listdetailpacking" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>No</th>
					<!-- <th>No Packing</th> -->
					<th>Qty Box</th>
					<th>Qty Array</th>
					<th>Net Weight</th>
					<th>Gross Weight</th>
					<th>Packing Date</th>
					<th class="dt-body-center">Option</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$i=1; 
				foreach($packing as $pack => $p) {?>
					<tr>
						<td style="padding-top: 17px;"><?php echo $i; ?></td>
	                  	<!-- <td>
							<input type="text" class="form-control no_pack-control" id="no_pack<?=$i-1;?>" name="no_pack<?=$i-1;?>" style="text-align:left;width: 90px;" required="required" required="required" value="<?php if(isset($p['no_packing'])){ echo $p['no_packing']; }?>">
	                  	</td> -->
		                <td>
		                	<input type="hidden" id="no_pack<?=$i-1;?>" name="no_pack<?=$i-1;?>" value="<?php if(isset($p['no_packing'])){ echo $p['no_packing']; }?>">
		                	<input type="text" class="form-control qtyBox-control " min="0" id="qtyBox<?=$i-1;?>" name="qtyBox<?=$i-1;?>" style="text-align:right;width: 90px;" required="required" value="<?php if(isset($p['qty_box'])){ echo number_format($p['qty_box'],0,',','.'); }?>" onkeypress="javascript:return isNumber(event)">
		                </td>
		                <td>
		                	<input type="text" class="form-control qtyArray-control" min="0" id="qtyArray<?=$i-1;?>" name="qtyArray<?=$i-1;?>" style="text-align:right;width: 90px;" required="required" value="<?php if(isset($p['qty_array'])){ echo number_format($p['qty_array'],0,',','.'); }?>" onkeypress="javascript:return isNumber(event)">
		                </td>
						<td>
		                	<input type="text" class="form-control netWeight-control" min="0" id="netWeight<?=$i-1;?>" name="netWeight<?=$i-1;?>" style="text-align:right;width: 90px;" required="required" value="<?php if(isset($p['qty_array'])){ echo number_format($p['qty_array'],0,',','.'); }?>" onkeypress="javascript:return isNumber(event)">
		                </td>
						<td>
		                	<input type="text" class="form-control grossWeight-control" min="0" id="grossWeight<?=$i-1;?>" name="qtyArray<?=$i-1;?>" style="text-align:right;width: 90px;" required="required" value="<?php if(isset($p['qty_array'])){ echo number_format($p['qty_array'],0,',','.'); }?>" onkeypress="javascript:return isNumber(event)">
		                </td>
						<td>
		                	<div class="input-group date">
							   <input placeholder="<?php echo date('Y-m-d'); ?>" type="text" class="form-control datePack-control datepicker" id="datePack<?=$i-1;?>" name="datePack<?=$i-1;?>" required="required" value="<?php if(isset($p['date_packing'])){ echo $p['date_packing']; }?>">
								<div class="input-group-addon">
									<span class="glyphicon glyphicon-th"></span>
								</div>
							</div>
						</td>
		                <td class="dt-body-center">
		                	<div class="btn-group">
		                		<button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Hapus Packing">
		                			<i class="fa fa-trash"></i>
		                		</button>
		                	</div>
		                </td>
		            </tr>
	            <?php $i++;} ?>
	            <tr>
					<td style="padding-top: 17px;"><?php echo $i; ?></td>
                  	<!-- <td>
						<input type="text" placeholder="No Packing" class="form-control no_pack-control" id="no_pack<?=$i-1;?>" name="no_pack<?=$i-1;?>" style="text-align:left;width: 90px;" value="">
					</td> -->
					<td>
						<input type="hidden" id="no_pack<?=$i-1;?>" name="no_pack<?=$i-1;?>" value="0">
						<input type="text" placeholder="0" class="form-control qtyBox-control" id="qtyBox<?=$i-1;?>" name="qtyBox<?=$i-1;?>" style="text-align:right;width: 90px;" value="" onkeypress="javascript:return isNumber(event)">
					</td>
					<td>
						<input type="text" placeholder="0" class="form-control qtyArray-control" id="qtyArray<?=$i-1;?>" name="qtyArray<?=$i-1;?>" style="text-align:right;width: 90px;" value="" onkeypress="javascript:return isNumber(event)">
					</td>
					<td>
						<input type="text" placeholder="0" class="form-control netWeight-control" id="netWeight<?=$i-1;?>" name="netWeight<?=$i-1;?>" style="text-align:right;width: 90px;" value="" onkeypress="javascript:return isNumber(event)">
					</td>
					<td>
						<input type="text" placeholder="0" class="form-control grossWeight-control" id="grossWeight<?=$i-1;?>" name="grossWeight<?=$i-1;?>" style="text-align:right;width: 90px;" value="" onkeypress="javascript:return isNumber(event)">
					</td>
					<td>
						<div class="input-group date">
						   <input placeholder="<?php echo date('Y-m-d'); ?>" type="text" class="form-control datePack-control datepicker" id="datePack<?=$i-1;?>" name="datePack<?=$i-1;?>">
							<div class="input-group-addon">
								<span class="glyphicon glyphicon-th"></span>
							</div>
						</div>
					</td>
	                <td class="dt-body-center">
	                	<div class="btn-group">
	                		<button class="btn btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Tambah Baru" onClick="add_item()">
	                			<i class="fa fa-plus"></i>
	                		</button>
	                	</div>
	                </td>
	            </tr>
			</tbody>
		</table>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<input type="hidden" class="form-control" id="id_order" name="id_order" value="<?=$id_order;?>">
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Ubah Packing</button>
		</div>
	</div>
</form>
	
<script type="text/javascript">
	var packing_list;
	var items = [],
		itemsLen = "<?php echo count($packing)+1;?>";
		
	$(document).ready(function() {

		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
		jQuery('#date_packing').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true,
			changeYear: true,
			minDate: '-3M',
			maxDate: '+30D',
		});
		<?php if(sizeof($packing) > 0) { for($i=0;$i<sizeof($packing);$i++) { ?>
			jQuery('#datePack<?=$i;?>').datepicker({
				format: "yyyy-mm-dd",
				autoclose: true,
				todayHighlight: true,
				changeYear: true,
				minDate: '-3M',
				maxDate: '+30D',
			});
		<?php } } else if(sizeof($packing) == 0) { ?>
			jQuery('#datePack0').datepicker({
				format: "yyyy-mm-dd",
				autoclose: true,
				todayHighlight: true,
				changeYear: true,
				minDate: '-3M',
				maxDate: '+30D',
			});
		<?php } ?>
		
		<?php for($x=$i-1;$x<$i;$x++) { ?>
			jQuery('#datePack<?=$x+1;?>').datepicker({
				format: "yyyy-mm-dd",
				autoclose: true,
				todayHighlight: true,
				changeYear: true,
				minDate: '-3M',
				maxDate: '+30D',
			});
		<?php } ?>
		
		$( ".btn-danger" ).on( "click", function() {
			$(this).parent().parent().parent().remove();
			itemsLen--;
			looptable();
		});
	});
	
	function add_item(){
		$('#listdetailpacking .btn-group .btn').removeClass('btn-primary');
		$('#listdetailpacking .btn-group .btn').addClass('btn-danger');
		$('#listdetailpacking .btn-group .btn').attr('title','Hapus');
		$('#listdetailpacking .btn-group .btn').attr('data-original-title','Hapus Packing');
		$('#listdetailpacking .btn-group .btn').removeAttr('onClick');
		$('#listdetailpacking .btn-group .fa').removeClass('fa-plus');
		$('#listdetailpacking .btn-group .fa').addClass('fa-trash');

		var itemCount = items.length,
			itemCur = itemsLen;
			itemsLen++;
		
		var element = '<tr>';
			element += '	<td style="padding-top: 17px;">'+itemsLen+'</td>';
			/*element += '	<td>';
			element += '		<input type="text" class="form-control no_pack-control" id="no_pack'+itemCur+'" name="disc_price'+itemCur+'" style="text-align:left;;display: inline-block;width: 90px;" value="0">';
			element += '	</td>';*/
			element += '	<td>';
			element += '		<input type="hidden" id="no_pack'+itemCur+'" name="disc_price'+itemCur+'" value="0">';
			element += '		<input type="text" class="form-control qtyBox-control" id="qtyBox'+itemCur+'" name="qtyBox'+itemCur+'" style="text-align:right;;width: 90px;" value="0" onkeypress="javascript:return isNumber(event)">';
			element += '	</td>';
			element += '	<td>';
			element += '		<input type="text" class="form-control qtyArray-control" id="qtyArray'+itemCur+'" name="qtyArray'+itemCur+'" style="text-align:right;width: 90px;" value="0" onkeypress="javascript:return isNumber(event)">';
			element += '	</td>';
			element += '	<td>';
			element += '		<input type="text" class="form-control netWeight-control" id="netWeight'+itemCur+'" name="netWeight'+itemCur+'" style="text-align:right;width: 90px;" value="0" onkeypress="javascript:return isNumber(event)">';
			element += '	</td>';
			element += '	<td>';
			element += '		<input type="text" class="form-control grossWeight-control" id="grossWeight'+itemCur+'" name="grossWeight'+itemCur+'" style="text-align:right;width: 90px;" value="0" onkeypress="javascript:return isNumber(event)">';
			element += '	</td>';
			element += '	<td>';
			element += '		<div class="input-group date">';
			element += '			<input placeholder="<?php echo date('Y-m-d'); ?>" type="text" class="form-control datePack-control datepicker" id="datePack'+itemCur+'" name="datePack'+itemCur+'" required="required">';
			element += '			<div class="input-group-addon">';
			element += '				<span class="glyphicon glyphicon-th"></span>';
			element += '			</div>';
			element += '		</div>';
			element += '	</td>';
			element += '	<td class="dt-body-center">';
			element += '		<div class="btn-group">';
			element += '			<button class="btn btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Tambah Baru" onClick="add_item()">';
			element += '				<i class="fa fa-plus"></i>';
			element += '			</button>';
			element += '		</div>';
			element += '	</td>';
			element += '</tr>';
			
		$('#listdetailpacking tbody').append(element);
		
		var x;
		var totalLength = parseInt(itemCur)+1;
		for(x=<?=$i-1;?>;x < totalLength; x++) { 
			var q = parseInt(x);
			jQuery('#datePack'+q+'').datepicker({
				format: "yyyy-mm-dd",
				autoclose: true,
				todayHighlight: true,
				changeYear: true,
				minDate: '-3M',
				maxDate: '+30D',
			});
		}
			
		$('[data-toggle="tooltip"]').tooltip();

		$(".btn-danger").on( "click", function() {
			$(this).parent().parent().parent().remove();
			itemsLen--;
			looptable();
		});
	}
	
	$('#packing_edit').on('submit',(function(e) {
		var id_order		= $("#id_order").val(),
			no_pack			= $(".no_pack-control"),
			qtyBox	 		= $(".qtyBox-control"),
			qtyArray		= $(".qtyArray-control"),
			netWeight		= $(".netWeight-control"),
			grossWeight		= $(".grossWeight-control"),
			datePack    	= $(".datePack-control"),
			noPackval   	= [],
			qtyBoxval 		= [],
			qtyArrayval 	= [],
			netWeightval 	= [],
			grossWeightval 	= [],
			dataPackval 	= [];

		var noPackLen   	= no_pack.length,
			qtyBoxLen  		= qtyBox.length,
			qtyArrayLen 	= qtyArray.length,
			netWeightLen 	= netWeight.length,
			grossWeightLen 	= grossWeight.length,
			datePackLen 	= datePack.length;

		for(var i = 0; i < noPackLen; i++){
			if($(no_pack[i]).val() !== ''){
				noPackval.push($(no_pack[i]).val());	
			}
		}

		for(var j = 0; j < qtyBoxLen; j++){
		    qtyBoxval.push($(qtyBox[j]).val());
		}
		
		for(var k = 0; k < qtyArrayLen; k++){
		    qtyArrayval.push($(qtyArray[k]).val());
		}
		
		for(var k = 0; k < netWeightLen; k++){
		    netWeightval.push($(netWeight[k]).val());
		}
		
		for(var k = 0; k < grossWeightLen; k++){
		    grossWeightval.push($(grossWeight[k]).val());
		}

		for(var l = 0; l < datePackLen; l++){
		    dataPackval.push($(datePack[l]).val());
		}

		var datapost={
			"id_order" 		: id_order,
			"no_pack"    	: noPackval,
			"qty_box"    	: qtyBoxval,
			"qty_array"  	: qtyArrayval,
			"net_weight"  	: netWeightval,
			"gross_weight"  : grossWeightval,
			"date_pack"  	: dataPackval
		};
			
	    $('#btn-submit').attr('disabled','disabled');
	    $('#btn-submit').text("Mengubah data...");
	    e.preventDefault();

	    $.ajax({
	        type:'POST',
	        url: $(this).attr('action'),
	        data : JSON.stringify(datapost),
	        dataType: 'json',
			contentType: 'application/json; charset=utf-8',
	        success: function(response) {
				console.log(response);
	            if (response.success == true) {
	              $('.panel-heading button').trigger('click');
				  listpacking();
	                swal({
	                  title: 'Success!',
	                  text: response.message,
	                  type: 'success',
	                  showCancelButton: false,
	                  confirmButtonText: 'Ok'
	                }).then(function () {
	                });
	            } else{
	                $('#btn-submit').removeAttr('disabled');
	                $('#btn-submit').text("Ubah Quotation");
	                swal("Failed!", response.message, "error");
	            }
	        }
	    }).fail(function(xhr, status, message) {
	        $('#btn-submit').removeAttr('disabled');
	        $('#btn-submit').text("Ubah Quotation");
	        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
	    });
	}));
	  
	function looptable(){
		$('#listdetailpacking tbody tr').each(function (index, value) {
			$(this).children('td:first-child').html(index+1);
		});
	}
	
	function isNumber(evt) {
		var iKeyCode = (evt.which) ? evt.which : evt.keyCode
		if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
			return false;

		return true;
	}
	</script>