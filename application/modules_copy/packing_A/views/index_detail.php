<style>
	.dt-body-left {text-align:left;}
	.dt-body-right {text-align:right;}
	.dt-body-center {text-align:center;}
</style>

<div class="container" style="height: auto;">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Packing Detail</h4>
		</div>
	</div>

	<div class="row">
        <div class="col-sm-12">
            <div class="card-box">
				<table id="listdetailpacking" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<!-- <th>No Packing</th> -->
							<th>Qty Box</th>
							<th>Qty Array</th>
							<th>Net Weight</th>
							<th>Gross Weight</th>
							<th>Packing Date</th>
							<th class="dt-body-center">Option</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
            </div>
        </div><!-- end col -->
    </div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:800px;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal-detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:70%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
function packing_edit(id){
	$('#panel-modal').removeData('bs.modal');
	$('#panel-modal .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal .panel-body').load('<?php echo base_url('packing/packing_edit');?>'+'/'+id);
	$('#panel-modal .panel-title').html('<i class="fa fa-pencil-o"></i> Edit Packing');
	$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}

function packing_detail(id){
	$('#panel-modal-detail').removeData('bs.modal');
	$('#panel-modal-detail .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal-detail .panel-body').load('<?php echo base_url('packing/packing_detail');?>'+'/'+id);
	$('#panel-modal-detail .panel-title').html('<i class="fa fa-search-o"></i> Detail Packing');
	$('#panel-modal-detail').modal({backdrop:'static',keyboard:false},'show');
}

function packing_delete(id){
	swal({
		title: 'Yakin akan Menghapus ?',
		text: 'data tidak dapat dikembalikan bila sudah dihapus !',
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Ya',
		cancelButtonText: 'Tidak'
	}).then(function () {
		var datapost={
			"id"  :   id
		};
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('packing/packing_delete');?>",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				
			   swal({
					title: 'Success!',
					text: response.message,
					type: 'success',
					showCancelButton: false,
					confirmButtonText: 'Ok'
				}).then(function () {
					window.location.href = "<?php echo base_url('packing');?>";
				})
				if (response.status == "success") {
				} else{
                    swal("Failed!", response.message, "error");
				}
			}
		});
	})
}

function listpacking(){
	$("#listdetailpacking").dataTable({
		"processing": true,
		"serverSide": true,
		"ajax": "<?php echo base_url().'packing/lists_detail?order_id='.$id_order;?>",
		"searchDelay": 700,
		"responsive": true,
		"lengthChange": false,
		"destroy": true,
		"info": false,
		"bSort": false,
		"dom": 'l<"toolbar">frtip',
		"initComplete": function(result){
			$("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_pack(<?php echo $id_order; ?>)"><i class="fa fa-plus"></i> Tambah Packing</a></div>');
		},
		"columnDefs": [{
			"targets": [0],
			"searchable": false,
		}, {
			"targets": [1,2,3,4],
			"searchable": false,
			"width": 120,
			"className": 'dt-body-right'
		}, {
			"targets": [6],
			"searchable": false,
			"width": 90,
			"className": 'dt-body-center'
		}]
    });
}
  
 function listpacking_s(id){
	$("#listdetailpacking").dataTable({
		"processing": true,
		"serverSide": true,
		"ajax": "<?php echo base_url().'packing/lists_detail?order_id=';?>"+id,
		"searchDelay": 700,
		"responsive": true,
		"lengthChange": false,
		"destroy": true,
		"info": false,
		"bSort": false,
		"dom": 'l<"toolbar">frtip',
		"initComplete": function(result){
			$("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_pack('+id,+')"><i class="fa fa-plus"></i> Tambah Packing</a></div>');
		},
		"columnDefs": [{
			"targets": [0],
			"searchable": false,
		},{
			"targets": [1],
			"searchable": false,
			"className": 'dt-body-left'
		}]
    });
}
  
function add_pack(id){
	$('#panel-modal').removeData('bs.modal');
	$('#panel-modal .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal .panel-body').load('<?php echo base_url('packing/packing_edit_a');?>'+'?dd='+id);
	$('#panel-modal .panel-title').html('<i class="fa fa-pencil-o"></i> Edit Packing');
	$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}
 
 function edit_detail(id){
	$('#panel-modal').removeData('bs.modal');
	$('#panel-modal .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal .panel-body').load('<?php echo base_url('packing/packing_edit_update');?>'+'?dd='+id);
	$('#panel-modal .panel-title').html('<i class="fa fa-pencil-o"></i> Edit Packing');
	$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}
  
function delete_detail(id){
	swal({
		title: 'Yakin akan Menghapus ?',
		text: 'data tidak dapat dikembalikan bila sudah dihapus !',
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Ya',
		cancelButtonText: 'Tidak'
    }).then(function () {
		var datapost={
			"id" : id
		};

      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>packing/delete_packing",
        data : JSON.stringify(datapost),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function(response) {
           $('.panel-heading button').trigger('click');
				listpacking();
				swal({
					title: 'Success!',
					text: response.message,
					type: 'success',
					showCancelButton: false,
					confirmButtonText: 'Ok'
				}).then(function () {
				})
			}
		});
    })
}

$(document).ready(function(){
	listpacking();
});
</script>