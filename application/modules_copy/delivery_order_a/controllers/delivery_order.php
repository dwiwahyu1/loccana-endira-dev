<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Delivery_order extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('delivery_order/do_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string)
	{
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index()
	{
		$this->template->load('maintemplate', 'delivery_order/views/index');
	}

	function lists()
	{
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'asc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

		$order_fields = array('no_do', 'no_pi');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->do_model->lists($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$no = 1;
		foreach ($list['data'] as $k => $v) {
			$no = $k + 1;
			if ($v['status'] != 2) {
				$actions = '<div class="btn-group">';
				$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit DO" onClick="delivery_order_edit(\'' . $v['id_do'] . '\')">';
				$actions .= '       <i class="fa fa-edit"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
				$actions .= '<div class="btn-group">';
				$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Detail DO" onClick="delivery_order_detail(\'' . $v['id_do'] . '\')">';
				$actions .= '       <i class="fa fa-search"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
				$actions .= '<div class="btn-group">';
				$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Edit Status" onClick="delivery_order_status(\'' . $v['id_do'] . '\')">';
				$actions .= '       <i class="fa fa-pencil"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
			} else {
				$actions = '<div class="btn-group">';
				$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit DO" onClick="delivery_order_edit(\'' . $v['id_do'] . '\')">';
				$actions .= '       <i class="fa fa-edit"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
				$actions .= '<div class="btn-group">';
				$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Detail DO" onClick="delivery_order_detail(\'' . $v['id_do'] . '\')">';
				$actions .= '       <i class="fa fa-search"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
			}

			if ($v['status'] == 0) {
				$status = '<div class="">';
				$status .=		'<span class="label label-primary">Proses</span>';
				$status .= '</div>';
			} elseif ($v['status'] == 1) {
				$status = '<div class="">';
				$status .=		'<span class="label label-warning">Dikirim</span>';
				$status .= '</div>';
			} else {
				$status = '<div class="">';
				$status .=		'<span class="label label-success">Diterima</span>';
				$status .= '</div>';
			}

			$actions .= '<div class="btn-group">';
			$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-default" type="button" data-toggle="tooltip" data-placement="top" title="Invoice" onClick="delivery_order_invoice(\'' . $v['id_do'] . '\')">';
			$actions .= '       <i class="fa fa-file-pdf-o"></i>';
			$actions .= '   </button>';
			$actions .= '</div>';
			$actions .= '<div class="btn-group">';
			$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Packing List" onClick="delivery_order_packing(\'' . $v['id_do'] . '\')">';
			$actions .= '       <i class="fa fa-file-pdf-o"></i>';
			$actions .= '   </button>';
			$actions .= '</div>';
			$actions .= '<div class="btn-group">';
			$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delivery Order" onClick="delivery_order_do(\'' . $v['id_do'] . '\')">';
			$actions .= '       <i class="fa fa-file-pdf-o"></i>';
			$actions .= '   </button>';
			$actions .= '</div>';
			array_push(
				$data,
				array(
					$no,
					$v['no_do'],
					$v['tanggal'],
					$v['detail_box'],
					$v['no_pi'],
					$v['remark'],
					$status,
					$actions
				)
			);
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function delivery_order_add()
	{
		$results = $this->do_model->delivery_order_nobc();

		$data = array(
			'pendaftaran' => $results
		);

		$this->load->view('delivery_order_add_view', $data);
	}

	public function delivery_order_detail($id_do)
	{
		$results = $this->do_model->delivery_order_detail($id_do);

		$data = array(
			'dp' => $results
		);

		$this->load->view('delivery_order_detail_view', $data);
	}

	public function delivery_order_edit($id_do)
	{
		$results = $this->do_model->delivery_order_detail($id_do);
		$pendaftaran = $this->do_model->delivery_order_nobc();

		$data = array(
			'do' => $results,
			'pendaftaran' => $pendaftaran
		);

		$this->load->view('delivery_order_edit_view', $data);
	}

	public function delivery_order_list_invoice($id_do)
	{
		$results = $this->do_model->delivery_list_invoice($id_do);
		
		$tanggal_terima = $results[0]['date_end'];
		$price_term = $results[0]['price_term'];

		$term = explode(" ", $price_term);
		$exp_date = $term[0];
		$due_date = date('Y-m-d', strtotime('+' . $exp_date . ' days', strtotime($tanggal_terima)));

		$data = array(
			'id_do' => $results[0]['id_do'],
			'invoice' => $results,
			'due_date' => $due_date
		);

		$this->load->view('delivery_order_invoice_view', $data);
	}
	
	public function delivery_order_list_packing($id_do)
	{
		$results = $this->do_model->delivery_list_packing($id_do);
		
		$tanggal_terima = $results[0]['date_end'];
		$price_term = $results[0]['price_term'];

		$term = explode(" ", $price_term);
		$exp_date = $term[0];
		$due_date = date('Y-m-d', strtotime('+' . $exp_date . ' days', strtotime($tanggal_terima)));

		$data = array(
			'id_do' => $results[0]['id_do'],
			'packing' => $results,
			'due_date' => $due_date
		);
		
		$this->load->view('delivery_order_packing_view', $data);
	}
	
	public function delivery_order_list_do($id_do)
	{
		$results = $this->do_model->delivery_list_do($id_do);
		
		$tanggal_terima = $results[0]['date_end'];
		$price_term = $results[0]['price_term'];

		$term = explode(" ", $price_term);
		$exp_date = $term[0];
		$due_date = date('Y-m-d', strtotime('+' . $exp_date . ' days', strtotime($tanggal_terima)));

		$totalQty 		= 0;
		$totalRemark	= 0;
		if(sizeof($results) > 0) {
			foreach ($results as $sk => $sv) {
				$totalQty 		= $sv['qty_array'] + $totalQty;
				$totalRemark	= $sv['qty_box'] + $totalRemark;
			}
		}

		$data = array(
			'id_do'			=> $results[0]['id_do'],
			'do'			=> $results,
			'due_date'		=> $due_date,
			'totalQty'		=> $totalQty,
			'totalRemark'	=> $totalRemark
		);
		// echo "<pre>";print_r($data);echo "</pre>";die;
		
		$this->load->view('delivery_order_do_view', $data);
	}

	public function delivery_order_edit_item()
	{
		$results =  $this->do_model->delivery_list_stbj();

		$data = array(
			'stbj' => $results
		);

		$this->load->view('delivery_order_edit_item_view', $data);
	}

	public function add_edit_item()
	{
		$result_spb = $this->purchase_order_model->spb();

		$data = array(
			'spb' => $result_spb
		);

		$this->load->view('edit_modal_item_view', $data);
	}

	public function delivery_order_add_item()
	{
		$results = $this->do_model->delivery_list_stbj();

		$data = array(
			'stbj' => $results
		);

		$this->load->view('delivery_order_add_item_view', $data);
	}

	public function delivery_stbj_search_id()
	{
		$data		= file_get_contents("php://input");
		$params		= json_decode($data, true);

		$stbj	= explode(",", $params['stbj']);
		$get_no_stbj = explode(" - ", $stbj[0]);

		$var = array(
			'no_stbj' => $get_no_stbj[1],
			'id_po_quot' => $stbj[1]
		);

		$list		= $this->do_model->delivery_stbj_search_id($var);

		$data = array();
		$tempObj = new stdClass();

		if (sizeof($params['tempSTBJ']) > 0) {
			foreach ($params['tempSTBJ'] as $p => $pk) {
				for ($i = 0; $i <= sizeof($list); $i++) {
					if (!empty($list[$i])) {
						if ($list[$i]['no_stbj'] == $pk[1] && $list[$i]['no_stbj'] == $pk[2]) {
							unset($list[$i]);
						}
					}
				}
			}
		}

		$i = 0;
		foreach ($list as $k => $v) {

			$strOption =
				'<div class="checkbox">' .
				'<input id="option[' . $i . ']" type="checkbox" value="' . $i . '">' .
				'<label for="option[' . $i . ']"></label>' .
				'</div>';

			array_push($data, array(
				$v['id_stbj'],
				$v['no_stbj'],
				$v['tanggal_stbj'],
				$v['detail_box'],
				$v['no_pi'],
				$v['remark'],
				$strOption
			));
			$i++;
		}

		$res = array(
			'status'	=> 'success',
			'data'		=> $data
		);

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function detail_delivery_order($id_do)
	{
		$list = $this->do_model->delivery_list_invoice($id_do);

		$data = array();
		$i = 0;
		$no = 0;

		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list as $k => $v) {

			$actions = '<div class="btn-group">';
			$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Hapus" onClick="delivery_detail_order_delete(\'' . $v['id_packing'] . '\')">';
			$actions .= '       <i class="fa fa-trash"></i>';
			$actions .= '   </button>';
			$actions .= '</div>';

			array_push($data, array(
				$no = $k + 1,
				$v['no_stbj'],
				$v['tanggal_stbj'],
				$v['db_stbj'],
				$v['no_pi_stbj'],
				$v['remark_stbj'],
				//$actions
			));
			$i++;
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function detail_delivery_order_packing($id_do)
	{
		$data = array();
		$list = $this->do_model->delivery_list_packing($id_do);
		
		$i = 0;
		$no = 0;
		$total_qty = 0;
		$total_net = 0;
		$total_gross = 0;
		
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list as $k => $v) {

			$actions = '<div class="btn-group">';
			$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Hapus" onClick="delivery_detail_order_delete(\'' . $v['id_packing'] . '\')">';
			$actions .= '       <i class="fa fa-trash"></i>';
			$actions .= '   </button>';
			$actions .= '</div>';
			
			$ctnNo = '<input type="text" class="form-control" id="no_kontainer'.$i.'" name="no_kontainer'.$i.'" onInput="numberCNT('.$i.')" style="width: 100%;" value="" placeholder="Silahkan Input"><label style="display: none;" id="lb_no_kontainer'.$i.'"></label>';
			
			$total_qty = $total_qty + (int)$v['qty_array'];
			$total_net = $total_net + (int)$v['net_weight'];
			$total_gross = $total_gross + (int)$v['gross_weight'];
			
			array_push($data, array(
				$i+1,
				$ctnNo,
				$v['stock_code'],
				$v['part_no'],
				number_format($v['qty_array'],2),
				number_format($v['net_weight'],2),
				number_format($v['gross_weight'],2),
				$total_qty,
				$total_net,
				$total_gross
			));
			$i++;
		}
		// echo "<pre>";print_r($data);echo "</pre>";die;
		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function detail_delivery_order_do($id_do)
	{
		$data = array();
		$list = $this->do_model->delivery_list_do($id_do);
		// echo "<pre>";print_r($list);echo "</pre>";die;
		$i = 0;
		$no = 0;
		$total_qty = 0;
		$total_net = 0;
		$total_gross = 0;
		
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list as $k => $v) {
			
			$ctnNo = '<input type="text" class="form-control" id="no_kontainer'.$i.'" name="no_kontainer'.$i.'" onInput="numberCNT('.$i.')" style="width: 100%;" value="">';
			
			$total_qty = $total_qty + (int)$v['qty_array'];
			$total_net = $total_net + (int)$v['net_weight'];
			$total_gross = $total_gross + (int)$v['gross_weight'];
			
			array_push($data, array(
				$i+1,
				$v['part_no'],
				$v['no_po'],
				number_format($v['qty_array'], 0, ',', '.'),
				number_format($v['qty_box'], 0, ',', '.').'&emsp;&emsp;&emsp;&emsp;'.number_format($v['qty_box'], 0, ',', '.').'&emsp;&emsp;@&emsp;&emsp;'.number_format($v['qty_per_box'], 0, ',', '.')
				// $v['remark_stbj']
			));
			$i++;
		}
		// echo "<pre>";print_r($data);echo "</pre>";die;
		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function delivery_detail_stbj($id_do)
	{
		$list = $this->do_model->delivery_order_detail($id_do);

		$data = array();
		$i = 0;

		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list as $k => $v) {

			$actions = '<div class="btn-group">';
			$actions .=   	'<button class="btn btn-icon waves-effect waves-light btn-danger">';
			$actions .=			'<i class="fa fa-trash"></i>';
			$actions .=		'</button>';
			$actions .= '</div>';

			array_push($data, array(
				$no = $k + 1,
				$v['id_do'],
				$v['id_stbj'],
				$v['no_stbj'],
				$v['tanggal_stbj'],
				$v['db_stbj'],
				$v['no_pi_stbj'],
				$v['remark_stbj'],
				$actions
			));
			$i++;
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function insert_delivery_order()
	{
		$no_do			= $this->Anti_sql_injection($this->input->post('no_do', TRUE));
		$tanggal_do		= $this->Anti_sql_injection($this->input->post('tanggal_do', TRUE));
		//$detail_box		= $this->Anti_sql_injection($this->input->post('detail_box', TRUE));
		$no_pi 			= $this->Anti_sql_injection($this->input->post('no_pi', TRUE));
		$remark 		= $this->Anti_sql_injection($this->input->post('remark', TRUE));
		$no_bc 			= $this->Anti_sql_injection($this->input->post('no_bc', TRUE));

		$data = array(
			'no_do'			=> $no_do,
			'tanggal_do'	=> $tanggal_do,
			//'detail_box'	=> $detail_box,
			'no_pi'			=> $no_pi,
			'remark'		=> $remark,
			'id_schedule'	=> 0,
			'status'		=> 0,
			'no_bc'			=> $no_bc
		);

		$results = $this->do_model->insert_delivery_order($data);

		if ($results > 0) {
			$msg = 'Berhasil menambahkan data Delivery Order';

			$results = array(
				'success' => true,
				'message' => $msg,
				'lastid' => $results['lastid']
			);

			$this->log_activity->insert_activity('insert', $msg . ' dengan No Delivery ' . $data['no_do']);
		} else {
			$msg = 'Gagal menambahkan Delivery Order ke database';

			$results = array(
				'success' => false,
				'message' => $msg
			);

			$this->log_activity->insert_activity('insert', $msg . ' dengan No Delivery ' . $data['no_do']);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}

	public function insert_detail_delivery_order()
	{
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		foreach ($params['liststbj'] as $k => $v) {
			$arrTemp = array(
				'id_do'		=> $params['id_do'],
				'id_stbj'	=> $params['liststbj'][$k]['0'],
			);

			$results = $this->do_model->insert_detail_stbj($arrTemp);
		}

		if ($results > 0) {
			$msg = 'Berhasil menambahkan detail Delivery Order';

			$results = array(
				'success' => true,
				'message' => $msg
			);

			$this->log_activity->insert_activity('insert', $msg . ' dengan ID DO ' . $arrTemp['id_do']);
		} else {
			$msg = 'Gagal menambahkan detail Delivery Order ke database';

			$results = array(
				'success' => false,
				'message' => $msg
			);

			$this->log_activity->insert_activity('insert', $msg . ' dengan ID DO ' . $arrTemp['id_do']);
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($results);
	}

	public function edit_delivery_order()
	{
		$this->form_validation->set_rules('id_do', 'Delivery Order', 'trim|required');
		$this->form_validation->set_rules('no_do', 'No Delivery Order', 'trim|required|min_length[4]|max_length[100]');
		$this->form_validation->set_rules('tanggal_do', 'Tanggal PO', 'trim|required');
		$this->form_validation->set_rules('detail_box', 'Detail Box', 'trim|required');
		$this->form_validation->set_rules('no_pi', 'No PI', 'trim|required');
		$this->form_validation->set_rules('remark', 'Remark', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			$id_do 			= $this->Anti_sql_injection($this->input->post('id_do', TRUE));
			$no_do 			= $this->Anti_sql_injection($this->input->post('no_do', TRUE));
			$tanggal_do		= $this->Anti_sql_injection($this->input->post('tanggal_do', TRUE));
			$detail_box		= $this->Anti_sql_injection($this->input->post('detail_box', TRUE));
			$no_pi 			= $this->Anti_sql_injection($this->input->post('no_pi', TRUE));
			$remark 		= $this->Anti_sql_injection($this->input->post('remark', TRUE));
			$no_bc 			= $this->Anti_sql_injection($this->input->post('no_bc', TRUE));

			$data = array(
				'id_do'			=> $id_do,
				'no_do'			=> $no_do,
				'tanggal_do'	=> $tanggal_do,
				'detail_box'	=> $detail_box,
				'no_pi'			=> $no_pi,
				'remark'		=> $remark,
				'id_schedule'	=> 0,
				'status'		=> 0,
				'no_bc'			=> $no_bc
			);

			$get_do = $this->do_model->do_list_stbj($id_do);

			if ($get_do[0]['tanggal'] != $tanggal_do || $get_do[0]['detail_box'] != $detail_box || $get_do[0]['no_pi'] != $no_pi || $get_do[0]['remark'] != $remark || $get_do[0]['no_bc'] != $no_bc) {
				$result = $this->do_model->insert_delivery_order_edit($data);

				$msg = 'Berhasil merubah data Delivery Order';

				$result = array(
					'success'	=> true,
					'message'	=> $msg,
					'no_do'		=> $no_do
				);

				$this->log_activity->insert_activity('update', $msg . ' dengan No Delivery ' . $no_do);
			} else {
				$msg = 'Gagal merubah data Delivery Order ke database';

				$result = array(
					'success' => false,
					'message' => $msg
				);

				$this->log_activity->insert_activity('update', $msg . ' dengan No Delivery ' . $no_do);
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}


	public function edit_detail_delivery_order()
	{
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data, true);

		$arrData = array();
		$statSave = false;
		foreach ($params['liststbj'] as $k => $v) {
			$this->db->delete('t_do_packing', array('id_do' => $params['id_do']));
			if ($v[0] != '') {

				$arrTemp = array(
					'id_do'		=> $params['id_do'],
					'id_stbj'	=> $v[2]
				);

				$results = $this->do_model->insert_detail_stbj($arrTemp);
				if ($results > 0) $statSave = true;
				else $statSave = false;
			}
		}

		if ($statSave == true) {
			$msg = 'Berhasil merubah detail Delivery Order';

			$result = array(
				'success'	=> true,
				'message'	=> $msg
			);

			$this->log_activity->insert_activity('update', $msg . ' dengan ID DO ' . $params['id_do']);
		} else {
			$msg = 'Gagal merubah detail Delivery Order ke database';

			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->log_activity->insert_activity('update', $msg . ' dengan ID DO ' . $params['id_do']);
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result);
	}

	public function delivery_list_invoice($id_do)
	{
		$list = $this->do_model->delivery_list_invoice($id_do);

		$data = array();
		$i = 0;
		$no = 0;
		$amount = 0;

		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list as $k => $v) {
			$amount = $v['qty_array'] * $v['price_order'];
			$qtyarray = explode(".", $v['qty_array']);
			$qty_array = $qtyarray[0];
			$qty_arrayComa = $qtyarray[1];

			$priceorder = explode(".", $v['price_order']);
			$price_order = $priceorder[0];
			$price_orderComa = $priceorder[1];

			array_push($data, array(
				$v['no_do'],
				$v['part_no'],
				$v['no_po'],
				number_format($qty_array, 0, '.', ',') . '.' . $qty_arrayComa,
				number_format($price_order, 0, '.', ',') . '.' . $price_orderComa,
				number_format($amount, 4, '.', ',')
				//$actions
			));
			$i++;
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function delivery_list_packing($id_do) 
	{
		$list = $this->do_model->delivery_list_packing($id_do);
		var_dump($list);die;
		$data = array();
		$i = 0;
		$no = 0;
		$amount = 0;
		
		$username = $this->session->userdata['logged_in']['username'];
		
		foreach ($list as $k => $v) {
			$cntNo = '<input type="text" class="form-control" id="no_kontainer'.$i.'" name="no_kontainer'.$i.'" value="">';
			
			array_push($data, array(
				$cntNo,
				$v['stock_code'],
				$v['stock_name'],
				$v['qty_array'],
				$v['net_weight'],
				$v['gross_weight']
			));
			$i++;
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function delivery_order_update_status()
	{
		$data 	= file_get_contents("php://input");
		$params = json_decode($data, true);

		if ($params['status'] == 2) {
			$list = $this->do_model->delivery_update_status($params);
			$detail_order = $this->do_model->delivery_order_detail($params['id_do']);

			if (!empty($detail_order)) {
				$get_coa = $this->do_model->delivery_get_id_coa($detail_order[0]['cust_id']);
				$get_value = $this->do_model->delivery_get_value_piutang($params['id_do']);

				$data_coa = array(
					'id_coa' 		=> $get_coa[0]['id_coa'],
					'id_parent' 	=> 0,
					'date' 			=> date('Y-m-d'),
					'date_insert' 	=> date('Y-m-d H:i:s'),
					'id_valas' 		=> $detail_order[0]['valas_id'],
					'value' 		=> $get_value[0]['value'],
					'adjustment' 	=> 0,
					'type_cash' 	=> 0,
					'note' 			=> 'Piutang PO ' . $detail_order[0]['no_po'],
					'value_real' 	=> $get_value[0]['value_real'],
					'rate' 			=> $get_value[0]['rate'],
					'bukti' 		=> NULL
				);

				$statInsert_coa = false;
				$insert_coa = $this->do_model->delivery_order_insert_coa_values($data_coa);
				if ($insert_coa['code'] == 1) {
					$statInsert_coa = true;
					$data_hp = array(
						'tanggal' 		=> $data_coa['date'],
						'ref' 			=> NULL,
						'source' 		=> NULL,
						'keterangan' 	=> $data_coa['note'],
						'status' 		=> 0,
						'date_insert' 	=> $data_coa['date_insert'],
						'saldo' 		=> $data_coa['value'],
						'saldo_akhir' 	=> 0,
						'id_valas' 		=> $data_coa['id_valas'],
						'type_kartu' 	=> 1,
						'id_master' 	=> $params['id_do'],
						'type_master' 	=> 1
					);

					$statInsert_hp = false;
					$insert_hp = $this->do_model->delivery_order_insert_kartu_hp($data_hp);
					if ($insert_hp > 0) $statInsert_hp = true;
				}

				if ($statInsert_coa == true && $statInsert_hp == true) {
					$msg = 'Data status Delivery Order berhasil di ubah';

					$results = array('success' => true, 'status' => 'success', 'message' => $msg);
					$this->log_activity->insert_activity('update', $msg . ' dengan No Delivery ' . $detail_order[0]['no_do']);
				} else {
					$msg = 'Data status Delivery Order gagal di ubah';

					$results = array('success' => false, 'status' => 'success', 'message' => $msg);
					$this->log_activity->insert_activity('update', $msg . ' dengan No Delivery ' . $detail_order[0]['no_do']);
				}
			} else {
				$msg = 'Data status COA Value dan Kartu HP gagal disimpan ke database';

				$results = array('success' => false, 'status' => 'error', 'message' => $msg);
				$this->log_activity->insert_activity('update', $msg . ' dengan No Delivery ' . $detail_order[0]['no_do']);
			}
		} else if ($params['status'] == 1) {
			$list = $this->do_model->delivery_update_status($params);
			$msg = 'Data status Delivery Order berhasil di ubah';

			$results = array('success' => true, 'status' => 'success', 'message' => $msg);
			$this->log_activity->insert_activity('update', $msg . ' dengan ID Delivery ' . $params['id_do']);
		} else {
			$msg = 'Data status Delivery Order belum di ubah';

			$results = array('success' => false, 'status' => 'success', 'message' => $msg);
			$this->log_activity->insert_activity('update', $msg . ' dengan ID Delivery ' . $params['id_do']);
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($results);
	}

	public function delivery_detail_order_delete()
	{

		$data 	= file_get_contents("php://input");
		$params = json_decode($data, true);

		$list = $this->do_model->delivery_detail_order_delete($params);

		$msg = 'Data detail delivery order berhasil di hapus';

		$results = array(
			'success' => true,
			'status' => 'success',
			'message' => $msg
		);

		$this->log_activity->insert_activity('delete', $msg . ' dengan ID DO ' . $params['id_do']);

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($results);
	}

	public function check_no_do()
	{
		$this->form_validation->set_rules('no_do', 'NoDo', 'trim|required|min_length[4]|max_length[20]|is_unique[t_delivery_order.no_do]');
		$this->form_validation->set_message('is_unique', 'No DO Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'No DO Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}
}
