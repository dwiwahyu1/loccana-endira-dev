	<style>
		.col-customer {
			border: solid 1px #b2b8b7;
		}

		.dt-body-left {
			text-align: left;
			vertical-align: middle;
		}

		.dt-body-right {
			text-align: right;
			vertical-align: middle;
		}

		.dt-body-center {
			text-align: center;
			vertical-align: middle;
		}

		img {
			max-width: 85%;
			height: auto;
		}

		.nama-perusahaan-margin {
			margin-top: 30px;

		}

		.margin-ttd {
			margin-bottom: 65px
		}
	</style>

	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download Invoice" id="btn_download">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="col-md-2 logo-place">
				<img src="<?php echo site_url(); ?>assets/images/logo-celebit.jpg" alt="logo-celebit">
			</div>
			<div class="col-md-10">
				<div class="col-md-12 titleReport">
					<h1 id="titleCelebit">CELEBIT</h1>
					<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
					<h4 id="titleAlamat">BANDUNG FACTORY : JL.BUAH DUA RT.01/RW.04 RANCAEKEK - BANDUNG-INDONESIA</h4>
					<h4 id="titleTlp">TEL 62-22-7798 561/7798 542, FAX : 62-22-7798 562 E-MAIL : invoicing@celebit.com</h4>
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-12 text-center">
			<h2 id="titleInvoice" style="font-weight:900;">INVOICE</h2>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="col-md-8 col-customer">
				<div class="row">
					<div class="col-md-12">
						<label class="control-label" id="namaCustomer"><?php if (isset($invoice[0]['name_eksternal'])) {
																															echo $invoice[0]['name_eksternal'];
																														} ?></label><br>
						<label class="control-label" id="lokasiCustomer"><?php if (isset($invoice[0]['eksternal_address'])) {
																																echo $invoice[0]['eksternal_address'];
																															} ?></label><br>
						<label class="control-label" id="alamatCustomer">
							<?php if (isset($invoice[0]['phone_1']) || isset($invoice[0]['phone_2']) || isset($invoice[0]['fax']) || isset($invoice[0]['email'])) {
								echo 'Telp : ' . $invoice[0]['phone_1'] . ' / ' . $invoice[0]['phone_2'] . ' FAX : ' . $invoice[0]['fax'] . ' E-MAIL : ' . $invoice[0]['email'];
							} ?>
						</label><br>
						<label class="control-label" id="divCustomer"><?php if (isset($invoice[0]['eksternal_loc'])) {
																														echo $invoice[0]['eksternal_loc'];
																													} ?></label><br>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-3" id="noInvoice1">No.</label>
						<label class="control-label col-md-1" id="noInvoice2">:</label>
						<input id="noInvoice3"></input>
						<!-- <label class="control-label col-md-8"  id="noInvoice3"><?php if (isset($invoice[0]['no_po'])) {
																																					echo $invoice[0]['no_po'];
																																				} ?></label> -->
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-3" id="tanggalInvoice1">Date</label>
						<label class="control-label col-md-1" id="tanggalInvoice2">:</label>
						<label class="control-label col-md-8" id="tanggalInvoice3">
							<?php if (isset($invoice[0]['tanggal_do'])) {
								$tanggal_do = date_create($invoice[0]['tanggal_do']);
								echo date_format($tanggal_do, "d F Y");
							}
							?>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-3" id="dueDateInvoice1">Due Date</label>
						<label class="control-label col-md-1" id="dueDateInvoice2">:</label>
						<label class="control-label col-md-8" id="dueDateInvoice3">
							<?php if (isset($due_date)) {
								$due_dates = date_create($due_date);
								echo date_format($due_dates, "d F Y");
							}
							?>
						</label>
					</div>
				</div>

			</div>
		</div>
	</div>

	<hr>
	<div class="row">
		<div class="col-md-12">
			<table id="listinvoice" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th rowspan="2">No DO</th>
						<th colspan="2" style="text-align:center">Description</th>
						<th rowspan="2">Qty (Pcs)</th>
						<th rowspan="2">Unit Price (<?php if (isset($invoice[0]['symbol_valas'])) {
																					echo $invoice[0]['symbol_valas'];
																				} ?>)</th>
						<th rowspan="2">Amount (<?php if (isset($invoice[0]['symbol_valas'])) {
																			echo $invoice[0]['symbol_valas'];
																		} ?>)</th>
					</tr>
					<tr>
						<th>Part No</th>
						<th>PO</th>
					</tr>
				</thead>
				<tbody></tbody>
				<tr style="">
					<td></td>
					<td style="text-align: left; ">
					<br>
					<br>
						TOTAL
						<br>PPN 10%
						<br>Bank Transfer :
						<br>BANK : BANK NEGARA INDONESIA (BNI)
						<br>Alamat : Jl. Buah Batu No 189 D Bandung Telp 022-7313371
						<br>No Rekening (Rp) : 77-1688-1688 (Rp)
						<br>No Rekening (USD) : 1688-1688-72(USD)
						<br>Account Name : PT CELEBIT CIRCUIT TECHNOLOGY INDONESIA
						<br>
						</td>
					<td></td>
					<td></td>
					<td></td>
					<td class="pull-right">
						<label id="total-label" name="ppn-label" />
						<br><label id="ppn-label" name="ppn-label" />
					</td>
				</tr>
				<tfoot>
					<tr>
						<th colspan="4" style="text-align: left;">TOTAL <?php if (isset($invoice[0]['symbol_valas'])) {
																															echo $invoice[0]['symbol_valas'];
																														} ?></th>
						<td id="rupiah">Rp.</td>
						<th id="tdAmount_Total"></th>
					</tr>
					<tr>
						<th colspan="6" id="totalBilangan" name="totalBilangan"></th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
	<div class="row nama-perusahaan-margin">
		<div class="col-md-4 col-md-offset-8">
			<div class="col-md-12 text-center">
				<label class="control-label margin-ttd">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</label>
				<input id="penandaTangan"></input>
				<!-- <label class="control-label col-md-8"  id="noInvoice3"><?php if (isset($invoice[0]['no_po'])) {
																																			echo $invoice[0]['no_po'];
																																		} ?></label> -->
			</div>
		</div>
	</div>
	<div class="row">
		<input type="hidden" id="totHide" name="totHide">
	</div>

	<script type="text/javascript">
		var dataImage = null;
		$(document).ready(function() {
			dtInvoice();

			$('#btn_download').click(function() {
				var doc = new jsPDF('p', 'mm', 'letter');
				var imgData = dataImage;
				var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
				var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

				// FOOTER
				doc.setTextColor(100);
				doc.addImage(imgData, 'JPEG', 5, 5, 25, 25)
				doc.setFontSize(12);
				doc.text($('#titleCelebit').html(), 33, 10, 'left');
				doc.setFontSize(10);
				doc.text($('#titlePerusahaan').html(), 33, 15, 'left');
				doc.setFontSize(11);
				doc.text($('#titleAlamat').html(), 33, 20, 'left');
				doc.setFontSize(11);
				doc.text($('#titleTlp').html(), 33, 25, 'left');

				doc.setDrawColor(116, 119, 122);
				doc.setLineWidth(0.1);
				doc.line(4, 30, 500, 30);

				doc.setFontSize(11);
				doc.text($('#titleInvoice').html(), pageWidth / 2, 40, 'center');

				doc.setDrawColor(116, 119, 122);
				doc.setLineWidth(0.2);
				doc.line(4, 45, 110, 45);
				doc.setLineWidth(0.2);
				doc.line(4, 45, 4, 65);
				doc.setLineWidth(0.2);
				doc.line(110, 45, 110, 65);
				doc.setLineWidth(0.2);
				doc.line(4, 65, 110, 65);

				doc.setFontSize(7);
				doc.text($('#namaCustomer').html(), 6, 50);
				doc.text($('#lokasiCustomer').html(), 6, 55);
				doc.text($('#alamatCustomer').html(), 6, 60);
				doc.text($('#divCustomer').html(), 6, 65);

				doc.setFontSize(7);
				doc.text($('#noInvoice1').html(), 115, 50, 'left');
				doc.text($('#noInvoice2').html(), 128, 50, 'left');
				doc.text($('#noInvoice3').val(), 135, 50, 'left'); //dari inputan

				doc.text($('#tanggalInvoice1').html(), 115, 57, 'left');
				doc.text($('#tanggalInvoice2').html(), 128, 57, 'left');
				doc.text($('#tanggalInvoice3').html(), 96, 54, 'left');

				doc.text($('#dueDateInvoice1').html(), 115, 64, 'left');
				doc.text($('#dueDateInvoice2').html(), 128, 64, 'left');
				doc.text($('#dueDateInvoice3').html(), 96, 61, 'left');

				doc.autoTable({
					html: '#listinvoice',
					theme: 'plain',
					styles: {
						fontSize: 8,
						lineColor: [116, 119, 122],
						lineWidth: 0.1,
						cellWidth: 'auto',

					},
					margin: 4.5,
					tableWidth: (pageWidth - 10),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					didParseCell: function(data) {
						if (data.table.foot[0]) {
							if (data.table.foot[0].cells[4]) {
								data.table.foot[0].cells[4].styles.halign = 'right';
							}
							if (data.table.foot[0].cells[5]) {
								data.table.foot[0].cells[5].styles.halign = 'right';
							}
						}
					},
					columnStyles: {
						0: {
							tableWidth: 10,
							halign: 'center'
						},
						1: {
							tableWidth: 10,
							halign: 'center'
						},
						2: {
							halign: 'center'
						},
						3: {
							halign: 'right'
						},
						4: {
							halign: 'right'
						},
						5: {
							halign: 'right',
							falign: 'left'
						},
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 70
				});

				var x = pageWidth * 70 / 100;
				var y = pageHeight * 85 / 100;
				doc.setFontSize(10);
				doc.text("PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA ", x, y, 'center');
				doc.text($('#penandaTangan').val(), x, y + 30, 'center');
				doc.save('INVOICE <?php echo $invoice[0]['tanggal_do']; ?>.pdf');
			});
		});

		function toDataURL(url, callback) {
			var xhr = new XMLHttpRequest();
			xhr.onload = function() {
				var reader = new FileReader();
				reader.onloadend = function() {
					callback(reader.result);
				}
				reader.readAsDataURL(xhr.response);
			};
			xhr.open('GET', url);
			xhr.responseType = 'blob';
			xhr.send();
		}

		toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
			//console.log('RESULT:', dataUrl)
			dataImage = dataUrl;
		})

		function dtInvoice() {
			$('#listinvoice').DataTable({
				"processing": true,
				"searching": false,
				"responsive": true,
				"lengthChange": false,
				"info": false,
				"bSort": false,
				"ajax": {
					"type": "GET",
					"url": "<?php echo base_url() . 'delivery_order/delivery_list_invoice/' . $id_do; ?>"
				},
				"columnDefs": [{
					"targets": [0],
					"searchable": false,
					"className": 'dt-body-left',
					"width": 40
				}, {
					"targets": [1],
					"searchable": false,
					"className": 'dt-body-center',
					"width": 80
				}, {
					"targets": [2],
					"searchable": false,
					"className": 'dt-body-center',
					"width": 80
				}, {
					"targets": [3],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 60
				}, {
					"targets": [4],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 40
				}, {
					"targets": [5],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 40
				}],
				"footerCallback": function(row, data, start, end, display) {
					var api = this.api(),
						data;
					var total = 0;
					// Remove the formatting to get integer data for summation
					var intVal = function(i) {
						return typeof i === 'string' ?
							i.replace(/[\$,]/g, '') * 1 :
							typeof i === 'number' ?
							i : 0;
					};

					// Total over all pages
					total = api
						.column(5)
						.data()
						.reduce(function(a, b) {
							total = total + intVal(a) + intVal(b);
							return (intVal(a) + intVal(b)) *(110/100);
						}, 0);

					// Total over this page
					pageTotal = api
						.column(5, {
							page: 'current'
						})
						.data()
						.reduce(function(a, b) {
							total = total + intVal(a) + intVal(b);
							return (intVal(a) + intVal(b)) ;
						}, 0);

					// Update footer
					$(api.column(5).footer()).html(
						'<?php $invoice[0]['symbol_valas']; ?>' + formatCurrencyComa(pageTotal*110/100)
					)
					$("#total-label").text(formatCurrencyComa(pageTotal),2,",",".");
					$("#ppn-label").text(formatCurrencyComa(pageTotal * 10 / 100),2,",",".");

					$('#totHide').val(pageTotal*110/100);
					<?php if ($invoice[0]['valas_id'] == 1) { ?>
						terbilangIND();
					<?php } else { ?>
						terbilangENG();
					<?php } ?>
				}
			});
		}

		function terbilangIND() {
			var bilangan = document.getElementById("totHide").value;
			var kalimat = "";
			var angka = new Array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
			var kata = new Array('', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan');
			var tingkat = new Array('', 'Ribu', 'Juta', 'Milyar', 'Triliun');
			var panjang_bilangan = bilangan.length;

			/* pengujian panjang bilangan */
			if (panjang_bilangan > 15) {
				kalimat = "Diluar Batas";
			} else {
				/* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
				for (i = 1; i <= panjang_bilangan; i++) {
					angka[i] = bilangan.substr(-(i), 1);
				}

				var i = 1;
				var j = 0;

				/* mulai proses iterasi terhadap array angka */
				while (i <= panjang_bilangan) {
					subkalimat = "";
					kata1 = "";
					kata2 = "";
					kata3 = "";

					/* untuk Ratusan */
					if (angka[i + 2] != "0") {
						if (angka[i + 2] == "1") {
							kata1 = "Seratus";
						} else {
							kata1 = kata[angka[i + 2]] + " Ratus";
						}
					}

					/* untuk Puluhan atau Belasan */
					if (angka[i + 1] != "0") {
						if (angka[i + 1] == "1") {
							if (angka[i] == "0") {
								kata2 = "Sepuluh";
							} else if (angka[i] == "1") {
								kata2 = "Sebelas";
							} else {
								kata2 = kata[angka[i]] + " Belas";
							}
						} else {
							kata2 = kata[angka[i + 1]] + " Puluh";
						}
					}

					/* untuk Satuan */
					if (angka[i] != "0") {
						if (angka[i + 1] != "1") {
							kata3 = kata[angka[i]];
						}
					}

					/* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
					if ((angka[i] != "0") || (angka[i + 1] != "0") || (angka[i + 2] != "0")) {
						subkalimat = kata1 + " " + kata2 + " " + kata3 + " " + tingkat[j] + " ";
					}

					/* gabungkan variabe sub kalimat (untuk Satu blok 3 angka) ke variabel kalimat */
					kalimat = subkalimat + kalimat;
					i = i + 3;
					j = j + 1;
				}

				/* mengganti Satu Ribu jadi Seribu jika diperlukan */
				if ((angka[5] == "0") && (angka[6] == "0")) {
					kalimat = kalimat.replace("Satu Ribu", "Seribu");
				}
			}

			document.getElementById("totalBilangan").innerHTML = kalimat;
		}

		function terbilangENG() {
			var bilangan = document.getElementById("totHide").value;
			var kalimat = "";
			var angka = new Array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
			var kata = new Array('', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine');
			var kataBelas = new Array('', 'One', 'Twelve', 'Thir', 'Four', 'Fif', 'Six', 'Seven', 'Eigh', 'Nine');
			var Puluh = new Array('', 'One', 'Twen', 'Thir', 'For', 'Fif', 'Six', 'Seven', 'Eigh', 'Nine');
			var tingkat = new Array('', 'Thousand', 'Million', 'Billion', 'Trillion');
			var panjang_bilangan = bilangan.length;

			/* pengujian panjang bilangan */
			if (panjang_bilangan > 15) {
				kalimat = "Diluar Batas";
			} else {
				/* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
				for (i = 1; i <= panjang_bilangan; i++) {
					angka[i] = bilangan.substr(-(i), 1);
				}

				var i = 1;
				var j = 0;

				/* mulai proses iterasi terhadap array angka */
				while (i <= panjang_bilangan) {
					subkalimat = "";
					kata1 = "";
					kata2 = "";
					kata3 = "";

					/* untuk Ratusan */
					if (angka[i + 2] != "0") {
						if (angka[i + 2] == "1") {
							kata1 = "Hundred";
						} else {
							kata1 = kata[angka[i + 2]] + " Hundred";
						}
					}

					/* untuk Puluhan atau Belasan */
					if (angka[i + 1] != "0") {
						if (angka[i + 1] == "1") {
							if (angka[i] == "0") {
								kata2 = "Ten";
							} else if (angka[i] == "1") {
								kata2 = "Eleven";
							} else if (angka[i] == "2") {
								kata2 = "Twelve";
							} else {
								kata2 = kataBelas[angka[i]] + "teen";
							}
						} else {
							kata2 = kataPuluh[angka[i + 1]] + "ty";
						}
					}

					/* untuk Satuan */
					if (angka[i] != "0") {
						if (angka[i + 1] != "1") {
							kata3 = kata[angka[i]];
						}
					}

					/* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
					if ((angka[i] != "0") || (angka[i + 1] != "0") || (angka[i + 2] != "0")) {
						subkalimat = kata1 + " " + kata2 + " " + kata3 + " " + tingkat[j] + " ";
					}

					/* gabungkan variabe sub kalimat (untuk Satu blok 3 angka) ke variabel kalimat */
					kalimat = subkalimat + kalimat;
					i = i + 3;
					j = j + 1;
				}

				/* mengganti Satu Ribu jadi Seribu jika diperlukan */
				if ((angka[5] == "0") && (angka[6] == "0")) {
					kalimat = kalimat.replace("One Thousand", "Thousand");
				}
			}

			document.getElementById("totalBilangan").innerHTML = kalimat;
		}
	</script>