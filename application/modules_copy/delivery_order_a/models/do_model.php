<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Do_model extends CI_Model {
	
	public function __construct() {
		
		parent::__construct();
		
	}

	public function lists($params = array())
	{
		$sql 	= 'CALL do_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();
	
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);
		
		return $return;
	}

	public function delivery_list_stbj()
	{
		$sql 	= 'CALL do_packing_list_not_same()';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function delivery_stbj_search_id($data) {
		$sql 	= 'CALL packing_list2(?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['no_stbj'],
				$data['id_po_quot']
			));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function delivery_list_invoice($id_do) {
		$sql 	= 'CALL do_detail_list(?)';
		
		$query 	=  $this->db->query($sql,array(
			$id_do
		));

		$result	= $query->result_array();
		
		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function delivery_list_packing($id_do) {
		$sql 	= 'CALL do_detail_list2(?)';
		
		$query 	=  $this->db->query($sql,array(
			$id_do
		));

		$result	= $query->result_array();
		
		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function delivery_list_do($id_do) {
		$sql 	= 'CALL do_detail_list2(?)';
		
		$query 	=  $this->db->query($sql,array(
			$id_do
		));

		$result	= $query->result_array();
		
		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function insert_delivery_order($data)
	{
		$sql 	= 'CALL do_add(?,?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['no_do'],
			$data['tanggal_do'],
			$data['detail_box'],
			$data['no_pi'],
			$data['remark'],
			$data['id_schedule'],
			$data['status'],
			$data['no_bc']
		));
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function insert_detail_stbj($data)
	{	
		$sql 	= 'CALL do_pack_add(?,?)';

		$query 	= $this->db->query($sql,
			array(
				$data['id_do'],
				$data['id_stbj']
		));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function do_list_stbj($id_do)
	{
		$sql 	= 'CALL do_search_id(?)';

		$query 	= $this->db->query($sql,
			array(
				$id_do
			));
			
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function insert_delivery_order_edit($data) {
		$sql 	= 'CALL do_update(?,?,?,?,?,?,?,?,?)';
		$query 	=  $this->db->query($sql,
			array(
				$data['id_do'],
				$data['no_do'],
				$data['tanggal_do'],
				$data['detail_box'],
				$data['no_pi'],
				$data['remark'],
				$data['id_schedule'],
				$data['status'],
				$data['no_bc']
			)
		);
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function delivery_order_nobc()
	{
		$sql 	= 'CALL do_list_no_bc()';

		$query 	= $this->db->query($sql);
			
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function delivery_order_detail($id_do)
	{
		$sql 	= 'CALL do_detail_list(?)';

		$query 	= $this->db->query($sql,array(
			$id_do
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function delivery_order_detail_packing($id_do)
	{
		$sql 	= 'CALL do_detail_list2(?)';

		$query 	= $this->db->query($sql,array(
			$id_do
		));
			
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function detail_packing_list($id)
	{
		$sql 	= 'CALL packing_list(?)';

		$query 	= $this->db->query($sql,array(
			$id
		));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function insert_po_packing($data)
	{
		$sql 	= 'CALL po_packing_add(?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_po_quot'],
			$data['id_schedule'],
			$data['id_packing'],
			$data['date_packing']
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function delivery_detail_order_delete($data)
	{
		$sql 	= 'CALL do_detail_packing_delete(?)';

		$query 	= $this->db->query($sql,array(
			$data['id_packing']
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function delivery_update_status($data)
	{
		$sql 	= 'CALL do_update_status(?,?,?)';

		$query 	= $this->db->query($sql,
			array(
				$data['id_do'],
				$data['status'],
				$data['date_end']
			));
			
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function delivery_get_id_coa($cust_id)
	{
		$sql 	= 'CALL coavalue_get_idcust(?)';

		$query 	= $this->db->query($sql,
			array(
				$cust_id
			));
			
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function delivery_get_value_piutang($id_do)
	{
		$sql 	= 'CALL get_piutang(?)';

		$query 	= $this->db->query($sql,
			array(
				$id_do
			));
			
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function delivery_order_insert_coa_values($data)
	{
		$sql 	= 'CALL coavalue_add(?,?,?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,
			array(
				$data['id_coa'],
				$data['id_parent'],
				$data['id_valas'],
				$data['value'],
				$data['adjustment'],
				$data['type_cash'],
				$data['note'],
				$data['rate'],
				$data['bukti']
			));
			
		$return['data'] = $query->result_array();
		$return['code'] = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function delivery_order_insert_kartu_hp($data)
	{
		$sql 	= 'CALL hp_add(?,?,?,?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,
			array(
				$data['ref'],
				$data['source'],
				$data['keterangan'],
				$data['status'],
				$data['saldo'],
				$data['saldo_akhir'],
				$data['id_valas'],
				$data['type_kartu'],
				$data['id_master'],
				$data['type_master']
			));
			
		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
}