<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pass_Qa_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	/**
      * This function is get the list data in qa table
      * @param : $params is where condition for select query
      */

	public function lists($params = array())
	{
		$sql_all 	= 'CALL qa_list_all(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				$params['filter']
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL qa_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total_row,
			'total' => $total_row,
		);

		return $return;
	}

	public function get_po($params) {
		$sql 	= 'CALL po_search_qa(?,?,?,?)';

		if($params['datefrom'] != ''){
			$datefrom=date_create($params['datefrom']);
			$datefrom=date_format($datefrom,"Y-m-d");
		}else{
			$datefrom='';
		}

		if($params['dateto'] != ''){
			$dateto=date_create($params['dateto']);
			$dateto=date_format($dateto,"Y-m-d");
		}else{
			$dateto='';
		}

		$query 	=  $this->db->query($sql,
			array(
				$datefrom,
				$dateto,
				$params['searchby'],
				$params['searchby_text']
			)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	/**
      * This function is get data in wip table by id
      * @param : $id is where condition for select query
      */
	public function detail($id)
	{
		$sql 	= 'SELECT 
						t_wip.id,
						order_no,
						stock_name,
						name_eksternal,                       
                        qty_out as qty,                        
                        m2,
                        panel_m2,
                        t_wip.id_produk
					FROM 
						t_wip
						LEFT JOIN t_po_quotation ON t_wip.id_po_quotation=t_po_quotation.id 
						LEFT JOIN t_eksternal ON t_po_quotation.cust_id = t_eksternal.id
						LEFT JOIN m_material ON m_material.id = t_wip.id_produk
						LEFT JOIN t_esf ON t_esf.id_po_quotation=t_wip.id_po_quotation AND t_esf.id_produk=t_wip.id_produk
					WHERE 
						t_wip.id='.$id;

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is get data in wip table by id
      * @param : $id is where condition for select query
      */
	public function detail_rework($id)
	{
		$sql 	= 'SELECT 
						t_wip.id,
						order_no,
						stock_name,
						name_eksternal,                       
                        qty_rework AS qty,                        
                        ROUND(panel_m2*qty_rework,2) AS m2,
                        panel_m2,
                        t_wip.id_produk
					FROM
						t_qa
						LEFT JOIN t_wip ON t_qa.id_wip=t_wip.id 
						LEFT JOIN t_po_quotation ON t_wip.id_po_quotation=t_po_quotation.id 
						LEFT JOIN t_eksternal ON t_po_quotation.cust_id = t_eksternal.id
						LEFT JOIN m_material ON m_material.id = t_wip.id_produk
						LEFT JOIN t_esf ON t_esf.id_po_quotation=t_wip.id_po_quotation AND t_esf.id_produk=t_wip.id_produk
					WHERE 
						t_qa.id_wip='.$id.' 
						AND
						isdone=0
					ORDER BY 
						t_qa.id DESC LIMIT 1';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is used to Insert Record in qa table
      * @param : $data - record array 
      */

	public function add_pass_qa($data)
	{
		$this->db->set('qty', 'qty+'.$data['qty_good'], FALSE);
		$this->db->where('id', $data['id_produk']);
		$this->db->update('m_material');

		$sql 	= 'CALL qa_add(?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id_wip'],
				$data['date_check'],
				$data['qty_good'],
				$data['qty_rework'],
				$data['qty_dispose'],
				$data['isdone']
			));

		$this->db->close();
		$this->db->initialize();
	}

	/**
      * This function is used to Update Record in qa table
      * @param : $data - record array 
      */
	public function edit_pass_qa($data)
	{
		$this->db->set('qty', 'qty+'.$data['qty_good'], FALSE);
		$this->db->where('id', $data['id_produk']);
		$this->db->update('m_material');
		
		$sql 	= 'CALL qa_update(?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id'],
				$data['id_wip'],
				$data['date_check'],
				$data['qty_good'],
				$data['qty_rework'],
				$data['qty_dispose'],
				$data['isdone']
			));

		$this->db->close();
		$this->db->initialize();
	}

	/**
     * This function is used to delete pass qa
     * @param: $id - id of wip table
     */
	function delete_qa($id) {
		$this->db->where('id_wip', $id);

		$this->db->delete('t_qa'); 

		$this->db->close();
		$this->db->initialize();
	}

	/**
      * This function is get data in qa table by id
      * @param : $id is where condition for select query
      */
	public function detailqa($id)
	{
		$sql 	= 'CALL qa_search_id(?)';

		$query 	= $this->db->query($sql,array(
				$id
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is used to Insert Record in qa table
      * @param : $data - record array 
      */

	public function add_dispose_qa($data)
	{
		$sql 	= 'CALL qa_addrework(?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id_wip'],
				$data['date_check'],
				$data['qty_good'],
				$data['qty_rework'],
				$data['qty_dispose'],
				$data['isdone']
			));

		$this->db->close();
		$this->db->initialize();
	}

	/**
      * This function is used to Update Record in qa table
      * @param : $data - record array 
      */
	public function edit_dispose_qa($data)
	{	
		$sql 	= 'CALL qa_update(?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id'],
				$data['id_wip'],
				$data['date_check'],
				$data['qty_good'],
				$data['qty_rework'],
				$data['qty_dispose'],
				$data['isdone']
			));

		$this->db->close();
		$this->db->initialize();
	}
}