  <style>
  #loading-us{display:none}
  #tick{display:none}

  #loading-mail{display:none}
  #cross{display:none}
  .input-sep{width: 65px;float: left;text-align: center;font-size: 30px;}
  body .modal{overflow-x: hidden;overflow-y: auto;}
  </style>

  <?php
    if(isset($detailqa[0]['qty_good'])){
      if(intval($detailqa[0]['qty_good']) != 0){
        $m2_good = intval($detailqa[0]['qty_good'])*floatval($detail[0]['panel_m2']);
      }else{
        $m2_good = '0.0';
      } 
    }else{
      $m2_good = '0.0';
    }

    if(isset($detailqa[0]['qty_rework'])){
      if(intval($detailqa[0]['qty_rework']) != 0){
        $m2_rework = intval($detailqa[0]['qty_rework'])*floatval($detail[0]['panel_m2']);
      }else{
        $m2_rework = '0.0';
      } 
    }else{
      $m2_rework = '0.0';
    }

    if(isset($detailqa[0]['qty_dispose'])){
      if(intval($detailqa[0]['qty_dispose']) != 0){
        $m2_dispose = intval($detailqa[0]['qty_dispose'])*floatval($detail[0]['panel_m2']);
      }else{
        $m2_dispose = '0.0';
      } 
    }else{
      $m2_dispose = '0.0';
    }
  ?>
  <form class="form-horizontal form-label-left" id="edit_qa" role="form" action="<?php echo base_url('pass_qa/edit_pass_qa');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

    <p style="text-align: center;">Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">PO No <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="order_no" name="order_no" class="form-control col-md-7 col-xs-12" placeholder="PO No" required="required" value="<?php if(isset($detail[0]['order_no'])){ echo $detail[0]['order_no']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Customer Name <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="cust_name" name="cust_name" class="form-control col-md-7 col-xs-12" placeholder="Customer Name" required="required" value="<?php if(isset($detail[0]['name_eksternal'])){ echo $detail[0]['name_eksternal']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Item <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="stock_name" name="stock_name" class="form-control col-md-7 col-xs-12" placeholder="Item" required="required" value="<?php if(isset($detail[0]['stock_name'])){ echo $detail[0]['stock_name']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Date Check
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <div class="input-group">
           <input placeholder="Date Check" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="date_check" name="date_check" required="required" value="<?php if(isset($detailqa[0]['date_check'])){ echo $detailqa[0]['date_check']; }?>">
          <div class="input-group-addon">
            <span class="glyphicon glyphicon-th"></span>
          </div>
        </div>
      </div>
    </div>

    <div class="item form-group form-item">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama" style="width: 100%;text-align: center;">-INCOMING GOODS-</label>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Qty <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="qty" name="qty" class="form-control col-md-7 col-xs-12" placeholder="Item" required="required" value="<?php if(isset($detail[0]['qty'])){ echo $detail[0]['qty']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">M&sup2; <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="m2" name="m2" class="form-control col-md-7 col-xs-12" placeholder="Item" required="required" value="<?php if(isset($detail[0]['m2'])){ echo $detail[0]['m2']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group form-item">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama" style="width: 100%;text-align: center;">-PASS GOODS-</label>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Qty Good <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="number" id="qty_good" name="qty_good" class="form-control col-md-7 col-xs-12" placeholder="Qty Good" required="required" min="0" max="<?php if(isset($detail[0]['qty'])){ echo $detail[0]['qty']; }?>" value="<?php if(isset($detailqa[0]['qty_good'])){ echo $detailqa[0]['qty_good']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">M&sup2; Good <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="m2_good" name="m2_good" class="form-control col-md-7 col-xs-12" placeholder="M&sup2; Good" maxlength="7" value="<?php echo $m2_good; ?>" required="required" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Qty Rework <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="number" id="qty_rework" name="qty_rework" class="form-control col-md-7 col-xs-12" placeholder="Qty Rework" required="required" min="0" max="<?php if(isset($detail[0]['qty'])){ echo $detail[0]['qty']; }?>" value="<?php if(isset($detailqa[0]['qty_rework'])){ echo $detailqa[0]['qty_rework']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">M&sup2; Rework <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="m2_rework" name="m2_rework" class="form-control col-md-7 col-xs-12" placeholder="M&sup2; Fresh" maxlength="7" value="<?php echo $m2_rework; ?>" required="required" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Qty Dispose <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="number" id="qty_dispose" name="qty_dispose" class="form-control col-md-7 col-xs-12" placeholder="Qty Dispose" required="required" min="0" max="<?php if(isset($detail[0]['qty'])){ echo $detail[0]['qty']; }?>" value="<?php if(isset($detailqa[0]['qty_dispose'])){ echo $detailqa[0]['qty_dispose']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">M&sup2; Dispose <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="m2_dispose" name="m2_dispose" class="form-control col-md-7 col-xs-12" placeholder="M&sup2; Dispose" maxlength="7" value="<?php echo $m2_dispose; ?>" required="required" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <button id="btn-submitdata" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Pass QA</button>
      </div>
    </div>

  <input type="hidden" id="id" name="id" value="<?php if(isset($detailqa[0]['id'])){ echo $detailqa[0]['id']; }?>">
  <input type="hidden" id="id_wip" name="id_wip" value="<?php if(isset($detail[0]['id'])){ echo $detail[0]['id']; }?>">
  <input type="hidden" id="panel_m2" name="panel_m2" value="<?php if(isset($detail[0]['panel_m2'])){ echo $detail[0]['panel_m2']; }?>">
  <input type="hidden" id="id_produk" name="panel_m2" value="<?php if(isset($detail[0]['id_produk'])){ echo $detail[0]['id_produk']; }?>">
</form><!-- /page content -->

<script type="text/javascript">
  $(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();
  });

  $('#qty_good').on('change',(function(e) {
    var qty_good = parseFloat($(this).val()),
        qty_max = parseFloat($(this).attr('max'));

    if(qty_good <= qty_max){
      var qty_max = parseFloat($('#qty').val())-qty_good;

      $('#m2_good').val(qty_good*parseFloat($('#panel_m2').val()));
      $('#qty_rework').attr('max',qty_max);
      $('#qty_dispose').attr('max',qty_max);
    }else{
      $(this).val(0);
      $('#m2_good').val('0.0');
    }
  }));

  $('#qty_rework').on('change',(function(e) {
    var qty_rework = parseFloat($(this).val()),
        qty_max = parseFloat($(this).attr('max'));

    if(qty_rework <= qty_max){
      var qty_max = parseFloat($('#qty').val())-qty_rework-parseInt($('#qty_good').val());

      $('#m2_rework').val(qty_rework*parseFloat($('#panel_m2').val()));
      $('#qty_dispose').attr('max',qty_max);
    }else{
      $(this).val(0);
      $('#m2_rework').val('0.0');
    }
  }));

  $('#qty_dispose').on('change',(function(e) {
    var qty_dispose = parseFloat($(this).val()),
        qty_max = parseFloat($(this).attr('max'));

    if(qty_dispose <= qty_max){
      $('#m2_dispose').val(parseFloat(qty_dispose*parseFloat($('#panel_m2').val())));
    }else{
      $(this).val(0);
      $('#m2_dispose').val('0.0');
    }
  }));

  $('#edit_qa').on('submit',(function(e) {
    var id                = $("#id").val(),
        id_wip            = $("#id_wip").val(),
        date_check        = $("#date_check").val(),
        qty_good          = parseInt($("#qty_good").val()),
        qty_rework        = parseInt($("#qty_rework").val()),
        qty_dispose       = parseInt($("#qty_dispose").val()),
        qty               = parseInt($("#qty").val()),
        id_produk         = $("#id_produk").val();

    var sum_qty = qty_good+qty_rework+qty_dispose;

    if(sum_qty === qty){
      $('#btn-submitdata').attr('disabled','disabled');
      $('#btn-submitdata').text("Mengubah data...");
      e.preventDefault();

      var isdone = (qty_rework>0)?0:1;

      var datapost = {
        "id"                : id,
        "id_wip"            : id_wip,
        "date_check"        : date_check,
        "qty_good"          : qty_good,
        "qty_rework"        : qty_rework,
        "qty_dispose"       : qty_dispose,
        "isdone"            : isdone,
        "id_produk"         : id_produk
      };

      $.ajax({
          type:'POST',
          url: $(this).attr('action'),
          data : JSON.stringify(datapost),
          dataType: 'json',
          contentType: 'application/json; charset=utf-8',
          success: function(response) {
              if (response.success == true) {
                $('.panel-heading button').trigger('click');
                  listqa();
                  swal({
                    title: 'Success!',
                    text: response.message,
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonText: 'Ok'
                  }).then(function () {
                  });
              } else{
                  $('#btn-submitdata').removeAttr('disabled');
                  $('#btn-submitdata').text("Edit Pass QA");
                  swal("Failed!", response.message, "error");
              }
          }
      }).fail(function(xhr, status, message) {
          $('#btn-submitdata').removeAttr('disabled');
          $('#btn-submitdata').text("Edit Pass QA");
          swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
      });
    }else{
      swal("Failed!", "Jumlah qty pass goods harus sama dengan qty incoming goods", "error");
    }
    return false;
  }));
</script>
