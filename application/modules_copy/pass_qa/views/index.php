<style>
  .changed_status{cursor:pointer;text-decoration: underline;color:#96b6e8;}
  .changed_status:hover{color:#ff8c00}
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Pass QA</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">

                    <table id="listqa" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>PO No</th>
                          <th>Customer</th>
                          <th>Item</th>
                          <th>Date Check</th>
                          <th>Qty</th>
                          <th>Qty Good</th>
                          <th>M&sup2</th>
                          <th>Option</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>No</td>
                          <td>PO No</td>
                          <td>Customer</td>
                          <td>Item</td>
                          <td>Date Check</td>
                          <td>Qty</td>
                          <td>Qty Good</td>
                          <td>M&sup2</td>
                          <td>Option</td>
                        </tr>
                      </tbody>
                    </table>

            </div>
        </div><!-- end col -->
    </div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <p></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <p></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalwip" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <p></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  function add_qa(){
      $('#panel-modal').removeData('bs.modal');
      $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modal  .panel-body').load('<?php echo base_url('pass_qa/add');?>');
      $('#panel-modal  .panel-title').html('<i class="fa fa-signal"></i> Choose PO');
      $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
  }

  function editqa(id){
      $('#panel-modalchild').removeData('bs.modal');
      $('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modalchild  .panel-body').load('<?php echo base_url('pass_qa/edit/');?>'+"/"+id);
      $('#panel-modalchild  .panel-title').html('<i class="fa fa-edit"></i> Edit Pass QA');
      $('#panel-modalchild').modal({backdrop:'static',keyboard:false},'show');
  }

  function add_disposeqa(id){
      $('#panel-modalchild').removeData('bs.modal');
      $('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modalchild  .panel-body').load('<?php echo base_url('pass_qa/add_disposeqa/');?>'+"/"+id);
      $('#panel-modalchild  .panel-title').html('<i class="fa fa-plus"></i> Add Pass QA');
      $('#panel-modalchild').modal({backdrop:'static',keyboard:false},'show');
  }

  function edit_disposeqa(id){
      $('#panel-modalchild').removeData('bs.modal');
      $('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modalchild  .panel-body').load('<?php echo base_url('pass_qa/edit_disposeqa/');?>'+"/"+id);
      $('#panel-modalchild  .panel-title').html('<i class="fa fa-plus"></i> Edit Pass QA');
      $('#panel-modalchild').modal({backdrop:'static',keyboard:false},'show');
  }

  function deleteqa(id){
    swal({
        title: 'Yakin akan Menghapus ?',
        text: 'data tidak dapat dikembalikan bila sudah dihapus !',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak'
      }).then(function () {
          var datapost={
            "id"  :   id
          };

          $.ajax({
                type:'POST',
                url: "<?php echo base_url().'pass_qa/delete_qa';?>",
                data:JSON.stringify(datapost),
                cache:false,
                contentType: false,
                processData: false,
                success: function(response) {
                    listqa();
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                    })
                }
            });
      });
  }

  function listqa(){
      $("#listqa").dataTable({
          "processing": true,
          "serverSide": true,
          "ajax": "<?php echo base_url().'pass_qa/lists';?>",
          "searchDelay": 700,
          "responsive": true,
          "lengthChange": false,
          "destroy": true,
          "info": false,
          "bSort": false,
          "dom": 'l<"toolbar">frtip',
          "initComplete": function(){
              var element = '<div class="btn-group pull-left">';
                  element += '  <a class="btn btn-primary" onClick="add_qa()">';
                  element += '    <i class="fa fa-plus"></i> Add Pass QA';
                  element += '  </a>';
                  element += '</div>';
              $("div.toolbar").prepend(element);
          }
      });
  }

	$(document).ready(function(){
      listqa();
  });
</script>