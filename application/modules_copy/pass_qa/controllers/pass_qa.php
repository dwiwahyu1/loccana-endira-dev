<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pass_Qa extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('pass_qa/pass_qa_model');
		$this->load->library('log_activity');
    }

    /**
     * anti sql injection
     * @return string
     */
    public function Anti_sql_injection($string) {
        $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
        return $string;
    }

    /**
      * This function is redirect to index pass qa page
      * @return Void
      */
    public function index() {
        $this->template->load('maintemplate', 'pass_qa/views/index');
    }

    /**
      * This function is used for showing pass qa list
      * @return Array
      */
    function lists() {
        $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
        $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
        $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
        $order = $this->input->get_post('order');
        $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
        $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

        $order_fields = array('', 'id', 'name_eksternal', 'stock_name');

        $search = $this->input->get_post('search');

        $search_val = (!empty($search['value'])) ? $search['value'] : null;

        $search_value = $this->Anti_sql_injection($search_val);

        // Build params for calling model
        $params['limit'] = (int) $length;
        $params['offset'] = (int) $start;
        $params['order_column'] = $order_fields[$order_column];
        $params['order_dir'] = $order_dir;
        $params['filter'] = $search_value;

        $list = $this->pass_qa_model->lists($params);

        $result["recordsTotal"] = $list['total'];
        $result["recordsFiltered"] = $list['total_filtered'];
        $result["draw"] = $draw;

        $data = array();

        $i = $params['offset'];
        foreach ($list['data'] as $k => $v) {
            $i++;

            $actions = '<div class="btn-group">';
            $actions .= '   <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="editqa(\'' . $v['id'] . '\')">';
            $actions .= '       <i class="fa fa-edit"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';
            $actions .= '<div class="btn-group">';
            $actions .= '   <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteqa(\'' . $v['id_wip'] . '\')">';
            $actions .= '       <i class="fa fa-trash"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';

            if($v['isdone']==0){
              if($v['idisdone']==''){
                $actions .= '<div class="btn-group">';
                $actions .= '   <button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Add" onClick="add_disposeqa(\'' . $v['id_wip'] . '\')">';
                $actions .= '       <i class="fa fa-plus"></i>';
                $actions .= '   </button>';
                $actions .= '</div>';
              }else{
                $actions .= '<div class="btn-group">';
                $actions .= '   <button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edit_disposeqa(\'' . $v['idisdone'] . '\')">';
                $actions .= '       <i class="fa fa-edit"></i>';
                $actions .= '   </button>';
                $actions .= '</div>';
              }
            }
            
            array_push($data, array(
                $i,
                $v['order_no'],
                $v['name_eksternal'],
                $v['stock_name'],
                $v['date_check'],
                $v['qty_out'],
                $v['qty'],
                $v['m2'],
                $actions
                    )
            );
        }

        $result["data"] = $data;

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is redirect to add wip page
      * @return Void
      */
    public function add() {;
        $this->load->view('add_modal_view');
    }

    /**
      * This function is used to add wip data
      * @return Array
      */
    public function get_po() {
        $data   = file_get_contents("php://input");
        $params = json_decode($data,true);

        $list   = $this->pass_qa_model->get_po($params);

        $data = array();

        $i = 0;
        foreach ($list as $k => $v) {
            $i++;

            $index = $i-1;

            $strOption =
                '<div class="radiobutton">'.
                    '<input id="option['.$index.']" type="radio" value="'.$v['id'].'">'.
                    '<label for="option['.$index.']"></label>'.
                '</div>';
            
            array_push($data, array(
                $i,
                $v['order_no'],
                $v['issue_date'],
                $v['name_eksternal'],
                $v['stock_name'],
                $strOption
            ));
        }
        
        $res = array(
            'status'    => 'success',
            'data'      => $data
        );

        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
        echo json_encode($res);
    }

    /**
      * This function is redirect to add pass qa page
      * @return Void
      */
    public function new($id) {
      $detail = $this->pass_qa_model->detail($id);

      $data = array(
          'detail'    => $detail
      );

      $this->load->view('new_modal_view',$data);
    }

    /**
      * This function is used to add pass qa data
      * @return Array
      */
    public function add_pass_qa() {
      $data     = file_get_contents("php://input");
      $params   = json_decode($data,true);

      $this->pass_qa_model->add_pass_qa($params);

      $msg = 'Berhasil menambah data Pass QA';

      $result = array(
          'success' => true,
          'message' => $msg
      );

		$this->log_activity->insert_activity('insert', $msg. ' dengan ID WIP ' .$params['id_wip']);
		
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is used to delete pass qa data
      * @return Array
      */
    public function delete_qa() {
        $data   = file_get_contents("php://input");
        $params = json_decode($data,true);

        $this->pass_qa_model->delete_qa($params['id']);

        $msg = 'Berhasil menghapus data Pass QA';

        $result = array(
            'success' => true,
            'message' => $msg
        );

		$this->log_activity->insert_activity('delete', $msg. ' dengan ID WIP ' .$params['id']);
		
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is redirect to edit pass qa page
      * @return Void
      */
    public function edit($id) {
      $detailqa = $this->pass_qa_model->detailqa($id);
      $detail = $this->pass_qa_model->detail($detailqa[0]['id_wip']);

      $data = array(
          'detailqa' => $detailqa,
          'detail'   => $detail
      );

      $this->load->view('edit_modal_view',$data);
    }

    /**
      * This function is used to edit pass qa data
      * @return Array
      */
    public function edit_pass_qa() {
      $data     = file_get_contents("php://input");
      $params   = json_decode($data,true);

      $this->pass_qa_model->edit_pass_qa($params);

      $msg = 'Berhasil mengubah data Pass QA';

      $result = array(
          'success' => true,
          'message' => $msg
      );

		$this->log_activity->insert_activity('update', $msg. ' dengan ID WIP ' .$params['id_wip']);

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is redirect to add pass qa page
      * @return Void
      */
    public function add_disposeqa($id) {
      $detail = $this->pass_qa_model->detail_rework($id);

      $data = array(
          'detail'    => $detail
      );

      $this->load->view('add_modal_dispose_view',$data);
    }

    /**
      * This function is used to add pass qa data
      * @return Array
      */
    public function add_dispose_qa() {
      $data     = file_get_contents("php://input");
      $params   = json_decode($data,true);

      $this->pass_qa_model->add_dispose_qa($params);

      $msg = 'Berhasil menambah data Rework Pass QA';

      $result = array(
          'success' => true,
          'message' => $msg
      );

		$this->log_activity->insert_activity('insert', $msg. ' dengan ID WIP ' .$params['id_wip']);
		
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is redirect to edit pass qa page
      * @return Void
      */
    public function edit_disposeqa($id) {
      $detailqa = $this->pass_qa_model->detailqa($id);
      $detail = $this->pass_qa_model->detail_rework($detailqa[0]['id_wip']);

      $data = array(
          'detailqa' => $detailqa,
          'detail'   => $detail
      );

      $this->load->view('edit_modal_dispose_view',$data);
    }

    /**
      * This function is used to edit pass qa data
      * @return Array
      */
    public function edit_dispose_qa() {
      $data     = file_get_contents("php://input");
      $params   = json_decode($data,true);

      $this->pass_qa_model->edit_dispose_qa($params);

      $msg = 'Berhasil mengubah data Rework Pass QA';

      $result = array(
          'success' => true,
          'message' => $msg
      );

		$this->log_activity->insert_activity('update', $msg. ' dengan ID WIP ' .$params['id_wip']);
		
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
}