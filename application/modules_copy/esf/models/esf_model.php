<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Esf_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	/**
      * This function is get the list data in esf table
      * @param : $params is where condition for select query
      */

	public function lists($params = array())
	{
		$sql_all 	= 'CALL esf_list_all2(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				$params['filter']
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL esf_list2(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}
	
	public function lists_detail($params = array())
	{
		$sql_all 	= 'CALL esf_list_all_add2(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				$params['filter'],
				$params['id_prd']
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL esf_list_add2(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter'],
				$params['id_prd']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}	
	
	public function lists_layout($params = array())
	{
		$sql_all 	= 'CALL esf_list_all_add_layout(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				$params['filter'],
				$params['id_prd']
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL esf_list_add_layout(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter'],
				$params['id_prd']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function get_po($params) {
		$sql 	= 'CALL po_search_esf(?,?,?,?)';

		if($params['datefrom'] != ''){
			$datefrom=date_create($params['datefrom']);
			$datefrom=date_format($datefrom,"Y-m-d");
		}else{
			$datefrom='';
		}

		if($params['dateto'] != ''){
			$dateto=date_create($params['dateto']);
			$dateto=date_format($dateto,"Y-m-d");
		}else{
			$dateto='';
		}

		$query 	=  $this->db->query($sql,
			array(
				$datefrom,
				$dateto,
				$params['searchby'],
				$params['searchby_text']
			)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	/**
      * This function is get data in esf table by id
      * @param : $id is where condition for select query
      */

	public function detail($id)
	{
		$sql 	= 'CALL esf_search_id(?)';

		$query 	= $this->db->query($sql,array(
				$id
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	

	public function listproducts($id,$flag) {
		$sql 	= 'SELECT 
						id_po_quotation,
						t_order.id_produk,
						stock_name,
						stock_description,
						t_order.qty,
						order_no,
						name_eksternal
					FROM 
						t_order 
						LEFT JOIN m_material ON t_order.id_produk = m_material.id
						LEFT JOIN t_po_quotation ON t_order.id_po_quotation = t_po_quotation.id
						LEFT JOIN t_eksternal ON t_po_quotation.cust_id = t_eksternal.id 
					WHERE 
						id_po_quotation='.$id;

		if($flag == 1){
			$sql 	.= ' AND t_order.id_produk NOT IN (SELECT id_produk FROM t_esf WHERE t_po_quotation.id=t_esf.id_po_quotation)';
		}

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function detail_sss($id) {
		$sql 	= 'SELECT a.*,c.`id` AS esf_id ,b.`name_eksternal`,b.uom_p FROM `m_material` a 
					LEFT JOIN `t_eksternal` b ON a.`cust_id` = b.`id`
					LEFT JOIN t_cust_spec c ON a.`id` = c.`id_produk`
					WHERE a.id = '.$id;



		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function detail_sss_layout($id) {
		$sql 	= 'SELECT a.*,b.`stock_code`,b.`stock_name`,a.id as esf_id,c.`name_eksternal` FROM `t_esf` a
					LEFT JOIN m_material b ON a.`id_produk` = b.`id`
					LEFT JOIN `t_eksternal` c ON b.`cust_id` = c.`id`
					WHERE a.id = '.$id;



		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is used to Insert Record in esf table
      * @param : $data - record array 
      */

	public function add_esf($data)
	{
		$sql 	= 'CALL esf_add(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				0,
				$data['id_produk'],
				$data['esf_no'],
				$data['esf_date'],
				$data['pcs_array'],
				$data['pcb_long'],
				$data['pcb_wide'],
				$data['pcb_type'],
				$data['unit'],
				$data['panel_long'],
				$data['panel_wide'],
				$data['panel_m2'],
				$data['panel_pcs_array'],
				$data['panel_pcs_m2'],
				$data['grain_direction'],
				$data['punching_direction'],
				$data['panel_img'],
				$data['notrec_sheet_long'],
				$data['notrec_sheet_wide'],
				$data['notrec_nopanel'],
				$data['notrec_pcb'],
				$data['notrec_yield'],
				$data['notrec_size_longup'],
				$data['notrec_size_wideup'],
				$data['notrec_size_longbottom'],
				$data['notrec_size_widebottom'],
				$data['notrec_m2'],
				$data['notrec_img'],
				$data['rec_sheet_long'],
				$data['rec_sheet_wide'],
				$data['rec_nopanel'],
				$data['rec_pcb'],
				$data['rec_yield'],
				$data['rec_size_longup'],
				$data['rec_size_wideup'],
				$data['rec_size_longbottom'],
				$data['rec_size_widebottom'],
				$data['rec_m2'],
				$data['rec_img']
			));

		$this->db->close();
		$this->db->initialize();
	}
	
	public function add_esf_layout($data)
	{
		$sql 	= 'CALL esf_add_layout(?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['esf_no'],
				$data['panel_long'],
				$data['panel_wide'],
				$data['sheet_panel'],
				$data['pcs_ary_pnl'],
				$data['panel_img'],
				$data['board_img']
			));

		$this->db->close();
		$this->db->initialize();
	}

	/**
      * This function is used to Update Record in esf table
      * @param : $data - record array 
      */

	public function edit_esf($data)
	{
		$sql 	= 'CALL esf_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id'],
				$data['id_po_quotation'],
				$data['id_produk'],
				$data['esf_no'],
				$data['esf_date'],
				$data['pcs_array'],
				$data['pcb_long'],
				$data['pcb_wide'],
				$data['pcb_type'],
				$data['unit'],
				$data['panel_long'],
				$data['panel_wide'],
				$data['panel_m2'],
				$data['panel_pcs_array'],
				$data['panel_pcs_m2'],
				$data['grain_direction'],
				$data['punching_direction'],
				$data['panel_img'],
				$data['notrec_sheet_long'],
				$data['notrec_sheet_wide'],
				$data['notrec_nopanel'],
				$data['notrec_pcb'],
				$data['notrec_yield'],
				$data['notrec_size_longup'],
				$data['notrec_size_wideup'],
				$data['notrec_size_longbottom'],
				$data['notrec_size_widebottom'],
				$data['notrec_m2'],
				$data['notrec_img'],
				$data['rec_sheet_long'],
				$data['rec_sheet_wide'],
				$data['rec_nopanel'],
				$data['rec_pcb'],
				$data['rec_yield'],
				$data['rec_size_longup'],
				$data['rec_size_wideup'],
				$data['rec_size_longbottom'],
				$data['rec_size_widebottom'],
				$data['rec_m2'],
				$data['rec_img']
			));

		$this->db->close();
		$this->db->initialize();
	}

	/**
     * This function is used to delete esf
     * @param: $id - id of esf table
     */
	function delete_esf($id) {
		$this->db->where('id', $id);  
		$this->db->delete('t_esf'); 

		$this->db->close();
		$this->db->initialize();
	}
	
	function delete_esf_layout($id) {
		$this->db->where('id_esf_layout', $id);  
		$this->db->delete('t_esf_layout'); 

		$this->db->close();
		$this->db->initialize();
	}
}