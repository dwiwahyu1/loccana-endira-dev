  <style>
  #loading-us{display:none}
  #tick{display:none}

  #loading-mail{display:none}
  #cross{display:none}
  .input-size{width:150px;float: left;}
  .input-sep{width: 65px;float: left;text-align: center;font-size: 30px;}
  body .modal{overflow-x: hidden;overflow-y: auto;}
  hr{width: 100%;}
  </style>

  <form class="form-horizontal form-label-left" id="edit_esf" role="form" action="<?php echo base_url('esf/add_esf');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

    <p style="text-align: center;">Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
    
        <input data-parsley-maxlength="255" type="hidden" id="order_no" name="order_no" class="form-control col-md-7 col-xs-12" placeholder="PO No" required="required" value="" readonly>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Customer Name <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="cust_name" name="cust_name" class="form-control col-md-7 col-xs-12" placeholder="Customer Name" required="required" value="<?php echo $detail_sss['name_eksternal']; ?>" readonly>
      </div>
    </div>


    <input data-parsley-maxlength="255" type="hidden" id="esf_no" name="esf_no" class="form-control col-md-7 col-xs-12" placeholder="Esf No" value="" readonly>
    <input data-parsley-maxlength="255" type="hidden" id="id_produk" name="id_produk" class="form-control col-md-7 col-xs-12" placeholder="Esf No" value="<?php echo $detail_sss['id']; ?>" readonly>


    <div class="item form-group form-item">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Part Number 
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="part_number" name="part_number" class="form-control col-md-7 col-xs-12" placeholder="Part Number" required="required" value="<?php echo $detail_sss['stock_name']; ?>" readonly>
      </div>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Esf Date
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <div class="input-group">
           <input placeholder="Esf Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="esf_date" name="esf_date" required="required" value="" autocomplete="off">
          <div class="input-group-addon">
            <span class="glyphicon glyphicon-th"></span>
          </div>
        </div>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Pcs /Arrays <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="pcs_array" name="pcs_array" class="form-control col-md-7 col-xs-12" placeholder="Pcs /Arrays" required="required" min="0" value="">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Pcb Size (mm) <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="pcb_long" name="pcb_long" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="">
        <span class="input-sep">x</span>
        <input data-parsley-maxlength="255" type="text" id="pcb_wide" name="pcb_wide" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Pcb Type <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
	  <select class="form-control" name="pcb_type" id="pcb_type" style="width: 100%" required>
          <option value="SINGLE SIDE" >SINGLE SIDE</option>
          <option value="DOUBLE SIDE" >DOUBLE SIDE</option>
        </select>
		
        <!--<input data-parsley-maxlength="255" type="text" id="pcb_type" name="pcb_type" class="form-control col-md-7 col-xs-12" placeholder="Pcb Type" required="required" value="<?php if(isset($detail[0]['pcb_type'])){ echo $detail[0]['pcb_type']; }?>"> -->
      </div>
    </div>

    <div class="item form-group form-item">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Unit 
        <span class="required">
          <sup>*</sup> 
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <select class="form-control" name="unit" id="unit" style="width: 100%" required>
          <option value="" >--Choose Unit--</option>
          <option value="arrays">Arrays</option>
          <option value="pieces">Pieces</option>
        </select>
      </div>
    </div>

	<!-- Panrl Layout -->

    <div class="item form-group form-item">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">PANEL LAYOUT</label>
      <hr/>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Panel size (mm) <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="panel_long" name="panel_long" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="">
        <span class="input-sep">x</span>
        <input data-parsley-maxlength="255" type="text" id="panel_wide" name="panel_wide" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Panel M&sup2;
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
          <input data-parsley-maxlength="255" type="text" id="panel_m2" name="panel_m2" class="form-control col-md-7 col-xs-12" value="" readonly >
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Pcs/Ary/Panel <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="panel_pcs_array" name="panel_pcs_array" class="form-control col-md-7 col-xs-12" placeholder="Pcs /Arrays" required="required" min="0" value="">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Board/Array Size<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="panel_board_long" name="panel_board_long" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="" readonly>
        <span class="input-sep">x</span>
        <input data-parsley-maxlength="255" type="text" id="panel_board_wide" name="panel_board_wide" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Pcs/Ary M&sup2;
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
          <input data-parsley-maxlength="255" type="text" id="panel_pcs_m2" name="panel_pcs_m2" class="form-control col-md-7 col-xs-12" value="" ReadOnly>
      </div>
    </div>

    <div class="item form-group form-item">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Grain Direction 
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <select class="form-control" name="grain_direction" id="grain_direction" style="width: 100%" required>
          <option value="yes">YES</option>
          <option value="no" >NO</option>
        </select>
      </div>
    </div>

    <div class="item form-group form-item">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Punching Direction 
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input type="radio" name="punching_direction" value="up"/><span style="font-size: 30px;">&#8593;</span>
        <input type="radio" name="punching_direction" value="down"style="margin-left: 15px;"/><span style="font-size: 30px;">&#8595;</span>
      </div>
    </div>

    <div class="item form-group has-feedback">
      <div style="text-align: right;margin-right: 50px;">

        <img src="<?php echo base_url();?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/crossword.png" id="panel_img_temp" style="width: 365px;height: 250px;"/>

      </div>
      <div class="col-md-8 col-sm-6 col-xs-12 img-spec" style="padding-top: 10px;margin-left: 155px;">
          <input type="file" class="form-control" id="panel_img" name="panel_img" data-height="110" accept=".jpg, .jpeg, .png"/>
          <span> Hanya format gambar jpg,jpeg,png dengan besaran max 9Mb yang diterima.</span>
      </div>
    </div>

 <div class="item form-group form-item">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama" style="width: 100%;text-align: left;">SHEET SIZE CALCULATION (RECOMMENDED)</label>
      <hr style="height: 1px;"/>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Sheet Size (mm) <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="rec_sheet_long" name="rec_sheet_long" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="">
        <span class="input-sep">x</span>
        <input data-parsley-maxlength="255" type="text" id="rec_sheet_wide" name="rec_sheet_wide" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">No Of Panel/Sheet (panel)<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="rec_nopanel" name="rec_nopanel" class="form-control col-md-7 col-xs-12" placeholder="Pcs /Arrays" required="required" min="0" value="">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Total PCB/Sheet (arrays)<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="rec_pcb" name="rec_pcb" class="form-control col-md-7 col-xs-12" placeholder="Pcs /Arrays" required="required" min="0" value="" readonly >
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Yield (%)<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="rec_yield" name="rec_yield" class="form-control col-md-7 col-xs-12" placeholder="Pcs /Arrays" required="required" value="" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">End Piece Size (mm) <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="rec_size_longup" name="rec_size_longup" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="">
        <span class="input-sep">x</span>
        <input data-parsley-maxlength="255" type="text" id="rec_size_wideup" name="rec_size_wideup" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="">
      </div>
    </div>

    <div class="item form-group">
      <div class="col-md-8 col-sm-6 col-xs-12" style="float: right;margin-right: 48px;">
        <input data-parsley-maxlength="255" type="text" id="rec_size_longbottom" name="rec_size_longbottom" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="">
        <span class="input-sep">x</span>
        <input data-parsley-maxlength="255" type="text" id="rec_size_widebottom" name="rec_size_widebottom" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">End Piece M&sup2;
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
          <input data-parsley-maxlength="255" type="text" id="rec_m2" name="rec_m2" class="form-control col-md-7 col-xs-12" value="" readonly >
      </div>
    </div>

    <div class="item form-group has-feedback">
      <div style="text-align: right;margin-right: 50px;">

        <img src="<?php echo base_url();?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/crossword.png" id="rec_img_temp" style="width: 365px;height: 250px;"/>

      </div>
      <div class="col-md-8 col-sm-6 col-xs-12 img-spec" style="padding-top: 10px;margin-left: 155px;">
          <input type="file" class="form-control" id="rec_img" name="rec_img" data-height="110" accept=".jpg, .jpeg, .png"/>
          <span> Hanya format gambar jpg,jpeg,png dengan besaran max 9Mb yang diterima.</span>
      </div>
    </div>


    <div class="item form-group form-item">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama" style="width: 100%;text-align: left;">SHEET SIZE CALCULATION (NOT RECOMMENDED)</label>
      <hr/>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Sheet Size (mm) <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="notrec_sheet_long" name="notrec_sheet_long" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="">
        <span class="input-sep">x</span>
        <input data-parsley-maxlength="255" type="text" id="notrec_sheet_wide" name="notrec_sheet_wide" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">No Of Panel/Sheet (panel)<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="notrec_nopanel" name="notrec_nopanel" class="form-control col-md-7 col-xs-12" placeholder="Pcs /Arrays" required="required" min="0" value="">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Total PCB/Sheet (arrays)<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="notrec_pcb" name="notrec_pcb" class="form-control col-md-7 col-xs-12" placeholder="Pcs /Arrays" required="required" min="0" value="">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Yield (%)<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="notrec_yield" name="notrec_yield" class="form-control col-md-7 col-xs-12" placeholder="Pcs /Arrays" required="required" value="" >
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">End Piece Size (mm) <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="notrec_size_longup" name="notrec_size_longup" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="">
        <span class="input-sep">x</span>
        <input data-parsley-maxlength="255" type="text" id="notrec_size_wideup" name="notrec_size_wideup" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="">
      </div>
    </div>

    <div class="item form-group">
      <div class="col-md-8 col-sm-6 col-xs-12" style="float: right;margin-right: 48px;">
        <input data-parsley-maxlength="255" type="text" id="notrec_size_longbottom" name="notrec_size_longbottom" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="">
        <span class="input-sep">x</span>
        <input data-parsley-maxlength="255" type="text" id="notrec_size_widebottom" name="notrec_size_widebottom" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">End Piece M&sup2;
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
          <input data-parsley-maxlength="255" type="text" id="notrec_m2" name="notrec_m2" class="form-control col-md-7 col-xs-12" value="">
      </div>
    </div>

    <div class="item form-group has-feedback">
      <div style="text-align: right;margin-right: 50px;">

        <img src="<?php echo base_url();?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/crossword.png" id="notrec_img_temp" style="width: 365px;height: 250px;"/>
       
      </div>
      <div class="col-md-8 col-sm-6 col-xs-12 img-spec" style="padding-top: 10px;margin-left: 155px;">
          <input type="file" class="form-control" id="notrec_img" name="notrec_img" data-height="110" accept=".jpg, .jpeg, .png"/>
          <span> Hanya format gambar jpg,jpeg,png dengan besaran max 9Mb yang diterima.</span>
      </div>
    </div>


    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <button id="btn-submitdata" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Save Esf</button>
      </div>
    </div>

  <input type="hidden" id="id_po_quotation" name="id_po_quotation" value="">
  <input type="hidden" id="id" name="id" value="">
</form><!-- /page content -->

<script type="text/javascript">
  $(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();

    $("#esf_date").datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
      todayHighlight: true,
    });
	
	var yields = 0;
	
	var m2unit =  $('#panel_pcs_m2').val()/$('#pcs_array').val();
	
  });

  $('#pcb_long').on('keyup',(function(e) {
	  
	  //alert('sss');
    var pcb_long = $(this).val();
    var pcb_wide = $('#pcb_wide').val();
    $('#panel_board_long').val(pcb_long);
    $('#panel_pcs_m2').val(pcb_long*pcb_wide/1000000);
  }));
  
  
  $('#pcs_array').on('keyup',(function(e) {

    var pcs_array = $(this).val();
    var panel_pcs_array = $('#panel_pcs_array').val();
    var rec_nopanel = $('#rec_nopanel').val();
	var rec_pcb_all = pcs_array * panel_pcs_array * rec_nopanel ;
	
	//if($('#rec_sheet_long').val() == 'arrays'){
		var rec_pcb = panel_pcs_array * rec_nopanel ;
	//}else{
		var rec_pcb = panel_pcs_array * rec_nopanel ;
		
	//}
	
	$('#rec_pcb').val(rec_pcb);
	var rec_sheet_long = $('#rec_sheet_long').val();
	var rec_sheet_wide = $('#rec_sheet_wide').val();
	
	var yields1 = $('#panel_pcs_m2').val()/pcs_array;
	
	var vol = (rec_sheet_wide*rec_sheet_long)/1000000;
	
	if(vol > 1.300){
		 var vv = 1.34;
		
	}else if(vol > 1.200 && vol < 1.300){
		 var vv = 1.2;
		
	}else if(vol > 1.100 && vol < 1.200){
		 var vv = 1.1;
		
	}else if(vol > 1.000 && vol < 1.100){
		 var vv = 1.0;
		
	}
	
	
	var yields = ((rec_pcb_all*yields1)/vv)*100;
	$('#rec_yield').val(yields.toFixed(2));
	
	var end_piece1 = 100 - yields;
	
	
  }));
  
    $('#panel_pcs_array').on('keyup',(function(e) {

    var pcs_array = $('#pcs_array').val();
    var panel_pcs_array = $(this).val();
    var rec_nopanel = $('#rec_nopanel').val();
	var rec_pcb_all = pcs_array * panel_pcs_array * rec_nopanel ;
	var rec_pcb = panel_pcs_array* rec_nopanel ;
	$('#rec_pcb').val(rec_pcb);
	var rec_sheet_long = $('#rec_sheet_long').val();
	var rec_sheet_wide = $('#rec_sheet_wide').val();
	
	var yields1 = $('#panel_pcs_m2').val()/pcs_array;
	
	var vol = (rec_sheet_wide*rec_sheet_long)/1000000;
	
	if(vol > 1.300){
		 var vv = 1.34;
		
	}else if(vol > 1.200 && vol < 1.300){
		 var vv = 1.2;
		
	}else if(vol > 1.100 && vol < 1.200){
		 var vv = 1.1;
		
	}else if(vol > 1.000 && vol < 1.100){
		 var vv = 1.0;
		
	}
	
	var yields = ((rec_pcb_all*yields1)/vv)*100;
	$('#rec_yield').val(yields.toFixed(2));
	
	var end_piece1 = 100 - yields;
	
	
  })); 
  
  $('#rec_nopanel').on('keyup',(function(e) {

    var pcs_array = $('#pcs_array').val();
    var panel_pcs_array = $('#panel_pcs_array').val();
    var rec_nopanel = $(this).val();
	var rec_pcb_all = pcs_array * panel_pcs_array * rec_nopanel ;
	var rec_pcb = panel_pcs_array* rec_nopanel ;
	$('#rec_pcb').val(rec_pcb);
	var rec_sheet_long = $('#rec_sheet_long').val();
	var rec_sheet_wide = $('#rec_sheet_wide').val();
	
	var yields1 = $('#panel_pcs_m2').val()/pcs_array;
	
	var vol = (rec_sheet_wide*rec_sheet_long)/1000000;
	
	if(vol > 1.300){
		 var vv = 1.34;
		
	}else if(vol > 1.200 && vol < 1.300){
		 var vv = 1.2;
		
	}else if(vol > 1.100 && vol < 1.200){
		 var vv = 1.1;
		
	}else if(vol > 1.000 && vol < 1.100){
		 var vv = 1.0;
		
	}
	
	var yields = ((rec_pcb_all*yields1)/vv)*100;
	$('#rec_yield').val(yields.toFixed(2));
	
	var end_piece1 = 100 - yields;
	
	
  }));

  $('#pcb_wide').on('keyup',(function(e) {
    var pcb_wide = $(this).val();
    var pcb_long = $('#pcb_long').val();
    $('#panel_board_wide').val(pcb_wide);
   $('#panel_pcs_m2').val(pcb_long*pcb_wide/1000000);
  }));
  
    $('#panel_long').on('keyup',(function(e) {
    var panel_long = $(this).val();
    var panel_wide = $('#panel_wide').val();
    $('#panel_m2').val(panel_long*panel_wide/1000000);
	})); 
	
	$('#panel_wide').on('keyup',(function(e) {
    var panel_wide = $(this).val();
    var panel_long = $('#panel_long').val();
    $('#panel_m2').val(panel_long*panel_wide/1000000);
	}));

  $('#panel_img').on('change',(function(e) {
    if (this.files && this.files[0]) {
      var reader = new FileReader();
          reader.onload = (function(theFile) {
              var image = new Image();
                  image.src = theFile.target.result;
              
                  image.onload = function() {
                      $("#panel_img_temp").attr('src', this.src);
                  };
          });
      reader.readAsDataURL(this.files[0]);
    }
  }));

  $('#notrec_img').on('change',(function(e) {
    if (this.files && this.files[0]) {
      var reader = new FileReader();
          reader.onload = (function(theFile) {
              var image = new Image();
                  image.src = theFile.target.result;
              
                  image.onload = function() {
                      $("#notrec_img_temp").attr('src', this.src);
                  };
          });
      reader.readAsDataURL(this.files[0]);
    }
  }));

  $('#rec_img').on('change',(function(e) {
    if (this.files && this.files[0]) {
      var reader = new FileReader();
          reader.onload = (function(theFile) {
              var image = new Image();
                  image.src = theFile.target.result;
              
                  image.onload = function() {
                      $("#rec_img_temp").attr('src', this.src);
                  };
          });
      reader.readAsDataURL(this.files[0]);
    }
  }));

  $('#edit_esf').on('submit',(function(e) {
    $('#btn-submitdata').attr('disabled','disabled');
    $('#btn-submitdata').text("Mengubah data...");
    e.preventDefault();

    var formData = new FormData(this);
        formData.set("punching_direction", $('input[name="punching_direction"]:checked').val());

    $.ajax({
        type:'POST',
        url: $(this).attr('action'),
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(response) {
            if (response.success == true) {
              $('.panel-heading button').trigger('click');
                listesf();
                swal({
                  title: 'Success!',
                  text: response.message,
                  type: 'success',
                  showCancelButton: false,
                  confirmButtonText: 'Ok'
                }).then(function () {
                });
            } else{
                $('#btn-submitdata').removeAttr('disabled');
                $('#btn-submitdata').text("Edit Esf");
                swal("Failed!", response.message, "error");
            }
        }
    }).fail(function(xhr, status, message) {
        $('#btn-submitdata').removeAttr('disabled');
        $('#btn-submitdata').text("Edit Esf");
        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
    });
  }));
</script>
