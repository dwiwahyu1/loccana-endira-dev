	<style>
		.col-customer {
			border: solid 1px #b2b8b7;
		}

		.dt-body-left {
			text-align: left;
			vertical-align: middle;
		}

		.dt-body-right {
			text-align: right;
			vertical-align: middle;
		}

		.dt-body-center {
			text-align: center;
			vertical-align: middle;
		}

		img {
			max-width: 85%;
			height: auto;
		}
	</style>

	<div id="print-area">
		<div class="row">
			<div class="col-md-12">
				<div class="pull-right">
					<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download Engineering Specification Form" id="btn_download">
						<i class="fa fa-download"></i>
					</a>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="col-md-2 logo-place">
					<img src="<?php echo site_url(); ?>assets/images/logo-celebit.jpg" alt="logo-celebit">
				</div>
				<div class="col-md-10">
					<div class="col-md-12 titleReport">
						<h1 id="titleCelebit">CELEBIT</h1>
						<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
						<h4 id="titleAlamat">BANDUNG FACTORY : JL.BUAH DUA RT.01/RW.04 RANCAEKEK - BANDUNG-INDONESIA</h4>
						<h4 id="titleTlp">TEL 62-22-7798 561/7798 542, FAX : 62-22-7798 562 E-MAIL : invoicing@celebit.com</h4>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-12 text-center">
				<h2 id="titleESF" style="font-weight:900;">ENGINEERING SPECIFICATION FORM</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5">
				<div id="titleTableCustomerInformation">1. Customer Information</div>
			</div>
			<div class="col-md-3">
				<div id="titleNew">New</div>
			</div>
			<div class="col-md-2">
				<div id="titleRevise">Revise</div>
			</div>
			<div class="col-md-2">
				<div id="titleDate">Date</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table id="tableCustomerInformation" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td width="25%">Customer Name : </td>
							<td width="25%"><?php echo $detail[0]['name_eksternal']; ?></td>
							<td width="25%">Ref : </td>
							<td width="25%"></td>
						</tr>
						<tr>
							<td>Part Number : </td>
							<td><?php echo $detail[0]['stock_name']; ?></td>
							<td>Date : </td>
							<td><?php echo $detail[0]['esf_date']; ?></td>
						</tr>
						<tr>
							<td>PCB SIZE : </td>
							<td><?php echo $detail[0]['pcb_long']; ?>mm x <?php echo $detail[0]['pcb_wide']; ?>mm</td>
							<td>PCB Type : </td>
							<td><?php echo $detail[0]['pcb_type']; ?></td>
						</tr>
						<tr>
							<td>Unit : </td>
							<td><?php echo $detail[0]['unit']; ?></td>
							<td>PCS/ARRAY : </td>
							<td><?php echo $detail[0]['pcs_array']; ?> PCS / Array</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5">
				<div id="titleTableLayoutPlan">2. Layout Plan</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table id="tableLayoutPlan" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td width="25%">Panel Size : </td>
							<td width="25%"><?php echo $detail[0]['panel_long']; ?>mm x <?php echo $detail[0]['panel_wide']; ?>mm</td>
							<td colspan="2" rowspan="5" width="50%" height="100px">
								<?php
								if ($detail[0]['panel_img'] != null) {
									$url = base_url() . 'uploads/esf/' . $detail[0]['panel_img'];
									echo '<img src="' . 'data:image/jpeg;base64,' . base64_encode(file_get_contents($url)) . '">';
								}
								?>
							</td>
						</tr>
						<tr>
							<td>PCS/ARRAY/PANEL : </td>
							<td><?php echo $detail[0]['panel_pcs_array']; ?> ARRAY</td>
						</tr>
						<tr>
							<td>BOARD/ARRAY SIZE : </td>
							<td><?php echo $detail[0]['pcb_long']; ?>mm x <?php echo $detail[0]['pcb_wide']; ?>mm</td>
						</tr>
						<tr>
							<td>PCS/ARRAY M2 : </td>
							<td><?php echo $detail[0]['panel_pcs_m2']; ?> ARRAY</td>
						</tr>
						<tr>
							<td>Punching Direction : </td>
							<td><?php echo $detail[0]['punching_direction']; ?> </td>
						</tr>
						<tr>
							<td colspan="2">B. Sheet Size Calculation (mm)<br>
								Not Recomended<br>
								<?php
								if ($detail[0]['notrec_img'] != null) {
									$url = base_url() . 'uploads/esf/' . $detail[0]['notrec_img'];
									echo '<img src="' . 'data:image/jpeg;base64,' . base64_encode(file_get_contents($url)) . '">';
								}
								?>
							</td>
							<td colspan="2"> Recomended<br>
								<?php
								if ($detail[0]['rec_img'] != null) {
									$url = base_url() . 'uploads/esf/' . $detail[0]['rec_img'];
									echo '<img src="' . 'data:image/jpeg;base64,' . base64_encode(file_get_contents($url)) . '">';
								}
								?>
							</td>
						</tr>
						<tr>
							<td>SHEET SIZE : </td>
							<td><?php echo $detail[0]['notrec_sheet_long']; ?>mm x <?php echo $detail[0]['notrec_sheet_wide']; ?>mm</td>
							<td width="25%">SHEET SIZE : </td>
							<td width="25%"><?php echo $detail[0]['rec_sheet_long']; ?> mm x <?php echo $detail[0]['notrec_sheet_wide']; ?>mm</td>
						</tr>
						<tr>
							<td>NO. OF PANEL/SHEET : </td>
							<td><?php echo $detail[0]['notrec_nopanel']; ?> PANEL</td>
							<td>NO. OF PANEL/SHEET : </td>
							<td><?php echo $detail[0]['rec_nopanel']; ?> PANEL</td>
						</tr>
						<tr>
							<td>TOTAL PCB/SHEET : </td>
							<td><?php echo $detail[0]['notrec_pcb']; ?> ARRAYS</td>
							<td>TOTAL PCB/SHEET : </td>
							<td><?php echo $detail[0]['rec_pcb']; ?> ARRAYS</td>
						</tr>
						<tr>
							<td>YIELD : </td>
							<td><?php echo $detail[0]['notrec_yield']; ?> %</td>
							<td>YIELD : </td>
							<td><?php echo $detail[0]['rec_yield']; ?> %</td>
						</tr>
						<tr>
							<td>END PIECE SIZE :</td>
							<td><?php echo $detail[0]['notrec_size_longup']; ?>mm x <?php echo $detail[0]['notrec_size_wideup']; ?>mm <br>
								<?php echo $detail[0]['notrec_size_longbottom']; ?>mm x <?php echo $detail[0]['notrec_size_widebottom']; ?>mm
							</td>
							<td>END PIECE SIZE :</td>
							<td><?php echo $detail[0]['rec_size_longup']; ?>mm x <?php echo $detail[0]['rec_size_wideup']; ?>mm <br>
								<?php echo $detail[0]['rec_size_longbottom']; ?>mm x <?php echo $detail[0]['rec_size_widebottom']; ?>mm
						</tr>
						<tr>
							<td>END PIECE M2 :</td>
							<td><?php echo $detail[0]['notrec_m2']; ?>M2
							</td>
							<td>END PIECE M2 :</td>
							<td><?php echo $detail[0]['rec_m2']; ?>M2
						</tr>
					</tbody>
				</table>
				<table id="tableLayoutPlan-Hide" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="display:none">
					<tbody>
						<tr>
							<td width="25%">Panel Size : </td>
							<td width="25%"><?php echo $detail[0]['panel_long']; ?>mm x <?php echo $detail[0]['panel_wide']; ?>mm</td>
							<td colspan="2" rowspan="5" width="50%" height="100px">
								<br>
								<?php
								if ($detail[0]['panel_img'] != null) {
									$url = base_url() . 'uploads/esf/' . $detail[0]['panel_img'];
									echo '<img src="' . 'data:image/jpeg;base64,' . base64_encode(file_get_contents($url)) . '">';
								}
								?>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
							</td>
						</tr>
						<tr>
							<td>PCS/ARRAY/PANEL : </td>
							<td><?php echo $detail[0]['panel_pcs_array']; ?> ARRAY</td>
						</tr>
						<tr>
							<td>BOARD/ARRAY SIZE : </td>
							<td><?php echo $detail[0]['pcb_long']; ?>mm x <?php echo $detail[0]['pcb_wide']; ?>mm</td>
						</tr>
						<tr>
							<td>PCS/ARRAY M2 : </td>
							<td><?php echo $detail[0]['panel_pcs_m2']; ?> ARRAY</td>
						</tr>
						<tr>
							<td>Punching Direction : </td>
							<td><?php echo $detail[0]['punching_direction']; ?> </td>
						</tr>
						<tr>
							<td colspan="2">B. Sheet Size Calculation (mm)<br>
								Not Recomended<br>
								<br>
								<?php
								if ($detail[0]['notrec_img'] != null) {
									$url = base_url() . 'uploads/esf/' . $detail[0]['notrec_img'];
									echo '<img src="' . 'data:image/jpeg;base64,' . base64_encode(file_get_contents($url)) . '">';
								}
								?>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
							</td>
							<td colspan="2"> Recomended<br>
								<br>
								<?php
								if ($detail[0]['rec_img'] != null) {
									$url = base_url() . 'uploads/esf/' . $detail[0]['rec_img'];
									echo '<img src="' . 'data:image/jpeg;base64,' . base64_encode(file_get_contents($url)) . '">';
								}
								?>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
								<br>
							</td>
						</tr>
						<tr>
							<td>SHEET SIZE : </td>
							<td><?php echo $detail[0]['notrec_sheet_long']; ?>mm x <?php echo $detail[0]['notrec_sheet_wide']; ?>mm</td>
							<td width="25%">SHEET SIZE : </td>
							<td width="25%"><?php echo $detail[0]['rec_sheet_long']; ?> mm x <?php echo $detail[0]['notrec_sheet_wide']; ?>mm</td>
						</tr>
						<tr>
							<td>NO. OF PANEL/SHEET : </td>
							<td><?php echo $detail[0]['notrec_nopanel']; ?> PANEL</td>
							<td>NO. OF PANEL/SHEET : </td>
							<td><?php echo $detail[0]['rec_nopanel']; ?> PANEL</td>
						</tr>
						<tr>
							<td>TOTAL PCB/SHEET : </td>
							<td><?php echo $detail[0]['notrec_pcb']; ?> ARRAYS</td>
							<td>TOTAL PCB/SHEET : </td>
							<td><?php echo $detail[0]['rec_pcb']; ?> ARRAYS</td>
						</tr>
						<tr>
							<td>YIELD : </td>
							<td><?php echo $detail[0]['notrec_yield']; ?> %</td>
							<td>YIELD : </td>
							<td><?php echo $detail[0]['rec_yield']; ?> %</td>
						</tr>
						<tr>
							<td>END PIECE SIZE :</td>
							<td><?php echo $detail[0]['notrec_size_longup']; ?>mm x <?php echo $detail[0]['notrec_size_wideup']; ?>mm <br>
								<?php echo $detail[0]['notrec_size_longbottom']; ?>mm x <?php echo $detail[0]['notrec_size_widebottom']; ?>mm
							</td>
							<td>END PIECE SIZE :</td>
							<td><?php echo $detail[0]['rec_size_longup']; ?>mm x <?php echo $detail[0]['rec_size_wideup']; ?>mm <br>
								<?php echo $detail[0]['rec_size_longbottom']; ?>mm x <?php echo $detail[0]['rec_size_widebottom']; ?>mm
						</tr>
						<tr>
							<td>END PIECE M2 :</td>
							<td><?php echo $detail[0]['notrec_m2']; ?>M2
							</td>
							<td>END PIECE M2 :</td>
							<td><?php echo $detail[0]['rec_m2']; ?>M2
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5">
				<div id="titleTableCustomerSpecification">3. Customer Specification</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table id="tableCustomerSpecification" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td width="50%">1. APPROVED LAMINATE</td>
							<td width="50%"><?php echo $detail[0]['approved_laminate']; ?></td>
						</tr>`
						<tr>
							<td>2. LAMINATE GRADE</td>
							<td><?php echo $detail[0]['laminate_grade']; ?></td>
						</tr>
						<tr>
							<td>3. LAM. THK BASE COPPER</td>
							<td>
								<?php echo $detail[0]['laminate']; ?> mm /<?php echo $detail[0]['thickness']; ?> micron /<?php echo $detail[0]['base_cooper']; ?> OZ
							</td>
						</tr>
						<tr>
							<td>4. CO LOGO/UL TYPE/UL LOGO</td>
							<td>
								   
								<?php echo $detail[0]['co_logo']; ?>
								&ensp;
								TYPE : <?php echo $detail[0]['type_logo']; ?>
								<?php
								if ($detail[0]['img_logo'] != null && file_exists ( base_url() . 'uploads/esf/' . $detail[0]['img_logo'])) {
									$url = base_url() . 'uploads/esf/' . $detail[0]['img_logo'];
									echo '<img src="' . 'data:image/jpeg;base64,' . base64_encode(file_get_contents($url)) . '">';
								}
								?>
								<br>
								<br>
							</td>
						</tr>
						<tr>
							<td>5. UL RECOGNITION</td>
							<td><?php echo $detail[0]['ul_recognition']; ?></td>
						</tr>
						<tr>
							<td>6. DATE CODE</td>
							<td>
								<?php echo $detail[0]['date_code']; ?>
								&ensp;
								<?php echo $detail[0]['date_type']; ?>
								<?php
								if ($detail[0]['date_img'] != null && file_exists(base_url() . 'uploads/esf/' . $detail[0]['date_img'])) {
									$url = base_url() . 'uploads/esf/' . $detail[0]['date_img'];
									echo '<img src="' . 'data:image/jpeg;base64,' . base64_encode(file_get_contents($url)) . '">';
								}
								?>
								<br>
								<br>
							</td>
						</tr>
						<tr>
							<td>7. SOLDER RESIST TYPE</td>
							<td><?php echo $detail[0]['solder_resist_type']; ?></td>
						</tr>
						<tr>
							<td>8. SOLDER RESIST</td>
							<td><?php echo $detail[0]['solder_resist']; ?> COLOUR OTHER (SPECIFY) : </td>
						</tr>
						<tr>
							<td>9. LEGEND CIRCUIT</td>
							<td><?php echo $detail[0]['legend_circuit']; ?> COLOUR OTHER (SPECIFY) : </td>
						</tr>
						<tr>
							<td>10. LEGEND COMPONENT</td>
							<td><?php echo $detail[0]['legend_component']; ?> COLOUR OTHER (SPECIFY) : </td>
						</tr>
						<tr>
							<td>11. CARBON</td>
							<td>
								<?php echo $detail[0]['carbon']; ?> TYPE : <?php echo $detail[0]['carbon_type']; ?> SPEC : <?php echo $detail[0]['carbon_spec']; ?>
							</td>
						</tr>
						<tr>
							<td>12. PEELABLE</td>
							<td>
								<?php echo $detail[0]['peelable']; ?> TYPE : <?php echo $detail[0]['peelable_type']; ?> SPEC : <?php echo $detail[0]['peelable_spec']; ?>
							</td>
						</tr>
						<tr>
							<td>13. PLATING</td>
							<td>
								<?php echo $detail[0]['plating']; ?> SPEC : <?php echo $detail[0]['plating_spec']; ?>
							</td>
						</tr>
						<tr>
							<td>14. PROCESS MOLDING</td>
							<td>
								<?php echo $detail[0]['process_molding']; ?> REMARKS : <?php echo $detail[0]['process_molding_remarks']; ?>
							</td>
						</tr>
						<tr>
							<td>15.V-CUT</td>
							<td>
								<?php echo $detail[0]['v_cut']; ?>
								<?php
								if ($detail[0]['v_cut_img'] != null  && file_exists(base_url() . 'uploads/esf/' . $detail[0]['v_cut_img'])) {
									$url = base_url() . 'uploads/esf/' . $detail[0]['v_cut_img'];
									echo '<img src="' . 'data:image/jpeg;base64,' . base64_encode(file_get_contents($url)) . '">';
								}
								?>
								<br>
								<br>
							</td>
						</tr>
						<tr>
							<td>16.TIN ROLL/HAL</td>
							<td>
								<?php echo $detail[0]['tin_roll']; ?> SPEC : <?php echo $detail[0]['tin_roll_spec']; ?>
							</td>
						</tr>
						<tr>
							<td>17. FINISHING</td>
							<td><?php echo $detail[0]['finishing']; ?></td>
						</tr>
						<tr>
							<td>18. PACKING STANDARD</td>
							<td>
								<?php echo $detail[0]['packing_standard']; ?> OTHERS (SPECIFY) :
							</td>
						</tr>
						<tr>
							<td>19.OTHER REQUIREMENT <br> (SPECIFY)</td>
							<td> <?php echo $detail[0]['others_req']; ?></td>
						</tr>
					</tbody>
				</table>
				<table id="tableTTDTop" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="display:none">>
					<tbody>
						<tr>
							<td width="25%">PREPARED BY : </td>
							<td width="25%"></td>
							<td colspan="2" width="50%">APPROVED BY : </td>
						</tr>
					</tbody>
				</table>

				<table id="tableTTDBottom" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="display:none">
					<tbody>
						<tr>
							<td width="100%" colspan="4">CIRCULATION TO : </td>
						</tr>
						<tr>
							<td width="25%">CADCAM :</td>
							<td width="25%"></td>
							<td width="25%">QC/QA : </td>
							<td width="25%"></td>
						</tr>
						<tr>
							<td>PRODUCTION CONTROL (PPIC) :</td>
							<td></td>
							<td>SALES : </td>
							<td></td>
						</tr>
						<tr>
							<td>PPRODUCTION :</td>
							<td></td>
							<td>OTHERS (SPECIFY) : </td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var dataImage = null;
		$(document).ready(function() {

			$('#btn_download').click(function() {
				var doc = new jsPDF('p', 'mm', 'letter');
				var imgData = dataImage;
				var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
				var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

				// FOOTER
				doc.setTextColor(100);
				doc.addImage(imgData, 'JPEG', 5, 5, 25, 25)
				doc.setFontSize(12);
				doc.text($('#titleCelebit').html(), 33, 10, 'left');
				doc.setFontSize(10);
				doc.text($('#titlePerusahaan').html(), 33, 15, 'left');
				doc.setFontSize(11);
				doc.text($('#titleAlamat').html(), 33, 20, 'left');
				doc.setFontSize(11);
				doc.text($('#titleTlp').html(), 33, 25, 'left');

				doc.setDrawColor(116, 119, 122);
				doc.setLineWidth(0.1);
				doc.line(4, 30, 500, 30);

				doc.setFontSize(11);
				doc.text($('#titleESF').html(), pageWidth / 2, 40, 'center');

				// Table Customer Information
				doc.text($('#titleTableCustomerInformation').html(), pageWidth / 20, 50, 'left');
				doc.text($('#titleNew').html(), pageWidth / 2, 50, 'left');
				doc.text($('#titleRevise').html(), pageWidth * 7 / 10, 50, 'left');
				doc.text($('#titleDate').html(), pageWidth * 8 / 10, 50, 'left');

				doc.autoTable({
					html: '#tableCustomerInformation',
					theme: 'plain',
					styles: {
						fontSize: 6,
						lineColor: [116, 119, 122],
						lineWidth: 0.1,
						cellWidth: 'auto',

					},
					margin: pageWidth / 20,
					tableWidth: (pageWidth) - (pageWidth / 10),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					columnStyles: {
						0: {
							halign: 'left',
							cellWidth: 30
						},
						1: {
							halign: 'left',
							cellWidth: 30
						},
						2: {
							cellWidth: 30
						},
						3: {
							cellWidth: 30
						},
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 55,
					startX: 'left'
				});

				// Table Layout Plan
				doc.text($('#titleTableLayoutPlan').html(), pageWidth / 20, 90, 'left');

				doc.autoTable({
					html: '#tableLayoutPlan-Hide',
					theme: 'plain',
					styles: {
						fontSize: 5.5,
						lineColor: [116, 119, 122],
						lineWidth: 0.1,
						cellWidth: 'auto'
					},
					margin: pageWidth / 20,
					tableWidth: (pageWidth) - (pageWidth / 10),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					columnStyles: {
						0: {
							halign: 'left',
							cellWidth: 30
						},
						1: {
							halign: 'left',
							cellWidth: 30
						},
						2: {
							cellWidth: 30
						},
						3: {
							cellWidth: 30
						},

					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 95,
					didDrawCell: function(data) {
						if ((data.cell.section == 'body')) {
							var td = data.cell.raw;
							var img = td.getElementsByTagName('img')[0];
							// data.cell.height = 50;
							var textpos = data.cell.textPos;
							if (img != null)
								doc.addImage(img.src, textpos.x - 0.5, textpos.y - 0.5, data.cell.width - 2, data.cell.height - 2);
						}
					}
				});

				// Next Page
				doc.addPage();

				//HEADER 
				doc.setTextColor(100);
				doc.addImage(imgData, 'JPEG', 5, 5, 25, 25)
				doc.setFontSize(12);
				doc.text($('#titleCelebit').html(), 33, 10, 'left');
				doc.setFontSize(10);
				doc.text($('#titlePerusahaan').html(), 33, 15, 'left');
				doc.setFontSize(11);
				doc.text($('#titleAlamat').html(), 33, 20, 'left');
				doc.setFontSize(11);
				doc.text($('#titleTlp').html(), 33, 25, 'left');

				doc.setDrawColor(116, 119, 122);
				doc.setLineWidth(0.1);
				doc.line(4, 30, 500, 30);

				//Table Customer Specification
				doc.text($('#titleTableCustomerSpecification').html(), pageWidth / 20, 40, 'left');

				doc.autoTable({
					html: '#tableCustomerSpecification',
					theme: 'plain',
					styles: {
						fontSize: 5.5,
						lineColor: [116, 119, 122],
						lineWidth: 0.1,
						cellWidth: 'auto'
					},
					margin: pageWidth / 20,
					tableWidth: (pageWidth) - (pageWidth / 10),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					columnStyles: {
						0: {
							halign: 'left',
							cellWidth: 30
						},
						1: {
							cellWidth: 30
						},
						2: {
							cellWidth: 30
						},
						3: {
							cellWidth: 30
						},
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 45,
					didDrawCell: function(data) {
						if ((data.cell.section == 'body')) {
							var td = data.cell.raw;
							console.log(td);
							if (td.getElementsByTagName('img')[0] != null)
								var img = td.getElementsByTagName('img')[0];
							// data.cell.height = 50;
							var textpos = data.cell.textPos;
							if (img != null)
								doc.addImage(img.src, textpos.x - 0.5, textpos.y - 0.5, data.cell.width - 2, data.cell.height - 2);
						}
					}
				});

				doc.autoTable({
					html: '#tableTTDTop',
					theme: 'plain',
					styles: {
						fontSize: 5.5,
						lineColor: [116, 119, 122],
						lineWidth: 0.1
					},
					margin: pageWidth / 20,
					tableWidth: (pageWidth) - (pageWidth / 10),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					columnStyles: {
						0: {
							cellWidth: 30
						},
						1: {
							cellWidth: 30
						},
						2: {
							cellWidth: 60
						},
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 180,
				});

				doc.autoTable({
					html: '#tableTTDBottom',
					theme: 'plain',
					styles: {
						fontSize: 5.5,
						lineColor: [116, 119, 122],
						lineWidth: 0.1,
					},
					margin: pageWidth / 20,
					tableWidth: (pageWidth) - (pageWidth / 10),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					columnStyles: {
						0: {
							cellWidth: 30
						},
						1: {
							cellWidth: 30
						},
						2: {
							cellWidth: 30
						},
						3: {
							cellWidth: 30
						},
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 190,
				});

				var options = {
					pagesplit: true,
					'background': '#fff'
				};
				doc.save('Engineer Specification Form <?php echo $detail[0]['approval_date']; ?>.pdf');
			});
		});

		function toDataURL(url, callback) {
			var xhr = new XMLHttpRequest();
			xhr.onload = function() {
				var reader = new FileReader();
				reader.onloadend = function() {
					callback(reader.result);
				}
				reader.readAsDataURL(xhr.response);
			};
			xhr.open('GET', url);
			xhr.responseType = 'blob';
			xhr.send();
		}

		toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
			dataImage = dataUrl;
		})


		function terbilangIND() {
			var bilangan = document.getElementById("totHide").value;
			var kalimat = "";
			var angka = new Array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
			var kata = new Array('', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan');
			var tingkat = new Array('', 'Ribu', 'Juta', 'Milyar', 'Triliun');
			var panjang_bilangan = bilangan.length;

			/* pengujian panjang bilangan */
			if (panjang_bilangan > 15) {
				kalimat = "Diluar Batas";
			} else {
				/* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
				for (i = 1; i <= panjang_bilangan; i++) {
					angka[i] = bilangan.substr(-(i), 1);
				}

				var i = 1;
				var j = 0;

				/* mulai proses iterasi terhadap array angka */
				while (i <= panjang_bilangan) {
					subkalimat = "";
					kata1 = "";
					kata2 = "";
					kata3 = "";

					/* untuk Ratusan */
					if (angka[i + 2] != "0") {
						if (angka[i + 2] == "1") {
							kata1 = "Seratus";
						} else {
							kata1 = kata[angka[i + 2]] + " Ratus";
						}
					}

					/* untuk Puluhan atau Belasan */
					if (angka[i + 1] != "0") {
						if (angka[i + 1] == "1") {
							if (angka[i] == "0") {
								kata2 = "Sepuluh";
							} else if (angka[i] == "1") {
								kata2 = "Sebelas";
							} else {
								kata2 = kata[angka[i]] + " Belas";
							}
						} else {
							kata2 = kata[angka[i + 1]] + " Puluh";
						}
					}

					/* untuk Satuan */
					if (angka[i] != "0") {
						if (angka[i + 1] != "1") {
							kata3 = kata[angka[i]];
						}
					}

					/* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
					if ((angka[i] != "0") || (angka[i + 1] != "0") || (angka[i + 2] != "0")) {
						subkalimat = kata1 + " " + kata2 + " " + kata3 + " " + tingkat[j] + " ";
					}

					/* gabungkan variabe sub kalimat (untuk Satu blok 3 angka) ke variabel kalimat */
					kalimat = subkalimat + kalimat;
					i = i + 3;
					j = j + 1;
				}

				/* mengganti Satu Ribu jadi Seribu jika diperlukan */
				if ((angka[5] == "0") && (angka[6] == "0")) {
					kalimat = kalimat.replace("Satu Ribu", "Seribu");
				}
			}

			document.getElementById("totalBilangan").innerHTML = kalimat;
		}

		function terbilangENG() {
			var bilangan = document.getElementById("totHide").value;
			var kalimat = "";
			var angka = new Array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
			var kata = new Array('', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine');
			var kataBelas = new Array('', 'One', 'Twelve', 'Thir', 'Four', 'Fif', 'Six', 'Seven', 'Eigh', 'Nine');
			var Puluh = new Array('', 'One', 'Twen', 'Thir', 'For', 'Fif', 'Six', 'Seven', 'Eigh', 'Nine');
			var tingkat = new Array('', 'Thousand', 'Million', 'Billion', 'Trillion');
			var panjang_bilangan = bilangan.length;

			/* pengujian panjang bilangan */
			if (panjang_bilangan > 15) {
				kalimat = "Diluar Batas";
			} else {
				/* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
				for (i = 1; i <= panjang_bilangan; i++) {
					angka[i] = bilangan.substr(-(i), 1);
				}

				var i = 1;
				var j = 0;

				/* mulai proses iterasi terhadap array angka */
				while (i <= panjang_bilangan) {
					subkalimat = "";
					kata1 = "";
					kata2 = "";
					kata3 = "";

					/* untuk Ratusan */
					if (angka[i + 2] != "0") {
						if (angka[i + 2] == "1") {
							kata1 = "Hundred";
						} else {
							kata1 = kata[angka[i + 2]] + " Hundred";
						}
					}

					/* untuk Puluhan atau Belasan */
					if (angka[i + 1] != "0") {
						if (angka[i + 1] == "1") {
							if (angka[i] == "0") {
								kata2 = "Ten";
							} else if (angka[i] == "1") {
								kata2 = "Eleven";
							} else if (angka[i] == "2") {
								kata2 = "Twelve";
							} else {
								kata2 = kataBelas[angka[i]] + "teen";
							}
						} else {
							kata2 = kataPuluh[angka[i + 1]] + "ty";
						}
					}

					/* untuk Satuan */
					if (angka[i] != "0") {
						if (angka[i + 1] != "1") {
							kata3 = kata[angka[i]];
						}
					}

					/* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
					if ((angka[i] != "0") || (angka[i + 1] != "0") || (angka[i + 2] != "0")) {
						subkalimat = kata1 + " " + kata2 + " " + kata3 + " " + tingkat[j] + " ";
					}

					/* gabungkan variabe sub kalimat (untuk Satu blok 3 angka) ke variabel kalimat */
					kalimat = subkalimat + kalimat;
					i = i + 3;
					j = j + 1;
				}

				/* mengganti Satu Ribu jadi Seribu jika diperlukan */
				if ((angka[5] == "0") && (angka[6] == "0")) {
					kalimat = kalimat.replace("One Thousand", "Thousand");
				}
			}

			document.getElementById("totalBilangan").innerHTML = kalimat;
		}
	</script>