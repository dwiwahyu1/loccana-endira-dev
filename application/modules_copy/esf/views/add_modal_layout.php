  <style>
  #loading-us{display:none}
  #tick{display:none}

  #loading-mail{display:none}
  #cross{display:none}
  .input-size{width:150px;float: left;}
  .input-sep{width: 65px;float: left;text-align: center;font-size: 30px;}
  body .modal{overflow-x: hidden;overflow-y: auto;}
  hr{width: 100%;}
  </style>

  <form class="form-horizontal form-label-left" id="edit_esf" role="form" action="<?php echo base_url('esf/add_esf_layout');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

    <p style="text-align: center;">Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
    
        <input data-parsley-maxlength="255" type="hidden" id="order_no" name="order_no" class="form-control col-md-7 col-xs-12" placeholder="PO No" required="required" value="" readonly>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Customer Name <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="cust_name" name="cust_name" class="form-control col-md-7 col-xs-12" placeholder="Customer Name" required="required" value="<?php echo $detail_sss['name_eksternal']; ?>" readonly>
      </div>
    </div>


    <input data-parsley-maxlength="255" type="hidden" id="esf_no" name="esf_no" class="form-control col-md-7 col-xs-12" placeholder="Esf No" value="<?php echo $ido; ?>" readonly>
    <input data-parsley-maxlength="255" type="hidden" id="id_produk" name="id_produk" class="form-control col-md-7 col-xs-12" placeholder="Esf No" value="<?php echo $detail_sss['id']; ?>" readonly>


    <div class="item form-group form-item">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Part Number 
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="part_number" name="part_number" class="form-control col-md-7 col-xs-12" placeholder="Part Number" required="required" value="<?php echo $detail_sss['stock_name']; ?>" readonly>
      </div>
      </div>
    </div>

	<!-- Panrl Layout -->

    <div class="item form-group form-item">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">PANEL LAYOUT</label>
      <hr/>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Panel size (mm) <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="panel_long" name="panel_long" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="">
        <span class="input-sep">x</span>
        <input data-parsley-maxlength="255" type="text" id="panel_wide" name="panel_wide" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Sheet/Panel <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="sheet_panel" name="sheet_panel" class="form-control col-md-7 col-xs-12" placeholder="sheet_panel" required="required" min="0" value="">
      </div>
    </div>
	
    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Pcs / Ary / Pnl <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="pcs_ary_pnl" name="pcs_ary_pnl" class="form-control col-md-7 col-xs-12" placeholder="sheet_panel" required="required" min="0" value="">
      </div>
    </div>


    <div class="item form-group form-item">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Cutting Map
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
	  		<div class="col-md-8 col-sm-6 col-xs-12 img-spec" >
          <input type="file" class="form-control" id="panel_img" name="panel_img" data-height="110" accept=".jpg, .jpeg, .png"/>
          <span> Hanya format gambar jpg,jpeg,png dengan besaran max 9Mb yang diterima.</span>
      </div>
    </div>

    <div class="item form-group has-feedback">
      <div style="text-align: right;margin-right: 50px;">

        <img src="<?php echo base_url();?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/crossword.png" id="panel_img_temp" style="width: 265px;height: 150px;"/>

      </div>
      
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Board Layout
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
	   <div class="col-md-8 col-sm-6 col-xs-12 img-spec">
          <input type="file" class="form-control" id="board_img" name="board_img" data-height="110" accept=".jpg, .jpeg, .png"/>
          <span> Hanya format gambar jpg,jpeg,png dengan besaran max 9Mb yang diterima.</span>
      </div>
    </div>

    <div class="item form-group has-feedback">
      <div style="text-align: right;margin-right: 50px;">

        <img src="<?php echo base_url();?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/crossword.png" id="board_img_temp" style="width: 265px;height: 150px;"/>
       
      </div>
     
    </div>

  <input type="hidden" id="id_po_quotation" name="id_po_quotation" value="">
  <input type="hidden" id="id" name="id" value="">
  
   <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <button id="btn-submitdata" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Save Layout</button>
      </div>
    </div>
  
</form><!-- /page content -->


                    <table id="listesf_layout" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No</th>
                        <!--  <th>PO No</th>
                          <th>ESF No</th>
                          <th>ESF Date</th> -->
                          <th>Panel Size</th>
                          <th>Sheet/Panel</th>
                          <th>Cutting Map</th>
                          <th>Pcs / Ary / Pnl</th>
                          <th>Board Layout</th>
                          <th>Option</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>No</td>
                       <!--   <td>PO No</td>
                          <td>ESF No</td>
                          <td>ESF Date</td> -->
                          <th>Panel Size</th>
                          <th>Sheet/Panel</th>
                          <th>Cutting Map</th>
                          <th>Pcs / Ary / Pnl</th>
                          <th>Board Layout</th>
                          <th>Option</th>
                        </tr>
                      </tbody>
                    </table>


<script type="text/javascript">
  $(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();

    $("#esf_date").datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
      todayHighlight: true,
    });
	
	var yields = 0;
	
	var m2unit =  $('#panel_pcs_m2').val()/$('#pcs_array').val();
	
	
	listesf_layout();
	
	
  });
  
    function listesf_layout(){
      $("#listesf_layout").dataTable({
          "processing": true,
          "serverSide": true,
          "ajax": "<?php echo base_url().'esf/lists_layout/'.$ido;?>",
          "searchDelay": 700,
          "responsive": true,
          "lengthChange": false,
          "destroy": true,
          "info": false,
          "bSort": false,
          "bFilter": false,
          "dom": 'l<"toolbar">frtip',
          "initComplete": function(){
			  var element = '';
              $("div.toolbar").prepend(element);
          }
      });
  }

  $('#pcb_long').on('keyup',(function(e) {
	  
	  //alert('sss');
    var pcb_long = $(this).val();
    var pcb_wide = $('#pcb_wide').val();
    $('#panel_board_long').val(pcb_long);
    $('#panel_pcs_m2').val(pcb_long*pcb_wide/1000000);
  }));
  
  
  $('#pcs_array').on('keyup',(function(e) {

    var pcs_array = $(this).val();
    var panel_pcs_array = $('#panel_pcs_array').val();
    var rec_nopanel = $('#rec_nopanel').val();
	var rec_pcb_all = pcs_array * panel_pcs_array * rec_nopanel ;
	var rec_pcb =  panel_pcs_array * rec_nopanel ;
	$('#rec_pcb').val(rec_pcb);
	var rec_sheet_long = $('#rec_sheet_long').val();
	var rec_sheet_wide = $('#rec_sheet_wide').val();
	
	var yields1 = $('#panel_pcs_m2').val()/pcs_array;
	
	var vol = (rec_sheet_wide*rec_sheet_long)/1000000;
	
	if(vol > 1.00000 && vol < 1.10000 ){
		var vv = 1.0;
	}else if(vol > 1.10000 && vol < 1.20000 ){
		var vv = 1.1;
	}else if(vol > 1.20000 && vol < 1.30000 ){
		var vv = 1.2;
	}else if(vol > 1.30000 && vol < 1.40000 ){
		var vv = 1.34;
	}		
	
	// if(vol > 1.30000){
		// var vv = vol.toFixed(2);
	// }else{
		
		// var vv = res.toFixed(1);
	// }
	
	var yields = ((rec_pcb_all*yields1)/vv)*100;
	$('#rec_yield').val(yields.toFixed(2));
	
	var end_piece1 = 100 - yields;
	
	$('#rec_m2').val(end_piece1);
	
	
	//alert(yields);
	
  }));
  
  
    $('#panel_pcs_array').on('keyup',(function(e) {

    var pcs_array = $('#pcs_array').val();
    var panel_pcs_array = $(this).val();
    var rec_nopanel = $('#rec_nopanel').val();
	var rec_pcb_all = pcs_array * panel_pcs_array * rec_nopanel ;
	var rec_pcb =  panel_pcs_array * rec_nopanel ;
	$('#rec_pcb').val(rec_pcb);
	var rec_sheet_long = $('#rec_sheet_long').val();
	var rec_sheet_wide = $('#rec_sheet_wide').val();
	
	var yields1 = $('#panel_pcs_m2').val()/pcs_array;
	
	var vol = (rec_sheet_wide*rec_sheet_long)/1000000;
	
	if(vol > 1.00000 && vol < 1.10000 ){
		var vv = 1.0;
	}else if(vol > 1.10000 && vol < 1.20000 ){
		var vv = 1.1;
	}else if(vol > 1.20000 && vol < 1.30000 ){
		var vv = 1.2;
	}else if(vol > 1.30000 && vol < 1.40000 ){
		var vv = 1.34;
	}		
	
	// if(vol > 1.30000){
		// var vv = vol.toFixed(2);
	// }else{
		
		// var vv = res.toFixed(1);
	// }
	
	var yields = ((rec_pcb_all*yields1)/vv)*100;
	$('#rec_yield').val(yields.toFixed(2));
	
	var end_piece1 = 100 - yields;
	
	$('#rec_m2').val(end_piece1);
	
  }));    
  
  $('#rec_nopanel').on('keyup',(function(e) {

    var pcs_array = $('#pcs_array').val();
    var panel_pcs_array = $('#panel_pcs_array').val();
    var rec_nopanel = $(this).val();
	var rec_pcb_all = pcs_array * panel_pcs_array * rec_nopanel ;
	var rec_pcb =  panel_pcs_array * rec_nopanel ;
	$('#rec_pcb').val(rec_pcb);
	var rec_sheet_long = $('#rec_sheet_long').val();
	var rec_sheet_wide = $('#rec_sheet_wide').val();
	
	var yields1 = $('#panel_pcs_m2').val()/pcs_array;
	
		var yields1 = $('#panel_pcs_m2').val()/pcs_array;
	
	var vol = (rec_sheet_wide*rec_sheet_long)/1000000;
	
	if(vol > 1.00000 && vol < 1.10000 ){
		var vv = 1.0;
	}else if(vol > 1.10000 && vol < 1.20000 ){
		var vv = 1.1;
	}else if(vol > 1.20000 && vol < 1.30000 ){
		var vv = 1.2;
	}else if(vol > 1.30000 && vol < 1.40000 ){
		var vv = 1.34;
	}		
	
	// if(vol > 1.30000){
		// var vv = vol.toFixed(2);
	// }else{
		
		// var vv = res.toFixed(1);
	// }
	
	var yields = ((rec_pcb_all*yields1)/vv)*100;
	$('#rec_yield').val(yields.toFixed(2));
	
	var end_piece1 = 100 - yields;
	
	$('#rec_m2').val(end_piece1);
	//alert((rec_pcb_all*yields1)+' | '+vv+' | '+vol);
	
  }));
  
    $('#panel_pcs_array').on('keyup',(function(e) {

    var pcs_array = $('#pcs_array').val();
    var panel_pcs_array = $(this).val();
    var rec_nopanel = $('#rec_nopanel').val();
	var rec_pcb = pcs_array * panel_pcs_array * rec_nopanel ;
	$('#rec_pcb').val(rec_pcb);
	
  })); 
  
  $('#rec_nopanel').on('keyup',(function(e) {

    var pcs_array = $('#pcs_array').val();
    var panel_pcs_array = $('#panel_pcs_array').val();
    var rec_nopanel = $(this).val();
	var rec_pcb = pcs_array * panel_pcs_array * rec_nopanel ;
	var rec_pcb = $('#rec_pcb').val(rec_pcb);
	
  }));

  $('#pcb_wide').on('keyup',(function(e) {
    var pcb_wide = $(this).val();
    var pcb_long = $('#pcb_long').val();
    $('#panel_board_wide').val(pcb_wide);
   $('#panel_pcs_m2').val(pcb_long*pcb_wide/1000000);
  }));
  
    $('#panel_long').on('keyup',(function(e) {
    var panel_long = $(this).val();
    var panel_wide = $('#panel_wide').val();
    $('#panel_m2').val(panel_long*panel_wide/1000000);
	})); 
	
	$('#panel_wide').on('keyup',(function(e) {
    var panel_wide = $(this).val();
    var panel_long = $('#panel_long').val();
    $('#panel_m2').val(panel_long*panel_wide/1000000);
	}));

  $('#panel_img').on('change',(function(e) {
    if (this.files && this.files[0]) {
      var reader = new FileReader();
          reader.onload = (function(theFile) {
              var image = new Image();
                  image.src = theFile.target.result;
              
                  image.onload = function() {
                      $("#panel_img_temp").attr('src', this.src);
                  };
          });
      reader.readAsDataURL(this.files[0]);
    }
  }));

  $('#board_img').on('change',(function(e) {
    if (this.files && this.files[0]) {
      var reader = new FileReader();
          reader.onload = (function(theFile) {
              var image = new Image();
                  image.src = theFile.target.result;
              
                  image.onload = function() {
                      $("#board_img_temp").attr('src', this.src);
                  };
          });
      reader.readAsDataURL(this.files[0]);
    }
  }));

  $('#rec_img').on('change',(function(e) {
    if (this.files && this.files[0]) {
      var reader = new FileReader();
          reader.onload = (function(theFile) {
              var image = new Image();
                  image.src = theFile.target.result;
              
                  image.onload = function() {
                      $("#rec_img_temp").attr('src', this.src);
                  };
          });
      reader.readAsDataURL(this.files[0]);
    }
  }));

  function deleteesflayout(id){
    swal({
        title: 'Yakin akan Menghapus ?',
        text: 'data tidak dapat dikembalikan bila sudah dihapus !',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak'
      }).then(function () {
          var datapost={
            "id"  :   id
          };

          $.ajax({
                type:'POST',
                url: "<?php echo base_url().'esf/delete_esf_layout';?>",
                data:JSON.stringify(datapost),
                cache:false,
                contentType: false,
                processData: false,
                success: function(response) {
                    listesf_layout();
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                    })
                }
            });
      });
  }


  $('#edit_esf').on('submit',(function(e) {
    $('#btn-submitdata').attr('disabled','disabled');
    $('#btn-submitdata').text("Mengubah data...");
    e.preventDefault();

    var formData = new FormData(this);
        formData.set("punching_direction", $('input[name="punching_direction"]:checked').val());

    $.ajax({
        type:'POST',
        url: $(this).attr('action'),
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(response) {
            if (response.success == true) {
             // $('.panel-heading button').trigger('click');
				$('#btn-submitdata').removeAttr('disabled');
                $('#btn-submitdata').text("Save Layout");
                listesf_layout();
                swal({
                  title: 'Success!',
                  text: response.message,
                  type: 'success',
                  showCancelButton: false,
                  confirmButtonText: 'Ok'
                }).then(function () {
                });
            } else{
                $('#btn-submitdata').removeAttr('disabled');
                $('#btn-submitdata').text("Save Layout");
                swal("Failed!", response.message, "error");
            }
        }
    }).fail(function(xhr, status, message) {
        $('#btn-submitdata').removeAttr('disabled');
        $('#btn-submitdata').text("Edit Esf");
        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
    });
  }));
</script>
