<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Po extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('po/po_model');
        $this->load->model('quotation/quotation_model');
		$this->load->library('log_activity');
    }

    /**
     * anti sql injection
     */
    public function Anti_sql_injection($string) {
        $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
        return $string;
    }

    public function index() {
        $this->template->load('maintemplate', 'po/views/index');
    }

    /**
      * This function is used for showing po list
      * @return Array
      */
    function lists() {
        $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
        $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
        $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
        $order = $this->input->get_post('order');
        $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
        $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

        $order_fields = array('', 'id', 'name_eksternal', 'order_no');

        $search = $this->input->get_post('search');

        $search_val = (!empty($search['value'])) ? $search['value'] : null;

        $search = $this->input->get_post('search');
        $cust_id = $this->input->get_post('cust_id');

        $search_value = $this->Anti_sql_injection($search_val);

        // Build params for calling model
        $params['limit'] = (int) $length;
        $params['offset'] = (int) $start;
        $params['order_column'] = $order_fields[$order_column];
        $params['order_dir'] = $order_dir;
        $params['filter'] = $search_value;
        $params['cust_id'] = $cust_id;

        $list = $this->po_model->lists($params);

        $result["recordsTotal"] = $list['total'];
        $result["recordsFiltered"] = $list['total_filtered'];
        $result["customer"] = $this->quotation_model->customer();
        $result["draw"] = $draw;

        $data = array();

        $i = $params['offset'];
        foreach ($list['data'] as $k => $v) {
            $i++;
            $actions = '<div class="btn-group">';
            $actions .= '   <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Proses Po" onClick="edit_po(\'' . $v['id'] . '\')">';
            $actions .= '       <i class="fa fa-edit"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';
            $actions .= '<div class="btn-group">';
            $actions .= '   <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="delete_po(\'' . $v['id'] . '\')">';
            $actions .= '       <i class="fa fa-trash"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';
            
            array_push($data, array(
                $i,
                $v['id'],
                $v['order_no'],
                $v['order_date'],
                $v['name_eksternal'],
                $v['total_products'],
                number_format($v['total_amount'],2,",","."),
                $v['nama_valas'],
                $v['status'],
                $actions
                    )
            );
        }

        $result["data"] = $data;

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is used to delete po data
      * @return Array
      */
    public function delete_po() {
        $data       = file_get_contents("php://input");
        $params     = json_decode($data,true);

        $this->quotation_model->delete_quotation($params['id']);

        $msg = 'Berhasil menghapus data po';

        $result = array(
            'success' => true,
            'message' => $msg
        );

		$this->log_activity->insert_activity('delete', $msg. ' dengan ID PO ' .$params['id']);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
    * This function is redirect to edit po page
    * @return Void
    */
    public function edit($id) {
        $detail = $this->quotation_model->detail($id);

        $data = array(
              'detail'    => $detail
        );

        $this->load->view('edit_modal_view',$data);
    }

    /**
    * This function is redirect to edit schedule of po page
    * @return Void
    */
    public function edit_schedule($id,$price) {
        $detail = $this->po_model->detail_schedule($id);

        $data = array(
              'id' => $id,
              'price' => $price,
              'detail' => $detail
        );

        $this->load->view('edit_modal_schedule_view',$data);
    }

    /**
      * This function is used to edit schedule of po data
      * @return Array
      */
    public function doedit_schedule() {
        $data   = file_get_contents("php://input");
        $params = json_decode($data,true);
        // var_dump($params);die;
        $schedule = $this->po_model->doedit_schedule($params);

        $msg = 'Berhasil mengubah schedule po';

        $dataLen = count($params);

        $result = array(
            'success' => true,
            'message' => $msg,
            'schedule' => $schedule
        );
        for($i=0;$i<$dataLen;$i++)
		    $this->log_activity->insert_activity('update', $msg. ' dengan ID Order ' .$params[$i]['id_order']);
		
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is used to edit schedule of po data
      * @return Array
      */
    public function edit_po() {
        $data   = file_get_contents("php://input");
        $params = json_decode($data,true);

        $schedule = $this->po_model->edit_po($params);

        $msg = 'Berhasil mengubah po';

        $result = array(
            'success' => true,
            'message' => $msg
        );

		$this->log_activity->insert_activity('update', $msg. ' dengan No Order ' .$params['order_no']);
		
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
}