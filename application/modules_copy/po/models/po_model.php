<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Po_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	/**
      * This function is get the list data in order table
      * @param : $params is where condition for select query
      */

	public function lists($params = array())
	{
		$sql_all 	= 'CALL po_list_all(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				$params['filter'],
				$params['cust_id']
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL order_list(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter'],
				$params['cust_id']
			));

		$result = $query->result_array();
		$result = (isset($result[0]['id']) && $result[0]['id']!=NULL)?$result:array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total_row,
			'total' => $total_row,
		);

		return $return;
	}

	/**
      * This function is get data in schedule_shipping table by id
      * @param : $id is where condition for select query
      */

	public function detail_schedule($id)
	{
		$sql 	= 'CALL schedule_search_id(?)';

		$query 	= $this->db->query($sql,array($id));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is get data in schedule_shipping table by id
      * @param : $id is where condition for select query
      */

	public function doedit_schedule($data)
	{
		$this->db->where('id_order', $data[0]['id_order']);  
		$this->db->delete('t_schedule_shipping'); 

		$sql 	= 'CALL schedule_add(?, ?, ?, ?, ?, ?)';
		
		$dataLen = count($data);
		$delivery = '';
		for($i=0;$i<$dataLen;$i++){
			$date_shipping_plan = ($data[$i]['date_shipping_plan']!='')?$data[$i]['date_shipping_plan']:NULL;
			$this->db->query($sql,array(
				$data[$i]['id_order'],
				$data[$i]['qty_shipping'],
				$data[$i]['type_shipping'],
				$data[$i]['note_shipping'],
				$date_shipping_plan,
				NULL
			));

			if(intval($data[$i]['type_shipping'])==0){
				$delivery = 'ASAP';
			}else if(intval($data[$i]['type_shipping'])==1){
				$delivery = 'Menunggu info';
			}else{
				if($i==0){
					$delivery = $date_shipping_plan;
				}else{
					$delivery .= ','.$date_shipping_plan;
				}
			}
		}

        $return = array(
		        'delivery' => $delivery
		);

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is used to Update Record in po_quotation table
      * @param : $data - updated array 
      */

	public function edit_po($data)
	{
		$return = array(
		        'order_no' => $data['order_no'],
		        'order_date' => $data['order_date'],
		        'transport' => $data['transport'],
		        'packing' => $data['packing'],
		        'status' => 11
		);

		$this->db->where('id',$data['id']);
		$this->db->update('t_po_quotation',$return);

		foreach($data['order'] as $orderkey) {
			$orderdata = array(
			        'qty' => $orderkey['qty'],
			        'amount_price' => $orderkey['amount_price']
			);

			$this->db->where('id_order',$orderkey['id_order']);
			$this->db->update('t_order',$orderdata);
		}

		$sql 	= 'update m_material LEFT JOIN t_order ON t_order.id_produk=m_material.id set `status`=6 where id_po_quotation=?';

		$this->db->query($sql,array($data['id']));

		$note   = 'PO '.$data['order_no'];

		$sql 	= 'SELECT COUNT(id) AS jumlah FROM t_coa_value WHERE note = ?;';
		$query 	= $this->db->query($sql,
			array($note)
		);
		$countcoa = $query->row_array();

		if(intval($countcoa) == 0){
			$sql 	= 'CALL coavalue_add(?,?,?,?,?,?,?)';
			$this->db->query($sql,array(121,1,1,$data['amount'],NULL,0,$note));
		}else{
			$return = array(
		        'value' => $data['amount']
			);

			$this->db->where('note',$note);
			$this->db->update('t_coa_value',$return);
		}

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
}