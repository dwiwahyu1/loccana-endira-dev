<style>
  .filter_separator{padding:0px 10px;}
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">PO</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">

                    <table id="listpo" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Quotation Number</th>
                          <th>PO Number</th>
                          <th>PO Date</th>
                          <th>Customer</th>
                          <th>Total Item</th>
                          <th>Total Amount</th>
                          <th>Currency</th>
                          <th>Status</th>
                          <th>Option</th>
                        </tr>
                      </thead>
                      <tbody>
                        <td>No</td>
                        <td>Quotation Number</td>
                        <td>PO Number</td>
                        <td>PO Date</td>
                        <td>Customer</td>
                        <td>Total Item</td>
                        <td>Total Amount</td>
                        <td>Currency</td>
                        <td>Status</td>
                        <td>Option</td>
                      </tbody>
                    </table>

            </div>
        </div><!-- end col -->
    </div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog" style="width:800px;">
    <div class="modal-content p-0 b-0">
      <div class="panel panel-color panel-primary panel-filled">
        <div class="panel-heading">
          <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 class="panel-title"></h3>
        </div>
        <div class="panel-body">
          <p></p>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <p></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
	$(document).ready(function(){
      listpo();
  });

  function listpo(){
      var cust_id = (typeof $('.select_customer').val() !== "undefined")?$('.select_customer').val():'0';

      $("#listpo").dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo base_url().'po/lists?cust_id="+cust_id+"';?>",
            "searchDelay": 700,
            "responsive": true,
            "lengthChange": false,
            "destroy": true,
            "info": false,
            "bSort": false,
            "dom": 'l<"toolbar">frtip',
            "initComplete": function(result){
                var element = '<div class="btn-group pull-left">';
                    element += '  <label>Filter</label>';
                    element += '  <label class="filter_separator">|</label>';
                    element += '  <label>Customer:';
                    element += '    <select class="form-control select_customer" onChange="listpo()" style="height:30px;width: 340px;">';
                    element += '      <option value="0">-- Semua Customer --</option>';
                    $.each( result.json.customer, function( key, val ) {
                      var selectcust = (val.id===cust_id)?'selected':'';
                            element += '    <option value="'+val.id+'" '+selectcust+'>'+val.name_eksternal+'</option>';
                        });
                    element += '    </select>';
                    element += '  </label>';
                        
               $("#listpo_filter").prepend(element);
            }
      });
  }
  
  function delete_po(id){
    swal({
      title: 'Yakin akan Menghapus ?',
      text: 'data tidak dapat dikembalikan bila sudah dihapus !',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak'
    }).then(function () {
      var datapost={
        "id" : id
      };

      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>po/delete_po",
        data : JSON.stringify(datapost),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function(response) {
           $('.panel-heading button').trigger('click');
                    listpo();
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                    })
        }
      });
    })
  }
  
  function edit_po(id){
    $('#panel-modal').removeData('bs.modal');
    $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    $('#panel-modal  .panel-body').load('<?php echo base_url('po/edit/');?>'+"/"+id);
    $('#panel-modal  .panel-title').html('<i class="fa fa-pencil"></i> Proses Po');
    $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
  }
  
  function edit_schedule(id,name,price,row){
      $('#panel-modalchild').removeData('bs.modal');
      $('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modalchild  .panel-body').load('<?php echo base_url('po/edit_schedule/');?>'+"/"+id+"/"+price);
      $('#panel-modalchild  .panel-title').html('<i class="fa fa-pencil"></i> Edit '+name);
      $('#panel-modalchild').modal({backdrop:'static',keyboard:false},'show');
  } 
</script>