<style>
    .form-item{margin-top: 15px;overflow: auto;}
</style>

<?php
 	$type = isset($detail[0]['type_shipping'])?$detail[0]['type_shipping']:'0';
 	$qty =  (isset($detail[0]['qty_shipping']) && $type != '2')?$detail[0]['qty_shipping']:'0';
 	$note =  (isset($detail[0]['note_shipping']) && $type != '2')?$detail[0]['note_shipping']:'';
 ?>
 <div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="schedule" style="margin-top: 10px;">Schedule</label>
    <div class="col-md-8 col-sm-6 col-xs-12">
       	<label style="width:70px">
       		<input type="radio" id="asap_ship" name="type" value="0" <?php if($type == '0'){ echo 'checked'; }?>> <span>ASAP</span>
       	</label>
       	<label style="width: 110px;">
       		<input type="radio" id="waiting_ship" name="type" value="1" <?php if($type == '1'){ echo 'checked'; }?>> <span>Waiting Info</span>
       	</label>
       	<label>
       		<input type="radio" id="date_ship" name="type" value="2" <?php if($type == '2'){ echo 'checked'; }?>> 
       		<input type="text" id="date_plan" class="form-control datepicker" style="width: 150px;display: inline-block;" value="<?php echo date('Y-m-d');?>">
       	</label>
    </div>
</div>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty">Qty
    </label>
    <div class="col-md-8 col-sm-6 col-xs-12">
       	<input data-parsley-maxlength="255" type="number" min="0" id="qty" name="qty" class="form-control col-md-7 col-xs-12" placeholder="Qty" value="<?php echo $qty;?>">
    </div>
</div>

<div class="item form-group form-item">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="notes">Notes</label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <textarea data-parsley-maxlength="255" type="text" id="notes" name="notes" class="form-control col-md-7 col-xs-12" placeholder="Notes"><?php echo $note;?></textarea>
    </div>
</div>

<div class="item form-group form-item" id="btn-addtemp" <?php if($type != '2'){ echo 'style="display:none"'; }?>>
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama" style="float: right;">
    	<button id="btn-add" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" style="min-width: 70px;">
    		<i class="fa fa-plus"></i> Add
    	</button>
    </label>
</div>

<div class="item form-group form-item" id="scheduletext" <?php if($type != '2'){ echo 'style="display:none"'; }?>>
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Schedule List : </label>
</div>

<div class="item form-group form-item" id="schedulelist" <?php if($type != '2'){ echo 'style="display:none"'; }?>>
	<table id="listschedule" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Jadwal</th>
				<th>Qty</th>
				<th>Note</th>
				<th>Option</th>
			</tr>
		</thead>
		<tbody>
			<?php
				$i=0; 
				foreach($detail as $key) {?>
				<tr id="shipping_temp<?php echo $i;?>">
					<td><?php if(isset($key['date_shipping_plan'])){ echo $key['date_shipping_plan']; }?></td>
					<td><?php if(isset($key['qty_shipping'])){ echo $key['qty_shipping']; }?></td>
					<td><?php if(isset($key['note_shipping'])){ echo $key['note_shipping']; }?></td>
					<td>
						<button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteShipping('<?php echo $i;?>')">
							<i class="fa fa-trash"></i>
						</button>
					</td>
				</tr>
			<?php $i++;} ?>
		</tbody>
	</table>
</div>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<button id="btn-addschedule" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Schedule</button>
	</div>
</div>
<input type="hidden" id="id_order" value="<?php if(isset($id)){ echo $id; }?>">
<input type="hidden" id="price" value="<?php if(isset($price)){ echo $price; }?>">
<!-- /page content -->

<script type="text/javascript">
    var scheduleArray = [];

    "<?php
    foreach($detail as $key) {
      $type = $key['type_shipping'];
      $qty = $key['qty_shipping'];
      $note_shipping = $key['note_shipping'];
      $date_shipping_plan = $key['date_shipping_plan'];
  ?>"
      var id_order = "<?php echo $id; ?>";
      var price = "<?php echo $price; ?>";
      var type = "<?php echo $type; ?>";
      var qty = "<?php echo $qty; ?>";
      var notes = "<?php echo $note_shipping; ?>";
      var date = "<?php echo $date_shipping_plan; ?>";

      var scheduleData = {
				"id_order":id_order,
				"price":price,
				"type_shipping":type,
				"qty_shipping":qty,
				"note_shipping":notes,
				"date_shipping_plan":date
			};

	  scheduleArray.push(scheduleData);
  "<?php    
    }
  ?>"

	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();

		if(scheduleArray.length){
			var maxqty = 0;
			for(var k=0;k<scheduleArray.length;k++){
				maxqty = maxqty + parseInt(scheduleArray[k]["qty_shipping"]);
			}

			$('#qty').attr('max',parseInt($('#inpt-qty'+$('#id_order').val()).val())-maxqty);
		}else{
			$('#qty').attr('max',$('#inpt-qty'+$('#id_order').val()).val());
		}

		$("#date_plan").datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});

		$( "input[name='type']" ).change(function() {
			if($( this ).val()==='2'){
				$('#btn-addtemp,#scheduletext,#schedulelist').show();
			}else{
				$('#btn-addtemp,#scheduletext,#schedulelist').hide();
				scheduleArray = [];

				var element ='<tr>';
			        element +='	<td>Jadwal</td>';
			        element +=' <td>Qty</td>';
			        element +=' <td>Note</td>';
			        element +=' <td>Option</td>';
			        element +='</tr>';

			    $('#listschedule tbody').html(element);
			}
		});

		$( "#btn-add" ).click(function() {
			var type = $('input[name="type"]:checked').val(),
				qty = $('#qty').val(),
				notes = $('#notes').val(),
				id_order = $('#id_order').val(),
				price = $('#price').val(),
				date = '';
					
			if(type==='2'){date = $('#date_plan').val();}

			var scheduleData = {
				"id_order":id_order,
				"price":price,
				"type_shipping":type,
				"qty_shipping":qty,
				"note_shipping":notes,
				"date_shipping_plan":date
			};

			scheduleArray.push(scheduleData);
			
			var qtymax = $('#qty').attr('max');
			$('#qty').attr('max',qtymax-qty);

			$('#qty').val(0);

			listShipping();
		});
	});

	function listShipping(){
		var scheduleLen = scheduleArray.length;

		var element ='';
	    for(var i=0;i<scheduleLen;i++){
	        element +='   <tr>';
	        element +='     <td>'+scheduleArray[i]["date_shipping_plan"]+'</td>';
	        element +='     <td>'+scheduleArray[i]["qty_shipping"]+'</td>';
	        element +='     <td>'+scheduleArray[i]["note_shipping"]+'</td>';
	        element +='     <td>';
	        element +='       <div class="btn-group">';
	        element +='         <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteShipping('+i+','+scheduleArray[i]["qty_shipping"]+')">';
	        element +='         <i class="fa fa-trash"></i>';
	        element +='       </div>';
	        element +='     </td>';
	        element +='   </tr>';
	    }

	    $('#listschedule tbody').html(element);
	}

	function deleteShipping(id,max){
		$('#shipping_temp'+id).remove();
	    scheduleArray.splice(id,1);

	    var qtymax = $('#qty').attr('max');
		$('#qty').attr('max',qtymax+max);

	    listShipping();
	}

	$('#btn-addschedule').on('click',(function(e) {
		$(this).attr('disabled','disabled');
	    $(this).text("Mengubah data...");

		if(scheduleArray.length === 0){
			var type = $('input[name="type"]:checked').val(),
				qty = $('#qty').val(),
				notes = $('#notes').val(),
				id_order = $('#id_order').val(),
				price = $('#price').val(),
				date = '';
					
			if(type==='2'){date = $('#date_plan').val();}

			var scheduleData = {
				"id_order":id_order,
				"price":price,
				"type_shipping":type,
				"qty_shipping":qty,
				"note_shipping":notes,
				"date_shipping_plan":date
			};

			scheduleArray.push(scheduleData);
		}

		$.ajax({
            type:'POST',
            url: "<?php echo base_url();?>po/doedit_schedule",
            data:JSON.stringify(scheduleArray),
            cache:false,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response.success == true) {
                    $('#panel-modalchild .panel-heading button').trigger('click');
                    $('#delivery'+$('#id_order').val()).html(response.schedule.delivery);
                } else{
                    $('#btn-addschedule').removeAttr('disabled');
                    $('#btn-addschedule').text("Edit Schedule");
                    swal("Failed!", response.message, "error");
                }
            }
        }).fail(function(xhr, status, message) {
            $('#btn-addschedule').removeAttr('disabled');
            $('#btn-addschedule').text("Edit Schedule");
            swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
        });
	}));
</script>
