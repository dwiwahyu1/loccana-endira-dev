	<style>
		#loading-us{display:none}
		#tick{display:none}

		#loading-mail{display:none}
		#cross{display:none}
		#add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
		#add_item:hover{color:#ff8c00}
		.dt-body-center{text-align:center}
		.dt-body-left{text-align:left}
		.dt-body-right{text-align:right}
	</style>
	
<div class="row">
	<div class="col-md-12">
		<div class="col-md-12 text-center">
			<div class="col-md-12">
				<h3>PURCHASE REQUEST</h3>
			</div>
		</div>
		<div class="pull-right">
			<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download PDF" id="btn_download">
				<i class="fa fa-download"></i>
			</a>
		</div>
	</div>
</div>

<div class="row" style="display: none;">
	<div class="col-md-12" style="display: none;">
		<div class="col-md-10">
			<div class="col-md-12 titleReport">
				<h1 id="titleMenu">PURCHASE REQUEST</h1>
			</div>
		</div>
	</div>
</div>

<hr>

<div class="row">
	<div class="col-md-12">
		<div class="row margin-bawah">
			<div class="col-md-1">
				<label class="col-md-12 col-sm-12 col-xs-12" style="" id="ms">M/S</label>
			</div>
			<div class="col-md-7">
				<label class="col-md-12 col-sm-12 col-xs-12" style="" id="titlePerusahaan">: PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</label>
			</div>
			<div class="col-md-4">
				<div class="col-md-4 col-md-offset-3">
					<label class="col-md-6 col-sm-6 col-xs-6" style="" id="titleNoPR">PR No.</label>
				</div>
				<div class="col-md-5">
					<label class="col-md-12 col-sm-12 col-xs-12" style="" id="no_pr">: <?php if(isset($detail[0]['no_spb'])) echo trim($detail[0]['no_spb']); ?></label>
				</div>
			</div>
		</div>
		<div class="row margin-bawah">
			<div class="col-md-5 col-md-offset-1">
				<label class="col-md-12 col-sm-12 col-xs-12" style="" id="titleAlamat">Jl. Buah Dua No. 168 Rancaekek - Bandung</label>
			</div>
			<div class="col-md-2 pull-left">
				<label class="col-md-12 col-sm-12 col-xs-12" style="" id="titleNonRutin">0 NON RUTIN</label>
			</div>
			<div class="col-md-4">
				<div class="col-md-4 col-md-offset-3">
					<label class="col-md-6 col-sm-6 col-xs-6" style="" id="titleDate">DATE</label>
				</div>
				<div class="col-md-5">
					<label class="col-md-12 col-sm-12 col-xs-12" style="" id="date">: <?php if(isset($detail[0]['tanggal_spb'])) $date = date_create($detail[0]['tanggal_spb']); echo date_format($date,'d-m-Y'); ?></label>
				</div>
			</div>
		</div>
		<div class="row margin-bawah">
			<div class="col-md-1">
				<label class="col-md-12 col-sm-12 col-xs-12" style="" id="titleAttn">ATTN</label>
			</div>
			<div class="col-md-5">
				<label class="col-md-12 col-sm-12 col-xs-12" style="" id="attn">:</label>
			</div>
			<div class="col-md-6 pull-left">
				<label class="col-md-12 col-sm-12 col-xs-12" style="" id="titlePenting">0 PENTING</label>
			</div>
		</div>
		<div class="row margin-bawah">
			<div class="col-md-1">
				<label class="col-md-12 col-sm-12 col-xs-12" style="" id="titlePhone">Phone</label>
			</div>
			<div class="col-md-5">
				<label class="col-md-12 col-sm-12 col-xs-12" style="" id="phone">:</label>
			</div>
			<div class="col-md-2 pull-left">
				<label class="col-md-12 col-sm-12 col-xs-12" style="" id="titleBiasa">0 Biasa</label>
			</div>
			<div class="col-md-4">
				<div class="col-md-4 col-md-offset-3">
					<label class="col-md-8 col-sm-8 col-xs-8" style="" id="titleSPBNo">SPB No.</label>
				</div>
				<div class="col-md-5">
					<label class="col-md-12 col-sm-12 col-xs-12" style="" id="SPBNo">:</label>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-md-12">
		<hr>
	</div>
	
	<div class="col-md-12">
		<div class="row">
			<table id="listaddedmaterial" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>NO</th>
						<th>KODE BARANG</th>
						<th>NAMA BARANG</th>
						<th>QTY</th>
						<th>SATUAN</th>
						<th>TGL Dibutuhkan</th>
						<th>Keterangan</th>
						<th>Supplier I</th>
						<th>Supplier II</th>
						<th>User</th>
					 </tr>
				</thead>
				<tbody>
					<?php /*$items = $detail;
						$itemsLen = count($items);
						if($itemsLen){
							$no = 0;
							for($i=0;$i<$itemsLen;$i++){ ?>
							<tr>
								<td><?php echo $no+1; ?></td>
								<td><?php echo $detail[$i]['stock_code']; ?></td>
								<td><?php echo $detail[$i]['nama_barang']; ?></td>
								<td><?php echo $detail[$i]['qty']; ?></td>
								<td><?php echo $detail[$i]['uom_symbol']; ?></td>
								<td><?php if(isset($detail[0]['tanggal_diperlukan'])) $date = date_create($detail[0]['tanggal_diperlukan']); echo date_format($date,'d-m-Y'); ?></td>
								<td colspan="4"><?php echo $detail[$i]['desc']; ?></td>
							</tr>
						<?php }
						}	
					*/ ?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="col-md-12" style="margin-bottom:50px">
		<div class="col-md-2 col-md-offset-2">
			<label class="col-md-12 col-sm-12 col-xs-12 text-center" style="" id="titlePreparedBy">Prepered By,</label>
		</div>
		<div class="col-md-2">
			<label class="col-md-12 col-sm-12 col-xs-12 text-center" style="" id="titleCheckedBy">Checked By,</label>
		</div>
		<div class="col-md-2">
			<label class="col-md-12 col-sm-12 col-xs-12 text-center" style="" id="titleRequiredBy">Required By,</label>
		</div>
		<div class="col-md-2">
			<label class="col-md-12 col-sm-12 col-xs-12 text-center" style="" id="titleApprovedBy">Approved By,</label>
		</div>
	</div>
	<div class="col-md-12">
		<div class="col-md-2 col-md-offset-2">
			<input type="text" class="form-control" id="prepered" name="prepered" value="" placeholder="">
		</div>
		<div class="col-md-2">
			<input type="text" class="form-control" id="checked" name="checked" value="" placeholder="">
		</div>
		<div class="col-md-2">
			<input type="text" class="form-control" id="required" name="required" value="" placeholder="">
		</div>
		<div class="col-md-2">
			<input type="text" class="form-control" id="approved" name="approved" value="" placeholder="">
		</div>
	</div>
	<div class="col-md-12">
		<div class="col-md-2 col-md-offset-2">
			<label class="col-md-12 col-sm-12 col-xs-12 text-center" style="" id="titleAdm">(ADM Dept)</label>
		</div>
		<div class="col-md-2">
			<label class="col-md-12 col-sm-12 col-xs-12 text-center" style="" id="titleStore">(Store Dept)</label>
		</div>
		<div class="col-md-2">
			<label class="col-md-12 col-sm-12 col-xs-12 text-center" style="" id="titleAccounting">(Accounting Dept)</label>
		</div>
		<div class="col-md-2">
			<label class="col-md-12 col-sm-12 col-xs-12 text-center" style="" id="titleFactory">(Factory Directure)</label>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<label class="control-label pull-right" id="kode_report">F-FCM-001</label>
	</div>
	<div class="col-md-12">
		<label class="control-label pull-right" id="kode_report_detail">REV 00</label>
	</div>
</div>
<!-- /page content -->

<script type="text/javascript">
var items = [];
var dataImage = null;
var t_prList;

$(document).ready(function() {
	$('form').parsley();
	$('[data-toggle="tooltip"]').tooltip();

	$("#spb_date").datepicker({
		format: 'yyyy-mm-dd',
		autoclose: true,
		todayHighlight: true,
	});
	
	dtPurchaseRequest();
});

function dtPurchaseRequest() {

	t_prList = $('#listaddedmaterial').DataTable({
		"processing": true,
		"searching": false,
		"responsive": true,
		"lengthChange": false,
		"info": false,
		"bSort": false,
		"destroy": true,
		"ajax": {
			"type": "GET",
			"url": "<?php echo base_url() . 'spb/detail_no_spb/' . $detail[0]['no_spb']; ?>"
		},
		"columnDefs": [{
			"targets": [0],
			"searchable": false,
			"visible": true,
			"className": 'dt-body-center',
			"width": 10
		}],
		createdRow: function(row, data, dataIndex){
			if(data[1] !== ''){
				$('td:eq(6)', row).attr('colspan', 4);
				$('td:eq(6)', row).attr('align', 'center');
				
				$('td:eq(7)', row).css('border', 'none');
				$('td:eq(7)', row).css('display', 'none');
				$('td:eq(8)', row).css('border', 'none');
				$('td:eq(8)', row).css('display', 'none');
				$('td:eq(9)', row).css('border', 'none');
				$('td:eq(9)', row).css('display', 'none');

				// Update cell data
				this.api().cell($('td:eq(6)', row)).data(data[6]);
			}
		}
	})
}
		
function toDataURL(url, callback) {
	var xhr = new XMLHttpRequest();
	xhr.onload = function() {
		var reader = new FileReader();
		reader.onloadend = function() {
			callback(reader.result);
		}
		reader.readAsDataURL(xhr.response);
	};
	xhr.open('GET', url);
	xhr.responseType = 'blob';
	xhr.send();
}

toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
	dataImage = dataUrl;
})

$('#btn_download').on('click', function() {
	var doc = new jsPDF("a4");
	var imgData = dataImage;
	var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
	var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

	doc.setFontSize(8);
	doc.setFontType("bold");
	doc.text($('#titleMenu').html(), 105, 15, 'center');

	doc.setFontSize(7);
	doc.setFontStyle('arial','arial','sans-serif','bold');

	doc.text($('#ms').html(), 5, 20, 'left');
	doc.text($('#titlePerusahaan').html(), 25, 20, 'left');
	doc.text($('#titleNoPR').html(), 165, 20, 'left');
	doc.text($('#no_pr').html(), 180, 20, 'left');
	
	doc.text($('#titleAlamat').html(), 25, 25, 'left');
	doc.text($('#titleNonRutin').html(), 90, 25, 'left');
	doc.text($('#titleDate').html(), 165, 25, 'left');
	doc.text($('#date').html(), 180, 25, 'left');
	
	doc.text($('#titleAttn').html(), 5, 30, 'left');
	doc.text($('#attn').html(), 25, 30, 'left');
	doc.text($('#titlePenting').html(), 90, 30, 'left');
	
	doc.text($('#titlePhone').html(), 5, 35, 'left');
	doc.text($('#phone').html(), 25, 35, 'left');
	doc.text($('#titleBiasa').html(), 90, 35, 'left');
	doc.text($('#titleSPBNo').html(), 165, 35, 'left');
	doc.text($('#SPBNo').html(), 180, 35, 'left');
	
	doc.autoTable({
		html 			: '#listaddedmaterial',
		theme			: 'plain',
		styles			: {
			fontSize 	: 8, 
			lineColor	: [116, 119, 122],
			lineWidth	: 0.1,
			cellWidth 	: 'auto',
			
		},
		margin 			: 4.5,
		tableWidth		: (pageWidth - 10),
		headStyles		: {
			valign		: 'middle', 
			halign		: 'center',
		},
		columns 		: [0,1,2,3,4,5,6,7,8,9],
		/*didParseCell	: function (data) {
			if(data.table.foot[0]) {
				if(data.table.foot[0].cells[3]) {
					data.table.foot[0].cells[3].styles.halign = 'right';
				}
				if(data.table.foot[0].cells[4]) {
					data.table.foot[0].cells[4].styles.balign = 'right';
				}
				if(data.table.foot[0].cells[5]) {
					data.table.foot[0].cells[5].styles.balign = 'right';
				}
			}
		},*/
		columnStyles	: {
			0: {halign: 'center'},
			1: {halign: 'center'},
			2: {halign: 'center', width: '150px'},
			3: {halign: 'center'},
			4: {halign: 'center'},
			5: {halign: 'center'},
			6: {halign: 'center'},
			7: {halign: 'center'},
			8: {halign: 'center'},
			9: {halign: 'center'}
		},
		rowPageBreak	: 'auto',
		showHead 		: 'firstPage',
		showFoot		: 'lastPage',
		startY			: 37
	});
	
	var lastTable =  doc.autoTable.previous.finalY;
	
	var x = pageWidth * 80 / 100;
	var y = pageHeight * 35 / 100;
	
	doc.text($('#titlePreparedBy').html(), 60, lastTable + 5);
	doc.text($('#titleCheckedBy').html(), 90, lastTable + 5);	
	doc.text($('#titleRequiredBy').html(), 120, lastTable + 5);	
	doc.text($('#titleApprovedBy').html(), 150, lastTable + 5);
	
	doc.text($('#prepered').val(), 60, lastTable + 20);
	doc.text($('#checked').val(), 90, lastTable + 20);	
	doc.text($('#required').val(), 120, lastTable + 20);	
	doc.text($('#approved').val(), 150, lastTable + 20);
	
	doc.text($('#titleAdm').html(), 60, lastTable + 23);
	doc.text($('#titleStore').html(), 90, lastTable + 23);	
	doc.text($('#titleAccounting').html(), 120, lastTable + 23);	
	doc.text($('#titleFactory').html(), 150, lastTable + 23);
	
	doc.text($('#kode_report').html(), 205, lastTable + 5, 'right');
	doc.text($('#kode_report_detail').html(), 205, lastTable + 10, 'right');
	
	doc.save('PURCHASE REQUEST REPORT_<?php if(isset($detail[0]['no_spb'])) echo $detail[0]['no_spb'];?>.pdf');
});
</script>
