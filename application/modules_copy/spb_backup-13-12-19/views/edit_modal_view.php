	<style>
		#loading-us{display:none}
		#tick{display:none}

		#loading-mail{display:none}
		#cross{display:none}
		#add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
		#add_item:hover{color:#ff8c00}
		
		.add_item{cursor:pointer;#96b6e8;padding-top: 6px;}
		.add_item:hover{color:#ff8c00}
		.add_item a:hover{color:#ff8c00}
	</style>
<form class="form-horizontal form-label-left" id="edit_spb" role="form" action="<?php echo base_url('spb/edit_spb');?>" method="post" enctype="multipart/form-data" data-parsley-validate>


	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<br>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">PR No
			<span class="required">
				<sup>*</sup>
			</span>
		</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="text" id="no_spb" name="no_spb" class="form-control col-md-7 col-xs-12" placeholder="Spb No" value="<?php if(isset($detail[0]['no_spb'])){ echo $detail[0]['no_spb']; }?>" readonly>
		</div>
	</div>
		
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">PR Date
			<span class="required">
				<sup>*</sup>
			</span>
		</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<div class="input-group date">
				<input placeholder="PR Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="spb_date" name="spb_date" required="required" value="<?php if(isset($detail[0]['tanggal_spb'])){ echo $detail[0]['tanggal_spb']; }?>">
					<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Detail Customer : </label>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Username Login 
		  <span class="required">
			<sup>*</sup>
		  </span>
		</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="text" id="cust_name" name="cust_name" class="form-control col-md-7 col-xs-12" placeholder="Customer Name" required="required"  value="<?php if(isset($detail[0]['cust_name'])){ echo $detail[0]['cust_name']; }?>" readonly>
			<input type="hidden" id="id_cust" name="id_cust" value="<?php if(isset($detail[0]['id_cust'])){ echo $detail[0]['id_cust']; }?>">     
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Department 
			<span class="required">
				<sup>*</sup>
			</span>
		</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="text" id="department" name="department" class="form-control col-md-7 col-xs-12" placeholder="Department" required="required" value="<?php if(isset($detail[0]['departemen'])){ echo $detail[0]['departemen']; }?>" readonly>
					
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Barang : </label>
		<div class="col-md-8 col-sm-6 col-xs-12 add_item" onclick="add_item()">
			<a type="button" class="btn btn-default btn-icon waves-effect waves-light m-b-5">
				<i class="fa fa-plus"></i>
			</a>
			Tambah Barang
		</div>
		<input type="hidden" id="id_spb" name="id_spb" value="<?php if(isset($detail[0]['id'])){ echo $detail[0]['id']; }?>">
	</div>

	<div class="item form-group">
		<table id="listaddedmaterial" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th style="display:none">ID SPB</th>
					<th style="display:none">NO SPB</th>
					<th>Kode Barang</th>
					<th>Nama Barang</th>
					<th>Satuan Unit</th>
					<th>Satuan</th>
					<th>Qty</th>
					<th>Qty Update</th>
					<th>Tanggal Diperlukan</th>
					<th>Tanggal Diperlukan Update</th>
					<th>Gudang ID</th>
					<th>Gudang</th>
					<th>Keterangan</th>
					<th>Option</th>
				</tr>
			</thead>
			<tbody>
			<?php /*
				$itemsLen = count($detail);
				$no = 0;
				if($itemsLen){
					for($i=0;$i<$itemsLen;$i++){
			?>
				<tr id="<?php echo $no;?>">
					<td style="display:none"><?php echo $detail[$i]['id']; ?></td>
					<td style="display:none"><?php echo $detail[$i]['no_spb']; ?></td>
					<td><?php echo $detail[$i]['stock_code']; ?></td>
					<td><?php echo $detail[$i]['nama_barang']; ?></td>
					<td><?php echo $detail[$i]['uom_symbol']; ?></td>
					<td><input type="number" class="form-control" id="qty<?=$i;?>" name="qty<?=$i;?>" style="text-align:right;" placeholder="Qty" value="<?php echo number_format($detail[$i]['qty'], 4, '.', '.'); ?>" step=".00001"></td>
					<td>
						<div class="input-group date">
							<input placeholder="PR Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="tanggal_diperlukan<?=$i;?>" name="tanggal_diperlukan<?=$i;?>" required="required" value="<?php echo $detail[$i]['tanggal_diperlukan']; ?>">
								<div class="input-group-addon">
								<span class="glyphicon glyphicon-th"></span>
							</div>
						</div>
					</td>
					<td><?php echo $detail[$i]['gudang']; ?></td>
					<td><?php echo $detail[$i]['desc']; ?></td>
					<td>
						<div class="btn-group">
							<button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="delete_spb_item('<?php echo $detail[$i]['id']; ?>',this)">
							<i class="fa fa-trash"></i>
						</div>
					</td>
				</tr>
			<?php $no++; } }*/ ?>
		  </tbody>
		</table>
	 </div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Edit Purchase Request</button>
		</div>
	</div>
</form><!-- /page content -->

<script type="text/javascript">
var items = [];
var rowParent = $(this).parents('tr');
var t_spb;
var last_items = <?php if(sizeof($detail) > 0) {$no = 0; for($i = 0; $i < sizeof($detail);$i++) { echo $no; } $no++;} ?>;

$(document).ready(function() {
	$('form').parsley();
	$('[data-toggle="tooltip"]').tooltip();

	$("#spb_date").datepicker({
		format: 'yyyy-mm-dd',
		autoclose: true,
		todayHighlight: true,
	});
	
	for(var y = 0; y <items.length;y++){
		$("#tanggal_diperlukan"+i).datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});
	}
	dtSPB();
});

function dtSPB() {
	t_spb = $("#listaddedmaterial").DataTable({
		"processing": true,
		"searching": false,
		"responsive": true,
		"lengthChange": false,
			"ajax": {
			"type" : "GET",
			"url" : "<?php echo base_url().'spb/lists_detail_spb/'.$detail[0]['no_spb'];?>"
		},
		"destroy": true,
		"info": false,
		"bSort": false,
		"columnDefs": [{
			"targets": [0,1,4,7,9,10],
			"searchable": false,
			"visible": false,
			"className": 'dt-body-center',
			"width": 120
		}],
	});
}

function redrawTable(row) {
	var rowData = t_spb.row(row).data();

	rowData[6] = $('#qty'+row).val();
	rowData[9] = $('#tanggal_diperlukan'+row).val();

	t_spb.draw();
}
	
$('#edit_spb').on('submit',(function(e) {
	if($('#id_cust').val() !== ''){
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Mengubah data...");
		
		e.preventDefault();
		
		var	no_spb 		= $('#no_spb').val();
		var	spb_date 	= $('#spb_date').val();
		var	id_cust 	= $('#id_cust').val();
		var	departemen 	= $('#department').val();
		
		for(var i = 0; i < t_spb.rows().data().length;i++) {
			var rowData = t_spb.row(i).data();
			
			items.push(rowData);
		}
		
		/*for(var i = 0; i < items.length;i++) {
			var qty = $('#qty'+i).val();
			var tanggal_diperlukan = $('#tanggal_diperlukan'+i).val();
			var itemsEdits = {
				qty,
				tanggal_diperlukan,
			};
			itemsEdit.push(itemsEdits);
		}*/
		
		var datapost = {
			"no_spb"      :   no_spb,
			"spb_date"    :   spb_date,
			"id_cust"     :   id_cust,
			"departemen"  :   departemen,
			"items"       :   items
		};
		
		$.ajax({
			type:'POST',
			url: $(this).attr('action'),
			data:JSON.stringify(datapost),
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
					  title: 'Success!',
					  text: response.message,
					  type: 'success',
					  showCancelButton: false,
					  confirmButtonText: 'Ok'
					}).then(function () {
					  $('.panel-heading button').trigger('click');
					  listspb();
					})
				} else{
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Edit Purchase Request");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Edit Purchase Request");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}else{
		swal("Failed!", "Harus menambah barang dahulu", "error");
	}
}));

<?php /*
	for($i=0;$i < sizeof($detail);$i++){
		$id_spb = $detail[$i]['id'];
		$no_spb = $detail[$i]['no_spb'];
		$nama_material = $detail[$i]['nama_material'];
		$stock_name = $detail[$i]['nama_barang'];
		$id_uom = $detail[$i]['id_uom'];
		$qty = $detail[$i]['qty'];
		$tgl_diperlukan = $detail[$i]['tanggal_diperlukan'];
		$gudang_id = $detail[$i]['id_gudang'];
		$gudang = $detail[$i]['gudang'];
		$desc = $detail[$i]['desc'];
		$uom_name = $detail[$i]['uom_name'];
		$uom_symbol = $detail[$i]['uom_symbol'];
		$notes = $detail[$i]['notes'];
?>
	var id_spb = "<?php echo $id_spb; ?>";
	var no_spb = "<?php echo $no_spb; ?>";
	var iditems = "<?php echo $nama_material; ?>";
	var item_name = "<?php echo $stock_name; ?>";
	var unit = "<?php echo $id_uom; ?>";
	var unitname = "<?php echo $uom_name; ?>";
	var unittext = "<?php echo $uom_symbol; ?>";
	var qty = "<?php echo $qty; ?>";
	var tgl_diperlukan = "<?php echo $tgl_diperlukan; ?>";
	var gudang_id = "<?php echo $gudang_id; ?>";
	var gudang = "<?php echo $gudang; ?>";
	var desc = "<?php echo trim($desc); ?>";
	var notes = "<?php echo $notes; ?>";

	var dataitem = {
		id_spb,
		no_spb,
		iditems,
		item_name,
		unit,
		qty,
		tgl_diperlukan,
		notes,
		gudang_id,
		gudang,
		desc,
		unittext
	};
	
	items.push(dataitem);
<?php }*/ ?>

// function deleteaddeditem(id) {
	// items.splice(id,1)

	// itemsElem();
// }

// function itemsElem(){
	// var itemslen = items.length; 
	// var element ='<thead>';
		// element +='   <tr>';
		// element +='     <th>ID SPB</th>';
		// element +='     <th>No SPB</th>';
		// element +='     <th>Kode Barang</th>';
		// element +='     <th>Nama Barang</th>';
		// element +='     <th>Satuan</th>';
		// element +='     <th>Qty</th>';
		// element +='     <th>Tanggal Diperlukan</th>';
		// element +='     <th>Gudang</th>';
		// element +='     <th>Keterangan</th>';
		// element +='     <th>Option</th>';
		// element +='   </tr>';
		// element +='</thead>';
		// element +='<tbody>';
	// for(var i=0;i < itemslen;i++){
		// element +='<tr>';
		// element +='     <td>'+items[i]["id_spb"]+'</td>';
		// element +='     <td>'+items[i]["no_spb"]+'</td>';
		// element +='     <td>'+items[i]["iditems"]+'</td>';
		// element +='     <td>'+items[i]["item_name"]+'</td>';
		// element +='     <td>'+items[i]["unittext"]+'</td>';
		// element +='     <td><input class="form-control" type="number" id="qty'+i+'" name="qty'+i+'" placeholder="Qty" style="text-align:right" value="'+items[i]["qty"]+'" step=".00001"></td>';
		// element +='     <td>';
		// element +='     	<div class="input-group date">';
		// element +='				<input placeholder="PR Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="tanggal_diperlukan'+i+'" name="tanggal_diperlukan'+i+'" required="required" value="'+items[i]["tgl_diperlukan"]+'">';
		// element +='				<div class="input-group-addon">';
		// element +='					<span class="glyphicon glyphicon-th"></span>';
		// element +='				</div>';
		// element +='			</div>';
		// element +='		</td>';
		// element +='     <td>'+items[i]["gudang"]+'</td>';
		// element +='     <td>'+items[i]["desc"]+'</td>';
		// element +='     <td>';
		// element +='       	<div class="btn-group">';
		// element +='         	<button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="delete_spb_item('+items[i]["id_spb"]+',this)">';
		// element +='         		<i class="fa fa-trash"></i>';
		// element +='         	</button>';
		// element +='       	</div>';
		// element +='     </td>';
		// element +='</tr>';
	// }
	// element +='</tbody>';
	
	// $('#listaddedmaterial').html(element);
// }
</script>
