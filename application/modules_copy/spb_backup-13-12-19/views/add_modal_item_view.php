	<style>
		#loading-us{display:none}
		#tick{display:none}

		.form-item{margin-top: 15px;overflow: auto;}
		.items-add{cursor:pointer;}
		.items-add:hover{color:#ff8c00}
		.x-hidden{display:none;}
		.sk-circle {
		  margin: 0px 250px;
		  width: 30px;
		  height: 30px;
		  position: relative;
		}
		.sk-circle .sk-child {
		  width: 100%;
		  height: 100%;
		  position: absolute;
		  left: 0;
		  top: 0;
		}
		.sk-circle .sk-child:before {
		  content: '';
		  display: block;
		  margin: 0 auto;
		  width: 15%;
		  height: 15%;
		  background-color: #6a6e6d;
		  border-radius: 100%;
		  -webkit-animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
				  animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
		}
		.sk-circle .sk-circle2 {
		  -webkit-transform: rotate(30deg);
			  -ms-transform: rotate(30deg);
				  transform: rotate(30deg); }
		.sk-circle .sk-circle3 {
		  -webkit-transform: rotate(60deg);
			  -ms-transform: rotate(60deg);
				  transform: rotate(60deg); }
		.sk-circle .sk-circle4 {
		  -webkit-transform: rotate(90deg);
			  -ms-transform: rotate(90deg);
				  transform: rotate(90deg); }
		.sk-circle .sk-circle5 {
		  -webkit-transform: rotate(120deg);
			  -ms-transform: rotate(120deg);
				  transform: rotate(120deg); }
		.sk-circle .sk-circle6 {
		  -webkit-transform: rotate(150deg);
			  -ms-transform: rotate(150deg);
				  transform: rotate(150deg); }
		.sk-circle .sk-circle7 {
		  -webkit-transform: rotate(180deg);
			  -ms-transform: rotate(180deg);
				  transform: rotate(180deg); }
		.sk-circle .sk-circle8 {
		  -webkit-transform: rotate(210deg);
			  -ms-transform: rotate(210deg);
				  transform: rotate(210deg); }
		.sk-circle .sk-circle9 {
		  -webkit-transform: rotate(240deg);
			  -ms-transform: rotate(240deg);
				  transform: rotate(240deg); }
		.sk-circle .sk-circle10 {
		  -webkit-transform: rotate(270deg);
			  -ms-transform: rotate(270deg);
				  transform: rotate(270deg); }
		.sk-circle .sk-circle11 {
		  -webkit-transform: rotate(300deg);
			  -ms-transform: rotate(300deg);
				  transform: rotate(300deg); }
		.sk-circle .sk-circle12 {
		  -webkit-transform: rotate(330deg);
			  -ms-transform: rotate(330deg);
				  transform: rotate(330deg); }
		.sk-circle .sk-circle2:before {
		  -webkit-animation-delay: -1.1s;
				  animation-delay: -1.1s; }
		.sk-circle .sk-circle3:before {
		  -webkit-animation-delay: -1s;
				  animation-delay: -1s; }
		.sk-circle .sk-circle4:before {
		  -webkit-animation-delay: -0.9s;
				  animation-delay: -0.9s; }
		.sk-circle .sk-circle5:before {
		  -webkit-animation-delay: -0.8s;
				  animation-delay: -0.8s; }
		.sk-circle .sk-circle6:before {
		  -webkit-animation-delay: -0.7s;
				  animation-delay: -0.7s; }
		.sk-circle .sk-circle7:before {
		  -webkit-animation-delay: -0.6s;
				  animation-delay: -0.6s; }
		.sk-circle .sk-circle8:before {
		  -webkit-animation-delay: -0.5s;
				  animation-delay: -0.5s; }
		.sk-circle .sk-circle9:before {
		  -webkit-animation-delay: -0.4s;
				  animation-delay: -0.4s; }
		.sk-circle .sk-circle10:before {
		  -webkit-animation-delay: -0.3s;
				  animation-delay: -0.3s; }
		.sk-circle .sk-circle11:before {
		  -webkit-animation-delay: -0.2s;
				  animation-delay: -0.2s; }
		.sk-circle .sk-circle12:before {
		  -webkit-animation-delay: -0.1s;
				  animation-delay: -0.1s; }

		@-webkit-keyframes sk-circleBounceDelay {
		  0%, 80%, 100% {
			-webkit-transform: scale(0);
					transform: scale(0);
		  } 40% {
			-webkit-transform: scale(1);
					transform: scale(1);
		  }
		}

		@keyframes sk-circleBounceDelay {
		  0%, 80%, 100% {
			-webkit-transform: scale(0);
					transform: scale(0);
		  } 40% {
			-webkit-transform: scale(1);
					transform: scale(1);
		  }
		}
	</style>

		<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
		<br>
		<hr>
		<div class="item form-group form-item">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Tipe Barang</label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<select class="form-control" id="type_barang" name="type_barang" required>
					<option value="0">Silahkan Pilih</option>
					<?php foreach($type_material as $material) { ?>
						<option value="<?php echo $material['id_type_material']; ?>"><?php echo $material['type_material_name']; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>

		<div class="item form-group form-item x-hidden" id="nama_mat">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_material">Nama Barang <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<select class="form-control" name="nama_material" id="nama_material" style="width: 100%" onchange="getSatuan();" required>
					<option value="">Silahkan Pilih</option>
				</select>
			</div>
		</div>
		
		<div class="item form-group form-item x-hidden" id="nama_satuan">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="satuan">Satuan Barang <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input type="text" class="form-control" id="satuan" name="satuan" value="" readonly>
			</div>
		</div>
		
		<div class="sk-circle x-hidden" id="loaders">
			<div class="sk-circle1 sk-child"></div>
			<div class="sk-circle2 sk-child"></div>
			<div class="sk-circle3 sk-child"></div>
			<div class="sk-circle4 sk-child"></div>
			<div class="sk-circle5 sk-child"></div>
			<div class="sk-circle6 sk-child"></div>
			<div class="sk-circle7 sk-child"></div>
			<div class="sk-circle8 sk-child"></div>
			<div class="sk-circle9 sk-child"></div>
			<div class="sk-circle10 sk-child"></div>
			<div class="sk-circle11 sk-child"></div>
			<div class="sk-circle12 sk-child"></div>
		</div>
		
		<div class="sk-circle x-hidden" id="loaders2">
			<div class="sk-circle1 sk-child"></div>
			<div class="sk-circle2 sk-child"></div>
			<div class="sk-circle3 sk-child"></div>
			<div class="sk-circle4 sk-child"></div>
			<div class="sk-circle5 sk-child"></div>
			<div class="sk-circle6 sk-child"></div>
			<div class="sk-circle7 sk-child"></div>
			<div class="sk-circle8 sk-child"></div>
			<div class="sk-circle9 sk-child"></div>
			<div class="sk-circle10 sk-child"></div>
			<div class="sk-circle11 sk-child"></div>
			<div class="sk-circle12 sk-child"></div>
		</div>
		
		<div class="item form-group form-item">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty">Qty <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input data-parsley-maxlength="255" type="number" id="qty" name="qty" class="form-control col-md-7 col-xs-12" placeholder="Qty" min="1" maxlength="100" value="1" onkeypress="javascript:return isNumber(event)" required="required" step=".00001">
			</div>
		</div>
		

		 <div class="item form-group form-item" style="overflow-y: hidden;height: 37.99px;">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_diperlukan">Tanggal Diperlukan </label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<div class="input-group date">
					<input placeholder="Tanggal Diperlukan" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="tanggal_diperlukan" name="tanggal_diperlukan" value="<?php echo date('Y-m-d');?>">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-th"></span>
					</div>
				</div>
			</div>
		</div>

		<div class="item form-group form-item">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="gudang_id">Gudang <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<select class="form-control" name="gudang_id" id="gudang_id" style="width: 100%" required>
					<?php foreach($gudang as $key) { ?>
						<option value="<?php echo $key['id_gudang'].','.$key['nama_gudang']; ?>" ><?php echo $key['nama_gudang']; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		
		<div class="item form-group form-item">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="keterangan">Keterangan</label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<textarea data-parsley-maxlength="255" type="text" id="desc" name="desc" class="form-control col-md-7 col-xs-12" placeholder="Keterangan"></textarea>
			</div>
		</div>
		
		<div class="item form-group form-item">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<button id="btn-submititem" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Barang</button>
			</div>
		</div>

<script type="text/javascript">
var addNew = [];

$(document).ready(function() {
	$('form').parsley();
	$('[data-toggle="tooltip"]').tooltip();

	$("#tanggal_diperlukan").datepicker({
		  format: 'yyyy-mm-dd',
		  autoclose: true,
		  todayHighlight: true,
	});
	
	$('#type_barang').select2({
		allowClear: true,
        placeholder: {
			"id": "",
			"text": "Cari Tipe Material"
		},
	});
	
	$('#nama_material').select2({
		allowClear: true,
        placeholder:  {
			"id": "",
            "text": "Cari Material"
		},
	});
});

$(function() {
    $('body').on('change', '#type_barang', function (e) {
        var _this = $(this); 
        var html = '';
        var type_barang_id = $(this).val(); 
        var type_barang_text = $('#type_barang option:selected').text(); 
        var item_name_id = $('#nama_material option:selected').val(); 

		$('#loaders').removeClass('x-hidden');
		$('#nama_mat').addClass('x-hidden');
		
		var datapost={
		  "type_barang" :   type_barang_id
		};
			
		$.ajax({
            type:'POST',
            url: '<?php echo base_url('spb/get_material');?>',
            data:JSON.stringify(datapost),
            cache:true,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response.success == true) {
					var material = response.material;
					for (var i = 0; i < material.length; i++) {
						var option_val = material[i]['id_material']; 
						var option_unit = material[i]['unit']; 
						var option_sat = material[i]['uom_name']; 
						var option_code = material[i]['stock_code'];
						var option_name = material[i]['stock_name'];
						html += "<option value='"+option_val+","+option_unit+","+option_sat+","+option_code+","+option_name+">"+option_code+" -- "+option_name+"</option>"; 
					} 
					$('#nama_material').val(null).trigger('change');
					$('#nama_material').html(html);
					$('#nama_mat').removeClass('x-hidden');
					$('#loaders').addClass('x-hidden');
                } else{
					html = "<option value=''>Silahkan Pilih</option>"; 
                    $('#nama_material').val(null).trigger('change');
					$('#nama_material').html(html);
					$('#nama_mat').removeClass('x-hidden');
					$('#loaders').removeClass('x-hidden');
                }
            }
        }).fail(function(xhr, status, message) {
            swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
        });
    }); 
}); 

function getSatuan() {
	var value_no = $("#nama_material").val();
	
	if(value_no != '' || value_no != null) {
		sendNo($("#nama_material option:selected").val()+','+value_no);
	}else{
		
	}
}

function sendNo(id) {
	material = id.split(',');
	id_material = material[0];
	
	var isi = '';
	$('#loaders2').removeClass('x-hidden');
	$('#nama_satuan').addClass('x-hidden');
	
	var datapost={
		"id_material" :   id_material
	};
		
	$.ajax({
		type:'POST',
		url: '<?php echo base_url('spb/get_satuan_by_material');?>',
		data:JSON.stringify(datapost),
		cache:true,
		contentType: false,
		processData: false,
		success: function(response) {
			if (response.success == true) {
				var material = response.material;
				for (var i = 0; i < material.length; i++) {
					var option_unit = material[i]['uom_name']; 
					var option_sat = material[i]['uom_name']; 
					
					if(option_unit != '' || option_unit != null && option_sat != '' || option_sat != null)
						var isi = '('+option_unit+') ' +option_sat;
					else
						var isi = 'data satuan tidak ada';
				} 
				$('#satuan').val(null).trigger('change');
				$('#satuan').val(isi);
				$('#nama_satuan').removeClass('x-hidden');
				$('#loaders2').addClass('x-hidden');
			} else{
				$('#nama_satuan').addClass('x-hidden');
				$('#loaders2').addClass('x-hidden');
			}
		}
	}).fail(function(xhr, status, message) {
		swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
	});
}

$('#btn-submititem').on('click',(function(e) {
	if($('#nama_material').val() === ''){
		swal("Warning", "Nama Barang harus diisi.", "error");
	} else {
		$(this).attr('disabled','disabled');
		$(this).text("Memasukkan data...");

		var vals = $('Select#nama_material option:selected').text();
		var vals2 = $('#gudang_id').val();
		var vals_material = $('Select#nama_material option:selected').val();
		
		var splits = vals.split(' -- ');
		var splits2 = vals2.split(',');
		var splits_material = vals_material.split(',');
		
		var id_spb 		= 0;
		var no_spb 		= 0;
		var iditems 	= splits_material[0];
		var kodeitems 	= splits[0];
		var itemname 	= splits[1];
		var satuans 	= splits_material[1];
		var nama_satuan = splits_material[2];
		var idgudang = splits2[0];
		var nama_gudang = splits2[1];
		
		var id_spb     		= id_spb,
			no_spb     		= no_spb,
			iditems     	= iditems,
			item_name     	= itemname,
			unit          	= satuans,
			qty           	= $('#qty').val(),
			tgl_diperlukan 	= $('#tanggal_diperlukan').val(),
			desc          	= $('#desc').val(),
			gudang_id  	  	= idgudang,
			gudang     	  	= nama_gudang,
			unittext      	= nama_satuan;
		
		
		if($('#qty').val() === ''){
			qty = $('#qty').val(1);
		}
		
		var dataitem = {
			id_spb,
			no_spb,
			iditems,
			item_name,
			unit,
			qty,
			tgl_diperlukan,
			gudang_id,
			gudang,
			desc,
			unittext 
		};
		
		$('#nama_material').val(null).trigger('change');
		//items.push(dataitem);
		t_spb.row.add([
			id_spb,
			no_spb,
			iditems,
			item_name,
			unit,
			unittext,
			'<input type="number" class="form-control text-right" id="qty'+last_items+'" name="qty'+last_items+'" style="text-align:right;" onchange="redrawTable('+last_items+')" placeholder="Qty" value="'+qty+'" step=".00001">',
			qty,
			'<div class="input-group date">'+
			'	<input placeholder="PR Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="tanggal_diperlukan'+last_items+'" name="tanggal_diperlukan'+last_items+'" required="required" onchange="redrawTable('+last_items+')"value="'+tgl_diperlukan+'">'+
			'	<div class="input-group-addon">'+
			'		<span class="glyphicon glyphicon-th"></span>'+
			'	</div>'+
			'</div>',
			tgl_diperlukan,
			gudang_id,
			gudang,
			desc,
			'<tr>'+
			'	<td>'+
			'       <div class="btn-group">'+
			'         	<button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="delete_spb_item('+id_spb+',this)">'+
			'         		<i class="fa fa-trash"></i>'+
			'         	</button>'+
			'       	</div>'+
			'     </td>'+
			'</tr>'
		]).draw();
		last_items++;
		$('#panel-modalchild .panel-heading button').trigger('click');
		
		//itemsElem();
	}
  
}));

function addNewItems(items) {
	var id_spb = items["id_spb"];
	var no_spb = items["no_spb"];
	var kode_stok = items["kode_stok"];
	var nama_stok = items["nama_stok"];
	var uomId = items["uomId"];
	var namaUom = items["namaUom"];
	var qty = items["qty"];
	var qty_null = items["qty"];
	var desk_stok = items["desk_stok"];
	var gudangId = items["gudangId"];
	var namaGudang = items["namaGudang"];
	
	$("#item_name").val(nama_stok+','+uomId+','+namaUom);
	if(qty != '') {
		$("#qty").val(qty);
	}else{
		$("#qty").val(1);
	}
	$("#gudang_id").val(gudangId+','+namaGudang);
	$("#desc").val(desk_stok);
	$("#notes").val(desk_stok);
	
}

function isNumber(evt) {
	var iKeyCode = (evt.which) ? evt.which : evt.keyCode
	if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
		return false;

	return true;
} 
</script>
