<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller {

    public function __construct()
	{
		parent::__construct();	
		//$this->load->model('Login_model');
	
	}
	
	public function index()
	{
		
			$this->template->load('maintemplate', 'home/views/home');
		
	}

	
}
