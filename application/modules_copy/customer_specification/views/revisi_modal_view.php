<style>
    #loading-us{display:none}
    #tick{display:none}

    #loading-mail{display:none}
    #cross{display:none}
    .multiple-input{float:left;font-weight: normal;}
    .multiple-span{line-height: 36px;vertical-align: sub;margin: 0px 5px;}
    .laminate-inpt{width:120px;margin-bottom: 0px;}
    .img-spec{margin-left: 190px;}
  </style>

  <?php
    switch($detail[0]['solder_resist']){
      case "red" :
      case "blue" :
      case "green" :
      case "black" :
      case "yellow" : $solder_resist = $detail[0]['solder_resist'];break;
      default : $solder_resist = "others";
    }

    switch($detail[0]['legend_circuit']){
      case "red" :
      case "blue" :
      case "green" :
      case "black" :
      case "yellow" : $legend_circuit = $detail[0]['legend_circuit'];break;
      default : $legend_circuit = "others";
    }

    switch($detail[0]['legend_component']){
      case "red" :
      case "blue" :
      case "green" :
      case "black" :
      case "yellow" : $legend_component = $detail[0]['legend_component'];break;
      default : $legend_component = "others";
    }

    switch($detail[0]['packing_standard']){
      case "local" :
      case "over sea" : $packing_standard = $detail[0]['packing_standard'];break;
      default : $packing_standard = "others";
    }
  ?>
  <form class="form-horizontal form-label-left" id="edit_customer_specification" role="form" action="<?php echo base_url('customer_specification/revisi_customer_specification');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

    <p style="text-align: center;">Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Customer Name<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
		<input data-parsley-maxlength="255" type="text" id="id_eksternal_n" name="id_eksternal_n" class="form-control col-md-7 col-xs-12" placeholder="Approved Laminate" required="required" value="<?php if(isset($detail[0]['name_eksternal'])){ echo $detail[0]['name_eksternal']; }?>" readOnly>
		<input data-parsley-maxlength="255" type="hidden" id="id_eksternal" name="id_eksternal" class="form-control col-md-7 col-xs-12" placeholder="Approved Laminate" required="required" value="0" readOnly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Part Number <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
		 <input data-parsley-maxlength="255" type="text" id="id_produk_n" name="id_produk_n" class="form-control col-md-7 col-xs-12" placeholder="Approved Laminate" required="required" value="<?php if(isset($detail[0]['stock_name'])){ echo $detail[0]['stock_name']; }?>" readOnly>
		 <input data-parsley-maxlength="255" type="hidden" id="id_produk" name="id_produk" class="form-control col-md-7 col-xs-12" placeholder="Approved Laminate" required="required" value="0" readOnly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Approved Laminate <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="approved_laminate" name="approved_laminate" class="form-control col-md-7 col-xs-12" placeholder="Approved Laminate" required="required" value="<?php if(isset($detail[0]['approved_laminate'])){ echo $detail[0]['approved_laminate']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Laminate Grade <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="laminate_grade" name="laminate_grade" class="form-control col-md-7 col-xs-12" placeholder="Approved Laminate" required="required" value="<?php if(isset($detail[0]['laminate_grade'])){ echo $detail[0]['laminate_grade']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Lam Thk Base Cooper <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="laminate" name="laminate" class="form-control laminate-inpt col-md-7 col-xs-12"  min="0" required="required" value="<?php if(isset($detail[0]['laminate'])){ echo $detail[0]['laminate']; }?>"> 
          <span class="multiple-span">mm</span>
        </label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="thickness" name="thickness" class="form-control laminate-inpt col-md-7 col-xs-12"  min="0" required="required" value="<?php if(isset($detail[0]['thickness'])){ echo $detail[0]['thickness']; }?>"> 
          <span class="multiple-span">micron</span>
        </label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="base_cooper" name="base_cooper" class="form-control laminate-inpt col-md-7 col-xs-12"  min="0" required="required" value="<?php if(isset($detail[0]['base_cooper'])){ echo $detail[0]['base_cooper']; }?>"> 
          <span class="multiple-span">OZ</span>
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">CO Logo / Type / UL Logo <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="co_logo" name="co_logo" style="width: 120px" required>
            <option value="yes" <?php if(isset($detail[0]['co_logo'])){ if( $detail[0]['co_logo'] == 'yes' ){ echo "selected"; } }?>>YES</option>
            <option value="no" <?php if(isset($detail[0]['co_logo'])){ if( $detail[0]['co_logo'] == 'no' ){ echo "selected"; } }?>>NO</option>
          </select>
        </label>
        <label class="multiple-input multiple-span" style="width: 43px;">type</label>
        <label class="multiple-input" style="font-weight: normal;">
          <input data-parsley-maxlength="255" type="text" id="type_logo" name="type_logo" class="form-control col-md-7 col-xs-12" placeholder="Type" <?php if(isset($detail[0]['co_logo'])){ if( $detail[0]['co_logo'] == 'yes' ){ echo "required"; } }?> style="width: 310px;" value="<?php if(isset($detail[0]['type_logo'])){ echo $detail[0]['type_logo']; }?>">
        </label>
      </div>
    </div>

    <div class="item form-group has-feedback">
      <div style="text-align: right;margin-right: 72px;">
        <?php
          if(isset($detail[0]['img_logo']) && $detail[0]['img_logo'] != ''){ 
        ?>
          <img src="<?php echo base_url() ."uploads/specification/". $detail[0]['img_logo']; ?>" id="img_logo_temp" style="width: 481.1px;height: 250px;"/>
        <?php
          } else {
        ?>
        <img src="<?php echo base_url();?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/crossword.png" id="img_logo_temp" style="width: 481.1px;height: 250px;"/>
        <?php
          } 
        ?>
      </div>
      
      <input type="hidden" id="img_logo_prev" name="img_logo_prev" value="<?php if(isset($detail[0]['img_logo'])){ echo $detail[0]['img_logo']; }?>">
      <div class="col-md-8 col-sm-6 col-xs-12 img-spec">
          <input type="file" class="form-control" id="img_logo" name="img_logo" data-height="110" accept=".jpg, .jpeg, .png"/>
          <span> Hanya format gambar jpg,jpeg,png dengan besaran max 9Mb yang diterima.</span>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">UL Recognition <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="ul_recognition" name="ul_recognition" class="form-control col-md-7 col-xs-12" placeholder="UL Recognition" required="required" value="<?php if(isset($detail[0]['ul_recognition'])){ echo $detail[0]['ul_recognition']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Date Code <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="date_code" name="date_code" style="width: 120px" required>
            <option value="yes" <?php if(isset($detail[0]['date_code'])){ if( $detail[0]['date_code'] == 'yes' ){ echo "selected"; } }?>>YES</option>
            <option value="no" <?php if(isset($detail[0]['date_code'])){ if( $detail[0]['date_code'] == 'no' ){ echo "selected"; } }?>>NO</option>
          </select>
        </label>
        <label class="multiple-input multiple-span" style="width: 43px;">type</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="date_type" name="date_type" class="form-control col-md-7 col-xs-12" placeholder="Type" <?php if(isset($detail[0]['date_code'])){ if( $detail[0]['date_code'] == 'yes' ){ echo "required"; } }?> style="width: 310px;" value="<?php if(isset($detail[0]['date_type'])){ echo $detail[0]['date_type']; }?>">
        </label>
      </div>
    </div>

    <div class="item form-group has-feedback">
      <div style="text-align: right;margin-right: 72px;">
        <?php
          if(isset($detail[0]['date_img']) && $detail[0]['date_img'] != ''){ 
        ?>
          <img src="<?php echo base_url() ."uploads/specification/". $detail[0]['date_img']; ?>" id="date_img_temp" style="width: 481.1px;height: 250px;"/>
        <?php
          } else {
        ?>
        <img src="<?php echo base_url();?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/crossword.png" id="date_img_temp" style="width: 481.1px;height: 250px;"/>
        <?php
          } 
        ?>
      </div>
      
      <input type="hidden" id="date_img_prev" name="date_img_prev" value="<?php if(isset($detail[0]['date_img'])){ echo $detail[0]['date_img']; }?>">
      <div class="col-md-8 col-sm-6 col-xs-12 img-spec">
          <input type="file" class="form-control" id="date_img" name="date_img" data-height="110" accept=".jpg, .jpeg, .png"/>
          <span> Hanya format gambar jpg,jpeg,png dengan besaran max 9Mb yang diterima.</span>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Solder Resist type <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="solder_resist_type" name="solder_resist_type" class="form-control col-md-7 col-xs-12" placeholder="UL Recognition" required="required" value="<?php if(isset($detail[0]['solder_resist_type'])){ echo $detail[0]['solder_resist_type']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Solder Resist <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="solder_resist" name="solder_resist" style="width: 120px" required>
            <option value="red" <?php if( $solder_resist == 'red' ){ echo "selected"; } ?>>Red</option>
            <option value="blue" <?php if( $solder_resist == 'blue' ){ echo "selected"; } ?>>Blue</option>
            <option value="green" <?php if( $solder_resist == 'green' ){ echo "selected"; } ?>>Green</option>
            <option value="black" <?php if( $solder_resist == 'black' ){ echo "selected"; } ?>>Black</option>
            <option value="white" <?php if( $solder_resist == 'white' ){ echo "selected"; } ?>>White</option>
            <option value="yellow" <?php if( $solder_resist == 'yellow' ){ echo "selected"; } ?>>Yellow</option>
            <option value="others" <?php if( $solder_resist == 'others' ){ echo "selected"; } ?>>Others</option>
          </select>
        </label>
        <label class="multiple-input" style="margin-left: 5px;">
          <input data-parsley-maxlength="255" type="text" id="solder_resist_others" name="solder_resist_others" class="form-control col-md-7 col-xs-12" placeholder="Others" value="<?php if($solder_resist == 'others'){ echo $detail[0]['solder_resist']; }?>" style="width: 358px;" <?php if( $solder_resist == 'others' ){ echo "required"; }else{ echo "disabled" ;} ?>>
        </label>
      </div>
    </div>
	
	<div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Under Coat 1<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="uc1" name="uc1" class="form-control col-md-7 col-xs-12" placeholder="UC1" value="<?php echo $detail[0]['uc1']; ?>" >
      </div>
    </div>	
	
	<div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Under Coat 2<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="uc2" name="uc2" class="form-control col-md-7 col-xs-12" placeholder="UC2" value="<?php echo $detail[0]['uc2']; ?>" >
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Legend Circuit <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="legend_circuit" name="legend_circuit" style="width: 120px" required>
            <option value="red" <?php if( $legend_circuit == 'red' ){ echo "selected"; } ?>>Red</option>
            <option value="blue" <?php if( $legend_circuit == 'blue' ){ echo "selected"; } ?>>Blue</option>
            <option value="green" <?php if( $legend_circuit == 'green' ){ echo "selected"; } ?>>Green</option>
            <option value="black" <?php if( $legend_circuit == 'black' ){ echo "selected"; } ?>>Black</option>
            <option value="white" <?php if( $legend_circuit == 'white' ){ echo "selected"; } ?>>White</option>
            <option value="yellow" <?php if( $legend_circuit == 'yellow' ){ echo "selected"; } ?>>Yellow</option>
            <option value="others" <?php if( $legend_circuit == 'others' ){ echo "selected"; } ?>>Others</option>
          </select>
        </label>
        <label class="multiple-input" style="margin-left: 5px;">
          <input data-parsley-maxlength="255" type="text" id="legend_circuit_others" name="legend_circuit_others" class="form-control col-md-7 col-xs-12" placeholder="Others" value="<?php if($legend_circuit == 'others'){ echo $detail[0]['legend_circuit']; }?>" style="width: 358px;" <?php if( $legend_circuit == 'others' ){ echo "required"; }else{ echo "disabled" ;} ?>>
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Legend Component <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="legend_component" name="legend_component" style="width: 120px" required>
            <option value="red" <?php if( $legend_component == 'red' ){ echo "selected"; } ?>>Red</option>
            <option value="blue" <?php if( $legend_component == 'blue' ){ echo "selected"; } ?>>Blue</option>
            <option value="green" <?php if( $legend_component == 'green' ){ echo "selected"; } ?>>Green</option>
            <option value="black" <?php if( $legend_component == 'black' ){ echo "selected"; } ?>>Black</option>
            <option value="white" <?php if( $legend_component == 'white' ){ echo "selected"; } ?>>White</option>
            <option value="yellow" <?php if( $legend_component == 'yellow' ){ echo "selected"; } ?>>Yellow</option>
            <option value="others" <?php if( $legend_component == 'others' ){ echo "selected"; } ?>>Others</option>
          </select>
        </label>
        <label class="multiple-input" style="margin-left: 5px;">
          <input data-parsley-maxlength="255" type="text" id="legend_component_others" name="legend_component_others" class="form-control col-md-7 col-xs-12" placeholder="Others" value="<?php if($legend_component == 'others'){ echo $detail[0]['legend_component']; }?>" style="width: 358px;" <?php if( $legend_component == 'others' ){ echo "required"; }else{ echo "disabled" ;} ?>>
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Carbon <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="carbon" name="carbon" class="form-control col-md-7 col-xs-12" placeholder="Carbon" required="required" value="<?php if(isset($detail[0]['carbon'])){ echo $detail[0]['carbon']; }?>" style="width: 120px;">
        </label>
        <label class="multiple-input multiple-span" style="width: 51px;">type</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="carbon_type" name="carbon_type" class="form-control col-md-7 col-xs-12" placeholder="Type" value="<?php if(isset($detail[0]['carbon_type'])){ echo $detail[0]['carbon_type']; }?>" style="width: 120px;">
        </label>
        <label class="multiple-input multiple-span" style="width: 51px;">spec</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="carbon_spec" name="carbon_spec" class="form-control col-md-7 col-xs-12" placeholder="Spec" value="<?php if(isset($detail[0]['carbon_spec'])){ echo $detail[0]['carbon_spec']; }?>" style="width: 120px;">
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Peelable <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="peelable" name="peelable" class="form-control col-md-7 col-xs-12" placeholder="Peelable" required="required" value="<?php if(isset($detail[0]['peelable'])){ echo $detail[0]['peelable']; }?>" style="width: 120px;">
        </label>
        <label class="multiple-input multiple-span" style="width: 51px;">type</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="peelable_type" name="peelable_type" class="form-control col-md-7 col-xs-12" placeholder="Type" value="<?php if(isset($detail[0]['peelable_type'])){ echo $detail[0]['peelable_type']; }?>" style="width: 120px;">
        </label>
        <label class="multiple-input multiple-span" style="width: 51px;">spec</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="peelable_spec" name="peelable_spec" class="form-control col-md-7 col-xs-12" placeholder="Spec" value="<?php if(isset($detail[0]['peelable_spec'])){ echo $detail[0]['peelable_spec']; }?>" style="width: 120px;">
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Plating <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="plating" name="plating" class="form-control col-md-7 col-xs-12" placeholder="Plating" required="required" value="<?php if(isset($detail[0]['plating'])){ echo $detail[0]['plating']; }?>" style="width: 120px;">
        </label>
        <label class="multiple-input" style="margin-left: 5px;">
          <input data-parsley-maxlength="255" type="text" id="plating_spec" name="plating_spec" class="form-control col-md-7 col-xs-12" placeholder="Spec" value="<?php if(isset($detail[0]['plating_spec'])){ echo $detail[0]['plating_spec']; }?>" style="width: 358px;">
        </label>
      </div>
    </div>

	<div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Shearing Cut<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="scut" name="scut" class="form-control col-md-7 col-xs-12" placeholder="Shearing Cut" value='<?php echo $detail[0]['shearing_cut']; ?>' required="required">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Process Molding <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="process_molding" name="process_molding" style="width: 120px" required>
            <option value="cnc" <?php if(isset($detail[0]['process_molding'])){ if( $detail[0]['process_molding'] == 'cnc' ){ echo "selected"; } }?>>CNC</option>
            <option value="punching" <?php if(isset($detail[0]['process_molding'])){ if( $detail[0]['process_molding'] == 'punching' ){ echo "selected"; } }?>>Punching</option>
          </select>
        </label>
        <label class="multiple-input multiple-span" style="width: 53px;">remarks</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="process_molding_remarks" name="process_molding_remarks" class="form-control col-md-7 col-xs-12" placeholder="Remarks" value="<?php if(isset($detail[0]['process_molding_remarks'])){ echo $detail[0]['process_molding_remarks']; }?>" style="width: 300px;">
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">V-Cut <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="v_cut" name="v_cut" style="width: 120px" required>
            <option value="yes" <?php if(isset($detail[0]['v_cut'])){ if( $detail[0]['v_cut'] == 'yes' ){ echo "selected"; } }?>>YES</option>
            <option value="no" <?php if(isset($detail[0]['v_cut'])){ if( $detail[0]['v_cut'] == 'no' ){ echo "selected"; } }?>>NO</option>
          </select>
        </label>
        <label class="multiple-input multiple-span" style="width: 53px;">spec</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="v_cut_spec" name="v_cut_spec" class="form-control col-md-7 col-xs-12" placeholder="Spec" <?php if(isset($detail[0]['v_cut'])){ if( $detail[0]['v_cut'] == 'yes' ){ echo "required"; } }?> style="width: 300px;" value="<?php if(isset($detail[0]['v_cut_spec'])){ echo $detail[0]['v_cut_spec']; }?>">
        </label>
      </div>
    </div>

    <div class="item form-group has-feedback">
      <div style="text-align: right;margin-right: 72px;">
        <?php
          if(isset($detail[0]['v_cut_img']) && $detail[0]['v_cut_img'] != ''){ 
        ?>
          <img src="<?php echo base_url() ."uploads/specification/". $detail[0]['v_cut_img']; ?>" id="v_cut_img_temp" style="width: 481.1px;height: 250px;"/>
        <?php
          } else {
        ?>
        <img src="<?php echo base_url();?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/crossword.png" id="v_cut_img_temp" style="width: 481.1px;height: 250px;"/>
        <?php
          } 
        ?>
      </div>
      
      <input type="hidden" id="v_cut_img_prev" name="v_cut_img_prev" value="<?php if(isset($detail[0]['v_cut_img'])){ echo $detail[0]['v_cut_img']; }?>">
      <div class="col-md-8 col-sm-6 col-xs-12 img-spec">
          <input type="file" class="form-control" id="v_cut_img" name="v_cut_img" data-height="110" accept=".jpg, .jpeg, .png"/>
          <span> Hanya format gambar jpg,jpeg,png dengan besaran max 9Mb yang diterima.</span>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Tin Roll / Hal <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="tin_roll" name="tin_roll" style="width: 120px" required>
            <option value="yes" <?php if(isset($detail[0]['tin_roll'])){ if( $detail[0]['tin_roll'] == 'yes' ){ echo "selected"; } }?>>YES</option>
            <option value="no" <?php if(isset($detail[0]['tin_roll'])){ if( $detail[0]['tin_roll'] == 'no' ){ echo "selected"; } }?>>NO</option>
          </select>
        </label>
        <label class="multiple-input multiple-span" style="width: 53px;">spec</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="tin_roll_spec" name="tin_roll_spec" class="form-control col-md-7 col-xs-12" placeholder="Spec" <?php if(isset($detail[0]['tin_roll'])){ if( $detail[0]['tin_roll'] == 'yes' ){ echo "required"; } }?> style="width: 300px;" value="<?php if(isset($detail[0]['tin_roll_spec'])){ echo $detail[0]['tin_roll_spec']; }?>">
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Finishing <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input" style="width: 100%;">
          <select class="form-control" id="finishing" name="finishing" required>
            <option value="osp" <?php if(isset($detail[0]['finishing'])){ if( $detail[0]['finishing'] == 'osp' ){ echo "selected"; } }?>>OSP</option>
            <option value="normal flux" <?php if(isset($detail[0]['finishing'])){ if( $detail[0]['finishing'] == 'normal flux' ){ echo "selected"; } }?>>Normal Flux</option>
          </select>
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Packing Standard <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="packing_standard" name="packing_standard" style="width: 120px" required>
            <option value="local" <?php if( $packing_standard == 'local' ){ echo "selected"; } ?>>Local</option>
            <option value="over sea" <?php if( $packing_standard == 'over sea' ){ echo "selected"; } ?>>Over Sea</option>
            <option value="others" <?php if( $packing_standard == 'others' ){ echo "selected"; } ?>>Others</option>
          </select>
        </label>
        <label class="multiple-input multiple-span" style="width: 53px;">others</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="packing_standard_others" name="packing_standard_others" class="form-control col-md-7 col-xs-12" placeholder="Others" style="width: 300px;" value="<?php if($packing_standard == 'others'){ echo $detail[0]['packing_standard']; }?>" <?php if( $packing_standard == 'others' ){ echo "required"; } ?>>
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Others Requirement</span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="others_req" name="others_req" class="form-control col-md-7 col-xs-12" placeholder="Others Requirement" value="<?php if(isset($detail[0]['others_req'])){ echo $detail[0]['others_req']; }?>">
      </div>
    </div>
    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Notes <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <textarea data-parsley-maxlength="255" type="text" id="notes" name="notes" class="form-control col-md-7 col-xs-12" placeholder="Notes"></textarea>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Customer Specification</button>
      </div>
    </div>

    <input type="hidden" id="id" name="id" value="<?php if(isset($detail[0]['id'])){ echo $detail[0]['id']; }?>">
    <input type="hidden" id="status" name="status" value="0">
  </form><!-- /page content -->

<script type="text/javascript">
  $(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();

    $(".select-control").select2();
  });
  $('#img_logo').on('change', (function(e) {
    if (this.files && this.files[0]) {
      var reader = new FileReader();
      reader.onload = (function(theFile) {
        var image = new Image();
        image.src = theFile.target.result;

        image.onload = function() {
          $("#img_logo_temp").attr('src', this.src);
        };
      });
      reader.readAsDataURL(this.files[0]);
    }
  }));

  $('#date_img').on('change', (function(e) {
    if (this.files && this.files[0]) {
      var reader = new FileReader();
      reader.onload = (function(theFile) {
        var image = new Image();
        image.src = theFile.target.result;

        image.onload = function() {
          $("#date_img_temp").attr('src', this.src);
        };
      });
      reader.readAsDataURL(this.files[0]);
    }
  }));

  $('#v_cut_img').on('change', (function(e) {
    if (this.files && this.files[0]) {
      var reader = new FileReader();
      reader.onload = (function(theFile) {
        var image = new Image();
        image.src = theFile.target.result;

        image.onload = function() {
          $("#v_cut_img_temp").attr('src', this.src);
        };
      });
      reader.readAsDataURL(this.files[0]);
    }
  }));
  $('#co_logo').on('change',(function(e) {
    if($(this).val() === 'no'){
      $( "#type_logo" ).prop( "required", false );
    }else{
      $( "#type_logo" ).prop( "required", true );
    }
  }));

  $('#date_code').on('change',(function(e) {
    if($(this).val() === 'no'){
      $( "#date_type" ).prop( "required", false );
    }else{
      $( "#date_type" ).prop( "required", true );
    }
  }));

  $('#v_cut').on('change',(function(e) {
    if($(this).val() === 'no'){
      $( "#v_cut_spec" ).prop( "required", false );
    }else{
      $( "#v_cut_spec" ).prop( "required", true );
    }
  }));

  $('#tin_roll').on('change',(function(e) {
    if($(this).val() === 'no'){
      $( "#tin_roll_spec" ).prop( "required", false );
    }else{
      $( "#tin_roll_spec" ).prop( "required", true );
    }
  }));

  $('#solder_resist').on('change',(function(e) {
    $( "#solder_resist_others" ).val('');
    if($(this).val() === 'others'){
      $( "#solder_resist_others" ).prop( "disabled", false );
      $( "#solder_resist_others" ).prop( "required", true );
    }else{
      $( "#solder_resist_others" ).prop( "disabled", true );
      $( "#solder_resist_others" ).prop( "required", false );
    }
  }));

  $('#legend_circuit').on('change',(function(e) {
    $( "#legend_circuit_others" ).val('');
    if($(this).val() === 'others'){
      $( "#legend_circuit_others" ).prop( "disabled", false );
      $( "#legend_circuit_others" ).prop( "required", true );
    }else{
      $( "#legend_circuit_others" ).prop( "disabled", true );
      $( "#legend_circuit_others" ).prop( "required", false );
    }
  }));

  $('#legend_component').on('change',(function(e) {
    $( "#legend_component_others" ).val('');
    if($(this).val() === 'others'){
      $( "#legend_component_others" ).prop( "disabled", false );
      $( "#legend_component_others" ).prop( "required", true );
    }else{
      $( "#legend_component_others" ).prop( "disabled", true );
      $( "#legend_component_others" ).prop( "required", false );
    }
  }));

  $('#packing_standard').on('change',(function(e) {
    $( "#packing_standard_others" ).val('');
    if($(this).val() === 'others'){
      $( "#packing_standard_others" ).prop( "disabled", false );
      $( "#packing_standard_others" ).prop( "required", true );
    }else{
      $( "#packing_standard_others" ).prop( "disabled", true );
      $( "#packing_standard_others" ).prop( "required", false );
    }
  })); 

  $('#edit_customer_specification').on('submit',(function(e) {
    $('#btn-submit').attr('disabled','disabled');
    $('#btn-submit').text("Mengubah data...");
    e.preventDefault();
    var formData = new FormData(this);

    $.ajax({
        type:'POST',
        url: $(this).attr('action'),
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(response) {
            if (response.success == true) {
              $('.panel-heading button').trigger('click');
                listcustspec();
                swal({
                  title: 'Success!',
                  text: response.message,
                  type: 'success',
                  showCancelButton: false,
                  confirmButtonText: 'Ok'
                }).then(function () {
                });
            } else{
                $('#btn-submit').removeAttr('disabled');
                $('#btn-submit').text("Edit Customer Specification");
                swal("Failed!", response.message, "error");
            }
        }
    }).fail(function(xhr, status, message) {
        $('#btn-submit').removeAttr('disabled');
        $('#btn-submit').text("Edit Customer Specification");
        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
    });
  }));
</script>
