  <style>
    #loading-us{display:none}
    #tick{display:none}

    #loading-mail{display:none}
    #cross{display:none}
    .multiple-input{float:left;font-weight: normal;}
    .multiple-span{line-height: 36px;vertical-align: sub;margin: 0px 5px;}
    .laminate-inpt{width:120px;margin-bottom: 0px;}
    .img-spec{margin-left: 190px;}
  </style>

  <form class="form-horizontal form-label-left" id="approval_customer_specification" role="form" action="<?php echo base_url('customer_specification/approval_customer_specification');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Approval Date</span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="others_req" name="others_req" class="form-control col-md-7 col-xs-12" placeholder="Others Requirement" value="<?php if(isset($detail[0]['approval_date'])){ echo $detail[0]['approval_date']; }?>" disabled>
      </div>
    </div>

    <div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Status 
          <span class="required">
            <sup>*</sup>
          </span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
            <select class="form-control" name="status" id="status" style="width: 100%" required disabled>
                <option value="1" <?php if(isset($detail[0]['status'])){ if( $detail[0]['status'] == '1' ){ echo "selected"; } }?>>Accept</option>
                <option value="2" <?php if(isset($detail[0]['status'])){ if( $detail[0]['status'] == '2' ){ echo "selected"; } }?>>Reject</option>
            </select>
        </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Notes <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <textarea data-parsley-maxlength="255" type="text" id="notes" name="notes" class="form-control col-md-7 col-xs-12" placeholder="Notes" disabled></textarea>
      </div>
    </div>
  </form><!-- /page content -->

<script type="text/javascript">
  $(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>