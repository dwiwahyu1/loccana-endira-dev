  <style>
    #loading-us{display:none}
    #tick{display:none}

    #loading-mail{display:none}
    #cross{display:none}
    .multiple-input{float:left;font-weight: normal;}
    .multiple-span{line-height: 36px;vertical-align: sub;margin: 0px 5px;}
    .laminate-inpt{width:120px;margin-bottom: 0px;}
    .img-spec{margin-left: 190px;}
  </style>

  <form class="form-horizontal form-label-left" id="add_customer_specification" role="form" action="<?php echo base_url('customer_specification/add_customer_specification');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

    <p style="text-align: center;">Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Customer Name<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <select class="form-control select-control" id="id_eksternal" name="id_eksternal" required>
          <option value="" >-- Select Customer --</option>
          <?php foreach($customer as $dk) { ?>
            <option value="<?php echo $dk['id']; ?>"><?php echo $dk['name_eksternal']; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Item Name <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <select class="form-control select-control" id="id_produk" name="id_produk" required>
          <option value="" >-- Select Item --</option>
          <?php foreach($item as $key) { ?>
            <option value="<?php echo $key['id']; ?>"><?php echo $key['stock_name']; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Approved Laminate <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="approved_laminate" name="approved_laminate" class="form-control col-md-7 col-xs-12" placeholder="Approved Laminate" required="required">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Laminate Grade <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="laminate_grade" name="laminate_grade" class="form-control col-md-7 col-xs-12" placeholder="Approved Laminate" required="required">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Lam Thk Base Cooper <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="laminate" name="laminate" class="form-control laminate-inpt col-md-7 col-xs-12"  min="0" value="0" required="required"> 
          <span class="multiple-span">mm</span>
        </label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="thickness" name="thickness" class="form-control laminate-inpt col-md-7 col-xs-12"  min="0" value="0" required="required"> 
          <span class="multiple-span">micron</span>
        </label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="base_cooper" name="base_cooper" class="form-control laminate-inpt col-md-7 col-xs-12"  min="0" value="0" required="required"> 
          <span class="multiple-span">OZ</span>
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">CO Logo / Type / UL Logo <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="co_logo" name="co_logo" style="width: 120px" required>
            <option value="yes">YES</option>
            <option value="no">NO</option>
          </select>
        </label>
        <label class="multiple-input multiple-span" style="width: 43px;">type</label>
        <label class="multiple-input" style="font-weight: normal;">
          <input data-parsley-maxlength="255" type="text" id="type_logo" name="type_logo" class="form-control col-md-7 col-xs-12" placeholder="Type" required="required" style="width: 310px;">
        </label>
      </div>
    </div>

    <div class="item form-group has-feedback">
      <div class="col-md-8 col-sm-6 col-xs-12 img-spec">
          <input type="file" class="form-control" id="img_logo" name="img_logo" data-height="110" accept=".jpg, .jpeg, .png"/>
          <span> Hanya format gambar jpg,jpeg,png dengan besaran max 9Mb yang diterima.</span>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">UL Recognition <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="ul_recognition" name="ul_recognition" class="form-control col-md-7 col-xs-12" placeholder="UL Recognition" required="required">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Date Code <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="date_code" name="date_code" style="width: 120px" required>
            <option value="yes">YES</option>
            <option value="no">NO</option>
          </select>
        </label>
        <label class="multiple-input multiple-span" style="width: 43px;">type</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="date_type" name="date_type" class="form-control col-md-7 col-xs-12" placeholder="Type" required="required" style="width: 310px;">
        </label>
      </div>
    </div>

    <div class="item form-group has-feedback">
      <div class="col-md-8 col-sm-6 col-xs-12 img-spec">
          <input type="file" class="form-control" id="date_img" name="date_img" data-height="110" accept=".jpg, .jpeg, .png"/>
          <span> Hanya format gambar jpg,jpeg,png dengan besaran max 9Mb yang diterima.</span>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Solder Resist Type <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="solder_resist_type" name="solder_resist_type" class="form-control col-md-7 col-xs-12" placeholder="Solder Resist Type" required="required">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Solder Resist <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="solder_resist" name="solder_resist" style="width: 120px" required>
            <option value="red">Red</option>
            <option value="blue">Blue</option>
            <option value="green">Green</option>
            <option value="black">Black</option>
            <option value="white">White</option>
            <option value="yellow">Yellow</option>
            <option value="others">Others</option>
          </select>
        </label>
        <label class="multiple-input" style="margin-left: 5px;">
          <input data-parsley-maxlength="255" type="text" id="solder_resist_others" name="solder_resist_others" class="form-control col-md-7 col-xs-12" placeholder="Others" style="width: 358px;" disabled>
        </label>
      </div>
    </div>
	
	<div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Under Coat 1<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="uc1" name="uc1" class="form-control col-md-7 col-xs-12" placeholder="UC1" required="required">
      </div>
    </div>	
	
	<div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Under Coat 2<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="uc2" name="uc2" class="form-control col-md-7 col-xs-12" placeholder="UC2" required="required">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Legend Circuit <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="legend_circuit" name="legend_circuit" style="width: 120px" required>
            <option value="red">Red</option>
            <option value="blue">Blue</option>
            <option value="green">Green</option>
            <option value="black">Black</option>
            <option value="white">White</option>
            <option value="yellow">Yellow</option>
            <option value="others">Others</option>
          </select>
        </label>
        <label class="multiple-input" style="margin-left: 5px;">
          <input data-parsley-maxlength="255" type="text" id="legend_circuit_others" name="legend_circuit_others" class="form-control col-md-7 col-xs-12" placeholder="Others" style="width: 358px;" disabled>
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Legend Component <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="legend_component" name="legend_component" style="width: 120px" required>
            <option value="red">Red</option>
            <option value="blue">Blue</option>
            <option value="green">Green</option>
            <option value="black">Black</option>
            <option value="white">White</option>
            <option value="yellow">Yellow</option>
            <option value="others">Others</option>
          </select>
        </label>
        <label class="multiple-input" style="margin-left: 5px;">
          <input data-parsley-maxlength="255" type="text" id="legend_component_others" name="legend_component_others" class="form-control col-md-7 col-xs-12" placeholder="Others" style="width: 358px;" disabled>
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Carbon <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="carbon" name="carbon" class="form-control col-md-7 col-xs-12" placeholder="Carbon" required="required" style="width: 120px;">
        </label>
        <label class="multiple-input multiple-span" style="width: 51px;">type</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="carbon_type" name="carbon_type" class="form-control col-md-7 col-xs-12" placeholder="Type" style="width: 120px;">
        </label>
        <label class="multiple-input multiple-span" style="width: 51px;">spec</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="carbon_spec" name="carbon_spec" class="form-control col-md-7 col-xs-12" placeholder="Spec" style="width: 120px;">
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Peelable <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="peelable" name="peelable" class="form-control col-md-7 col-xs-12" placeholder="Peelable" required="required" style="width: 120px;">
        </label>
        <label class="multiple-input multiple-span" style="width: 51px;">type</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="peelable_type" name="peelable_type" class="form-control col-md-7 col-xs-12" placeholder="Type" style="width: 120px;">
        </label>
        <label class="multiple-input multiple-span" style="width: 51px;">spec</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="peelable_spec" name="peelable_spec" class="form-control col-md-7 col-xs-12" placeholder="Spec" style="width: 120px;">
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Plating <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="plating" name="plating" class="form-control col-md-7 col-xs-12" placeholder="Plating" required="required" style="width: 120px;">
        </label>
        <label class="multiple-input" style="margin-left: 5px;">
          <input data-parsley-maxlength="255" type="text" id="plating_spec" name="plating_spec" class="form-control col-md-7 col-xs-12" placeholder="Spec" style="width: 358px;">
        </label>
      </div>
    </div>

	<div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Shearing Cut<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="scut" name="scut" class="form-control col-md-7 col-xs-12" placeholder="Shearing Cut" required="required">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Process Molding <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="process_molding" name="process_molding" style="width: 120px" required>
            <option value="cnc">CNC</option>
            <option value="punching">Punching</option>
          </select>
        </label>
        <label class="multiple-input multiple-span" style="width: 53px;">remarks</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="process_molding_remarks" name="process_molding_remarks" class="form-control col-md-7 col-xs-12" placeholder="Remarks" style="width: 300px;">
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">V-Cut <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="v_cut" name="v_cut" style="width: 120px" required>
            <option value="yes">YES</option>
            <option value="no">NO</option>
          </select>
        </label>
        <label class="multiple-input multiple-span" style="width: 53px;">spec</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="v_cut_spec" name="v_cut_spec" class="form-control col-md-7 col-xs-12" placeholder="Spec" required="required" style="width: 300px;">
        </label>
      </div>
    </div>

    <div class="item form-group has-feedback">
      <div class="col-md-8 col-sm-6 col-xs-12 img-spec">
          <input type="file" class="form-control" id="v_cut_img" name="v_cut_img" data-height="110" accept=".jpg, .jpeg, .png"/>
          <span> Hanya format gambar jpg,jpeg,png dengan besaran max 9Mb yang diterima.</span>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Tin Roll / Hal <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="tin_roll" name="tin_roll" style="width: 120px" required>
            <option value="yes">YES</option>
            <option value="no">NO</option>
          </select>
        </label>
        <label class="multiple-input multiple-span" style="width: 53px;">spec</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="tin_roll_spec" name="tin_roll_spec" class="form-control col-md-7 col-xs-12" placeholder="Spec" required="required" style="width: 300px;">
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Finishing <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input" style="width: 100%;">
          <select class="form-control" id="finishing" name="finishing" required>
            <option value="osp">OSP</option>
            <option value="normal flux">Normal Flux</option>
          </select>
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Packing Standard <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <label class="multiple-input">
          <select class="form-control" id="packing_standard" name="packing_standard" style="width: 120px" required>
            <option value="local">Local</option>
            <option value="over sea">Over Sea</option>
            <option value="others">Others</option>
          </select>
        </label>
        <label class="multiple-input multiple-span" style="width: 53px;">others</label>
        <label class="multiple-input">
          <input data-parsley-maxlength="255" type="text" id="packing_standard_others" name="packing_standard_others" class="form-control col-md-7 col-xs-12" placeholder="Others" style="width: 300px;">
        </label>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Others Requirement</span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="others_req" name="others_req" class="form-control col-md-7 col-xs-12" placeholder="Others Requirement">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Add Customer Specification</button>
      </div>
    </div>

  </form><!-- /page content -->

<script type="text/javascript">
  $(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();

    $(".select-control").select2();
  });

  $('#co_logo').on('change',(function(e) {
    if($(this).val() === 'no'){
      $( "#type_logo" ).prop( "required", false );
    }else{
      $( "#type_logo" ).prop( "required", true );
    }
  }));

  $('#date_code').on('change',(function(e) {
    if($(this).val() === 'no'){
      $( "#date_type" ).prop( "required", false );
    }else{
      $( "#date_type" ).prop( "required", true );
    }
  }));

  $('#v_cut').on('change',(function(e) {
    if($(this).val() === 'no'){
      $( "#v_cut_spec" ).prop( "required", false );
    }else{
      $( "#v_cut_spec" ).prop( "required", true );
    }
  }));

  $('#tin_roll').on('change',(function(e) {
    if($(this).val() === 'no'){
      $( "#tin_roll_spec" ).prop( "required", false );
    }else{
      $( "#tin_roll_spec" ).prop( "required", true );
    }
  }));

  $('#solder_resist').on('change',(function(e) {
    $( "#solder_resist_others" ).val('');
    if($(this).val() === 'others'){
      $( "#solder_resist_others" ).prop( "disabled", false );
      $( "#solder_resist_others" ).prop( "required", true );
    }else{
      $( "#solder_resist_others" ).prop( "disabled", true );
      $( "#solder_resist_others" ).prop( "required", false );
    }
  }));

  $('#legend_circuit').on('change',(function(e) {
    $( "#legend_circuit_others" ).val('');
    if($(this).val() === 'others'){
      $( "#legend_circuit_others" ).prop( "disabled", false );
      $( "#legend_circuit_others" ).prop( "required", true );
    }else{
      $( "#legend_circuit_others" ).prop( "disabled", true );
      $( "#legend_circuit_others" ).prop( "required", false );
    }
  }));

  $('#legend_component').on('change',(function(e) {
    $( "#legend_component_others" ).val('');
    if($(this).val() === 'others'){
      $( "#legend_component_others" ).prop( "disabled", false );
      $( "#legend_component_others" ).prop( "required", true );
    }else{
      $( "#legend_component_others" ).prop( "disabled", true );
      $( "#legend_component_others" ).prop( "required", false );
    }
  }));

  $('#packing_standard').on('change',(function(e) {
    $( "#packing_standard_others" ).val('');
    if($(this).val() === 'others'){
      $( "#packing_standard_others" ).prop( "disabled", false );
      $( "#packing_standard_others" ).prop( "required", true );
    }else{
      $( "#packing_standard_others" ).prop( "disabled", true );
      $( "#packing_standard_others" ).prop( "required", false );
    }
  })); 

  $('#add_customer_specification').on('submit',(function(e) {
    $('#btn-submit').attr('disabled','disabled');
    $('#btn-submit').text("Memasukkan data...");
    e.preventDefault();
    var formData = new FormData(this);

    $.ajax({
        type:'POST',
        url: $(this).attr('action'),
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(response) {
            if (response.success == true) {
              $('.panel-heading button').trigger('click');
                listcustspec();
                swal({
                  title: 'Success!',
                  text: response.message,
                  type: 'success',
                  showCancelButton: false,
                  confirmButtonText: 'Ok'
                }).then(function () {
                });
            } else{
                $('#btn-submit').removeAttr('disabled');
                $('#btn-submit').text("Add Customer Specification");
                swal("Failed!", response.message, "error");
            }
        }
    }).fail(function(xhr, status, message) {
        $('#btn-submit').removeAttr('disabled');
        $('#btn-submit').text("Add Customer Specification");
        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
    });
  }));
</script>
