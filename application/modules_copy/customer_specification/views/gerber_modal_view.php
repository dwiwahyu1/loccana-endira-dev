<style>
  .fa-file-o{color: #337ab7;font-size: 30px;cursor:pointer;}
</style>
<form class="form-horizontal form-label-left" id="upload_gerberdata" role="form" action="<?php echo base_url('customer_specification/upload_gerberdata');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

    <div class="item form-group">
    <table id="listgerberdata" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>Title</th>
          <th>Document</th>
          <th>Option</th>
        </tr>
      </thead>
      <tbody>
        <?php 
        $i=1; 
        foreach($gerber_data as $key) {?>
          <tr>
            <td>
              <?php if(isset($key['title'])){ echo $key['title']; }?>
            </td>
            <td>
              <?php if(isset($key['doc'])){?>
                <a href="uploads/gerber_data/<?php echo $key['doc'];?>" target="_blank" download>
                  <i class="fa fa-file-o" title="<?php echo $key['doc'];?>"></i>
                </a>
                <?php echo $key['doc'];?>
              <?php }else{echo "-";}?>
            </td>
            <td>
              <div class="btn-group">
                <button class="btn btn-danger btn-delete" type="button" data-toggle="tooltip" data-placement="top" data-id="<?php if(isset($key['id'])){ echo $key['id']; }?>" title="Hapus">
                  <i class="fa fa-trash"></i>
                </button>
              </div>
            </td>
          </tr>
        <?php } ?>
        <tr>
              <td>
                <input type="text" class="form-control title-control" id="title<?php echo $i-1;?>" value="" style="width:300px;">
              </td>
              <td>
              <input type="file" class="form-control" id="doc<?php echo $i-1;?>" name="doc<?php echo $i-1;?>" data-height="110" accept=".pdf, .txt, .doc, .docx"/>
          </td>
            <td>
              <div class="btn-group">
                <button class="btn btn-primary btn-delete" type="button" data-toggle="tooltip" data-placement="top" title="Tambah Baru" onClick="add_gerber('<?php echo $i-1; ?>')">
                  <i class="fa fa-plus"></i>
                </button>
              </div>
            </td>
        </tr>
      </tbody>
    </table>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Upload Gerber Data</button>
    </div>
  </div>

  <input type="hidden" id="id" name="id" value="<?php if(isset($id)){ echo $id; }?>">
</form>
<!-- /page content -->

<script type="text/javascript">
  var deletedItems = [],
      itemsLen = '<?php echo count($gerber_data)+1;?>';
  
  $(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    
    $(".select-control").select2();

    $( ".btn-danger" ).on( "click", function() {
      $(this).parent().parent().parent().remove();
      itemsLen--;

      var itemCur = itemsLen-1;
      $('.fa-plus').parent().parent().parent().prev().children().attr('id','doc'+itemCur);
      $('.fa-plus').parent().parent().parent().prev().children().attr('name','doc'+itemCur);
      $('.fa-plus').parent().parent().parent().prev().prev().children().attr('id','title'+itemCur);
      
      var id = $(this).data('id');
      if(typeof id !== 'undefined'){
        deletedItems.push(id);
      }
    });
  });

  function add_gerber(row){
    $('#listgerberdata .btn-group .btn-delete').removeClass('btn-primary');
    $('#listgerberdata .btn-group .btn-delete').addClass('btn-danger');
    $('#listgerberdata .btn-group .btn-delete').attr('title','Hapus');
    $('#listgerberdata .btn-group .btn-delete').attr('data-original-title','Hapus');
    $('#listgerberdata .btn-group .btn-delete').removeAttr('onClick');
    $('#listgerberdata .btn-group .btn-delete .fa').removeClass('fa-plus');
    $('#listgerberdata .btn-group .btn-delete .fa').addClass('fa-trash');
    var title  = $(".title-control"),
        titleLen = title.length;

        itemsLen++;

    var element = '<tr>';
        element += '  <td>';
        element += '    <input type="text" class="form-control title-control" id="title'+titleLen+'" value="" style="width:300px;">';
        element += '  </td>';
        element += '  <td>';
        element += '    <input type="file" class="form-control" id="doc'+titleLen+'" name="doc'+titleLen+'" data-height="110" accept=".pdf, .txt, .doc, .docx"/>';
        element += '  </td>';
        element += '  <td>';
        element += '    <div class="btn-group">';
        element += '      <button class="btn btn-primary btn-delete" type="button" data-toggle="tooltip" data-placement="top" title="Tambah Baru" onClick="add_gerber('+titleLen+')">';
        element += '        <i class="fa fa-plus"></i>';
        element += '      </button>';
        element += '    </div>';
        element += '  </td>';
        element += '</tr>';
    $('#listgerberdata tbody').append(element);
    
    $('[data-toggle="tooltip"]').tooltip();

    $( ".btn-danger" ).on( "click", function() {
      $(this).parent().parent().parent().remove();
      itemsLen--;

      var itemCur = itemsLen-1;
      $('.fa-plus').parent().parent().parent().prev().children().attr('id','doc'+itemCur);
      $('.fa-plus').parent().parent().parent().prev().children().attr('name','doc'+itemCur);
      $('.fa-plus').parent().parent().parent().prev().prev().children().attr('id','title'+itemCur);

      var id = $(this).data('id');
      if(typeof id !== 'undefined'){
        deletedItems.push(id);
      }
    });
  }

  $('#upload_gerberdata').on('submit',(function(e) {
    var titles  = $(".title-control"),
        datagerber = [];

    var titlesLen    = titles.length;

    for(var k = 0; k < titlesLen; k++){
      var title = $("#title"+k).val();
      if(title !== ''){
        datagerber.push(title);
      }
    }

      $('#btn-submit').attr('disabled','disabled');
      $('#btn-submit').text("Menyimpan data...");
      e.preventDefault();
      var formData = new FormData(this);
          formData.set("deletedItems", deletedItems);
          formData.set("datagerber", datagerber);

      $.ajax({
          type:'POST',
          url: $(this).attr('action'),
          data : formData,
          cache:false,
          contentType: false,
          processData: false,
          success: function(response) {
              if (response.success == true) {
                $('.panel-heading button').trigger('click');
                  listcustspec();
                  swal({
                    title: 'Success!',
                    text: response.message,
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonText: 'Ok'
                  }).then(function () {
                  });
              } else{
                  $('#btn-submit').removeAttr('disabled');
                  $('#btn-submit').text("Upload Gerber Data");
                  swal("Failed!", response.message, "error");
              }
          }
      }).fail(function(xhr, status, message) {
          $('#btn-submit').removeAttr('disabled');
          $('#btn-submit').text("Upload Gerber Data");
          swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
      });
    }));
</script>