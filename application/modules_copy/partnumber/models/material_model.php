<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Material_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
    
	
	public function detail_prop_full($id)
	{
		$sql_all 	= 'CALL d_prop_search_id_prop(?)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				$id
			));

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
	public function detail_prop($params = array())
	{
		$sql_all 	= 'CALL prop_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				1000,
				0,
				NULL,
				NULL,
				''
			));

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
	public function type_material($params = array())
	{
		$sql_all 	= 'CALL type_mate_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				1000,
				0,
				NULL,
				NULL,
				''
			));

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}	
	
	public function gudang($params = array())
	{
		$sql_all 	= 'CALL gudang_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				1000,
				0,
				NULL,
				NULL,
				''
			));

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
	public function unit($params = array())
	{
		$sql_all 	= 'CALL uom_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				1000,
				0,
				NULL,
				NULL,
				''
			));

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
	public function valas($params = array())
	{
		$sql_all 	= 'CALL valas_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				1000,
				0,
				NULL,
				NULL,
				''
			));

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
    public function edit($id) {
    	$sql = 'CALL material_search_id(?)';

    	$query 	=  $this->db->query($sql, array(
    		$id
    	));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function lists($params = array()) {
		$sql 	= 'CALL material_list_finish(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function add_material($data) {
		$sql 	= 'CALL material_add(?,?,?,?,?,?,?,?,?,?,?,?,?)';
		
		$query 	=  $this->db->query($sql, array(
			"",
			$data['kode_stok'],
			$data['nama_stok'],
			$data['desk_stok'],
			$data['unit'],
			$data['type_material'],
			$data['qty'],
			10,
			$data['detail'],
			$data['gudang'],
			1,
			0,
			0
		));
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		
		//$id = $this->db->insert_id();
		//print_r($lastid);die;
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		
		return $arr_result;
	}
	
	public function customer() {
		// $this->db->select('id,name_eksternal,kode_eksternal');
		// $this->db->from('t_eksternal');
		// $this->db->where('type_eksternal', 1);
		// $this->db->where('kode_eksternal', 1);

		// $query 	= $this->db->get();
		
		$query =   $this->db->query("SELECT id,name_eksternal,kode_eksternal from t_eksternal where
		type_eksternal =1 and kode_eksternal IS NOT NULL ");
		
		//$resultas = $queryas->result_array();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function new_add_material($data) {
		
				$queryas =   $this->db->query("SELECT * from t_eksternal where id = ".$data['cust_id']." ");
				$resultas = $queryas->result_array();
				$oos = $resultas[0];
				$new_seq = $oos['last_seq'] + 1;
				$length_seq = strlen($new_seq);

				if($length_seq < 2) $new_seq_t = '0'.$new_seq;
				else $new_seq_t = $new_seq;

				$m_code = $oos['kode_eksternal']."-PSS-P-".$new_seq_t;
				$sqlm 	= 'INSERT INTO m_material(
					stock_code,
					stock_name,
					stock_description,
					unit,
					`type`,
					treshold,
					id_properties,
					id_gudang,
					`status`,
					cust_id,
					date_create,
					id_coa
				)VALUES (
					"'.$m_code.'",
					"'.$data['nama_stok'].'",
					"'.$data['desk_stok'].'",
					'.$data['unit'].',
					4,
					10,
					0,
					'.$data['gudang'].',
					1,
					'.$data['cust_id'].',
					NOW(),
					0
				)';

				$querym = $this->db->query($sqlm);

				$queryld 		= $this->db->query('SELECT LAST_INSERT_ID()');
				$rowld 			= $queryld->row_array();
				$id_material 	= $rowld['LAST_INSERT_ID()'];
				
				$result	= $this->db->affected_rows();
				
				$arr_result['lastid'] = $id_material;
				$arr_result['result'] = $result;
				
				
		
			
				
				$this->db->query("update t_eksternal set last_seq = ".$new_seq." where id = ".$data['cust_id']." ");
				
				$sqlms 	= 'INSERT INTO t_cust_spec(
					id_produk,
					id_eksternal
				)VALUES (
					"'.$id_material.'",
					'.$data['cust_id'].'							
				)';

				$queryms 	= $this->db->query($sqlms);
				
				$sqlms2 	= 'INSERT INTO t_esf(
					id_produk,
					id_po_quotation
				)VALUES (
					"'.$id_material.'",
					'.$data['cust_id'].'							
				)';

				$queryms2 	= $this->db->query($sqlms2);
				
				$queryld2 = $this->db->query('SELECT LAST_INSERT_ID()');
				$rowld2 = $queryld2->row_array();
				$id_esft = $rowld2['LAST_INSERT_ID()'];

				$sqllay 	= 'INSERT INTO t_esf_layout(
					id_esf
				)VALUES (
					"'.$id_esft.'"
				)';

				$querylay 	= $this->db->query($sqllay);

				$sqlms 	= 'INSERT INTO t_esft(
					id_cust_spec
				)VALUES (
					"'.$id_esft.'"
				)';

				$queryms 	= $this->db->query($sqlms);

				$sql = 'INSERT INTO t_bom_m SELECT NULL,'.$id_material.' AS ID,category,material,NULL,0 FROM `t_supp` where `status` = 1 GROUP BY material ORDER BY seq';

				$query 	= $this->db->query($sql);
				
				$sql2 	= 'INSERT INTO t_bom_m (
					id_stock,
					category,
					material,
					id_material_comp
				)VALUES (
					'.$id_material.',
					"MAIN",
					"MAIN",
					NULL
				)';

				$query2 = $this->db->query($sql2);
				
				
				$sql3 	= 'INSERT INTO t_bom_m (
					id_stock,
					category,
					material,
					id_material_comp
				)VALUES (
					'.$id_material.',
					"MAIN",
					"MAIN2",
					NULL
				)';

				$query3 = $this->db->query($sql3);
				
				$sql4 	= 'INSERT INTO t_prod_layout (
					id_material
				)VALUES (
					'.$id_material.'
				)';

				$query4 	= $this->db->query($sql4);
				
				$queryld4 = $this->db->query('SELECT LAST_INSERT_ID()');
				$rowld4 = $queryld4->row_array();
				$id_prod_lay = $rowld4['LAST_INSERT_ID()'];
				

				$sql3 	= "INSERT INTO `t_process_mat` SELECT NULL AS AAS,".$id_material." AS IDP,id,1,'' as os,'' ink, '' as lot, ".$id_esft." as gg ,".$id_prod_lay." as pord_ly 
					FROM `t_process_flow` ORDER BY `level`";

				$query3 	= $this->db->query($sql3);

				
			
				return $arr_result;
				
	}

	public function get_komponen($id) {
		$sql 	= 'CALL get_komponen(?)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$id['detail']
			));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
		
	}

	public function edit_material($data) {
		$sql 	= 'CALL material_update_partnumber(?,?,?,?,?,?,?)';
		
		$query 	=  $this->db->query($sql,
			array(
				// IN `pin_id` INT(10),
				// IN `pin_stock_code` VARCHAR(50),
				// IN `pin_stock_name` VARCHAR(255),
				// IN `pin_stock_description` VARCHAR(255),
				// IN `pin_unit` INT(11),
				// IN `pin_id_valas` INT(11),
				// IN `pin_price` DECIMAL(20,4),
				// IN `pin_id_gudang` INT(11),
				// IN `pin_type` INT(11),
				// IN `pin_qty` DECIMAL(20,10),
				// IN `pin_weight` DECIMAL(20,10),
				// IN `pin_treshold` INT(11),
				// IN `pin_id_properties` INT(11),
				// IN `pin_status` INT(11),
				// IN `pin_base_price` DECIMAL(20,4),
				// IN `pin_base_qty` DECIMAL(20,10),
				// IN `pin_cust_id` INT(11)
				$data['id_type_material'],
				$data['kode_stok'],
				$data['nama_stok'],
				$data['desk_stok'],
				$data['unit'],
				//$data['valas'],
				//$data['harga'],
				$data['gudang'],
				$data['cust']
			//	$data['qty'],
			//	$data['weight'],
			//	$data['treshold'],
			//	$data['id_properties'],
			//	$data['status'],
			//	$data['base_price'],
			//	$data['base_qty'],
			//	$data['user_id']
		));
		
		// $sql 	= 'CALL material_update(?,?,?)';

		// $query 	=  $this->db->query($sql,
		// 	array(
		// 		$data['id_type_material'],
		// 		$data['type_material_name'],
		// 		$data['type_material_description']
		// 	));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function add_dvalues($id_last,$val_komp,$id_det_komp) {
		$sql 	= 'CALL dvalues_add(?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$id_last,
				$id_det_komp,
				$val_komp
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}	
	
	public function add_price($data,$id_komp)
	{
		$sql 	= 'CALL price_add(?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['detail'],
				$data['harga'],
				$data['valas'],
				$id_komp, 
				$data['harga_satuan']
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}	
	
	public function add_mutasi($data,$id_komp)
	{
		$sql 	= 'CALL mutasi_add(?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$id_komp,
				$id_komp,
				$data['harga']
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function deletes($id) {
		$sql 	= 'CALL material_delete(?)';

		$query 	=  $this->db->query($sql, array(
			$id
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}


}
