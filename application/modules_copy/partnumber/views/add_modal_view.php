<style>
	#loading-us{display:none}
	#tick{display:none}

	#loading-mail{display:none}
	#cross{display:none}
</style>

<form class="form-horizontal form-label-left" id="add_material" role="form" action="<?php echo base_url('partnumber/add_material');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<input data-parsley-maxlength="255" type="hidden" id="kode_stok" name="kode_stok" class="form-control" placeholder="Kode Stok" autocomplete="off" required="required">
	<!-- <div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Kode Stok <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="text" id="kode_stok" name="kode_stok" class="form-control" placeholder="Kode Stok" autocomplete="off" required="required">
		</div>
	</div> -->

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Partnumber <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="text" id="nama_stok" name="nama_stok" class="form-control" placeholder="Nama Stok" autocomplete="off" required="required">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Deskripsi Partnumber <span class="required"><sup></sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<textarea data-parsley-maxlength="255" id="desk_stok" name="desk_stok" class="form-control" autocomplete="off"></textarea>
		</div>
	</div>

<!--	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_material">Tipe Stok <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="type_material" id="type_material" style="width: 100%" required>
				<option value="" selected='selected' >-- Pilih Tipe Stok --</option>
			<?php foreach($type_material as $key) { ?>
				<option value="<?php echo $key['id_type_material']; ?>" ><?php echo $key['type_material_name']; ?></option>
			<?php } ?>
			</select>
		</div>
	</div> -->

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_material">Unit <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="unit" id="unit" style="width: 100%" required placeholder="Pilih Unit" autocomplete="off">
				<option value="" selected='selected' disabled >-- Pilih Unit --</option>
			<?php foreach($unit as $unit) { ?>
				<option value="<?php echo $unit['id_uom']; ?>" ><?php echo $unit['uom_name']; ?></option>
			<?php } ?>
			</select>
		</div>
	</div>

<!--
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Harga <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="number" id="harga" name="harga" class="form-control" placeholder="Harga" autocomplete="off" required="required">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Quantity <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="number" id="qty" name="qty" class="form-control" placeholder="Quantity" autocomplete="off" required="required">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="weight">Weight <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="number" id="weight" name="weight" class="form-control" placeholder="Weight" autocomplete="off" required="required">
		</div>
	</div>
 -->
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_material">Gudang <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="gudang" id="gudang" style="width: 100%" required autocomplete="off">
				<option value="" disabled='disabled' selected='selected' >-- Pilih Gudang Penyimpanan --</option>
			<?php foreach($Gudang as $Gudang) { ?>
				<option value="<?php echo $Gudang['id_gudang']; ?>" ><?php echo $Gudang['nama_gudang']; ?></option>
			<?php } ?>
			</select>
		</div>
	</div>



	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_prop">Customer <span class="required"><sup></sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="customer" id="customer" style="width: 100%" required autocomplete="off">
				<option value="" disabled='disabled' selected='selected' >-- Pilih Customer --</option>
				<option value="0" >Tanpa Customer</option>
			<?php foreach($customer as $customer) { ?>
				<option value="<?php echo $customer['id']; ?>" ><?php echo $customer['kode_eksternal'].' - '.$customer['name_eksternal']; ?></option>
			<?php } ?>
			</select>
		</div>
	</div>

	<div id='detail_div'></div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Partnumber</button>
		</div>
	</div>
</form>

<script type="text/javascript">
	$(document).ready(function() {
		$('#unit').select2();
		$('#type_material').select2();
		$('#customer').select2();
		$('#gudang').select2();
	});
	$('#detail').on('change',(function(e) {
		var formData = new FormData();
		formData.append('id', $('#detail').val());

		$.ajax({
			type:'POST',
			url: '<?php echo base_url('partnumber/get_properties');?>',
			data:formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					var data = response.data;
					var length_data = data.length;
					
					if(length_data > 0 ){
						var form_add = '';
						for(var i=0;i<length_data;i++){
							form_add += ' '+ create_form_element(data[i].type_properties,data[i].detail_name,data[i].param_detail_name);
						}

						$('#detail_div').html(form_add);
					}else $('#detail_div').html('');
				}
			}
		});
	}));

	function create_form_element(type,name,name_param) {
		var html = '';
		if(type == '1'){
			html += '<div class="item form-group">';
			html += '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">'+name+'</label>';
			html += '<div class="col-md-8 col-sm-6 col-xs-12">';
			html += '<input data-parsley-maxlength="255" type="text" id="'+name_param+'" name="'+name_param+'" class="form-control col-md-7 col-xs-12" placeholder="'+name+'" required="required">';
			html += '</div></div>';
		}else if(type == '2'){
			html += '<div class="item form-group">';
			html += '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">'+name+'</label>';
			html += '<div class="col-md-8 col-sm-6 col-xs-12">';
			html += '<input data-parsley-maxlength="255" type="text" id="'+name_param+'" name="'+name_param+'" class="form-control col-md-7 col-xs-12" placeholder="'+name+'" required="required">';
			html += '</div></div>';
		}
		return html;	
	}
	
	$('#add_material').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		var formData = new FormData(this);

		$.ajax({
			type:'POST',
			url: $(this).attr('action'),
			data:formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
					  title: 'Success!',
					  text: response.message,
					  type: 'success',
					  showCancelButton: false,
					  confirmButtonText: 'Ok'
					}).then(function () {
					  window.location.href = "<?php echo base_url('partnumber');?>";
					})
				} else{
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Tambah Stok");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah Stok");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}));
</script>
