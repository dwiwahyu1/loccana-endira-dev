<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Partnumber extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('partnumber/material_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'partnumber/views/index');
	}

	function lists() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

		$order_fields = array('','date_create', 'stock_code', 'stock_name');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->material_model->lists($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		//$i = 0;
		$i = $params['offset'];
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$status_akses =
				'<div class="btn-group"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="editmaterial(\'' . $v['id'] . '\')"><i class="fa fa-edit"></i></button></div>
				<div class="btn-group"><button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deletematerial(\'' . $v['id'] . '\')"><i class="fa fa-trash"></i></button></div>';
			
			array_push($data, array(
				$i,
				$v['stock_code'],
				$v['stock_name'],
				$v['name_eksternal'],
				$v['uom_name'],
				number_format($v['qty'],4,',','.'),
				$v['nama_gudang'],
				$v['date_create'],
				$status_akses
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add() {
		$type_material = $this->material_model->type_material();
		$unit = $this->material_model->unit();
		$detail = $this->material_model->detail_prop();
		$Gudang = $this->material_model->gudang();
		$customer = $this->material_model->customer();

		$data = array(
			'type_material' => $type_material,
			'unit' => $unit,
			'detail' => $detail,
			'Gudang' => $Gudang,
			'customer' => $customer
		);

		$this->load->view('add_modal_view', $data);
	}
	
	public function edit($id) {
		$result = $this->material_model->edit($id);
		$type_material = $this->material_model->type_material();
		$valas = $this->material_model->valas();
		$unit = $this->material_model->unit();
		$detail = $this->material_model->detail_prop();
		$customer = $this->material_model->customer();
		$Gudang = $this->material_model->gudang();

		$data = array(
			'stok' 				=> $result,
			'type_material' 	=> $type_material,
			'valas' 			=> $valas,
			'unit' 				=> $unit,
			'detail' 			=> $detail,
			'customer' 			=> $customer,
			'Gudang' 			=> $Gudang
		);

		$this->load->view('edit_modal_view', $data);
	}

	public function deletes() {
		$data 	= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		
		$list = $this->material_model->deletes($params['id']);
		
		if($list > 0) {
			$this->log_activity->insert_activity('insert', 'Berhasil Insert Stok');
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}else {
			$this->log_activity->insert_activity('insert', 'Berhasil Insert Stok');
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
		

	}

	public function edit_material() {
		$this->form_validation->set_rules('kode_stok', 'Kode Stok', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('nama_stok', 'Nama Stok', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('desk_stok', 'Deskripsi', 'trim|required');
	//	$this->form_validation->set_rules('type_material', 'Tipe Stok', 'trim|required');
		$this->form_validation->set_rules('unit', 'Unit', 'trim|required');
		//$this->form_validation->set_rules('valas', 'Valas', 'trim|required');
		//$this->form_validation->set_rules('harga', 'Harga', 'trim|required');
	//	$this->form_validation->set_rules('qty', 'Quantity', 'trim|required');
	//	$this->form_validation->set_rules('weight', 'Weight', 'trim|required');
		$this->form_validation->set_rules('gudang', 'Gudang', 'trim|required');
		$this->form_validation->set_rules('cust', 'cust', 'trim');
	//	$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
	  

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			$user_id 			= $this->session->userdata['logged_in']['user_id'];
			$id_type_material 	= $this->Anti_sql_injection($this->input->post('id_type_material', TRUE));
			$kode_stok 			= $this->Anti_sql_injection($this->input->post('kode_stok', TRUE));
			$nama_stok 			= $this->Anti_sql_injection($this->input->post('nama_stok', TRUE));
			$desk_stok 			= $this->Anti_sql_injection($this->input->post('desk_stok', TRUE));
		//	$type_material 		= $this->Anti_sql_injection($this->input->post('type_material', TRUE));
			$unit 				= $this->Anti_sql_injection($this->input->post('unit', TRUE));
		//	$valas 				= $this->Anti_sql_injection($this->input->post('valas', TRUE));
		//	$harga 				= $this->Anti_sql_injection($this->input->post('harga', TRUE));
		//	$qty 				= $this->Anti_sql_injection($this->input->post('qty', TRUE));
		//	$weight 			= $this->Anti_sql_injection($this->input->post('weight', TRUE));
			$gudang 			= $this->Anti_sql_injection($this->input->post('gudang', TRUE));
			$cust 			= $this->Anti_sql_injection($this->input->post('cust', TRUE));
		//	$detail 			= $this->Anti_sql_injection($this->input->post('detail', TRUE));

			$data = array(
				'id_type_material' 	=> $id_type_material,
				'no_bc' 			=> NULL,
				'kode_stok' 		=> $kode_stok,
				'nama_stok' 		=> $nama_stok,
				'desk_stok' 		=> $desk_stok,
				'unit' 				=> $unit,
				'cust' 				=> $cust,
			//	'valas' 			=> $valas,
			//	'type_material' 	=> $type_material,
			//	'qty' 				=> $qty,
			//	'weight' 			=> $weight,
			//	'treshold' 			=> 10,
			//	'detail' 			=> $detail,
				'gudang' 			=> $gudang,
			//	'status' 			=> 1,
			//	'base_price' 		=> $harga,
			//	'base_qty' 			=> $qty,
			//	'harga' 			=> $harga,
			//	'id_properties' 			=> 0,
				'user_id' 			=> NULL
			);
			$result = $this->material_model->edit_material($data);

			if ($result > 0) {
				$msg = 'Berhasil mengubah Partnumber ke database';
				$this->log_activity->insert_activity('update', 'Berhasil Update Partnumber');
				$result = array('success' => true, 'message' => $msg);
			}else {
				$msg = 'Gagal mengubah Partnumber ke database';
				$this->log_activity->insert_activity('update', 'Gagal Update Partnumber');
				$result = array('success' => false, 'message' => $msg);
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}
	
	public function get_properties() {
		$id = $this->Anti_sql_injection($this->input->post('id', TRUE));
		$detail_prop = $this->material_model->detail_prop_full($id);
		 
		$result = array(
			'success' => true,
			'message' => '',
			'data' => $detail_prop
		);

		$this->output->set_content_type('application/json')->set_output(json_encode($result,true));
	}
	
	public function add_material() {
//		$this->form_validation->set_rules('kode_stok', 'Kode Stok', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('nama_stok', 'Nama Stok', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('desk_stok', 'Deskripsi', 'trim|required');
	//	$this->form_validation->set_rules('type_material', 'Tipe Stok', 'trim|required');
		$this->form_validation->set_rules('unit', 'Unit', 'trim|required');
	//	$this->form_validation->set_rules('harga', 'Harga', 'trim|required');
	//	$this->form_validation->set_rules('qty', 'Quantity', 'trim|required');
	//	$this->form_validation->set_rules('weight', 'Weight', 'trim|required');
		$this->form_validation->set_rules('gudang', 'Gudang', 'trim|required');
		$this->form_validation->set_rules('customer', 'customer', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$user_id 		= $this->session->userdata['logged_in']['user_id'];
			//$kode_stok 		= $this->Anti_sql_injection($this->input->post('kode_stok', TRUE));
			$nama_stok 		= $this->Anti_sql_injection($this->input->post('nama_stok', TRUE));
			$desk_stok 		= $this->Anti_sql_injection($this->input->post('desk_stok', TRUE));
			//$type_material 	= $this->Anti_sql_injection($this->input->post('type_material', TRUE));
			$unit 			= $this->Anti_sql_injection($this->input->post('unit', TRUE));
			//$harga 			= $this->Anti_sql_injection($this->input->post('harga', TRUE));
			//$qty 			= $this->Anti_sql_injection($this->input->post('qty', TRUE));
			//$weight 		= $this->Anti_sql_injection($this->input->post('weight', TRUE));
			$gudang 		= $this->Anti_sql_injection($this->input->post('gudang', TRUE));
			$customer 		= $this->Anti_sql_injection($this->input->post('customer', TRUE));

			$data = array(
				'no_bc' 		=> NULL,
				'kode_stok' 	=> NULL,
				'nama_stok' 	=> $nama_stok,
				'desk_stok' 	=> $desk_stok,
				'unit'			=> $unit,
				'type_material' => 4,
				'qty' 			=> 0,
				'weight' 		=> 0,
				'treshold' 		=> 10,
				'detail' 		=> NULL,
				'gudang' 		=> $gudang,
				'status' 		=> 1,
				'base_price' 	=> NULL,
				'base_qty' 		=> 0,
				'harga' 		=> 0,
				'user_id' 		=> NULL,
				'cust_id'		=> $customer 
			);

			$result = $this->material_model->new_add_material($data);
			if ($result['result'] > 0) {
				
				$msg = 'Berhasil menambahkan Stok ke database.';
				$this->log_activity->insert_activity('insert', 'Berhasil Insert Stok');
				$results = array('success' => true, 'message' => $msg);
			}else {
				$msg = 'Gagal menambahkan Stok ke database.';
				$this->log_activity->insert_activity('insert', 'Gagal Insert Stok');
				$results = array('success' => false, 'message' => $msg);
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($results));
		}
	}
}
