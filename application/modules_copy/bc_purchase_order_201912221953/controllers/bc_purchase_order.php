<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class BC_Purchase_Order extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('bc_purchase_order/bc_purchase_order_model');
		$this->load->library('log_activity');
		$this->load->library('sequence');
		$this->load->library('excel');
		$this->codeData = array(
            'codeBC'		=> '0505',
            'codeCelebit'	=> '005347',
            'codeTgl'		=> date('Ymd')
        );
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'bc_purchase_order/views/index');
	}

	function list_bc() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 2;
		$order_fields = array('', 'jenis_bc', 'no_pendaftaran', 'no_pengajuan', 'tgl_pengajuan');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->bc_purchase_order_model->list_bc($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$btnstr = '';

			if($v['file_loc'] != '' || $v['file_loc'] != NULL) {
				$btnstr =
					'<div class="btn-group">'.
						'<a href="'. $v['file_loc'] .'" class="btn btn-info btn-sm" target="_blank" title="Download File" download>'.
							'<i class="fa fa-file"></i>'.
						'</a>'.
					'</div>';
			}
			// $status_akses ='';
			$status_akses =
				/*'<div class="btn-group">'.
					'<button class="btn btn-warning btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
						onClick="edit_bc(\'' . $v['id'] . '\')">'.
						'<i class="fa fa-edit"></i>'.
					'</button>'.
				'</div>'.
				'<div class="btn-group">'.
					'<button class="btn btn-danger btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Delete"
						onClick="delete_bc(\'' . $v['id'] . '\')">'.
						'<i class="fa fa-trash"></i>'.
					'</button>'.
				'</div>'.*/
				// '<div class="btn-group">'.
				// 	'<button class="btn btn-info btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Details"
				// 		onClick="details_bc(\'' . $v['id'] . '\')">'.
				// 		'<i class="fa fa-list-alt"></i>'.
				// 	'</button>'.
				// '</div>'.
				// '<div class="btn-group">'.
				// 	'<button class="btn btn-success btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Download"
				// 		onClick="download_bc(\'' . $v['id'] . '\')">'.
				// 		'<i class="fa fa-file-excel-o"></i>'.
				// 	'</button>'.
				// '</div>'.
				'<div class="btn-group">'.
					'<button class="btn btn-info btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
						onClick="edit_bc(\'' . $v['id'] . '\')">'.
						'<i class="fa fa-edit"></i>'.
					'</button>'.
				'</div>';

			array_push($data, array(
				$i,
				$v['jenis_bc'],
				ucwords($v['no_pendaftaran']),
				//ucwords($v['no_pengajuan']),
				date('d M Y', strtotime($v['tanggal_pengajuan'])),
				// $btnstr,
				$status_akses
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function get_no_ajul() {
		$jenis_bc = $this->Anti_sql_injection($this->input->post('jenis_bc', TRUE));
		$tempJenis = 'bc_masuk_'.$jenis_bc;

		$no_aju = $this->sequence->get_max_no($tempJenis);
		$no_pengajuan = $this->codeData['codeBC'].$jenis_bc.'-'.$this->codeData['codeCelebit'].'-'.$this->codeData['codeTgl'].'-'.$no_aju;

		/*echo "<pre>";
		// print_r($jenis_bc);
		// echo $no_pengajuan;
		echo "</pre>";
		die;*/

		if($jenis_bc != '' && $jenis_bc != null) $return = array('success' => true, 'no_pengajuan' => $no_pengajuan, 'menus' => $tempJenis);
		else $return = array('success' => false, 'no_pengajuan' => $no_pengajuan);
		$this->output->set_content_type('application/json')->set_output(json_encode($return));
	}

	public function get_btb_by_po() 
	{
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);
		
		$btb = $this->bc_purchase_order_model->get_btb_by_po($params);
		
		if(sizeof($btb) > 0) {
			$result = array(
				'success' => true,
				'btb' => $btb
			);
		}else{
			$result = array(
				'success' => false
			);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function add_bc() {
		$result_type 	= $this->bc_purchase_order_model->type_bc();
		$tempType 		= array();
		foreach ($result_type as $tkey => $tval) {
			$tempJenis = preg_replace('/\D/', '', $tval['jenis_bc']);
			if($tempJenis == 23 || $tempJenis == 40 || $tempJenis == 27 || $tempJenis == 262) {
				array_push($tempType, $tval);
			}
		}

		$data = array(
			'jenis_bc' => $tempType
		);

		/*echo "<pre>";
		print_r($data);
		echo "</pre>";
		die;*/

		$this->load->view('add_modal_view', $data);
	}

	public function check_nopendaftaran() {
		$this->form_validation->set_rules('nopendaftaran', 'No Pendaftaran', 'trim|required|min_length[4]|max_length[100]|is_unique[t_bc.no_pendaftaran]');
		$this->form_validation->set_message('is_unique', 'No Pendaftaran Already Exists.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'No Pendaftaran Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}

	public function check_nopengajuan() {
		//$this->form_validation->set_rules('nopengajuan', 'No Pengajuan', 'trim|required|min_length[4]|max_length[100]|is_unique[t_bc.no_pengajuan]');
		$this->form_validation->set_message('is_unique', 'No Pengajuan Already Exists.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'No pengajuan Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}

	public function save_bc() {
		$this->form_validation->set_rules('jenis_bc', 'Jenis BC', 'trim|required');
		$this->form_validation->set_rules('nopendaftaran', 'No Pendaftaran', 'trim|required|min_length[4]|max_length[100]|is_unique[t_bc.no_pendaftaran]');
		//$this->form_validation->set_rules('nopengajuan', 'No Pengajuan', 'trim|required|min_length[4]|max_length[100]|is_unique[t_bc.no_pengajuan]');
		$this->form_validation->set_rules('tglpengajuan', 'Tanggal Pengajuan', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('id_btb', 'BTB', 'trim|required|max_length[100]');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$menus = $this->Anti_sql_injection($this->input->post('menus', TRUE));
			$jenis_bc = $this->Anti_sql_injection($this->input->post('jenis_bc', TRUE));
			$nopendaftaran = $this->Anti_sql_injection($this->input->post('nopendaftaran', TRUE));
			//$nopengajuan = $this->Anti_sql_injection($this->input->post('nopengajuan', TRUE));
			$id_btb = $this->Anti_sql_injection($this->input->post('id_btb', TRUE));
			$tglinput = $this->Anti_sql_injection($this->input->post('tglpengajuan', TRUE));
			$temptgl = explode("/", $tglinput);
			$tglpengajuan = date('Y-m-d', strtotime($temptgl[2].'-'.$temptgl[1].'-'.$temptgl[0]));
			$type_text = ucwords($this->Anti_sql_injection($this->input->post('type_text', TRUE)));
			$no_po = ucwords($this->Anti_sql_injection($this->input->post('no_po', TRUE)));
			$upload_error = NULL;
			$file_bc = NULL;

			/*if ($_FILES['file_bc']['name']) {
				$this->load->library('upload');
				$new_filename =
					preg_replace('/\s/', '', $type_text) .' '.
					$nopendaftaran .' '.
					date('d-m-Y', strtotime($temptgl[2].'-'.$temptgl[1].'-'.$temptgl[0])).
					'.'.pathinfo($_FILES['file_bc']['name'], PATHINFO_EXTENSION);

				$config = array(
					'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/bc",
					'upload_url' => base_url() . "uploads/bc",
					'encrypt_name' => FALSE,
					'max_filename' => 100,
					'file_name' => $new_filename,
					'overwrite' => FALSE,
					'allowed_types' => 'pdf|txt|doc|docx',
					'max_size' => '10000'
				);
				$this->upload->initialize($config);

				if ($this->upload->do_upload("file_bc")) {
					// General result data
					$result = $this->upload->data();

					// Add our stuff
					$file_bc = 'uploads/bc/'.$result['file_name'];
				}else {
					$pesan = $this->upload->display_errors();
					$upload_error = strip_tags(str_replace("\n", '', $pesan));

					$result = array('success' => false, 'message' => $upload_error);
				}
			}*/

			// if (!isset($upload_error)) {
				$data = array(
					'menus'				=> $menus,
					'jenis_bc'			=> $jenis_bc,
					'no_pendaftaran'	=> $nopendaftaran,
					//'no_pengajuan'		=> preg_replace('/\D/', '', $nopengajuan),
					'tanggal_pengajuan' => $tglpengajuan,
					'no_po'				=> $no_po,
					'file_loc'			=> $file_bc
				);

				/*$resultHeader 	=  $this->bc_purchase_order_model->add_report_header($data);
				echo "<pre>";
				print_r($data);
				print_r($resultHeader);
				echo "</pre>";
				die;*/

				$this->sequence->get_no($menus);
				$resultAddBC 	= $this->bc_purchase_order_model->add_bc($data);
				$resultHeader 	=  $this->bc_purchase_order_model->add_report_header($data);
				// if ($resultAddBC['result'] > 0) {
				if ($resultAddBC['result'] > 0 && $resultHeader > 0) {
					$dataBC_po = array(
						'id_bc'		=> $resultAddBC['lastid'],
						'no_po'		=> $no_po,
						'id_btb'	=> $id_btb
					);

					$resultAddBC_po = $this->bc_purchase_order_model->add_bc_po($dataBC_po);
					if($resultAddBC_po > 0) {
						$this->log_activity->insert_activity('insert', 'Insert BC Masuk');
						$result = array('success' => true, 'message' => 'Berhasil menambahkan BC Masuk ke database');
					}else {
						$this->log_activity->insert_activity('insert', 'Gagal Insert BC Masuk');
						$result = array('success' => false, 'message' => 'Gagal menambahkan BC Masuk ke database');
					}
				}else {
					$this->log_activity->insert_activity('insert', 'Gagal Insert BC Masuk');
					$result = array('success' => false, 'message' => 'Gagal menambahkan BC Masuk ke database');
				}
			// }

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function edit_bc($id) {
		$bc 			= $this->bc_purchase_order_model->edit_bc($id);
		$bc_po 			= $this->bc_purchase_order_model->get_bc_po($id);
		$result_type 	= $this->bc_purchase_order_model->type_bc();
		$get_po 		= $this->bc_purchase_order_model->get_purchase_order();
		$temptgl 		= explode('-', $bc[0]['tanggal_pengajuan']);
		$bc[0]['tanggal_pengajuan'] = date('d/M/Y', strtotime($temptgl[0].'-'.$temptgl[1].'-'.$temptgl[2]));
		
		if($bc[0]['file_loc'] != '' || $bc[0]['file_loc'] != NULL) $bc[0]['file_loc'] = explode('uploads/bc/', $bc[0]['file_loc'])[1];
		$data = array(
			'bc'		=> $bc,
			'bc_po'		=> $bc_po,
			'jenis_bc'	=> $result_type,
			'list_po'	=> $get_po,
			'folder'	=> 'uploads/bc/',
			'url'		=> base_url()
		);

		/*echo "<pre>";
		print_r($data);
		echo "</pre>";
		die;*/

		$this->load->view('edit_modal_view', $data);
	}

	public function save_edit_bc() {
		$this->form_validation->set_rules('jenis_bc', 'Jenis BC', 'trim|required');
		$this->form_validation->set_rules('nopendaftaran', 'No Pendaftaran', 'trim|required|min_length[4]|max_length[100]');
		//$this->form_validation->set_rules('nopengajuan', 'No Pengajuan', 'trim|required|min_length[4]|max_length[100]');
		$this->form_validation->set_rules('tglpengajuan', 'Tanggal Pengajuan', 'trim|required|max_length[100]');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$bc_id				= $this->Anti_sql_injection($this->input->post('bc_id', TRUE));
			$old_file 			= $this->Anti_sql_injection($this->input->post('old_file', TRUE));
			$menus 				= $this->Anti_sql_injection($this->input->post('menus', TRUE));
			$old_no_pengajuan 	= $this->Anti_sql_injection($this->input->post('old_no_pengajuan', TRUE));
			$jenis_bc 			= $this->Anti_sql_injection($this->input->post('jenis_bc', TRUE));
			$nopendaftaran 		= $this->Anti_sql_injection($this->input->post('nopendaftaran', TRUE));
			$nopengajuan 		= ucwords($this->Anti_sql_injection($this->input->post('nopengajuan', TRUE)));
			$tglinput 			= $this->Anti_sql_injection($this->input->post('tglpengajuan', TRUE));
			$temptgl 			= explode("/", $tglinput);
			$tglpengajuan 		= date('Y-m-d', strtotime($temptgl[2].'-'.$temptgl[1].'-'.$temptgl[0]));
			$type_text 			= ucwords($this->Anti_sql_injection($this->input->post('type_text', TRUE)));
			$upload_error 		= NULL;
			$file_bc 			= NULL;

			/*if ($_FILES['file_bc']['name']) {
				$this->load->library('upload');
				$new_filename =
					preg_replace('/\s/', '', $type_text) .' '.
					$nopendaftaran .' '.
					date('d-m-Y', strtotime($temptgl[2].'-'.$temptgl[1].'-'.$temptgl[0])).
					'.'.pathinfo($_FILES['file_bc']['name'], PATHINFO_EXTENSION);

				$config = array(
					'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "uploads/bc",
					'upload_url' => base_url() . "uploads/bc",
					'encrypt_name' => FALSE,
					'max_filename' => 100,
					'file_name' => $new_filename,
					'overwrite' => FALSE,
					'allowed_types' => 'pdf|txt|doc|docx',
					'max_size' => '10000'
				);
				$this->upload->initialize($config);

				if ($this->upload->do_upload("file_bc")) {
					// General result data
					$result = $this->upload->data();

					// Add our stuff
					$file_bc = 'uploads/bc/'.$result['file_name'];
				}else {
					$pesan = $this->upload->display_errors();
					$upload_error = strip_tags(str_replace("\n", '', $pesan));

					$result = array('success' => false, 'message' => $upload_error);
				}
			}*/

			// if (!isset($upload_error)) {
				/*if($file_bc == '' || $file_bc == NULL) {
					$file_bc = 'uploads/bc/'.$old_file;
				}else {
					$filepath = dirname($_SERVER["SCRIPT_FILENAME"]) . "uploads/bc/" . $old_file;
					if (is_file($filepath)) {
						unlink($filepath);
					}
				}*/

				$data = array(
					'menus'				=> $menus,
					'bc_id'				=> $bc_id,
					'jenis_bc'			=> $jenis_bc,
					'no_pendaftaran'	=> $nopendaftaran,
					'no_pengajuan'		=> $nopengajuan,
					'tanggal_pengajuan' => $tglpengajuan,
					'file_loc'			=> $file_bc
				);

				/*echo "<pre>";
				print_r($data);
				echo "</pre>";
				die;*/

				if(($menus != '' || $menus != null) && $old_no_pengajuan != $nopengajuan) $this->sequence->get_no($menus);
				$result = $this->bc_purchase_order_model->save_edit_bc($data);
				if ($result > 0) {
					$this->log_activity->insert_activity('update', 'Update BC Masuk id : '.$bc_id);
					$result = array('success' => true, 'message' => 'Berhasil mengubah BC Masuk ke database');
				} else {
					$this->log_activity->insert_activity('update', 'Gagal Update BC Masuk id : '.$bc_id);
					$result = array('success' => false, 'message' => 'Gagal mengubah BC Masuk ke database');
				}
			// }

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function delete_bc() {
		$data 	= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		$result = $this->bc_purchase_order_model->edit_bc($params['id']);

		//Delete File if exists
		$filepath = dirname($_SERVER["SCRIPT_FILENAME"]) ."/". $result[0]['file_loc'];
		if (is_file($filepath)) {
			unlink($filepath);
		}
		
		$result 	= $this->bc_purchase_order_model->delete_bc($params['id']);
		$resulttbc 	= $this->bc_purchase_order_model->delete_t_bc_po($params['id']);
		if($result > 0 && $resulttbc > 0) {
			$this->log_activity->insert_activity('delete', 'Delete BC Masuk id : '.$params['id']);
			$res = array('success' => true, 'message' => 'Data telah di hapus');
		}else {
			$this->log_activity->insert_activity('delete', 'Gagal Delete BC Masuk id : '.$params['id']);
			$res = array('success' => false, 'message' => 'Data gagal di hapus');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function detail_bc($id) {
		$bc 			= $this->bc_purchase_order_model->edit_bc($id);
		$type_bc 		= $this->bc_purchase_order_model->type_bc();
		$ref_dok		= $this->bc_purchase_order_model->get_ref_dokumen();
		$ref_kppbc		= $this->bc_purchase_order_model->get_ref_kantor_pabean();
		// $ref_negara		= $this->bc_purchase_order_model->get_ref_negara();
		$ref_satuan		= $this->bc_purchase_order_model->get_ref_satuan();
		// $ref_pelab		= $this->bc_purchase_order_model->get_ref_pelabuhan();
		// $ref_angkut		= $this->bc_purchase_order_model->get_ref_cara_angkut();
		$exportList		= array();
		$data_key 		= array();

		if($bc[0]) {
			foreach ($type_bc as $tbk => $tbv) {
				if($tbv['id'] == $bc[0]['jenis_bc']) {
					$bc[0]['jenis_bc'] = $tbv['jenis_bc'];
					$bc[0]['desc_bc'] = trim($tbv['desc_bc']);
				}
			}
		}

		$jenis_bc = preg_replace('/\D/', '', $bc[0]['jenis_bc']);
		switch ($jenis_bc) {
			case 23:
				$exportList = array('Header', 'Barang', 'BarangTarif', 'BarangDokumen', 'Dokumen', 'Kemasan', 'Kontainer', 'Respon', 'Status', 'Pungutan');
				break;
			case 40:
				$exportList = array('Header', 'BahanBaku', 'BahanBakuTarif', 'BahanBakuDokumen', 'Barang', 'BarangTarif', 'BarangDokumen', 'Dokumen', 'Kemasan', 'Kontainer', 'Respon', 'Status');
				break;
		}

		$data = array(
			'bc'			=> $bc,
			'ref_dok'		=> $ref_dok,
			'ref_kppbc'		=> $ref_kppbc,
			// 'ref_negara'	=> $ref_negara,
			'ref_satuan'	=> $ref_satuan,
			// 'ref_pelab'		=> $ref_pelab,
			// 'ref_angkut'	=> $ref_angkut
		);

		for ($is = 0; $is < sizeof($exportList); $is++) {
			$data_key 	= array();
			$tempTitle  = strtolower($exportList[$is]);
			$no_aju 	= preg_replace('/\D/', '', $bc[0]['no_pengajuan']);

			switch ($exportList[$is]) {
				case 'Header':
					$data[$tempTitle] = $this->bc_purchase_order_model->get_report_header($no_aju);
					break;

				case 'BahanBaku':
					$data[$tempTitle] = $this->bc_purchase_order_model->get_report_bahanbaku($no_aju);
					break;

				case 'BahanBakuTarif':
					$data[$tempTitle] = $this->bc_purchase_order_model->get_report_bahanbakutarif($no_aju);
					break;

				case 'BahanBakuDokumen':
					$data[$tempTitle] = $this->bc_purchase_order_model->get_report_bahanbakudokumen($no_aju);
					break;

				case 'Barang':
					$data[$tempTitle] = $this->bc_purchase_order_model->get_report_barang($no_aju);
					break;

				case 'BarangTarif':
					$data[$tempTitle] = $this->bc_purchase_order_model->get_report_barangtarif($no_aju);
					break;

				case 'BarangDokumen':
					$data[$tempTitle] = $this->bc_purchase_order_model->get_report_barangdokumen($no_aju);
					break;

				case 'Dokumen':
					$data[$tempTitle] = $this->bc_purchase_order_model->get_report_dokumen($no_aju);
					break;

				case 'Kemasan':
					$data[$tempTitle] = $this->bc_purchase_order_model->get_report_kemasan($no_aju);
					break;

				case 'Kontainer':
					$data[$tempTitle] = $this->bc_purchase_order_model->get_report_kontainer($no_aju);
					break;

				case 'Respon':
					$data[$tempTitle] = $this->bc_purchase_order_model->get_report_respons($no_aju);
					break;

				case 'Status':
					$data[$tempTitle] = $this->bc_purchase_order_model->get_report_status($no_aju);
					break;

				case 'Billing':
					$data[$tempTitle] = $this->bc_purchase_order_model->get_report_Billing($no_aju);
					break;

				case 'Pungutan':
					$data[$tempTitle] = $this->bc_purchase_order_model->get_report_pungutan($no_aju);
					break;
			}
		}

		if($jenis_bc == 23) $fileView = 'detail_bc23_view';
		else if($jenis_bc == 262) $fileView = 'detail_bc262_view';
		else if($jenis_bc == 40) $fileView = 'detail_bc40_view';
		else if($jenis_bc == 25) $fileView = 'detail_bc25_view';
		else if($jenis_bc == 27) $fileView = 'detail_bc27_view';
		else $fileView = 'detail_bc_view';

		// echo "<pre>";
		// print_r($data['header']);
		// // print_r($data['kontainer']);
		// // print_r($data['kemasan']);
		// // print_r($bc);
		// // print_r($type_bc);
		// echo "</pre>";
		// die;

		$this->load->view($fileView, $data);
	}

	public function list_detail_bc() {
		$id_bc = ($this->input->get_post('id_bc') != FALSE) ? $this->input->get_post('id_bc') : 0;
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 2;
		$order_fields = array(
			'',
			'a.id_bc_po',
			'b.no_po',
			'b.date_po',
			'b.delivery_date',
			'b.term_of_payment',
			'b.total_amount',
			'c.nama_valas',
			'c.symbol_valas',
			'd.kode_eksternal',
			'd.name_eksternal'
		);

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['id_bc'] = (int) $id_bc;
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->bc_purchase_order_model->detail_bc($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			if($v['id_bc_po'] != '' || $v['id_bc_po'] != NULL) {
				$status_akses =
					/*
					'<div class="btn-group">'.
						'<button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
							onClick="edit_detail_bc(\'' . $v['id'] . '\')">'.
							'<i class="fa fa-edit"></i>'.
						'</button>'.
					'</div>'.
					*/
					'<div class="btn-group">'.
						'<button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete"
							onClick="delete_detail_bc(\'' . $v['id_bc_po'] . '\')">'.
							'<i class="fa fa-trash"></i>'.
						'</button>'.
					'</div>';

				array_push($data, array(
					$i,
					$v['no_po'],
					date('d-m-Y', strtotime($v['date_po'])),
					ucwords($v['name_eksternal']),
					$v['symbol_valas'].' '.number_format($v['total_amount'],0,",","."),
					date('d-m-Y', strtotime($v['delivery_date'])),
					$v['term_of_payment'],
					$status_akses
				));
			}
		}
		$result['data'] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_detail_bc() {
		$this->load->view('add_modal_d_bc_view');
	}

	public function get_purchaseOrder() {
		$result_po = $this->bc_purchase_order_model->get_purchase_order();

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result_po);
	}

	public function check_kd_brg_bc(){
		$this->form_validation->set_rules('kd_brg_bc', 'KodeBarangBC', 'trim|required|min_length[4]|max_length[20]|is_unique[d_bc_list.kode_barang_bc]');
		$this->form_validation->set_message('is_unique', 'Kode barang BC Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'Kode Barang BC Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}
	
	public function check_kd_brg(){
		$this->form_validation->set_rules('kd_brg', 'KodeBarang', 'trim|required|min_length[4]|max_length[20]|is_unique[d_bc_list.kode_barang]');
		$this->form_validation->set_message('is_unique', 'Kode barang Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'Kode Barang Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}
	
	public function save_detail() {
		$this->form_validation->set_rules('id_po', 'Purchase Order', 'trim|required');
		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$bc_id = $this->Anti_sql_injection($this->input->post('bc_id', TRUE));
			$id_po = $this->Anti_sql_injection($this->input->post('id_po', TRUE));

			$data = array(
				'id_bc' 	=> $bc_id,
				'id_po' 	=> $id_po
			);
			
			$result = $this->bc_purchase_order_model->add_detail_bc($data);
			if($result > 0) {
				$this->log_activity->insert_activity('insert', 'Insert Detail BC Masuk Purchase Order');
				$result = array('success' => true, 'message' => 'Berhasil menambahkan detail bea cukai ke database');
			}else{
				$this->log_activity->insert_activity('insert', 'Gagal Insert Detail BC Masuk Purchase Order');
				$result = array('success' => false, 'message' => 'Gagal menambahkan detail bea cukai ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function edit_detail_bc($id) {
		$result = $this->bc_purchase_order_model->edit_detail_bc($id);
		$result_uom = $this->bc_purchase_order_model->search_uom();
		$result_valas = $this->bc_purchase_order_model->search_valas();
		//$result_stock = $this->bc_purchase_order_model->stock();
		
		$data = array(
			'detail' => $result,
			'uom' => $result_uom,
			'valas' => $result_valas
			//'stock' => $result_stock
		);
		// print_r($data);die;

		$this->load->view('edit_modal_d_bc_view', $data);
	}

	public function save_edit_detail() {
		$this->form_validation->set_rules('kd_brg_bc', 'KodeBarangBC', 'trim|required|min_length[4]|max_length[20]',array('is_unique' => 'This %s Kode Barang BC already exists.'));
		$this->form_validation->set_rules('kd_brg', 'KodeBarang', 'trim|required|min_length[4]|max_length[20]',array('is_unique' => 'This %s Kode Barang already exists.'));
		//$this->form_validation->set_rules('stock_id', 'Stock', 'trim|required');
		//var_dump($this->form_validation->run());die;
		
		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$dbc_id = $this->Anti_sql_injection($this->input->post('dbc_id', TRUE));
			$bc_id = $this->Anti_sql_injection($this->input->post('bc_id', TRUE));
			$kd_brg_bc = strtoupper($this->Anti_sql_injection($this->input->post('kd_brg_bc', TRUE)));
			$kd_brg = strtoupper($this->Anti_sql_injection($this->input->post('kd_brg', TRUE)));
			$uom_id = $this->Anti_sql_injection($this->input->post('uom_id', TRUE));
			$valas_id = $this->Anti_sql_injection($this->input->post('valas_id', TRUE));
			$price = $this->Anti_sql_injection($this->input->post('price', TRUE));
			$weight = $this->Anti_sql_injection($this->input->post('weight', TRUE));
			$qty = $this->Anti_sql_injection($this->input->post('qty', TRUE));

			$data = array(
				'id' 				=> $dbc_id,
				'id_bc' 			=> $bc_id,
				'kode_barang_bc' 	=> $kd_brg_bc,
				'kode_barang' 		=> $kd_brg,
				'uom'		 		=> $uom_id,
				'valas' 			=> $valas_id,
				'price' 			=> $price,
				'weight' 			=> $weight,
				'qty' 				=> $qty
			);

			$result = $this->bc_purchase_order_model->save_edit_detail($data);

			if ($result > 0) {
				$this->log_activity->insert_activity('update', 'Update Detail BC Masuk Purchase Order id : '.$dbc_id);
				$result = array('success' => true, 'message' => 'Berhasil mengubah detail bea cukai ke database');
			}else {
				$this->log_activity->insert_activity('update', 'Update Detail BC Masuk Purchase Order id : '.$dbc_id);
				$result = array('success' => false, 'message' => 'Gagal mengubah detail bea cukai ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function delete_detail_bc() {
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		$result 	= $this->bc_purchase_order_model->delete_detail_bc($params['id']);

		if($result > 0) {
			$this->log_activity->insert_activity('delete', 'Delete Detail BC Masuk Purchase Order id : '.$params['id']);
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}else {
			$this->log_activity->insert_activity('delete', 'Gagal Delete Detail BC Masuk Purchase Order id : '.$params['id']);
			$res = array('status' => 'failed', 'message' => 'Data gagal di hapus');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function download_bc($id_bc) {
		$data_bc 		= $this->bc_purchase_order_model->edit_bc($id_bc)[0];
		$exportList		= array();
		$data 			= array();
		$data_key 		= array();

		switch (preg_replace('/\D/', '', $data_bc['type_bc'])) {
			case 23:
				$exportList = array('Header', 'Barang', 'BarangTarif', 'BarangDokumen', 'Dokumen', 'Kemasan', 'Kontainer', 'Respon', 'Status', 'Pungutan');
				break;
			/*case 25:
				$exportList = array('Header', 'BahanBaku', 'BahanBakuTarif', 'BahanBakuDokumen', 'Barang', 'BarangTarif', 'BarangDokumen', 'Dokumen', 'Kemasan', 'Kontainer', 'Respon', 'Status', 'Billing', 'Pungutan');
				break;
			case 27:
				$exportList = array('Header', 'BahanBaku', 'BahanBakuTarif', 'BahanBakuDokumen', 'Barang', 'BarangTarif', 'BarangDokumen', 'Dokumen', 'Kemasan', 'Kontainer', 'Respon', 'Status');
				break;*/
			case 40:
				$exportList = array('Header', 'BahanBaku', 'BahanBakuTarif', 'BahanBakuDokumen', 'Barang', 'BarangTarif', 'BarangDokumen', 'Dokumen', 'Kemasan', 'Kontainer', 'Respon', 'Status');
				break;
		}

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("System")
			->setLastModifiedBy("System")
			->setTitle($data_bc['type_bc'])
			->setSubject("Laporan Bukti Terima Barang")
			->setDescription($data_bc['desc_bc'])
			->setKeywords("Bukti Terima Barang")
			->setCategory("Report");

		for ($is = 0; $is < sizeof($exportList); $is++) {
			$data_key 	= array();
			$no_aju 	= preg_replace('/\D/', '', $data_bc['no_pengajuan']);

			switch ($exportList[$is]) {
				case 'Header':
					$data = $this->bc_purchase_order_model->get_report_header($no_aju);
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_header']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}					
					break;

				case 'BahanBaku':
					$data = $this->bc_purchase_order_model->get_report_bahanbaku($no_aju);
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_bahanbaku']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;

				case 'BahanBakuTarif':
					$data = $this->bc_purchase_order_model->get_report_bahanbakutarif($no_aju);
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_bahanbakutarif']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;

				case 'BahanBakuDokumen':
					$data = $this->bc_purchase_order_model->get_report_bahanbakudokumen($no_aju);
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_bahanbakudokumen']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;

				case 'Barang':
					$data = $this->bc_purchase_order_model->get_report_barang($no_aju);
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_barang']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;

				case 'BarangTarif':
					$data = $this->bc_purchase_order_model->get_report_barangtarif($no_aju);
					if(sizeof($data) > 0){
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_barangtarif']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;

				case 'BarangDokumen':
					$data = $this->bc_purchase_order_model->get_report_barangdokumen($no_aju);
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_barangdokumen']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}

					$objPHPExcel->createSheet($is);
					break;

				case 'Dokumen':
					$data = $this->bc_purchase_order_model->get_report_dokumen($no_aju);
					if(sizeof($data)) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_dokumen']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}

					$objPHPExcel->createSheet($is);
					break;

				case 'Kemasan':
					$data = $this->bc_purchase_order_model->get_report_kemasan($no_aju);;
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_kemasan']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;

				case 'Kontainer':
					$data = $this->bc_purchase_order_model->get_report_kontainer($no_aju);;
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_kontainer']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;

				case 'Respon':
					$data = $this->bc_purchase_order_model->get_report_respons($no_aju);;
					if(sizeof($data)) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_respons']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;

				case 'Status':
					$data = $this->bc_purchase_order_model->get_report_status($no_aju);;
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_status']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;

				case 'Billing':
					$data = $this->bc_purchase_order_model->get_report_Billing($no_aju);;
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_billing']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;

				case 'Pungutan':
					$data = $this->bc_purchase_order_model->get_report_pungutan($no_aju);;
					if(sizeof($data) > 0) {
						for ($i = 0; $i < sizeof($data); $i++) unset($data[$i]['id_report_pungutan']);
						foreach ($data[0] as $key => $value) array_push($data_key, $key);
					}
					$objPHPExcel->createSheet($is);
					break;
			}
			$objPHPExcel->setActiveSheetIndex($is)->setTitle($exportList[$is]);
			$objPHPExcel->getActiveSheet()->fromArray($data_key, null, 'A1');
			$no = 2;
			for ($i = 0; $i < sizeof($data); $i++) { 
				$objPHPExcel->getActiveSheet()->fromArray($data[$i], null, 'A'.$no);
				$no++;
			}
		}

		$objPHPExcel->setActiveSheetIndex(0);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$data_bc['type_bc'].'.xls"');
		
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;
	}
}