<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bc_Purchase_Order_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function all_type_bc() {
		$sql 	= 'SELECT * FROM m_type_bc';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function type_bc() {
		$sql 	= 'SELECT * FROM m_type_bc WHERE type_bc = 0';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function check_userid($params = array()) {
		$sql 	= 'SELECT COUNT(id) AS jumlah FROM u_user WHERE id = ? AND status_akses = 1;';
		$query 	= $this->db->query($sql,
			array($params['user_id'])
		);
		$return = $query->row_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function list_bc($params = array()) {
		$sql 	= 'CALL bc_masuk_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function add_bc($data) {
		$sql 	= 'CALL bc_add(?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['jenis_bc'],
				$data['no_pendaftaran'],
				$data['tanggal_pengajuan'],
				$data['file_loc']
			)
		);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		
		return $arr_result;
	}

	public function add_report_header($data) {
		$sql 	= 'CALL report_header(?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['no_pengajuan'],
				$data['jenis_bc'],
				$data['tanggal_pengajuan'],
				$data['no_po']
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}

	public function add_bc_po($data) {
		$sql 	= 'CALL bc_po_add(?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id_bc'],
				$data['no_po'],
				$data['id_btb']
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_bc($id) {
		$sql 	= 'CALL bc_search_id(?)';

		$query 	=  $this->db->query($sql,
			array($id)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function get_btb_by_po($data) {
		$sql 	= 'SELECT a.* FROM t_btb a
					LEFT JOIN t_btb_po b ON a.id_btb = b.id_btb
					LEFT JOIN t_spb c ON b.id_spb = c.id
					LEFT JOIN t_po_spb d ON c.id = d.id_spb
					LEFT JOIN t_purchase_order e ON d.id_po = e.id_po
					LEFT JOIN m_material f ON c.nama_material = f.id
					LEFT JOIN m_uom g ON f.unit=g.id_uom
					LEFT JOIN t_eksternal h ON e.id_distributor=h.id
					WHERE e.id_po = ? GROUP BY a.id_btb';

		$query 	=  $this->db->query($sql,array(
			$data['id_po']
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_bc_po($id) {
		$sql 	= 'CALL bc_po_search_id_bc(?)';

		$query 	=  $this->db->query($sql,
			array($id)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function add_material($data)
	{
		$sql 	= 'CALL material_add(?,?,?,?,?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['kode_barang_bc'],
				$data['kode_barang'],
				$data['stock_name'],
				$data['stock_description'],
				$data['unit'],
				$data['type'],
				$data['qty'],
				$data['treshold'],
				$data['id_properties'],
				$data['id_gudang'],
				$data['status'],
			));
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		
		return $arr_result;
	}
	
	public function save_edit_bc($data) {
		$sql 	= 'CALL bc_update(?,?,?,?,?)';
		
		$query 	=  $this->db->query($sql,
			array(
				$data['bc_id'],
				$data['jenis_bc'],
				$data['no_pendaftaran'],
				// $data['no_pengajuan'],
				$data['tanggal_pengajuan'],
				$data['file_loc']
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function delete_bc($data) {
		$sql 	= 'CALL bc_delete(?)';
		$query 	=  $this->db->query($sql,
			array($data)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function delete_t_bc_po($data) {
		$sql 	= 'CALL tbc_bc_delete(?)';
		$query 	=  $this->db->query($sql,
			array($data)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function search_kode_barang($kd_brg) {
		$sql 	= 'CALL dbc_search_stock_code(?)';

		$query 	=  $this->db->query($sql,
			array($kd_brg)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function search_uom() {
		$sql 	= 'SELECT * FROM m_uom;';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function search_valas() {
		$sql 	= 'SELECT * FROM m_valas;';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function stock() {
		$sql 	= 'SELECT * FROM t_stock;';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function detail_bc($params = array()) {
		$sql 	= 'CALL tbc_search_id_bc_po_list(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['id_bc'],
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function get_purchase_order() {
		$sql 	= "SELECT a.id_po,a.no_po,c.name_eksternal FROM t_purchase_order a LEFT JOIN t_eksternal c ON c.id = a.id_distributor WHERE NOT EXISTS(SELECT NULL FROM t_bc_po b WHERE a.id_po = b.id_po)";

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_detail_bc($data) {
		$sql 	= 'CALL tbc_add(?,?)';
		
		$query 	=  $this->db->query($sql,
			array(
				$data['id_bc'],
				$data['id_po']
			)
		);
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function add_bc_stock($id,$lastid) {
		$sql 	= 'CALL bcs_add(?,?)';
		
		$query 	=  $this->db->query($sql,
			array(
				$id,
				$lastid
			)
		);
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function add_bc_price($price,$valas,$lastid) {
		$sql 	= 'CALL price_add(?,?,?,?)';
		//$date 	= date('Y-m-d H:i:s');
		$query 	=  $this->db->query($sql,
			array(
				1,
				$price,
				$valas,
				//$date,
				$lastid
			)
		);
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function edit_detail_bc($id) {
		$sql 	= 'CALL dbc_search_id(?)';

		$query 	=  $this->db->query($sql,
			array($id)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function save_edit_detail($data) {
		$sql 	= 'CALL dbc_update(?,?,?,?,?,?,?,?,?)';
		
		$query 	=  $this->db->query($sql,
			array(
				$data['id'],
				$data['id_bc'],
				$data['kode_barang_bc'],
				$data['kode_barang'],
				$data['uom'],
				$data['valas'],
				$data['price'],
				$data['weight'],
				$data['qty']
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function delete_detail_bc($data) {
		$sql 	= 'CALL tbc_delete(?)';
		$query 	=  $this->db->query($sql,
			array($data)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_ref_kantor_pabean() {
		$sql 	= 'SELECT * FROM `referensi_kantor_pabean`';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_ref_dokumen() {
		$sql 	= 'SELECT * FROM `referensi_dokumen`';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_ref_negara() {
		$sql 	= 'SELECT * FROM `referensi_negara`';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_ref_satuan() {
		$sql 	= 'SELECT * FROM `referensi_satuan`';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_ref_pelabuhan() {
		$sql 	= 'SELECT * FROM `referensi_pelabuhan`';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_ref_cara_angkut() {
		$sql 	= 'SELECT * FROM `referensi_cara_angkut`';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_report_header($no_aju) {
		$sql 	= 'SELECT * FROM `t_report_header` WHERE nomor_aju = \''.$no_aju.'\'';
		// $sql 	= 'SELECT * FROM `t_report_header`';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_report_bahanbaku($no_aju) {
		$sql 	= 'SELECT * FROM `t_report_bahanbaku` WHERE nomor_aju = \''.$no_aju.'\'';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_report_bahanbakutarif($no_aju) {
		$sql 	= 'SELECT * FROM `t_report_bahanbakutarif` WHERE nomor_aju = \''.$no_aju.'\'';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_report_bahanbakudokumen($no_aju) {
		$sql 	= 'SELECT * FROM `t_report_bahanbakudokumen` WHERE nomor_aju = \''.$no_aju.'\'';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_report_barang($no_aju) {
		$sql 	= 'SELECT * FROM `t_report_barang` WHERE nomor_aju = \''.$no_aju.'\'';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_report_barangtarif($no_aju) {
		$sql 	= 'SELECT * FROM `t_report_barangtarif` WHERE NOMOR_AJU = \''.$no_aju.'\'';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_report_barangdokumen($no_aju) {
		$sql 	= 'SELECT * FROM `t_report_barangdokumen` WHERE NOMOR_AJU = \''.$no_aju.'\'';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_report_dokumen($no_aju) {
		$sql 	= 'SELECT * FROM `t_report_dokumen` WHERE NOMOR_AJU = \''.$no_aju.'\'';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_report_kemasan($no_aju) {
		$sql 	= 'SELECT * FROM `t_report_kemasan` WHERE NOMOR_AJU = \''.$no_aju.'\'';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_report_kontainer($no_aju) {
		$sql 	= 'SELECT * FROM `t_report_kontainer` WHERE NOMOR_AJU = \''.$no_aju.'\'';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_report_respons($no_aju) {
		$sql 	= 'SELECT * FROM `t_report_respons` WHERE NOMOR_AJU = \''.$no_aju.'\'';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_report_status($no_aju) {
		$sql 	= 'SELECT * FROM `t_report_status` WHERE NOMOR_AJU = \''.$no_aju.'\'';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_report_billing($no_aju) {
		$sql 	= 'SELECT * FROM `t_report_billing` WHERE NOMOR_AJU = \''.$no_aju.'\'';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_report_pungutan($no_aju) {
		$sql 	= 'SELECT * FROM `t_report_pungutan` WHERE NOMOR_AJU = \''.$no_aju.'\'';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
}