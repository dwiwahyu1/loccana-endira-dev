<style>
	#loading-us{display:none}
	#tick{display:none}

	#loading-mail{display:none}
	#cross{display:none}
</style>

<form class="form-horizontal form-label-left" id="add_form" role="form" action="<?php echo base_url('bc_purchase_order/save_detail');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_po">Purchase Order <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="id_po" name="id_po" required>
				<option value="">... Pilih ...</option>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Detail</button>
			<input type="hidden" id="bc_id" name="bc_id" value="">
		</div>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	$(document).ready(function() {
		$('#bc_id').val(id_bc);
		setPurchaseOrder();
	});

	function setPurchaseOrder() {
		$('#id_po').attr('disabled', 'disabled');
		$.ajax({
			type: "GET",
			url: "<?php echo base_url('bc_purchase_order/get_PuchaseOrder');?>",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					for (var i = 0; i < r.length; i++) {
						var newOption = new Option(r[i].no_po, r[i].id_po, false, false);
						$('#id_po').append(newOption);
					}
					$('#id_po').removeAttr('disabled');
				}
			}
		});
	}
	
	$('#add_form').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		var formData = new FormData(this);

		$.ajax({
			type:'POST',
			url: $(this).attr('action'),
			data:formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						redrawData();
						$('#panel-modal').modal('hide');
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Tambah Detail");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah Detail");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}));
</script>
