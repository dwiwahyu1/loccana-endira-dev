<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bom_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	/**
      * This function is get the list data in coa table
      * @param : $params is where condition for select query
      */

	public function lists2($params = array())
	{
		$sql_all 	= 'CALL schd_list_all(?, ?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				$params['filter'],
				$params['po'],
				$params['m_id']
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL schd_list(?, ?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter'],
				$params['po'],
				$params['m_id']
			));

		$result = $query->result_array();

		//print_r($result);die;

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total_row,
			'total' => $total_row,
		);

		return $return;
	}
	
	public function lists3($params = array())
	{
		$sql_all 	= 'CALL schd_list_all_3(?, ?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				$params['filter'],
				$params['po'],
				$params['m_id']
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL schd_list_3(?, ?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter'],
				$params['po'],
				$params['m_id']
			));

		$result = $query->result_array();

		//print_r($result);die;

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total_row,
			'total' => $total_row,
		);

		return $return;
	}

	public function lists($params = array())
	{
		$sql_all 	= 'CALL bom_list_all(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				$params['filter'],
				$params['cust_id']
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL bom_list(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter'],
				$params['cust_id']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total_row,
			'total' => $total_row,
		);

		return $return;
	}

	/**
      * This function is get data in bom table by id
      * @param : $id is where condition for select query
      */

	public function detail_sch($po,$m_id){
		
		$sql 	= 'SELECT SQL_CALC_FOUND_ROWS
			 t_po_quotation.`id` AS id_po_q,`name_eksternal`,req_no,order_no,
                         DATE_FORMAT(`issue_date`,"%d-%b-%Y") AS `issue_date`,`price_term`,t_status_g.`status`,
			m.`id` AS m_id,stock_name,stock_code, nama_valas,c.`status`,t_order.`qty`, sum(es.`rec_nopanel`) as rec_nopanel, sum(es.`rec_pcb`) as rec_pcb
			, t_eksternal.lot_number, t_eksternal.seq_lot, es.unit, t_eksternal.uom_p FROM t_po_quotation 
			   LEFT JOIN t_eksternal ON t_po_quotation.cust_id=t_eksternal.id
			   LEFT JOIN t_order ON t_po_quotation.id=t_order.id_po_quotation
			   LEFT JOIN m_valas ON t_po_quotation.valas_id=m_valas.valas_id
			   LEFT JOIN t_status_g ON t_po_quotation.status=t_status_g.id_status
			   LEFT JOIN `t_request_item` c ON t_order.`id_produk` = c.`id_produk` AND t_order.`id_po_quotation` = c.`id_po_quotation`
			   LEFT JOIN m_material m ON c.`id_produk` = m.`id`
			   LEFT JOIN `t_esf` es ON t_order.`id_produk` = es.`id_produk` 
			   WHERE t_po_quotation.`id` = ? AND t_order.id_produk = ? ' ;

		$query 	= $this->db->query($sql,array(
				$po,$m_id
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
		
	}

	public function detail_main($id)
	{
		//$sql 	= 'CALL bom_search_id(?)';
		$sql 	= '
			SELECT c.`id_bom`,a.id,a.`stock_code`,a.`stock_name`,a.`stock_description`,a.`cust_id`,b.`name_eksternal`,b.uom_p,c.* 
			FROM m_material a 
			LEFT JOIN t_eksternal b ON a.`cust_id` = b.`id`
			LEFT JOIN t_bom_m c ON a.`id` = c.`id_stock`
			WHERE a.`id` = ?
			AND c.`material` = "MAIN"
		';

		$query 	= $this->db->query($sql,array(
				$id
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	

	public function detail_layout($id)
	{
		//$sql 	= 'CALL bom_search_id(?)';
		$sql 	= '
			SELECT a.*,b.`pcb_type` from t_prod_layout a
				left join `t_esf` b on a.id_material = `id_produk` 
			WHERE a.`id_material`  = ?
		';

		$query 	= $this->db->query($sql,array(
				$id
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function detail($id)
	{
		//$sql 	= 'CALL bom_search_id(?)';
		// $sql 	= '
				// SELECT a.*,b.`name`, b.`lot_no`,b.`tipe_ins`,c.`stock_name`, d.`pcb_type` AS esf FROM `t_process_mat` a
			// LEFT JOIN ( SELECT * FROM `t_esf` WHERE id_produk = ? GROUP BY id_produk ) d ON a.`id_material` = d.`id_produk`
			// LEFT JOIN `t_process_flow` b ON a.`t_process_flow` = b.`id`
			// LEFT JOIN m_material c ON a.`id_material` = c.`id`
			// WHERE a.`id_material`  = ? AND d.`pcb_type` = b.`tipe_prod`
			// ORDER BY b.`level`
		// ';
		
		$sql 	= '
				SELECT b.*,c.* FROM (select * from t_esf group by id_produk) a
				LEfT JOIN t_process_flow b on a.`pcb_type` = b.`tipe_prod`
				LEFT JOIN t_process_mat c on a.`id_produk` = c.`id_material` and b.`id` = c.`id_proc_mat`
				where a.`id_produk` = ?
				ORDER BY `level`
		';

		$query 	= $this->db->query($sql,array(
				$id
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function detail_esf($id)
	{
		//$sql 	= 'CALL bom_search_id(?)';
		$sql 	= '
				SELECT c.*,c.pcs_ary_panel as panel_pcs_array,b.`stock_code`,b.stock_name,b.id as id_produk, a.unit, a.pcb_type FROM t_esf a
				join m_material b on a.`id_produk` = b.`id`
				join t_esf_layout c on a.id = c.id_esf
				where id_produk = ?
		';
 
		$query 	= $this->db->query($sql,array(
				$id
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is get data in production_schedule table by id
      * @param : $id is where condition for select query
      */

	public function edit_bom($data)
	{
			//$split_data = explode("|",$data);

				// $sql 	= 'INSERT INTO t_prod_layout(
			// sheet_size,panel_size,sheet_panel,cutting_map,pcs_ary_pnl,bds_ary_pnl,board_layout,remark,id_material
	// ) VALUES(
			// ?,?,?,?,?,?,?,?,?
	// )';
	$sql 	= "
	 INSERT INTO t_prod_layout
 SELECT NULL AS id_s, b.`id_produk`, CONCAT(b.`rec_sheet_long`,' mm x ',b.`rec_sheet_wide`,' mm') AS sheet_size,
 CONCAT(a.`panel_long`,' mm x ',a.`panel_wide`,' mm') AS panel_size,a.`sheet_panel`,a.`cuting_map`,
 CONCAT(a.`pcs_ary_panel`,' UP x ',b.`pcs_array`,' = ',a.`pcs_ary_panel`*b.`pcs_array`,' PCS') AS pcs_ary_panel,
 CONCAT(b.`pcb_long`,' mm x ',b.`pcb_wide`,' mm') bds_ary_pnl,a.`board_layout`,a.`remark` FROM `t_esf_layout` a
LEFT JOIN `t_esf` b ON a.`id_esf` = b.`id`
	 WHERE `id_esf_layout` = ?
		";
		
		

					$this->db->query($sql,array(
						$data['layout']
					));
				// }

				$this->db->close();
				$this->db->initialize();
			
			
	}	
	
	public function insert_chsd_list($ticket_text,$data,$dsds)
	{
			//$split_data = explode("|",$data);

				$sql 	= 'INSERT INTO t_production_schedule(
					id_po_quot,
					id_produk,
					date_issue,
					lot_number,
					qty,
					qty_sheet,
					due_date,
					tipe,
					`status`,
					id_esf,
					qty_pcb,
					ticket,
					splits_qty
				) VALUES(
					?,
					?,
					?,
					?,
					?,
					?,
					?,
					?,
					?,
					?,
					?,
					?,
					?
					
				);	';

					$this->db->query($sql,array(
						$data['po'],
						$data['m_id'],
						$data['date_issue'],
						$data['lot_number'],
						$data['order_array'],
						$data['qty'],
						$data['type'],
						$data['tipe'],
						"Waiting",
						$dsds['id_esf_layout'],
						$data['qty'],
						$ticket_text,
						$data['split']
						
					));
				// }

				$this->db->close();
				$this->db->initialize();
			
			

	}
	
		public function edit_bom_esf($data)
	{
			//$split_data = explode("|",$data);

				$sql 	= 'update t_esf_layout set
					remark = ?
					where id_esf_layout = ?
					';

					$this->db->query($sql,array(
						$data['remark'],
						$data['layout']
						
					));
				// }

				$this->db->close();
				$this->db->initialize();
			
			
	}

	public function update_chsd_list($data)
	{
			//$split_data = explode("|",$data);

				$sql 	= 'update t_production_schedule set
					id_po_quot = ?,
					id_produk = ?,
					date_issue = ?,
					lot_number = ?,
					qty = ?,
					qty_sheet = ?,
					due_date = ?,
					tipe = ?,
					status = ?
					where id_prod = ?
					';

					$this->db->query($sql,array(
						$data['po'],
						$data['m_id'],
						$data['date_issue'],
						$data['lot_number'],
						$data['order_array'],
						$data['qty'],
						$data['type'],
						$data['tipe'],
						'Waiting',
						$data['id_schd']
						
					));
				// }

				$this->db->close();
				$this->db->initialize();
			
			
	}
	
	public function edit_mat($data,$i)
	{
			//$split_data = explode("|",$data);

				$sql 	= 'update t_process_mat set instruction = ?,`status` = ?  
				where id_material = ? and t_process_flow = ? ';

					$this->db->query($sql,array(
						$data['instruction_'.$i],
						$data['status_'.$i],
						$data['bom_no'],
						$i
						
					));
				// }

				$this->db->close();
				$this->db->initialize();
			
			
	}		
	
	public function insert_mat($data,$i)
	{
			//$split_data = explode("|",$data);

				$sql 	= 'insert into t_process_mat(
					id_material,t_process_flow,status,instruction,ink_type,lot_no_v,id_esf
				)values(
					?,?,?,?,?,?,?
				) ';

					$this->db->query($sql,array(
						$data['bom_no'],
						$i,
						$data['status_'.$i],
						$data['instruction_'.$i],
						NULL,
						NULL,
						$data['layout']
					));
				// }

				$this->db->close();
				$this->db->initialize();
			
			
	}	
	
	public function delete_schd($i)
	{
			//$split_data = explode("|",$data);

				$sql 	= 'delete from t_production_schedule 
				where id_prod = ? ';

					$this->db->query($sql,array(
						$i
					));
				// }

				$this->db->close();
				$this->db->initialize();
			
			
	}	

	
	
	public function app_schd($i)
	{
			//$split_data = explode("|",$data);

				$sql 	= 'update t_production_schedule set status = "Approve"
				where id_prod = ? ';

					$this->db->query($sql,array(
						$i
					));
					
					
					// $sql2 	= '
					// insert into t_wip
					// SELECT NULL as ida, ? as id_prod, (select id_produk from t_production_schedule where id_prod = ? ) as idm, 
					// id_proc_mat,0 as qin, 0 as qiot, 0 as mm,null as dd, null as ll,NULL AS sada,NULL AS sdsd,NULL AS daad 
					// FROM `t_process_mat`
					// WHERE id_material = (select id_produk from t_production_schedule where id_prod = ? )
					// AND `status` = 1
					// ORDER BY id_proc_mat
					// ';
					
					$sql2 	= '
					INSERT INTO t_wip
					SELECT  NULL AS ida, b.`id_prod` AS id_prod, id_material,id_proc_mat,0 AS qin, 0 AS qiot, 0 AS mm,NULL AS dd, NULL AS ll,NULL AS sada,NULL AS sdsd,NULL AS daad 
					FROM t_process_mat a
					JOIN t_production_schedule b
					ON a.`id_material` = b.`id_produk` AND a.`id_esf` = b.`id_esf`
					WHERE b.`id_prod` = ?
					';

					$this->db->query($sql2,array(
						$i
					));
					
					
					$sql3 = '
						INSERT INTO `t_flow_reject_val`
						SELECT NULL AS DAS, a.`id`,f.`id_reject`,0 AS val FROM t_wip a
						LEFT JOIN t_process_mat b ON a.`id_process_flow` = b.`id_proc_mat`
						LEFT JOIN t_esf c ON a.`id_produk` = c.`id_produk` AND b.`id_esf` = c.`id`
						LEFT JOIN `t_prod_layout` d ON d.`id_material` = c.`id_produk` AND c.`rec_nopanel` = d.`panel_size`
						LEFT JOIN `t_process_flow` e ON b.`t_process_flow` = e.`id`
						LEFT JOIN `t_flow_reject` f ON e.`id` = f.`id_process_flow`
						WHERE a.`id_po_quotation` = ?
					';
				// }	
					$this->db->query($sql3,array(
						$i
					));
				
				

				$this->db->close();
				$this->db->initialize();
			
			
	}	
	
	public function rej_schd($i)
	{
			//$split_data = explode("|",$data);

				$sql 	= 'update t_production_schedule set status = "Reject"
				where id_prod = ? ';

					$this->db->query($sql,array(
						$i
					));
				// }

				$this->db->close();
				$this->db->initialize();
			
			
	}

	/**
      * This function is get the list data in material table
      */
	public function item($type)
	{
		// $this->db->select('id,stock_code,stock_name,stock_description');
		// $this->db->from('m_material');
		// $this->db->where('type', $type);
		// $this->db->where('status', 1);

		// $query 	= $this->db->get();
		
		$sql = '
		SELECT a.*,b.`stock_name`,b.`stock_code` FROM `t_supp` a
		LEFT JOIN m_material b ON a.`id_material` = b.`id`
		ORDER BY seq,id_material
		';
		
		$query 	= $this->db->query($sql);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}		
	
	public function detail_schd($id) 
	{
		
		$sql = '
		SELECT a.*,b.`order_no`,b.`req_date`,b.`status`,b.`req_no`,`rec_nopanel`,`rec_pcb`,
			d.`qty` AS qty_order,c.unit  FROM `t_production_schedule` a
			LEFT JOIN t_po_quotation b on a.`id_po_quot` = b.`id`
			LEFT JOIN `t_esf` c on a.`id_produk` = c.`id_produk`
			LEFT JOIN t_order d on d.`id_produk` = a.`id_produk` and d.`id_po_quotation` = a.`id_po_quot`
			LEFT JOIN `t_prod_layout` p ON p.`id_material` = d.`id_produk`
			WHERE p.`id_prod_layout` = '.$id.' 
		';
		
		$query 	= $this->db->query($sql);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}		
	
	public function detail_layout2($po,$id)
	{
		
		$sql = '
		SELECT a.*,b.`rec_sheet_long`,b.`rec_sheet_wide`,b.`pcb_long`,b.`pcb_wide`,b.`id_produk`,b.`pcs_array`,b.pcb_type FROM `t_esf_layout` a
		LEFT JOIN `t_esf` b ON a.`id_esf` = b.`id`
		 WHERE b.`id_produk` =  '.$id.'
		';
		
		$query 	= $this->db->query($sql);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function list_po($po,$id)
	{
		
		
		
		$sql = '
		SELECT o.* FROM `t_order` o
		LEFT JOIN t_po_quotation p ON o.`id_po_quotation` = p.`id`
		WHERE o.`id_produk` = '.$id.' AND `STATUS` = 11
		ORDER BY order_date DESC 
		';
		
		$query 	= $this->db->query($sql);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function mainitem($type)
	{
		// $this->db->select('id,stock_code,stock_name,stock_description');
		// $this->db->from('m_material');
		// $this->db->where('type', $type);
		// $this->db->where('status', 1);

		// $query 	= $this->db->get();
		
		$sql = '
		SELECT a.* FROM m_material a 
		where a.type = 1
		ORDER BY stock_code 
		';
		
		$query 	= $this->db->query($sql);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
}