<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Filetype_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
    
    public function edit($id)
	{
		$sql 	= 'CALL filetype_search_id(?)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$id
			));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function lists($params = array())
	{
		$sql_all 	= 'CALL filetype_list_all(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				''
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL filetype_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => count($result),
			'total' => $total_row,
		);

		return $return;
	}
	
	public function edit_filetype($data)
	{
		$sql 	= 'CALL filetype_update(?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id_type_spec'],
				$data['type_spec_name'],
				$data['type_spec_desc']
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function add_filetype($data)
	{
		$sql 	= 'CALL filetype_add(?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['type_spec_name'],
				$data['type_spec_desc']
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	
	public function deletes($data)
	{
		$sql 	= 'CALL filetype_delete(?)';

		$query 	=  $this->db->query($sql,
			array(
				$data
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}


}
