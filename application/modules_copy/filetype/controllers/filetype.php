<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Filetype extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('filetype/filetype_model');
    }

    /**
     * anti sql injection
     */
    public function Anti_sql_injection($string) {
        $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
        return $string;
    }

    public function mail_service($result) {
        $name = 'Cashier App';
        $from = 'mail@google.com';
        $to = $result['mail_to']; //change this
        $subject = $result['mail_subject']; //change this

        $header = $result['mail_header']; //change this
        $body = $result['mail_body']; //change this
        $footer = $result['mail_footer']; //change this
        // Timpa isi email dengan data
        $a = array('xxheaderxx', 'xxbodyxx', 'xxfooterxx');
        $b = array($header, $body, $footer);

        $template = APPPATH . 'modules/template/email_default.html';
        $fd = fopen($template, "r");
        $message = fread($fd, filesize($template));
        fclose($fd);
        $message = str_replace($a, $b, $message);


        $this->load->library('email'); // load email library
        $this->email->from($from, $name);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        if ($this->email->send()) {
            return 1;
        } else {
            return 0;
        }
    }

    public function index() {
        $this->template->load('maintemplate', 'filetype/views/index');
    }

    function lists() {

        $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
        $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
        $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
        $order = $this->input->get_post('order');
        $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
        $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

        $order_fields = array('', 'type_spec_name', 'type_spec_desc');

        $search = $this->input->get_post('search');

        $search_val = (!empty($search['value'])) ? $search['value'] : null;

        $search_value = $this->Anti_sql_injection($search_val);

        // Build params for calling model
        $params['limit'] = (int) $length;
        $params['offset'] = (int) $start;
        $params['order_column'] = $order_fields[$order_column];
        $params['order_dir'] = $order_dir;
        $params['filter'] = $search_value;

        $list = $this->filetype_model->lists($params);

        $result["recordsTotal"] = $list['total'];
        $result["recordsFiltered"] = $list['total_filtered'];
        $result["draw"] = $draw;

        $data = array();
        $i = 0;
        $username = $this->session->userdata['logged_in']['username'];
        foreach ($list['data'] as $k => $v) {
            $i = $i + 1;
			
                $status_akses = '
                <div class="btn-group"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edit_filetype(\'' . $v['id_type_spec'] . '\')"><i class="fa fa-edit"></i></button></div>
				<div class="btn-group"><button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="delete_filetype(\'' . $v['id_type_spec'] . '\')"><i class="fa fa-trash"></i></button></div>';
            
            array_push($data, array(
                $i,
                $v['type_spec_name'],
                $v['type_spec_desc'],
                $status_akses
            ));
        }

        $result["data"] = $data;

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    public function add() {
        //$result = $this->filetype_model->group();

        $data = array(
            'group' => ''
        );

        $this->load->view('add_modal_view', $data);
    }
	
    public function edit($id) {
        $result = $this->filetype_model->edit($id);
		
        $data = array(
            'filetype' => $result,
        );

        $this->load->view('edit_modal_view', $data);
    }

    public function deletes() {
		
		$data 	= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		
		$list = $this->filetype_model->deletes($params['id']);
		
		$res = array(
				'status' => 'success',
				'message' => 'Data telah di hapus'
			);
		
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
        echo json_encode($res);
        

    }

  public function edit_filetype() {
        $this->form_validation->set_rules('type_spec_name', 'type_spec_name', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('type_spec_desc', 'type_spec_desc', 'trim|required|max_length[255]');
      

        if ($this->form_validation->run() == FALSE) {

            $pesan = validation_errors();
            $msg = strip_tags(str_replace("\n", '', $pesan));

            $result = array(
                'success' => false,
                'message' => $msg
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        } else {


                $id_type_spec = $this->Anti_sql_injection($this->input->post('id_type_spec', TRUE));
                $type_spec_name = $this->Anti_sql_injection($this->input->post('type_spec_name', TRUE));
                $type_spec_desc = $this->Anti_sql_injection($this->input->post('type_spec_desc', TRUE));
               
                $message = "";

                $data = array(
                    'id_type_spec' => $id_type_spec,
                    'type_spec_name' => $type_spec_name,
                    'type_spec_desc' => $type_spec_desc
                );
				
                $result = $this->filetype_model->edit_filetype($data);

                if ($result > 0) {
                    $msg = 'Berhasil merubah tipe file.';

                    $result = array(
                        'success' => true,
                        'message' => $msg
                    );
                } else {
                    $msg = 'Gagal merubah tipe file.';

                    $result = array(
                        'success' => false,
                        'message' => $msg
                    );
                }

            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
    }

    public function add_filetype() {
        $this->form_validation->set_rules('type_spec_name', 'type_spec_name', 'trim|required|max_length[255]');
        $this->form_validation->set_rules('type_spec_desc', 'type_spec_desc', 'trim|required|max_length[255]');
      

        if ($this->form_validation->run() == FALSE) {

            $pesan = validation_errors();
            $msg = strip_tags(str_replace("\n", '', $pesan));

            $result = array(
                'success' => false,
                'message' => $msg
            );

            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        } else {


                $user_id = $this->session->userdata['logged_in']['user_id'];
                $type_spec_name = $this->Anti_sql_injection($this->input->post('type_spec_name', TRUE));
                $type_spec_desc = $this->Anti_sql_injection($this->input->post('type_spec_desc', TRUE));
               
                $message = "";

                $data = array(
                    'user_id' => $user_id,
                    'type_spec_name' => $type_spec_name,
                    'type_spec_desc' => $type_spec_desc
                );

                $result = $this->filetype_model->add_filetype($data);

                if ($result > 0) {
                    $msg = 'Berhasil menambahkan tipe file.';

                    $result = array(
                        'success' => true,
                        'message' => $msg
                    );
                } else {
                    $msg = 'Gagal menambahkan tipe file ke database.';

                    $result = array(
                        'success' => false,
                        'message' => $msg
                    );
                }

            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
    }

}
