<style>
	.changed_status{cursor:pointer;text-decoration: underline;color:#96b6e8;}
	.changed_status:hover{color:#ff8c00}
	.custom-tables, th{text-align:center;vertical-align:middle;}
	.custom-tables.align-text, th{vertical-align:middle;}
	.dt-body-right{text-align:right;font-weight:bold;}
	#titleTanggal{margin-bottom:10px;}
	.left_numb{text-align:left;}
	.right_numb{text-align:right;}
	.tableReport {
		box-sizing: border-box;
		text-align: left;
		width: 90%;
		margin: 10px auto;
		padding-bottom:5px;
		page-break-before: always;
		z-index:99;
	}
</style>

<div class="card-box">
	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<a class="btn btn-icon waves-effect waves-light btn-success m-b-5" title="Refresh Report" id="refresh_report">
					<i class="fa fa-refresh"></i>
				</a>
				<a class="btn btn-icon waves-effect waves-light btn-success m-b-5" data-toggle="tooltip" data-placement="top" title="Export to Excel" id="export_excel">
					<i class="fa fa-file-excel-o"></i>
				</a>
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download PDF" id="btn_download">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>

	<div class="col-md-12 text-center titleReport">
		<h2 id="titlePerusahaan">LAPORAN <?php echo strtoupper($report); ?></h2>
		<h2 id="namaPerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
		<h3 id="titleTanggal">PERIODE <?php echo $tanggal_masuk.' S.D '.$tanggal_keluar; ?></h3>
	</div>
	<hr>
	<table id="listReportMutasi" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th rowspan="2" class="custom-tables align-text">No</th>
				<th rowspan="2" class="custom-tables align-text">Kode Barang</th>
				<th rowspan="2" class="custom-tables align-text">Nama Barang</th>
				<th rowspan="2" class="custom-tables align-text">Sat</th>
				<th rowspan="2" class="custom-tables align-text">Saldo Awal</th>
				<th rowspan="2" class="custom-tables align-text">Pemasukan</th>
				<th rowspan="2" class="custom-tables align-text">Pengeluaran</th>
				<th rowspan="2" class="custom-tables align-text">Penyesuaian</th>
				<th rowspan="2" class="custom-tables align-text">Saldo Akhir</th>
				<th rowspan="2" class="custom-tables align-text">Stock Opname</th>
				<th rowspan="2" class="custom-tables align-text">Selisih</th>
				<th rowspan="2" class="custom-tables align-text">Ket</th>
			</tr>
		</thead>
		<tbody>
			<?php $no = 1;
				foreach($mutasi as $mut) { ?>
				<tr>
					<td><?php echo $no; ?></td>
					<td><?php echo $mut['stock_code']; ?></td>
					<td><?php echo $mut['stock_name']; ?></td>
					<td align="center"><?php echo $mut['uom_name']; ?></td>
					<td class="right_numb"><?php echo number_format($mut['saldo_awal'], 2,',','.'); ?></td>
					<td class="right_numb"><?php echo number_format($mut['pemasukan'], 2,',','.'); ?></td>
					<td class="right_numb"><?php echo number_format($mut['pengeluaran'], 2,',','.'); ?></td>
					<td class="right_numb"><?php echo number_format($mut['penyesuaian'], 2,',','.'); ?></td>
					<td class="right_numb"><?php echo number_format($mut['saldo_akhir'], 2,',','.'); ?></td>
					<td class="left_numb"></td>
					<td class="left_numb"></td>
					<td class="left_numb"></td>
				</tr>
			<?php $no++; } ?>
		</tbody>
	</table>

</div>

<script>
$(document).ready(function() {
	var pesan = '<?php echo $msg['message']; ?>';
	var jenis_report = '<?php echo $jenis_report; ?>';
	var tgl_masuk = '<?php echo $tanggal_masuk; ?>';
	var tgl_keluar = '<?php echo $tanggal_keluar; ?>';

	$('#refresh_report').on('click', function() {
		$('#div_report').removeData('');
		$('#div_report').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');

		var data = {
			'tanggal_masuk'		: '<?php echo $tanggalMasuk; ?>',
			'tanggal_keluar'	: '<?php echo $tanggalKeluar; ?>',
			'jenis_report'		: jenis_report
		};

		$('#div_report').load('<?php echo base_url('report_bc_masuk/view_report/');?>', data, function(response, status, xhr) {
			if (status != "error") {
			}else {
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			}
		});
	});
	
	$('#btn_print').on('click', function() {
		newWin= window.open("");
		newWin.document.write($('#divTitleReport').html());
		newWin.document.write($('#divBodyReport').html());
		newWin.print();
		newWin.close();
	});

	$('#btn_download').click(function () {
		var doc = new jsPDF('p', 'pt', 'a4');
		var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
		var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
		
		doc.setTextColor(50);
		doc.setFont('Helvetica');
		doc.setFontStyle('helvetica','arial','sans-serif','bold');
		doc.text($('#titlePerusahaan').html(), pageWidth / 2, 25, 'center');
		doc.text($('#namaPerusahaan').html(), pageWidth / 2, 45, 'center');
		doc.text($('#titleTanggal').html(), pageWidth / 2, 65, 'center');
		
		doc.autoTable({
			html 			: '#listReportMutasi',
			headStyles		: {
				fontSize 	: 7,
				valign		: 'middle', 
				halign		: 'center',
			},
			theme			: 'plain',
			styles			: {
				fontSize : 6, 
				lineColor: [0, 0, 0],
				lineWidth: 0.35,
				cellWidth : 'auto',
				
			},
			margin 			: 6,
			columnStyles	: {
				0: {halign: 'center'},
				1: {halign: 'left'},
				2: {halign: 'left'},
				3: {cellWidth: 30, halign: 'left'},
				4: {cellWidth: 30, halign: 'left'},
				5: {cellWidth: 30, halign: 'right'},
				6: {cellWidth: 70, halign: 'left'},
				7: {cellWidth: 50, halign: 'left'},
				8: {halign: 'left'},
				9: {halign: 'center'},
				10: {halign: 'right'},
				11: {halign: 'center'}
			},
			rowPageBreak	: 'auto',
			showHead 		: 'firstPage',
			showFoot		: 'lastPage',
			startY			: 80
		});
		doc.save('Report <?php echo $report; ?>.pdf');
	});
	
	$('#export_excel').click(function () {
		
		var url = '<?php echo base_url(); ?>report_bc_masuk/export_excel'; 
		var tanggal_awal = '<?php echo $tanggal_masuk; ?>';
		var tanggal_akhir = '<?php echo $tanggal_keluar; ?>';
		var report = '<?php echo $report ?>';
		  
		 //$("#laod").append(' <img id="loading" src="<?php echo base_url();?>assets/urate-frontend-master/assets/images/icon_loader.gif">');
		  var form = $("<form action='" + url + "' method='post' target='_blank'>" +
			"<input type='hidden' name='tanggal_awal' value='" + tanggal_awal + "' />" +
			"<input type='hidden' name='tanggal_akhir' value='" + tanggal_akhir + "' />" +
			"<input type='hidden' name='report' value='" + report + "' />" +
			"</form>");
		  $('body').append(form);
		  form.submit();
	});
	
	function objectSize(obj) {
		var size = 0;
		for (key in obj) {
			if (obj.hasOwnProperty(key)) size++;
		}
		return size;
	}
	
	if(pesan == 1){
		swal("Maaf!", "Laporan periode "+tgl_masuk+" S.D "+tgl_keluar+" tidak ditemukan...!!!", "info");
	}
});
</script>