<style type="text/css">
	.changed_status{cursor:pointer;text-decoration: underline;color:#96b6e8;}
	.changed_status:hover{color:#ff8c00}
	.custom-tables, th{text-align:center;vertical-align:middle;}
	.custom-tables.align-text, th{vertical-align:middle;}
	.dt-body-right{text-align:right;}
	#titleTanggal{margin-bottom:10px;}
	.right_numb{text-align:right;font-weight:bold;}
	.tableReport {
		box-sizing: border-box;
		text-align: left;
		width: 90%;
		margin: 10px auto;
		padding-bottom:5px;
		page-break-before: always;
		z-index:99;
	}
</style>
<style media="screen">
  .noPrint{ display: block; }
  .yesPrint{ display: block !important; }
</style>

<style media="print">
  .noPrint{ display: none; }
  .yesPrint{ display: block !important; }
</style>

<div class="card-box">
	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<a class="btn btn-icon waves-effect waves-light btn-success m-b-5" title="Refresh Report" id="refresh_report">
					<i class="fa fa-refresh"></i>
				</a>
				<a class="btn btn-icon waves-effect waves-light btn-success m-b-5" data-toggle="tooltip" data-placement="top" title="Export to Excel" id="export_excel">
					<i class="fa fa-file-excel-o"></i>
				</a>
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download PDF" id="btn_download">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>
	
	<div class="col-md-12 text-center titleReport">
		<h2 id="titlePerusahaan">LAPORAN <?php echo strtoupper($report); ?></h2>
		<h2 id="namaPerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
		<h3 id="titleTanggal">PERIODE <?php echo $tanggal_masuk.' S.D '.$tanggal_keluar; ?></h3>
	</div>
	<hr>
	<div class="yesPrint" id="report">
		<table id="listreportpabean" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th rowspan="2" class="custom-tables align-text">No</th>
					<th colspan="3" class="custom-tables align-text">Dokumen Pabean</th>
					<th colspan="2" class="custom-tables align-text">Bukti Penerimaan Barang</th>
					<th rowspan="2" class="custom-tables align-text">Supplier</th>
					<th rowspan="2" class="custom-tables align-text">Kode Barang</th>
					<th rowspan="2" class="custom-tables align-text">Uraian Barang</th>
					<th rowspan="2" class="custom-tables align-text">Sat</th>
					<th rowspan="2" class="custom-tables align-text">Jml</th>
					<th rowspan="2" class="custom-tables align-text">Valas</th>
					<th rowspan="2" class="custom-tables align-text">Nilai Barang</th>
				</tr>
				<tr>
					<th class="custom-tables">Jenis</th>
					<th class="custom-tables">Nomor</th>
					<th class="custom-tables">Tanggal</th>
					<th class="custom-tables">Nomor</th>
					<th class="custom-tables">Tanggal</th>
				</tr>
			</thead>
			<tbody>
				<?php $no = 1;
				foreach($pabean as $pab) { 
					$date_create1 = date_create($pab['tanggal']);
					$date_create2 = date_create($pab['tanggal']);
					$tanggal = date_format($date_create1,'d-F-Y');
					$tanggal_btb = date_format($date_create2,'d-F-Y');
				?>
					<tr>
						<td><?php echo $no; ?></td>
						<td><?php echo $pab['jenis']; ?></td>
						<td class="right_numb"><?php echo $pab['nomor']; ?></td>
						<td><?php echo $pab['tanggal']; ?></td>
						<td class="right_numb"><?php echo $pab['nomor_btb']; ?></td>
						<td><?php echo $pab['tanggal_btb']; ?></td>
						<td><?php echo $pab['supplier']; ?></td>
						<td><?php echo $pab['kode_barang']; ?></td>
						<td><?php echo $pab['uraian_barang']; ?></td>
						<td class="right_numb"><?php echo $pab['sat']; ?></td>
						<td class="right_numb"><?php echo number_format($pab['jumlah'], 2,',','.'); ?></td>
						<td><?php echo $pab['symbol_valas']; ?></td>
						<td class="right_numb"><?php echo number_format($pab['Nilai_Barang'], 2,',','.'); ?></td>
					</tr>
				<?php $no++; } ?>
			</tbody>
		</table>
	</div>
</div>

<script>

$(document).ready(function() {
	var pesan = '<?php echo $msg['message']; ?>';
	var tgl_masuk = '<?php echo $tanggal_masuk; ?>';
	var tgl_keluar = '<?php echo $tanggal_keluar; ?>';
	var report = '<?php echo $report; ?>';
	var jenis_report = '<?php echo $jenis_report; ?>';

	$('#refresh_report').on('click', function() {
		$('#div_report').removeData('');
		$('#div_report').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');

		var data = {
			'tanggal_masuk'		: '<?php echo $tanggalMasuk; ?>',
			'tanggal_keluar'	: '<?php echo $tanggalKeluar; ?>',
			'jenis_report'		: jenis_report
		};

		$('#div_report').load('<?php echo base_url('report_bc_masuk/view_report/');?>', data, function(response, status, xhr) {
			if (status != "error") {
			}else {
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			}
		});
	});

	$('#btn_print').on('click', function() {
		var divToPrint=document.getElementById("listreportpabean");
		newWin= window.open('', '', 'height=700,width=700');
		newWin.document.write(divToPrint.outerHTML);
		newWin.print();
		newWin.close();
	});
	
	$('#btn_download').click(function () {
		var doc = new jsPDF('p', 'pt', 'a4');
		var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
		var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
		
		// FOOTER
		doc.setTextColor(50);
		doc.setFont('Helvetica');
		doc.setFontStyle('helvetica','arial','sans-serif','bold');
		doc.text($('#titlePerusahaan').html(), pageWidth / 2, 20, 'center');
		doc.text($('#namaPerusahaan').html(), pageWidth / 2, 30, 'center');
		doc.text($('#titleTanggal').html(), pageWidth / 2, 40, 'center');
		
		doc.autoTable({
			html 			: '#listreportpabean',
			headStyles		: {
				fontSize 	: 7,
				valign		: 'middle', 
				halign		: 'center',
			},
			theme			: 'plain',
			styles			: {
				fontSize : 6, 
				lineColor: [0, 0, 0],
				lineWidth: 0.35,
				cellWidth : 'auto',
				
			},
			margin 			: 6,
			columnStyles	: {
				0: {halign: 'center'},
				1: {halign: 'left'},
				2: {halign: 'left'},
				3: {cellWidth: 30, halign: 'left'},
				4: {cellWidth: 30, halign: 'left'},
				5: {cellWidth: 30, halign: 'right'},
				6: {cellWidth: 70, halign: 'left'},
				7: {cellWidth: 50, halign: 'left'},
				8: {halign: 'left'},
				9: {halign: 'center'},
				10: {halign: 'right'},
				11: {halign: 'center'},
				12: {halign: 'right'}
			},
			rowPageBreak	: 'auto',
			showHead 		: 'firstPage',
			showFoot		: 'lastPage',
			startY			: 70
		});
		doc.save('Report '+report+'.pdf');
	});
	
	$('#export_excel').click(function () {
		
		var url = '<?php echo base_url(); ?>report_bc_masuk/export_excel'; 
		var tanggal_awal = '<?php echo $tanggal_masuk; ?>';
		var tanggal_akhir = '<?php echo $tanggal_keluar; ?>';
		var report = '<?php echo $report ?>';
		//  console.log(tahun);
		  
		 //$("#laod").append(' <img id="loading" src="<?php echo base_url();?>assets/urate-frontend-master/assets/images/icon_loader.gif">');
		  var form = $("<form action='" + url + "' method='post' target='_blank'>" +
			"<input type='hidden' name='tanggal_awal' value='" + tanggal_awal + "' />" +
			"<input type='hidden' name='tanggal_akhir' value='" + tanggal_akhir + "' />" +
			"<input type='hidden' name='report' value='" + report + "' />" +
			"</form>");
		  $('body').append(form);
		  form.submit();
	});
	
	function objectSize(obj) {
		var size = 0;
		for (key in obj) {
			if (obj.hasOwnProperty(key)) size++;
		}
		return size;
	}
	
	if(pesan == 1){
		swal("Maaf!", "Laporan periode "+tgl_masuk+" S.D "+tgl_keluar+" tidak ditemukan...!!!", "info");
	}
});
</script>