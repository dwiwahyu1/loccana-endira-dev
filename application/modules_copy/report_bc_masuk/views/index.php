<style>
	.dt-body-left {text-align:left;}
	.dt-body-right {text-align:right;}
	.dt-body-center {text-align:center; vertical-align: middle;}
	.force-overflow {height: 250px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow {min-height: 250px}
	#modal-report::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	#modal-report::-webkit-scrollbar
	{
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-report::-webkit-scrollbar-thumb
	{
		background-image: -webkit-gradient(linear,
		   left bottom,
		   left top,
		   color-stop(0.44, rgb(122,153,217)),
		   color-stop(0.72, rgb(73,125,189)),
		   color-stop(0.86, rgb(28,58,148)));
	}
</style>

<div class="container" style="height: auto;">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title" id="title_menu">Laporan BC <a class="btn btn-primary" onclick="report_detail()"><i class="fa fa-search"></i></a></h4>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div style="height: 300px;" id="div_report"></div>
		</div>
	</div>
</div>

<div id="panel-modal-detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-report">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
function report_detail(){
	$('#panel-modal-detail').removeData('bs.modal');
	$('#panel-modal-detail .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal-detail .panel-body').load('<?php echo base_url('report_bc_masuk/report_detail');?>');
	$('#panel-modal-detail .panel-title').html('<i class="fa fa-building-o"></i> Detail Report');
	$('#panel-modal-detail').modal({backdrop:'static',keyboard:false},'show');
}
</script>