<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Report_Bc_Masuk extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('report_bc_masuk/Report_BC_model');
		$this->load->library('excel');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'report_bc_masuk/views/index');
	}

	public function report_detail() {
		$this->load->view('report_detail_view');
	}

	public function view_report() {
		$jenis_report = $this->Anti_sql_injection($this->input->post('jenis_report', TRUE));
		$tanggalMasuk = $this->Anti_sql_injection($this->input->post('tanggal_masuk', TRUE));
		$tanggalKeluar = $this->Anti_sql_injection($this->input->post('tanggal_keluar', TRUE));
		
		$tglMsk = explode("/", $tanggalMasuk);
		$tglKel = explode("/", $tanggalKeluar);
		$tanggal_masuk = date('Y-m-d', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0]));
		$tanggal_keluar = date('Y-m-d', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0]));
		
		if($jenis_report == 'pemasukan') {
			//PABEAN
			$pemasukanPabean = array('tanggal_masuk' => $tanggal_masuk, 'tanggal_keluar' => $tanggal_keluar);
			$result_pabean = $this->Report_BC_model->get_report_pemasukan_pabean($pemasukanPabean);
			$result_pabean_via_maintenance = $this->Report_BC_model->get_report_pemasukan_pabean_via_maintenance($pemasukanPabean);
      $result_merge = (array_merge($result_pabean,$result_pabean_via_maintenance));
			$data = array(
				'report'			=> 'Pemasukan Barang Per Dokumen Pabean',
				'tanggal_masuk'		=> date('d-F-Y', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0])),
				'tanggal_keluar'	=> date('d-F-Y', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0])),
				'pabean'			=> $result_merge,
				'jenis_report'		=> $jenis_report,
				'tanggalMasuk'		=> $tanggalMasuk,
				'tanggalKeluar'		=> $tanggalKeluar
			);
				
			if(count($result_pabean) > 0){
				$data['msg'] = array(
					'message'	=> 0
				);
			}else{
				$data['msg'] = array(
					'message'	=> 1
				);
			}
			
			$this->load->view('report_pabean_view', $data);
		} elseif($jenis_report == 'pengeluaran') {
			//PABEAN
			$pengeluaranPabean = array('tanggal_masuk' => $tanggal_masuk, 'tanggal_keluar' => $tanggal_keluar);
			$result_pabean = $this->Report_BC_model->get_report_pengeluaran_pabean($pengeluaranPabean);
			//echo"<pre>";print_r($result_pabean);die;
			$result_pabean_via_maintenance = $this->Report_BC_model->get_report_pengeluaran_pabean_via_maintenance($pengeluaranPabean);
			$result_pabean_via_limbah = $this->Report_BC_model->get_report_pengeluaran_pabean_via_limbah($pengeluaranPabean);
      $result_merge = (array_merge($result_pabean,$result_pabean_via_maintenance,$result_pabean_via_limbah));
			$data = array(
				'report'			=> 'Pengeluaran Barang Per Dokumen Pabean',
				'tanggal_masuk'		=> date('d-F-Y', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0])),
				'tanggal_keluar'	=> date('d-F-Y', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0])),
				'pabean'			=> $result_pabean,
				'jenis_report'		=> $jenis_report,
				'tanggalMasuk'		=> $tanggalMasuk,
				'tanggalKeluar'		=> $tanggalKeluar
			);
				
			if(count($result_pabean) > 0){
				$data['msg'] = array(
					'message'	=> 0
				);
			}else{
				$data['msg'] = array(
					'message'	=> 1
				);
			}
			
			$this->load->view('report_pengeluaran_pabean_view', $data);
		} else if($jenis_report == 'mutasi_bahan_baku') {
			//MUTASI BAHAN BAKU
			$mutasiBahbak = array('tanggal_masuk' => $tanggal_masuk, 'tanggal_keluar' => $tanggal_keluar);
			$result_mutasi_bahbak = $this->Report_BC_model->get_report_mutasi_bahan_baku($mutasiBahbak);
			
			$data = array(
				'report'			=> 'Pertanggungjawaban Mutasi Bahan Baku dan Penolong',
				'tanggal_masuk'		=> date('d-F-Y', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0])),
				'tanggal_keluar'	=> date('d-F-Y', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0])),
				'mutasi'			=> $result_mutasi_bahbak,
				'jenis_report'		=> $jenis_report,
				'tanggalMasuk'		=> $tanggalMasuk,
				'tanggalKeluar'		=> $tanggalKeluar
			);
				
			if(count($result_mutasi_bahbak) > 0){
				$data['msg'] = array(
					'message' => 0
				);
			}else{
				$data['msg'] = array(
					'message' => 1
				);
			}
			
			$this->load->view('report_pertanggungjawaban_view', $data);
		} else if($jenis_report == 'mutasi_barang_jadi') {
			//MUTASI BARANG JADI
			$mutasiBarJad = array('tanggal_masuk' => $tanggal_masuk, 'tanggal_keluar' => $tanggal_keluar);
			$result_mutasi_barjad = $this->Report_BC_model->get_report_mutasi_barang_jadi($mutasiBarJad);
			
			$data = array(
				'message'			=> 0,
				'report'			=> 'Pertanggungjawaban Mutasi Barang Jadi',
				'tanggal_masuk'		=> date('d-F-Y', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0])),
				'tanggal_keluar'	=> date('d-F-Y', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0])),
				'mutasi'			=> $result_mutasi_barjad,
				'jenis_report'		=> $jenis_report,
				'tanggalMasuk'		=> $tanggalMasuk,
				'tanggalKeluar'		=> $tanggalKeluar
			);
			
			if(count($result_mutasi_barjad) > 0){
				$data['msg'] = array(
					'message'	=> 0
				);
			}else{
				$data['msg'] = array(
					'message'	=> 1
				);
			}
			
			$this->load->view('report_pertanggungjawaban_view', $data);
		} else if($jenis_report == 'mutasi_mesin') {
			//MUTASI MESIN
			$mutasiMesin = array('tanggal_masuk' => $tanggal_masuk, 'tanggal_keluar' => $tanggal_keluar);
			$result_mutasi_mesin = $this->Report_BC_model->get_report_mutasi_mesin($mutasiMesin);
			
			$data = array(
				'message'			=> 0,
				'report'			=> 'Pertanggungjawaban Mutasi Mesin dan Peralatan',
				'tanggal_masuk'		=> date('d-F-Y', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0])),
				'tanggal_keluar'	=> date('d-F-Y', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0])),
				'mutasi'			=> $result_mutasi_mesin,
				'jenis_report'		=> $jenis_report,
				'tanggalMasuk'		=> $tanggalMasuk,
				'tanggalKeluar'		=> $tanggalKeluar
			);
				
			if(count($result_mutasi_mesin) > 0){
				$data['msg'] = array(
					'message'	=> 0
				);
			}else{
				$data['msg'] = array(
					'message'	=> 1
				);
			}
			
			$this->load->view('report_pertanggungjawaban_view', $data);
		}else if($jenis_report == 'barang_reject') {
			//MUTASI BARANG REJECT DAN SCRAP
			$mutasiBarRej = array('tanggal_masuk' => $tanggal_masuk, 'tanggal_keluar' => $tanggal_keluar);
			$result_mutasi_BarRej = $this->Report_BC_model->get_report_barang_reject($mutasiBarRej);
			
			$data = array(
				'report'			=> 'Pertanggungjawaban Mutasi Barang Reject dan Scrap',
				'tanggal_masuk'		=> date('d-F-Y', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0])),
				'tanggal_keluar'	=> date('d-F-Y', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0])),
				'mutasi'			=> $result_mutasi_BarRej,
				'jenis_report'		=> $jenis_report,
				'tanggalMasuk'		=> $tanggalMasuk,
				'tanggalKeluar'		=> $tanggalKeluar
			);
			
			if(count($result_mutasi_BarRej) > 0){
				$data['msg'] = array(
					'message'	=> 0
				);
			}else{
				$data['msg'] = array(
					'message'	=> 1
				);
			}
			
			$this->load->view('report_pertanggungjawaban_view', $data);
		}else if($jenis_report == 'pemusnahan_barang') {
			//MUTASI BARANG PEMUSNAHAN BARANG
			$pemusnahan = array('tanggal_masuk' => $tanggal_masuk, 'tanggal_keluar' => $tanggal_keluar);
			$result_pemusnahan = $this->Report_BC_model->get_report_pemusnahan($pemusnahan);
			
			$data = array(
				'report'			=> 'Pertanggungjawaban Pemusnahan Barang',
				'tanggal_masuk'		=> date('d-F-Y', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0])),
				'tanggal_keluar'	=> date('d-F-Y', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0])),
				'pemusnahan'		=> $result_pemusnahan,
				'jenis_report'		=> $jenis_report,
				'tanggalMasuk'		=> $tanggalMasuk,
				'tanggalKeluar'		=> $tanggalKeluar
			);
			
			if(count($result_pemusnahan) > 0){
				$data['msg'] = array(
					'message'	=> 0
				);
			}else{
				$data['msg'] = array(
					'message'	=> 1
				);
			}
			
			$this->load->view('report_pemusnahan_view', $data);
		}else if($jenis_report == 'wip') {
			$wip = array('tanggal_masuk' => $tanggal_masuk, 'tanggal_keluar' => $tanggal_keluar);
			$result_wip = $this->Report_BC_model->get_report_wip($wip);
			
			$data = array(
				'report'			=> 'Posisi WIP',
				'tanggal_masuk'		=> date('d-F-Y', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0])),
				'tanggal_keluar'	=> date('d-F-Y', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0])),
				'wip'				=> $result_wip,
				'jenis_report'		=> $jenis_report,
				'tanggalMasuk'		=> $tanggalMasuk,
				'tanggalKeluar'		=> $tanggalKeluar
			);
			
			if(count($result_wip) > 0){
				$data['msg'] = array(
					'message'	=> 0
				);
			}else{
				$data['msg'] = array(
					'message'	=> 1
				);
			}
			
			$this->load->view('report_wip_view', $data);
		}else {
			$data['msg'] = array(
				'message'	=> 1
			);
			$result = array(
				'msg' => $data['msg']['message'],
				'success' => false
			);
			
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}
	
	public function export_excel() {
		$jenis_report = $this->Anti_sql_injection($this->input->post('report', TRUE));
		$tanggalMasuk = $this->Anti_sql_injection($this->input->post('tanggal_awal', TRUE));
		$tanggalKeluar = $this->Anti_sql_injection($this->input->post('tanggal_akhir', TRUE));
		
		$datemasuk=date_create($tanggalMasuk);
		$date_masuk_fix = date_format($datemasuk,"Y-m-d");
		
		$datekeluar=date_create($tanggalKeluar);
		$date_keluar_fix = date_format($datekeluar,"Y-m-d");
		
		if($jenis_report == 'Pemasukan Barang Per Dokumen Pabean') {
			$pemasukanPabean = array('tanggal_masuk' => $date_masuk_fix, 'tanggal_keluar' => $date_keluar_fix);
			$result_pabean = $this->Report_BC_model->get_report_pemasukan_pabean($pemasukanPabean);
			
			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getActiveSheet()->setTitle("Report Pemasukan Barang");
			$objPHPExcel->getProperties()->setCreator("System")
				->setLastModifiedBy("System")
				->setTitle("Report Pemasukan Barang Per Dokumen Pabean")
				->setSubject("Report Bea Cukai")
				->setDescription("Report Pemasukan Barang Per Dokumen Pabean")
				->setKeywords("Pemasukan Barang")
				->setCategory("Report");
							 
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'LAPORAN '.strtoupper($jenis_report))
				->setCellValue('A2', 'PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA')
				->setCellValue('A3', 'PERIODE '.strtoupper($tanggalMasuk).' S.D '.strtoupper($tanggalKeluar).'');
								
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A5', 'No')
				->setCellValue('B5', 'Dokumen Pabean')
				->setCellValue('E5', 'Bukti Penerimaan Barang')
				->setCellValue('B6', 'Jenis')
				->setCellValue('C6', 'Nomor')
				->setCellValue('D6', 'Tanggal')
				->setCellValue('E6', 'Nomor')
				->setCellValue('F6', 'Tanggal')
				->setCellValue('G5', 'Supplier')
				->setCellValue('H5', 'Kode Barang')
				->setCellValue('I5', 'Uraian Barang')
				->setCellValue('J5', 'Sat')
				->setCellValue('K5', 'Jml')
				->setCellValue('L5', 'Valas')
				->setCellValue('M5', 'Nilai Barang');
			
			$styleArray = array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			
			$i = 7;
			$no = 1;
			$nilai_barang = '';
			foreach($result_pabean as $result_pabeans){
				$objPHPExcel->setActiveSheetIndex()->getStyle('A5:M'.$i)->applyFromArray($styleArray);
				
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$i, $no)
					->setCellValue('B'.$i, $result_pabeans['jenis'])
					->setCellValue('C'.$i, $result_pabeans['nomor'])
					->setCellValue('D'.$i, $result_pabeans['tanggal'])
					->setCellValue('E'.$i, $result_pabeans['nomor_btb'])
					->setCellValue('F'.$i, $result_pabeans['tanggal_btb'])
					->setCellValue('G'.$i, $result_pabeans['supplier'])
					->setCellValue('H'.$i, $result_pabeans['kode_barang'])
					->setCellValue('I'.$i, $result_pabeans['uraian_barang'])
					->setCellValue('J'.$i, $result_pabeans['sat'])
					->setCellValue('K'.$i, $result_pabeans['jumlah'])
					->setCellValue('L'.$i, $result_pabeans['symbol_valas'])
					->setCellValueExplicit('M'.$i, str_replace('.','',number_format($result_pabeans['Nilai_Barang'], 2,',','.')),PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('M'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(27);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(27);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(26);
				
				$i++;
				$no++;
			}
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:M1');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:M1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:M2');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:M2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:M3');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:M3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:M4');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:A6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:D5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:D5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E5:F5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('E5:F5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G5:G6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('G5:G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('G5:G6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H5:H6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('H5:H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('H5:H6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I5:I6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J5:J6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('K5:K6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L5:L6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:L6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('M5:M6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('M5:M6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('M5:M6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Pemasukan Barang Per Dokumen Pabean '.$tanggalMasuk.' S.D '.$tanggalKeluar.'.xls"');
			
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit;
		}else if($jenis_report == 'Pengeluaran Barang Per Dokumen Pabean'){
			$pengeluaranPabean = array('tanggal_masuk' => $date_masuk_fix, 'tanggal_keluar' => $date_keluar_fix);
			$result_pabean = $this->Report_BC_model->get_report_pengeluaran_pabean($pengeluaranPabean);
			
			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getActiveSheet()->setTitle("Report Pengeluaran Barang");
			$objPHPExcel->getProperties()->setCreator("System")
				->setLastModifiedBy("System")
				->setTitle("Report Pengeluaran Barang Per Dokumen Pabean")
				->setSubject("Report Bea Cukai")
				->setDescription("Report Pengeluaran Barang Per Dokumen Pabean")
				->setKeywords("Pengeluaran Barang")
				->setCategory("Report");
							 
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'LAPORAN '.strtoupper($jenis_report))
				->setCellValue('A2', 'PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA')
				->setCellValue('A3', 'PERIODE '.strtoupper($tanggalMasuk).' S.D '.strtoupper($tanggalKeluar).'');
								
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A5', 'No')
				->setCellValue('B5', 'Dokumen Pabean')
				->setCellValue('E5', 'Bukti Penerimaan Barang')
				->setCellValue('B6', 'Jenis')
				->setCellValue('C6', 'Nomor')
				->setCellValue('D6', 'Tanggal')
				->setCellValue('E6', 'Nomor')
				->setCellValue('F6', 'Tanggal')
				->setCellValue('G5', 'Supplier')
				->setCellValue('H5', 'Kode Barang')
				->setCellValue('I5', 'Uraian Barang')
				->setCellValue('J5', 'Sat')
				->setCellValue('K5', 'Jml')
				->setCellValue('L5', 'Valas')
				->setCellValue('M5', 'Nilai Barang');
			
			$styleArray = array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			
			$i = 7;
			$no = 1;
			$nilai_barang = '';
			foreach($result_pabean as $result_pabeans) {
				$objPHPExcel->setActiveSheetIndex()->getStyle('A5:M'.$i)->applyFromArray($styleArray);
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$i, $no)
					->setCellValue('B'.$i, $result_pabeans['jenis_bc'])
					->setCellValue('C'.$i, $result_pabeans['no_pendaftaran'])
					->setCellValue('D'.$i, $result_pabeans['tanggal_pengajuan'])
					->setCellValue('E'.$i, $result_pabeans['order_no'])
					->setCellValue('F'.$i, $result_pabeans['tanggal'])
					->setCellValue('G'.$i, $result_pabeans['name_eksternal'])
					->setCellValue('H'.$i, $result_pabeans['stock_code'])
					->setCellValue('I'.$i, $result_pabeans['stock_name'])
					->setCellValue('J'.$i, $result_pabeans['uom_name'])
					->setCellValue('K'.$i, $result_pabeans['jumlah'])
					->setCellValue('L'.$i, $result_pabeans['nama_valas'])
					->setCellValueExplicit('M'.$i, str_replace('.','',number_format($result_pabeans['Nilai_barang'], 2,',','.')),PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('M'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(27);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(27);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(26);
				
				$i++;
				$no++;
			}
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:M1');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:M1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:M2');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:M2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:M3');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:M3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:M4');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:A6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:D5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:D5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E5:F5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('E5:F5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G5:G6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('G5:G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('G5:G6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H5:H6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('H5:H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('H5:H6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I5:I6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J5:J6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('K5:K6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L5:L6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:L6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('M5:M6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('M5:M6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('M5:M6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Pengeluaran Barang Per Dokumen Pabean '.$tanggalMasuk.' S.D '.$tanggalKeluar.'.xls"');
			
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit; 
		}else if($jenis_report == 'Posisi WIP') {
			$Posisi_WIP = array('tanggal_masuk' => $date_masuk_fix, 'tanggal_keluar' => $date_keluar_fix);
			$result_wip = $this->Report_BC_model->get_report_wip($Posisi_WIP);
			
			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getActiveSheet()->setTitle("Report Posisi WIP");
			$objPHPExcel->getProperties()->setCreator("System")
				->setLastModifiedBy("System")
				->setTitle("Report Posisi WIP")
				->setSubject("Report Bea Cukai")
				->setDescription("Report Posisi WIP")
				->setKeywords("Posisi WIP")
				->setCategory("Report");
							 
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'LAPORAN '.strtoupper($jenis_report))
				->setCellValue('A2', 'PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA')
				->setCellValue('A3', 'PERIODE '.strtoupper($tanggalMasuk).' S.D '.strtoupper($tanggalKeluar).'');
								
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A5', 'No')
				->setCellValue('B5', 'Kode Barang')
				->setCellValue('C5', 'Nama Barang')
				->setCellValue('D5', 'Sat')
				->setCellValue('E5', 'Jumlah');

			$objPHPExcel->setActiveSheetIndex(0)
					->getStyle('A5:E5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$styleArray = array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			
			$i = 6;
			$no = 1;
			$nilai_barang = '';
			foreach($result_wip as $result_wips) {
				$objPHPExcel->setActiveSheetIndex()->getStyle('A5:E'.$i)->applyFromArray($styleArray);
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$i, $no)
					->setCellValue('B'.$i, $result_wips['stock_code'])
					->setCellValue('C'.$i, $result_wips['stock_name'])
					->setCellValue('D'.$i, $result_wips['uom_name'])
					->setCellValueExplicit('E'.$i, str_replace('.','',number_format($result_wips['qty_in'], 2,',','.')),PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				
				$objPHPExcel->setActiveSheetIndex(0)
					->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->setActiveSheetIndex(0)
					->getStyle('D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);

				$i++;
				$no++;
			}
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:E1');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:E2');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:E2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:E3');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:E3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:E4');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:A5');
			
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Posisi WIP '.$tanggalMasuk.' S.D '.$tanggalKeluar.'.xls"');
			
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit;
		}else if($jenis_report == 'Pertanggungjawaban Mutasi Bahan Baku') {
			$mutasiBahbak = array('tanggal_masuk' => $date_masuk_fix, 'tanggal_keluar' => $date_keluar_fix);
			$result_mutasiBahbak = $this->Report_BC_model->get_report_mutasi_bahan_baku($mutasiBahbak);

			$objPHPExcel = new PHPExcel();

			$objPHPExcel->getActiveSheet()->setTitle("Report Pertanggungjawaban");
			$objPHPExcel->getProperties()->setCreator("System")
				->setLastModifiedBy("System")
				->setTitle("Report Pertanggungjawaban Mutasi Bahan Baku")
				->setSubject("Report Bea Cukai")
				->setDescription("Report Pertanggungjawaban Mutasi Bahan Baku")
				->setKeywords("Pertanggungjawaban Mutasi Bahan Baku")
				->setCategory("Report");
							 
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'LAPORAN '.strtoupper($jenis_report))
				->setCellValue('A2', 'PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA')
				->setCellValue('A3', 'PERIODE '.strtoupper($tanggalMasuk).' S.D '.strtoupper($tanggalKeluar).'');
								
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A5', 'No')
				->setCellValue('B5', 'Kode Barang')
				->setCellValue('C5', 'Nama Barang')
				->setCellValue('D5', 'Sat')
				->setCellValue('E5', 'Saldo Awal')
				->setCellValue('F5', 'Pemasukan')
				->setCellValue('G5', 'Pengeluaran')
				->setCellValue('H5', 'Penyesuaian')
				->setCellValue('I5', 'Saldo Akhir')
				->setCellValue('J5', 'Stock Opname')
				->setCellValue('K5', 'Selisih')
				->setCellValue('L5', 'Ket');

			$objPHPExcel->setActiveSheetIndex(0)
					->getStyle('A5:L5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$styleArray = array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			
			$i = 7;
			$no = 1;
			foreach($result_mutasiBahbak as $result_bahbak) {
				$objPHPExcel->setActiveSheetIndex()->getStyle('A5:L'.$i)->applyFromArray($styleArray);
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$i, $no)
					->setCellValue('B'.$i, $result_bahbak['stock_code'])
					->setCellValue('C'.$i, $result_bahbak['stock_name'])
					->setCellValue('D'.$i, $result_bahbak['uom_name'])
					->setCellValue('E'.$i, str_replace('.','', number_format($result_bahbak['saldo_awal'], 2,',','.')))
					->setCellValue('F'.$i, str_replace('.','', number_format($result_bahbak['pemasukan'], 2,',','.')))
					->setCellValue('G'.$i, str_replace('.','', number_format($result_bahbak['pengeluaran'], 2,',','.')))
					->setCellValue('H'.$i, str_replace('.','', number_format($result_bahbak['penyesuaian'], 2,',','.')))
					->setCellValue('I'.$i, str_replace('.','', number_format($result_bahbak['saldo_akhir'], 2,',','.')))
					->setCellValue('J'.$i, '')
					->setCellValue('K'.$i, '')
					->setCellValue('L'.$i, '');

				$objPHPExcel->setActiveSheetIndex(0)
					->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->setActiveSheetIndex(0)
					->getStyle('D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->setActiveSheetIndex(0)
					->getStyle('E'.$i.':I'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(18);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(13);
				
				$i++;
				$no++;
			}
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:L1');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:L2');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:L2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:L3');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:L3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:L4');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:A6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:B6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:B6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C5:C6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('C5:C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('C5:C6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D5:D6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('D5:D6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('D5:D6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E5:E6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('E5:E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('E5:E6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F5:F6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('F5:F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('F5:F6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G5:G6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('G5:G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('G5:G6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H5:H6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('H5:H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('H5:H6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I5:I6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J5:J6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('K5:K6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L5:L6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:L6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Pertanggungjawaban Mutasi Bahan Baku '.$tanggalMasuk.' S.D '.$tanggalKeluar.'.xls"');
			
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit;
		}else if($jenis_report == 'Pertanggungjawaban Mutasi Barang Jadi') {
			$mutasiBahJad = array('tanggal_masuk' => $date_masuk_fix, 'tanggal_keluar' => $date_keluar_fix);
			$result_mutasiBahJad = $this->Report_BC_model->get_report_mutasi_barang_jadi($mutasiBahJad);
			
			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getActiveSheet()->setTitle("Report Pertanggungjawaban");
			$objPHPExcel->getProperties()->setCreator("System")
				->setLastModifiedBy("System")
				->setTitle("Report Pertanggungjawaban Mutasi Barang Jadi")
				->setSubject("Report Bea Cukai")
				->setDescription("Report Pertanggungjawaban Mutasi Barang Jadi")
				->setKeywords("Pertanggungjawaban Mutasi Barang Jadi")
				->setCategory("Report");
							 
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'LAPORAN '.strtoupper($jenis_report))
				->setCellValue('A2', 'PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA')
				->setCellValue('A3', 'PERIODE '.strtoupper($tanggalMasuk).' S.D '.strtoupper($tanggalKeluar).'');
								
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A5', 'No')
				->setCellValue('B5', 'Kode Barang')
				->setCellValue('C5', 'Nama Barang')
				->setCellValue('D5', 'Sat')
				->setCellValue('E5', 'Saldo Awal')
				->setCellValue('F5', 'Pemasukan')
				->setCellValue('G5', 'Pengeluaran')
				->setCellValue('H5', 'Penyasuaian')
				->setCellValue('I5', 'Saldo Akhir')
				->setCellValue('J5', 'Stock Opname')
				->setCellValue('K5', 'Selisih')
				->setCellValue('L5', 'Ket');

			$objPHPExcel->setActiveSheetIndex(0)
					->getStyle('A5:L5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$styleArray = array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			
			$i = 7;
			$no = 1;
			
			foreach($result_mutasiBahJad as $result_bahbak) {
				$objPHPExcel->setActiveSheetIndex()->getStyle('A5:L'.$i)->applyFromArray($styleArray);
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$i, $no)
					->setCellValue('B'.$i, $result_bahbak['stock_code'])
					->setCellValue('C'.$i, $result_bahbak['stock_name'])
					->setCellValue('D'.$i, $result_bahbak['uom_name'])
					->setCellValue('E'.$i, str_replace('.','', number_format($result_bahbak['saldo_awal'], 2,',','.')))
					->setCellValue('F'.$i, str_replace('.','', number_format($result_bahbak['pemasukan'], 2,',','.')))
					->setCellValue('G'.$i, str_replace('.','', number_format($result_bahbak['pengeluaran'], 2,',','.')))
					->setCellValue('H'.$i, str_replace('.','', number_format($result_bahbak['penyesuaian'], 2,',','.')))
					->setCellValue('I'.$i, str_replace('.','', number_format($result_bahbak['saldo_akhir'], 2,',','.')))
					->setCellValue('J'.$i, '')
					->setCellValue('K'.$i, '')
					->setCellValue('L'.$i, '');

				$objPHPExcel->setActiveSheetIndex(0)
					->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->setActiveSheetIndex(0)
					->getStyle('D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->setActiveSheetIndex(0)
					->getStyle('E'.$i.':I'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(40);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(18);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(13);
				
				$i++;
				$no++;
			}
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:L1');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:L2');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:L2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:L3');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:L3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:L4');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:A6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:B6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:B6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C5:C6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('C5:C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('C5:C6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D5:D6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('D5:D6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('D5:D6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E5:E6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('E5:E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('E5:E6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F5:F6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('F5:F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('F5:F6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G5:G6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('G5:G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('G5:G6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H5:H6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('H5:H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('H5:H6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I5:I6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J5:J6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('K5:K6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L5:L6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:L6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Pertanggungjawaban Mutasi Barang Jadi '.$tanggalMasuk.' S.D '.$tanggalKeluar.'.xls"');
			
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit; 
			
		}else if($jenis_report == 'Pertanggungjawaban Mutasi Mesin dan Peralatan') {
			$mutasiMesin = array('tanggal_masuk' => $date_masuk_fix, 'tanggal_keluar' => $date_keluar_fix);
			$result_mutasiMesin = $this->Report_BC_model->get_report_mutasi_mesin($mutasiMesin);

			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getActiveSheet()->setTitle("Report Pertanggungjawaban");
			$objPHPExcel->getProperties()->setCreator("System")
				->setLastModifiedBy("System")
				->setTitle("Report Pertanggungjawaban Mutasi Mesin dan Peralatan")
				->setSubject("Report Bea Cukai")
				->setDescription("Report Pertanggungjawaban Mutasi Mesin dan Peralatan")
				->setKeywords("Pertanggungjawaban Mutasi Mesin dan Peralatan")
				->setCategory("Report");
							 
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'LAPORAN '.strtoupper($jenis_report))
				->setCellValue('A2', 'PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA')
				->setCellValue('A3', 'PERIODE '.strtoupper($tanggalMasuk).' S.D '.strtoupper($tanggalKeluar).'');
								
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A5', 'No')
				->setCellValue('B5', 'Kode Barang')
				->setCellValue('C5', 'Nama Barang')
				->setCellValue('D5', 'Sat')
				->setCellValue('E5', 'Saldo Awal')
				->setCellValue('F5', 'Pemasukan')
				->setCellValue('G5', 'Pengeluaran')
				->setCellValue('H5', 'Penyasuaian')
				->setCellValue('I5', 'Saldo Akhir')
				->setCellValue('J5', 'Stock Opname')
				->setCellValue('K5', 'Selisih')
				->setCellValue('l5', 'Ket');
			
			$objPHPExcel->setActiveSheetIndex(0)
					->getStyle('A5:L5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$styleArray = array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			
			$i = 7;
			$no = 1;
			foreach($result_mutasiMesin as $result_bahbak) {
				$objPHPExcel->setActiveSheetIndex()->getStyle('A5:L'.$i)->applyFromArray($styleArray);
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$i, $no)
					->setCellValue('B'.$i, $result_bahbak['stock_code'])
					->setCellValue('C'.$i, $result_bahbak['stock_name'])
					->setCellValue('D'.$i, $result_bahbak['uom_name'])
					->setCellValue('E'.$i, str_replace('.','', number_format($result_bahbak['saldo_awal'], 2,',','.')))
					->setCellValue('F'.$i, str_replace('.','', number_format($result_bahbak['pemasukan'], 2,',','.')))
					->setCellValue('G'.$i, str_replace('.','', number_format($result_bahbak['pengeluaran'], 2,',','.')))
					->setCellValue('H'.$i, str_replace('.','', number_format($result_bahbak['penyesuaian'], 2,',','.')))
					->setCellValue('I'.$i, str_replace('.','', number_format($result_bahbak['saldo_akhir'], 2,',','.')))
					->setCellValue('J'.$i, '')
					->setCellValue('K'.$i, '')
					->setCellValue('L'.$i, '');

				$objPHPExcel->setActiveSheetIndex(0)
					->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->setActiveSheetIndex(0)
					->getStyle('D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->setActiveSheetIndex(0)
					->getStyle('E'.$i.':I'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(28);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(18);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(13);
				
				$i++;
				$no++;
			}
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:L1');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:L2');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:L2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:L3');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:L3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:L4');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:A6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:B6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:B6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C5:C6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('C5:C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('C5:C6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D5:D6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('D5:D6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('D5:D6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E5:E6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('E5:E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('E5:E6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F5:F6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('F5:F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('F5:F6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G5:G6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('G5:G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('G5:G6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H5:H6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('H5:H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('H5:H6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I5:I6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J5:J6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('K5:K6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L5:L6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:L6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Pertanggungjawaban Mutasi Mesin dan Peralatan '.$tanggalMasuk.' S.D '.$tanggalKeluar.'.xls"');
			
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit; 
		
		}else if($jenis_report == 'Pertanggungjawaban Mutasi Barang Reject dan Scrap') {
			$barangRej = array('tanggal_masuk' => $date_masuk_fix, 'tanggal_keluar' => $date_keluar_fix);
			$result_barangRej = $this->Report_BC_model->get_report_barang_reject($barangRej);
			
			/*echo "<pre>";
			echo $jenis_report."<br/>";
			echo $tanggalMasuk."<br/>";
			echo $tanggalKeluar."<br/>";
			print_r($result_barangRej);
			echo "</pre>";
			die;*/

			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getActiveSheet()->setTitle("Report Pertanggungjawaban");
			$objPHPExcel->getProperties()->setCreator("System")
				->setLastModifiedBy("System")
				->setTitle("Report Pertanggungjawaban Barang Reject dan Scrap")
				->setSubject("Report Bea Cukai")
				->setDescription("Report Pertanggungjawaban Barang Reject dan Scrap")
				->setKeywords("Pertanggungjawaban Mutasi Barang Reject dan Scrap")
				->setCategory("Report");
							 
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'LAPORAN '.strtoupper($jenis_report))
				->setCellValue('A2', 'PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA')
				->setCellValue('A3', 'PERIODE '.strtoupper($tanggalMasuk).' S.D '.strtoupper($tanggalKeluar).'');
								
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A5', 'No')
				->setCellValue('B5', 'Kode Barang')
				->setCellValue('C5', 'Nama Barang')
				->setCellValue('D5', 'Sat')
				->setCellValue('E5', 'Saldo Awal')
				->setCellValue('F5', 'Pemasukan')
				->setCellValue('G5', 'Pengeluaran')
				->setCellValue('H5', 'Penyasuaian')
				->setCellValue('I5', 'Saldo Akhir')
				->setCellValue('J5', 'Stock Opname')
				->setCellValue('K5', 'Selisih')
				->setCellValue('l5', 'Ket');

			$objPHPExcel->setActiveSheetIndex(0)
					->getStyle('A5:L5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$styleArray = array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			
			$i = 7;
			$no = 1;
			
			foreach($result_barangRej as $result_bahbak) {
				$objPHPExcel->setActiveSheetIndex()->getStyle('A5:L'.$i)->applyFromArray($styleArray);
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$i, $no)
					->setCellValue('B'.$i, $result_bahbak['stock_code'])
					->setCellValue('C'.$i, $result_bahbak['stock_name'])
					->setCellValue('D'.$i, $result_bahbak['uom_name'])
					->setCellValue('E'.$i, str_replace('.','', number_format($result_bahbak['saldo_awal'], 2,',','.')))
					->setCellValue('F'.$i, str_replace('.','', number_format($result_bahbak['pemasukan'], 2,',','.')))
					->setCellValue('G'.$i, str_replace('.','', number_format($result_bahbak['pengeluaran'], 2,',','.')))
					->setCellValue('H'.$i, str_replace('.','', number_format($result_bahbak['penyesuaian'], 2,',','.')))
					->setCellValue('I'.$i, str_replace('.','', number_format($result_bahbak['saldo_akhir'], 2,',','.')))
					->setCellValue('J'.$i, '')
					->setCellValue('K'.$i, '')
					->setCellValue('L'.$i, '');

				$objPHPExcel->setActiveSheetIndex(0)
					->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->setActiveSheetIndex(0)
					->getStyle('D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->setActiveSheetIndex(0)
					->getStyle('E'.$i.':I'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(28);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(18);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(8);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(13);
				
				$i++;
				$no++;
			}
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:L1');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:L2');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:L2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:L3');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:L3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:L4');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:A6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:B6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:B6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C5:C6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('C5:C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('C5:C6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D5:D6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('D5:D6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('D5:D6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E5:E6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('E5:E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('E5:E6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F5:F6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('F5:F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('F5:F6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G5:G6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('G5:G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('G5:G6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H5:H6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('H5:H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('H5:H6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I5:I6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J5:J6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('K5:K6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L5:L6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:L6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Pertanggungjawaban Barang Reject dan Scrap '.$tanggalMasuk.' S.D '.$tanggalKeluar.'.xls"');
			
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit;
		}else if($jenis_report == 'Pertanggungjawaban Pemusnahan Barang') {
			$pemusnahan = array('tanggal_masuk' => $date_masuk_fix, 'tanggal_keluar' => $date_keluar_fix);
			$result_pemusnahan = $this->Report_BC_model->get_report_pemusnahan($pemusnahan);
			
			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getActiveSheet()->setTitle("Report Pertanggungjawaban Pemusnahan");
			$objPHPExcel->getProperties()->setCreator("System")
				->setLastModifiedBy("System")
				->setTitle("Report Pertanggungjawaban Pemusnahan Barang")
				->setSubject("Report Bea Cukai")
				->setDescription("Report Pertanggungjawaban Pemusnahan Barang")
				->setKeywords("Pertanggungjawaban Pemusnahan Barang")
				->setCategory("Report");
							 
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'LAPORAN '.strtoupper($jenis_report))
				->setCellValue('A2', 'PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA')
				->setCellValue('A3', 'PERIODE '.strtoupper($tanggalMasuk).' S.D '.strtoupper($tanggalKeluar).'');
								
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A5', 'No')
				->setCellValue('B5', 'No Pemusnahan')
				->setCellValue('C5', 'Kode Barang')
				->setCellValue('D5', 'Nama Barang')
				->setCellValue('E5', 'Sat')
				->setCellValue('F5', 'Tanggal Input')
				->setCellValue('G5', 'Tanggal Pengajuan')
				->setCellValue('H5', 'Tanggal Pemusnahan')
				->setCellValue('I5', 'Keterangan');
			
			$styleArray = array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			
			$i = 7;
			$no = 1;
			
			foreach($result_pemusnahan as $result_pemusnahan){
				
				$objPHPExcel->setActiveSheetIndex()->getStyle('A5:I'.$i)->applyFromArray($styleArray);
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$i, $no)
					->setCellValue('B'.$i, $result_pemusnahan['no_pemusnahan'])
					->setCellValue('C'.$i, $result_pemusnahan['stock_code'])
					->setCellValue('D'.$i, $result_pemusnahan['stock_name'])
					->setCellValue('E'.$i, $result_pemusnahan['uom_name'])
					->setCellValue('F'.$i, $result_pemusnahan['tanggal_input'])
					->setCellValue('G'.$i, $result_pemusnahan['tanggal_pengajuan'])
					->setCellValue('H'.$i, $result_pemusnahan['tanggal_pemusnahan'])
					->setCellValue('I'.$i, $result_pemusnahan['keterangan']);
					
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(23);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(45);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(19);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(19);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(19);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(19);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
				
				$i++;
				$no++;
				
				
			}
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:I1');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:I1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:I2');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:I2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:I3');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:I3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:I4');
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:A6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:B6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:B6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C5:C6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('C5:C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('C5:C6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D5:D6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('D5:D6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('D5:D6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E5:E6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('E5:E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('E5:E6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F5:F6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('F5:F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('F5:F6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G5:G6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('G5:G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('G5:G6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H5:H6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('H5:H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('H5:H6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I5:I6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Pertanggungjawaban Pemusnahan Barang '.$tanggalMasuk.' S.D '.$tanggalKeluar.'.xls"');
			
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit;
		}else {
			exit;
		}
	}
}