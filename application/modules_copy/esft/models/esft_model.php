<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Esft_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * This function is get the list data in esft table
	 * @param : $params is where condition for select query
	 */

	public function lists($params = array())
	{
		$sql 	= 'CALL esft_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query(
			$sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			)
		);

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function get_esf($params)
	{
		$sql 	= 'CALL esft_search_esf(?,?,?,?)';

		if ($params['datefrom'] != '') {
			$datefrom = date_create($params['datefrom']);
			$datefrom = date_format($datefrom, "Y-m-d");
		} else {
			$datefrom = '';
		}

		if ($params['dateto'] != '') {
			$dateto = date_create($params['dateto']);
			$dateto = date_format($dateto, "Y-m-d");
		} else {
			$dateto = '';
		}

		$query 	=  $this->db->query(
			$sql,
			array(
				$datefrom,
				$dateto,
				$params['searchby'],
				$params['searchby_text']
			)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	/**
	 * This function is get the list data in eksternal table
	 */
	public function listto()
	{
		$this->db->select('id,name_eksternal');
		$this->db->from('t_eksternal');
		$this->db->where('type_eksternal', 2);
		$this->db->or_where('type_eksternal', 3);

		$query 	= $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
	 * This function is get data in esf table by id
	 * @param : $id is where condition for select query
	 */

	public function detail($id)
	{
		$sql 	= "CALL esft_search_id(?)";

		$query 	= $this->db->query($sql, array(
			$id
		));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
	 * This function is used to Insert Record in esft table
	 * @param : $data - record array 
	 */

	public function add_esft($data)
	{
		$sql 	= 'CALL esft_add(?,?,?,?,?,?,?,?,?,?,?)';

		$this->db->query(
			$sql,
			array(
				$data['esft_no'],
				$data['id_cust_spec'],
				$data['id_esf'],
				$data['esft_date'],
				$data['due_date'],
				$data['to'],
				$data['tool'],
				$data['punching'],
				$data['type_tool'],
				$data['guide_hool'],
				$data['tonnage']
			)
		);

		$this->db->close();
		$this->db->initialize();
	}
		/**
	 * This function is used to Insert Record in esft table
	 * @param : $data - record array 
	 */

	public function edit_esft($data)
	{
		$sql 	= 'CALL esft_edit(?,?,?,?,?,?,?,?,?,?,?,?)';

		$this->db->query(
			$sql,
			array(
				$data['esft_no'],
				$data['id_cust_spec'],
				$data['id_esf'],
				$data['esft_date'],
				$data['due_date'],
				$data['to'],
				$data['tool'],
				$data['punching'],
				$data['type_tool'],
				$data['guide_hool'],
				$data['tonnage'],
				$data['id']
			)
		);

		$this->db->close();
		$this->db->initialize();
	}
}
