	<style>
		.col-customer {
			border: solid 1px #b2b8b7;
		}

		.dt-body-left {
			text-align: left;
			vertical-align: middle;
		}

		.dt-body-right {
			text-align: right;
			vertical-align: middle;
		}

		.dt-body-center {
			text-align: center;
			vertical-align: middle;
		}

		img {
			max-width: 85%;
			height: auto;
		}
	</style>

	<div id="print-area">
		<div class="row">
			<div class="col-md-12">
				<div class="pull-right">
					<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download Engineering Specification Form" id="btn_download">
						<i class="fa fa-download"></i>
					</a>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="col-md-2 logo-place">
					<img src="<?php echo site_url(); ?>assets/images/logo-celebit.jpg" alt="logo-celebit">
				</div>
				<div class="col-md-10">
					<div class="col-md-12 titleReport">
						<h1 id="titleCelebit">CELEBIT</h1>
						<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
						<h4 id="titleAlamat">BANDUNG FACTORY : JL.BUAH DUA RT.01/RW.04 RANCAEKEK - BANDUNG-INDONESIA</h4>
						<h4 id="titleTlp">TEL 62-22-7798 561/7798 542, FAX : 62-22-7798 562 E-MAIL : celebit@melsa.net.id</h4>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-12 text-center">
				<h2 id="titleESF" style="font-weight:900;">ENGINEERING SPECIFICATION FOR TOOL MAKER</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table id="tableTopESFT" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td width="25%">TO : </td>
							<td width="25%"><?php echo $detail[0]['name_eksternal']; ?></td>
							<td width="25%">REF : </td>
							<td width="25%"></td>
						</tr>
						<tr>
							<td>ATTN : </td>
							<td></td>
							<td>ISSUE DATE : </td>
							<td><?php echo $detail[0]['esft_date']; ?></td>
						</tr>
						<tr>
							<td>FROM : </td>
							<td></td>
							<td>DUE DATE : </td>
							<td><?php echo $detail[0]['due_date']; ?></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table id="tableMidESFT" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td width="5%">S/N</td>
							<td width="50%" colspan="2">DESCRIPTION</td>
							<td width="45%">REMARKS</td>
						</tr>
						<tr>
							<td>01</td>
							<td width="25%">TOOL : </td>
							<td width="25%"><?php echo $detail[0]['tool']; ?></td>
							<td></td>
						</tr>
						<tr>
							<td>02</td>
							<td>CUSTOMER : </td>
							<td><?php echo $detail[0]['name_customer']; ?></td>
							<td></td>
						</tr>
						<tr>
							<td>03</td>
							<td>PART NO : </td>
							<td><?php echo $detail[0]['stock_name']; ?></td>
							<td></td>
						</tr>
						<tr>
							<td>04</td>
							<td>PCB MAT'L : </td>
							<td>
								<?php echo $detail[0]['laminate_grade']; ?>,
								Thickness <?php echo $detail[0]['thickness']; ?>mm
								OZ
							</td>
							<td></td>
						</tr>
						<tr>
							<td>05</td>
							<td>PCB OUTLINE SIZE : </td>
							<td></td>
							<td>TOLERANSI : </td>
						</tr>
						<tr>
							<td>06</td>
							<td>PUNCHING : </td>
							<td>
								<?php echo $detail[0]['punching']; ?>
							</td>
							<td>FOLLOW MECH DRAWING - mm / + mm</td>
						</tr>
						<tr>
							<td>07</td>
							<td>TYPE OF TOOL : </td>
							<td><?php echo $detail[0]['type_tool']; ?></td>
							<td>IF NOT THERE - mm / + mm</td>
						</tr>
						<tr>
							<td>08</td>
							<td>GUIDE HOLE PING : </td>
							<td><?php echo $detail[0]['guide_hool']; ?></td>
							<td>Please see attachment for detail</td>
						</tr>
						<tr>
							<td>09</td>
							<td>TONNAGE : </td>
							<td> <?php echo $detail[0]['tonnage']; ?> - </td>
							<td>Direkomendasikan menggunakan toleransi atas</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table id="tableBottomESFT" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td width="5%">S/NO</td>
							<td width="45%" colspan="2">DOCUMENTATION ATTACHED</td>
							<td width="5%">NO</td>
							<td width="45%">REMARKS</td>
						</tr>
						<tr>
							<td>01</td>
							<td colspan="2">DATA LIST : </td>
							<td>1</td>
							<td rowspan="4">GERBER DATA</td>
						</tr>
						<tr>
							<td>02</td>
							<td colspan="2">NO ROUTING TAPE/DISK : </td>
							<td>1</td>
						</tr>
						<tr>
							<td>03</td>
							<td colspan="2">NO DRILL TAPE/DISK : </td>
							<td>1</td>
						</tr>
						<tr>
							<td>04</td>
							<td colspan="2">MECH. DRAWING</td>
							<td>1</td>
						</tr>
						<tr>
							<td>05</td>
							<td colspan="2">PITCH DRAWING</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td rowspan="4">06</td>
							<td width="20%" rowspan="4">A/W FILM</td>
							<td width="25%">PATTERN</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td width="25%">COMPONENT</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td width="25%">CARBON / PEELABLE</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td width="25%">OUTLINE / HOLE CHART</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td rowspan="2">07</td>
							<td width="20%" rowspan="2">PCB SAMPLE</td>
							<td width="25%">DRILL & ROUT</td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td width="25%">PUNCH</td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table id="tableTTDESFT" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="display:none">
					<tbody>
						<tr>
							<td width="25%">PREPARED BY : </td>
							<td width="25%"></td>
							<td width="50%" rowspan="3">NOTE : </td>
						</tr>
						<tr>
							<td>CONFIRMED BY : </td>
							<td></td>
						</tr>
						<tr>
							<td>APPROVED BY : </td>
							<td></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<table id="tablePageTwoTop" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td colspan="2">STD - 006</td>
						</tr>
						<tr>
							<td width="5%">
								<?php
								if ($detail[0]['img_logo'] != null) {
									$url = base_url() . 'uploads/esft/' . $detail[0]['img_logo'];
									echo '<img src="' . 'data:image/jpeg;base64,' . base64_encode(file_get_contents($url)) . '">';
								}
								?>
							</td>
							<td>
								<?php echo $detail[0]['name_eksternal']; ?>
								<br><?php echo $detail[0]['eksternal_address']; ?>
								<br>Tel : <?php echo $detail[0]['phone_1']; ?> Fax : <?php echo $detail[0]['fax']; ?>
							</td>
						</tr>
					</tbody>
				</table>
				<table id="tablePageTwoMiddle" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td rowspan="2">
								To :
								<br>PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA
								<br>BANDUNG FACTORY : JL.BUAH DUA RT.01/RW.04 RANCAEKEK - BANDUNG-INDONESIA
								<br>TEL 62-22-7798 561/7798 542
								<br>FAX : 62-22-7798 562
								<br>ATTN :
								<br>CC :
							</td>
							<td style="text-align: right;" colspan="2">Revision Quotation</td>
						</tr>
						<tr>
							<td>
								Issued Date
								<br>Quote Ref.
								<br>Delivery Date
								<br>Customer Reff
								<br>Payment Terms
								<br>Currency
								<br>Validity
								<br>Relative Quote
							</td>
							<td>
								<?php echo $detail[0]['esft_date']; ?>
								<br>-
								<br><?php echo (strtotime($detail[0]['due_date']) - strtotime($detail[0]['esft_date'])) / (60 * 60 * 24); ?> Working Days
								<br>-
								<br>Days
								<br>-
								<br>
								<br>
							</td>
						</tr>
						<tr>
							<td rowspan="2">
								Dear Sir
								<br>Below please find our quotation for your kind consideration
								<br>Please do not hestitate to contact us if you need any further assistance
							</td>
							<td colspan="2">
								Contact Information
							</td>
						</tr>
						<tr>
							<td>
								Sales Contact
								<br>Technical Contacts
								<br>Tel
								<br>Fax
							</td>
							<td>
								-
								<br>-
								<br>-
								<br>-
							</td>
						</tr>
					</tbody>
				</table>
				<table id="tablePageTwoMiddleBottom" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td width="5%">No</td>
							<td width="35%">Description</td>
							<td width="20%">Qty</td>
							<td width="20%">Unit Price</td>
							<td width="20%">Amount</td>
						</tr>
						<tr>
							<td></td>
							<td>
								New PCB Soft Tooling Dies
								<br>Part No. -
								<br>PCB Board Size x
								<br>Cavity : Cav
								<br>Laminated <?php echo $detail[0]['laminate_grade']; ?>
								<br>Total Tonnage <?php echo $detail[0]['tonnage']; ?>
							</td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>1</td>
							<td>BASIC CHARGE :</td>
							<td> x </td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>2</td>
							<td>OUTLINE :</td>
							<td> x </td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>3</td>
							<td>BREAKING PROFILE :</td>
							<td> x </td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>4</td>
							<td>ROUND HOLE :</td>
							<td> x </td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>5</td>
							<td>
								SLOT : PERIMEETER <= mm <br>Rectanggular Slot
									<br>Full Radius Slot
							</td>
							<td> x </td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>6</td>
							<td>Carbon Milling</td>
							<td> x </td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>7</td>
							<td>Die Plate</td>
							<td> x </td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td colspan="2">
								Note : Price Tool :
								<br>
								<br>
								<br>please note this quotation is based on your information given
							</td>
							<td>
								Sub Total :
								<br>Discount
								<br>Tax 10%
								<br>Best Price
							</td>
							<td></td>
							<td></td>
						</tr>
					</tbody>
				</table>
				<table id="tablePageTwoBottom" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<tbody>
						<tr>
							<td rowspan="3" width="60%">
								This tooling is warranty for 200k punching step of tooling life, including punches spare part
								<br>Repair tooling problems of Tech-2 Process, stucked hole & Technical support on Schedule.
								<br>For Critical design we only Warranty for 25k punching step of tooling life time.
								<br>Tooling life warranty will NOT BE VALID once the modification is carried on the new tooling and
								<br>This Tooling didn't regularly service every 25k - 30k.
								<br> Please Return this QUOTATION upon your confirmation
							</td>
							<td>Receipt of quotation</td>
						</tr>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td>Signature</td>
						</tr>
						<tr>
							<td colspan="2">
								We hope to hear from you soon
								<br>"HAVE A NICE DAY"
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var dataImage = null;
		$(document).ready(function() {

			$('#btn_download').click(function() {
				var doc = new jsPDF('p', 'mm', 'letter');
				var imgData = dataImage;
				var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
				var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

				// FOOTER
				doc.setTextColor(100);
				doc.addImage(imgData, 'JPEG', 5, 5, 25, 25)
				doc.setFontSize(12);
				doc.text($('#titleCelebit').html(), 33, 10, 'left');
				doc.setFontSize(10);
				doc.text($('#titlePerusahaan').html(), 33, 15, 'left');
				doc.setFontSize(11);
				doc.text($('#titleAlamat').html(), 33, 20, 'left');
				doc.setFontSize(11);
				doc.text($('#titleTlp').html(), 33, 25, 'left');

				doc.setDrawColor(116, 119, 122);
				doc.setLineWidth(0.1);
				doc.line(4, 30, 500, 30);

				doc.setFontSize(11);
				doc.text($('#titleESF').html(), pageWidth / 2, 40, 'center');

				// Table Customer Information
				// doc.text($('#titleTableCustomerInformation').html(), pageWidth / 20, 50, 'left');
				// doc.text($('#titleNew').html(), pageWidth / 2, 50, 'left');
				// doc.text($('#titleRevise').html(), pageWidth * 7 / 10, 50, 'left');
				// doc.text($('#titleDate').html(), pageWidth * 8 / 10, 50, 'left');

				doc.autoTable({
					html: '#tableTopESFT',
					theme: 'plain',
					styles: {
						fontSize: 6,
						lineColor: [116, 119, 122],
						lineWidth: 0.1,
						cellWidth: 'auto',

					},
					margin: pageWidth / 20,
					tableWidth: (pageWidth) - (pageWidth / 10),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					columnStyles: {
						0: {
							halign: 'left',
							cellWidth: 30
						},
						1: {
							halign: 'left',
							cellWidth: 30
						},
						2: {
							cellWidth: 30
						},
						3: {
							cellWidth: 30
						},
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 55,
					startX: 'left'
				});

				// Table Layout Plan
				// doc.text($('#titleTableLayoutPlan').html(), pageWidth / 20, 90, 'left');

				doc.autoTable({
					html: '#tableMidESFT',
					theme: 'plain',
					styles: {
						fontSize: 5.5,
						lineColor: [116, 119, 122],
						lineWidth: 0.1,
						cellWidth: 'auto'
					},
					margin: pageWidth / 20,
					tableWidth: (pageWidth) - (pageWidth / 10),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					columnStyles: {
						0: {
							halign: 'left',
							cellWidth: 6
						},
						1: {
							halign: 'left',
							cellWidth: 38
						},
						2: {
							cellWidth: 38
						},
						3: {
							cellWidth: 38
						},

					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 75,
					didDrawCell: function(data) {
						if ((data.cell.section == 'body')) {
							var td = data.cell.raw;
							var img = td.getElementsByTagName('img')[0];
							// data.cell.height = 50;
							var textpos = data.cell.textPos;
							if (img != null)
								doc.addImage(img.src, textpos.x - 0.5, textpos.y - 0.5, data.cell.width - 2, data.cell.height - 2);
						}
					}
				});
				//Table Customer Specification
				// doc.text($('#titleTableCustomerSpecification').html(), pageWidth / 20, 40, 'left');

				doc.autoTable({
					html: '#tableBottomESFT',
					theme: 'plain',
					styles: {
						fontSize: 5.5,
						lineColor: [116, 119, 122],
						lineWidth: 0.1,
						cellWidth: 'auto'
					},
					margin: pageWidth / 20,
					tableWidth: (pageWidth) - (pageWidth / 10),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					columnStyles: {
						0: {
							halign: 'left',
							cellWidth: 6
						},
						1: {
							cellWidth: 30
						},
						2: {
							cellWidth: 30
						},
						3: {
							cellWidth: 30
						},
						3: {
							cellWidth: 24
						},
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 135,
					didDrawCell: function(data) {
						if ((data.cell.section == 'body')) {
							var td = data.cell.raw;
							console.log(td);
							if (td.getElementsByTagName('img')[0] != null)
								var img = td.getElementsByTagName('img')[0];
							// data.cell.height = 50;
							var textpos = data.cell.textPos;
							if (img != null)
								doc.addImage(img.src, textpos.x - 0.5, textpos.y - 0.5, data.cell.width - 2, data.cell.height - 2);
						}
					}
				});

				doc.autoTable({
					html: '#tableTTDESFT',
					theme: 'plain',
					styles: {
						fontSize: 5.5,
						lineColor: [116, 119, 122],
						lineWidth: 0.1
					},
					margin: pageWidth / 20,
					tableWidth: (pageWidth) - (pageWidth / 10),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					columnStyles: {
						0: {
							cellWidth: 30
						},
						1: {
							cellWidth: 30
						},
						2: {
							cellWidth: 60
						},
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 205,
				});

				//next page 
				doc.addPage()

				doc.autoTable({
					html: '#tablePageTwoTop',
					theme: 'plain',
					styles: {
						fontSize: 5.5,
						lineColor: [116, 119, 122],
						lineWidth: 0.1
					},
					margin: pageWidth / 20,
					tableWidth: (pageWidth) - (pageWidth / 10),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					columnStyles: {
						0: {
							cellWidth: 3
						},
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 5,
				});
				doc.autoTable({
					html: '#tablePageTwoMiddle',
					theme: 'plain',
					styles: {
						fontSize: 5.5,
						lineColor: [116, 119, 122],
						lineWidth: 0.1
					},
					margin: pageWidth / 20,
					tableWidth: (pageWidth) - (pageWidth / 10),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					columnStyles: {
						0: {
							cellWidth: 60
						},
						1: {
							cellWidth: 30
						},
						2: {
							cellWidth: 30
						},
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 25,
				});
				doc.autoTable({
					html: '#tablePageTwoMiddleBottom',
					theme: 'plain',
					styles: {
						fontSize: 5.5,
						lineColor: [116, 119, 122],
						lineWidth: 0.1
					},
					margin: pageWidth / 20,
					tableWidth: (pageWidth) - (pageWidth / 10),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					
					// <td width="5%">No</td>
					// <td width="35%">Description</td>
					// <td width="20%">Qty</td>
					// <td width="20%">Unit Price</td>
					// <td width="20%">Amount</td>
					columnStyles: {
						0: {
							cellWidth: 6
						},
						1: {
							cellWidth: 45
						},
						2: {
							cellWidth: 25
						},
						3: {
							cellWidth: 25
						},
						4: {
							cellWidth: 25
						},
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 80,
				});
				doc.autoTable({
					html: '#tablePageTwoBottom',
					theme: 'plain',
					styles: {
						fontSize: 5.5,
						lineColor: [116, 119, 122],
						lineWidth: 0.1
					},
					margin: pageWidth / 20,
					tableWidth: (pageWidth) - (pageWidth / 10),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					
					// <td rowspan="3" width="60%">
					// This tooling is warranty for 200k punching step of tooling life, including punches spare part
					// <br>Repair tooling problems of Tech-2 Process, stucked hole & Technical support on Schedule.
					// <br>For Critical design we only Warranty for 25k punching step of tooling life time.
					// <br>Tooling life warranty will NOT BE VALID once the modification is carried on the new tooling and
					// <br>This Tooling didn't regularly service every 25k - 30k.
					// <br> Please Return this QUOTATION upon your confirmation
					// </td>
					// <td>Receipt of quotation</td>
					columnStyles: {
						0: {
							cellWidth:85
						},
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 165,
				});


				doc.save('Engineer Specification Form <?php echo $detail[0]['esft_date']; ?>.pdf');
			});
		});

		function toDataURL(url, callback) {
			var xhr = new XMLHttpRequest();
			xhr.onload = function() {
				var reader = new FileReader();
				reader.onloadend = function() {
					callback(reader.result);
				}
				reader.readAsDataURL(xhr.response);
			};
			xhr.open('GET', url);
			xhr.responseType = 'blob';
			xhr.send();
		}

		toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
			dataImage = dataUrl;
		})


		function terbilangIND() {
			var bilangan = document.getElementById("totHide").value;
			var kalimat = "";
			var angka = new Array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
			var kata = new Array('', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan');
			var tingkat = new Array('', 'Ribu', 'Juta', 'Milyar', 'Triliun');
			var panjang_bilangan = bilangan.length;

			/* pengujian panjang bilangan */
			if (panjang_bilangan > 15) {
				kalimat = "Diluar Batas";
			} else {
				/* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
				for (i = 1; i <= panjang_bilangan; i++) {
					angka[i] = bilangan.substr(-(i), 1);
				}

				var i = 1;
				var j = 0;

				/* mulai proses iterasi terhadap array angka */
				while (i <= panjang_bilangan) {
					subkalimat = "";
					kata1 = "";
					kata2 = "";
					kata3 = "";

					/* untuk Ratusan */
					if (angka[i + 2] != "0") {
						if (angka[i + 2] == "1") {
							kata1 = "Seratus";
						} else {
							kata1 = kata[angka[i + 2]] + " Ratus";
						}
					}

					/* untuk Puluhan atau Belasan */
					if (angka[i + 1] != "0") {
						if (angka[i + 1] == "1") {
							if (angka[i] == "0") {
								kata2 = "Sepuluh";
							} else if (angka[i] == "1") {
								kata2 = "Sebelas";
							} else {
								kata2 = kata[angka[i]] + " Belas";
							}
						} else {
							kata2 = kata[angka[i + 1]] + " Puluh";
						}
					}

					/* untuk Satuan */
					if (angka[i] != "0") {
						if (angka[i + 1] != "1") {
							kata3 = kata[angka[i]];
						}
					}

					/* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
					if ((angka[i] != "0") || (angka[i + 1] != "0") || (angka[i + 2] != "0")) {
						subkalimat = kata1 + " " + kata2 + " " + kata3 + " " + tingkat[j] + " ";
					}

					/* gabungkan variabe sub kalimat (untuk Satu blok 3 angka) ke variabel kalimat */
					kalimat = subkalimat + kalimat;
					i = i + 3;
					j = j + 1;
				}

				/* mengganti Satu Ribu jadi Seribu jika diperlukan */
				if ((angka[5] == "0") && (angka[6] == "0")) {
					kalimat = kalimat.replace("Satu Ribu", "Seribu");
				}
			}

			document.getElementById("totalBilangan").innerHTML = kalimat;
		}

		function terbilangENG() {
			var bilangan = document.getElementById("totHide").value;
			var kalimat = "";
			var angka = new Array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
			var kata = new Array('', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine');
			var kataBelas = new Array('', 'One', 'Twelve', 'Thir', 'Four', 'Fif', 'Six', 'Seven', 'Eigh', 'Nine');
			var Puluh = new Array('', 'One', 'Twen', 'Thir', 'For', 'Fif', 'Six', 'Seven', 'Eigh', 'Nine');
			var tingkat = new Array('', 'Thousand', 'Million', 'Billion', 'Trillion');
			var panjang_bilangan = bilangan.length;

			/* pengujian panjang bilangan */
			if (panjang_bilangan > 15) {
				kalimat = "Diluar Batas";
			} else {
				/* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
				for (i = 1; i <= panjang_bilangan; i++) {
					angka[i] = bilangan.substr(-(i), 1);
				}

				var i = 1;
				var j = 0;

				/* mulai proses iterasi terhadap array angka */
				while (i <= panjang_bilangan) {
					subkalimat = "";
					kata1 = "";
					kata2 = "";
					kata3 = "";

					/* untuk Ratusan */
					if (angka[i + 2] != "0") {
						if (angka[i + 2] == "1") {
							kata1 = "Hundred";
						} else {
							kata1 = kata[angka[i + 2]] + " Hundred";
						}
					}

					/* untuk Puluhan atau Belasan */
					if (angka[i + 1] != "0") {
						if (angka[i + 1] == "1") {
							if (angka[i] == "0") {
								kata2 = "Ten";
							} else if (angka[i] == "1") {
								kata2 = "Eleven";
							} else if (angka[i] == "2") {
								kata2 = "Twelve";
							} else {
								kata2 = kataBelas[angka[i]] + "teen";
							}
						} else {
							kata2 = kataPuluh[angka[i + 1]] + "ty";
						}
					}

					/* untuk Satuan */
					if (angka[i] != "0") {
						if (angka[i + 1] != "1") {
							kata3 = kata[angka[i]];
						}
					}

					/* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
					if ((angka[i] != "0") || (angka[i + 1] != "0") || (angka[i + 2] != "0")) {
						subkalimat = kata1 + " " + kata2 + " " + kata3 + " " + tingkat[j] + " ";
					}

					/* gabungkan variabe sub kalimat (untuk Satu blok 3 angka) ke variabel kalimat */
					kalimat = subkalimat + kalimat;
					i = i + 3;
					j = j + 1;
				}

				/* mengganti Satu Ribu jadi Seribu jika diperlukan */
				if ((angka[5] == "0") && (angka[6] == "0")) {
					kalimat = kalimat.replace("One Thousand", "Thousand");
				}
			}

			document.getElementById("totalBilangan").innerHTML = kalimat;
		}
	</script>