<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Distributor extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('distributor/distributor_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 * @return string
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	  * This function is redirect to index distributor page
	  * @return Void
	  */
	public function index() {
		$this->template->load('maintemplate', 'distributor/views/index');
	}

	/**
	  * This function is used for showing distributor list
	  * @return Array
	  */
	function lists() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

		$order_fields = array('', 'id', 'name_eksternal', 'eksternal_address');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search = $this->input->get_post('search');

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['type_eksternal'] = 2;
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->distributor_model->lists($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();

		$i = $params['offset'];
		foreach ($list['data'] as $k => $v) {
			$i++;
			$actions = '<div class="btn-group">';
			$actions .= '   <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="editdistributor(\'' . $v['id'] . '\')">';
			$actions .= '       <i class="fa fa-edit"></i>';
			$actions .= '   </button>';
			$actions .= '</div>';
			$actions .= '<div class="btn-group">';
			$actions .= '   <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deletedistributor(\'' . $v['id'] . '\')">';
			$actions .= '       <i class="fa fa-trash"></i>';
			$actions .= '   </button>';
			$actions .= '</div>';
			
			array_push($data, array(
				$i,
				$v['kode_eksternal'],
				$v['name_eksternal'],
				$v['eksternal_address'],
				$v['ship_address'],
				$v['phone_1'],
				$v['phone_2'],
				$v['fax'],
				$v['email'],
				$v['pic'],
				$v['name_eksternal_loc'],
				$actions
					)
			);
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	/**
	  * This function is redirect to add distributor page
	  * @return Void
	  */
	public function add() {
		$result = $this->distributor_model->location();

		$data = array(
			'location' => $result
		);

		$this->load->view('add_modal_view',$data);
	}

	/**
	  * This function is used to add distributor data
	  * @return Array
	  */
	public function add_distributor() {
		$this->form_validation->set_rules('kode_distributor', 'Kode Distributor', 'trim|required|min_length[4]|max_length[100]');
		$this->form_validation->set_rules('dist_name', 'Nama Distributor', 'trim|required|min_length[4]|max_length[50]');
		$this->form_validation->set_rules('dist_address', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('phone_1', 'Telepon', 'trim|required|min_length[4]|max_length[15]');
		$this->form_validation->set_rules('email', 'Email Distributor', 'trim|required');
		$this->form_validation->set_rules('pic', 'PIC', 'trim|required');
		$this->form_validation->set_rules('location', 'Lokasi', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$kode_distributor = $this->Anti_sql_injection($this->input->post('kode_distributor', TRUE));
			$dist_name = $this->Anti_sql_injection($this->input->post('dist_name', TRUE));
			$dist_address = $this->Anti_sql_injection($this->input->post('dist_address', TRUE));
			$ship_address = $this->Anti_sql_injection($this->input->post('ship_address', TRUE));
			$phone_1 = $this->Anti_sql_injection($this->input->post('phone_1', TRUE));
			$phone_2 = $this->Anti_sql_injection($this->input->post('phone_2', TRUE));
			$fax = $this->Anti_sql_injection($this->input->post('fax', TRUE));
			$email = $this->Anti_sql_injection($this->input->post('email', TRUE));
			$pic = $this->Anti_sql_injection($this->input->post('pic', TRUE));
			$location = $this->Anti_sql_injection($this->input->post('location', TRUE));

			$data = array(
				'kode_distributor' 	=> $kode_distributor,
				'name_eksternal' 	=> $dist_name,
				'eksternal_address' => $dist_address,
				'ship_address' 		=> $ship_address,
				'phone_1' 			=> $phone_1,
				'phone_2' 			=> $phone_2,
				'fax' 				=> $fax,
				'email' 			=> $email,
				'pic' 				=> $pic,
				'eksternal_loc' 	=> $location,
				'type_eksternal' 	=> 2
			);

			$add_dist_result = $this->distributor_model->add_distributor($data);
			if ($add_dist_result['result'] > 0) {
				$dataDistributorCoa = array(
					'id_parent'		=> '50100',
					'keterangan'	=> $dist_name,
					'type_coa'		=> 1,
					'id_eksternal'	=> $add_dist_result['lastid']
				);
				$add_dist_coa_result = $this->distributor_model->add_distributor_coa($dataDistributorCoa);

				if($add_dist_coa_result > 0) {
					$msg = 'Berhasil menambahkan distributor';

					$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ' .$kode_distributor);
					$result = array('success' => true, 'message' => $msg);
				}
			}else {
				$msg = 'Gagal menambahkan customer ke database';

				$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ' .$kode_distributor);
				$result = array('success' => false, 'message' => $msg);
			}
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function edit($id) {
		$result = $this->distributor_model->detail($id);
		$location = $this->distributor_model->location();

		$data = array(
			'detail' => $result,
			'location' => $location
		);

		$this->load->view('edit_modal_view', $data);
	}

	public function edit_distributor() {
		$this->form_validation->set_rules('kode_distributor', 'Kode Distributor', 'trim|required|min_length[4]|max_length[100]');
		$this->form_validation->set_rules('dist_name', 'Nama Distributor', 'trim|required|min_length[4]|max_length[50]');
		$this->form_validation->set_rules('dist_address', 'Alamat', 'trim|required');
		$this->form_validation->set_rules('phone_1', 'Telepon', 'trim|required|min_length[4]|max_length[15]');
		$this->form_validation->set_rules('email', 'Email Distributor', 'trim|required');
		$this->form_validation->set_rules('pic', 'PIC', 'trim|required');
		$this->form_validation->set_rules('location', 'Lokasi', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$id = $this->Anti_sql_injection($this->input->post('id', TRUE));
			$kode_distributor = $this->Anti_sql_injection($this->input->post('kode_distributor', TRUE));
			$dist_name = $this->Anti_sql_injection($this->input->post('dist_name', TRUE));
			$dist_address = $this->Anti_sql_injection($this->input->post('dist_address', TRUE));
			$ship_address = $this->Anti_sql_injection($this->input->post('ship_address', TRUE));
			$phone_1 = $this->Anti_sql_injection($this->input->post('phone_1', TRUE));
			$phone_2 = $this->Anti_sql_injection($this->input->post('phone_2', TRUE));
			$fax = $this->Anti_sql_injection($this->input->post('fax', TRUE));
			$email = $this->Anti_sql_injection($this->input->post('email', TRUE));
			$pic = $this->Anti_sql_injection($this->input->post('pic', TRUE));
			$location = $this->Anti_sql_injection($this->input->post('location', TRUE));

			$data = array(
				'id' => $id,
				'kode_distributor' => $kode_distributor,
				'name_eksternal' => $dist_name,
				'eksternal_address' => $dist_address,
				'ship_address' => $ship_address,
				'phone_1' => $phone_1,
				'phone_2' => $phone_2,
				'fax' => $fax,
				'email' => $email,
				'pic' => $pic,
				'eksternal_loc' => $location
			);

			$dataCoa = array(
				'id_eksternal'	=> $id,
				'keterangan'	=> $dist_name
			);
			
			$result_dist 		= $this->distributor_model->edit_distributor($data);
			$result_dist_coa	= $this->distributor_model->edit_distributor_coa($dataCoa);

			if($result_dist['status'] > 0 && $result_dist_coa['status'] > 0) {
				$msg = 'Berhasil merubah data distributor';

				$this->log_activity->insert_activity('update', $msg. ' dengan kode distributor ' .$kode_distributor);
				$result = array('success' => true, 'message' => $msg);
			}else{
				$msg = 'gagal merubah data distributor di database';

				$this->log_activity->insert_activity('update', $msg. ' dengan kode distributor ' .$kode_distributor);
				$result = array('success' => true, 'message' => $msg);
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function delete_distributor() {
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);

		$result_dist 		= $this->distributor_model->delete_distributor($params['id']);
		$result_dist_coa	= $this->distributor_model->delete_distributor_coa($params['id']);

		$msg = 'Berhasil menghapus data distributor.';
		$result = array('success' => true, 'message' => $msg);
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function check_kode(){
		$this->form_validation->set_rules('kode_distributor', 'Kode_Distributor', 'trim|required|min_length[4]|max_length[100]|is_unique[t_eksternal.kode_eksternal]');
		$this->form_validation->set_message('is_unique', 'Kode Distributor Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'Kode Distributor Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}
}