<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Distributor_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	/**
      * This function is get the list data in eksternal table
      * @param : $params is where condition for select query
      */

	public function lists($params = array())
	{
		$sql_all 	= 'CALL eksternal_list_all(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				1,
				NULL,
				NULL,
				NULL,
				NULL,
				''
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL eksternal_list(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['type_eksternal'],
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	/**
      * This function is get data in eksternal table by id
      * @param : $id is where condition for select query
      */

	public function detail($id)
	{
		$sql 	= 'CALL eksternal_search_id(?)';

		$query 	= $this->db->query($sql,array(
				$id
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_distributor($data) {
		$sql 	= 'CALL eksternal_add(?,?,?,?,?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['kode_distributor'],
			$data['name_eksternal'],
			$data['eksternal_address'],
			$data['ship_address'],
			$data['phone_1'],
			$data['phone_2'],
			$data['fax'],
			$data['email'],
			$data['pic'],
			$data['eksternal_loc'],
			$data['type_eksternal']
		));
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_distributor_coa($data) {
		$sql 	= 'CALL coa_add(?,?,?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['id_parent'],
			$data['keterangan'],
			$data['type_coa'],
			$data['id_eksternal']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_distributor($data) {
		$sql 	= 'CALL eksternal_update(?,?,?,?,?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id'],
				$data['kode_distributor'],
				$data['name_eksternal'],
				$data['eksternal_address'],
				$data['ship_address'],
				$data['phone_1'],
				$data['phone_2'],
				$data['fax'],
				$data['email'],
				$data['pic'],
				$data['eksternal_loc']
			));

		$result['code']	= $this->db->affected_rows();
		$result['status'] = 1;

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_distributor_coa($data) {
		$sql 	= 'CALL cust_coa_update(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['id_eksternal'],
			$data['keterangan']
		));

		$result['code']	= $this->db->affected_rows();
		$result['status'] = 1;

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	function delete_distributor($id) {
		$sql 	= 'CALL cust_delete(?)';

		$query 	=  $this->db->query($sql, array(
			$id
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	function delete_distributor_coa($id) {
		$sql 	= 'CALL cust_coa_delete(?)';

		$query 	=  $this->db->query($sql, array(
			$id
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function location()
	{
		$query 	= $this->db->get("m_eksternal_loc");
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
}