<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Quotation_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * This function is get the list data in po_quotation table
	 * @param : $params is where condition for select query
	 */

	public function doedit_schedule($data)
	{
		$this->db->where('id_order', $data[0]['id_order']);
		$this->db->delete('t_schedule_shipping');

		$sql 	= 'CALL schedule_add(?, ?, ?, ?, ?, ?)';

		$dataLen = count($data);
		$delivery = '';
		for ($i = 0; $i < $dataLen; $i++) {
			$date_shipping_plan = ($data[$i]['date_shipping_plan'] != '') ? $data[$i]['date_shipping_plan'] : NULL;
			$this->db->query($sql, array(
				$data[$i]['id_order'],
				$data[$i]['qty_shipping'],
				$data[$i]['type_shipping'],
				$data[$i]['note_shipping'],
				$date_shipping_plan,
				NULL
			));

			if (intval($data[$i]['type_shipping']) == 0) {
				$delivery = 'ASAP';
			} else if (intval($data[$i]['type_shipping']) == 1) {
				$delivery = 'Menunggu info';
			} else {
				if ($i == 0) {
					$delivery = $date_shipping_plan;
				} else {
					$delivery .= ',' . $date_shipping_plan;
				}
			}
		}

		$return = array(
			'delivery' => $delivery
		);

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function check_po($id)
	{
		$sql = 'SELECT COUNT(DISTINCT(a.no_po)) AS check_po , no_po 
			FROM t_order a
			LEFT JOIN m_material b ON a.id_produk = b.id
			WHERE a.id_po_quotation = ' . $id;

		$query =   $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function edit_po($data)
	{
		$return = array(
			'order_no' => $data['order_no'],
			'order_date' => $data['order_date'],
			'transport' => $data['transport'],
			'packing' => $data['packing'],
			'status' => 11
		);

		$this->db->where('id', $data['id']);
		$this->db->update('t_po_quotation', $return);

		foreach ($data['order'] as $orderkey) {
			$orderdata = array(
				'qty' => $orderkey['qty'],
				'amount_price' => $orderkey['amount_price']
			);

			$this->db->where('id_order', $orderkey['id_order']);
			$this->db->update('t_order', $orderdata);
		}

		// $sql 	= 'update m_material LEFT JOIN t_order ON t_order.id_produk=m_material.id set `status`=6 where id_po_quotation=?';

		// $this->db->query($sql,array($data['id']));

		$note   = 'PO ' . $data['order_no'];

		$sql 	= 'SELECT COUNT(id) AS jumlah FROM t_coa_value WHERE note = ?;';
		$query 	= $this->db->query(
			$sql,
			array($note)
		);
		$countcoa = $query->row_array();

		if (intval($countcoa) == 0) {
			$sql 	= 'CALL coavalue_add(?,?,?,?,?,?,?)';
			$this->db->query($sql, array(121, 1, 1, $data['amount'], NULL, 0, $note));
		} else {
			$return = array(
				'value' => $data['amount']
			);

			$this->db->where('note', $note);
			$this->db->update('t_coa_value', $return);
		}

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function detail_schedule($id)
	{
		$sql 	= 'CALL schedule_search_id(?)';

		$query 	= $this->db->query($sql, array($id));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function lists($params = array())
	{
		$sql_all 	= 'CALL po_list_all(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query(
			$sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				$params['filter'],
				0
			)
		);

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL po_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query(
			$sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			)
		);

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total_row,
			'total' => $total_row,
		);

		return $return;
	}

	/**
	 * This function is get the list data in valas table
	 */
	public function valas()
	{
		$this->db->select('valas_id,nama_valas');
		$this->db->from('m_valas');

		$query 	= $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
	 * This function is get the list data in eksternal table
	 */
	public function customer()
	{
		// $this->db->select('id,name_eksternal,kode_eksternal');
		// $this->db->from('t_eksternal');
		// $this->db->where('type_eksternal', 1);

		$query =   $this->db->query("SELECT id,name_eksternal,kode_eksternal from t_eksternal where
		type_eksternal =1 and kode_eksternal IS NOT NULL ");

		//$query 	= $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
	 * This function is get the list data in material table
	 */
	public function item()
	{
		$this->db->select('id,stock_name, stock_code');
		$this->db->from('m_material');
		//$this->db->where('status', 1);
		$this->db->where('type', 4);

		$query 	= $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
	 * This function is used to Insert Record in po_quotation and order table
	 * @param : $data - record array 
	 */


	public function add_quotation_ro($data)
	{
		$sql 	= 'CALL po_add_ro(?,?,?,?,?,?,?,?,?,?,?,?)';

		$query = $this->db->query($sql, array(
			$data['cust_id'],
			$data['issue_date'],
			$data['issue_date'],
			NULL,
			$data['term_of_payment'],
			$data['transport'],
			$data['packing'],
			$data['valas'],
			$data['rate'],
			11,
			$data['no'],
			$data['term_of_pay']
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$return['result'] = $query->result_array();
		$return['code'] = $this->db->affected_rows();
		$return['lastid'] = $lastid;

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_quotation($data)
	{
		$sql 	= 'CALL po_add(?,?,?,?,?,?,?,?,?)';

		$this->db->query(
			$sql,
			array(
				$data['cust_id'],
				$data['issue_date'],
				NULL,
				NULL,
				$data['term_of_pay'],
				NULL,
				NULL,
				$data['id_valas'],
				3
			)
		);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$id_po_quotation = $row['LAST_INSERT_ID()'];

		$id_produk = $data['id_produk'];
		$id_produklen = count($data['id_produk']);
		$price = $data['price'];
		$diskon = $data['diskon'];

		$bom_no = strtoupper(randomString());

		for ($i = 0; $i < $id_produklen; $i++) {
			$sql 	= 'INSERT INTO m_material(			
												no_bc,
												stock_code,
												stock_name,
												stock_description,
												unit,
												type,
												qty,
												treshold,
												id_properties,
												id_gudang,
												status
								  )
								  SELECT 
								  		no_bc,
										stock_code,
										stock_name,
										stock_description,
										unit,
										type,
										qty,
										treshold,
										id_properties,
										id_gudang,
										3
								  FROM
								  		m_material
								  WHERE
								  		id=?';

			$query 	= $this->db->query($sql, array($id_produk[$i]));

			$id_productnew	= $this->db->insert_id();

			// $sql 	= 'CALL order_add(?,?,?,?,?,?)';

			// $this->db->query($sql,
			// array(
			// $id_po_quotation,
			// $id_productnew,
			// $id_produk[$i],
			// NULL,
			// $price[$i],
			// $diskon[$i]
			// ));

			// $sql 	= 'CALL bom_add(?,?,?,?,?,?)';

			// $this->db->query($sql,
			// array(
			// $bom_no,
			// $id_po_quotation,
			// $id_produk[$i],
			// 0,
			// 0,
			// 0
			// ));

			// $sql 	= '	INSERT INTO t_bom_m
			// SELECT NULL,'.$id_produk[$i].' AS ID,category,material,NULL,0 FROM `t_supp`
			// GROUP BY material
			// ORDER BY seq';

			// //$query 	= $this->db->query($sql,array($id_produk[$i]));
			// $query 	= $this->db->query($sql);

			// $sql2 	= '	INSERT INTO t_bom_m
			// (
			// id_stock,
			// category,
			// material,
			// id_material_comp
			// ) VALUES(
			// '.$id_produk[$i].',
			// "MAIN",
			// "MAIN",
			// NULL

			// )';

			// //$query2 	= $this->db->query($sql2,array($id_produk[$i]));
			// $query2 	= $this->db->query($sql2);

			// $sql3 	= "

			// INSERT INTO `t_process_mat`
			// SELECT NULL AS AAS,".$id_produk[$i]." AS IDP,id,1,'' as os,'' ink, '' as lot FROM `t_process_flow`
			// ORDER BY `level`


			// ";

			// //$query3 	= $this->db->query($sql3,array($id_produk[$i]));
			// $query3 	= $this->db->query($sql3);


			// $sql4 	= 'INSERT INTO t_prod_layout
			// (
			// id_material
			// ) VALUES(
			// '.$id_produk[$i].'
			// )';

			// //$query4 	= $this->db->query($sql4,array($id_produk[$i]));
			// $query4 	= $this->db->query($sql4);
			//$this->db->query($sql);
		}

		$this->db->close();
		$this->db->initialize();
	}

	/**
	 * This function is used to delete po_quotation and t_order
	 * @param: $id - id of po_quotation table
	 */
	function delete_quotation($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('t_po_quotation');

		$this->db->where('id_po_quotation', $id);
		$this->db->delete('t_order');

		$this->db->where('id_po_quot', $id);
		$this->db->delete('t_bom');

		$this->db->close();
		$this->db->initialize();
	}

	/**
	 * This function is get data in po_quotation table by id
	 * @param : $id is where condition for select query
	 */

	public function po_detail($id)
	{
		$sql 	= 'CALL po_search_id(?)';

		$query 	= $this->db->query($sql, array(
			$id
		));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
	 * This function is get data in t_order table by id
	 * @param : $id is where condition for select query
	 */

	public function add_order($data)
	{
		$sql 	= 'CALL order_add_req(?,?,?,?,?,?,?,?,?)';

		$query = $this->db->query($sql, array(
			$data['id_po_quotation'],
			$data['id_product'],
			$data['id_product'],
			$data['qty'],
			$data['price'],
			$data['diskon'],
			$data['disc_price'],
			$data['amount_price'],
			$data['no_po']
		));

		$return['result'] = $query->result_array();
		$return['code'] = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_request_item($id_po_quotation, $id_material, $arrQty)
	{
		$sql 	= 'INSERT INTO t_request_item(id_po_quotation,id_produk,qty,status) VALUES (?,?,?,"Approve")';

		$query = $this->db->query($sql, array(
			$id_po_quotation,
			$id_material,
			$arrQty
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$return['result'] = $query->result_array();
		$return['code'] = $this->db->affected_rows();
		$return['lastid'] = $lastid;


		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function order_detail($id)
	{
		// $this->db->select('id_order,id_produk,id_produkold,price,diskon');
		// $this->db->from('t_order');
		// $this->db->where('id_po_quotation', $id);

		//$query 	= $this->db->get();

		$query =   $this->db->query("SELECT a.id_order,a.id_produk,id_produkold,a.price,a.diskon,stock_name,a.qty,c.`status`,a.no_po FROM t_order a 
		LEFT JOIN m_material b ON a.id_produk = b.id
		LEFT JOIN `t_request_item` c ON a.`id_produk` = c.`id_produk` AND a.`id_po_quotation` = c.`id_po_quotation`
		
		WHERE a.id_po_quotation = " . $id . " AND c.`status` = 'Approve'
		");


		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
	 * This function is used to Insert Record in po_quotation and order table
	 * @param : $data - record array 
	 */

	public function edit_quotation($data)
	{

		$id_produklen = count($data['id_order']);
		$id_order = $data['id_order'];
		$qty = $data['qty'];
		$price = $data['price'];
		$diskon = $data['diskon'];
		$rate = $data['rate'];

		$this->db->query("update t_po_quotation set rate = '" . $data['rate'] . "',issue_date = '" . $data['issue_date'] . "', valas_id = " . $data['id_valas'] . ", price_term = '" . $data['term_of_pay'] . "' where id = " . $data['id_quotation'] . " ");


		for ($i = 0; $i < $id_produklen; $i++) {

			$disc_price = $price[$i] - $diskon[$i];
			$this->db->query("update t_order set price = '" . $price[$i] . "', diskon = '" . $diskon[$i] . "', disc_price = '" . $disc_price . "', amount_price = " . $disc_price . "*qty 
			where id_order = " . $id_order[$i] . " ");
		}

		$this->db->close();
		$this->db->initialize();
	}

	/**
	 * This function is get data in po_quotation table by id
	 * @param : $id is where condition for select query
	 */

	public function detail($id)
	{
		$this->db->select("name_eksternal,
							order_no,
							order_date,
							transport,
							packing,
							issue_date,
							`price_term`,
							GROUP_CONCAT(t_order.id_order SEPARATOR '~') AS id_order,
							GROUP_CONCAT(t_order.id_produk SEPARATOR '~') AS id_produk,
							GROUP_CONCAT(t_order.price SEPARATOR '~') AS price,
							GROUP_CONCAT(t_order.diskon SEPARATOR '~') AS diskon,
							GROUP_CONCAT(disc_price SEPARATOR '~') AS disc_price,
							nama_valas,
							GROUP_CONCAT(stock_name SEPARATOR '~') AS stock_name,
							GROUP_CONCAT(t_order.qty SEPARATOR '~') AS qty,
							GROUP_CONCAT(amount_price SEPARATOR '~') AS amount_price,
							t_status_g.`status`,
	                        approval_date,
	                        notes");
		$this->db->from('t_po_quotation');
		$this->db->join('t_eksternal', 't_po_quotation.cust_id=t_eksternal.id');
		$this->db->join('t_order', 't_po_quotation.id=t_order.id_po_quotation');
		$this->db->join('m_valas', 't_po_quotation.valas_id=m_valas.valas_id');
		$this->db->join('m_material', 't_order.id_produk=m_material.id');
		$this->db->join('t_status_g', 't_po_quotation.status=t_status_g.id_status');
		$this->db->join('t_request_item', 't_order.id_produk=t_request_item.id_produk AND t_order.`id_po_quotation` = t_request_item.`id_po_quotation` ');
		$this->db->where('t_po_quotation.id', $id);
		$this->db->where('t_request_item.status', 'Approve');

		// $query =   $this->db->query("SELECT a.id_order,a.id_produk,id_produkold,a.price,a.diskon,stock_name,a.qty,c.`status` FROM t_order a 
		// LEFT JOIN m_material b ON a.id_produk = b.id
		// LEFT JOIN `t_request_item` c ON a.`id_produk` = c.`id_produk` AND a.`id_po_quotation` = c.`id_po_quotation`

		// WHERE a.id_po_quotation = ".$id." AND c.`status` = 'Approve'
		// ");

		$query 	= $this->db->get();

		$return = $query->result_array();

		$orderArray = explode('~', $return[0]['id_order']);
		$idArray = explode('~', $return[0]['id_produk']);
		$stockArray = explode('~', $return[0]['stock_name']);
		$priceArray = explode('~', $return[0]['price']);
		$diskonArray = explode('~', $return[0]['diskon']);
		$disc_priceArray = explode('~', $return[0]['disc_price']);
		$qtyArray = explode('~', $return[0]['qty']);
		$amount_priceArray = explode('~', $return[0]['amount_price']);

		$itemCount = count($stockArray);

		$data = array();
		for ($i = 0; $i < $itemCount; $i++) {
			$this->db->select('type_shipping,date_shipping_plan');
			$this->db->from('t_schedule_shipping');
			$this->db->where('id_order', $orderArray[$i]);

			$query_shipping = $this->db->get();

			$shipping = $query_shipping->result_array();

			if (count($shipping)) {
				if ($shipping[0]["type_shipping"] == '0') {
					$delivery = 'ASAP';
				} else if ($shipping[0]["type_shipping"] == '1') {
					$delivery = 'Menunggu info';
				} else {
					$delivery = '';
					$j = 0;
					foreach ($shipping as $key) {
						if ($j == 0) {
							$delivery = $key['date_shipping_plan'];
						} else {
							$delivery .= ',' . $key['date_shipping_plan'];
						}
						$j++;
					}
				}
			} else {
				$delivery = '-';
			}

			$qty = (!isset($qtyArray[$i]) || $qtyArray[$i] == '') ? '0' : $qtyArray[$i];
			$amount_price = (!isset($amount_priceArray[$i]) || $amount_priceArray[$i] == '') ? '0' : $amount_priceArray[$i];

			$data[] = array(
				"id_order" => $orderArray[$i],
				"id_produk" => $idArray[$i],
				"stock_name" => $stockArray[$i],
				"price" => $priceArray[$i],
				"diskon" => $diskonArray[$i],
				"disc_price" => $disc_priceArray[$i],
				"qty" => $qty,
				"amount_price" => $amount_price,
				"delivery" => $delivery
			);
		}

		$approval_date = ($return[0]['approval_date'] == '') ? '-' : date_format(date_create($return[0]['approval_date']), 'd F Y');
		$notes = ($return[0]['notes'] == '') ? '-' : $return[0]['notes'];

		$result = array(
			"id" => $id,
			"name_eksternal" => $return[0]['name_eksternal'],
			"issue_date" => date_format(date_create($return[0]['issue_date']), 'd F Y'),
			"nama_valas" => $return[0]['nama_valas'],
			"price_term" => $return[0]['price_term'],
			"status" => $return[0]['status'],
			"approval_date" => $approval_date,
			"notes" => $notes,
			"order_no" => $return[0]['order_no'],
			"order_date" => $return[0]['order_date'],
			"transport" => $return[0]['transport'],
			"packing" => $return[0]['packing'],
			"item" => $data
		);

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	/**
	 * This function is used to Update Record in po_quotation table
	 * @param : $data - updated array 
	 */

	public function change_status($data, $no)
	{
		$array = array(
			'status' => $data['status'],
			'notes' => $data['notes'],
			'approval_date' => date('Y-m-d'),
			'order_date' => date('Y-m-d'),
			'order_no' => $no
		);

		$this->db->where('id', $data['id_quotation']);
		$this->db->update('t_po_quotation', $array);



		$this->db->close();
		$this->db->initialize();
	}
}
