<form class="form-horizontal form-label-left" id="change_status" role="form" action="<?php echo base_url('quotation/change_status');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Issue Date
        	<span class="required">
        		<sup>*</sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
			<div class="input-group date">
			   <input data-parsley-maxlength="255" type="text" id="department" name="department" class="form-control col-md-7 col-xs-12" placeholder="Department" required="required" value="<?php if(isset($detail['issue_date'])){ echo $detail['issue_date']; }?>" disabled="disabled" autocomplete="off">
			</div>
		</div>
   	</div>

   	<div class="item form-group" id="subdetail_temp">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id">Currency</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="text" id="department" name="department" class="form-control col-md-7 col-xs-12" placeholder="Department" required="required" value="<?php if(isset($detail['nama_valas'])){ echo $detail['nama_valas']; }?>" disabled="disabled" autocomplete="off">
		</div>
	</div>

   	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="term_of_pay">Term of Payment</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="text" id="department" name="department" class="form-control col-md-7 col-xs-12" placeholder="Department" required="required" value="<?php if(isset($detail['price_term'])){ echo $detail['price_term']; }?>" disabled="disabled" autocomplete="off">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="dist">Customer</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="text" id="department" name="department" class="form-control col-md-7 col-xs-12" placeholder="Department" required="required" value="<?php if(isset($detail['name_eksternal'])){ echo $detail['name_eksternal']; }?>" disabled="disabled" autocomplete="off">
		</div>
	</div>
	
		 <div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Order Date
        	<span class="required">
        		<sup>*</sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
			<div class="input-group date">
			   <input placeholder="Order Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="order_date" name="order_date" required="required" value="" autocomplete="off">
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
   	</div>

   	<div class="item form-group">
    	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Transport
    		<span class="required">
    			<sup>*</sup>
    		</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
           	<input data-parsley-maxlength="255" type="text" id="transport" name="transport" class="form-control col-md-7 col-xs-12" placeholder="Transport" autocomplete="off">
        </div>
     </div>

     <div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="packing">Packing</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="packing" name="packing" style="width: 100%" autocomplete="off">
				<option value="0" <?php if(isset($packing)){ if( $packing == '0' ){ echo "selected"; } }?>>-- Select Packing --</option>
				<option value="1" <?php if(isset($packing)){ if( $packing == '1' ){ echo "selected"; } }?>>Standard Packing</option>
			</select>
		</div>
	</div>

	<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Item : </label>
    </div>

    <div class="item form-group">
		<table id="listedititem" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>No</th>
					<th>Items</th>
					<th>Qty</th>
					<th>Normal Price</th>
					<th>Discount</th>
					<th>Delivery</th>
					<th>Option</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$id_produkArray = array(); 
				$i=1; 
				foreach($detail['item'] as $orderkey) {
					$id_produkArray[] = $orderkey['id_produk'];
				?>
					<tr>
						<td style="padding-top: 17px;"><?php echo $i; ?></td>
	                  	<td>
	                  		<?php if(isset($orderkey['stock_name'])){ echo $orderkey['stock_name']; }?>
	                  	</td>
		                <td>
		                	<?php if(isset($orderkey['price'])){ echo number_format($orderkey['qty'],0,",","."); }?>
		                </td>
						 <td>
		                	<?php if(isset($orderkey['price'])){ echo number_format($orderkey['price'],5,",","."); }?>
		                </td>
		                <td>
		                	<?php if(isset($orderkey['diskon'])){ echo $orderkey['diskon']; }?> %
		                </td>
						 <td>
		                	<?php if(isset($orderkey['diskon'])){ echo $orderkey['diskon']; }?> %
		                </td>
						 <td>
		                	<?php if(isset($orderkey['diskon'])){ echo $orderkey['diskon']; }?> %
		                </td>
		            </tr>
	            <?php $i++;} ?>
			</tbody>
		</table>
	</div>

	<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Status 
        	<span class="required">
        		<sup>*</sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
            <select class="form-control" name="status" id="status" style="width: 100%" required>
              	<option value="5">Accept</option>
              	<option value="2">Reject</option>
            </select>
        </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Notes</label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <textarea data-parsley-maxlength="255" type="text" id="notes" name="notes" class="form-control col-md-7 col-xs-12" placeholder="Notes"></textarea>
      </div>
    </div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Status</button>
		</div>
	</div>

	<input type="hidden" id="id_quotation" value="<?php if(isset($detail['id'])){ echo $detail['id']; }?>">
	<input type="hidden" id="id_produk" value="<?php echo implode(',',$id_produkArray);?>">
</form>
<!-- /page content -->

<script type="text/javascript">
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
		
		$("#order_date").datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});
		
	});

	$('#change_status').on('submit',(function(e) {
		var id_quotation= $('#id_quotation').val(),
			status      = $('#status').val(),
			notes 	    = $('#notes').val(),
			id_produk 	= $('#id_produk').val();

		var datapost={
				"id_quotation"   : id_quotation,
				"status"         : status,
				"notes"          : notes,
				"id_produk"      : id_produk
			};
			
	    $('#btn-submit').attr('disabled','disabled');
	    $('#btn-submit').text("Mengubah status...");
	    e.preventDefault();

	    $.ajax({
	        type:'POST',
	        url: $(this).attr('action'),
	        data : JSON.stringify(datapost),
	        dataType: 'json',
			contentType: 'application/json; charset=utf-8',
	        success: function(response) {
	            if (response.success == true) {
	              $('.panel-heading button').trigger('click');
	                listquotation();
	                swal({
	                  title: 'Success!',
	                  text: response.message,
	                  type: 'success',
	                  showCancelButton: false,
	                  confirmButtonText: 'Ok'
	                }).then(function () {
	                });
	            } else{
	                $('#btn-submit').removeAttr('disabled');
	                $('#btn-submit').text("Edit Status");
	                swal("Failed!", response.message, "error");
	            }
	        }
	    }).fail(function(xhr, status, message) {
	        $('#btn-submit').removeAttr('disabled');
	        $('#btn-submit').text("Edit Status");
	        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
	    });
	  }));
</script>
