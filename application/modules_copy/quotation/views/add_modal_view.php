<form class="form-horizontal form-label-left" id="add_quotation" role="form" action="<?php echo base_url('quotation/add_quotation');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p style="text-align:center;">Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	 <div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Issue Date
        	<span class="required">
        		<sup>*</sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
			<div class="input-group date">
			   <input placeholder="Issue Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="issue_date" name="issue_date" required="required" autocomplete="off" value="<?php echo date('Y-m-d');?>">
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
   	</div>

   	<div class="item form-group" id="subdetail_temp">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id">Currency <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="id_valas" name="id_valas" style="width: 100%" required autocomplete="off">
				<?php foreach($valas as $key) { ?>
					<option value="<?php echo $key['valas_id']; ?>" ><?php echo $key['nama_valas']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

   	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="term_of_pay">Term of Payment <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="term_of_pay" name="term_of_pay" style="width: 100%" required autocomplete="off">
				<option value="" >-- Select Term --</option>
				<option value="CASH">CASH</option>
				<option value="0">None</option>
				<option value="1 Day">1 Day</option>
				<option value="15 Days">15 Days</option>
				<option value="30 Days">30 Days</option>
				<option value="60 Days">60 Days</option>
				<option value="90 Days">90 Days</option>
				<option value="120 Days">120 Days</option>
				<option value="180 Days">180 Days</option>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="dist">Customer <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="cust_id" name="cust_id" style="width: 100%" required autocomplete="off">
				<option value="" >-- Select Customer --</option>
				<?php foreach($customer as $dk) { ?>
					<option value="<?php echo $dk['id']; ?>"><?php echo $dk['name_eksternal']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Item : </label>
    </div>

	<div class="item form-group">
		<table id="listadditem" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>No</th>
					<th>Items</th>
					<th>Normal Price</th>
					<th>Discount</th>
					<th>Option</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="padding-top: 17px;">1</td>
                  	<td>
                  		<select class="form-control select-control item-control" id="item_id0" name="item_id0" style="width: 340px" required autocomplete="off">
							<option value="" >-- Select Item --</option>
							<?php foreach($item as $key) { ?>
								<option value="<?php echo $key['id']; ?>"><?php echo $key['stock_name']; ?></option>
							<?php } ?>
						</select>
                  	</td>
	                <td>
	                	<input type="number" class="form-control normal-control" min="0" id="normal_price0" name="normal_price0" style="height:37.99px;width: 90px;" step=".0001" value="0" autocomplete="off">
	                </td>
	                <td>
	                	<input type="number" class="form-control disc-control" min="0" id="disc_price0" name="disc_price0" style="height:37.99px;display: inline-block;width: 90px;" step=".0001" value="0" autocomplete="off"><label style="margin-left: 5px;">%</label>
	                </td>
	                <td>
	                	<div class="btn-group">
	                		<button class="btn btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Tambah Baru" onClick="add_item()">
	                			<i class="fa fa-plus"></i>
	                		</button>
	                	</div>
	                </td>
	            </tr>
			</tbody>
		</table>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Quotation</button>
		</div>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	var items = [],
		itemsLen = 1;
	'<?php
	    foreach($item as $key) {
	      $id = $key['id'];
	      $stock_name = $key['stock_name'];
  ?>'
  	  var id = "<?php echo $id; ?>";
      var stock_name = "<?php echo $stock_name; ?>";

      var dataitem = {
          id,
          stock_name
      };

      items.push(dataitem);
  '<?php    
    	}
  ?>'
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();

		$("#issue_date").datepicker({
			  format: 'yyyy-mm-dd',
			  autoclose: true,
			  todayHighlight: true,
		  });
		
		$("#cust_id").select2();
		$(".select-control").select2();
	});

	function add_item(){
		$('#listadditem .btn-group .btn').removeClass('btn-primary');
		$('#listadditem .btn-group .btn').addClass('btn-danger');
		$('#listadditem .btn-group .btn').attr('title','Hapus');
		$('#listadditem .btn-group .btn').attr('data-original-title','Hapus');
		$('#listadditem .btn-group .btn').removeAttr('onClick');
		$('#listadditem .btn-group .fa').removeClass('fa-plus');
		$('#listadditem .btn-group .fa').addClass('fa-trash');

		var itemCount = items.length,
			itemCur = itemsLen;
			itemsLen++;

		var element = '<tr>';
			element += '	<td style="padding-top: 17px;">'+itemsLen+'</td>';
			element += '	<td>';
			element += '		<select class="form-control select-control item-control" id="item_id'+itemCur+'" name="item_id'+itemCur+'" style="width: 340px">';
			element += '			<option value="">-- Select Item --</option>';
			for(var i=0;i<itemCount;i++){
				element += '		<option value="'+items[i]['id']+'">'+items[i]['stock_name']+'</option>';
			}
			element += '		</select>';
			element += '	</td>';
			element += '	<td>';
			element += '		<input type="number" class="form-control normal-control" min="0" id="normal_price'+itemCur+'" name="normal_price'+itemCur+'" style="height:37.99px;width: 90px;" value="0">';
			element += '	</td>';
			element += '	<td>';
			element += '		<input type="number" class="form-control disc-control" min="0" id="disc_price'+itemCur+'" name="disc_price'+itemCur+'" style="height:37.99px;display: inline-block;width: 90px;" value="0">';
			element += '		<label style="margin-left: 5px;">%</label>';
			element += '	</td>';
			element += '	<td>';
			element += '		<div class="btn-group">';
			element += '			<button class="btn btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Tambah Baru" onClick="add_item()">';
			element += '				<i class="fa fa-plus"></i>';
			element += '			</button>';
			element += '		</div>';
			element += '	</td>';
			element += '</tr>';
		$('#listadditem tbody').append(element);
		
		$("#item_id"+itemCur).select2();
		$('[data-toggle="tooltip"]').tooltip();

		$( ".btn-danger" ).on( "click", function() {
		  $(this).parent().parent().parent().remove();
		  itemsLen--;
		  looptable();
		});
	}

	function looptable(){
		$('#listadditem tbody tr').each(function (index, value) {
			$(this).children('td:first-child').html(index+1);
		});
	}

	$('#add_quotation').on('submit',(function(e) {
		var issue_date  = $('#issue_date').val(),
			term_of_pay = $('#term_of_pay').val(),
			cust_id 	= $('#cust_id').val(),
			id_valas 	= $('#id_valas').val(),
			items 		= $(".item-control"),
			normals 	= $(".normal-control"),
			discs 		= $(".disc-control"),
			itemsval    = [],
			normalsval  = [],
			discsval    = [];

		var itemsLen    = items.length,
			normalsLen  = normals.length,
			discsLen    = discs.length;

		for(var i = 0; i < itemsLen; i++){
			if($(items[i]).val() !== ''){
				itemsval.push($(items[i]).val());	
			}
		}

		for(var j = 0; j < normalsLen; j++){
		    normalsval.push($(normals[j]).val());
		}

		for(var k = 0; k < discsLen; k++){
		    discsval.push($(discs[k]).val());
		}

		var datapost={
				"issue_date"     : issue_date,
				"term_of_pay"    : term_of_pay,
				"cust_id"  		 : cust_id,
				"id_valas"  	 : id_valas,
				"id_produk"  	 : itemsval,
				"price"  	 	 : normalsval,
				"diskon"  	 	 : discsval
			};
	    $('#btn-submit').attr('disabled','disabled');
	    $('#btn-submit').text("Memasukkan data...");
	    e.preventDefault();

	    $.ajax({
	        type:'POST',
	        url: $(this).attr('action'),
	        data : JSON.stringify(datapost),
	        dataType: 'json',
			contentType: 'application/json; charset=utf-8',
	        success: function(response) {
	            if (response.success == true) {
	              $('.panel-heading button').trigger('click');
	                listquotation();
	                swal({
	                  title: 'Success!',
	                  text: response.message,
	                  type: 'success',
	                  showCancelButton: false,
	                  confirmButtonText: 'Ok'
	                }).then(function () {
	                });
	            } else{
	                $('#btn-submit').removeAttr('disabled');
	                $('#btn-submit').text("Tambah Quotation");
	                swal("Failed!", response.message, "error");
	            }
	        }
	    }).fail(function(xhr, status, message) {
	        $('#btn-submit').removeAttr('disabled');
	        $('#btn-submit').text("Tambah Quotation");
	        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
	    });
	  }));
</script>
