<?php 
	$order_no = isset($detail['order_no'])?$detail['order_no']:strtoupper(randomString());
	$order_date = isset($detail['order_date'])?$detail['order_date']:date('Y-m-d');
	$packing = isset($detail['packing'])?$detail['packing']:'0';
?>
<form class="form-horizontal form-label-left" id="edit_po" role="form" action="<?php echo base_url('quotation/edit_po');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p style="text-align:center;">Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	

           	<input data-parsley-maxlength="255" type="hidden" id="id" name="id" class="form-control col-md-7 col-xs-12" placeholder="Quotation No"  value="<?php if(isset($detail['id'])){ echo $detail['id']; }?>" readonly autocomplete="off">
           	<input data-parsley-maxlength="255" type="hidden" id="order_no" name="order_no" class="form-control col-md-7 col-xs-12" placeholder="Order No"  value="<?php echo $order_no;?>" readonly autocomplete="off">


	 <div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Order Date
        	<span class="required">
        		<sup>*</sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
			<div class="input-group date">
			   <input placeholder="Order Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="order_date" name="order_date" required="required" value="<?php echo $order_date;?>" autocomplete="off">
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
   	</div>
	
		<div class="item form-group" id="subdetail_temp">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id">Currency</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="text" id="department" name="department" class="form-control col-md-7 col-xs-12" placeholder="Department" required="required" value="<?php if(isset($detail['nama_valas'])){ echo $detail['nama_valas']; }?>" disabled="disabled" autocomplete="off">
		</div>
	</div>

   	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="term_of_pay">Term of Payment</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="text" id="department" name="department" class="form-control col-md-7 col-xs-12" placeholder="Department" required="required" value="<?php if(isset($detail['price_term'])){ echo $detail['price_term']; }?>" disabled="disabled" autocomplete="off">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="dist">Customer</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="text" id="department" name="department" class="form-control col-md-7 col-xs-12" placeholder="Department" required="required" value="<?php if(isset($detail['name_eksternal'])){ echo $detail['name_eksternal']; }?>" disabled="disabled" autocomplete="off">
		</div>
	</div>

   	<div class="item form-group">
    	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Transport
    		<span class="required">
    			<sup>*</sup>
    		</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
           	<input data-parsley-maxlength="255" type="text" id="transport" name="transport" class="form-control col-md-7 col-xs-12" placeholder="Transport" value="<?php if(isset($detail['transport'])){ echo $detail['transport']; } else echo "TRUCK";?>" autocomplete="off">
        </div>
     </div>

     <div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="packing">Packing</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="packing" name="packing" style="width: 100%" autocomplete="off">
				<option value="0" <?php if(isset($packing)){ if( $packing == '0' ){ echo "selected"; } }?>>-- Select Packing --</option>
				<option value="1" <?php if(isset($packing)){ if( $packing == '1' ){ echo "selected"; } }?>>Standard Packing</option>
			</select>
		</div>
	</div>

	<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Item : </label>
    </div>

	<div class="item form-group">
		<table id="listadditem" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>No</th>
					<th>Item</th>
					<th>Price</th>
					<th>Qty</th>
					<th>Amount</th>
					<th>Delivery</th>
					<th>Option</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$i=0; 
				foreach($detail['item'] as $orderkey) {?>
					<tr>
						<td style="padding-top: 17px;"><?php echo $i+1; ?></td>
	                  	<td>
	                  		<?php if(isset($orderkey['stock_name'])){ echo $orderkey['stock_name']; }?>
	                  		<input type="hidden" class="id_order" value="<?php if(isset($orderkey['id_order'])){ echo $orderkey['id_order']; }?>">
	                  		<input type="hidden" id="amount_price<?php echo $i;?>" value="<?php if(isset($orderkey['amount_price'])){ echo $orderkey['amount_price']; }?>" class="amount_price"/>
	                  	</td>
		                <td id="disc_price<?php echo $i;?>">
		                	<?php if(isset($orderkey['disc_price'])){ echo number_format((float)$orderkey['disc_price'],5,",","."); }?>
		                	<input type="hidden" value="<?php if(isset($orderkey['disc_price'])){ echo $orderkey['disc_price']; }?>">
		                </td>
		                <td id="order_qty<?php echo $i;?>">
		                	<?php if(isset($orderkey['qty'])){ echo number_format($orderkey['qty'],2,",","."); }?>
		                </td>
		                <td id="order_amount<?php echo $i;?>">
		                	<?php if(isset($orderkey['amount_price'])){ echo number_format($orderkey['amount_price'],5,",","."); }?>
		                	
		                </td>
		                <td id="delivery<?php echo $orderkey['id_order'];?>">
		                	<?php if(isset($orderkey['delivery'])){ echo $orderkey['delivery']; }?>
		                </td>
		                <td>
		                	<div class="btn-group">
		                		<button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit Schedule" onClick="edit_schedule('<?php echo $orderkey['id_order'];?>','<?php echo $orderkey['stock_name'];?>','<?php echo $orderkey['disc_price'];?>','<?php echo $i;?>')">
		                			<i class="fa fa-pencil"></i>
		                		</button>
		                	</div>
		                </td>
		            </tr>
	            <?php $i++;} ?>
			</tbody>
		</table>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Po</button>
		</div>
	</div>
	
</form>
<!-- /page content -->

<script type="text/javascript">
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();

		$("#order_date").datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});
	});

	$('#edit_po').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
	    $('#btn-submit').text("Mengubah data...");
	    e.preventDefault();

	    var amount_price = $(".amount_price"),
	    	order_qty = $(".order_qty"),
	    	id_order = $(".id_order"),
	        amount = 0,
	        order = [];

	    for(var i = 0; i < order_qty.length; i++){
		    amount = amount + parseInt($(amount_price[i]).val());

		    order.push({"id_order":$(id_order[i]).val(),"qty":$(order_qty[i]).val(),"amount_price":$(amount_price[i]).val()});
		}

		var id = $('#id').val(),
			order_no = $('#order_no').val(),
			order_date = $('#order_date').val(),
			transport = $('#transport').val(),
			packing = $('#packing').val();

		var data = {
			"id":id,
			"order_no":order_no,
			"order_date":order_date,
			"transport":transport,
			"packing":packing,
			"amount":amount,
			"order":order
		};

		$.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:JSON.stringify(data),
            cache:false,
            contentType: false,
            processData: false,
            success: function(response) {
				
                if (response.success == true) {
                	$('.panel-heading button').trigger('click');
                	listquotation();
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                    })
                } else{
                    $('#btn-submit').removeAttr('disabled');
                    $('#btn-submit').text("Edit Po");
                    swal("Failed!", response.message, "error");
                }
            }
        }).fail(function(xhr, status, message) {
            $('#btn-submit').removeAttr('disabled');
            $('#btn-submit').text("Edit Po");
            swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
        });
	}));

		  function listpo(){
     // var cust_id = (typeof $('.select_customer').val() !== "undefined")?$('.select_customer').val():'0';

      $("#listpo").dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo base_url().'quotation/lists?cust_id="+cust_id+"';?>",
            "searchDelay": 700,
            "responsive": true,
            "lengthChange": false,
            "destroy": true,
            "info": false,
            "bSort": false,
            "dom": 'l<"toolbar">frtip',
            "initComplete": function(result){
                var element = '<div class="btn-group pull-left">';
                    element += '  <label>Filter</label>';
                    element += '  <label class="filter_separator">|</label>';
                    element += '  <label>Customer:';
                    element += '    <select class="form-control select_customer" onChange="listpo()" style="height:30px;width: 340px;">';
                    element += '      <option value="0">-- Semua Customer --</option>';
                    $.each( result.json.customer, function( key, val ) {
                      var selectcust = (val.id===cust_id)?'selected':'';
                            element += '    <option value="'+val.id+'" '+selectcust+'>'+val.name_eksternal+'</option>';
                        });
                    element += '    </select>';
                    element += '  </label>';
                        
               $("#listpo_filter").prepend(element);
            }
      });
  }

	function redrawTable(row){
		var order_qty = $('#order_qty'+row+' input').val()
			disc_price = parseInt($('#disc_price'+row+' input').val());
		
		$('#amount_price'+row).val(order_qty*disc_price);
        $('#order_amount'+row).html(currencyFormat(order_qty*disc_price, 2, ',', '.'));
	}

	function currencyFormat(number, decimals, decPoint, thousandsSep) {
	  number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
	  var n = !isFinite(+number) ? 0 : +number
	  var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
	  var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
	  var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
	  var s = ''

	  var toFixedFix = function (n, prec) {
	    if (('' + n).indexOf('e') === -1) {
	      return +(Math.round(n + 'e+' + prec) + 'e-' + prec)
	    } else {
	      var arr = ('' + n).split('e')
	      var sig = ''
	      if (+arr[1] + prec > 0) {
	        sig = '+'
	      }
	      return (+(Math.round(+arr[0] + 'e' + sig + (+arr[1] + prec)) + 'e-' + prec)).toFixed(prec)
	    }
	  }

	  // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
	  s = (prec ? toFixedFix(n, prec).toString() : '' + Math.round(n)).split('.')
	  if (s[0].length > 3) {
	    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
	  }
	  if ((s[1] || '').length < prec) {
	    s[1] = s[1] || ''
	    s[1] += new Array(prec - s[1].length + 1).join('0')
	  }

	  return s.join(dec)
	}
</script>
