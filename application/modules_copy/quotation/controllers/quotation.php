<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Quotation extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('quotation/quotation_model');
		$this->load->library('log_activity');
		$this->load->library('sequence');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string)
	{
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	 * This function is redirect to index quotation page
	 * @return Void
	 */
	public function index()
	{
		$this->template->load('maintemplate', 'quotation/views/index');
	}

	/**
	 * This function is used for showing quotation list
	 * @return Array
	 */
	function lists()
	{
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;
		$order_fields = array('', 'req_no', 'no_po', 'name_eksternal', 'stock_name');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->quotation_model->lists($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		foreach ($list['data'] as $k => $v) {

			if ($v['status'] == "On Quotation") {
				$id = '<div class="changed_status" onClick="detail_quotation(\'' . $v['id'] . '\')">';
				$id .=      $v['req_no'];
				$id .= '</div>';
			} else {
				$id = '<div class="changed_status" >';
				$id .=      $v['req_no'];
				$id .= '</div>';
			}

			$actions = '';
			if ($v['status'] == "On Quotation") {
				$actions .= '<div class="btn-group">';
				$actions .= '   <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edit_quotation(\'' . $v['id'] . '\')">';
				$actions .= '       <i class="fa fa-edit"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
			}
			if ($v['status'] == "Finish PO") {
				$actions .= '<div class="btn-group">';
				$actions .= '   <button class="btn btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Repeat Order" onClick="ro_quotation(\'' . $v['id'] . '\')">';
				$actions .= '       <i class="fa fa-refresh"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
			} else {
				if ($v['no_do'] != null) {
					$actions .= '<div class="btn-group">';
					$actions .= '   <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="delete_quotation(\'' . $v['id'] . '\')">';
					$actions .= '       <i class="fa fa-trash"></i>';
					$actions .= '   </button>';
					$actions .= '</div>';
				}
			}

			if ($v['status'] == "On Quotation" || $v['status'] == "Prepare Production") {
				$actions .= '<div class="btn-group">';
				$actions .= '   <button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Approval" onClick="approval_quotation(\'' . $v['id'] . '\')">';
				$actions .= '       <i class="fa fa-check"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
			}
			if ($v['valas_id'] == 1) {
				$price = number_format($v['price'], 2, ",", ".");
				$total_normal_price = number_format($v['total_normal_price'], 2, ",", ".");
				$total_disc_price = number_format($v['total_discount_price'], 2, ",", ".");
			} else {
				$price = number_format($v['price'], 5, ",", ".");
				$total_normal_price = number_format($v['total_normal_price'], 5, ",", ".");
				$total_disc_price = number_format($v['total_discount_price'], 5, ",", ".");
			}

			array_push($data, array(
				$v['req_no'],
				$v['no_po'],
				$v['name_eksternal'],
				$v['issue_date'],
				$v['part_number'],
				$v['total_products'],
				$price,
				$total_normal_price,
				$total_disc_price,
				$v['nama_valas'],
				$v['price_term'],
				$v['status'],
				$actions
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	/**
	 * This function is redirect to add quotation page
	 * @return Void
	 */
	public function add()
	{
		$valas = $this->quotation_model->valas();
		$customer = $this->quotation_model->customer();
		$item = $this->quotation_model->item();

		$data = array(
			'valas' => $valas,
			'customer' => $customer,
			'item' => $item
		);

		$this->load->view('add_modal_view', $data);
	}

	/**
	 * This function is redirect to edit quotation page
	 * @return Void
	 */
	public function edit($id)
	{
		$valas = $this->quotation_model->valas();
		$customer = $this->quotation_model->customer();
		$item = $this->quotation_model->item();
		$po_detail = $this->quotation_model->po_detail($id);
		$order_detail = $this->quotation_model->order_detail($id);

		//print_r($order_detail);die;

		$data = array(
			'valas' => $valas,
			'customer' => $customer,
			'item' => $item,
			'po_detail'    => $po_detail,
			'order_detail'    => $order_detail
		);

		$this->load->view('edit_modal_view', $data);
	}

	public function ro($id)
	{
		$customer 		= $this->quotation_model->customer();
		$valas 			= $this->quotation_model->valas();
		$po_detail 		= $this->quotation_model->po_detail($id);
		$item 			= $this->quotation_model->item();
		$order_detail 	= $this->quotation_model->order_detail($id);
		$check_po 		= $this->quotation_model->check_po($id);

		$data = array(
			'customer'		=> $customer,
			'valas'			=> $valas,
			'item'			=> $item,
			'po_detail'		=> $po_detail,
			'order_detail'	=> $order_detail,
			'check_po'		=> $check_po[0]
		);

		//print_r($po_detail);die;

		$this->load->view('edit_modal_view_ro', $data);
	}



	/**
	 * This function is used to add quotation data
	 * @return Array
	 */
	public function add_quotation()
	{
		$data   = file_get_contents("php://input");
		$params = json_decode($data, true);

		$this->quotation_model->add_quotation($params);

		$msg = 'Berhasil menambah data quotation';

		$result = array(
			'success' => true,
			'message' => $msg
		);

		$this->log_activity->insert_activity('insert', $msg . ' dengan ID Customer ' . $params['cust_id']);


		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	/**
	 * This function is used to delete quotation data
	 * @return Array
	 */
	public function delete_quotation()
	{
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$this->quotation_model->delete_quotation($params['id']);

		$msg = 'Berhasil menghapus data kas';

		$result = array(
			'success' => true,
			'message' => $msg
		);

		$this->log_activity->insert_activity('delete', $msg . ' dengan ID Customer ' . $params['id']);

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	/**
	 * This function is used to update kas data
	 * @return Array
	 */

	public function repeat_quotation()
	{
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);


		$no = $this->sequence->get_no('req_id');
		$datas['valas_id'] = $this->input->post('valas_id');
		$datas['rate'] = $this->input->post('rate');
		$datas['id_quotation'] = $this->input->post('id_quotation');
		$datas['issue_date'] = $this->input->post('issue_date');
		$temp_issue_date	= explode("/", $datas['issue_date']);
		$datas['cust_id'] = $this->input->post('cust_id');
		$datas['term_of_payment'] = $this->input->post('term_of_payment');
		$datas['transport'] = $this->input->post('transport');
		$datas['packing'] = $this->input->post('packing');
		$datas['term_of_pay'] = $this->input->post('term_of_pay');
		$datas['no_po_isi'] = $this->input->post('no_po_isi');
		$arridPoQuot = explode(',', $this->input->post('arridPoQuot'));
		$arridMaterial = explode(',', $this->input->post('arridMaterial'));
		$arrItemId = explode(',', $this->input->post('arrItemId'));
		$arrPartName = explode(',', $this->input->post('arrPartName'));
		$arrQty = explode(',', $this->input->post('arrQty'));
		$arrNoPO = explode(',', $this->input->post('arrNoPO'));
		$arrPrice = explode(',', $this->input->post('arrPrice'));
		$arrDiskon = explode(',', $this->input->post('arrDiskon'));
		//echo "asasas";

		$dataQuotation = array(
			'no'				=> $no,
			'cust_id'			=> $datas['cust_id'],
			'issue_date'		=> date('Y-m-d', strtotime($temp_issue_date[2] . '-' . $temp_issue_date[1] . '-' . $temp_issue_date[0])),
			'term_of_pay' 		=> $datas['term_of_pay'],
			'valas'				=> $datas['valas_id'],
			'rate'				=> $datas['rate'],
			'term_of_payment'	=> $datas['term_of_payment'],
			'packing'				=> $datas['packing'],
			'transport'				=> $datas['transport'],

		);


		$result_quotation = $this->quotation_model->add_quotation_ro($dataQuotation);

		//$id_po_quotation = $result_quotation['lastid'];
		//print_r($datas);die;
		$id_po_quotation = $result_quotation['lastid'];
		for ($i = 0; $i < sizeof($arrItemId); $i++) {

			$id_material = $arrItemId[$i];

			$request_item		 	= $this->quotation_model->add_request_item($id_po_quotation, $id_material, $arrQty[$i]);
			$id_request 			= $request_item['lastid'];

			//$get_request_item 		= $this->quotation_model->get_request_item($id_po_quotation);

			$data_order = array(
				'id_po_quotation' 	=> $id_po_quotation,
				'id_product' 		=> $id_material,
				'qty' 				=> $arrQty[$i],
				'price' 			=> $arrPrice[$i],
				'diskon' 			=> $arrDiskon[$i],
				'disc_price'		=> $arrPrice[$i] - $arrDiskon[$i],
				'amount_price' 		=> ($arrPrice[$i] - $arrDiskon[$i]) * $arrQty[$i],
				'no_po' 			=> $arrNoPO[$i]
			);

			$result_order_request	= $this->quotation_model->add_order($data_order);
		}

		//$this->quotation_model->repeat_quotation($params);

		$msg = 'Berhasil mengubah data quotation ';

		$result = array(
			'success' => true,
			'message' => $msg
		);

		$this->log_activity->insert_activity('update', $msg . ' dengan id Quotation ' . $params['id_quotation']);

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function edit_quotation()
	{
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		//print_r($params);die;  

		$this->quotation_model->edit_quotation($params);

		$msg = 'Berhasil mengubah data quotation ';

		$result = array(
			'success' => true,
			'message' => $msg
		);

		$this->log_activity->insert_activity('update', $msg . ' dengan id Quotation ' . $params['id_quotation']);

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	/**
	 * This function is redirect to detail quotation page
	 * @return Void
	 */
	public function detail($id)
	{
		$detail = $this->quotation_model->detail($id);

		$data = array(
			'detail' => $detail
		);

		$this->load->view('detail_modal_view', $data);
	}

	/**
	 * This function is redirect to approval quotation page
	 * @return Void
	 */
	public function edit_approval($id)
	{
		$result = $this->quotation_model->detail($id);

		//print_r($result);die;

		$data = array(
			'detail' => $result
		);

		//$this->load->view('edit_modal_approval_view',$data);
		$this->load->view('edit_modal_view_ap', $data);
	}


	public function edit_schedule($id, $price)
	{
		$detail = $this->quotation_model->detail_schedule($id);

		$data = array(
			'id' => $id,
			'price' => $price,
			'detail' => $detail
		);

		$this->load->view('edit_modal_schedule_view', $data);
	}
	
	public function doedit_schedule()
	{
		$data   = file_get_contents("php://input");
		$params = json_decode($data, true);
		// var_dump($params);die;
		$schedule = $this->quotation_model->doedit_schedule($params);

		$msg = 'Berhasil mengubah schedule po';

		$dataLen = count($params);

		$result = array(
			'success' => true,
			'message' => $msg,
			'schedule' => $schedule
		);
		for ($i = 0; $i < $dataLen; $i++)
			$this->log_activity->insert_activity('update', $msg . ' dengan ID Order ' . $params[$i]['id_order']);

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	public function edit_po()
	{
		$data   = file_get_contents("php://input");
		$params = json_decode($data, true);

		//print_r($data);die;

		$schedule = $this->quotation_model->edit_po($params);

		$msg = 'Berhasil mengubah po';

		$result = array(
			'success' => true,
			'message' => $msg
		);

		//print_r($result);die;

		$this->log_activity->insert_activity('update', $msg . ' dengan No Order ' . $params['order_no']);

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	/**
	 * This function is used to change status quotation data
	 * @return Array
	 */
	public function change_status()
	{
		$data   = file_get_contents("php://input");
		$params = json_decode($data, true);

		$no = $this->sequence->get_no('po_quotation');

		$this->quotation_model->change_status($params, $no);

		$msg = 'Berhasil mengubah status quotation';

		$result = array(
			'success' => true,
			'message' => $msg
		);

		$this->log_activity->insert_activity('update', $msg . ' dengan status ' . $params['status']);

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	/**
	 * This function is redirect to edit approval quotation page
	 * @return Void
	 */
	public function detail_status($id)
	{
		$result = $this->quotation_model->detail_status($id);

		$data = array(
			'detail' => $result
		);

		$this->load->view('detail_modal_approval_view', $data);
	}
}
