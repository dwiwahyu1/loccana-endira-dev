<form class="form-horizontal form-label-left" id="edit_bomid" role="form" action="<?php echo base_url('schedule_production/edit_bom');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p style="text-align:center;">Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>


        	<input data-parsley-maxlength="255" type="hidden" name="bom_no" id="bom_no" class="form-control col-md-7 col-xs-12" placeholder="Bom No" autocomplete="off" value="<?php if(isset($detail_esf[0]['id_produk'])){ echo $detail_esf[0]['id_produk']; }?>" readonly>
			
			<input data-parsley-maxlength="255" type="hidden" name="id_esf" id="id_esf" class="form-control col-md-7 col-xs-12" placeholder="Bom No" autocomplete="off" value="" readonly>


   	<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Part No
        	<span class="required">
        		<sup>*</sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
        	<input data-parsley-maxlength="255" type="text" name="part_no" class="form-control col-md-7 col-xs-12" placeholder="Part No" autocomplete="off" value="<?php if(isset($detail_esf[0]['stock_name'])){ echo $detail_esf[0]['stock_name']; }?>" readonly>
		</div>
   	</div>
	
	  	<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Tipe
        	<span class="required">
        		<sup>*</sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
        	<input data-parsley-maxlength="255" type="text" name="tipe_esf" class="form-control col-md-7 col-xs-12" placeholder="Part No" autocomplete="off" value="<?php if(isset($detail_esf[0]['pcb_type'])){ echo $detail_esf[0]['pcb_type']; }?>" readonly>
		</div>
   	</div>
	
<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Layout</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select id="layout" name="layout" class="form-control col-md-7 col-xs-12" placeholder="Lot No" value="" autocomplete="off">
			<option disabled selected> -- Layout -- </option>
			<?php $i = 0; foreach($detail_esf as $detail_esfs){ ?>
				<option value="<?php echo $detail_esfs['id_esf_layout']; ?>" ><?php echo $detail_esfs['panel_pcs_array'] ?> UP </option>
			<?php $i++; }  ?>
			</select>
		</div>
	</div>
<!--		
		<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Sheet Size
        	<span class="required">
        		<sup>*</sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
        	<input data-parsley-maxlength="255" type="text" id="sheet_size" name="sheet_size" class="form-control col-md-7 col-xs-12" placeholder="Sheet Size" value="<?php if(isset($detail_layout[0]['sheet_size'])){ echo $detail_layout[0]['sheet_size']; }?>" readOnly >
		</div>
   	</div>
	
		<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Panel Size
        	<span class="required">
        		<sup>*</sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
        	<input data-parsley-maxlength="255" type="text" name="panel_size" id="panel_size" class="form-control col-md-7 col-xs-12" placeholder="Panel Size" value="<?php if(isset($detail_layout[0]['panel_size'])){ echo $detail_layout[0]['panel_size']; }?>" readOnly >
		</div>
   	</div>
	
		<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Sheet/Panel
        	<span class="required">
        		<sup>*</sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
        	<input data-parsley-maxlength="255" type="text" name="sheet_panel" id="sheet_panel"  class="form-control col-md-7 col-xs-12" placeholder="Sheet/Panel" value="<?php if(isset($detail_layout[0]['sheet_panel'])){ echo $detail_layout[0]['sheet_panel']; }?>" readOnly >
		</div>
   	</div>
	
	<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Cutting Map
        	<span class="required">
        		<sup>*</sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
        	<input type="file" name="cutting_map" id="cutting_map" class="form-control col-md-7 col-xs-12" placeholder="Sheet/Panel" value="" >
        	<input type="hidden" name="cutting_map_curr" id="cutting_map_curr" class="form-control col-md-7 col-xs-12" placeholder="Sheet/Panel" value="" >
		</div>
   	</div>
	
	<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">
        	<span class="required">
        		<sup></sup>
        	</span>
        </label>
		 <div style="text-align: right;margin-right: 80px;">

			 <img src="<?php echo base_url();?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/crossword.png" id="cutting_map_temp" style="width: 365px;height: 250px;"/>
			 
        
      </div>
   	</div>


			<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">PCS/ ARY/ PNL
        	<span class="required">
        		<sup>*</sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
        	<input data-parsley-maxlength="255" type="text" name="pcs_ary_pnl" id="pcs_ary_pnl" class="form-control col-md-7 col-xs-12" placeholder="PCS/ ARY/ PNL" value="<?php if(isset($detail_layout[0]['pcs_ary_pnl'])){ echo $detail_layout[0]['pcs_ary_pnl']; }?>" readOnly >
		</div>
   	</div>
	
		<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">BDS/ ARY/ SIZE
        	<span class="required">
        		<sup>*</sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
        	<input data-parsley-maxlength="255" type="text" name="bds_ary_pnl" id="bds_ary_pnl" class="form-control col-md-7 col-xs-12" placeholder="BDS/ ARY/ SIZE" value="<?php if(isset($detail_layout[0]['bds_ary_pnl'])){ echo $detail_layout[0]['bds_ary_pnl']; }?>" readOnly >
		</div>
   	</div>
	
	<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Board Layout
        	<span class="required">
        		<sup>*</sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
        	<input type="file" name="board_layout" id="board_layout" class="form-control col-md-7 col-xs-12" placeholder="Sheet/Panel" value="" >
			
			<input type="hidden" name="board_layout_curr" id="board_layout_curr" class="form-control col-md-7 col-xs-12" placeholder="Sheet/Panel" value="" >
		</div>
   	</div>
	
	<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">
        	<span class="required">
        		<sup></sup>
        	</span>
        </label>
		 <div style="text-align: right;margin-right: 80px;">


			 <img src="<?php echo base_url();?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/crossword.png" id="board_layout_temp" style="width: 365px;height: 250px;"/>
		
		 
      </div>
   	</div> -->
	
	<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Remarks
        	<span class="required">
        		<sup></sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
        	<input data-parsley-maxlength="255" type="text" name="remark" class="form-control col-md-7 col-xs-12" placeholder="Remark" autocomplete="off" value="<?php if(isset($detail_layout[0]['remark'])){ echo $detail_layout[0]['remark']; }?>" >
		</div>
   	</div>

   	<!--<div class="item form-group" id="subdetail_temp">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id">Material Utama <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="id_mainproduk" name="id_mainproduk" style="width: 100%" required>
				<option value="" disabled selected>-- Select Item --</option>
				<?php foreach($mainitem as $key) { ?>
					<option value="<?php echo 'main|'.$detail[0]['id'].'|'.$key['id']; ?>"  
					<?php if(isset($detail[0]['material'])){ if( $detail_main[0]['id_material_comp'] == $key['id'] ){ 
					echo "selected"; } }?>
					><?php echo $key['stock_code'].'|'.$key['stock_name'].'|'.$key['stock_description']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div> -->

	<!-- <div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Sheet Ratio
        	<span class="required">
        		<sup>*</sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
        	<input data-parsley-maxlength="255" type="text" id="ratio" name="ratio" class="form-control col-md-7 col-xs-12" placeholder="Bom No" value="<?php if(isset($detail[0]['qty_main'])){ echo $detail[0]['qty_main']; }else{ echo 1;}?>">
		</div>
   	</div> -->

	<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Process Flow : </label>
    </div>

    <div class="item form-group">
		<table id="listedititem" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>PROCESS FLOW</th>
					<th>INSTRUCTION</th>
					<th>STATUS</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$i=1; 
				foreach($detail as $key) {  ?>
					<tr>
						<td><?php echo $key['name']; ?></td>
						<td>
						
						<?php if($key['tipe_ins'] == 1){ ?>
							
							<input type="text" class="form-control select-control item-control" id="instruction_<?php echo $key['id']; ?>_<?php echo $key['tipe_ins']; ?>" name="instruction_<?php echo $key['id']; ?>" value="<?php echo $key['instruction']; ?>" /> 
							
						<?php }else{ ?>
							
							<input type="file" class="form-control item-control" id="instruction_<?php echo $key['id']; ?>" name="instruction_<?php echo $key['id']; ?>" /> 
							
							<input type="hidden" class="form-control item-control" id="instruction_<?php echo $key['id']; ?>_curr" name="instruction_<?php echo $key['id']; ?>_curr" value="<?php echo $key['instruction']; ?>" /> 
							
						<?php if($key['instruction'] <> "" ){ ?>
							<img class="form-control item-control" src="<?php echo base_url();?>uploads/layout/<?php echo $key['instruction'] ?>" id="instruction_<?php echo $key['id']; ?>_temp" style="width: 365px;height: 50px;"/>
						<?php }else{ ?>
							 <img class="form-control item-control" src="<?php echo base_url();?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/crossword.png" id="instruction_<?php echo $key['id']; ?>_temp" style="width: 365px;height: 50px;"/>
							 
						 <?php }?>
						<?php } ?>
						
						
						
						</td>
	                  	<td>
	                  		<select class="form-control select-control item-control" id="status_<?php echo $key['id']; ?>" name="status_<?php echo $key['id']; ?>" style="width: 340px" >
								<option value="<?php echo '1'?>" Selected> Use </option>
								<option value="<?php echo '0'?>" > Not Use </option>
							</select>
	                  	</td>
		            </tr>
	            <?php $i++;} ?>
			</tbody>
		</table>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Save</button>
		</div>
	</div>

	<input type="hidden" id="bom_no" value="<?php if(isset($detail[0]['bom_no'])){ echo $detail[0]['bom_no']; }?>">
	<input type="hidden" id="id_quotation" value="<?php if(isset($detail[0]['id_po_quot'])){ echo $detail[0]['id_po_quot']; }?>">
</form>
<!-- /page content -->

<script type="text/javascript">

	$(document).ready(function() {
		
		var items_esf = [];
		

		
		
		$('#layout').on('change',(function(e) {
			
			var ints = $(this).val();
			
			//alert(items_esf[ints]['bds_ary_pnl']);
			
			$('#sheet_size').val(items_esf[ints]['sheet_size']);
			$('#panel_size').val(items_esf[ints]['panel_size']);
			$('#sheet_panel').val(items_esf[ints]['sheet_panel']);
			$('#pcs_ary_pnl').val(items_esf[ints]['pcs_ary_pnl']);
			$('#bds_ary_pnl').val(items_esf[ints]['bds_ary_pnl']);
			$('#id_esf').val(items_esf[ints]['id_esf']);
			
			
		})); 
		
		 $('#instruction_14').on('change',(function(e) {
			if (this.files && this.files[0]) {
			  var reader = new FileReader();
				  reader.onload = (function(theFile) {
					  var image = new Image();
						  image.src = theFile.target.result;
					  
						  image.onload = function() {
							  $("#instruction_14_temp").attr('src', this.src);
						  };
				  });
			  reader.readAsDataURL(this.files[0]);
			}
		  }));		 
		  
		   $('#instruction_43').on('change',(function(e) {
			if (this.files && this.files[0]) {
			  var reader = new FileReader();
				  reader.onload = (function(theFile) {
					  var image = new Image();
						  image.src = theFile.target.result;
					  
						  image.onload = function() {
							  $("#instruction_43_temp").attr('src', this.src);
						  };
				  });
			  reader.readAsDataURL(this.files[0]);
			} 
		  }));		 
		  
		   $('#cutting_map').on('change',(function(e) {
			if (this.files && this.files[0]) {
			  var reader = new FileReader();
				  reader.onload = (function(theFile) {
					  var image = new Image();
						  image.src = theFile.target.result;
					  
						  image.onload = function() {
							  $("#cutting_map_temp").attr('src', this.src);
						  };
				  });
			  reader.readAsDataURL(this.files[0]);
			}
		  }));		 
		  
		  $('#board_layout').on('change',(function(e) {
			if (this.files && this.files[0]) {
			  var reader = new FileReader();
				  reader.onload = (function(theFile) {
					  var image = new Image();
						  image.src = theFile.target.result;
					  
						  image.onload = function() {
							  $("#board_layout_temp").attr('src', this.src);
						  };
				  });
			  reader.readAsDataURL(this.files[0]);
			}
		  }));
		
		$('[data-toggle="tooltip"]').tooltip();
		
		//$(".select-control").select2();

		$( ".btn-danger" ).on( "click", function() {
			$(this).parent().parent().parent().remove();
			itemsLen--;

			var itemCur = itemsLen-1;
			$('.fa-plus').parent().parent().parent().prev().children().attr('id','qty'+itemCur);
			$('.fa-plus').parent().parent().parent().prev().prev().children().attr('id','item_id'+itemCur);
			$('.fa-plus').parent().parent().parent().prev().prev().children().attr('name','item_id'+itemCur);
		});
			
	  
	  $('#edit_bomid').on('submit',(function(e) {
		e.preventDefault();
	
	
		//alert('sdsds');
	    
	var formData = new FormData(this);
		
		
	    $.ajax({
	        type:'POST',
	        url: $(this).attr('action'),
	       // data : JSON.stringify(datapost),
	        data : formData,
	        dataType: 'json',
			processData: false,
			contentType: false,
	        success: function(response) {
	            if (response.success == true) {
	              $('.panel-heading button').trigger('click');
	                listbom();
	                swal({
	                  title: 'Success!',
	                  text: response.message,
	                  type: 'success',
	                  showCancelButton: false,
	                  confirmButtonText: 'Ok'
	                }).then(function () {
	                });
	            } else{
	                $('#btn-submit').removeAttr('disabled');
	                $('#btn-submit').text("Edit Schedule");
	                swal("Failed!", response.message, "error");
	            }
	        }
	    }).fail(function(xhr, status, message) {
	        $('#btn-submit').removeAttr('disabled');
	        $('#btn-submit').text("Edit Schedule");
	        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
	    });
	  }));
		
	});

	function add_item(row){
		var uom_id = $('#item_id'+row).val();

		if(uom_id !== ''){
			$('#listedititem .btn-group .btn-delete').removeClass('btn-primary');
			$('#listedititem .btn-group .btn-delete').addClass('btn-danger');
			$('#listedititem .btn-group .btn-delete').attr('title','Hapus');
			$('#listedititem .btn-group .btn-delete').attr('data-original-title','Hapus');
			$('#listedititem .btn-group .btn-delete').removeAttr('onClick');
			$('#listedititem .btn-group .btn-delete .fa').removeClass('fa-plus');
			$('#listedititem .btn-group .btn-delete .fa').addClass('fa-trash');
			var products 	= $(".qty-control")

			var itemCount = items.length,
				productsLen = products.length;

				itemsLen++;

			var element = '<tr>';
				element += '	<td>';
				element += '		<select class="form-control select-control item-control" id="item_id'+productsLen+'" name="item_id'+productsLen+'" style="width: 340px" required>';
				element += '			<option value="">-- Select Item --</option>';
				for(var i=0;i<itemCount;i++){
					element += '		<option value="'+items[i]['id']+'">'+items[i]['stock_name']+'</option>';
				}
				element += '		</select>';
				element += '	</td>';
				element += '	<td>';
				element += '		<input type="text" class="form-control qty-control" id="qty'+productsLen+'" value="0,0000000000" style="width:230px;">';
				element += '	</td>';
				element += '	<td>';
				element += '		<div class="btn-group">';
				element += '			<button class="btn btn-primary btn-delete" type="button" data-toggle="tooltip" data-placement="top" title="Tambah Baru" onClick="add_item('+productsLen+')">';
				element += '				<i class="fa fa-plus"></i>';
				element += '			</button>';
				element += '		</div>';
				element += '		<div class="btn-group">';
				element += '			<button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Detail" onClick="detail_item()">';
				element += '				<i class="fa fa-book"></i>';
				element += '			</button>';
				element += '		</div>';
				element += '	</td>';
				element += '</tr>';
			$('#listedititem tbody').append(element);
			
			$("#item_id"+productsLen).select2();
			$('[data-toggle="tooltip"]').tooltip();

			$( ".btn-danger" ).on( "click", function() {
			  $(this).parent().parent().parent().remove();
			  itemsLen--;

			  var itemCur = itemsLen-1;
			  $('.fa-plus').parent().parent().parent().prev().children().attr('id','qty'+itemCur);
			  $('.fa-plus').parent().parent().parent().prev().prev().children().attr('id','item_id'+itemCur);
			  $('.fa-plus').parent().parent().parent().prev().prev().children().attr('name','item_id'+itemCur);
			});
		}
	}


</script>