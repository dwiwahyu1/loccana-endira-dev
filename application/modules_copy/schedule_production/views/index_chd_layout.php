<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">List Layout</h4>
		</div>
	</div>
<div class="btn-group pull-left"><a href="<?php echo base_url().'schedule_production' ?>" class="btn btn-danger" ><i class="fa fa-left"></i> Kembali</a></div>
	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<table id="listbom2" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="width: 5px;">No</th>
							<th>Layout</th>
							<th>Pcs Ary Panel</th>
							<th>Sheet Panel</th>
							<th style="width: 150px;">Option</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th style="width: 5px;">No</th>
							<th>Layout</th>
							<th>Pcs Ary Panel</th>
							<th>Sheet Panel</th>
							<th style="width: 150px;">Option</th>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:750px;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <p></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$(document).ready(function(){
		listbom();
	});

	function listbom(){
		var cust_id = (typeof $('.select_customer').val() !== "undefined")?$('.select_customer').val():'0';

        $("#listbom2").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'schedule_production/lists3?po='.$po.'&m_id='.$m_id.'' ;?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false,
			"destroy": true,
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip',
			"initComplete": function(result){
				$("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_sch()"><i class="fa fa-plus"></i> Tambah Layout</a></div>');

			}
		});
    }

	function edit_bom(id){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('schedule_production/edit/');?>'+'/'+id);
		$('#panel-modal  .panel-title').html('<i class="fa fa-at"></i> Production Schedule');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}
	
	function add_sch(){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('schedule_production/edit/');?>'+'/'+<?php echo $m_id ?>);
		$('#panel-modal  .panel-title').html('<i class="fa fa-at"></i> Production Schedule');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}
	
	function edit_schd_d(id){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('schedule_production/edit_schd/');?>'+'/'+id);
		$('#panel-modal  .panel-title').html('<i class="fa fa-at"></i> Production Schedule');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function detail_item(row){
		var uom_id = $('#item_id'+row).val();

		if(uom_id !== ''){
		    $('#panel-modalchild').removeData('bs.modal');
		    $('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		    $('#panel-modalchild  .panel-body').load('<?php echo base_url('schedule_production/detail_item');?>'+'/'+uom_id);
		    $('#panel-modalchild  .panel-title').html('<i class="fa fa-book"></i> Detail Barang');
		    $('#panel-modalchild').modal({backdrop:'static',keyboard:false},'show');
	    }
	}
	
	function del_sch(id){
			swal({
			title: 'Yakin akan Menghapus ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost={
				"id" : id
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>schedule_production/delete_schd",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
				   $('.panel-heading button').trigger('click');
                    listbom();
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                    })
				}
			});
		})
	}
	
	function del_app_sch(id){
			swal({
			title: 'Yakin akan Menghapus ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost={
				"id" : id
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>schedule_production/delete_schd_d",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
				   $('.panel-heading button').trigger('click');
                    listbom();
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                    })
				}
			});
		})
	}
	
		function app_sch(id){
			swal({
			title: 'Yakin akan Setujui Schedule ?',
			text: 'data tidak dapat dirubah bila sudah disetujui !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost={
				"id" : id
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>schedule_production/app_sch",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
				   $('.panel-heading button').trigger('click');
                    listbom();
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                    })
				}
			});
		})
	}		
	
	function rej_schd(id){
			swal({
			title: 'Yakin akan Menolak Schedule ?',
			text: 'data tidak dapat dikembalikan bila sudah ditolak !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost={
				"id" : id
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>schedule_production/rej_schd",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
				   $('.panel-heading button').trigger('click');
                    listbom();
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                    })
				}
			});
		})
	}
	
</script>