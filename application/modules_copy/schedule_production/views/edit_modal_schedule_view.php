<style>
   .form-item{margin-top: 15px;overflow: auto;}
</style>

<p style="text-align:center;">Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
<form class="form-horizontal form-label-left" id="add_form_schd" role="form" action="<?php echo base_url('schedule_production/add_schd_list');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Issue Date
		<span class="required">
			<sup>*</sup>
		</span>
	</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<div class="input-group date">
		   <input placeholder="Issue Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="date_issue" name="date_issue" required="required" autocomplete="off" value="<?php echo date('Y-m-d');?>">
		   <input placeholder="Issue Date" type="hidden" class="form-control col-md-7 col-xs-12" id="pcb_type" name="pcb_type" required="required" value="
		   <?php if(isset($detail_layout[0]['pcb_type'])){ echo $detail_layout[0]['pcb_type']; } ?>">
			<div class="input-group-addon">
				<span class="glyphicon glyphicon-th"></span>
			</div>
		</div>
	</div>
</div>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">No Po</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
	   	<select id="no_po" name="no_po" class="form-control col-md-7 col-xs-12" placeholder="Lot No" value="" autocomplete="off">
		<option value="" disabled selected >-- Pilih PO --</option>
		<?php foreach($list_po as $list_pos){ ?>
			<option value="<?php echo $list_pos['id_order'] ?>" ><?php echo $list_pos['no_po'] ?></option>
		<?php } ?>
		</select>
	</div>
</div>


<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Tipe</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
	   	<select id="tipe" name="tipe" class="form-control col-md-7 col-xs-12" placeholder="Lot No" value="" autocomplete="off">
			<option value="Sample" >Sample</option>
			<option value="Produksi" >Produksi</option>
			<option value="Makeup" >Makeup</option>
		</select>
	</div>
</div>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Layout</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
	   	<select id="layout" name="layout" class="form-control col-md-7 col-xs-12" placeholder="Lot No" value="" autocomplete="off">
			<?php $i = 0; foreach($detail_layout as $detail_layouts){ ?>
				
				<option value='<?php echo $i; ?>'> <?php echo $detail_layouts['pcs_ary_panel']; ?> UP </option>
				
			<?php $i++; } ?>
		</select>
	</div>
</div>


<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Lot No</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
	   	<input data-parsley-maxlength="255" type="text" id="lot_number" name="lot_number" class="form-control col-md-7 col-xs-12" placeholder="Lot No" autocomplete="off" value="<?php echo $lot_number; ?>">
	   	<input data-parsley-maxlength="255" type="hidden" id="po" name="po" class="form-control col-md-7 col-xs-12" placeholder="Lot No" autocomplete="off" value="<?php  echo $po; ?>">
	   	<input data-parsley-maxlength="255" type="hidden" id="m_id" name="m_id" class="form-control col-md-7 col-xs-12" placeholder="Lot No" autocomplete="off" value="<?php  echo $m_id; ?>">
	   	<input data-parsley-maxlength="255" type="hidden" id="id_prod_l" name="id_prod_l" class="form-control col-md-7 col-xs-12" placeholder="Lot No" autocomplete="off" value="">
	</div>
</div>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty">Order Qty
		<span class="required">
			<sup>*</sup>
		</span>
	</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
	   	<input type="number" min="0" id="order_qty" name="order_qty" class="form-control col-md-7 col-xs-12" placeholder="Order Qty" autocomplete="off" value="<?php  echo $detail_sch[0]['qty']; ?>" required="required" readonly>
	</div>
</div>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty">Run Down Qty(sheet)
		<span class="required">
			<sup>*</sup>
		</span>
	</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
	   	<input type="number" min="0" id="qty" name="qty" class="form-control col-md-7 col-xs-12" placeholder="Run Down Qty" value="0" required="required" autocomplete="off" >
	</div>
</div>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty">Split Run Down
		<span class="required">
			<sup>*</sup>
		</span>
	</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
	   	<input type="text" min="0" id="split" name="split" class="form-control col-md-7 col-xs-12" placeholder="Run Down Qty" value="0" required="required" readonly autocomplete="off" >
	   	<input type="hidden" min="0" id="panel_ss" name="panel_ss" class="form-control col-md-7 col-xs-12" placeholder="Run Down Qty" value="0" required="required" readonly autocomplete="off" >
	</div>
</div>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty">Qty Array
		<span class="required">
			<sup>*</sup>
		</span>
	</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
	   	<input type="number" min="0" id="order_array" name="order_array" class="form-control col-md-7 col-xs-12" placeholder="Order Qty" value="0" required="required" readonly autocomplete="off">
	</div>
</div>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="schedule">Due Date</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
	   	<label style="width:70px;font-weight: inherit;">
	   		<input type="radio" id="asap_schedule" name="type" value="ASAP" checked> <span>ASAP</span>
	   	</label>
	   	<label style="font-weight: inherit;">
	   		<input type="radio" id="date_schedule" name="type" value="<?php echo date('Y-m-d');?>" autocomplete="off">
	   		<input type="text" id="date" name="date"  class="form-control datepicker" style="width: 357px;display: inline-block;" value="<?php echo date('Y-m-d');?>" autocomplete="off">
	   		<div class="input-group-addon" style="width: 40px;float: right;height: 36.99px;line-height: 26.99px;">
				<span class="glyphicon glyphicon-th"></span>
			</div>
	   	</label>
	</div>
</div>

<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<button id="btn-submitschedule" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Schedule</button>
	</div>
</div>

</form>

<script type="text/javascript">
	var scheduleArray = [],
		qtyTemp = 0;

	var order_qty = '';
	//$('#order_qty').val(order_qty);

	$(document).ready(function() {
		
		var items_esf = []; 
		
			<?php foreach($detail_layout as $sss){?>
			var person = {sheet_size:"<?php echo $sss['rec_sheet_long'] ?> mm x <?php echo $sss['rec_sheet_wide'] ?> mm" , panel_size:"<?php echo $sss['panel_long'] ?> mm x <?php echo $sss['panel_wide'] ?> mm", sheet_panel:"<?php echo $sss['sheet_panel'] ?>", pcs_ary_pnl:"<?php echo $sss['pcs_ary_panel'] ?> UP x <?php echo $sss['pcs_array'] ?> = <?php echo ($sss['pcs_ary_panel']*$sss['pcs_array']) ?> PCS",pcs_array:"<?php echo $sss['pcs_array']; ?>", bds_ary_pnl:"<?php echo $sss['pcb_long'] ?> x <?php echo $sss['pcb_wide'] ?> mm ", id_esf:"<?php echo $sss['id_esf_layout'] ?>", id_prod_lay:"<?php echo $sss['id_prod_layout'] ?>"};
			
			items_esf.push(person);
			
		<?php } ?>
		
		$('[data-toggle="tooltip"]').tooltip();

		$("#date_issue").datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});

		$("#date").datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});
	});

	$('#qty').on('keyup',(function(e) {
		
					var items_esf = []; 
		
			<?php foreach($detail_layout as $sss){?>
			var person = {sheet_size:"<?php echo $sss['rec_sheet_long'] ?> mm x <?php echo $sss['rec_sheet_wide'] ?> mm" , panel_size:"<?php echo $sss['panel_long'] ?> mm x <?php echo $sss['panel_wide'] ?> mm", sheet_panel:"<?php echo $sss['sheet_panel'] ?>",pcs_pnl:"<?php echo $sss['pcs_ary_panel'] ?>", pcs_ary_pnl:"<?php echo $sss['pcs_ary_panel'] ?> UP x <?php echo $sss['pcs_array'] ?> = <?php echo ($sss['pcs_ary_panel']*$sss['pcs_array']) ?> PCS",pcs_array:"<?php echo $sss['pcs_array']; ?>", bds_ary_pnl:"<?php echo $sss['pcb_long'] ?> x <?php echo $sss['pcb_wide'] ?> mm ", id_esf:"<?php echo $sss['id_esf_layout'] ?>", id_prod_lay:"<?php echo $sss['id_prod_layout'] ?>"};
			
			items_esf.push(person);
			
		<?php } ?>
		
		//alert(items_esf);
		//if(parseInt($(this).val()) > order_qty){
			var qty_sheet = $(this).val();
			var select_layout = $('#layout').val();
			var order_qty = $('#order_qty').val();
			
			var unit = '<?php echo $detail_sch[0]['unit']; ?>';
			var uom_p = '<?php echo $detail_sch[0]['uom_p']; ?>';
			//alert(uom_p);
			
			//var new_qty = qty_sheet*<?php echo $detail_sch[0]['rec_pcb'] ?>;
			
			if(unit == 'arrays'){
				var new_qty = qty_sheet*items_esf[select_layout]['sheet_panel']*(items_esf[select_layout]['pcs_pnl']);
				
				var panel_ss = qty_sheet*items_esf[select_layout]['sheet_panel']
			}else{
				if(uom_p == 'Arrays'){
					var new_qty = qty_sheet*items_esf[select_layout]['sheet_panel']*(items_esf[select_layout]['pcs_pnl']);
					
					var panel_ss = qty_sheet*items_esf[select_layout]['sheet_panel']
				}else{
					var new_qty = qty_sheet*items_esf[select_layout]['sheet_panel']*(items_esf[select_layout]['pcs_array']*items_esf[select_layout]['pcs_pnl']);
					
					var panel_ss = qty_sheet*items_esf[select_layout]['sheet_panel']
					
				}
			}
			
			var pcb_type = '<?php echo $detail_layout[0]['pcb_type']; ?>';
			
			if(pcb_type == 'SINGLE SIDE'){
				var splits = Math.ceil(panel_ss/200);
			}else{
				var splits = Math.ceil(panel_ss/50);
			}
			
			
			//alert(panel_ss); 
			
			$('#panel_ss').val(panel_ss);
			$('#order_array').val(new_qty);
			$('#split').val(splits);
			
			
		//}
	}));	
	
	// $('#no_po').on('change',(function(e) { 
	
	// var id = $('#no_po').val();
	
	 // var datapost={
            // "id"  :   id
          // };
			
			// $.ajax({
                // type:'POST',
                // url: "<?php echo base_url().'schedule_production/get_lot';?>",
                // data:JSON.stringify(datapost),
                // cache:false,
                // contentType: false,
                // processData: false,
                // success: function(response) {

                // }
            // });
			
	
	// } ));
		
		
	$('#layout').on('change',(function(e) {
		
					var items_esf = []; 
		
			<?php foreach($detail_layout as $sss){?>
			var person = {sheet_size:"<?php echo $sss['rec_sheet_long'] ?> mm x <?php echo $sss['rec_sheet_wide'] ?> mm" , panel_size:"<?php echo $sss['panel_long'] ?> mm x <?php echo $sss['panel_wide'] ?> mm", sheet_panel:"<?php echo $sss['sheet_panel'] ?>",pcs_pnl:"<?php echo $sss['pcs_ary_panel'] ?>", pcs_ary_pnl:"<?php echo $sss['pcs_ary_panel'] ?> UP x <?php echo $sss['pcs_array'] ?> = <?php echo ($sss['pcs_ary_panel']*$sss['pcs_array']) ?> PCS",pcs_array:"<?php echo $sss['pcs_array']; ?>", bds_ary_pnl:"<?php echo $sss['pcb_long'] ?> x <?php echo $sss['pcb_wide'] ?> mm ", id_esf:"<?php echo $sss['id_esf_layout'] ?>", id_prod_lay:"<?php echo $sss['id_prod_layout'] ?>"};
			
			items_esf.push(person);
			
		<?php } ?>
		
		//alert(items_esf);
		//if(parseInt($(this).val()) > order_qty){
			var qty_sheet = $('#qty').val();
			var select_layout = $('#layout').val();
			
			//var new_qty = qty_sheet*<?php echo $detail_sch[0]['rec_pcb'] ?>;
			var new_qty = qty_sheet*items_esf[select_layout]['sheet_panel']*(items_esf[select_layout]['pcs_array']*items_esf[select_layout]['pcs_pnl']);
			
			//alert(new_qty);
			
			$('#order_array').val(new_qty);
			
			
		//}
	}));

	$('#btn-add').on('click',(function(e) {
		var date_issue = $('#date_issue').val(),
			qty = (parseInt($('#qty').val())*parseInt($('#nopanel').val()))*parseInt($('#pcb').val()),
			lot_number = $('#lot_number').val(),
			due_date = $('input[name="type"]:checked').val(),
			qty_sheet = parseInt($('#qty').val());
					
		if(due_date!=='ASAP'){due_date = $('#date').val();}

		var scheduleData = {
			    "id_po_quot":$('#id_po_quot').val(),
      	        "id_produk":$('#id_produk').val(),
				"date_issue":date_issue,
				"lot_number":lot_number,
				"qty":qty,
				"qty_sheet":qty_sheet,
				"due_date":due_date
			};

	  	scheduleArray.push(scheduleData);
	  	
	  	qtyMax = qtyMax - parseInt(qty_sheet);
	  	
	  	if(qtyMax === 0){
			$('#btn-addtemp').hide();
		}

	  	$('#qty').attr('max',qtyMax);
	  	$('#qty').val(0);

	  	listSchedule();
	}));

	$('#add_form_schd').on('submit',(function(e) {
		e.preventDefault();
	
		$('#btn-submitschedule').attr('disabled','disabled');
		$('#btn-submitschedule').text("Memasukkan data...");
						var items_esf = []; 
		
			<?php foreach($detail_layout as $sss){?>
			var person = {sheet_size:"<?php echo $sss['rec_sheet_long'] ?> mm x <?php echo $sss['rec_sheet_wide'] ?> mm" , panel_size:"<?php echo $sss['panel_long'] ?> mm x <?php echo $sss['panel_wide'] ?> mm", sheet_panel:"<?php echo $sss['sheet_panel'] ?>",pcs_pnl:"<?php echo $sss['pcs_ary_panel'] ?>", pcs_ary_pnl:"<?php echo $sss['pcs_ary_panel'] ?> UP x <?php echo $sss['pcs_array'] ?> = <?php echo ($sss['pcs_ary_panel']*$sss['pcs_array']) ?> PCS",pcs_array:"<?php echo $sss['pcs_array']; ?>", bds_ary_pnl:"<?php echo $sss['pcb_long'] ?> x <?php echo $sss['pcb_wide'] ?> mm ", id_esf:"<?php echo $sss['id_esf_layout'] ?>", id_prod_lay:"<?php echo $sss['id_prod_layout'] ?>"};
			
			items_esf.push(person);
			
		<?php } ?>
		
		//alert(items_esf);
		//if(parseInt($(this).val()) > order_qty){
			// var qty_sheet = $('#qty').val();
			var select_layout = $('#layout').val();
			
			// //var new_qty = qty_sheet*<?php echo $detail_sch[0]['rec_pcb'] ?>;
			// var new_qty = qty_sheet*items_esf[select_layout]['sheet_panel']*(items_esf[select_layout]['pcs_array']*items_esf[select_layout]['id_prod_lay']);
			
			//alert(new_qty);
			
			$('#id_prod_l').val(items_esf[select_layout]['id_prod_lay']);
	
	    
	var formData = new FormData(this);
		
		
	    $.ajax({
	        type:'POST',
	        url: $(this).attr('action'),
	       // data : JSON.stringify(datapost),
	        data : formData,
	        dataType: 'json',
			processData: false,
			contentType: false,
	        success: function(response) {
	            if (response.success == true) {
	              $('.panel-heading button').trigger('click');
	                listbom();
	                swal({
	                  title: 'Success!',
	                  text: response.message,
	                  type: 'success',
	                  showCancelButton: false,
	                  confirmButtonText: 'Ok'
	                }).then(function () {
	                });
	            } else{
	                $('#btn-submitschedule').removeAttr('disabled');
	                $('#btn-submitschedule').text("Edit Schedule");
	                swal("Failed!", response.message, "error");
	            }
	        }
	    }).fail(function(xhr, status, message) {
	        $('#btn-submitschedule').removeAttr('disabled');
	        $('#btn-submitschedule').text("Edit Schedule");
	        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
	    });
	  }));
		

	function deleteSchedule(id,max){
		$('#schedule_temp'+id).remove();
	    scheduleArray.splice(id,1);

	    qtyMax = qtyMax + max;
		$('#qty').attr('max',qtyMax);

	    listSchedule();

	    $('#btn-addtemp').show();
	}

	function listSchedule(){
		var scheduleLen = scheduleArray.length;

		var element ='';
	    for(var i=0;i<scheduleLen;i++){
	    	var ticket = (i+1)+'/'+scheduleLen;
	        element +='   <tr>';
	        element +='     <td>'+scheduleArray[i]["date_issue"]+'</td>';
	        element +='     <td>'+scheduleArray[i]["lot_number"]+'</td>';
	        element +='     <td>'+currencyFormatDE(scheduleArray[i]["qty"])+'</td>';
	        element +='     <td>'+ticket+'</td>';
	        element +='     <td>'+scheduleLen+'</td>';
	        element +='     <td>'+scheduleArray[i]["due_date"]+'</td>';
	        element +='     <td>';
	        element +='       <div class="btn-group">';
	        element +='         <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteSchedule('+i+','+scheduleArray[i]["qty"]+')">';
	        element +='         <i class="fa fa-trash"></i>';
	        element +='       </div>';
	        element +='     </td>';
	        element +='   </tr>';
	    }

	    $('#listschedule tbody').html(element);
	}

	function currencyFormatDE(num) {
		var num = parseFloat(num);
	  return (
	    num
	      .toFixed(2) // always two decimal digits
	      .replace('.', ',') // replace decimal point character with ,
	      .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
	  ) // use . as a separator
	}
</script>
