	<style>
		.col-customer {
			border: solid 1px #b2b8b7;
		}

		.dt-body-left {
			text-align: left;
			vertical-align: middle;
		}

		.dt-body-right {
			text-align: right;
		}

		.dt-body-center {
			text-align: center;
		}

		img {
			max-width: 85%;
			height: auto;
		}

		.nama-perusahaan-margin {
			margin-top: 30px;

		}

		.margin-row {
			margin-bottom: 30px
		}

		.margin-ttd {
			margin-bottom: 75px
		}

		.margin-nama {
			margin-bottom: 55px
		}

		.padding-box-nama {
			border: 1px solid #ebf2f2;
			padding: 10px 5px 10px 5px;
		}
	</style>

	<form class="form-horizontal form-label-left" id="delivery_order_add" role="form" action="<?php echo base_url('delivery_order/save_invoice');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	
		<div class="row">
			<div class="col-md-12">
				<div class="pull-right">
					<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download Invoice" id="btn_download">
						<i class="fa fa-download"></i>
					</a>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="col-md-2 logo-place">
					<img src="<?php echo site_url(); ?>assets/images/logo-celebit.jpg" alt="logo-celebit">
				</div>
				<div class="col-md-10">
					<div class="col-md-12 titleReport">
						<h1 id="titleCelebit">CELEBIT</h1>
						<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
						<h4 id="titleAlamat">BANDUNG FACTORY : JL.BUAH DUA RT.01/RW.04 RANCAEKEK - BANDUNG-INDONESIA</h4>
						<h4 id="titleTlp">TEL 62-22-7798 561/7798 542, FAX : 62-22-7798 562 E-MAIL : invoicing@celebit.com</h4>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-12 text-center">
				<h4 id="titleInvoice" style="font-weight:900;">COMMERCIAL INVOICE</h4>
			</div>
		</div>

		<div class="row margin-row">
			<div class="col-md-12">
				<div class="col-md-7">
					<div class="row">
						<table class="table table-bordered" id="print_bill_to">
							<thead>
								<tr>
									<th>
										<p class="col-md-12" id="billTo1">BILL TO :</p>
										<div class="col-md-11 col-md-offset-1">
											<br><label class="control-label" id="billTo2"><?php if (isset($invoice[0]['name_eksternal'])) { echo $invoice[0]['name_eksternal']; } ?></label>
											<br><label class="control-label" id="billTo3"><?php if (isset($invoice[0]['eksternal_address'])) { echo $invoice[0]['eksternal_address']; } ?></label>
											<br><label class="control-label" id="billTo4"><?php if (isset($invoice[0]['phone_1']) || isset($invoice[0]['phone_2']) || isset($invoice[0]['fax']) || isset($invoice[0]['email'])) { echo 'Telp : ' . $invoice[0]['phone_1'] . ' / ' . $invoice[0]['phone_2'] . ' FAX : ' . $invoice[0]['fax'] . ' E-MAIL : ' . $invoice[0]['email']; } ?>
											</label><br>
											<label class="control-label" id="billTo5">Fax<?php if (isset($invoice[0]['fax'])) { echo $invoice[0]['fax']; } ?></label><br>
										</div>
									</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				<div class="col-md-4 col-md-offset-1">
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="noInvoice1">No P INV</label>
							<label class="control-label col-md-1" id="noInvoice2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="noInvoice" name="noInvoice" placeholder="No Invoice" value="<?php if(isset($invoiced[0]['no_invoice'])) echo $invoiced[0]['no_invoice']; ?>" required>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="tanggalInvoice1">Date</label>
							<label class="control-label col-md-1" id="tanggalInvoice2">:</label>
							<div class="col-md-8">
								<div class="input-group date">
									<input placeholder="<?php echo date('d-M-Y'); ?>" type="text" class="form-control datepicker" id="tanggalInvoice" name="tanggalInvoice" required="required" value="<?php if (isset($invoice[0]['tanggal_do'])) { echo $invoice[0]['tanggal_do'];} ?>">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-th"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="poNo1">PO No</label>
							<label class="control-label col-md-1" id="poNo2">:</label>
							<div class="col-md-8">
								<!--<input placeholder="PO No" type="text" class="form-control" id="poNo" name="poNo" required="required" value="">-->
								<label class="control-label" id="poNo-label" name="poNo-label">As Mentioned</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="paymentTerm1">Payment Term</label>
							<label class="control-label col-md-1" id="paymentTerm2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="paymentTerm" name="paymentTerm" required="required" value="" placeholder="Payment Term">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="shipmentBy1">Shipment By</label>
							<label class="control-label col-md-1" id="shipmentBy2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="shipmentBy" name="shipmentBy" required="required" value="" placeholder="Shipment By">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="etd1">ETD</label>
							<label class="control-label col-md-1" id="etd2">:</label>
							<div class="col-md-8">
								<div class="input-group date">
									<input placeholder="<?php date('Y-m-d'); ?>" type="text" class="form-control datepicker" id="etd" name="etd" required="required" value="<?php if (isset($invoice[0]['tanggal_do'])) { $date = date_create($invoice[0]['tanggal_do']); echo date_format($date, 'Y-m-d'); } ?>">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-th"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="deliveryTerm1">Delivery Term</label>
							<label class="control-label col-md-1" id="deliveryTerm2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="deliveryTerm" name="deliveryTerm" required="required" value="" placeholder="Delivery Term">
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="col-md-7">
					<div class="row">
						<table class="table table-bordered" id="print_ship_to">
							<thead>
								<tr>
									<th>
										<p class="col-md-12" id="shipTo1">SHIP TO :</p>
										<div class="col-md-11 col-md-offset-1">
											<br><label class="control-label" id="shipTo2"><?php if (isset($invoice[0]['name_eksternal'])) { echo $invoice[0]['name_eksternal']; } ?></label>
											<br><label class="control-label" id="shipTo3"><?php if (isset($invoice[0]['eksternal_address'])) { echo $invoice[0]['eksternal_address']; } ?></label>
											<br><label class="control-label" id="shipTo4"><?php if (isset($invoice[0]['phone_1']) || isset($invoice[0]['phone_2']) || isset($invoice[0]['fax']) || isset($invoice[0]['email'])) { echo 'Telp : ' . $invoice[0]['phone_1'] . ' / ' . $invoice[0]['phone_2'] . ' FAX : ' . $invoice[0]['fax'] . ' E-MAIL : ' . $invoice[0]['email']; } ?>
											</label><br>
											<label class="control-label" id="shipTo5">Fax<?php if (isset($invoice[0]['fax'])) { echo $invoice[0]['fax']; } ?></label>
										</div>
									</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				<div class="col-md-4 col-md-offset-1">
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="netWeight1">Net Weight</label>
							<label class="control-label col-md-1" id="netWeight2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="netWeight" name="netWeight" placeholder="Net Weight">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="shipForm1">Ship Form</label>
							<label class="control-label col-md-1" id="shipForm2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="shipForm" name="shipForm" required="required" value="" placeholder="Ship Form">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="shipTo-1">Ship To</label>
							<label class="control-label col-md-1" id="shipTo-2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="shipTo-" name="shipTo-" required="required" value="<?php if (isset($invoice[0]['name_eksternal'])) { echo $invoice[0]['name_eksternal']; } ?>" placeholder="Ship To">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="blNumber1">BL Number</label>
							<label class="control-label col-md-1" id="blNumber2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="blNumber" name="blNumber" required="required" value="" placeholder="BL Number">
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="row margin-row">
			<div class="col-md-12">
				<table id="listinvoice" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th rowspan="2" style="vertical-align: middle;">NO</th>
							<th colspan="2" style="text-align:center">Description</th>
							<th rowspan="2" style="vertical-align: middle;">Qty (Pcs)</th>
							<th rowspan="2" style="vertical-align: middle;">Unit Price (<?php if (isset($invoice[0]['symbol_valas'])) { echo $invoice[0]['symbol_valas']; } ?>)</th>
							<th rowspan="2" style="vertical-align: middle;">Unit Price Hid(<?php if (isset($invoice[0]['symbol_valas'])) { echo $invoice[0]['symbol_valas']; } ?>)</th>
							<th rowspan="2" style="vertical-align: middle;">Amount (<?php if (isset($invoice[0]['symbol_valas'])) { echo $invoice[0]['symbol_valas']; } ?>)</th>
							<th rowspan="2" style="vertical-align:middle">Amount Hid(<?php if (isset($invoice[0]['symbol_valas'])) { echo $invoice[0]['symbol_valas']; } ?>)</th>
						</tr>
						<tr>
							<th>Part No</th>
							<th>PO</th>
						</tr>
					</thead>
					<tbody></tbody>
					<tfoot>
						<tr>
							<th colspan="5" style="text-align: left;">TOTAL <?php if (isset($invoice[0]['symbol_valas'])) { echo $invoice[0]['symbol_valas']; } ?></th>
							<th id=""></th>
							<th id="tdAmount_Total"></th>
							<label class="text-right" id="total-label" style="display:none;"></label>
						</tr>
						<tr>
							<th colspan="7" id="totalBilangan" name="totalBilangan"></th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>

		<div class="row">
			<p class="col-md-12" id="noted-text">Noted :</p>
		</div>

		<div class="row margin-row">
			<div class="col-md-12">
				<div class="col-md-7">
					<div class="row">
						<table id="print_approve" class="table table-bordered"cellspacing="0" width="100%" style="margin-bottom:0;">
							<tbody>
								<tr>
									<td>
										<br>Approved by Customer : <input type="text" class="form-control" id="approved" name="approved" required="required" value="" placeholder="Approved by Customer"><label class="control-label" id="approved-label" name="approved-label" style="display:none;"></label>
										<br>Date : <label class="control-label" id="dateCust-label" name="dateCust-label" style="display:none;"></label>
										<div class="input-group date">
											<input placeholder="<?php echo date('d-M-Y'); ?>" type="text" class="form-control datepicker" id="dateCust" name="dateCust" required="required" value="<?php if (isset($invoice[0]['tanggal_do'])) { $date = date_create($invoice[0]['tanggal_do']); echo date_format($date, 'd-M-Y'); } ?>">
											<div class="input-group-addon">
												<span class="glyphicon glyphicon-th"></span>
											</div>
										</div>
										<br><br><br><br><br>
										Name :<input type="text" class="form-control" id="name" name="name" required="required" value="" placeholder="Name"><label class="control-label" id="name-label" name="name-label" style="display:none;"></label>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

				<div class="col-md-4 col-md-offset-1">
					<div class="col-md-12 text-center">
						<p class="text-center"><b>PT. CELEBIT CIRCUIT TECH. IND</b></p>
						<br><br><br><br><br>
						<input class="form-control " id="penandaTangan" name="penandaTangan" value="<?php
							if(isset($invoiced[0]['sign'])) echo $invoiced[0]['sign'];
						?>">
						<label class="control-label" id="penandaTangan-label" name="penandaTangan-label" style="display:none;"></label>
					</div>
				</div>

			</div>
		</div>
		<div class="row">
			<input type="hidden" id="totHide" name="totHide">
		</div>
		<div class="row">
			<div class="col-md-12">
				<label class="control-label pull-right" id="kode_report">F_MCI-002</label>
			</div>
			<div class="col-md-12">
				<label class="control-label pull-right" id="kode_report_detail">REV : 00</label>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="item form-group">
				<div class="col-md-2 col-md-offset-10">
					<div class="col-md-12 pull-right">
						<input type="hidden" class="form-control" id="id_eks" name="id_eks" required="required" value="<?php if(isset($invoice[0]['id_eks'])) echo $invoice[0]['id_eks']; ?>" placeholder="ID Eksternal">
						<input type="hidden" class="form-control" id="id_do" name="id_do" required="required" value="<?php if(isset($invoice[0]['id_do'])) echo $invoice[0]['id_do']; ?>" placeholder="ID DO">
						<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Simpan</button>
					</div>
				</div>
			</div>
		</div>
	</form>
	
	<script type="text/javascript">
		var dataImage = null;
		var invoice = <?php echo count($invoice); ?>;
		var nama_valas = '<?php if (isset($invoice[0]['nama_valas'])) {
																																																																																					echo $invoice[0]['nama_valas'];
																																																																																				} ?>';

		var t_invoice_list;

		$(document).ready(function() {
			dtInvoice();
			$(".date").datepicker({
				format: 'yyyy-mm-dd',
				autoclose: true,
				todayHighlight: true,
			});
			
			$("#approved").on("keyup", function() {
				$("#approved-label").text($("#approved").val());
			})
			
			$("#dateCust").on("keyup", function() {
				$("#dateCust-label").text($("#dateCust").val());
			})
			
			$("#name").on("keyup", function() {
				$("#name-label").text($("#name").val());
			})
		
			for (var i = 0; i < invoice.length; i++) {
				$("#unit_price" + i).on("keyup", function() {
					total = parseFloat($("#total-label").text().replace(/[^\d.-]/g, ''));

					// console.log(parseFloat($("#total-label").text())+parseFloat($("#ppn-label").text()),parseFloat($("#total-label").text()),parseFloat($("#ppn-label").text()));
					$("#tdAmount_Total").html(formatCurrencyComa(total));
					$("#totalBilangan").html(terbilang(total));
				})
			}
			
			$('#btn_download').click(function() {
				if($('#noInvoice').val() == '' && $('#tanggalInvoice').val() == '') {
					swal("Perhatian", "Mohon lengkapi data invoice ini", "info");
				}else{
					var doc = new jsPDF('p', 'mm', 'letter');
					var imgData = dataImage;
					var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
					var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
					console.log($("#notes-label").val());
					// FOOTER
					doc.setTextColor(100);
					doc.addImage(imgData, 'JPEG', 5, 5, 25, 25)
					doc.setFontSize(12);
					doc.text($('#titleCelebit').html(), 33, 10, 'left');
					doc.setFontSize(10);
					doc.text($('#titlePerusahaan').html(), 33, 15, 'left');
					doc.setFontSize(11);
					doc.text($('#titleAlamat').html(), 33, 20, 'left');
					doc.setFontSize(11);
					doc.text($('#titleTlp').html(), 33, 25, 'left');

					doc.setDrawColor(116, 119, 122);
					doc.setLineWidth(0.1);
					doc.line(4, 30, 500, 30);

					doc.setFontSize(11);
					doc.text($('#titleInvoice').html(), pageWidth / 2, 40, 'center');

					doc.setFontSize(8);
					doc.autoTable({
						html: '#print_bill_to',
						theme: 'plain',
						styles: {
							fontSize: 8,
							lineColor: [116, 119, 122],
							lineWidth: 0.1,
							cellWidth: 'auto',

						},
						margin: 4.5,
						tableWidth: (pageWidth / 2) - 15,
						headStyles: {
							valign: 'middle',
							halign: 'left',
						},
						rowStyles: {
							1: {
								halign: 'left'
							},
						},
						rowPageBreak: 'auto',
						showHead: 'firstPage',
						showFoot: 'lastPage',
						startY: 45
					});
					
					var startYPO = doc.autoTable.previous.finalY;
					
					doc.setFontSize(8);
					doc.text($('#noInvoice1').html(), 115, 46, 'left');
					doc.text($('#noInvoice2').html(), 135, 46, 'left');
					doc.text($('#noInvoice').val(), 140, 46, 'left'); //dari inputan

					doc.text($('#tanggalInvoice1').html(), 115, 52, 'left');
					doc.text($('#tanggalInvoice2').html(), 135, 52, 'left');
					doc.text($('#tanggalInvoice').val(), 140, 52, 'left');

					doc.text($('#poNo1').html(), 115, 58, 'left');
					doc.text($('#poNo2').html(), 135, 58, 'left');
					doc.text($('#poNo-label').text(), 140, 58, 'left');
					
					doc.text($('#paymentTerm1').html(), 115, 64, 'left');
					doc.text($('#paymentTerm2').html(), 135, 64, 'left');
					doc.text($('#paymentTerm').val(), 140, 64, 'left');
					
					doc.text($('#shipmentBy1').html(), 115, 70, 'left');
					doc.text($('#shipmentBy2').html(), 135, 70, 'left');
					doc.text($('#shipmentBy').val(), 140, 70, 'left');
					
					doc.text($('#etd1').html(), 115, 76, 'left');
					doc.text($('#etd2').html(), 135, 76, 'left');
					doc.text($('#etd').val(), 140, 76, 'left');
					
					doc.text($('#deliveryTerm1').html(), 115, 82, 'left');
					doc.text($('#deliveryTerm2').html(), 135, 82, 'left');
					doc.text($('#deliveryTerm').val(), 140, 82, 'left');
					
					doc.setFontSize(8);
					doc.text($('#netWeight1').html(), 115, 92, 'left');
					doc.text($('#netWeight2').html(), 135, 92, 'left');
					doc.text($('#netWeight').val(), 140, 92, 'left'); //dari inputan

					doc.text($('#shipForm1').html(), 115, 98, 'left');
					doc.text($('#shipForm2').html(), 135, 98, 'left');
					doc.text($('#shipForm').val(), 140, 98, 'left');

					doc.text($('#shipTo-1').html(), 115, 104, 'left');
					doc.text($('#shipTo-2').html(), 135, 104, 'left');
					doc.text($('#shipTo-').val(), 140, 104, 'left');
					
					doc.text($('#blNumber1').html(), 115, 110, 'left');
					doc.text($('#blNumber2').html(), 135, 110, 'left');
					doc.text($('#blNumber').val(), 140, 110, 'left');

					doc.autoTable({
						html: '#print_ship_to',
						theme: 'plain',
						styles: {
							fontSize: 8,
							lineColor: [116, 119, 122],
							lineWidth: 0.1,
							cellWidth: 'auto',

						},
						margin: 4.5,
						tableWidth: (pageWidth / 2) - 15,
						headStyles: {
							valign: 'middle',
							halign: 'left',
						},
						rowStyles: {
							1: {
								tableWidth: 10,
								halign: 'left'
							}
						},
						rowPageBreak: 'auto',
						showHead: 'firstPage',
						showFoot: 'lastPage',
						startY: startYPO + 15
					});
					
					var startYPS = doc.autoTable.previous.finalY;
					
					doc.autoTable({
						html: '#listinvoice',
						theme: 'plain',
						styles: {
							fontSize: 8,
							lineColor: [116, 119, 122],
							lineWidth: 0.1,
							cellWidth: 'auto',

						},
						margin: 4.5,
						tableWidth: (pageWidth - 15),
						headStyles: {
							valign: 'middle',
							halign: 'center',
						},
						didParseCell: function(data) {
							if (data.table.foot[0]) {
								if (data.table.foot[0].cells[4]) {
									data.table.foot[0].cells[4].styles.halign = 'right';
								}
								if (data.table.foot[0].cells[5]) {
									data.table.foot[0].cells[5].styles.halign = 'right';
								}
							}
						},
						columnStyles: {
							0: {
								tableWidth: 10,
								halign: 'center'
							},
							1: {
								tableWidth: 10,
								halign: 'left'
							},
							2: {
								halign: 'center'
							},
							3: {
								halign: 'right'
							},
							4: {
								halign: 'right'
							},
							5: {
								halign: 'right',
								falign: 'left'
							},
						},
						rowPageBreak: 'auto',
						showHead: 'firstPage',
						showFoot: 'lastPage',
						startY: startYPS + 10
					});
				
					var startYPI = doc.autoTable.previous.finalY;
					
					doc.autoTable({
						html: '#print_approve',
						theme: 'plain',
						styles: {
							fontSize: 8,
							lineColor: [116, 119, 122],
							lineWidth: 0.1,
							cellWidth: 'auto',

						},
						margin: 4.5,
						tableWidth: (pageWidth / 2) - 15,
						rowPageBreak: 'auto',
						showHead: 'firstPage',
						showFoot: 'lastPage',
						startY: startYPI + 11
					});
					
					doc.setFontSize(8);
					doc.text("Noted", 10, startYPI + 10, 'center');
					doc.setFontType('bold');
					doc.text($('#kode_report').html(), 205, pageHeight - 5, 'right');
					doc.text($('#kode_report_detail').html(), 205, pageHeight - 10, 'right');

					var x = pageWidth * 70 / 100;
					var y = pageHeight * 75 / 100;
					doc.setFontSize(10);
					doc.text("PT. CELEBIT CIRCUIT TECH INDONESIA ", x, startYPI + 15, 'center');
					doc.text($('#penandaTangan').val(), x, startYPI + 40, 'center');
					doc.save('INVOICE <?php echo $invoice[0]['name_eksternal'].'-'.$invoice[0]['tanggal_do']; ?>.pdf');
				}
			});
		});

		function toDataURL(url, callback) {
			var xhr = new XMLHttpRequest();
			xhr.onload = function() {
				var reader = new FileReader();
				reader.onloadend = function() {
					callback(reader.result);
				}
				reader.readAsDataURL(xhr.response);
			};
			xhr.open('GET', url);
			xhr.responseType = 'blob';
			xhr.send();
		}

		toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
			//console.log('RESULT:', dataUrl)
			dataImage = dataUrl;
		})

		function dtInvoice() {
			t_invoice_list = $('#listinvoice').DataTable({
				"processing": true,
				"searching": false,
				"responsive": true,
				"lengthChange": false,
				"info": false,
				"bSort": false,
				"ajax": {
					"type": "GET",
					"url": "<?php echo base_url() . 'delivery_order/delivery_list_invoice/' . $id_do; ?>",
					"dataSrc": function(response) {
						$('#tdAmount_Total').html(formatCurrencyComaUSD(response.total_amounts));
						return response.data;
					}
				},
				"columnDefs": [{
					"targets": [0],
					"searchable": false,
					"className": 'dt-body-center',
					"width": 10
				}, {
					"targets": [1],
					"searchable": false,
					"className": 'dt-body-center',
					"width": 250
				}, {
					"targets": [2],
					"searchable": false,
					"className": 'dt-body-center',
					"width": 250
				}, {
					"targets": [3],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 150
				}, {
					"targets": [4],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 150
				}, {
					"targets": [5, 7],
					"searchable": false,
					"visible": false,
					"className": 'dt-body-right',
					"width": 60
				}, {
					"targets": [6],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 150
				}],
				"footerCallback": function(row, data, start, end, display) {
					var api = this.api(),
						data;
					var total = 0;
					
					var intVal = function(i) {
						return typeof i === 'string' ?
							i.replace(/[\$,]/g, '') * 1 :
							typeof i === 'number' ?
							i : 0;
					};

					// Total over all pages
					total = api
						.column(7)
						.data()
						.reduce(function(a, b) {
							total = total + intVal(a) + intVal(b);
							return (intVal(a) + intVal(b));
						}, 0);

					// Total over this page
					pageTotal = api
						.column(7, {
							page: 'current'
						})
						.data()
						.reduce(function(a, b) {
							total = total + intVal(a) + intVal(b);
							return (intVal(a) + intVal(b));
						}, 0);

					// Update footer
					$(api.column(7).footer()).html(
						'<?php $invoice[0]['symbol_valas']; ?>' + formatCurrencyComaUSD(pageTotal)
					)
					
					$("#approved-label").text($("#approved").val());
					$("#dateCust-label").text($("#dateCust").val());
					$("#name-label").text($("#name").val());
					$("#total-label").text(formatCurrencyRupiah(total));
					$("#tdAmount_Total").html(formatCurrencyRupiah(total));
					$('#totHide').val(total * 110 / 100);
					
					$('#totHide').val(pageTotal);
					<?php if ($invoice[0]['valas_id'] == 1) { ?>
						terbilangIND();
					<?php } else { ?>
						terbilangENG();
					<?php } ?>
				}
			});
		}
		
		function terb_depan(uang) {
			var sub = '';
			if (uang == 1) {
				sub = 'Satu '
			} else
			if (uang == 2) {
				sub = 'Dua '
			} else
			if (uang == 3) {
				sub = 'Tiga '
			} else
			if (uang == 4) {
				sub = 'Empat '
			} else
			if (uang == 5) {
				sub = 'Lima '
			} else
			if (uang == 6) {
				sub = 'Enam '
			} else
			if (uang == 7) {
				sub = 'Tujuh '
			} else
			if (uang == 8) {
				sub = 'Delapan '
			} else
			if (uang == 9) {
				sub = 'Sembilan '
			} else
			if (uang == 0) {
				sub = '  '
			} else
			if (uang == 10) {
				sub = 'Sepuluh '
			} else
			if (uang == 11) {
				sub = 'Sebelas '
			} else
			if ((uang >= 11) && (uang <= 19)) {
				sub = terb_depan(uang % 10) + 'Belas ';
			} else
			if ((uang >= 20) && (uang <= 99)) {
				sub = terb_depan(Math.floor(uang / 10)) + 'Puluh ' + terb_depan(uang % 10);
			} else
			if ((uang >= 100) && (uang <= 199)) {
				sub = 'Seratus ' + terb_depan(uang - 100);
			} else
			if ((uang >= 200) && (uang <= 999)) {
				sub = terb_depan(Math.floor(uang / 100)) + 'Ratus ' + terb_depan(uang % 100);
			} else
			if ((uang >= 1000) && (uang <= 1999)) {
				sub = 'Seribu ' + terb_depan(uang - 1000);
			} else
			if ((uang >= 2000) && (uang <= 999999)) {
				sub = terb_depan(Math.floor(uang / 1000)) + 'Ribu ' + terb_depan(uang % 1000);
			} else
			if ((uang >= 1000000) && (uang <= 999999999)) {
				sub = terb_depan(Math.floor(uang / 1000000)) + 'Juta ' + terb_depan(uang % 1000000);
			} else
			if ((uang >= 100000000) && (uang <= 999999999999)) {
				sub = terb_depan(Math.floor(uang / 1000000000)) + 'Milyar ' + terb_depan(uang % 1000000000);
			} else
			if ((uang >= 1000000000000)) {
				sub = terb_depan(Math.floor(uang / 1000000000000)) + 'Triliun ' + terb_depan(uang % 1000000000000);
			}
			return sub;
		}

		function terb_belakang(t) {
			if (t.length == 0) {
				return '';
			}
			return t
				.split('0').join('Kosong ')
				.split('1').join('Satu ')
				.split('2').join('Dua ')
				.split('3').join('Tiga ')
				.split('4').join('Empat ')
				.split('5').join('Lima ')
				.split('6').join('Enam ')
				.split('7').join('Tujuh ')
				.split('8').join('Delapan ')
				.split('9').join('Dembilan ');
		}

		function terbilang(nangka) {
			var
				v = 0,
				sisa = 0,
				tanda = '',
				tmp = '',
				sub = '',
				subkoma = '',
				p1 = '',
				p2 = '',
				pkoma = 0;
			// nangka = nangka.replace(/[^\d.-]/g, '');
			if (nangka > 999999999999999999) {
				return 'Buset dah buanyak amat...';
			}
			v = nangka;
			if (v < 0) {
				tanda = 'Minus ';
			}
			v = Math.abs(v);
			tmp = v.toString().split('.');
			p1 = tmp[0];
			p2 = '';
			// console.log(parseFloat(tmp[1]), 0, parseFloat(tmp[1]) > 0)
			if (parseFloat(tmp[1]) > 0) {
				p2 = tmp[1];
			}
			v = parseFloat(p1);
			sub = terb_depan(v);
			/* sisa = parseFloat('0.'+p2);
			subkoma = terb_belakang(sisa); */
			subkoma = 'Koma ' + terb_belakang(p2).replace('  ', ' ');;
			if (subkoma == 'Koma ')
				subkoma = '';
			sub = tanda + sub.replace('  ', ' ') + subkoma;
			return sub.replace('  ', ' ') + " Rupiah";
		}

		function terbilangIND() {
			var bilangan = document.getElementById("totHide").value;
			var kalimat = terbilang(bilangan);
			
			document.getElementById("totalBilangan").innerHTML = kalimat;
		}

		function toWordsconver(s) 
		{
			var th_val = ['', 'Thousand', 'Million', 'Billion', 'Trillion'];
			var dg_val = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];
			var tn_val = ['Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
			var tw_val = ['Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];
			s = s.toString();
			s = s.replace(/[\, ]/g, '');
			if (s != parseFloat(s))
				return 'not a number ';
			var x_val = s.indexOf('.');
			if (x_val == -1)
				x_val = s.length;
			if (x_val > 15)
				return 'too big';
			var n_val = s.split('');
			var str_val = '';
			var sk_val = 0;
			for (var i = 0; i < x_val; i++) {
				if ((x_val - i) % 3 == 2) {
					if (n_val[i] == '1') {
						str_val += tn_val[Number(n_val[i + 1])] + ' ';
						i++;
						sk_val = 1;
					} else if (n_val[i] != 0) {
						str_val += tw_val[n_val[i] - 2] + ' ';
						sk_val = 1;
					}
				} else if (n_val[i] != 0) {
					str_val += dg_val[n_val[i]] + ' ';
					if ((x_val - i) % 3 == 0)
						str_val += 'Hundred ';
					sk_val = 1;
				}
				if ((x_val - i) % 3 == 1) {
					if (sk_val)
						str_val += th_val[(x_val - i - 1) / 3] + ' ';
					sk_val = 0;
				}
			}
			// console.log(dg_val, n_val);
			if (x_val != s.length) {
				var y_val = s.length;
				str_val += 'And Cents ';
				for (var i = x_val + 1; i < y_val; i++) {
					// str_val += dg_val[n_val[i]] + ' ';
					if ((y_val - i) % 3 == 2) {
						if (n_val[i] == '1') {
							str_val += tn_val[Number(n_val[i + 1])] + ' ';
							i++;
							sk_val = 1;
						}else if (n_val[i] != 0) {
							str_val += tw_val[n_val[i] - 2] + ' ';
							sk_val = 1;
						}
					}else if (n_val[i] != 0) {
						str_val += dg_val[n_val[i]] + ' ';
						if ((x_val - i) % 3 == 0)
							str_val += 'Hundred ';
						sk_val = 1;
					}

					if ((y_val - i) % 3 == 1) {
						if (sk_val)
							str_val += th_val[(y_val - i - 1) / 3] + ' ';
						sk_val = 0;
					}
				}
			}
			return "Says : ( " + str_val.replace(/\s+/g, ' ') + " Only )";
		}

		function terbilangENG() {
			var bilangan = $("#tdAmount_Total").html();
			var kalimat = toWordsconver(bilangan);

			document.getElementById("totalBilangan").innerHTML = kalimat;
		}

		function cal_price(row) {
			var total = 0;
			var rowData = t_invoice_list.row(row).data();
			var amount = rowData[5];

			var qtykoma = rowData[3];
			var qty = qtykoma.replace(/[^\d.-]/g, '');
			var unitPrice = $('#unit_price' + row).val();
			var total = numberWithCommas($('#total-label').html());

			amount = parseFloat(unitPrice) * parseFloat(qty);

			$('input[id="amount' + row + '"]').val(parseFloat(amount).toFixed(2));
			<?php if ($invoice[0]['valas_id'] == 1) { ?>
				$("#amount-label" + row).html(formatCurrencyRupiah(amount));
				$("#unit_price-label" + row).html(formatCurrencyRupiah(parseFloat(unitPrice)));
			<?php } else { ?>
				$("#amount-label" + row).html(formatCurrencyComaUSD(amount));
				$("#unit_price-label" + row).html(formatCurrencyComaUSD(parseFloat(unitPrice)));
			<?php } ?>
			var tempTotal = 0;
			var totalSeluruh = 0;
			$('input[name="amount[]"]').each(function() {
				if (this.value !== '' && this.value !== null) {
					tempTotal = tempTotal + parseFloat(this.value);
					totalSeluruh = parseFloat(tempTotal);
				} else {
					tempTotal = tempTotal + 0;
					totalSeluruh = tempTotal + 0;
				}
			})
			// console.log(tempTotal);
			$('#total-label').text(formatCurrencyRupiah(tempTotal));
			$('#tdAmount_Total').html(formatCurrencyRupiah(tempTotal));
			$("#totalBilangan").html(terbilang(tempTotal));
		}

		function redrawTable(row) {
			var rowData = t_invoice_list.row(row).data();

			rowData[5] = $('#unit_price' + row).val();
			rowData[7] = $('#amount' + row).val();
			t_invoice_list.draw();
		}
		
		function numberWithCommas(x) {
			var parts = x.toString().split(".");
			parts[0] = parts[0].replace(",", "");
			return parts.join(".");
		}
	</script>