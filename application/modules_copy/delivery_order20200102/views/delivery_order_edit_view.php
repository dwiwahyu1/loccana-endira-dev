	<style>
		#loading-us{display:none}
		#tick{display:none}

		#loading-mail{display:none}
		#cross{display:none}
		.add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
		.add_item:hover{color:#ff8c00}
		.right-text{text-align:right}
		
		.dt-body-left {text-align:left;}
		.dt-body-right {text-align:right;}
		.dt-body-center {text-align:center;}
	</style>

	<form class="form-horizontal form-label-left" id="delivery_detail_order_edit" role="form" action="<?php echo base_url('delivery_order/edit_delivery_order');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

  		<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
		<br>
        <div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_do">No DO <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input placeholder="No. DO" type="text" class="form-control" id="no_do" name="no_do" value="<?php if(isset($do[0]['no_do'])){ echo $do[0]['no_do']; }?>" readonly required>
				<span id="loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking No DO...</span>
				<span id="tick"></span>
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_do">Tanggal Delivery <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<div class="input-group date">
					<input placeholder="<?php echo date('Y-m-d'); ?>" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="tanggal_do" name="tanggal_do" placeholder="" value="<?php if(isset($do[0]['tanggal_do'])){ echo $do[0]['tanggal_do']; }?>" required>
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-th"></span>
					</div>
				</div>
			</div>
		</div>
		
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="detail_box">Detail Box <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<textarea placeholder="Detail Box" class="form-control" id="detail_box" name="detail_box" required><?php if(isset($do[0]['detail_box'])){ echo $do[0]['detail_box']; }?></textarea>
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_pi">No PI <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input placeholder="No. PI" type="text" class="form-control" id="no_pi" name="no_pi" value="<?php if(isset($do[0]['no_pi'])){ echo $do[0]['no_pi']; }?>" required>
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="remark">Remark <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<textarea placeholder="Remark" class="form-control" id="remark" name="remark" required><?php if(isset($do[0]['remark'])){ echo $do[0]['remark']; }?></textarea>
			</div>
		</div>
		
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_bc">No BC <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<select class="form-control" id="no_bc" name="no_bc" style="width: 100%" required>
					<option value="">Tidak ada No Pendaftaran</option>
					<?php foreach($pendaftaran as $bc) { ?>
						<option value="<?php echo $bc['id'];?>" <?php if(isset($do[0]['id_bc'])) { if( $do[0]['id_bc'] == $bc['id'] ) { echo "selected"; } }?>><?php echo $bc['no_pendaftaran'].' - '.$bc['jenis_bc']; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Barang : </label>
			<div class="col-md-8 col-sm-6 col-xs-12 add_item">
				<a class="btn btn-primary" onclick="delivery_order_edit_item()">
					<i class="fa fa-plus"></i> Tambah Barang STBJ
				</a> 
				<input type="hidden" id="tambah_barang" name="tambah_barang" value="0">
			</div>
		</div>

	
		<div class="item form-group">
			<table id="listeditstbj" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th></th>
						<th>No STBJ</th>
						<th>Tanggal STBJ</th>
						<th>Part No</th>
						<th>Detail Box</th>
						<th>No PI</th>
						<th>Remark</th>
						<th></th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	
		<hr>
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Simpan</button>
				<input type="hidden" id="id_do" name="id_do" value="<?php if(isset($do[0]['id_do'])){ echo $do[0]['id_do']; }?>">
				<input type="hidden" id="id_stbj" name="id_stbj" value="<?php if(isset($do[0]['id_stbj'])){ echo $do[0]['id_stbj']; }?>">
				<input type="hidden" id="id_packing" name="id_packing" value="<?php if(isset($do[0]['id_packing'])){ echo $do[0]['id_packing']; }?>">
			</div>
		</div>
	</form>
	
	<script type="text/javascript">
	var t_editSTBJ;
	
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
		
		$('#tanggal_do').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true,
			changeYear: true,
			minDate: '-3M',
			maxDate: '+30D',
		});
		
		$('#no_bc').select2({
			closeOnSelect: false
		});
		dt_STBJ();
	});

	function dt_STBJ() {
		t_editSTBJ = $('#listeditstbj').DataTable( {
			"processing": true,
			"searching": false,
			"responsive": true,
			"lengthChange": false,
			"ajax": {
				"type" : "GET",
				"url" : "<?php echo base_url().'delivery_order/delivery_detail_stbj/'.$do[0]['id_do'];?>"
            },
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0],
				"visible": false,
				"searchable": false
			}]
		});
	}
	
	function delivery_order_edit_item(){
		$('#panel-modal-detail').removeData('bs.modal');
		$('#panel-modal-detail  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal-detail  .panel-body').load('<?php echo base_url('delivery_order/delivery_order_edit_item');?>');
		$('#panel-modal-detail  .panel-title').html('<i class="fa fa-plus"></i> Edit STBJ Barang');
		$('#panel-modal-detail').modal({backdrop:'static',keyboard:false},'show');
	}

	$('#listeditstbj').on("click", "button", function(){
		var delStat = false;
		var dataSTBJ = t_editSTBJ.row($(this).parents('tr')).data();
		if(dataSTBJ[0] != "") {
			swal({
				title: 'Yakin akan Menghapus Barang yang sudah ada ?',
				text: 'data tidak dapat dikembalikan bila sudah dihapus !',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak'
			}).then(function () {
				var datapost={
					"id_stbj" : parseInt(dataSTBJ[0])
				};

				$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>delivery_order/delete_delivery_order_detail",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
						if (response.status == "success") {
							swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () { })
						}else {
							swal("Failed!", response.message, "error");
							delStat = false;
						}
					}
				});
			})
			t_editSTBJ.row($(this).parents('tr')).remove().draw(false);
		}else t_editSTBJ.row($(this).parents('tr')).remove().draw(false);

		for (var i = 0; i < t_editSTBJ.rows().data().length; i++) {
			var rowData = t_editSTBJ.row(i).data();
		}
	});
	
	$('#delivery_detail_order_edit').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		var formData = new FormData(this);
		
		$.ajax({
			type:'POST',
			url: $(this).attr('action'),
			data:formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				console.log(response);
				if (response.success == true) {
					save_stbj();
				}else{
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Simpan");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Simpan");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}));
	
	function save_stbj() {
		var arrTemp = [];
		var id_do = '<?php echo $do[0]['id_do']; ?>';
		for (var i = 0; i < t_editSTBJ.rows().data().length; i++) {
			var rowData = t_editSTBJ.row(i).data();
			arrTemp.push(rowData); 
		}

		var datapost = {
			"id_do"		: id_do,
			"liststbj"	: arrTemp
		};
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>delivery_order/edit_detail_delivery_order",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						$('#panel-modal').modal('toggle');
						listdelivery();
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Simpan");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Simpan");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}
	
	function isNumber(evt) {
		var iKeyCode = (evt.which) ? evt.which : evt.keyCode
		if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
			return false;

		return true;
	}
	</script>