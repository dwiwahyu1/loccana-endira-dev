<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Delivery_order extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('delivery_order/do_model');
		$this->load->library('log_activity');
		$this->load->library('numbering');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string)
	{
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index()
	{
		$this->template->load('maintemplate', 'delivery_order/views/index');
	}

	function lists()
	{
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'asc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

		$order_fields = array('no_do', 'no_pi');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->do_model->lists($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		// echo "<pre>";print_r($list);die;
		$data = array();
		$no = 1;
		foreach ($list['data'] as $k => $v) {
			$no = $k + 1;
			if ($v['status'] != 1 && $v['status'] != 2 && $v['status'] != 3) {
				$actions = '<div class="btn-group">';
				$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit DO" onClick="delivery_order_edit(\'' . $v['id_do'] . '\')">';
				$actions .= '       <i class="fa fa-edit"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
				$actions .= '<div class="btn-group">';
				$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Detail DO" onClick="delivery_order_detail(\'' . $v['id_do'] . '\')">';
				$actions .= '       <i class="fa fa-search"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
				// $actions .= '<div class="btn-group">';
				// $actions .= '   <button class="btn btn-icon waves-effect waves-light btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Edit Status" onClick="delivery_order_status(\'' . $v['id_do'] . '\')">';
				// $actions .= '       <i class="fa fa-pencil"></i>';
				// $actions .= '   </button>';
				// $actions .= '</div>';
				// $actions .= '<div class="btn-group">';
				// $actions .= '   <button class="btn btn-icon waves-effect waves-light btn-default" type="button" data-toggle="tooltip" data-placement="top" title="Invoice" onClick="delivery_order_invoice(\'' . $v['id_do'] . '\')">';
				// $actions .= '       <i class="fa fa-file-pdf-o"></i>';
				// $actions .= '   </button>';
				// $actions .= '</div>';
			} else {
				$actions = '<div class="btn-group">';
				$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Detail DO" onClick="delivery_order_detail(\'' . $v['id_do'] . '\')">';
				$actions .= '       <i class="fa fa-search"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
			}

			if ($v['status'] == 0) {
				$status = '<div class="">';
				$status .=		'<span class="label label-yellow">Pending</span>';
				$status .= '</div>';
			} elseif ($v['status'] == 1) {
				$status = '<div class="">';
				$status .=		'<span class="label label-primary">Proses</span>';
				$status .= '</div>';
			} elseif ($v['status'] == 2) {
				$status = '<div class="">';
				$status .=		'<span class="label label-success">Selesai</span>';
				$status .= '</div>';
			} else {
				$status = '<div class="">';
				$status .=		'<span class="label label-danger">Ditolak</span>';
				$status .= '</div>';
			}

			//if ($v['status'] != 0 && $v['status'] != 3) {
			if ($v['no_invoice'] != '' ) {
				$actions .= '<div class="btn-group">';
				$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-default" type="button" data-toggle="tooltip" data-placement="top" title="Invoice" onClick="delivery_order_invoice(\'' . $v['id_do'] . '\')">';
				$actions .= '       <i class="fa fa-file-pdf-o"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
				$actions .= '<div class="btn-group">';
				$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Packing List" onClick="delivery_order_packing(\'' . $v['id_do'] . '\')">';
				$actions .= '       <i class="fa fa-file-pdf-o"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
				$actions .= '<div class="btn-group">';
				$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delivery Order" onClick="delivery_order_do(\'' . $v['id_do'] . '\')">';
				$actions .= '       <i class="fa fa-file-pdf-o"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
			}else{
				$actions .= '<div class="btn-group">';
				$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-default" type="button" data-toggle="tooltip" data-placement="top" title="Invoice" onClick="delivery_order_invoice(\'' . $v['id_do'] . '\')">';
				$actions .= '       <i class="fa fa-file-pdf-o"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
			}

			array_push(
				$data,
				array(
					$no,
					$v['no_do'],
					$v['name_eksternal'],
					$v['tanggal'],
					$v['detail_box'],
					$v['no_pi'],
					$v['remark'],
					$status,
					$actions
				)
			);
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function delivery_order_add()
	{
		$results = $this->do_model->delivery_order_nobc();

		$data = array(
			'pendaftaran' => $results
		);

		$this->load->view('delivery_order_add_view', $data);
	}

	public function delivery_order_detail($id_do)
	{
		$results = $this->do_model->delivery_order_detail($id_do);

		$data = array(
			'dp' => $results
		);

		$this->load->view('delivery_order_detail_view', $data);
	}

	public function delivery_order_edit($id_do)
	{
		$results = $this->do_model->delivery_order_detail($id_do);
		$pendaftaran = $this->do_model->delivery_order_nobc();

		$data = array(
			'do' => $results,
			'pendaftaran' => $pendaftaran
		);

		$this->load->view('delivery_order_edit_view', $data);
	}

	public function delivery_order_list_invoice($id_do)
	{
		$results = $this->do_model->delivery_list_invoice($id_do);
		$invoices = $this->do_model->get_invoice($id_do);

		$tanggal_terima = $results[0]['date_end'];
		$price_term = $results[0]['price_term'];

		$term = explode(" ", $price_term);
		$exp_date = $term[0];
		$due_date = date('Y-m-d', strtotime('+' . $exp_date . ' days', strtotime($tanggal_terima)));

		$data = array(
			'id_do' => $results[0]['id_do'],
			'invoice' => $results,
			'invoiced' => $invoices,
			'due_date' => $due_date
		);

		if ($results[0]['id_eks'] == 136) { // FLEXTRONICS INTERNATIONAL HTR KFT 475
			$this->load->view('delivery_order_invoice_flextronic_view', $data);
		} elseif ($results[0]['id_eks'] == 140) {// SMK
			$this->load->view('delivery_order_invoice_smk_view', $data);
		} elseif ($results[0]['id_eks'] == 579) {//UNDERWRITERS LABORATORIES TAIWAN
			$this->load->view('delivery_order_invoice_proforma_view', $data);
		} elseif ($results[0]['id_eks'] == 138) {// MIDORIYA ELECTRIC (H.K) CO., LIMITED C/O
			$this->load->view('delivery_order_invoice_midoriya_view', $data);
		} else {
			$this->load->view('delivery_order_invoice_view', $data);
		}
		
	}

	public function delivery_order_list_packing($id_do)
	{
		$invoices = $this->do_model->get_invoice($id_do);
		$results = $this->do_model->delivery_list_packing($id_do);

		$tanggal_terima = $results[0]['date_end'];
		$price_term = $results[0]['price_term'];

		$term = explode(" ", $price_term);
		$exp_date = $term[0];
		$due_date = date('Y-m-d', strtotime('+' . $exp_date . ' days', strtotime($tanggal_terima)));

		$data = array(
			'id_do' => $results[0]['id_do'],
			'packing' => $results,
			'due_date' => $due_date
		);

		if($results[0]['id_eks'] == 136) { // FLEXTRONICS INTERNATIONAL HTR KFT 475
			$this->load->view('delivery_order_packing_flextronic_view', $data);
		}elseif($results[0]['id_eks'] == 140){ // SMK
			$this->load->view('delivery_order_packing_smk_view', $data);
		}elseif($results[0]['id_eks'] == 579){ //UNDERWRITERS LABORATORIES TAIWAN
			$this->load->view('delivery_order_packing_proforma_view', $data);
		}elseif($results[0]['id_eks'] == 138){// MIDORIYA ELECTRIC (H.K) CO., LIMITED C/O
			$this->load->view('delivery_order_packing_midoriya_view', $data);
		}else{ //selain di atas
			$this->load->view('delivery_order_packing_view', $data);
		}
	}

	public function delivery_order_list_do($id_do)
	{
		$invoices = $this->do_model->get_invoice($id_do);
		$results = $this->do_model->delivery_list_do($id_do);

		$tanggal_terima = $results[0]['date_end'];
		$price_term = $results[0]['price_term'];

		$term = explode(" ", $price_term);
		$exp_date = $term[0];
		$due_date = date('Y-m-d', strtotime('+' . $exp_date . ' days', strtotime($tanggal_terima)));

		$totalQty 		= 0;
		$totalRemark	= 0;
		if (sizeof($results) > 0) {
			foreach ($results as $sk => $sv) {
				$totalQty 		= $sv['qty_array'] + $totalQty;
				$totalRemark	= $sv['qty_box'] + $totalRemark;
			}
		}

		$data = array(
			'id_do'			=> $results[0]['id_do'],
			'do'			=> $results,
			'due_date'		=> $due_date,
			'totalQty'		=> $totalQty,
			'totalRemark'	=> $totalRemark
		);

		$this->load->view('delivery_order_do_view', $data);
	}

	public function delivery_order_edit_item()
	{
		$results =  $this->do_model->delivery_list_stbj_22();

		$data = array(
			'stbj' => $results
		);

		$this->load->view('delivery_order_edit_item_view', $data);
	}

	public function add_edit_item()
	{
		$result_spb = $this->purchase_order_model->spb();

		$data = array(
			'spb' => $result_spb
		);

		$this->load->view('edit_modal_item_view', $data);
	}

	public function delivery_order_add_item()
	{
		$results = $this->do_model->delivery_list_stbj();

		$data = array(
			'stbj' => $results
		);

		$this->load->view('delivery_order_add_item_view', $data);
	}

	public function delivery_stbj_search_id()
	{
		$data		= file_get_contents("php://input");
		$params		= json_decode($data, true);
		
		$stbj	= explode(",", $params['stbj']);
		$get_no_stbj = explode(" - ", $stbj[0]);

		$var = array(
			'no_stbj' => $get_no_stbj[0],
			'id_po_quot' => $stbj[1]
		);
		
		$list		= $this->do_model->delivery_stbj_search_id($var);
		
		$data = array();
		$tempObj = new stdClass();

		if (sizeof($params['tempSTBJ']) > 0) {
			foreach ($params['tempSTBJ'] as $p => $pk) {
				for ($i = 0; $i <= sizeof($list); $i++) {
					if (!empty($list[$i])) {
						if ($list[$i]['no_stbj'] == $pk[1] && $list[$i]['no_stbj'] == $pk[2]) {
							unset($list[$i]);
						}
					}
				}
			}
		}

		$i = 0;
		foreach ($list as $k => $v) {

			$strOption =
				'<div class="checkbox">' .
				'<input id="option[' . $i . ']" type="checkbox" value="' . $i . '">' .
				'<label for="option[' . $i . ']"></label>' .
				'</div>';

			array_push($data, array(
				$v['id_stbjorder'],
				$v['no_stbj'],
				$v['tanggal_stbj'],
				$v['stock_name'],
				$v['detail_box'],
				$v['no_pi'],
				$v['remark'],
				$strOption
			));
			$i++;
		}

		$res = array(
			'status'	=> 'success',
			'data'		=> $data
		);

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function detail_delivery_order($id_do)
	{
		$list = $this->do_model->delivery_list_invoice($id_do);

		$data = array();
		$i = 0;
		$no = 0;

		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list as $k => $v) {

			$actions = '<div class="btn-group">';
			$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Hapus" onClick="delivery_detail_order_delete(\'' . $v['id_packing'] . '\')">';
			$actions .= '       <i class="fa fa-trash"></i>';
			$actions .= '   </button>';
			$actions .= '</div>';

			array_push($data, array(
				$no = $k + 1,
				$v['no_stbj'],
				$v['part_no'],
				$v['tanggal_stbj'],
				$v['db_stbj'],
				$v['no_pi_stbj'],
				$v['remark_stbj'],
				//$actions
			));
			$i++;
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function detail_delivery_order_packing($id_do)
	{
		$data = array();
		$list = $this->do_model->delivery_list_packing($id_do);

		$i = 0;
		$no = 0;
		$total_qty = 0;
		$total_net = 0;
		$total_gross = 0;

		$username = $this->session->userdata['logged_in']['username'];

		foreach ($list as $k => $v) {
			$actions = '<div class="btn-group">';
			$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Hapus" onClick="delivery_detail_order_delete(\'' . $v['id_packing'] . '\')">';
			$actions .= '       <i class="fa fa-trash"></i>';
			$actions .= '   </button>';
			$actions .= '</div>';
			
			$part = '<label id="part">PRINTED CIRCUIT BOARD</label><br>';
			$part .= '<p id="hs_code">Hs Code : 8534.00.10</p>';
			
			$ctnNo = '<input type="text" class="form-control" id="no_kontainer' . $i . '" name="no_kontainer' . $i . '" value="" onInput="cal_price(' . $i . ')" onChange="redrawTable(' . $i . ')" style="width: 100%;" value="" placeholder="Silahkan Input">';
			$ctnNo_label = '<label style="display: none;" id="lb_no_kontainer' . $i . '"></label>';
			
			$epsonCode = '<input type="text" class="form-control" id="epson_code' . $i . '" name="epson_code' . $i . '" value="" onInput="cal_code(' . $i . ')" onChange="redrawTableCode(' . $i . ')" style="width: 100%;" value="" placeholder="Silahkan Input">';
			$epsonCode_label = '<label style="display: none;" id="lb_epson_code' . $i . '"></label>';

			
			//id_eks : 15 : Hartono & id_eks : 136 : Flextronic pembagi array_pcs = 1
			if ($v['id_eks'] == 136 || $v['id_eks'] == 140 || $v['id_eks'] == 579) {
				$total_qty = $total_qty + (int) $v['qty_box'];
				$total_net = $total_net + (int) $v['qty_array'];
				$total_gross = 0;
				$part = '';
				
				array_push($data, array(
					$i + 1,
					$ctnNo.' '.$ctnNo_label,
					$ctnNo_label,
					$v['no_po'],
					$v['part_no'],
					number_format($v['qty_box'], 2),
					number_format($v['qty_array'], 2),
					number_format($v['weight_n'], 2),
					$total_qty,
					$total_net,
					$total_gross,
				));
				$i++;
			} else if($v['id_eks'] == 138) {
				$total_qty = $total_qty + (int) $v['pcs_array'];
				$total_net = $total_net + (int) $v['netto'];
				$total_gross = $total_gross + (int) $v['brutto'];
				
				array_push($data, array(
					$i + 1,
					$ctnNo.' '.$ctnNo_label,
					$ctnNo_label,
					$v['part_no'],
					$epsonCode.' '.$epsonCode_label,
					$epsonCode_label,
					$v['no_po'],
					number_format($v['qty_per_box'], 2),
					number_format($v['pcs_array'], 2),
					number_format($v['netto'], 2),
					number_format($v['brutto'], 2),
					$total_qty,
					$total_net,
					$total_gross,
					$part
				));
				$i++;
			}else {
				$total_qty = $total_qty + (int) $v['qty_array'];
				$total_net = $total_net + (int) $v['netto'];
				$total_gross = $total_gross + (int) $v['brutto'];
				$part = '';
				
				array_push($data, array(
					$i + 1,
					$ctnNo.' '.$ctnNo_label,
					$ctnNo_label,
					$v['no_po'],
					$v['stock_code'],
					$v['part_no'],
					number_format($v['qty_array'], 2),
					number_format($v['netto'], 2),
					number_format($v['brutto'], 2),
					$total_qty,
					$total_net,
					$total_gross
				));
				$i++;
			}
		}

		$result["data"] = $data;
		$result["part"] = $part;
		$result["total_qty"] = $total_qty;
		$result["total_net"] = $total_net;
		$result["total_gross"] = $total_gross;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function detail_delivery_order_do($id_do)
	{
		$data = array();
		$list = $this->do_model->delivery_list_do($id_do);
		$i = 0;
		$no = 0;
		$total_qty = 0;
		$total_net = 0;
		$total_gross = 0;

		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list as $k => $v) {

			$ctnNo = '<input type="text" class="form-control" id="no_kontainer' . $i . '" name="no_kontainer' . $i . '" onInput="cal_price(' . $i . ')" style="width: 100%;" value="">';

			$total_qty = $total_qty + (int) $v['qty_array'];
			$total_net = $total_net + (int) $v['net_weight'];
			$total_gross = $total_gross + (int) $v['gross_weight'];

			array_push($data, array(
				$i + 1,
				$v['part_no'],
				$v['no_po'],
				number_format($v['qty_array'], 0, ',', '.'),
				number_format($v['qty_box'], 0, ',', '.') . '&emsp;&emsp;&emsp;&emsp;' . number_format($v['qty_box'], 0, ',', '.') . '&emsp;&emsp;@&emsp;&emsp;' . number_format($v['qty_per_box'], 0, ',', '.')
				// $v['remark_stbj']
			));
			$i++;
		}
		
		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function delivery_detail_stbj($id_do)
	{
		$list = $this->do_model->delivery_order_detail($id_do);

		$data = array();
		$i = 0;

		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list as $k => $v) {

			$actions = '<div class="btn-group">';
			$actions .=   	'<button class="btn btn-icon waves-effect waves-light btn-danger">';
			$actions .=			'<i class="fa fa-trash"></i>';
			$actions .=		'</button>';
			$actions .= '</div>';

			array_push($data, array(
				$v['id_stbj'],
				$v['no_stbj'],
				$v['tanggal_stbj'],
				$v['part_no'],
				$v['db_stbj'],
				$v['no_pi_stbj'],
				$v['remark_stbj'],
				$actions
			));
			$i++;
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function save_invoice()
	{
		$id_do				= $this->Anti_sql_injection($this->input->post('id_do', TRUE));
		$id_eksternal		= $this->Anti_sql_injection($this->input->post('id_eks', TRUE));
		$no_invoice			= $this->Anti_sql_injection($this->input->post('noInvoice', TRUE));
		$tanggal_invoice	= $this->Anti_sql_injection($this->input->post('tanggalInvoice', TRUE));
		$due_date			= $this->Anti_sql_injection($this->input->post('dueDateInvoice', TRUE));
		$attn				= $this->Anti_sql_injection($this->input->post('attn', TRUE));
		$ppn				= $this->Anti_sql_injection($this->input->post('ppn-input', TRUE));
		$penandaTangan		= $this->Anti_sql_injection($this->input->post('penandaTangan', TRUE));
		$total_amount		= $this->Anti_sql_injection($this->input->post('total_amount', TRUE));
		$ppn_harga			= $this->Anti_sql_injection($this->input->post('ppn_harga', TRUE));
		$total				= $this->Anti_sql_injection($this->input->post('total', TRUE));
		$id_valas			= $this->Anti_sql_injection($this->input->post('id_valas', TRUE));
		$note				= $this->Anti_sql_injection($this->input->post('notes-input', TRUE));

		if($ppn == 0 || $ppn == '') {
			$ppn = 0;
		}
		
		if($due_date == ''){
			$due_date = $tanggal_invoice;
		}
		
		$data = array(
			'id_do'				=> $id_do,
			'status'			=> 1,
			'date_end'			=> date('Y-m-d'),
			'id_do'				=> $id_do,
			'no_invoice'		=> $no_invoice,
			'tanggal_invoice'	=> $tanggal_invoice,
			'due_date'			=> $due_date,
			'sign'				=> $penandaTangan,
			'note'				=> $note,
			'attn'				=> $attn
		);
		
		$list = $this->do_model->delivery_update_status($data);
		
		$delete_cur_inv = $this->do_model->delete_invoice_cur($data);

		$results = $this->do_model->insert_report_invoice($data);

		if ($results > 0) {
			$msg = 'Berhasil menambahkan data Report Invoice dengan No Invoice : ' . $no_invoice;
			
			$results = array(
				'success' 		=> true,
				'message' 		=> $msg,
				'lastid' 		=> $results['lastid'],
				'id_valas' 		=> $id_valas,
				'id_eksternal' 	=> $id_eksternal,
				'total_amount'	=> $total_amount,
				'ppn_harga'		=> $ppn_harga,
				'ppn'			=> $ppn
			);

			$this->log_activity->insert_activity('insert', $msg . ' dengan No Invoice : ' . $no_invoice);
		} else {
			$msg = 'Gagal menambahkan data Invoice ke database';

			$results = array(
				'success' => false,
				'message' => $msg
			);

			$this->log_activity->insert_activity('insert', $msg . ' dengan No Invoice : ' . $no_invoice);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}
	
	public function insert_detail_invoice()
	{
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);
		
		foreach ($params['list_invoice'] as $k => $v) {
			$arrTemp = array(
				'id_invoice'	=> $params['id_invoice'],
				'qty'			=> $params['list_invoice'][$k]['3'],
				'unit_price'	=> $params['list_invoice'][$k]['5'],
				'amount'		=> $params['list_invoice'][$k]['7'],
				'ppn'			=> $params['ppn'],
			);

			$results = $this->do_model->insert_report_detail_invoice($arrTemp);
		}

		$get_inv  = $this->do_model->get_inv($params['id_invoice']);
		$get_rate = $this->do_model->get_rate($params['id_invoice']);
		$get_coa  = $this->do_model->get_coa_eksternal($params['id_eksternal']);
		
		$data_coa_total_ppn = array(
			'id_coa' 		=> $get_coa[0]['id_coa'],
			'id_parent' 	=> $get_coa[0]['id_parent'],
			'date' 			=> $get_inv[0]['tanggal_invoice'],
			'id_valas' 		=> $params['id_valas'],
			'value' 		=> $params['total_amount'],
			'adjustment' 	=> 0,
			'type_cash' 	=> 0,
			'note' 			=> 'Penjualan No Invoice : ' .$get_inv[0]['no_invoice'],
			'rate' 			=> $get_rate[0]['rate'],
			'bukti' 		=> NULL,
			'id_coa_temp' 	=> NULL
		);
		
		$this->do_model->insert_coa_value_total_ppn($data_coa_total_ppn);
		
		if($params['id_valas'] == 1){
			$id_coa = 462;
			$id_parent = 70102;
		}else{
			$id_coa = 461;
			$id_parent = 70101;
		}
		
		$data_coa_total = array(
			'id_coa' 		=> $id_coa,
			'id_parent' 	=> $id_parent,
			'date' 			=> $get_inv[0]['tanggal_invoice'],
			'id_valas' 		=> $params['id_valas'],
			'value' 		=> $get_inv[0]['amount'],
			'adjustment' 	=> 0,
			'type_cash' 	=> 1,
			'note' 			=> 'Penjualan No Invoice : ' .$get_inv[0]['no_invoice'],
			'rate' 			=> $get_rate[0]['rate'],
			'bukti' 		=> NULL,
			'id_coa_temp' 	=> NULL
		);
		
		$this->do_model->insert_coa_value_total($data_coa_total);
		
		$data_coa_ppn = array(
			'id_coa' 		=> 449, //$get_coa[0]['id_coa'],
			'id_parent' 	=> 50701, //$get_coa[0]['id_parent'],
			'date' 			=> $get_inv[0]['tanggal_invoice'],
			'id_valas' 		=> $params['id_valas'],
			'value' 		=> $params['ppn_harga'],
			'adjustment' 	=> 0,
			'type_cash' 	=> 1,
			'note' 			=> 'Penjualan No Invoice : ' .$get_inv[0]['no_invoice'],
			'rate' 			=> $get_rate[0]['rate'],
			'bukti' 		=> NULL,
			'id_coa_temp' 	=> NULL
		);
		
		$this->do_model->insert_coa_value_ppn($data_coa_ppn);
		
		if ($results > 0) {
			$msg = 'Berhasil menambahkan detail Report Invoice';

			$results = array(
				'success' => true,
				'message' => $msg
			);

			$this->log_activity->insert_activity('insert', $msg . ' dengan ID Invoice' . $arrTemp['id_invoice']);
		} else {
			$msg = 'Gagal menambahkan detail Report Invoice ke database';

			$results = array(
				'success' => false,
				'message' => $msg
			);

			$this->log_activity->insert_activity('insert', $msg . ' dengan ID Invoice ' . $arrTemp['id_invoice']);
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($results);
	}
	
	public function save_packing()
	{
		$id_do				= $this->Anti_sql_injection($this->input->post('id_do', TRUE));
		$id_eksternal		= $this->Anti_sql_injection($this->input->post('id_eks', TRUE));
		$no_invoice			= $this->Anti_sql_injection($this->input->post('noInvoice', TRUE));
		$tanggal_invoice	= $this->Anti_sql_injection($this->input->post('tanggalInvoice', TRUE));
		$attn				= $this->Anti_sql_injection($this->input->post('attn', TRUE));
		$penandaTangan		= $this->Anti_sql_injection($this->input->post('nama_ttd', TRUE));
		$total_qty			= $this->Anti_sql_injection($this->input->post('total_qty', TRUE));
		$total_net			= $this->Anti_sql_injection($this->input->post('total_net', TRUE));
		$total_gross		= $this->Anti_sql_injection($this->input->post('total_gross', TRUE));

		$data = array(
			'id_do'				=> $id_do,
			'date_end'			=> date('Y-m-d'),
			'no_invoice'		=> $no_invoice,
			'tanggal_invoice'	=> $tanggal_invoice,
			'due_date'			=> $tanggal_invoice,
			'sign'				=> $penandaTangan,
			'note'				=> NULL,
			'attn'				=> $attn
		);
		
		$get_inv = $this->do_model->invoice_search_id_do($id_do);
		
		if ($get_inv > 0) {
			
			$results_update = $this->do_model->update_report_invoice($data);
			
			$msg = 'Berhasil menambahkan data Report Packing dengan No Invoice : ' . $no_invoice;
			
			$results = array(
				'success' 		=> true,
				'message' 		=> $msg,
				'lastid' 		=> $get_inv[0]['id_invoice'],
				'id_eksternal' 	=> $id_eksternal,
				'total_qty'		=> $total_qty,
				'total_net'		=> $total_net,
				'total_gross'	=> $total_gross,
			);

			$this->log_activity->insert_activity('insert', $msg . ' dengan No Invoice : ' . $no_invoice);

		} else {
			$msg = 'Gagal menambahkan data Packing List ke database';

			$results = array(
				'success' => false,
				'message' => $msg
			);

			$this->log_activity->insert_activity('insert', $msg . ' dengan No Invoice : ' . $no_invoice);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}
	
	public function insert_detail_packing()
	{
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);
	
		if($params['id_eksternal'] == 138){
			foreach ($params['list_packing'] as $k => $v) {
				$arrTemp = array(
					'id_invoice'	=> $params['id_invoice'],
					'cnt_no'		=> $params['list_packing'][$k]['2'],
					'part_number'	=> $params['list_packing'][$k]['3'],
					'epson_code'	=> $params['list_packing'][$k]['5'],
					'no_po'			=> $params['list_packing'][$k]['6'],
					'qty'			=> preg_replace("/[^\d.-]/", "", $params['list_packing'][$k]['6']),
					'net'			=> preg_replace("/[^\d.-]/", "", $params['list_packing'][$k]['7']),
					'gross'			=> preg_replace("/[^\d.-]/", "", $params['list_packing'][$k]['8'])
				);

				$results = $this->do_model->insert_report_detail_packing($arrTemp);
			}
		}else{
			foreach ($params['list_packing'] as $k => $v) {
				$arrTemp = array(
					'id_invoice'	=> $params['id_invoice'],
					'cnt_no'		=> $params['list_packing'][$k]['2'],
					'part_number'	=> $params['list_packing'][$k]['5'],
					'epson_code'	=> NULL,
					'no_po'			=> $params['list_packing'][$k]['3'],
					'qty'			=> preg_replace("/[^\d.-]/", "", $params['list_packing'][$k]['6']),
					'net'			=> preg_replace("/[^\d.-]/", "", $params['list_packing'][$k]['7']),
					'gross'			=> preg_replace("/[^\d.-]/", "", $params['list_packing'][$k]['8'])
				);

				$results = $this->do_model->insert_report_detail_packing($arrTemp);
			}
		}
		
		if ($results > 0) {
			$msg = 'Berhasil menambahkan detail Report Packing List';

			$results = array(
				'success' => true,
				'message' => $msg
			);

			$this->log_activity->insert_activity('insert', $msg . ' dengan ID Invoice' . $arrTemp['id_invoice']);
		} else {
			$msg = 'Gagal menambahkan detail Report Packing List ke database';

			$results = array(
				'success' => false,
				'message' => $msg
			);

			$this->log_activity->insert_activity('insert', $msg . ' dengan ID Invoice ' . $arrTemp['id_invoice']);
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($results);
	}
	
	public function insert_delivery_order()
	{
		$no_do			= $this->Anti_sql_injection($this->input->post('no_do', TRUE));
		$tanggal_do		= $this->Anti_sql_injection($this->input->post('tanggal_do', TRUE));
		$remark 		= $this->Anti_sql_injection($this->input->post('remark', TRUE));
		$no_bc 			= $this->Anti_sql_injection($this->input->post('no_bc', TRUE));

		$data = array(
			'no_do'			=> $no_do,
			'tanggal_do'	=> $tanggal_do,
			'remark'		=> $remark,
			'id_schedule'	=> 0,
			'status'		=> 0,
			'no_bc'			=> $no_bc
		);

		$results = $this->do_model->insert_delivery_order($data);

		if ($results > 0) {
			$msg = 'Berhasil menambahkan data Delivery Order';

			$results = array(
				'success' => true,
				'message' => $msg,
				'lastid' => $results['lastid']
			);

			$this->log_activity->insert_activity('insert', $msg . ' dengan No Delivery ' . $data['no_do']);
		} else {
			$msg = 'Gagal menambahkan Delivery Order ke database';

			$results = array(
				'success' => false,
				'message' => $msg
			);

			$this->log_activity->insert_activity('insert', $msg . ' dengan No Delivery ' . $data['no_do']);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}

	public function insert_detail_delivery_order()
	{
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		foreach ($params['liststbj'] as $k => $v) {
			$arrTemp = array(
				'id_do'		=> $params['id_do'],
				'id_stbj'	=> $params['liststbj'][$k]['0'],
			);

			$results = $this->do_model->insert_detail_stbj($arrTemp);
			$results_update = $this->do_model->update_detail_box($arrTemp);
		}

		if ($results > 0) {
			$msg = 'Berhasil menambahkan detail Delivery Order';

			$results = array(
				'success' => true,
				'message' => $msg
			);

			$this->log_activity->insert_activity('insert', $msg . ' dengan ID DO ' . $arrTemp['id_do']);
		} else {
			$msg = 'Gagal menambahkan detail Delivery Order ke database';

			$results = array(
				'success' => false,
				'message' => $msg
			);

			$this->log_activity->insert_activity('insert', $msg . ' dengan ID DO ' . $arrTemp['id_do']);
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($results);
	}

	public function edit_delivery_order()
	{
		$this->form_validation->set_rules('id_do', 'Delivery Order', 'trim|required');
		$this->form_validation->set_rules('no_do', 'No Delivery Order', 'trim|required|min_length[4]|max_length[100]');
		$this->form_validation->set_rules('tanggal_do', 'Tanggal PO', 'trim|required');
		$this->form_validation->set_rules('detail_box', 'Detail Box', 'trim|required');
		$this->form_validation->set_rules('no_pi', 'No PI', 'trim|required');
		$this->form_validation->set_rules('remark', 'Remark', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			$id_do 			= $this->Anti_sql_injection($this->input->post('id_do', TRUE));
			$no_do 			= $this->Anti_sql_injection($this->input->post('no_do', TRUE));
			$tanggal_do		= $this->Anti_sql_injection($this->input->post('tanggal_do', TRUE));
			$detail_box		= $this->Anti_sql_injection($this->input->post('detail_box', TRUE));
			$no_pi 			= $this->Anti_sql_injection($this->input->post('no_pi', TRUE));
			$remark 		= $this->Anti_sql_injection($this->input->post('remark', TRUE));
			$no_bc 			= $this->Anti_sql_injection($this->input->post('no_bc', TRUE));

			$data = array(
				'id_do'			=> $id_do,
				'no_do'			=> $no_do,
				'tanggal_do'	=> $tanggal_do,
				'detail_box'	=> $detail_box,
				'no_pi'			=> $no_pi,
				'remark'		=> $remark,
				'id_schedule'	=> 0,
				'status'		=> 0,
				'no_bc'			=> $no_bc
			);

			$get_do = $this->do_model->do_list_stbj($id_do);

			if ($get_do[0]['tanggal'] != $tanggal_do || $get_do[0]['detail_box'] != $detail_box || $get_do[0]['no_pi'] != $no_pi || $get_do[0]['remark'] != $remark || $get_do[0]['no_bc'] != $no_bc) {
				$result = $this->do_model->insert_delivery_order_edit($data);

				$msg = 'Berhasil merubah data Delivery Order';

				$result = array(
					'success'	=> true,
					'message'	=> $msg,
					'no_do'		=> $no_do
				);

				$this->log_activity->insert_activity('update', $msg . ' dengan No Delivery ' . $no_do);
			} else {
				$msg = 'Gagal merubah data Delivery Order ke database';

				$result = array(
					'success' => false,
					'message' => $msg
				);

				$this->log_activity->insert_activity('update', $msg . ' dengan No Delivery ' . $no_do);
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}


	public function edit_detail_delivery_order()
	{
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data, true);
		
		$arrData = array();
		$statSave = false;
		foreach ($params['liststbj'] as $k => $v) {
			$this->db->delete('t_do_packing', array('id_do' => $params['id_do']));
			if ($v[0] != '') {

				$arrTemp = array(
					'id_do'		=> $params['id_do'],
					'id_stbj'	=> $v[0]
				);

				$results = $this->do_model->insert_detail_stbj($arrTemp);
				if ($results > 0) $statSave = true;
				else $statSave = false;
			}
		}

		if ($statSave == true) {
			$msg = 'Berhasil merubah detail Delivery Order';

			$result = array(
				'success'	=> true,
				'message'	=> $msg
			);

			$this->log_activity->insert_activity('update', $msg . ' dengan ID DO ' . $params['id_do']);
		} else {
			$msg = 'Gagal merubah detail Delivery Order ke database';

			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->log_activity->insert_activity('update', $msg . ' dengan ID DO ' . $params['id_do']);
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result);
	}

	public function delivery_list_invoice($id_do) {
		$list = $this->do_model->delivery_list_invoice($id_do);

		$data = array();
		$i = 0;
		$no = 0;
		$amount = 0;

		$username = $this->session->userdata['logged_in']['username'];
		$i = 0;
		$totalAmount = 0;
		foreach ($list as $k => $v) {
			$amount = (float) $v['qty_array'] * (float) $v['price_order'];
			$totalAmount = $totalAmount + $amount;

			if ($v['valas_id'] == 1) {
				$strPrice =
					'<div class="form-group">'.
						'<label class="control-label col-md-1 pull-left">'.$v['symbol'].'. </label>'.
						'<div class="col-md-11 pull-right">'.
							'<input type="number" class="form-control text-right" id="unit_price' . $i . '" name="unit_price' . $i . '" onInput="cal_price(' . $i . ')" onChange="redrawTable(' . $i . ')" placeholder="" value="' . $v['price_order'] . '" step=".01" style="">'.
							'<label id="unit_price-label' . $i . '" style="display:none;">' . number_format($v['price_order'], 2, '.', ',') . '</label>'.
						'</div>'.
					'</div>';

				$strAmount =
					'<div class="form-group">'.
						'<label class="control-label col-md-1 pull-left">'.$v['symbol'].'. </label>'.
						'<div class="col-md-11 pull-right">'.
							'<input type="number" class="form-control text-right" id="amount' . $i . '" name="amount[]" placeholder="" value="' . $amount . '" step=".01" style="" readonly>'.
							'<label id="amount-label' . $i . '" style="display:none;">' . number_format($amount, 2, '.', ',') . '</label>'.
						'</div>'.
					'</div>';

				$qty_array = number_format($v['qty_array'], 2, '.', '');
			} else {
				$strPrice =
					'<div class="form-group">'.
						'<label class="control-label col-md-1 pull-left">'.$v['symbol'].'. </label>'.
						'<div class="col-md-11 pull-right">'.
							'<input type="number" class="form-control text-right" id="unit_price' . $i . '" name="unit_price' . $i . '" onInput="cal_price(' . $i . ')" onChange="redrawTable(' . $i . ')" placeholder="" value="' . $this->numbering->format_unit_price($v['price_order'], $v['valas_id']) . '" step=".00001" style="">'.
							'<label id="unit_price-label' . $i . '" style="display:none;">' . $this->numbering->format_unit_price($v['price_order'], $v['valas_id']) . '</label>'.
						'</div>'.
					'</div>';

				$strAmount =
					'<div class="form-group">'.
						'<label class="control-label col-md-1 pull-left">'.$v['symbol'].'. </label>'.
						'<div class="col-md-11 pull-right">'.
							'<input type="number" class="form-control text-right" id="amount' . $i . '" name="amount[]" placeholder="" value="' . $this->numbering->format_amount($amount) . '" style="" readonly>'.
							'<label id="amount-label' . $i . '" style="display:none;">' . number_format($amount, 5, '.', ',') . '</label>'.
						'</div>'.
					'</div>';

				$qty_array = number_format($v['qty_array'], 5, '.', '');
			}

			if($v['id_eks'] == 136) {
				array_push($data, array(
					$i+1,
					$v['part_no'],
					$v['no_po'],
					$qty_array,
					$strPrice,
					$this->numbering->format_unit_price($v['price_order'], $v['valas_id']),
					$strAmount,
					$amount
				));
				$i++;
			}else{
				array_push($data, array(
					$v['no_do'],
					$v['part_no'],
					$v['no_po'],
					$qty_array,
					$strPrice,
					$this->numbering->format_unit_price($v['price_order'], $v['valas_id']),
					$strAmount,
					$amount
				));
				$i++;
			}
		}

		$result["data"] = $data;
		$result["total_amounts"] = $totalAmount;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function delivery_list_packing($id_do)
	{
		$list = $this->do_model->delivery_list_packing($id_do);
		$data = array();
		$i = 0;
		$no = 0;
		$amount = 0;

		$username = $this->session->userdata['logged_in']['username'];

		foreach ($list as $k => $v) {
			$cntNo = '<input type="text" class="form-control" id="no_kontainer' . $i . '" name="no_kontainer' . $i . '" value="" onInput="cal_price(' . $i . ')" onChange="redrawTable(' . $i . ')">';

			array_push($data, array(
				$cntNo,
				$v['stock_code'],
				$v['stock_name'],
				$v['qty_array'],
				$v['net_weight'],
				$v['gross_weight']
			));
			$i++;
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function delivery_order_update_status()
	{
		$data 	= file_get_contents("php://input");
		$params = json_decode($data, true);

		if ($params['status'] == 1) {
			$list = $this->do_model->delivery_update_status($params);

			$detail_order = $this->do_model->delivery_order_detail($params['id_do']);

			if (!empty($detail_order)) {
				$get_coa = $this->do_model->delivery_get_id_coa($detail_order[0]['cust_id']);
				$get_value = $this->do_model->delivery_get_value_piutang($params['id_do']);

				$data_coa = array(
					'id_coa' 		=> $get_coa[0]['id_coa'],
					'id_parent' 	=> 0,
					'date' 			=> date('Y-m-d'),
					'date_insert' 	=> date('Y-m-d H:i:s'),
					'id_valas' 		=> $detail_order[0]['valas_id'],
					'value' 		=> $get_value[0]['values'],
					'adjustment' 	=> 0,
					'type_cash' 	=> 0,
					'note' 			=> 'Piutang PO ' . $detail_order[0]['no_po'],
					'value_real' 	=> $get_value[0]['value_real'],
					'rate' 			=> $get_value[0]['rate'],
					'bukti' 		=> NULL
				);

				$ptr = explode(' ', $get_value[0]['price_term']);

				$statInsert_coa = false;
				$insert_coa = $this->do_model->delivery_order_insert_coa_values($data_coa);
				//if ($insert_coa['code'] > 1) { 
				$statInsert_coa = true;
				$data_hp = array(
					'tanggal' 		=> $data_coa['date'],
					'ref' 			=> NULL,
					'source' 		=> NULL,
					'keterangan' 	=> $data_coa['note'],
					'status' 		=> 0,
					'date_insert' 	=> $data_coa['date_insert'],
					'saldo' 		=> $data_coa['value_real'],
					'saldo_akhir' 	=> $data_coa['value_real'],
					'id_valas' 		=> $data_coa['id_valas'],
					'type_kartu' 	=> 1,
					'id_master' 	=>	$insert_coa['lastid'],
					'type_master' 	=> 1,
					'durasi'	 	=> $ptr[0]
				);

				$statInsert_hp = false;
				$insert_hp = $this->do_model->delivery_order_insert_kartu_hp2($data_hp);

				if ($detail_order[0]['eksternal_loc'] == 1) {
					$coa_jual = 461;
				} else {
					$coa_jual = 462;
				}

				$data_coa2 = array(
					'id_coa' 		=> $coa_jual,
					'id_parent' 	=> 0,
					'date' 			=> date('Y-m-d'),
					'date_insert' 	=> date('Y-m-d H:i:s'),
					'id_valas' 		=> $detail_order[0]['valas_id'],
					'value' 		=> $get_value[0]['values'],
					'adjustment' 	=> 0,
					'type_cash' 	=> 1,
					'note' 			=> 'Penjualan PO ' . $detail_order[0]['no_po'],
					'value_real' 	=> $get_value[0]['value_real'],
					'rate' 			=> $get_value[0]['rate'],
					'bukti' 		=> NULL
				);

				$ptr = explode(' ', $get_value[0]['price_term']);

				//$statInsert_coa = false;
				$insert_coa = $this->do_model->delivery_order_insert_coa_values($data_coa2);

				if ($insert_hp > 0) $statInsert_hp = true;
				//}

				if ($statInsert_coa == true && $statInsert_hp == true) {
					$msg = 'Data status Delivery Order berhasil di ubah';
					$this->log_activity->insert_activity('update', $msg . ' dengan No Delivery ' . $detail_order[0]['no_do']);
				} else {
					$msg = 'Data status Delivery Order gagal di ubah';
					$this->log_activity->insert_activity('update', $msg . ' dengan No Delivery ' . $detail_order[0]['no_do']);
				}
			} else {
				$msg = 'Data status COA Value dan Kartu HP gagal disimpan ke database';

				$results = array('success' => false, 'status' => 'error', 'message' => $msg);
				$this->log_activity->insert_activity('update', $msg . ' dengan No Delivery ' . $detail_order[0]['no_do']);
			}

			$results = array(
				'success' => true,
				'status' => 'success',
				'value' => 1,
				'message' => $msg
			);

			$this->log_activity->insert_activity('update', $msg . ' dengan No Delivery ' . $detail_order[0]['no_do']);
		} elseif ($params['status'] == 2) {
			$list = $this->do_model->delivery_update_status($params);
			$msg = 'Data status Delivery Order berhasil di ubah';

			$results = array(
				'success' => true,
				'status' => 'success',
				'value' => 2,
				'message' => $msg
			);

			$this->log_activity->insert_activity('update', $msg . ' dengan ID Delivery ' . $params['id_do']);
		} elseif ($params['status'] == 3) {
			$msg = 'Data status Delivery Order ditolak silahkan hubungi admin';

			$results = array(
				'success' => true,
				'status' => 'success',
				'value' => 3,
				'message' => $msg
			);

			$this->log_activity->insert_activity('update', $msg . ' dengan ID Delivery ' . $params['id_do']);
		} else {
			$msg = 'Data status Delivery Order belum di ubah';

			$results = array(
				'success' => false,
				'status' => 'success',
				'value' => 1,
				'message' => $msg
			);
			$this->log_activity->insert_activity('update', $msg . ' dengan ID Delivery ' . $params['id_do']);
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($results);
	}

	public function delivery_detail_order_delete()
	{

		$data 	= file_get_contents("php://input");
		$params = json_decode($data, true);

		$list = $this->do_model->delivery_detail_order_delete($params);

		$msg = 'Data detail delivery order berhasil di hapus';

		$results = array(
			'success' => true,
			'status' => 'success',
			'message' => $msg
		);

		$this->log_activity->insert_activity('delete', $msg . ' dengan ID DO ' . $params['id_do']);

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($results);
	}

	public function check_no_do()
	{
		$this->form_validation->set_rules('no_do', 'NoDo', 'trim|required|min_length[4]|max_length[20]|is_unique[t_delivery_order.no_do]');
		$this->form_validation->set_message('is_unique', 'No DO Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'No DO Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}
}
