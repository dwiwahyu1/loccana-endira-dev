<style type="text/css">
	.dt-body-center {
		text-align: center;
	}
	.dt-body-right {
		text-align: right;
	}
</style>

<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Valas</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<table id="listpemohon" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="width: 5%;">No</th>
							<th>Nama Valas</th>
							<th>Simbol</th>
							<th>Option</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
		function add_valas(){
			$('#panel-modal').removeData('bs.modal');
			$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
			$('#panel-modal  .panel-body').load('<?php echo base_url('valas/add');?>');
			$('#panel-modal  .panel-title').html('<i class="fa fa-dollar"></i> Add Valas');
			$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
		}

		function editvalas(id){
			$('#panel-modal').removeData('bs.modal');
			$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
			$('#panel-modal  .panel-body').load('<?php echo base_url('valas/edit/');?>'+"/"+id);
			$('#panel-modal  .panel-title').html('<i class="fa fa-dollar"></i> Edit Valas');
			$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
		}
		
	function deletevalas(id){
		swal({
			title: 'Yakin akan Menghapus ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost = {"id" : id};
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>valas/deletes",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						window.location.href = "<?php echo base_url('valas');?>";
					})
				  if (response.status == "success") {
				  }else{
					swal("Failed!", response.message, "error");
				  }
				}
			  });
		})
	}

	$(document).ready(function(){
		$("#listpemohon").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'valas/lists/';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip',
			"initComplete": function(){
				$("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_valas()"><i class="fa fa-dollar"></i> Tambah Valas</a></div>');
			},
			"columnDefs": [{
				targets: [0],
				className: 'dt-body-center'
			}]
		});
	});
</script>
