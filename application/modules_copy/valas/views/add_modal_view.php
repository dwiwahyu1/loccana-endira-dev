    <!--Parsley-->
    <script type="text/javascript" src="<?php echo base_url('assets'); ?>/gentelella-master/vendors/parsleyjs/dist/parsley.min.js"></script>

	<style>
	#loading-us{display:none}
	#tick{display:none}

	#loading-mail{display:none}
	#cross{display:none}
	</style>

                    <form class="form-horizontal form-label-left" id="add_valas" role="form" action="<?php echo base_url('valas/add_valas');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

                      <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Valas <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input data-parsley-maxlength="255" type="text" id="nama_valas" name="nama_valas" class="form-control col-md-7 col-xs-12" placeholder="Valas" required="required">
						</div>
                      </div>
					  
					  
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Simbol Valas <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input data-parsley-maxlength="255" type="text" id="symbol_valas" name="symbol_valas" class="form-control col-md-7 col-xs-12" placeholder="Simbol Valas" required="required">
						</div>
                      </div>


                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Valas</button>
                        </div>
                      </div>

                    </form>

        <!-- /page content -->

<script type="text/javascript">
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
	});

var lastusername = $('#username').val();

$('#username').on('input',function(event) {
   if($('#username').val() != lastusername) {
       username_check();
   }
});

function username_check(){
    var username = $('#username').val();
    if(username.length > 3) {
        var post_data = {
          'username': username
        };

    	$('#tick').empty();
    	$('#tick').hide();
    	$('#loading-us').show();
    	jQuery.ajax({
    	   type: "POST",
    	   url: "<?php echo base_url('users/check_username');?>",
    	   data: post_data,
    	   cache: false,
    	   success: function(response){
    			if(response.success == true){
    				$('#username').css('border', '3px #090 solid');
    				$('#loading-us').hide();
    				$('#tick').empty();
    				$("#tick").append('<span class="fa fa-check"> '+response.message+'</span>');
    				$('#tick').show();
    				}else{
    				$('#username').css('border', '3px #C33 solid');
    				$('#loading-us').hide();
    				$('#tick').empty();
    				$("#tick").append('<span class="fa fa-close"> '+response.message+'</span>');
    				$('#tick').show();
    			}
    		}
    	});
    } else {
        $('#username').css('border', '3px #C33 solid');
        $('#loading-us').hide();
        $('#tick').empty();
        $("#tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
        $('#tick').show();
    }
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

var lastemail = $('#email').val();

$('#email').on('input',function(event) {
   if($('#email').val() != lastemail) {
       email_check();
   }
});

function email_check() {
    var email = $("#email").val();

    $('#cross').empty();
    $('#cross').hide();
    $('#loading-mail').show();

    if (validateEmail(email)) {
        var post_data = {
          'email': email
        };

        jQuery.ajax({
    	   type: "POST",
    	   url: "<?php echo base_url('users/check_email');?>",
    	   data: post_data,
    	   cache: false,
    	   success: function(response){
    			if(response.success == true){
    				$('#email').css('border', '3px #090 solid');
    				$('#loading-mail').hide();
    				$('#cross').empty();
    				$("#cross").append('<span class="fa fa-check"> '+response.message+'</span>');
    				$('#cross').show();
    				}else{
    				$('#email').css('border', '3px #C33 solid');
    				$('#loading-mail').hide();
    				$('#cross').empty();
    				$("#cross").append('<span class="fa fa-close"> '+response.message+'</span>');
    				$('#cross').show();
    			}
    		}
    	});
    } else {
        $('#email').css('border', '3px #C33 solid');
        $('#loading-mail').hide();
        $('#cross').empty();
        $("#cross").append('<span class="fa fa-close"> Not valid email.</span>');
        $('#cross').show();
    }
    return false;
}

$('#add_valas').on('submit',(function(e) {
    $('#btn-submit').attr('disabled','disabled');
    $('#btn-submit').text("Memasukkan data...");
    e.preventDefault();
    var formData = new FormData(this);

    $.ajax({
        type:'POST',
        url: $(this).attr('action'),
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(response) {
            if (response.success == true) {
                swal({
                  title: 'Success!',
                  text: response.message,
                  type: 'success',
                  showCancelButton: false,
                  confirmButtonText: 'Ok'
                }).then(function () {
                  window.location.href = "<?php echo base_url('valas');?>";
                })
            } else{
                $('#btn-submit').removeAttr('disabled');
                $('#btn-submit').text("Tambah User");
                swal("Failed!", response.message, "error");
            }
        }
    }).fail(function(xhr, status, message) {
        $('#btn-submit').removeAttr('disabled');
        $('#btn-submit').text("Tambah Valas");
        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
    });
}));
</script>
