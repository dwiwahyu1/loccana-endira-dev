<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Bpp extends MX_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('bpp/bpp_model');
		$this->load->model('jurnal_np/jurnal_np_model');
		$this->load->model('jurnal/jurnal_model');
		$this->load->library('log_activity');
		$this->load->library('sequence');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'bpp/views/index');
	}
	
	public function get_invoice_param() {
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data, true);
		
		$delete_jurnal = $this->bpp_model->get_invoice_param($params['id']);
		
		if($delete_jurnal > 0){
			//$this->log_activity->insert_activity('insert', 'Berhasil hapus Data Jurnal Non Produksi ID : ' . $params['id']);
			$res = array('status' => 'success', 'message' => 'Data jurnal berhasil di hapus');
		}else{
			//$this->log_activity->insert_activity('insert', 'Gagal hapus data Jurnal Non Produksi ID : ' . $params['id']);
			$res = array('status' => 'failed', 'message' => 'Data jurnal gagal di hapus');
		}
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($delete_jurnal);
	}

	function lists_bpp() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'ASC';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

		$order_fields = array('', 'nama');
		$search = $this->input->get_post('search');
		$search_val = (!empty($search['value'])) ? $search['value'] : null;
		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		if ($params['limit'] < 0) $params['limit'] = $this->payment_voucher_kas_model->count_payment($params);
		$list = $this->bpp_model->lists_bpp($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = $params['offset'] + 1;
		foreach ($list['data'] as $k => $v) {
			if($v['status'] == 0) {
			$strBtn =
				'<div class="btn-group">' .
					'<a class="btn btn-primary btn-sm" title="Detail" onClick="detail_bpp(\'' . $v['id_bpp'] . '\')">' .
						'<i class="fa fa-search"></i>' .
					'</a>' .
				'</div>' .
				'<div class="btn-group">' .
					'<a class="btn btn-success btn-sm" title="Update" onClick="edit_bpp(\'' . $v['id_bpp'] . '\')">' .
						'<i class="fa fa-pencil"></i>' .
					'</a>' .
				'</div>' .
				'<div class="btn-group">' .
					'<a class="btn btn-danger btn-sm" title="Delete" onClick="delete_bpp_temporary(\'' . $v['id_bpp'] . '\')">' .
						'<i class="fa fa-trash"></i>' .
					'</a>' .
				'</div>'.
				'<div class="btn-group">' .
					'<button class="btn btn-success btn-sm" type="button" title="Approval" onClick="approve_bpp(\'' . $v['id_bpp'] . '\')">' .
						'<i class="fa fa-check"></i>' .
					'</button>' .
				'</div>';
			}else {
				$strBtn =
				'<div class="btn-group">' .
					'<a class="btn btn-primary btn-sm" title="Detail" onClick="detail_bpp(\'' . $v['id_bpp'] . '\')">' .
						'<i class="fa fa-search"></i>' .
					'</a>' .
				'</div>';
			}
			
			array_push($data, array(
				$i++,
				$v['no_bpp'],
				$v['nama'],
				date('d M Y', strtotime($v['tanggal_bpp'])),
				$v['name_eksternal'],
				// $v['debit_detail'],
				$v['symbol_valas'] . ' ' . number_format(round($v['total_piutang']), 0, ',', '.'),
				// $v['kredit_detail'],
				$v['symbol_valas'] . ' ' . number_format(round($v['total_coa']), 0, ',', '.'),
				$strBtn
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function add_jurnal() {
		$list_bank = $this->bpp_model->list_bank();
		$coa_all = $this->bpp_model->coa_list();
		$valas = $this->jurnal_model->valas();
		$invoice = $this->bpp_model->invoice();
		$data = array(
			'list_bank' => $list_bank,
			'coa_all' =>$coa_all,
			'valas'=>$valas,
			'invoice' => $invoice
		);
		
		$this->load->view('add_modal_bpp_view', $data);
	}
	
	public function edit_bpp($id) {
		$list_bank 		= $this->bpp_model->list_bank();
		$coa_all 		= $this->bpp_model->coa_list();
		$valas 			= $this->jurnal_model->valas();
		$invoice 		= $this->bpp_model->invoice();

		$result_bpp 	= $this->bpp_model->detail_bpp($id);

		/*$data_list = array(
			'id_bpp'	=> $id,
			'type_bpp'	=> $type_bpp
		);

		$list_piutang = $this->bpp_model->detail_lists_bpp($data_list);*/
		
		$data = array(
			'bpp'			=> $result_bpp,
			'list_bank' 	=> $list_bank,
			'coa_all'		=> $coa_all,
			'valas'			=> $valas,
			'invoice'		=> $invoice
		);
		
		$this->load->view('edit_modal_bpp_view', $data);
	}

	function edit_detail_lists_bpp($id_bpp, $type_bpp) {
		// $list_bank 		= $this->bpp_model->list_bank();
		$coa_all 		= $this->bpp_model->coa_list();
		$valas 			= $this->jurnal_model->valas();
		$invoice 		= $this->bpp_model->invoice();

		$data_list = array(
			'id_bpp'	=> $id_bpp,
			'type_bpp'	=> $type_bpp
		);
		$list_piutang = $this->bpp_model->detail_lists_bpp($data_list);
		
		$no = 1;
		$data = array();
		if($type_bpp == 1) {
			$list_piutang = $this->bpp_model->detail_lists_bpp($data_list);

			$no = 0;
			foreach ($list_piutang as $lpk => $lpv) {
				$no++;
				$strBtn =
					'<a class="btn btn-icon waves-effect waves-light btn-danger">'.
						'<i class="fa fa-trash"></i>'.
					'</a>';

				$strInvoice =
					'<select onchange="changeCoaMasuk('.$no.')" class="form-control ddl_coa_masuk" id="ddl_coa_masuk'.$no.'" name="ddl_coa_masuk[]"'.
						'style="width: 100%" required>';
				foreach ($invoice as $key) {
					if($key['id_invoice'] == $lpv['id_invoice']) $strSelected = 'selected';
					else $strSelected = '';
					$strInvoice .= "<option value=\"".$key['id_invoice']."\" ".$strSelected.">".$key['no_invoice']." - ".$key['name_eksternal']."</option>";
				}
				$strInvoice .=
					'</select>';

				$strValas =
					'<select  class="form-control ddl_valas_masuk" id="ddl_valas_masuk'.$no.'" name="ddl_valas_masuk[]" style="width: 100%" required>';
				foreach ($valas as $key) {
					if($key['valas_id'] == $lpv['valas_id']) $strSelected = 'selected';
					else $strSelected = '';
					$strValas .= '<option value="'.$key['valas_id'].'">'.$key['nama_valas'].'</option>';
				}
				$strValas .=
					'</select>';

				$strRate =
					'<input type="number" class="form-control" id="rate'.$no.'" name="rate[]" step=".0001"'.
						'value="'.number_format($lpv['rate'], 0, '.', '').'" placeholder="Rate" autocomplete="off">';

				$strNilai =
					'<input type="number" onKeyup="calculateValueMasuk()" class="form-control" id="value_kas_masuk'.$no.'" name="value_kas_masuk[]"'.
						'step=".0001" value="'.number_format($lpv['VALUE'], 0, '.', '').'" placeholder="Nominal" autocomplete="off">';

				array_push($data, array(
					$lpv['id_bpp'],
					$lpv['id_detail_bpp'],
					'',
					$strInvoice,
					$strValas,
					$strRate,
					$strNilai,
					$strBtn
				));
			}
		}else {
			$list_coa = $this->bpp_model->detail_lists_bpp_coa($data_list);

			$no = 0;
			foreach ($list_coa as $n => $o) {
				if($o['type_cash'] == 0) $status = 'Debit';
				else $status = 'Kredit';
				$strBtn =
					'<a class="btn btn-icon waves-effect waves-light btn-danger">'.
						'<i class="fa fa-trash"></i>'.
					'</a>'.
					'<input type="hidden" id="rate_keluar'.$no.'" name="rate_keluar[]" value="'.$o['rate'].'">'.
					'<input type="hidden" id="type_cash'.$no.'" name="type_cash[]" value="'.$o['type_cash'].'">';

				$strPerkiraan =
					'<select onchange="changeCoaKeluar('.$no.')" class="form-control ddl_coa_keluar" id="ddl_coa_keluar'.$no.'" name="ddl_coa_keluar[]"'.
						'style="width: 100%" required>';
				foreach ($coa_all as $key) {
					if($key['id_coa'] == $o['id_invoice']) $strSelected = 'selected';
					else $strSelected = '';
					$strPerkiraan .= "<option value=\"".$key['id_coa']."\" ".$strSelected.">".$key['coa']."-".$key['keterangan']."</option>";
				}
				$strPerkiraan .=
					'</select>';

				$strCoa =
					'<input type="text" class="form-control" id="coa_number_keluar'.$no.'" name="coa_number_keluar[]" value="'.$o['coa'].'"'.
						'placeholder="Coa Number Masuk" autocomplete="off" readonly>';

				if($o['note'] != '' && $o['note'] != NULL && $o['note'] != 'null') $keterangan = $o['note'];
				else $keterangan = '';
				$strKeterangan =
					'<input type="text" class="form-control" id="note_keluar'.$no.'" name="note_keluar[]" value="'.$keterangan.'"'.
					'placeholder="Note" autocomplete="off">';

				$strValas =
					'<select  class="form-control ddl_valas_keluar" id="ddl_valas_keluar'.$no.'" name="ddl_valas_keluar[]" style="width: 100%" required>';
				foreach ($valas as $key) {
					if($key['valas_id'] == $o['valas_id']) $strSelected = 'selected';
					else $strSelected = '';
					$strValas .= '<option value="'.$key['valas_id'].'">'.$key['nama_valas'].'</option>';
				}
				$strValas .=
					'</select>';

				$strNilai =
					'<input type="number" class="form-control" id="value_kas_keluar'.$no.'" name="value_kas_keluar[]"'.
						'value="'.number_format($o['VALUE'], 0, '.', '').'" placeholder="Nominal" autocomplete="off">';
					
				array_push($data, array(
					$o['id_bpp'],
					$o['id_detail_bpp'],
					'',
					$strPerkiraan,
					$strCoa,
					$strKeterangan,
					$strValas,
					$strNilai,
					$status,
					$strBtn
				));
			}
		}
		
		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function change_coa()
	{
		$id = $this->input->get('id');
		$result = $this->jurnal_model->coa("id_parent", $id);

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function add_coa_value_masuk() {
		$coa_all = $this->bpp_model->coa_list();

		$valas = $this->jurnal_model->valas();
		$invoice = $this->bpp_model->invoice();

		$data = array(
			'coa_all' => $coa_all,
			'invoice' => $invoice,
			'valas' => $valas
		);

		$this->load->view('add_modal_kas_masuk_view', $data);
	}
	
	public function add_coa_value_keluar() {
		$coa_all = $this->bpp_model->coa_list();

		$valas = $this->jurnal_model->valas();

		$data = array(
			'coa_all' => $coa_all,
			'valas' => $valas
		);

		$this->load->view('add_modal_kas_keluar_view', $data);
	}
	
	public function edit_coa_value_masuk() {
		$coa_all = $this->bpp_model->coa_list();

		$valas = $this->jurnal_model->valas();
		$invoice = $this->bpp_model->invoice();

		$data = array(
			'coa_all'	=> $coa_all,
			'invoice'	=> $invoice,
			'valas' => $valas
		);

		$this->load->view('edit_modal_kas_masuk_view', $data);
	}
	
	public function edit_coa_value_keluar() {
		$coa_all = $this->bpp_model->coa_list();
		$valas = $this->jurnal_model->valas();

		$data = array(
			'coa_all' => $coa_all,
			'valas' => $valas
		);

		$this->load->view('edit_modal_kas_keluar_view', $data);
	}

	public function save_bpp() {
		$user_id 			= $this->session->userdata['logged_in']['user_id'];
		$no_bpp 			= ($this->input->post('no_bpp', true));
		$tgl_bpvk 			= ($this->input->post('tgl_bpvk', true));
		$id_customer 		= ($this->input->post('id_customer', true));
		
		//Start Variable Item Masuk
		$arrCoaMasuk		= $this->input->post('arrCoaMasuk', TRUE);
		$note_masuk			= $this->input->post('note_masuk', TRUE);
		$arrValasMasuk		= $this->input->post('arrValasMasuk', TRUE);
		$arrRateMasuk		= $this->input->post('arrRateMasuk', TRUE);
		$arrValueKasMasuk	= $this->input->post('arrValueKasMasuk', TRUE);
		//End Variable Item Masuk

		//Start Variable Item Keluar
		$arrCoaKeluar		= $this->input->post('arrCoaKeluar', TRUE);
		$id_parent_keluar	= $this->input->post('id_parent_keluar', TRUE);
		$arrCoaKeluar		= $this->input->post('arrCoaKeluar', TRUE);
		$arrNotesKeluar		= $this->input->post('arrNotesKeluar', TRUE);
		$arrValasKeluar		= $this->input->post('arrValasKeluar', TRUE);
		$type_cash_keluar	= $this->input->post('type_cash_keluar', TRUE);
		$rate_keluar		= $this->input->post('rate_keluar', TRUE);
		$arrValueKasKeluar	= $this->input->post('arrValueKasKeluar', TRUE);
		//End Variable Item Keluar

		$get_coa_customer	= $this->bpp_model->get_coa_eks($id_customer);

		$date = date_create($tgl_bpvk);
		if(sizeof($get_coa_customer) > 0) {
			$data_bpp = array(
				'no_bpp'		=> $no_bpp,
				'tanggal_bpp'	=> date_format($date, 'Y-m-d H:i:s'),
				'status'		=> 0,
				'id_customer'	=> $id_customer,
				'pic'			=> $user_id
			);
			
			$result_bpp = $this->bpp_model->add_bpp($data_bpp);
			$id_bpp = $result_bpp['lastid'];
			
			if($result_bpp['result'] > 0) {
				$this->log_activity->insert_activity('insert', 'Insert BPP Tanggal : ' . $data_bpp['tanggal_bpp']);
				
				for($i = 0; $i < sizeof($arrCoaMasuk); $i++) {
					$data_coa = array(
						'id_coa' 		=> $get_coa_customer[0]['id_coa'],
						'id_parent' 	=> $get_coa_customer[0]['id_parent'],
						'date' 			=> $tgl_bpvk,
						'id_valas' 		=> $arrValasMasuk[$i],
						'value' 		=> $arrValueKasMasuk[$i],
						'adjusment' 	=> 0,
						'type_cash' 	=> 1,
						'note' 			=> $note_masuk[$i],
						'rate' 			=> $arrRateMasuk[$i],
						'bukti' 		=> NULL,
						'pic' 			=> $user_id,
						'pic_approve' 	=> NULL,
						'status' 		=> 0,
						'type_bpp'		=> 1
					);
					
					$result_coa = $this->bpp_model->add_coa_value_temporary($data_coa);
					if($result_coa['result'] > 0) {
						$this->log_activity->insert_activity('insert', 'Insert Jurnal Coa Value Temporary ID Coa : '. $get_coa_customer[0]['id_coa']);

						$data_d_bpp = array(
							'id_bpp'			=> $id_bpp,
							'id_invoice'		=> $arrCoaMasuk[$i],
							'value'				=> $arrValasMasuk[$i],
							'valas_id'			=> $arrValasMasuk[$i],
							'rate'				=> $arrRateMasuk[$i],
							'value_real'		=> floatval($arrValasMasuk[$i]) * floatval($arrRateMasuk[$i]),
							'id_coa_val_temp'	=> $result_coa['lastid'],
							'id_coa_val'		=> 0,
							'type_bpp'			=> 1
						);
						$result_d_bpp = $this->bpp_model->add_d_bpp($data_d_bpp);

						if($result_d_bpp['result'] > 0) $this->log_activity->insert_activity('insert', 'Insert Detail BPP ID BPP : ' . $id_bpp);
						else $this->log_activity->insert_activity('insert', 'Gagal Insert Detail BPP ID : ' . $id_bpp);
					}else $this->log_activity->insert_activity('insert', 'Gagal Insert Coa Value Temporary ID Coa : '. $get_coa_customer[0]['id_coa']);
				}
				
				for($i = 0; $i < sizeof($arrCoaKeluar); $i++) {
					$data_coa = array(
						'id_coa' 		=> $arrCoaKeluar[$i],
						'id_parent' 	=> $id_parent_keluar[$i],
						'date' 			=> $tgl_bpvk,
						'id_valas' 		=> $arrValasKeluar[$i],
						'value' 		=> $arrValueKasKeluar[$i],
						'adjusment' 	=> 0,
						'type_cash' 	=> $type_cash_keluar[$i],
						'note' 			=> $arrNotesKeluar[$i],
						'rate' 			=> $rate_keluar[$i],
						'bukti' 		=> NULL,
						'pic' 			=> $user_id,
						'pic_approve' 	=> NULL,
						'status' 		=> 0,
						'type_bpp'		=> 0
					);
					
					$result_coa = $this->bpp_model->add_coa_value_temporary($data_coa);
					if($result_coa['result'] > 0) {
						$this->log_activity->insert_activity('insert', 'Insert Jurnal Coa Value Temporary ID Coa : '. $get_coa_customer[0]['id_coa']);

						$data_d_bpp = array(
							'id_bpp'			=> $id_bpp,
							'id_invoice'		=> $arrCoaKeluar[$i],
							'value'				=> $arrValueKasKeluar[$i],
							'valas_id'			=> $arrValasKeluar[$i],
							'rate'				=> $rate_keluar[$i],
							'value_real'		=> floatval($arrValueKasKeluar[$i]) * floatval($rate_keluar[$i]),
							'id_coa_val_temp'	=> $result_coa['lastid'],
							'id_coa_val'		=> 0,
							'type_bpp'			=> 0
						);
						$result_d_bpp = $this->bpp_model->add_d_bpp($data_d_bpp);

						if($result_d_bpp['result'] > 0) $this->log_activity->insert_activity('insert', 'Insert Detail BPP ID BPP : ' . $id_bpp);
						else $this->log_activity->insert_activity('insert', 'Gagal Insert Detail BPP ID : ' . $id_bpp);
					}else $this->log_activity->insert_activity('insert', 'Gagal Insert Coa Value Temporary ID Coa : '. $get_coa_customer[0]['id_coa']);
				}
				
				$results = array('success' => true, 'message' => 'Berhasil menambahkan Jurnal Non Produksi ke database');
			}else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert Jurnal Non Produksi tanggal ' . $tgl_bpv);
				$results = array('success' => false, 'message' => 'Gagal Jurnal Coa Value ke database');
			}
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}

	public function save_edit_bpp() {
		/*echo "<pre>";
		print_r($this->input->post());
		echo "</pre>";
		die;*/

		$user_id 		= $this->session->userdata['logged_in']['user_id'];
		$id_bpp 		= ($this->input->post('id_bpp', true));
		$no_bpp 		= ($this->input->post('no_bpp', true));
		$id_customer 	= ($this->input->post('customer', true));
		$tanggal_bpp 	= ($this->input->post('tanggal_bpp', true));

		//Start Variable Item Masuk
		$arrIdBppMasuk		= $this->input->post('arrIdBppMasuk', TRUE);
		$arrIdDBppMasuk		= $this->input->post('arrIdDBppMasuk', TRUE);
		$arrCoaMasuk		= $this->input->post('arrCoaMasuk', TRUE);
		$arrNoteMasuk		= $this->input->post('arrNoteMasuk', TRUE);
		$arrValasMasuk		= $this->input->post('arrValasMasuk', TRUE);
		$arrRateMasuk		= $this->input->post('arrRateMasuk', TRUE);
		$arrValueKasMasuk	= $this->input->post('arrValueKasMasuk', TRUE);
		//End Variable Item Masuk

		//Start Variable Item Keluar
		$arrIdBppKeluar		= $this->input->post('arrIdBppKeluar', TRUE);
		$arrIdDBppKeluar	= $this->input->post('arrIdDBppKeluar', TRUE);
		$arrCoaKeluar		= $this->input->post('arrCoaKeluar', TRUE);
		$arrNoteKeluar		= $this->input->post('arrNoteKeluar', TRUE);
		$arrValasKeluar		= $this->input->post('arrValasKeluar', TRUE);
		$arrRateKeluar		= $this->input->post('arrRateKeluar', TRUE);
		$arrtypeCashKeluar	= $this->input->post('arrtypeCashKeluar', TRUE);
		$arrValueKasKeluar	= $this->input->post('arrValueKasKeluar', TRUE);
		//End Variable Item Keluar

		// if( (sizeof($arrIdBppMasuk) == sizeof($arrIdBppKeluar)) && sizeof($arrIdBppMasuk) > 0 && sizeof($arrIdBppKeluar) > 0 ) {
			$get_coa_customer	= $this->bpp_model->get_coa_eks($id_customer);
			
			//Update BPP
			$data_bpp = array(
				'id_bpp'		=> $id_bpp,
				'no_bpp'		=> $no_bpp,
				'tanggal_bpp'	=> date('Y-m-d', strtotime($tanggal_bpp)),
				'customer'		=> $id_customer,
				'pic'			=> $user_id
			);
			$result_bpp = $this->bpp_model->edit_bpp($data_bpp);
			$result_del_detail = $this->bpp_model->bpp_temp_delete($id_bpp);

			//CRUD ITEM MASUK
			for ($i=0; $i < sizeof($arrIdDBppMasuk); $i++) {
				if($arrNoteMasuk[$i] != '' && $arrNoteMasuk[$i] != 'null' && $arrNoteMasuk[$i] != NULL) $strNote = $arrNoteMasuk[$i];
				else $strNote = NULL;

				$data_coa = array(
					'id_coa' 		=> $get_coa_customer[0]['id_coa'],
					'id_parent' 	=> $get_coa_customer[0]['id_parent'],
					'date' 			=> $tanggal_bpp,
					'id_valas' 		=> $arrValasMasuk[$i],
					'value' 		=> $arrValueKasMasuk[$i],
					'adjusment' 	=> 0,
					'type_cash' 	=> 1,
					'note' 			=> $strNote,
					'rate' 			=> $arrRateMasuk[$i],
					'bukti' 		=> NULL,
					'pic' 			=> $user_id,
					'pic_approve' 	=> NULL,
					'status' 		=> 0,
					'type_bpp'		=> 1
				);
				$result_coa = $this->bpp_model->add_coa_value_temporary($data_coa);

				if($result_coa['result'] > 0) {
					$this->log_activity->insert_activity('insert', 'Insert Jurnal Coa Value Temporary ID Coa : '. $get_coa_customer[0]['id_coa']);

					$data_d_bpp = array(
						'id_bpp'			=> $id_bpp,
						'id_invoice'		=> $arrCoaMasuk[$i],
						'value'				=> $arrValueKasMasuk[$i],
						'valas_id'			=> $arrValasMasuk[$i],
						'rate'				=> $arrRateMasuk[$i],
						'value_real'		=> floatval($arrValueKasMasuk[$i]) * floatval($arrRateMasuk[$i]),
						'id_coa_val_temp'	=> $result_coa['lastid'],
						'id_coa_val'		=> 0,
						'type_bpp'			=> 1
					);
					$result_d_bpp = $this->bpp_model->add_d_bpp($data_d_bpp);

					if($result_d_bpp['result'] > 0) $this->log_activity->insert_activity('insert', 'Insert Detail BPP ID BPP : ' . $id_bpp);
					else $this->log_activity->insert_activity('insert', 'Gagal Insert Detail BPP ID : ' . $id_bpp);
				}else $this->log_activity->insert_activity('insert', 'Gagal Insert Coa Value Temporary ID Coa : '. $get_coa_customer[0]['id_coa']);
			}

			//CRUD ITEM KELUAR
			for ($i=0; $i < sizeof($arrIdDBppKeluar); $i++) {
				if($arrNoteKeluar[$i] != '' && $arrNoteKeluar[$i] != 'null' && $arrNoteKeluar[$i] != NULL) $strNote = $arrNoteKeluar[$i];
				else $strNote = NULL;

				$data_coa = array(
					'id_coa' 		=> $arrCoaKeluar[$i],
					'id_parent' 	=> $arrCoaKeluar[$i],
					'date' 			=> $tanggal_bpp,
					'id_valas' 		=> $arrValasKeluar[$i],
					'value' 		=> $arrValueKasKeluar[$i],
					'adjusment' 	=> 0,
					'type_cash' 	=> $arrtypeCashKeluar[$i],
					'note' 			=> $strNote,
					'rate' 			=> $arrRateKeluar[$i],
					'bukti' 		=> NULL,
					'pic' 			=> $user_id,
					'pic_approve' 	=> NULL,
					'status' 		=> 0,
					'type_bpp'		=> 0
				);
				
				$result_coa = $this->bpp_model->add_coa_value_temporary($data_coa);
				if($result_coa['result'] > 0) {
					$this->log_activity->insert_activity('insert', 'Insert Jurnal Coa Value Temporary ID Coa : '. $get_coa_customer[0]['id_coa']);

					$data_d_bpp = array(
						'id_bpp'			=> $id_bpp,
						'id_invoice'		=> $arrCoaKeluar[$i],
						'value'				=> $arrValueKasKeluar[$i],
						'valas_id'			=> $arrValasKeluar[$i],
						'rate'				=> $arrRateKeluar[$i],
						'value_real'		=> floatval($arrValueKasKeluar[$i]) * floatval($arrRateKeluar[$i]),
						'id_coa_val_temp'	=> $result_coa['lastid'],
						'id_coa_val'		=> 0,
						'type_bpp'			=> 0
					);
					$result_d_bpp = $this->bpp_model->add_d_bpp($data_d_bpp);

					if($result_d_bpp['result'] > 0) $this->log_activity->insert_activity('insert', 'Insert Detail BPP ID BPP : ' . $id_bpp);
					else $this->log_activity->insert_activity('insert', 'Gagal Insert Detail BPP ID : ' . $id_bpp);
				}else $this->log_activity->insert_activity('insert', 'Gagal Insert Coa Value Temporary ID Coa : '. $get_coa_customer[0]['id_coa']);
			}
		// }

		$results = array('success' => true, 'message' => 'Berhasil update data Jurnal Umum ke database', 'id_bpp' => $id_bpp);
		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}
	
	public function save_edit_jurnal_coa_temporary() {
		$user_id 	= $this->session->userdata['logged_in']['user_id'];
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		
		$arrData = array();
		//$statSave = false;
		
		foreach ($params['listcoamasuk'] as $k => $v) {
			$arrTempMasuk = array(
				'id_jurnal'		=> $params['id_jurnal'],
				'id_coa_value'	=> $v[0],
				'id_coa' 		=> $v[8],
				'id_parent' 	=> $v[2],
				'date' 			=> $v[4],
				'id_valas' 		=> $v[13],
				'value' 		=> $v[10],
				'adjusment' 	=> 0,
				'type_cash' 	=> 0,
				'note' 			=> $v[3],
				'rate' 			=> $v[6],
				'bukti' 		=> NULL,
				'pic' 			=> $user_id,
				'pic_approve' 	=> NULL,
				'status' 		=> 0
			);
			
			$result_coa_masuk = $this->bpp_model->add_coa_value_jurnal_temporary($arrTempMasuk);
			$data_coa_masuk = array(
				'id_jurnal_np'	=> $params['id_jurnal'],
				'id_coa_value' 	=> $result_coa_masuk['lastid']
			);
			
			$result_detail_jurnal = $this->bpp_model->add_jurnal_coa_value_temporary($data_coa_masuk);
		}
		
		foreach ($params['listcoakeluar'] as $k => $v) {
			$arrTempKeluar = array(
				'id_jurnal'		=> $params['id_jurnal'],
				'id_coa_value'	=> $v[0],
				'id_coa' 		=> $v[8],
				'id_parent' 	=> $v[2],
				'date' 			=> $v[4],
				'id_valas' 		=> $v[13],
				'value' 		=> $v[10],
				'adjusment' 	=> 0,
				'type_cash' 	=> 1,
				'note' 			=> $v[3],
				'rate' 			=> $v[6],
				'bukti' 		=> NULL,
				'pic' 			=> $user_id,
				'pic_approve' 	=> NULL,
				'status' 		=> 0
			);
			
			$result_coa_keluar = $this->bpp_model->add_coa_value_jurnal_temporary($arrTempKeluar);
			$data_coa_keluar = array(
				'id_jurnal_np'	=> $params['id_jurnal'],
				'id_coa_value' 	=> $result_coa_keluar['lastid']
			);
			
			$result_detail_jurnal = $this->bpp_model->add_jurnal_coa_value_temporary($data_coa_keluar);
		}
		
		$statSave = true;
		
		if ($statSave == true) {
			$this->log_activity->insert_activity('insert', 'Berhasil Update data Coa Value : '.$arrTempMasuk['note']);
			$result = array('success' => true, 'message' => 'Berhasil update data Coa Value ke database');
		}else {
			$this->log_activity->insert_activity('insert', 'Gagal Update data Coa Value');
			$result = array('success' => false, 'message' => 'Gagal update data coa value ke database');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result);
	}
	
	public function detail_bpp($id) {
		$detail_bpp = $this->bpp_model->detail_bpp($id);

		$data = array(
			'detail_bpp' => $detail_bpp,
		);

		$this->load->view('detail_bpp_view', $data);
	}

	function detail_lists_bpp($id_bpp, $type_bpp) {
		$data_list = array(
			'id_bpp'	=> $id_bpp,
			'type_bpp'	=> $type_bpp
		);
		$list_piutang = $this->bpp_model->detail_lists_bpp($data_list);
		
		$no = 1;
		$data = array();
		if($type_bpp == 1) {
			$list_piutang = $this->bpp_model->detail_lists_bpp($data_list);

			foreach ($list_piutang as $lpk => $lpv) {
				array_push($data, array(
					$no++,
					$lpv['no_invoice'],
					$lpv['nama_valas'],
					$lpv['rate'],
					$lpv['symbol_valas'].' '.number_format(round($lpv['VALUE']))
				));
			}
		}else {
			$list_coa = $this->bpp_model->detail_lists_bpp_coa($data_list);

			foreach ($list_coa as $lck => $lcv) {
				if($lcv['type_cash'] == 0) $status = 'Debit';
				else $status = 'Kredit';
				array_push($data, array(
					$no++,
					$lcv['coa'],
					$lcv['id_parent'],
					$status,
					$lcv['keterangan'],
					$lcv['symbol_valas'] . ' ' . number_format(round($lcv['VALUE'])),
					$lcv['rate']
				));
			}
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function approve_bpp_update_status() {
		$data 	= file_get_contents("php://input");
		$params = json_decode($data, true);
		$params['userid'] = $this->session->userdata['logged_in']['user_id'];
		
		if ($params['status'] == 1) {
			$list_coa = $this->bpp_model->bpp_coa_temporary($params['id']);

			foreach ($list_coa as $lpk => $lpv) {
				$data_coa_value = array(
					'id_coa' 		=> $lpv['id_coa'],
					'id_parent' 	=> $lpv['id_parent'],
					'date' 			=> $lpv['date'],
					'id_valas' 		=> $lpv['id_valas'],
					'value' 		=> $lpv['value'],
					'adjusment' 	=> $lpv['adjusment'],
					'type_cash' 	=> $lpv['type_cash'],
					'note' 			=> $lpv['note'],
					'rate' 			=> $lpv['rate'],
					'bukti' 		=> $lpv['bukti'],
					'id_coa_temp' 	=> NULL
				);

				$add_coa_val_bpp = $this->bpp_model->add_coa_value($data_coa_value);
				if($add_coa_val_bpp['result'] > 0) {
					$this->log_activity->insert_activity('insert', 'Insert Coa Value dengan ID : '.$add_coa_val_bpp['lastid']);

					$data_d_bpp_update = array(
						'id_d_bpp'			=> (int)$lpv['id_detail_bpp'],
						'id_coa_val_temp'	=> 0,
						'id_coa_value'		=> (int)$add_coa_val_bpp['lastid']
					);
					$result_update = $this->bpp_model->edit_d_bpp_coa_val($data_d_bpp_update);

					if($lpv['type_bpp'] == 1) {
						$data_kartu_hp = array(
							'ref'			=> NULL,
							'source'		=> $lpv['no_invoice'],
							'keterangan'	=> 'Piutang invoice '.$lpv['no_invoice'],
							'status'		=> $lpv['type_cash'],
							'saldo'			=> $lpv['value'],
							'saldo_akhir'	=> $lpv['value'],
							'id_valas'		=> $lpv['id_valas'],
							'type_kartu'	=> 1,
							'id_master'		=> (int)$add_coa_val_bpp['lastid'],
							'type_master'	=> 1,
							'status_hp'		=> 1
						);
						$result_hp = $this->bpp_model->add_kartu_hp($data_kartu_hp);
					}

					if ($result_update > 0) {
						$this->bpp_model->delete_coa_temporary($lpv['id']);
						$this->log_activity->insert_activity('insert', 'Data BPP Berhasil di simpan dengan status Approve');
					}else $this->log_activity->insert_activity('insert', 'Data BPP Gagal di simpan dengan status Approve');
				}else {
					$results = array('success' => false, 'status' => 'error', 'message' => 'Data BPP Gagal di simpan');
					$this->log_activity->insert_activity('insert', 'Insert Coa Value dengan ID : '.$add_coa_val_bpp['lastid']);
				}
			}

			$data_update_bpp = array(
				'id_bpp'	=> $params['id'],
				'status'	=> $params['status']
			);
			
			$result_status = $this->bpp_model->update_bpp_status($data_update_bpp);
			if ($result_status > 0) {
				$results = array('success' => true, 'status' => 'success', 'message' => 'Data BPP berhasil di simpan');
				$this->log_activity->insert_activity('insert', 'Data BPP Berhasil di simpan dengan status Approve');
			}else {
				$results = array('success' => false, 'status' => 'error', 'message' => 'Data BPP Gagal di simpan');
				$this->log_activity->insert_activity('insert', 'Data BPP Gagal di simpan dengan status Approve');
			}
		}else if($params['status'] == 2) {
			$data_update_bpp = array(
				'id_bpp'	=> $params['id'],
				'status'	=> $params['status']
			);
			$result_status = $this->bpp_model->update_bpp_status($data_update_bpp);
			
			if($result_status > 0) {
				$results = array('success' => true, 'message' => 'Data BPP berhasil di simpan');
				$this->log_activity->insert_activity('insert', 'Data BPP Berhasil di simpan dengan status Reject');
			}else {
				$results = array('success' => false, 'message' => 'Data BPP berhasil di simpan');
				$this->log_activity->insert_activity('insert', 'Data BPP Gagal di simpan dengan status Reject');
			}
		}else {
			$results = array('success' => false, 'message' => 'Data BPP Gagal di simpan');
			$this->log_activity->insert_activity('insert', 'Data BPP Gagal di simpan');
		}
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($results);
	}
	
	public function delete_bpp_temporary() {
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data, true);
		
		$delete_jurnal = $this->bpp_model->delete_bpp_temporary($params['id']);
		
		if($delete_jurnal > 0){
			$this->log_activity->insert_activity('insert', 'Berhasil hapus Data BPP ID : ' . $params['id']);
			$res = array('status' => 'success', 'message' => 'Data jurnal berhasil di hapus');
		}else{
			$this->log_activity->insert_activity('insert', 'Gagal hapus data BPP ID : ' . $params['id']);
			$res = array('status' => 'failed', 'message' => 'Data jurnal gagal di hapus');
		}
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	public function delete_bpp_coa_temporary() {
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data, true);
		
		if($params['id_d_bpp'] != '' || $params['id_d_bpp'] != NULL){
			$this->bpp_model->delete_bpp_coa_temporary($params['id_d_bpp']);
			
			$this->log_activity->insert_activity('insert', 'Berhasil hapus Data Detail BPP ID : ' . $params['id_d_bpp']);
			$res = array('status' => 'success', 'message' => 'Data jurnal berhasil di hapus');
		}else{
			$this->log_activity->insert_activity('insert', 'Gagal hapus data Detail BPP ID : ' . $params['id_d_bpp']);
			$res = array('status' => 'failed', 'message' => 'Data jurnal gagal di hapus');
		}
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	public function check_no_voucher(){
		$this->form_validation->set_rules('no_voucher', 'NoVoucher', 'trim|required|min_length[4]|max_length[20]|is_unique[t_jurnal_non_prod.no_voucher]');
		$this->form_validation->set_message('is_unique', 'No Voucher Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'No Voucher Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}
}
