<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Bpp_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function lists_bpp($params){
		$sql 	= 'CALL bpp_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query(
			$sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			)
		);

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}
	
	public function list_bank($id_coa=null){
		$sql 	= 'SELECT a.*, b.*, c.* 
							FROM `t_bank` a
							LEFT JOIN  `t_coa` b ON a.`id_coa` = b.`id_coa`
							LEFT JOIN `m_valas` c ON a.`id_valas` = c.`valas_id`';
		if($id_coa) $sql=$sql.'WHERE a.id_coa ='.$id_coa;
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
		
	public function get_invoice_param($id_inv){
		$sql 	= 'call piutang_get_id_invoice('.$id_inv.')';
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_coa_eks($id_customer) {
		$sql 	= 'SELECT * FROM t_coa WHERE id_eksternal = ?';
	
		$query 	=  $this->db->query($sql, array(
			$id_customer
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function invoice(){
		$sql 	= 'CALL eksternal_invoice()';
	
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function coa_list(){
		$sql 	= 'SELECT * from t_coa order by coa';
	
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function list_coa($id,$type_cash=1){
		$sql 	= 'CALL coavalue_search_id_temporary(?,?)';

		$out = array();
		$query 	=  $this->db->query($sql, array(
			$id,
			$type_cash //default nya pengeluaran
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function add_bpp($data) {
		$sql 	= 'CALL bpp_add(?,?,?,?,?)';

		$query = $this->db->query($sql, array(
			$data['no_bpp'],
			$data['tanggal_bpp'],
			$data['status'],
			$data['id_customer'],
			$data['pic']
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		
		return $arr_result;
	}
	
	public function edit_bpp($data){
		$sql 	= 'CALL bpp_update(?,?,?,?,?)';

		$query = $this->db->query($sql, array(
			$data['id_bpp'],
			$data['no_bpp'],
			$data['tanggal_bpp'],
			$data['customer'],
			$data['pic']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
	public function add_coa_value_jurnal($data){
		$sql 	= 'CALL coavalue_add4(?,?,?,?,?,?,?,?,?,?,?)';

		$query = $this->db->query($sql, array(
			$data['id_coa'],
			$data['id_parent'],
			$data['date'],
			$data['id_valas'],
			$data['value'],
			$data['adjusment'],
			$data['type_cash'],
			$data['note'],
			$data['rate'],
			$data['bukti'],
			$data['id_coa_temp']
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		
		return $arr_result;
	}
	
	public function add_d_bpp($data){
		$sql 	= 'CALL bpp_detail_add(?,?,?,?,?,?,?,?,?)';

		$query = $this->db->query($sql, array(
			$data['id_bpp'],
			$data['id_invoice'],
			$data['value'],
			$data['valas_id'],
			$data['rate'],
			$data['value_real'],
			$data['id_coa_val_temp'],
			$data['id_coa_val'],
			$data['type_bpp']
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		
		return $arr_result;
	}

	public function add_bpp_coavalue_temporary($data) {
		$sql 	= 'CALL coavalue_add_invoice(?,?,?,?,?,?,?,?,?,?)';

		$query = $this->db->query($sql, array(
			$data['id_coa'],
			$data['id_parent'],
			$data['date'],
			$data['id_valas'],
			$data['value'],
			$data['adjusment'],
			$data['type_cash'],
			$data['note'],
			$data['rate'],
			$data['bukti']
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		
		return $arr_result;
	}

	public function add_coa_value_jurnal_temporary($data){
		$sql 	= 'CALL coavalue_add_temporary_jurnal(?,?,?,?,?,?,?,?,?,?,?,?,?)';

		$query = $this->db->query($sql, array(
			$data['id_coa'],
			$data['id_parent'],
			$data['date'],
			$data['id_valas'],
			$data['value'],
			$data['adjusment'],
			$data['type_cash'],
			$data['note'],
			$data['rate'],
			$data['bukti'],
			$data['pic'],
			$data['pic_approve'],
			$data['status']
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		
		return $arr_result;
	}

	public function add_t_bpp_coa($data) {
		$sql 	= 'CALL bpp_coa_add(?,?,?,?)';

		$query = $this->db->query($sql, array(
			$data['id_bpp'],
			$data['id_coa'],
			$data['id_coa_val_temp'],
			$data['id_coa_value']
		));
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
	public function add_jurnal_coa_value_temporary($data) {
		$sql 	= 'CALL jurnal_np_coa_add(?,?)';

		$query = $this->db->query($sql, array(
			$data['id_jurnal_np'],
			$data['id_coa_value']
		));
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
	public function list_coa_masuk($id_coa=null){
		$sql 	= 'SELECT a.*, b.keterangan, b.type_coa, c.nama_valas, c.symbol_valas, c.symbol
							FROM t_coa_value a
							LEFT JOIN t_coa b ON a.id_coa = b.id_coa
							LEFT JOIN m_valas c ON a.id_valas = c.valas_id';
		if($id_coa) $sql = $sql.' WHERE a.id_coa ='.$id_coa;
		
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function add_coa_value_temporary($data) {
		$sql 	= 'CALL coavalue_add_temporary_bpp(?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

		$query = $this->db->query($sql, array(
			$data['id_coa'],
			$data['id_parent'],
			$data['date'],
			$data['id_valas'],
			$data['value'],
			$data['adjusment'],
			$data['type_cash'],
			$data['note'],
			$data['rate'],
			$data['bukti'],
			$data['pic'],
			$data['pic_approve'],
			$data['status'],
			$data['type_bpp']
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		return $arr_result;
	}

	public function add_coa_value($data){
		$sql 	= 'CALL coavalue_add4(?,?,?,?,?,?,?,?,?,?,?)';

		$query = $this->db->query($sql, array(
			$data['id_coa'],
			$data['id_parent'],
			$data['date'],
			$data['id_valas'],
			$data['value'],
			$data['adjusment'],
			$data['type_cash'],
			$data['note'],
			$data['rate'],
			$data['bukti'],
			$data['id_coa_temp']
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		return $arr_result;
	}

	public function add_kartu_hp($data) {
		$sql 	= 'CALL hp_add_pays(?,?,?,?,?,?,?,?,?,?,?)';

		$query = $this->db->query($sql, array(
			$data['ref'],
			$data['source'],
			$data['keterangan'],
			$data['status'],
			$data['saldo'],
			$data['saldo_akhir'],
			$data['id_valas'],
			$data['type_kartu'],
			$data['id_master'],
			$data['type_master'],
			$data['status_hp']
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		return $arr_result;
	}

	public function edit_d_bpp_coa_val($data) {
		$sql =
			'UPDATE `d_bpp`
			SET 	`id_coa_val_temp`=?,
					`id_coa_value`=?
			WHERE	`id_detail_bpp`=?';

		$query = $this->db->query($sql, array(
			$data['id_coa_val_temp'],
			$data['id_coa_value'],
			$data['id_d_bpp']
		));
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}

	public function update_bpp_status($data) {
		$sql =
			'UPDATE `t_bpp`
			SET 	`status`=?
			WHERE	`id_bpp`=?';

		$query = $this->db->query($sql, array(
			$data['status'],
			$data['id_bpp']
		));
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}

	public function bpp_coa_temporary($id) {
		$sql 	= 'CALL bpp_coa_temp_search_id(?)';

		$query 	=  $this->db->query($sql, array(
			$id
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function detail_bpp($id) {
		$sql 	= 'CALL bpp_search_id(?)';

		$query 	=  $this->db->query($sql, array(
			$id
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function detail_lists_bpp($data) {
		$sql 	= 'CALL bpp_detail_search_id(?,?)';
		
		$query 	=  $this->db->query($sql, array(
			$data['id_bpp'],
			$data['type_bpp']
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function detail_lists_bpp_coa($data) {
		$sql 	= 'CALL bpp_detail_search_id(?,?)';
		
		$query 	=  $this->db->query($sql, array(
			$data['id_bpp'],
			$data['type_bpp']
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function detail_list_jurnal_np_coa_temporary($data)
	{
		$sql 	= 'CALL coavalue_temporary_search_id_jurnal(?,?)';
		
		$query 	=  $this->db->query($sql, array(
			$data['id_jurnal'],
			$data['type_cash']
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function detail_list_total_coa_temporary($data)
	{
		$sql 	= 'CALL jurnal_np_total_value_search_id_temporary(?)';
		
		$query 	=  $this->db->query($sql, array(
			$data['id_jurnal']
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function delete_bpp_temporary($id) {
		$sql 	= 'CALL bpp_delete(?)';
		
		$query 	=  $this->db->query($sql, array(
			$id
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function bpp_temp_delete($id) {
		$sql 	= 'CALL bpp_detail_temp_delete(?)';
		
		$query 	=  $this->db->query($sql, array(
			$id
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function delete_bpp_coa_temporary($id) {
		$sql 	= 'CALL bpp_detail_delete(?)';
		
		$query 	=  $this->db->query($sql, array(
			$id
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function delete_coa_temporary($id) {
		$sql 	= 'DELETE FROM `t_bpp_coa_value_temporary` WHERE id = ?';
		
		$query 	=  $this->db->query($sql, array(
			$id
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}
