<div class="row">
	<div class="col-md-12">
		<div class="col-md-5 pull-left">
			<div class="row">
				<label class="col-md-4">Tanggal Input </label>
				<label class="col-md-1 pull-left">:</label>
				<label class="col-md-7 pull-left"><?php if(isset($detail_bpp[0]['tanggal_bpp'])) {
					echo date('d M Y', strtotime($detail_bpp[0]['tanggal_bpp']));
				}?></label>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="col-md-5 pull-left">
			<div class="row">
				<label class="col-md-4">Nama PIC </label>
				<label class="col-md-1 pull-left">:</label>
				<label class="col-md-7 pull-left"><?php if(isset($detail_bpp[0]['nama_pic'])) {
					echo ucwords($detail_bpp[0]['nama_pic']);
				} ?></label>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="row">
	<div class="col-md-12">
		<div class="col-sm-6">
			<div class="row">
				<h4 class="page-title" id="title_menu">Piutang</h4>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table id="list_detail_bpp_piutang" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>No</th>
								<th>Invoice</th>
								<th>Valas</th>
								<th>Rate</th>
								<th>Value</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="row">
				<h4 class="page-title" id="title_menu">COA</h4>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table id="list_detail_coa" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th>No</th>
								<th>COA</th>
								<th>Parent</th>
								<th>Type Cash</th>
								<th>Keterangan</th>
								<th>Value</th>
								<th>Rate</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		dt_bpp_piutang();
		dt_bpp_coa();
	});

	function dt_bpp_piutang() {
		$('#list_detail_bpp_piutang').DataTable( {
			"processing": true,
			"searching": false,
			"serverSide": true,
			"ajax": {
				"type" : "GET",
				"url" : "<?php echo base_url().'bpp/detail_lists_bpp/'.$detail_bpp[0]['id_bpp'].'/1';?>"
			},
			"responsive": true,
			"lengthChange": false,
			"destroy": true,
			"info": false,
			"bSort": false,
			"paging": false,
			"columnDefs": [{
				"targets": [0],
				"width": 10
			},{
				"targets": [4],
				"className": 'dt-body-right'
			}, {
				"targets": [3],
				"className": 'dt-body-center'
			}]
		});
	}
	
	function dt_bpp_coa() {
		$('#list_detail_coa').DataTable( {
			"processing": true,
			"searching": false,
			"serverSide": true,
			"ajax": {
				"type" : "GET",
				"url" : "<?php echo base_url().'bpp/detail_lists_bpp/'.$detail_bpp[0]['id_bpp'].'/0';?>"
			},
			"responsive": true,
			"lengthChange": false,
			"destroy": true,
			"info": false,
			"bSort": false,
			"paging": false,
			"columnDefs": [{
				"targets": [0],
				"width": 10
			},{
				"targets": [4],
				"className": 'dt-body-right'
			}, {
				"targets": [3],
				"className": 'dt-body-center'
			}]
		});
	}
</script>