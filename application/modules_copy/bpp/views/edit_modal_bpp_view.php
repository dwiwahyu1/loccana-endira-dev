<form class="form-horizontal form-label-left" id="edit_form" role="form" action="<?php echo base_url('bpp/save_edit_bpp'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_bpp">No BPP <span class="required"><sup>*</sup></span></label>
		<div class="input-group col-md-8 col-sm-6 col-xs-12 ">
			<input placeholder="No BPP" type="text" class="form-control" id="no_bpp" name="no_bpp" required="required" value="<?php
				if(isset($bpp[0]['no_bpp'])) echo $bpp[0]['no_bpp'];
			?>" readonly>
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_bpp">Tanggal <span class="required"><sup>*</sup></span></label>

		<div class="input-group date col-md-8 col-sm-6 col-xs-12 ">
			<input placeholder="Tanggal" type="text" class="form-control datepicker" id="tanggal_bpp" name="tanggal_bpp" value="<?php
				if(isset($bpp[0]['tanggal_bpp'])) echo date('Y-m-d', strtotime($bpp[0]['tanggal_bpp']));
				else echo date('Y-m-d');
			?>" required>
			<div class="input-group-addon">
				<span class="glyphicon glyphicon-th"></span>
			</div>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_customer">Customer <span class="required"><sup>*</sup></span></label>

		<div class="input-group col-md-8 col-sm-6 col-xs-12 ">
			<select class="form-control select-control" id="id_customer" name="id_customer" style="width: 100%" required>
			<option value="0" data-coa="0" selected='selected'>-- Pilih Customer --</option>
				<?php foreach ($invoice as $keys) { ?>
					<option value="<?php echo $keys['id_eks']; ?>" data-coa="<?php echo $keys['id_eks']; ?>" <?php
						if(isset($bpp[0]['id_customer']) && ($keys['id_eks'] == $bpp[0]['id_customer'])) echo "selected";
					?>><?php echo $keys['name_eksternal']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Piutang: </label>
		<div class="col-md-8 col-sm-6 col-xs-12 add_item" onclick="edit_coa_value_masuk()">
			<a type="button" class="btn btn-default btn-icon waves-effect waves-light m-b-5">
				<i class="fa fa-plus"></i>
			</a>
			Tambah Piutang
		</div>
	</div>

	<div class="item form-group">
		<div class="col-md-8 col-md-offset-3">
			<table id="list_barang_masuk" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>ID BPP</th>
						<th>ID D BPP</th>
						<th></th>
						<th>Invoice</th>
						<th>Valas</th>
						<th>Rate</th>
						<th>Nilai</th>
						<th class="text-center" style="width: 5%">Option</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jumlah">Jumlah Piutang</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="jumlah_masuk" name="jumlah_masuk" class="form-control" placeholder="Jumlah Piutang" autocomplete="off" style="text-align:right" readonly required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar COA: </label>
		<div class="col-md-8 col-sm-6 col-xs-12 add_item" onclick="edit_coa_value_keluar()">
			<a type="button" class="btn btn-default btn-icon waves-effect waves-light m-b-5">
				<i class="fa fa-plus"></i>
			</a>
			Tambah COA
		</div>
	</div>

	<div class="item form-group">
		<div class="col-md-8 col-md-offset-3">
			<table id="list_barang_keluar" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>ID BPP</th>
						<th>ID D BPP</th>
						<th></th>
						<th>Perkiraan</th>
						<th>COA</th>
						<th>Keterangan</th>
						<th>Valas</th>
						<th>Nilai</th>
						<th>Type Cash</th>
						<th class="text-center" style="width: 5%">Option</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>

	<div class="item form-group" style="">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jumlah">Jumlah COA</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="jumlah_keluar" name="jumlah_keluar" class="form-control" placeholder="Jumlah Debit" autocomplete="off" style="text-align:right" readonly required>
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Jurnal Umum</button>
			<input type="hidden" id="id_bpp" name="id_bpp" value="<?php if(isset($bpp[0]['id_bpp'])){ echo $bpp[0]['id_bpp']; }?>">
		</div>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	var lastItemMasuk = 0;
	var lastRowMasuk = "";
	var lastValasMasuk = "";
	var lastCoaMasuk = "";
	var lastItemKeluar = 0;
	var lastRowKeluar = "";
	var lastValasKeluar = "";
	var lastCoaKeluar = "";
	var itemsMasuk = [];
	var itemsKeluar = [];
	var t_editBarangMasuk;
	var t_editBarangKeluar;
	
	$(document).ready(function() {
		$(".date").datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});
		$('#id_customer').select2();
		dtBarangMasuk();
		dtBarangKeluar();
	});

	// START JS ITEM MASUK
	function dtBarangMasuk() {
		t_editBarangMasuk = $('#list_barang_masuk').DataTable( {
			"processing": true,
			"searching": false,
			"responsive": true,
			"lengthChange": false,
			"ajax": {
				"type" : "GET",
				"url" : "<?php echo base_url().'bpp/edit_detail_lists_bpp/'.$bpp[0]['id_bpp'].'/1';?>"
            },
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0, 1],
				"visible": false,
				"searchable": false
			}, {
				"targets": [2],
				"width": 0
			}],
			"drawCallback": function(e) {
				$('.ddl_coa_masuk').select2('destroy');
				$('.ddl_coa_masuk').select2();

				if(lastCoaMasuk != '' && lastCoaMasuk != null) {
					$('#ddl_coa_masuk'+lastRowMasuk).val(lastCoaMasuk).trigger('change');
					$('#ddl_valas_masuk'+lastRowMasuk).val(lastValasMasuk).trigger('change');
					lastRowMasuk = "";
					lastCoaMasuk = "";
				}
				$('input[name="value_kas_masuk[]"]').each(function() {
					lastItemMasuk = parseInt(this.id.replace(/[^0-9]+/g, ""));
				});
				calculateValueMasuk();
			}
		});
	}
	
	$('#list_barang_masuk').on("click", "a", function() {
		var dataBarangMasuk = t_editBarangMasuk.row($(this).parents('tr')).data();
		var rowParent = $(this).parents('tr');

		if(dataBarangMasuk[0] != "" && dataBarangMasuk[0] != "new" && dataBarangMasuk[0] != null) {
			swal({
				title: 'Yakin akan Menghapus Barang yang sudah ada ?',
				text: 'data tidak dapat dikembalikan bila sudah dihapus !',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak'
			}).then(function () {
				var datapost = {
					"id_bpp"	: parseInt(dataBarangMasuk[0]),
					"id_d_bpp"	: parseInt(dataBarangMasuk[1])
				};

				$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>bpp/delete_bpp_coa_temporary",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
						if (response.status == "success") {
							delRowMasuk(rowParent);

							swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () { })
						}else {
							swal("Failed!", response.message, "error");
						}
					}
				});
			});
		}else delRowMasuk(rowParent);
	});

	function add_kas_masuk_temp_data(itemsKasMasuk) {
		var tempItems = [
			'new',
			'new',
			'',
			'<select onchange="changeCoaMasuk('+lastItemMasuk+')" class="form-control ddl_coa_masuk" id="ddl_coa_masuk'+lastItemMasuk+'"'+
				'name="ddl_coa_masuk[]" style="width: 100%" required>'+
				<?php foreach ($invoice as $key) { ?>
					"<option value=\"<?php echo $key['id_invoice']; ?>\"><?php echo $key['no_invoice'].'-'.$key['name_eksternal']; ?></option>"+
				<?php } ?>
			'</select>',
			'<select  class="form-control ddl_valas_masuk" id="ddl_valas_masuk'+lastItemMasuk+'" name="ddl_valas_masuk[]" style="width: 100%" required >'+
				<?php foreach ($valas as $key) { ?>
					'<option value="<?php echo $key['valas_id']; ?>"><?php echo $key['nama_valas']; ?></option>'+
				<?php } ?>
			'</select>',
			'<input type="number" class="form-control" id="rate'+lastItemMasuk+'" name="rate[]" step=".0001"'+
						'value="'+itemsKasMasuk.rate_masuk+'" placeholder="Rate" autocomplete="off">',
			'<input type="number" onKeyup="calculateValueMasuk()" class="form-control" id="value_kas_masuk'+lastItemMasuk+'" name="value_kas_masuk[]"'+
				'value="'+itemsKasMasuk.value_masuk+'" placeholder="Nominal" autocomplete="off">',
			'<a class="btn btn-icon waves-effect waves-light btn-danger">'+
				'<i class="fa fa-trash"></i>'+
			'</a>'
		]
		t_editBarangMasuk.row.add(tempItems).draw();
	}

	function delRowMasuk(rowParent) {
		t_editBarangMasuk.row(rowParent).remove().draw(false);
		calculateValueMasuk();
	}

	function calculateValueMasuk() {
		var jumlahValueMasuk = 0;

		$('input[name="value_kas_masuk[]"]').each(function() {
			jumlahValueMasuk = jumlahValueMasuk + parseFloat(this.value);
		});
		$('#jumlah_masuk').val(jumlahValueMasuk);
	}

	function changeCoaMasuk(idRow) {
		var strOption = $('#ddl_coa_masuk'+idRow+' option:selected').text();
		var arrStr = strOption.split("-");
		if(arrStr[0]) {
			$('#coa_number_masuk'+idRow).val(arrStr[0].trim());
		}
	}
	// END JS ITEM MASUK

	// START JS ITEM KELUAR
	function dtBarangKeluar() {
		t_editBarangKeluar = $('#list_barang_keluar').DataTable( {
			"processing": true,
			"searching": false,
			"responsive": true,
			"lengthChange": false,
			"ajax": {
				"type" : "GET",
				"url" : "<?php echo base_url().'bpp/edit_detail_lists_bpp/'.$bpp[0]['id_bpp'].'/0';?>"
            },
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0, 1],
				"visible": false,
				"searchable": false
			}],
			"drawCallback": function(e) {
				$('.ddl_coa_keluar').select2('destroy');
				$('.ddl_coa_keluar').select2();

				if(lastCoaKeluar != '' && lastCoaKeluar != null) {
					$('#ddl_coa_keluar'+lastRowKeluar).val(lastCoaKeluar).trigger('change');
					$('#ddl_valas_keluar'+lastRowKeluar).val(lastValasKeluar).trigger('change');
					lastRowMasuk = "";
					lastCoaKeluar = "";
				}
				$('input[name="note_keluar[]"]').each(function() {
					lastItemKeluar = parseInt(this.id.replace(/[^0-9]+/g, ""));
				});
				calculateValueKeluar();
			}
		});
	}

	$('#list_barang_keluar').on("click", "a", function() {
		var dataBarangKeluar = t_editBarangKeluar.row($(this).parents('tr')).data();
		var rowParent = $(this).parents('tr');

		if(dataBarangKeluar[0] != "" && dataBarangKeluar[0] != "new" && dataBarangKeluar[0] != null) {
			swal({
				title: 'Yakin akan Menghapus Barang yang sudah ada ?',
				text: 'data tidak dapat dikembalikan bila sudah dihapus !',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak'
			}).then(function () {
				var datapost = {
					"id" : parseInt(dataBarangKeluar[0])
				};

				$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>jurnal_np/delete_jurnal_coa_temporary",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
						if (response.status == "success") {
							delRowKeluar(rowParent);

							swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () { })
						}else {
							swal("Failed!", response.message, "error");
						}
					}
				});
			});
		}else delRowKeluar(rowParent);
	});

	function add_kas_keluar_temp_data(itemsKasKeluar) {
		var tempItems = [
			'new',
			'new',
			'',
			'<select class="form-control ddl_coa_keluar" id="ddl_coa_keluar'+lastItemKeluar+'"'+
				'name="ddl_coa_keluar[]" style="width: 100%" required>'+
				<?php foreach ($coa_all as $key) { ?>
					"<option value=\"<?php echo $key['id_coa']; ?>\"><?php echo $key['coa'].'-'.$key['keterangan']; ?></option>"+
				<?php } ?>
			'</select>',
			'<input type="text" class="form-control" id="coa_number_keluar'+ lastItemKeluar+'" name="coa_number_keluar[]"'+
				'value="'+itemsKasKeluar.coa_number_keluar+'" placeholder="Coa Number Keluar" autocomplete="off" readonly>',
			'<input type="text" class="form-control" id="note_keluar'+lastItemKeluar+'" name="note_keluar[]" value="'+itemsKasKeluar.note_keluar+'"'+
				'placeholder="Note" autocomplete="off">',
			'<select  class="form-control ddl_valas_keluar" id="ddl_valas_keluar'+lastItemKeluar+'" name="ddl_valas_keluar[]" style="width: 100%" required >'+
				<?php foreach ($valas as $key) { ?>
					'<option value="<?php echo $key['valas_id']; ?>"><?php echo $key['nama_valas']; ?></option>'+
				<?php } ?>
			'</select>',
			'<input type="number" onchange="calculateValueKeluar()" class="form-control" id="value_kas_keluar'+lastItemKeluar+'" name="value_kas_keluar[]"'+
				'value="'+itemsKasKeluar.value_keluar+'" placeholder="Nominal" autocomplete="off">',
			itemsKasKeluar.type_cash_text_keluar,
			'<a class="btn btn-icon waves-effect waves-light btn-danger">'+
				'<i class="fa fa-trash"></i>'+
			'</a>'+
			'<input type="hidden" id="rate_keluar'+lastItemKeluar+'" name="rate_keluar[]" value="'+itemsKasKeluar.rate_keluar+'">'+
			'<input type="hidden" id="type_cash'+lastItemKeluar+'" name="type_cash[]" value="'+itemsKasKeluar.type_cash_keluar+'">'
		]
		t_editBarangKeluar.row.add(tempItems).draw();
	}
	
	function delRowKeluar(rowParent) {
		t_editBarangKeluar.row(rowParent).remove().draw(false);
	}

	function calculateValueKeluar() {
		var jumlahValueKeluar = 0;

		$('input[name="value_kas_keluar[]"]').each(function() {
			jumlahValueKeluar = jumlahValueKeluar + parseFloat(this.value);
		});
		$('#jumlah_keluar').val(jumlahValueKeluar);
	}
	// END JS ITEM KELUAR
	
	$('#edit_form').on('submit',(function(e) {
		var arrTempMasuk = [];
		var arrTempKeluar = [];
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();

		if($('#jumlah_masuk').val() == $('#jumlah_keluar').val()) {
			var formData = new FormData();
			formData.set('id_bpp', $('#id_bpp').val());
			formData.set('no_bpp', $('#no_bpp').val());
			formData.set('tanggal_bpp', $('#tanggal_bpp').val());
			formData.set('customer', $('#id_customer').val());
			formData.set('jumlah_masuk', $('#jumlah_masuk').val());

			//Start Set Form Item Masuk
			for (var i = 0; i < t_editBarangMasuk.rows().data().length; i++) {
				var rowDataMasuk = t_editBarangMasuk.row(i).data();
				formData.append('arrIdBppMasuk[]', rowDataMasuk[0]);
				formData.append('arrIdDBppMasuk[]', rowDataMasuk[1]);
			}

			$('select[name="ddl_coa_masuk[]"] option:selected').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '') {
						formData.append('arrCoaMasuk[]', this.value);
					}
				}
			})
			$('input[name="note_masuk[]"]').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '') {
						formData.append('arrNoteMasuk[]', this.value);
					}else formData.append('arrNoteMasuk[]', null);
				}else formData.append('arrNoteMasuk[]', null);
			})

			$('select[name="ddl_valas_masuk[]"] option:selected').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '') {
						formData.append('arrValasMasuk[]', this.value);
					}
				}
			})

			$('input[name="rate[]"]').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '' && this.value > 0) {
						formData.append('arrRateMasuk[]', this.value);
					}
				}
			})

			$('input[name="value_kas_masuk[]"]').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '' && this.value >= 0) {
						formData.append('arrValueKasMasuk[]', this.value);
					}
				}
			})
			//End Set Form Item Masuk

			//Start Set Form Item Keluar
			for (var i = 0; i < t_editBarangKeluar.rows().data().length; i++) {
				var rowDataKeluar = t_editBarangKeluar.row(i).data();
				formData.append('arrIdBppKeluar[]', rowDataKeluar[0]);
				formData.append('arrIdDBppKeluar[]', rowDataKeluar[1]);
			}

			$('select[name="ddl_coa_keluar[]"] option:selected').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '') {
						formData.append('arrCoaKeluar[]', this.value);
					}
				}
			})

			$('input[name="note_keluar[]"]').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '') {
						formData.append('arrNoteKeluar[]', this.value);
					}else formData.append('arrNoteKeluar[]', null);
				}else formData.append('arrNoteKeluar[]', null);
			})

			$('select[name="ddl_valas_keluar[]"] option:selected').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '') {
						formData.append('arrValasKeluar[]', this.value);
					}
				}
			})

			$('input[name="rate_keluar[]"]').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '') {
						formData.append('arrRateKeluar[]', this.value);
					}else formData.append('arrRateKeluar[]', null);
				}else formData.append('arrRateKeluar[]', null);
			})

			$('input[name="value_kas_keluar[]"]').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '' && this.value >= 0) {
						formData.append('arrValueKasKeluar[]', this.value);
					}
				}
			})

			$('input[name="type_cash[]"]').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '' && this.value >= 0) {
						formData.append('arrtypeCashKeluar[]', this.value);
					}
				}
			})
			//End Set Form Item Keluar

			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: formData,
				cache: false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function() {
							$('#panel-modal').modal('toggle');
							list_jurnal_np();
						})
					} else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Edit Jurnal Umum");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Edit Jurnal Umum");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}else{
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Edit Jurnal Umum");
			swal("Warning!", "Maaf, Jumlah Piutang tidak sama dengan Jumlah Coa silahkan cek kembali.", "info");
		}
	}));
</script>