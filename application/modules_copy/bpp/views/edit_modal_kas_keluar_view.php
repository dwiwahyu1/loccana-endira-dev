<style>
	#loading {
		position: absolute;
		background: #FFFFFF;
		opacity: 0.5;
		width: 93%;
		height: 85%;
		z-index: 3;
		text-align: center;
		display: none;
	}
	
	.x-hidden {
		display: none;
	}
</style>
<div id="loading"><img src="<?php echo base_url(); ?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/preloader.gif" /></div>
<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
<div class="row">
	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="level1">Coa <span class="required"><sup>*</sup></span></label>
		</div>
		<div class="col-md-8">
			<select class="form-control select-control" id="level1" name="level1" style="width: 100%" required>
				<?php foreach ($coa_all as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>"><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3" id="subdetail_temp">
			<label style="margin: 10px 0 10px 10px;" for="id_valas">Valas <span class="required"><sup>*</sup></span></label>
		</div>
		<div class="col-md-8">
			<select class="form-control" id="id_valas" name="id_valas" style="width: 100%" readonly>
				<?php foreach ($valas as $key) { ?>
					<option value="<?php echo $key['valas_id']; ?>"><?php echo $key['nama_valas']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3"></div>
		<div class="col-md-8"></div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="rate">Rate <span class="required"><sup>*</sup></span></label>
		</div>
		<div class="col-md-8">
			<input type="number" class="form-control" id="rate" name="rate" min="1" placeholder="Rate" value="1" readonly>
		</div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="value">Nilai <span class="required"><sup>*</sup></span></label>
		</div>
		<div class="col-md-8">
			<input type="number" class="form-control" id="value" name="value" min="0" placeholder="Nilai" value="0" required>
		</div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="type_cash">Type <span class="required"><sup>*</sup></span></label>
		</div>
		<div class="col-md-8">
			<select class="form-control" id="type_cash" name="type_cash" style="width: 100%" required> 
				<option value="0">Debit</option>
				<option value="1" selected>Kredit</option>
			</select>
		</div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="note">Catatan</label>
		</div>

		<div class="col-md-8">
			<textarea class="form-control" data-parsley-maxlength="255" type="text" id="note" name="note" placeholder="Catatan"></textarea>
		</div>
	</div>

	<div class="col-sm-12 x-hidden" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="file_kas_keluar">File Bukti</label>
		</div>
		<div class="col-md-8">
			<input type="file" class="form-control" id="file_kas_keluar" name="file_kas_keluar" data-height="110" accept=".pdf, .txt, .doc, .docx" />
			<span id="file_bc_tick">Hanya format file pdf,txt,doc,docx dengan besaran max 10Mb yang diterima.</span>
		</div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="submit"></label>
		</div>
		<div class="col-md-8">
			<button id="btn-submit-kas" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Tambah Kredit</button>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();

		$('#level1').select2();
	});

	$('#btn-submit-kas').on('click', (function(e) {
		var tempArrKeluar = [];
		var jumlahKeluar = 0;
		var statusKeluar = 0;
		
		$('#btn-submit-kas').attr('disabled', 'disabled');
		$('#btn-submit-kas').text("Memasukkan data...");
		e.preventDefault();
		
		if($('#value').val() >= 0) {
			lastItemKeluar++;

			var id_coa_keluar = $('#level1').val(),
				id_parent_keluar = $('#level1').val(),
				coa_number_keluar = $('#level1 option:selected').data("coa"),
				text_coa_keluar = $('#level1 option:selected').text();

			var temp_kas_data_keluar = {
				id_coa_keluar: id_coa_keluar,
				text_coa_keluar: text_coa_keluar,
				coa_number_keluar: coa_number_keluar,
				id_parent_keluar: id_parent_keluar,
				id_valas_keluar: $('#id_valas').val(),
				text_valas_keluar: $('#id_valas option:selected').text(),
				value_keluar: $('#value').val(),
				rate_keluar: $('#rate').val(),
				type_cash_keluar: $('#type_cash').val(),
				type_cash_text_keluar: $('#type_cash option:selected').text(),
				note_keluar: $('#note').val(),
			}

			lastRowKeluar = lastItemKeluar;
			lastCoaKeluar = temp_kas_data_keluar.id_coa_keluar;
			lastValasKeluar = temp_kas_data_keluar.id_valas_keluar;
			add_kas_keluar_temp_data(temp_kas_data_keluar);
			$('#panel-modal-lv1').modal('toggle');
		}else{
			$('#btn-submit-kas').removeAttr('disabled');
			$('#btn-submit-kas').text("Tambah Kas");
			swal("Warning!", "Maaf, jumlah nilai tidak boleh 0 (nol).", "info");
		}
	}));
</script>