<style>
	#loading {
		position: absolute;
		background: #FFFFFF;
		opacity: 0.5;
		width: 93%;
		height: 85%;
		z-index: 3;
		text-align: center;
		display: none;
	}
	
	.x-hidden {display: none;}
</style>
<div id="loading"><img src="<?php echo base_url(); ?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/preloader.gif" /></div>
<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
<div class="row">
	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="type_id">Invoice <span class="required"><sup>*</sup></span></label>
		</div>
		<div class="col-md-8">
			<select class="form-control select-control" id="type_id" name="type_id" style="width: 100%" required>
				<option value="0" data-coa="0" selected='selected'>-- Pilih Invoice --</option>
				<?php foreach ($invoice as $key) { ?>
					<option value="<?php echo $key['id_invoice']; ?>" data-coa="<?php echo $key['no_invoice']; ?>"><?php echo $key['no_invoice'].' - '.$key['name_eksternal']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;" id="subdetail_temp">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="id_valas">Valas <span class="required"><sup>*</sup></span></label>
		</div>
		<div class="col-md-8">
			<select class="form-control" id="id_valas" name="id_valas" style="width: 100%" readonly>
				<?php foreach ($valas as $key) { ?>
					<option value="<?php echo $key['valas_id']; ?>"><?php echo $key['nama_valas']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="rate">Rate <span class="required"><sup>*</sup></span></label>
		</div>
		<div class="col-md-8">
			<input type="number" class="form-control" id="rate" name="rate" min="1" placeholder="Rate" value="1" readonly>
		</div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="value">Nilai <span class="required"><sup>*</sup></span></label>
		</div>
		<div class="col-md-8">
			<input type="number" class="form-control" id="value" name="value" min="0" placeholder="Nilai" value="0" readonly>
		</div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="submit"></label>
		</div>
		<div class="col-md-8">
			<button id="btn-submit-kas" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Tambah Piutang</button>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
		
		$('#type_id').select2();
	});

	$('#type_id').on('click', (function(e) {
		var id 			= $('#type_id').val();
		var datapost 	= {'id' : id};
			
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>bpp/get_invoice_param",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				$('#id_valas').val(response[0].valas_id);
				$('#rate').val(response[0].rate);
				$('#value').val(response[0].total_amount);
			}
		});
	}));

	$('#btn-submit-kas').on('click', (function(e) {
		var tempArrMasuk = [];
		var jumlahMasuk = 0;
		var statusMasuk = 0;
		
		$('#btn-submit-kas').attr('disabled', 'disabled');
		$('#btn-submit-kas').text("Memasukkan data...");
		e.preventDefault();
		
		if($('#value').val() != 0 && $('#value').val() >= 0 && $('#value').val() != null) {
			lastItemMasuk++;

			var temp_kas_data_masuk = {
				id_invoice_masuk: $('#type_id').val(),
				id_valas_masuk: $('#id_valas').val(),
				text_valas_masuk: $('#id_valas option:selected').text(),
				value_masuk: $('#value').val(),
				rate_masuk: $('#rate').val()
			}

			lastRowMasuk = lastItemMasuk;
			lastCoaMasuk = temp_kas_data_masuk.id_invoice_masuk;
			lastValasMasuk = temp_kas_data_masuk.id_valas_masuk;
			add_kas_masuk_temp_data(temp_kas_data_masuk);
			$('#panel-modal-lv1').modal('toggle');
		}else{
			$('#btn-submit-kas').removeAttr('disabled');
			$('#btn-submit-kas').text("Tambah Kas");
			swal("Warning!", "Maaf, jumlah nilai tidak boleh 0 (nol).", "info");
		}
	}));
</script>