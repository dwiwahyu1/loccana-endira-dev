<style>
  #nopendaftaran_loading-us {
    display: none
  }

  #nopendaftaran_tick {
    display: none
  }

  #nopengajuan_loading-us {
    display: none
  }

  #nopengajuan_tick {
    display: none
  }
</style>

<form class="form-horizontal form-label-left" id="add_form" role="form" action="<?php echo base_url('bc_maintenance/save_bc'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
  <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="jenis_bc">Jenis BC <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <select class="form-control" id="jenis_bc" name="jenis_bc" style="width: 100%" required>
        <option value="1">-- Select Jenis --</option>
        <?php foreach ($jenis_bc as $key) { ?>
          <option value="<?php echo $key['id']; ?>"><?php echo $key['jenis_bc']; ?></option>
        <?php } ?>
      </select>
      <input type="hidden" id="type_text" name="type_text" value="">
    </div>
  </div>

  <div class="item form-group" id="div_maintenance">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nomaintenance">No Maintenance <span class=""><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <select class="form-control" id="nomaintenance" name="nomaintenance" style="width: 100%">
        <option value="">-- Select BC First ! --</option>
      </select>
      <input type="hidden" id="type_text" name="type_text" value="">
    </div>
  </div>

  <!-- <div class="item form-group" id="div_maintenance_order">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nomaintenance">No Maintenance Order<span class=""><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <select class="form-control" id="nomaintenance_order" name="nomaintenance_order" style="width: 100%">
        <option value="">-- Select Maintenance Order--</option>
        <?php foreach ($list_maintenance as $key) {
          // var_dump($list_maintenance,$key);
        ?>
          <option value="<?php echo $key["id_mo"]; ?>"><?php echo $key["no_main_order"] . '-' . $key['name_eksternal']; ?></option>
        <?php } ?>
      </select>
      <input type="hidden" id="type_text" name="type_text" value="">
    </div>
  </div> -->
  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nopendaftaran">No Pendaftaran </label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input data-parsley-minlength="4" data-parsley-maxlength="100" type="number" id="nopendaftaran" name="nopendaftaran" class="form-control col-md-7 col-xs-12" placeholder="no pendaftaran minimal 4 karakter" autocomplete="off">
      <span id="nopendaftaran_loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking No Pendaftaran...</span>
      <span id="nopendaftaran_tick"></span>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nopengajuan">No Pengajuan <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input data-parsley-minlength="4" data-parsley-maxlength="100" type="text" id="nopengajuan" name="nopengajuan" class="form-control col-md-7 col-xs-12" placeholder="no pengajuan minimal 4 karakter" autocomplete="off" required>
      <span id="nopengajuan_loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking No Pengajuan...</span>
      <span id="nopengajuan_tick"></span>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tglpengajuan">Tanggal Pengajuan <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input data-parsley-maxlength="100" type="text" id="tglpengajuan" name="tglpengajuan" class="form-control col-md-7 col-xs-12" placeholder="tanggal pengajuan" autocomplete="off" required="required">
    </div>
  </div>


  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah BC</button>
    </div>
  </div>
</form>
<!-- /page content -->

<script type="text/javascript">
  $(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();
    $('#tglpengajuan').datepicker({
      format: "dd/M/yyyy",
      autoclose: true,
      todayHighlight: true
    });
    $('#jenis_bc').on('change', function(event) {
      var id_type = $(this).val();
      if (id_type != '') {
        var _this = $(this);
        $('select#nomaintenance').empty();
        var strHtml = '';

        $.ajax({
          type: "POST",
          url: '<?php echo base_url(); ?>bc_maintenance/get_maintenance',
          data: JSON.stringify({
            'id_bc': $(this).val()
          }),
          dataType: 'json',
          contentType: 'application/json; charset=utf-8',
          success: function(r) {
            if (id_type == 5)
              strHtml += '<option value="">-- Select Maintenance --</option>';
            else
              strHtml += '<option value="">-- Select Maintenance Order--</option>';
            for (var i = 0; i < r.data.length; i++) {
              if (id_type == 5)
                strHtml += "<option value='" + r.data[i].id_main + "'>" + r.data[i].no_main +" | "+ r.data[i].name_eksternal +"</option>";
              else
                strHtml += "<option value='" + r.data[i].id_mo + "'>" + r.data[i].no_main_order + "</option>";
            }
            // console.log(strHtml,r.data.length,r);
            $('select#nomaintenance').append(strHtml);
            $('select#nomaintenance').select2();
          },
          fail: function() {
            strHtml = '<option value="">Please try again</option>';
            $('select#nomaintenance').append(strHtml);
          }
        });
      };
    });
    // $('#jenis_bc').on('change', function() {
    //   if (this.value) {
    //     var post_data = {
    //       'jenis_bc': $(this).find("option:selected").text().replace(/\D/g, '')
    //     };
    //     $('#div_maintenance').hide()
    //     $('#div_maintenance_order').hide()
    //     if (post_data['jenis_bc'] == 261) {
    //       $('#div_maintenance').hide()
    //       $('#div_maintenance_order').show()
    //       jQuery.ajax({
    //         type: "POST",
    //         url: "<?php echo base_url('bc_maintenance/get_maintenance'); ?>",
    //         data: post_data,
    //         cache: false,
    //         success: function(response) {
    //           if (response.success == true) {
    //             $('#nopengajuan').val(response.no_pengajuan);
    //             $('#menus').val(response.menus);
    //           } else {
    //             $('#nopengajuan').val('');
    //             $('#menus').val('');
    //           }
    //         }
    //       });
    //     }
    //     if (post_data['jenis_bc'] == 262) {
    //       $('#div_maintenance').show()
    //       $('#div_maintenance_order').hide()
    //       // jQuery.ajax({
    //       // 	type: "POST",
    //       // 	url: "<?php echo base_url('bc_maintenance/get_no_ajul'); ?>",
    //       // 	data: post_data,
    //       // 	cache: false,
    //       // 	success: function(response) {
    //       // 		if (response.success == true) {
    //       // 			$('#nopengajuan').val(response.no_pengajuan);
    //       // 			$('#menus').val(response.menus);
    //       // 		} else {
    //       // 			$('#nopengajuan').val('');
    //       // 			$('#menus').val('');
    //       // 		}
    //       // 	}
    //       // });
    //     }


    //   } else {
    //     $('#nopengajuan').val('');
    //     $('#menus').val('');
    //   }
    // })
  });

  var last_nopendaftaran = $('#nopendaftaran').val();
  $('#nopendaftaran').on('input', function(event) {
    if ($('#nopendaftaran').val() != last_nopendaftaran) {
      nopendaftaran_check();
    }
  });

  function nopendaftaran_check() {
    var nopendaftaran = $('#nopendaftaran').val();
    if (nopendaftaran.length > 3) {
      var post_data = {
        'nopendaftaran': nopendaftaran
      };

      $('#nopendaftaran_tick').empty();
      $('#nopendaftaran_tick').hide();
      $('#nopendaftaran_loading-us').show();
      jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url('bc_maintenance/check_nopendaftaran'); ?>",
        data: post_data,
        cache: false,
        success: function(response) {
          if (response.success == true) {
            $('#nopendaftaran').css('border', '3px #090 solid');
            $('#nopendaftaran_loading-us').hide();
            $('#nopendaftaran_tick').empty();
            $("#nopendaftaran_tick").append('<span class="fa fa-check"> ' + response.message + '</span>');
            $('#nopendaftaran_tick').show();
          } else {
            $('#nopendaftaran').css('border', '3px #C33 solid');
            $('#nopendaftaran_loading-us').hide();
            $('#nopendaftaran_tick').empty();
            $("#nopendaftaran_tick").append('<span class="fa fa-close"> ' + response.message + '</span>');
            $('#nopendaftaran_tick').show();
          }
        }
      });
    } else {
      $('#nopendaftaran').css('border', '3px #C33 solid');
      $('#nopendaftaran_loading-us').hide();
      $('#nopendaftaran_tick').empty();
      $("#nopendaftaran_tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
      $('#nopendaftaran_tick').show();
    }
  }

  var last_nopengajuan = $('#nopengajuan').val();
  $('#nopengajuan').on('input', function(event) {
    if ($('#nopengajuan').val() != last_nopengajuan) {
      nopengajuan_check();
    }
  });

  function nopengajuan_check() {
    var nopengajuan = $('#nopengajuan').val();
    if (nopengajuan.length > 3) {
      var post_data = {
        'nopengajuan': nopengajuan
      };

      $('#nopengajuan_tick').empty();
      $('#nopengajuan_tick').hide();
      $('#nopengajuan_loading-us').show();
      jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url('bc_maintenance/check_nopengajuan'); ?>",
        data: post_data,
        cache: false,
        success: function(response) {
          if (response.success == true) {
            $('#nopengajuan').css('border', '3px #090 solid');
            $('#nopengajuan_loading-us').hide();
            $('#nopengajuan_tick').empty();
            $("#nopengajuan_tick").append('<span class="fa fa-check"> ' + response.message + '</span>');
            $('#nopengajuan_tick').show();
          } else {
            $('#nopengajuan').css('border', '3px #C33 solid');
            $('#nopengajuan_loading-us').hide();
            $('#nopengajuan_tick').empty();
            $("#nopengajuan_tick").append('<span class="fa fa-close"> ' + response.message + '</span>');
            $('#nopengajuan_tick').show();
          }
        }
      });
    } else {
      $('#nopengajuan').css('border', '3px #C33 solid');
      $('#nopengajuan_loading-us').hide();
      $('#nopengajuan_tick').empty();
      $("#nopengajuan_tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
      $('#nopengajuan_tick').show();
    }
  }

  $('#add_form').on('submit', (function(e) {
    $('#btn-submit').attr('disabled', 'disabled');
    $('#btn-submit').text("Memasukkan data...");
    e.preventDefault();
    var formData = new FormData(this);
    save_Form(formData, $(this).attr('action'));
  }));

  function save_Form(formData, url) {
    $.ajax({
      type: 'POST',
      url: url,
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success: function(response) {
        if (response.success == true) {
          swal({
            title: 'Success!',
            text: response.message,
            type: 'success',
            showCancelButton: false,
            confirmButtonText: 'Ok'
          }).then(function() {
            window.location.href = "<?php echo base_url('bc_maintenance'); ?>";
          })
        } else {
          $('#btn-submit').removeAttr('disabled');
          $('#btn-submit').text("Tambah BC");
          swal("Failed!", response.message, "error");
        }
      }
    }).fail(function(xhr, status, message) {
      $('#btn-submit').removeAttr('disabled');
      $('#btn-submit').text("Tambah BC");
      swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
    });
  }
</script>