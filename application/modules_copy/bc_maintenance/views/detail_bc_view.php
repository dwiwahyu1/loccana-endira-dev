<style>
	.col-customer {
		border: solid 1px #b2b8b7;
	}

	.dt-body-left {
		text-align: left;
		vertical-align: middle;
	}

	.dt-body-right {
		text-align: right;
		vertical-align: middle;
	}

	.dt-body-center {
		text-align: center;
		vertical-align: middle;
	}

	img {
		max-width: 85%;
		height: auto;
	}
</style>

<div id="print-area">
	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download Engineering Specification Form" id="btn_download" onclick="fnExcelReport();">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<table id="TablePrintHeaderPage1" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td colspan="4">
							<h4 id="titleCelebit" style="text-align:center;">PEMBERITAHUAN PENGELUARAN BARANG DARI</h4>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<h4 id="titlePerusahaan" style="text-align:center;">TEMPAT PENIMBUNAN BERIKAT DENGAN JAMINAN</h4>
						</td>
					</tr>
					<tr>
						<td width="25%">Kantor Pabean : </td>
						<td width="25%"></td>
						<td width="25%"><?php  echo $report_data['KPPBC']?></td>
						<td width="25%"></td>
					</tr>
					<tr>
						<td>Nomor Pengajuan : </td>
						<td><?php  echo $detail['no_pengajuan']; ?></td>
					</tr>
					<tr>
						<td>TUJUAN PENGIRIMAN : </td>
						<td><?php  echo $report_data['KODE_TUJUAN_PENGIRIMAN'];?></td>
						<td colspan="2">1. Diperbaiki 2.Disubkontraktorkan 3.Dipinjamkan 4.Lainnya</td>
					</tr>
				</tbody>
			</table>
			<table id="tablePrintTopPage1" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td colspan="2">B. Data Pemberitahuan</td>
					</tr>
					<tr>
						<td>Pengusaha TPB
							<br>1. NPWP : <?php  echo $report_data['NPWP_BILLING']?>
							<br>2. Nama :<?php  echo $report_data['ID_PENGUSAHA']?>
							<br>3. Alamat : <?php  echo $report_data['ALAMAT_PENGUSAHA']?>
							<br>4. Nomor dan Tanggal Izin TPB : <?php  echo $report_data['NOMOR_IJIN_TPB']?> Tanggal : <?php  echo $report_data['TANGGAL_IJIN_TPB']?> 
						</td>
						<td>D. DIISI OLEH BEA DAN CUKAI
							<br>Nomor Pendaftaran :  <?php  echo $detail['no_pendaftaran']?>
							<br>Tanggal :
						</td>
					</tr>
					<tr>
						<td>Penerima Barang
							<br>5. NPWP :
							<br>6. Nama : <?php  echo $report_data['ID_PENERIMA_BARANG']?>
							<br>7. Alamat : <?php  echo $report_data['ALAMAT_PENERIMA_BARANG']?>
						</td>
						<td>Dokumen Pelengkap Pabean
							<br>8. Packing List No : Tgl :
							<br>9. Pemenuhan Persyaratan/Fasilitas Impor
							<br>No : Tgl:
							<br>10. Surat Keputusan Dokumen Lainnya :
							<br>No : Tgl:
						</td>
					</tr>
					<tr>
						<td colspan="2">11. Valuta :  <?php  echo $report_data['KODE_VALUTA']?>
							<br>12. NDPBM : <?php  echo $report_data['NDPBM']?>
							<br>13. Nilai CIF : <?php  echo $report_data['CIF']?>
						</td>
					</tr>
					<tr>
						<td colspan="2">14. Jenis Sarana Pengangkut : <?php  echo $report_data['NAMA_PENGANGKUT']?>
						</td>
					</tr>
					<tr>
						<td>15. Nomor, Ukuran dan Tipe Peti Kemas &nbsp; 16. Jumlah, Jenis dan Merek Kemasan 1 0 Unpacked or unpackaged
						</td>
						<td>17. Berat Kotor (kg) :
							<br><?php  echo $report_data['BRUTO']?>
							<br>18. Berat Bersih (kg) :
							<br><?php  echo $report_data['NETTO']?>
						</td>
					</tr>
				</tbody>
			</table>
			<table id="tablePrintBottomPage1" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td>19<br>No.</td>
						<td>20
							<br>-Pos Tarif/HS
							<br>Uraian Jumlah Barang Secara Lengkap, Merek, Tipe, Ukuran dan Spesifikasi Lain.
						</td>
						<td>21
							<br>Negara
							<br>Asal Barang
						</td>
						<td>22 Tarif dan Fasilitas
							<br>BM, BMT, Cukai
							<br>PPN, PPnBM,
							<br>PPh
						</td>
						<td>23 Jumlah dan
							<br>Jenis Satuan
							<br>Berat Bersih (kg)
						</td>
						<td>24 Nilai CIF
						</td>
					</tr>
					<tr>
						<td>1</td>
						<td>
							<br>- Pos Tarif/HS : 
							<br>- Kode Barang : TTD
							<br>- SOFT TOOLING DN24C336-42, Merk: , Tipe:
							<br>- 0 Unpacked or unpackaged (NE)
						</td>
						<td><?php  echo $report_data['KODE_NEGARA_PENGIRIM']?></td>
						<td></td>
						<td>Satuan : 1,0000
							<br>NIU(Number Of International Units)
							<br>Berat Bersih :
							<br>135,50000
						</td>
						<td><?php  echo $report_data['CIF_RUPIAH']?></td>
					</tr>
				</tbody>
			</table>

			<table id="tablePrintLastBottomPage1" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td colspan="2">Data Perhitungan Jaminan</td>
						<td colspan="2">Data Jaminan</td>
					</tr>
					<tr>
						<td>Jenis Pungutan</td>
						<td>Jumlah</td>
						<td colspan="2" >
							32. Jaminan :
						</td>
					</tr>
					<tr>
						<td>25.Bea Masuk</td>
						<td></td>
						<td colspan="2" >33. Nomor Jaminan : &nbsp; Tanggal :</td>
					</tr>
					<tr>
						<td>26.Bea Masuk Tambahan</td>
						<td></td>
						<td colspan="2" >34. Nilai Jaminan :
					</tr>
					<tr>
						<td>27.Cukai</td>
						<td></td>
						<td colspan="2" >35. Tanggal Jatuh Tempo : <?php  echo $report_data['TANGGAL_JATUH_TEMPO']?>
					</tr>
					<tr>
						<td>28.PPN</td>
						<td></td>
						<td colspan="2" >36. Penjamin :
					</tr>
					<tr>
						<td>29.PPn BM</td>
						<td></td>
						<td colspan="2" >37. Nomor dan tanggal :
					</tr>
					<tr>
						<td  >30.PPh</td>
						<td></td>
						<td colspan="2" >Bukti Penerimaan Jaminan : &nbsp; Tanggal :</td>
					</tr>
					<tr>
						<td>31.Jumlah Total</td>
						<td></td>
						<td colspan="2" ></td>
					</tr>
					<tr>
						<td colspan="3">
							C. Pengesahan Pengusaha TPB
							<br>Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberitahukan
							<br>dalam pemberitahuan pabean ini.
							<br>Tempat, Tanggal 	:
							<br>Nama Lengkap 			:<?php  echo $report_data['NAMA_PENERIMA_BARANG']?>
							<br>Jabatan 					:
							<br>Tanda Tangan dan Stempel Perusahaan : 
						</td>
						<td>
						E. Untuk Pejabat BEA DAN CUKAI
						</td>
					</tr>
				</tbody>
			</table>
			
			<table id="tablePrintTopPage2" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td>No</td>
						<td>Jenis Dokumen</td>
						<td>Nomor Dokumen</td>
						<td>Tanggal Dokumen</td>
					</tr>
					<tr>
						<td>1</td>
						<td>INVOICE</td>
						<td>BDG 002</td>
						<td>08-02-2019</td>
					</tr>
					<tr>
						<td>2</td>
						<td>PACKING LIST</td>
						<td>BDG 002</td>
						<td>08-02-2019</td>
					<tr>
						<td>3</td>
						<td>Persetujuan Pengeluaran Barang Modal Untuk
							<br>Perbaikan/Reparasi Ke TLDDP
						</td>
						<td>S-435/WBC.09/KPP.MP.04/2019</td>
						<td>07-02-2019</td>
					</tr>
					<tr>
						<td>4</td>
						<td>BC 4.0</td>
						<td>079096</td>
						<td>04-12-2018</td>
					</tr>
					<tr>
						<td>5</td>
						<td>KONTRAK</td>
						<td>27/SE/02/2019</td>
						<td>01-02-2019</td>
					</tr>
					<tr>
						<td colspan="4">
									C. Pengesahan Pengusaha TPB
							<br>Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberitahukan dalam pemberitahuan pabean ini
							<br>Tempat, Tanggal :<?php  echo $report_data['KOTA_TTD']." , "?> <?php  echo $report_data['TANGGAL_TTD']?>
							<br>Nama Lengkap : <?php  echo $report_data['NAMA_TTD']?>
							<br>Jabatan :<?php  echo $report_data['JABATAN_TTD']?>
							<br>Tanda Tangan dan Stempel Perusahaan :
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<iframe id="txtArea1" style="display:none"></iframe>
	<script type="text/javascript">
		var dataImage = null;
		$(document).ready(function() {

			// $('#btn_download').click(function() {
			// 	var doc = new jsPDF('p', 'mm', 'letter');
			// 	var imgData = dataImage;
			// 	var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
			// 	var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

			// 	// FOOTER
			// 	doc.setTextColor(100);
			// 	doc.addImage(imgData, 'JPEG', 5, 5, 25, 25)
			// 	doc.setFontSize(12);
			// 	doc.text($('#titleCelebit').html(), 33, 10, 'left');
			// 	doc.setFontSize(10);
			// 	doc.text($('#titlePerusahaan').html(), 33, 15, 'left');
			// 	doc.setFontSize(11);
			// 	doc.text($('#titleAlamat').html(), 33, 20, 'left');
			// 	doc.setFontSize(11);
			// 	doc.text($('#titleTlp').html(), 33, 25, 'left');

			// 	doc.setDrawColor(116, 119, 122);
			// 	doc.setLineWidth(0.1);
			// 	doc.line(4, 30, 500, 30);

			// 	doc.setFontSize(11);
			// 	doc.text($('#titleESF').html(), pageWidth / 2, 40, 'center');

			// 	// Table Customer Information
			// 	doc.text($('#titleTableCustomerInformation').html(), pageWidth / 20, 50, 'left');
			// 	doc.text($('#titleNew').html(), pageWidth / 2, 50, 'left');
			// 	doc.text($('#titleRevise').html(), pageWidth * 7 / 10, 50, 'left');
			// 	doc.text($('#titleDate').html(), pageWidth * 8 / 10, 50, 'left');

			// 	doc.autoTable({
			// 		html: '#tableCustomerInformation',
			// 		theme: 'plain',
			// 		styles: {
			// 			fontSize: 6,
			// 			lineColor: [116, 119, 122],
			// 			lineWidth: 0.1,
			// 			cellWidth: 'auto',

			// 		},
			// 		margin: pageWidth / 20,
			// 		tableWidth: (pageWidth) - (pageWidth / 10),
			// 		headStyles: {
			// 			valign: 'middle',
			// 			halign: 'center',
			// 		},
			// 		columnStyles: {
			// 			0: {
			// 				halign: 'left',
			// 				cellWidth: 30
			// 			},
			// 			1: {
			// 				halign: 'left',
			// 				cellWidth: 30
			// 			},
			// 			2: {
			// 				cellWidth: 30
			// 			},
			// 			3: {
			// 				cellWidth: 30
			// 			},
			// 		},
			// 		rowPageBreak: 'auto',
			// 		showHead: 'firstPage',
			// 		showFoot: 'lastPage',
			// 		startY: 55,
			// 		startX: 'left'
			// 	});

			// 	// Table Layout Plan
			// 	doc.text($('#titleTableLayoutPlan').html(), pageWidth / 20, 90, 'left');

			// 	doc.autoTable({
			// 		html: '#tableLayoutPlan-Hide',
			// 		theme: 'plain',
			// 		styles: {
			// 			fontSize: 5.5,
			// 			lineColor: [116, 119, 122],
			// 			lineWidth: 0.1,
			// 			cellWidth: 'auto'
			// 		},
			// 		margin: pageWidth / 20,
			// 		tableWidth: (pageWidth) - (pageWidth / 10),
			// 		headStyles: {
			// 			valign: 'middle',
			// 			halign: 'center',
			// 		},
			// 		columnStyles: {
			// 			0: {
			// 				halign: 'left',
			// 				cellWidth: 30
			// 			},
			// 			1: {
			// 				halign: 'left',
			// 				cellWidth: 30
			// 			},
			// 			2: {
			// 				cellWidth: 30
			// 			},
			// 			3: {
			// 				cellWidth: 30
			// 			},

			// 		},
			// 		rowPageBreak: 'auto',
			// 		showHead: 'firstPage',
			// 		showFoot: 'lastPage',
			// 		startY: 95,
			// 		didDrawCell: function(data) {
			// 			if ((data.cell.section == 'body')) {
			// 				var td = data.cell.raw;
			// 				var img = td.getElementsByTagName('img')[0];
			// 				// data.cell.height = 50;
			// 				var textpos = data.cell.textPos;
			// 				if (img != null)
			// 					doc.addImage(img.src, textpos.x - 0.5, textpos.y - 0.5, data.cell.width - 2, data.cell.height - 2);
			// 			}
			// 		}
			// 	});

			// 	// Next Page
			// 	doc.addPage();

			// 	//HEADER 
			// 	doc.setTextColor(100);
			// 	doc.addImage(imgData, 'JPEG', 5, 5, 25, 25)
			// 	doc.setFontSize(12);
			// 	doc.text($('#titleCelebit').html(), 33, 10, 'left');
			// 	doc.setFontSize(10);
			// 	doc.text($('#titlePerusahaan').html(), 33, 15, 'left');
			// 	doc.setFontSize(11);
			// 	doc.text($('#titleAlamat').html(), 33, 20, 'left');
			// 	doc.setFontSize(11);
			// 	doc.text($('#titleTlp').html(), 33, 25, 'left');

			// 	doc.setDrawColor(116, 119, 122);
			// 	doc.setLineWidth(0.1);
			// 	doc.line(4, 30, 500, 30);

			// 	//Table Customer Specification
			// 	doc.text($('#titleTableCustomerSpecification').html(), pageWidth / 20, 40, 'left');

			// 	doc.autoTable({
			// 		html: '#tableCustomerSpecification',
			// 		theme: 'plain',
			// 		styles: {
			// 			fontSize: 5.5,
			// 			lineColor: [116, 119, 122],
			// 			lineWidth: 0.1,
			// 			cellWidth: 'auto'
			// 		},
			// 		margin: pageWidth / 20,
			// 		tableWidth: (pageWidth) - (pageWidth / 10),
			// 		headStyles: {
			// 			valign: 'middle',
			// 			halign: 'center',
			// 		},
			// 		columnStyles: {
			// 			0: {
			// 				halign: 'left',
			// 				cellWidth: 30
			// 			},
			// 			1: {
			// 				cellWidth: 30
			// 			},
			// 			2: {
			// 				cellWidth: 30
			// 			},
			// 			3: {
			// 				cellWidth: 30
			// 			},
			// 		},
			// 		rowPageBreak: 'auto',
			// 		showHead: 'firstPage',
			// 		showFoot: 'lastPage',
			// 		startY: 45,
			// 		didDrawCell: function(data) {
			// 			if ((data.cell.section == 'body')) {
			// 				var td = data.cell.raw;
			// 				console.log(td);
			// 				if (td.getElementsByTagName('img')[0] != null)
			// 					var img = td.getElementsByTagName('img')[0];
			// 				// data.cell.height = 50;
			// 				var textpos = data.cell.textPos;
			// 				if (img != null)
			// 					doc.addImage(img.src, textpos.x - 0.5, textpos.y - 0.5, data.cell.width - 2, data.cell.height - 2);
			// 			}
			// 		}
			// 	});

			// 	doc.autoTable({
			// 		html: '#tableTTDTop',
			// 		theme: 'plain',
			// 		styles: {
			// 			fontSize: 5.5,
			// 			lineColor: [116, 119, 122],
			// 			lineWidth: 0.1
			// 		},
			// 		margin: pageWidth / 20,
			// 		tableWidth: (pageWidth) - (pageWidth / 10),
			// 		headStyles: {
			// 			valign: 'middle',
			// 			halign: 'center',
			// 		},
			// 		columnStyles: {
			// 			0: {
			// 				cellWidth: 30
			// 			},
			// 			1: {
			// 				cellWidth: 30
			// 			},
			// 			2: {
			// 				cellWidth: 60
			// 			},
			// 		},
			// 		rowPageBreak: 'auto',
			// 		showHead: 'firstPage',
			// 		showFoot: 'lastPage',
			// 		startY: 180,
			// 	});

			// 	doc.autoTable({
			// 		html: '#tableTTDBottom',
			// 		theme: 'plain',
			// 		styles: {
			// 			fontSize: 5.5,
			// 			lineColor: [116, 119, 122],
			// 			lineWidth: 0.1,
			// 		},
			// 		margin: pageWidth / 20,
			// 		tableWidth: (pageWidth) - (pageWidth / 10),
			// 		headStyles: {
			// 			valign: 'middle',
			// 			halign: 'center',
			// 		},
			// 		columnStyles: {
			// 			0: {
			// 				cellWidth: 30
			// 			},
			// 			1: {
			// 				cellWidth: 30
			// 			},
			// 			2: {
			// 				cellWidth: 30
			// 			},
			// 			3: {
			// 				cellWidth: 30
			// 			},
			// 		},
			// 		rowPageBreak: 'auto',
			// 		showHead: 'firstPage',
			// 		showFoot: 'lastPage',
			// 		startY: 190,
			// 	});

			// 	var options = {
			// 		pagesplit: true,
			// 		'background': '#fff'
			// 	};
			// 	// doc.save('Engineer Specification Form  // echo $detail[0]['approval_date']; ?>.pdf');
			// });
		});

		// function toDataURL(url, callback) {
		// 	var xhr = new XMLHttpRequest();
		// 	xhr.onload = function() {
		// 		var reader = new FileReader();
		// 		reader.onloadend = function() {
		// 			callback(reader.result);
		// 		}
		// 		reader.readAsDataURL(xhr.response);
		// 	};
		// 	xhr.open('GET', url);
		// 	xhr.responseType = 'blob';
		// 	xhr.send();
		// }

		// toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
		// 	dataImage = dataUrl;
		// })


		function terbilangIND() {
			var bilangan = document.getElementById("totHide").value;
			var kalimat = "";
			var angka = new Array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
			var kata = new Array('', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan');
			var tingkat = new Array('', 'Ribu', 'Juta', 'Milyar', 'Triliun');
			var panjang_bilangan = bilangan.length;

			/* pengujian panjang bilangan */
			if (panjang_bilangan > 15) {
				kalimat = "Diluar Batas";
			} else {
				/* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
				for (i = 1; i <= panjang_bilangan; i++) {
					angka[i] = bilangan.substr(-(i), 1);
				}

				var i = 1;
				var j = 0;

				/* mulai proses iterasi terhadap array angka */
				while (i <= panjang_bilangan) {
					subkalimat = "";
					kata1 = "";
					kata2 = "";
					kata3 = "";

					/* untuk Ratusan */
					if (angka[i + 2] != "0") {
						if (angka[i + 2] == "1") {
							kata1 = "Seratus";
						} else {
							kata1 = kata[angka[i + 2]] + " Ratus";
						}
					}

					/* untuk Puluhan atau Belasan */
					if (angka[i + 1] != "0") {
						if (angka[i + 1] == "1") {
							if (angka[i] == "0") {
								kata2 = "Sepuluh";
							} else if (angka[i] == "1") {
								kata2 = "Sebelas";
							} else {
								kata2 = kata[angka[i]] + " Belas";
							}
						} else {
							kata2 = kata[angka[i + 1]] + " Puluh";
						}
					}

					/* untuk Satuan */
					if (angka[i] != "0") {
						if (angka[i + 1] != "1") {
							kata3 = kata[angka[i]];
						}
					}

					/* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
					if ((angka[i] != "0") || (angka[i + 1] != "0") || (angka[i + 2] != "0")) {
						subkalimat = kata1 + " " + kata2 + " " + kata3 + " " + tingkat[j] + " ";
					}

					/* gabungkan variabe sub kalimat (untuk Satu blok 3 angka) ke variabel kalimat */
					kalimat = subkalimat + kalimat;
					i = i + 3;
					j = j + 1;
				}

				/* mengganti Satu Ribu jadi Seribu jika diperlukan */
				if ((angka[5] == "0") && (angka[6] == "0")) {
					kalimat = kalimat.replace("Satu Ribu", "Seribu");
				}
			}

			document.getElementById("totalBilangan").innerHTML = kalimat;
		}

		function terbilangENG() {
			var bilangan = document.getElementById("totHide").value;
			var kalimat = "";
			var angka = new Array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
			var kata = new Array('', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine');
			var kataBelas = new Array('', 'One', 'Twelve', 'Thir', 'Four', 'Fif', 'Six', 'Seven', 'Eigh', 'Nine');
			var Puluh = new Array('', 'One', 'Twen', 'Thir', 'For', 'Fif', 'Six', 'Seven', 'Eigh', 'Nine');
			var tingkat = new Array('', 'Thousand', 'Million', 'Billion', 'Trillion');
			var panjang_bilangan = bilangan.length;

			/* pengujian panjang bilangan */
			if (panjang_bilangan > 15) {
				kalimat = "Diluar Batas";
			} else {
				/* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
				for (i = 1; i <= panjang_bilangan; i++) {
					angka[i] = bilangan.substr(-(i), 1);
				}

				var i = 1;
				var j = 0;

				/* mulai proses iterasi terhadap array angka */
				while (i <= panjang_bilangan) {
					subkalimat = "";
					kata1 = "";
					kata2 = "";
					kata3 = "";

					/* untuk Ratusan */
					if (angka[i + 2] != "0") {
						if (angka[i + 2] == "1") {
							kata1 = "Hundred";
						} else {
							kata1 = kata[angka[i + 2]] + " Hundred";
						}
					}

					/* untuk Puluhan atau Belasan */
					if (angka[i + 1] != "0") {
						if (angka[i + 1] == "1") {
							if (angka[i] == "0") {
								kata2 = "Ten";
							} else if (angka[i] == "1") {
								kata2 = "Eleven";
							} else if (angka[i] == "2") {
								kata2 = "Twelve";
							} else {
								kata2 = kataBelas[angka[i]] + "teen";
							}
						} else {
							kata2 = kataPuluh[angka[i + 1]] + "ty";
						}
					}

					/* untuk Satuan */
					if (angka[i] != "0") {
						if (angka[i + 1] != "1") {
							kata3 = kata[angka[i]];
						}
					}

					/* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
					if ((angka[i] != "0") || (angka[i + 1] != "0") || (angka[i + 2] != "0")) {
						subkalimat = kata1 + " " + kata2 + " " + kata3 + " " + tingkat[j] + " ";
					}

					/* gabungkan variabe sub kalimat (untuk Satu blok 3 angka) ke variabel kalimat */
					kalimat = subkalimat + kalimat;
					i = i + 3;
					j = j + 1;
				}

				/* mengganti Satu Ribu jadi Seribu jika diperlukan */
				if ((angka[5] == "0") && (angka[6] == "0")) {
					kalimat = kalimat.replace("One Thousand", "Thousand");
				}
			}

			document.getElementById("totalBilangan").innerHTML = kalimat;
		}

		function fnHtmltoExcelTable(idHTML) {
			var tab_text = "<table border='2px'>";
			var textRange;
			var j = 0;
			tab = document.getElementById(idHTML); // id of table
			console.log(idHTML)
			for (j = 0; j < tab.rows.length; j++) {
				tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
				//tab_text=tab_text+"</tr>";
			}
			tab_text = tab_text + "</table>";
			return tab_text;
		}

		function fnExcelReport() {
			tab_text = fnHtmltoExcelTable('TablePrintHeaderPage1') + 
				fnHtmltoExcelTable('tablePrintTopPage1') + 
				fnHtmltoExcelTable('tablePrintBottomPage1')+ 
				fnHtmltoExcelTable('tablePrintLastBottomPage1')+
				fnHtmltoExcelTable('tablePrintTopPage2');
			tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
			tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
			tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

			var ua = window.navigator.userAgent;
			var msie = ua.indexOf("MSIE ");

			if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer
			{
				txtArea1.document.open("txt/html", "replace");
				txtArea1.document.write(tab_text);
				txtArea1.document.close();
				txtArea1.focus();
				sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
			} else //other browser not tested on IE 11
				// sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

				var data_type = 'data:application/vnd.ms-excel';
				var a = document.createElement('a');
        a.href = data_type + ', ' + encodeURIComponent(tab_text);
        //setting the file name
        a.download = 'BC MAINTENANCE - '+<?php echo "'".$detail['tanggal_pengajuan']."'"; ?>+' .xls';
        //triggering the function
        a.click();
        //just in case, prevent default behaviour
        e.preventDefault();
		}
	</script>