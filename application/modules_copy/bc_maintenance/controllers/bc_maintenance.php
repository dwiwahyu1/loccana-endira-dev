<?php

defined('BASEPATH') or exit('No direct script access allowed');

class BC_Maintenance extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('bc_maintenance/bc_maintenance_model');
    $this->load->model('maintenance/maintenance_model');
    $this->load->model('maintenance_order/maintenance_order_model');
    $this->load->library('log_activity');
    $this->load->library('sequence');
    $this->codeData = array(
      'codeBC'    => '0505',
      'codeCelebit'  => '005347',
      'codeTgl'    => date('Ymd')
    );
  }

  /**
   * anti sql injection
   */
  public function Anti_sql_injection($string)
  {
    $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
    return $string;
  }

  public function index()
  {
    $this->template->load('maintemplate', 'bc_maintenance/views/index');
  }

  function list_bc()
  {
    $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
    $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
    $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
    $order = $this->input->get_post('order');
    $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
    $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 2;
    $order_fields = array('', 'jenis_bc', 'no_pendaftaran', 'no_pengajuan', 'tgl_pengajuan');

    $search = $this->input->get_post('search');
    $type_bc = ($this->input->get_post('type_bc') != FALSE) ? $this->input->get_post('type_bc') : 5;

    $search_val = (!empty($search['value'])) ? $search['value'] : null;

    $search_value = $this->Anti_sql_injection($search_val);

    // Build params for calling model
    $params['limit'] = (int) $length;
    $params['offset'] = (int) $start;
    $params['order_column'] = $order_fields[$order_column];
    $params['order_dir'] = $order_dir;
    $params['filter'] = $search_value;
    $params['type_bc'] = $type_bc;

    $list = $this->bc_maintenance_model->list_bc($params);
    // var_dump($list);die;
    $result["recordsTotal"] = $list['total'];
    $result["recordsFiltered"] = $list['total_filtered'];
    $result["draw"] = $draw;
    $result['type_bc'] = $type_bc;

    $data = array();
    $i = 0;
    $username = $this->session->userdata['logged_in']['username'];
    foreach ($list['data'] as $k => $v) {
      $i = $i + 1;
      $btnstr = '';
      if ($v['file_loc'] == "/uploads/bc/")
        $v['file_loc'] = '';
      if ($v['file_loc'] != '' || $v['file_loc'] != NULL) {
        $btnstr =
          '<div class="btn-group">' .
          '<a href="' . $v['file_loc'] . '" class="btn btn-info btn-sm" target="_blank" title="Download File" download>' .
          '<i class="fa fa-file"></i>' .
          '</a>' .
          '</div>';
      }

      $status_akses =
        '<div class="btn-group">' .
        '<button class="btn btn-warning btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
						onClick="edit_bc(\'' . $v['id'] . '\')">' .
        '<i class="fa fa-edit"></i>' .
        '</button>' .
        '</div>' .
        '<div class="btn-group">' .
        '<button class="btn btn-danger btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Delete"
						onClick="delete_bc(\'' . $v['id'] . '\',\''.$v['type_bc'].'\')">' .
        '<i class="fa fa-trash"></i>' .
        '</button>' .
        '</div>' .
        // '<div class="btn-group">' .
        // '<button class="btn btn-info btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Details"
        // 		onClick="details_bc(\'' . $v['id'] . '\')">' .
        // '<i class="fa fa-list-alt"></i>' .
        // '</button>' .
        '</div>';

      array_push($data, array(
        $i,
        $v['jenis_bc'],
        ucwords($v['no_pendaftaran']),
        ucwords($v['no_pengajuan']),
        date('d M Y', strtotime($v['tanggal_pengajuan'])),
        $btnstr,
        $status_akses
      ));
    }

    $result["data"] = $data;

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function add_bc()
  {
    $result_type = $this->bc_maintenance_model->type_bc();
    // Build params for calling model
    $params['limit'] = 1000;
    $params['offset'] = null;
    $params['order_column'] = null;
    $params['order_dir'] = null;
    $params['filter'] = null;

    $list_maintenance = $this->maintenance_order_model->lists($params)['data'];
    $data = array(
      'jenis_bc' => $result_type,
      'list_maintenance' => $list_maintenance
    );
    // echo "<pre>";print_r($data); echo "</pre>";
    $this->load->view('add_modal_view', $data);
  }

  public function check_nopendaftaran()
  {
    $this->form_validation->set_rules('nopendaftaran', 'No Pendaftaran', 'trim|required|min_length[4]|max_length[100]|is_unique[t_bc.no_pendaftaran]');
    $this->form_validation->set_message('is_unique', 'No Pendaftaran Already Exists.');

    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));

      $return = array('success' => false, 'message' => $msg);
      $this->output->set_content_type('application/json')->set_output(json_encode($return));
    } else if ($this->form_validation->run() == TRUE) {
      $return = array('success' => true, 'message' => 'No Pendaftaran Available');
      $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }
  }

  public function check_nopengajuan()
  {
    $this->form_validation->set_rules('nopengajuan', 'No Pengajuan', 'trim|required|min_length[4]|max_length[100]|is_unique[t_bc.no_pengajuan]');
    $this->form_validation->set_message('is_unique', 'No Pengajuan Already Exists.');

    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));

      $return = array('success' => false, 'message' => $msg);
      $this->output->set_content_type('application/json')->set_output(json_encode($return));
    } else if ($this->form_validation->run() == TRUE) {
      $return = array('success' => true, 'message' => 'No pengajuan Available');
      $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }
  }

  public function save_bc()
  {
    $this->form_validation->set_rules('jenis_bc', 'Jenis BC', 'trim|required');
    $this->form_validation->set_rules('nopengajuan', 'No Pengajuan', 'trim|required');
    $this->form_validation->set_rules('tglpengajuan', 'Tanggal Pengajuan', 'trim|required');

    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));

      $result = array('success' => false, 'message' => $msg);
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      $jenis_bc = $this->Anti_sql_injection($this->input->post('jenis_bc', TRUE));
      $nopendaftaran = $this->Anti_sql_injection($this->input->post('nopendaftaran', TRUE));
      $nopengajuan = ucwords($this->Anti_sql_injection($this->input->post('nopengajuan', TRUE)));
      $nomaintenance = ucwords($this->Anti_sql_injection($this->input->post('nomaintenance', TRUE)));
      $tglinput = $this->Anti_sql_injection($this->input->post('tglpengajuan', TRUE));
      $temptgl = explode("/", $tglinput);
      $tglpengajuan = date('Y-m-d', strtotime($temptgl[2] . '-' . $temptgl[1] . '-' . $temptgl[0]));
      $type_text = ucwords($this->Anti_sql_injection($this->input->post('type_text', TRUE)));
      $upload_error = NULL;
      $file_bc = NULL;


      $data = array(
        'jenis_bc' => $jenis_bc,
        'no_pendaftaran' => $nopendaftaran,
        'no_pengajuan' => $nopengajuan,
        'no_maintenance' => $nomaintenance,
        'tanggal_pengajuan' => $tglpengajuan,
        'file_loc' => $file_bc
      );

      $result = $this->bc_maintenance_model->add_bc_maintenance($data);
      if ($result > 0) {
        $this->log_activity->insert_activity('insert', 'Insert BC Masuk');
        $result = array('success' => true, 'message' => 'Berhasil menambahkan BC Masuk ke database');
      } else {
        $this->log_activity->insert_activity('insert', 'Gagal Insert BC Masuk');
        $result = array('success' => false, 'message' => 'Gagal menambahkan BC Masuk ke database');
      }

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
  }

  public function edit_bc($id)
  {
    $result = $this->bc_maintenance_model->edit_bc($id);
    $result_type = $this->bc_maintenance_model->type_bc();
    $temptgl = explode('-', $result[0]['tanggal_pengajuan']);
    $result[0]['tanggal_pengajuan'] = date('d/M/Y', strtotime($temptgl[0] . '-' . $temptgl[1] . '-' . $temptgl[2]));
    if ($result[0]['file_loc'] != '' || $result[0]['file_loc'] != NULL) $result[0]['file_loc'] = explode('uploads/bc/', $result[0]['file_loc'])[1];

    $params['limit'] = 1000;
    $params['offset'] = null;
    $params['order_column'] = null;
    $params['order_dir'] = null;
    $params['filter'] = null;

    $list_maintenance = $this->maintenance_model->lists($params)['data'];
    $data = array(
      'bc' => $result,
      'jenis_bc' => $result_type,
      'list_maintenance' => $list_maintenance,
      'folder' => 'uploads/bc/',
      'url' => base_url()
    );
    $this->load->view('edit_modal_view', $data);
  }

  public function save_edit_bc()
  {
    $this->form_validation->set_rules('jenis_bc', 'Jenis BC', 'trim|required');
    $this->form_validation->set_rules('nopengajuan', 'No Pengajuan', 'trim|required|min_length[4]|max_length[100]');
    $this->form_validation->set_rules('nomaintenance', 'No Maintenance', 'trim|required|min_length[4]|max_length[100]');
    $this->form_validation->set_rules('tglpengajuan', 'Tanggal Pengajuan', 'trim|required|max_length[100]');

    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));

      $result = array(
        'success' => false,
        'message' => $msg
      );

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      $bc_id = $this->Anti_sql_injection($this->input->post('bc_id', TRUE));
      $bc_maintenance_id = $this->Anti_sql_injection($this->input->post('bc_maintenance_id', TRUE));
      $old_file = $this->Anti_sql_injection($this->input->post('old_file', TRUE));
      $jenis_bc = $this->Anti_sql_injection($this->input->post('jenis_bc', TRUE));
      $nopendaftaran = $this->Anti_sql_injection($this->input->post('nopendaftaran', TRUE));
      $nopengajuan = ucwords($this->Anti_sql_injection($this->input->post('nopengajuan', TRUE)));
      $nomaintenance = ucwords($this->Anti_sql_injection($this->input->post('nomaintenance', TRUE)));
      $tglinput = $this->Anti_sql_injection($this->input->post('tglpengajuan', TRUE));
      $temptgl = explode("/", $tglinput);
      $tglpengajuan = date('Y-m-d', strtotime($temptgl[2] . '-' . $temptgl[1] . '-' . $temptgl[0]));
      $type_text = ucwords($this->Anti_sql_injection($this->input->post('type_text', TRUE)));

      $upload_error = NULL;
      $file_bc = NULL;

      $data = array(
        'bc_maintenance_id' => $bc_maintenance_id,
        'bc_id' => $bc_id,
        'no_maintenance' => $nomaintenance,
        'jenis_bc' => $jenis_bc,
        'no_pendaftaran' => $nopendaftaran,
        'no_pengajuan' => $nopengajuan,
        'tanggal_pengajuan' => $tglpengajuan,
        'file_loc' => $file_bc
      );

      $result = $this->bc_maintenance_model->save_edit_bc_maintenance($data);

      if ($result > 0) {
        $this->log_activity->insert_activity('update', 'Update BC Masuk id : ' . $bc_id);
        $result = array('success' => true, 'message' => 'Berhasil mengubah BC Masuk ke database');
      } else {
        $this->log_activity->insert_activity('update', 'Gagal Update BC Masuk id : ' . $bc_id);
        $result = array('success' => false, 'message' => 'Gagal mengubah BC Masuk ke database');
      }


      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
  }

  public function delete_bc()
  {
    $data   = file_get_contents("php://input");
    $params   = json_decode($data, true);


    $result   = $this->bc_maintenance_model->delete_bc_maintenance($params);
    // var_dump($result);die;
    if ($result > 0) {
      $this->log_activity->insert_activity('delete', 'Delete BC Maintenance id : ' . $params['id']);
      $res = array('status' => 'success', 'message' => 'Data telah di hapus');
    } else {
      $this->log_activity->insert_activity('delete', 'Gagal Delete BC Maintenance id : ' . $params['id']);
      $res = array('status' => 'failed', 'message' => 'Data gagal di hapus');
    }

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }
  public function get_no_ajul()
  {
    $jenis_bc = $this->Anti_sql_injection($this->input->post('jenis_bc', TRUE));
    // var_dump($jenis_bc);die;
    if ($jenis_bc == "262")
      $tempJenis = 'bc_masuk_' . $jenis_bc;
    else
      $tempJenis = 'bc_keluar_' . $jenis_bc;

    $no_aju = $this->sequence->get_max_no($tempJenis);
    $no_pengajuan = $this->codeData['codeBC'] . $jenis_bc . '-' . $this->codeData['codeCelebit'] . '-' . $this->codeData['codeTgl'] . '-' . $no_aju;

    /*echo "<pre>";
		// print_r($jenis_bc);
		// echo $no_pengajuan;
		echo "</pre>";
		die;*/

    if ($jenis_bc != '' && $jenis_bc != null) $return = array('success' => true, 'no_pengajuan' => $no_pengajuan, 'menus' => $tempJenis);
    else $return = array('success' => false, 'no_pengajuan' => $no_pengajuan);
    $this->output->set_content_type('application/json')->set_output(json_encode($return));
  }

  public function detail_bc($id)
  {
    $result = $this->bc_maintenance_model->edit_bc($id);
    $result_type = $this->bc_maintenance_model->type_bc();
    $temptgl = explode('-', $result[0]['tanggal_pengajuan']);
    $result[0]['tanggal_pengajuan'] = date('d/M/Y', strtotime($temptgl[0] . '-' . $temptgl[1] . '-' . $temptgl[2]));
    if ($result[0]['file_loc'] != '' || $result[0]['file_loc'] != NULL) $result[0]['file_loc'] = explode('uploads/bc/', $result[0]['file_loc'])[1];

    $params['limit'] = 1000;
    $params['offset'] = null;
    $params['order_column'] = null;
    $params['order_dir'] = null;
    $params['filter'] = null;
    //parameter print
    $list_maintenance = $this->maintenance_model->lists($params)['data'];
    $report_data_header = $this->bc_maintenance_model->report_data_header($result[0]['no_pengajuan']);
    $report_data_bahanbaku = $this->bc_maintenance_model->report_data_bahanbaku($result[0]['no_pengajuan']);
    $report_data_bahanbakutarif = $this->bc_maintenance_model->report_data_bahanbakutarif($result[0]['no_pengajuan']);
    $report_data_bahanbakudokumen = $this->bc_maintenance_model->report_data_bahanbakudokumen($result[0]['no_pengajuan']);
    $report_data_barang = $this->bc_maintenance_model->report_data_barang($result[0]['no_pengajuan']);
    $report_data_barangtarif = $this->bc_maintenance_model->report_data_barangtarif($result[0]['no_pengajuan']);
    $report_data_barangdokumen = $this->bc_maintenance_model->report_data_barangdokumen($result[0]['no_pengajuan']);
    $report_data_dokumen = $this->bc_maintenance_model->report_data_dokumen($result[0]['no_pengajuan']);
    $report_data_kemasan = $this->bc_maintenance_model->report_data_kemasan($result[0]['no_pengajuan']);
    $report_data_kontainer = $this->bc_maintenance_model->report_data_kontainer($result[0]['no_pengajuan']);
    $report_data_jaminan = $this->bc_maintenance_model->report_data_jaminan($result[0]['no_pengajuan']);
    // $report_data_jaminan = null;
    $report_data_respon = $this->bc_maintenance_model->report_data_respon($result[0]['no_pengajuan']);
    $report_data_status = $this->bc_maintenance_model->report_data_status($result[0]['no_pengajuan']);
    $report_data_pungutan = $this->bc_maintenance_model->report_data_pungutan($result[0]['no_pengajuan']);

    $data = array(
      'report_data_header' => $report_data_header,
      'report_data_list_header' => $report_data_header,
      'report_data_bahanbaku' => $report_data_bahanbaku,
      'report_data_bahanbakutarif' => $report_data_bahanbakutarif,
      'report_data_bahanbakudokumen' => $report_data_bahanbakudokumen,
      'report_data_barang' => $report_data_barang,
      'report_data_barangtarif' => $report_data_barangtarif,
      'report_data_barangdokumen' => $report_data_barangdokumen,
      'report_data_dokumen' => $report_data_dokumen,
      'report_data_kemasan' => $report_data_kemasan,
      'report_data_kontainer' => $report_data_kontainer,
      'report_data_jaminan' => $report_data_jaminan,
      'report_data_respon' => $report_data_respon,
      'report_data_status' => $report_data_status,
      'report_data_pungutan' => $report_data_pungutan,
      'detail' => $result
    );

    // echo "<pre>";print_r($result);echo"</pre>";die;
    if ($result[0]['jenis_bc'] == "BC 2.6.1")
      $this->load->view('detail_bc_view_pengeluaran', $data);
    else
      $this->load->view('detail_bc_view_pemasukan', $data);
  }

  public function list_detail_bc()
  {
    $id_bc = ($this->input->get_post('id_bc') != FALSE) ? $this->input->get_post('id_bc') : 0;
    $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
    $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
    $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
    $order = $this->input->get_post('order');
    $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
    $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 2;
    $order_fields = array('', 'tanggal_maintenance', 'no_maintenance');

    $search = $this->input->get_post('search');

    $search_val = (!empty($search['value'])) ? $search['value'] : null;

    $search_value = $this->Anti_sql_injection($search_val);

    // Build params for calling model

    $params['limit'] = (int) $length;
    $params['offset'] = (int) $start;
    $params['order_column'] = $order_fields[$order_column];
    $params['order_dir'] = $order_dir;
    $params['filter'] = $search_value;
    $params['id_bc'] = $id_bc;
    $list = $this->bc_maintenance_model->detail_bc($params);
    $data = array();
    $i = 1;
    foreach ($list['data'] as $k => $v) {
      if ($v['no_maintenance'] != '' || $v['no_maintenance'] != NULL) {
        $status_akses =
          /*
					'<div class="btn-group">'.
						'<button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
							onClick="edit_detail_bc(\'' . $v['id'] . '\')">'.
							'<i class="fa fa-edit"></i>'.
						'</button>'.
					'</div>'.
					*/
          '<div class="btn-group">' .
          '<button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete"
							onClick="delete_detail_bc(\'' . $v['no_maintenance'] . '\')">' .
          '<i class="fa fa-trash"></i>' .
          '</button>' .
          '</div>';
      }
      array_push(
        $data,
        array(
          $i,
          $v['no_maintenance'],
          $v['cust_name'],
          $v['tanggal_maintenance'],
          $v['departemen'],
          $v['total_products'],
          $status_akses
        )
      );
      $i++;
    }

    $result["data"] = $data;

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function add_detail_bc()
  {
    $this->load->view('bc_maintenance/add_modal_d_bc_view');
  }

  public function get_maintenance()
  {

    $data    = file_get_contents("php://input");
    $params    = json_decode($data, true);
    $result_po = $this->bc_maintenance_model->get_maintenance_by_type_bc($params['id_bc']);

    echo json_encode(['data' =>  $result_po]);
  }

  public function check_kd_brg_bc()
  {
    $this->form_validation->set_rules('kd_brg_bc', 'KodeBarangBC', 'trim|required|min_length[4]|max_length[20]|is_unique[d_bc_list.kode_barang_bc]');
    $this->form_validation->set_message('is_unique', 'Kode barang BC Already Registered.');

    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));

      $return = array('success' => false, 'message' => $msg);
      $this->output->set_content_type('application/json')->set_output(json_encode($return));
    } else if ($this->form_validation->run() == TRUE) {
      $return = array('success' => true, 'message' => 'Kode Barang BC Available');
      $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }
  }

  public function check_kd_brg()
  {
    $this->form_validation->set_rules('kd_brg', 'KodeBarang', 'trim|required|min_length[4]|max_length[20]|is_unique[d_bc_list.kode_barang]');
    $this->form_validation->set_message('is_unique', 'Kode barang Already Registered.');

    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));

      $return = array('success' => false, 'message' => $msg);
      $this->output->set_content_type('application/json')->set_output(json_encode($return));
    } else if ($this->form_validation->run() == TRUE) {
      $return = array('success' => true, 'message' => 'Kode Barang Available');
      $this->output->set_content_type('application/json')->set_output(json_encode($return));
    }
  }

  public function save_detail()
  {
    $this->form_validation->set_rules('no_maintenance', 'Maintenance', 'trim|required');
    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));

      $result = array('success' => false, 'message' => $msg);
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      $bc_id = $this->Anti_sql_injection($this->input->post('bc_id', TRUE));
      $no_maintenance = $this->Anti_sql_injection($this->input->post('no_maintenance', TRUE));

      $data = array(
        'id_bc'   => $bc_id,
        'no_maintenance'   => $no_maintenance
      );

      $result = $this->bc_maintenance_model->add_detail_bc($data);
      if ($result > 0) {
        $this->log_activity->insert_activity('insert', 'Insert Detail BC Masuk Purchase Order');
        $result = array('success' => true, 'message' => 'Berhasil menambahkan detail bea cukai ke database');
      } else {
        $this->log_activity->insert_activity('insert', 'Gagal Insert Detail BC Masuk Purchase Order');
        $result = array('success' => false, 'message' => 'Gagal menambahkan detail bea cukai ke database');
      }

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
  }

  public function edit_detail_bc($id)
  {
    $result = $this->bc_maintenance_model->edit_detail_bc($id);
    $result_uom = $this->bc_maintenance_model->search_uom();
    $result_valas = $this->bc_maintenance_model->search_valas();
    //$result_stock = $this->bc_maintenance_model->stock();

    $data = array(
      'detail' => $result,
      'uom' => $result_uom,
      'valas' => $result_valas
      //'stock' => $result_stock
    );
    // print_r($data);die;

    $this->load->view('edit_modal_d_bc_view', $data);
  }

  public function save_edit_detail()
  {
    $this->form_validation->set_rules('kd_brg_bc', 'KodeBarangBC', 'trim|required|min_length[4]|max_length[20]', array('is_unique' => 'This %s Kode Barang BC already exists.'));
    $this->form_validation->set_rules('kd_brg', 'KodeBarang', 'trim|required|min_length[4]|max_length[20]', array('is_unique' => 'This %s Kode Barang already exists.'));
    //$this->form_validation->set_rules('stock_id', 'Stock', 'trim|required');
    //var_dump($this->form_validation->run());die;

    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));

      $result = array(
        'success' => false,
        'message' => $msg
      );

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      $dbc_id = $this->Anti_sql_injection($this->input->post('dbc_id', TRUE));
      $bc_id = $this->Anti_sql_injection($this->input->post('bc_id', TRUE));
      $kd_brg_bc = strtoupper($this->Anti_sql_injection($this->input->post('kd_brg_bc', TRUE)));
      $kd_brg = strtoupper($this->Anti_sql_injection($this->input->post('kd_brg', TRUE)));
      $uom_id = $this->Anti_sql_injection($this->input->post('uom_id', TRUE));
      $valas_id = $this->Anti_sql_injection($this->input->post('valas_id', TRUE));
      $price = $this->Anti_sql_injection($this->input->post('price', TRUE));
      $weight = $this->Anti_sql_injection($this->input->post('weight', TRUE));
      $qty = $this->Anti_sql_injection($this->input->post('qty', TRUE));

      $data = array(
        'id'         => $dbc_id,
        'id_bc'       => $bc_id,
        'kode_barang_bc'   => $kd_brg_bc,
        'kode_barang'     => $kd_brg,
        'uom'         => $uom_id,
        'valas'       => $valas_id,
        'price'       => $price,
        'weight'       => $weight,
        'qty'         => $qty
      );

      $result = $this->bc_maintenance_model->save_edit_detail($data);

      if ($result > 0) {
        $this->log_activity->insert_activity('update', 'Update Detail BC Masuk Purchase Order id : ' . $dbc_id);
        $result = array('success' => true, 'message' => 'Berhasil mengubah detail bea cukai ke database');
      } else {
        $this->log_activity->insert_activity('update', 'Update Detail BC Masuk Purchase Order id : ' . $dbc_id);
        $result = array('success' => false, 'message' => 'Gagal mengubah detail bea cukai ke database');
      }

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
  }

  public function delete_detail_bc()
  {
    $data     = file_get_contents("php://input");
    $params   = json_decode($data, true);
    $result   = $this->bc_maintenance_model->delete_detail_bc($params['id']);

    if ($result > 0) {
      $this->log_activity->insert_activity('delete', 'Delete Detail BC Masuk Purchase Order id : ' . $params['id']);
      $res = array('status' => 'success', 'message' => 'Data telah di hapus');
    } else {
      $this->log_activity->insert_activity('delete', 'Gagal Delete Detail BC Masuk Purchase Order id : ' . $params['id']);
      $res = array('status' => 'failed', 'message' => 'Data gagal di hapus');
    }

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }
}
