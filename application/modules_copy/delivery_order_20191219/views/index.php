<style>
	.dt-body-left {text-align:left;}
	.dt-body-right {text-align:right;}
	.dt-body-center {text-align:center; vertical-align: middle;}
	.force-overflow {height: 650px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow {min-height: 650px}
	.force-overflow-packing {height: 950px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow-packing {min-height: 950px}
	#modal-do::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	#modal-do::-webkit-scrollbar
	{
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-do::-webkit-scrollbar-thumb
	{
		background-image: -webkit-gradient(linear,
		   left bottom,
		   left top,
		   color-stop(0.44, rgb(122,153,217)),
		   color-stop(0.72, rgb(73,125,189)),
		   color-stop(0.86, rgb(28,58,148)));
	}
</style>

<div class="container" style="height: auto;">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Delivery Order</h4>
		</div>
	</div>

	<div class="row">
        <div class="col-sm-12">
            <div class="card-box">
				<table id="listdelivery" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
                        <tr>
							<th>No</th>
							<th>Nomor DO</th>
							<th>Tanggal</th>
							<th>Detail Box</th>
							<th>No PI</th>
							<th>Remark</th>
							<th>status</th>
							<th>Option</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
            </div>
        </div><!-- end col -->
    </div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-do">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal-detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-do">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal-invoice" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg" style="width:90%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-do">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal-packing" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg" style="width:90%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow-packing" id="modal-do">
					<div class="scroll-overflow-packing">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal-do" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg" style="width:90%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow-packing" id="modal-do">
					<div class="scroll-overflow-packing">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
function delivery_order_add(){
	$('#panel-modal').removeData('bs.modal');
	$('#panel-modal .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal .panel-body').load('<?php echo base_url('delivery_order/delivery_order_add');?>');
	$('#panel-modal .panel-title').html('<i class="fa fa-plus"></i> Tambah Delivery');
	$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}

function delivery_order_detail(id){
	$('#panel-modal').removeData('bs.modal');
	$('#panel-modal .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal .panel-body').load('<?php echo base_url('delivery_order/delivery_order_detail');?>'+'/'+id);
	$('#panel-modal .panel-title').html('<i class="fa fa-search"></i> Detail Delivery');
	$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}

function delivery_order_edit(id){
	$('#panel-modal').removeData('bs.modal');
	$('#panel-modal .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal .panel-body').load('<?php echo base_url('delivery_order/delivery_order_edit');?>'+'/'+id);
	$('#panel-modal .panel-title').html('<i class="fa fa-pencil"></i> Edit Delivery');
	$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}

function delivery_order_invoice(id){
	$('#panel-modal-invoice').removeData('bs.modal');
	$('#panel-modal-invoice .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal-invoice .panel-body').load('<?php echo base_url('delivery_order/delivery_order_list_invoice');?>'+'/'+id);
	$('#panel-modal-invoice .panel-title').html('<i class="fa fa-newspaper-o"></i> Invoice');
	$('#panel-modal-invoice').modal({backdrop:'static',keyboard:false},'show');
}

function delivery_order_packing(id){
	$('#panel-modal-packing').removeData('bs.modal');
	$('#panel-modal-packing .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal-packing .panel-body').load('<?php echo base_url('delivery_order/delivery_order_list_packing');?>'+'/'+id);
	$('#panel-modal-packing .panel-title').html('<i class="fa fa-newspaper-o"></i> Packing List');
	$('#panel-modal-packing').modal({backdrop:'static',keyboard:false},'show');
}

function delivery_order_do(id){
	$('#panel-modal-packing').removeData('bs.modal');
	$('#panel-modal-packing .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal-packing .panel-body').load('<?php echo base_url('delivery_order/delivery_order_list_do');?>'+'/'+id);
	$('#panel-modal-packing .panel-title').html('<i class="fa fa-newspaper-o"></i> Delivery Order');
	$('#panel-modal-packing').modal({backdrop:'static',keyboard:false},'show');
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

function delivery_order_status(id_do){
	var date_now = new Date().toISOString().slice(0,10); 
	swal({
		title: 'Silahkan Pilih Status',
		text: 'Update status Delivery Order',
		input: 'select',
		inputClass: 'form-control',
		inputPlaceholder: 'Please Select',
		inputOptions: {
		  '1' : 'Proses',
		  '2' : 'Selesai',
		  '3' : 'Ditolak',
		},
		inputValidator: (value) => {
			return new Promise((resolve) => {
				if (value === '') {
					resolve('Pilih Status!')
				} else {
					resolve()
				}
			})
		}
	}).then(function (value) {
		var datapost={
			"id_do"   	:   id_do,
			"status"  	:   value,
			"date_end"  :   date_now
		};
		
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('delivery_order/delivery_order_update_status');?>",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				console.log(response);
				if(response.value == 1) {
				   swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						listdelivery();
					});
				}else if(response.value == 2){
					swal("Warning!", response.message, "info");
				}else if(response.value == 3){
					swal("Warning!", response.message, "info");
				}else{
					swal("Warning!", response.message, "info");
				}
			}
		});
	})
}

function delivery_order_delete(id){
	swal({
		title: 'Yakin akan Menghapus ?',
		text: 'data tidak dapat dikembalikan bila sudah dihapus !',
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Ya',
		cancelButtonText: 'Tidak'
	}).then(function () {
		var datapost={
			"id"  :   id
		};
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('delivery_order/delivery_order_delete');?>",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				
			   swal({
					title: 'Success!',
					text: response.message,
					type: 'success',
					showCancelButton: false,
					confirmButtonText: 'Ok'
				}).then(function () {
					window.location.href = "<?php echo base_url('delivery_order');?>";
				})
				if (response.status == "success") {
				} else{
                    swal("Failed!", response.message, "error");
				}
			}
		});
	})
}

function listdelivery(){
	$("#listdelivery").dataTable({
		"processing": true,
		"serverSide": true,
		"ajax": "<?php echo base_url().'delivery_order/lists';?>",
		"searchDelay": 700,
		"responsive": true,
		"lengthChange": false,
		"destroy": true,
		"info": false,
		"bSort": false,
		"dom": 'l<"toolbar">frtip',
		"initComplete": function(){
			var element = '<div class="btn-group pull-left">';
				element += '  	<a class="btn btn-primary" onClick="delivery_order_add()">';
				element += '    	<i class="fa fa-plus"></i> Add Delivery Order';
				element += '  	</a>';
				element += '</div>';
			$("div.toolbar").prepend(element);
		},
		"columnDefs": [{
			"targets": [0],
			"searchable": false,
			"className": 'dt-body-center',
			"width": 10
		},{
			"targets": [1],
			"searchable": false,
			"className": 'dt-body-left',
			"width": 180
		},{
			"targets": [2],
			"searchable": false,
			"className": 'dt-body-center',
			"width": 20,
		},{
			"targets": [3],
			"searchable": false,
			"className": 'dt-body-left',
			"width": 180
		},{
			"targets": [4],
			"searchable": false,
			"className": 'dt-body-left',
			"width": 180
		},{
			"targets": [5],
			"searchable": false,
			"className": 'dt-body-left',
			"width": 30
			
		},{
			"targets": [6],
			"searchable": false,
			"className": 'dt-body-center',
			"width": 80
		},{
			"targets": [7],
			"searchable": false,
			"className": 'dt-body-center',
			"width": 300
		}]
    });
}
  
function delivery_order_delete(id){
	swal({
		title: 'Yakin akan Menghapus ?',
		text: 'data tidak dapat dikembalikan bila sudah dihapus !',
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Ya',
		cancelButtonText: 'Tidak'
    }).then(function () {
		var datapost={
			"id" : id
		};

      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>delivery_order/delivery_detail_order_delete",
        data : JSON.stringify(datapost),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function(response) {
           $('.panel-heading button').trigger('click');
				listdelivery();
				swal({
					title: 'Success!',
					text: response.message,
					type: 'success',
					showCancelButton: false,
					confirmButtonText: 'Ok'
				}).then(function () {
				})
			}
		});
    })
}

$(document).ready(function(){
	listdelivery();
});
</script>