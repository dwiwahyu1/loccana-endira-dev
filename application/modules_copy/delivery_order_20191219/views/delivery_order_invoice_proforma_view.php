	<style>
		.col-customer {
			border: solid 1px #b2b8b7;
		}

		.dt-body-left {
			text-align: left;
			vertical-align: middle;
		}

		.dt-body-right {
			text-align: right;
		}

		.dt-body-center {
			text-align: center;
		}

		img {
			max-width: 85%;
			height: auto;
		}

		.nama-perusahaan-margin {
			margin-top: 30px;

		}

		.margin-row {
			margin-bottom: 30px
		}

		.margin-ttd {
			margin-bottom: 75px
		}

		.margin-nama {
			margin-bottom: 55px
		}

		.padding-box-nama {
			border: 1px solid #ebf2f2;
			padding: 10px 5px 10px 5px;
		}
	</style>

	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download Invoice" id="btn_download">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="col-md-2 logo-place">
				<img src="<?php echo site_url(); ?>assets/images/logo-celebit.jpg" alt="logo-celebit">
			</div>
			<div class="col-md-10">
				<div class="col-md-12 titleReport">
					<h1 id="titleCelebit">CELEBIT</h1>
					<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
					<h4 id="titleAlamat">BANDUNG FACTORY : JL.BUAH DUA RT.01/RW.04 RANCAEKEK - BANDUNG-INDONESIA</h4>
					<h4 id="titleTlp">TEL 62-22-7798 561/7798 542, FAX : 62-22-7798 562 E-MAIL : invoicing@celebit.com</h4>
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-12 text-center">
			<h4 id="titleInvoice" style="font-weight:900;">PROFORMA INVOICE</h4>
		</div>
	</div>

	<div class="row margin-row">
		<div class="col-md-12">
			<div class="col-md-7 padding-box-nama">
				<div class="row">
					<label class="control-label col-md-2" id="shipTo1">SHIP TO :</label>
					<div class="col-md-10">
						<label class="control-label" id="shipTo2"><?php if (isset($invoice[0]['name_eksternal'])) {
																												echo $invoice[0]['name_eksternal'];
																											} ?></label><br>
						<label class="control-label" id="shipTo3"><?php if (isset($invoice[0]['eksternal_address'])) {
																												echo $invoice[0]['eksternal_address'];
																											} ?></label><br>
						<label class="control-label" id="shipTo4">
							Tel : <?php if (isset($invoice[0]['phone_1']) || isset($invoice[0]['phone_2'])) echo $invoice[0]['phone_1'] . ' / ' . $invoice[0]['phone_2']; ?>
						</label><br>
						<label class="control-label" id="shipTo5">Fax<?php if (isset($invoice[0]['fax'])) {
																														echo $invoice[0]['fax'];
																													} ?></label><br>
					</div>
				</div>
				<div class="row">
					<label class="control-label col-md-2" id="att1">ATT :</label>
					<div class="col-md-11">
						<input type="text" class="form-control" id="att" name="att" value="" placeholder="ATT">
					</div>
				</div>
			</div>

			<div class="col-md-4 col-md-offset-1">
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-3" id="noInvoice1">No P Invoice</label>
						<label class="control-label col-md-1" id="noInvoice2">:</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="noInvoice" name="noInvoice">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-3" id="tanggalInvoice1">Date</label>
						<label class="control-label col-md-1" id="tanggalInvoice2">:</label>
						<!-- <label class="control-label col-md-8" id="tanggalInvoice3">
							<?php if (isset($invoice[0]['tanggal_do'])) {
																														$tanggal_do = date_create($invoice[0]['tanggal_do']);
																														echo date_format($tanggal_do, "d F Y");
																													}
							?> -->
						<div class="col-md-8">
							<div class="input-group date">
								<input placeholder="Tanggal" type="text" class="form-control datepicker" id="tanggalInvoice3" name="tanggalInvoice3" required="required" value="<?php if (isset($invoice[0]['tanggal_do'])) {
																																																																																} ?>">
								<div class="input-group-addon">
									<span class="glyphicon glyphicon-th"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-3" id="poNo1">PO Number</label>
						<label class="control-label col-md-1" id="poNo2">:</label>
						<div class="col-md-8">
							<input placeholder="PO No" type="text" class="form-control" id="poNo" name="poNo" required="required" value="">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-3" id="shipmentBy1">Shipment By</label>
						<label class="control-label col-md-1" id="shipmentBy2">:</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="shipmentBy" name="shipmentBy" required="required" value="" placeholder="Shipment By">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-3" id="etd1">ETD</label>
						<label class="control-label col-md-1" id="etd2">:</label>
						<div class="col-md-8">
							<div class="input-group date">
								<input placeholder="<?php echo date('d-M-Y'); ?>" type="text" class="form-control datepicker" id="etd" name="etd" required="required" value="<?php if (isset($invoice[0]['tanggal_do'])) {
																																																																																	$date = date_create($invoice[0]['tanggal_do']);
																																																																																	echo date_format($date, 'd-M-Y');
																																																																																} ?>">
								<div class="input-group-addon">
									<span class="glyphicon glyphicon-th"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-3" id="deliveryTerm1">Delivery Term</label>
						<label class="control-label col-md-1" id="deliveryTerm2">:</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="deliveryTerm" name="deliveryTerm" required="required" value="" placeholder="Delivery Term">
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="col-md-4 col-md-offset-8">
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-3" id="netWeight1">Net Weight</label>
						<label class="control-label col-md-1" id="netWeight2">:</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="netWeight" name="netWeight" placeholder="Net Weight">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-3" id="shipForm1">Ship Form</label>
						<label class="control-label col-md-1" id="shipForm2">:</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="shipForm" name="shipForm" required="required" value="" placeholder="Ship Form">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-3" id="shipTo1">Ship To</label>
						<label class="control-label col-md-1" id="shipTo2">:</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="shipTo" name="shipTo" required="required" value="" placeholder="Ship To">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-3" id="blNumber1">BL Number</label>
						<label class="control-label col-md-1" id="blNumber2">:</label>
						<div class="col-md-8">
							<input type="text" class="form-control" id="blNumber" name="blNumber" required="required" value="" placeholder="BL Number">
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="row">
		<label class="control-label col-md-12" id="being">Being Charge for Delivery of Goods Period:</label>
	</div>

	<div class="row margin-row">
		<div class="col-md-12">
			<table id="listinvoice" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th rowspan="2" style="vertical-align: middle;">NO</th>
						<th colspan="2" style="text-align:center">Description</th>
						<th rowspan="2" style="vertical-align: middle;">Qty (Pcs)</th>
						<th rowspan="2" style="vertical-align: middle;">Unit Price (<?php if (isset($invoice[0]['symbol_valas'])) {
																																																																																	echo $invoice[0]['symbol_valas'];
																																																																																} ?>)</th>
						<th rowspan="2" style="vertical-align: middle;">Unit Price Hid(<?php if (isset($invoice[0]['symbol_valas'])) {
																																																																																	echo $invoice[0]['symbol_valas'];
																																																																																} ?>)</th>
						<th rowspan="2" style="vertical-align: middle;">Amount (<?php if (isset($invoice[0]['symbol_valas'])) {
																																																																																	echo $invoice[0]['symbol_valas'];
																																																																																} ?>)</th>
						<th rowspan="2" style="vertical-align:middle">Amount Hid(<?php if (isset($invoice[0]['symbol_valas'])) {
																																																																																	echo $invoice[0]['symbol_valas'];
																																																																																} ?>)</th>
					</tr>
					<tr>
						<th>Part No</th>
						<th>PO</th>
					</tr>
				</thead>
				<tbody></tbody>
				<tfoot>
					<tr>
						<th colspan="5" style="text-align: left;">TOTAL <?php if (isset($invoice[0]['symbol_valas'])) {
																																																																																	echo $invoice[0]['symbol_valas'];
																																																																																} ?></th>
						<th id=""></th>
						<th id="tdAmount_Total"></th>
					</tr>
					<tr>
						<th colspan="7" id="totalBilangan" name="totalBilangan"></th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>

	<div class="row margin-row">
		<label class="control-label col-md-12" id="noted"><i>NO COMMERCIAL VALUE</i></label>
	</div>

	<div class="row">
		<label class="control-label col-md-12" id="noted">Noted :</label>
	</div>

	<div class="row margin-row">
		<div class="col-md-12">
			<div class="col-md-7 padding-box-nama">
				<div class="row margin-nama">
					<div class="col-md-6">
						<label class="control-label col-md-6" id="approved1">Approved by Customer :</label>
						<div class="col-md-6">
							<input type="text" class="form-control" id="approved" name="approved" required="required" value="" placeholder="Approved by Customer">
						</div>
					</div>
					<div class="col-md-6">
						<label class="control-label col-md-2" id="dateCust1">Date :</label>
						<div class="col-md-10">
							<div class="input-group date">
								<input placeholder="<?php echo date('d-M-Y'); ?>" type="text" class="form-control datepicker" id="dateCust" name="dateCust" required="required" value="<?php if (isset($invoice[0]['tanggal_do'])) {
																																																																																					$date = date_create($invoice[0]['tanggal_do']);
																																																																																					echo date_format($date, 'd-M-Y');
																																																																																				} ?>">
								<div class="input-group-addon">
									<span class="glyphicon glyphicon-th"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-2" id="name1">Name :</label>
						<div class="col-md-10">
							<input type="text" class="form-control" id="name" name="name" required="required" value="" placeholder="Name">
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-md-offset-1">
				<div class="col-md-12 text-center">
					<label class="control-label margin-ttd">PT. CELEBIT CIRCUIT TECH. IND</label>
					<input class="form-control " id="penandaTangan">
				</div>
			</div>

		</div>
	</div>
	<div class="row">
		<input type="hidden" id="totHide" name="totHide">
	</div>
	<div class="row">
		<div class="col-md-12">
			<label class="control-label pull-right" id="kode_report">F_MCI-002</label>
		</div>
		<div class="col-md-12">
			<label class="control-label pull-right" id="kode_report_detail">REV : 00</label>
		</div>
	</div>
	<script type="text/javascript">
		var dataImage = null;
		var invoice = <?php echo count($invoice); ?>;
		var nama_valas = '<?php if (isset($invoice[0]['nama_valas'])) {
																				echo $invoice[0]['nama_valas'];
																			} ?>';

		var t_invoice_list;

		$(document).ready(function() {
			dtInvoice();
			$(".date").datepicker({
				format: 'yyyy-mm-dd',
				autoclose: true,
				todayHighlight: true,
			});

			$('#btn_download').click(function() {
				var doc = new jsPDF('p', 'mm', 'letter');
				var imgData = dataImage;
				var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
				var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
				console.log($("#notes-label").val());
				// FOOTER
				doc.setTextColor(100);
				doc.addImage(imgData, 'JPEG', 5, 5, 25, 25)
				doc.setFontSize(12);
				doc.text($('#titleCelebit').html(), 33, 10, 'left');
				doc.setFontSize(10);
				doc.text($('#titlePerusahaan').html(), 33, 15, 'left');
				doc.setFontSize(11);
				doc.text($('#titleAlamat').html(), 33, 20, 'left');
				doc.setFontSize(11);
				doc.text($('#titleTlp').html(), 33, 25, 'left');

				doc.setDrawColor(116, 119, 122);
				doc.setLineWidth(0.1);
				doc.line(4, 30, 500, 30);

				doc.setFontSize(11);
				doc.text($('#titleInvoice').html(), pageWidth / 2, 40, 'center');

				doc.setDrawColor(116, 119, 122);
				doc.setLineWidth(0.2);
				doc.line(4, 45, 110, 45);
				doc.setLineWidth(0.2);
				doc.line(4, 45, 4, 65);
				doc.setLineWidth(0.2);
				doc.line(110, 45, 110, 65);
				doc.setLineWidth(0.2);
				doc.line(4, 65, 110, 65);

				doc.setFontSize(7);
				doc.text($('#namaCustomer').html(), 6, 50);
				doc.text($('#lokasiCustomer').html(), 6, 55);
				doc.text($('#alamatCustomer').html(), 6, 60);
				doc.text($('#divCustomer').html(), 6, 65);

				doc.setFontSize(7);
				doc.text($('#noInvoice1').html(), 115, 50, 'left');
				doc.text($('#noInvoice2').html(), 128, 50, 'left');
				doc.text($('#noInvoice3').val(), 135, 50, 'left'); //dari inputan

				doc.text($('#tanggalInvoice1').html(), 115, 57, 'left');
				doc.text($('#tanggalInvoice2').html(), 128, 57, 'left');
				doc.text($('#tanggalInvoice3').val(), 135, 57, 'left');

				doc.text($('#dueDateInvoice1').html(), 115, 64, 'left');
				doc.text($('#dueDateInvoice2').html(), 128, 64, 'left');
				doc.text($('#dueDateInvoice3').val(), 135, 64, 'left');

				doc.autoTable({
					html: '#listinvoice',
					theme: 'plain',
					styles: {
						fontSize: 8,
						lineColor: [116, 119, 122],
						lineWidth: 0.1,
						cellWidth: 'auto',

					},
					margin: 4.5,
					tableWidth: (pageWidth - 20),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					didParseCell: function(data) {
						if (data.table.foot[0]) {
							if (data.table.foot[0].cells[4]) {
								data.table.foot[0].cells[4].styles.halign = 'right';
							}
							if (data.table.foot[0].cells[5]) {
								data.table.foot[0].cells[5].styles.halign = 'right';
							}
						}
					},
					columnStyles: {
						0: {
							tableWidth: 10,
							halign: 'center'
						},
						1: {
							tableWidth: 10,
							halign: 'center'
						},
						2: {
							halign: 'center'
						},
						3: {
							halign: 'right'
						},
						4: {
							halign: 'right'
						},
						5: {
							halign: 'right'
						}
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 70
				});

				doc.setFontSize(8);
				doc.setFontType('bold');
				doc.text($('#kode_report').html(), 205, pageHeight - 5, 'right');
				doc.text($('#kode_report_detail').html(), 205, pageHeight - 10, 'right');

				var x = pageWidth * 70 / 100;
				var y = pageHeight * 75 / 100;
				doc.setFontSize(10);
				doc.text("PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA ", x, y, 'center');
				doc.text($('#penandaTangan').val(), x, y + 30, 'center');
				doc.save('INVOICE <?php echo $invoice[0]['tanggal_do']; ?>.pdf');
			});
		});

		function toDataURL(url, callback) {
			var xhr = new XMLHttpRequest();
			xhr.onload = function() {
				var reader = new FileReader();
				reader.onloadend = function() {
					callback(reader.result);
				}
				reader.readAsDataURL(xhr.response);
			};
			xhr.open('GET', url);
			xhr.responseType = 'blob';
			xhr.send();
		}

		toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
			//console.log('RESULT:', dataUrl)
			dataImage = dataUrl;
		})

		function dtInvoice() {
			t_invoice_list = $('#listinvoice').DataTable({
				"processing": true,
				"searching": false,
				"responsive": true,
				"lengthChange": false,
				"info": false,
				"bSort": false,
				"ajax": {
					"type": "GET",
					"url": "<?php echo base_url() . 'delivery_order/delivery_list_invoice/' . $id_do; ?>",
					"dataSrc": function(response) {
						$('#tdAmount_Total').html(formatCurrencyComaUSD(response.total_amounts));
						return response.data;
					}
				},
				"columnDefs": [{
					"targets": [0],
					"searchable": false,
					"className": 'dt-body-center',
					"width": 10
				}, {
					"targets": [1],
					"searchable": false,
					"className": 'dt-body-center',
					"width": 250
				}, {
					"targets": [2],
					"searchable": false,
					"className": 'dt-body-center',
					"width": 250
				}, {
					"targets": [3],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 150
				}, {
					"targets": [4],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 150
				}, {
					"targets": [5, 7],
					"searchable": false,
					"visible": false,
					"className": 'dt-body-right',
					"width": 60
				}, {
					"targets": [6],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 150
				}],
				"footerCallback": function(row, data, start, end, display) {
					var api = this.api(),
						data;
					var total = 0;
					// Remove the formatting to get integer data for summation
					var intVal = function(i) {
						return typeof i === 'string' ?
							i.replace(/[\$,]/g, '') * 1 :
							typeof i === 'number' ?
							i : 0;
					};

					// Total over all pages
					total = api
						.column(7)
						.data()
						.reduce(function(a, b) {
							total = total + intVal(a) + intVal(b);
							return (intVal(a) + intVal(b));
						}, 0);

					// Total over this page
					pageTotal = api
						.column(7, {
							page: 'current'
						})
						.data()
						.reduce(function(a, b) {
							total = total + intVal(a) + intVal(b);
							return (intVal(a) + intVal(b));
						}, 0);

					// Update footer
					$(api.column(7).footer()).html(
						'<?php $invoice[0]['symbol_valas']; ?>' + formatCurrencyComaUSD(pageTotal)
					)

					$("#total-label").text(formatCurrencyComaUSD(pageTotal));

					$('#totHide').val(pageTotal);

					terbilangENG();
				}
			});
		}

		// System for American Numbering 
		var th_val = ['', 'Thousand', 'Million', 'Billion', 'Trillion'];
		// System for uncomment this line for Number of English 
		// var th_val = ['','thousand','million', 'milliard','billion'];

		var dg_val = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];
		var tn_val = ['Ten', 'eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
		var tw_val = ['Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];

		function toWordsconver(s) {
			s = s.toString();
			s = s.replace(/[\, ]/g, '');
			if (s != parseFloat(s))
				return 'not a number ';
			var x_val = s.indexOf('.');
			if (x_val == -1)
				x_val = s.length;
			if (x_val > 15)
				return 'too big';
			var n_val = s.split('');
			var str_val = '';
			var sk_val = 0;
			for (var i = 0; i < x_val; i++) {
				if ((x_val - i) % 3 == 2) {
					if (n_val[i] == '1') {
						str_val += tn_val[Number(n_val[i + 1])] + ' ';
						i++;
						sk_val = 1;
					} else if (n_val[i] != 0) {
						str_val += tw_val[n_val[i] - 2] + ' ';
						sk_val = 1;
					}
				} else if (n_val[i] != 0) {
					str_val += dg_val[n_val[i]] + ' ';
					if ((x_val - i) % 3 == 0)
						str_val += 'hundred ';
					sk_val = 1;
				}
				if ((x_val - i) % 3 == 1) {
					if (sk_val)
						str_val += th_val[(x_val - i - 1) / 3] + ' ';
					sk_val = 0;
				}
			}
			if (x_val != s.length) {
				var y_val = s.length;
				str_val += 'And Cents ';
				for (var i = x_val + 1; i < y_val; i++)
					str_val += dg_val[n_val[i]] + ' ';
			}
			return "Says : ( " + str_val.replace(/\s+/g, ' ') + " Only )";
		}

		function terbilangENG() {
			var bilangan = document.getElementById("totHide").value;
			var kalimat = toWordsconver(bilangan);

			document.getElementById("totalBilangan").innerHTML = kalimat;
		}

		function cal_price(row) {
			var totAmount = 0;
			var rowData = t_invoice_list.row(row).data();
			var amount = rowData[5];

			var qtykoma = rowData[3];
			var qty = qtykoma.replace(/[^\d.-]/g, '');
			var unitPrice = $('#unit_price' + row).val();
			amount = parseFloat(unitPrice) * parseFloat(qty);
			
			$('input[id="amount'+row+'"]').val(parseFloat(amount));
			$("#amount-label"+row).html(formatCurrencyRupiah(amount));
			$("#unit_price-label"+row).html(formatCurrencyRupiah(parseFloat(unitPrice)));

			totAmount = formatCurrencyComa(amount);
			$('input[id="amount' + row + '"]').val(amount);

			var tempTotal = 0;
			$('input[name="amount[]"]').each(function() {
				if (this.value !== '' && this.value !== null)
					tempTotal = tempTotal + parseFloat(this.value);
				else
					tempTotal = tempTotal + 0;
			})

			$('#tdAmount_Total').html(formatCurrencyComaUSD(tempTotal));

			document.getElementById("totalBilangan").innerHTML = toWordsconver(tempTotal);
		}

		function redrawTable(row) {
			var rowData = t_invoice_list.row(row).data();

			rowData[4] = $('#unit_price' + row).val();
			rowData[6] = $('#amount' + row).val();
			// t_invoice_list.draw();
		}
	</script>