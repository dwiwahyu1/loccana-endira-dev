	<style>
		.col-customer {
			border: solid 1px #b2b8b7;
		}

		.dt-body-left {
			text-align: left;
			vertical-align: middle;
		}

		.dt-body-right {
			text-align: right;
			vertical-align: middle;
		}

		.dt-body-center {
			text-align: center;
			vertical-align: middle;
		}

		img {
			width: 40%;
			position: absolute;
			height: auto;
		}

		.titleReport {
			text-align: center;
		}

		.left-header {
			width: 30%;
			float: left;
			position: relative;
			text-align: center;
			padding-left: 35px;
			padding-right: 35px;
		}

		.right-header {
			position: relative;
			text-align: center;
			padding-left: 35px;
			padding-right: 35px;
		}

		.border-packing {
			border: 1px solid #ebeff2;
		}
		
		.nama-perusahaan-margin {
			margin-top: 30px;
		}

		.margin-row {
			margin-bottom: 30px
		}
		.margin-ttd {
			margin-bottom: 75px
		}
		
		.margin-nama {
			margin-bottom: 55px
		}
		
		.padding-box-nama {
			border: 1px solid #ebf2f2;
			padding: 10px 5px 10px 5px;
		}
	</style>

	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download Packing List" id="btn_download">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>

	<div class="row border-packing">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="left-header">
						<img src="<?php echo site_url(); ?>assets/images/logo-celebit.jpg" alt="logo-celebit">
					</div>
					<div class="right-header">
						<h1 id="titleCelebit">CELEBIT</h1>
						<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
						<h4 id="titleAlamat">BANDUNG FACTORY : JL.BUAH DUA RT.01/RW.04 RANCAEKEK - BANDUNG-INDONESIA</h4>
						<h4 id="titleTlp">TEL 62-22-7798 561/7798 542, FAX : 62-22-7798 562 E-MAIL : invoicing@celebit.com</h4>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-12 text-center">
				<h3 id="titleInvoice" style="font-weight:bold;">PACKING / WEIGHT LIST</h3>
			</div>
		</div>

		<div class="row margin-row">
			<div class="col-md-12">
				<div class="col-md-7">
					<div class="row">
						<table class="table table-bordered" id="tableCustomer">
							<thead>
								<tr>
									<th>
										<label class="control-label col-md-12" id="billTo1">BILL TO :</label>
										<div class="col-md-11 col-md-offset-1">
											<label class="control-label col-md-12" id="billTo2"><?php if (isset($invoice[0]['name_eksternal'])) { echo $invoice[0]['name_eksternal']; } ?></label><br>
											<label class="control-label col-md-12" id="billTo3"><?php if (isset($invoice[0]['eksternal_address'])) { echo $invoice[0]['eksternal_address']; } ?></label><br>
											<label class="control-label col-md-12" id="billTo4">
												Tel : <?php if (isset($invoice[0]['phone_1']) || isset($invoice[0]['phone_2'])) echo $invoice[0]['phone_1'].' / '.$invoice[0]['phone_2']; ?>
											</label><br>
											<label class="control-label col-md-12" id="billTo5">Fax<?php if (isset($invoice[0]['fax'])) {echo $invoice[0]['fax'];} ?></label><br>
										</div>
									</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				<div class="col-md-4 col-md-offset-1">
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="noInvoice1">No P/L</label>
							<label class="control-label col-md-1" id="noInvoice2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="noInvoice" name="noInvoice">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="tanggalInvoice1">Date</label>
							<label class="control-label col-md-1" id="tanggalInvoice2">:</label>
							<div class="col-md-8">
								<div class="input-group date">
									<input placeholder="<?php echo date('d-M-Y');?>" type="text" class="form-control datepicker" id="tanggalInvoice3" name="tanggalInvoice3" required="required" value="<?php if (isset($invoice[0]['tanggal_do'])) {																																																																					} ?>">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-th"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="paymentTerm1">Payment Term</label>
							<label class="control-label col-md-1" id="paymentTerm2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="paymentTerm" name="paymentTerm" required="required" value="" placeholder="Payment Term">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="shipmentBy1">Shipment By</label>
							<label class="control-label col-md-1" id="shipmentBy2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="shipmentBy" name="shipmentBy" required="required" value="" placeholder="Shipment By">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="etd1">ETD Vessel</label>
							<label class="control-label col-md-1" id="etd2">:</label>
							<div class="col-md-8">
								<div class="input-group date">
									<input placeholder="<?php date('d-M-Y'); ?>" type="text" class="form-control datepicker" id="etd" name="etd" required="required" value="<?php if(isset($invoice[0]['tanggal_do'])) {$date = date_create($invoice[0]['tanggal_do']); echo date_format($date,'d-M-Y'); } ?>">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-th"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="deliveryTerm1">Delivery Term</label>
							<label class="control-label col-md-1" id="deliveryTerm2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="deliveryTerm" name="deliveryTerm" required="required" value="" placeholder="Delivery Term">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="poNo1">PO No</label>
							<label class="control-label col-md-1" id="poNo2">:</label>
							<div class="col-md-8">
								<input placeholder="PO No" type="text" class="form-control" id="poNo" name="poNo" required="required" value="">
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="col-md-7">
					<div class="row">
						<table class="table table-bordered" id="tableCustomer2">
							<thead>
								<tr>
									<th>
										<label class="control-label col-md-12" id="shipTo1">SHIP TO :</label>
										<div class="col-md-11 col-md-offset-1">
											<label class="control-label" id="shipTo2"><?php if (isset($invoice[0]['name_eksternal'])) { echo $invoice[0]['name_eksternal']; } ?></label><br>
											<label class="control-label" id="shipTo3"><?php if (isset($invoice[0]['eksternal_address'])) { echo $invoice[0]['eksternal_address']; } ?></label><br>
											<label class="control-label" id="shipTo4">
												Tel : <?php if (isset($invoice[0]['phone_1']) || isset($invoice[0]['phone_2'])) echo $invoice[0]['phone_1'].' / '.$invoice[0]['phone_2']; ?>
											</label><br>
											<label class="control-label" id="shipTo5">Fax<?php if (isset($invoice[0]['fax'])) {echo $invoice[0]['fax'];} ?></label><br>
										</div>
										<div class="row">
											<label class="control-label col-md-2" id="att1">ATTN :</label>
											<div class="col-md-11">
												<input type="text" class="form-control" id="att" name="att" value="" placeholder="ATTN">
											</div>
										</div>
									</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				<div class="col-md-4 col-md-offset-1">
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="totAmount1">Tot. Amount</label>
							<label class="control-label col-md-1" id="totAmount2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="totAmount" name="totAmount" placeholder="Total Amount">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="shipForm1">Ship Form</label>
							<label class="control-label col-md-1" id="shipForm2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="shipForm" name="shipForm" required="required" value="" placeholder="Ship Form">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="shipTo1">Ship To</label>
							<label class="control-label col-md-1" id="shipTo2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="shipTo" name="shipTo" required="required" value="" placeholder="Ship To">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="awbNumber1">AWB Number</label>
							<label class="control-label col-md-1" id="awbNumber2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="awbNumber" name="awbNumber" required="required" value="" placeholder="AWB Number">
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	<hr style="margin-top: 35px;"></br>
	<div class="row">
		<div class="col-md-12">
			<table id="listPackingList" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th class="text-center" rowspan="2" style="vertical-align: middle;">No.</th>
						<th class="text-center" rowspan="2" style="vertical-align: middle;">CTN NO.</th>
						<th class="text-center" rowspan="2" style="vertical-align: middle;">PART NUMBER</th>
						<th class="text-center" rowspan="2" style="vertical-align: middle;">QTY BOX</th>
						<th class="text-center" rowspan="">TOTAL QTY</th>
						<th class="text-center" rowspan="">NETT WEIGHT</th>
					</tr>
					<tr>
						<th class="text-right" rowspan="">(PCS)<?php //if(isset($packing[0]['uom_symbol'])) echo '('.$packing[0]['uom_symbol'].')'; 
																										?></th>
						<th class="text-right" rowspan="">(KGM)<?php //if(isset($packing[0]['uom_symbol'])) echo '('.$packing[0]['uom_symbol'].')'; 
																										?></th>
					</tr>
				</thead>
				<tbody></tbody>
				<tfoot>
					<tr>
						<th colspan="4" style="text-align: left;">TOTAL</th>
						<th id="tdQty"></th>
						<th id="tdNet"></th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>

	<div class="row margin-row">
		<div class="col-md-12">
			<div class="col-md-3 col-md-offset-9 text-center">
				<label class="control-label margin-row" id="namaPerusahaan">PT. CELEBIT CIRCUIT TECH. IND</label>
				<input class="form-control text-center" id="nama_ttd" name="nama_ttd" placeholder="NAMA TANDA TANGAN" value="">
			</div> 
		</div>
	</div>
	</div>

	<script type="text/javascript">
		var dataImage = null;
		var t_packingList;
		var valas_id = <?php if(isset($packing[0]['valas_id'])) echo $packing[0]['valas_id'];?>;
		
		$(document).ready(function() {
			dtPacking();
			$(".date").datepicker({
				format: 'yyyy-mm-dd',
				autoclose: true,
				todayHighlight: true,
			});
			$("#noInvoice3").on('keyup',function(){
				$("#labelNoInvoice").text($("#noInvoice3").val());
			})
			$('#btn_download').click(function() {
				var doc = new jsPDF('p', 'mm', 'letter');
				var imgData = dataImage;
				var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
				var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

				// FOOTER
				doc.setTextColor(100);
				doc.addImage(imgData, 'JPEG', 30, 5, 25, 25)
				doc.setFontSize(10);
				doc.text($('#titleCelebit').html(), 120, 10, 'center');
				doc.setFontSize(8);
				doc.text($('#titlePerusahaan').html(), 120, 15, 'center');
				doc.setFontSize(8);
				doc.text($('#titleAlamat').html(), 120, 20, 'center');
				doc.setFontSize(8);
				doc.text($('#titleTlp').html(), 120, 25, 'center');

				doc.setDrawColor(116, 119, 122);
				doc.setLineWidth(0.1);
				doc.line(4, 30, 500, 30);

				doc.setFontSize(11);
				doc.text($('#titleInvoice').html(), pageWidth / 2, 40, 'center');

				// doc.setDrawColor(116, 119, 122);
				// doc.setLineWidth(0.1);
				// doc.line(4, 45, 110, 45);
				// doc.setLineWidth(0.1);
				// doc.line(4, 45, 4, 68);
				// doc.setLineWidth(0.1);
				// doc.line(110, 45, 110, 68);
				// doc.setLineWidth(0.1);
				// doc.line(4, 68, 110, 68);

				doc.setFontSize(7);
				// doc.text($('#namaCustomer').html(), 6, 50);
				// doc.text($('#lokasiCustomer').html(), 6, 55);
				// doc.text($('#alamatCustomer').html(), 6, 60);
				// doc.text($('#divCustomer').html(), 6, 65);
				doc.autoTable({
					html: '#tableCustomer',
					theme: 'plain',
					styles: {
						fontSize: 8,
						lineColor: [116, 119, 122],
						lineWidth: 0.1,
						cellWidth: 'auto',

					},
					margin: 4,
					tableWidth: ((pageWidth / 2)),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 45
				});

				doc.setFontSize(7);
				// doc.autoTable({
				// 	html: '#tableDataNoInvoice',
				// 	theme: 'plain',
				// 	styles: {
				// 		fontSize: 8,
				// 		lineColor: [116, 119, 122],
				// 		lineWidth: 0.1,
				// 		cellWidth: 'auto',

				// 	},
				// 	margin: (pageWidth / 2) + 10,
				// 	tableWidth: ((pageWidth / 2)) - 30,
				// 	headStyles: {
				// 		valign: 'middle',
				// 		halign: 'center',
				// 	},
				// 	rowPageBreak: 'auto',
				// 	showHead: 'firstPage',
				// 	showFoot: 'lastPage',
				// 	startY: 45
				// });
				doc.text($('#noInvoice1').html(), ((pageWidth / 2) + 35), 50, 'left');
				doc.text($('#noInvoice2').html(), ((pageWidth / 2) + 65), 50, 'left');
				doc.text($('#noInvoice').val(), ((pageWidth / 2) + 70), 50, 'left');

				doc.text($('#tanggalInvoice1').html(), ((pageWidth / 2) + 35), 60, 'left');
				doc.text($('#tanggalInvoice2').html(), ((pageWidth / 2) + 65), 60, 'left');
				doc.text($('#tanggalInvoice').val(), ((pageWidth / 2) + 70), 60, 'left');

				doc.autoTable({
					html: '#listPackingList',
					theme: 'plain',
					styles: {
						fontSize: 8,
						lineColor: [116, 119, 122],
						lineWidth: 0.1,
						cellWidth: 'auto',

					},
					margin: 4,
					tableWidth: (pageWidth - 8),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					didParseCell: function(data) {
						if (data.table.foot[0]) {
							if (data.table.foot[0].cells[4]) {
								data.table.foot[0].cells[4].styles.halign = 'right';
							}
							if (data.table.foot[0].cells[5]) {
								data.table.foot[0].cells[5].styles.halign = 'right';
							}
							if (data.table.foot[0].cells[6]) {
								data.table.foot[0].cells[6].styles.halign = 'right';
							}
						}
					},
					columnStyles: {
						0: {
							tableWidth: 10,
							halign: 'center'
						},
						1: {
							tableWidth: 18,
							halign: 'center'
						},
						2: {
							halign: 'center'
						},
						3: {
							halign: 'center',
							falign: 'center'
						},
						4: {
							halign: 'right',
							falign: 'right'
						},
						5: {
							halign: 'right',
							falign: 'right'
						},
						6: {
							halign: 'right',
							falign: 'right'
						},
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 70
				});

				var x = pageWidth * 80 / 100;
				var y = pageHeight * 35 / 100;

				doc.setFontSize(10);
				doc.text($('#namaPerusahaan').html(), x, y + 40, 'center');
				doc.text($('#nama_ttd').val(), x, y + 60, 'center');

				doc.save('PACKING LIST <?php echo date('d-M-Y'); ?>.pdf');
			});

		});

		function toDataURL(url, callback) {
			var xhr = new XMLHttpRequest();
			xhr.onload = function() {
				var reader = new FileReader();
				reader.onloadend = function() {
					callback(reader.result);
				}
				reader.readAsDataURL(xhr.response);
			};
			xhr.open('GET', url);
			xhr.responseType = 'blob';
			xhr.send();
		}

		toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
			dataImage = dataUrl;
		})

		// function numberCNT(row) {
			// var rowData = t_packingList.row(row).data();
			// console.log(rowData);
			// var no_cnt = rowData[1];
			// var tot_qty = rowData[7];
			// var tot_net = rowData[8];
			// var tot_gross = rowData[9];

			// var no_kont = $('#no_kontainer' + row).val();
			// $('#lb_no_kontainer' + row).html(no_kont);
			// // console.log(tot_net,tot_gross);

			// $('#tdQty').html(tot_qty);
			// $('#tdNet').html(tot_net);
			// $('#tdGross').html(tot_gross);
		// }

		function dtPacking() {
			
			t_packingList = $('#listPackingList').DataTable({
				"processing": true,
				"searching": false,
				"responsive": true,
				"lengthChange": false,
				"info": false,
				"bSort": false,
				"ajax": {
					"type": "GET",
					"url": "<?php echo base_url() . 'delivery_order/detail_delivery_order_packing/' . $id_do; ?>",
					"dataSrc": function(response) {
						if(valas_id == 1) {
							$('#tdQty').html(formatCurrencyComaIDR(response.total_qty));
							$('#tdNet').html(formatCurrencyComaIDR(response.total_net));
							$('#tdGross').html(formatCurrencyComaIDR(response.total_gross));
						}else{
							$('#tdQty').html(formatCurrencyComaUSD(response.total_qty));
							$('#tdNet').html(formatCurrencyComaUSD(response.total_net));
							$('#tdGross').html(formatCurrencyComaUSD(response.total_gross));
						}
						return response.data;
					}
				},
				"columnDefs": [{
					"targets": [0],
					"searchable": false,
					"visible": true,
					"className": 'dt-body-center',
					"width": 10
				}, {
					"targets": [1],
					"searchable": false,
					"className": 'dt-body-center',
					"width": 90
				}, {
					"targets": [2],
					"searchable": false,
					"className": 'dt-body-center',
					"width": 175
				}, {
					"targets": [3],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 175
				}, {
					"targets": [4],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 150
				}, {
					"targets": [5],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 150
				}, {
					"targets": [6],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 150
				}]
			})
		}
	</script>