	<style>
		@primary-color: #242222;

		.col-customer {
			border: solid 1px #b2b8b7;
		}

		.dt-body-left {
			text-align: left;
			vertical-align: middle;
		}

		.dt-body-right {
			text-align: right;
			vertical-align: middle;
		}

		.dt-body-center {
			text-align: center;
			vertical-align: middle;
		}

		img {
			max-width: 50%;
			height: auto;
		}

		#header-celebit .width-img {
			width: 25%;
			border-right-style: hidden;
		}

		.nama-perusahaan-margin {
			margin-top: 30px;
		}

		.margin-ttd {
			margin-bottom: 65px
		}

		#header-celebit .border-none {
			border: none;
			border-bottom: none;


			#header-invoice-num .border-none {
				border: none;
				border-bottom-style: hidden;
			}

			#header-celebit-invoice,
			#header-invoice-num .border-none {
				border: none;
				border-bottom-style: hidden;
			}

			#header-invoice-num .border-right-table {
				border-right-style: hidden;
			}

			.border-table {
				border: 1px solid #242222;
			}

			.border-table-invoice {
				border: 1px solid #242222;
				border-left-style: initial;
				border-right-style: initial;
			}

			.border-table-supplier {
				border: 1px solid #242222;
				border-top-style: initial;
				border-left-style: initial;
			}

			.border-table-bill {
				border: 1px solid #242222;
				border-top-style: initial;
				border-right-style: initial;
				border-left-style: initial;
			}

			.border-luar-table {
				border-style: double;
				border: 1px solid #242222;
			}
	</style>


	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download Invoice" id="btn_download">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<table id="header-celebit" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom:0;">
				<thead>
					<tr>
						<th class="width-img border-right-table">
							<div class="text-center">
								<img src="<?php echo site_url(); ?>assets/images/logo-celebit.jpg" alt="logo-celebit">
							</div>
						</th>
						<th>
							<div class="col-md-12 titleReport">
								<h1 id="titleCelebit">CELEBIT</h1>
								<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
								<h4 id="titleAlamat">BANDUNG FACTORY : JL.BUAH DUA RT.01/RW.04 RANCAEKEK - BANDUNG-INDONESIA</h4>
								<h4 id="titleTlp">TEL 62-22-7798 561/7798 542, FAX : 62-22-7798 562 E-MAIL : invoicing@celebit.com</h4>
							</div>
						</th>
					</tr>
				</thead>
			</table>

			<table id="header-celebit-invoice" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom:0;">
				<thead>
					<tr>
						<th colspan="2" class="text-center">
							<h4 id="titleInvoice" style="font-weight:bold;">INVOICE</h4>
						</th>
					</tr>
					<tr>
						<th>
							<div class="item-form-group">
								<label class="control-label col-md-3" id="titleInvNumber">Invoice Number :</label>
								<div class="col-md-9">
									<input type="text" class="form-control" id="invNumber" name="invNumber" value="" placeholder="Invoice Number">
								</div>
							</div>
						</th>
						<th>
							<div class="item-form-group">
								<label class="control-label col-md-3" id="titleInvDate">Invoice Date :</label>
								<div class="col-md-9">
									<div class="input-group date">
										<input placeholder="<?php echo date('Y-m-d'); ?>" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="invDate" name="invDate" placeholder="<?php echo date('Y-m-d'); ?>" value="" required>
										<div class="input-group-addon">
											<span class="glyphicon glyphicon-th"></span>
										</div>
									</div>
								</div>
							</div>
						</th>
					</tr>
					<tr>
						<th class="text-center">
							<label class="control-label" id="titleAddressSupplier">Suppliers Address :</label>
						</th>
						<th class="text-center">
							<label class="control-label" id="titleAddressBill">Bill To Address :</label>
						</th>
					</tr>
					<tr>
						<th rowspan="3" style="width:50%">
							<label class="control-label" id="titleSingapore">
								Singapore Office :</br>
								CELEBIT CIRCUIT TECHNOLOGY S.P. Ltd</br>
								35 Kallang Puding Road</br>
								349314 09-04 Tong Lee Building</br>
								SG 349314- SINGAPORE</br>
								Phone : 0065 66 786 2148</br>
								attn : Mr.C.K. Cheng</br>
								Bandung Factory</br>
								PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</br>
								Jl. Buah Dua No. 168 RT.01/RW.04</br>
								Rancaekek - Bandung</br>
								Jawa Barat - Indonesia</br>
								Phone : 006222 779 8561; Fax # 006222 779 8562</br>
								Attn : Endang Wati
							</label>
						</th>
						<th style="width:50%">
							<label class="control-label" id="titleNameEksternal"><?php if (isset($invoice[0]['name_eksternal'])) echo $invoice[0]['name_eksternal']; ?></label><br>
							<label class="control-label" id="titleEksternalAddress"><?php if (isset($invoice[0]['eksternal_address'])) echo $invoice[0]['eksternal_address']; ?></label>
						</th>
					</tr>
					<tr>
						<th class="text-center" style="width:50%">
							<label class="control-label" id="titleShipAddress">Ship To Address</label>
						</th>
					</tr>
					<tr>
						<th style="width:50%">
							<label class="control-label" id="titleShipAddress"><?php if (isset($invoice[0]['ship_address'])) echo $invoice[0]['ship_address']; ?></label><br><br><br>
							<label class="control-label" id="titlePhone1">+36 (3) 8118893</label><br>
							<label class="control-label" id="titleAttn1">Ms. Anita Soltesz</label>
						</th>
					</tr>
					<tr>
						<td colspan="2"><label class="control-label" id="titleBeing">Being Charge for Delivery of Goods Period :</label></td>
					</tr>
				</thead>
			</table>
		</div>

		<div class="col-md-12">
			<table id="listinvoice" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom:0;">
				<thead>
					<tr>
						<th rowspan="2" style="vertical-align:middle">No</th>
						<th class="text-center" colspan="2" style="vertical-align:middle">Description</th>
						<th rowspan="2" style="vertical-align:middle">Qty (Pcs)</th>
						<th rowspan="2" style="vertical-align:middle">Unit Price (<?php if (isset($invoice[0]['symbol_valas'])) {
																																																											echo $invoice[0]['symbol_valas'];
																																																										} ?>)</th>
						<th rowspan="2" style="vertical-align:middle">Unit Price Hid(<?php if (isset($invoice[0]['symbol_valas'])) {
																																																													echo $invoice[0]['symbol_valas'];
																																																												} ?>)</th>
						<th rowspan="2" style="vertical-align:middle">Amount (<?php if (isset($invoice[0]['symbol_valas'])) {
																																																									echo $invoice[0]['symbol_valas'];
																																																								} ?>)</th>
						<th rowspan="2" style="vertical-align:middle">Amount Hid(<?php if (isset($invoice[0]['symbol_valas'])) {
																																																										echo $invoice[0]['symbol_valas'];
																																																									} ?>)</th>
					</tr>
					<tr>
						<th class="text-center">Part No</th>
						<th class="text-center">PO</th>
					</tr>
				</thead>
				<tbody></tbody>
				<tr>
					<td colspan="5"></td>
				</tr>
				<tr>
					<th colspan="4" style="text-align: left;border-bottom-style:none;border-right-style: none; ">TOTAL <?php if (isset($invoice[0]['symbol_valas'])) {
																																																																															echo $invoice[0]['symbol_valas'];
																																																																														} ?></th>
					<th class="pull-right" id="rupiah" style="border-right-style: none;"><?php if (isset($invoice[0]['symbol'])) {
																																																											echo $invoice[0]['symbol'];
																																																										} ?></th>
					<th class="text-right" id="tdAmount_Total">
						<!--<label class="pull-right" id="total-label" name="ppn-label" />-->
					</th>
				</tr>
				<tr>
					<th colspan="7" id="totalBilangan" name="totalBilangan"></th>
				</tr>
				<tr>
					<th colspan="3" style="width:50%;border-right-style:none;border-bottom-style:none;">
						<div class="item-form-group">
							<label class="control-label col-md-3">Term Of Payment</label>
							<div class="col-md-9">
								<input type="text" style="width:100%" class="form-control" id="top" name="top" placeholder="Days Net" value="0" lang="en-150" required>
							</div>
						</div>
					</th>
					<th colspan="3" style="width:50%;border-bottom-style:none;">
						<div class="item-form-group">
							<label class="control-label col-md-3">Delivery By</label>
							<div class="col-md-9">
								<input type="text" style="width:100%" class="form-control" id="delveryBy" name="delveryBy" placeholder="Courier" value="" required>
							</div>
						</div>
					</th>
				</tr>
				<tr>
					<th colspan="3" style="width:50%;border-right-style:none;border-bottom-style:none;">
						<div class="item-form-group">
							<label class="control-label col-md-3">Term Of Delivery</label>
							<div class="col-md-9">
								<input type="text" style="width:100%" class="form-control" id="tod" name="tod" placeholder="Term Of Delivery" value="" required>
							</div>
						</div>
					</th>
					<th colspan="3" style="width:50%;border-bottom-style:none;">
						<div class="item-form-group">
							<label class="control-label col-md-3">Delivery Method</label>
							<div class="col-md-9">
								<input type="text" style="width:100%" class="form-control" id="delveryMethod" name="delveryMethod" placeholder="Delivery Method" value="" required>
							</div>
						</div>
					</th>
				</tr>
				<tr>
					<th colspan="3" style="width:50%;border-right-style:none;border-bottom-style:none;">
						<div class="item-form-group">
							<label class="control-label col-md-3">PO Number</label>
							<div class="col-md-9">
								<input type="text" style="width:100%" class="form-control" id="PONumber" name="PONumber" placeholder="PO Number" value="" required>
							</div>
						</div>
					</th>
					<th colspan="3" style="width:50%;border-bottom-style:none;">
						<div class="item-form-group">
							<label class="control-label col-md-3">ETD</label>
							<div class="col-md-9">
								<div class="input-group date" style="width:100%">
									<input placeholder="<?php echo date('Y-m-d'); ?>" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="etd" name="etd" placeholder="<?php echo date('Y-m-d'); ?>" value="" required>
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-th"></span>
									</div>
								</div>
							</div>
						</div>
					</th>
				</tr>
				<tr>
					<th colspan="3" style="width:50%;border-right-style:none;border-bottom-style:none;">
						<div class="item-form-group">
							<label class="control-label col-md-3">Harmony System #</label>
							<div class="col-md-9">
								<input type="text" style="width:100%" class="form-control" id="harmonySystem" name="harmonySystem" placeholder="Harmony System" value="" required>
							</div>
						</div>
					</th>
					<th colspan="3" style="width:50%;border-bottom-style:none;">
						<div class="item-form-group">
							<label class="control-label col-md-3">Vessel Name</label>
							<div class="col-md-9">
								<input type="text" style="width:100%" class="form-control" id="vesselName" name="vesselName" placeholder="Vessel Name" value="" required>
							</div>
						</div>
					</th>
				</tr>
				<tr>
					<th colspan="3" style="width:50%;border-right-style:none;border-bottom-style:none;">
						<div class="item-form-group">
							<label class="control-label col-md-3">Shippment Form</label>
							<div class="col-md-9">
								<input type="text" style="width:100%" class="form-control" id="shippmentForm" name="shippmentForm" placeholder="Shippment Form" value="" required>
							</div>
						</div>
					</th>
					<th colspan="3" style="width:50%;border-bottom-style:none;">
						<div class="item-form-group">
							<label class="control-label col-md-3">AWB Number</label>
							<div class="col-md-9">
								<input type="text" style="width:100%" class="form-control" id="awbNumber" name="awbNumber" placeholder="AWB Number" value="" required>
							</div>
						</div>
					</th>
				</tr>
				<tr>
					<th colspan="3" style="width:50%;border-right-style:none;border-bottom-style:none;">
						<div class="item-form-group">
							<label class="control-label col-md-3">Shippment To</label>
							<div class="col-md-9">
								<input type="text" style="width:100%" class="form-control" id="shippmentTo" name="shippmentTo" placeholder="Shippment To" value="" required>
							</div>
						</div>
					</th>
					<th colspan="3" style="width:50%;border-bottom-style:none;">
						<div class="item-form-group">
							<label class="control-label col-md-3">Nett/ Gross Weight</label>
							<div class="col-md-9">
								<input type="text" style="width:100%" class="form-control" id="nettGross" name="nettGross" placeholder="Nett / Gross Weight" value="" required>
							</div>
						</div>
					</th>
				</tr>
				<tr>
					<th colspan="3" style="width:50%; border: 1px solid #ebf2f2">
						<div class="item-form-group">
							<label class="control-label col-md-12">BANK TRANSFER :</label><br>
							<label class="control-label col-md-12">BANK NAME : BANK NEGARA INDONESIA (BNI)</label><br>
							<label class="control-label col-md-12">Jl. Buah Batu No. 189 D Bandung</label><br>
							<label class="control-label col-md-12">Phone : 022-7313371</label><br>
							<div class="item-form-group">
								<label class="control-label col-md-7">Account Number (US $)</label>
								<div class="col-md-5">
									<label class="control-label col-md-1">: </label>
									<div class="col-md-11">
										<input type="text" style="width:100%" class="form-control" id="accountNumber" name="accountNumber" placeholder="Account Number" value="" required>
									</div>
								</div>
							</div><br>
							<div class="item-form-group">
								<label class="control-label col-md-7">Swift Code</label>
								<div class="col-md-5">
									<label class="control-label col-md-1">: </label>
									<div class="col-md-11">
										<input type="text" style="width:100%" class="form-control" id="swiftCode" name="swiftCode" placeholder="Swift Code" value="" required>
									</div>
								</div>
							</div><br>
							<label class="control-label col-md-12">Account Name : PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</label>
						</div>
					</th>
					<th class="text-center" colspan="3" style="width:50%">
						<label class="control-label col-md-12" style="margin-bottom: 120px;">Signature</label><br>
						<input type="text" style="width:50%" class="form-control text-center" id="signature" name="signature" placeholder="Signature Name" value="" required>
					</th>
				</tr>
			</table>
		</div>
	</div>

	<div class="row">
		<input type="hidden" id="totHide" name="totHide">
	</div>
	<div class="row">
		<div class="col-md-12">
			<label class="control-label pull-right" id="kode_report">F_MCI-002</label>
		</div>
		<div class="col-md-12">
			<label class="control-label pull-right" id="kode_report_detail">REV : 00</label>
		</div>
	</div>
	<script type="text/javascript">
		var dataImage = null;
		var invoice = <?php echo count($invoice); ?>;
		var nama_valas = '<?php if (isset($invoice[0]['nama_valas'])) {echo $invoice[0]['nama_valas'];} ?>';

		var t_invoice_list;

		$(document).ready(function() {
			dtInvoice();
			$(".date").datepicker({
				format: 'yyyy-mm-dd',
				autoclose: true,
				todayHighlight: true,
			});
			$("#notes-input").on("keyup", function() {
				$("#notes-label").text($("#notes-input").val());
			})


			for (var i = 0; i < invoice.length; i++) {
				$("#unit_price" + i).on("keyup", function() {
					total = parseFloat($("#total-label").text().replace(/[^\d.-]/g, ''));

					// console.log(parseFloat($("#total-label").text())+parseFloat($("#ppn-label").text()),parseFloat($("#total-label").text()),parseFloat($("#ppn-label").text()));
					$("#tdAmount_Total").html(formatCurrencyComa(total));
					$("#totalBilangan").html(terbilang(total));
				})
			}

			$('#btn_download').click(function() {
				var doc = new jsPDF('p', 'mm', 'letter');
				var imgData = dataImage;
				var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
				var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
				console.log($("#notes-label").val());
				// FOOTER
				doc.setTextColor(100);
				doc.addImage(imgData, 'JPEG', 5, 5, 25, 25)
				doc.setFontSize(12);
				doc.text($('#titleCelebit').html(), 33, 10, 'left');
				doc.setFontSize(10);
				doc.text($('#titlePerusahaan').html(), 33, 15, 'left');
				doc.setFontSize(11);
				doc.text($('#titleAlamat').html(), 33, 20, 'left');
				doc.setFontSize(11);
				doc.text($('#titleTlp').html(), 33, 25, 'left');

				doc.setDrawColor(116, 119, 122);
				doc.setLineWidth(0.1);
				doc.line(4, 30, 500, 30);

				doc.setFontSize(11);
				doc.text($('#titleInvoice').html(), pageWidth / 2, 40, 'center');

				doc.setDrawColor(116, 119, 122);
				doc.setLineWidth(0.2);
				doc.line(4, 45, 110, 45);
				doc.setLineWidth(0.2);
				doc.line(4, 45, 4, 65);
				doc.setLineWidth(0.2);
				doc.line(110, 45, 110, 65);
				doc.setLineWidth(0.2);
				doc.line(4, 65, 110, 65);

				doc.setFontSize(7);
				doc.text($('#namaCustomer').html(), 6, 50);
				doc.text($('#lokasiCustomer').html(), 6, 55);
				doc.text($('#alamatCustomer').html(), 6, 60);
				doc.text($('#divCustomer').html(), 6, 65);

				doc.setFontSize(7);
				doc.text($('#noInvoice1').html(), 115, 50, 'left');
				doc.text($('#noInvoice2').html(), 128, 50, 'left');
				doc.text($('#noInvoice3').val(), 135, 50, 'left'); //dari inputan

				doc.text($('#tanggalInvoice1').html(), 115, 57, 'left');
				doc.text($('#tanggalInvoice2').html(), 128, 57, 'left');
				doc.text($('#tanggalInvoice3').val(), 135, 57, 'left');

				doc.text($('#dueDateInvoice1').html(), 115, 64, 'left');
				doc.text($('#dueDateInvoice2').html(), 128, 64, 'left');
				doc.text($('#dueDateInvoice3').val(), 135, 64, 'left');

				doc.autoTable({
					html: '#listinvoice',
					theme: 'plain',
					styles: {
						fontSize: 8,
						lineColor: [116, 119, 122],
						lineWidth: 0.1,
						cellWidth: 'auto',

					},
					margin: 4.5,
					tableWidth: (pageWidth - 20),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					didParseCell: function(data) {
						if (data.table.foot[0]) {
							if (data.table.foot[0].cells[4]) {
								data.table.foot[0].cells[4].styles.halign = 'right';
							}
							if (data.table.foot[0].cells[5]) {
								data.table.foot[0].cells[5].styles.halign = 'right';
							}
						}
					},
					columnStyles: {
						0: {
							tableWidth: 10,
							halign: 'center'
						},
						1: {
							tableWidth: 10,
							halign: 'left'
						},
						2: {
							halign: 'center'
						},
						3: {
							halign: 'right'
						},
						4: {
							halign: 'right'
						},
						5: {
							halign: 'right',
							falign: 'left'
						},
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 70
				});

				doc.setFontSize(8);
				doc.setFontType('bold');
				doc.text($('#kode_report').html(), 205, pageHeight - 5, 'right');
				doc.text($('#kode_report_detail').html(), 205, pageHeight - 10, 'right');

				var x = pageWidth * 70 / 100;
				var y = pageHeight * 75 / 100;
				doc.setFontSize(10);
				doc.text("PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA ", x, y, 'center');
				doc.text($('#penandaTangan').val(), x, y + 30, 'center');
				doc.save('INVOICE <?php echo $invoice[0]['tanggal_do']; ?>.pdf');
			});
		});

		function toDataURL(url, callback) {
			var xhr = new XMLHttpRequest();
			xhr.onload = function() {
				var reader = new FileReader();
				reader.onloadend = function() {
					callback(reader.result);
				}
				reader.readAsDataURL(xhr.response);
			};
			xhr.open('GET', url);
			xhr.responseType = 'blob';
			xhr.send();
		}

		toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
			//console.log('RESULT:', dataUrl)
			dataImage = dataUrl;
		})

		function dtInvoice() {
			t_invoice_list = $('#listinvoice').DataTable({
				"processing": true,
				"searching": false,
				"responsive": true,
				"lengthChange": false,
				"info": false,
				"bSort": false,
				"ajax": {
					"type": "GET",
					"url": "<?php echo base_url() . 'delivery_order/delivery_list_invoice/' . $id_do; ?>",
					"dataSrc": function(response) {
						$('#tdAmount_Total').html(formatNumber(response.total_amounts));
						return response.data;
					}
				},
				"columnDefs": [{
					"targets": [0],
					"searchable": false,
					"className": 'dt-body-center',
					"width": 10
				}, {
					"targets": [1],
					"searchable": false,
					"className": 'dt-body-center',
					"width": 250
				}, {
					"targets": [2],
					"searchable": false,
					"className": 'dt-body-center',
					"width": 250
				}, {
					"targets": [3],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 150
				}, {
					"targets": [4],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 150
				}, {
					"targets": [5, 7],
					"searchable": false,
					"visible": false,
					"className": 'dt-body-right',
					"width": 60
				}, {
					"targets": [6],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 150
				}],
				"footerCallback": function(row, data, start, end, display) {
					var api = this.api(),
						data;
					var total = 0;
					// Remove the formatting to get integer data for summation
					var intVal = function(i) {
						return typeof i === 'string' ?
							i.replace(/[\$,]/g, '') * 1 :
							typeof i === 'number' ?
							i : 0;
					};

					// Total over all pages
					total = api
						.column(5)
						.data()
						.reduce(function(a, b) {
							total = total + intVal(a) + intVal(b);
							return (intVal(a) + intVal(b)) * (110 / 100);
						}, 0);

					// Total over this page
					pageTotal = api
						.column(5, {
							page: 'current'
						})
						.data()
						.reduce(function(a, b) {
							total = total + intVal(a) + intVal(b);
							return (intVal(a) + intVal(b));
						}, 0);

					// Update footer
					$(api.column(5).footer()).html(
						'<?php $invoice[0]['symbol_valas']; ?>' + formatCurrencyComa(pageTotal)
					)
					$("#total-label").text(formatCurrencyComaUSD(pageTotal));

					$('#totHide').val(pageTotal);


					terbilangENG();
				}
			});
		}

		// System for American Numbering 
		var th_val = ['', 'Thousand', 'Million', 'Billion', 'Trillion'];
		// System for uncomment this line for Number of English 
		// var th_val = ['','thousand','million', 'milliard','billion'];

		var dg_val = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];
		var tn_val = ['Ten', 'eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
		var tw_val = ['Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];

		function toWordsconver(s) {
			s = s.toString();
			s = s.replace(/[\, ]/g, '');
			if (s != parseFloat(s))
				return 'not a number ';
			var x_val = s.indexOf('.');
			if (x_val == -1)
				x_val = s.length;
			if (x_val > 15)
				return 'too big';
			var n_val = s.split('');
			var str_val = '';
			var sk_val = 0;
			for (var i = 0; i < x_val; i++) {
				if ((x_val - i) % 3 == 2) {
					if (n_val[i] == '1') {
						str_val += tn_val[Number(n_val[i + 1])] + ' ';
						i++;
						sk_val = 1;
					} else if (n_val[i] != 0) {
						str_val += tw_val[n_val[i] - 2] + ' ';
						sk_val = 1;
					}
				} else if (n_val[i] != 0) {
					str_val += dg_val[n_val[i]] + ' ';
					if ((x_val - i) % 3 == 0)
						str_val += 'hundred ';
					sk_val = 1;
				}
				if ((x_val - i) % 3 == 1) {
					if (sk_val)
						str_val += th_val[(x_val - i - 1) / 3] + ' ';
					sk_val = 0;
				}
			}
			if (x_val != s.length) {
				var y_val = s.length;
				str_val += 'And Cents ';
				for (var i = x_val + 1; i < y_val; i++)
					str_val += dg_val[n_val[i]] + ' ';
			}
			return "Says : ( " + str_val.replace(/\s+/g, ' ') + " Only )";
		}

		function terbilangENG() {
			var bilangan = document.getElementById("totHide").value;
			var kalimat = toWordsconver(bilangan);

			document.getElementById("totalBilangan").innerHTML = kalimat;
		}

		function cal_price(row) {
			var totAmount = 0;
			var rowData = t_invoice_list.row(row).data();
			var amount = rowData[5];

			var qtykoma = rowData[3];
			var qty = qtykoma.replace(/[^\d.-]/g, '');
			var unitPrice = $('#unit_price' + row).val();
			amount = parseFloat(unitPrice) * parseFloat(qty);

			totAmount = formatCurrencyComa(amount);
			
			$('input[id="amount'+row+'"]').val(parseFloat(amount));
			$("#amount-label"+row).html(formatCurrencyRupiah(amount));
			$("#unit_price-label"+row).html(formatCurrencyRupiah(parseFloat(unitPrice)));

			var tempTotal = 0;
			$('input[name="amount[]"]').each(function() {
				if (this.value !== '' && this.value !== null)
					tempTotal = tempTotal + parseFloat(this.value);
				else
					tempTotal = tempTotal + 0;
			})

			$('#tdAmount_Total').html(formatCurrencyComaUSD(tempTotal));

			document.getElementById("totalBilangan").innerHTML = toWordsconver(tempTotal);
		}

		function redrawTable(row) {
			var rowData = t_invoice_list.row(row).data();

			rowData[4] = $('#unit_price' + row).val();
			// t_invoice_list.draw();
		}
	</script>