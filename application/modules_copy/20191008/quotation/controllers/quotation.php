<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Quotation extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('quotation/quotation_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
    * This function is redirect to index quotation page
    * @return Void
    */
	public function index() {
		$this->template->load('maintemplate', 'quotation/views/index');
	}

	/**
    * This function is used for showing quotation list
    * @return Array
    */
	function lists() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;
		$order_fields = array('', 'id', 'name_eksternal');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->quotation_model->lists($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		foreach ($list['data'] as $k => $v) {
	      $id = '<div class="changed_status" onClick="detail_quotation(\'' . $v['id'] . '\')">';
	      $id .=      $v['id'];
	      $id .= '</div>';

	      $actions = '';
	      if($v['status'] == "On Quotation"){
		      $actions .= '<div class="btn-group">';
		      $actions .= '   <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edit_quotation(\'' . $v['id'] .'\')">';
		      $actions .= '       <i class="fa fa-edit"></i>';
		      $actions .= '   </button>';
		      $actions .= '</div>';
	 	  }

	      $actions .= '<div class="btn-group">';
	      $actions .= '   <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="delete_quotation(\'' . $v['id'] . '\')">';
	      $actions .= '       <i class="fa fa-trash"></i>';
	      $actions .= '   </button>';
	      $actions .= '</div>';

	      if($v['status'] == "On Quotation"){
	          $actions .= '<div class="btn-group">';
	          $actions .= '   <button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Approval" onClick="approval_quotation(\'' . $v['id'] . '\')">';
	          $actions .= '       <i class="fa fa-check"></i>';
	          $actions .= '   </button>';
	          $actions .= '</div>';
	      }

			array_push($data, array(
				$id,
				$v['name_eksternal'],
				$v['issue_date'],
				$v['total_products'],
		        number_format($v['total_normal_price'],2,",","."),
		        number_format($v['total_discount_price'],2,",","."),
		        $v['nama_valas'],
		        $v['price_term'],
		        $v['status'],
				$actions
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

  /**
    * This function is redirect to add quotation page
    * @return Void
    */
  public function add() {
      $valas = $this->quotation_model->valas();
      $customer = $this->quotation_model->customer();
      $item = $this->quotation_model->item();

      $data = array(
            'valas' => $valas,
            'customer' => $customer,
            'item' => $item
      );

      $this->load->view('add_modal_view',$data);
  }

  /**
    * This function is redirect to edit quotation page
    * @return Void
    */
  public function edit($id) {
  	  $valas = $this->quotation_model->valas();
      $customer = $this->quotation_model->customer();
      $item = $this->quotation_model->item();
      $po_detail = $this->quotation_model->po_detail($id);
      $order_detail = $this->quotation_model->order_detail($id);

      $data = array(
      	  'valas' => $valas,
          'customer' => $customer,
          'item' => $item,
          'po_detail'    => $po_detail,
          'order_detail'    => $order_detail
      );

      $this->load->view('edit_modal_view',$data);
  }

  /**
    * This function is used to add quotation data
      * @return Array
    */
  public function add_quotation() {
		$data   = file_get_contents("php://input");
		$params = json_decode($data,true);
    
		$this->quotation_model->add_quotation($params);

		$msg = 'Berhasil menambah data quotation';

		$result = array(
			'success' => true,
			'message' => $msg
		);

		$this->log_activity->insert_activity('insert', $msg. ' dengan ID Customer ' .$params['cust_id']);
		

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  /**
      * This function is used to delete quotation data
      * @return Array
      */
    public function delete_quotation() {
        $data   	= file_get_contents("php://input");
        $params     = json_decode($data,true);

        $this->quotation_model->delete_quotation($params['id']);

        $msg = 'Berhasil menghapus data kas';

        $result = array(
            'success' => true,
            'message' => $msg
        );
		
		$this->log_activity->insert_activity('delete', $msg. ' dengan ID Customer ' .$params['id']);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is used to update kas data
      * @return Array
      */
    public function edit_quotation() {
        $data   	= file_get_contents("php://input");
        $params     = json_decode($data,true);

        $this->quotation_model->edit_quotation($params);

        $msg = 'Berhasil mengubah data kas';

        $result = array(
            'success' => true,
            'message' => $msg
        );

		$this->log_activity->insert_activity('update', $msg. ' dengan ID Customer ' .$params['cust_id']);
		
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

  /**
    * This function is redirect to detail quotation page
    * @return Void
    */
  public function detail($id) {
      $detail = $this->quotation_model->detail($id);

      $data = array(
            'detail' => $detail
      );

      $this->load->view('detail_modal_view',$data);
  }

  /**
      * This function is redirect to approval quotation page
      * @return Void
      */
    public function edit_approval($id) {
        $result = $this->quotation_model->detail($id);

        $data = array(
            'detail' => $result
        );

        $this->load->view('edit_modal_approval_view',$data);
    }

    /**
      * This function is used to change status quotation data
      * @return Array
      */
    public function change_status() {
        $data   = file_get_contents("php://input");
        $params = json_decode($data,true);

        $this->quotation_model->change_status($params);

        $msg = 'Berhasil mengubah status quotation';

        $result = array(
            'success' => true,
            'message' => $msg
        );
		
		$this->log_activity->insert_activity('update', $msg. ' dengan status ' .$params['status']);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is redirect to edit approval quotation page
      * @return Void
      */
    public function detail_status($id) {
        $result = $this->quotation_model->detail_status($id);

        $data = array(
            'detail' => $result
        );

        $this->load->view('detail_modal_approval_view', $data);
    }
}