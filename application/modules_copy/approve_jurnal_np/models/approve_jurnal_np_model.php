<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Approve_Jurnal_NP_model extends CI_Model {
	
	public function __construct() 
	{
		parent::__construct();
	}

	public function list_approve_jurnal_np($params){
		$sql 	= 'CALL approve_jurnal_np_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query(
			$sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			)
		);

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}
	
	public function add_jurnal_coa_value($data){
		$sql 	= 'CALL jurnal_np_coa_add(?,?)';

		$query = $this->db->query($sql, array(
			$data['id_jurnal'],
			$data['id_coa_value']
		));
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
	public function add_approve_jurnal_np($data){
		$sql 	= 'CALL coavalue_add4(?,?,?,?,?,?,?,?,?,?,?)';

		$query = $this->db->query($sql, array(
			$data['id_coa'],
			$data['id_parent'],
			$data['date'],
			$data['id_valas'],
			$data['value'],
			$data['adjusment'],
			$data['type_cash'],
			$data['note'],
			$data['rate'],
			$data['bukti'],
			$data['id_coa_temp'],

		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		return $arr_result;
	}
	
	public function add_coa_value_temporary($data){
		$sql 	= 'CALL coavalue_add_temporary_jurnal(?,?,?,?,?,?,?,?,?,?,?,?,?)';

		$query = $this->db->query($sql, array(
			$data['id_coa'],
			$data['id_parent'],
			$data['date'],
			$data['id_valas'],
			$data['value'],
			$data['adjusment'],
			$data['type_cash'],
			$data['note'],
			$data['rate'],
			$data['bukti'],
			$data['pic'],
			$data['pic_approve'],
			$data['status']
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		
		return $arr_result;
	}
	
	public function add_coa_counter($data)
	{
		$sql 	= 'CALL jurnal_np_coa_counter_add(?,?)';

		$query = $this->db->query($sql, array(
			$data['id_coa'],
			$data['id_counter']
		));
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
	public function update_status_coa_value($data) 
	{
		$sql 	= 'CALL coavalue_temporary_update(?,?,?)';

		$query = $this->db->query($sql,array(
			$data['id'],
			$data['userid'],
			$data['status']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
			
	public function list_coa($id,$typecash)
	{
		$sql 	= 'CALL coavalue_temporary_search_id_jurnal(?,?)';
		
		$query 	=  $this->db->query($sql, array(
			$id,
			$typecash
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function detail_list_jurnal_np_coa_temporary($data)
	{
		$sql 	= 'CALL coavalue_temporary_search_id_jurnal(?,?)';
		
		$query 	=  $this->db->query($sql, array(
			$data['id_jurnal'],
			$data['type_cash']
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function update_jurnal_detail($data)
	{
		$sql 	= 'CALL jurnal_np_detail_update(?,?)';
		
		$query 	=  $this->db->query($sql, array(
			$data['id_jurnal'],
			$data['id_coa_value']
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function update_jurnal_np_status($data)
	{
		$sql 	= 'CALL jurnal_np_update_status(?,?)';
		
		$query 	=  $this->db->query($sql, array(
			$data['id_jurnal'],
			$data['status']
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function detail_list_total_coa_temporary($data)
	{
		$sql 	= 'CALL jurnal_np_total_value_search_id_temporary(?)';
		
		$query 	=  $this->db->query($sql, array(
			$data['id_jurnal']
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function detail_jurnal_non_prod($id) {
		$sql 	= 'CALL jurnal_np_search_id(?)';

		$query 	=  $this->db->query($sql, array(
			$id
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function add_kartu_hp($data)
	{
		$sql 	= 'CALL hp_add_nn(?,?,?,?,?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['ref'],
			$data['source'],
			$data['keterangan'],
			$data['status'],
			$data['saldo'],
			$data['saldo_akhir'],
			$data['id_valas'],
			$data['type_kartu'],
			$data['id_master'],
			$data['type_master'],
			$data['durasi']
		));
		
		$return['code'] = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function detail_jurnal_non_prod_by_type_cash($data) {
		$sql 	= 'CALL jurnal_np_search_id_by_type_cash(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['id_jurnal'],
			$data['type_cash']
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function detail_jurnal_non_prod_by_type_cash_is_approved($data) {
		$sql 	= 'CALL jurnal_np_search_id_by_type_cash_2(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['id_jurnal'],
			$data['type_cash']
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function detail_jurnal_non_prod_temporary($id)
	{
		$sql 	= 'CALL jurnal_np_search_id_temporary(?)';

		$query 	=  $this->db->query($sql, array(
			$id
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function delete_jurnal_np_detail($id)
	{
		$sql 	= 'CALL jurnal_np_coa_delete_detail(?)';
		
		$query 	=  $this->db->query($sql, array(
			$id
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function delete_detail_jurnal_np($id)
	{
		$sql 	= 'CALL jurnal_delete_detail_temporary(?)';
		
		$query 	=  $this->db->query($sql, array(
			$id
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function delete_coa_value_temporary($id)
	{
		$sql 	= 'CALL jurnal_delete_detail_temporary(?)';
		
		$query 	=  $this->db->query($sql, array(
			$id
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}