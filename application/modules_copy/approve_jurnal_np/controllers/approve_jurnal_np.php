<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Approve_Jurnal_NP extends MX_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('approve_jurnal_np/approve_jurnal_np_model');
		$this->load->model('jurnal/jurnal_model');
		$this->load->library('log_activity');
		$this->load->library('sequence');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'approve_jurnal_np/views/index');
	}

	function lists_approve_jurnal_np() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'DESC';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

		$order_fields = array('', 'nama');
		$search = $this->input->get_post('search');
		$search_val = (!empty($search['value'])) ? $search['value'] : null;
		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->approve_jurnal_np_model->list_approve_jurnal_np($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = $params['offset'] + 1;
		foreach ($list['data'] as $k => $v) {
			if($v['status'] == 0) {
				$status = '<span class="label label-info">Process</span>';
				$strBtn =
					'<div class="btn-group">' .
						'<a class="btn btn-primary btn-sm" title="Detail" onClick="detail_approve_jurnal(\'' . $v['id_jurnal'] . '\')">' .
							'<i class="fa fa-search"></i>' .
						'</a>' .
					'</div>'.
					'<div class="btn-group">' .
						'<a class="btn btn-success btn-sm" title="Approval" onClick="approve_jurnal_np_update_status(\'' . $v['id_jurnal'] . '\')">' .
							'<i class="fa fa-gavel"></i>' .
						'</a>' .
					'</div>';
			}elseif($v['status'] == 1){
				$status = '<span class="label label-success">Approved</span>';
				$strBtn =
					'<div class="btn-group">' .
						'<a class="btn btn-primary btn-sm" title="Detail" onClick="detail_approve_jurnal(\'' . $v['id_jurnal'] . '\')">' .
							'<i class="fa fa-search"></i>' .
						'</a>' .
					'</div>';

			}elseif($v['status'] == 2){
				$status = '<span class="label label-danger">Rejected</span>';
				$strBtn =
					'<div class="btn-group">' .
						'<a class="btn btn-primary btn-sm" title="Detail" onClick="detail_approve_jurnal(\'' . $v['id_jurnal'] . '\')">' .
							'<i class="fa fa-search"></i>' .
						'</a>' .
					'</div>';
			}
			
			array_push($data, array(
				$i++,
				$v['no_voucher'],
				$v['nama'],
				date('d M Y', strtotime($v['date_input'])),
				$v['debit_detail'],
				$v['symbol_valas'] . ' ' . number_format(round($v['debit']), 0, ',', '.'),
				$v['kredit_detail'],
				$v['symbol_valas'] . ' ' . number_format(round($v['kredit']), 0, ',', '.'),
				$status,
				$strBtn
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function detail_approve_jurnal_np($id) {
		$result_jurnal = $this->approve_jurnal_np_model->detail_jurnal_non_prod($id);
		
		for ($i = 0; $i < sizeof($result_jurnal); $i++) {
			if($result_jurnal[$i]['type_cash'] == 0) {
				$data_coa = array(
					'id_jurnal' => $id,
					'type_cash' => 0
				);
				
				$result_coa			= $this->approve_jurnal_np_model->detail_list_jurnal_np_coa_temporary($data_coa);
				$result_total_coa	= $this->approve_jurnal_np_model->detail_list_total_coa_temporary($data_coa);
			}else{
				$data_coa = array(
					'id_jurnal' => $id,
					'type_cash' => 1
				);
				
				$result_coa			= $this->approve_jurnal_np_model->detail_list_jurnal_np_coa_temporary($data_coa);
				$result_total_coa	= $this->approve_jurnal_np_model->detail_list_total_coa_temporary($data_coa);
			}
		}
		
		for ($i = 0; $i < sizeof($result_coa); $i++) {
			if ($result_coa[$i]['bukti'] != null) {
				$handle = fopen($result_jurnal_coa[$i]['bukti'], "r");
				$result_coa[$i]['have_file'] = "yes";
			} else {
				$result_coa[$i]['have_file'] = "no";
			}
		}
		
		$data = array(
			'detail_jurnal'	=> $result_jurnal,
			'detail_coa'	=> $result_coa,
			'total_coa'		=> $result_total_coa,
		);
		
		$this->load->view('detail_approve_jurnal_np_modal_view', $data);
	}
	
	public function approve_jurnal_np_update_status() {
		$data 	= file_get_contents("php://input");
		$params = json_decode($data, true);

		$params['userid'] = $this->session->userdata['logged_in']['user_id'];
		
		$list = $this->approve_jurnal_np_model->detail_jurnal_non_prod_temporary($params['id']);
		
		$dist_p = '';
		$dist_hutang = 0;
		$note_sss = '';
		
		if ($params['status'] == 1) {
			
			$status = 'Approve';
			
			$data_status_jurnal_np = array(
				'id_jurnal' 	=> $params['id'],
				'status'		=> $params['status']
			);
			
			for($i = 0; $i < sizeof($list);$i++) {
				
				$update_jurnal = $this->approve_jurnal_np_model->update_jurnal_np_status($data_status_jurnal_np);
				
				$data_coa_value = array(
					'id' 			=> $list[$i]['id_coa_value'],
					'id_coa' 		=> $list[$i]['id_coa'],
					'id_parent' 	=> $list[$i]['id_parent'],
					'date' 			=> $list[$i]['date_input'],
					'id_valas' 		=> $list[$i]['id_valas'],
					'value' 		=> $list[$i]['value'],
					'adjusment' 	=> 0,
					'type_cash' 	=> $list[$i]['type_cash'],
					'note' 			=> $list[$i]['note'],
					'rate' 			=> $list[$i]['rate'],
					'bukti' 		=> $list[$i]['bukti'],
					'id_coa_temp' 	=> $list[$i]['id_coa_value']
				);
				
				$add_approve_jurnal_np_coa_val = $this->approve_jurnal_np_model->add_approve_jurnal_np($data_coa_value);
				$id_coa_value = $add_approve_jurnal_np_coa_val['lastid'];
				
				$data_jurnal_coa = array(
					'id_jurnal' 	=> $params['id'],
					'id_coa_value' 	=> $id_coa_value
				);
				
				$delete_jurnal_coa = $this->approve_jurnal_np_model->delete_jurnal_np_detail($list[$i]['id_coa_value']);
				$add_jurnal_detail = $this->approve_jurnal_np_model->add_jurnal_coa_value($data_jurnal_coa);
				
				
				if($list[$i]['type_cash'] == 0 ){
					if($list[$i]['id_eksternal'] <> null ){
						
						$data_kartu_hp = array(
							'ref' 			=> NULL, 
							'source' 		=> NUll,
							'keterangan' 	=> $list[$i]['note'],
							'status' 		=> 0,
							'saldo' 		=> $list[$i]['value'],
							'saldo_akhir' 	=> $list[$i]['value'],
							'id_valas'		=> $list[$i]['id_valas'],
							'type_kartu'	=> 1,
							'id_master'		=> $id_coa_value,
							'type_master'	=> 6,
							'durasi'		=> $list[$i]['durasi']
							
						);
						
						$dist_p = $list[$i]['id_coa'];
						$note_sss = $note_sss.','.$list[$i]['note'];
						
						$this->approve_jurnal_np_model->add_kartu_hp($data_kartu_hp);
						
					}
					
					$dist_hutang = $dist_hutang + $list[$i]['value'];
				}else{
					
					if($list[$i]['id_eksternal'] <> null ){
						
						$data_kartu_hp = array(
							'ref' 			=> NULL, 
							'source' 		=> NUll,
							'keterangan' 	=> $list[$i]['note'],
							'status' 		=> 0,
							'saldo' 		=> $list[$i]['value'],
							'saldo_akhir' 	=> $list[$i]['value'],
							'id_valas'		=> $list[$i]['id_valas'],
							'type_kartu'	=> 0,
							'id_master'		=> $id_coa_value,
							'type_master'	=> 6,
							'durasi'		=> $list[$i]['durasi']
							
						);
						
						$dist_p = $list[$i]['id_coa'];
						$note_sss = $note_sss.','.$list[$i]['note'];
						
						$this->approve_jurnal_np_model->add_kartu_hp($data_kartu_hp);
						
					}
					
					
				}
				
			}
			
			if($dist_p <> ''){
				
				// $data_kartu_hp = array(
							// 'ref' 			=> NULL, 
							// 'source' 		=> NUll, 
							// 'keterangan' 	=> $note_sss,
							// 'status' 		=> 0,
							// 'saldo' 		=> $dist_hutang,
							// 'saldo_akhir' 	=> $dist_hutang,
							// 'id_valas'		=> $list[0]['id_valas'],
							// 'type_kartu'	=> 0,
							// 'id_master'		=> $dist_p,
							// 'type_master'	=> 6,
							// 'durasi'		=> $list[0]['durasi']
							
						// );
				
				//$this->approve_jurnal_np_model->add_kartu_hp($data_kartu_hp);
				
			}
			
			
			// $dist_p
			// $params['id']
			
			
		
			$list_coa_d = $this->approve_jurnal_np_model->list_coa($params['id'],0);
			$list_coa_k = $this->approve_jurnal_np_model->list_coa($params['id'],1);
			
			for($y = 0; $y < sizeof($list_coa_d);$y++) {
				for($l = 0; $l < sizeof($list_coa_k);$l++) {
					$data_coa = array(
						'id_coa' => $list_coa_d[$y]['id'],
						'id_counter' => $list_coa_k[$l]['id']
					);
					$this->approve_jurnal_np_model->add_coa_counter($data_coa);
				}
			}
			
			if ($list > 0 && $list_coa_d > 0 && $list_coa_k > 0) {
				$msg = 'Data Jurnal berhasil di simpan';
				$title = 'Akan mengapprove data Jurnal ini ?';

				$results = array('success' => true, 'status' => 'success', 'title' => $title, 'message' => $msg);
				$this->log_activity->insert_activity('insert', $msg . ' dengan status ' . $status);
				
			}else {
				$msg = 'Data Jurnal Gagal di simpan';

				$results = array('success' => false, 'status' => 'error', 'message' => $msg);
				$this->log_activity->insert_activity('insert', $msg . ' dengan status ' . $status);
			}
			
		}else if($params['status'] == 2) {
			$status = 'Reject';
			$data_status_jurnal_np = array(
				'id_jurnal' 	=> $params['id'],
				'status'		=> $params['status']
			);
			
			$update_jurnal = $this->approve_jurnal_np_model->update_jurnal_np_status($data_status_jurnal_np);
			
			if($update_jurnal > 0) {
				$msg = 'Data Jurnal Berhasil di simpan';

				$results = array('success' => true, 'status' => 'error', 'message' => $msg);
				$this->log_activity->insert_activity('insert', $msg . ' dengan status ' . $status);
			}else {
				$msg = 'Data Jurnal gagal di simpan';
				$results = array('success' => false, 'status' => 'error', 'message' => $msg);
			}
			
			$this->log_activity->insert_activity('insert', $msg . ' dengan status ' . $status);
			
		}else {
			$status = 'Gagal di simpan';
			$msg = 'Data Jurnal Gagal di simpan';

			$results = array('success' => false, 'status' => 'error', 'message' => $msg);
			$this->log_activity->insert_activity('insert', $msg . ' dengan status ' . $status);
		}
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($results);
	}
	
	function detail_lists_jurnal_np_coa($id,$type_cash) {
		// var_dump($id,$type_cash);
		$list = $this->approve_jurnal_np_model->detail_jurnal_non_prod($id);
		$data = array();
		$x = 0;
		
		$username = $this->session->userdata['logged_in']['username'];
		
		$data_coa = array(
			'id_jurnal' => $id,
			'type_cash'	=> $type_cash
		);
		
		$no = 0;
		if($type_cash == 0) {

			if($list[0]['status'] == 1) $list_coa_masuk = $this->approve_jurnal_np_model->detail_jurnal_non_prod_by_type_cash_is_approved($data_coa);
			else $list_coa_masuk = $this->approve_jurnal_np_model->detail_jurnal_non_prod_by_type_cash($data_coa);
			
			foreach ($list_coa_masuk as $m => $a) {
				if($type_cash == 0) { $status = 'Debit';}
				array_push($data, array(
					$no = $m+1,
					$a['id_coa'],
					$a['id_parent'],
					$a['symbol_valas'] . ' ' . number_format(round($a['value']), 0),
					$status,
					$a['note'],
					$a['symbol_valas'] . ' ' . number_format(round($a['value_real']), 0),
					$a['value'],
					$a['bukti'],
				));
			}
		}else {
			if($list[0]['status'] == 1) $list_coa_keluar = $this->approve_jurnal_np_model->detail_jurnal_non_prod_by_type_cash_is_approved($data_coa);
			else $list_coa_keluar = $this->approve_jurnal_np_model->detail_jurnal_non_prod_by_type_cash($data_coa);
			
			foreach ($list_coa_keluar as $n => $o) {
				if($type_cash == 1) { $status = 'Kredit';}
				array_push($data, array(
					$no = $n+1,
					$o['id_coa'],
					$o['id_parent'],
					$o['symbol_valas'] . ' ' . number_format(round($o['value']), 0),
					$status,
					$o['note'],
					$o['symbol_valas'] . ' ' . number_format(round($o['value_real']), 0),
					$o['value'],
					$o['bukti']
				));
			}
		}
		
		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
}
