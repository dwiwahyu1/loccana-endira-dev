<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Login extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_model');
    }

    /**
     * anti sql injection
     */
    public function Anti_sql_injection($string) {
        $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
        return $string;
    }

    public function index() {

        $this->template->load('login_view');
    }

    function auth_post() {
        $data = json_decode(file_get_contents("php://input"));

        $appkey = $this->Anti_sql_injection($data->appkey) == '' ? null : $this->Anti_sql_injection($data->appkey);
        $dataArray = array(
            'username' => $this->Anti_sql_injection($data->username) == '' ? null : $this->Anti_sql_injection($data->username)
        );
		
        if ($appkey !== $this->config->item('app_key')) {

            $res = array('status' => 'error', 'message' => 'Wrong API key');
        } else {

            $password = $this->Anti_sql_injection($data->password) == '' ? null : $this->Anti_sql_injection($data->password);
            $data = $this->login_model->login($dataArray);

            if ($data) {
                $hash = $data['passwords'];
				// print_r($password);
				// echo "<br/>";
				// print_r($hash);
                if (password_verify($password, $hash)) {
					// die;
                    //jika password cocok cek status
                    if ($data['status_akses'] == 1) {
                        $set_token = $this->db->query('select set_token(?, ?, ?) as is_token', array($data['user_id'], $data['token'], $data['id_role']));
                        $row = $set_token->row_array();
                        $picture = $data['profile_picture'];
                        if (!$picture) {
                            $picture = 'uploads/profile/user.png';
                        }
                        if ($row['is_token']) {
                            $datauser = array(
                                'user_id' => $data['user_id'],
                                'nama' => $data['nama'],
                                'profile_picture' => base_url() . $picture,
                                'email' => $data['email'],
                                'token' => $data['token'],
                                'id_role' => $data['id_role'],
                                'name_role' => $data['name_role'],
                                'username' => $data['username'],
                                'idlokasi' => $data['idlokasi']
                            );

                            // $datamenu = $this->login_model->login_menu($data['id_role']);
                            // $this->session->sess_expiration = '604800';// expires in 7 days
                            //set user session
                            $this->session->set_userdata('logged_in', $datauser);
                            
                            $nama = $this->session->userdata['logged_in']['nama'];
                            
//                            print_r($nama);die;
                            log_activity('Login','Nama user '.$nama.'.');

                            $res = array(
                                'status' => 'success',
                                'message' => 'Success Login',
                                'data' => $datauser
                                    // 'menu'      => $datamenu
                            );
                        } else {
                            //jika user masih aktif login
                            $res = array(
                                'status' => 'error',
                                'message' => 'Tidak bisa menggenerate token.'
                            );
                        }
                    } else {
                        //jika status tidak aktif tampilkan error
                        $res = array(
                            'status' => 'error',
                            'message' => $data['status_name']
                        );
                    }
                } else {
                    $res = array(
                        'status' => 'error',
                        'message' => 'Password Doesnt Matchs'
                    );
                }
            } else {
                $res = array(
                    'status' => 'error',
                    'message' => 'User Not Found'
                );
            }
        }

        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
        echo json_encode($res);
    }

    function test_get() {
        $data = json_decode(file_get_contents("php://input"));

        $res = array(
			'status' => 'success',
			'message' => 'Ini Hanya test'
		);
        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
        echo json_encode($res);
    }

}
