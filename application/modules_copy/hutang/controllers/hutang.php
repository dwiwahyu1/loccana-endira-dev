<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hutang extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('hutang/hutang_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	  * This function is redirect to index btb page
	  * @return Void
	  */
	public function index() {
		$this->template->load('maintemplate', 'hutang/views/index');
	}

	/**
	  * This function is used for showing btb list
	  * @return Array
	  */
	function lists() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

		$order_fields = array('', 'id_hp');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;
		$params['filter_type'] = 1;
		
		$list = $this->hutang_model->lists($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();
		$no=1;
		foreach ($list['data'] as $k => $v) {
			$no = $k+1;
			$valas = $v['nama_valas'].'('.$v['symbol_valas'].')';
			$tgl_awal = $v['tanggal'];
			
			// if(is_numeric($v['durasi']) == true) $jatuh_tempo = $v['term_of_payment'].' Hari';
			// else $jatuh_tempo = date('d-M-Y', strtotime(preg_replace('|/|', '-', $v['term_of_payment'])));
			
			if(is_numeric($v['jatuh_tempo']) == true){
				$due_date = date('Y-m-d', strtotime($tgl_awal.'+ '.$v['jatuh_tempo'].' days'));
			}else{
				$due_date = $v['jatuh_tempo'];
			}
			
			
			if($v['status_bayar'] == "Lunas") {
				$actions = '<span class="label label-info text-center">'.$v['status_bayar'].'</span>';
			}else{
				$actions = '<span class="label label-danger text-center">'.$v['status_bayar'].'</span>';
			}
			
			if($v['status'] == 0) {
				/*$actions .=
					'<div class="btn-group" style="display: none;">'.
						'<button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="pay_debt(\'' . $v['id_hp'] . '\')">'.
							'<i class="fa fa-money"></i>'.
						'</button>'.
					'</div>';*/
			}
			
			array_push($data, 
				array(
					$no,
					$v['name_eksternal'],
					$v['tanggal'],
					$v['ref'],
					$v['source'],
					$v['date_insert'],
					$v['symbol'].'. '.number_format($v['r_saldo'],0,'.','.'),
					$due_date,
					$v['keterangan'],
					$actions
				)
			);
		}
		
		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function report_detail($type_eksternal) {
		$results = $this->hutang_model->filter_type_eksternal($type_eksternal);
		
		$data = array(
			'filter' => $results
		);

		$this->load->view('report_detail_modal_view',$data);
	}
	
	public function report_view() {
		$data   = file_get_contents("php://input");
		$params     = json_decode($data,true);
		
		//print_r($params);die; 
		
		$results = $this->hutang_model->report_hutang_view($params['id_eks'],$params['typex']);
		
		if (count($results) > 0) {
			$msg = 'Data report supplier '. $results[0]['name_eksternal'].' ditemukan';
			
			$return = array(
				'success' => true,
				'message' => $msg,
				'data' => $results
			);
			
			$this->log_activity->insert_activity('view', $msg. ' dengan ID Supplier ' .$params['id_eks']);
			
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else {
			$msg = 'Data report supplier tidak tercatat dalam report hutang';
			
			$return = array(
				'success' => false, 
				'message' => 'Maaf, '.$msg
			);
			
			$this->log_activity->insert_activity('view', $msg);
			
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}
	
	public function report_filter($id_eks) {
		
		$results = $this->hutang_model->report_hutang_ideks($id_eks);
		
		if($results['code'] == '1') {
			$message = 'Data report supplier atas nama '.$results['list'][0]['name_eksternal'].' ditemukan';
			$msg = 'Lihat report filter hutang';
			
			$data = array(
				'success' => true,
				'message' => $message,
				'data' => $results['list']
			);
			
			$this->log_activity->insert_activity('view', $msg. ' dengan ID Supplier ' .$id_eks);
			
		}else{
			$message = 'Data report supplier tidak ditemukan';
			$msg = 'Gagal lihat report filter hutang';
			
			$data = array(
				'success' => false,
				'message' => $message,
				'data' => ''
			);
			
			$this->log_activity->insert_activity('view', $msg);
		}
		
		$this->load->view('report_hutang_modal_view',$data);
	}
	
	public function list_report($id_eks,$type) {
		
		$username = $this->session->userdata['logged_in']['username'];
		
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'asc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

		$order_fields = array('', 'id_hp');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;
		$params['id_eks'] = $id_eks;
		$params['type_eks'] = $type;
		
		//print_r($params);die;
		
		$list = $this->hutang_model->report_hutang_view($params);
		
		//print_r($list);die;
		
		$result["recordsTotal"] = $list['total_filtered'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();
		
		$no = 1;
		$sum_tot = 0;
		
		foreach ($list['data'] as $k => $v) {
			
			$no = (int)$k+1;
			
			if($v['type_cash'] == 1) {
				$debit = number_format($v['value_real'],0,'.','.');
				$kredit = 0;
				$sum_tot = $sum_tot - $v['value_real'];
			} else {
				$kredit = number_format($v['value_real'],0,'.','.');
				$debit = 0;
				$sum_tot = $sum_tot + $v['value_real'];
			}

			array_push($data, array(
				$no, 
				$v['tanggal'],
				$v['ref'],
				$v['source'],
				$v['keterangan'],
				$v['symbol'].' '.$debit,
				$v['symbol'].' '.$kredit,
				$v['symbol'].' '.number_format($sum_tot,0,'.','.')
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function pay_debt($id_hp) {
		$results = $this->hutang_model->report_pay_debt($id_hp);
		$coa = $this->hutang_model->report_get_coa_values();
		$valas = $this->hutang_model->report_get_valas();
		
		$data = array(
			'list' => $results['list'],
			'valas' => $valas,
			'coa' => $coa
		);
		// print_r($data);die;
		
		$this->load->view('report_bayar_modal_view',$data);
	}
	
	public function add_pay_debt() {
		$this->form_validation->set_rules('tanggal_bayar', 'Tanggal Bayar', 'trim|required');
		$this->form_validation->set_rules('coa', 'Tipe Pembayaran', 'trim|required');
		$this->form_validation->set_rules('valas', 'Valas', 'trim|required');
		$this->form_validation->set_rules('jml_byr', 'Jumlah Bayar', 'trim|required');
		$this->form_validation->set_rules('ket', 'keterangan', 'trim|required');
		$this->form_validation->set_rules('rate', 'Rate', 'trim|required');
		$this->form_validation->set_rules('konversi', 'Rate', 'trim|required');
		
		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$id_hp			= $this->Anti_sql_injection($this->input->post('id_hp', TRUE));
			$tanggal		= $this->Anti_sql_injection($this->input->post('tanggal_bayar', TRUE));
			$coa 			= $this->Anti_sql_injection($this->input->post('coa', TRUE));
			$valas			= $this->Anti_sql_injection($this->input->post('valas', TRUE));
			$saldo_akhir	= $this->Anti_sql_injection($this->input->post('saldo_akhir', TRUE));
			$jml_byr		= $this->Anti_sql_injection($this->input->post('jml_byr', TRUE));
			$ket			= $this->Anti_sql_injection($this->input->post('ket', TRUE));
			$status			= $this->Anti_sql_injection($this->input->post('status', TRUE));
			$rate			= $this->Anti_sql_injection($this->input->post('rate', TRUE));
			$konversi		= $this->Anti_sql_injection($this->input->post('konversi', TRUE));
			$upload_error 	= NULL;
			$file_bayar		= NULL;

			if(sizeof($_FILES) > 0) {
				if($_FILES['file_bayar']['name']) {
					$this->load->library('upload');
					$config = array(
						'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/kas",
						'upload_url' => base_url() . "uploads/kas",
						'encrypt_name' => FALSE,
						'max_filename' => 100,
						'file_name' => $_FILES['file_bayar']['name'],
						'overwrite' => FALSE,
						'allowed_types' => 'pdf|txt|doc|docx',
						'max_size' => '10000'
					);
					$this->upload->initialize($config);

					if ($this->upload->do_upload("file_bayar")) {
						// General result data
						$result = $this->upload->data();

						// Add our stuff
						$file_bayar = 'uploads/kas/'.$result['file_name'];
					}else {
						$pesan = $this->upload->display_errors();
						$upload_error = strip_tags(str_replace("\n", '', $pesan));
						$msg = $upload_error;
						
						$result = array('success' => false, 'message' => $upload_error);
						
						$this->log_activity->insert_activity('error', $msg);
					}
				}
			}

			if (!isset($upload_error)) {
				$data = array(
					'id_hp'			=> $id_hp,
					'tanggal'		=> $tanggal,
					'coa'			=> $coa,
					'valas'			=> $valas,
					'saldo_akhir'	=> $saldo_akhir,
					'jml_byr'		=> $jml_byr,
					'ket'			=> $ket,
					'status'		=> $status,
					'rate'			=> $rate,
					'konversi'		=> $konversi,
					'bukti'			=> $file_bayar
				);

				$results = $this->hutang_model->report_pay_debt($id_hp);
				if($results['code'] == 1){
					$dataP = array(
						'params'	=> $data,
						'results'	=> $results['list'][0]
					);

					$buyer = $this->hutang_model->add_kartu_hp_pays($dataP);
					if($buyer['code'] == 1){
						$dataC = array(
							'params'	=> $data,
							'results'	=> $results['list'][0],
							'last_id'	=> $buyer['lastid']
						);
					
						$payment = $this->hutang_model->add_payment_hp($dataC);
						if($payment['code'] == 1){
							$get_coa = $this->hutang_model->get_id_coa($id_hp);
							$dataCPO = array(
								'params'	=> $data,
								'list'		=> $results['list'][0],
								'results'	=> $get_coa['list'][0]
							);
							
							$coa_value = $this->hutang_model->add_coa_values($dataCPO);
							if($coa_value['code'] == 1) {
								$dataCOA = array(
									'coa'		=> $get_coa['list'][0],
									'coa_value'	=> $dataCPO['params']
								);
								$coa_value_add = $this->hutang_model->add_coa_values2($dataCOA);
							}
						}
					}
					
					$this->log_activity->insert_activity('insert', 'Insert Bayar Hutang id : '.$id_hp);
					$msg = $data['ket'].', Berhasil dilakukan.';
					$result = array('success' => true, 'message' => $msg);
				}else{
					$this->log_activity->insert_activity('insert', 'Gagal Insert Bayar Hutang id : '.$id_hp);
					$msg = $data['ket'].', Gagal dilakukan.';
					$result = array('success' => true, 'message' => $msg);
				}
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function check_eksternal($id_eks,$type){
		
		$results = $this->hutang_model->check_eksternal($id_eks,$type);
		
		$data = array(
			'detail' => $results['array']
		);
		
		if ($results['code'] == 1) {
			$msg = 'Selamat type eksternal ditemukan';
			$return = array(
				'success' => true, 
				'message' => $msg, 
				'type_eksternal' => $results['array'][0]['type_eksternal'],
				'keterangan' => $results['array'][0]['keterangan'], 
				'typex' => $results['array'][0]['id']
			);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($results['code'] == 0) {
			$return = array(
				'success' => false, 
				'message' => 'Maaf, Type Eksternal atas nama supplier tidak tercatat dalam report hutang'
			);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}
}