	<style>
		#loading-us{display:none}
		#tick{display:none}

		#loading-mail{display:none}
		#cross{display:none}
		.dt-body-right{text-align:right;}
	</style>
	
	<form class="form-horizontal form-label-left" id="report_hutang_detail" role="form" action="" method="POST" enctype="multipart/form-data" data-parsley-validate>
	
        <div class="item form-group">
        	<table id="list-hutang-supplier" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>No</th>
						<th>Tanggal</th>
						<th>Ref</th>
						<th>Source</th>
						<th>Keterangan</th>
						<th>Debit</th>
						<th>Kredit</th>
						<th>Saldo</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
        </div>
	</form><!-- /page content -->

<script type="text/javascript">
var id_eks = '<?php echo $data[0]['id']; ?>';
var name_eks = '<?php echo $data[0]['name_eksternal']; ?>';
var type_eks = '<?php echo $data[0]['type_eksternal']; ?>';

$(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();
	//get_report();
	var buttonCommon = {
		exportOptions: {
			format: {
				body: function ( data, row, column, node ) {
					// Strip $ from salary column to make it numeric
					return column === 5 ?
						data.replace( /[$,]/g, '' ) :
						data;
				}
			}
		}
	};
	var table = $('#list-hutang-supplier').DataTable( {
		processing: true,
		serverSide: true,
		deferRender: true,
		destroy: true,
		searching: false,
		aaSorting: [],
		bLengthChange: true,
		searchDelay: 700,
		autoWidth: false,
		ajax: { 
			url : "<?php echo base_url().'hutang/list_report';?>"+"/"+id_eks+"/"+type_eks,
			type : "POST",
			datatype: "json"
		},
		dom: 'Blfrtip',
		buttons: [
		{
			extend: 'print',
			customize: function ( win ) {
				$(win.document.body)
					.css( 'font-size', '11pt' )
					.css( 'text-align', 'center' );

				$(win.document.body).find( 'table' )
					.addClass( 'compact' )
					.css( 'font-size', 'inherit');
			},
			title: 'Laporan Hutang '+name_eks
		},
		{
			text: 'PDF',
			pageSize: 'LEGAL',
			action: function (e, dt, button, config)
			{
				dt.one('preXhr', function (e, s, data)
				{
					data.length = 18446744073709551610 ;
				}).one('draw', function (e, settings, json, xhr)
					{
						var pdfButtonConfig = $.fn.DataTable.ext.buttons.pdfHtml5;
						var pdfConfig = $.extend( true, {}, buttonCommon, {
												extend: 'Laporan Hutang'+name_eks,
												title: 'Laporan Hutang '+name_eks
											});
						var addOptions = { exportOptions: { 'columns': ':visible',  format: {
										body: function(data, row, column, node) {              
											return data.toString().replace('.', '');
											//return data.replace(/<\/?[^>]+(>|$)/g, "");
										}
									}
								} 
							};
							 
							$.extend(true, pdfButtonConfig, pdfConfig, addOptions);
							pdfButtonConfig.action(e, dt, button, pdfButtonConfig);
							get_report();
					}).draw();
			}
		}],
		columnDefs: [{
			render: function ( data, type, row, meta ) {
				return data;
			},
			targets: [5,6,7],
			className: 'dt-body-right'
		},
		{
			orderable: true,
			targets: [0,1,2,3]
		}]
	});
});

function get_report() {
	var table = $('#list-hutang-supplier').DataTable( {
		processing: true,
		serverSide: true,
		deferRender: true,
		destroy: true,
		searching: false,
		aaSorting: [],
		bLengthChange: true,
		searchDelay: 700,
		autoWidth: false,
		ajax: { 
			url : "<?php echo base_url().'hutang/list_report';?>"+"/"+id_eks+"/"+type_eks,
			type : "POST",
			datatype: "json"
		},
		dom: 'Blfrtip',
		buttons: [
		{
			extend: 'print',
			customize: function ( win ) {
				$(win.document.body)
					.css( 'font-size', '11pt' )
					.css( 'text-align', 'center' );

				$(win.document.body).find( 'table' )
					.addClass( 'compact' )
					.css( 'font-size', 'inherit');
			},
			title: 'Laporan Hutang '+name_eks
		},
		{
			text: 'PDF',
			pageSize: 'LEGAL',
			action: function (e, dt, button, config)
			{
				dt.one('preXhr', function (e, s, data)
				{
					data.length = 18446744073709551610 ;
				}).one('draw', function (e, settings, json, xhr)
					{
						var pdfButtonConfig = $.fn.DataTable.ext.buttons.pdfHtml5;
						var pdfConfig = $.extend( true, {}, buttonCommon, {
												extend: 'Laporan Hutang'+name_eks,
												title: 'Laporan Hutang '+name_eks
											});
						var addOptions = { exportOptions: { 'columns': ':visible',  format: {
										body: function(data, row, column, node) {              
											return data.toString().replace('.', '');
											//return data.replace(/<\/?[^>]+(>|$)/g, "");
										}
									}
								} 
							};
							 
							$.extend(true, pdfButtonConfig, pdfConfig, addOptions);
							pdfButtonConfig.action(e, dt, button, pdfButtonConfig);
							get_report();
					}).draw();
			}
		}],
		columnDefs: [{
			render: function ( data, type, row, meta ) {
				return data;
			},
			targets: [5,6,7],
			className: 'dt-body-right'
		},
		{
			orderable: true,
			targets: [0,1,2,3]
		}]
	});
}
</script>
