<style>
	#loading-us{display:none}
	#tick{display:none}

	#loading-mail{display:none}
	#cross{display:none}
	.add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
	.add_item:hover{color:#ff8c00}
	.form-item{margin-top: 15px;overflow: auto;}
	.saldo{text-align: right;}
</style>

<form class="form-horizontal form-label-left" id="bayar_form" role="form" action="<?php echo base_url().'hutang/add_pay_debt';?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<br>
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_bayar">Tanggal Bayar <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<div class="input-group">
				<input type="text" class="form-control" placeholder="<?php echo date('Y-m-d'); ?>" id="tanggal_bayar" name="tanggal_bayar" autocomplete="off">
				<span class="input-group-addon bg-primary b-0 text-white"><i class="ti-calendar"></i></span>
			</div><!-- input-group -->
		</div>
	</div>
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_valas">Valas <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="id_valas" id="id_valas" style="width: 100%" required>
				<option value="">Silahkan Pilih Valas</option>
				<?php foreach($valas as $key) { ?>
					<option value="<?php echo $key['valas_id']; ?>" ><?php echo $key['nama_valas']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_coa">Tipe Pembayaran <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="id_coa" id="id_coa" style="width: 100%" required>
				<option value="">Silahkan Pilih Pembayaran</option>
				<?php foreach($coa as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" ><?php echo $key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="saldo_akhir">Saldo Akhir </label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input placeholder="Saldo Akhir" type="text" class="form-control col-md-9 col-xs-12 saldo" id="saldo" name="saldo" value="<?php echo $list[0]['symbol_valas'].' '.number_format($list[0]['r_saldo'],0,'.','.');?>" readonly>
			<input type="hidden" id="saldo_akhir" name="saldo_akhir" value="<?php echo round($list[0]['r_saldo']);?>" readonly>
		</div>
	</div>
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jumlah_bayar">Jumlah Bayar <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input placeholder="Jumlah Bayar" type="text" class="form-control col-md-9 col-xs-12 saldo" id="jumlah_bayar" name="jumlah_bayar" value="0" autocomplete="off" required>
			<span id="tick"></span>
		</div>
	</div>
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="rate">Rate <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input placeholder="Rate" type="text" class="form-control col-md-9 col-xs-12 saldo" id="rate" name="rate" value="1" autocomplete="off" required>
			<span id="tick"></span>
		</div>
	</div>
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="konversi">Konversi <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input placeholder="Konversi" type="text" class="form-control col-md-9 col-xs-12 saldo" id="konversi" name="konversi" value="0" autocomplete="off" required>
			<span id="tick"></span>
		</div>
	</div>
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Keterangan <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<textarea data-parsley-maxlength="255" id="keterangan" name="keterangan" class="form-control col-md-7 col-xs-12" placeholder="Keterangan" required><?php echo $list[0]['keterangan'];?></textarea>
		</div>
	</div>
	<div class="item form-group has-feedback">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="file_bayar">File</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="file" class="form-control" id="file_bayar" name="file_bayar" data-height="110" accept=".pdf, .txt, .doc, .docx"/>
			<span id="file_bc_tick">Hanya format file pdf,txt,doc,docx dengan besaran max 10Mb yang diterima.</span>
		</div>
	</div>
	<hr>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Bayar</button>
		</div>
	</div>
</form>
	
<script type="text/javascript">
	var items = [];
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
		jQuery('#tanggal_bayar').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		}).focus(function() {
			$(this).prop("autocomplete", "off");
		});

		$('#jumlah_bayar').on('focus', function() {
			var jmlBayar = this.value.replace(/[^0-9]/g, "");
			if(jmlBayar == '') this.value = 0;
			else this.value = formatNumber(parseInt(jmlBayar));
		});
		$('#jumlah_bayar').on('keyup', function() {
			var jmlBayar = this.value.replace(/[^0-9]/g, "");
			if(jmlBayar == '') {
				this.value = 0;
				$('#konversi').val(0);
			}else {
				if(parseInt(jmlBayar) > parseInt($('#saldo_akhir').val())) {
					this.value = formatNumber(parseInt($('#saldo_akhir').val()));
					var valKonversi = parseInt($('#saldo_akhir').val()) * $('#rate').val().replace(/[^0-9]/g, "");
					$('#konversi').val(formatNumber(valKonversi));
				}else {
					this.value = formatNumber(parseInt(jmlBayar));
					var valKonversi = parseInt(jmlBayar) * $('#rate').val().replace(/[^0-9]/g, "");
					$('#konversi').val(formatNumber(valKonversi));
				}
			}
		});
		$('#jumlah_bayar').on('change', function() {
			var jmlBayar = this.value.replace(/[^0-9]/g, "");
			if(jmlBayar == '') {
				this.value = 0;
				$('#konversi').val(0);
			}else this.value = formatNumber(jmlBayar.replace(/[^0-9]/g, ""));
		});

		$('#rate').on('focus', function() {
			var rate = this.value.replace(/[^0-9]/g, "");
			if(rate == '' || rate == 0) {
				this.value = 1;
				var valKonversi = parseInt($('#jumlah_bayar').val().replace(/[^0-9]/g, "")) * this.value;
				$('#konversi').val(formatNumber(valKonversi));
			}else {
				this.value = formatNumber(parseInt(rate));
				var valKonversi = parseInt($('#jumlah_bayar').val().replace(/[^0-9]/g, "")) * parseInt(rate);
				$('#konversi').val(formatNumber(valKonversi));
			}
		});
		$('#rate').on('keyup', function() {
			var maxValue = 999999999999;
			var rate = this.value.replace(/[^0-9]/g, "");
			if(rate == '' || rate == 0) {
				this.value = 1;
				var valKonversi = parseInt($('#jumlah_bayar').val().replace(/[^0-9]/g, "")) * this.value;
				$('#konversi').val(formatNumber(valKonversi));
			}else {
				if(rate > maxValue) {
					this.value = formatNumber(maxValue);
					var valKonversi = parseInt($('#jumlah_bayar').val().replace(/[^0-9]/g, "")) * maxValue;
					$('#konversi').val(formatNumber(valKonversi));
				}else {
					this.value = formatNumber(parseInt(rate));
					var valKonversi = parseInt($('#jumlah_bayar').val().replace(/[^0-9]/g, "")) * parseInt(rate);
					$('#konversi').val(formatNumber(valKonversi));
				}
			}
		});
		$('#rate').on('change', function() {
			var rate = this.value.replace(/[^0-9]/g, "");
			if(rate == '' || rate == 1) {
				this.value = 1;
				var valKonversi = parseInt($('#jumlah_bayar').val().replace(/[^0-9]/g, "")) * this.value;
				$('#konversi').val(formatNumber(valKonversi));
			}else {
				this.value = formatNumber(this.value.replace(/[^0-9]/g, ""));
				var valKonversi = parseInt($('#jumlah_bayar').val().replace(/[^0-9]/g, "")) * parseInt(rate);
				$('#konversi').val(formatNumber(valKonversi));
			}
		});

		$('#konversi').on('focus', function() {
			var jmlBayar = this.value.replace(/[^0-9]/g, "");
			if(jmlBayar == '') {
				this.value = 0;
				$('#jumlah_bayar').val(0);
			}else this.value = formatNumber(parseInt(jmlBayar));
		});
		$('#konversi').on('keyup', function() {
			var jmlBayar = this.value.replace(/[^0-9]/g, "");
			if(jmlBayar == '') {
				this.value = 0;
				$('#jumlah_bayar').val(0);
			}else {
				this.value = formatNumber(parseInt(jmlBayar));
				var valBayar = parseInt(jmlBayar) / $('#rate').val().replace(/[^0-9]/g, "");
				$('#jumlah_bayar').val(formatNumber(valBayar));
			}
		});
		$('#konversi').on('change', function() {
			if(this.value == '') {
				this.value = 0;
				$('#jumlah_bayar').val(0);
			}else this.value = formatNumber(this.value.replace(/[^0-9]/g, ""));
		});
	});

	$('#bayar_form').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		var jml_byr 		= $('#jumlah_bayar').val().replace(/[^0-9]/g, "");
		var saldo_akhir 	= $('#saldo_akhir').val().replace(/[^0-9]/g, "");

		if(parseInt(jml_byr) > parseInt(saldo_akhir)) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Bayar");
			$('#jumlah_bayar').css('border', '3px #C33 solid').focus();
			$('#tick').empty();
			$("#tick").append('<span class="fa fa-close"> Jumlah harga lebih besar dari saldo hutang</span>');
			$('#tick').show();
			swal("Failed!", "Jumlah bayar lebih besar dari saldo hutang.", "error");
		}else{
			var formData = new FormData();
			var str = $('#keterangan').val();
			var keterangan = str.replace('Pembelian','Pembayaran');
			var ket = '';
			
			if(str != '') ket = keterangan;
			else ket = 'Pembayaran Berhasil dilakukan';
			
			var id_hp = '<?php echo $list[0]['id_hp']; ?>';
			var status = 1;

			formData.set('id_hp',			id_hp);
			formData.set('tanggal_bayar',	$('#tanggal_bayar').val());
			formData.set('coa',				$('#id_coa').val());
			formData.set('valas',			$('#id_valas').val());
			formData.set('saldo_akhir',		saldo_akhir);
			formData.set('jml_byr',			jml_byr);
			formData.set('ket',				ket);
			formData.set('status',			status);
			formData.set('rate',			$('#rate').val());
			formData.set('konversi',		$('#konversi').val().replace(/[^0-9]/g, ""));

			if($('#file_bayar')[0].files.length > 0) {
				if($('#file_bayar')[0].files[0].size <= 9437184) {
					formData.set('file_bayar', $('#file_bayar')[0].files[0], $('#file_bayar')[0].files[0].name);
					save_Form(formData, $(this).attr('action'));
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Bayar");
					swal("Failed!", "Ukuran File Terlalu besar, silahkan cek kembali", "error");
				}
			}else save_Form(formData, $(this).attr('action'));
		}
	}));

	function save_Form(formData, url) {
		$.ajax({
			type:'POST',
			url: url,
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						$('#panel-modal').modal('toggle');
						listhutang();
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Bayar");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submites').text("Tambah Barang Baru");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}

/*
$('#btn-submited').on('click',(function(e) {
	if($('#tanggal').val() === ''){
		swal("Warning", "Tanggal pembayaran harus diisi.", "error");
	}else if($('#jumlah_bayar').val() === ''){
		swal("Warning", "Jumlah pembayaran harus diisi.", "error");
	}else if($('#id_valas').val() === ''){
		swal("Warning", "Tipe valas harus diisi.", "error");
	}else if($('#id_coa').val() === ''){
		swal("Warning", "Tipe pembayaran harus diisi.", "error");
	}else if($('#jumlah_bayar').val().length > $('#saldo').val().length){
		$('#btn-submites').removeAttr('disabled');
		$('#btn-submites').text("Bayar");
		$('#jumlah_bayar').css('border', '3px #C33 solid').focus();
		$('#tick').empty();
		$("#tick").append('<span class="fa fa-close"> Jumlah harga lebih besar dari saldo hutang</span>');
		$('#tick').show();
		swal("Failed!", "Jumlah bayar lebih besar dari saldo hutang.", "error");
	}else{
		$(this).attr('disabled','disabled');
		$(this).text("Memasukkan data...");

		var str = $('#keterangan').val();
		var keterangan = str.replace('Pembelian','Pembayaran');
		var ket = '';
		
		if(str != ''){
			ket = keterangan;
		}else{
			ket = 'Pembayaran Berhasil dilakukan';
		}
		
		var id_hp = '<?php echo $list[0]['id_hp']; ?>';
		var tanggal = $('#tanggal').val();
		var coa = $('#id_coa').val();
		var valas = $('#id_valas').val();
		var saldo_akhir = $('#saldo_akhir').val();
		var jumlah_bayar = $('#jumlah_bayar').val();
		var bayar2 = jumlah_bayar.replace("<?php echo $list[0]['symbol_valas'] ?>", "");
		
		var number = bayar2.replace(/[.,\s]/g, '', bayar2);
		//var pisah = number.split('<?php echo $list[0]['symbol_valas'] ?>');
		var jml_byr = number;
		
		//console.log(jml_byr);
		
		var status = 1;
		
		var dataitem = {
			id_hp,
			tanggal,
			coa,
			valas,
			saldo_akhir,
			jml_byr,
			ket,
			status
		}
		
		$.ajax({
			type:'POST',
			url: "",
			data:JSON.stringify(dataitem),
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						$('#panel-modal').modal('toggle');
						listhutang();
					})
				} else{
					$('#btn-submites').removeAttr('disabled');
					$('#btn-submites').text("Bayar");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submites').removeAttr('disabled');
			$('#btn-submites').text("Tambah Barang Baru");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}
  
}));
*/

	function isNumber(evt) {
		var iKeyCode = (evt.which) ? evt.which : evt.keyCode
		if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
			return false;
		return true;
	}
</script>

