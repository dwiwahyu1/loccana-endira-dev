<style>
	.dt-body-left {text-align:left;}
	.dt-body-right {text-align:right;}
	.dt-body-center {text-align:center; vertical-align: middle;}
	.force-overflow {height: 650px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow {min-height: 650px}
	#modal-distributor::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar
	{
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar-thumb
	{
		background-image: -webkit-gradient(linear,
		   left bottom,
		   left top,
		   color-stop(0.44, rgb(122,153,217)),
		   color-stop(0.72, rgb(73,125,189)),
		   color-stop(0.86, rgb(28,58,148)));
	}

</style>

 <div class="product-sales-area mg-tb-30">
            <div class="container-fluid">
                        <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            <ul id="myTabedu1" class="tab-review-design">
                                <li class="active"><a href="#description">Tambah Principal</a></li>
                            </ul>
                            <div id="myTabContent" class="tab-content custom-product-edit">
                                <div class="product-tab-list tab-pane fade active in" id="description">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="review-content-section">
                                                <form id="add_principle" action="<?php echo base_url().'principal_management/add_principal';?>" class="add-department">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
														 <div class="form-group">
                                                                <input name="kode" id="kode" type="text" class="form-control" placeholder="Kode Principal">
                                                            </div>
                                                            <div class="form-group">
                                                                <input name="name" id="name"  type="text" class="form-control" placeholder="Nama Principal">
                                                            </div>
                                                            <div class="form-group">
                                                                <input name="alamat" id="alamat"  type="text" class="form-control" placeholder="Alamat Principal">
                                                            </div>
                                                            <div class="form-group">
                                                                <input name="telp" id="telp"  type="text" class="form-control" placeholder="Telephone">
                                                            </div>
															 <div class="form-group">
                                                                <input name="fax" id="fax"  type="text" class="form-control" placeholder="Fax">
                                                            </div>
                                                        </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
														 <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                                                <input name="bank1" id="bank1" type="text" class="form-control" placeholder="Bank 1">
                                                            </div>
                                                            <div class="form-group col-lg-8 col-md-8 col-sm-8">
                                                                <input name="norek1" id="norek1" type="text" class="form-control" placeholder="No Rek 1">
                                                            </div>
                                                           
														    <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                                                <input name="bank2" id="bank2" type="text" class="form-control" placeholder="Bank 2">
                                                            </div>
                                                            <div class="form-group col-lg-8 col-md-8 col-sm-8">
                                                                <input name="norek2" id="norek2" type="text" class="form-control" placeholder="No Rek 2">
                                                            </div>
															
															 <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                                                <input name="bank3" id="bank3" type="text" class="form-control" placeholder="Bank 3">
                                                            </div>
                                                            <div class="form-group col-lg-8 col-md-8 col-sm-8">
                                                                <input name="norek3" id="norek3" type="text" class="form-control" placeholder="No Rek 3">
                                                            </div>
                                                        </div>
													</div>

                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="payment-adress">
                                                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
																<button type="button" class="btn btn-danger waves-effect waves-light">Batal</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               </div>
                        </div>
                    </div>
                </div>
</div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body force-overflow" id="modal-distributor">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>


  function deletedistributor(id){
    swal({
        title: 'Yakin akan Menghapus ?',
        text: 'data tidak dapat dikembalikan bila sudah dihapus !',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak'
      }).then(function () {
          var datapost={
            "id"  :   id
          };

          $.ajax({
                type:'POST',
                url: "<?php echo base_url().'distributor/delete_distributor';?>",
                data:JSON.stringify(datapost),
                cache:false,
                contentType: false,
                processData: false,
                success: function(response) {
                    $('.panel-heading button').trigger('click');
                    listdist();
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                    })
                }
            });
      });
  }

  function listdist(){
	  var user_id = '0001';
	  var token = '093940349';
	  
	  
   $('#datatable_pricipal').DataTable({
	   //"dom": 'rtip',
		"bFilter": false,
		"aaSorting": [],
		"bLengthChange": true,
		'iDisplayLength': 10,
		"sPaginationType": "simple_numbers",
		"Info" : false,
		"processing": true,
        "serverSide": true,
        "destroy": true,
		"ajax": "<?php echo base_url().'principal_management/lists'?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token,
		"searching": true,
		"language": {
            "decimal": ",",
            "thousands": "."
        },
		"dom": 'l<"toolbar">frtip',
		"initComplete": function(){
                           $("div.toolbar").prepend('<div class="btn-group pull-left"><button type="button" class="btn btn-custon-rounded-two btn-primary" > Tambah </button></div>');
        }
	});	
  }

 // $(document).ready(function() {
                // $('#datatable_pricipal').dataTable();
                // $('#datatable-keytable').DataTable( { keys: true } );
                // $('#datatable-responsive').DataTable();
                // $('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
                // var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
            // } );

	$(document).ready(function(){
		
      listdist();
	  
	  $('#add_principle').on('submit', function(e){
		// validation code here
		//if(!valid) {
			e.preventDefault();

			// var datapost={
				// "kode"   :   $('#kode').val(),
				// "alamat" :   $('#alamat').val(),
				// "name"   :   $('#name').val(),
				// "telp"   :   $('#telp').val(),
				// "fax"    :   $('#fax').val(),
				// "norek1" :   $('#norek1').val(),
				// "bank1"  :   $('#bank1').val(),
				// "bank2"  :   $('#bank2').val(),
				// "norek2" :   $('#norek2').val(),
				// "bank3"  :   $('#bank3').val(),
				// "norek3" :   $('#norek3').val()
			// };
			
			var formData = new FormData(this);

          $.ajax({
              type:'POST',
				url: $(this).attr('action'),
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                success: function(response) {
                   // $('.panel-heading button').trigger('click');
                    //listdist();
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                    })
                }
            });
			
			
			//alert(kode);
	
	
		//}
	  });
  });
</script>