<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Piutang_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	/**
      * This function is get the list data in spb table
      * @param : $params is where condition for select query
      */

	public function lists($params = array()) {
		$sql 	= 'CALL piutang_list2(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter'],
				$params['filter_type']
			));

		$result = $query->result_array();
		
		

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();
		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);
		
		return $return;
	}
	
	public function filter_type_eksternal($type_eksternal) {
		$sql 	= 'CALL hp_filter_eksternal(?)';

		$query 	= $this->db->query($sql,array(
			$type_eksternal
		));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function report_get_valas() {
		$sql 	= 'CALL hp_get_valas()';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function check_eksternal($id_eks,$type) {
		$sql 	= 'CALL hp_check_eksternal(?,?)';

		$query 	= $this->db->query($sql,array(
			$id_eks,$type
		));
		
		$return['code'] = $this->db->affected_rows();
		$return['array'] = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function report_piutang_ideks($id_eks) {
		$sql 	= 'CALL hp_report_hutang_ideks(?)';

		$query 	= $this->db->query($sql,array(
			$id_eks
		));
		
		$return['code'] = $this->db->affected_rows();
		$return['list'] = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function report_piutang_views($id_eks,$type) {
		$sql 	= 'CALL hp_filter_report_hutang(?,?)';

		$query 	= $this->db->query($sql,array(
			$id_eks,$type
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function report_piutang_view($params = array()) {
		// print_r($params);die;
		$sql 	= 'CALL hp_filter_report_piutang_list(?, ?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql, array(
			$params['limit'],
			$params['offset'],
			$params['order_column'],
			$params['order_dir'],
			$params['filter'],
			$params['id_eks'],
			$params['type_eks']
		));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);
		
		return $return;
	}
	
	public function report_pay_debt($id_hp) {
		$sql 	= 'CALL hp_search_id(?)';

		$query 	= $this->db->query($sql,array(
			$id_hp
		));
		
		$return['code'] = $this->db->affected_rows();
		$return['list'] = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function report_get_coa_values() {
		$sql 	= 'CALL hp_coa_value()';

		$query 	= $this->db->query($sql);
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_id_coa($id_hp) {
		$sql 	= 'CALL hp_get_id_coa2(?)';

		$query 	= $this->db->query($sql,array(
			$id_hp
		));
		
		$return['code'] = $this->db->affected_rows();
		$return['list'] = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_kartu_hp($data) {
		$sql 	= 'CALL hp_add_piutang_pays(?,?,?,?,?,?,?,?,?,?)';
		
		$query 	= $this->db->query($sql,array(
			$data['results']['ref'],
			$data['results']['source'],
			$data['params']['ket'],
			$data['params']['status'],
			$data['params']['jml_byr'],
			$data['params']['saldo_akhir'],
			$data['params']['valas'],
			$data['results']['type_kartu'],
			$data['results']['id_master'],
			$data['results']['type_master']
		));
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$result['code']	= $this->db->affected_rows();
		$result['list']	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['code'] = $result['code'];
		$arr_result['results'] = $result['list'];
		
		return $arr_result;
	}
	
	public function add_payment_hp($data) {
		$sql 	= 'CALL hp_add_payment(?,?,?,?,?,?)';
		
		$query 	= $this->db->query($sql,array(
			$data['last_id'],
			$data['results']['id_hp'],
			$data['params']['jml_byr'],
			$data['params']['ket'],
			$data['params']['rate'],
			$data['params']['konversi']
		));
		
		$return['code'] = $this->db->affected_rows();
		$return['list'] = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_coa_values($data) {
		$sql 	= 'CALL coavalue_add(?,?,?,?,?,?,?,?,?)';
		
		$query 	= $this->db->query($sql,array(
			$data['params']['coa'],
			0,
			$data['params']['valas'],
			$data['params']['jml_byr'],
			0,
			0,
			$data['params']['ket'],
			$data['params']['rate'],
			$data['params']['bukti']
		));
		
		$return['code'] = $this->db->affected_rows();
		$return['list'] = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_coa_values2($data) {
		$sql 	= 'CALL coavalue_add2(?,?,?,?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['coa']['id_coa'],
			0,
			$data['coa_value']['tanggal'],
			$data['coa_value']['valas'],
			$data['coa_value']['jml_byr'],
			0,
			$data['coa_value']['status'],
			$data['coa_value']['ket'],
			$data['coa_value']['rate'],
			$data['coa_value']['bukti']
		));
		
		$return['code'] = $this->db->affected_rows();
		$return['list'] = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
}