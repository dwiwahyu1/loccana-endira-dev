	<style>
		#loading-us{display:none}
		#tick{display:none}

		#loading-mail{display:none}
		#cross{display:none}
		.add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
		.add_item:hover{color:#ff8c00}
		.form-item{margin-top: 15px;overflow: auto;}
	</style>
	
	
		<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
		<br>
  		<div class="item form-group form-item">
        	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_po">Nama Customer <span class="required"><sup>*</sup></span></label>
            <div class="col-md-8 col-sm-6 col-xs-12">
               	<select class="form-control" name="id_cuss" id="id_cuss" style="width: 100%" required>
					<option value="">Silahkan Pilih Customer</option>
					<?php foreach($filter as $key) { ?>
						<option value="<?php echo $key['id_eksternal']; ?>" ><?php echo $key['name_eksternal']; ?></option>
					<?php } ?>
                </select>
            </div>
        </div>
		
		<div class="item form-group form-item detail-eksternal">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_spb">Type </label>
			<div class="col-md-8 col-sm-6">
				<input placeholder="Tyep Eksternal" type="text" class="form-control col-md-9 col-xs-12" id="type_eksternal" name="type_eksternal" readonly>
				<span id="loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking type eksternal...</span>
				<span id="tick"></span>
			</div>
		</div>
			
		<div class="item form-group form-item detail-eksternal">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="keterangan">Keterangan </label>
			<div class="col-md-8 col-sm-6">
				<input placeholder="Type Eksternal" type="hidden" class="form-control col-md-9 col-xs-12" id="typex" name="typex" readonly>
				<input placeholder="Keterangan" type="text" class="form-control col-md-9 col-xs-12" id="keterangan" name="keterangan" readonly>
				<span id="loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking Keterangan...</span>
				<span id="tick"></span>
			</div>
		</div>
		<hr>
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
            <div class="col-md-8 col-sm-6 col-xs-12">
            	<button id="btn-submited" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Filter</button>
            </div>
        </div>
	
<script type="text/javascript">
var items = [];
$(document).ready(function() {
    $('.detail-eksternal').hide();
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();
	$('#id_cuss').select2();
});

var id_eks = $('#id_cuss').val();
$('#id_cuss').on('change',function(event) {
	var id = $('#id_cuss').val();
	if($('#id_cuss').val() != id_eks) {
		select_eks();
	}else {
		$('#id_cuss').removeAttr("style");
		$('.detail-eksternal').hide();
		$('#tick').empty();
		$('#tick').hide();
		$('#loading-us').hide();
	}
});

function select_eks() {
	var id = $('#id_cuss').val();
	if(id.length != '') {
		var post_data = {
			'id': id
		};
		
		$('#tick').empty();
		$('#tick').hide();
		$('#loading-us').show();
		
		jQuery.ajax({
			type: "POST",
			url: "<?php echo base_url('piutang/check_eksternal');?>"+'/'+id+'/1',
			data: post_data,
			cache: false,
			success: function(response){
				if(response.success == true){
					//$('#id_cuss').css('border', '3px #090 solid');
					$('#typex').val(response.typex);
					$('#type_eksternal').val(response.type_eksternal);
					$('#type_eksternal').css({'border': '3px #090 solid', 'width': '100%'});
					$('#keterangan').val(response.keterangan);
					$('#keterangan').css({'border': '3px #090 solid', 'width': '100%'});
					$('.detail-eksternal').show();
					$('#loading-us').hide();
					$('#tick').empty();
					$('#tick').show();
				}else {
					$('#id_cuss').css('border', '3px #C33 solid');
					$('#type_eksternal').css('border', '3px #090 solid');
					$('#keterangan').css('border', '3px #090 solid');
					$('.detail-eksternal').show();
					$('#loading-us').hide();
					$('#tick').empty();
					$("#tick").append('<span class="fa fa-close"> '+response.message+'</span>');
					$('#tick').show();
				}
			}
		});
	}else {
		$('#id_cuss').css('border', '3px #C33 solid');
		$('#loading-us').hide();
		$('#tick').empty();
		$("#tick").append('<span class="fa fa-close"> This value is null</span>');
		$('#tick').show();
	}
}

$('#btn-submited').on('click',(function(e) {
    if($('#id_cuss').val() === ''){
        swal("Warning", "Nama Customer harus diisi.", "error");
    }else{
        $(this).attr('disabled','disabled');
        $(this).text("Memasukkan data...");

		var id_eks = $('#id_cuss').val();
		var typex = $('#typex').val();
		var type_eks = $('#type_eksternal').val();
		var keterangan = $('#keterangan').val();
		
		
        var id_eks     		= id_eks,
			typex     		= typex,
            type_eks        = type_eks,
            keterangan      = keterangan;
		
        var dataitem = {
            id_eks,
			typex,
            type_eks,
            keterangan
        };
		report_filter(dataitem);
		$('#panel-modal').modal('hide');
    }
  
}));
</script>
