<style>
  .changed_status{cursor:pointer;text-decoration: underline;color:#96b6e8;}
  .changed_status:hover{color:#ff8c00}
  .custom-tables, th{text-align:center;vertical-align:middle;}
  .custom-tables.align-text, th{vertical-align:middle;}
  .dt-body-right{text-align:right;}
  .dt-body-center{text-align:center;}
</style>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Piutang</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<table id="listpiutang" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Customer</th>
							<th>Tanggal</th>
							<th>Ref</th>
							<th>Source</th>
							<th>Tanggal Masuk</th>
							<th>Valas</th>
							<th>Saldo</th>
							<th>Due Date</th>
							<th>Keterangan</th>
							<th>Option</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>

			</div>
		</div><!-- end col -->
	</div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:50%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal-child" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg" style="width:90%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script> 
function report_detail(){
	$('#panel-modal').removeData('bs.modal');
	$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal  .panel-body').load('<?php echo base_url('piutang/report_detail/1');?>');
	$('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Report Filter');
	$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}

function pay_debt(id_hp){
	$('#panel-modal').removeData('bs.modal');
	$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal  .panel-body').load('<?php echo base_url('piutang/pay_debt');?>'+'/'+id_hp);
	$('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Bayar Piutang');
	$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}

function report_filter(dataitem){
	$('#panel-modal-child').removeData('bs.modal');
	$('#panel-modal-child  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal-child  .panel-body').load('<?php echo base_url('piutang/report_filter');?>'+"/"+dataitem['id_eks']);
	$('#panel-modal-child  .panel-title').html('<i class="fa fa-search"></i> Report Filter '+dataitem['keterangan']);
	$('#panel-modal-child').modal({backdrop:'static',keyboard:false},'show');
}

function listpiutang(){
	$("#listpiutang").dataTable({
		"processing": true,
		"serverSide": true,
		"ajax": "<?php echo base_url().'piutang/lists/';?>",
		"searchDelay": 700,
		"responsive": true,
		"lengthChange": false,
		"destroy": true,
		"info": false,
		"bSort": false,
		"dom": 'l<"toolbar">frtip',
		"columnDefs": [{
			targets: [7],
			className: 'dt-body-right'
		}],
		"initComplete": function(){
			var element = '<div class="btn-group pull-left">';
				element += '  	<a class="btn btn-primary" onClick="report_detail()">';
				element += '    	<i class="fa fa-search"></i> Report Detail';
				element += '  	</a>';
				element += '</div>';
			$("div.toolbar").prepend(element);
		}
	});
}

$(document).ready(function(){
	listpiutang();
});
</script>