	<style>
		.dt-body-left {text-align:left;}
		.dt-body-right {text-align:right;}
		.dt-body-center {text-align:center; vertical-align: middle;}
		#loading-us{display:none}
		#tick{display:none}

		#loading-mail{display:none}
		#cross{display:none}
		.add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
		.add_item:hover{color:#ff8c00}
		.right-text{text-align:right}
	</style>

	<form class="form-horizontal form-label-left" id="invoice_add" role="form" action="<?php echo base_url('invoice/insert_invoice');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

  		<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
		<br>
        <div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_invoice">No Invoice <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input placeholder="No. Invoice" type="text" class="form-control" id="no_invoice" name="no_invoice">
				<span id="loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking No Invoice...</span>
				<span id="tick"></span>
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_invoice">Tanggal Invoice <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<div class="input-group date">
					<input type="text" class="form-control col-md-7 col-xs-12 datepicker" onChange="setDueDate();" id="tanggal_invoice" name="tanggal_invoice" placeholder="<?php echo date('Y-m-d'); ?>">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-th"></span>
					</div>
				</div>
			</div>
		</div>
		
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="table_invoice">List Packing <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<table id="listpacking" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
                        <tr>
							<th>No</th>
							<th>No Packing</th>
							<th>Qty Box</th>
							<th>Qty Array</th>
							<th>Option</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
					<tfoot>
						<tr>
							<th colspan="2" style="text-align: right;">Total Qty</th>
							<td style="text-align: right;" id="tdQty_box_Total"></td>
							<td style="text-align: right;" id="tdQty_array_Total"></td>
							<td></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
		
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="due_date">Due Date <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<div class="input-group date">
					<input type="text" class="form-control col-md-7 col-xs-12" id="due_date" name="due_date" placeholder="<?php echo date('Y-m-d'); ?>" readOnly>
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-th"></span>
					</div>
				</div>
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty">Qty <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input min="0" placeholder="Qty" type="text" class="form-control" id="qty" name="qty" style="text-align:right" readOnly>
				<input min="0" placeholder="Qty" type="hidden" class="form-control" id="qtyc" name="qtyc" style="text-align:right" readOnly>
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="amount">Amount <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input data-parsley-minlength="0" placeholder="Amount" type="text" class="form-control" style="text-align:right;" id="amount" name="amount" readOnly>
				<input placeholder="Amount" type="hidden" class="form-control" style="text-align:right;" id="amountc" name="amountc" readOnly>
				<input placeholder="PO Quot" type="hidden" class="form-control" style="text-align:right;" id="id_po_quot" name="id_po_quot" value="<?php if(isset($invoice[0]['id_po_quot'])) {echo $invoice[0]['id_po_quot'];} ?>" readOnly>
				<input placeholder="COA" type="hidden" class="form-control" style="text-align:right;" id="id_coa" name="id_coa" value="<?php if(isset($coa[0]['id_coa'])) {echo $coa[0]['id_coa'];} ?>" readOnly>
			</div>
		</div>
	
		<hr>
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Simpan</button>
			</div>
		</div>
	</form>
	
	<script type="text/javascript">
	var dataPacking = [];
	var t_packing;
	var last_no_invoice = $('#no_invoice').val();
	var qty = document.getElementById('qty').value;
	var qtyx = 0;
	var amount = 0;
	var total_amount = 0;
	var disc_price = '<?php echo $invoice[0]['disc_price'];?>';
	
	$(document).ready(function() {
		//$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
		
		$('#tanggal_invoice').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true,
			changeYear: true,
			minDate: '-3M',
			maxDate: '+30D',
		});
		
		dtPacking();
	});
	
	$('#no_invoice').on('input',function(event) {
		if($('#no_invoice').val() != last_no_invoice) {
			no_invoice_check();
		}
	});

	function no_invoice_check() {
		var no_invoice = $('#no_invoice').val();
		if(no_invoice.length > 3) {
			var post_data = {
				'no_invoice': no_invoice
			};

			$('#tick').empty();
			$('#tick').hide();
			$('#loading-us').show();
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url('invoice/check_no_invoice');?>",
				data: post_data,
				cache: false,
				success: function(response){
					if(response.success == true){
						$('#no_invoice').css('border', '3px #090 solid');
						$('#loading-us').hide();
						$('#tick').empty();
						$("#tick").append('<span class="fa fa-check"> '+response.message+'</span>');
						$('#tick').show();
					}else {
						$('#no_invoice').css('border', '3px #C33 solid');
						$('#loading-us').hide();
						$('#tick').empty();
						$("#tick").append('<span class="fa fa-close"> '+response.message+'</span>');
						$('#tick').show();
					}
				}
			});
		}else {
			$('#no_invoice').css('border', '3px #C33 solid');
			$('#loading-us').hide();
			$('#tick').empty();
			$("#tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
			$('#tick').show();
		}
	}
	
	function setDueDate() {
		var tanggal = $('#tanggal_invoice').val();
		var tanggalInvoice = new Date(tanggal);
		var dueDate = new Date(tanggalInvoice);
		var priceTerm = '<?php echo preg_replace('/\D/', '', $invoice[0]['price_term']); ?>';
		
		dueDate.setDate(dueDate.getDate() + parseInt(priceTerm));
		
		var dd = dueDate.getDate();
		var mm = dueDate.getMonth() + 1;
		var y = dueDate.getFullYear();
		if (dd < 10) {
			dd = '0' + dd;
		} 
		var tanggalDueDate = y + '-' + mm + '-' + dd;
		
		document.getElementById('due_date').value = tanggalDueDate;
	}

	function getValueCheck(row) {
		var rowData = t_packing.data();
		var ins = 0;
		
		QtyBox = 0;
		QtyArray = 0;
		
		$('input[type=checkbox]').each(function () {
			
			if(this.checked == true) {
				QtyBox = QtyBox + parseInt(rowData[ins][2]);
				QtyArray = QtyArray + parseInt(rowData[ins][3]);
			}
			ins = ins + 1;
		});
		
		totalQtyBox = QtyBox;
		totalQtyArray = QtyArray;
		totalQty = totalQtyBox * totalQtyArray;
		
		qtyx = formatCurrencyRupiah(totalQty);
		$('#qty').val(qtyx);
		$('#qtyc').val(totalQty);
		 
		amount = parseInt(disc_price) * parseInt(totalQty);
		total_amount = formatCurrencyRupiah(amount);
		$('#amount').val(total_amount);
		$('#amountc').val(amount);
	}
	
	function dtPacking() {
		t_packing = $('#listpacking').DataTable( {
			"processing": true,
			"searching": false,
			"responsive": true,
			"lengthChange": false,
			"ajax": {
				"type" : "GET",
				"url" : "<?php echo base_url().'invoice/invoice_search_packing/'.$invoice[0]['id_po_quot'];?>",
				"dataSrc": function(response) {
					$('#tdQty_box_Total').html(formatCurrencyComa(response.totBox));
					$('#tdQty_array_Total').html(formatCurrencyComa(response.totArray));
                	return response.data;
                }
            },
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0],
				"visible": false,
				"searchable": false
			},{
				"targets": [2],
				"searchable": false,
				"className": 'dt-body-right',
			},{
				"targets": [3],
				"searchable": false,
				"className": 'dt-body-right',
			}]
		});
	}
	
	$('#invoice_add').on('submit',(function(e) {
		if($('#no_invoice').val() !== '' &&  $('#tanggal_invoice').val() !== '' &&  $('#dueDate').val() !== '' &&  $('#qty').val() !== '' &&  $('#amount').val() !== ''){
			$('#btn-submit').attr('disabled','disabled');
			$('#btn-submit').text("Memasukkan data...");
			e.preventDefault();
			var formData = new FormData(this);
			
			$.ajax({
				type:'POST',
				url: $(this).attr('action'),
				data:formData,
				cache:false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function () {
							$('#panel-modal').modal('toggle');
							listinvoice();
						})
					} else{
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Simpan");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Simpan");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}else{
			swal("Failed!", "Maaf kolom yang kosong harus diisi", "error");
		}
		return false;
	}));
	
	function isNumber(evt) {
		var iKeyCode = (evt.which) ? evt.which : evt.keyCode
		if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
			return false;

		return true;
	}
	</script>