<style>
	.dt-body-left {text-align:left;}
	.dt-body-right {text-align:right;}
	.dt-body-center {text-align:center; vertical-align: middle;}
	.force-overflow {height: 450px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow {min-height: 650px}
	#modal-invoice::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	#modal-invoice::-webkit-scrollbar
	{
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-invoice::-webkit-scrollbar-thumb
	{
		background-image: -webkit-gradient(linear,
		   left bottom,
		   left top,
		   color-stop(0.44, rgb(122,153,217)),
		   color-stop(0.72, rgb(73,125,189)),
		   color-stop(0.86, rgb(28,58,148)));
	}
</style>

<div class="container" style="height: auto;">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Invoice</h4>
		</div>
	</div>

	<div class="row">
        <div class="col-sm-12">
            <div class="card-box">
				<table id="listinvoice" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
                        <tr>
							<th>No</th>
							<th>Order No</th>
							<th>Nama Customer</th>
							<th>Tanggal Order</th>
							<th>Total Produk</th>
							<th>Total Amount</th>
							<th>Option</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
            </div>
        </div><!-- end col -->
    </div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-invoice">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
function invoice_add(id){
	$('#panel-modal').removeData('bs.modal');
	$('#panel-modal .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal .panel-body').load('<?php echo base_url('invoice/invoice_add');?>'+'/'+id);
	$('#panel-modal .panel-title').html('<i class="fa fa-plus"></i> Tambah Invoice');
	$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

function invoice_delete(id){
	swal({
		title: 'Yakin akan Menghapus ?',
		text: 'data tidak dapat dikembalikan bila sudah dihapus !',
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Ya',
		cancelButtonText: 'Tidak'
	}).then(function () {
		var datapost={
			"id"  :   id
		};
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('invoice/invoice_delete');?>",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
			   swal({
					title: 'Success!',
					text: response.message,
					type: 'success',
					showCancelButton: false,
					confirmButtonText: 'Ok'
				}).then(function () {
					window.location.href = "<?php echo base_url('invoice');?>";
				})
				if (response.status == "success") {
				} else{
                    swal("Failed!", response.message, "error");
				}
			}
		});
	})
}

function listinvoice(){
	$("#listinvoice").dataTable({
		"processing": true,
		"serverSide": true,
		"ajax": "<?php echo base_url().'invoice/lists';?>",
		"searchDelay": 700,
		"responsive": true,
		"lengthChange": false,
		"destroy": true,
		"info": false,
		"bSort": false,
		"dom": 'l<"toolbar">frtip',
		"columnDefs": [{
			"targets": [0],
			"searchable": false,
			"className": 'dt-body-center',
			"width": 10
		},{
			"targets": [1],
			"searchable": false,
			"className": 'dt-body-left',
			"width": 120
		},{
			"targets": [2],
			"searchable": false,
			"className": 'dt-body-left',
			"width": 250,
		},{
			"targets": [3],
			"searchable": false,
			"className": 'dt-body-left',
			"width": 150
		},{
			"targets": [4],
			"searchable": false,
			"className": 'dt-body-right',
			"width": 80
		},{
			"targets": [5],
			"searchable": false,
			"className": 'dt-body-right',
			"width": 150
		},{
			"targets": [6],
			"searchable": false,
			"className": 'dt-body-center',
			"width": 70
		}]
    });
}

$(document).ready(function(){
	listinvoice();
});
</script>