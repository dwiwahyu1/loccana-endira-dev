<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Invoice_model extends CI_Model {
	
	public function __construct() {
		
		parent::__construct();
		
	}

	public function lists($params = array())
	{
		$sql 	= 'CALL inv_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();
	
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);
		
		return $return;
	}
	
	public function invoice_search_id($id) {
		$sql 	= 'CALL inv_id(?)';

		$query 	=  $this->db->query($sql,
			array(
				$id
			));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function invoice_list_packing($id) {
		$sql 	= 'CALL inv_search_packing(?)';

		$query 	=  $this->db->query($sql,
			array(
				$id
			));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function invoice_coa($custid) {
		$sql 	= 'CALL inv_coa_id(?)';

		$query 	=  $this->db->query($sql,
			array(
				$custid
			));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function insert_invoice($data)
	{
		$sql 	= 'CALL inv_add(?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_po_quot'],
			$data['no_invoice'],
			$data['tanggal_invoice'],
			$data['due_date'],
			$data['qty'],
			$data['amount']
		));
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function insert_invoice_coa_value($data)
	{
		$sql 	= 'CALL coavalue_add_invoice(?,?,?,?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_coa'],
			$data['id_parent'],
			$data['date'],
			$data['id_valas'],
			$data['value'],
			$data['adjustment'],
			$data['type_cash'],
			$data['note'],
			$data['rate'],
			$data['bukti']
		));
		
		$results	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $results;
	}
}