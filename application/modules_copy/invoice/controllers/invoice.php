<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('invoice/invoice_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'invoice/views/index');
	}

	function lists() {
        $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
        $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
        $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
        $order = $this->input->get_post('order');
        $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'asc';
        $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

        $order_fields = array('', 'name_eksternal');

        $search = $this->input->get_post('search');

        $search_val = (!empty($search['value'])) ? $search['value'] : null;

        $search_value = $this->Anti_sql_injection($search_val);

        // Build params for calling model
        $params['limit'] = (int) $length;
        $params['offset'] = (int) $start;
        $params['order_column'] = $order_fields[$order_column];
        $params['order_dir'] = $order_dir;
        $params['filter'] = $search_value;
		
        $list = $this->invoice_model->lists($params);
		
        $result["recordsTotal"] = $list['total'];
        $result["recordsFiltered"] = $list['total_filtered'];
        $result["draw"] = $draw;
		
        $data = array();
		$no=1;
        foreach ($list['data'] as $k => $v) {
            $no = $k+1;
			$actions = '';
			
			$actions = '<div class="btn-group">';
			$actions .= '   <button class="btn btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Tambah Invoice" onClick="invoice_add(\'' . $v['id'] . '\')">';
			$actions .= '       <i class="fa fa-plus"></i>';
			$actions .= '   </button>';
			$actions .= '</div>';
				
            array_push($data, array(
                $no,
                $v['order_no'],
                $v['name_eksternal'],
                $v['order_date'],
                $v['total_products'],
                $v['symbol_valas'].' '. number_format($v['total_amount'],4,',','.'),
				$actions
				)
            );
        }
		
        $result["data"] = $data;
		
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	public function invoice_add($id) {
		$results = $this->invoice_model->invoice_search_id($id);
		$coa	= $this->invoice_model->invoice_coa($results[0]['cust_id']);
		
		$data = array(
			'invoice' => $results,
			'coa'	=> $coa
		);
		
		$this->load->view('invoice_add_view',$data);
	}
	
	public function invoice_search_packing($id_po_quot) {
		$list = $this->invoice_model->invoice_list_packing($id_po_quot);
		
		$data = array();
		$i = 0;
		$totQty_box = 0;
		$totQty_array = 0;
		$username = $this->session->userdata['logged_in']['username'];

		foreach ($list as $k => $v) {
			$jumlah_box[] = $v['qty_box'];
			$jumlah_array[] = $v['qty_array'];
			$totQty_box = array_sum($jumlah_box);
			$totQty_array = array_sum($jumlah_array);
			
			$strOption = '<div class="checkbox">';
			$strOption .=	'<input class="checks" id="option['.$i.']" type="checkbox" onClick="getValueCheck('.$i.')" value="'.$i.'">';
			$strOption .=	'<label for="option['.$i.']"></label>';
			$strOption .='</div>';
			
			array_push($data, array(
				$v['id_packing'],
				$v['no_packing'],
				$v['qty_box'],
				$v['qty_array'],
				$strOption
			));
			$i++;
		}
		
		$results["data"] = $data;
		$results["totBox"] = $totQty_box;
		$results["totArray"] = $totQty_array;

		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}
	
	public function insert_invoice() {
		$id_po_quot			= $this->Anti_sql_injection($this->input->post('id_po_quot', TRUE));
		$no_invoice			= $this->Anti_sql_injection($this->input->post('no_invoice', TRUE));
		$tanggal_invoice	= $this->Anti_sql_injection($this->input->post('tanggal_invoice', TRUE));
		$due_date			= $this->Anti_sql_injection($this->input->post('due_date', TRUE));
		$qty 				= $this->Anti_sql_injection($this->input->post('qty', TRUE));
		$qtyc 				= $this->Anti_sql_injection($this->input->post('qtyc', TRUE));
		$amount 			= $this->Anti_sql_injection($this->input->post('amount', TRUE));
		$amountc 			= $this->Anti_sql_injection($this->input->post('amountc', TRUE));
		
		$results_id = $this->invoice_model->invoice_search_id($id_po_quot);
		$coa	= $this->invoice_model->invoice_coa($results_id[0]['cust_id']);
		
		$data = array(
			'id_po_quot'		=> $id_po_quot,
			'no_invoice'		=> $no_invoice,
			'tanggal_invoice'	=> $tanggal_invoice,
			'due_date'			=> $due_date,
			'qty'				=> $qtyc,
			'amount'			=> $amountc
		);
		
		$results = $this->invoice_model->insert_invoice($data);
		
		if ($results > 0) {
			
			$data_coa = array(
				'id_coa' 		=> $coa[0]['id_coa'],
				'id_parent' 	=> 0,
				'date' 			=> $tanggal_invoice,
				'id_valas' 		=> $results_id[0]['valas_id'],
				'value' 		=> $amountc,
				'adjustment' 	=> 0,
				'type_cash' 	=> 0,
				'note' 			=> 'Invoice PO dengan No PO '.$results_id[0]['no_po'],
				'rate' 			=> $results_id[0]['rate'],
				'bukti' 		=> NULL
			);
			
			$results = $this->invoice_model->insert_invoice_coa_value($data_coa);
			
			$msg = 'Berhasil menambahkan data Invoice';

			$results = array(
				'success' => true,
				'message' => $msg,
				'lastid' => $results['lastid']
			);
			
			$this->log_activity->insert_activity('insert', $msg. ' dengan No Invoice ' .$no_invoice);
		} else {
			$msg = 'Gagal menambahkan data Invoice ke database';

			$results = array(
				'success' => false,
				'message' => $msg
			);
			
			$this->log_activity->insert_activity('insert', $msg. ' dengan No Invoice ' .$no_invoice);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}
	
	public function invoice_delete() {
		
		$data 	= file_get_contents("php://input");
		$params = json_decode($data,true);
		
		$list = $this->invoice_model->invoice_delete($params);
		
		$msg = 'Data Invoice berhasil di hapus';
			
		$results = array(
			'success' => true,
			'status' => 'success',
			'message' => $msg
		);
		
		$this->log_activity->insert_activity('delete', $msg. ' dengan ID DO ' .$params['id_do']);
		
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
        echo json_encode($results);
    }
	
	public function check_no_invoice(){
		$this->form_validation->set_rules('no_invoice', 'no_invoice', 'trim|required|min_length[4]|max_length[20]|is_unique[t_invoice.no_invoice]');
		$this->form_validation->set_message('is_unique', 'No Invoice Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'No Invoice Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}
}