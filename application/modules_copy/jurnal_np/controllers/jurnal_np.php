<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Jurnal_NP extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('jurnal_np/jurnal_np_model');
		$this->load->model('jurnal/jurnal_model');
		$this->load->library('log_activity');
		$this->load->library('sequence');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string)
	{
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index()
	{
		$this->template->load('maintemplate', 'jurnal_np/views/index');
	}

	function lists_jurnal_np()
	{
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'ASC';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

		$order_fields = array('', 'nama');
		$search = $this->input->get_post('search');
		$search_val = (!empty($search['value'])) ? $search['value'] : null;
		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		if ($params['limit'] < 0) $params['limit'] = $this->payment_voucher_kas_model->count_payment($params);
		$list = $this->jurnal_np_model->list_jurnal_np($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = $params['offset'] + 1;
		foreach ($list['data'] as $k => $v) {
			if($v['status'] == 0) {
			$strBtn =
				'<div class="btn-group">' .
					'<a class="btn btn-primary btn-sm" title="Detail" onClick="detail_jurnal(\'' . $v['id_jurnal'] . '\')">' .
						'<i class="fa fa-search"></i>' .
					'</a>' .
				'</div>' .
				'<div class="btn-group">' .
					'<a class="btn btn-success btn-sm" title="Update" onClick="edit_jurnal(\'' . $v['id_jurnal'] . '\')">' .
						'<i class="fa fa-pencil"></i>' .
					'</a>' .
				'</div>' .
				'<div class="btn-group">' .
					'<a class="btn btn-danger btn-sm" title="Delete" onClick="delete_jurnal_temporary(\'' . $v['id_jurnal'] . '\')">' .
						'<i class="fa fa-trash"></i>' .
					'</a>' .
				'</div>';
			}else{
				$strBtn =
				'<div class="btn-group">' .
					'<a class="btn btn-primary btn-sm" title="Detail" onClick="detail_jurnal(\'' . $v['id_jurnal'] . '\')">' .
						'<i class="fa fa-search"></i>' .
					'</a>' .
				'</div>';
			}
			
			array_push($data, array(
				$i++,
				$v['no_voucher'],
				$v['nama'],
				date('d M Y', strtotime($v['date_input'])),
				$v['debit_detail'],
				$v['symbol_valas'] . ' ' . number_format(round($v['debit']), 0, ',', '.'),
				$v['kredit_detail'],
				$v['symbol_valas'] . ' ' . number_format(round($v['kredit']), 0, ',', '.'),
				$strBtn
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function add_jurnal()
	{
		$list_bank = $this->jurnal_np_model->list_bank();
		$coa_all = $this->jurnal_np_model->coa_list();
		$valas = $this->jurnal_model->valas();
		$data = array(
			'list_bank' => $list_bank,
			'coa_all' =>$coa_all,
			'valas'=>$valas
		);
		
		$this->load->view('add_modal_jurnal_view', $data);
	}
	
	public function edit_jurnal($id) {
		$list_bank 			= $this->jurnal_np_model->list_bank();
		$coa_all 			= $this->jurnal_np_model->coa_list();
		$valas 				= $this->jurnal_model->valas();
		$result_jurnal		= $this->jurnal_np_model->detail_jurnal_non_prod($id);
		$result_bank		= $this->jurnal_np_model->edit_payment_bank($id);
		
		for ($i = 0; $i < sizeof($result_jurnal); $i++) {
			if($result_jurnal[$i]['type_cash'] == 0) {
				$data_coa = array(
					'id_jurnal' => $id,
					'type_cash' => 0
				);
				
				$result_coa			= $this->jurnal_np_model->detail_list_jurnal_np_coa_temporary($data_coa);
				$result_total_coa	= $this->jurnal_np_model->detail_list_total_coa_temporary($data_coa);
			}else{
				$data_coa = array(
					'id_jurnal' => $id,
					'type_cash' => 1
				);
				
				$result_coa			= $this->jurnal_np_model->detail_list_jurnal_np_coa_temporary($data_coa);
				$result_total_coa	= $this->jurnal_np_model->detail_list_total_coa_temporary($data_coa);
			}
		}
		
		for ($i = 0; $i < sizeof($result_coa); $i++) {
			if ($result_coa[$i]['bukti'] != null) {
				$handle = fopen($result_jurnal_coa[$i]['bukti'], "r");
				$result_coa[$i]['have_file'] = "yes";
			} else {
				$result_coa[$i]['have_file'] = "no";
			}
		}
		
		$data = array(
			'detail_jurnal'	=> $result_jurnal,
			'detail_coa'	=> $result_coa,
			'total_coa'		=> $result_total_coa,
			'list_bank' 	=> $list_bank,
			'coa_all'		=> $coa_all,
			'valas'			=> $valas
		);
		
		$this->load->view('edit_modal_jurnal_view', $data);
	}
	
	public function change_coa()
	{
		$id = $this->input->get('id');
		$result = $this->jurnal_model->coa("id_parent", $id);

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function add_coa_value_masuk()
	{
		$coa_all = $this->jurnal_np_model->coa_list();
		$level1 = $this->jurnal_model->coa("id_parent", 0);
		$level2 = $this->jurnal_model->coa("id_parent", $level1[0]["coa"]);
		$level3 = $this->jurnal_model->coa("id_parent", $level2[0]["coa"]);
		$level4 = $this->jurnal_model->coa("id_parent", $level3[0]["coa"]);

		$valas = $this->jurnal_model->valas();

		$data = array(
			'coa_all' => $coa_all,
			// 'level1' => $level1,
			// 'level2' => $level2,
			// 'level3' => $level3,
			// 'level4' => $level4,
			'valas' => $valas
		);

		$this->load->view('add_modal_kas_masuk_view', $data);
	}
	
	public function add_coa_value_keluar()
	{
		$coa_all = $this->jurnal_np_model->coa_list();
		// $level1 = $this->jurnal_model->coa("id_parent", 0);
		// $level2 = $this->jurnal_model->coa("id_parent", $level1[0]["coa"]);
		// $level3 = $this->jurnal_model->coa("id_parent", $level2[0]["coa"]);
		// $level4 = $this->jurnal_model->coa("id_parent", $level3[0]["coa"]);

		$valas = $this->jurnal_model->valas();

		$data = array(
			'coa_all' => $coa_all,
			// 'level1' => $level1,
			// 'level2' => $level2,
			// 'level3' => $level3,
			// 'level4' => $level4,
			'valas' => $valas
		);

		$this->load->view('add_modal_kas_keluar_view', $data);
	}
	
	public function edit_coa_value_masuk()
	{
		
		$coa_all = $this->jurnal_np_model->coa_list();
		$level1 = $this->jurnal_model->coa("id_parent", 0);
		$level2 = $this->jurnal_model->coa("id_parent", $level1[0]["coa"]);
		$level3 = $this->jurnal_model->coa("id_parent", $level2[0]["coa"]);
		$level4 = $this->jurnal_model->coa("id_parent", $level3[0]["coa"]);

		$valas = $this->jurnal_model->valas();

		$data = array(
			'coa_all' => $coa_all,
			// 'level1' => $level1,
			// 'level2' => $level2,
			// 'level3' => $level3,
			// 'level4' => $level4,
			'valas' => $valas
		);

		$this->load->view('edit_modal_kas_masuk_view', $data);
	}
	
	public function edit_coa_value_keluar()
	{
		$coa_all = $this->jurnal_np_model->coa_list();
		$level1 = $this->jurnal_model->coa("id_parent", 0);
		$level2 = $this->jurnal_model->coa("id_parent", $level1[0]["coa"]);
		$level3 = $this->jurnal_model->coa("id_parent", $level2[0]["coa"]);
		$level4 = $this->jurnal_model->coa("id_parent", $level3[0]["coa"]);

		$valas = $this->jurnal_model->valas();

		$data = array(
			'coa_all' => $coa_all,
			// 'level1' => $level1,
			// 'level2' => $level2,
			// 'level3' => $level3,
			// 'level4' => $level4,
			'valas' => $valas
		);

		$this->load->view('edit_modal_kas_keluar_view', $data);
	}

	public function save_jurnal_np()
	{
		$user_id 		= $this->session->userdata['logged_in']['user_id'];
		$no_voucher 	= ($this->input->post('no_voucher', true));
		$tgl_bpvk 		= ($this->input->post('tgl_bpvk', true));
		$durasi 		= ($this->input->post('durasi', true));
		
		$id_coa_masuk 			= ($this->input->post('arrCoaMasuk',true));
		if($id_coa_masuk != '') {
			$id_parent_masuk		= ($this->input->post('id_parent_masuk', TRUE));
			$date_masuk 			= $tgl_bpvk;
			$id_valas_masuk 		= ($this->input->post('arrValasMasuk', TRUE));
			$value_masuk 			= ($this->input->post('arrValueKasMasuk', TRUE));
			$rate_masuk 			= ($this->input->post('rate_masuk', TRUE));
			$type_cash_masuk 		= ($this->input->post('type_cash_masuk', TRUE));
			$note_masuk 			= ($this->input->post('arrNoteMasuk', TRUE));
			$jumlah_masuk 			= ($this->input->post('jumlah_masuk', TRUE));
			
			$upload_error_masuk 	= NULL;
			
			$file_kas_masuk	= array();
			for ($i = 0; $i < sizeof($_FILES['file_kas_masuk']['name']); $i++) {
				$file_kas_masuk[$i]['file_kas_masuk']['name'] = $_FILES['file_kas_masuk']['name'][$i];
				if ($_FILES['file_kas_masuk']['name'][$i]) {
					$file_kas_masuk[$i]['file_kas_masuk']['final_name'] = 'uploads/jurnal_non_produksi/' . $_FILES['file_kas_masuk']['name'][$i];
				} else {
					$file_kas_masuk[$i]['file_kas_masuk']['final_name'] = $_FILES['file_kas_masuk']['name'][$i];
					$file_kas_masuk[$i]['file_kas_masuk']['type'] = $_FILES['file_kas_masuk']['type'][$i];
					$file_kas_masuk[$i]['file_kas_masuk']['tmp_name'] = $_FILES['file_kas_masuk']['tmp_name'][$i];
					$file_kas_masuk[$i]['file_kas_masuk']['error'] = $_FILES['file_kas_masuk']['error'][$i];
					$file_kas_masuk[$i]['file_kas_masuk']['size'] = $_FILES['file_kas_masuk']['size'][$i];
				}
			}

			for ($i = 0; $i < sizeof($file_kas_masuk); $i++) {
				if (sizeof($file_kas_masuk) > 0) {
					if ($file_kas_masuk[$i]['file_kas_masuk']['name']) {
						if (!move_uploaded_file($file_kas_masuk[$i]['file_kas_masuk']['tmp_name'], $file_kas_masuk[$i]['file_kas_masuk']['final_name'])) {
							$pesan = "Gagal Upload File!";
							$upload_error_masuk = strip_tags(str_replace("\n", '', $pesan));

							$results = array('success' => false, 'message' => $upload_error_masuk);
						}
					}
				}
			}
		}
		
		$id_coa_keluar 			= ($this->input->post('arrCoaKeluar',true));
		if($id_coa_keluar != '') {
			$id_parent_keluar		= ($this->input->post('id_parent_keluar', TRUE));
			$date_keluar 			= $tgl_bpvk;
			$id_valas_keluar 		= ($this->input->post('arrValasKeluar', TRUE));
			$value_keluar 			= ($this->input->post('arrValueKasKeluar', TRUE));
			$rate_keluar 			= ($this->input->post('rate_keluar', TRUE));
			$type_cash_keluar 		= ($this->input->post('type_cash_keluar', TRUE));
			$note_keluar 			= ($this->input->post('arrNoteKeluar', TRUE));
			$jumlah_keluar 			= ($this->input->post('_keluar', TRUE));
			
			$upload_error_keluar 	= NULL;
			$file_kas_keluar	= array();
			for ($i = 0; $i < sizeof($_FILES['file_kas_keluar']['name']); $i++) {
				$file_kas_keluar[$i]['file_kas_keluar']['name'] = $_FILES['file_kas_keluar']['name'][$i];
				if ($_FILES['file_kas_keluar']['name'][$i]) {
					$file_kas_keluar[$i]['file_kas_keluar']['final_name'] = 'uploads/jurnal_non_produksi/' . $_FILES['file_kas_keluar']['name'][$i];
				} else {
					$file_kas_keluar[$i]['file_kas_keluar']['final_name'] = $_FILES['file_kas_keluar']['name'][$i];
					$file_kas_keluar[$i]['file_kas_keluar']['type'] = $_FILES['file_kas_keluar']['type'][$i];
					$file_kas_keluar[$i]['file_kas_keluar']['tmp_name'] = $_FILES['file_kas_keluar']['tmp_name'][$i];
					$file_kas_keluar[$i]['file_kas_keluar']['error'] = $_FILES['file_kas_keluar']['error'][$i];
					$file_kas_keluar[$i]['file_kas_keluar']['size'] = $_FILES['file_kas_keluar']['size'][$i];
				}
			}

			for ($i = 0; $i < sizeof($file_kas_keluar); $i++) {
				if (sizeof($file_kas_keluar) > 0) {
					if ($file_kas_keluar[$i]['file_kas_keluar']['name']) {
						if (!move_uploaded_file($file_kas_keluar[$i]['file_kas_keluar']['tmp_name'], $file_kas_keluar[$i]['file_kas_keluar']['final_name'])) {
							$pesan = "Gagal Upload File!";
							$upload_error_keluar = strip_tags(str_replace("\n", '', $pesan));

							$results = array('success' => false, 'message' => $upload_error_keluar);
						}
					}
				}
			}
		}
		
		$result_jurnal_np = array();
		$result_jurnal_np_coa_value = array();
		
		$date = date_create($tgl_bpvk);
		
		// if($durasi == '0'){
			// $newDate = date_create($tgl_bpvk);
			
			// $newDate = $newDate->format('Y-m-d');
			
		// }else{
			// $due_date = date('Y-m-d', strtotime($tgl_bpvk. ' + '.$durasi.' days'));
			
			
			
			// $newDate = $due_date->format('Y-m-d');
		// }
		
		if (!isset($upload_error_masuk) && !isset($upload_error_keluar)) {
			
			$data_jurnal = array(
				'no_voucher'	=> $no_voucher,
				'date_input'	=> date_format($date, 'Y-m-d H:i:s'),
				'pic'			=> $user_id,
				'due_date'		=> $durasi
			);
			
			$result_jurnal_np = $this->jurnal_np_model->add_jurnal_np($data_jurnal);
			$id_jurnal = $result_jurnal_np['lastid'];
			
			if ($result_jurnal_np) {
				
				$this->log_activity->insert_activity('insert', 'Insert Jurnal Non Produksi Tanggal : ' . $data_jurnal['date_input']);
				
				for ($i = 0; $i < sizeof($id_coa_masuk); $i++) {
					$data_coa_masuk = array(
						'id_coa' 		=> $id_coa_masuk[$i],
						'id_parent' 	=> $id_parent_masuk[$i],
						'date' 			=> $tgl_bpvk,
						'id_valas' 		=> $id_valas_masuk[$i],
						'value' 		=> $value_masuk[$i],
						'adjusment' 	=> 0,
						'type_cash' 	=> $type_cash_masuk[$i],
						'note' 			=> $note_masuk[$i],
						'rate' 			=> $rate_masuk[$i],
						'bukti' 		=> $file_kas_masuk[$i]['file_kas_masuk']['name'],
						'pic' 			=> $user_id,
						'pic_approve' 	=> NULL,
						'status' 		=> 0
					);
					
					$result_coa_masuk = $this->jurnal_np_model->add_coa_value_jurnal_temporary($data_coa_masuk);
					$this->log_activity->insert_activity('insert', 'Insert Jurnal Coa Value ID Coa Value : ' . $data_coa_masuk['id_coa']);
					
					$data_jurnal_coa_value = array(
						'id_jurnal_np'	=> $id_jurnal,
						'id_coa_value'	=> $result_coa_masuk['lastid'],
					);
					
					$result_jurnal_coa_value = $this->jurnal_np_model->add_jurnal_coa_value_temporary($data_jurnal_coa_value);
					$this->log_activity->insert_activity('insert', 'Insert Jurnal Coa Value ID Jurnal : ' . $data_jurnal_coa_value['id_jurnal_np']);
				}
				
				for ($i = 0; $i < sizeof($id_coa_keluar); $i++) {
					$data_coa_keluar = array(
						'id_coa' 		=> $id_coa_keluar[$i],
						'id_parent' 	=> $id_parent_keluar[$i],
						'date' 			=> $tgl_bpvk,
						'id_valas' 		=> $id_valas_keluar[$i],
						'value' 		=> $value_keluar[$i],
						'adjusment' 	=> 0,
						'type_cash' 	=> $type_cash_keluar[$i],
						'note' 			=> $note_keluar[$i],
						'rate' 			=> $rate_keluar[$i],
						'bukti' 		=> $file_kas_keluar[$i]['file_kas_keluar']['name'],
						'pic' 			=> $user_id,
						'pic_approve' 	=> NULL,
						'status' 		=> 0
					);
					
					$result_coa_keluar = $this->jurnal_np_model->add_coa_value_jurnal_temporary($data_coa_keluar);
					$this->log_activity->insert_activity('insert', 'Insert Jurnal Coa Value ID Coa Value : ' . $data_coa_keluar['id_coa']);
					
					$data_jurnal_coa_value = array(
						'id_jurnal_np'	=> $id_jurnal,
						'id_coa_value'	=> $result_coa_keluar['lastid'],
					);
					
					$result_jurnal_coa_value = $this->jurnal_np_model->add_jurnal_coa_value_temporary($data_jurnal_coa_value);
					$this->log_activity->insert_activity('insert', 'Insert Jurnal Coa Value ID Jurnal : ' . $data_jurnal_coa_value['id_jurnal_np']);
				}
				
				$results = array('success' => true, 'message' => 'Berhasil menambahkan Jurnal Non Produksi ke database');
				
			} else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert Jurnal Non Produksi tanggal ' . $tgl_bpv);
				$results = array('success' => false, 'message' => 'Gagal Jurnal Coa Value ke database');
			}
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}

	public function save_edit_jurnal_np() {
		$user_id 		= $this->session->userdata['logged_in']['user_id'];
		$id_jurnal 		= ($this->input->post('id_jurnal', true));
		$no_voucher 	= ($this->input->post('no_voucher', true));
		$tgl_bpvk 		= ($this->input->post('tgl_bpvk', true));

		//Start Variable Item Masuk
		$arrIdMasuk			= $this->input->post('arrIdMasuk', TRUE);
		$arrCoaMasuk		= $this->input->post('arrCoaMasuk', TRUE);
		$arrNoteMasuk		= $this->input->post('arrNoteMasuk', TRUE);
		$arrValasMasuk		= $this->input->post('arrValasMasuk', TRUE);
		$arrRateMasuk		= $this->input->post('arrRateMasuk', TRUE);
		$arrValueKasMasuk	= $this->input->post('arrValueKasMasuk', TRUE);
		//End Variable Item Masuk

		//Start Variable Item Keluar
		$arrIdKeluar		= $this->input->post('arrIdKeluar', TRUE);
		$arrCoaKeluar		= $this->input->post('arrCoaKeluar', TRUE);
		$arrNoteKeluar		= $this->input->post('arrNoteKeluar', TRUE);
		$arrValasKeluar		= $this->input->post('arrValasKeluar', TRUE);
		$arrRateKeluar		= $this->input->post('arrRateKeluar', TRUE);
		$arrValueKasKeluar	= $this->input->post('arrValueKasKeluar', TRUE);
		//End Variable Item Keluar

		$data_jurnal = array(
			'id_jurnal'		=> $id_jurnal,
			'no_voucher'	=> $no_voucher,
			'date_input'	=> date('Y-m-d H:i:s', strtotime($tgl_bpvk.' '.date('H:i:s'))),
			'pic'			=> $user_id
		);
		
		$result_jurnal_np = $this->jurnal_np_model->edit_jurnal_np($data_jurnal);
		if( (sizeof($arrIdMasuk) == sizeof($arrIdKeluar)) && sizeof($arrIdMasuk) > 0 && sizeof($arrIdKeluar) > 0 ) {
			//CRUD ITEM MASUK
			for ($i=0; $i < sizeof($arrIdMasuk); $i++) {
				if($arrIdMasuk[$i] == 'new') {
					if($arrNoteMasuk[$i] != '' && $arrNoteMasuk[$i] != 'null') $strNote = $arrNoteMasuk[$i];
					else $strNote = NULL;

					$data_coa_masuk = array(
						'id_coa' 		=> $arrCoaMasuk[$i],
						'id_parent' 	=> $arrCoaMasuk[$i],
						'date' 			=> date('Y-m-d', strtotime($tgl_bpvk)),
						'id_valas' 		=> $arrValasMasuk[$i],
						'value' 		=> $arrValueKasMasuk[$i],
						'adjusment' 	=> 0,
						'type_cash' 	=> 0,
						'note' 			=> $strNote,
						'rate' 			=> floatval($arrRateMasuk[$i]),
						'bukti' 		=> NULL,
						'pic' 			=> $user_id,
						'pic_approve' 	=> NULL,
						'status' 		=> 0
					);

					$result_add_coa_masuk = $this->jurnal_np_model->add_coa_value_jurnal_temporary($data_coa_masuk);
					$this->log_activity->insert_activity('insert', 'Insert Jurnal Coa Value ID Coa Value : ' . $data_coa_masuk['id_coa']);
					
					$data_jurnal_coa_value = array(
						'id_jurnal_np'	=> $id_jurnal,
						'id_coa_value'	=> $result_add_coa_masuk['lastid'],
					);
					
					$result_jurnal_coa_value = $this->jurnal_np_model->add_jurnal_coa_value_temporary($data_jurnal_coa_value);
					$this->log_activity->insert_activity('insert', 'Insert Jurnal Coa Value ID Jurnal : ' . $data_jurnal_coa_value['id_jurnal_np']);
				}else {
					if($arrNoteMasuk[$i] != '' && $arrNoteMasuk[$i] != 'null') $strNote = $arrNoteMasuk[$i];
					else $strNote = NULL;

					$data_coa_masuk = array(
						'id'			=> $arrIdMasuk[$i],
						'id_coa'		=> $arrCoaMasuk[$i],
						'id_parent'		=> $arrCoaMasuk[$i],
						'date'			=> date('Y-m-d', strtotime($tgl_bpvk)),
						'id_valas'		=> $arrValasMasuk[$i],
						'value'			=> $arrValueKasMasuk[$i],
						'value_real'	=> $arrValueKasMasuk[$i],
						'type_cash'		=> 0,
						'note'			=> $strNote,
						'rate'			=> $arrRateMasuk[$i],
						'bukti'			=> NULL
					);
					$result_update_coa_masuk = $this->jurnal_np_model->edit_coa_value($data_coa_masuk);
					$this->log_activity->insert_activity('update', 'Update Jurnal Coa Value ID Jurnal : ' . $id_jurnal);
				}
			}

			//CRUD ITEM KELUAR
			for ($i=0; $i < sizeof($arrIdKeluar); $i++) {
				if($arrIdKeluar[$i] == 'new') {
					if($arrNoteKeluar[$i] != '' && $arrNoteKeluar[$i] != 'null') $strNote = $arrNoteKeluar[$i];
					else $strNote = NULL;

					$data_coa_keluar = array(
						'id_coa' 		=> $arrCoaKeluar[$i],
						'id_parent' 	=> $arrCoaKeluar[$i],
						'date' 			=> date('Y-m-d', strtotime($tgl_bpvk)),
						'id_valas' 		=> $arrValasKeluar[$i],
						'value' 		=> $arrValueKasKeluar[$i],
						'adjusment' 	=> 0,
						'type_cash' 	=> 1,
						'note' 			=> $strNote,
						'rate' 			=> floatval($arrRateKeluar[$i]),
						'bukti' 		=> NULL,
						'pic' 			=> $user_id,
						'pic_approve' 	=> NULL,
						'status' 		=> 0
					);

					$result_add_coa_keluar = $this->jurnal_np_model->add_coa_value_jurnal_temporary($data_coa_keluar);
					$this->log_activity->insert_activity('insert', 'Insert Jurnal Coa Value ID Coa Value : ' . $data_coa_keluar['id_coa']);
					
					$data_jurnal_coa_value = array(
						'id_jurnal_np'	=> $id_jurnal,
						'id_coa_value'	=> $result_add_coa_keluar['lastid'],
					);
					
					$result_jurnal_coa_value = $this->jurnal_np_model->add_jurnal_coa_value_temporary($data_jurnal_coa_value);
					$this->log_activity->insert_activity('insert', 'Insert Jurnal Coa Value ID Jurnal : ' . $data_jurnal_coa_value['id_jurnal_np']);
				}else {
					if($arrNoteKeluar[$i] != '' && $arrNoteKeluar[$i] != 'null') $strNote = $arrNoteKeluar[$i];
					else $strNote = NULL;
					$data_coa_keluar = array(
						'id'			=> $arrIdKeluar[$i],
						'id_coa'		=> $arrCoaKeluar[$i],
						'id_parent'		=> $arrCoaKeluar[$i],
						'date'			=> date('Y-m-d', strtotime($tgl_bpvk)),
						'id_valas'		=> $arrValasKeluar[$i],
						'value'			=> $arrValueKasKeluar[$i],
						'value_real'	=> $arrValueKasKeluar[$i],
						'type_cash'		=> 1,
						'note'			=> $strNote,
						'rate'			=> $arrRateKeluar[$i],
						'bukti'			=> NULL
					);
					$result_update_coa_keluar = $this->jurnal_np_model->edit_coa_value($data_coa_keluar);

					$this->log_activity->insert_activity('update', 'Update Jurnal Coa Value ID Jurnal : ' . $id_jurnal);
				}
			}
		}
		$results = array('success' => true, 'message' => 'Berhasil update data Jurnal Umum ke database', 'id_jurnal' => $id_jurnal);

		/*$check = 0;
		if($result_select_jurnal_np[0]['date_input'] != $tgl_bpvk || $result_select_jurnal_np[0]['pic'] != $user_id){
			$check = 1;
		}elseif($result_select_jurnal_np[0]['date_input'] == $tgl_bpvk || $result_select_jurnal_np[0]['pic'] == $user_id){
			$check = 1;
		}else{
			$check = 0;
		}
		
		$result_jurnal_np = $this->jurnal_np_model->edit_jurnal_np($data_jurnal);
		if($check == 1) {
			$delete_detail_jurnal = $this->jurnal_np_model->delete_detail_jurnal_np($id_jurnal);
			
			$this->log_activity->insert_activity('insert', 'Berhasil update data Jurnal Non Produksi tanggal ' . $tgl_bpvk);
			$results = array('success' => true, 'message' => 'Berhasil update data Jurnal Non Produksi ke database','id_jurnal' => $id_jurnal);
		}else{
			$this->log_activity->insert_activity('insert', 'Gagal update data Jurnal Non Produksi tanggal ' . $tgl_bpvk);
			$results = array('success' => false, 'message' => 'Gagal update data Jurnal Non Produksi ke database','id_jurnal' => $id_jurnal);
		}*/
		
		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}
	
	public function save_edit_jurnal_coa_temporary() {
		$user_id 	= $this->session->userdata['logged_in']['user_id'];
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		
		$arrData = array();
		//$statSave = false;
		
		foreach ($params['listcoamasuk'] as $k => $v) {
			$arrTempMasuk = array(
				'id_jurnal'		=> $params['id_jurnal'],
				'id_coa_value'	=> $v[0],
				'id_coa' 		=> $v[8],
				'id_parent' 	=> $v[2],
				'date' 			=> $v[4],
				'id_valas' 		=> $v[13],
				'value' 		=> $v[10],
				'adjusment' 	=> 0,
				'type_cash' 	=> 0,
				'note' 			=> $v[3],
				'rate' 			=> $v[6],
				'bukti' 		=> NULL,
				'pic' 			=> $user_id,
				'pic_approve' 	=> NULL,
				'status' 		=> 0
			);
			
			$result_coa_masuk = $this->jurnal_np_model->add_coa_value_jurnal_temporary($arrTempMasuk);
			$data_coa_masuk = array(
				'id_jurnal_np'	=> $params['id_jurnal'],
				'id_coa_value' 	=> $result_coa_masuk['lastid']
			);
			
			$result_detail_jurnal = $this->jurnal_np_model->add_jurnal_coa_value_temporary($data_coa_masuk);
		}
		
		foreach ($params['listcoakeluar'] as $k => $v) {
			$arrTempKeluar = array(
				'id_jurnal'		=> $params['id_jurnal'],
				'id_coa_value'	=> $v[0],
				'id_coa' 		=> $v[8],
				'id_parent' 	=> $v[2],
				'date' 			=> $v[4],
				'id_valas' 		=> $v[13],
				'value' 		=> $v[10],
				'adjusment' 	=> 0,
				'type_cash' 	=> 1,
				'note' 			=> $v[3],
				'rate' 			=> $v[6],
				'bukti' 		=> NULL,
				'pic' 			=> $user_id,
				'pic_approve' 	=> NULL,
				'status' 		=> 0
			);
			
			$result_coa_keluar = $this->jurnal_np_model->add_coa_value_jurnal_temporary($arrTempKeluar);
			$data_coa_keluar = array(
				'id_jurnal_np'	=> $params['id_jurnal'],
				'id_coa_value' 	=> $result_coa_keluar['lastid']
			);
			
			$result_detail_jurnal = $this->jurnal_np_model->add_jurnal_coa_value_temporary($data_coa_keluar);
		}
		
		$statSave = true;
		
		if ($statSave == true) {
			$this->log_activity->insert_activity('insert', 'Berhasil Update data Coa Value : '.$arrTempMasuk['note']);
			$result = array('success' => true, 'message' => 'Berhasil update data Coa Value ke database');
		}else {
			$this->log_activity->insert_activity('insert', 'Gagal Update data Coa Value');
			$result = array('success' => false, 'message' => 'Gagal update data coa value ke database');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result);
	}
	
	public function detail_jurnal($id)
	{
		$detail_jurnal = $this->jurnal_np_model->detail_jurnal_non_prod($id);

		$data = array(
			'detail_jurnal' => $detail_jurnal,
		);

		$this->load->view('detail_jurnal_view', $data);
	}
	
	function edit_detail_lists_jurnal_np_coa($id, $type_cash) {
		$list 		= $this->jurnal_np_model->detail_jurnal_non_prod($id);
		$coa_all 	= $this->jurnal_np_model->coa_list();
		$valas 		= $this->jurnal_model->valas();
		
		$data = array();
		$x = 0;
		
		$username = $this->session->userdata['logged_in']['username'];
		
		$data_coa = array(
			'id_jurnal' => $id,
			'type_cash'	=> $type_cash
		);
		
		if($type_cash == 0) {
			$list_coa_masuk = $this->jurnal_np_model->detail_list_jurnal_np_coa_temporary($data_coa);

			$no = 0;
			foreach ($list_coa_masuk as $m => $a) {
				$no++;
				if($type_cash == 0) $status = 'Debit';
				$strBtn =
					'<a class="btn btn-icon waves-effect waves-light btn-danger">'.
						'<i class="fa fa-trash"></i>'.
					'</a>';

				$strPerkiraan =
					'<select onchange="changeCoaMasuk('.$no.')" class="form-control ddl_coa_masuk" id="ddl_coa_masuk'.$no.'" name="ddl_coa_masuk[]"'.
						'style="width: 100%" required>';
				foreach ($coa_all as $key) {
					if($key['id_coa'] == $a['id_coa']) $strSelected = 'selected';
					else $strSelected = '';
					$strPerkiraan .= "<option value=\"".$key['id_coa']."\" ".$strSelected.">".$key['coa']."-".$key['keterangan']."</option>";
				}
				$strPerkiraan .=
					'</select>';

				$strCoa =
					'<input type="text" class="form-control" id="coa_number_masuk'.$no.'" name="coa_number_masuk[]" value="'.$a['coa'].'"'.
						'placeholder="Coa Number Masuk" autocomplete="off" readonly>';

				$strKeterangan =
					'<input type="text" class="form-control" id="note_masuk'.$no.'" name="note_masuk[]" value="'.$a['note'].'"'.
					'placeholder="Note" autocomplete="off">';

				$strValas =
					'<select  class="form-control ddl_valas_masuk" id="ddl_valas_masuk'.$no.'" name="ddl_valas_masuk[]" style="width: 100%" required>';
				foreach ($valas as $key) {
					if($key['valas_id'] == $a['id_valas']) $strSelected = 'selected';
					else $strSelected = '';
					$strValas .= '<option value="'.$key['valas_id'].'">'.$key['nama_valas'].'</option>';
				}
				$strValas .=
					'</select>';

				$strNilai =
					'<input type="number" onKeyup="calculateValueMasuk()" class="form-control" id="value_kas_masuk'.$no.'" name="value_kas_masuk[]"'.
						'value="'.number_format($a['value'], 0, '.', '').'" placeholder="Nominal" autocomplete="off">';

				array_push($data, array(
					$a['id_coa_temp'],
					'',
					$strPerkiraan,
					$strCoa,
					$strKeterangan,
					$strValas,
					$a['rate'],
					$strNilai,
					$status,
					$strBtn
				));
			}
		}else {
			$list_coa_keluar = $this->jurnal_np_model->detail_list_jurnal_np_coa_temporary($data_coa);
			
			$no = 0;
			foreach ($list_coa_keluar as $n => $o) {
				$no++;
				if($type_cash == 1) $status = 'Kredit';
				$strBtn =
					'<a class="btn btn-icon waves-effect waves-light btn-danger">'.
						'<i class="fa fa-trash"></i>'.
					'</a>';

				$strPerkiraan =
					'<select onchange="changeCoaKeluar('.$no.')" class="form-control ddl_coa_keluar" id="ddl_coa_keluar'.$no.'" name="ddl_coa_keluar[]"'.
						'style="width: 100%" required>';
				foreach ($coa_all as $key) {
					if($key['id_coa'] == $o['id_coa']) $strSelected = 'selected';
					else $strSelected = '';
					$strPerkiraan .= "<option value=\"".$key['id_coa']."\" ".$strSelected.">".$key['coa']."-".$key['keterangan']."</option>";
				}
				$strPerkiraan .=
					'</select>';

				$strCoa =
					'<input type="text" class="form-control" id="coa_number_keluar'.$no.'" name="coa_number_keluar[]" value="'.$o['coa'].'"'.
						'placeholder="Coa Number Masuk" autocomplete="off" readonly>';

				$strKeterangan =
					'<input type="text" class="form-control" id="note_keluar'.$no.'" name="note_keluar[]" value="'.$o['note'].'"'.
					'placeholder="Note" autocomplete="off">';

				$strValas =
					'<select  class="form-control ddl_valas_keluar" id="ddl_valas_keluar'.$no.'" name="ddl_valas_keluar[]" style="width: 100%" required>';
				foreach ($valas as $key) {
					if($key['valas_id'] == $o['id_valas']) $strSelected = 'selected';
					else $strSelected = '';
					$strValas .= '<option value="'.$key['valas_id'].'">'.$key['nama_valas'].'</option>';
				}
				$strValas .=
					'</select>';

				$strNilai =
					'<input type="number" onKeyup="calculateValueKeluar()" class="form-control" id="value_kas_keluar'.$no.'" name="value_kas_keluar[]"'.
						'value="'.number_format($o['value'], 0, '.', '').'" placeholder="Nominal" autocomplete="off">';
					
				array_push($data, array(
					$o['id_coa_temp'],
					'',
					$strPerkiraan,
					$strCoa,
					$strKeterangan,
					$strValas,
					$o['rate'],
					$strNilai,
					$status,
					$strBtn
				));
			}
		}
		
		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	function detail_lists_jurnal_np_coa($id,$type_cash)
	{
		$list = $this->jurnal_np_model->detail_jurnal_non_prod($id);
		
		$data = array();
		$x = 0;
		
		$username = $this->session->userdata['logged_in']['username'];
		
		$data_coa = array(
			'id_jurnal' => $id,
			'type_cash'	=> $type_cash
		);
		
		$no = 0;
		if($type_cash == 0) {
			$list_coa_masuk = $this->jurnal_np_model->detail_list_jurnal_np_coa_temporary($data_coa);
			
			foreach ($list_coa_masuk as $m => $a) {
				if($type_cash == 0) { $status = 'Pemasukan';}
				array_push($data, array(
					$no = $m+1,
					$a['coa'],
					$a['id_parent'],
					$a['symbol_valas'] . ' ' . number_format(round($a['value'])),
					$status,
					$a['keterangan'],
					$a['symbol_valas'] . ' ' . number_format(round($a['value_real'])),
					$a['rate'],
					$a['bukti'],
				));
			}
		}else{
			$list_coa_keluar = $this->jurnal_np_model->detail_list_jurnal_np_coa_temporary($data_coa);
			
			foreach ($list_coa_keluar as $n => $o) {
				if($type_cash == 1) { $status = 'Pengeluaran';}
				array_push($data, array(
					$no = $n+1,
					$o['coa'],
					$o['id_parent'],
					$o['symbol_valas'] . ' ' . number_format(round($o['value'])),
					$status,
					$o['keterangan'],
					$o['symbol_valas'] . ' ' . number_format(round($o['value_real'])),
					$o['rate'],
					$o['bukti']
				));
			}
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function delete_jurnal_temporary()
	{
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data, true);
		
		$delete_jurnal = $this->jurnal_np_model->delete_jurnal_temporary($params['id']);
		
		if($delete_jurnal > 0){
			$this->log_activity->insert_activity('insert', 'Berhasil hapus Data Jurnal Non Produksi ID : ' . $params['id']);
			$res = array('status' => 'success', 'message' => 'Data jurnal berhasil di hapus');
		}else{
			$this->log_activity->insert_activity('insert', 'Gagal hapus data Jurnal Non Produksi ID : ' . $params['id']);
			$res = array('status' => 'failed', 'message' => 'Data jurnal gagal di hapus');
		}
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	public function delete_jurnal_coa_temporary()
	{
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data, true);
		
		if($params['id'] != '' || $params['id'] != NULL){
			$this->jurnal_np_model->delete_jurnal_coa_temporary($params['id']);
			
			$this->log_activity->insert_activity('insert', 'Berhasil hapus Data Jurnal Non Produksi ID : ' . $params['id']);
			$res = array('status' => 'success', 'message' => 'Data jurnal berhasil di hapus');
		}else{
			$this->log_activity->insert_activity('insert', 'Gagal hapus data Jurnal Non Produksi ID : ' . $params['id']);
			$res = array('status' => 'failed', 'message' => 'Data jurnal gagal di hapus');
		}
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	public function check_no_voucher(){
		$this->form_validation->set_rules('no_voucher', 'NoVoucher', 'trim|required|min_length[4]|max_length[20]|is_unique[t_jurnal_non_prod.no_voucher]');
		$this->form_validation->set_message('is_unique', 'No Voucher Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'No Voucher Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}
}
