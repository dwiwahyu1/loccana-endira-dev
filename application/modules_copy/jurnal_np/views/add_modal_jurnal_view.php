<style>
	#no_voucher_loading-us {
		display: none
	}

	#no_voucher_tick {
		display: none
	}
</style>
<form class="form-horizontal form-label-left" id="add_form" role="form" action="<?php echo base_url('jurnal_np/save_jurnal_np'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_voucher">No Voucher <span class="required"><sup>*</sup></span></label>
		<div class="input-group col-md-8 col-sm-6 col-xs-12 ">
			<input placeholder="No Voucher" type="text" class="form-control" id="no_voucher" name="no_voucher" required="required" value="">
			<span id="no_voucher_tick"></span>
			<span id="no_voucher_loading-us"></span>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tgl_bpvk">Tanggal <span class="required"><sup>*</sup></span></label>

		<div class="input-group date col-md-8 col-sm-6 col-xs-12 ">
			<input placeholder="Tanggal" type="text" class="form-control datepicker" id="tgl_bpvk" name="tgl_bpvk" required="required" value="<?php echo date('Y-m-d'); ?>">
			<div class="input-group-addon">
				<span class="glyphicon glyphicon-th"></span>
			</div>
		</div>
	</div>

	<div class="item form-group" style="display: none;">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="durasi">Durasi </label>
		<div class="input-group col-md-8 col-sm-6 col-xs-12 ">
			<select class="form-control" id="durasi" name="durasi">
				<option value="0" selected>Tanpa Durasi</option>
				<option value="1">1 Day</option>
				<option value="15">15 Days</option>
				<option value="30">30 Days</option>
				<option value="45">45 Days</option>
				<option value="60">60 Days</option>
				<option value="90">90 Days</option>
				<option value="120">120 Days</option>
				<option value="180">180 Days</option>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Debit: </label>
		<div class="col-md-8 col-sm-6 col-xs-12 add_item" onclick="add_coa_value_masuk()">
			<a type="button" class="btn btn-default btn-icon waves-effect waves-light m-b-5">
				<i class="fa fa-plus"></i>
			</a>
			Tambah Debit
		</div>
	</div>

	<div class="item form-group">
		<div class="col-md-8 col-md-offset-3">
			<table id="list_barang_masuk" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Perkiraan</th>
						<th>COA</th>
						<th>Keterangan</th>
						<!--<th>Tanggal</th>-->
						<th>Valas</th>
						<th>Nilai</th>
						<th>Tipe</th>
						<th class="text-center" style="width: 5%">Option</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jumlah">Jumlah Debit</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="jumlah_masuk" name="jumlah_masuk" class="form-control" placeholder="Jumlah Masuk" autocomplete="off" style="text-align:right" readonly required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Kredit: </label>
		<div class="col-md-8 col-sm-6 col-xs-12 add_item" onclick="add_coa_value_keluar()">
			<a type="button" class="btn btn-default btn-icon waves-effect waves-light m-b-5">
				<i class="fa fa-plus"></i>
			</a>
			Tambah Kredit
		</div>
	</div>

	<div class="item form-group">
		<div class="col-md-8 col-md-offset-3">
			<table id="list_barang_keluar" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Perkiraan</th>
						<th>COA</th>
						<th>Keterangan</th>
						<!--<th>Tanggal</th>-->
						<th>Valas</th>
						<th>Nilai</th>
						<th>Tipe</th>
						<th class="text-center" style="width: 5%">Option</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jumlah">Jumlah Kredit</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="jumlah_keluar" name="jumlah_keluar" class="form-control" placeholder="Jumlah Keluar" autocomplete="off" style="text-align:right" readonly required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Jurnal Umum</button>
		</div>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	var formData = new FormData();
	var itemsMasuk = [];
	var itemsKeluar = [];
	var idRowMasuk = 1;
	var idRowKeluar = 1;
	$(document).ready(function() {
		$(".date").datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});
	});

	function changeCoaMasuk(id) {
		$("#coa_number_masuk" + id).val($("#ddl_coa_masuk" + id).find(":selected").text().split("-")[0]);
	}

	function changeCoaKeluar(id) {
		$("#coa_number_keluar" + id).val($("#ddl_coa_keluar" + id).find(":selected").text().split("-")[0]);
	}

	function calTotalMasuk() {
		total = 0;
		$('input[name="value_kas_masuk[]"]').each(function() {
			if (this.value) {
				if (this.value != undefined && this.value != '' && this.value > 0) {
					total = total + parseFloat(this.value)
				}
			}
		})
		$("#jumlah_masuk").val(total);
	}

	function calTotalKeluar() {
		total = 0;
		$('input[name="value_kas_keluar[]"]').each(function() {
			if (this.value) {
				if (this.value != undefined && this.value != '' && this.value > 0) {
					total = total + parseFloat(this.value)
				}
			}
		})
		$("#jumlah_keluar").val(total);
	}
	var last_no_voucher = $('#no_voucher').val();
	$('#no_voucher').on('input', function(event) {
		if ($('#no_voucher').val().toUpperCase() != last_no_voucher) no_voucher_check();
		else {
			$('#no_voucher').removeAttr("style");
			$('#no_voucher_tick').empty();
			$('#no_voucher_tick').hide();
			$('#no_voucher_loading-us').hide();
		}
	});

	function no_voucher_check() {
		var no_voucher = $('#no_voucher').val();
		if (no_voucher.length > 3) {
			var post_data = {
				'no_voucher': no_voucher
			};

			$('#no_voucher_tick').empty();
			$('#nono_voucher_tick').hide();
			$('#no_voucher_loading-us').show();
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url('jurnal_np/check_no_voucher'); ?>",
				data: post_data,
				cache: false,
				success: function(response) {
					if (response.success == true) {
						$('#no_voucher').css('border', '3px #090 solid');
						$('#no_voucher_loading-us').hide();
						$('#no_voucher_tick').empty();
						$("#no_voucher_tick").append('<span class="fa fa-check"> ' + response.message + '</span>');
						$('#no_voucher_tick').show();
					} else {
						$('#no_voucher').css('border', '3px #C33 solid');
						$('#no_voucher_loading-us').hide();
						$('#no_voucher_tick').empty();
						$("#no_voucher_tick").append('<span class="fa fa-close"> ' + response.message + '</span>');
						$('#no_voucher_tick').show();
					}
				}
			});
		} else {
			$('#no_voucher').css('border', '3px #C33 solid');
			$('#no_voucher_loading-us').hide();
			$('#no_voucher_tick').empty();
			$("#no_voucher_tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
			$('#no_voucher_tick').show();
		}
	}

	function draw_kas_masuk_table() {
		var itemslen_masuk = itemsMasuk.length;
		var element_masuk = '';
		var totalValue_masuk = 0;
		for (var i = 0; i < itemslen_masuk; i++) {
			selectedText = '';
			selectCoaText =
				'<select onchange="changeCoaMasuk(' + i + ')" class="form-control ddl_coa_masuk" id="ddl_coa_masuk' + i + '" name="ddl_coa_masuk[]" style="width: 100%" required >' +
				<?php foreach ($coa_all as $key) { ?> '<option value="' +
					'<?php echo $key['id_coa']; ?>';
			if (itemsMasuk[i].id_coa_masuk == <?php if (isset($key['id_coa'])) echo $key['id_coa'];
																				else echo 'null'; ?>)
				selectedText = 'selected="selected"';
			else selectedText = '';
			selectCoaText = selectCoaText + '"' + selectedText +
				'>' + "<?php echo $key['coa'] . "-" . $key['keterangan']; ?>" + '</option>' +
			<?php } ?> '</select>';
			selectedText = '';
			selectValasText =
				'<select  class="form-control ddl_valas_masuk" id="ddl_valas_masuk' + i + '" name="ddl_valas_masuk[]" style="width: 100%" required >' +
				<?php foreach ($valas as $key) { ?> '<option value="' +
					'<?php echo $key['valas_id']; ?>';
			if (itemsMasuk[i].id_valas_masuk == <?php if (isset($key['valas_id'])) echo $key['valas_id'];
																					else echo 'null'; ?>)
				selectedText = 'selected="selected"';
			else selectedText = '';
			selectValasText = selectValasText + '"' + selectedText +
				'>' + "<?php echo $key['nama_valas']; ?>" + '</option>' +
			<?php } ?> '</select>';
			element_masuk += '   <tr>' +
				'<tr id="trRowCoaMasuk' + i + '">' +
				'<td style="display:none;">' + itemsMasuk[i]['id_coa_masuk'] + '</td>' +
				'<td>' +
				selectCoaText +
				// itemsMasuk[i]["text_coa_masuk"] +
				'</td>' +
				'<td>' +
				'<input type="text" class="form-control" id="coa_number_masuk' + i + '" name="coa_number_masuk[]" value="' + itemsMasuk[i]["coa_number_masuk"] + '" placeholder="Coa Number Masuk" autocomplete="off" readonly>' +
				// itemsMasuk[i]["coa_number_masuk"] + '</td>' +
				'<td>' +
				'<input type="text" class="form-control" id="note_masuk' + i + '" name="note_masuk[]" value="' + itemsMasuk[i]["note_masuk"] + '" placeholder="Note" autocomplete="off">' +
				// itemsMasuk[i]["note_masuk"] + 
				'</td>' +
				'<td>' +
				selectValasText +
				'</td>' +
				'<td>' +
				'<input type="number" onchange="calTotalMasuk()" class="form-control" id="value_kas' + i + '" name="value_kas_masuk[]" value="' + itemsMasuk[i]["value_masuk"] + '" placeholder="Nominal" autocomplete="off">' +
				'</td>' +
				'<td>' + itemsMasuk[i]["type_cash_text_masuk"] + '</td>' +
				'<td style="display:none;">' + itemsMasuk[i]["file_kas_masuk"] + '</td>' +
				'<td>' +
				'  <div class="btn-group">' +
				'    <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="delete_kas_masuk_item(' + i + ')">' +
				'    <i class="fa fa-trash"></i>' +
				'  </div>' +
				'</td>' +
				'</tr>';
			totalValue_masuk = totalValue_masuk + parseInt(itemsMasuk[i]["value_masuk"]);
			$('#list_barang_masuk tbody').html(element_masuk);
			$('#ddl_coa_masuk'+i).select2();
			$('#ddl_valas_masuk'+i).select2();
		}
		$('#jumlah_masuk').val(totalValue_masuk);
	}

	function draw_kas_keluar_table() {
		var itemslen_keluar = itemsKeluar.length;
		var element_keluar = '';
		var totalValue_keluar = 0;
		for (var i = 0; i < itemslen_keluar; i++) {
			selectedText = '';
			selectCoaText =
				'<select onchange="changeCoaKeluar(' + i + ')" class="form-control ddl_coa_keluar" id="ddl_coa_keluar' + i + '" name="ddl_coa_keluar[]" style="width: 100%" required >' +
				<?php foreach ($coa_all as $key) { ?> '<option value="' +
					'<?php echo $key['id_coa']; ?>';
			if (itemsKeluar[i].id_coa_keluar == <?php if (isset($key['id_coa'])) echo $key['id_coa'];
																					else echo 'null'; ?>)
				selectedText = 'selected="selected"';
			else selectedText = '';
			selectCoaText = selectCoaText + '"' + selectedText +
				'>' + "<?php echo $key['coa'] . "-" . $key['keterangan']; ?>" + '</option>' +
			<?php } ?> '</select>';
			selectedText = '';
			selectValasText =
				'<select  class="form-control ddl_valas_keluar" id="ddl_valas_keluar' + i + '" name="ddl_valas_keluar[]" style="width: 100%" required >' +
				<?php foreach ($valas as $key) { ?> '<option value="' +
					'<?php echo $key['valas_id']; ?>';
			if (itemsKeluar[i].id_valas_masuk == <?php if (isset($key['valas_id'])) echo $key['valas_id'];
																						else echo 'null'; ?>)
				selectedText = 'selected="selected"';
			else selectedText = '';
			selectValasText = selectValasText + '"' + selectedText +
				'>' + "<?php echo $key['nama_valas']; ?>" + '</option>' +
			<?php } ?> '</select>';
			element_keluar += '   <tr>' +
				'<tr id="trRowCoaMasuk' + i + '">' +
				'<td style="display:none;">' + itemsKeluar[i]['id_coa_keluar'] + '</td>' +
				'<td>' +
				selectCoaText +
				// itemsMasuk[i]["text_coa_masuk"] +
				'</td>' +
				'<td>' +
				'<input type="text" class="form-control" id="coa_number_keluar' + i + '" name="coa_number_keluar[]" value="' + itemsKeluar[i]["coa_number_keluar"] + '" placeholder="Coa Number Keluar" autocomplete="off" readonly>' +
				// itemsMasuk[i]["coa_number_masuk"] + '</td>' +
				'<td>' +
				'<input type="text" class="form-control" id="note_keluar' + i + '" name="note_keluar[]" value="' + itemsKeluar[i]["note_keluar"] + '" placeholder="Note" autocomplete="off">' +
				// itemsMasuk[i]["note_masuk"] + 
				'</td>' +
				'<td>' +
				selectValasText +
				'</td>' +
				'<td>' +
				'<input type="number" onchange="calTotalKeluar()" class="form-control" id="value_kas_keluar' + i + '" name="value_kas_keluar[]" value="' + itemsKeluar[i]["value_keluar"] + '" placeholder="Nominal" autocomplete="off">' +
				'</td>' +
				'<td>' + itemsKeluar[i]["type_cash_text_keluar"] + '</td>' +
				'<td style="display:none;">' + itemsKeluar[i]["file_kas_keluar"] + '</td>' +
				'<td>' +
				'  <div class="btn-group">' +
				'    <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="delete_kas_keluar_item(' + i + ')">' +
				'    <i class="fa fa-trash"></i>' +
				'  </div>' +
				'</td>' +
				'</tr>';
			totalValue_keluar = totalValue_keluar + parseInt(itemsKeluar[i]["value_keluar"]);
			$('#list_barang_keluar tbody').html(element_keluar);
			$('#ddl_coa_keluar' + i).select2();
			$('#ddl_valas_keluar' + i).select2();
		}
		$('#jumlah_keluar').val(totalValue_keluar);
	}

	function add_kas_masuk_temp_data(itemsKasMasuk) {
		itemsMasuk.push(itemsKasMasuk);
		draw_kas_masuk_table();
	}

	function add_kas_keluar_temp_data(itemsKasKeluar) {
		itemsKeluar.push(itemsKasKeluar);
		draw_kas_keluar_table();
	}

	function removeRowCoaMasuk(rowCoaMasuk) {
		itemsMasuk.splice(rowCoaMasuk, 1);
		$('#trRowCoaMasuk' + rowCoaMasuk).remove();
	}

	function removeRowCoaKeluar(rowCoaKeluar) {
		itemsKeluar.splice(rowCoaKeluar, 1);
		$('#trRowCoaKeluar' + rowCoaKeluar).remove();
	}

	function delete_kas_masuk_item(rowCoaMasuk) {
		itemsMasuk.splice(rowCoaMasuk, 1);

		$('#list_barang_masuk tbody').html('');
		formData.delete('id_coa_masuk[]', '');
		formData.delete('text_coa_masuk[]', '');
		formData.delete('coa_number_masuk[]', '');
		formData.delete('id_parent_masuk[]', '');
		formData.delete('date_masuk[]', '');
		formData.delete('id_valas_masuk[]', '');
		formData.delete('value_masuk[]', '');
		formData.delete('rate_masuk[]', '');
		formData.delete('type_cash_masuk[]', '');
		formData.delete('type_cash_text_masuk[]', '');
		formData.delete('note_masuk[]', '');
		formData.delete('have_file_masuk[]', '');

		for (i = 0; i < itemsMasuk.length; i++) {
			formData.append('id_coa_masuk[]', itemsMasuk[i].id_coa_masuk);
			formData.append('text_coa_masuk[]', itemsMasuk[i].text_coa_masuk);
			formData.append('coa_number_masuk[]', itemsMasuk[i].coa_number_masuk);
			formData.append('id_parent_masuk[]', itemsMasuk[i].id_parent_masuk);
			formData.append('date_masuk[]', itemsMasuk[i].date_masuk);
			formData.append('id_valas_masuk[]', itemsMasuk[i].id_valas_masuk);
			formData.append('value_masuk[]', itemsMasuk[i].value_masuk);
			formData.append('rate_masuk[]', itemsMasuk[i].rate_masuk);
			formData.append('type_cash_masuk[]', itemsMasuk[i].type_cash_masuk);
			formData.append('type_cash_text_masuk[]', itemsMasuk[i].type_cash_text_masuk);
			formData.append('note_masuk[]', itemsMasuk[i].note_masuk);
			//formData.append('file_kas_masuk[]', itemsMasuk[i].file_kas_masuk, itemsMasuk[i].file_name);
		}

		draw_kas_masuk_table();
	}

	function delete_kas_keluar_item(rowCoaKeluar) {
		itemsKeluar.splice(rowCoaKeluar, 1);

		$('#list_barang_keluar tbody').html('');
		formData.delete('id_coa_keluar[]', '');
		formData.delete('text_coa_keluar[]', '');
		formData.delete('coa_number_keluar[]', '');
		formData.delete('id_parent_keluar[]', '');
		formData.delete('date_keluar[]', '');
		formData.delete('id_valas_keluar[]', '');
		formData.delete('value_keluar[]', '');
		formData.delete('rate_keluar[]', '');
		formData.delete('type_cash_keluar[]', '');
		formData.delete('type_cash_text_keluar[]', '');
		formData.delete('note_keluar[]', '');
		formData.delete('have_file_keluar[]', '');

		for (i = 0; i < itemsKeluar.length; i++) {
			formData.append('id_coa_keluar[]', itemsKeluar[i].id_coa_keluar);
			formData.append('text_coa_keluar[]', itemsKeluar[i].text_coa_keluar);
			formData.append('coa_number_keluar[]', itemsKeluar[i].coa_number_keluar);
			formData.append('id_parent_keluar[]', itemsKeluar[i].id_parent_keluar);
			formData.append('date_keluar[]', itemsKeluar[i].date_keluar);
			formData.append('id_valas_keluar[]', itemsKeluar[i].id_valas_keluar);
			formData.append('value_keluar[]', itemsKeluar[i].value_keluar);
			formData.append('rate_keluar[]', itemsKeluar[i].rate_keluar);
			formData.append('type_cash_keluar[]', itemsKeluar[i].type_cash_keluar);
			formData.append('type_cash_text_keluar[]', itemsKeluar[i].type_cash_text_keluar);
			formData.append('note_keluar[]', itemsKeluar[i].note_keluar);
			//formData.append('file_kas_keluar[]', itemsKeluar[i].file_kas_keluar, itemsKeluar[i].file_name);
		}

		draw_kas_keluar_table();
	}

	$('#add_form').on('submit', (function(e) {
		$('#btn-submit').attr('disabled', 'disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		arrValue = [];
		if ($('#jumlah_masuk').val() == $('#jumlah_keluar').val()) {
			formData.set('no_voucher', $('#no_voucher').val());
			formData.set('tgl_bpvk', $('#tgl_bpvk').val());
			formData.set('jumlah_masuk', $('#jumlah_masuk').val());
			formData.set('jumlah_keluar', $('#jumlah_keluar').val());
			formData.set('durasi', $('#durasi').val());
			formData.delete('arrValueKasMasuk[]', '');
			$('input[name="value_kas_masuk[]"]').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '' && this.value > 0) {
						formData.append('arrValueKasMasuk[]', this.value);
					}
				}
			})
			formData.delete('arrValueKasKeluar[]', '');
			$('input[name="value_kas_keluar[]"]').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '' && this.value > 0) {
						formData.append('arrValueKasKeluar[]', this.value);
					}
				}
			})
			formData.delete('arrCoaMasuk[]', '');
			$('select[name="ddl_coa_masuk[]"] option:selected').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '') {
						formData.append('arrCoaMasuk[]', this.value);
					}
				}
			})
			formData.delete('arrCoaKeluar[]', '');
			$('select[name="ddl_coa_keluar[]"] option:selected').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '') {
						formData.append('arrCoaKeluar[]', this.value);
					}
				}
			})
			formData.delete('arrValasMasuk[]', '');
			$('select[name="ddl_valas_masuk[]"] option:selected').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '') {
						formData.append('arrValasMasuk[]', this.value);
					}
				}
			})
			formData.delete('arrValasKeluar[]', '');
			$('select[name="ddl_valas_keluar[]"] option:selected').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '') {
						formData.append('arrValasKeluar[]', this.value);
					}
				}
			})
			formData.delete('arrNotesKeluar[]', '');
			$('input[name="note_keluar[]"]').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '') {
						formData.append('arrNotesKeluar[]', this.value);
					}
				}
			})
			formData.delete('arrNotesMasuk[]', '');
			$('input[name="note_masuk[]"]').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '') {
						formData.append('arrNotesMasuk[]', this.value);
					}
				}
			})
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: formData,
				cache: false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function() {
							window.location.href = "<?php echo base_url('jurnal_np'); ?>";
						})
					} else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Tambah Jurnal Umum");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Tambah Jurnal Umum");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		} else {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah Jurnal Umum");
			swal("Warning!", "Maaf, jumlah debit tidak sama dengan jumlah kredit silahkan cek kembali.", "info");
		}
	}));
</script>