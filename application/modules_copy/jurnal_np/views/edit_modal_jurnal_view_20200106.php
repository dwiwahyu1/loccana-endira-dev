<form class="form-horizontal form-label-left" id="edit_form" role="form" action="<?php echo base_url('jurnal_np/save_edit_jurnal_np'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_voucher">No Voucher <span class="required"><sup>*</sup></span></label>
		<div class="input-group col-md-8 col-sm-6 col-xs-12 ">
			<input placeholder="No Voucher" type="text" class="form-control" id="no_voucher" name="no_voucher" required="required" value="<?php if(isset($detail_jurnal[0]['no_voucher'])) { echo $detail_jurnal[0]['no_voucher']; } ?>" readonly>
			<span id="no_voucher_tick"></span>
			<span id="no_voucher_loading-us"></span>
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tgl_bpvk">Tanggal <span class="required"><sup>*</sup></span></label>

		<div class="input-group date col-md-8 col-sm-6 col-xs-12 ">
			<input placeholder="Tanggal" type="text" class="form-control datepicker" id="tgl_bpvk" name="tgl_bpvk" required="required" value="<?php if(isset($detail_jurnal[0]['date_input'])) { echo date('Y-m-d', strtotime($detail_jurnal[0]['date_input'])); } else { echo date('Y-m-d');} ?>">
			<div class="input-group-addon">
				<span class="glyphicon glyphicon-th"></span>
			</div>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Debit: </label>
		<div class="col-md-8 col-sm-6 col-xs-12 add_item" onclick="edit_coa_value_masuk()">
			<a type="button" class="btn btn-default btn-icon waves-effect waves-light m-b-5">
				<i class="fa fa-plus"></i>
			</a>
			Tambah Debit
		</div>
	</div>
	
	<div class="item form-group">
		<div class="col-md-8 col-md-offset-3">
			<table id="list_barang_masuk" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>ID</th>
						<th>Perkiraan</th>
						<th>COA</th>
						<th>Keterangan</th>
						<th>Tanggal</th>
						<th>Valas</th>
						<th>Rate</th>
						<th>Tipe</th>
						<th>ID COA</th>
						<th>ID Parent</th>
						<th>Nilai</th>
						<th>Value Real</th>
						<th>Bukti</th>
						<th>ID Valas</th>
						<th style="width: 5%">Option</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jumlah">Jumlah Debit</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="jumlah_masuk" name="jumlah_masuk" class="form-control" placeholder="Jumlah Masuk" autocomplete="off" style="text-align:right" value="<?php
				if(isset($total_coa[0]['total_pemasukan'])) echo number_format($total_coa[0]['total_pemasukan'], 0);
			?>" readonly required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Kredit: </label>
		<div class="col-md-8 col-sm-6 col-xs-12 add_item" onclick="edit_coa_value_keluar()">
			<a type="button" class="btn btn-default btn-icon waves-effect waves-light m-b-5">
				<i class="fa fa-plus"></i>
			</a>
			Tambah Kredit
		</div>
	</div>
	
	<div class="item form-group">
		<div class="col-md-8 col-md-offset-3">
			<table id="list_barang_keluar" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>ID</th>
						<th>Perkiraan</th>
						<th>COA</th>
						<th>Keterangan</th>
						<th>Tanggal</th>
						<th>Valas</th>
						<th>Rate</th>
						<th>Tipe</th>
						<th>ID COA</th>
						<th>ID Parent</th>
						<th>Nilai</th>
						<th>Value Real</th>
						<th>Bukti</th>
						<th>ID Valas</th>
						<th style="width: 5%">Option</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jumlah">Jumlah Kredit</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="jumlah_keluar" name="jumlah_keluar" class="form-control" placeholder="Jumlah Keluar" autocomplete="off" style="text-align:right" value="<?php
				if(isset($total_coa[0]['total_pengeluaran'])) echo number_format($total_coa[0]['total_pengeluaran'], 0);
			?>" readonly required>
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Jurnal Non Produksi</button>
			<input type="hidden" id="id_jurnal" name="id_jurnal" value="<?php if(isset($detail_jurnal[0]['id_jurnal'])){ echo $detail_jurnal[0]['id_jurnal']; }?>">
		</div>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	var t_editBarangMasuk;
	var t_editBarangKeluar;
	
	$(document).ready(function() {
		$(".date").datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});
		dtBarangMasuk();
		dtBarangKeluar();
	});
	
	function redrawTableMasuk(row) {
		var rowDataMasuk = t_editBarangMasuk.rows(row).data();
		t_editBarangMasuk.draw();
	}
	
	function redrawTableKeluar(row) {
		var rowDataKeluar = t_editBarangKeluar.rows(row).data();
		t_editBarangKeluar.draw();
	}
	
	$('#list_barang_masuk').on("click", "a", function() {
		var dataBarangMasuk = t_editBarangMasuk.row($(this).parents('tr')).data();
		var rowParent = $(this).parents('tr');

		if(dataBarangMasuk[0] != "") {
			swal({
				title: 'Yakin akan Menghapus Barang yang sudah ada ?',
				text: 'data tidak dapat dikembalikan bila sudah dihapus !',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak'
			}).then(function () {
				delRowMasuk(rowParent);
				var datapost = {
					"id" : parseInt(dataBarangMasuk[0])
				};

				$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>jurnal_np/delete_jurnal_coa_temporary",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
						if (response.status == "success") {
							delRowMasuk(rowParent);

							swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () { })
						}else {
							swal("Failed!", response.message, "error");
						}
					}
				});
			});
		}else delRowMasuk(rowParent);
		
		var jumlahDebitMasuk = 0;
		for (var i = 0; i < t_editBarangMasuk.rows().data().length; i++) {
			var rowDataMasuk = t_editBarangMasuk.row(i).data();
			var tempDebitMasuk = rowDataMasuk[10];
			jumlahDebitMasuk = jumlahDebitMasuk + parseInt(tempDebitMasuk);
		}
		$('#jumlah_masuk').val(jumlahDebitMasuk);
	});

	$('#list_barang_keluar').on("click", "a", function() {
		var dataBarangKeluar = t_editBarangKeluar.row($(this).parents('tr')).data();
		var rowParent = $(this).parents('tr');

		if(dataBarangKeluar[0] != "") {
			swal({
				title: 'Yakin akan Menghapus Barang yang sudah ada ?',
				text: 'data tidak dapat dikembalikan bila sudah dihapus !',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak'
			}).then(function () {
				delRowKeluar(rowParent);
				var datapost = {
					"id" : parseInt(dataBarangKeluar[0])
				};

				$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>jurnal_np/delete_jurnal_coa_temporary",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
						if (response.status == "success") {
							delRowKeluar(rowParent);

							swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () { })
						}else {
							swal("Failed!", response.message, "error");
						}
					}
				});
			});
		}else delRowKeluar(rowParent);
		
		var jumlahKreditKeluar = 0;
		for (var i = 0; i < t_editBarangKeluar.rows().data().length; i++) {
			var rowDataKeluar = t_editBarangKeluar.row(i).data();
			var tempKreditKeluar = rowDataKeluar[10];
			jumlahKreditKeluar = jumlahKreditKeluar + parseInt(tempKreditKeluar);
		}
		$('#jumlah_keluar').val(jumlahKreditKeluar);
		
	});
	
	function delRowMasuk(rowParent) {
		t_editBarangMasuk.row(rowParent).remove().draw(false);
	}
	
	function delRowKeluar(rowParent) {
		t_editBarangKeluar.row(rowParent).remove().draw(false);
	}
	
	function dtBarangMasuk() {
		t_editBarangMasuk = $('#list_barang_masuk').DataTable( {
			"processing": true,
			"searching": false,
			"responsive": true,
			"lengthChange": false,
			"ajax": {
				"type" : "GET",
				"url" : "<?php echo base_url().'jurnal_np/edit_detail_lists_jurnal_np_coa/'.$detail_jurnal[0]['id_jurnal'].'/0';?>"
            },
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0],
				"visible": true,
				"searchable": false
			},{
				"targets": [6,8,9,11,12,13],
				"visible": false,
				"searchable": false
			},{
				"targets": [14],
				"width": 15,
				"searchable": false
			}]
		});
	}
	
	function dtBarangKeluar() {
		t_editBarangKeluar = $('#list_barang_keluar').DataTable( {
			"processing": true,
			"searching": false,
			"responsive": true,
			"lengthChange": false,
			"ajax": {
				"type" : "GET",
				"url" : "<?php echo base_url().'jurnal_np/edit_detail_lists_jurnal_np_coa/'.$detail_jurnal[0]['id_jurnal'].'/1';?>"
            },
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0],
				"visible": true,
				"searchable": false
			},{
				"targets": [6,8,9,11,12,13],
				"visible": false,
				"searchable": false
			},{
				"targets": [14],
				"width": 15,
				"searchable": false
			}]
		});
	}
	
	$('#edit_form').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		
		if($('#jumlah_masuk').val() == $('#jumlah_keluar').val()) {
			var formData = new FormData(this);
			$.ajax({
				type:'POST',
				url: $(this).attr('action'),
				data: formData,
				cache:false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						save_coa_temporary(response.id_jurnal);
					}else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Edit Purchase Order");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Edit Jurnal Non Produksi");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}else{
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah Jurnal Non Produksi");
			swal("Warning!", "Maaf, jumlah debit tidak sama dengan jumlah kredit silahkan cek kembali.", "info");
		}
	}));
	
	function save_coa_temporary(id_jurnal) {
		var arrTempMasuk = [];
		var arrTempKeluar = [];
		for (var i = 0; i < t_editBarangMasuk.rows().data().length; i++) {
			var rowDataMasuk = t_editBarangMasuk.row(i).data();
			arrTempMasuk.push(rowDataMasuk);
		}
		
		for (var i = 0; i < t_editBarangKeluar.rows().data().length; i++) {
			var rowDataKeluar = t_editBarangKeluar.row(i).data();
			arrTempKeluar.push(rowDataKeluar);
		}

		var datapost = {
			"id_jurnal"		: id_jurnal,
			'listcoamasuk'	: arrTempMasuk,
			'listcoakeluar'	: arrTempKeluar,
		};
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>jurnal_np/save_edit_jurnal_coa_temporary",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				console.log(response);
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						window.location.href = "<?php echo base_url('jurnal_np');?>";
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Edit Jurnal Non Produksi");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Edit Jurnal Non Produksi");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}
</script>