<style>
	#loading {
		position: absolute;
		background: #FFFFFF;
		opacity: 0.5;
		width: 93%;
		height: 85%;
		z-index: 3;
		text-align: center;
		display: none;
	}
	
	.x-hidden {
		display: none;
	}
</style>
<div id="loading"><img src="<?php echo base_url(); ?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/preloader.gif" /></div>
<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
<div class="row">
	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="level1">Coa <span class="required"><sup>*</sup></span></label>
		</div>
		<div class="col-md-8">
			<select class="form-control select-control" id="level1" name="level1" style="width: 100%" required>
				<?php foreach ($coa_all as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>"><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;" id="subdetail_temp">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="id_valas">Valas <span class="required"><sup>*</sup></span></label>
		</div>
		<div class="col-md-8">
			<select class="form-control" id="id_valas" name="id_valas" style="width: 100%" required>
				<?php foreach ($valas as $key) { ?>
					<option value="<?php echo $key['valas_id']; ?>"><?php echo $key['nama_valas']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="rate">Rate <span class="required"><sup>*</sup></span></label>
		</div>
		<div class="col-md-8">
			<input type="number" class="form-control" id="rate" name="rate" min="1" placeholder="Rate" value="1" required>
		</div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="value">Nilai <span class="required"><sup>*</sup></span></label>
		</div>
		<div class="col-md-8">
			<input type="number" class="form-control" id="value" name="value" min="0" placeholder="Nilai" value="0" required="required">
		</div>
	</div>

	<div class="col-sm-12 x-hidden" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="type_cash">Type <span class="required"><sup>*</sup></span></label>
		</div>
		<div class="col-md-8">
			<select class="form-control" id="type_cash" name="type_cash" style="width: 100%" required readonly>
				<option value="0" selected>Pemasukan</option>
			</select>
		</div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="note">Catatan</label>
		</div>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<textarea data-parsley-maxlength="255" type="text" id="note" name="note" class="form-control col-md-7 col-xs-12" placeholder="Catatan"></textarea>
		</div>
	</div>

	<div class="col-sm-12 x-hidden" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="file_kas_masuk">File Bukti</label>
		</div>
		<div class="col-md-8">
			<input type="file" class="form-control" id="file_kas_masuk" name="file_kas_masuk" data-height="110" accept=".pdf, .txt, .doc, .docx" />
			<span id="file_bc_tick">Hanya format file pdf,txt,doc,docx dengan besaran max 10Mb yang diterima.</span>
		</div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="submit"></label>
		</div>
		<div class="col-md-8">
			<button id="btn-submit-kas" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Tambah Debit</button>
		</div>
	</div>
</div>
<!-- <form class="form-horizontal form-label-left"> -->

	<!-- <div class="item form-group" id="level2_temp">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control select-control" id="level2" name="level2" style="width: 100%">
				<?php foreach ($level2 as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>"><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group" id="level3_temp">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control select-control" id="level3" name="level3" style="width: 100%">
				<?php foreach ($level3 as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>"><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group" id="level4_temp">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="level4" name="level4" style="width: 100%">
				<?php foreach ($level4 as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>"><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div> -->

	<!-- <div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Tanggal <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<div class="input-group date">
				<input placeholder="Tanggal" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="date" name="date" required="required" value="<?php echo date('Y-m-d'); ?>">
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
	</div> -->
<!-- </form> -->
<!-- /page content -->

<script type="text/javascript">
	// function coa(level, id) {
	// 	$('#loading').show();
	// 	$.ajax({
	// 		type: 'GET',
	// 		url: "<?php echo base_url(); ?>jurnal_np/change_coa",
	// 		data: {
	// 			id: id
	// 		},
	// 		success: function(response) {
	// 			if (response.length) {
	// 				var element = '';
	// 				$.each(response, function(key, val) {
	// 					element += '<option value="' + val.id_coa + '" data-coa="' + val.coa + '">' + val.keterangan + '</option>';
	// 				});

	// 				if (level === 'level1') {
	// 					$('#level2').html(element);
	// 					$('#level2_temp').show();

	// 					coa('level2', response[0].coa);
	// 				} else if (level === 'level2') {
	// 					$('#level3').html(element);
	// 					$('#level3_temp').show();

	// 					coa('level3', response[0].coa);
	// 				} else {
	// 					$('#level4').html(element);
	// 					$('#level4_temp').show();
	// 					$('#loading').hide();
	// 				}
	// 			} else {
	// 				if (level === 'level1') {
	// 					$('#level2_temp,#level3_temp,#level4_temp').hide();
	// 				} else if (level === 'level2') {
	// 					$('#level3_temp,#level4_temp').hide();
	// 				} else {
	// 					$('#level4_temp').hide();
	// 				}
	// 				$('#loading').hide();
	// 			}
	// 		}
	// 	});
	// }
	
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();

		/*$("#date").datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});*/

		/*$('.select-control').on('change', (function(e) {
			var id = $(this).children('option:selected').data('coa'),
				level = $(this).attr('id');

			coa(level, id);
		}));*/
		
		$('#level1').select2();
		// $('#level2').select2();
		// $('#level3').select2();
		// $('#level4').select2();
	});

	$('#btn-submit-kas').on('click', (function(e) {
		var tempArrMasuk = [];
		var jumlahMasuk = 0;
		var statusMasuk = 0;
		
		$('#btn-submit-kas').attr('disabled', 'disabled');
		$('#btn-submit-kas').text("Memasukkan data...");
		e.preventDefault();
		
		if($('#value').val() != 0 && $('#value').val() >= 0 && $('#value').val() != null) {
			lastItemMasuk++;

			var id_coa_masuk = '0',
				id_parent_masuk = $('#level1').val(),
				coa_number_masuk = $('#level1 option:selected').data("coa"),
				text_coa_masuk = $('#level1 option:selected').text();

			if($('#level4_temp').is(':visible')) {
				id_coa_masuk = $('#level4').val();
				text_coa_masuk = $('#level4 option:selected').text();
				coa_number_masuk = $('#level4 option:selected').data("coa");
			}else if ($('#level3_temp').is(':visible')) {
				id_coa_masuk = $('#level3').val();
				text_coa_masuk = $('#level3 option:selected').text();
				coa_number_masuk = $('#level3 option:selected').data("coa");
			}else if ($('#level2_temp').is(':visible')) {
				id_coa_masuk = $('#level2').val();
				text_coa_masuk = $('#level2 option:selected').text();
				coa_number_masuk = $('#level2 option:selected').data("coa");
			}else id_coa_masuk = id_parent_masuk;
			
			var temp_kas_data_masuk = {
				id_coa_masuk: id_coa_masuk,
				text_coa_masuk: text_coa_masuk,
				coa_number_masuk: coa_number_masuk,
				id_parent_masuk: id_parent_masuk,
				//date_masuk: $('#date').val(),
				id_valas_masuk: $('#id_valas').val(),
				text_valas_masuk: $('#id_valas option:selected').text(),
				value_masuk: $('#value').val(),
				rate_masuk: $('#rate').val(),
				type_cash_masuk: 0,
				type_cash_text_masuk: 'Debit',
				note_masuk: $('#note').val(),
			}
			
			/*formData.append('id_coa_masuk[]', temp_kas_data_masuk.id_coa_masuk);
			formData.append('text_coa_masuk[]', temp_kas_data_masuk.text_coa_masuk);
			formData.append('coa_number_masuk[]', temp_kas_data_masuk.coa_number_masuk);
			formData.append('id_parent_masuk[]', temp_kas_data_masuk.id_parent_masuk);
			//formData.append('date_masuk[]', temp_kas_data_masuk.date_masuk);
			formData.append('id_valas_masuk[]', temp_kas_data_masuk.id_valas_masuk);
			formData.append('value_masuk[]', temp_kas_data_masuk.value_masuk);
			formData.append('rate_masuk[]', temp_kas_data_masuk.rate_masuk);
			formData.append('type_cash_masuk[]', temp_kas_data_masuk.type_cash_masuk);
			formData.append('type_cash_text_masuk[]', temp_kas_data_masuk.type_cash_text_masuk);
			formData.append('note_masuk[]', temp_kas_data_masuk.note_masuk);
			formData.append('have_file_masuk[]', "new");
			
			if ($('#file_kas_masuk')[0].files.length > 0) {
				if ($('#file_kas_masuk')[0].files[0].size <= 9437184) {
					formData.append('file_kas_masuk[]', $('#file_kas_masuk')[0].files[0], $('#file_kas_masuk')[0].files[0].name);
					temp_kas_data_masuk.file = new Blob();
					temp_kas_data_masuk.file_name = $('#file_kas_masuk')[0].files[0].name;

				} else {
					$('#btn-submit-kas').removeAttr('disabled');
					$('#btn-submit-kas').text("Tambah KAS");
					swal("Failed!", "Ukuran File Terlalu besar, silahkan cek kembali", "error");
				}
			}else {
				formData.append('file_kas_masuk[]', new Blob, '');
				temp_kas_data_masuk.file = new Blob;
				temp_kas_data_masuk.file_name = '';
			}*/

			lastRowMasuk = lastItemMasuk;
			lastCoaMasuk = temp_kas_data_masuk.id_coa_masuk;
			add_kas_masuk_temp_data(temp_kas_data_masuk);
			$('#panel-modal-lv1').modal('toggle');
		}else{
			$('#btn-submit-kas').removeAttr('disabled');
			$('#btn-submit-kas').text("Tambah Kas");
			swal("Warning!", "Maaf, jumlah nilai tidak boleh 0 (nol).", "info");
		}
	}));
</script>