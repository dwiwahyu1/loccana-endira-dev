	<div class="row">
		<div class="col-md-12">
			<div class="col-md-5 pull-left">
				<div class="row">
					<label class="col-md-4">Tanggal Input </label>
					<label class="col-md-1 pull-left">:</label>
					<label class="col-md-7 pull-left"><?php if(isset($detail_jurnal[0]['date'])){ echo date('d M Y', strtotime($detail_jurnal[0]['date'])); }?></label>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<div class="col-md-5 pull-left">
				<div class="row">
					<label class="col-md-4">Nama PIC </label>
					<label class="col-md-1 pull-left">:</label>
					<label class="col-md-7 pull-left"><?php if(isset($detail_jurnal[0]['nama'])){ echo $detail_jurnal[0]['nama']; }?></label>
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<div class="col-sm-6">
				<div class="row">
					<h4 class="page-title" id="title_menu">Debit</h4>
				</div>
				<div class="row">
					<div class="col-md-12">
						<table id="list-detail-coa-masuk" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>No</th>
									<th>COA</th>
									<th>Parent</th>
									<th>Value</th>
									<th>Status</th>
									<th>Keterangan</th>
									<th>Value Real</th>
									<th>Rate</th>
									<th>Bukti</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="row">
					<h4 class="page-title" id="title_menu">Kredit</h4>
				</div>
				<div class="row">
					<div class="col-md-12">
						<table id="list-detail-coa-keluar" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>No</th>
									<th>COA</th>
									<th>Parent</th>
									<th>Value</th>
									<th>Status</th>
									<th>Keterangan</th>
									<th>Value Real</th>
									<th>Rate</th>
									<th>Bukti</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<script type="text/javascript">
	$(document).ready(function(){
		dt_jurnal_coa_masuk();
		dt_jurnal_coa_keluar();
	});

	function dt_jurnal_coa_masuk() {
		$('#list-detail-coa-masuk').DataTable( {
			"processing": true,
			"searching": false,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"ajax": {
				"type" : "GET",
				"url" : "<?php echo base_url().'jurnal_np/detail_lists_jurnal_np_coa/'.$detail_jurnal[0]['id_jurnal'].'/0';?>"
			},
			"columnDefs": [{
				"targets": [3,6,7],
				"className": 'dt-body-right',
			}]
		});
	}
	
	function dt_jurnal_coa_keluar() {
		$('#list-detail-coa-keluar').DataTable( {
			"processing": true,
			"searching": false,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"ajax": {
				"type" : "GET",
				"url" : "<?php echo base_url().'jurnal_np/detail_lists_jurnal_np_coa/'.$detail_jurnal[0]['id_jurnal'].'/1';?>"
			},
			"columnDefs": [{
				"targets": [3,6,7],
				"className": 'dt-body-right',
			}]
		});
	}
</script>