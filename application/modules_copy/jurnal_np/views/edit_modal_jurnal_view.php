<form class="form-horizontal form-label-left" id="edit_form" role="form" action="<?php echo base_url('jurnal_np/save_edit_jurnal_np'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_voucher">No Voucher <span class="required"><sup>*</sup></span></label>
		<div class="input-group col-md-8 col-sm-6 col-xs-12 ">
			<input placeholder="No Voucher" type="text" class="form-control" id="no_voucher" name="no_voucher" required="required" value="<?php if(isset($detail_jurnal[0]['no_voucher'])) { echo $detail_jurnal[0]['no_voucher']; } ?>" readonly>
			<span id="no_voucher_tick"></span>
			<span id="no_voucher_loading-us"></span>
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tgl_bpvk">Tanggal <span class="required"><sup>*</sup></span></label>

		<div class="input-group date col-md-8 col-sm-6 col-xs-12 ">
			<input placeholder="Tanggal" type="text" class="form-control datepicker" id="tgl_bpvk" name="tgl_bpvk" value="<?php if(isset($detail_jurnal[0]['date_input'])) { echo date('Y-m-d', strtotime($detail_jurnal[0]['date_input'])); } else { echo date('Y-m-d');} ?>" required>
			<div class="input-group-addon">
				<span class="glyphicon glyphicon-th"></span>
			</div>
		</div>
	</div>

	<div class="item form-group" style="display: none;">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="durasi">Durasi </label>
		<div class="input-group col-md-8 col-sm-6 col-xs-12 ">
			<select class="form-control" id="durasi" name="durasi">
				<option value="0" selected>Tanpa Durasi</option>
				<option value="1">1 Day</option>
				<option value="15">15 Days</option>
				<option value="30">30 Days</option>
				<option value="45">45 Days</option>
				<option value="60">60 Days</option>
				<option value="90">90 Days</option>
				<option value="120">120 Days</option>
				<option value="180">180 Days</option>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Debit: </label>
		<div class="col-md-8 col-sm-6 col-xs-12 add_item" onclick="edit_coa_value_masuk()">
			<a class="btn btn-default btn-icon waves-effect waves-light m-b-5">
				<i class="fa fa-plus"></i>
			</a>
			Tambah Debit
		</div>
	</div>
	
	<div class="item form-group">
		<div class="col-md-8 col-md-offset-3">
			<table id="list_barang_masuk" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>ID</th>
						<th></th>
						<th>Perkiraan</th>
						<th>COA</th>
						<th>Keterangan</th>
						<th>Valas</th>
						<th>Rate</th>
						<th>Nilai</th>
						<th>Bukti</th>
						<th style="width: 5%">Option</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jumlah">Jumlah Debit</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="jumlah_masuk" name="jumlah_masuk" class="form-control" placeholder="Jumlah Masuk" autocomplete="off" style="text-align:right" value="<?php
				if(isset($total_coa[0]['total_pemasukan'])) echo number_format($total_coa[0]['total_pemasukan'], 0);
			?>" readonly required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Kredit: </label>
		<div class="col-md-8 col-sm-6 col-xs-12 add_item" onclick="edit_coa_value_keluar()">
			<a class="btn btn-default btn-icon waves-effect waves-light m-b-5">
				<i class="fa fa-plus"></i>
			</a>
			Tambah Kredit
		</div>
	</div>
	
	<div class="item form-group">
		<div class="col-md-8 col-md-offset-3">
			<table id="list_barang_keluar" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>ID</th>
						<th></th>
						<th>Perkiraan</th>
						<th>COA</th>
						<th>Keterangan</th>
						<th>Valas</th>
						<th>Rate</th>
						<th>Nilai</th>
						<th>Bukti</th>
						<th style="width: 5%">Option</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jumlah">Jumlah Kredit</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="jumlah_keluar" name="jumlah_keluar" class="form-control" placeholder="Jumlah Keluar" autocomplete="off" style="text-align:right" value="<?php
				if(isset($total_coa[0]['total_pengeluaran'])) echo number_format($total_coa[0]['total_pengeluaran'], 0);
			?>" readonly required>
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Jurnal Umum</button>
			<input type="hidden" id="id_jurnal" name="id_jurnal" value="<?php if(isset($detail_jurnal[0]['id_jurnal'])){ echo $detail_jurnal[0]['id_jurnal']; }?>">
		</div>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	var lastItemMasuk = 0;
	var lastRowMasuk = "";
	var lastCoaMasuk = "";
	var lastItemKeluar = 0;
	var lastRowKeluar = "";
	var lastCoaKeluar = "";
	var itemsMasuk = [];
	var itemsKeluar = [];
	var t_editBarangMasuk;
	var t_editBarangKeluar;
	
	$(document).ready(function() {
		$(".date").datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});
		dtBarangMasuk();
		dtBarangKeluar();
	});

	// START JS ITEM MASUK
	function dtBarangMasuk() {
		t_editBarangMasuk = $('#list_barang_masuk').DataTable( {
			"processing": true,
			"scrollX": true,
			"scrollCollapse": true,
			"searching": false,
			/*"responsive": {
				"details": {
					"type": "column"
				}

			},*/
			"responsive": false,
			"lengthChange": false,
			"ajax": {
				"type" : "GET",
				"url" : "<?php echo base_url().'jurnal_np/edit_detail_lists_jurnal_np_coa/'.$detail_jurnal[0]['id_jurnal'].'/0';?>"
            },
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0, 6],
				"visible": false,
				"searchable": false
			}, {
				"targets": [1],
				"width": 0
			}, {
				"targets": [2],
				"width": 170
			}, {
				"targets": [3],
				"width": 25
			}, {
				"targets": [9],
				"width": 15
			}],
			"drawCallback": function(e) {
				$('.ddl_coa_masuk').select2('destroy');
				$('.ddl_coa_masuk').select2();

				if(lastCoaMasuk != '' && lastCoaMasuk != null) {
					$('#ddl_coa_masuk'+lastRowMasuk).val(lastCoaMasuk).trigger('change');
					lastRowMasuk = "";
					lastCoaMasuk = "";
				}
				$('input[name="note_masuk[]"]').each(function() {
					lastItemMasuk = parseInt(this.id.replace(/[^0-9]+/g, ""));
				});
				calculateValueMasuk();
			}
		});
	}
	
	$('#list_barang_masuk').on("click", "a", function() {
		var dataBarangMasuk = t_editBarangMasuk.row($(this).parents('tr')).data();
		var rowParent = $(this).parents('tr');

		if(dataBarangMasuk[0] != "" && dataBarangMasuk[0] != "new" && dataBarangMasuk[0] != null) {
			swal({
				title: 'Yakin akan Menghapus Barang yang sudah ada ?',
				text: 'data tidak dapat dikembalikan bila sudah dihapus !',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak'
			}).then(function () {
				var datapost = {
					"id" : parseInt(dataBarangMasuk[0])
				};

				$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>jurnal_np/delete_jurnal_coa_temporary",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
						if (response.status == "success") {
							delRowMasuk(rowParent);

							swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () { })
						}else {
							swal("Failed!", response.message, "error");
						}
					}
				});
			});
		}else delRowMasuk(rowParent);
	});

	function add_kas_masuk_temp_data(itemsKasMasuk) {
		var tempItems = [
			'new',
			'',
			'<select onchange="changeCoaMasuk('+lastItemMasuk+')" class="form-control ddl_coa_masuk" id="ddl_coa_masuk'+lastItemMasuk+'"'+
				'name="ddl_coa_masuk[]" style="width: 100%" required>'+
				<?php foreach ($coa_all as $key) { ?>
					"<option value=\"<?php echo $key['id_coa']; ?>\"><?php echo $key['coa'].'-'.$key['keterangan']; ?></option>"+
				<?php } ?>
			'</select>',
			'<input type="text" class="form-control" id="coa_number_masuk'+ lastItemMasuk+'" name="coa_number_masuk[]"'+
				'value="'+itemsKasMasuk.coa_number_masuk+'" placeholder="Coa Number Masuk" autocomplete="off" readonly>',
			'<input type="text" class="form-control" id="note_masuk'+lastItemMasuk+'" name="note_masuk[]" value="'+itemsKasMasuk.note_masuk+'"'+
				'placeholder="Note" autocomplete="off">',
			'<select  class="form-control ddl_valas_masuk" id="ddl_valas_masuk'+lastItemMasuk+'" name="ddl_valas_masuk[]" style="width: 100%" required >'+
				<?php foreach ($valas as $key) { ?>
					'<option value="<?php echo $key['valas_id']; ?>"><?php echo $key['nama_valas']; ?></option>'+
				<?php } ?>
			'</select>',
			itemsKasMasuk.rate_masuk,
			'<input type="number" onKeyup="calculateValueMasuk()" class="form-control" id="value_kas_masuk'+lastItemMasuk+'" name="value_kas_masuk[]"'+
				'value="'+itemsKasMasuk.value_masuk+'" placeholder="Nominal" autocomplete="off">',
			itemsKasMasuk.type_cash_text_masuk,
			'<a class="btn btn-icon waves-effect waves-light btn-danger">'+
				'<i class="fa fa-trash"></i>'+
			'</a>'
		]
		t_editBarangMasuk.row.add(tempItems).draw();
	}

	function delRowMasuk(rowParent) {
		t_editBarangMasuk.row(rowParent).remove().draw(false);
		calculateValueMasuk();
	}

	function calculateValueMasuk() {
		var jumlahValueMasuk = 0;

		$('input[name="value_kas_masuk[]"]').each(function() {
			jumlahValueMasuk = jumlahValueMasuk + parseFloat(this.value);
		});
		$('#jumlah_masuk').val(jumlahValueMasuk);
	}

	function changeCoaMasuk(idRow) {
		var strOption = $('#ddl_coa_masuk'+idRow+' option:selected').text();
		var arrStr = strOption.split("-");
		if(arrStr[0]) {
			$('#coa_number_masuk'+idRow).val(arrStr[0].trim());
		}
	}
	// END JS ITEM MASUK

	// START JS ITEM KELUAR
	function dtBarangKeluar() {
		t_editBarangKeluar = $('#list_barang_keluar').DataTable( {
			"processing": true,
			"searching": false,
			"scrollX": true,
			"scrollCollapse": true,
			"responsive": false,
			"lengthChange": false,
			"ajax": {
				"type" : "GET",
				"url" : "<?php echo base_url().'jurnal_np/edit_detail_lists_jurnal_np_coa/'.$detail_jurnal[0]['id_jurnal'].'/1';?>"
            },
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0, 6],
				"visible": false,
				"searchable": false
			}, {
				"targets": [1],
				"width": 0
			}, {
				"targets": [2],
				"width": 170
			}, {
				"targets": [3],
				"width": 25
			}, {	
				"targets": [9],
				"width": 15
			}],
			"drawCallback": function(e) {
				$('.ddl_coa_keluar').select2('destroy');
				$('.ddl_coa_keluar').select2();

				if(lastCoaKeluar != '' && lastCoaKeluar != null) {
					$('#ddl_coa_keluar'+lastRowKeluar).val(lastCoaKeluar).trigger('change');
					lastRowMasuk = "";
					lastCoaKeluar = "";
				}
				$('input[name="note_keluar[]"]').each(function() {
					lastItemKeluar = parseInt(this.id.replace(/[^0-9]+/g, ""));
				});
				calculateValueKeluar();
			}
		});
	}

	$('#list_barang_keluar').on("click", "a", function() {
		var dataBarangKeluar = t_editBarangKeluar.row($(this).parents('tr')).data();
		var rowParent = $(this).parents('tr');

		if(dataBarangKeluar[0] != "" && dataBarangKeluar[0] != "new" && dataBarangKeluar[0] != null) {
			swal({
				title: 'Yakin akan Menghapus Barang yang sudah ada ?',
				text: 'data tidak dapat dikembalikan bila sudah dihapus !',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak'
			}).then(function () {
				var datapost = {
					"id" : parseInt(dataBarangKeluar[0])
				};

				$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>jurnal_np/delete_jurnal_coa_temporary",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
						if (response.status == "success") {
							delRowKeluar(rowParent);

							swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () { })
						}else {
							swal("Failed!", response.message, "error");
						}
					}
				});
			});
		}else delRowKeluar(rowParent);
	});

	function add_kas_keluar_temp_data(itemsKasKeluar) {
		var tempItems = [
			'new',
			'',
			'<select onchange="changeCoaKeluar('+lastItemKeluar+')" class="form-control ddl_coa_keluar" id="ddl_coa_keluar'+lastItemKeluar+'"'+
				'name="ddl_coa_keluar[]" style="width: 100%" required>'+
				<?php foreach ($coa_all as $key) { ?>
					"<option value=\"<?php echo $key['id_coa']; ?>\"><?php echo $key['coa'].'-'.$key['keterangan']; ?></option>"+
				<?php } ?>
			'</select>',
			'<input type="text" class="form-control" id="coa_number_keluar'+ lastItemKeluar+'" name="coa_number_keluar[]"'+
				'value="'+itemsKasKeluar.coa_number_keluar+'" placeholder="Coa Number Keluar" autocomplete="off" readonly>',
			'<input type="text" class="form-control" id="note_keluar'+lastItemKeluar+'" name="note_keluar[]" value="'+itemsKasKeluar.note_keluar+'"'+
				'placeholder="Note" autocomplete="off">',
			'<select  class="form-control ddl_valas_keluar" id="ddl_valas_keluar'+lastItemKeluar+'" name="ddl_valas_keluar[]" style="width: 100%" required >'+
				<?php foreach ($valas as $key) { ?>
					'<option value="<?php echo $key['valas_id']; ?>"><?php echo $key['nama_valas']; ?></option>'+
				<?php } ?>
			'</select>',
			itemsKasKeluar.rate_keluar,
			'<input type="number" onKeyup="calculateValueKeluar()" class="form-control" id="value_kas_keluar'+lastItemKeluar+'" name="value_kas_keluar[]"'+
				'value="'+itemsKasKeluar.value_keluar+'" placeholder="Nominal" autocomplete="off">',
			itemsKasKeluar.type_cash_text_keluar,
			'<a class="btn btn-icon waves-effect waves-light btn-danger">'+
				'<i class="fa fa-trash"></i>'+
			'</a>'
		]
		t_editBarangKeluar.row.add(tempItems).draw();
	}
	
	function delRowKeluar(rowParent) {
		t_editBarangKeluar.row(rowParent).remove().draw(false);
	}

	function calculateValueKeluar() {
		var jumlahValueKeluar = 0;

		$('input[name="value_kas_keluar[]"]').each(function() {
			jumlahValueKeluar = jumlahValueKeluar + parseFloat(this.value);
		});
		$('#jumlah_keluar').val(jumlahValueKeluar);
	}

	function changeCoaKeluar(idRow) {
		var strOption = $('#ddl_coa_keluar'+idRow+' option:selected').text();
		var arrStr = strOption.split("-");
		if(arrStr[0]) {
			$('#coa_number_keluar'+idRow).val(arrStr[0].trim());
		}
	}
	// END JS ITEM KELUAR
	
	$('#edit_form').on('submit',(function(e) {
		var arrTempMasuk = [];
		var arrTempKeluar = [];
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();

		if($('#jumlah_masuk').val() == $('#jumlah_keluar').val()) {
			var formData = new FormData();
			formData.set('id_jurnal', $('#id_jurnal').val());
			formData.set('no_voucher', $('#no_voucher').val());
			formData.set('tgl_bpvk', $('#tgl_bpvk').val());
			formData.set('jumlah_masuk', $('#jumlah_masuk').val());
			formData.set('jumlah_keluar', $('#jumlah_keluar').val());
			formData.set('durasi', $('#durasi').val());

			//Start Set Form Item Masuk
			for (var i = 0; i < t_editBarangMasuk.rows().data().length; i++) {
				var rowDataMasuk = t_editBarangMasuk.row(i).data();
				formData.append('arrIdMasuk[]', rowDataMasuk[0]);
				formData.append('arrRateMasuk[]', rowDataMasuk[6]);
			}

			$('select[name="ddl_coa_masuk[]"] option:selected').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '') {
						formData.append('arrCoaMasuk[]', this.value);
					}
				}
			})
			$('input[name="note_masuk[]"]').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '') {
						formData.append('arrNoteMasuk[]', this.value);
					}else formData.append('arrNoteMasuk[]', null);
				}else formData.append('arrNoteMasuk[]', null);
			})

			$('select[name="ddl_valas_masuk[]"] option:selected').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '') {
						formData.append('arrValasMasuk[]', this.value);
					}
				}
			})

			$('input[name="value_kas_masuk[]"]').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '' && this.value > 0) {
						formData.append('arrValueKasMasuk[]', this.value);
					}
				}
			})
			//End Set Form Item Masuk

			//Start Set Form Item Keluar
			for (var i = 0; i < t_editBarangKeluar.rows().data().length; i++) {
				var rowDataKeluar = t_editBarangKeluar.row(i).data();
				formData.append('arrIdKeluar[]', rowDataKeluar[0]);
				formData.append('arrRateKeluar[]', rowDataKeluar[6]);
			}

			$('select[name="ddl_coa_keluar[]"] option:selected').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '') {
						formData.append('arrCoaKeluar[]', this.value);
					}
				}
			})

			$('input[name="note_keluar[]"]').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '') {
						formData.append('arrNoteKeluar[]', this.value);
					}else formData.append('arrNoteKeluar[]', null);
				}else formData.append('arrNoteKeluar[]', null);
			})

			$('select[name="ddl_valas_keluar[]"] option:selected').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '') {
						formData.append('arrValasKeluar[]', this.value);
					}
				}
			})

			$('input[name="value_kas_keluar[]"]').each(function() {
				if (this.value) {
					if (this.value != undefined && this.value != '' && this.value > 0) {
						formData.append('arrValueKasKeluar[]', this.value);
					}
				}
			})
			//End Set Form Item Keluar

			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: formData,
				cache: false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function() {
							$('#panel-modal').modal('toggle');
							list_jurnal_np();
							// window.location.href = "<?php echo base_url('jurnal_np'); ?>";
						})
					} else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Edit Jurnal Umum");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Edit Jurnal Umum");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}else{
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Edit Jurnal Umum");
			swal("Warning!", "Maaf, jumlah debit tidak sama dengan jumlah kredit silahkan cek kembali.", "info");
		}
	}));
</script>