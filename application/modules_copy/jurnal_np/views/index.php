<style type="text/css">
	.dt-body-center {text-align: center;}
	.dt-body-right {text-align: right;}
	.force-overflow {height: 650px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow {min-height: 650px}
	.x-hidden {display: none;}
	.x-show {display: flex;}
	#modal-jurnal::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	#modal-jurnal::-webkit-scrollbar
	{
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-jurnal::-webkit-scrollbar-thumb
	{
		background-image: -webkit-gradient(linear,
		   left bottom,
		   left top,
		   color-stop(0.44, rgb(122,153,217)),
		   color-stop(0.72, rgb(73,125,189)),
		   color-stop(0.86, rgb(28,58,148)));
	}
	html, body {
		height: 100%;
		font-family: 'Roboto', sans-serif;
	}
	.page-content {
		height: 100%;
	}

	/* loader css starts from here */
	.loader {
		position: fixed;
		top: 20%;
		bottom: 0;
		left: 0;
		right: 0;
		z-index: 99;
		justify-content: center;
		align-items: center;
	}

	.loader .loader-inner {
		position: relative;
		width: 100%;
		height: 100%;
		display: flex;
		justify-content: center;
		align-items: center;
		-webkit-transition: width .5s, height 1s; /* For Safari 3.1 to 6.0 */
		transition: width .5s, height 1s;
	}
	.loader.success .loader-inner,
	.loader.error .loader-inner {
		width: 300px;
		height: 210px;
		border-radius: 5px;
	}

	.loader .loader-inner .loading-box {
		text-align: center;
		width: 100%;
		padding: 2em;
	}
	.loader .loader-inner .loading-box .loader-message {
		padding: 1em 0;
		color: #444;
	}
	.loader .loader-inner .loading-box  button {
		outline: none;
		border: 1px solid white;
		padding: .8em 2em;
		color: white;
		border-radius: 2px;
	}

	.loader .loader-inner .loading-box  button.cancel {
		background-color: #444;
	}
	.loader .loader-inner .loading-box  button.done,
	.loader .loader-inner .loading-box  button.retry {
		background-color: #f17f7f;
	}
	.loader .loader-inner .loading-box .circular-loader {
		border: 3px solid #f3f3f3; /* Light grey */
		border-top: 3px solid #444; /* Blue */
		border-radius: 50%;
		width: 50px;
		height: 50px;
		animation: spin 2s linear infinite;
		margin: 0 auto;
		transition: all .5s ease-out;
		position: relative;
		display: flex;
		justify-content: center;
		align-items: center;
	}

	.loader .loader-inner .loading-box  .loader-result-control {
		display: flex;
		justify-content: center;
	}
	.loader.error  .loader-inner .loading-box  .loader-result-control {
		justify-content:space-between;
	}
	.loader.error .loader-inner .loading-box  button.done {
		display: none;
	}
	.loader.error .loader-inner .loading-box  button.cancel,
	.loader.error .loader-inner .loading-box  button.retry {
		display: block;
	}
	.loader.success .loader-inner .loading-box  button.done {
		display: block;
	}
	.loader.success .loader-inner .loading-box  button.cancel,
	.loader.success .loader-inner .loading-box  button.retry {
		display: none;
	}
	.loader .loader-inner .loading-box  button.cancel,
	.loader .loader-inner .loading-box  button.retry,
	.loader .loader-inner .loading-box  button.done {
		display: none;
	}

	.loader.error .loader-inner .loading-box .circular-loader {
		border-top: 3px solid #f3f3f3; 
		animation: none;
	}

	.loader .loader-inner .loading-box .circular-loader:before,
	.loader .loader-inner .loading-box .circular-loader:after {
		content: '';
		height: 0px;
		width: 0px;
		background-color: white;
		position: absolute;
		-webkit-transition: height .5s; /* For Safari 3.1 to 6.0 */
		transition: height .5s;
	}

	/* for error */
	.loader.error .loader-inner .loading-box .circular-loader:before,
	.loader.error .loader-inner .loading-box .circular-loader:after,
	.loader.success .loader-inner .loading-box .circular-loader:before,
	.loader.success .loader-inner .loading-box .circular-loader:after {
		height: 30px;
		width: 3px;
		border-radius: 3px;
	}

	.loader.error .loader-inner .loading-box .circular-loader:before {
		transform: rotate(50deg);
	}
	.loader.error .loader-inner .loading-box .circular-loader:after {
		transform: rotate(130deg);
	}

	/* for success */
	.loader.success .loader-inner .loading-box .circular-loader {
		border: 3px solid #aff5b2; 
		animation: none;
	}

	.loader.success .loader-inner .loading-box .circular-loader:before {
		transform: rotate(38deg);
	}
	.loader.success .loader-inner .loading-box .circular-loader:after {
		transform: rotate(130deg);
	}
	.loader.success .loader-inner .loading-box .circular-loader:before,
	.loader.success .loader-inner .loading-box .circular-loader:after {
		background-color: #aff5b2;
	}
	.loader.success .loader-inner .loading-box .circular-loader:after {
		left: 11px;
		height: 10px;
		bottom: 12px
	}

	@keyframes spin {
		0% { transform: rotate(0deg); }
		100% { transform: rotate(360deg); }
	}

	/* testing control css*/

	.testing-controls {
		position: absolute;
		bottom: 20px;
	  left: 50%;
	transform: translateX(-50%);
	}
	.testing-controls button {
		outline: none;
		border: 1px solid white;
		padding: .8em 1em;
		color: white;
		border-radius: 2px;
	}

	.testing-controls .error-test  {
		background-color: #444;
	}

	.testing-controls .success-test {
		background-color: #579a59;
	}
	.testing-controls .loading-test {
		background-color: #f17f7f;
	}

</style>
<div class="container">
	<div class="loader x-hidden" id="loader-data">
		<div class="loader-inner loading">
			<div class="loading-box">
				<div class="circular-loader">
				</div>
				<div class="error-cross"></div>
				<div class="loader-message">Loading</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title" id="title_menu">Jurnal Umum</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<table id="list_jurnal" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>No Voucher</th>
							<th>Nama Pemohon</th>
							<th>Tanggal Masuk</th>
							<th>Debit</th>
							<th>Total Debit</th>
							<th>Kredit</th>
							<th>Total Kredit</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-dialog-scrollable" style="width: 90%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-jurnal">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal-detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg modal-dialog-scrollable" style="width: 100%; height: 5%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-jurnal">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal-lv1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-dialog-scrollable" style="width: 80%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-jurnal">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$(document).ready(function(){
		list_jurnal_np();
	});

	function list_jurnal_np() {
		$("#list_jurnal").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'jurnal_np/lists_jurnal_np';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false,
			"destroy": true,
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip',
			"initComplete": function(){
				var element = '<div class="btn-group pull-left">';
					element += '  	<a class="btn btn-primary" onClick="add_jurnal()">';
					element += '    	<i class="fa fa-plus"></i> Tambah Jurnal Umum';
					element += '  	</a>';
					element += '</div>';
				$("div.toolbar").prepend(element);
			},
			"columnDefs": [{
				targets: [0],
				width: 5,
				className: 'dt-body-center'
			},{ 
				targets: [5,7],
				width: 250,
				className: 'dt-body-right'
			}]
		});
	}

	function add_jurnal(){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('jurnal_np/add_jurnal');?>');
		$('#panel-modal  .panel-title').html('<i class="fa fa-plus"></i> Tambah Jurnal Umum');
		
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}
	
	function add_coa_value_masuk(){
		$('#panel-modal-lv1').removeData('bs.modal');
		$('#panel-modal-lv1  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal-lv1  .panel-body').load('<?php echo base_url('jurnal_np/add_coa_value_masuk');?>');
		$('#panel-modal-lv1  .panel-title').html('<i class="fa fa-plus"></i> Tambah Kas Masuk');
		
				
		// var date_bvk = $('#panel-modal  #tgl_bpvk').val();
		
		// $('#panel-modal-lv1  #date').val(date_bvk);
		
		$('#panel-modal-lv1').modal({backdrop:'static',keyboard:false},'show');
	}
	
	function add_coa_value_keluar(){
		$('#panel-modal-lv1').removeData('bs.modal');
		$('#panel-modal-lv1  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal-lv1  .panel-body').load('<?php echo base_url('jurnal_np/add_coa_value_keluar');?>');
		$('#panel-modal-lv1  .panel-title').html('<i class="fa fa-plus"></i> Tambah Kas Keluar');
		$('#panel-modal-lv1').modal({backdrop:'static',keyboard:false},'show');
	}
	
	function edit_coa_value_masuk(){
		$('#panel-modal-lv1').removeData('bs.modal');
		$('#panel-modal-lv1  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal-lv1  .panel-body').load('<?php echo base_url('jurnal_np/edit_coa_value_masuk');?>');
		$('#panel-modal-lv1  .panel-title').html('<i class="fa fa-plus"></i> Tambah Kas Masuk');
		$('#panel-modal-lv1').modal({backdrop:'static',keyboard:false},'show');
	}
	
	function edit_coa_value_keluar(){
		$('#panel-modal-lv1').removeData('bs.modal');
		$('#panel-modal-lv1  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal-lv1  .panel-body').load('<?php echo base_url('jurnal_np/edit_coa_value_keluar');?>');
		$('#panel-modal-lv1  .panel-title').html('<i class="fa fa-plus"></i> Tambah Kas Keluar');
		$('#panel-modal-lv1').modal({backdrop:'static',keyboard:false},'show');
	}

	function edit_jurnal(id){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('jurnal_np/edit_jurnal/');?>'+"/"+id);
		$('#panel-modal  .panel-title').html('<i class="fa fa-edit"></i> Edit Jurnal Umum');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}
	
	function detail_jurnal(id){
		$('#panel-modal-detail').removeData('bs.modal');
		$('#panel-modal-detail  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal-detail  .panel-body').load('<?php echo base_url('jurnal_np/detail_jurnal/');?>'+"/"+id);
		$('#panel-modal-detail  .panel-title').html('<i class="fa fa-edit"></i> Detail Jurnal Umum');
		$('#panel-modal-detail').modal({backdrop:'static',keyboard:false},'show');
	}

	function delete_jurnal_temporary(id) {
		swal({
			title: 'Yakin akan Menghapus ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Ya!",
			showCancelButton: true,
			cancelButtonClass: "btn-default",
			cancelButtonText: "Tidak!"
		}).then(function () {
			var datapost = {'id' : id};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>jurnal_np/delete_jurnal_temporary",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
					console.log(response);
					$('#loader-data').removeClass('x-hidden');
					$('#loader-data').addClass('x-show');
					if (response.status == "success") {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function () {
							window.location.href = "<?php echo base_url('jurnal_np');?>";
						})
					}else{
						swal("Failed!", response.message, "error");
					}
					
				}
			});
		})
	}
	
	var $loader = $(".loader");
	var $btnSuccessTest = $loader.find(".success-test");
	var $btnErrorTest = $loader.find(".error-test");
	var $btnLoadingTest = $loader.find(".loading-test");

	var $loadingMessage = $loader.find(".loader-message");
	var $btnRetry = $loader.find(".retry");
	var $btnCancel = $loader.find(".cancel");

	var errorMessage = "Unable to load data";
	var successMessage = "Congo, data loaded successfully";
	var loadingMessage = "Loading your data...";

	$btnSuccessTest.on('click', function() {
		if($loader.hasClass("error")) {
			$loader.removeClass("error");
		}
		
		$loader.addClass("success");
		$loadingMessage.text(successMessage);
		
	});

	$btnErrorTest.on('click', function() {
		if($loader.hasClass("success")) {
			$loader.removeClass("success");
		}
		
		$loader.addClass("error");
		$loadingMessage.text(errorMessage);
	});

	$btnLoadingTest.on('click', function() {
		if($loader.hasClass("success")) {
			$loader.removeClass("success");
		}
		else if($loader.hasClass("error")) {
			$loader.removeClass("error");
		}
		$loadingMessage.text(loadingMessage);
	});

	$btnRetry.on('click', function() {
		if($loader.hasClass("success")) {
			$loader.removeClass("success");
		}
		if($loader.hasClass("error")) {
			$loader.removeClass("error");
		}
		$loadingMessage.text(loadingMessage);
	});
</script>