<style>
	#loading {
		position: absolute;
		background: #FFFFFF;
		opacity: 0.5;
		width: 93%;
		height: 85%;
		z-index: 3;
		text-align: center;
		display: none;
	}
	
	.x-hidden {
		display: none;
	}
</style>
<div id="loading"><img src="<?php echo base_url(); ?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/preloader.gif" /></div>
<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
<div class="row">
	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="level1">Coa <span class="required"><sup>*</sup></span></label>
		</div>
		<div class="col-md-8">
			<select class="form-control select-control" id="level1" name="level1" style="width: 100%" required>
				<?php foreach ($coa_all as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>"><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3" id="subdetail_temp">
			<label style="margin: 10px 0 10px 10px;" for="id_valas">Valas <span class="required"><sup>*</sup></span></label>
		</div>
		<div class="col-md-8">
			<select class="form-control" id="id_valas" name="id_valas" style="width: 100%" required>
				<?php foreach ($valas as $key) { ?>
					<option value="<?php echo $key['valas_id']; ?>"><?php echo $key['nama_valas']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3"></div>
		<div class="col-md-8"></div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="rate">Rate <span class="required"><sup>*</sup></span></label>
		</div>
		<div class="col-md-8">
			<input type="number" class="form-control" id="rate" name="rate" min="1" placeholder="Rate" value="1" required>
		</div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="value">Nilai <span class="required"><sup>*</sup></span></label>
		</div>
		<div class="col-md-8">
			<input type="number" class="form-control" id="value" name="value" min="0" placeholder="Nilai" value="0" required>
		</div>
	</div>

	<div class="col-sm-12 x-hidden" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="type_cash">Type <span class="required"><sup>*</sup></span></label>
		</div>
		<div class="col-md-8">
			<select class="form-control" id="type_cash" name="type_cash" style="width: 100%" required readonly> 
				<option value="1" selected>Pengeluaran</option>
			</select>
		</div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="note">Catatan</label>
		</div>

		<div class="col-md-8">
			<textarea class="form-control" data-parsley-maxlength="255" type="text" id="note" name="note" placeholder="Catatan"></textarea>
		</div>
	</div>

	<div class="col-sm-12 x-hidden" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="file_kas_keluar">File Bukti</label>
		</div>
		<div class="col-md-8">
			<input type="file" class="form-control" id="file_kas_keluar" name="file_kas_keluar" data-height="110" accept=".pdf, .txt, .doc, .docx" />
			<span id="file_bc_tick">Hanya format file pdf,txt,doc,docx dengan besaran max 10Mb yang diterima.</span>
		</div>
	</div>

	<div class="col-sm-12" style="margin-bottom: 10px;">
		<div class="col-md-3">
			<label style="margin: 10px 0 10px 10px;" for="submit"></label>
		</div>
		<div class="col-md-8">
			<button id="btn-submit-kas" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Tambah Kredit</button>
		</div>
	</div>
</div>
<!-- <form class="form-horizontal form-label-left"> -->
	<!-- <div class="item form-group" id="level2_temp">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control select-control" id="level2" name="level2" style="width: 100%">
				<?php foreach ($level2 as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>"><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group" id="level3_temp">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control select-control" id="level3" name="level3" style="width: 100%">
				<?php foreach ($level3 as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>"><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group" id="level4_temp">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="level4" name="level4" style="width: 100%">
				<?php foreach ($level4 as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>"><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div> -->

	<!-- <div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Tanggal <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<div class="input-group date">
				<input placeholder="Tanggal" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="date" name="date" required="required" value="<?php echo date('Y-m-d'); ?>">
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
	</div> -->
<!-- </form> -->
<!-- /page content -->

<script type="text/javascript">
	// function coa(level, id) {
	// 	$('#loading').show();
	// 	$.ajax({
	// 		type: 'GET',
	// 		url: "<?php echo base_url(); ?>jurnal_np/change_coa",
	// 		data: {
	// 			id: id
	// 		},
	// 		success: function(response) {
	// 			if (response.length) {
	// 				var element = '';
	// 				$.each(response, function(key, val) {
	// 					element += '<option value="' + val.id_coa + '" data-coa="' + val.coa + '">' + val.keterangan + '</option>';
	// 				});

	// 				if (level === 'level1') {
	// 					$('#level2').html(element);
	// 					$('#level2_temp').show();

	// 					coa('level2', response[0].coa);
	// 				} else if (level === 'level2') {
	// 					$('#level3').html(element);
	// 					$('#level3_temp').show();

	// 					coa('level3', response[0].coa);
	// 				} else {
	// 					$('#level4').html(element);
	// 					$('#level4_temp').show();
	// 					$('#loading').hide();
	// 				}
	// 			} else {
	// 				if (level === 'level1') {
	// 					$('#level2_temp,#level3_temp,#level4_temp').hide();
	// 				} else if (level === 'level2') {
	// 					$('#level3_temp,#level4_temp').hide();
	// 				} else {
	// 					$('#level4_temp').hide();
	// 				}
	// 				$('#loading').hide();
	// 			}
	// 		}
	// 	});
	// }
	
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();

		/*$("#date").datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});*/

		/*$('.select-control').on('change', (function(e) {
			var id = $(this).children('option:selected').data('coa'),
				level = $(this).attr('id');

			coa(level, id);
		}));*/
		
		$('#level1').select2();
		// $('#level2').select2();
		// $('#level3').select2();
		// $('#level4').select2();
	});

	$('#btn-submit-kas').on('click', (function(e) {
		var tempArrKeluar = [];
		var jumlahKeluar = 0;
		var statusKeluar = 0;
		
		$('#btn-submit-kas').attr('disabled', 'disabled');
		$('#btn-submit-kas').text("Memasukkan data...");
		e.preventDefault();
		
		if($('#value').val() != 0 && $('#value').val() >= 0 && $('#value').val() != null) {
			lastItemKeluar++;

			var id_coa_keluar = '1',
				id_parent_keluar = $('#level1').val(),
				coa_number_keluar = $('#level1 option:selected').data("coa"),
				text_coa_keluar = $('#level1 option:selected').text();

			if ($('#level4_temp').is(':visible')) {
				id_coa_keluar = $('#level4').val();
				text_coa_keluar = $('#level4 option:selected').text();
				coa_number_keluar = $('#level4 option:selected').data("coa");
			} else if ($('#level3_temp').is(':visible')) {
				id_coa_keluar = $('#level3').val();
				text_coa_keluar = $('#level3 option:selected').text();
				coa_number_keluar = $('#level3 option:selected').data("coa");
			} else if ($('#level2_temp').is(':visible')) {
				id_coa_keluar = $('#level2').val();
				text_coa_keluar = $('#level2 option:selected').text();
				coa_number_keluar = $('#level2 option:selected').data("coa");
			} else id_coa_keluar = id_parent_keluar;
			
			var temp_kas_data_keluar = {
				id_coa_keluar: id_coa_keluar,
				text_coa_keluar: text_coa_keluar,
				coa_number_keluar: coa_number_keluar,
				id_parent_keluar: id_parent_keluar,
				//date_keluar: $('#date').val(),
				id_valas_keluar: $('#id_valas').val(),
				text_valas_keluar: $('#id_valas option:selected').text(),
				value_keluar: $('#value').val(),
				rate_keluar: $('#rate').val(),
				type_cash_keluar: 1,
				type_cash_text_keluar: 'Kredit',
				note_keluar: $('#note').val(),
			}

			/*formData.append('id_coa_keluar[]', temp_kas_data_keluar.id_coa_keluar);
			formData.append('text_coa_keluar[]', temp_kas_data_keluar.text_coa_keluar);
			formData.append('coa_number_keluar[]', temp_kas_data_keluar.coa_number_keluar);
			formData.append('id_parent_keluar[]', temp_kas_data_keluar.id_parent_keluar);
			//formData.append('date_keluar[]', temp_kas_data_keluar.date_keluar);
			formData.append('id_valas_keluar[]', temp_kas_data_keluar.id_valas_keluar);
			formData.append('value_keluar[]', temp_kas_data_keluar.value_keluar);
			formData.append('rate_keluar[]', temp_kas_data_keluar.rate_keluar);
			formData.append('type_cash_keluar[]', temp_kas_data_keluar.type_cash_keluar);
			formData.append('type_cash_text_keluar[]', temp_kas_data_keluar.type_cash_text_keluar);
			formData.append('note_keluar[]', temp_kas_data_keluar.note_keluar);
			formData.append('have_file_keluar[]', "new");

			if ($('#file_kas_keluar')[0].files.length > 0) {
				if ($('#file_kas_keluar')[0].files[0].size <= 9437184) {
					formData.append('file_kas_keluar[]', $('#file_kas_keluar')[0].files[0], $('#file_kas_keluar')[0].files[0].name);
					temp_kas_data_keluar.file = new Blob();
					temp_kas_data_keluar.file_name = $('#file_kas_keluar')[0].files[0].name;

				} else {
					$('#btn-submit-kas').removeAttr('disabled');
					$('#btn-submit-kas').text("Tambah KAS");
					swal("Failed!", "Ukuran File Terlalu besar, silahkan cek kembali", "error");
				}
			} else {
				formData.append('file_kas_keluar[]', new Blob, '');
				temp_kas_data_keluar.file = new Blob;
				temp_kas_data_keluar.file_name = '';
			}*/

			lastRowKeluar = lastItemKeluar;
			lastCoaKeluar = temp_kas_data_keluar.id_coa_keluar;
			add_kas_keluar_temp_data(temp_kas_data_keluar);
			$('#panel-modal-lv1').modal('toggle');
		}else{
			$('#btn-submit-kas').removeAttr('disabled');
			$('#btn-submit-kas').text("Tambah Kas");
			swal("Warning!", "Maaf, jumlah nilai tidak boleh 0 (nol).", "info");
		}
	}));
</script>