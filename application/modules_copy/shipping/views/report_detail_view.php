<style type="text/css">
	.row-search{
		padding: 5px;
	}
</style>

<div class="row row-search">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jenis_report">Jenis Report</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<select class="form-control" id="jenis_report" name="jenis_report" required>
			<option value="">-- Select Jenis --</option>
			<option value="pemasukan">Pemasukan Barang Per Dokumen Pabean</option>
			<option value="pengeluaran">Pengeluaran Barang Per Dokumen Pabean</option>
			<option value="wip">Posisi WIP</option>
			<option value="mutasi_bahan_baku">Pertanggungjawaban Mutasi Bahan Baku</option>
			<option value="mutasi_barang_jadi">Pertanggungjawaban Mutasi Barang Jadi</option>
			<option value="mutasi_mesin">Pertanggungjawaban Mutasi Mesin dan Peralatan</option>
			<option value="barang_reject">Pertanggungjawaban Barang Reject dan Scrap</option>
			<option value="pemusnahan_barang">Pertanggungjawaban Pemusnahan Barang</option>
		</select>
	</div>
</div>

<div class="row row-search">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tgl_report">Tanggal Masuk</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<input type="text" class="form-control" id="tanggal_masuk" name="tanggal_masuk" required>
	</div>
</div>

<div class="row row-search">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tgl_report">Tanggal Keluar</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<input type="text" class="form-control" id="tanggal_keluar" name="tanggal_keluar" required>
	</div>
</div>
<hr>
<div class="row row-search">
	<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<button id="btn-search" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Cari</button>
	</div>
</div>

<script type="text/javascript">
$(function() {
	$('#startDate').datepicker({
		maxDate: 0,
		onSelect: function(dateStr) {      
			var date = $(this).datepicker('getDate');
			if (date) {
				date.setDate(date.getDate() + 1);
			}
			$('#endDate').datepicker('option', 'minDate', date);
		}
      });
      $('#endDate').datepicker({
            maxDate: 0,
            onSelect: function (selectedDate) {
                  var date = $(this).datepicker('getDate');
                  if (date) {
                        date.setDate(date.getDate() - 1);
                  }
                  $('#startDate').datepicker('option', 'maxDate', date || 0);
            }
      });
});

	$(document).ready(function() {
		$("#tanggal_masuk").datepicker({
			autoclose: true,
			maxDate: 0,
			format: 'dd/M/yyyy',
			onSelect: function(dateStr) {      
                var date = $(this).datepicker('getDate');
                if (date) {
                    date.setDate(date.getDate() + 1);
                }
                $('#tanggal_keluar').datepicker('option', 'minDate', date);
            }
		});
        $("#tanggal_keluar").datepicker({
			autoclose: true,
			maxDate: 0,
			format: 'dd/M/yyyy',
			onSelect: function (selectedDate) {
                var date = $(this).datepicker('getDate');
                if (date) {
                    date.setDate(date.getDate() - 1);
                }
                $('#tanggal_masuk').datepicker('option', 'maxDate', date || 0);
            }
		});
		
		$('#btn-search').on('click', function () {
			$('#btn-search').attr('disabled','disabled');
			$('#btn-search').text("Loading...");
			$('#div_report').removeData('');
			$('#div_report').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');

			if($('#tanggal_masuk').val() != '' && $('#tanggal_keluar').val() != '' && $('#jenis_report').val() != '') {
				var data = {
					'tanggal_masuk'		: $('#tanggal_masuk').val(),
					'tanggal_keluar'	: $('#tanggal_keluar').val(),
					'jenis_report'		: $('#jenis_report').val()
				};
				$('#div_report').load('<?php echo base_url('report_bc_masuk/view_report/');?>', data, function(response, status, xhr) {
					if (status != "error") {
						$('#btn-search').removeAttr('disabled');
						$('#btn-search').text("Cari");
						$('#panel-modal-detail').modal('toggle');
					}else {
						$('#btn-search').removeAttr('disabled');
						$('#btn-search').text("Cari");
						$('#div_report').removeData('');
						$('#div_report').html('');
						swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
					}
				});
			}else {
				$('#btn-search').removeAttr('disabled');
				$('#btn-search').text("Cari");
				$('#div_report').removeData('');
				$('#div_report').html('');
				swal("Failed!", "Invalid inputan, silahkan cek kembali.", "error");
			}
		})
	});
</script>