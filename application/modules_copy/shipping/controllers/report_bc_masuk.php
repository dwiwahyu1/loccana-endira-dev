<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Report_Bc_Masuk extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('report_bc_masuk/Report_BC_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'report_bc_masuk/views/index');
	}

	public function report_detail() {
		$this->load->view('report_detail_view');
	}

	public function view_report() {
		$jenis_report = $this->Anti_sql_injection($this->input->post('jenis_report', TRUE));
		$tanggalMasuk = $this->Anti_sql_injection($this->input->post('tanggal_masuk', TRUE));
		$tanggalKeluar = $this->Anti_sql_injection($this->input->post('tanggal_keluar', TRUE));
		
		$tglMsk = explode("/", $tanggalMasuk);
		$tglKel = explode("/", $tanggalKeluar);
		$tanggal_masuk = date('Y-m-d', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0]));
		$tanggal_keluar = date('Y-m-d', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0]));

		if($jenis_report == 'pemasukan') {
			//PABEAN
			$pemasukanPabean = array('tanggal_masuk' => $tanggal_masuk, 'tanggal_keluar' => $tanggal_keluar);
			$result_pabean = $this->Report_BC_model->get_report_pemasukan_pabean($pemasukanPabean);
			
			$data = array(
				'report'			=> 'Pemasukan Barang Per Dokumen Pabean',
				'tanggal_masuk'		=> date('d-F-Y', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0])),
				'tanggal_keluar'	=> date('d-F-Y', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0])),
				'pabean'			=> $result_pabean
			);
				
			if(count($result_pabean) > 0){
				$msg = "Lihat report";
				
				$data['msg'] = array(
					'message'	=> 0
				);
		
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}else{
				$msg = "Gagal melihat report";
				
				$data['msg'] = array(
					'message'	=> 1
				);
		
				$this->log_activity->insert_activity('error', $msg. ' dengan jenis report ' .$jenis_report);
			}
			
			$this->load->view('report_pabean_view', $data);
		}else if($jenis_report == 'mutasi_bahan_baku') {
			//MUTASI BAHAN BAKU
			$mutasiBahbak = array('tanggal_masuk' => $tanggal_masuk, 'tanggal_keluar' => $tanggal_keluar);
			$result_mutasi_bahbak = $this->Report_BC_model->get_report_mutasi_bahan_baku($mutasiBahbak);
			
			$data = array(
				'report'				=> 'Pertanggungjawaban Mutasi Bahan Baku',
				'tanggal_masuk'			=> date('d-F-Y', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0])),
				'tanggal_keluar'		=> date('d-F-Y', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0])),
				'mutasi'				=> $result_mutasi_bahbak
			);
				
			if(count($result_mutasi_bahbak) > 0){
				$msg = "Lihat report";
				
				$data['msg'] = array(
					'message'	=> 0
				);
		
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}else{
				$msg = "Gagal melihat report";
				
				$data['msg'] = array(
					'message'	=> 1
				);
		
				$this->log_activity->insert_activity('error', $msg. ' dengan jenis report ' .$jenis_report);
			}
			
			$this->load->view('report_pertanggungjawaban_view', $data);
		}else if($jenis_report == 'mutasi_barang_jadi') {
			//MUTASI BARANG JADI
			$mutasiBarJad = array('tanggal_masuk' => $tanggal_masuk, 'tanggal_keluar' => $tanggal_keluar);
			$result_mutasi_barjad = $this->Report_BC_model->get_report_mutasi_barang_jadi($mutasiBarJad);
			
			$data = array(
				'message'				=> 0,
				'report'				=> 'Pertanggungjawaban Mutasi Barang Jadi',
				'tanggal_masuk'			=> date('d-F-Y', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0])),
				'tanggal_keluar'		=> date('d-F-Y', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0])),
				'mutasi'				=> $result_mutasi_barjad
			);
			
			if(count($result_mutasi_barjad) > 0){
				$msg = "Lihat report";
				
				$data['msg'] = array(
					'message'	=> 0
				);
		
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}else{
				$msg = "Gagal melihat report";
				
				$data['msg'] = array(
					'message'	=> 1
				);
		
				$this->log_activity->insert_activity('error', $msg. ' dengan jenis report ' .$jenis_report);
			}
			//echo "<pre>"; print_r($data);die;
			$this->load->view('report_pertanggungjawaban_view', $data);
		}else if($jenis_report == 'mutasi_mesin') {
			//MUTASI MESIN
			$mutasiMesin = array('tanggal_masuk' => $tanggal_masuk, 'tanggal_keluar' => $tanggal_keluar);
			$result_mutasi_mesin = $this->Report_BC_model->get_report_mutasi_mesin($mutasiMesin);
			
			$data = array(
				'message'				=> 0,
				'report'				=> 'Pertanggungjawaban Mutasi Mesin dan Peralatan',
				'tanggal_masuk'			=> date('d-F-Y', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0])),
				'tanggal_keluar'		=> date('d-F-Y', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0])),
				'mutasi'				=> $result_mutasi_mesin
			);
				
			if(count($result_mutasi_mesin) > 0){
				$msg = "Lihat report";
				
				$data['msg'] = array(
					'message'	=> 0
				);
		
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}else{
				$msg = "Gagal melihat report";
				
				$data['msg'] = array(
					'message'	=> 1
				);
		
				$this->log_activity->insert_activity('error', $msg. ' dengan jenis report ' .$jenis_report);
			}
			
			$this->load->view('report_pertanggungjawaban_view', $data);
		}else if($jenis_report == 'barang_reject') {
			//MUTASI BARANG REJECT DAN SCRAP
			$mutasiBarRej = array('tanggal_masuk' => $tanggal_masuk, 'tanggal_keluar' => $tanggal_keluar);
			$result_mutasi_BarRej = $this->Report_BC_model->get_report_barang_reject($mutasiBarRej);
			
			$data = array(
				'report'				=> 'Pertanggungjawaban Mutasi Barang Reject dan Scrap',
				'tanggal_masuk'			=> date('d-F-Y', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0])),
				'tanggal_keluar'		=> date('d-F-Y', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0])),
				'mutasi'				=> $result_mutasi_BarRej
			);
			
			if(count($result_mutasi_BarRej) > 0){
				$msg = "Lihat report";
				
				$data['msg'] = array(
					'message'	=> 0
				);
		
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}else{
				$msg = "Gagal melihat report";
				
				$data['msg'] = array(
					'message'	=> 1
				);
		
				$this->log_activity->insert_activity('error', $msg. ' dengan jenis report ' .$jenis_report);
			}
			
			$this->load->view('report_pertanggungjawaban_view', $data);
		}else {
			$msg = "Gagal melihat report bc masuk";
			
			$results = array(
				'success' => false,
			);
			
			$this->log_activity->insert_activity('error', $msg);
			
			$this->output->set_content_type('application/json')->set_output(json_encode($results));
		}
	}
}