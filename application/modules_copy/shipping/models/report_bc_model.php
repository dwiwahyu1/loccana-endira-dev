<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Report_BC_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function get_report($data) {
		$sql 	= 'CALL report_finance(?,?)';

		$query 	=  $this->db->query($sql, array(
			// $data['start_date'],
			// $data['end_date'],
			$data['date_search'],
			$data['report_finance']
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function get_report_pemasukan_pabean($data) {
		$sql 	= 'CALL report_masuk_barang(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['tanggal_masuk'],
			$data['tanggal_keluar']
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function get_report_mutasi_bahan_baku($data) {
		$sql 	= 'CALL report_bahan_material(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['tanggal_masuk'],
			$data['tanggal_keluar']
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function get_report_mutasi_barang_jadi($data) {
		$sql 	= 'CALL report_finish_good(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['tanggal_masuk'],
			$data['tanggal_keluar']
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function get_report_mutasi_mesin($data) {
		$sql 	= 'CALL report_mesin_produksi(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['tanggal_masuk'],
			$data['tanggal_keluar']
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function get_report_barang_reject($data) {
		$sql 	= 'CALL report_scrap(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['tanggal_masuk'],
			$data['tanggal_keluar']
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
}