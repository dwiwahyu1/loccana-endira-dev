<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule_production extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('schedule_production/bom_model');
		$this->load->model('quotation/quotation_model');
		$this->load->model('material/material_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
    * This function is redirect to index bom page
    * @return Void
    */
	public function index() {
		$this->template->load('maintemplate', 'schedule_production/views/index');
	}
	
	public function index_schd() {
		
		$this->load->helper('url');
		$po = $this->uri->segment(3);
		$m_id = $this->uri->segment(4);
		$detail_sch = $this->bom_model->detail_sch($po,$m_id);
		
		
		
		//print_r($detail_sch);die;
		
		 $data = array(
          'po' => $po,
          'm_id' => $m_id,
		  'detail_sch' => $detail_sch
      );
		
		$this->template->load('maintemplate', 'schedule_production/views/index_chd',$data);
	}

		public function index_schd_layout() {
		
		$this->load->helper('url');
		$po = $this->uri->segment(3);
		$m_id = $this->uri->segment(4);
		$detail_sch = $this->bom_model->detail_sch($po,$m_id);
		
		//print_r($detail_sch);die;
		
		 $data = array(
          'po' => $po,
          'm_id' => $m_id,
		  'detail_sch' => $detail_sch
      );
		
		$this->template->load('maintemplate', 'schedule_production/views/index_chd_layout',$data);
	}

	public function upload($data) {
        $this->load->library('upload');
        $config = array(
            'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/layout",
            'upload_url' => base_url() . "uploads/layout",
            'encrypt_name' => TRUE,
            'overwrite' => FALSE,
            'allowed_types' => 'jpg|jpeg|png',
            'max_size' => '10000'
        );

        $this->upload->initialize($config);

        $this->upload->do_upload($data);
        // General result data
        $result = $this->upload->data();

        // Load resize library
        $this->load->library('image_lib');

        // Resizing parameters large
        $resize = array
            (
            'source_image' => $result['full_path'],
            'new_image' => $result['full_path'],
            'maintain_ratio' => TRUE,
            'width' => 300,
            'height' => 300
        );

        // Do resize
        $this->image_lib->initialize($resize);
        $this->image_lib->resize();
        $this->image_lib->clear();

        // Add our stuff
        $img = $result['file_name'];
            
        return $img;
    }

	/**
    * This function is used for showing bom list
    * @return Array
    */
	function lists() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;
		$order_fields = array('', 'DATE_FORMAT(`issue_date`,"%Y-%m-%d")', 'id');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);
		$cust_id = $this->input->get_post('cust_id');


		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;
		$params['cust_id'] = $cust_id;
		

		$list = $this->bom_model->lists($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["customer"] = $this->quotation_model->customer();
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$actions = '<div class="btn-group">';
            $actions .= '   <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Schedule" onClick="edit_schd(\'' . $v['id'] .'\',\'' . $v['m_id'] .'\')">';
            $actions .= '       <i class="fa fa-calendar"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';
			
			$actions .= '<div class="btn-group">'; 
			//$actions .= '   <button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Create Schedule" onClick="edit_bom(\'' . $v['m_id'] .'\')">';
			$actions .= '   <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Schedule" onClick="edit_schd_layout(\'' . $v['id'] .'\',\'' . $v['m_id'] .'\')">';
            $actions .= '       <i class="fa fa-edit"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';

			array_push($data, array(
				$i,
				$v['no_po'],
				$v['stock_code'],
				$v['name_eksternal'],
				$v['stock_name'], 
				$v['total_layout'],
				$v['status'],
				$actions
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	function lists2() {
		
		
		
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;
		$order_fields = array('issue_date', 'req_no', 'stock_name');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);
		$cust_id = $this->input->get_post('cust_id');
		$po = $this->input->get_post('po');
		$m_id = $this->input->get_post('m_id');

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;
		$params['cust_id'] = $cust_id;
		$params['po'] = $po;
		$params['m_id'] = $m_id;

		//print_r($params);die;
		$list = $this->bom_model->lists2($params);

		//print_r($list);die;

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["customer"] = $this->quotation_model->customer();
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		
		$current_panel = 0;
		$current_panel_tot = 0;
		
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			
			if($v['status'] == 'Approve'){ 
				$actions = '';
			}elseif($v['status'] == 'Reject'){
				$actions = '<div class="btn-group">';
				$actions .= '   <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edit_schd_d(\'' . $v['id_prod'] .'\')">';
				$actions .= '       <i class="fa fa-edit"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
			}else{
			
			$actions = '<div class="btn-group">';
            $actions .= '   <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edit_schd_d(\'' . $v['id_prod'] .'\')">';
            $actions .= '       <i class="fa fa-edit"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';
			
			$actions .= '<div class="btn-group">';
            $actions .= '   <button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Approve Schedule" onClick="app_sch(\'' . $v['id_prod'] .'\')">';
            $actions .= '       <i class="fa fa-check"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';
			
			$actions .= '<div class="btn-group">';
          //  $actions .= '   <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete Schedule" onClick="del_sch(\'' . $v['id_prod'] .'\')">';
            $actions .= '   <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete Schedule" onClick="rej_schd(\'' . $v['id_prod'] .'\')">';
            $actions .= '       <i class="fa fa-trash"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';
			
			}
			
			$tk = explode('/',$v['ticket']);
			$tk2 = explode(' ',$tk[1]);
			$t_panel = $tk2[1];
			
			// if($current_panel == 0 ){
				// $total_sheet = floor($t_panel/$v['sheet_panel']);
			// }else{
			
				// if($current_panel == floor($t_panel/$v['sheet_panel']) ){
					// $total_sheet = floor($t_panel/$v['sheet_panel']);
				// }else{
					// $total_sheet = $v['qty_sheet'] - $current_panel_tot ; 
				// }
			
			// }
			// $current_panel = $total_sheet;
			
			// $current_panel_tot = $current_panel_tot + $total_sheet;
			
			
			// $total_panel = $v['sheet_panel']*$total_sheet;
			//print_r($tk2[1]);die;

			array_push($data, array(
				$i,
				$v['no_po'],
				$v['date_issue'],
				$v['lot_number'],
				//$v['qty']." PCS / ".$v['sheet_panel']*$v['qty_sheet']." PNL (".$v['pcs_ary_panel']." UP) / ".$v['qty_sheet']." SHT",
				//$v['pcs_array']*$v['panel_pcs_array']*$total_panel." PCS / ".$total_panel." PNL (".$v['pcs_ary_panel']." UP) / ".$total_sheet." SHT",
				$v['rundown_qty'],
				$v['ticket'],
				$v['splits_qty'],
				$v['due_date'],
				$v['tipe'],
				$v['status'],
				$actions
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	function lists3() {
		
		
		
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;
		$order_fields = array('','pcs_ary_pnl');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);
		$cust_id = $this->input->get_post('cust_id');
		$po = $this->input->get_post('po');
		$m_id = $this->input->get_post('m_id');

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;
		$params['cust_id'] = $cust_id;
		$params['po'] = $po;
		$params['m_id'] = $m_id;

		//print_r($params);die;
		$list = $this->bom_model->lists3($params);

		//print_r($list);die;

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["customer"] = $this->quotation_model->customer();
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			
			
			// $actions = '<div class="btn-group">';
            // $actions .= '   <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edit_schd_d(\'' . $v['id_prod_layout'] .'\')">';
            // $actions .= '       <i class="fa fa-edit"></i>';
            // $actions .= '   </button>';
            // $actions .= '</div>';
			
			$actions = '<div class="btn-group">';
            $actions .= '   <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="del_app_sch(\'' . $v['id_prod_layout'] .'\')">';
            $actions .= '       <i class="fa fa-trash"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';
			
			
			$layout = explode('x',$v['pcs_ary_pnl']);

			array_push($data, array(
				$i,
				$layout[0],
				$v['pcs_ary_pnl'],
				$v['panel_size'],
				$actions
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	/**
    * This function is redirect to edit bom page
    * @return Void
    */
  public function edit($id) {
	  
	  //print_r($id);die;
	  
      $item = $this->bom_model->item(3);
      $mainitem = $this->bom_model->mainitem(1);
      $detail = $this->bom_model->detail($id);
      $detail_main = $this->bom_model->detail_main($id);
      $detail_layout = $this->bom_model->detail_layout($id);
      $detail_esf = $this->bom_model->detail_esf_4($id);

		//print_r($detail_esf[0]['stock_code']);die;

      $data = array(
          'item' => $item,
          'mainitem' => $mainitem,
          'detail' => $detail,
		  'detail_main' => $detail_main,
		  'detail_layout' => $detail_layout,
		  'detail_esf' => $detail_esf
      );
      
      $this->load->view('edit_modal_view',$data);
  }   
  
  public function edit_schd($id) {

      $detail_schd = $this->bom_model->detail_schd($id);

		//print_r($id);die;

      $data = array(
		  'detail_sch' => $detail_schd
      );
      
      $this->load->view('edit_modal_schedule_view_update',$data);
  } 

  public function add() {
     $this->load->helper('url');
		$po = $this->uri->segment(3);
		$m_id = $this->uri->segment(4);
		$detail_sch = $this->bom_model->detail_sch($po,$m_id);
		$detail_layout = $this->bom_model->detail_layout2($po,$m_id);
		$list_po = $this->bom_model->list_po($po,$m_id);
		
		//print_r($detail_layout);die;
		
		$arr_lot_m = ['0','A','B','C','D','E','F','G','H','I','J','K','L'];
		
		$sasacas = $detail_sch[0]['seq_lot']+1;

		if($detail_sch[0]['seq_lot'] < 100){
			$sq_lot = '0'.$sasacas;
		}else{
			$sq_lot = $sasacas;
		}
		
		$lot_number = date('d').''.$arr_lot_m[(int)date('m')].''.date('y').'-'.$detail_sch[0]['lot_number'].'-'.$sq_lot;
		
		// var_dump($detail_sch,$arr_lot_m,date('m'),$lot_number);die;
		
		
      $data = array(
		  'detail_layout' => $detail_layout,
		  'detail_sch' => $detail_sch,
		  'm_id' => $m_id,
		  'po' => $po,
		  'lot_number' => $lot_number,
		  'list_po' => $list_po
      );
      
      $this->load->view('edit_modal_schedule_view',$data);
  }

  /**
    * This function is redirect to edit bom page
    * @return Void
    */
  public function detail_item($id) {
      $result = $this->material_model->edit($id);

      $data = array(
          'uom' => $result,
      );

      $this->load->view('edit_modal_material_view',$data);
  }
  
    public function delete_schd() {
        $data   	= file_get_contents("php://input");
        $params     = json_decode($data,true);

        $this->bom_model->delete_schd($params['id']);

        $msg = 'Berhasil menghapus Schedule Production';

        $result = array(
            'success' => true,
            'message' => $msg
        );
		
		$this->log_activity->insert_activity('delete', $msg. ' dengan id ' .$params['id']);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }  

	public function delete_schd_d() {
        $data   	= file_get_contents("php://input");
        $params     = json_decode($data,true);

        $this->bom_model->delete_schd_d($params['id']);

        $msg = 'Berhasil menghapus Schedule Production';

        $result = array(
            'success' => true,
            'message' => $msg
        );
		
		$this->log_activity->insert_activity('delete', $msg. ' dengan id ' .$params['id']);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }       
	
	public function get_lot() {
        $data   	= file_get_contents("php://input");
        $params     = json_decode($data,true);

		//print_r($param);die;
		$list = $this->bom_model->get_lot($params['id']);
        //$this->bom_model->delete_schd($params['id']);

		

      		foreach ($list as $k => $v) {

				array_push($data, array(
					$v['id'],
					$v['no_spb'],
					$v['stock_name'],
					$v['uom_name'],
					number_format((int)$v['qty'],0,'',''),
					$strUnitPrice,
					$strDiskon,
					$strAmount,
					$strRemark,
					$strOption
				));
				$i++;
			}
		
		$res = array('status' => 'success', 'data' => $data);

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
    }   

	public function app_sch() {
        $data   	= file_get_contents("php://input");
        $params     = json_decode($data,true);

        $this->bom_model->app_schd($params['id']);

        $msg = 'Berhasil Menyetujui Schedule Production';

        $result = array(
            'success' => true,
            'message' => $msg
        );
		
		$this->log_activity->insert_activity('Approve', $msg. ' dengan id ' .$params['id']);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	public function rej_schd() {
        $data   	= file_get_contents("php://input");
        $params     = json_decode($data,true);

        $this->bom_model->rej_schd($params['id']);

        $msg = 'Berhasil menghapus Schedule Production';

        $result = array(
            'success' => true,
            'message' => $msg
        );
		
		$this->log_activity->insert_activity('delete', $msg. ' dengan id ' .$params['id']);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

  /**
      * This function is used to edit bom data
      * @return Array
      */
    public function add_schd_list() {
		
		$data['date_issue'] = $this->Anti_sql_injection($this->input->post('date_issue', TRUE));
		$data['tipe'] = $this->Anti_sql_injection($this->input->post('tipe', TRUE));
		$data['no_po'] = $this->Anti_sql_injection($this->input->post('no_po', TRUE));
		$data['lot_number'] = $this->Anti_sql_injection($this->input->post('lot_number', TRUE));
        $data['po'] = $this->Anti_sql_injection($this->input->post('po', TRUE));
        $data['m_id'] = $this->Anti_sql_injection($this->input->post('m_id', TRUE));
        $data['order_qty'] = $this->Anti_sql_injection($this->input->post('order_qty', TRUE));
        $data['qty'] = $this->Anti_sql_injection($this->input->post('qty', TRUE));
        $data['id_prod_l'] = $this->Anti_sql_injection($this->input->post('id_prod_l', TRUE));
        $data['layout'] = $this->Anti_sql_injection($this->input->post('layout', TRUE));
        $data['split'] = $this->Anti_sql_injection($this->input->post('split', TRUE));
        $data['order_array'] = $this->Anti_sql_injection($this->input->post('order_array', TRUE));
        $data['panel_ss'] = $this->Anti_sql_injection($this->input->post('panel_ss', TRUE));
       	
		if($this->Anti_sql_injection($this->input->post('type', TRUE)) == "ASAP"){
			$data['type'] = 'ASAP';
		}else{
			$data['type'] = $this->Anti_sql_injection($this->input->post('date', TRUE));
		}
		
		//print_r($data['id_prod_l']);die;
		
		$detail_esf = $this->bom_model->detail_esf($data['m_id'],$data['id_prod_l']);
		
		
		//print_r($detail_esf);die;  
		$ff = $data['panel_ss'];
		
		if($detail_esf[0]['pcb_type'] == 'SINGLE SIDE'){
			$int_split = 200;
		}else{
			$int_split = 50;
		}
		
		$tot_spl = 0;
		
		for($a=1;$a<=$data['split'];$a++){
			
			//echo $a;
			$ff = $ff - $int_split;
			
			$new_ff = $ff + $int_split;
			
			if($a == $data['split']){
				
				
				
				$ddsd = ($data['qty'] - $tot_spl);
				
				//echo $data['qty']."-".$tot_spl."-".$ddsd."<br>";die;
				$ticket_text = 'TOTAL '.$data['split'].' SPLIT, SPLIT '.$a.'/'.$data['split'].' '.$ddsd*$detail_esf[0]['sheet_panel'].' UP';
				
				$rnd = $ddsd*$detail_esf[0]['sheet_panel']*$detail_esf[0]['pcs_per_panel'].' PCS / '.$ddsd*$detail_esf[0]['sheet_panel'].' PNL ('.$detail_esf[0]['pcs_ary_panel'].' UP) / '.$ddsd.' SHT';
				
			}else{
				
				//echo $detail_esf[0]['sheet_panel']."<br>"; 
				
				$fsht = floor($int_split/$detail_esf[0]['sheet_panel']);
				$ticket_text = 'TOTAL '.$data['split'].' SPLIT, SPLIT '.$a.'/'.$data['split'].' '.$fsht*$detail_esf[0]['sheet_panel'].' UP';
				$tot_spl = $tot_spl + $fsht;
				
				
				
				$rnd = $fsht*$detail_esf[0]['sheet_panel']*$detail_esf[0]['pcs_per_panel'].' PCS / '.$fsht*$detail_esf[0]['sheet_panel'].' PNL ('.$detail_esf[0]['pcs_ary_panel'].' UP) / '.$fsht.' SHT';
				
			}
			
			//print_r($rnd.'<br>');
			
			
			$schedule = $this->bom_model->insert_chsd_list($ticket_text,$data,$detail_esf,$rnd);
			
			
		}
		
		//die;
		//print_r($detail_esf);die;
		//foreach($detail_esf as $dsds){
			
		//}
		
		
		//print_r($this->input->post());die;
		$msg = 'Berhasil Menambah schedule Production';

        $result = array(
            'success' => true,
            'message' => $msg
        );

		$this->log_activity->insert_activity('Insert', $msg);

		$this->load->helper('url');
		//if (condition == TRUE) {
		//   redirect('schedule_production');
		//}

       $this->output->set_content_type('application/json')->set_output(json_encode($result));
		
	}    
	
	public function update_schd_list() {
		
		$data['id_schd'] = $this->Anti_sql_injection($this->input->post('id_schd', TRUE));
		$data['date_issue'] = $this->Anti_sql_injection($this->input->post('date_issue', TRUE));
		$data['tipe'] = $this->Anti_sql_injection($this->input->post('tipe', TRUE));
		$data['lot_number'] = $this->Anti_sql_injection($this->input->post('lot_number', TRUE));
        $data['po'] = $this->Anti_sql_injection($this->input->post('po', TRUE));
        $data['m_id'] = $this->Anti_sql_injection($this->input->post('m_id', TRUE));
        $data['order_qty'] = $this->Anti_sql_injection($this->input->post('order_qty', TRUE));
        $data['qty'] = $this->Anti_sql_injection($this->input->post('qty', TRUE));
        $data['order_array'] = $this->Anti_sql_injection($this->input->post('order_array', TRUE));
		
		if($this->Anti_sql_injection($this->input->post('type', TRUE)) == "ASAP"){
			$data['type'] = 'ASAP';
		}else{
			$data['type'] = $this->Anti_sql_injection($this->input->post('date', TRUE));
		}
		
        
		//$data['date'] = 
		
		//print_r($data);die;
		
		$schedule = $this->bom_model->update_chsd_list($data);
		//print_r($this->input->post());die;
		$msg = 'Berhasil Menambah schedule Production';

        $result = array(
            'success' => true,
            'message' => $msg
        );

		$this->log_activity->insert_activity('Insert', $msg);

		$this->load->helper('url');
		//if (condition == TRUE) {
		//   redirect('schedule_production');
		//}

       $this->output->set_content_type('application/json')->set_output(json_encode($result));
		
	}
	
    public function edit_bom() {
        // $data   = file_get_contents("php://input");
        // $params = json_decode($data,true);
		 
		$data['id_esf'] = $this->Anti_sql_injection($this->input->post('id_esf', TRUE));
		$data['bom_no'] = $this->Anti_sql_injection($this->input->post('bom_no', TRUE));
		$data['layout'] = $this->Anti_sql_injection($this->input->post('layout', TRUE));
		$data['tipe_esf'] = $this->Anti_sql_injection($this->input->post('tipe_esf', TRUE));
		// $data['sheet_size'] = $this->Anti_sql_injection($this->input->post('sheet_size', TRUE));
        // $data['panel_size'] = $this->Anti_sql_injection($this->input->post('panel_size', TRUE));
        // $data['sheet_panel'] = $this->Anti_sql_injection($this->input->post('sheet_panel', TRUE));
        // $data['pcs_ary_pnl'] = $this->Anti_sql_injection($this->input->post('pcs_ary_pnl', TRUE));
        // $data['bds_ary_pnl'] = $this->Anti_sql_injection($this->input->post('bds_ary_pnl', TRUE));
        $data['remark'] = $this->Anti_sql_injection($this->input->post('remark', TRUE));
		// $data['board_layout'] = null;
		// $data['cutting_map'] = null;
        
		if($data['tipe_esf'] == 'SINGLE SIDE'){
		
			$data['instruction_1'] = $this->Anti_sql_injection($this->input->post('instruction_1', TRUE));
			$data['instruction_2'] = $this->Anti_sql_injection($this->input->post('instruction_2', TRUE));
			$data['instruction_3'] = $this->Anti_sql_injection($this->input->post('instruction_3', TRUE));
			$data['instruction_4'] = $this->Anti_sql_injection($this->input->post('instruction_4', TRUE));
			$data['instruction_5'] = $this->Anti_sql_injection($this->input->post('instruction_5', TRUE));
			$data['instruction_6'] = $this->Anti_sql_injection($this->input->post('instruction_6', TRUE));
			$data['instruction_7'] = $this->Anti_sql_injection($this->input->post('instruction_7', TRUE));
			$data['instruction_8'] = $this->Anti_sql_injection($this->input->post('instruction_8', TRUE));
			$data['instruction_9'] = $this->Anti_sql_injection($this->input->post('instruction_9', TRUE));
			$data['instruction_10'] = $this->Anti_sql_injection($this->input->post('instruction_10', TRUE));
			$data['instruction_11'] = $this->Anti_sql_injection($this->input->post('instruction_11', TRUE));
			$data['instruction_12'] = $this->Anti_sql_injection($this->input->post('instruction_12', TRUE));
			$data['instruction_13'] = $this->Anti_sql_injection($this->input->post('instruction_13', TRUE));
			$data['instruction_14'] = NULL;
			$data['instruction_15'] = $this->Anti_sql_injection($this->input->post('instruction_15', TRUE));
			$data['instruction_16'] = $this->Anti_sql_injection($this->input->post('instruction_16', TRUE));
			$data['instruction_17'] = $this->Anti_sql_injection($this->input->post('instruction_17', TRUE));
			$data['instruction_18'] = $this->Anti_sql_injection($this->input->post('instruction_18', TRUE));
			$data['instruction_19'] = $this->Anti_sql_injection($this->input->post('instruction_19', TRUE));
			$data['instruction_20'] = $this->Anti_sql_injection($this->input->post('instruction_20', TRUE));
			$data['instruction_21'] = $this->Anti_sql_injection($this->input->post('instruction_21', TRUE));
			$data['instruction_22'] = $this->Anti_sql_injection($this->input->post('instruction_22', TRUE));
			$data['instruction_23'] = $this->Anti_sql_injection($this->input->post('instruction_23', TRUE));
			$data['instruction_50'] = $this->Anti_sql_injection($this->input->post('instruction_50', TRUE));
			$data['instruction_51'] = $this->Anti_sql_injection($this->input->post('instruction_51', TRUE));
			$data['instruction_52'] = $this->Anti_sql_injection($this->input->post('instruction_52', TRUE));
			
			$data['status_1'] = $this->Anti_sql_injection($this->input->post('status_1', TRUE));
			$data['status_2'] = $this->Anti_sql_injection($this->input->post('status_2', TRUE));
			$data['status_3'] = $this->Anti_sql_injection($this->input->post('status_3', TRUE));
			$data['status_4'] = $this->Anti_sql_injection($this->input->post('status_4', TRUE));
			$data['status_5'] = $this->Anti_sql_injection($this->input->post('status_5', TRUE));
			$data['status_6'] = $this->Anti_sql_injection($this->input->post('status_6', TRUE));
			$data['status_7'] = $this->Anti_sql_injection($this->input->post('status_7', TRUE));
			$data['status_8'] = $this->Anti_sql_injection($this->input->post('status_8', TRUE));
			$data['status_9'] = $this->Anti_sql_injection($this->input->post('status_9', TRUE));
			$data['status_10'] = $this->Anti_sql_injection($this->input->post('status_10', TRUE));
			$data['status_11'] = $this->Anti_sql_injection($this->input->post('status_11', TRUE));
			$data['status_12'] = $this->Anti_sql_injection($this->input->post('status_12', TRUE));
			$data['status_13'] = $this->Anti_sql_injection($this->input->post('status_13', TRUE));
			$data['status_14'] = $this->Anti_sql_injection($this->input->post('status_14', TRUE));
			$data['status_15'] = $this->Anti_sql_injection($this->input->post('status_15', TRUE));
			$data['status_16'] = $this->Anti_sql_injection($this->input->post('status_16', TRUE));
			$data['status_17'] = $this->Anti_sql_injection($this->input->post('status_17', TRUE));
			$data['status_18'] = $this->Anti_sql_injection($this->input->post('status_18', TRUE));
			$data['status_19'] = $this->Anti_sql_injection($this->input->post('status_19', TRUE));
			$data['status_20'] = $this->Anti_sql_injection($this->input->post('status_20', TRUE));
			$data['status_21'] = $this->Anti_sql_injection($this->input->post('status_21', TRUE));
			$data['status_22'] = $this->Anti_sql_injection($this->input->post('status_22', TRUE));
			$data['status_23'] = $this->Anti_sql_injection($this->input->post('status_23', TRUE));
			$data['status_50'] = $this->Anti_sql_injection($this->input->post('status_50', TRUE));
			$data['status_51'] = $this->Anti_sql_injection($this->input->post('status_51', TRUE));
			$data['status_52'] = $this->Anti_sql_injection($this->input->post('status_52', TRUE));
			
			 if ($_FILES['instruction_14']['name']) {
				$data['instruction_14'] = $this->upload('instruction_14');
			}

			// if ($_FILES['board_layout']['name']) {
				// $data['board_layout'] = $this->upload('board_layout');
			// }

			// if ($_FILES['cutting_map']['name']) {
				// $data['cutting_map'] = $this->upload('cutting_map');
			// }
			// //$esf_no = $this->Anti_sql_injection($this->input->post('esf_no', TRUE));

			// if($data['board_layout'] == ''){
				
				// $data['board_layout'] = $this->Anti_sql_injection($this->input->post('board_layout_curr', TRUE));
				
			// }
			
			// if($data['cutting_map'] == ''){
				
				// $data['cutting_map'] = $this->Anti_sql_injection($this->input->post('cutting_map_curr', TRUE));
				
			// }
			
			if($data['instruction_14'] == ''){
				
				$data['instruction_14'] = $this->Anti_sql_injection($this->input->post('instruction_14_curr', TRUE));
				
			}
			
			
			//print_r($data);die;
			
			// foreach($params[0] as $paramss){
				
				//print_r($data);die;
				
				$schedule2 = $this->bom_model->edit_bom_esf($data);  
				$schedule = $this->bom_model->edit_bom($data); 
			// }
			$array_tp = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,50,51,52];
			
			for($i=1;$i<28;$i++){ 
				
				$this->bom_model->insert_mat($data,$array_tp[$i],$schedule['lastid']);
				
			}
		
		}ELSE{
			
			//echo 'asasasa';die;
			
			$data['instruction_24'] = $this->Anti_sql_injection($this->input->post('instruction_24', TRUE));
			$data['instruction_25'] = $this->Anti_sql_injection($this->input->post('instruction_25', TRUE));
			$data['instruction_26'] = $this->Anti_sql_injection($this->input->post('instruction_26', TRUE));
			$data['instruction_27'] = $this->Anti_sql_injection($this->input->post('instruction_27', TRUE));
			$data['instruction_28'] = $this->Anti_sql_injection($this->input->post('instruction_28', TRUE));
			$data['instruction_29'] = $this->Anti_sql_injection($this->input->post('instruction_29', TRUE));
			$data['instruction_30'] = $this->Anti_sql_injection($this->input->post('instruction_30', TRUE));
			$data['instruction_31'] = $this->Anti_sql_injection($this->input->post('instruction_31', TRUE));
			$data['instruction_32'] = $this->Anti_sql_injection($this->input->post('instruction_32', TRUE));
			$data['instruction_33'] = $this->Anti_sql_injection($this->input->post('instruction_33', TRUE));
			$data['instruction_34'] = $this->Anti_sql_injection($this->input->post('instruction_34', TRUE));
			$data['instruction_35'] = $this->Anti_sql_injection($this->input->post('instruction_35', TRUE));
			$data['instruction_36'] = $this->Anti_sql_injection($this->input->post('instruction_36', TRUE));
			$data['instruction_37'] = $this->Anti_sql_injection($this->input->post('instruction_37', TRUE));
			$data['instruction_38'] = $this->Anti_sql_injection($this->input->post('instruction_38', TRUE));
			$data['instruction_39'] = $this->Anti_sql_injection($this->input->post('instruction_39', TRUE));
			$data['instruction_40'] = $this->Anti_sql_injection($this->input->post('instruction_40', TRUE));
			$data['instruction_41'] = $this->Anti_sql_injection($this->input->post('instruction_41', TRUE));
			$data['instruction_42'] = $this->Anti_sql_injection($this->input->post('instruction_42', TRUE));
			$data['instruction_43'] = NULL;
			$data['instruction_44'] = $this->Anti_sql_injection($this->input->post('instruction_44', TRUE));
			$data['instruction_45'] = $this->Anti_sql_injection($this->input->post('instruction_45', TRUE));
			$data['instruction_46'] = $this->Anti_sql_injection($this->input->post('instruction_46', TRUE));
			$data['instruction_47'] = $this->Anti_sql_injection($this->input->post('instruction_47', TRUE));
			$data['instruction_48'] = $this->Anti_sql_injection($this->input->post('instruction_48', TRUE));
			$data['instruction_49'] = $this->Anti_sql_injection($this->input->post('instruction_49', TRUE));
			
			$data['status_24'] = $this->Anti_sql_injection($this->input->post('status_24', TRUE));
			$data['status_25'] = $this->Anti_sql_injection($this->input->post('status_25', TRUE));
			$data['status_26'] = $this->Anti_sql_injection($this->input->post('status_26', TRUE));
			$data['status_27'] = $this->Anti_sql_injection($this->input->post('status_27', TRUE));
			$data['status_28'] = $this->Anti_sql_injection($this->input->post('status_28', TRUE));
			$data['status_29'] = $this->Anti_sql_injection($this->input->post('status_29', TRUE));
			$data['status_30'] = $this->Anti_sql_injection($this->input->post('status_30', TRUE));
			$data['status_31'] = $this->Anti_sql_injection($this->input->post('status_31', TRUE));
			$data['status_32'] = $this->Anti_sql_injection($this->input->post('status_32', TRUE));
			$data['status_33'] = $this->Anti_sql_injection($this->input->post('status_33', TRUE));
			$data['status_34'] = $this->Anti_sql_injection($this->input->post('status_34', TRUE));
			$data['status_35'] = $this->Anti_sql_injection($this->input->post('status_35', TRUE));
			$data['status_36'] = $this->Anti_sql_injection($this->input->post('status_36', TRUE));
			$data['status_37'] = $this->Anti_sql_injection($this->input->post('status_37', TRUE));
			$data['status_38'] = $this->Anti_sql_injection($this->input->post('status_38', TRUE));
			$data['status_39'] = $this->Anti_sql_injection($this->input->post('status_39', TRUE));
			$data['status_40'] = $this->Anti_sql_injection($this->input->post('status_40', TRUE));
			$data['status_41'] = $this->Anti_sql_injection($this->input->post('status_41', TRUE));
			$data['status_42'] = $this->Anti_sql_injection($this->input->post('status_42', TRUE));
			$data['status_43'] = NULL;
			$data['status_44'] = $this->Anti_sql_injection($this->input->post('status_44', TRUE));
			$data['status_45'] = $this->Anti_sql_injection($this->input->post('status_45', TRUE));
			$data['status_46'] = $this->Anti_sql_injection($this->input->post('status_46', TRUE));
			$data['status_47'] = $this->Anti_sql_injection($this->input->post('status_47', TRUE));
			$data['status_48'] = $this->Anti_sql_injection($this->input->post('status_48', TRUE));
			$data['status_49'] = $this->Anti_sql_injection($this->input->post('status_49', TRUE));
			
			 if ($_FILES['instruction_43']['name']) {
				$data['instruction_43'] = $this->upload('instruction_43');
			}

			// if ($_FILES['board_layout']['name']) {
				// $data['board_layout'] = $this->upload('board_layout');
			// }

			// if ($_FILES['cutting_map']['name']) {
				// $data['cutting_map'] = $this->upload('cutting_map');
			// }
			// //$esf_no = $this->Anti_sql_injection($this->input->post('esf_no', TRUE));

			// if($data['board_layout'] == ''){
				
				// $data['board_layout'] = $this->Anti_sql_injection($this->input->post('board_layout_curr', TRUE));
				
			// }
			
			// if($data['cutting_map'] == ''){
				
				// $data['cutting_map'] = $this->Anti_sql_injection($this->input->post('cutting_map_curr', TRUE));
				
			// }
			
			if($data['instruction_43'] == ''){
				
				$data['instruction_43'] = $this->Anti_sql_injection($this->input->post('instruction_43_curr', TRUE));
				
			}
			
			
			//print_r($data);die;
			
			// foreach($params[0] as $paramss){
				
				//print_r($data);die;
				
				$schedule2 = $this->bom_model->edit_bom_esf($data);  
				$schedule = $this->bom_model->edit_bom($data); 
			// }
			$array_tp = [0,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49];
			
			for($i=1;$i<27;$i++){
				
				$this->bom_model->insert_mat($data,$array_tp[$i],$schedule['lastid']);
				
			}

		}
		
        $msg = 'Berhasil mengubah schedule Production';

        $result = array(
            'success' => true,
            'message' => $msg
        );

		$this->log_activity->insert_activity('update', $msg);

		$this->load->helper('url');
		//if (condition == TRUE) {
		//   redirect('schedule_production');
		//}

       $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
}