<style>
  .changed_status{cursor:pointer;text-decoration: underline;color:#96b6e8;}
  .changed_status:hover{color:#ff8c00}
</style>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Quotation</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<table id="listquotation" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No Request</th>
							<th>Customer</th>
							<th>Issue Date</th>
							<th>Total Items</th>
							<th>Total Normal price</th>
							<th>Total Discount Price</th>
							<th>Currency</th>
							<th>Term of Payment</th>
							<th>Status</th>
							<th style="width:140px;">Option</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>No Request</td>
							<td>Customer</td>
							<td>Issue Date</td>
							<td>Total Items</td>
							<td>Total Normal price</td>
							<td>Total Discount price</td>
							<td>Currency</td>
							<td>Term of Payment</td>
							<td>Status</td>
							<td>Option</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:800px;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:900px;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="panel-modalchild" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <p></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$(document).ready(function(){
		listquotation();
	});

	function listquotation(){
      $("#listquotation").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'quotation/lists/';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false,
			"destroy": true,
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip',
			"initComplete": function(){
				// var element = '<div class="btn-group pull-left">';
					// element += '	<a class="btn btn-primary" onClick="add_quotation()">';
					// element += '		<i class="fa fa-plus"></i> Add Quotation';
					// element += '	</a>';
					// element += '</div>';
var element = '';
				$("div.toolbar").prepend(element);
			}
		});
    }

	function add_quotation(){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('quotation/add');?>');
		$('#panel-modal  .panel-title').html('<i class="fa fa-plus"></i> Add Quotation');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function edit_quotation(id){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('quotation/edit/');?>'+"/"+id);
		$('#panel-modal  .panel-title').html('<i class="fa fa-at"></i> Edit Quotation');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}
	
	function ro_quotation(id){
		$('#panel-modal2').removeData('bs.modal');
		$('#panel-modal2  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal2  .panel-body').load('<?php echo base_url('quotation/ro/');?>'+"/"+id);
		$('#panel-modal2  .panel-title').html('<i class="fa fa-at"></i> Repeat Order Quotation');
		$('#panel-modal2').modal({backdrop:'static',keyboard:false},'show');
	}

	function detail_quotation(id){
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .panel-body').load('<?php echo base_url('quotation/detail');?>'+"/"+id);
        $('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Detail Quotation');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
  	}

  	function approval_quotation(id){
	    $('#panel-modal').removeData('bs.modal');
	    $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	    $('#panel-modal  .panel-body').load('<?php echo base_url('quotation/edit_approval');?>'+"/"+id);
	    $('#panel-modal  .panel-title').html('<i class="fa fa-pencil"></i> Edit Status');
	    $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function detail_status(id){
	     $('#panel-modal').removeData('bs.modal');
	     $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	     $('#panel-modal  .panel-body').load('<?php echo base_url('quotation/detail_status');?>'+"/"+id);
	     $('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Detail Status');
	     $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}
	
	 function edit_schedule(id,name,price,row){
		 		 
      $('#panel-modalchild').removeData('bs.modal');
      $('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modalchild  .panel-body').load('<?php echo base_url('quotation/edit_schedule/');?>'+"/"+id+"/"+price);
      $('#panel-modalchild  .panel-title').html('<i class="fa fa-pencil"></i> Edit '+name);
      $('#panel-modalchild').modal({backdrop:'static',keyboard:false},'show');
  } 
		
	function delete_quotation(id){
		swal({
			title: 'Yakin akan Menghapus ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost={
				"id" : id
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>quotation/delete_quotation",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
				   $('.panel-heading button').trigger('click');
                    listquotation();
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                    })
				}
			});
		})
	}
	

  
</script>