<style type="text/css">
	#listedititem {
		counter-reset: rowNumber;
	}

	#listedititem tr > td:first-child {
		counter-increment: rowNumber;
	}

	#listedititem tr td:first-child::before {
		content: counter(rowNumber);
		min-width: 1em;
		margin-right: 0.5em;
	}
</style>

<form class="form-horizontal form-label-left" id="repeat_quotation" role="form" action="<?php echo base_url('quotation/repeat_quotation');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p style="text-align:center;">Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	 <div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="issue_date">Issue Date <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<div class="input-group date">
			   <input placeholder="Issue Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="issue_date" name="issue_date" required="required" value="<?php if(isset($po_detail[0]['issue_date'])){ echo date('d/M/Y', strtotime($po_detail[0]['issue_date'])); }?>">
			   
			   <input placeholder="Issue Date" type="hidden" class="form-control col-md-7 col-xs-12" id="id_quotation" name="id_quotation" value="<?php if(isset($po_detail[0]['id'])){ echo $po_detail[0]['id']; }?>">
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="cust_id">Customer <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="cust_id" name="cust_id" style="width: 100%" disabled>
				<option value="" selected>-- Select Customer --</option>
				<?php foreach($customer as $dk) { ?>
					<option value="<?php echo $dk['id']; ?>" <?php if($dk['id'] == $po_detail[0]['cust_id']) echo "selected"; ?>><?php echo $dk['kode_eksternal'].' - '.$dk['name_eksternal']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>


	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="valas_id">Valas <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="valas_id" name="valas_id" style="width: 100%" >
				<option value="" disabled>-- Select Valas --</option>
				<?php foreach($valas as $dkv) { ?>
					<option value="<?php echo $dkv['valas_id']; ?>" <?php if($dkv['valas_id'] == $po_detail[0]['valas_id']) echo "selected"; ?>><?php echo $dkv['nama_valas']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="rate">Rate <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			   <input placeholder="rate" type="number" class="form-control" id="rate" name="rate" value="<?php if(isset($po_detail[0]['rate'])){ echo $po_detail[0]['rate']; }?>" autocomplete="off" required>
		</div>
	</div>
	
		<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="term_of_payment">Term of Payment <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="term_of_payment" name="term_of_payment" style="width: 100%" required>
				<option value="" >-- Select Term --</option>
				<option value="CASH" <?php if(isset($po_detail[0]['price_term'])){ if( $po_detail[0]['price_term'] == "CASH" ){ echo "selected"; } }?>>CASH</option>
				<option value="1 Day" <?php if(isset($po_detail[0]['price_term'])){ if( $po_detail[0]['price_term'] == "1 Day" ){ echo "selected"; } }?>>1 Day</option>
				<option value="15 Days" <?php if(isset($po_detail[0]['price_term'])){ if( $po_detail[0]['price_term'] == "15 Days" ){ echo "selected"; } }?>>15 Days</option>
				<option value="60 Days" <?php if(isset($po_detail[0]['price_term'])){ if( $po_detail[0]['price_term'] == "60 Days" ){ echo "selected"; } }?>>60 Days</option>
				<option value="90 Days" <?php if(isset($po_detail[0]['price_term'])){ if( $po_detail[0]['price_term'] == "90 Days" ){ echo "selected"; } }?>>90 Days</option>
				<option value="120 Days" <?php if(isset($po_detail[0]['price_term'])){ if( $po_detail[0]['price_term'] == "120 Days" ){ echo "selected"; } }?>>120 Days</option>
				<option value="180 Days" <?php if(isset($po_detail[0]['price_term'])){ if( $po_detail[0]['price_term'] == "180 Days" ){ echo "selected"; } }?>>180 Days</option>
			</select>
		</div>
	</div>
	
	<div class="item form-group">
    	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Transport
    		<span class="required">
    			<sup>*</sup>
    		</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
           	<input data-parsley-maxlength="255" type="text" id="transport" name="transport" class="form-control col-md-7 col-xs-12" placeholder="Transport" value="<?php if(isset($po_detail[0]['transport'])){ echo $po_detail[0]['transport']; }?>">
        </div>
     </div>

     <div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="packing">Packing</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="packing" name="packing" style="width: 100%">
				<option value="0" <?php if(isset($po_detail[0]['packing'])){ if( $po_detail[0]['packing'] == '0' ){ echo "selected"; } }?>>-- Select Packing --</option>
				<option value="1" <?php if(isset($po_detail[0]['packing'])){ if( $po_detail[0]['packing'] == '1' ){ echo "selected"; } }?>>Standard Packing</option>
			</select>
		</div>
		</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="term_of_pay">Term of Production <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			   <input placeholder="Production Days" type="number" class="form-control" id="term_of_pay" name="term_of_pay" value="<?php if(isset($po_detail[0]['prod_term'])){ echo $po_detail[0]['prod_term']; }?>" autocomplete="off" required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="pilih">PO <span class="required"><sup>*</sup></span></label>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="checkbox checkbox-success">
			   <input type="checkbox" class="form-control" id="check_no_po" name="check_no_po" <?php if($check_po['check_po'] == 1){ echo 'checked'; }else{ echo 'unchecked'; } ?>><label for="no_po">PO Sama</label>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12 disabled" id="isi">
			<input placeholder="No PO" type="text" class="form-control" id="no_po_isi" name="no_po_check" value="<?php if($check_po['check_po'] == 1){ echo $check_po['no_po']; }else{ echo ''; } ?>" autocomplete="off" <?php if($check_po['check_po'] == 1){ echo ''; }else{ echo 'readonly'; } ?>>
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Item : </label>
	</div>

	<div class="item form-group">
		<table id="listedititem" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th style="width: 5%;">No</th>
					<th>Items</th>
					<th>Part Number</th>
					<th>Qty</th>
					<th>Price</th>
					<th>Discount</th>
					<th>No PO</th>
					<th style="width: 5%;"><a id="btn_item_add" class="btn btn-primary"><i class="fa fa-plus"></i></a></th>
				</tr>
			</thead>
			<tbody>
		<?php
			$i=1;
			if(sizeof($order_detail) > 0) {
				foreach($order_detail as $orderkey) { ?>
					<tr id="trRowItem<?php echo $i; ?>">
						<td>
							<input type="hidden" id="id_order<?php echo $i; ?>" name="id_order[]" value="<?php
								if(isset($orderkey['id_order'])) echo $orderkey['id_order'];
							?>">
						</td>
						<td style="display:none">
							<input type="hidden" id="id_material<?php echo $i; ?>" name="id_material[]" value="<?php
								if(isset($orderkey['id_material'])) echo $orderkey['id_material'];
							?>">
						</td>
						<td>
							<select class="form-control select-control item-control" id="item_id<?php echo $i; ?>" name="item_id[]" >
								<option value="new" selected >Part Number Baru</option>
								<?php foreach($item as $key) { ?>
									<option value="<?php echo $key['id']; ?>" <?php if($key['id'] == $orderkey['id_produk']) echo "selected"; ?>><?php echo $key['stock_code'].' | '.$key['stock_name']; ?></option>
								<?php } ?>
							</select>
						</td>
						<td>
							<input type="text" class="form-control part-control" id="part_name<?php echo $i; ?>" name="part_name[]" value="<?php
								if(isset($orderkey['stock_name'])) echo $orderkey['stock_name'];
							?>" >
						</td>
						<td>
							<input type="number" class="form-control qty-control" min="0" id="qty<?php echo $i; ?>" name="qty[]" value="<?php
								if(isset($orderkey['qty'])) echo $orderkey['qty'];
							?>">
						</td>
						<td>
							<input type="text" class="form-control part-control" id="price<?php echo $i; ?>" name="price[]" value="<?php
								if(isset($orderkey['price'])) echo $orderkey['price'];
							?>" >
						</td>
						<td>
							<input type="text" class="form-control part-control" id="diskon<?php echo $i; ?>" name="diskon[]" value="<?php
								if(isset($orderkey['diskon'])) echo $orderkey['diskon'];
							?>" >
						</td>
						<td>
							<input type="text" class="form-control qty-control" id="no_po<?php echo $i; ?>" name="no_po[]" placeholder="No PO"  <?php if($check_po['check_po'] == 1){ echo 'readonly'; }else{ echo ''; } ?> value="<?php
								if(isset($orderkey['no_po'])) echo $orderkey['no_po'];
							?>">
						</td>
						<td>
							<a id="btn_item_add" class="btn btn-danger" title="Hapus" onclick="DelItem(this, '<?php echo $orderkey['id_order']; ?>', '<?php echo $orderkey['id_produk']; ?>', '<?php echo $po_detail[0]['id'] ?>')">
								<i class="fa fa-trash"></i>
							</a>
						</td>
					</tr>
		<?php
					$i++;
				}
			}else { ?>
				<tr>
					<td><input type="hidden" id="id_order<?php echo $i; ?>" name="id_order[]" value="0"></td>
					<td style="display:none"><input type="hidden" id="id_material<?php echo $i; ?>" name="id_material[]" value="0"></td>
					<td>
						<select class="form-control select-control item-control" id="item_id<?php echo $i; ?>" name="item_id[]" style="width: 240px" required>
							<option value="new" selected >Part Number Baru</option>
							<?php foreach($item as $key) { ?>
								<option value="<?php echo $key['id']; ?>"><?php echo $key['stock_code'].' | '.$key['stock_name']; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
						<input type="text" class="form-control part-control" id="part_name<?php echo $i; ?>" name="part_name[]" value="">
					</td>
					<td>
						<input type="number" class="form-control qty-control" min="0" id="qty<?php echo $i; ?>" name="qty[]" value="0">
					</td>
					<td>
						<input type="text" class="form-control qty-control" min="0" id="price<?php echo $i; ?>" name="price[]" value="0">
					</td>
					<td>
						<input type="text" class="form-control qty-control" min="0" id="diskon<?php echo $i; ?>" name="diskon[]" value="0">
					</td>
					<td>
						<input type="text" class="form-control no_po-control" id="no_po<?php echo $i; ?>" placeholder="No PO" readonly name="no_po[]" value="">
					</td>
					<td></td>
				</tr>
		<?php
			} ?>
			</tbody>
		</table>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Repeat Quotation</button>
		</div>
	</div>

	<input type="hidden" id="id_quotation" value="<?php if(isset($po_detail[0]['id'])){ echo $po_detail[0]['id']; }?>">
</form>
<!-- /page content -->

<script type="text/javascript">
	var idRowItem 		= '<?php echo $i; ?>';
	var arridPoQuot		= [];
	var arridMaterial	= [];
	var arrItemId 		= [];
	var arrPartName 	= [];
	var arrQty			= [];
	var arrPrice		= [];
	var arrDiskon		= [];
	var arrNoPO			= [];

	var items = [],
		itemsLen = "<?php echo count($order_detail)+1;?>";
	'<?php
		foreach($item as $key) {
		  $id = $key['id'];
		  $stock_name = $key['stock_name'];
  ?>'
	  var id = "<?php echo $id; ?>";
	  var stock_name = "<?php echo $stock_name; ?>";

	  var dataitem = {
		  id,
		  stock_name
	  };

	  items.push(dataitem);
  '<?php    
		}
  ?>'

	$(document).ready(function() {
		$("#issue_date").datepicker({
			format: 'dd/M/yyyy',
			autoclose: true,
			todayHighlight: true,
		});
		
		$("#cust_id").select2();
		$("#cust_id").attr('disabled', 'disabled');
		$(".select-control").select2();

		$('#btn_item_add').on('click', function() {
			var get_no_po_isi = $('#no_po_isi').val();
			var checked_po = $('input:checkbox[name=check_no_po]');
			
			var readonly = '';
			if(checked_po.is(':checked') === true){
				readonly = 'readonly';
				get_no_po = get_no_po_isi;
				
				$('input[name="no_po[]"]').each(function() {
					this.value = get_no_po_isi;
					$(this).attr('readonly','readonly');
				})
			}else{
				readonly = 'readonly';
				get_no_po = '';
				
				$('input[name="no_po[]"]').each(function() {
					this.value = $(this).val();
					$(this).removeAttr('readonly','readonly');
				})
			}
			
			idRowItem++;
			$('#listedititem tbody').append(
				'<tr id="trRowItem'+idRowItem+'">'+
					'<td><input type="hidden" id="id_order'+idRowItem+'" name="id_order[]" value="0"></td>'+
					'<td style="display:none"><input type="hidden" id="id_material'+idRowItem+'" name="id_material[]" value="0"></td>'+
					'<td>'+
						'<select class="form-control select-control item-control" id="item_id'+idRowItem+'" name="item_id[]" required>'+
							'<option value="new" selected>Part Number Baru</option>'+
						'</select>'+
					'</td>'+
					'<td>'+
						'<input type="text" class="form-control part-control" id="part_name'+idRowItem+'" name="part_name[]" value="">'+
					'</td>'+
					'<td>'+
						'<input type="number" class="form-control qty-control" min="0" id="qty'+idRowItem+'" name="qty[]" value="0">'+
					'</td>'+
					'<td>'+
						'<input type="text" class="form-control qty-control"  id="price'+idRowItem+'" name="price[]" value="0">'+
					'</td>'+
					'<td>'+
						'<input type="text" class="form-control qty-control"  id="diskon'+idRowItem+'" name="diskon[]" value="0">'+
					'</td>'+
					'<td>'+
						'<input type="text" class="form-control no_po-control" id="no_po'+idRowItem+'" name="no_po[]" '+readonly+' placeholder="No PO" value="'+get_no_po+'">'+
					'</td>'+
					'<td>'+
						'<a class="btn btn-danger" onclick="removeRow('+idRowItem+')"><i class="fa fa-minus"></i></a>'+
					'</td>'+
				'</tr>'
			);
			$("#item_id"+idRowItem).select2();
			$('#item_id'+idRowItem).attr('disabled', 'disabled');

			$('#item_id'+idRowItem).on('change', function() {
				var idItem = $(this).attr('id').replace(/[^\d]/g, "");
				if(this.value == 'new') {
					$('#part_name'+idItem).val('');
					$('#part_name'+idItem).removeAttr('disabled');
				}else {
					$('#part_name'+idItem).val('-');
					$('#part_name'+idItem).attr('disabled', 'disabled');
				}
			});

			$.ajax({
				type: "GET",
				url: "<?php echo base_url('request_order/get_item');?>",
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(r) {
					if(r.length > 0) {
						for (var i = 0; i < r.length; i++) {
							var newOption = new Option(r[i].stock_code+' | '+r[i].stock_name, r[i].id, false, false);
							$('#item_id'+idRowItem).append(newOption);
						}
						$('#item_id'+idRowItem).removeAttr('disabled');
					}else $('#item_id'+idRowItem).removeAttr('disabled');
					idRowItem++;
				}
			});
		});
		
	});

	function removeRow(rowItem) {
		$('#trRowItem'+rowItem).remove();
	}

		$('#no_po_isi').on('keyup', function(){
		var inputNoPO = $('#no_po_isi').val(); 
		$('input[name="no_po[]"]').each(function() {
			this.value = inputNoPO;
			$(this).attr('readonly','readonly');
		})
	});
	
	$('#check_no_po').on('change', function () {
		if(this.checked) {
			//alert('check');
			$('#isi').removeClass('readonly');
			$('#no_po_isi').removeAttr('readonly','readonly');
			
			$('input[name="no_po[]"]').each(function() {
				//this.value = '';
				$(this).attr('readonly','readonly');
			})
		}else{
			
			//alert('not check');
			
			var val_po = $('#no_po_isi').val();
			
			$('#isi').addClass('readonly');
			//$('#no_po_isi').val('');
			$('#no_po_isi').attr('readonly','readonly');
			$('#val_po').val();
			//$('#no_po').val('');
			$('#no_po').removeAttr('readonly','readonly');
			
			$('input[name="no_po[]"]').each(function() {
				this.value = val_po;
				$(this).removeAttr('readonly','readonly');
			})
		}
	});

	function DelItem(btnDel, idItem, idProduk, idQuot) {
		var rowParent = $(btnDel).parents('tr');

		swal({
			title: 'Yakin akan Menghapus Barang yang sudah ada ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost = {
				"id"		: parseInt(idItem),
				"idProduk"	: parseInt(idProduk),
				"idQuot"	: parseInt(idQuot)
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>request_order/delete_item",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function () {
							rowParent.remove();
						})
					}else swal("Failed!", response.message, "error");
				}
			});
		});
	}

	$('#no_po_isi').on('keyup', function(){
		var inputNoPO = $('#no_po_isi').val(); 
		$('input[name="no_po[]"]').each(function() {
			this.value = inputNoPO;
			$(this).attr('readonly','readonly');
		})
	});
			
	// $('#check_no_po').on('change', function () {
		// if(this.checked) {
			// $('#isi').removeClass('disabled');
			// $('#no_po_isi').removeAttr('readonly','readonly');
			
			// $('input[name="no_po[]"]').each(function() {
				// this.value = '';
				// $(this).attr('readonly','readonly');
			// })
		// }else{
			// $('#isi').addClass('disabled');
			// $('#no_po_isi').val('');
			// $('#no_po_isi').attr('readonly','readonly');
			
			// $('#no_po').val('');
			// $('#no_po').removeAttr('readonly','readonly');
			
			// $('input[name="no_po[]"]').each(function() {
				// this.value = '';
				// $(this).removeAttr('readonly','readonly');
			// })
		// }
	// });
	
	$('#repeat_quotation').on('submit',(function(e) {
		
		//alert("ssasa");
		
		arridPoQuot		= [];
		arridMaterial	= [];
		arrItemId 		= [];
		arrPartName		= [];
		arrQty			= [];
		arrPrice		= [];
		arrDiskon		= [];
		arrNoPO			= [];

		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();

		$('input[name="id_order[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arridPoQuot.push(this.value);
			}
		})
		
		$('input[name="id_material[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arridMaterial.push(this.value);
			}
		})

		$('select[name="item_id[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arrItemId.push(this.value);
			}
		})

		$('input[name="part_name[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arrPartName.push(this.value);
			}
		})

		$('input[name="qty[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '' && this.value > 0) arrQty.push(this.value);
			}
		})
		
		$('input[name="price[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '' && this.value > 0) arrPrice.push(this.value);
			}
		})
		
		$('input[name="diskon[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '' ) arrDiskon.push(this.value);
			}
		})
		
		$('input[name="no_po[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arrNoPO.push(this.value);
			}
		})
		console.log(arrDiskon);
		if(arrItemId.length > 0 && arrPartName.length > 0 && arrQty.length > 0 && arrNoPO.length > 0) {
			var formDatas = new FormData();
			formDatas.append('id_quotation',	$('#id_quotation').val());
			formDatas.append('issue_date',		$('#issue_date').val());
			formDatas.append('cust_id',			$('#cust_id').val());
			formDatas.append('term_of_pay',		$('#term_of_pay').val());
			formDatas.append('term_of_payment',	$('#term_of_payment').val());
			formDatas.append('transport',		$('#transport').val());
			formDatas.append('packing',			$('#packing').val());
			formDatas.append('no_po_isi',		$('#no_po_isi').val());
			formDatas.append('valas_id',		$('#valas_id').val());
			formDatas.append('rate',			$('#rate').val());
			formDatas.append('arridPoQuot',		arridPoQuot);
			formDatas.append('arridMaterial',	arridMaterial);
			formDatas.append('arrItemId',		arrItemId);
			formDatas.append('arrPartName',		arrPartName);
			formDatas.append('arrQty',			arrQty);
			formDatas.append('arrNoPO',			arrNoPO);
			formDatas.append('arrPrice',		arrPrice);	
			formDatas.append('arrDiskon',		arrDiskon);	
			
			

			for (var value of formDatas.values()) {
			   console.log(value); 
			}
			//console.log(formData); 

			$.ajax({
				type:'POST',
				url: $(this).attr('action'),
				data: formDatas,
				cache:false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function () {
							window.location.href = "<?php echo base_url('quotation');?>";
						})
					}else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Repeat Quotation");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Repeat Quotation");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}else {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Repeat Quotation");
			swal("Failed!", "Invalid Inputan Item, silahkan cek kembali.", "error");
		}
	}));
</script>
