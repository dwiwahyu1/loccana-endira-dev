<style>
  #nopendaftaran_loading-us {
    display: none
  }

  #nopendaftaran_tick {
    display: none
  }

  #nopengajuan_loading-us {
    display: none
  }

  #nopengajuan_tick {
    display: none
  }
</style>

<form class="form-horizontal form-label-left" id="edit_form" role="form" action="<?php echo base_url('bc_limbah/save_edit_bc'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
  <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="jenis_bc">Jenis BC <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <select class="form-control" id="jenis_bc" name="jenis_bc" style="width: 100%" required>
        <option value="">-- Select Jenis --</option>
        <?php
        foreach ($jenis_bc as $key) { ?>
          <option value="<?php echo $key['id']; ?>" <?php
                                                    if (isset($bc[0]['jenis_bc'])) {
                                                      if ($bc[0]['jenis_bc'] == $key['id']) {
                                                        echo "selected";
                                                      }
                                                    } ?>><?php
                                                                  echo $key['jenis_bc']; ?>
          </option>
        <?php } ?>
      </select>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nomaintenance">No Limbah Order <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <select class="form-control" id="nomaintenance" name="nomaintenance" style="width: 100%" required>
        <option value="">-- Select No Limbah Order --</option>
        <?php
        foreach ($list_maintenance as $key) { ?>
          <option value="<?php echo $key['id_lo']; ?>" <?php
                                                          if (isset($bc[0]['id_lo'])) {
                                                            if ($bc[0]['id_lo'] == $key['id_lo']) {
                                                              echo "selected";
                                                            }
                                                          } ?>><?php
                                                                  echo $key['no_limbah_order']; ?>
          </option>
        <?php } ?>
      </select>
      <!-- <input type="hidden" id="type_text" name="type_text" value="<?php foreach ($jenis_bc as $key) {
                                                                          if (isset($bc[0]['jenis_bc'])) {
                                                                            if ($bc[0]['jenis_bc'] == $key['id']) {
                                                                              echo $key['jenis_bc'];
                                                                            }
                                                                          }
                                                                        } ?>"> -->
    </div>
  </div>
  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nopendaftaran">No Pendaftaran <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input data-parsley-minlength="4" data-parsley-maxlength="100" type="number" id="nopendaftaran" name="nopendaftaran" class="form-control col-md-7 col-xs-12" placeholder="no pendaftaran minimal 4 karakter" value="<?php if (isset($bc[0]['no_pendaftaran'])) {
                                                                                                                                                                                                                            echo $bc[0]['no_pendaftaran'];
                                                                                                                                                                                                                          } ?>" autocomplete="off">
      <span id="nopendaftaran_loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking No Pendaftaran...</span>
      <span id="nopendaftaran_tick"></span>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nopengajuan">No Pengajuan <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input data-parsley-minlength="4" data-parsley-maxlength="100" type="text" id="nopengajuan" name="nopengajuan" class="form-control col-md-7 col-xs-12" placeholder="no pengajuan minimal 4 karakter" value="<?php if (isset($bc[0]['no_pengajuan'])) {
                                                                                                                                                                                                                    echo $bc[0]['no_pengajuan'];
                                                                                                                                                                                                                  } ?>" autocomplete="off" readonly required>
      <span id="nopengajuan_loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking No Pengajuan...</span>
      <span id="nopengajuan_tick"></span>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tglpengajuan">Tanggal Pengajuan <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input data-parsley-maxlength="100" type="text" id="tglpengajuan" name="tglpengajuan" class="form-control col-md-7 col-xs-12" placeholder="tanggal pengajuan" autocomplete="off" required="required">
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit BC</button>
      <input type="hidden" id="bc_maintenance_id" name="bc_maintenance_id" value="<?php if (isset($bc[0]['id'])) {
                                                                                    echo $bc[0]['id'];
                                                                                  } ?>">
      <input type="hidden" id="bc_id" name="bc_id" value="<?php if (isset($bc[0]['id_bc'])) {
                                                            echo $bc[0]['id_bc'];
                                                          } ?>">
    </div>
  </div>
</form>
<!-- /page content -->

<script type="text/javascript">
  $(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();
    $('#tglpengajuan').datepicker({
      format: "dd/M/yyyy",
      autoclose: true,
      todayHighlight: true
    });
    $("#tglpengajuan").datepicker("setDate", '<?php if (isset($bc[0]['tanggal_pengajuan'])) {
                                                echo $bc[0]['tanggal_pengajuan'];
                                              } ?>');

    $('#jenis_bc').change(function() {
      if (this.options[this.selectedIndex].value != '') $('#type_text').val(this.options[this.selectedIndex].text);
      else $('#type_text').val('');
    });

  });

  var last_nopendaftaran = $('#nopendaftaran').val().toUpperCase();
  $('#nopendaftaran').on('input', function(event) {
    if ($('#nopendaftaran').val().toUpperCase() != last_nopendaftaran) nopendaftaran_check();
    else {
      $('#nopendaftaran').removeAttr("style");
      $('#nopendaftaran_tick').empty();
      $('#nopendaftaran_tick').hide();
      $('#nopendaftaran_loading-us').hide();
    }
  });

  function nopendaftaran_check() {
    var nopendaftaran = $('#nopendaftaran').val();
    if (nopendaftaran.length > 3) {
      var post_data = {
        'nopendaftaran': nopendaftaran
      };

      $('#nopendaftaran_tick').empty();
      $('#nopendaftaran_tick').hide();
      $('#nopendaftaran_loading-us').show();
      jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url('bc_maintenance/check_nopendaftaran'); ?>",
        data: post_data,
        cache: false,
        success: function(response) {
          if (response.success == true) {
            $('#nopendaftaran').css('border', '3px #090 solid');
            $('#nopendaftaran_loading-us').hide();
            $('#nopendaftaran_tick').empty();
            $("#nopendaftaran_tick").append('<span class="fa fa-check"> ' + response.message + '</span>');
            $('#nopendaftaran_tick').show();
          } else {
            $('#nopendaftaran').css('border', '3px #C33 solid');
            $('#nopendaftaran_loading-us').hide();
            $('#nopendaftaran_tick').empty();
            $("#nopendaftaran_tick").append('<span class="fa fa-close"> ' + response.message + '</span>');
            $('#nopendaftaran_tick').show();
          }
        }
      });
    } else {
      $('#nopendaftaran').css('border', '3px #C33 solid');
      $('#nopendaftaran_loading-us').hide();
      $('#nopendaftaran_tick').empty();
      $("#nopendaftaran_tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
      $('#nopendaftaran_tick').show();
    }
  }

  var last_nopengajuan = $('#nopengajuan').val().toUpperCase();
  $('#nopengajuan').on('input', function(event) {
    if ($('#nopengajuan').val().toUpperCase() != last_nopengajuan) nopengajuan_check();
    else {
      $('#nopengajuan').removeAttr("style");
      $('#nopengajuan_tick').empty();
      $('#nopengajuan_tick').hide();
      $('#nopengajuan_loading-us').hide();
    }
  });

  function nopengajuan_check() {
    var nopengajuan = $('#nopengajuan').val();
    if (nopengajuan.length > 3) {
      var post_data = {
        'nopengajuan': nopengajuan
      };

      $('#nopengajuan_tick').empty();
      $('#nopengajuan_tick').hide();
      $('#nopengajuan_loading-us').show();
      jQuery.ajax({
        type: "POST",
        url: "<?php echo base_url('bc_maintenance/check_nopengajuan'); ?>",
        data: post_data,
        cache: false,
        success: function(response) {
          if (response.success == true) {
            $('#nopengajuan').css('border', '3px #090 solid');
            $('#nopengajuan_loading-us').hide();
            $('#nopengajuan_tick').empty();
            $("#nopengajuan_tick").append('<span class="fa fa-check"> ' + response.message + '</span>');
            $('#nopengajuan_tick').show();
          } else {
            $('#nopengajuan').css('border', '3px #C33 solid');
            $('#nopengajuan_loading-us').hide();
            $('#nopengajuan_tick').empty();
            $("#nopengajuan_tick").append('<span class="fa fa-close"> ' + response.message + '</span>');
            $('#nopengajuan_tick').show();
          }
        }
      });
    } else {
      $('#nopengajuan').css('border', '3px #C33 solid');
      $('#nopengajuan_loading-us').hide();
      $('#nopengajuan_tick').empty();
      $("#nopengajuan_tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
      $('#nopengajuan_tick').show();
    }
  }

  $('#edit_form').on('submit', (function(e) {
    $('#btn-submit').attr('disabled', 'disabled');
    $('#btn-submit').text("Memasukkan data...");
    e.preventDefault();
    var formData = new FormData(this);
    save_Form(formData, $(this).attr('action'));

  }));

  function save_Form(formData, url) {
    $.ajax({
      type: 'POST',
      url: url,
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success: function(response) {
        if (response.success == true) {
          swal({
            title: 'Success!',
            text: response.message,
            type: 'success',
            showCancelButton: false,
            confirmButtonText: 'Ok'
          }).then(function() {
            window.location.href = "<?php echo base_url('bc_limbah'); ?>";
          })
        } else {
          $('#btn-submit').removeAttr('disabled');
          $('#btn-submit').text("Edit BC");
          swal("Failed!", response.message, "error");
        }
      }
    }).fail(function(xhr, status, message) {
      $('#btn-submit').removeAttr('disabled');
      $('#btn-submit').text("Edit BC");
      swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
    });
  }
</script>