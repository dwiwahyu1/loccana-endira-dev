<style type="text/css">
  .dt-body-center {
    text-align: center;
  }

  .dt-body-right {
    text-align: right;
  }
</style>

<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <h4 class="page-title" id="title_menu">Bea Cukai Limbah</h4>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <div class="card-box" id="div_list_bc">
        <table id="listbc" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th style="width: 5px;">No</th>
              <th>Jenis</th>
              <th>No. Pendaftaran</th>
              <th>No. Pengajuan</th>
              <th>Tanggal Pengajuan</th>
              <th style="width: 5px;">File</th>
              <th style="width: 150px;">Option</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>

      <div class="card-box" id="div_detail_bc" style="display: none;"></div>
    </div>
  </div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content p-0 b-0">
      <div class="panel panel-color panel-primary panel-filled">
        <div class="panel-heading">
          <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 class="panel-title"></h3>
        </div>
        <div class="panel-body">
          <p></p>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal-print" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog" style="width:80%;">
    <div class="modal-content p-0 b-0">
      <div class="panel panel-color panel-primary panel-filled">
        <div class="panel-heading">
          <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 class="panel-title"></h3>
        </div>
        <div class="panel-body">
          <p></p>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
  $(document).ready(function() {
    $('#div_detail_properties').hide();
    get_list_bc();
  });

  function get_list_bc() {
    $("#listbc").dataTable({
      "processing": true,
      "serverSide": true,
      "ajax": "<?php echo base_url() . 'bc_limbah/list_bc'; ?>",
      "searchDelay": 700,
      "responsive": true,
      "lengthChange": false,
      "destroy": true,
      "info": false,
      "bSort": false,
      "dom": 'l<"toolbar">frtip',
      "initComplete": function(r) {
        $("div.toolbar").prepend(
          '<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_bc()"><i class="fa fa-plus"></i> Add BC Limbah</a></div>'
        );
      },
      "columnDefs": [{
        targets: [0],
        className: 'dt-body-center'
      }]
    });
  }

  function add_bc() {
    $('#panel-modal').removeData('bs.modal');
    $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    $('#panel-modal  .panel-body').load('<?php echo base_url('bc_limbah/add_bc'); ?>');
    $('#panel-modal  .panel-title').html('<i class="fa fa-building-o"></i> Add BC Limbah');
    $('#panel-modal').modal({
      backdrop: 'static',
      keyboard: false
    }, 'show');
  }

  function edit_bc(id) {
    $('#panel-modal').removeData('bs.modal');
    $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    $('#panel-modal  .panel-body').load('<?php echo base_url('bc_limbah/edit_bc/'); ?>' + "/" + id);
    $('#panel-modal  .panel-title').html('<i class="fa fa-building-o"></i> Edit BC Limbah');
    $('#panel-modal').modal({
      backdrop: 'static',
      keyboard: false
    }, 'show');
  }

  function details_bc(bc_id) {
    $('#panel-modal-print').removeData('bs.modal');
    $('#panel-modal-print  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    $('#panel-modal-print  .panel-body').load('<?php echo base_url('bc_limbah/detail_bc/'); ?>' + "/" + bc_id);
    $('#panel-modal-print  .panel-title').html('<i class="fa fa-building-o"></i> Print BC Limbah');
    $('#panel-modal-print').modal({
      backdrop: 'static',
      keyboard: false
    }, 'show');
  }

  function delete_bc(id) {
    swal({
      title: 'Yakin akan Menghapus ?',
      text: 'data tidak dapat dikembalikan bila sudah dihapus !',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak'
    }).then(function() {
      var datapost = {
        "id": id
      };

      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>bc_limbah/delete_bc",
        data: JSON.stringify(datapost),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function(response) {
          swal({
            title: 'Success!',
            text: response.message,
            type: 'success',
            showCancelButton: false,
            confirmButtonText: 'Ok'
          }).then(function() {
            window.location.href = "<?php echo base_url('bc_limbah'); ?>";
          })

          if (response.status == "success") {

          } else {
            swal("Failed!", response.message, "error");
          }
        }
      });
    })
  }
</script>