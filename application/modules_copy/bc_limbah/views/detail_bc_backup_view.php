<div class="btn-group pull-left">
	<button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Back" onClick="back_bc()">
		<i class="fa fa-arrow-left"></i>
	</button>
</div>

<table id="list_detail_bc" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th style="width: 5px;">No</th>
			<th>No Order</th>
			<th>Tanggal Order</th>
			<th>Customer</th>
			<th>Term of Payment</th>
			<th>Keterangan</th>
			<th style="width: 5%;">Option</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>

<script type="text/javascript">
	var id_bc = "<?php if(isset($bc_id)){ echo $bc_id; } ?>";
	var table_d_bc;
	$(document).ready(function() {
		get_detail_bc();
	});

	function get_detail_bc() {
		table_d_bc = $("#list_detail_bc").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'bc_po_quotation/list_detail_bc?id_bc='; ?>" + id_bc,
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false,
			// "bFilter": false,
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip',
			"initComplete": function() {
				$("#div_detail_bc div.toolbar").prepend(
					'<div class="btn-group pull-left">'+
						'<a class="btn btn-primary" onClick="add_detail_bc()">'+
							'<i class="fa fa-plus"></i> Add Detail BC'+
						'</a>'+
					'</div>'
				);
			},
			"columnDefs": [{
				targets: [0],
				className: 'dt-body-center'
			}]
		});
	}

	function add_detail_bc() {
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('bc_po_quotation/add_detail_bc');?>');
		$('#panel-modal  .panel-title').html('<i class="fa fa-cogs"></i> Add Detail BC');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function edit_detail_bc(id) {
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('bc_po_quotation/edit_detail_bc/');?>'+"/"+id);
		$('#panel-modal  .panel-title').html('<i class="fa fa-cogs"></i> Edit Detail BC');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function delete_detail_bc(id) {
		swal({
			title: 'Yakin akan Menghapus ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost={
				"id" : id
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>bc_po_quotation/delete_detail_bc",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
				   swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						redrawData();
					})

					if (response.status == "success") {

					}else {
						swal("Failed!", response.message, "error");
					}
				}
			});
		})
	}

	function redrawData() {
		table_d_bc.fnClearTable();
		table_d_bc.fnDestroy();
		get_detail_bc();
	}

	function back_bc() {
		$('#title_Menu').html('Bea Cukai');
		$('#div_list_bc').show();
		$('#div_detail_bc').hide();
		$('#div_detail_bc').html('');
	}
</script>