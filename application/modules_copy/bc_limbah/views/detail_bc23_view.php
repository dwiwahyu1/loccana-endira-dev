<style type="text/css">
	.separator {
		display: flex;
		align-items: center;
		text-align: center;
	}
	
	.separator::before, .separator::after {
		content: '';
		flex: 1;
		border-bottom: 1px solid #000;
		border-style: dashed;
	}

	.separator::before {
		margin-right: .25em;
	}

	.separator::after {
		margin-left: .25em;
	}

	h4 { line-height: 20px !important; }
	.table {border-color: black !important}
	.table thead tr td, th {border-color: black !important}
	.table tbody tr td, th {border-color: black !important}
	.table tfoot tr td, th {border-color: black !important}
</style>
<?php
	/*echo "<pre>";
	print_r($header);
	// print_r($pungutan);
	echo "</pre>";*/
?>
<!-- Start Lembar 1 -->
<div class="card-box" style="margin-top: 20px;">
	<div class="btn-group pull-left" style="padding-bottom: 20px;">
		<a class="btn btn-info btn-sm" id="back_bc" name="back_bc" data-toggle="tooltip" data-placement="top" title="Back">
			<i class="fa fa-arrow-left"></i>
		</a>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border: 0px;">
				<tr style="font-size: 20px;">
					<th style="border: 0px; padding: 0px;" class="dt-body-center"><?php
						if(isset($bc[0]['desc_bc'])) echo strtoupper($bc[0]['desc_bc']);
					?></th>
				</tr>
				<tr>
					<th style="border: 0px; padding: 0px;" class="dt-body-right"><?php
						if(isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']);
					?></th>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<tr>
					<td style="border: 0px; width: 15%;">Nomor Pengajuaan</td>
					<td style="border: 0px;" colspan="3">: &nbsp; <?php if(isset($bc[0]['no_pengajuan'])) echo $bc[0]['no_pengajuan']; ?></td>
					<th style="border: 0px; width: 15%;" rowspan="2">Halaman Ke-1 dari 3</th>
				</tr>
				<tr>
					<td style="border: 0px; width: 15%;">A. Jenis TPB</td>
					<td style="border: 0px; width: 1px;">:</td>
					<td style="border: 1px solid; width: 30px;" class="dt-body-center">1</td>
					<td style="border: 0px;" colspan="2">1. Kawasan Berikat &nbsp; 2. Gudang Serikat &nbsp; 3. TPPB &nbsp; 4.TBB &nbsp; 5. TLB &nbsp; 6. KDUB &nbsp; 7. Lainnya</td>
				</tr>
				<tr>
					<td style="border: 0px;" colspan="5"></td>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<th style="width: 50%; border-bottom: 0px; padding-left: 15px; padding-bottom: 0px;">B. DATA PEMBERITAHUAN PEMASOK</th>
						<th style="width: 50%; border-bottom: 0px; padding-left: 15px; padding-bottom: 0px;">D. DIISI OLEH BEA DAN CUKAI</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="width: 50%; border-top: 0px; padding-top: 0px;">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<tr>
									<td style="border: 0px; width: 30%;;">1. Nama, Alamat, Negara</td>
									<td style="border: 0px;" colspan="2">: &nbsp; <?php
										if(isset($header[0]['PEMASOK']) && isset($header[0]['ALAMAT_PEMASOK'])) echo $header[0]['PEMASOK'].'<br/>'.$header[0]['ALAMAT_PEMASOK'].'<br/><br/>'.$header[0]['KODE_ID_PEMASOK'];
									?></td>
								</tr>
								<tr>
									<td style="border: 0px;" class="dt-body-right" colspan="2"><?php
										if(isset($header[0]['KODE_NEGARA_PEMASOK'])) echo trim($header[0]['KODE_NEGARA_PEMASOK'])
									?></td>
									<td style="border: 1px solid; height: 30px; width: 20%;"><input style="padding: 0px 0px 0px 5px; margin: 0px 0px 0px 0px;" type="text" class="form-control" id="kode_negara_pemasok" name="kode_negara_pemasok" placeholder="Kode Negara" autocomplete="off" value="<?php
										if(isset($header[0]['KODE_NEGARA_PEMASOK'])) echo trim($header[0]['KODE_NEGARA_PEMASOK']);
									?>" required></td>
								</tr>
							</table>
						</td>
						<td style="width: 50%; border-top: 0px; padding-top: 0px;">
							<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="border: 0px; margin-bottom: 0px;">
								<tr>
									<td style="border: 0px; width: 35%;">No. dan Tgl. Pendaftaraan</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($bc[0]['no_pendaftaran'])) echo $bc[0]['no_pendaftaran'];
									?></td>
									<td style="border: 0px;"><?php
										if(isset($bc[0]['tanggal_pengajuan'])) echo date('d-m-Y', strtotime($bc[0]['tanggal_pengajuan']));
									?></td>
								</tr>
								<tr>
									<td style="border: 0px; width: 35%;">Kantor Pabean bongkar</td>
									<td style="border: 0px; width: 32.5%;">: <input style="padding: 0px 0px 0px 5px; margin: -30px 0px 0px 15px; width: 55%;" class="form-control" type="text" id="kode_kantor_bongkar" name="kode_kantor_bongkar" placeholder="Kode KPPBC" value="<?php
											if(isset($header[0]['KODE_KANTOR_BONGKAR'])) echo $header[0]['KODE_KANTOR_BONGKAR']; ?>" autocomplete="off" required> &nbsp; <?php
											if(isset($header[0]['KODE_KANTOR_BONGKAR'])) {
												$kodekantor = (int)$header[0]['KODE_KANTOR_BONGKAR'];
												foreach ($ref_kppbc as $rkk => $rkv) {
													if($rkv['KODE_KANTOR'] == $kodekantor) echo $rkv['URAIAN_KANTOR'];
												}
											}
										?></td>
									<td style="border: 0px; width: 32.5%;"><?php
										if(isset($header[0]['KODE_KANTOR_BONGKAR'])) echo trim($header[0]['KODE_KANTOR_BONGKAR']);
									?></td>
								</tr>
								<tr>
									<td style="border: 0px; width: 35%;">Kantor Pabean Pengawas</td>
									<td style="border: 0px; width: 32.5%;">: &nbsp; </td>
									<td style="border: 0px; width: 32.5%;"></td>
								</tr>
							</table>
						</td>
					</tr>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<tr>
					<th style="width: 50%; border-bottom: 0px; padding-left: 15px; padding-bottom: 0px;">IMPORTIR</th>
					<td style="width: 50%;" rowspan="6">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
							<tr>
								<td style="border: 0px; width: 35%;">16. Invoice</td>
								<td style="border: 0px; width: 40%;">: &nbsp; <?php
									if(isset($dokumen) && sizeof($dokumen) > 0) {
										foreach ($dokumen as $dk => $dv) {
											if($dv['KODE_JENIS_DOKUMEN'] == 380) echo trim($dv['NOMOR_DOKUMEN']);
										}
									}
								?></td>
								<td style="border: 0px;">Tgl. <?php
									if(isset($dokumen) && sizeof($dokumen) > 0) {
										foreach ($dokumen as $dk => $dv) {
											if($dv['KODE_JENIS_DOKUMEN'] == 380) echo date('d-m-Y', strtotime($dv['TANGGAL_DOKUMEN']));
										}
									}
								?></td>
							</tr>
							<tr>
								<td style="border: 0px; width: 35%;">17. Fasilitas Impor</td>
								<td style="border: 0px; width: 40%;">: &nbsp; <?php
									if(isset($dokumen) && sizeof($dokumen) > 0) {
										foreach ($dokumen as $dk => $dv) {
											if($dv['KODE_JENIS_DOKUMEN'] == 808 || $dv['KODE_JENIS_DOKUMEN'] == 836) echo trim($dv['NOMOR_DOKUMEN']);
										}
									}
								?></td>
								<td style="border: 0px;">Tgl. <?php
									if(isset($dokumen) && sizeof($dokumen) > 0) {
										foreach ($dokumen as $dk => $dv) {
											if($dv['KODE_JENIS_DOKUMEN'] == 808 || $dv['KODE_JENIS_DOKUMEN'] == 836) echo date('d-m-Y', strtotime($dv['TANGGAL_DOKUMEN']));
										}
									}
								?></td>
							</tr>
							<tr>
								<td style="border: 0px; width: 35%;">18. Surat Keputusan / Dokumen Lainnya</td>
								<td style="border: 0px; width: 40%;">: &nbsp; <?php
									if(isset($dokumen) && sizeof($dokumen) > 0) {
										foreach ($dokumen as $dk => $dv) {
											if($dv['KODE_JENIS_DOKUMEN'] == 854 || $dv['KODE_JENIS_DOKUMEN'] == 911 || $dv['KODE_JENIS_DOKUMEN'] == 959) echo trim($dv['NOMOR_DOKUMEN']);
										}
									}
								?></td>
								<td style="border: 0px;">Tgl. <?php
									if(isset($dokumen) && sizeof($dokumen) > 0) {
										foreach ($dokumen as $dk => $dv) {
											if($dv['KODE_JENIS_DOKUMEN'] == 854 || $dv['KODE_JENIS_DOKUMEN'] == 911 || $dv['KODE_JENIS_DOKUMEN'] == 959) echo date('d-m-Y', strtotime($dv['TANGGAL_DOKUMEN']));
										}
									}
								?></td>
							</tr>
							<tr>
								<td style="border: 0px; width: 35%;">19. LC</td>
								<td style="border: 0px; width: 40%;">: &nbsp; <?php
									if(isset($dokumen) && sizeof($dokumen) > 0) {
										foreach ($dokumen as $dk => $dv) {
											if($dv['KODE_JENIS_DOKUMEN'] == 645) echo trim($dv['NOMOR_DOKUMEN']);
										}
									}
								?></td>
								<td style="border: 0px;">Tgl. <?php
									if(isset($dokumen) && sizeof($dokumen) > 0) {
										foreach ($dokumen as $dk => $dv) {
											if($dv['KODE_JENIS_DOKUMEN'] == 645) echo date('d-m-Y', strtotime($dv['TANGGAL_DOKUMEN']));
										}
									}
								?></td>
							</tr>
							<tr>
								<td style="border: 0px; width: 35%;">20. BL / AWB</td>
								<td style="border: 0px; width: 40%;">: &nbsp; <?php
									if(isset($dokumen) && sizeof($dokumen) > 0) {
										foreach ($dokumen as $dk => $dv) {
											if($dv['KODE_JENIS_DOKUMEN'] == 705 || $dv['KODE_JENIS_DOKUMEN'] == 740) echo trim($dv['NOMOR_DOKUMEN']);
										}
									}
								?></td>
								<td style="border: 0px;">Tgl. <?php
									if(isset($dokumen) && sizeof($dokumen) > 0) {
										foreach ($dokumen as $dk => $dv) {
											if($dv['KODE_JENIS_DOKUMEN'] == 705 || $dv['KODE_JENIS_DOKUMEN'] == 740) echo date('d-m-Y', strtotime($dv['TANGGAL_DOKUMEN']));
										}
									}
								?></td>
							</tr>
							<tr>
								<td style="border: 0px; width: 35%;">21. BC 1.1</td>
								<td style="border: 0px; width: 40%;">: <input style="padding: 0px 0px 0px 5px; margin: -27px 0px 0px 15px; width: 90%;" class="form-control" type="number" id="nomor_bc11" name="nomor_bc11" value="<?php
									if(isset($header[0]['NOMOR_BC11'])) echo trim($header[0]['NOMOR_BC11']);
								?>" placeholder="Nomor" required> <input style="padding: 0px 0px 0px 5px; margin: 5px 0px 0px 15px; width: 90%;" class="form-control" type="number" id="pos_bc11" name="pos_bc11" value="<?php
									if(isset($header[0]['POS_BC11'])) echo trim($header[0]['POS_BC11']);
								?>" placeholder="POS" required> <input style="padding: 0px 0px 0px 5px; margin: 5px 0px 0px 15px; width: 90%;" class="form-control" type="number" id="subpos_bc11" name="subpos_bc11" value="<?php
									if(isset($header[0]['SUBPOS_BC11'])) echo trim($header[0]['SUBPOS_BC11']);
								?>" placeholder="SUB POS" required> <input style="padding: 0px 0px 0px 5px; margin: 5px 0px 0px 15px; width: 90%;" class="form-control" type="number" id="sub_subpos_bc11" name="sub_subpos_bc11" value="<?php
									if(isset($header[0]['SUB_SUBPOS_BC11'])) echo trim($header[0]['SUB_SUBPOS_BC11']);
								?>" placeholder="SUB SUB POS" required>
								</td>
								<td style="border: 0px;">Tgl. <input style="padding: 0px 0px 0px 5px; margin: -29px 0px 0px 30px; width: 80%;" class="form-control" type="text" id="tanggal_bc11" name="tanggal_bc11" value="<?php
									if(isset($header[0]['TANGGAL_BC11']) && $header[0]['TANGGAL_BC11'] != '') echo date('d-m-Y', strtotime($header[0]['TANGGAL_BC11']));
								?>" placeholder="Tanggal" autocomplete="off" required></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="width: 50%; border-top: 0px; padding-top: 0px;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
							<tr>
								<td style="border: 0px; width: 25%;">2. Indentitas (NPWP)</td>
								<td style="border: 0px;">: &nbsp; <?php
									if(isset($header[0]['ID_PENGUSAHA'])) echo $header[0]['ID_PENGUSAHA'];
								?></td>
							</tr>
							<tr>
								<td style="border: 0px; width: 25%;">3. Nama, Alamat</td>
								<td style="border: 0px;">: &nbsp; <?php
									if(isset($header[0]['PERUSAHAAN']) && isset($header[0]['ALAMAT_PENGUSAHA'])) echo $header[0]['PERUSAHAAN'].'<br/>'.$header[0]['ALAMAT_PENGUSAHA'];
								?></td>
							</tr>
							<tr>
								<td style="border: 0px; width: 25%;">4. No Izin TPB</td>
								<td style="border: 0px;">: <input style="padding: 0px 0px 0px 5px; margin: -28px 0px 0px 15px;" class="form-control" type="text" id="nomor_ijin_tpb" name="nomor_ijin_tpb" value="<?php
									if(isset($header[0]['NOMOR_IJIN_TPB'])) echo trim($header[0]['NOMOR_IJIN_TPB']);
								?>" placeholder="Nomor Ijin TPB" autocomplete="off" required></td>
							</tr>
							<tr>
								<td style="border: 0px; width: 25%;">5. API</td>
								<td style="border: 0px;">: &nbsp;<?php
									if(isset($header[0]['API_PENGUSAHA'])) echo $header[0]['API_PENGUSAHA'];
								?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<th style="width: 50%; border-bottom: 0px; padding-left: 15px; padding-bottom: 0px;">PEMILIK BARANG</th>
				</tr>
				<tr>
					<td style="border-top: 0px; padding-top: 0px;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
							<tr>
								<td style="border: 0px; width: 25%;">6. Indentitas (NPWP)</td>
								<td style="border: 0px;">: &nbsp; <?php
									if(isset($header[0]['ID_PEMILIK'])) echo $header[0]['ID_PEMILIK'];
								?></td>
							</tr>
							<tr>
								<td style="border: 0px; width: 25%;">7. Nama, Alamat</td>
								<td style="border: 0px;">: &nbsp;<?php
									if(isset($header[0]['NAMA_PEMILIK']) && isset($header[0]['ALAMAT_PEMILIK'])) echo $header[0]['NAMA_PEMILIK'].'<br/>'.$header[0]['ALAMAT_PEMILIK'];
								?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<th style="width: 50%; border-bottom: 0px; padding-left: 15px; padding-bottom: 0px;">PPJK</th>
				</tr>
				<tr>
					<td style="border-top: 0px; padding-top: 0px;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
							<tr>
								<td style="border: 0px; width: 30%;">8. NPWP</td>
								<td style="border: 0px;" colspan="3">: &nbsp; <?php
									if(isset($header[0]['ID_PPJK'])) echo $header[0]['ID_PPJK'];
								?></td>
							</tr>
							<tr>
								<td style="border: 0px; width: 30%;">9. Nama, Alamat</td>
								<td style="border: 0px;" colspan="3">: &nbsp; <?php
									if(isset($header[0]['NAMA_PPJK']) && isset($header[0]['ALAMAT_PPJK'])) echo $header[0]['NAMA_PPJK'].'<br/>'.$header[0]['ALAMAT_PPJK'];
								?></td>
							</tr>
							<tr>
								<td style="border: 0px; width: 30%;">10. No dan Tgl NP-PPJK</td>
								<td style="border: 0px; width: 1px;">:</td>
								<td style="border: 1px solid; width: 35%;"><?php
									if(isset($header[0]['NPPJK'])) echo $header[0]['NPPJK'];
								?></td>
								<td style="border: 1px solid; width: 35%;"><?php
									if(isset($header[0]['TANGGAL_NPPPJK']) && trim($header[0]['TANGGAL_NPPPJK']) != '') echo date('d-m-Y', strtotime($header[0]['TANGGAL_NPPPJK']));
								?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<tr>
					<td style="width: 50%;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
							<tr>
								<td style="border: 0px; width: 30%;">11. Cara Pengangkutan</td>
								<td style="border: 0px;">: &nbsp; <?php
									if(isset($header[0]['KODE_CARA_ANGKUT'])) echo trim($header[0]['KODE_CARA_ANGKUT']);
								?></td>
								<td style="border: 1px solid; width: 15%;"><input style="padding: 0px 0px 0px 10px; margin: 0px 0px 0px 0px;" class="form-control" type="text" id="kode_cara_angkut" name="kode_cara_angkut" value="<?php
									if(isset($header[0]['KODE_CARA_ANGKUT'])) echo $header[0]['KODE_CARA_ANGKUT'];
								?>" autocomplete="off" required></td>
							</tr>
						</table>
					</td>
					<td style="width: 50%;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
							<tr>
								<td style="border: 0px; width: 35%;">22. Tempat Penimbunan</td>
								<td style="border: 0px;">: &nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<tr>
					<td style="width: 49.99%;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
							<tr>
								<td style="width: 70%; border: 0px;" colspan="4">12. Nama Sarana Pengangkutan & No Voy/Flight dan Bendera :</td>
							</tr>
							<tr>
								<td style="border: 0px; width: 25%;">
									<input style="padding: 0px 0px 0px 5px; margin: 0px;" class="form-control" type="text" id="nama_pengangkut" name="nama_pengangkut" placeholder="Nama Sarana Pengangkutan" value="<?php if(isset($header[0]['NAMA_PENGANGKUT'])) echo $header[0]['NAMA_PENGANGKUT']; ?>" autocomplete="off" required>
								</td>
								<td style="border: 0px; width: 15%;">
									<input style="padding: 0px 0px 0px 5px; margin: 0px;" class="form-control" type="text" id="nomor_voyv_flight" name="nomor_voyv_flight" placeholder="No Voy/Flight" value="<?php if(isset($header[0]['NOMOR_VOYV_FLIGHT'])) echo $header[0]['NOMOR_VOYV_FLIGHT']; ?>" placeholder="No Voy/Flight" autocomplete="off" required>
								</td>
								<td style="border: 0px; width: 15%;" class="dt-body-right"><?php
									if(isset($header[0]['KODE_BENDERA'])) echo trim($header[0]['KODE_BENDERA']);
								?></td>
								<td style="border: 1px solid; width: 15%;" class="dt-body-center"><input type="text" class="form-control" id="kode_bendera" name="kode_bendera" placeholder="Bendera" value="<?php
									if(isset($header[0]['KODE_BENDERA'])) echo $header[0]['KODE_BENDERA'];
								?>" autocomplete="off" required></td>
							</tr>
						</table>
					</td>
					<td style="width: 25%;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
							<tr>
								<td style="width: 35%; border: 0px;">23. Valuta</td>
								<td style="border: 0px;">: &nbsp; <?php
									if(isset($header[0]['KODE_VALUTA'])) echo $header[0]['KODE_VALUTA'];
								?></td>
							</tr>
						</table>
					</td>
					<td style="width: 25%;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
							<tr>
								<td style="width: 35%; border: 0px;">24. NDPBM</td>
								<td style="border: 0px;">: &nbsp; <?php
									if(isset($header[0]['NDPBM'])) echo number_format((int)$header[0]['NDPBM'], 4);
								?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<tr>
					<td style="width: 49.99%;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
							<tr>
								<td style="border: 0px; width: 30%">13. Pelabuhan Muat</td>
								<td style="border: 0px;">: <input style="padding: 0px 0px 0px 5px; margin: -27px 0px 0px 15px; width: 30%;" class="form-control" type="text" id="kode_pel_muat" name="kode_pel_muat" placeholder="Kode Pelabuhan" value="<?php
									if(isset($header[0]['KODE_PEL_MUAT'])) echo trim($header[0]['KODE_PEL_MUAT']);
								?>" autocomplete="off" required> &nbsp; <?php
									if(isset($header[0]['KODE_PEL_MUAT'])) echo trim($header[0]['KODE_PEL_MUAT']);
								?></td>
							</tr>
							<tr>
								<td style="border: 0px; width: 30%;">14. Pelabuhan Transit</td>
								<td style="border: 0px;">: <input style="padding: 0px 0px 0px 5px; margin: -27px 0px 0px 15px; width: 30%;" class="form-control" type="text" id="kode_pel_transit" name="kode_pel_transit" placeholder="Kode Pelabuhan" value="<?php
									if(isset($header[0]['KODE_PEL_TRANSIT'])) echo trim($header[0]['KODE_PEL_TRANSIT']);
								?>" autocomplete="off" required> &nbsp; <?php
									if(isset($header[0]['KODE_PEL_TRANSIT'])) echo trim($header[0]['KODE_PEL_TRANSIT']);
								?></td>
							</tr>
							<tr>
								<td style="border: 0px; width: 30%;">15. Pelabuhan Bongkar</td>
								<td style="border: 0px;">: <input style="padding: 0px 0px 0px 5px; margin: -27px 0px 0px 15px; width: 30%;" class="form-control" type="text" id="kode_pel_bongkar" name="kode_pel_bongkar" placeholder="Kode Pelabuhan" value="<?php
									if(isset($header[0]['KODE_PEL_BONGKAR'])) echo trim($header[0]['KODE_PEL_BONGKAR']);
								?>" autocomplete="off" required> &nbsp; <?php
									if(isset($header[0]['KODE_PEL_BONGKAR'])) echo trim($header[0]['KODE_PEL_BONGKAR']);
								?></td>
							</tr>
						</table>
					</td>
					<td style="width: 25%;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
							<tr>
								<td style="border: 0px; width: 50%">25. FOB</td>
								<td style="border: 0px;">: &nbsp; <?php
									if(isset($header[0]['FOB'])) echo number_format((int)$header[0]['FOB'], 4);
								?></td>
							</tr>
							<tr>
								<td style="border: 0px; width: 50%;">26. Freight</td>
								<td style="border: 0px;">: &nbsp; <?php
									if(isset($header[0]['FREIGHT'])) echo number_format((int)$header[0]['FREIGHT'], 4);
								?></td>
							</tr>
							<tr>
								<td style="border: 0px; width: 50%;">27. Asuransi LB/DN</td>
								<td style="border: 0px;">: &nbsp;</td>
							</tr>
						</table>
					</td>
					<td style="width: 25%;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
							<tr>
								<td style="border: 0px; width: 35%">28. Nilai CIF</td>
								<td style="border: 0px;">: &nbsp; <?php
									if(isset($header[0]['CIF'])) echo number_format((int)$header[0]['CIF'], 4);
								?></td>
							</tr>
							<tr>
								<td style="border: 0px; width: 35%;">Rp.</td>
								<td style="border: 0px;">&nbsp; <?php
									if(isset($header[0]['CIF_RUPIAH'])) echo number_format((int)$header[0]['CIF_RUPIAH'], 2);
								?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<tr>
					<td style="width: 74.99%;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
							<tr>
								<td style="border: 0px;">29. Nomor, Ukuran, dan Tipe Peti Kemas : <input style="padding: 0px 0px 0px 5px; margin: 0px 0px 0px 0px; width: 20%;" type="number" class="form-control" id="jml_kontainer" name="jml_kontainer" step=".0001" value="<?php
									if(isset($header[0]['JUMLAH_KONTAINER'])) echo number_format(floatval($header[0]['JUMLAH_KONTAINER']), 4);
								?>" placeholder="Jumlah" required> &nbsp; <?php
									if(isset($kontainer) && sizeof($kontainer) > 0) {
										foreach ($kontainer as $kk => $kv) {
											echo $kv['NOMOR_KONTAINER'].', '.$kv['KODE_UKURAN_KONTAINER'].', '.$kv['KODE_TIPE_KONTAINER'].'<br/>';
										}
									}
								?></td>
								<td style="border: 0px;">30. Jumlah, Jenis, dan Merek Kemasan : <input style="padding: 0px 0px 0px 5px; margin: 0px 0px 0px 0px; width: 20%;" type="number" class="form-control" id="jml_kemasan" name="jml_kemasan" step=".0001" value="<?php
									if(isset($header[0]['JUMLAH_KEMASAN'])) echo number_format(floatval($header[0]['JUMLAH_KEMASAN']), 4);
								?>" placeholder="Jumlah" required> &nbsp; <?php
									if(isset($kemasan) && sizeof($kemasan) > 0) {
										foreach ($kemasan as $kk => $kv) {
											echo ' '.trim($kv['KODE_JENIS_KEMASAN']).', '.trim($kv['MEREK_KEMASAN']).'<br/>';
										}
									}
								?></td>
							</tr>
						</table>
					</td>
					<td style="width: 25%;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
							<tr>
								<td style="border: 0px; width: 51%;">31. Berat Kotor (Kg)</td>
								<td style="border: 0px;">: <input style="padding: 0px 0px 0px 10px; margin-top: -27px; margin-left: 10px;" type="number" class="form-control" id="bruto" name="bruto" step=".0001" value="<?php
									if(isset($header[0]['BRUTO'])) echo number_format((int)$header[0]['BRUTO'], 4);
								?>" placeholder="Berat Kotor" required></td>
							</tr>
							<tr>
								<td style="border: 0px; width: 51%;">32. Berat Bersih (Kg)</td>
								<td style="border: 0px;">: &nbsp; <?php
									if(isset($header[0]['NETTO'])) echo number_format((int)$header[0]['NETTO'], 4);
								?></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<td style="width: 5%; padding-left: 16px;">33. No</td>
						<td style="width: 22%; padding-left: 16px;">34. <ul style="margin: -20px 0px 0px 0px; padding: 0px 0px 0px 45px;">
							<li>Pos Tarif/HS</li>
							<li>Kode Barang</li>
							<li>Uraian barang secara lengkap, merk, tipe, ukuran, spesifikasi lain</li>
							<li>Jumlah dan Jenis Kemasan</li>
							<li>Fasilitas Impor</li>
							<li>Surat Keputusan/Dokumen Lainnya</li>
						</ul></td>
						<td style="width: 10%; padding-left: 16px;">35. Kategori Barang</td>
						<td style="width: 9%; padding-left: 16px;">36. Negara Asal</td>
						<td style="width: 12%; padding-left: 16px;">37. <ul style="margin: -20px 0px 0px 0px; padding: 0px 0px 0px 45px;">
							<li>Tarif dan Fasilitas</li>
							<li>BM</li>
							<li>BMT</li>
							<li>Cukai</li>
							<li>PPN</li>
							<li>PPnBM</li>
							<li>PPh</li>
						</ul></td>
						<td style="width: 12%; padding-left: 16px;">38. <ul style="margin: -20px 0px 0px 0px; padding: 0px 0px 0px 45px;">
							<li>Jumlah</li>
							<li>Jenis Satuan</li>
							<li>Berat Bersih (Kg)</li>
						</ul></td>
						<td style="width: 10%; padding-left: 16px;">39. Jumlah Nilai CIF</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="7">
							<div style="height: 70px;" class="col-sm-12 separator">
								<h5><?php if(isset($header[0]['JUMLAH_BARANG'])) echo $header[0]['JUMLAH_BARANG']; ?> Jenis Barang. Lihat lembar lanjutan</h5>
							</div>
						</td>
					</tr>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<td class="dt-body-center" colspan="2">Jenis Pungutan</td>
						<td style="width: 12%;" class="dt-body-center">Ditanggungkan (Rp)</td>
						<td style="width: 12%;" class="dt-body-center">Dibebaskan (Rp)</td>
						<td style="width: 12%;" class="dt-body-center">Tidak Dipungut (Rp)</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="width: 5%;" class="dt-body-center">34.</td>
						<td>BM</td>
						<td class="dt-body-right">0,0000</td>
						<td class="dt-body-right">0,0000</td>
						<td class="dt-body-right"><?php
						if(isset($pungutan) && sizeof($pungutan) > 0) {
							$nilai = 0;
							foreach ($pungutan as $pk => $pv) {
								if(trim($pv['JENIS_TARIF']) == 'BM') $nilai = $pv['NILAI_PUNGUTAN'];
							}
							echo number_format((int)$nilai, 4);
						}else echo "0"; ?></td>
					</tr>
					<tr>
						<td style="width: 5%;" class="dt-body-center">35</td>
						<td>BMT</td>
						<td class="dt-body-right">0,0000</td>
						<td class="dt-body-right">0,0000</td>
						<td class="dt-body-right"><?php
						if(isset($pungutan) && sizeof($pungutan) > 0) {
							$nilai = 0;
							foreach ($pungutan as $pk => $pv) {
								if(trim($pv['JENIS_TARIF']) == 'BMT') $nilai = $pv['NILAI_PUNGUTAN'];
							}
							echo number_format($nilai, 4);
						}else echo "0"; ?></td>
					</tr>
					<tr>
						<td style="width: 5%;" class="dt-body-center">36.</td>
						<td>Cukai</td>
						<td class="dt-body-right">0,0000</td>
						<td class="dt-body-right">0,0000</td>
						<td class="dt-body-right"><?php
						if(isset($pungutan) && sizeof($pungutan) > 0) {
							$nilai = 0;
							foreach ($pungutan as $pk => $pv) {
								if(trim($pv['JENIS_TARIF']) == 'CUKAI') $nilai = $pv['NILAI_PUNGUTAN'];
							}
							echo number_format($nilai, 4);
						}else echo "0"; ?></td>
					</tr>
					<tr>
						<td style="width: 5%;" class="dt-body-center">37.</td>
						<td>PPN</td>
						<td class="dt-body-right">0,0000</td>
						<td class="dt-body-right">0,0000</td>
						<td class="dt-body-right"><?php
						if(isset($pungutan) && sizeof($pungutan) > 0) {
							$nilai = 0;
							foreach ($pungutan as $pk => $pv) {
								if(trim($pv['JENIS_TARIF']) == 'PPN') $nilai = $pv['NILAI_PUNGUTAN'];
							}
							echo number_format($nilai, 4);
						}else echo "0"; ?></td>
					</tr>
					<tr>
						<td style="width: 5%;" class="dt-body-center">38.</td>
						<td>PPnBM</td>
						<td class="dt-body-right">0,0000</td>
						<td class="dt-body-right">0,0000</td>
						<td class="dt-body-right"><?php
						if(isset($pungutan) && sizeof($pungutan) > 0) {
							$nilai = 0;
							foreach ($pungutan as $pk => $pv) {
								if(trim($pv['JENIS_TARIF']) == 'PPNBM') $nilai = $pv['NILAI_PUNGUTAN'];
							}
							echo number_format($nilai, 4);
						}else echo "0"; ?></td>
					</tr>
					<tr>
						<td style="width: 5%;" class="dt-body-center">39.</td>
						<td>PPh</td>
						<td class="dt-body-right">0,0000</td>
						<td class="dt-body-right">0,0000</td>
						<td class="dt-body-right"><?php
						if(isset($pungutan) && sizeof($pungutan) > 0) {
							$nilai = 0;
							foreach ($pungutan as $pk => $pv) {
								if(trim($pv['JENIS_TARIF']) == 'PPH') $nilai = $pv['NILAI_PUNGUTAN'];
							}
							echo number_format($nilai, 4);
						}else echo "0"; ?></td>
					</tr>
					<tr>
						<td style="width: 5%;" class="dt-body-center">40.</td>
						<td>TOTAL</td>
						<td class="dt-body-right">0,0000</td>
						<td class="dt-body-right">0,0000</td>
						<td class="dt-body-right"><?php
						if(isset($pungutan) && sizeof($pungutan) > 0) {
							$total = 0;
							foreach ($pungutan as $pk => $pv) {
								$total = $total + ((int)$pv['NILAI_PUNGUTAN']);
							}
							echo number_format($total, 2);
						}else echo "0"; ?></td>
					</tr>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
				<tr>
					<td style="width: 50%;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
							<thead>
								<tr>
									<td style="border: 0px;" colspan="2">C. PENGESAHAN PENGUSAHA TPB</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="border: 0px;" colspan="2">Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberitahukan dalam pemberitahuan pabean ini</td>
								</tr>
								<tr>
									<td style="width: 30%; border: 0px;">Tempat, Tanggal</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['KOTA_TTD'])) echo strtoupper($header[0]['KOTA_TTD']);
										echo ", ";
									?> <input style="padding: 0px 0px 0px 5px; margin: -30px 0px 0px 90px; width: 40%;" class="form-control" type="text" id="tanggal_ttd" name="tanggal_ttd" value="<?php
										if(isset($header[0]['TANGGAL_TTD']) && $header[0]['TANGGAL_TTD'] != '') echo date('d-m-Y', strtotime($header[0]['TANGGAL_TTD']));
									?>" placeholder="Tanggal Tanda Tangan" required></td>
								</tr>
								<tr>
									<td style="width: 30%; border: 0px;">Nama Lengkap</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['NAMA_TTD'])) echo $header[0]['NAMA_TTD'];
									?></td>
								</tr>
								<tr>
									<td style="width: 30%; border: 0px;">Jabatan</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['JABATAN_TTD'])) echo $header[0]['JABATAN_TTD'];
									?></td>
								</tr>
								<tr>
									<td style="border: 0px; height: 150px;" colspan="2">Tanda Tangan dan Stempel Perusahaan :</td>
								</tr>
							</tbody>
						</table>
					</td>
					<td style="width: 50%;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
							<thead>
								<tr>
									<td style="border: 0px;" colspan="2">E. UNTUK PEJABAT BEA DAN CUKAI</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="border: 0px; height: 314.4px;" colspan="2"></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<!-- End Lembar 1 -->

<!-- Start Lembar 2 -->
<div class="card-box" style="margin-top: 20px;">
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border: 0px;">
				<tr style="font-size: 20px;">
					<th style="border: 0px; padding: 0px;" class="dt-body-center">LEMBAR LANJUTAN<br/><?php
						if(isset($bc[0]['desc_bc'])) echo strtoupper($bc[0]['desc_bc']);
					?></th>
				</tr>
				<tr>
					<th style="border: 0px; padding: 0px;" class="dt-body-right"><?php
						if(isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']);
					?></th>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<tr>
					<td style="border: 0px; width: 15%;">Nomor Pengajuaan</td>
					<td style="border: 0px;">: &nbsp; <?php if(isset($bc[0]['no_pengajuan'])) echo $bc[0]['no_pengajuan']; ?></td>
					<th style="border: 0px; width: 15%;" rowspan="2">Halaman Ke-2 dari 3</th>
				</tr>
				<tr>
					<td style="border: 0px; width: 15%;">Nomor Pendaftaran</td>
					<td style="border: 0px;">: &nbsp; <?php if(isset($bc[0]['no_pendaftaran'])) echo $bc[0]['no_pendaftaran']; ?></td>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<td style="width: 5%; padding-left: 16px;">33. No</td>
						<td style="width: 22%; padding-left: 16px;">34. <ul style="margin: -20px 0px 0px 0px; padding: 0px 0px 0px 45px;">
							<li>Pos Tarif/HS</li>
							<li>Kode Barang</li>
							<li>Uraian barang secara lengkap, merk, tipe, ukuran, spesifikasi lain</li>
							<li>Jumlah dan Jenis Kemasan</li>
							<li>Fasilitas Impor</li>
							<li>Surat Keputusan/Dokumen Lainnya</li>
						</ul></td>
						<td style="width: 10%; padding-left: 16px;">35. Kategori Barang</td>
						<td style="width: 9%; padding-left: 16px;">36. Negara Asal</td>
						<td style="width: 12%; padding-left: 16px;">37. Tarif dan Fasilitas<ul style="padding: 0px 0px 0px 45px;">
							<li>BM</li>
							<li>BMT</li>
							<li>Cukai</li>
							<li>PPN</li>
							<li>PPnBM</li>
							<li>PPh</li>
						</ul></td>
						<td style="width: 12%; padding-left: 16px;">38. <ul style="margin: -20px 0px 0px 0px; padding: 0px 0px 0px 45px;">
							<li>Jumlah</li>
							<li>Jenis Satuan</li>
							<li>Berat Bersih (Kg)</li>
						</ul></td>
						<td style="width: 10%; padding-left: 16px;">39. Jumlah Nilai CIF</td>
					</tr>
				</thead>
				<tbody>
			<?php
				if(isset($barang) && sizeof($barang) > 0) {
					for ($i=0; $i < sizeof($barang); $i++) { ?>
						<tr>
							<td style="border-top: 0px; border-bottom: 0px; text-align: center;"><?php echo ($i+1); ?></td>
							<td style="border-top: 0px; border-bottom: 0px;"><ul style="padding-left: 15px;">
								<li><?php
									if(isset($barang[$i]['POS_TARIF'])) echo trim($barang[$i]['POS_TARIF']);
									if(isset($barang[$i]['SERI_POS_TARIF'])) echo ' '.trim($barang[$i]['SERI_POS_TARIF']);
								?></li>
								<li><?php
									if(isset($barang[$i]['KODE_BARANG'])) echo trim($barang[$i]['KODE_BARANG']);
								?></li>
								<li><?php
									if(isset($barang[$i]['URAIAN'])) echo trim($barang[$i]['URAIAN']);
									if(isset($barang[$i]['MERK'])) echo ', '.trim($barang[$i]['MERK']);
									if(isset($barang[$i]['TIPE'])) echo ', '.trim($barang[$i]['TIPE']);
									if(isset($barang[$i]['UKURAN'])) echo ', '.trim($barang[$i]['UKURAN']);
									if(isset($barang[$i]['SPESIFIKASI_LAIN'])) echo ', '.trim($barang[$i]['SPESIFIKASI_LAIN']);
								?></li>
								<li><?php
									if(isset($barang[$i]['JUMLAH_KEMASAN'])) echo trim($barang[$i]['JUMLAH_KEMASAN']);
									if(isset($barang[$i]['KODE_KEMASAN'])) echo ', '.trim($barang[$i]['KODE_KEMASAN']);
								?></li>
								<!-- <li><?php
									if(isset($barang[$i]['JUMLAH_KEMASAN'])) echo trim($barang[$i]['JUMLAH_KEMASAN']);
								?></li>
								<li><?php
									if(isset($barang[$i]['JUMLAH_KEMASAN'])) echo trim($barang[$i]['JUMLAH_KEMASAN']);
								?></li> -->
							</ul></td>
							<td style="border-top: 0px; border-bottom: 0px;"><?php
								if(isset($barang[$i]['KATEGORI_BARANG'])) echo trim($barang[$i]['KATEGORI_BARANG']);
							?></td>
							<td style="border-top: 0px; border-bottom: 0px;"><?php
								if(isset($barang[$i]['KODE_NEGARA_ASAL'])) echo trim($barang[$i]['KODE_NEGARA_ASAL']);
							?></td>
							<td style="border-top: 0px; border-bottom: 0px;"><ul style="padding-left: 15px;">
						<?php
							foreach ($barangtarif as $btk => $btv) {
								if((int)$barang[$i]['SERI_BARANG'] == (int)$btv['SERI_BARANG']) { ?>
									<li><?php
										echo $btv['JENIS_TARIF'].': '.$btv['TARIF'].', '.$btv['TARIF_FASILITAS'].', '.$btv['KODE_FASILITAS'];
									?></li>
						<?php
								}
							}
						?></ul></td>
							<td style="border-top: 0px; border-bottom: 0px;"><ul style="padding-left: 15px;">
								<li><?php
									if(isset($barang[$i]['JUMLAH_SATUAN'])) echo number_format((int)$barang[$i]['JUMLAH_SATUAN'], 4);
								?></li>
								<li><?php
									if(isset($barang[$i]['KODE_SATUAN'])) echo trim($barang[$i]['KODE_SATUAN']);
								?></li>
								<li><?php
									if(isset($barang[$i]['NETTO'])) echo number_format((int)$barang[$i]['NETTO'], 4);
								?></li>
							</ul></td>
							<td style="border-top: 0px; border-bottom: 0px; text-align: right;"><?php
								if(isset($barang[$i]['CIF'])) echo number_format($barang[$i]['CIF'], 4);
							?></td>
						</tr>
			<?php
					}
				}else { ?>
					<tr>
						<td style="border-top: 0px; border-bottom: 0px; text-align: center;"></td>
						<td style="border-top: 0px; border-bottom: 0px;"></td>
						<td style="border-top: 0px; border-bottom: 0px;"></td>
						<td style="border-top: 0px; border-bottom: 0px;"></td>
						<td style="border-top: 0px; border-bottom: 0px;"></td>
						<td style="border-top: 0px; border-bottom: 0px;"></td>
						<td style="border-top: 0px; border-bottom: 0px; text-align: right;"></td>
					</tr>
			<?php
				}
			?>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
				<tr>
					<td>
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
							<thead>
								<tr>
									<td style="border: 0px;" colspan="2">C. PENGESAHAN PENGUSAHA TPB</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="border: 0px;" colspan="2">Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberitahukan dalam pemberitahuan pabean ini</td>
								</tr>
								<tr>
									<td style="width: 15%; border: 0px;">Tempat, Tanggal</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['KOTA_TTD'])) echo strtoupper($header[0]['KOTA_TTD']);
										echo ", ";
										if(isset($header[0]['TANGGAL_TTD'])) echo date('d F Y', strtotime($header[0]['TANGGAL_TTD']));
									?></td>
								</tr>
								<tr>
									<td style="width: 15%; border: 0px;">Nama Lengkap</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['NAMA_TTD'])) echo $header[0]['NAMA_TTD'];
									?></td>
								</tr>
								<tr>
									<td style="width: 15%; border: 0px;">Jabatan</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['JABATAN_TTD'])) echo $header[0]['JABATAN_TTD'];
									?></td>
								</tr>
								<tr>
									<td style="border: 0px; height: 150px;" colspan="2">Tanda Tangan dan Stempel Perusahaan :</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<!-- End Lembar 2 -->

<!-- Start Lembar 3 -->
<div class="card-box" style="margin-top: 20px;">
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border: 0px;">
				<tr style="font-size: 20px;">
					<th style="border: 0px; padding: 0px;" class="dt-body-center">LEMBAR LAMPIRAN<br/><?php
						if(isset($bc[0]['desc_bc'])) echo strtoupper($bc[0]['desc_bc']);
					?><br/>UNTUK DOKUMEN DAN SKEP/PERSETUJUAN</th>
				</tr>
				<tr>
					<th style="border: 0px; padding: 0px;" class="dt-body-right"><?php
						if(isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']);
					?></th>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<tr>
					<td style="border: 0px; width: 15%;">Nomor Pengajuaan</td>
					<td style="border: 0px;">: &nbsp; <?php if(isset($bc[0]['no_pengajuan'])) echo $bc[0]['no_pengajuan']; ?></td>
					<th style="border: 0px; width: 15%;" rowspan="2">Halaman Ke-3 dari 3</th>
				</tr>
				<tr>
					<td style="border: 0px; width: 15%;">Nomor Pendaftaran</td>
					<td style="border: 0px;">: &nbsp; <?php if(isset($bc[0]['no_pendaftaran'])) echo $bc[0]['no_pendaftaran']; ?></td>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<td class="dt-body-center">Jenis Dokumen</td>
						<td class="dt-body-center">Nomor Dokumen</td>
						<td class="dt-body-center">Tanggal Dokumen</td>
					</tr>
				</thead>
				<tbody>
			<?php
				if(isset($dokumen) && sizeof($dokumen) > 0) {
					foreach ($dokumen as $dkk => $dkv) {?>
						<tr>
							<td style="border-top: 0px; border-bottom: 0px;"><?php echo trim($dkv['KODE_JENIS_DOKUMEN']); ?></td>
							<td style="border-top: 0px; border-bottom: 0px;"><?php echo trim($dkv['NOMOR_DOKUMEN']); ?></td>
							<td style="border-top: 0px; border-bottom: 0px;" class="dt-body-center"><?php echo date('d-m-Y', strtotime($dkv['TANGGAL_DOKUMEN'])); ?></td>
						</tr>
			<?php
					}
				}else { ?>
					<tr>
						<td style="border-top: 0px; border-bottom: 0px;"></td>
						<td style="border-top: 0px; border-bottom: 0px;"></td>
						<td style="border-top: 0px; border-bottom: 0px;" class="dt-body-center"></td>
					</tr>
			<?php
				}
			?>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
				<tr>
					<td>
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
							<thead>
								<tr>
									<td style="border: 0px;" colspan="2">C. PENGESAHAN PENGUSAHA TPB</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="border: 0px;" colspan="2">Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberitahukan dalam pemberitahuan pabean ini</td>
								</tr>
								<tr>
									<td style="width: 15%; border: 0px;">Tempat, Tanggal</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['KOTA_TTD'])) echo strtoupper($header[0]['KOTA_TTD']);
										echo ", ";
										if(isset($header[0]['TANGGAL_TTD'])) echo date('d F Y', strtotime($header[0]['TANGGAL_TTD']));
									?></td>
								</tr>
								<tr>
									<td style="width: 15%; border: 0px;">Nama Lengkap</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['NAMA_TTD'])) echo $header[0]['NAMA_TTD'];
									?></td>
								</tr>
								<tr>
									<td style="width: 15%; border: 0px;">Jabatan</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($header[0]['JABATAN_TTD'])) echo $header[0]['JABATAN_TTD'];
									?></td>
								</tr>
								<tr>
									<td style="border: 0px; height: 150px;" colspan="2">Tanda Tangan dan Stempel Perusahaan :</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<!-- End Lembar 3 -->

<script type="text/javascript">
	var id_bc = "<?php if(isset($bc[0])){ echo $bc[0]['id']; } ?>";

	$(document).ready(function() {
		$('#back_bc').on('click', function() {
			$('#title_Menu').html('Bea Cukai');
			$('#div_list_bc').show();
			$('#div_detail_bc').hide();
			$('#div_detail_bc').html('');
		})

		$('#downloadDetail').on('click', function() {

		})

		$('#tanggal_bc11').datepicker({
			format: "dd-M-yyyy",
			autoclose: true,
			todayHighlight: true
		});
		$('#tanggal_ttd').datepicker({
			format: "dd-M-yyyy",
			autoclose: true,
			todayHighlight: true
		});
	});
</script>