<style>
	.col-customer {
		border: solid 1px #b2b8b7;
	}

	.dt-body-left {
		text-align: left;
		vertical-align: middle;
	}

	.dt-body-right {
		text-align: right;
		vertical-align: middle;
	}

	.dt-body-center {
		text-align: center;
		vertical-align: middle;
	}

	img {
		max-width: 85%;
		height: auto;
	}
</style>

<div id="print-area">
	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download Engineering Specification Form" id="btn_download" onclick="htmlToExcel('export_table[]', 'xlsx', 'REPORT BC MAINTENANCE')">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<table id="TablePrintHeaderPage1" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td colspan="4">
							<h4 id="titleCelebit" style="text-align:center;">PEMBERITAHUAN PENGELUARAN BARANG DARI</h4>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<h4 id="titlePerusahaan" style="text-align:center;">TEMPAT PENIMBUNAN BERIKAT DENGAN JAMINAN</h4>
						</td>
					</tr>
					<tr>
						<td colspan="4" style="text-align: right;"><?php echo $detail[0]['jenis_bc'] ?></td>
					</tr>
					<tr>
						<td width="25%">Kantor Pabean : </td>
						<td width="25%"></td>
						<td width="25%"><?php if (sizeof($report_data_header)>1) echo $report_data_header['KPPBC'] ?></td>
						<td width="25%"></td>
					</tr>
					<tr>
						<td>Nomor Pengajuan : </td>
						<td><?php echo $detail[0]['no_pengajuan']; ?></td>
					</tr>
					<tr>
						<td>TUJUAN PENGIRIMAN : </td>
						<td><?php if (sizeof($report_data_header)>1) echo $report_data_header['KODE_TUJUAN_PENGIRIMAN']; ?></td>
						<td colspan="2">1. Diperbaiki 2.Disubkontraktorkan 3.Dipinjamkan 4.Lainnya</td>
					</tr>
				</tbody>
			</table>
			<table id="tablePrintTopPage1" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td colspan="2">B. Data Pemberitahuan</td>
					</tr>
					<tr>
						<td>Pengusaha TPB
							<br>1. NPWP : <?php if (sizeof($report_data_header)>1) echo $report_data_header['NPWP_BILLING'] ?>
							<br>2. Nama :<?php if (sizeof($report_data_header)>1) echo $report_data_header['ID_PENGUSAHA'] ?>
							<br>3. Alamat : <?php if (sizeof($report_data_header)>1) echo $report_data_header['ALAMAT_PENGUSAHA'] ?>
							<br>4. Nomor dan Tanggal Izin TPB : <?php if (sizeof($report_data_header)>1) echo $report_data_header['NOMOR_IJIN_TPB'] ?> Tanggal : <?php if (sizeof($report_data_header)>1) echo $report_data_header['TANGGAL_IJIN_TPB'] ?>
						</td>
						<td>D. DIISI OLEH BEA DAN CUKAI
							<br>Nomor Pendaftaran : <?php echo $detail[0]['no_pendaftaran'] ?>
							<br>Tanggal :
						</td>
					</tr>
					<tr>
						<td>Penerima Barang
							<br>5. NPWP :
							<br>6. Nama : <?php if (sizeof($report_data_header)>1) echo $report_data_header['ID_PENERIMA_BARANG'] ?>
							<br>7. Alamat : <?php if (sizeof($report_data_header)>1) echo $report_data_header['ALAMAT_PENERIMA_BARANG'] ?>
						</td>
						<td>Dokumen Pelengkap Pabean
							<br>8. Packing List No : Tgl :
							<br>9. Pemenuhan Persyaratan/Fasilitas Impor
							<br>No : Tgl:
							<br>10. Surat Keputusan Dokumen Lainnya :
							<br>No : Tgl:
						</td>
					</tr>
					<tr>
						<td colspan="2">11. Valuta : <?php if (sizeof($report_data_header)>1) echo $report_data_header['KODE_VALUTA'] ?>
							<br>12. NDPBM : <?php if (sizeof($report_data_header)>1) echo $report_data_header['NDPBM'] ?>
							<br>13. Nilai CIF : <?php if (sizeof($report_data_header)>1) echo $report_data_header['CIF'] ?>
						</td>
					</tr>
					<tr>
						<td colspan="2">14. Jenis Sarana Pengangkut : <?php if (sizeof($report_data_header)>1) echo $report_data_header['NAMA_PENGANGKUT'] ?>
						</td>
					</tr>
					<tr>
						<td>15. Nomor, Ukuran dan Tipe Peti Kemas &nbsp; 16. Jumlah, Jenis dan Merek Kemasan 1 0 Unpacked or unpackaged
						</td>
						<td>17. Berat Kotor (kg) :
							<br><?php if (sizeof($report_data_header)>1) echo $report_data_header['BRUTO'] ?>
							<br>18. Berat Bersih (kg) :
							<br><?php if (sizeof($report_data_header)>1) echo $report_data_header['NETTO'] ?>
						</td>
					</tr>
				</tbody>
			</table>
			<table id="tablePrintBottomPage1" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td>19<br>No.</td>
						<td>20
							<br>-Pos Tarif/HS
							<br>Uraian Jumlah Barang Secara Lengkap, Merek, Tipe, Ukuran dan Spesifikasi Lain.
						</td>
						<td>21
							<br>Negara
							<br>Asal Barang
						</td>
						<td>22 Tarif dan Fasilitas
							<br>BM, BMT, Cukai
							<br>PPN, PPnBM,
							<br>PPh
						</td>
						<td>23 Jumlah dan
							<br>Jenis Satuan
							<br>Berat Bersih (kg)
						</td>
						<td>24 Nilai CIF
						</td>
					</tr>
					<tr>
						<td>1</td>
						<td>
							<br>- Pos Tarif/HS :
							<br>- Kode Barang : TTD
							<br>- SOFT TOOLING DN24C336-42, Merk: , Tipe:
							<br>- 0 Unpacked or unpackaged (NE)
						</td>
						<td><?php if (sizeof($report_data_header)>1) echo $report_data_header['KODE_NEGARA_PENGIRIM'] ?></td>
						<td></td>
						<td>Satuan : 1,0000
							<br>NIU(Number Of International Units)
							<br>Berat Bersih :
							<br>135,50000
						</td>
						<td><?php if (sizeof($report_data_header)>1) echo $report_data_header['CIF_RUPIAH'] ?></td>
					</tr>
				</tbody>
			</table>

			<table id="tablePrintLastBottomPage1" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td colspan="2">Data Perhitungan Jaminan</td>
						<td colspan="2">Data Jaminan</td>
					</tr>
					<tr>
						<td>Jenis Pungutan</td>
						<td>Jumlah</td>
						<td colspan="2">
							32. Jaminan :
						</td>
					</tr>
					<tr>
						<td>25.Bea Masuk</td>
						<td></td>
						<td colspan="2">33. Nomor Jaminan : &nbsp; Tanggal :</td>
					</tr>
					<tr>
						<td>26.Bea Masuk Tambahan</td>
						<td></td>
						<td colspan="2">34. Nilai Jaminan :
					</tr>
					<tr>
						<td>27.Cukai</td>
						<td></td>
						<td colspan="2">35. Tanggal Jatuh Tempo : <?php if (sizeof($report_data_header)>1) echo $report_data_header['TANGGAL_JATUH_TEMPO'] ?>
					</tr>
					<tr>
						<td>28.PPN</td>
						<td></td>
						<td colspan="2">36. Penjamin :
					</tr>
					<tr>
						<td>29.PPn BM</td>
						<td></td>
						<td colspan="2">37. Nomor dan tanggal :
					</tr>
					<tr>
						<td>30.PPh</td>
						<td></td>
						<td colspan="2">Bukti Penerimaan Jaminan : &nbsp; Tanggal :</td>
					</tr>
					<tr>
						<td>31.Jumlah Total</td>
						<td></td>
						<td colspan="2"></td>
					</tr>
					<tr>
						<td colspan="3">
							C. Pengesahan Pengusaha TPB
							<br>Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberitahukan
							<br>dalam pemberitahuan pabean ini.
							<br>Tempat, Tanggal :
							<br>Nama Lengkap :<?php if (sizeof($report_data_header)>1) echo $report_data_header['NAMA_PENERIMA_BARANG'] ?>
							<br>Jabatan :
							<br>Tanda Tangan dan Stempel Perusahaan :
						</td>
						<td>
							E. Untuk Pejabat BEA DAN CUKAI
						</td>
					</tr>
				</tbody>
			</table>

			<table id="tablePrintTopPage2" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td>No</td>
						<td>Jenis Dokumen</td>
						<td>Nomor Dokumen</td>
						<td>Tanggal Dokumen</td>
					</tr>
					<tr>
						<td>1</td>
						<td>INVOICE</td>
						<td>BDG 002</td>
						<td>08-02-2019</td>
					</tr>
					<tr>
						<td>2</td>
						<td>PACKING LIST</td>
						<td>BDG 002</td>
						<td>08-02-2019</td>
					<tr>
						<td>3</td>
						<td>Persetujuan Pengeluaran Barang Modal Untuk
							<br>Perbaikan/Reparasi Ke TLDDP
						</td>
						<td>S-435/WBC.09/KPP.MP.04/2019</td>
						<td>07-02-2019</td>
					</tr>
					<tr>
						<td>4</td>
						<td>BC 4.0</td>
						<td>079096</td>
						<td>04-12-2018</td>
					</tr>
					<tr>
						<td>5</td>
						<td>KONTRAK</td>
						<td>27/SE/02/2019</td>
						<td>01-02-2019</td>
					</tr>
					<tr>
						<td colspan="4">
							C. Pengesahan Pengusaha TPB
							<br>Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberitahukan dalam pemberitahuan pabean ini
							<br>Tempat, Tanggal :<?php if (sizeof($report_data_header)>1) echo $report_data_header['KOTA_TTD'] . " , " ?> <?php if (sizeof($report_data_header)>1) echo $report_data_header['TANGGAL_TTD'] ?>
							<br>Nama Lengkap : <?php if (sizeof($report_data_header)>1) echo $report_data_header['NAMA_TTD'] ?>
							<br>Jabatan :<?php if (sizeof($report_data_header)>1) echo $report_data_header['JABATAN_TTD'] ?>
							<br>Tanda Tangan dan Stempel Perusahaan :
						</td>
					</tr>
				</tbody>
			</table>
			<table id="Header" name="export_table[]" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="display:none">
				<tbody>
					<tr>
						<td> NOMOR AJU </td>
						<td> KPPBC </td>
						<td> PERUSAHAAN </td>
						<td> PEMASOK </td>
						<td> STATUS </td>
						<td> KODE DOKUMEN PABEAN </td>
						<td> NPPJK </td>
						<td> ALAMAT PEMASOK </td>
						<td> ALAMAT PEMILIK </td>
						<td> ALAMAT PENERIMA BARANG </td>
						<td> ALAMAT PENGIRIM </td>
						<td> ALAMAT PENGUSAHA </td>
						<td> ALAMAT PPJK </td>
						<td> API PEMILIK </td>
						<td> API PENERIMA </td>
						<td> API PENGUSAHA </td>
						<td> ASAL DATA </td>
						<td> ASURANSI </td>
						<td> BIAYA TAMBAHAN </td>
						<td> BRUTO </td>
						<td> CIF </td>
						<td> CIF RUPIAH </td>
						<td> DISKON </td>
						<td> FLAG PEMILIK </td>
						<td> URL DOKUMEN PABEAN </td>
						<td> FOB </td>
						<td> FREIGHT </td>
						<td> HARGA BARANG LDP </td>
						<td> HARGA INVOICE </td>
						<td> HARGA PENYERAHAN </td>
						<td> HARGA TOTAL </td>
						<td> ID MODUL </td>
						<td> ID PEMASOK </td>
						<td> ID PEMILIK </td>
						<td> ID PENERIMA BARANG </td>
						<td> ID PENGIRIM </td>
						<td> ID PENGUSAHA </td>
						<td> ID PPJK </td>
						<td> JABATAN TTD </td>
						<td> JUMLAH BARANG </td>
						<td> JUMLAH KEMASAN </td>
						<td> JUMLAH KONTAINER </td>
						<td> KESESUAIAN DOKUMEN </td>
						<td> KETERANGAN </td>
						<td> KODE ASAL BARANG </td>
						<td> KODE ASURANSI </td>
						<td> KODE BENDERA </td>
						<td> KODE CARA ANGKUT </td>
						<td> KODE CARA BAYAR </td>
						<td> KODE DAERAH ASAL </td>
						<td> KODE FASILITAS </td>
						<td> KODE FTZ </td>
						<td> KODE HARGA </td>
						<td> KODE ID PEMASOK </td>
						<td> KODE ID PEMILIK </td>
						<td> KODE ID PENERIMA BARANG </td>
						<td> KODE ID PENGIRIM </td>
						<td> KODE ID PENGUSAHA </td>
						<td> KODE ID PPJK </td>
						<td> KODE JENIS API </td>
						<td> KODE JENIS API PEMILIK </td>
						<td> KODE JENIS API PENERIMA </td>
						<td> KODE JENIS API PENGUSAHA </td>
						<td> KODE JENIS BARANG </td>
						<td> KODE JENIS BC25 </td>
						<td> KODE JENIS NILAI </td>
						<td> KODE JENIS PEMASUKAN01 </td>
						<td> KODE JENIS PEMASUKAN 02 </td>
						<td> KODE JENIS TPB </td>
						<td> KODE KANTOR BONGKAR </td>
						<td> KODE KANTOR TUJUAN </td>
						<td> KODE LOKASI BAYAR </td>
						<td> KODE NEGARA PEMASOK </td>
						<td> KODE NEGARA PENGIRIM </td>
						<td> KODE NEGARA PEMILIK </td>
						<td> KODE NEGARA TUJUAN </td>
						<td> KODE PEL BONGKAR </td>
						<td> KODE PEL MUAT </td>
						<td> KODE PEL TRANSIT </td>
						<td> KODE PEMBAYAR </td>
						<td> KODE STATUS PENGUSAHA </td>
						<td> STATUS PERBAIKAN </td>
						<td> KODE TPS </td>
						<td> KODE TUJUAN PEMASUKAN </td>
						<td> KODE TUJUAN PENGIRIMAN </td>
						<td> KODE TUJUAN TPB </td>
						<td> KODE TUTUP PU </td>
						<td> KODE VALUTA </td>
						<td> KOTA TTD </td>
						<td> NAMA PEMILIK </td>
						<td> NAMA PENERIMA BARANG </td>
						<td> NAMA PENGANGKUT </td>
						<td> NAMA PENGIRIM </td>
						<td> NAMA PPJK </td>
						<td> NAMA TTD </td>
						<td> NDPBM </td>
						<td> NETTO </td>
						<td> NILAI INCOTERM </td>
						<td> NIPER PENERIMA </td>
						<td> NOMOR API </td>
						<td> NOMOR BC11 </td>
						<td> NOMOR BILLING </td>
						<td> NOMOR DAFTAR </td>
						<td> NOMOR IJIN BPK PEMASOK </td>
						<td> NOMOR IJIN BPK PENGUSAHA </td>
						<td> NOMOR IJIN TPB </td>
						<td> NOMOR IJIN TPB PENERIMA </td>
						<td> NOMOR VOYV FLIGHT </td>
						<td> NPWP BILLING </td>
						<td> POS BC11 </td>
						<td> SERI </td>
						<td> SUBPOS BC11 </td>
						<td> SUB SUBPOS BC11 </td>
						<td> TANGGAL BC11 </td>
						<td> TANGGAL BERANGKAT </td>
						<td> TANGGAL BILLING </td>
						<td> TANGGAL DAFTAR </td>
						<td> TANGGAL IJIN BPK PEMASOK </td>
						<td> TANGGAL IJIN BPK PENGUSAHA </td>
						<td> TANGGAL IJIN TPB </td>
						<td> TANGGAL NPPPJK </td>
						<td> TANGGAL TIBA </td>
						<td> TANGGAL TTD </td>
						<td> TANGGAL JATUH TEMPO </td>
						<td> TOTAL BAYAR </td>
						<td> TOTAL BEBAS </td>
						<td> TOTAL DILUNASI </td>
						<td> TOTAL JAMIN </td>
						<td> TOTAL SUDAH DILUNASI </td>
						<td> TOTAL TANGGUH </td>
						<td> TOTAL TANGGUNG </td>
						<td> TOTAL TIDAK DIPUNGUT </td>
						<td> URL DOKUMEN PABEAN </td>
						<td> VERSI MODUL </td>
						<td> VOLUME </td>
						<td> WAKTU BONGKAR </td>
						<td> WAKTU STUFFING </td>
						<td> NOMOR POLISI </td>
						<td> CALL SIGN </td>
						<td> JUMLAH TANDA PENGAMAN </td>
						<td> KODE JENIS TANDA PENGAMAN </td>
						<td> KODE KANTOR MUAT </td>
						<td> KODE PEL TUJUAN </td>
						<td> TANGGAL STUFFING </td>
						<td> TANGGAL MUAT </td>
						<td> KODE GUDANG ASAL </td>
						<td> KODE GUDANG TUJUAN </td>
					</tr>
					<?php
					foreach ($report_data_list_header as $row) {
						// var_dump($row); 
						unset($row['id_report_header']);
						// var_dump($row); 
						?>
						<tr>
							<?php
								echo "<td>" . implode("</td><td>", $row) . "</td>";
								?>
						</tr>
					<?php
					}
					?>
				</tbody>
			</table>
			<table id="BahanBaku" name="export_table[]" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="display:none">
				<tbody>
					<tr>
						<td> NOMOR AJU </td>
						<td> SERI BARANG </td>
						<td> SERI BAHAN BAKU </td>
						<td> CIF </td>
						<td> CIF RUPIAH </td>
						<td> HARGA PENYERAHAN </td>
						<td> HARGA PEROLEHAN </td>
						<td> JENIS SATUAN </td>
						<td> JUMLAH SATUAN </td>
						<td> KODE ASAL BAHAN BAKU </td>
						<td> KODE BARANG </td>
						<td> KODE FASILITAS </td>
						<td> KODE JENIS DOK ASAL </td>
						<td> KODE KANTOR </td>
						<td> KODE SKEMA TARIF </td>
						<td> KODE STATUS </td>
						<td> MERK </td>
						<td> NDPBM </td>
						<td> NETTO </td>
						<td> NOMOR AJU DOKUMEN ASAL </td>
						<td> NOMOR DAFTAR DOKUMEN ASAL </td>
						<td> POS TARIF </td>
						<td> SERI BARANG DOKUMEN ASAL </td>
						<td> SPESIFIKASI LAIN </td>
						<td> TANGGAL DAFTAR DOKUMEN ASAL </td>
						<td> TIPE </td>
						<td> UKURAN </td>
						<td> URAIAN </td>
						<td> SERI IJIN </td>
					</tr>
					<?php
					foreach ($report_data_bahanbaku as $row) {
						// var_dump($row); 
						unset($row['id_report_bahanbaku']);
						// var_dump($row); 
						?>
						<tr>
							<?php
								echo "<td>" . implode("</td><td>", $row) . "</td>";
								?>
						</tr>
					<?php
					}
					?>
				</tbody>
			</table>
			<table id="BahanBakuTarif" name="export_table[]" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="display:none">
				<tbody>
					<tr>
						<td> NOMOR AJU </td>
						<td> SERI BARANG </td>
						<td> SERI BAHAN BAKU </td>
						<td> JENIS TARIF </td>
						<td> JUMLAH SATUAN </td>
						<td> KODE ASAL BAHAN BAKU </td>
						<td> KODE FASILITAS </td>
						<td> KODE KOMODITI CUKAI </td>
						<td> KODE SATUAN </td>
						<td> KODE TARIF </td>
						<td> NILAI BAYAR </td>
						<td> NILAI FASILITAS </td>
						<td> NILAI SUDAH DILUNASI </td>
						<td> TARIF </td>
						<td> TARIF FASILITAS </td>
					</tr>

					<?php
					foreach ($report_data_bahanbakutarif as $row) {
						// var_dump($row); 
						unset($row['id_report_bahanbakutarif']);
						// var_dump($row); 
						?>
						<tr>
							<?php
								echo "<td>" . implode("</td><td>", $row) . "</td>";
								?>
						</tr>
					<?php
					}
					?>
				</tbody>
			</table>
			<table id="BahanBakuDokumen" name="export_table[]" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="display:none">
				<tbody>
					<tr>
						<td> NOMOR AJU </td>
						<td> SERI BARANG </td>
						<td> SERI BAHAN BAKU </td>
						<td> SERI DOKUMEN </td>
						<td> KODE ASAL BAHAN BAKU </td>
					</tr>
					<?php
					foreach ($report_data_bahanbakudokumen as $row) {
						// var_dump($row); 
						unset($row['id_report_bahanbakudokumen']);
						// var_dump($row); 
						?>
						<tr>
							<?php
								echo "<td>" . implode("</td><td>", $row) . "</td>";
								?>
						</tr>
					<?php
					}
					?>
				</tbody>
			</table>
			<table id="Barang" name="export_table[]" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="display:none">
				<tbody>
					<tr>
						<td> NOMOR AJU </td>
						<td> SERI BARANG </td>
						<td> ASURANSI </td>
						<td> CIF </td>
						<td> CIF RUPIAH </td>
						<td> DISKON </td>
						<td> FLAG KENDARAAN </td>
						<td> FOB </td>
						<td> FREIGHT </td>
						<td> BARANG BARANG LDP </td>
						<td> HARGA INVOICE </td>
						<td> HARGA PENYERAHAN </td>
						<td> HARGA SATUAN </td>
						<td> JENIS KENDARAAN </td>
						<td> JUMLAH BAHAN BAKU </td>
						<td> JUMLAH KEMASAN </td>
						<td> JUMLAH SATUAN </td>
						<td> KAPASITAS SILINDER </td>
						<td> KATEGORI BARANG </td>
						<td> KODE_ASAL BARANG </td>
						<td> KODE BARANG </td>
						<td> KODE FASILITAS </td>
						<td> KODE GUNA </td>
						<td> KODE JENIS NILAI </td>
						<td> KODE KEMASAN </td>
						<td> KODE LEBIH DARI 4 TAHUN </td>
						<td> KODE NEGARA ASAL </td>
						<td> KODE SATUAN </td>
						<td> KODE SKEMA TARIF </td>
						<td> KODE STATUS </td>
						<td> KONDISI BARANG </td>
						<td> MERK </td>
						<td> NETTO </td>
						<td> NILAI INCOTERM </td>
						<td> NILAI PABEAN </td>
						<td> NOMOR MESIN </td>
						<td> POS TARIF </td>
						<td> SERI POS TARIF </td>
						<td> SPESIFIKASI LAIN </td>
						<td> TAHUN PEMBUATAN </td>
						<td> TIPE </td>
						<td> UKURAN </td>
						<td> URAIAN </td>
						<td> VOLUME </td>
						<td> SERI IJIN </td>
						<td> ID EKSPORTIR </td>
						<td> NAMA EKSPORTIR </td>
						<td> ALAMAT EKSPORTIR </td>
						<td> KODE PERHITUNGAN </td>
					</tr>
					<?php
					foreach ($report_data_barang as $row) {
						// var_dump($row); 
						unset($row['id_report_barang']);
						// var_dump($row); 
						?>
						<tr>
							<?php
								echo "<td>" . implode("</td><td>", $row) . "</td>";
								?>
						</tr>
					<?php
					}
					?>
				</tbody>
			</table>
			<table id="BarangTarif" name="export_table[]" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="display:none">
				<tbody>
					<tr>
						<td> NOMOR AJU </td>
						<td> SERI BARANG </td>
						<td> JENIS TARIF </td>
						<td> JUMLAH SATUAN </td>
						<td> KODE FASILITAS </td>
						<td> KODE KOMODITI CUKAI </td>
						<td> TARIF KODE SATUAN </td>
						<td> TARIF KODE TARIF </td>
						<td> TARIF NILAI BAYAR </td>
						<td> TARIF NILAI FASILITAS </td>
						<td> TARIF NILAI SUDAH DILUNASI </td>
						<td> TARIF </td>
						<td> TARIF FASILITAS </td>
					</tr>
					<?php
					foreach ($report_data_barangtarif as $row) {
						// var_dump($row); 
						unset($row['id_report_barangtarif']);
						// var_dump($row); 
						?>
						<tr>
							<?php
								echo "<td>" . implode("</td><td>", $row) . "</td>";
								?>
						</tr>
					<?php
					}
					?>
				</tbody>
			</table>
			<table id="BarangDokumen" name="export_table[]" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="display:none">
				<tbody>
					<tr>
						<td> NOMOR AJU </td>
						<td> SERI BARANG </td>
						<td> SERI DOKUMEN </td>
					</tr>
					<?php
					foreach ($report_data_barangdokumen as $row) {
						// var_dump($row); 
						unset($row['id_report_barangdokumen']);
						// var_dump($row); 
						?>
						<tr>
							<?php
								echo "<td>" . implode("</td><td>", $row) . "</td>";
								?>
						</tr>
					<?php
					}
					?>
				</tbody>
			</table>
			<table id="Dokumen" name="export_table[]" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="display:none">
				<tbody>
					<tr>
						<td> NOMOR AJU </td>
						<td> SERI DOKUMEN </td>
						<td> FLAG URL DOKUMEN </td>
						<td> KODE JENIS DOKUMEN </td>
						<td> NOMOR DOKUMEN </td>
						<td> TANGGAL DOKUMEN </td>
						<td> TIPE DOKUMEN </td>
						<td> URL DOKUMEN </td>
					</tr>

					<?php
					foreach ($report_data_dokumen as $row) {
						// var_dump($row); 
						unset($row['id_report_dokumen']);
						// var_dump($row); 
						?>
						<tr>
							<?php
								echo "<td>" . implode("</td><td>", $row) . "</td>";
								?>
						</tr>
					<?php
					}
					?>
				</tbody>
			</table>
			<table id="Kemasan" name="export_table[]" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="display:none">
				<tbody>
					<tr>
						<td> NOMOR AJU </td>
						<td> SERI KEMASAN </td>
						<td> JUMLAH KEMASAN </td>
						<td> KESESUAIAN DOKUMEN </td>
						<td> KETERANGAN </td>
						<td> KODE JENIS KEMASAN </td>
						<td> MEREK KEMASAN </td>
						<td> NIP GATE IN </td>
						<td> NIP GATE OUT </td>
						<td> NOMOR POLISI </td>
						<td> NOMOR SEGEL </td>
						<td> WAKTU GATE IN </td>
						<td> WAKTU GATE OUT </td>
					<tr>
						<?php
						foreach ($report_data_kemasan as $row) {
							// var_dump($row); 
							unset($row['id_report_kemasan']);
							// var_dump($row); 
							?>
					<tr>
						<?php
							echo "<td>" . implode("</td><td>", $row) . "</td>";
							?>
					</tr>
				<?php
				}
				?>
				</tbody>
			</table>
			<table id="Kontainer" name="export_table[]" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="display:none">
				<tbody>
					<tr>
						<td> NOMOR AJU </td>
						<td> SERI KONTAINER </td>
						<td> KESESUAIAN DOKUMEN </td>
						<td> KETERANGAN </td>
						<td> KODE STUFFING </td>
						<td> KODE TIPE KONTAINER </td>
						<td> KODE UKURAN KONTAINER </td>
						<td> FLAG GATE IN </td>
						<td> FLAG GATE OUT </td>
						<td> NOMOR POLISI </td>
						<td> NOMOR KONTAINER </td>
						<td> NOMOR SEGEL </td>
						<td> WAKTU GATE IN </td>
						<td> WAKTU GATE OUT </td>
					</tr>

					<?php
					foreach ($report_data_kontainer as $row) {
						// var_dump($row); 
						unset($row['id_report_kontainer']);
						// var_dump($row); 
						?>
						<tr>
							<?php
								echo "<td>" . implode("</td><td>", $row) . "</td>";
								?>
						</tr>
					<?php
					}
					?>
				</tbody>
			</table>
			<table id="Jaminan" name="export_table[]" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="display:none">
				<tbody>
					<tr>
						<td> NOMOR AJU </td>
						<td> KODE JENIS JAMINAN </td>
						<td> KODE KANTOR </td>
						<td> NILAI JAMINAN </td>
						<td> NOMOR BPJ </td>
						<td> NOMOR JAMINAN </td>
						<td> PENJAMIN </td>
						<td> TANGGAL BPJ </td>
						<td> TANGGAL JAMINAN </td>
						<td> TANGGAL JATUH TEMPO </td>
					</tr>
					<?php
					foreach ($report_data_kontainer as $row) {
						// var_dump($row); 
						unset($row['id_report_jaminan']);
						// var_dump($row); 
						?>
						<tr>
							<?php
								echo "<td>" . implode("</td><td>", $row) . "</td>";
								?>
						</tr>
					<?php
					}
					?>
				</tbody>
			</table>
			<table id="Respon" name="export_table[]" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="display:none">
				<tbody>
					<tr>
						<td> NOMOR AJU </td>
						<td> KODE RESPON </td>
						<td> NOMOR RESPON </td>
						<td> TANGGAL RESPON </td>
						<td> WAKTU RESPON </td>
						<td> BYTE STRAM PDF </td>
					</tr>
					<?php
					foreach ($report_data_respon as $row) {
						// var_dump($row); 
						unset($row['id_report_respon']);
						// var_dump($row); 
						?>
						<tr>
							<?php
								echo "<td>" . implode("</td><td>", $row) . "</td>";
								?>
						</tr>
					<?php
					}
					?>
				</tbody>
			</table>
			<table id="Status" name="export_table[]" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="display:none">
				<tbody>
					<tr>
						<td> NOMOR AJU </td>
						<td> KODE RESPON </td>
						<td> NOMOR RESPON </td>
					</tr>

					<?php
					foreach ($report_data_status as $row) {
						// var_dump($row); 
						unset($row['id_report_status']);
						// var_dump($row); 
						?>
						<tr>
							<?php
								echo "<td>" . implode("</td><td>", $row) . "</td>";
								?>
						</tr>
					<?php
					}
					?>
				</tbody>
			</table>
			<table id="Pungutan" name="export_table[]" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="display:none">
				<tbody>
					<tr>
						<td> NOMOR AJU </td>
						<td> JENIS TARIF </td>
						<td> KODE FASILITAS </td>
						<td> NILAI PUNGUTAN </td>
					</tr>
					<?php
					foreach ($report_data_pungutan as $row) {
						// var_dump($row); 
						unset($row['id_report_pungutan']);
						// var_dump($row); 
						?>
						<tr>
							<?php
								echo "<td>" . implode("</td><td>", $row) . "</td>";
								?>
						</tr>
					<?php
					}
					?>
				</tbody>
			</table>

		</div>
	</div>
	<iframe id="txtArea1" style="display:none"></iframe>
	<script type="text/javascript" src="https://oss.sheetjs.com/sheetjs/xlsx.full.min.js"></script>
	<script type="text/javascript">
		var dataImage = null;

		function terbilangIND() {
			var bilangan = document.getElementById("totHide").value;
			var kalimat = "";
			var angka = new Array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
			var kata = new Array('', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan');
			var tingkat = new Array('', 'Ribu', 'Juta', 'Milyar', 'Triliun');
			var panjang_bilangan = bilangan.length;

			/* pengujian panjang bilangan */
			if (panjang_bilangan > 15) {
				kalimat = "Diluar Batas";
			} else {
				/* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
				for (i = 1; i <= panjang_bilangan; i++) {
					angka[i] = bilangan.substr(-(i), 1);
				}

				var i = 1;
				var j = 0;

				/* mulai proses iterasi terhadap array angka */
				while (i <= panjang_bilangan) {
					subkalimat = "";
					kata1 = "";
					kata2 = "";
					kata3 = "";

					/* untuk Ratusan */
					if (angka[i + 2] != "0") {
						if (angka[i + 2] == "1") {
							kata1 = "Seratus";
						} else {
							kata1 = kata[angka[i + 2]] + " Ratus";
						}
					}

					/* untuk Puluhan atau Belasan */
					if (angka[i + 1] != "0") {
						if (angka[i + 1] == "1") {
							if (angka[i] == "0") {
								kata2 = "Sepuluh";
							} else if (angka[i] == "1") {
								kata2 = "Sebelas";
							} else {
								kata2 = kata[angka[i]] + " Belas";
							}
						} else {
							kata2 = kata[angka[i + 1]] + " Puluh";
						}
					}

					/* untuk Satuan */
					if (angka[i] != "0") {
						if (angka[i + 1] != "1") {
							kata3 = kata[angka[i]];
						}
					}

					/* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
					if ((angka[i] != "0") || (angka[i + 1] != "0") || (angka[i + 2] != "0")) {
						subkalimat = kata1 + " " + kata2 + " " + kata3 + " " + tingkat[j] + " ";
					}

					/* gabungkan variabe sub kalimat (untuk Satu blok 3 angka) ke variabel kalimat */
					kalimat = subkalimat + kalimat;
					i = i + 3;
					j = j + 1;
				}

				/* mengganti Satu Ribu jadi Seribu jika diperlukan */
				if ((angka[5] == "0") && (angka[6] == "0")) {
					kalimat = kalimat.replace("Satu Ribu", "Seribu");
				}
			}

			document.getElementById("totalBilangan").innerHTML = kalimat;
		}

		function terbilangENG() {
			var bilangan = document.getElementById("totHide").value;
			var kalimat = "";
			var angka = new Array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
			var kata = new Array('', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine');
			var kataBelas = new Array('', 'One', 'Twelve', 'Thir', 'Four', 'Fif', 'Six', 'Seven', 'Eigh', 'Nine');
			var Puluh = new Array('', 'One', 'Twen', 'Thir', 'For', 'Fif', 'Six', 'Seven', 'Eigh', 'Nine');
			var tingkat = new Array('', 'Thousand', 'Million', 'Billion', 'Trillion');
			var panjang_bilangan = bilangan.length;

			/* pengujian panjang bilangan */
			if (panjang_bilangan > 15) {
				kalimat = "Diluar Batas";
			} else {
				/* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
				for (i = 1; i <= panjang_bilangan; i++) {
					angka[i] = bilangan.substr(-(i), 1);
				}

				var i = 1;
				var j = 0;

				/* mulai proses iterasi terhadap array angka */
				while (i <= panjang_bilangan) {
					subkalimat = "";
					kata1 = "";
					kata2 = "";
					kata3 = "";

					/* untuk Ratusan */
					if (angka[i + 2] != "0") {
						if (angka[i + 2] == "1") {
							kata1 = "Hundred";
						} else {
							kata1 = kata[angka[i + 2]] + " Hundred";
						}
					}

					/* untuk Puluhan atau Belasan */
					if (angka[i + 1] != "0") {
						if (angka[i + 1] == "1") {
							if (angka[i] == "0") {
								kata2 = "Ten";
							} else if (angka[i] == "1") {
								kata2 = "Eleven";
							} else if (angka[i] == "2") {
								kata2 = "Twelve";
							} else {
								kata2 = kataBelas[angka[i]] + "teen";
							}
						} else {
							kata2 = kataPuluh[angka[i + 1]] + "ty";
						}
					}

					/* untuk Satuan */
					if (angka[i] != "0") {
						if (angka[i + 1] != "1") {
							kata3 = kata[angka[i]];
						}
					}

					/* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
					if ((angka[i] != "0") || (angka[i + 1] != "0") || (angka[i + 2] != "0")) {
						subkalimat = kata1 + " " + kata2 + " " + kata3 + " " + tingkat[j] + " ";
					}

					/* gabungkan variabe sub kalimat (untuk Satu blok 3 angka) ke variabel kalimat */
					kalimat = subkalimat + kalimat;
					i = i + 3;
					j = j + 1;
				}

				/* mengganti Satu Ribu jadi Seribu jika diperlukan */
				if ((angka[5] == "0") && (angka[6] == "0")) {
					kalimat = kalimat.replace("One Thousand", "Thousand");
				}
			}

			document.getElementById("totalBilangan").innerHTML = kalimat;
		}

		function xls(table, title) {
			if (Array.isArray(table)) {
				var uri = 'data:application/vnd.ms-excel;base64,',
					template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>',
					base64 = function(s) {
						return window.btoa(unescape(encodeURIComponent(s)))
					},
					format = function(s, c) {
						return s.replace(/{(\w+)}/g, function(m, p) {
							return c[p];
						})
					}
				ctx = [];
				// console.log(name);
				// console.log(template);
				table.forEach(function(entry) {
					tableObj = document.getElementById(entry)
					ctx.push({
						worksheet: entry,
						table: tableObj.innerHTML
					});
				});
				console.log(ctx);
				// window.download = "filename.xls";
				// window.location.href = uri + base64(format(template, ctx));

				var link = document.createElement("a");
				link.download = title + '.xls';
				newctx = '';
				ctx.forEach(function(ctxEl) {
					newctx = newctx + base64(format(template, ctxEl))
				})
				link.href = uri + base64(format(template, newctx));
				link.click();


			} else {
				var uri = 'data:application/vnd.ms-excel;base64,',
					template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>',
					base64 = function(s) {
						return window.btoa(unescape(encodeURIComponent(s)))
					},
					format = function(s, c) {
						return s.replace(/{(\w+)}/g, function(m, p) {
							return c[p];
						})
					}



				// console.log(name);
				// console.log(template);
				if (!table.nodeType) table = document.getElementById(table)
				var ctx = {
					worksheet: 'Worksheet',
					table: table.innerHTML
				}
				// window.download = "filename.xls";
				// window.location.href = uri + base64(format(template, ctx));

				var link = document.createElement("a");
				link.download = title + '.xls';
				link.href = uri + base64(format(template, ctx));
				link.click();
			}

		}

		function xlsx(table, title) {
			console.log(Array.isArray(table));
			var workbook = XLSX.utils.book_new()
			if (Array.isArray(table)) {
				let a = [];
				table.forEach(function(entry) {
					var config = {
						raw: true,
						type: 'string'
					};
					var tbl = document.getElementById(entry);
					var wb = XLSX.utils.table_to_sheet(tbl, config);
					XLSX.utils.book_append_sheet(workbook, wb, entry);
				})

				XLSX.writeFile(workbook, title + '.xls');
			} else {
				// let a = [];
				var config = {
					raw: true,
					type: 'string',
					cellStyles: true
				};
				// var tbl = document.getElementById(table);
				// var wb = XLSX.utils.table_to_sheet(tbl, config);
				// console.log(wb)
				// a = a.concat(XLSX.utils.sheet_to_json(wb, { header: 1, raw: false })).concat(['']);


				// let worksheet = XLSX.utils.json_to_sheet(a, { skipHeader: true, cellStyles : true });

				// const new_workbook = XLSX.utils.book_new();
				// XLSX.utils.book_append_sheet(new_workbook, worksheet, "worksheet");
				// XLSX.writeFile(new_workbook, title+'.xlsx');
				// var canvas = document.querySelector('#barChart2');
				//creates image
				// var canvasImg = canvas.toDataURL("image/png", 1.0);
				// console.log(canvasImg);
				var tbl = document.getElementById(table);
				console.log(tbl);
				var wb = XLSX.utils.table_to_book(tbl, config);
				// wb.Sheets['my-sheet']['!images'] = [
				// {
				// name: 'image1.jpg',
				// data: canvasImg,
				// opts: { base64: true },
				// position: {
				// type: 'twoCellAnchor',
				// attrs: { editAs: 'oneCell' },
				// from: { col: 2, row : 2 },
				// to: { col: 6, row: 5 }
				// }
				// }
				// ];


				var read_opts = {
					cellStyles: true
				}

				// XLSX.readFile(wb, read_opts);
				XLSX.writeFile(wb, title + '.xlsx', read_opts);
			}
		}

		function htmlToExcel(tableName, fName, title) {
			$(':button[id="download_excel"]').prop('disabled', true);
			$(':button[id="download_excel"]').html("Sedang Proses...");
			arrTable = [];
			$('table[name="' + tableName + '"]').each(function() {
				if (this.id) {
					if (this.id != undefined && this.value != '') arrTable.push(this.id);
				}
			})
			setTimeout(function() {
				if (fName == "xls") {
					if (arrTable.length < 2)
						xls(tableName, title);
					else
						xls(arrTable, title);
				}
				if (fName == "xlsx") {
					if (arrTable.length < 2)
						xlsx(tableName, title);
					else
						xlsx(arrTable, title);

				}


				// $(':button[id="download_excel"]').prop('disabled', false);
				// $(':button[id="download_excel"]').html("<i class='fa fa-file-excel-o '></i> Excel");
				// $('#myModal').modal('hide');
				// toastr.info("Download sukses!");
			}, 1000);

		}

		function fnHtmltoExcelTable(idHTML) {
			var tab_text = "<table border='2px'>";
			var textRange;
			var j = 0;
			tab = document.getElementById(idHTML); // id of table
			console.log(idHTML)
			for (j = 0; j < tab.rows.length; j++) {
				tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
				//tab_text=tab_text+"</tr>";
			}
			tab_text = tab_text + "</table>";
			return tab_text;
		}

		function fnExcelReport() {
			tab_text = fnHtmltoExcelTable('tableExportSheet1');
			tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
			tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
			tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

			var ua = window.navigator.userAgent;
			var msie = ua.indexOf("MSIE ");

			if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) // If Internet Explorer
			{
				txtArea1.document.open("txt/html", "replace");
				txtArea1.document.write(tab_text);
				txtArea1.document.close();
				txtArea1.focus();
				sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
			} else //other browser not tested on IE 11
				// sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

				var data_type = 'data:application/vnd.ms-excel';
			var a = document.createElement('a');
			a.href = data_type + ', ' + encodeURIComponent(tab_text);
			//setting the file name
			console.log(a);
			a.download = 'BC MAINTENANCE - ' + <?php echo "'" . $detail[0]['tanggal_pengajuan'] . "'"; ?> + ' .xls';
			//triggering the function
			a.click();
			//just in case, prevent default behaviour
		}
	</script>