<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Bc_Limbah_Model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('maintenance/maintenance_model');
  }

  public function report_data_header($no_pengajuan)
  {
    $sql   = 'SELECT * 
							FROM t_report_header 
							where NOMOR_AJU = ?';

    $query   = $this->db->query($sql, array(
      $no_pengajuan
    ));
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }
  public function report_data_bahanbaku($no_pengajuan)
  {
    $sql   = 'SELECT * FROM t_report_bahanbaku
							where NOMOR_AJU = ?';

    $query   = $this->db->query($sql, array(
      $no_pengajuan
    ));
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }
  public function report_data_bahanbakutarif($no_pengajuan)
  {
    $sql   = 'SELECT * FROM t_report_bahanbakutarif						
			where NOMOR_AJU = ?';

    $query   = $this->db->query($sql, array(
      $no_pengajuan
    ));
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }
  public function report_data_bahanbakudokumen($no_pengajuan)
  {
    $sql   = 'SELECT * FROM t_report_bahanbakudokumen
		where NOMOR_AJU = ?';

    $query   = $this->db->query($sql, array(
      $no_pengajuan
    ));
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }
  public function report_data_barang($no_pengajuan)
  {
    $sql   = 'SELECT * FROM t_report_barang
		where NOMOR_AJU = ?';

    $query   = $this->db->query($sql, array(
      $no_pengajuan
    ));
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }
  public function report_data_barangtarif($no_pengajuan)
  {
    $sql   = 'SELECT * FROM t_report_barangtarif
		where NOMOR_AJU = ?';

    $query   = $this->db->query($sql, array(
      $no_pengajuan
    ));
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }
  public function report_data_barangdokumen($no_pengajuan)
  {
    $sql   = 'SELECT * FROM t_report_barangdokumen
		where NOMOR_AJU = ?';

    $query   = $this->db->query($sql, array(
      $no_pengajuan
    ));
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }
  public function report_data_dokumen($no_pengajuan)
  {
    $sql   = 'SELECT * FROM t_report_dokumen
		where NOMOR_AJU = ?';

    $query   = $this->db->query($sql, array(
      $no_pengajuan
    ));
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }
  public function report_data_kemasan($no_pengajuan)
  {
    $sql   = 'SELECT * FROM t_report_kemasan
		where NOMOR_AJU = ?';

    $query   = $this->db->query($sql, array(
      $no_pengajuan
    ));
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }
  public function report_data_kontainer($no_pengajuan)
  {
    $sql   = 'SELECT * FROM t_report_kontainer
		where NOMOR_AJU = ?';

    $query   = $this->db->query($sql, array(
      $no_pengajuan
    ));
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }
  public function report_data_jaminan($no_pengajuan)
  {
    $sql   = 'SELECT * FROM t_report_jaminan
		where NOMOR_AJU = ?';

    $query   = $this->db->query($sql, array(
      $no_pengajuan
    ));
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }
  public function report_data_respon($no_pengajuan)
  {
    $sql   = 'SELECT * FROM t_report_respons
		where NOMOR_AJU = ?';

    $query   = $this->db->query($sql, array(
      $no_pengajuan
    ));
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }
  public function report_data_status($no_pengajuan)
  {
    $sql   = 'SELECT * FROM t_report_status
		where NOMOR_AJU = ?';

    $query   = $this->db->query($sql, array(
      $no_pengajuan
    ));
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }
  public function report_data_pungutan($no_pengajuan)
  {
    $sql   = 'SELECT * FROM t_report_pungutan
		where NOMOR_AJU = ?';

    $query   = $this->db->query($sql, array(
      $no_pengajuan
    ));
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }
  public function all_type_bc()
  {
    $sql   = 'SELECT * FROM m_type_bc';

    $query   = $this->db->query($sql);
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }

  public function type_bc()
  {
    $sql   = 'SELECT * 
    FROM m_type_bc 
    WHERE type_bc = 1';

    $query   = $this->db->query($sql);
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }

  public function check_userid($params = array())
  {
    $sql   = 'SELECT COUNT(id) AS jumlah FROM u_user WHERE id = ? AND status_akses = 1;';
    $query   = $this->db->query(
      $sql,
      array($params['user_id'])
    );
    $return = $query->row_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }

  public function list_bc($params = array())
  {
    $sql   = 'CALL bc_limbah_order_list(?, ?, ?, ?, ?, @total_filtered, @total)';
    $query   =  $this->db->query(
      $sql,
      array(
        $params['limit'],
        $params['offset'],
        $params['order_column'],
        $params['order_dir'],
        $params['filter'],
      )
    );

    $result = $query->result_array();

    $this->load->helper('db');
    free_result($this->db->conn_id);

    $total = $this->db->query('select @total_filtered, @total')->row_array();

    $return = array(
      'data' => $result,
      'total_filtered' => $total['@total_filtered'],
      'total' => $total['@total'],
    );

    return $return;
  }

  public function add_bc_limbah($data)
  {
    $sql   = 'CALL bc_add(?,?,?,?,?)';

    $query   =  $this->db->query(
      $sql,
      array(
        $data['jenis_bc'],
        $data['no_pendaftaran'],
        $data['no_pengajuan'],
        $data['tanggal_pengajuan'],
        $data['file_loc'],
      )
    );
    $query = $this->db->query('SELECT LAST_INSERT_ID()');
    $row = $query->row_array();
    $lastid = $row['LAST_INSERT_ID()'];

    $result  = $this->db->affected_rows();

    $this->db->close();
    $this->db->initialize();

    $data['id_bc'] = $lastid;
    $arr_result['result'] = $result;

    $this->add_detail_bc_limbah_order($data);

    return $arr_result;
  }
  public function add_detail_bc_maintenance($data)
  {
    // // $result_detail_maintenance = $this->maintenance_model->detail($data['no_maintenance']);
    // // for ($i=0; $i<count($result_detail_maintenance);$i++){

    // $detail_maintenance = $this->detail($data['no_maintenance']);
    // $detail_bc = $this->get_type_bc($data['id_bc']);
    // // var_dump($detail_bc,$detail_maintenance);die;
    $sql   = 'CALL bc_maintenance_add(?,?)';

    $query   =  $this->db->query(
      $sql,
      array(
        $data['id_bc'],
        $data['no_maintenance']
      )
    );

    $result  = $this->db->affected_rows();

    $this->db->close();
    $this->db->initialize();
    // // }
    // if ($detail_bc[0]['type_bc']==0){
    // 	$this->insert_t_mutasi($detail_maintenance, "masuk");
    // 	$this->update_m_material($detail_maintenance, "add");
    // }
    return $result;
  }

  public function add_detail_bc_limbah_order($data)
  {
    // // $result_detail_maintenance = $this->maintenance_model->detail($data['no_maintenance']);
    // // for ($i=0; $i<count($result_detail_maintenance);$i++){

    // $detail_maintenance = $this->detail($data['no_maintenance']);
    // $detail_bc = $this->get_type_bc($data['id_bc']);
    // // var_dump($detail_bc,$detail_maintenance);die;
    $sql   = 'CALL bc_limbah_order_add(?,?)';

    $query   =  $this->db->query(
      $sql,
      array(
        $data['id_bc'],
        $data['no_maintenance']
      )
    );

    $result  = $this->db->affected_rows();

    $this->db->close();
    $this->db->initialize();
    // // }
    // if ($detail_bc[0]['type_bc']==0){
    // 	$this->insert_t_mutasi($detail_maintenance, "masuk");
    // 	$this->update_m_material($detail_maintenance, "add");
    // }
    return $result;
  }
  public function edit_bc($id)
  {
    $sql   = 'CALL bc_limbah_search_id(?)';

    $query   =  $this->db->query(
      $sql,
      array($id)
    );

    $result  = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }


  public function save_edit_bc($data)
  {
    $sql   = 'CALL bc_update(?,?,?,?,?,?)';

    $query   =  $this->db->query(
      $sql,
      array(
        $data['bc_id'],
        $data['jenis_bc'],
        $data['no_pendaftaran'],
        $data['no_pengajuan'],
        $data['tanggal_pengajuan'],
        $data['file_loc']
      )
    );

    $result  = $this->db->affected_rows();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }
  public function save_edit_bc_limbah($data)
  {
    $sql   = 'CALL bc_limbah_update(?,?,?,?,?,?,?,?)';
    // IN `pin_id_bc_maintenance` INT(11),
    // IN `pin_id_bc` INT(11),
    // IN `pin_no_main` VARCHAR(255),
    // IN `pin_jenis_bc` INT(11),
    // IN `pin_no_pendaftaran` VARCHAR(255),
    // IN `pin_no_pengajuan` VARCHAR(255),
    // IN `pin_tanggal_pengajuan` DATE,
    // IN `pin_file_loc` VARCHAR(255)
    $query   =  $this->db->query(
      $sql,
      array(
        $data['bc_maintenance_id'],
        $data['bc_id'],
        $data['no_maintenance'],
        $data['jenis_bc'],
        $data['no_pendaftaran'],
        $data['no_pengajuan'],
        $data['tanggal_pengajuan'],
        $data['file_loc']
      )
    );

    $result  = $this->db->affected_rows();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }
  public function delete_bc($data)
  {
    $sql   = 'CALL bc_limbah_order_delete(?)';
    $query   =  $this->db->query(
      $sql,
      array($data)
    );

    $result  = $this->db->affected_rows();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function delete_bc_limbah($data)
  {
    $sql   = 'CALL bc_limbah_order_delete(?)';
    $query   =  $this->db->query(
      $sql,
      array($data)
    );

    $result  = $this->db->affected_rows();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function detail_bc($params = array())
  {
    $sql   = 'CALL bc_maintenance_detail_list(?,?,?,?,?,?, @total_filtered, @total)';

    $out = array();
    $query   =  $this->db->query(
      $sql,
      array(

        $params['id_bc'],
        $params['limit'],
        $params['offset'],
        $params['order_column'],
        $params['order_dir'],
        $params['filter']
      )
    );

    $result = $query->result_array();

    $this->load->helper('db');
    free_result($this->db->conn_id);

    $total = $this->db->query('select @total_filtered, @total')->row_array();

    $return = array(
      'data' => $result,
      'total_filtered' => $total['@total_filtered'],
      'total' => $total['@total'],
    );

    return $return;
  }
  public function get_type_bc($id_bc)
  {
    $sql   = "	SELECT e.type_bc
								FROM m_type_bc e
								LEFT JOIN t_bc f ON f.`jenis_bc` = e.`id`
								WHERE f.id = ?";

    $query   =  $this->db->query(
      $sql,
      array(
        $id_bc
      )
    );
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }
  public function get_maintenance_by_type_bc($id_bc)
  {
    $type_bc = $this->get_type_bc($id_bc);
    // var_dump($type_bc,$id_bc);die;
    if ($id_bc == 5)
      $sql   = "call maintenance_search_by_type_bc(?)";
    else
      $sql   = "call maintenance_order_search_by_type_bc(?)";
    $query   =  $this->db->query(
      $sql,
      array(
        $id_bc
      )
    );
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }
  public function get_maintenance_backup()
  {
    $sql   = "SELECT a.id,a.no_maintenance, c.stock_name
							FROM t_maintenance a 
							LEFT JOIN m_material c ON a.nama_material =c.id
							WHERE NOT EXISTS(
								SELECT NULL 
									FROM t_maintenance_bc b 
									WHERE   a.id = b.id_maintenance)";

    $query   = $this->db->query($sql);
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }
  public function detail($nomaintenance)
  {
    $sql   = 'CALL maintenance_search_no_maintenance(?)';

    $query   = $this->db->query($sql, array(
      $nomaintenance
    ));
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }
  public function update_m_material($items, $type)
  {
    $itemsLen = count($items);
    // var_dump($items);
    for ($i = 0; $i < $itemsLen; $i++) {
      $typeUpdate = 1;
      if ($type != "add") $typeUpdate = -1;
      $sql   = 'UPDATE m_material 
								SET qty=qty+?*?
								WHERE id=?';
      $query   =  $this->db->query(
        $sql,
        array(
          $items[$i]["qty"],
          $typeUpdate,
          $items[$i]["nama_material"],
        )
      );
    }
    $this->db->close();
    $this->db->initialize();
  }
  /**
   * This function is used to insert mutation data
   * @param: $data - id item and quantity
   */
  public function insert_t_mutasi($items, $type)
  {
    $itemsLen = count($items);
    // var_dump($items);
    for ($i = 0; $i < $itemsLen; $i++) {
      $typeMutation = 1;
      if ($type != "keluar") $typeMutation = 0;
      $sql   = 'CALL mutasi_add(?,?,?,?)';
      $query   =  $this->db->query(
        $sql,
        array(
          $items[$i]["nama_material"],
          $items[$i]["nama_material"],
          $items[$i]["qty"],
          $typeMutation
        )
      );
    }
    $this->db->close();
    $this->db->initialize();
  }
  public function add_bc_stock($id, $lastid)
  {
    $sql   = 'CALL bcs_add(?,?)';

    $query   =  $this->db->query(
      $sql,
      array(
        $id,
        $lastid
      )
    );

    $result  = $this->db->affected_rows();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function add_bc_price($price, $valas, $lastid)
  {
    $sql   = 'CALL price_add(?,?,?,?)';
    //$date 	= date('Y-m-d H:i:s');
    $query   =  $this->db->query(
      $sql,
      array(
        1,
        $price,
        $valas,
        //$date,
        $lastid
      )
    );

    $result  = $this->db->affected_rows();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function edit_detail_bc($id)
  {
    $sql   = 'CALL dbc_search_id(?)';

    $query   =  $this->db->query(
      $sql,
      array($id)
    );

    $result  = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function save_edit_detail($data)
  {
    $sql   = 'CALL dbc_update(?,?,?,?,?,?,?,?,?)';

    $query   =  $this->db->query(
      $sql,
      array(
        $data['id'],
        $data['id_bc'],
        $data['kode_barang_bc'],
        $data['kode_barang'],
        $data['uom'],
        $data['valas'],
        $data['price'],
        $data['weight'],
        $data['qty']
      )
    );

    $result  = $this->db->affected_rows();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function delete_detail_bc($data)
  {
    $sql   = 'CALL bc_maintenance_delete(?)';
    $query   =  $this->db->query(
      $sql,
      array($data)
    );

    $result  = $this->db->affected_rows();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }
}
