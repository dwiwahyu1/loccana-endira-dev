	<style>
		.dt-body-left {text-align:left;}
		.dt-body-right {text-align:right;}
		.dt-body-center {text-align:center; vertical-align: middle;}
		.force-overflow {height: 350px; overflow-y: auto;overflow-x: auto}
		.scroll-overflow {min-height: 350px;}
		#modal-material::-webkit-scrollbar-track
		{
			-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
			background-color: #F5F5F5;
		}

		#modal-material::-webkit-scrollbar
		{
			width: 10px;
			background-color: #F5F5F5;
		}

		#modal-material::-webkit-scrollbar-thumb
		{
			background-image: -webkit-gradient(linear,
				 left bottom,
				 left top,
				 color-stop(0.44, rgb(122,153,217)),
				 color-stop(0.72, rgb(73,125,189)),
				 color-stop(0.86, rgb(28,58,148)));
		}
	</style>
	
 <div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Traceability Search Finish Good</h4>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-12">
			<div class="panel">
				<div class="panel-heading" style="background-color: #96b6e8; color: #fff">
					<h3 class="panel-title">Detail Material <?php if(isset($detail_material['stock_name'])) {echo $detail_material['stock_name']; }?></h3>
				</div>
				
				<form class="form-horizontal form-label-left" id="edit_material" role="form" action="<?php echo base_url('traceability/edit_material');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
					<div class="panel-body">
						<div class="col-sm-6">
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Kode Stok <span class="required"><sup>*</sup></span></label>
								<div class="col-md-8 col-sm-6 col-xs-12">
									<input data-parsley-maxlength="255" type="text" id="kode_stok" name="kode_stok" class="form-control" placeholder="Kode Stok" autocomplete="off" required="required" value="<?php if(isset($detail_material['stock_code'])) {echo $detail_material['stock_code']; }?>" readOnly>
								</div>
							</div>
						
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Stok <span class="required"><sup>*</sup></span></label>
								<div class="col-md-8 col-sm-6 col-xs-12">
									<input data-parsley-maxlength="255" type="text" id="nama_stok" name="nama_stok" class="form-control" placeholder="Nama Stok"
									value="<?php if(isset($detail_material['stock_name'])) {echo $detail_material['stock_name']; }?>" autocomplete="off" required="required" readOnly>
								</div>
							</div>

							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="desk_stok">Deskripsi Stok <span class="required"><sup>*</sup></span></label>
								<div class="col-md-8 col-sm-6 col-xs-12">
									<textarea data-parsley-maxlength="255" id="desk_stok" name="desk_stok" class="form-control" readOnly><?php if(isset($detail_material['stock_description'])) {
										echo $detail_material['stock_description'];
									}?></textarea>
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tipe_material">Tipe Material <span class="required"><sup>*</sup></span></label>
								<div class="col-md-8 col-sm-6 col-xs-12">
									<input data-parsley-maxlength="255" id="tipe_material" name="tipe_material" class="form-control" readOnly value="<?php if(isset($detail_material['type_material_name'])) {
										echo $detail_material['type_material_name'];
									}?>">
								</div>
							</div>
							
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="satuan">Satuan <span class="required"><sup>*</sup></span></label>
								<div class="col-md-8 col-sm-6 col-xs-12">
									<input data-parsley-maxlength="255" id="satuan" name="satuan" class="form-control" readOnly value="<?php if(isset($detail_material['uom_name'])) {
										echo $detail_material['uom_name'];
									}?>">
								</div>
							</div>
							
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty">Qty <span class="required"><sup>*</sup></span></label>
								<div class="col-md-8 col-sm-6 col-xs-12">
									<input data-parsley-maxlength="255" id="qty" name="qty" class="form-control" readOnly value="<?php if(isset($detail_material['uom_name'])) {
										echo $detail_material['qty'].' '.$detail_material['uom_symbol'];
									}?>">
								</div>
							</div>
						</div>
					</div>
				
				</form>
			</div>
		</div><!-- end col -->
	</div>
	
	<div class="row">
		<div class="col-sm-12">
		<h4 class="page-title">PO Quotation</h4>
			<div class="card-box">
				<table id="listPOQuot" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>No Order</th>
							<th>Tanggal Order</th>
							<th>Tanggal Permintaan</th>
							<th>Tanggal Pembuatan</th>
							<th>Tanggal Persetujuan</th>
							<th>Ketentuan Bayar </th>
							<th>Qty </th>
							<th>Rate</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div><!-- end col -->
	</div>
	
	<div class="row">
		<div class="col-sm-12">
		<h4 class="page-title">Packing</h4>
			<div class="card-box">
				<table id="listPacking" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>No Packing</th>
							<th>Qty Array</th>
							<th>Qty Box</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div><!-- end col -->
	</div>
	
	<div class="row">
		<div class="col-sm-12">
		<h4 class="page-title">STBJ</h4>
			<div class="card-box">
				<table id="listSTBJ" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>No STBJ</th>
							<th>Tanggal STBJ</th>
							<th>No PI</th>
							<th>Detail Box</th>
							<th>Remark</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div><!-- end col -->
	</div>
	
	<div class="row">
		<div class="col-sm-12">
		<h4 class="page-title">Delivery Order</h4>
			<div class="card-box">
				<table id="listDO" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr> 
							<th>No</th>
							<th>No BC</th>
							<th>No DO</th>
							<th>Tanggal DO</th>
							<th>Tanggal Akhir</th>
							<th>No PI</th>
							<th>Detail Box</th>
							<th>Remark</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div><!-- end col -->
	</div>
	
	<div class="row">
		<div class="col-sm-12">
		<h4 class="page-title">BOM</h4>
			<div class="card-box">
				<table id="listBOM" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>ID Material</th>
							<th>ID Stok</th>
							<th>Tanggal SPB</th>
							<th>Tanggal Diperlukan</th>
							<th>Kode Material</th>
							<th>Nama Material</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div><!-- end col -->
	</div>
	
	<!--<div class="row">
		<div class="col-sm-12">
		<h4 class="page-title">Finish Good</h4>
			<div class="card-box">
				<table id="listFG" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>No Pendaftaran</th>
							<th>No Pengajuan</th>
							<th>Tanggal Pengajuan</th>
							<th>No Order</th>
							<th>Tanggal Order</th>
							<th>Nama Customer</th>
							<th>Jenis BC</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div><!-- end col
	</div>-->
</div>

<script type="text/javascript">
	var id_material = '<?php if(isset($detail_material['id'])) {echo $detail_material['id'];} ?>';
	$(document).ready(function(){
		setTablePOQuot(id_material);
		setTablePacking(id_material);
		setTableSTBJ(id_material);
		setTableDO(id_material);
		setTableBOM(id_material);
		setTableFG(id_material);
	});
	
	function setTablePOQuot(id_material) {
		$("#listPOQuot").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'traceability/lists_poquot2?id_material="+id_material+"';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false, 
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip', 
			"initComplete": function(){
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tambah Stok</a></div><div  class="btn-group pull-right"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tipe Stok</a></div><div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Gudang</a></div>');
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-plus"></i> Tambah Stok</a></div>');
			},
			"columnDefs": [{
				targets: [0],
				className: 'dt-body-center',
				width: 10
			},{
				targets: [1,2,3,4,5],
				width: 140
			},{
				targets: [6,7,8],
				className: 'dt-body-right',
				width: 150
			}]
		});
	}
	
	function setTablePacking(id_material) {
		$("#listPacking").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'traceability/lists_packing2?id_material="+id_material+"';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false, 
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip', 
			"initComplete": function(){
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tambah Stok</a></div><div  class="btn-group pull-right"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tipe Stok</a></div><div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Gudang</a></div>');
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-plus"></i> Tambah Stok</a></div>');
			},
			"columnDefs": [{
				targets: [0],
				className: 'dt-body-center',
				width: 10
			},{
				targets: [1],
				width: 140
			},{
				targets: [2,3],
				className: 'dt-body-right',
				width: 150
			}]
		});
	}
	
	function setTableSTBJ(id_material) {
		$("#listSTBJ").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'traceability/lists_stbj2?id_material="+id_material+"';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false, 
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip', 
			"initComplete": function(){
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tambah Stok</a></div><div  class="btn-group pull-right"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tipe Stok</a></div><div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Gudang</a></div>');
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-plus"></i> Tambah Stok</a></div>');
			},
			"columnDefs": [{
				targets: [0],
				className: 'dt-body-center',
				width: 10
			},{
				targets: [1,2,3],
				width: 140
			},{
				targets: [4,5],
				width: 170
			}]
		});
	}
	
	function setTableDO(id_material) {
		$("#listDO").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'traceability/lists_do2?id_material="+id_material+"';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false, 
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip', 
			"initComplete": function(){
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tambah Stok</a></div><div  class="btn-group pull-right"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tipe Stok</a></div><div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Gudang</a></div>');
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-plus"></i> Tambah Stok</a></div>');
			},
			"columnDefs": [{
				targets: [0],
				className: 'dt-body-center',
				width: 10
			},{
				targets: [1,2,3,4,5],
				width: 140
			},{
				targets: [6,7],
				width: 150
			}]
		});
	}
	
	function setTableBOM(id_material) {
		$("#listBOM").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'traceability/lists_bom2?id_material="+id_material+"';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false, 
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip', 
			"initComplete": function(){
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tambah Stok</a></div><div  class="btn-group pull-right"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tipe Stok</a></div><div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Gudang</a></div>');
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-plus"></i> Tambah Stok</a></div>');
			},
			"columnDefs": [{
				targets: [0],
				className: 'dt-body-center',
				width: 10
			},{
				targets: [1,2,3],
				width: 120
			},{
				targets: [4],
				width: 150
			},{
				targets: [5],
				className: 'dt-body-right',
				width: 120
			}]
		});
	}	
	
	function setTableFG(id_material) {
		$("#listFG").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'traceability/lists_fg2?id_material="+id_material+"';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false, 
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip', 
			"initComplete": function(){
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tambah Stok</a></div><div  class="btn-group pull-right"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tipe Stok</a></div><div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Gudang</a></div>');
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-plus"></i> Tambah Stok</a></div>');
			},
			"columnDefs": [{
				targets: [0],
				className: 'dt-body-center',
				width: 10
			},{
				targets: [1,2,3,4,5],
				width: 150
			},{
				targets: [6],
				width: 180
			},{
				targets: [7],
				width: 100
			}]
		});
	}
</script>
