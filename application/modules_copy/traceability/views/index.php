	<style>
		.dt-body-left {text-align:left;}
		.dt-body-right {text-align:right;}
		.dt-body-center {text-align:center; vertical-align: middle;}
		.force-overflow {height: 350px; overflow-y: auto;overflow-x: auto}
		.scroll-overflow {min-height: 350px;}
		#modal-material::-webkit-scrollbar-track
		{
			-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
			background-color: #F5F5F5;
		}

		#modal-material::-webkit-scrollbar
		{
			width: 10px;
			background-color: #F5F5F5;
		}

		#modal-material::-webkit-scrollbar-thumb
		{
			background-image: -webkit-gradient(linear,
				 left bottom,
				 left top,
				 color-stop(0.44, rgb(122,153,217)),
				 color-stop(0.72, rgb(73,125,189)),
				 color-stop(0.86, rgb(28,58,148)));
		}
	</style>
 <div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Traceability</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<table id="listmaterial" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>Stok</th>
							<th>Unit</th>
							<th>Tipe</th>
							<th>Harga</th>
							<th>Stok</th>
							<th>Gudang</th>
							<th>Trace</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div><!-- end col -->
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$("#listmaterial").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'traceability/lists/';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false, 
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip', 
			"initComplete": function(){
				 //$("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tambah Stok</a></div><div  class="btn-group pull-right"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tipe Stok</a></div><div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Gudang</a></div>');
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-plus"></i> Tambah Stok</a></div>');
			},
			"columnDefs": [{
				targets: [0],
				className: 'dt-body-center',
				width: 10
			},{
				targets: [1,3],
				width: 250
			},{
				targets: [2],
				className: 'dt-body-center',
				width: 90
			},{
				targets: [4,5],
				className: 'dt-body-right',
				width: 120
			},{
				targets: [6],
				className: 'dt-body-center',
				width: 120
			},{
				targets: [7],
				className: 'dt-body-center',
				width: 80
			}]
		});
	});
	
</script>
