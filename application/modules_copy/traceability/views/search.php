	<style>
		.dt-body-left {text-align:left;}
		.dt-body-right {text-align:right;}
		.dt-body-center {text-align:center; vertical-align: middle;}
		.force-overflow {height: 350px; overflow-y: auto;overflow-x: auto}
		.scroll-overflow {min-height: 350px;}
		#modal-material::-webkit-scrollbar-track
		{
			-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
			background-color: #F5F5F5;
		}

		#modal-material::-webkit-scrollbar
		{
			width: 10px;
			background-color: #F5F5F5;
		}

		#modal-material::-webkit-scrollbar-thumb
		{
			background-image: -webkit-gradient(linear,
				 left bottom,
				 left top,
				 color-stop(0.44, rgb(122,153,217)),
				 color-stop(0.72, rgb(73,125,189)),
				 color-stop(0.86, rgb(28,58,148)));
		}
	</style>
 <div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Traceability Search</h4>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-12">
			<div class="panel">
				<div class="panel-heading" style="background-color: #96b6e8; color: #fff">
					<h3 class="panel-title">Detail Material <?php if(isset($detail_material['stock_name'])) {echo $detail_material['stock_name']; }?></h3>
				</div>
				
				<form class="form-horizontal form-label-left" id="edit_material" role="form" action="<?php echo base_url('traceability/edit_material');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
					<div class="panel-body">
						<div class="col-sm-6">
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Kode Stok <span class="required"><sup>*</sup></span></label>
								<div class="col-md-8 col-sm-6 col-xs-12">
									<input data-parsley-maxlength="255" type="text" id="kode_stok" name="kode_stok" class="form-control" placeholder="Kode Stok" autocomplete="off" required="required" value="<?php if(isset($detail_material['stock_code'])) {echo $detail_material['stock_code']; }?>" readOnly>
								</div>
							</div>
						
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Stok <span class="required"><sup>*</sup></span></label>
								<div class="col-md-8 col-sm-6 col-xs-12">
									<input data-parsley-maxlength="255" type="text" id="nama_stok" name="nama_stok" class="form-control" placeholder="Nama Stok"
									value="<?php if(isset($detail_material['stock_name'])) {echo $detail_material['stock_name']; }?>" autocomplete="off" required="required" readOnly>
								</div>
							</div>

							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="desk_stok">Deskripsi Stok <span class="required"><sup>*</sup></span></label>
								<div class="col-md-8 col-sm-6 col-xs-12">
									<textarea data-parsley-maxlength="255" id="desk_stok" name="desk_stok" class="form-control" readOnly><?php if(isset($detail_material['stock_description'])) {
										echo $detail_material['stock_description'];
									}?></textarea>
								</div>
							</div>
						</div>
						
						<div class="col-sm-6">
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tipe_material">Tipe Material <span class="required"><sup>*</sup></span></label>
								<div class="col-md-8 col-sm-6 col-xs-12">
									<input data-parsley-maxlength="255" id="tipe_material" name="tipe_material" class="form-control" readOnly value="<?php if(isset($detail_material['type_material_name'])) {
										echo $detail_material['type_material_name'];
									}?>">
								</div>
							</div>
							
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="satuan">Satuan <span class="required"><sup>*</sup></span></label>
								<div class="col-md-8 col-sm-6 col-xs-12">
									<input data-parsley-maxlength="255" id="satuan" name="satuan" class="form-control" readOnly value="<?php if(isset($detail_material['uom_name'])) {
										echo $detail_material['uom_name'];
									}?>">
								</div>
							</div>
							
							<div class="item form-group">
								<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty">Qty <span class="required"><sup>*</sup></span></label>
								<div class="col-md-8 col-sm-6 col-xs-12">
									<input data-parsley-maxlength="255" id="qty" name="qty" class="form-control" readOnly value="<?php if(isset($detail_material['uom_name'])) {
										echo $detail_material['qty'].' '.$detail_material['uom_symbol'];
									}?>">
								</div>
							</div>
						</div>
					</div>
				
				</form>
			</div>
		</div><!-- end col -->
	</div>

	<div class="row">
		<div class="col-sm-12">
		<h4 class="page-title">Purchase Order</h4>
			<div class="card-box">
				<table id="listPO" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>No PO</th>
							<th>Tanggal PO</th>
							<th>Tanggal Pengiriman</th>
							<th>Nama Distributor</th>
							<th>Waktu Pembayaran</th>
							<th>Total Harga</th>
							<th>Keterangan</th>
							<th>Valas</th>
							<th>Nilai</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div><!-- end col -->
	</div>
	
	<div class="row">
		<div class="col-sm-12">
		<h4 class="page-title">SPB</h4>
			<div class="card-box">
				<table id="listSPB" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>No SPB</th>
							<th>Tanggal SPB</th>
							<th>Nama Material</th>
							<th>Qty</th>
							<th>Tanggal Diperlukan</th>
							<th>Keterangan</th>
							<th>Note</th>
							<th>Nama Gudang</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div><!-- end col -->
	</div>
	
	<div class="row">
		<div class="col-sm-12">
		<h4 class="page-title">BTB</h4>
			<div class="card-box">
				<table id="listBTB" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>No BTB</th>
							<th>Tanggal BTB</th>
							<th>Tipe BTB</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div><!-- end col -->
	</div>
	
	<div class="row">
		<div class="col-sm-12">
		<h4 class="page-title">BOM</h4>
			<div class="card-box">
				<table id="listBOM" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>Kode Stok</th>
							<th>Kategory</th>
							<th>Kode Material</th>
							<th>Nama Material</th>
							<th>Nilai</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div><!-- end col -->
	</div>
	
	<div class="row">
		<div class="col-sm-12">
		<h4 class="page-title">Finish Good 1</h4>
			<div class="card-box">
				<table id="listFG1" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>No PO</th>
							<th>No BTB</th>
							<th>Nama Customer</th>
							<th>Jenis BC</th>
							<th>Material</th>
							<th>Kode Stok</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div><!-- end col -->
	</div>
</div>


<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:800px;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-material">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	var id_material = '<?php echo $this->uri->segment(3); ?>';
	
	function add_material(){
			$('#panel-modal').removeData('bs.modal');
			$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
			$('#panel-modal  .panel-body').load('<?php echo base_url('traceability/add');?>');
			$('#panel-modal  .panel-title').html('<i class="fa fa-plus"></i> Tambah Stok');
			$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function editmaterial(id){
			$('#panel-modal').removeData('bs.modal');
			$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
			$('#panel-modal  .panel-body').load('<?php echo base_url('traceability/edit/');?>'+"/"+id);
			$('#panel-modal  .panel-title').html('<i class="fa fa-pencil"></i> Edit Stok');
			$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}
	
	$(document).ready(function(){
		setTablePO(id_material);
		setTableSPB(id_material);
		setTableBTB(id_material);
		setTableBOM(id_material);
		setTableFG1(id_material);
	});
	
	function setTablePO(id_material) {
		$("#listPO").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'traceability/lists_po?id_material="+id_material+"';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false, 
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip', 
			"initComplete": function(){
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tambah Stok</a></div><div  class="btn-group pull-right"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tipe Stok</a></div><div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Gudang</a></div>');
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-plus"></i> Tambah Stok</a></div>');
			},
			"columnDefs": [{
				targets: [0],
				className: 'dt-body-center',
				width: 10
			},{
				targets: [1],
				width: 120
			},{
				targets: [2,3],
				width: 100
			},{
				targets: [5],
				width: 100
			},{
				targets: [6,9],
				className: 'dt-body-right',
				width: 100
			}]
		});
	}
	
	function setTableSPB(id_material) {
		$("#listSPB").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'traceability/lists_spb?id_material="+id_material+"';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false, 
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip', 
			"initComplete": function(){
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tambah Stok</a></div><div  class="btn-group pull-right"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tipe Stok</a></div><div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Gudang</a></div>');
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-plus"></i> Tambah Stok</a></div>');
			},
			"columnDefs": [{
				targets: [0],
				className: 'dt-body-center',
				width: 10
			},{
				targets: [1],
				width: 120
			},{
				targets: [2],
				width: 120
			},{
				targets: [3],
				width: 200
			},{
				targets: [4],
				className: 'dt-body-right',
				width: 50
			},{
				targets: [5],
				width: 170
			},{
				targets: [6],
				width: 170
			},{
				targets: [7],
				width: 150
			},{
				targets: [8],
				width: 170
			}]
		});
	}
	
	function setTableBTB(id_material) {
		$("#listBTB").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'traceability/lists_btb?id_material="+id_material+"';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false, 
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip', 
			"initComplete": function(){
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tambah Stok</a></div><div  class="btn-group pull-right"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tipe Stok</a></div><div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Gudang</a></div>');
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-plus"></i> Tambah Stok</a></div>');
			},
			"columnDefs": [{
				targets: [0],
				className: 'dt-body-center',
				width: 10
			},{
				targets: [1],
				width: 320
			},{
				targets: [2],
				width: 150
			},{
				targets: [3],
				width: 150
			}]
		});
	}
	
	function setTableBOM(id_material) {
		$("#listBOM").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'traceability/lists_bom?id_material="+id_material+"';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false, 
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip', 
			"initComplete": function(){
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tambah Stok</a></div><div  class="btn-group pull-right"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tipe Stok</a></div><div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Gudang</a></div>');
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-plus"></i> Tambah Stok</a></div>');
			},
			"columnDefs": [{
				targets: [0],
				className: 'dt-body-center',
				width: 10
			},{
				targets: [1],
				width: 100
			},{
				targets: [2,3],
				width: 100
			},{
				targets: [4],
				width: 150
			},{
				targets: [5],
				className: 'dt-body-right',
				width: 100
			}]
		});
	}
	
	function setTableFG1(id_material) {
		$("#listFG1").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'traceability/lists_fg1?id_material="+id_material+"';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false, 
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip', 
			"initComplete": function(){
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tambah Stok</a></div><div  class="btn-group pull-right"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Tipe Stok</a></div><div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-dollar"></i> Gudang</a></div>');
				// $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_material()"><i class="fa fa-plus"></i> Tambah Stok</a></div>');
			},
			"columnDefs": [{
				targets: [0],
				className: 'dt-body-center',
				width: 10
			},{
				targets: [1,2],
				width: 120
			},{
				targets: [3],
				width: 150
			},{
				targets: [4],
				width: 150
			},{
				targets: [5],
				width: 100
			},{
				targets: [6],
				width: 100
			}]
		});
	}
</script>
