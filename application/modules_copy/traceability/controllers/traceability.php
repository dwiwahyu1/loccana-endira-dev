<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Traceability extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('traceability/material_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'traceability/views/index');
	}

	function lists() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

		$order_fields = array('', 'stock_code', 'stock_name');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->material_model->lists($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		//$i = 0;
		$i = $params['offset'];
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$status_akses =
				'<div class="btn-group"><a href="traceability/search/'. $v['id'].'" class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Edit" ><i class="fa fa-search"></i></a></div>';
			
			array_push($data, array(
				$i,
				$v['stock_name'],
				$v['uom_name'],
				$v['type_material_name'],
				$v['symbol_valas'].' '.number_format($v['base_price'], 4, ',', '.'),
				number_format($v['qty'],4,',','.'),
				$v['nama_gudang'],
				$status_akses
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	function lists_po() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'asc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

		$order_fields = array('', 'no_po', 'date_po');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		$id_material = ($this->input->get_post('id_material') != FALSE) ? $this->input->get_post('id_material') : 0;
		
		// Build params for calling model
		$params['id_material'] = $id_material;
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->material_model->lists_po($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		//$i = 0;
		$i = $params['offset'];
		$username = $this->session->userdata['logged_in']['username'];
		
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			
			array_push($data, array(
				$i,
				$v['no_po'],
				$v['date_po'],
				$v['delivery_date'],
				$v['name_eksternal'],
				$v['term_of_payment'],
				number_format($v['total_amount'],4,',','.'),
				$v['keterangan'],
				$v['nama_valas'],
				number_format($v['rate'],4,',','.')
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	function lists_spb() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'asc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

		$order_fields = array('', 'no_spb', 'nama_material');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		$id_material = ($this->input->get_post('id_material') != FALSE) ? $this->input->get_post('id_material') : 0;
		
		// Build params for calling model
		$params['id_material'] = $id_material;
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->material_model->lists_spb($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		//$i = 0;
		$i = $params['offset'];
		$username = $this->session->userdata['logged_in']['username'];
		
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			
			array_push($data, array(
				$i,
				$v['no_spb'],
				$v['tanggal_spb'],
				$v['stock_name'],
				$v['qty'],
				$v['tanggal_diperlukan'],
				$v['desc'],
				$v['notes'],
				$v['nama_gudang']
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	function lists_btb() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'asc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

		$order_fields = array('', 'no_btb', 'type_btb');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		$id_material = ($this->input->get_post('id_material') != FALSE) ? $this->input->get_post('id_material') : 0;
		
		// Build params for calling model
		$params['id_material'] = $id_material;
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->material_model->lists_btb($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		//$i = 0;
		$i = $params['offset'];
		$username = $this->session->userdata['logged_in']['username'];
		
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			
			array_push($data, array(
				$i,
				$v['no_btb'],
				$v['tanggal_btb'],
				$v['type_btb']
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	function lists_bom() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'asc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

		$order_fields = array('', 'category', 'material');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		$id_material = ($this->input->get_post('id_material') != FALSE) ? $this->input->get_post('id_material') : 0;
		
		// Build params for calling model
		$params['id_material'] = $id_material;
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->material_model->lists_bom($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		//$i = 0;
		$i = $params['offset'];
		$username = $this->session->userdata['logged_in']['username'];
		
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			
			array_push($data, array(
				$i,
				$v['id_stock'],
				$v['category'],
				$v['material'],
				$v['stock_name'],
				number_format($v['value'],4,',','.')
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	function lists_fg1() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'asc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

		$order_fields = array('', 'no_po', 'name_eksternal', 'material');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		$id_material = ($this->input->get_post('id_material') != FALSE) ? $this->input->get_post('id_material') : 0;
		
		// Build params for calling model
		$params['id_material'] = $id_material;
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->material_model->lists_fg1($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		//$i = 0;
		$i = $params['offset'];
		$username = $this->session->userdata['logged_in']['username'];
		
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			
			array_push($data, array(
				$i,
				$v['no_po'],
				$v['no_btb'],
				$v['name_eksternal'],
				$v['jenis_bc'],
				$v['material'],
				$v['id_stock']
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	function lists_poquot2() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'asc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

		$order_fields = array('', 'order_no', 'stock_code', 'stock_name');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		$id_material = ($this->input->get_post('id_material') != FALSE) ? $this->input->get_post('id_material') : 0;
		
		// Build params for calling model
		$params['id_material'] = $id_material;
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->material_model->lists_poquot2($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		//$i = 0;
		$i = $params['offset'];
		$username = $this->session->userdata['logged_in']['username'];
		
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			
			array_push($data, array(
				$i,
				$v['order_no'],
				$v['order_date'],
				$v['req_date'],
				$v['issue_date'],
				$v['approval_date'],
				$v['price_term'],
				$v['qty'],
				$v['rate']
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	function lists_packing2() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'asc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

		$order_fields = array('', 'no_packing', 'stock_code', 'stock_name');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		$id_material = ($this->input->get_post('id_material') != FALSE) ? $this->input->get_post('id_material') : 0;
		
		// Build params for calling model
		$params['id_material'] = $id_material;
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->material_model->lists_packing2($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		//$i = 0;
		$i = $params['offset'];
		$username = $this->session->userdata['logged_in']['username'];
		
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			
			array_push($data, array(
				$i,
				$v['no_packing'],
				$v['qty_array'],
				$v['qty_box']
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	function lists_stbj2() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'asc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

		$order_fields = array('', 'no_stbj');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		$id_material = ($this->input->get_post('id_material') != FALSE) ? $this->input->get_post('id_material') : 0;
		
		// Build params for calling model
		$params['id_material'] = $id_material;
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->material_model->lists_stbj2($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		//$i = 0;
		$i = $params['offset'];
		$username = $this->session->userdata['logged_in']['username'];
		
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			
			array_push($data, array(
				$i,
				$v['no_stbj'],
				$v['tanggal_stbj'],
				$v['no_pi'],
				$v['detail_box'],
				$v['remark']
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	function lists_do2() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'asc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

		$order_fields = array('', 'no_bc', 'no_do', 'no_pi');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		$id_material = ($this->input->get_post('id_material') != FALSE) ? $this->input->get_post('id_material') : 0;
		
		// Build params for calling model
		$params['id_material'] = $id_material;
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->material_model->lists_do2($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		//$i = 0;
		$i = $params['offset'];
		$username = $this->session->userdata['logged_in']['username'];
		
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			
			array_push($data, array(
				$i,
				$v['no_bc'],
				$v['no_do'],
				$v['tanggal'],
				$v['date_end'],
				$v['no_pi'],
				$v['detail_box'],
				$v['remark'],
				$v['status_do']
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	function lists_bom2() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'asc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

		$order_fields = array('', 'stock_code', 'stock_name');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		$id_material = ($this->input->get_post('id_material') != FALSE) ? $this->input->get_post('id_material') : 0;
		
		// Build params for calling model
		$params['id_material'] = $id_material;
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->material_model->lists_bom2($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		//$i = 0;
		$i = $params['offset'];
		$username = $this->session->userdata['logged_in']['username'];
		
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			
			array_push($data, array(
				$i,
				$v['id_material_comp'],
				$v['id_stock'],
				$v['tanggal_spb'],
				$v['tanggal_diperlukan'],
				$v['stock_code'],
				$v['stock_name']
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	function lists_fg2() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'asc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

		$order_fields = array('', 'no_pendaftaran', 'no_pengajuan', 'order_no', 'name_eksternal', 'jenis_bc', 'stock_code', 'stock_name');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		$id_material = ($this->input->get_post('id_material') != FALSE) ? $this->input->get_post('id_material') : 0;
		
		// Build params for calling model
		$params['id_material'] = $id_material;
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->material_model->lists_fg2($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		//$i = 0;
		$i = $params['offset'];
		$username = $this->session->userdata['logged_in']['username'];
		
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			
			array_push($data, array(
				$i,
				$v['no_pendaftaran'],
				$v['no_pengajuan'],
				$v['tanggal_pengajuan'],
				$v['order_no'],
				$v['order_date'],
				$v['name_eksternal'],
				$v['jenis_bc']
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function search() {
		
		$id = $this->uri->segment(3);
		
		$detail_material = $this->material_model->detail_material($id);
		//echo"<pre>";print_r($detail_material);die;
		if($detail_material[0]['id_type_material'] == 4){
			$data = array(
				'detail_material'=> $detail_material[0]
			);
			
			$this->template->load('maintemplate', 'traceability/views/search_fg',$data);
		}elseif($detail_material[0]['id_type_material'] == 1 || $detail_material[0]['id_type_material'] == 3){
			$data = array(
				'detail_material'=> $detail_material[0]
			);
			
		   $this->template->load('maintemplate', 'traceability/views/search',$data);
		}elseif($detail_material[0]['id_type_material'] == 5 ){
			$data = array(
				'detail_material'=> $detail_material[0]
			);
			
		   $this->template->load('maintemplate', 'traceability/views/search',$data);
		}
    }

	public function add() {
		$type_material = $this->material_model->type_material();
		$unit = $this->material_model->unit();
		$detail = $this->material_model->detail_prop();
		$Gudang = $this->material_model->gudang();

		$data = array(
			'type_material' => $type_material,
			'unit' => $unit,
			'detail' => $detail,
			'Gudang' => $Gudang
		);

		$this->load->view('add_modal_view', $data);
	}
	
	
	
	public function edit($id) {
		$result = $this->material_model->edit($id);
		$type_material = $this->material_model->type_material();
		$valas = $this->material_model->valas();
		$unit = $this->material_model->unit();
		$detail = $this->material_model->detail_prop();
		$Gudang = $this->material_model->gudang();

		$data = array(
			'stok' 				=> $result,
			'type_material' 	=> $type_material,
			'valas' 			=> $valas,
			'unit' 				=> $unit,
			'detail' 			=> $detail,
			'Gudang' 			=> $Gudang
		);

		$this->load->view('edit_modal_view', $data);
	}

	public function deletes() {
		$data 	= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		
		$list = $this->material_model->deletes($params['id']);
		
		if($list > 0) {
			$this->log_activity->insert_activity('insert', 'Berhasil Insert Stok');
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}else {
			$this->log_activity->insert_activity('insert', 'Berhasil Insert Stok');
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
		

	}

	public function edit_material() {
		$this->form_validation->set_rules('kode_stok', 'Kode Stok', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('nama_stok', 'Nama Stok', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('desk_stok', 'Deskripsi', 'trim|required');
		$this->form_validation->set_rules('type_material', 'Tipe Stok', 'trim|required');
		$this->form_validation->set_rules('unit', 'Unit', 'trim|required');
		$this->form_validation->set_rules('valas', 'Valas', 'trim|required');
		$this->form_validation->set_rules('harga', 'Harga', 'trim|required');
		$this->form_validation->set_rules('qty', 'Quantity', 'trim|required');
		$this->form_validation->set_rules('weight', 'Weight', 'trim|required');
		$this->form_validation->set_rules('gudang', 'Gudang', 'trim|required');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');
	  

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			$user_id 			= $this->session->userdata['logged_in']['user_id'];
			$id_type_material 	= $this->Anti_sql_injection($this->input->post('id_type_material', TRUE));
			$kode_stok 			= $this->Anti_sql_injection($this->input->post('kode_stok', TRUE));
			$nama_stok 			= $this->Anti_sql_injection($this->input->post('nama_stok', TRUE));
			$desk_stok 			= $this->Anti_sql_injection($this->input->post('desk_stok', TRUE));
			$type_material 		= $this->Anti_sql_injection($this->input->post('type_material', TRUE));
			$unit 				= $this->Anti_sql_injection($this->input->post('unit', TRUE));
			$valas 				= $this->Anti_sql_injection($this->input->post('valas', TRUE));
			$harga 				= $this->Anti_sql_injection($this->input->post('harga', TRUE));
			$qty 				= $this->Anti_sql_injection($this->input->post('qty', TRUE));
			$weight 			= $this->Anti_sql_injection($this->input->post('weight', TRUE));
			$gudang 			= $this->Anti_sql_injection($this->input->post('gudang', TRUE));
			$detail 			= $this->Anti_sql_injection($this->input->post('detail', TRUE));

			$data = array(
				'id_type_material' 	=> $id_type_material,
				'no_bc' 			=> NULL,
				'kode_stok' 		=> $kode_stok,
				'nama_stok' 		=> $nama_stok,
				'desk_stok' 		=> $desk_stok,
				'unit' 				=> $unit,
				'valas' 			=> $valas,
				'type_material' 	=> $type_material,
				'qty' 				=> $qty,
				'weight' 			=> $weight,
				'treshold' 			=> 10,
				'detail' 			=> $detail,
				'gudang' 			=> $gudang,
				'status' 			=> 1,
				'base_price' 		=> $harga,
				'base_qty' 			=> $qty,
				'harga' 			=> $harga,
				'id_properties' 			=> 0,
				'user_id' 			=> NULL
			);
			$result = $this->material_model->edit_material($data);

			if ($result > 0) {
				$msg = 'Berhasil mengubah Stok ke database';
				$this->log_activity->insert_activity('insert', 'Berhasil Update Stok');
				$result = array('success' => true, 'message' => $msg);
			}else {
				$msg = 'Gagal mengubah Stok ke database';
				$this->log_activity->insert_activity('insert', 'Gagal Update Stok');
				$result = array('success' => false, 'message' => $msg);
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}
	
	public function get_properties() {
		$id = $this->Anti_sql_injection($this->input->post('id', TRUE));
		$detail_prop = $this->material_model->detail_prop_full($id);
		 
		$result = array(
			'success' => true,
			'message' => '',
			'data' => $detail_prop
		);

		$this->output->set_content_type('application/json')->set_output(json_encode($result,true));
	}
	
	public function add_material() {
		$this->form_validation->set_rules('kode_stok', 'Kode Stok', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('nama_stok', 'Nama Stok', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('desk_stok', 'Deskripsi', 'trim|required');
		$this->form_validation->set_rules('type_material', 'Tipe Stok', 'trim|required');
		$this->form_validation->set_rules('unit', 'Unit', 'trim|required');
		$this->form_validation->set_rules('harga', 'Harga', 'trim|required');
		$this->form_validation->set_rules('qty', 'Quantity', 'trim|required');
		$this->form_validation->set_rules('weight', 'Weight', 'trim|required');
		$this->form_validation->set_rules('gudang', 'Gudang', 'trim|required');
		$this->form_validation->set_rules('detail', 'Detail', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$user_id 		= $this->session->userdata['logged_in']['user_id'];
			$kode_stok 		= $this->Anti_sql_injection($this->input->post('kode_stok', TRUE));
			$nama_stok 		= $this->Anti_sql_injection($this->input->post('nama_stok', TRUE));
			$desk_stok 		= $this->Anti_sql_injection($this->input->post('desk_stok', TRUE));
			$type_material 	= $this->Anti_sql_injection($this->input->post('type_material', TRUE));
			$unit 			= $this->Anti_sql_injection($this->input->post('unit', TRUE));
			$harga 			= $this->Anti_sql_injection($this->input->post('harga', TRUE));
			$qty 			= $this->Anti_sql_injection($this->input->post('qty', TRUE));
			$weight 		= $this->Anti_sql_injection($this->input->post('weight', TRUE));
			$gudang 		= $this->Anti_sql_injection($this->input->post('gudang', TRUE));
			$detail 		= $this->Anti_sql_injection($this->input->post('detail', TRUE));

			$data = array(
				'no_bc' 		=> NULL,
				'kode_stok' 	=> $kode_stok,
				'nama_stok' 	=> $nama_stok,
				'desk_stok' 	=> $desk_stok,
				'unit'			=> $unit,
				'type_material' => $type_material,
				'qty' 			=> $qty,
				'weight' 		=> $weight,
				'treshold' 		=> 10,
				'detail' 		=> $detail,
				'gudang' 		=> $gudang,
				'status' 		=> 1,
				'base_price' 	=> $harga,
				'base_qty' 		=> $qty,
				'harga' 		=> $harga,
				'user_id' 		=> NULL,
			);

			$result = $this->material_model->new_add_material($data);
			if ($result['result'] > 0) {
				$komp = $this->material_model->get_komponen($data);

				if(count($komp) > 0){
					foreach($komp as $komps) {
						if($komps['id_detail_prop'] <> '') {
							$val_komp = $this->Anti_sql_injection($this->input->post($komps['param_detail_name'], TRUE));
							$this->material_model->add_dvalues($result['lastid'],$val_komp,$komps['id_detail_prop']);
						}
					}
				}

				$msg = 'Berhasil menambahkan Stok ke database.';
				$this->log_activity->insert_activity('insert', 'Berhasil Insert Stok');
				$results = array('success' => true, 'message' => $msg);
			}else {
				$msg = 'Gagal menambahkan Stok ke database.';
				$this->log_activity->insert_activity('insert', 'Gagal Insert Stok');
				$results = array('success' => false, 'message' => $msg);
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($results));
		}
	}
}
