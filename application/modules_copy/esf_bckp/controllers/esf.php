<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Esf extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('esf/esf_model');
		$this->load->library('log_activity');
    }

    /**
     * anti sql injection
     * @return string
     */
    public function Anti_sql_injection($string) {
        $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
        return $string;
    }

    /**
     * upload file
     * @return string
     */
    public function upload($data) {
        $this->load->library('upload');
        $config = array(
            'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/esf",
            'upload_url' => base_url() . "uploads/esf",
            'encrypt_name' => TRUE,
            'overwrite' => FALSE,
            'allowed_types' => 'jpg|jpeg|png',
            'max_size' => '10000'
        );

        $this->upload->initialize($config);

        $this->upload->do_upload($data);
        // General result data
        $result = $this->upload->data();

        // Load resize library
        $this->load->library('image_lib');

        // Resizing parameters large
        $resize = array
            (
            'source_image' => $result['full_path'],
            'new_image' => $result['full_path'],
            'maintain_ratio' => TRUE,
            'width' => 300,
            'height' => 300
        );

        // Do resize
        $this->image_lib->initialize($resize);
        $this->image_lib->resize();
        $this->image_lib->clear();

        // Add our stuff
        $img = $result['file_name'];
            
        return $img;
    }

    /**
      * This function is redirect to index esf page
      * @return Void
      */
    public function index() {
        $this->template->load('maintemplate', 'esf/views/index');
    }

    /**
      * This function is used for showing esf list
      * @return Array
      */
    function lists() {
        $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
        $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
        $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
        $order = $this->input->get_post('order');
        $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
        $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

        $order_fields = array('', 'id', 'name_eksternal', 'stock_name');

        $search = $this->input->get_post('search');

        $search_val = (!empty($search['value'])) ? $search['value'] : null;

        $search_value = $this->Anti_sql_injection($search_val);

        // Build params for calling model
        $params['limit'] = (int) $length;
        $params['offset'] = (int) $start;
        $params['order_column'] = $order_fields[$order_column];
        $params['order_dir'] = $order_dir;
        $params['filter'] = $search_value;

        $list = $this->esf_model->lists($params);

        $result["recordsTotal"] = $list['total'];
        $result["recordsFiltered"] = $list['total_filtered'];
        $result["draw"] = $draw;

        $data = array();

        $i = $params['offset'];
        foreach ($list['data'] as $k => $v) {
            $i++;

            $actions = '<div class="btn-group">';
            $actions .= '   <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="editesf(\'' . $v['id'] . '\')">';
            $actions .= '       <i class="fa fa-edit"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';
            $actions .= '<div class="btn-group">';
            $actions .= '   <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteesf(\'' . $v['id'] . '\')">';
            $actions .= '       <i class="fa fa-trash"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';
            
            array_push($data, array(
                $i,
                $v['order_no'],
                $v['esf_no'],
                $v['esf_date'],
                $v['name_eksternal'],
                $v['stock_name'],
                $v['pcs_array'],
                $v['pcb_type'],
                $v['pcb_size'],
                $v['unit'],
                $actions
                    )
            );
        }

        $result["data"] = $data;

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is redirect to add esf page
      * @return Void
      */
    public function add() {;
        $this->load->view('add_modal_view');
    }

    /**
      * This function is used to add customer specification data
      * @return Array
      */
    public function get_po() {
        $data   = file_get_contents("php://input");
        $params = json_decode($data,true);

        $list   = $this->esf_model->get_po($params);

        $data = array();

        $i = 0;
        foreach ($list as $k => $v) {
            $i++;

            $index = $i-1;

            $strTotal =
                '<div class="totalproducts" onClick="listproducts(\'' . $v['id'] .'-'. $v['order_no'] . '\')">'.
                    $v['total_products'].
                '</div>';

            $strOption =
                '<div class="radiobutton">'.
                    '<input id="option['.$index.']" type="radio" value="'.$v['id'].'">'.
                    '<label for="option['.$index.']"></label>'.
                '</div>';
            
            array_push($data, array(
                $i,
                $v['order_no'],
                $v['issue_date'],
                $v['name_eksternal'],
                $strTotal,
                $strOption
            ));
        }
        
        $res = array(
            'status'    => 'success',
            'data'      => $data
        );

        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
        echo json_encode($res);
    }

    /**
      * This function is redirect to list of products page
      * @return Void
      */
    public function listproducts($id,$flag) {
        $listproducts = $this->esf_model->listproducts($id,$flag);

        $data = array(
            'listproducts'    => $listproducts
        );

        $this->load->view('listproducts_modal_view',$data);
    }

    /**
      * This function is redirect to add esf page
      * @return Void
      */
    public function new($id) {
        $detail = $this->esf_model->listproducts($id,1);

        $data = array(
            'detail'    => $detail
        );

        $this->load->view('new_modal_view',$data);
    }

    /**
      * This function is used to add esf data
      * @return Array
      */
    public function add_esf() {
        $id_po_quotation = $this->Anti_sql_injection($this->input->post('id_po_quotation', TRUE));
        $id_produk = $this->Anti_sql_injection($this->input->post('id_produk', TRUE));
        $esf_no = $this->Anti_sql_injection($this->input->post('esf_no', TRUE));
        $esf_date = $this->Anti_sql_injection($this->input->post('esf_date', TRUE));
        $pcs_array = $this->Anti_sql_injection($this->input->post('pcs_array', TRUE));
        $pcb_long = $this->Anti_sql_injection($this->input->post('pcb_long', TRUE));
        $pcb_wide = $this->Anti_sql_injection($this->input->post('pcb_wide', TRUE));
        $pcb_type = $this->Anti_sql_injection($this->input->post('pcb_type', TRUE));
        $unit = $this->Anti_sql_injection($this->input->post('unit', TRUE));
        $panel_long = $this->Anti_sql_injection($this->input->post('panel_long', TRUE));
        $panel_wide = $this->Anti_sql_injection($this->input->post('panel_wide', TRUE));
        $panel_m2 = $this->Anti_sql_injection($this->input->post('panel_m2', TRUE));
        $panel_pcs_array = $this->Anti_sql_injection($this->input->post('panel_pcs_array', TRUE));
        $panel_board_long = $this->Anti_sql_injection($this->input->post('panel_board_long', TRUE));
        $panel_board_wide = $this->Anti_sql_injection($this->input->post('panel_board_wide', TRUE));
        $panel_pcs_m2 = $this->Anti_sql_injection($this->input->post('panel_pcs_m2', TRUE));
        $grain_direction = $this->Anti_sql_injection($this->input->post('grain_direction', TRUE));
        $punching_direction = $this->Anti_sql_injection($this->input->post('punching_direction', TRUE));
        $panel_img = NULL;
        $notrec_sheet_long = $this->Anti_sql_injection($this->input->post('notrec_sheet_long', TRUE));
        $notrec_sheet_wide = $this->Anti_sql_injection($this->input->post('notrec_sheet_wide', TRUE));
        $notrec_nopanel = $this->Anti_sql_injection($this->input->post('notrec_nopanel', TRUE));
        $notrec_pcb = $this->Anti_sql_injection($this->input->post('notrec_pcb', TRUE));
        $notrec_yield = $this->Anti_sql_injection($this->input->post('notrec_yield', TRUE));
        $notrec_size_longup = $this->Anti_sql_injection($this->input->post('notrec_size_longup', TRUE));
        $notrec_size_wideup = $this->Anti_sql_injection($this->input->post('notrec_size_wideup', TRUE));
        $notrec_size_longbottom = $this->Anti_sql_injection($this->input->post('notrec_size_longbottom', TRUE));
        $notrec_size_widebottom = $this->Anti_sql_injection($this->input->post('notrec_size_widebottom', TRUE));
        $notrec_m2 = $this->Anti_sql_injection($this->input->post('notrec_m2', TRUE));
        $notrec_img = NULL;
        $rec_sheet_long = $this->Anti_sql_injection($this->input->post('rec_sheet_long', TRUE));
        $rec_sheet_wide = $this->Anti_sql_injection($this->input->post('rec_sheet_wide', TRUE));
        $rec_nopanel = $this->Anti_sql_injection($this->input->post('rec_nopanel', TRUE));
        $rec_pcb = $this->Anti_sql_injection($this->input->post('rec_pcb', TRUE));
        $rec_yield = $this->Anti_sql_injection($this->input->post('rec_yield', TRUE));
        $rec_size_longup = $this->Anti_sql_injection($this->input->post('rec_size_longup', TRUE));
        $rec_size_wideup = $this->Anti_sql_injection($this->input->post('rec_size_wideup', TRUE));
        $rec_size_longbottom = $this->Anti_sql_injection($this->input->post('rec_size_longbottom', TRUE));
        $rec_size_widebottom = $this->Anti_sql_injection($this->input->post('rec_size_widebottom', TRUE));
        $rec_m2 = $this->Anti_sql_injection($this->input->post('rec_m2', TRUE));
        $rec_img = NULL;

        if ($_FILES['panel_img']['name']) {
            $panel_img = $this->upload('panel_img');
        }

        if ($_FILES['notrec_img']['name']) {
            $notrec_img = $this->upload('notrec_img');
        }

        if ($_FILES['rec_img']['name']) {
            $rec_img = $this->upload('rec_img');
        }

        $data = array(
            'id_po_quotation' => $id_po_quotation,
            'id_produk' => $id_produk,
            'esf_no' => $esf_no,
            'esf_date' => $esf_date,
            'pcs_array' => $pcs_array,
            'pcb_long' => $pcb_long,
            'pcb_wide' => $pcb_wide,
            'pcb_type' => $pcb_type,
            'unit' => $unit,
            'panel_long' => $panel_long,
            'panel_wide' => $panel_wide,
            'panel_m2' => str_replace(",",".",$panel_m2),
            'panel_pcs_array' => $panel_pcs_array,
            'panel_board_long' => $panel_board_long,
            'panel_board_wide' => $panel_board_wide,
            'panel_pcs_m2' => str_replace(",",".",$panel_pcs_m2),
            'grain_direction' => $grain_direction,
            'punching_direction' => $punching_direction,
            'panel_img' => $panel_img,
            'notrec_sheet_long' => $notrec_sheet_long,
            'notrec_sheet_wide' => $notrec_sheet_wide,
            'notrec_nopanel' => $notrec_nopanel,
            'notrec_pcb' => $notrec_pcb,
            'notrec_yield' => str_replace(",",".",$notrec_yield),
            'notrec_size_longup' => $notrec_size_longup,
            'notrec_size_wideup' => $notrec_size_wideup,
            'notrec_size_longbottom' => $notrec_size_longbottom,
            'notrec_size_widebottom' => $notrec_size_widebottom,
            'notrec_m2' => str_replace(",",".",$notrec_m2),
            'notrec_img' => $notrec_img,
            'rec_sheet_long' => $rec_sheet_long,
            'rec_sheet_wide' => $rec_sheet_wide,
            'rec_nopanel' => $rec_nopanel,
            'rec_pcb' => $rec_pcb,
            'rec_yield' => str_replace(",",".",$rec_yield),
            'rec_size_longup' => $rec_size_longup,
            'rec_size_wideup' => $rec_size_wideup,
            'rec_size_longbottom' => $rec_size_longbottom,
            'rec_size_widebottom' => $rec_size_widebottom,
            'rec_m2' => str_replace(",",".",$rec_m2),
            'rec_img' => $rec_img
        );

        $this->esf_model->add_esf($data);

        $msg = 'Berhasil menambah data enginering specification';

        $result = array(
            'success' => true,
            'message' => $msg
        );

		$this->log_activity->insert_activity('insert', $msg. ' dengan ID Product ' .$data['id_produk']);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is used to delete esf data
      * @return Array
      */
    public function delete_esf() {
        $data   = file_get_contents("php://input");
        $params = json_decode($data,true);

        $this->esf_model->delete_esf($params['id']);

        $msg = 'Berhasil menghapus data enginering specification';

        $result = array(
            'success' => true,
            'message' => $msg
        );
	
		$this->log_activity->insert_activity('delete', $msg. ' dengan ID ESF ' .$params['id']);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is redirect to edit esf page
      * @return Void
      */
    public function edit($id) {
        $detail = $this->esf_model->detail($id);
        $listproducts = $this->esf_model->listproducts($detail[0]['id_po_quotation'],0);

        $data = array(
            'listproducts'    => $listproducts,
            'detail'    => $detail
        );

        $this->load->view('edit_modal_view',$data);
    }

    /**
      * This function is used to edit esf data
      * @return Array
      */
    public function edit_esf() {
        $id = $this->Anti_sql_injection($this->input->post('id', TRUE));
        $id_po_quotation = $this->Anti_sql_injection($this->input->post('id_po_quotation', TRUE));
        $id_produk = $this->Anti_sql_injection($this->input->post('id_produk', TRUE));
        $esf_no = $this->Anti_sql_injection($this->input->post('esf_no', TRUE));
        $esf_date = $this->Anti_sql_injection($this->input->post('esf_date', TRUE));
        $pcs_array = $this->Anti_sql_injection($this->input->post('pcs_array', TRUE));
        $pcb_long = $this->Anti_sql_injection($this->input->post('pcb_long', TRUE));
        $pcb_wide = $this->Anti_sql_injection($this->input->post('pcb_wide', TRUE));
        $pcb_type = $this->Anti_sql_injection($this->input->post('pcb_type', TRUE));
        $unit = $this->Anti_sql_injection($this->input->post('unit', TRUE));
        $panel_long = $this->Anti_sql_injection($this->input->post('panel_long', TRUE));
        $panel_wide = $this->Anti_sql_injection($this->input->post('panel_wide', TRUE));
        $panel_m2 = $this->Anti_sql_injection($this->input->post('panel_m2', TRUE));
        $panel_pcs_array = $this->Anti_sql_injection($this->input->post('panel_pcs_array', TRUE));
        $panel_board_long = $this->Anti_sql_injection($this->input->post('panel_board_long', TRUE));
        $panel_board_wide = $this->Anti_sql_injection($this->input->post('panel_board_wide', TRUE));
        $panel_pcs_m2 = $this->Anti_sql_injection($this->input->post('panel_pcs_m2', TRUE));
        $grain_direction = $this->Anti_sql_injection($this->input->post('grain_direction', TRUE));
        $punching_direction = $this->Anti_sql_injection($this->input->post('punching_direction', TRUE));
        $panel_img = NULL;
        $notrec_sheet_long = $this->Anti_sql_injection($this->input->post('notrec_sheet_long', TRUE));
        $notrec_sheet_wide = $this->Anti_sql_injection($this->input->post('notrec_sheet_wide', TRUE));
        $notrec_nopanel = $this->Anti_sql_injection($this->input->post('notrec_nopanel', TRUE));
        $notrec_pcb = $this->Anti_sql_injection($this->input->post('notrec_pcb', TRUE));
        $notrec_yield = $this->Anti_sql_injection($this->input->post('notrec_yield', TRUE));
        $notrec_size_longup = $this->Anti_sql_injection($this->input->post('notrec_size_longup', TRUE));
        $notrec_size_wideup = $this->Anti_sql_injection($this->input->post('notrec_size_wideup', TRUE));
        $notrec_size_longbottom = $this->Anti_sql_injection($this->input->post('notrec_size_longbottom', TRUE));
        $notrec_size_widebottom = $this->Anti_sql_injection($this->input->post('notrec_size_widebottom', TRUE));
        $notrec_m2 = $this->Anti_sql_injection($this->input->post('notrec_m2', TRUE));
        $notrec_img = NULL;
        $rec_sheet_long = $this->Anti_sql_injection($this->input->post('rec_sheet_long', TRUE));
        $rec_sheet_wide = $this->Anti_sql_injection($this->input->post('rec_sheet_wide', TRUE));
        $rec_nopanel = $this->Anti_sql_injection($this->input->post('rec_nopanel', TRUE));
        $rec_pcb = $this->Anti_sql_injection($this->input->post('rec_pcb', TRUE));
        $rec_yield = $this->Anti_sql_injection($this->input->post('rec_yield', TRUE));
        $rec_size_longup = $this->Anti_sql_injection($this->input->post('rec_size_longup', TRUE));
        $rec_size_wideup = $this->Anti_sql_injection($this->input->post('rec_size_wideup', TRUE));
        $rec_size_longbottom = $this->Anti_sql_injection($this->input->post('rec_size_longbottom', TRUE));
        $rec_size_widebottom = $this->Anti_sql_injection($this->input->post('rec_size_widebottom', TRUE));
        $rec_m2 = $this->Anti_sql_injection($this->input->post('rec_m2', TRUE));
        $rec_img = NULL;

        if ($_FILES['panel_img']['name']) {
            $panel_img = $this->upload('panel_img');
        }

        if ($_FILES['notrec_img']['name']) {
            $notrec_img = $this->upload('notrec_img');
        }

        if ($_FILES['rec_img']['name']) {
            $rec_img = $this->upload('rec_img');
        }

        $data = array(
            'id' => $id,
            'id_po_quotation' => $id_po_quotation,
            'id_produk' => $id_produk,
            'esf_no' => $esf_no,
            'esf_date' => $esf_date,
            'pcs_array' => $pcs_array,
            'pcb_long' => $pcb_long,
            'pcb_wide' => $pcb_wide,
            'pcb_type' => $pcb_type,
            'unit' => $unit,
            'panel_long' => $panel_long,
            'panel_wide' => $panel_wide,
            'panel_m2' => str_replace(",",".",$panel_m2),
            'panel_pcs_array' => $panel_pcs_array,
            'panel_board_long' => $panel_board_long,
            'panel_board_wide' => $panel_board_wide,
            'panel_pcs_m2' => str_replace(",",".",$panel_pcs_m2),
            'grain_direction' => $grain_direction,
            'punching_direction' => $punching_direction,
            'panel_img' => $panel_img,
            'notrec_sheet_long' => $notrec_sheet_long,
            'notrec_sheet_wide' => $notrec_sheet_wide,
            'notrec_nopanel' => $notrec_nopanel,
            'notrec_pcb' => $notrec_pcb,
            'notrec_yield' => str_replace(",",".",$notrec_yield),
            'notrec_size_longup' => $notrec_size_longup,
            'notrec_size_wideup' => $notrec_size_wideup,
            'notrec_size_longbottom' => $notrec_size_longbottom,
            'notrec_size_widebottom' => $notrec_size_widebottom,
            'notrec_m2' => str_replace(",",".",$notrec_m2),
            'notrec_img' => $notrec_img,
            'rec_sheet_long' => $rec_sheet_long,
            'rec_sheet_wide' => $rec_sheet_wide,
            'rec_nopanel' => $rec_nopanel,
            'rec_pcb' => $rec_pcb,
            'rec_yield' => str_replace(",",".",$rec_yield),
            'rec_size_longup' => $rec_size_longup,
            'rec_size_wideup' => $rec_size_wideup,
            'rec_size_longbottom' => $rec_size_longbottom,
            'rec_size_widebottom' => $rec_size_widebottom,
            'rec_m2' => str_replace(",",".",$rec_m2),
            'rec_img' => $rec_img
        );

        $this->esf_model->edit_esf($data);

        $msg = 'Berhasil mengubah data esf';

        $result = array(
            'success' => true,
            'message' => $msg
        );
		
		$this->log_activity->insert_activity('update', $msg. ' dengan ID Product ' .$data['id_produk']);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
}