  <div class="item form-group">
    <table id="listproducts" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>No</th>
          <th>Item</th>
          <th>Descripntion</th>
          <th>Qty</th>
        </tr>
      </thead>
      <tbody>
        <?php 
        $i=1; 
        foreach($listproducts as $key) {?>
          <tr>
            <td><?php echo $i;?></td>
            <td><?php if(isset($key['stock_name'])){ echo $key['stock_name']; }?></td>
            <td><?php if(isset($key['stock_description'])){ echo $key['stock_description']; }?></td>
            <td><?php if(isset($key['qty'])){ echo $key['qty']; }?></td>
          </tr>
        <?php $i++;} ?>
      </tbody>
    </table>
  </div>

<script type="text/javascript">
  $(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>
