  <style>
  #loading-us{display:none}
  #tick{display:none}

  #loading-mail{display:none}
  #cross{display:none}
  .input-size{width:150px;float: left;}
  .input-sep{width: 65px;float: left;text-align: center;font-size: 30px;}
  body .modal{overflow-x: hidden;overflow-y: auto;}
  hr{width: 100%;}
  </style>

  <form class="form-horizontal form-label-left" id="edit_esf" role="form" action="<?php echo base_url('esf/edit_esf');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

    <p style="text-align: center;">Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
    
    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">PO No <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="order_no" name="order_no" class="form-control col-md-7 col-xs-12" placeholder="PO No" required="required" value="<?php if(isset($listproducts[0]['order_no'])){ echo $listproducts[0]['order_no']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Customer Name <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="cust_name" name="cust_name" class="form-control col-md-7 col-xs-12" placeholder="Customer Name" required="required" value="<?php if(isset($listproducts[0]['name_eksternal'])){ echo $listproducts[0]['name_eksternal']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Esf No
        <span class="required">
          <sup>*</sup>
        </span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
            <input data-parsley-maxlength="255" type="text" id="esf_no" name="esf_no" class="form-control col-md-7 col-xs-12" placeholder="Esf No" value="<?php if(isset($detail[0]['esf_no'])){ echo $detail[0]['esf_no']; }?>" readonly>
        </div>
    </div>

    <div class="item form-group form-item">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Item 
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <select class="form-control" name="id_produk" id="id_produk" style="width: 100%" required>
          <option value="" >--Choose Item--</option>
          <?php foreach($listproducts as $key) { ?>
            <option value="<?php echo $key['id_produk']; ?>" <?php if($detail[0]['id_produk'] == $key['id_produk']){ echo 'selected';}?>><?php echo $key['stock_name']; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Esf Date
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <div class="input-group">
           <input placeholder="Esf Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="esf_date" name="esf_date" required="required" value="<?php if(isset($detail[0]['esf_date'])){ echo $detail[0]['esf_date']; }?>">
          <div class="input-group-addon">
            <span class="glyphicon glyphicon-th"></span>
          </div>
        </div>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Pcs /Arrays <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="number" id="pcs_array" name="pcs_array" class="form-control col-md-7 col-xs-12" placeholder="Pcs /Arrays" required="required" min="0" value="<?php if(isset($detail[0]['pcs_array'])){ echo $detail[0]['pcs_array']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Pcb Size (mm) <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="number" id="pcb_long" name="pcb_long" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="<?php if(isset($detail[0]['pcb_long'])){ echo $detail[0]['pcb_long']; }?>">
        <span class="input-sep">x</span>
        <input data-parsley-maxlength="255" type="number" id="pcb_wide" name="pcb_wide" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="<?php if(isset($detail[0]['pcb_wide'])){ echo $detail[0]['pcb_wide']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Pcb Type <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="pcb_type" name="pcb_type" class="form-control col-md-7 col-xs-12" placeholder="Pcb Type" required="required" value="<?php if(isset($detail[0]['pcb_type'])){ echo $detail[0]['pcb_type']; }?>">
      </div>
    </div>

    <div class="item form-group form-item">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Unit 
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <select class="form-control" name="unit" id="unit" style="width: 100%" required>
          <option value="" >--Choose Unit--</option>
          <option value="arrays" <?php if($detail[0]['unit'] == 'arrays'){ echo 'selected';}?>>Arrays</option>
          <option value="pieces" <?php if($detail[0]['unit'] == 'pieces'){ echo 'selected';}?>>Pieces</option>
        </select>
      </div>
    </div>

    <div class="item form-group form-item">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">PANEL LAYOUT</label>
      <hr/>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Panel size (mm) <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="number" id="panel_long" name="panel_long" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="<?php if(isset($detail[0]['panel_long'])){ echo $detail[0]['panel_long']; }?>">
        <span class="input-sep">x</span>
        <input data-parsley-maxlength="255" type="number" id="panel_wide" name="panel_wide" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="<?php if(isset($detail[0]['panel_wide'])){ echo $detail[0]['panel_wide']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Panel M&sup2;
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
          <input data-parsley-maxlength="255" type="text" id="panel_m2" name="panel_m2" class="form-control col-md-7 col-xs-12" value="<?php if(isset($detail[0]['panel_m2'])){ echo $detail[0]['panel_m2']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Pcs/Ary/Panel <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="number" id="panel_pcs_array" name="panel_pcs_array" class="form-control col-md-7 col-xs-12" placeholder="Pcs /Arrays" required="required" min="0" value="<?php if(isset($detail[0]['panel_pcs_array'])){ echo $detail[0]['pcb_long']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Board/Array Size<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="number" id="panel_board_long" name="panel_board_long" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="<?php if(isset($detail[0]['pcb_long'])){ echo $detail[0]['pcb_long']; }?>" readonly>
        <span class="input-sep">x</span>
        <input data-parsley-maxlength="255" type="number" id="panel_board_wide" name="panel_board_wide" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="<?php if(isset($detail[0]['pcb_wide'])){ echo $detail[0]['pcb_wide']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Pcs/Ary M&sup2;
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
          <input data-parsley-maxlength="255" type="text" id="panel_pcs_m2" name="panel_pcs_m2" class="form-control col-md-7 col-xs-12" value="<?php if(isset($detail[0]['panel_pcs_m2'])){ echo $detail[0]['panel_pcs_m2']; }?>">
      </div>
    </div>

    <div class="item form-group form-item">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Grain Direction 
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <select class="form-control" name="grain_direction" id="grain_direction" style="width: 100%" required>
          <option value="yes" <?php if($detail[0]['grain_direction'] == 'yes'){ echo 'selected';}?>>YES</option>
          <option value="no" <?php if($detail[0]['grain_direction'] == 'no'){ echo 'selected';}?>>NO</option>
        </select>
      </div>
    </div>

    <div class="item form-group form-item">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Punching Direction 
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input type="radio" name="punching_direction" value="up" <?php if($detail[0]['punching_direction'] == 'up'){ echo 'checked';}?>/><span style="font-size: 30px;">&#8593;</span>
        <input type="radio" name="punching_direction" value="down" <?php if($detail[0]['punching_direction'] == 'down'){ echo 'checked';}?> style="margin-left: 15px;"/><span style="font-size: 30px;">&#8595;</span>
      </div>
    </div>

    <div class="item form-group has-feedback">
      <div style="text-align: right;margin-right: 50px;">
        <?php
          if(isset($detail[0]['panel_img']) && $detail[0]['panel_img'] != ''){ 
        ?>
          <img src="<?php echo base_url() ."uploads/esf/". $detail[0]['panel_img']; ?>" id="panel_img_temp" style="width: 365px;height: 250px;"/>
        <?php
          } else {
        ?>
        <img src="<?php echo base_url();?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/crossword.png" id="panel_img_temp" style="width: 365px;height: 250px;"/>
        <?php
          } 
        ?>
      </div>
      <div class="col-md-8 col-sm-6 col-xs-12 img-spec" style="padding-top: 10px;margin-left: 155px;">
          <input type="file" class="form-control" id="panel_img" name="panel_img" data-height="110" accept=".jpg, .jpeg, .png"/>
          <span> Hanya format gambar jpg,jpeg,png dengan besaran max 9Mb yang diterima.</span>
      </div>
    </div>

    <div class="item form-group form-item">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama" style="width: 100%;text-align: left;">SHEET SIZE CALCULATION (NOT RECOMMENDED)</label>
      <hr/>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Sheet Size (mm) <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="number" id="notrec_sheet_long" name="notrec_sheet_long" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="<?php if(isset($detail[0]['notrec_sheet_long'])){ echo $detail[0]['notrec_sheet_long']; }?>">
        <span class="input-sep">x</span>
        <input data-parsley-maxlength="255" type="number" id="notrec_sheet_wide" name="notrec_sheet_wide" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="<?php if(isset($detail[0]['notrec_sheet_wide'])){ echo $detail[0]['notrec_sheet_wide']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">No Of Panel/Sheet (panel)<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="number" id="notrec_nopanel" name="notrec_nopanel" class="form-control col-md-7 col-xs-12" placeholder="Pcs /Arrays" required="required" min="0" value="<?php if(isset($detail[0]['notrec_nopanel'])){ echo $detail[0]['notrec_nopanel']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Total PCB/Sheet (arrays)<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="number" id="notrec_pcb" name="notrec_pcb" class="form-control col-md-7 col-xs-12" placeholder="Pcs /Arrays" required="required" min="0" value="<?php if(isset($detail[0]['rec_pcb'])){ echo $detail[0]['rec_pcb']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Yield (%)<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="notrec_yield" name="notrec_yield" class="form-control col-md-7 col-xs-12" placeholder="Pcs /Arrays" required="required" value="<?php if(isset($detail[0]['notrec_yield'])){ echo $detail[0]['notrec_yield']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">End Piece Size (mm) <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="number" id="notrec_size_longup" name="notrec_size_longup" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="<?php if(isset($detail[0]['notrec_size_longup'])){ echo $detail[0]['notrec_size_longup']; }?>">
        <span class="input-sep">x</span>
        <input data-parsley-maxlength="255" type="number" id="notrec_size_wideup" name="notrec_size_wideup" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="<?php if(isset($detail[0]['notrec_size_wideup'])){ echo $detail[0]['notrec_size_wideup']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <div class="col-md-8 col-sm-6 col-xs-12" style="float: right;margin-right: 48px;">
        <input data-parsley-maxlength="255" type="number" id="notrec_size_longbottom" name="notrec_size_longbottom" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="<?php if(isset($detail[0]['notrec_size_longbottom'])){ echo $detail[0]['notrec_size_longbottom']; }?>">
        <span class="input-sep">x</span>
        <input data-parsley-maxlength="255" type="number" id="notrec_size_widebottom" name="notrec_size_widebottom" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="<?php if(isset($detail[0]['notrec_size_widebottom'])){ echo $detail[0]['notrec_size_widebottom']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">End Piece M&sup2;
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
          <input data-parsley-maxlength="255" type="text" id="notrec_m2" name="notrec_m2" class="form-control col-md-7 col-xs-12" value="<?php if(isset($detail[0]['notrec_m2'])){ echo $detail[0]['notrec_m2']; }?>">
      </div>
    </div>

    <div class="item form-group has-feedback">
      <div style="text-align: right;margin-right: 50px;">
        <?php
          if(isset($detail[0]['notrec_img']) && $detail[0]['notrec_img'] != ''){ 
        ?>
          <img src="<?php echo base_url() ."uploads/esf/". $detail[0]['notrec_img']; ?>" id="notrec_img_temp" style="width: 365px;height: 250px;"/>
        <?php
          } else {
        ?>
        <img src="<?php echo base_url();?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/crossword.png" id="notrec_img_temp" style="width: 365px;height: 250px;"/>
        <?php
          } 
        ?>
      </div>
      <div class="col-md-8 col-sm-6 col-xs-12 img-spec" style="padding-top: 10px;margin-left: 155px;">
          <input type="file" class="form-control" id="notrec_img" name="notrec_img" data-height="110" accept=".jpg, .jpeg, .png"/>
          <span> Hanya format gambar jpg,jpeg,png dengan besaran max 9Mb yang diterima.</span>
      </div>
    </div>

    <div class="item form-group form-item">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama" style="width: 100%;text-align: left;">SHEET SIZE CALCULATION (RECOMMENDED)</label>
      <hr style="height: 1px;"/>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Sheet Size (mm) <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="number" id="rec_sheet_long" name="rec_sheet_long" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="<?php if(isset($detail[0]['rec_sheet_long'])){ echo $detail[0]['rec_sheet_long']; }?>">
        <span class="input-sep">x</span>
        <input data-parsley-maxlength="255" type="number" id="rec_sheet_wide" name="rec_sheet_wide" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="<?php if(isset($detail[0]['rec_sheet_wide'])){ echo $detail[0]['rec_sheet_wide']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">No Of Panel/Sheet (panel)<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="number" id="rec_nopanel" name="rec_nopanel" class="form-control col-md-7 col-xs-12" placeholder="Pcs /Arrays" required="required" min="0" value="<?php if(isset($detail[0]['rec_nopanel'])){ echo $detail[0]['rec_nopanel']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Total PCB/Sheet (arrays)<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="number" id="rec_pcb" name="rec_pcb" class="form-control col-md-7 col-xs-12" placeholder="Pcs /Arrays" required="required" min="0" value="<?php if(isset($detail[0]['rec_pcb'])){ echo $detail[0]['rec_pcb']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Yield (%)<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="rec_yield" name="rec_yield" class="form-control col-md-7 col-xs-12" placeholder="Pcs /Arrays" required="required" value="<?php if(isset($detail[0]['rec_yield'])){ echo $detail[0]['rec_yield']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">End Piece Size (mm) <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="number" id="rec_size_longup" name="rec_size_longup" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="<?php if(isset($detail[0]['rec_size_longup'])){ echo $detail[0]['rec_size_longup']; }?>">
        <span class="input-sep">x</span>
        <input data-parsley-maxlength="255" type="number" id="rec_size_wideup" name="rec_size_wideup" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="<?php if(isset($detail[0]['rec_size_wideup'])){ echo $detail[0]['rec_size_wideup']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <div class="col-md-8 col-sm-6 col-xs-12" style="float: right;margin-right: 48px;">
        <input data-parsley-maxlength="255" type="number" id="rec_size_longbottom" name="rec_size_longbottom" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="<?php if(isset($detail[0]['rec_size_longbottom'])){ echo $detail[0]['rec_size_longbottom']; }?>">
        <span class="input-sep">x</span>
        <input data-parsley-maxlength="255" type="number" id="rec_size_widebottom" name="rec_size_widebottom" class="form-control input-size col-md-7 col-xs-12" required="required" min="0" value="<?php if(isset($detail[0]['rec_size_widebottom'])){ echo $detail[0]['rec_size_widebottom']; }?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">End Piece M&sup2;
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
          <input data-parsley-maxlength="255" type="text" id="rec_m2" name="rec_m2" class="form-control col-md-7 col-xs-12" value="<?php if(isset($detail[0]['rec_m2'])){ echo $detail[0]['rec_m2']; }?>">
      </div>
    </div>

    <div class="item form-group has-feedback">
      <div style="text-align: right;margin-right: 50px;">
        <?php
          if(isset($detail[0]['rec_img']) && $detail[0]['rec_img'] != ''){ 
        ?>
          <img src="<?php echo base_url() ."uploads/esf/". $detail[0]['rec_img']; ?>" id="notrec_img_temp" id="rec_img_temp" style="width: 365px;height: 250px;"/>
        <?php
          } else {
        ?>
        <img src="<?php echo base_url();?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/crossword.png" id="rec_img_temp" style="width: 365px;height: 250px;"/>
        <?php
          } 
        ?>
      </div>
      <div class="col-md-8 col-sm-6 col-xs-12 img-spec" style="padding-top: 10px;margin-left: 155px;">
          <input type="file" class="form-control" id="rec_img" name="rec_img" data-height="110" accept=".jpg, .jpeg, .png"/>
          <span> Hanya format gambar jpg,jpeg,png dengan besaran max 9Mb yang diterima.</span>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <button id="btn-submitdata" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Esf</button>
      </div>
    </div>

  <input type="hidden" id="id_po_quotation" name="id_po_quotation" value="<?php if(isset($listproducts[0]['id_po_quotation'])){ echo $listproducts[0]['id_po_quotation']; }?>">
  <input type="hidden" id="id" name="id" value="<?php if(isset($detail[0]['id'])){ echo $detail[0]['id']; }?>">
</form><!-- /page content -->

<script type="text/javascript">
  $(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();

    $("#esf_date").datepicker({
      format: 'yyyy-mm-dd',
      autoclose: true,
      todayHighlight: true,
    });
  });

  $('#pcb_long').on('change',(function(e) {
    var pcb_long = $(this).val();
    $('#panel_board_long').val(pcb_long);
  }));

  $('#pcb_wide').on('change',(function(e) {
    var pcb_wide = $(this).val();
    $('#panel_board_wide').val(pcb_wide);
  }));

  $('#panel_img').on('change',(function(e) {
    if (this.files && this.files[0]) {
      var reader = new FileReader();
          reader.onload = (function(theFile) {
              var image = new Image();
                  image.src = theFile.target.result;
              
                  image.onload = function() {
                      $("#panel_img_temp").attr('src', this.src);
                  };
          });
      reader.readAsDataURL(this.files[0]);
    }
  }));

  $('#notrec_img').on('change',(function(e) {
    if (this.files && this.files[0]) {
      var reader = new FileReader();
          reader.onload = (function(theFile) {
              var image = new Image();
                  image.src = theFile.target.result;
              
                  image.onload = function() {
                      $("#notrec_img_temp").attr('src', this.src);
                  };
          });
      reader.readAsDataURL(this.files[0]);
    }
  }));

  $('#rec_img').on('change',(function(e) {
    if (this.files && this.files[0]) {
      var reader = new FileReader();
          reader.onload = (function(theFile) {
              var image = new Image();
                  image.src = theFile.target.result;
              
                  image.onload = function() {
                      $("#rec_img_temp").attr('src', this.src);
                  };
          });
      reader.readAsDataURL(this.files[0]);
    }
  }));

  $('#edit_esf').on('submit',(function(e) {
    $('#btn-submitdata').attr('disabled','disabled');
    $('#btn-submitdata').text("Mengubah data...");
    e.preventDefault();

    var formData = new FormData(this);
        formData.set("punching_direction", $('input[name="punching_direction"]:checked').val());

    $.ajax({
        type:'POST',
        url: $(this).attr('action'),
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(response) {
            if (response.success == true) {
              $('.panel-heading button').trigger('click');
                listesf();
                swal({
                  title: 'Success!',
                  text: response.message,
                  type: 'success',
                  showCancelButton: false,
                  confirmButtonText: 'Ok'
                }).then(function () {
                });
            } else{
                $('#btn-submitdata').removeAttr('disabled');
                $('#btn-submitdata').text("Edit Esf");
                swal("Failed!", response.message, "error");
            }
        }
    }).fail(function(xhr, status, message) {
        $('#btn-submitdata').removeAttr('disabled');
        $('#btn-submitdata').text("Edit Esf");
        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
    });
  }));
</script>
