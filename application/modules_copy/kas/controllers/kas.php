<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Kas extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('kas/kas_model');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	  * This function is redirect to index kas page
	  * @return Void
	  */
	public function index() {
		$this->template->load('maintemplate', 'kas/views/index');
	}

	/**
	  * This function is used for showing kas list
	  * @return Array
	  */
	function lists() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;
		$order_fields = array('', 'id', 'date');

		$search = $this->input->get_post('search');
		$coa = $this->input->get_post('coa');
		$type = $this->input->get_post('type');
		$valas = $this->input->get_post('valas');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;
		$params['coa'] = $coa;
		$params['type'] = $type;
		$params['valas'] = $valas;

		$list = $this->kas_model->lists($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["coafilter"] = $this->kas_model->coa("id_parent",0);
		$result["valasfilter"] = $this->kas_model->valas();
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$actions = '<div class="btn-group">';
			$actions .= '   <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edit_kas(\'' . $v['id'] . '\')">';
			$actions .= '       <i class="fa fa-edit"></i>';
			$actions .= '   </button>';
			$actions .= '</div>';
			$actions .= '<div class="btn-group">';
			$actions .= '   <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="delete_kas(\'' . $v['id'] . '\')">';
			$actions .= '       <i class="fa fa-trash"></i>';
			$actions .= '   </button>';
			$actions .= '</div>';

			array_push($data, array(
				$i,
				$v['perkiraan'],
				$v['coa'],
				$v['keterangan'],
				$v['date'],
				$v['nama_valas'],
				$v['value'],
				$v['type'],
				$actions
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	/**
	  * This function is redirect to add kas page
	  * @return Void
	  */
	public function add() {
		$level1 = $this->kas_model->coa("id_parent",0);
		$level2 = $this->kas_model->coa("id_parent",$level1[0]["coa"]);
		$level3 = $this->kas_model->coa("id_parent",$level2[0]["coa"]);
		$level4 = $this->kas_model->coa("id_parent",$level3[0]["coa"]);

		$valas = $this->kas_model->valas();

		$data = array(
			'level1' => $level1,
			'level2' => $level2,
			'level3' => $level3,
			'level4' => $level4,
			'valas' => $valas
		);

		$this->load->view('add_modal_view',$data);
	}

	/**
	  * This function is used to add coa_value data
	  * @return Array
	  */
	public function add_kas() {
		$this->form_validation->set_rules('id_coa', 'COA', 'trim|required');
		$this->form_validation->set_rules('date', 'Tanggal', 'trim|required');
		$this->form_validation->set_rules('id_valas', 'Valas', 'trim|required');
		$this->form_validation->set_rules('rate', 'Rate', 'trim|required');
		$this->form_validation->set_rules('value', 'Nilai', 'trim|required');
		$this->form_validation->set_rules('type_cash', 'Type Cash', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$id_coa			= $this->Anti_sql_injection($this->input->post('id_coa', TRUE));
			$id_parent		= $this->Anti_sql_injection($this->input->post('id_parent', TRUE));
			$date 			= $this->Anti_sql_injection($this->input->post('date', TRUE));
			$id_valas 		= $this->Anti_sql_injection($this->input->post('id_valas', TRUE));
			$value 			= $this->Anti_sql_injection($this->input->post('value', TRUE));
			$rate 			= $this->Anti_sql_injection($this->input->post('rate', TRUE));
			$type_cash 		= $this->Anti_sql_injection($this->input->post('type_cash', TRUE));
			$note 			= $this->Anti_sql_injection($this->input->post('note', TRUE));
			$upload_error 	= NULL;
			$file_kas 		= NULL;

			if(sizeof($_FILES) > 0) {
				if ($_FILES['file_kas']['name']) {
					$this->load->library('upload');

					$config = array(
						'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/kas",
						'upload_url' => base_url() . "uploads/kas",
						'encrypt_name' => FALSE,
						'max_filename' => 100,
						'file_name' => $_FILES['file_kas']['name'],
						'overwrite' => FALSE,
						'allowed_types' => 'pdf|txt|doc|docx',
						'max_size' => '10000'
					);
					$this->upload->initialize($config);

					if ($this->upload->do_upload("file_kas")) {
						// General result data
						$result = $this->upload->data();

						// Add our stuff
						$file_kas = 'uploads/kas/'.$result['file_name'];
					}else {
						$pesan = $this->upload->display_errors();
						$upload_error = strip_tags(str_replace("\n", '', $pesan));

						$result = array(
							'success' => false,
							'message' => $upload_error
						);
					}
				}
			}
		}
		if (!isset($upload_error)) {
			$data = array(
				'id_coa'		=> $id_coa,
				'id_parent'		=> $id_parent,
				'id_valas'		=> $id_valas,
				'value'			=> $value,
				'date'			=> date($date),
				'type_cash'		=> $type_cash,
				'note'			=> $note,
				'rate'			=> $rate,
				'bukti'			=> $file_kas
			);

			$data =  array(
				'id_coa'		=> $id_coa,
				'id_parent'		=> $id_parent,
				'id_valas'		=> $id_valas,
				'value'			=> $value,
				'date'			=> date($date),
				'type_cash'		=> $type_cash,
				'note'			=> $note,
				'rate'			=> $rate,
				'bukti'			=> $file_kas
			);
			
			$result = $this->kas_model->add_approve_kas($data);

			if ($result > 0) {
				$msg = 'Berhasil menambahkan KAS.';
				$result = array('success' => true, 'message' => $msg);
			}else {
				$msg = 'Gagal menambahkan KAS ke database.';
				$result = array('success' => false, 'message' => $msg);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	/**
	  * This function is used to delete kas data
	  * @return Array
	  */
	public function delete_kas() {
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);
		$result		= $this->kas_model->detail($params['id']);

		//Delete File if exists
		$filepath = dirname($_SERVER["SCRIPT_FILENAME"]) ."/". $result[0]['bukti'];
		if (is_file($filepath)) {
			unlink($filepath);
		}

		$this->kas_model->delete_kas($params['id']);

		$msg = 'Berhasil menghapus data kas.';

		$result = array(
			'success' => true,
			'message' => $msg
		);


		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	/**
	  * This function is redirect to edit kas page
	  * @return Void
	  */
	public function edit($id) {
		$valas = $this->kas_model->valas();

		$detail = $this->kas_model->detail($id);
		
		if(isset($detail[0]['bukti'])){
			$file = explode('.',$detail[0]['bukti']);
			$files = explode('/',$detail[0]['bukti']);
			$filetype = $file[1];
			$filename = $files[2];
			$fileupload = $files[2];
		}else{
			$filetype = NULL;
			$fileupload = NULL;
		}
		
		if($detail[0]['id_parent'] == $detail[0]['id_coa']){
			$level1 = $this->kas_model->coa("id_coa",$detail[0]['id_coa']);

			$level1_id = $level1[0]['coa'];
			$level2    = array();
			$level2_id = '';
			$level3    = array();  
			$level3_id = '';
			$level4    = array(); 
			$level4_id = '';
		}else{
			$level3 = $this->kas_model->coa("id_coa",$detail[0]['id_coa']);
			$level2 = $this->kas_model->coa("coa",$level3[0]['id_parent']);
			$level1 = $this->kas_model->coa("coa",$level2[0]['id_parent']);

			if(count($level1) == 0){
				$level1_id = $level2[0]['coa'];

				$level2_id = $level3[0]['coa'];
				$level2 = $this->kas_model->coa("id_parent",$level3[0]['id_parent']);
				
				$level3 = array();
				$level3_id = '';
				$level4    = array();
				$level4_id = '';
			}else{
				$level1_id = $level1[0]['id_parent'];

				$level4    = $this->kas_model->coa("id_parent",$level3[0]['id_parent']);
				$level4_id = $level3[0]['coa'];

				$level3 = $level2;
				$level3_id = $level2[0]['coa'];

				$level2_id = $level1[0]['coa'];
				$level2 = $this->kas_model->coa("id_parent",$level1[0]['id_parent']);
				
				if($level1_id == '0'){
					$level1_id = $level1[0]['coa'];

					$level2_id = $level3_id;
					$level2 = $this->kas_model->coa("id_parent",$level3[0]['id_parent']);

					$level3 = $level4;
					$level3_id = $level4_id;

					$level4    = array();
					$level4_id = '';
				}
			}
		}

		$level1 = $this->kas_model->coa("id_parent",0);

		$data = array(
			'level1'    => $level1,
			'level1_id' => $level1_id,
			'level2'    => $level2,
			'level2_id' => $level2_id,
			'level3'    => $level3,
			'level3_id' => $level3_id,
			'level4'    => $level4,
			'level4_id' => $level4_id,
			'valas'     => $valas,
			'detail'    => $detail,
			'filename' 	=> $filename,
			'filetype' 	=> $filetype,
			'fileupload' => $fileupload
		);

		$this->load->view('edit_modal_view', $data);
	}

	/**
	  * This function is used to update kas data
	  * @return Array
	  */
	public function edit_kas() {
		$id_coas = 0;
		$id_parents = $this->Anti_sql_injection($this->input->post('level1', TRUE));
		
		if($this->Anti_sql_injection($this->input->post('level4', TRUE))) {
			$id_coas = $this->Anti_sql_injection($this->input->post('level4', TRUE));
		}elseif($this->Anti_sql_injection($this->input->post('level3', TRUE))){
			$id_coas = $this->Anti_sql_injection($this->input->post('level3', TRUE));
		}elseif($this->Anti_sql_injection($this->input->post('level2', TRUE))){
			$id_coas = $this->Anti_sql_injection($this->input->post('level2', TRUE));
		}else{
			$id_coas = $id_parents;
		}
		
		//$this->form_validation->set_rules('id_coa', 'COA', 'trim|required');
		$this->form_validation->set_rules('date', 'Tanggal', 'trim|required');
		$this->form_validation->set_rules('id_valas', 'Valas', 'trim|required');
		$this->form_validation->set_rules('rate', 'Rate', 'trim|required');
		$this->form_validation->set_rules('value', 'Nilai', 'trim|required');
		$this->form_validation->set_rules('type_cash', 'Type Cash', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			$id				= $this->Anti_sql_injection($this->input->post('id', TRUE));
			$id_coa			= $id_coas;
			$id_parent		= $id_parents;
			$date 			= $this->Anti_sql_injection($this->input->post('date', TRUE));
			$id_valas 		= $this->Anti_sql_injection($this->input->post('id_valas', TRUE));
			$value 			= $this->Anti_sql_injection($this->input->post('value', TRUE));
			$rate 			= $this->Anti_sql_injection($this->input->post('rate', TRUE));
			$type_cash 		= $this->Anti_sql_injection($this->input->post('type_cash', TRUE));
			$note 			= $this->Anti_sql_injection($this->input->post('note', TRUE));
			$old_file		= $this->Anti_sql_injection($this->input->post('old_file', TRUE));
			$upload_error 	= NULL;
			$file_kas 		= NULL;
			
			//var_dump($_FILES);die;
			if ($_FILES['file_kas']['name']) {
				$this->load->library('upload');
					
				$config = array(
					'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/kas",
					'upload_url' => base_url() . "uploads/kas",
					'encrypt_name' => FALSE,
					'max_filename' => 100,
					'file_name' => $_FILES['file_kas']['name'],
					'overwrite' => FALSE,
					'allowed_types' => 'pdf|txt|doc|docx',
					'max_size' => '10000'
				);
				$this->upload->initialize($config);

				if ($this->upload->do_upload("file_kas")) {
					// General result data
					$result = $this->upload->data();

					// Add our stuff
					$file_kas = 'uploads/kas/'.$result['file_name'];
				} else {
					$pesan = $this->upload->display_errors();
					$upload_error = strip_tags(str_replace("\n", '', $pesan));

					$msg = $upload_error;
					
					$result = array(
						'success' => false,
						'message' => $msg
					);
					
					$this->log_activity->insert_activity('update', $msg. ' dengan ID Kas ' .$id);
				}
			}

			if (!isset($upload_error)) {
				if($file_kas == '' || $file_kas == NULL) {
					$file_kas = $old_file;
				} else {
					$filepath = dirname($_SERVER["SCRIPT_FILENAME"]) .''.$old_file;
					if (is_file($filepath)) {
						unlink($filepath);
					}
				}

				$data = array(
					'id'			=> $id,
					'id_coa'		=> $id_coa,
					'id_parent'		=> $id_parent,
					'id_valas'		=> $id_valas,
					'value'			=> $value,
					'date'			=> $date,
					'type_cash'		=> $type_cash,
					'note'			=> $note,
					'rate'			=> $rate,
					'bukti'			=> $file_kas
				);

				$result = $this->kas_model->edit_kas($data);

				if ($result > 0) {
					$msg = 'Berhasil merubah KAS.';
					$result = array('success' => true, 'message' => $msg);
				} else {
					$msg = 'Gagal merubah KAS.';
					$result = array('success' => false, 'message' => $msg);
				}
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	/**
	  * This function is used to change coa list
	  * @return Array
	  */
	public function change_coa() {
		$id = $this->input->get('id');
		$result = $this->kas_model->coa("id_parent",$id);

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
}