<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kas_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	/**
      * This function is get the list data in coa_value table
      * @param : $params is where condition for select query
      */

	public function lists($params = array())
	{
		$sql_all 	= 'CALL coavalue_list_all(?, ?, ?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				'',
				$params['coa'],
				$params['type'],
				$params['valas']
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL coavalue_list(?, ?, ?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter'],
				$params['coa'],
				$params['type'],
				$params['valas']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total_row,
			'total' => $total_row,
		);

		return $return;
	}

	/**
      * This function is get the list data in coa table
      * @param : $params is where condition for select query
      */
	public function coa($search,$id)
	{
		$this->db->select('id_coa,id_parent,coa,keterangan');
		$this->db->from('t_coa');
		$this->db->where($search, $id);

		$query 	= $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is get the list data in valas table
      */
	public function valas()
	{
		$this->db->select('valas_id,nama_valas');
		$this->db->from('m_valas');

		$query 	= $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is used to Insert Record in coa_value table
      * @param : $data - record array 
      */

	public function add_kas($data) {
		$sql 	= 'CALL coavalue_add2(?,?,?,?,?,?,?,?,?,?)';

		$query = $this->db->query($sql,array(
			$data['id_coa'],
			$data['id_parent'],
			$data['date'],
			$data['id_valas'],
			$data['value'],
			0,
			$data['type_cash'],
			$data['note'],
			$data['rate'],
			$data['bukti']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function add_approve_kas($data) {
		$sql 	= 'CALL coavalue_add_temporary(?,?,?,?,?,?,?,?,?,?)';

		$query = $this->db->query($sql,array(
			$data['id_coa'],
			$data['id_parent'],
			$data['date'],
			$data['id_valas'],
			$data['value'],
			0,
			$data['type_cash'],
			$data['note'],
			$data['rate'],
			$data['bukti']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	/**
     * This function is used to delete coa_value
     * @param: $id - id of coa_value table
     */
	function delete_kas($id) {
		$this->db->where('id', $id);  
		$this->db->delete('t_coa_value'); 

		$this->db->close();
		$this->db->initialize();
	}

	/**
      * This function is get data in coa_value table by id
      * @param : $id is where condition for select query
      */

	public function detail($id)
	{
		$sql 	= 'CALL coavalue_search_id(?)';

		$query 	= $this->db->query($sql,array(
				$id
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is used to Insert Record in coa_value table
      * @param : $data - record array 
      */

	public function edit_kas($data) {
		$sql 	= 'CALL coavalue_update(?,?,?,?,?,?,?,?,?,?)';

		$this->db->query($sql, array(
			$data['id'],
			$data['id_coa'],
			$data['id_parent'],
			$data['date'],
			$data['id_valas'],
			$data['value'],
			$data['type_cash'],
			$data['note'],
			$data['rate'],
			$data['bukti']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}