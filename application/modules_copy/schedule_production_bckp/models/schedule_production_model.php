<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Schedule_Production_Model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	/**
      * This function is get the list data in production schedule table
      * @param : $params is where condition for select query
      */

	public function lists($params = array())
	{
		$sql_all 	= 'CALL prodsche_list_all(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				$params['filter'],
				$params['cust_id']
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL prodsche_list(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter'],
				$params['cust_id']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total_row,
			'total' => $total_row,
		);

		return $return;
	}

	/**
      * This function is get data in production_schedule table by id
      * @param : $id is where condition for select query
      */

	public function detail($id)
	{
		$sql 	= 'SELECT 
					      order_no,
					      id_produk,
					      stock_name,
					      t_order.qty AS order_qty,
					      IFNULL(( SELECT SUM(qty) FROM t_production_schedule WHERE t_po_quotation.id=t_production_schedule.id_po_quot AND t_production_schedule.id_produk=t_order.id_produk),0) AS jumlah_run_down_qty
					FROM 
					      t_po_quotation 
					       INNER JOIN t_order ON t_po_quotation.id=t_order.id_po_quotation 
					       INNER JOIN m_material ON t_order.id_produk=m_material.id
					WHERE 
					      t_po_quotation.id=?';

		$query 	=  $this->db->query($sql,
			array(
				$id
			));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is get data in production_schedule table by id
      * @param : $id is where condition for select query
      */

	public function edit_schedule_production($data)
	{
		$schedulDeletedList = $data['schedulDeletedList'];
		$deletedItemsLen = count($schedulDeletedList);

		$schedulList = $data['schedulList'];
		$itemsLen = count($schedulList);

		$produkTemp = '';

		if($itemsLen>0){
			for($i=0;$i<$deletedItemsLen;$i++){
				if($schedulDeletedList[$i]['id_produk'] != $produkTemp){
					$this->db->where('id_po_quot', $schedulDeletedList[$i]['id_po_quot']);
					$this->db->where('id_produk', $schedulDeletedList[$i]['id_produk']);
					  
					$this->db->delete('t_production_schedule');

					$produkTemp = $schedulDeletedList[$i]['id_produk'];
				}
			}

			for($j=0;$j<$itemsLen;$j++){
				$sql 	= 'CALL prodsche_add(?, ?, ?, ?, ?, ?, ?)';

				$this->db->query($sql,array(
					$schedulList[$j]['id_po_quot'],
					$schedulList[$j]['id_produk'],
					$schedulList[$j]['date_issue'],
					$schedulList[$j]['lot_number'],
					$schedulList[$j]['qty'],
					$schedulList[$j]['qty_sheet'],
					$schedulList[$j]['due_date']
				));
			}
		}

		$this->db->close();
		$this->db->initialize();
	}

	/**
      * This function is get data in production_schedule table by id
      * @param : $id is where condition for select query
      */

	public function detail_schedule($id,$id_produk)
	{
		$sql 	= 'CALL prodsche_search_id(?, ?)';

		$query 	=  $this->db->query($sql,
			array(
				$id,
				$id_produk
			));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is get data in esf table by id_po_quotation and id_produk
      * @param : $id is where condition for select query
      */

	public function detail_esf($id_po_quotation,$id_produk)
	{
		$sql 	= 'SELECT 
					      rec_nopanel,
					      rec_pcb
					FROM 
					      t_esf 
					WHERE 
					      id_po_quotation = ? AND id_produk=?';

		$query 	=  $this->db->query($sql,
			array(
				$id_po_quotation,
				$id_produk
			));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
}