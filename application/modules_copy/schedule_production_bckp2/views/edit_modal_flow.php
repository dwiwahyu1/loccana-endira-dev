<style>
   .form-item{margin-top: 15px;overflow: auto;}
</style>

<p style="text-align:center;">Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>


<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Process Flow</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
	   	<select class="form-control col-md-7 col-xs-12"  id="ps" name="ps"  >
		<?php foreach($proses_flow as $proses_flows){ ?>
			
			<option value = '<?php echo $proses_flows['id'] ?>'><?php echo $proses_flows['name'] ?></option>
			
		<?php } ?>
		</select>
	</div>
</div>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Instruction</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
	   	<input data-parsley-maxlength="255" type="text" id="instruction" name="instruction" class="form-control col-md-7 col-xs-12" placeholder="instruction" value="">
	</div>
</div>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Sequence</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
	   	<input data-parsley-maxlength="255" type="text" id="sequence" name="sequence" class="form-control col-md-7 col-xs-12" placeholder="sequence" value="">
	</div>
</div>


<div class="item form-group form-item" id="btn-addtemp" style="margin-bottom: 0px;">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama" style="float: right;">
		<button id="btn-add" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">
			<i class="fa fa-plus"></i> Add
		</button>
	</label>
</div>

<div class="item form-group form-item" id="scheduletext" style="margin-top: 0px;">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Schedule List : </label>
</div>

<div class="item form-group form-item" id="schedulelist">
	<table id="listschedule" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th>Sequence</th>
				<th>Process Flow</th>
				<th>Instruction</th>
				<th>Option</th>
			</tr>

		</thead>
		<tbody id="bodyss">
			 <?php foreach($detail as $detailf){ ?>
                        <tr>
                         <td><?php echo $detailf['seqeuence'] ?></td>
                          <td><?php echo $detailf['name'] ?></td>
                          <td><?php echo $detailf['instruction'] ?></td>
                          <td></td>
                        </tr>
						<?php } ?>
		</tbody>
	</table>
</div>



<!-- /page content -->

<script type="text/javascript">
	var scheduleArray = [],
		qtyTemp = 0;

	$('#btn-add').on('click',(function(e) {
	
			 var form_data = {
              ps     : $('#ps').val(),
              sequence      : $('#sequence').val(),
              instruction	 : $('#instruction').val(),
              id	 : '<?php echo $id ?>',
              schd	 : '<?php echo $schd ?>'
			  
          };       
	
		    $.ajax({
              url : "<?php echo base_url().'schedule_production/save_flow'?>",
              method : "POST",
              data : form_data,
              success: function(response) {
                  var rsd = response;
				  
				  //console.log(rsd[0]);
				  
				  lgth = rsd.length;
                  
				  $('#bodyss').html('');
				  
				  
				  var txhth = '';
				   for(var i=0;i<lgth;i++){
					   
					 txhth +=  "<tr>";
                     txhth +=   " <td>"+rsd[i].seqeuence+"</td>";
                     txhth +=   "  <td>"+rsd[i].name+"</td>";
                     txhth +=   "  <td>"+rsd[i].instruction+"</td>";
                     txhth +=    " <td></td>"
                      txhth +=  "</tr>";
					   
				   }
				   $('#bodyss').html(txhth);
				  
                  //chart          
                  //create_chart(response.data,colgroup,profile);
                  //end chart		
              },
              error: function(obj, response) {
                  console.log('ajax list_project error:' + response);
              }
          });   
	
	}));

	$('#btn-submitschedule').on('click',(function(e) {
	
	}));

	function deleteSchedule(id,max){
		$('#schedule_temp'+id).remove();
	    scheduleArray.splice(id,1);

	    qtyMax = qtyMax + max;
		$('#qty').attr('max',qtyMax);

	    listSchedule();

	    $('#btn-addtemp').show();
	}


	function currencyFormatDE(num) {
		var num = parseFloat(num);
	  return (
	    num
	      .toFixed(2) // always two decimal digits
	      .replace('.', ',') // replace decimal point character with ,
	      .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
	  ) // use . as a separator
	}
</script>
