<style>
  .filter_separator{padding:0px 10px;}
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Schedule Production</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">

                    <table id="listsp" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Process Flow</th>
                          <th>Insctruction</th>
                        </tr>
                      </thead>
                      <tbody>
					  <?php foreach($detail as $detailf){ ?>
                        <tr>
                         <th><?php echo $detailf['sequence'] ?></th>
                          <th><?php echo $detailf['id_process_flow'] ?></th>
                          <th><?php echo $detailf['instruction'] ?></th>
                        </tr>
						<?php } ?>
                      </tbody>
                    </table>

            </div>
        </div><!-- end col -->
    </div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content p-0 b-0">
      <div class="panel panel-color panel-primary panel-filled">
        <div class="panel-heading">
          <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 class="panel-title"></h3>
        </div>
        <div class="panel-body">
          <p></p>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <p></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
	$(document).ready(function(){
      listsp();
  });

  function listsp(){


      $("#listsp").dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo base_url().'schedule_production/lists';?>",
            "searchDelay": 700,
            "responsive": true,
            "lengthChange": false,
            "destroy": true,
            "info": false,
            "bSort": false,
			"bFilter": false,
            //"dom": 'l<"toolbar">frtip',
            "initComplete": function(result){
               
            }
      });
  }
</script>