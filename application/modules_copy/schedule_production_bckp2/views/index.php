<style>
  .filter_separator{padding:0px 10px;}
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Schedule Production</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">

                    <table id="listsp" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Quotation No</th>
                          <th>PO No</th>
                          <th>Customer</th>
                          <th>Total Products</th>
                          <th>Total Schedules</th>
                          <th>Order Qty</th>
                          <th>Total Run Down Qty(array)</th>
                          <th>Option</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>No</td>
                          <td>Quotation No</td>
                          <td>PO No</td>
                          <td>Customer</td>
                          <td>Total Products</td>
                          <td>Total Schedules</td>
                          <td>Order Qty</td>
                          <td>Total Run Down Qty</td>
                          <td>Option</td>
                        </tr>
                      </tbody>
                    </table>

            </div>
        </div><!-- end col -->
    </div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content p-0 b-0">
      <div class="panel panel-color panel-primary panel-filled">
        <div class="panel-heading">
          <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 class="panel-title"></h3>
        </div>
        <div class="panel-body">
          <p></p>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width:800px;">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <p></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
	$(document).ready(function(){
      listsp();
  });

  function listsp(){
      var cust_id = (typeof $('.select_customer').val() !== "undefined")?$('.select_customer').val():'0';

      $("#listsp").dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": "<?php echo base_url().'schedule_production/lists?cust_id="+cust_id+"';?>",
            "searchDelay": 700,
            "responsive": true,
            "lengthChange": false,
            "destroy": true,
            "info": false,
            "bSort": false,
            "dom": 'l<"toolbar">frtip',
            "initComplete": function(result){
                var element = '<div class="btn-group pull-left">';
                    element += '  <label>Filter</label>';
                    element += '  <label class="filter_separator">|</label>';
                    element += '  <label>Customer:';
                    element += '    <select class="form-control select_customer" onChange="listsp()" style="height:30px;width: 340px;">';
                    element += '      <option value="0">-- Semua Customer --</option>';
                    $.each( result.json.customer, function( key, val ) {
                      var selectcust = (val.id===cust_id)?'selected':'';
                            element += '    <option value="'+val.id+'" '+selectcust+'>'+val.name_eksternal+'</option>';
                        });
                    element += '    </select>';
                    element += '  </label>';
                        
               $("#listsp_filter").prepend(element);
            }
      });
  }
  
  	function save_all(po_quot,id_prod){
		
			 var form_data = {
              po_quot: po_quot,
              schd	 : id_prod
			  
          };       
	
		    $.ajax({
              url : "<?php echo base_url().'schedule_production/save_flow_all'?>",
              method : "POST",
              data : form_data,
              success: function(response) {
                 
              },
              error: function(obj, response) {
                  console.log('ajax list_project error:' + response);
              }
          }); 
		
	}
  
  
  function edit_sp(id){
    $('#panel-modal').removeData('bs.modal');
    $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    $('#panel-modal  .panel-body').load('<?php echo base_url('schedule_production/edit/');?>'+"/"+id);
    $('#panel-modal  .panel-title').html('<i class="fa fa-pencil"></i> Proses Schedule');
    $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
  }

  function edit_schedule(name,id,id_produk){
      $('#panel-modalchild').removeData('bs.modal');
      $('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modalchild  .panel-body').load('<?php echo base_url('schedule_production/edit_schedule/');?>'+"/"+id+"/"+id_produk);
      $('#panel-modalchild  .panel-title').html('<i class="fa fa-pencil"></i> Edit '+name);
      $('#panel-modalchild').modal({backdrop:'static',keyboard:false},'show');
  }
  
  function edit_flow(id,id_sch){
      $('#panel-modalchild').removeData('bs.modal');
      $('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modalchild  .panel-body').load('<?php echo base_url('schedule_production/edit_flow/');?>'+"/"+id+"/"+id_sch);
      $('#panel-modalchild  .panel-title').html('<i class="fa fa-pencil"></i> Edit '+name);
      $('#panel-modalchild').modal({backdrop:'static',keyboard:false},'show');
  }
</script>