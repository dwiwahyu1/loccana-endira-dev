<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule_Production extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('quotation/quotation_model');
        $this->load->model('schedule_production/schedule_production_model');
		$this->load->library('log_activity');
    }

    /**
     * anti sql injection
     */
    public function Anti_sql_injection($string) {
        $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
        return $string;
    }

    public function index() {
        $this->template->load('maintemplate', 'schedule_production/views/index');
    }

    /**
      * This function is used for showing schedule production list
      * @return Array
      */
    function lists() {
        $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
        $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
        $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
        $order = $this->input->get_post('order');
        $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'asc';
        $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

        $order_fields = array('', 'id_prod', 'name_eksternal', 'order_no');

        $search = $this->input->get_post('search');

        $search_val = (!empty($search['value'])) ? $search['value'] : null;

        $search = $this->input->get_post('search');
        $cust_id = $this->input->get_post('cust_id');

        $search_value = $this->Anti_sql_injection($search_val);

        // Build params for calling model
        $params['limit'] = (int) $length;
        $params['offset'] = (int) $start;
        $params['order_column'] = $order_fields[$order_column];
        $params['order_dir'] = $order_dir;
        $params['filter'] = $search_value;
        $params['cust_id'] = $cust_id;

        $list = $this->schedule_production_model->lists($params);

        $result["recordsTotal"] = $list['total'];
        $result["recordsFiltered"] = $list['total_filtered'];
        $result["customer"] = $this->quotation_model->customer();
        $result["draw"] = $draw;

        $data = array();
		
		//print_r($list['data']);die;

        $i = $params['offset'];
        foreach ($list['data'] as $k => $v) {
            $i++;
			
            $actions = '<div class="btn-group">';
            $actions .= '   <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Proses Schedule" onClick="edit_sp(\'' . $v['id_po_quot'] . '\')">';
            $actions .= '       <i class="fa fa-edit"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';
			
			// $actions .= '<div class="btn-group">';
            // $actions .= '   <a href="schedule_production/proses_flow/'. $v['id_po_quot'].'" class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Proses Flow">';
            // $actions .= '       <i class="fa fa-edit"></i>';
            // $actions .= '  </a> ';
            // $actions .= '</div>';
			
			$actions .= '<div class="btn-group">';
            $actions .= '   <button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Proses Schedule" onClick="edit_flow(\'' . $v['id_po_quot'] . '\',\'' . $v['id_prod'] . '\')">';
            $actions .= '       <i class="fa fa-edit"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';
			
			
			$actions .= '<div class="btn-group">';
            $actions .= '   <button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Save Schedule" onClick="save_all(\'' . $v['id_po_quot'] . '\',\'' . $v['id_prod'] . '\')">';
            $actions .= '       <i class="fa fa-check-circle"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';
			
            
            array_push($data, array(
                $i,
                $v['id_po_quot'],
                $v['order_no'],
                $v['name_eksternal'],
                $v['total_products'],
                $v['total_schedules'],
                number_format($v['order_qty'],2,",","."),
                number_format($v['total_run_down_qty'],2,",","."),
                $actions
                    )
            );
        }

        $result["data"] = $data;

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
    * This function is redirect to edit schedule production page
    * @return Void
    */
    public function edit($id) {
        $detail = $this->schedule_production_model->detail($id);

        $data = array(
              'id'    => $id,
              'detail'=> $detail
        );

        $this->load->view('edit_modal_view',$data);
    }
	
    public function proses_flow() {
		
		$id = $this->uri->segment(3);
		//$detail = $this->schedule_production_model->detail_schedule_l($id);

		//print_r($detail);die;

        $data = array(
              'detail'=> ''
        );

        //$this->load->view('proses',$data);
		
       $this->template->load('maintemplate', 'schedule_production/views/proses',$data);
    }

    /**
      * This function is used to edit schedule of schedule production data
      * @return Array
      */
    public function edit_schedule_production() {
        $data   = file_get_contents("php://input");
        $params = json_decode($data,true);

        $schedule = $this->schedule_production_model->edit_schedule_production($params);

        $msg = 'Berhasil mengubah schedule production';

        $result = array(
            'success' => true,
            'message' => $msg
        );

		$this->log_activity->insert_activity('update', $msg);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
    * This function is redirect to edit schedule production page
    * @return Void
    */
    public function edit_schedule($id,$id_produk) {
        $detail = $this->schedule_production_model->detail_schedule($id,$id_produk);
        $detail_esf = $this->schedule_production_model->detail_esf($id,$id_produk);

		//print_r($detail_esf);die;

        $data = array(
              'id'=> $id,
              'id_produk'=> $id_produk,
              'detail'=> $detail,
              'detail_esf'=> $detail_esf
        );

        $this->load->view('edit_modal_schedule_view',$data);
    }    
	
	public function save_flow() {
		
		$params['ps'] = $_POST['ps'];
		$params['sequence'] = $_POST['sequence'];
		$params['instruction'] = $_POST['instruction'];
		$params['id'] = $_POST['id'];
		$params['schd'] = $_POST['schd'];
		
		//print_r($params);die;
		
		$detail = $this->schedule_production_model->insert_detail_flow($params);
		
		$this->output->set_content_type('application/json')->set_output(json_encode($detail));
		//print_r($detail);
		
	}	
	
	public function save_flow_all() {
		
		$params['po_quot'] = $_POST['po_quot'];
		$params['schd'] = $_POST['schd'];
		
		//print_r($params);die;
		
		$detail = $this->schedule_production_model->insert_detail_flow_all($params);
		
		//$this->output->set_content_type('application/json')->set_output(json_encode($detail));
		//print_r($detail);
		
	}
	
	public function edit_flow() {
		$id = $this->uri->segment(3);
		$schd = $this->uri->segment(4);
		//echo $id;die;
		
        $detail = $this->schedule_production_model->detail_flow($id,$schd);
        $proses_flow = $this->schedule_production_model->proses_flow();

		//print_r($detail);die;

        $data = array(
              'proses_flow' => $proses_flow,
			  'detail' => $detail,
			  'id' => $id,
			  'schd' => $schd
			  
        );

        $this->load->view('edit_modal_flow',$data);
    }
}