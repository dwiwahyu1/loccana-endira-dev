<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Material_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
    
	
	public function detail_prop_full($id)
	{
		$sql_all 	= 'CALL d_prop_search_id_prop(?)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				$id
			));

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
	public function detail_prop($params = array())
	{
		$sql_all 	= 'CALL prop_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				1000,
				0,
				NULL,
				NULL,
				''
			));

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
	public function detail_prop_2($params = array())
	{
		$sql_all 	= 'CALL prop_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				1000,
				0,
				NULL,
				NULL,
				''
			));

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
	public function type_material($params = array())
	{
		$sql_all 	= 'CALL type_mate_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				1000,
				0,
				NULL,
				NULL,
				''
			));

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}	
	
	public function jenis_material()
	{
		$sql 	= 'SELECT id_coa,id_parent,coa,keterangan FROM t_coa WHERE id_parent = 80300 order by coa';

		$query_all 	=  $this->db->query($sql);

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}	
	
	public function gudang() {
		$sql_all 	= 'CALL gudang_get_list()';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all);

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
	public function unit($params = array())
	{
		$sql_all 	= 'CALL uom_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				1000,
				0,
				NULL,
				NULL,
				''
			));

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
	public function valas($params = array())
	{
		$sql_all 	= 'CALL valas_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				1000,
				0,
				NULL,
				NULL,
				''
			));

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
    public function edit($id) {
    	$sql = 'CALL material_search_id(?)';

    	$query 	=  $this->db->query($sql, array(
    		$id
    	));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function lists($params = array()) {
		$sql 	= 'CALL material_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function add_material($data) {
		$sql 	= 'CALL material_add(?,?,?,?,?,?,?,?,?,?,?,?,?)';
		
		$query 	=  $this->db->query($sql, array(
			"",
			$data['kode_stok'],
			$data['nama_stok'],
			$data['desk_stok'],
			$data['unit'],
			$data['type_material'],
			$data['qty'],
			10,
			$data['detail'],
			$data['gudang'],
			1,
			0,
			0
		));
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		
		//$id = $this->db->insert_id();
		//print_r($lastid);die;
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		
		return $arr_result;
	}

	public function new_add_material($data) {
		$sql 	= 'CALL material_add(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
		
		$query 	=  $this->db->query($sql,
			array(
				$data['no_bc'],
				$data['kode_stok'],
				$data['nama_stok'],
				$data['desk_stok'],
				$data['unit'],
				$data['type_material'],
				$data['qty'],
				$data['weight'],
				$data['treshold'],
				$data['detail'],
				$data['gudang'],
				$data['status'],
				$data['base_price'],
				$data['base_qty'],
				$data['user_id'],
				$data['jenis_material']
		));
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		
		return $arr_result;
	}

	public function get_komponen($data) {
		$sql 	= 'CALL get_komponen(?)';

		$out = array();
		$query 	=  $this->db->query($sql,array(
			$data['detail']
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
		
	}

	public function get_komponen2($id,$idm) { 
		$sql 	= 'CALL get_komponen2(?, ?)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$id,$idm
			));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
		
	}	
	
	public function clear_detail($data) { 
		$sql 	= 'delete from m_detail_values where id_master_prop = ?';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$data['id_type_material']
			));

		//$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		//return $result;
		
	}

	public function edit_material($data) {
		$sql 	= 'CALL material_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
		
		$query 	=  $this->db->query($sql,
			array(
				// IN `pin_id` INT(10),
				// IN `pin_stock_code` VARCHAR(50),
				// IN `pin_stock_name` VARCHAR(255),
				// IN `pin_stock_description` VARCHAR(255),
				// IN `pin_unit` INT(11),
				// IN `pin_id_valas` INT(11),
				// IN `pin_price` DECIMAL(20,4),
				// IN `pin_id_gudang` INT(11),
				// IN `pin_type` INT(11),
				// IN `pin_qty` DECIMAL(20,10),
				// IN `pin_weight` DECIMAL(20,10),
				// IN `pin_treshold` INT(11),
				// IN `pin_id_properties` INT(11),
				// IN `pin_status` INT(11),
				// IN `pin_base_price` DECIMAL(20,4),
				// IN `pin_base_qty` DECIMAL(20,10),
				// IN `pin_cust_id` INT(11)
				$data['id_type_material'],
				$data['kode_stok'],
				$data['nama_stok'],
				$data['desk_stok'],
				$data['unit'],
				$data['valas'],
				$data['harga'],
				$data['gudang'],
				$data['type_material'],
				$data['qty'],
				$data['weight'],
				$data['treshold'],
				$data['id_properties'],
				$data['status'],
				$data['base_price'],
				$data['base_qty'],
				$data['user_id'],
				$data['jenis_material']
		));
		
		// $sql 	= 'CALL material_update(?,?,?)';

		// $query 	=  $this->db->query($sql,
		// 	array(
		// 		$data['id_type_material'],
		// 		$data['type_material_name'],
		// 		$data['type_material_description']
		// 	));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function add_dvalues($id_last,$val_komp,$id_det_komp) {
		$sql 	= 'CALL dvalues_add(?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$id_last,
				$id_det_komp,
				$val_komp
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}	
	
	public function add_price($data,$id_komp)
	{
		$sql 	= 'CALL price_add(?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['detail'],
				$data['harga'],
				$data['valas'],
				$id_komp, 
				$data['harga_satuan']
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}	
	
	public function add_mutasi($data,$id_komp)
	{
		$sql 	= 'CALL mutasi_add(?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$id_komp,
				$id_komp,
				$data['harga']
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function deletes($id) {
		$sql 	= 'CALL material_delete(?)';

		$query 	=  $this->db->query($sql, array(
			$id
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}


}
