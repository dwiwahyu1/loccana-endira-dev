<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Btb_Maintenance_Order extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('btb_maintenance_order/btb_maintenance_order_model');
		$this->load->library('log_activity');
		$this->load->library('sequence');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	  * This function is redirect to index btb page
	  * @return Void
	  */
	public function index() {
		$this->template->load('maintemplate', 'btb_maintenance_order/views/index');
	}

	/**
	  * This function is used for showing btb list
	  * @return Array
	  */
	function lists() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'DESC';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;
		
		$order_fields = array('a.no_btbmo', 'a.tanggal_btbmo', 'a.type_btbmo','f.no_main','f.nama_main');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->btb_maintenance_order_model->lists($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();
		
		$no = $start;
		foreach ($list['data'] as $k => $v) {
			$no++;
			$actions =
				'<div class="text-center">'.
					'<div class="btn-group">'.
						'<button class="btn btn-info btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Detail PO" onClick="btbmo_detail(\'' . $v['id_btbmo'] . '\')">'.
							'<i class="fa fa-search"></i>'.
						'</button>'.
					'</div>'.
				'</div>';
			
			array_push($data, 
				array(
					$no,
					$v['no_btbmo'],
					$v['name_eksternal'],
					$v['no_main'],
					$v['nama_main'],
					date('d-M-Y', strtotime($v['tanggal_btbmo'])),
					ucwords($v['type_btbmo']),
					$actions
				)
			);
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function get_mainOrder() {
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		$mainOrder 	= $this->btb_maintenance_order_model->get_mainOrder();
		$result 	= array();

		if(sizeof($params['listMainOrder']) > 0) {
			for ($j = 0; $j < sizeof($mainOrder); $j++) {
				for ($i = 0; $i < sizeof($params['listMainOrder']); $i++) {
					if(!empty($mainOrder[$j])) {
						if(trim($params['listMainOrder'][$i]) == trim($mainOrder[$j]['no_main_order'])) {
							unset($mainOrder[$j]);
							$j++;
						}
					}
				}
			}

			foreach ($mainOrder as $mk) {
				array_push($result, $mk);
			}
		}else $result = $mainOrder;

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result);
	}

	public function get_mainOrder_id($id_mainOrder) {
		$result = $this->btb_maintenance_order_model->get_mainOrder_id($id_mainOrder);

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result);
	}

	public function get_mainOrder_idDm() {
		$data 		= file_get_contents("php://input");
		$params		= json_decode($data, TRUE);
		$result 	= array();

		if(sizeof($params['arrNoDm']) > 0) {
			$arrNoDm = $params['arrNoDm'];
			for ($i = 0; $i < sizeof($arrNoDm); $i++) {
				$getDm = $this->btb_maintenance_order_model->get_mainOrder_idDm($arrNoDm[$i]);
				if($getDm[0]) {
					$dmk = $getDm[0];
					$amountRow = floatval($dmk['unit_price']) * floatval($dmk['qty']);

					array_push($result,  array(
						$dmk['no_main_order'],
						$dmk['no_main'],
						$dmk['nama_main'],
						$dmk['qty'],
						$dmk['uom_name'],
						$dmk['symbol_valas'].' '.number_format($amountRow, 2, ',', '.'),
						$dmk['remark'].
						'<input type="hidden" id="id_mo[]" name="id_mo[]" value="'.$dmk['id_mo'].'">'.
						'<input type="hidden" id="id_dm[]" name="id_dm[]" value="'.$dmk['id_dm'].'">'
					));
				}
			}
		}

		/*echo "<pre>";
		print_r($result);
		echo "</pre>";
		die;*/

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result);
	}
	
	public function add() {
		$this->load->view('add_modal_view');
	}

	public function add_item() {
		$mainOrder = $this->btb_maintenance_order_model->get_mainOrder();
		
		$data = array(
			'mainOrder' => $mainOrder
		);

		/*echo "<pre>";
		print_r($data);
		echo "</pre>";
		die;*/
		
		$this->load->view('add_item_modal_view', $data);
	}

	public function add_btb() {
		$this->form_validation->set_rules('type_btb', 'Type BTB', 'trim|required');
		$this->form_validation->set_rules('tanggal_btb', 'Tanggal BRB', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$type_btb		= $this->Anti_sql_injection($this->input->post('type_btb', TRUE));
			$tgl_btb		= $this->Anti_sql_injection($this->input->post('tanggal_btb', TRUE));
			$tempTgl_btb	= explode("-", $tgl_btb);
			
			$tempNoMo 		= $this->Anti_sql_injection($this->input->post('arrMO', TRUE));
			$arrMO			= explode(',', $tempNoMo);

			$tempNoDm 		= $this->Anti_sql_injection($this->input->post('arrDM', TRUE));
			$arrDM			= explode(',', $tempNoDm);

			if($type_btb == 'lokal') $no_btb = $this->sequence->get_max_no('btb_lokal');
			else if($type_btb == 'import') $no_btb = $this->sequence->get_max_no('btb_impor');
			else $no_btb = $this->sequence->get_max_no('btb_kb');

			$data = array(
				'no_btb'		=> $no_btb,
				'type_btb'		=> $type_btb,
				'tanggal_btb'	=> date('Y-m-d', strtotime($tempTgl_btb[2].'-'.$tempTgl_btb[1].'-'.$tempTgl_btb[0]))
			);

			$resultDetail = false;
			$resultsAddBtb = $this->btb_maintenance_order_model->btbmo_insert($data);
			if ($resultsAddBtb['result'] > 0) {
				for ($i = 0; $i < sizeof($arrMO); $i++) {
					$dataDetail = array(
						'id_btbmo'		=> $resultsAddBtb['lastid'],
						'id_mo'			=> $arrMO[$i],
						'id_dm'			=> $arrDM[$i]
					);

					$resultAddDetailBtb = $this->btb_maintenance_order_model->btbmo_insert_detail($dataDetail);
					if($resultAddDetailBtb > 0) $resultDetail = true;
					else {
						$resultDetail = false;
						$msg = 'Gagal menambahkan detail BTB yang ke-'.($i+1);
						$this->log_activity->insert_activity('insert', $msg);
						$results = array('success' => false, 'message' => $msg);
						break;
					}
				}

				if($resultDetail == true) {
					if($type_btb == 'lokal') $this->sequence->save_max_no('btb_lokal');
					else if($type_btb == 'import') $this->sequence->save_max_no('btb_impor');
					else $this->sequence->save_max_no('btb_kb');
			
					$msg = 'Berhasil menambahkan BTB ke database';
					$this->log_activity->insert_activity('insert', $msg.' dengan ID : '.$resultsAddBtb['result']);
					$results = array('success' => true, 'message' => $msg);
				}
			}else {
				$msg = 'Gagal menambahkan data BTB ke database';
				$results = array('success' => false, 'message' => $msg);
				
				$this->log_activity->insert_activity('insert', $msg. ' dengan No BTB ' .$no_btb);
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($results));
		}
	}

	public function btbmo_detail($id_btbmo) {
		$btbmo 		= $this->btb_maintenance_order_model->btbmo_search_id($id_btbmo);
		$d_btbmo 	= $this->btb_maintenance_order_model->btbmo_detail_search_id($id_btbmo);

		$data = array(
			'btbmo'		=> $btbmo,
			'd_btbmo'	=> $d_btbmo
		);

		/*echo "<pre>";
		echo $id_btbmo."<br/>";
		print_r($data);
		echo "</pre>";
		die;*/
		
		$this->load->view('detail_modal_view',$data);
	}
	
	public function btb_report() {
		$data = array(
			'msg' => '',
			'tanggal_awal' => '',
			'tanggal_akhir' => ''
		);
		
		$this->load->view('report_modal_view',$data);
	}
	
	public function btb_view_report()  {
		$jenis_report = $this->Anti_sql_injection($this->input->post('jenis_report', TRUE));
		$tanggalAwal = $this->Anti_sql_injection($this->input->post('tanggal_awal', TRUE));
		$tanggalAkhir = $this->Anti_sql_injection($this->input->post('tanggal_akhir', TRUE));
		
		$tglAwl = explode("/", $tanggalAwal);
		$tglAkhr = explode("/", $tanggalAkhir);
		$tanggal_awal = date('Y-m-d', strtotime($tglAwl[2].'-'.$tglAwl[1].'-'.$tglAwl[0]));
		$tanggal_akhir = date('Y-m-d', strtotime($tglAkhr[2].'-'.$tglAkhr[1].'-'.$tglAkhr[0]));

		$dataFilter = array(
			'jenis_report' => $jenis_report,
			'tanggal_awal' => $tanggal_awal,
			'tanggal_akhir' => $tanggal_akhir
		);
		
		if($jenis_report == 'lokal') {
			$result_lokal	= $this->btb_maintenance_order_model->btb_view_report($dataFilter);
			
			$data = array(
				'report'			=> 'Bukti Maintenance Order Lokal',
				'tanggal_awal'		=> date('d-F-Y', strtotime($tglAwl[2].'-'.$tglAwl[1].'-'.$tglAwl[0])),
				'tanggal_akhir'		=> date('d-F-Y', strtotime($tglAkhr[2].'-'.$tglAkhr[1].'-'.$tglAkhr[0])),
				'hasil'				=> $result_lokal
			);
			
			if(count($result_lokal) > 0) $data['msg'] = array('message'	=> 0, 'success'	=> true, 'status' => 'success');
			else $data['msg'] = array('message' => 1, 'success' => false, 'status' => 'error');

			$this->load->view('list_report_modal_view', $data);
		}else if($jenis_report == 'import') {
			$result_import = $this->btb_maintenance_order_model->btb_view_report($dataFilter);
			
			$data = array(
				'report'			=> 'Bukti Maintenance Order Import',
				'tanggal_awal'		=> date('d-F-Y', strtotime($tglAwl[2].'-'.$tglAwl[1].'-'.$tglAwl[0])),
				'tanggal_akhir'		=> date('d-F-Y', strtotime($tglAkhr[2].'-'.$tglAkhr[1].'-'.$tglAkhr[0])),
				'hasil'				=> $result_import
			);
			
			if(count($result_import) > 0) $data['msg'] = array('message' => 0, 'success' => true, 'status' => 'success');
			else $data['msg'] = array('message' => 1, 'success' => false, 'status' => 'error');

			$this->load->view('list_report_modal_view', $data);
		}else if($jenis_report == 'antar_kb') {
			$result_kb = $this->btb_maintenance_order_model->btb_view_report($dataFilter);
			
			$data = array(
				'report'			=> 'Bukti Maintenance Order Antar KB',
				'tanggal_awal'		=> date('d-F-Y', strtotime($tglAwl[2].'-'.$tglAwl[1].'-'.$tglAwl[0])),
				'tanggal_akhir'		=> date('d-F-Y', strtotime($tglAkhr[2].'-'.$tglAkhr[1].'-'.$tglAkhr[0])),
				'hasil'				=> $result_kb
			);
			
			if(count($result_kb) > 0) $data['msg'] = array('message' => 0, 'success' => true, 'status' => 'success');
			else $data['msg'] = array('message' => 1, 'success' => false, 'status' => 'error');

			$this->load->view('list_report_modal_view', $data);
		}else {
			$result = array('success' => false);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}
	
	public function btb_add_item() {
		$results = $this->btb_maintenance_order_model->btb_list_po();
		
		$data = array(
			'po' => $results,
		);
		
		$this->load->view('btb_add_item_modal_view',$data);
	}
	
	public function btb_search_po() {
		$data   = file_get_contents("php://input");
		$params = json_decode($data,true);
		
		$explode = explode(',',$params['po']);
		
		$var = array(
			'id_po' => $explode[1],
		);

		// print_r($var);die;
		
		$list = $this->btb_maintenance_order_model->btb_search_po($var);
		
		if(empty($list)){
			$data = array();
			$res = array(
				'success'	=> false,
				'message'	=> 'Data PO tidak ditemukan',
				'data'		=> $data
			);
		}else{
			$data = array();
			$tempObj = new stdClass();

			if(sizeof($params['tempPO']) > 0) {
				foreach ($params['tempPO'] as $p => $pk) {
					for ($i=0; $i <= sizeof($list); $i++) {
						if(!empty($list[$i])) {
							if($list[$i]['no_po'] == $pk[1] && $list[$i]['no_po'] == $pk[2]) {
								unset($list[$i]);
							}
						}
					}
				}
			}
			
			$i = 0;
			foreach ($list as $k => $v) {
				if($v['diskon'] != 0 || $v['diskon'] != '') { $diskon = $v['diskon']; }else{ $diskon = 0; }
				if($v['no_po'] != 0 || $v['no_po'] != '') { $po = $v['no_po']; }else{ $po = '-'; }
				
				$status = $v['status_po'];
				if($status == 0){
					$stat_btb	= 'Processed';
					$status = '		<label class="label label-success">';
					$status .=    		$stat_btb;
					$status .= '	</label>';
				}else{
					$stat_btb	= 'Approved';
					$status = '		<label class="label label-success">';
					$status .=    		$stat_btb;
					$status .= '	</label>';
				}
				
				$strQty =
				'<input type="number" class="form-control" min="0" id="qty_diterima'.$i.'" name="qty_diterima'.$i.'" onInput="valQty('.$i.')" style="height:25px; width: 100px;" value="'.$v['qty_btb'].'">';
				
				$strOption =
					'<div class="checkbox">'.
						'<input id="option['.$i.']" type="checkbox" value="'.$i.'">'.
						'<label for="option['.$i.']"></label>'.
					'</div>';
				$harga_unit = $v['unit_price'] * $v['qty'];

				array_push($data, array(
					$v['id_spb'],
					$v['id_po'],
					$v['id_material'],
					$v['no_po'],
					$v['stock_code'],
					$v['stock_name'],
					number_format($v['qty_btb'], 2,'.','.'),
					$strQty,
					$status,
					number_format($v['unit_price'], 2,'.','.'),
					$v['notes_po'],
					$strOption
				));
				$i++;
			}
			
			$res = array(
				'success'	=> true,
				'status'	=> 'success',
				'data'		=> $data
			);
		}
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	public function btbwo_insert_detail_po() {
		$data   = file_get_contents("php://input");
		$params = json_decode($data,true);
		
		if (is_array($params['listwo']) || is_object($params['listwo'])) {
			foreach($params['listwo'] as $k => $v) {
				$arrTemp = array(
					'id_btbwo'		=> $params['id_btbwo'],
					'id_spb'		=> $v[0],
					'id_po'			=> $v[1],
					'id_material'	=> $v[2],
					'qty_diterima'	=> $v[7],
					'unit_price'	=> $v[9],
					'total_amount'	=> $v[7]*$v[9]
				);
				
				$results = $this->btb_maintenance_order_model->btbwo_insert_detail_po($arrTemp);

				// $upPoSpb = $this->btb_maintenance_order_model->btb_update_qty($arrTemp);
				// $material = $this->btb_maintenance_order_model->btb_update_qty_material($arrTemp);
				// $mutasi = $this->btb_maintenance_order_model->btb_mutasi_add($arrTemp);
			}
		}
		if ($results > 0) {
			$msg = 'Berhasil menambahkan data BTB Work Order';
			$results = array('success' => true, 'message' => $msg);
			
			$this->log_activity->insert_activity('insert', $msg. ' dengan ID BTB ' .$params['id_btbwo']);
		}else {
			$msg = 'Gagal menambahkan data BTB Work Order ke database';
			$results = array('success' => false, 'message' => $msg);
			
			$this->log_activity->insert_activity('insert', $msg. ' dengan ID BTB ' .$params['id_btbwo']);
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($results);
	}
	
	/**
	  * This function is redirect to edit customer page
	  * @return Void
	  */
	public function approve($idpospb) {
		
		$results = $this->btb_maintenance_order_model->approve_detail($idpospb);
		$result_type = $this->btb_maintenance_order_model->type_bc('0');
		$result_type2 = $this->btb_maintenance_order_model->list_bc_all('0');
		
		$data = array(
			'approve' => $results,
			'jenis_bc' => $result_type,
			'list_bc_all' => $result_type2
		);
		
		$this->load->view('approve_btb_modal_view',$data);
	}
	
	/**
	  * This function is redirect to edit customer page
	  * @return Void
	  */
	public function approve_btb() {
		$data   = file_get_contents("php://input");
		$params     = json_decode($data,true);
		
		//echo $params['qty_diterima'];die;
	
		if(count($params) > 0) {
			$get_key = $this->btb_maintenance_order_model->get_data($params);
			
			$total_harga = ($get_key[0]['harga'] - ($get_key[0]['harga'] * $get_key[0]['diskon'] / 100)) * $params['qty_diterima'];
			$base_price = ($get_key[0]['harga'] - ($get_key[0]['harga'] * $get_key[0]['diskon'] / 100));

			//echo $total_harga;die;
			$new_qty = $get_key[0]['qty_diterima'] + $params['qty_diterima'];

			if($get_key[0]['qty'] > $new_qty ){
				$status_appr = 1;			
			}else{
				$status_appr = 2;
			}

			
			$this->btb_maintenance_order_model->insert_no_btb($params,$params['qty_diterima']);
			$approve = $this->btb_maintenance_order_model->approve_btb($params,$get_key[0]['id_material'],$new_qty,$base_price,$status_appr);
			
			if($approve['code'] <> '0') {
 
				$data_kartu_hp = array(
					'ref' 			=> NULL, 
					'source' 		=> NUll,
					'keterangan' 	=> 'Pembelian PO '.$get_key[0]['no_po'], 
					'status' 		=> 0,
					'saldo' 		=> $total_harga,
					'saldo_akhir' 	=> $total_harga, 
					'id_valas'		=> $get_key[0]['valas_id'], 
					'type_kartu'	=> 0,
					'id_master'		=> $get_key[0]['id_po'],
					'type_master'	=> 0,
					'delivery_date'	=> $get_key[0]['delivery_date'],
					'payment_coa'	=> $get_key[0]['payment_coa']
				);
				
				if($get_key[0]['term_of_payment'] == 'CASH'){
						
					$this->btb_maintenance_order_model->add_coa_values_cash($data_kartu_hp);
						
				}else{
					$this->btb_maintenance_order_model->add_kartu_hp($data_kartu_hp);
					
					$get_data_id = $this->btb_maintenance_order_model->get_id_coa($get_key);
					
					$data_coa = array (
						'id_coa'	=> $get_data_id[0]['id_coa'],
						'id_parent'	=> 0,
						'id_valas'	=> $get_key[0]['valas_id'],
						'value'		=> $total_harga,
						'adjusment'	=> 0,
						'type_cash'	=> 0,
						'note'		=> $data_kartu_hp['keterangan'],
						'rate'		=> $get_key[0]['rate']
					);
					
					$add_coa = $this->btb_maintenance_order_model->add_coa_values($data_coa);
					
					$insert_val_d = $this->btb_maintenance_order_model->add_po_spb_coa($add_coa['lastid'],$get_key[0]['id_po']);
						
				}
				
				$this->btb_maintenance_order_model->mutasi_add($get_key[0],$params['qty_diterima']);
				
			} else {
				
				$msg = 'Barang Gagal di approve karena, Data belum lengkap silahkan cek kembali.';
			
				$result = array(
					'success' => false,
					'message' => $msg
				);
			
			}
			
			$msg = 'Barang berhasil di approve karena, '.$params['keterangan'];

			$result = array(
				'success' => true,
				'message' => $msg
			);
			
		}else{
			
			$msg = 'Barang Gagal di approve karena, Data belum lengkap silahkan cek kembali.';
			
			$result = array(
				'success' => false,
				'message' => $msg
			);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
}