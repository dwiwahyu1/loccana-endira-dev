<style type="text/css">
	.row-search{
		padding: 5px;
	}
	
	.sk-fading-circle {
		margin: 100px auto;
		width: 40px;
		height: 40px;
		position: relative;
	}

	.sk-fading-circle .sk-circle {
		width: 100%;
		height: 100%;
		position: absolute;
		left: 0;
		top: 0;
	}

	.sk-fading-circle .sk-circle:before {
		content: '';
		display: block;
		margin: 0 auto;
		width: 15%;
		height: 15%;
		background-color: #333;
		border-radius: 100%;
		-webkit-animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;
		animation: sk-circleFadeDelay 1.2s infinite ease-in-out both;
	}

	.sk-fading-circle .sk-circle2 {
		-webkit-transform: rotate(30deg);
		-ms-transform: rotate(30deg);
		transform: rotate(30deg);
	}

	.sk-fading-circle .sk-circle3 {
		-webkit-transform: rotate(60deg);
		-ms-transform: rotate(60deg);
		transform: rotate(60deg);
	}

	.sk-fading-circle .sk-circle4 {
		-webkit-transform: rotate(90deg);
		-ms-transform: rotate(90deg);
		transform: rotate(90deg);
	}

	.sk-fading-circle .sk-circle5 {
		-webkit-transform: rotate(120deg);
		-ms-transform: rotate(120deg);
		transform: rotate(120deg);
	}

	.sk-fading-circle .sk-circle6 {
		-webkit-transform: rotate(150deg);
		-ms-transform: rotate(150deg);
		transform: rotate(150deg);
	}

	.sk-fading-circle .sk-circle7 {
		-webkit-transform: rotate(180deg);
		-ms-transform: rotate(180deg);
		transform: rotate(180deg);
	}

	.sk-fading-circle .sk-circle8 {
		-webkit-transform: rotate(210deg);
		-ms-transform: rotate(210deg);
		transform: rotate(210deg);
	}

	.sk-fading-circle .sk-circle9 {
		-webkit-transform: rotate(240deg);
		-ms-transform: rotate(240deg);
		transform: rotate(240deg);
	}

	.sk-fading-circle .sk-circle10 {
		-webkit-transform: rotate(270deg);
		-ms-transform: rotate(270deg);
		transform: rotate(270deg);
	}

	.sk-fading-circle .sk-circle11 {
		-webkit-transform: rotate(300deg);
		-ms-transform: rotate(300deg);
		transform: rotate(300deg);
	}

	.sk-fading-circle .sk-circle12 {
		-webkit-transform: rotate(330deg);
		-ms-transform: rotate(330deg);
		transform: rotate(330deg);
	}

	.sk-fading-circle .sk-circle2:before {
		-webkit-animation-delay: -1.1s;
		animation-delay: -1.1s;
	}

	.sk-fading-circle .sk-circle3:before {
		-webkit-animation-delay: -1s;
		animation-delay: -1s;
	}

	.sk-fading-circle .sk-circle4:before {
		-webkit-animation-delay: -0.9s;
		animation-delay: -0.9s;
	}

	.sk-fading-circle .sk-circle5:before {
		-webkit-animation-delay: -0.8s;
		animation-delay: -0.8s;
	}

	.sk-fading-circle .sk-circle6:before {
		-webkit-animation-delay: -0.7s;
		animation-delay: -0.7s; 
	}

	.sk-fading-circle .sk-circle7:before {
		-webkit-animation-delay: -0.6s;
		animation-delay: -0.6s;
	}

	.sk-fading-circle .sk-circle8:before {
		-webkit-animation-delay: -0.5s;
		animation-delay: -0.5s;
	}

	.sk-fading-circle .sk-circle9:before {
		-webkit-animation-delay: -0.4s;
		animation-delay: -0.4s;
	}

	.sk-fading-circle .sk-circle10:before {
		-webkit-animation-delay: -0.3s;
		animation-delay: -0.3s;
	}

	.sk-fading-circle .sk-circle11:before {
		-webkit-animation-delay: -0.2s;
		animation-delay: -0.2s;
	}

	.sk-fading-circle .sk-circle12:before {
		-webkit-animation-delay: -0.1s;
		animation-delay: -0.1s;
	}

	@-webkit-keyframes sk-circleFadeDelay {
		0%, 39%, 100% { opacity: 0; }
		40% { opacity: 1; }
	}

	@keyframes sk-circleFadeDelay {
		0%, 39%, 100% { opacity: 0; }
		40% { opacity: 1; } 
	}

	.x-hidden {
		display: none;
	}
</style>

<div class="form-report">
	<div class="row row-search">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jenis_report">Jenis Report</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="jenis_report" name="jenis_report" required>
				<option value="">-- Select Jenis --</option>
				<option value="lokal">Lokal</option>
				<option value="import">Import</option>
				<option value="antar_kb">Antar KB</option>
			</select>
		</div>
	</div>

	<div class="row row-search">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="date-range">Tanggal</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<div class="input-daterange input-group" id="date-range">
				<input type="text" class="form-control" id="tanggal_awal" name="tanggal_awal" autocomplete="off" required>
				<span class="input-group-addon bg-primary b-0 text-white">to</span>
				<input type="text" class="form-control" id="tanggal_akhir" name="tanggal_akhir" autocomplete="off">
			</div>
		</div>
	</div>
	<hr>
	<div class="row row-search">
		<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-search" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Cari</button>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		jQuery('#date-range').datepicker({
			format: "dd/M/yyyy",
			toggleActive: true
		}).focus(function() {
			$(this).prop("autocomplete", "off");
		});
	
		$('#btn-search').on('click', function () {
			$('#btn-search').attr('disabled','disabled');
			$('#btn-search').text("Loading...");
			$('#panel-modal-report').removeData('bs.modal');
		
			if($('#tanggal_awal').val() != '' && $('#tanggal_akhir').val() != '' && $('#jenis_report').val() != '') {
				var data = {
					'tanggal_awal'		: $('#tanggal_awal').val(),
					'tanggal_akhir'		: $('#tanggal_akhir').val(),
					'jenis_report'		: $('#jenis_report').val()
				};

				$('#panel-modal-viewReport').load('<?php echo base_url('btb_maintenance_order/btb_view_report/');?>', data, function(response, status, xhr) {
					if (status != "error") {
						$('#btn-search').removeAttr('disabled');
						$('#btn-search').text("Cari");
						$('#panel-modal-report').modal('toggle');
						$('#panel-modal-viewReport').modal('toggle');
						btb_view_report();
					}else {
						$('#btn-search').removeAttr('disabled');
						$('#btn-search').text("Cari");
						$('#panel-modal-view-report').modal('toggle');
						$('#panel-modal-view-report').removeData('');
						$('#panel-modal-view-report').html('');
						swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
					}
				});
			}else {
				$('#btn-search').removeAttr('disabled');
				$('#btn-search').text("Cari");
				swal("Failed!", "Invalid inputan, silahkan cek kembali.", "error");
			}
		});
	});
</script>