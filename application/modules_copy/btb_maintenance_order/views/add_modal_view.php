<style>
	.dt-body-left {text-align: left;}
	.dt-body-center {text-align: center;}
	.dt-body-right {text-align: right;}

	/*#listItemWO {
		counter-reset: rowNumber;
	}

	#listItemWO tbody tr > td:first-child {
		counter-increment: rowNumber;
		text-align: center;
	}

	#listItemWO tbody tr td:first-child::before {
		content: counter(rowNumber);
		min-width: 1em;
		margin-right: 0.5em;
	}*/
</style>

<form class="form-horizontal form-label-left" id="add_btb" role="form" action="<?php echo base_url('btb_maintenance_order/add_btb');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_btb">Type BTB <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="type_btb" id="type_btb" style="width: 100%" required>
				<option value="" selected>Select Type BTB</option>
				<option value="lokal">Lokal</option>
				<option value="import">Import</option>
				<option value="antar_kb">Antar KB</option>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_btb">Tanggal BTB <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<div class="input-group date">
				<input type="text" class="form-control" id="tanggal_btb" name="tanggal_btb" placeholder="<?php
					echo date('d-M-Y'); ?>" value="<?php echo date('d-M-Y'); ?>" autocomplete="off" required>
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Maintenance Order : </label>
		<div class="col-md-8 col-sm-6 col-xs-12 add_item">
			<a class="btn btn-primary" id="btn_add_mo">
				<i class="fa fa-plus"></i> Tambah Maintenance Order
			</a>
		</div>
	</div>
	
	<div class="item form-group">
		<div>
			<table id="listItemMO" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<!-- <th style="width: 5%;"></th> -->
						<th>MO</th>
						<th>MP</th>
						<th>Nama Maintenance</th>
						<th class="dt-body-center" style="width: 5%;">Qty</th>
						<th class="dt-body-center" style="width: 5%;">UOM</th>
						<th class="dt-body-center">Amount</th>
						<th>Remark</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Tambah BTB</button>
		</div>
	</div>
</form>
	
<script type="text/javascript">
	var arrMO	= [];
	var arrDM	= [];
	var dt_itemMO;
	
	$(document).ready(function() {
		$('#tanggal_btb').datepicker({
			format: "dd-M-yyyy",
			autoclose: true,
			todayHighlight: true,
			changeYear: true,
			minDate: '-3M',
			maxDate: '+30D',
		});
		
		$("#type_btb").select2();
		dtPO();
	});
	
	function dtPO() {
		dt_itemMO = $('#listItemMO').DataTable( {
			"processing": true,
			"searching": false,
			"paging": false,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [3,4],
				"className": 'dt-body-center'
			}, {
				"targets": [5],
				"className": 'dt-body-right'
			}]
		});
	}
	
	$('#btn_add_mo').on('click', function() {
		$('#panel-modalchild').removeData('bs.modal');
		$('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modalchild  .panel-body').load('<?php echo base_url('btb_maintenance_order/add_item');?>');
		$('#panel-modalchild  .panel-title').html('<i class="fa fa-plus"></i> Tambah Maintenance Order');
		$('#panel-modalchild').modal({backdrop:'static',keyboard:false},'show');
	})
	
	$('#add_btb').on('submit',(function(e) {
		arrMO	= [];
		arrDM	= [];

		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();

		$('input[name="id_mo[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arrMO.push(this.value);
			}
		})

		$('input[name="id_dm[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arrDM.push(this.value);
			}
		})

		if(arrMO.length > 0 && arrDM.length > 0) {
			var formData = new FormData();
			formData.set('type_btb',	$('#type_btb').val());
			formData.set('tanggal_btb',	$('#tanggal_btb').val());
			formData.set('arrMO',		arrMO);
			formData.set('arrDM',		arrDM);

			$.ajax({
				type:'POST',
				url: $(this).attr('action'),
				data: formData,
				cache:false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function () {
							$('#panel-modal').modal('toggle');
							listbtb();
						})
					}else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Tambah BTB");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Tambah BTB");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}else {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah BTB");
			swal("Failed!", 'List Maintenance Order belum ada!', "error");
		}
	}));
</script>