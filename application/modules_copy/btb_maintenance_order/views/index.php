<style>
	.changed_status{cursor:pointer;text-decoration: underline;color:#96b6e8;}
	.changed_status:hover{color:#ff8c00}
	.dt-body-left {text-align:left;}
	.dt-body-right {text-align:right;}
	.dt-body-center {text-align:center; vertical-align: middle;}
	.force-overflow {height: 450px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow {min-height: 350px;}
	#panel-modalchild::-webkit-scrollbar-track {
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	#panel-modal-modalchild::-webkit-scrollbar {
		width: 10px;
		background-color: #F5F5F5;
	}

	#panel-modal-modalchild::-webkit-scrollbar-thumb {
		background-image: -webkit-gradient(linear,
		left bottom,
		left top,
		color-stop(0.44, rgb(122,153,217)),
		color-stop(0.72, rgb(73,125,189)),
		color-stop(0.86, rgb(28,58,148)));
	}
</style>

<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">BTB Maintenance Order</h4>
		</div>
	</div>
	
	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">

				<table id="listbtb" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th class="custom-tables text-center" style="width: 5%;">No</th>
							<th class="custom-tables text-center">No BTB</th>
							<th class="custom-tables text-center">Nama Supplier</th>
							<th class="custom-tables text-center">No Maintenance</th>
							<th class="custom-tables text-center">Nama Maintenance</th>
							<th class="custom-tables text-center" style="width: 15%;">Tanggal BTB</th>
							<th class="custom-tables text-center" style="width: 15%;">Type BTB</th>
							<th class="custom-tables text-center" style="width: 5%;">Actions</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div><!-- end col -->
	</div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:90%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-modal">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width: 70%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal-report" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width: 700px;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-report-overflow">
					<div class="scroll-report-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal-viewReport" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width: 700px;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-report-overflow">
					<div class="scroll-report-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$(document).ready(function(){
		listbtb();
	});

	function btb_add() {
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('btb_maintenance_order/add/');?>');
		$('#panel-modal  .panel-title').html('<i class="fa fa-plus"></i> Add BTB');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function btb_report() {
		$('#panel-modal-report').removeData('bs.modal');
		$('#panel-modal-report  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal-report  .panel-body').load('<?php echo base_url('btb_maintenance_order/btb_report/');?>');
		$('#panel-modal-report  .panel-title').html('<i class="fa fa-search"></i> Filter Report BTB');
		$('#panel-modal-report').modal({backdrop:'static',keyboard:false},'show');
	}

	function btb_view_report() {
		$('#panel-modal-view-report').removeData('bs.modal');
		$('#panel-modal-view-report  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal-view-report  .panel-body').load('<?php echo base_url('btb_maintenance_order/btb_view_report/');?>');
		$('#panel-modal-view-report  .panel-title').html('<i class="fa fa-file-pdf-o"></i> View Report BTB');
		$('#panel-modal-view-report').modal({backdrop:'static',keyboard:false},'show');
	}

	function btbmo_detail(id_btbmo) {
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('btb_maintenance_order/btbmo_detail');?>'+"/"+id_btbmo);
		$('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Detail BTB Maintenance');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function listbtb(){
		$("#listbtb").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'btb_maintenance_order/lists/';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false,
			"destroy": true,
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip',
			"initComplete": function(){
				var element =
					'<div class="btn-group pull-left">'+
						'<a class="btn btn-primary" onClick="btb_add()">'+
							'<i class="fa fa-plus"></i> Add BTB'+
						'</a>'+
					'</div>'+
					'<div class="btn-group pull-left">'+
						'<a class="btn btn-success" onClick="btb_report()">'+
							'<i class="fa fa-file-pdf-o"></i> Cetak Laporan BTB'+
						'</a>'+
					'</div>';
				$("div.toolbar").prepend(element);
			},
			"columnDefs": [{
				"targets": [0, 7],
				"className": 'dt-body-center',
				"width": "30px"
			}, {
				"targets": [2],
				"className": 'dt-body-center',
				"width": "150px"
			}, {
				"targets": [1, 3, 4],
				"className": 'dt-body-center',
				"width": "120px"
			}, {
				"targets": [0,7],
				"className": 'dt-body-center',
				"width": "30px"
			}]
		});
	}
</script>