<style type="text/css">
	.form-item{margin-top: 15px;overflow: auto;}
	#listMo {
		counter-reset: rowNumber;
	}

	#listMo tbody tr > td:first-child {
		counter-increment: rowNumber;
		text-align: center;
	}

	#listMo tbody tr td:first-child::before {
		content: counter(rowNumber);
		min-width: 1em;
		margin-right: 0.5em;
	}
</style>

<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
<br>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_mo">Nomor Maintenance Order<span class="required"><sup>*</sup></span></label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<select class="form-control" name="no_mo" id="no_mo" style="width: 100%" required>
			<option value="" selected>-- Pilih Nomor Maintenance Order --</option>
		<!-- <?php foreach($mainOrder as $mod => $mov) { ?>
			<option value="<?php echo $mov['id_mo']; ?>"><?php echo $mov['no_main_order']; ?></option>
		<?php } ?> -->
		</select>
	</div>
</div>

<div class="item form-group form-item">
	<table id="listMo" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class="dt-body-center" style="width: 5%;">No</th>
				<th>No Maintenance</th>
				<th>Nama Maintenance</th>
				<th class="dt-body-center">Qty</th>
				<th class="dt-body-center">UOM</th>
				<th>Harga</th>
				<th>Deskripsi</th>
				<th style="width: 5%;"></th>
			</tr>
		</thead>
		<tbody></tbody>
	</table>
</div>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<a class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" id="btn_simpan_item">Simpan</a>
	</div>
</div>
	
<script type="text/javascript">
	$(document).ready(function() {
		$("#no_mo").select2();

		getListMO();
	});

	$('#no_mo').on('change', function() {
		if(this.value != '') listDtMo(this.value);
		else listDtMo(0);
	})

	function getListMO() {
		$('#no_mo').attr('disabled', 'disabled')
		var dtLength = dt_itemMO.rows().data().length;
		var arrTemp = [];
		if(dtLength > 0) {
			for (var i = 0; i < dtLength; i++) {
				var dtData = dt_itemMO.row(i).data();
				arrTemp.push(dtData[0]);
			}
		}

		var datapost = {
			'listMainOrder' : arrTemp
		};
			
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('btb_maintenance_order/get_mainOrder');?>",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				console.log(r);
				if(r.length > 0) {
					for (var i = 0; i < r.length; i++) {
						var newOption = new Option(r[i].no_main_order, r[i].id_mo, false, false);
						$('#no_mo').append(newOption);
					}
					$('#no_mo').removeAttr('disabled');
				}else $('#no_mo').removeAttr('disabled');
			}
		});
	}
	
	function listDtMo(id_mo) {
		$.ajax({
			type: "GET",
			url: "<?php echo base_url('btb_maintenance_order/get_mainOrder_id');?>"+'/'+id_mo,
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					$('#listWo tbody').empty();
					var strAppend = '';
					for (var i = 0; i < r.length; i++) {
						strAppend +=
							'<tr>'+
								'<td class="dt-body-center"></td>'+
								'<td>'+r[i].no_main+'</td>'+
								'<td>'+r[i].nama_main+'</td>'+
								'<td class="dt-body-center">'+r[i].qty+'</td>'+
								'<td class="dt-body-center">'+r[i].uom_name+'</td>'+
								'<td click="dt-body-right">'+r[i].unit_price+'</td>'+
								'<td>'+
									r[i].remark+
									'<input type="hidden" id="checkNoDm'+i+'" name="checkNoDm[]" value="'+r[i].id_dm+'">'+
								'</td>'+
								'<td>'+
									'<div class="checkbox">'+
										'<input id="checkDm'+i+'" name="checkDm[]" type="checkbox" value="'+r[i].id_dm+'">'+
										'<label for="checkDm'+i+'"></label>'+
									'</div>'+
								'</td>'+
							'</tr>';
					}
					$('#listMo tbody').html(strAppend);
				}else {
					$('#listMo tbody').empty();
				}
			}
		});
	}

	$('#btn_simpan_item').on('click', function() {
		$(this).attr('disabled','disabled');
		$(this).text("Memasukkan data...");
		tempArr = [];

		var ids = 0;
		$('input[name="checkNoDm[]"]').each(function() {
			
			// if ($('#checkDm'+ids)[0].checked){
				
				// //alert('sdsds');
				
				// tempArr.push(this.value);
			// }
			
			if(this.value) {
				
				if(this.value != undefined && this.value != ''){
					tempArr.push(this.value);
					console.log(this);
				}
			}
			
			ids = ids +1;
		})

		if(tempArr.length > 0) {
			var datapost = {
				"arrNoDm" : tempArr
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url('btb_maintenance_order/get_mainOrder_idDm');?>",
				data:JSON.stringify(datapost),
				cache:false,
				contentType: false,
				processData: false,
				contentType: 'application/json; charset=utf-8',
				success: function(r) {
					if(r.length > 0) {
						dt_itemMO.rows.add(r).draw();
						$(this).removeAttr('disabled');
						$(this).text("Simpan");
						$('#panel-modalchild').modal('toggle');
					}
				}
			});
		}else {
			$(this).removeAttr('disabled');
			$(this).text("Simpan");
			swal("Failed!", 'List Maintenance belum ada yang dipilih!', "error");
		}
	})
</script>