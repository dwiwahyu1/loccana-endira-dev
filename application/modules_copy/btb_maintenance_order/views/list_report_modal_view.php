<style>
	.changed_status{cursor:pointer;text-decoration: underline;color:#96b6e8;}
	.changed_status:hover{color:#ff8c00}
	.custom-tables, th{text-align:center;vertical-align:middle;}
	.custom-tables.align-text, th{vertical-align:middle;}

	.dt-body-right{text-align: right;}
	.dt-body-center{text-align: center;}
	.dt-body-left{text-align: left;}

	#titleTanggal{margin-bottom:10px;}
	.center_numb{text-align:center;}
	.left_numb{text-align:left;}
	.right_numb{text-align:right;font-weight:bold;}
	.tableReport {
		box-sizing: border-box;
		text-align: left;
		width: 90%;
		margin: 10px auto;
		padding-bottom:5px;
		page-break-before: always;
		z-index:99;
	}
	.table-scroll {
		position:relative;
		max-width:100%;
		margin:auto;
		overflow:hidden;
		border:1px solid #f0fffd;
	}
	.table-wrap {
		width:100%;
		overflow:auto;
	}
	.table-scroll table {
		width:100%;
		margin:auto;
		border-spacing:0;
	}
	.table-scroll th, .table-scroll td {
		padding:5px 10px;
		border:1px solid #000;
		background:#fff;
		white-space:nowrap;
		vertical-align:top;
		n-right: 0.5em;
	}
</style>

<div class="card-box" id="view_report_btb">
	<div class="row">
		<div class="col-md-12">
			<div class="pull-left">
				<a class="btn btn-icon waves-effect waves-light btn-danger m-b-5" data-toggle="tooltip" data-placement="top" data-dismiss="modal" title="Keluar" id="keluar">
					<i class="fa fa-times"></i>
				</a>
			</div>
			<div class="pull-right">
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download PDF" id="btn_download">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>

	<div class="col-md-12 text-center titleReport">
		<h2 id="titlePerusahaan">LAPORAN <?php echo strtoupper($report); ?></h2>
		<h2 id="namaPerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
		<h3 id="titleTanggal">PERIODE <?php echo $tanggal_awal.' S.D '.$tanggal_akhir; ?></h3>
	</div>
	<hr>
	<div id="table-scroll" class="table-scroll">
		<div class="table-wrap">
			<table id="listbtbreport" class="table table-striped table-bordered table-responsive dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th class="custom-tables text-center">No</th>
							<th class="custom-tables text-center">No BTB</th>
							<th class="custom-tables text-center">Tanggal BTB</th>
							<th class="custom-tables text-center">Type BTB</th>
							<th class="custom-tables text-center">Kode Maintenance</th>
							<th class="custom-tables text-center">Nama Maintenance</th>
							<th class="custom-tables text-center">Qty</th>
							<th class="custom-tables text-center">Qty Diterima</th>
							<th class="custom-tables text-center">Keterangan</th>
					</tr>
				</thead>
				<tbody>
					<?php $no = 1;
					foreach($hasil as $h) { ?>
						<tr>
							<td class="dt-body-center"><?php echo $no; ?></td>
							<td class="left_numb"><?php echo $h['no_btbmo']; ?></td>
							<td class="left_numb"><?php echo date('d-M-Y', strtotime($h['tanggal_btbmo'])); ?></td>
							<td class="left_numb"><?php echo ucwords($h['type_btbmo']); ?></td>
							<td class="left_numb"><?php echo $h['no_main']; ?></td>
							<td class="left_numb"><?php echo $h['nama_main']; ?></td>
							<td class="dt-body-center"><?php echo number_format($h['qty'], 4, ',', '.'); ?></td>
							<td class="dt-body-right"><?php
								$amount = floatval($h['unit_price']) * floatval($h['qty']);
								echo $h['symbol_valas']." ".number_format($amount, 4, ',', '.');
							?></td>
							<td class="left_numb"><?php echo $h['deskripsi']; ?></td>
						</tr>
					<?php $no++; } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script>

$(document).ready(function() {
	var pesan = '<?php echo $msg['message']; ?>';
	var tgl_awal = '<?php echo $tanggal_awal; ?>';
	var tgl_akhir = '<?php echo $tanggal_akhir; ?>';
	
	$('#btn_download').click(function () {
		var doc = new jsPDF("l", "mm", "a4");
		var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
		var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
		
		// FOOTER
		doc.setTextColor(50);
		doc.setFontSize(12); 
		doc.setFontStyle('helvetica','arial','sans-serif','bold');
		doc.text($('#titlePerusahaan').html(), pageWidth / 2, 20, 'center');
		doc.text($('#namaPerusahaan').html(), pageWidth / 2, 25, 'center');
		doc.text($('#titleTanggal').html(), pageWidth / 2, 30, 'center');
		
		doc.autoTable({
			html 			: '#listbtbreport',
			headStyles		: {
				fontSize 	: 7,
				valign		: 'middle', 
				halign		: 'center',
			},
			bodyStyles		: {
				fontSize 	: 7,
			},
			theme			: 'plain',
			styles			: {
				fontSize : 6, 
				lineColor: [0, 0, 0],
				lineWidth: 0.15,
				cellWidth : 'auto',
				
			},
			margin 			: 4,
			rowPageBreak	: 'auto',
			showHead 		: 'firstPage',
			showFoot		: 'lastPage',
			startY			: 45
		});
		doc.save('Report <?php echo $report; ?>.pdf');
	});
});
</script>