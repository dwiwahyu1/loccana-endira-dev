<style>
	.dt-body-left {text-align: left;}
	.dt-body-center {text-align: center;}
	.dt-body-right {text-align: right;}

	#listItemMO {
		counter-reset: rowNumber;
	}

	#listItemMO tbody tr > td:first-child {
		counter-increment: rowNumber;
		text-align: center;
	}

	#listItemMO tbody tr td:first-child::before {
		content: counter(rowNumber);
		min-width: 1em;
		margin-right: 0.5em;
	}
</style>
<form class="form-horizontal form-label-left" id="detail_btb" role="form" action="<?php echo base_url('btb_maintenance_order/detail_btb');?>" method="post" enctype="multipart/form-data" data-parsley-validate>


	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_btb">Type BTB <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text-align" class="form-control" id="type_btb" name="type_btb" value="<?php
				if(isset($btbmo[0]['type_btbmo'])) echo ucwords($btbmo[0]['type_btbmo']);
			?>" disabled>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_btb">Tanggal BTB <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="tanggal_btb" name="tanggal_btb" value="<?php
				if(isset($btbmo[0]['tanggal_btbmo'])) echo date('d-M-Y', strtotime($btbmo[0]['tanggal_btbmo']));
			?>" disabled>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Maintenance Order : </label>
		<div class="col-md-8 col-sm-6 col-xs-12"></div>
	</div>
	
	<div class="item form-group">
		<div>
			<table id="listItemMO" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th class="dt-body-center" style="width: 5%;">No</th>
						<th style="width: 10%;">MO</th>
						<th style="width: 10%;">MR</th>
						<th>Nama Maintenance</th>
						<th class="dt-body-center" style="width: 7%;">Qty</th>
						<th class="dt-body-center" style="width: 7%;">UOM</th>
						<th>Harga</th>
						<th>Amount</th>
						<th>Remark</th>
					</tr>
				</thead>
				<tbody>
		<?php
			if(isset($d_btbmo)) {
				if(sizeof($d_btbmo) > 0) {
					foreach ($d_btbmo as $dk => $dv) {
						$amount = floatval($dv['unit_price']) * floatval($dv['qty']);
						?>
						<tr>
							<td></td>
							<td><?php echo $dv['no_main_order']; ?></td>
							<td><?php echo $dv['no_main']; ?></td>
							<td><?php echo $dv['nama_main']; ?></td>
							<td class="dt-body-center"><?php echo $dv['qty']; ?></td>
							<td class="dt-body-center"><?php echo $dv['uom_name']; ?></td>
							<td class="dt-body-right"><?php echo $dv['symbol_valas'].' '.number_format($dv['unit_price'], 4, ',', '.'); ?></td>
							<td class="dt-body-right"><?php echo $dv['symbol_valas'].' '.number_format($amount, 4, ',', '.'); ?></td>
							<td><?php echo $dv['deskripsi']; ?></td>
						</tr>
		<?php
					}
				}
			}
		?>
				</tbody>
			</table>
		</div>
	</div>
</form>
	
<script type="text/javascript">
	var listItemMO;

	$(document).ready(function() {
		dtMO();
	});

	function dtMO() {
		listItemMO = $('#listItemMO').DataTable( {
			"processing": true,
			"searching": false,
			"paging": false,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0,4,5],
				"className": 'dt-body-center'
			}, {
				"targets": [6,7],
				"className": 'dt-body-right'
			}]
		});
	}
</script>