<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Stok_Opname_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function list_periode($params) {
		$sql 	= 'CALL stockopname_list2(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function count_opname($periode) {
		$sql 	= 'CALL stockopname_list(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql, array( NULL, NULL, NULL, NULL, NULL, $periode));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		return $total['@total'];
	}

	public function list_opname($params) {
		$sql 	= 'CALL stockopname_list(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter'],
				$params['filterPeriode']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function get_material() {
		$sql 	= "SELECT id, stock_name FROM m_material";

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_stok_opname_master($data) {
		$sql 	= 'CALL stockopname_master_add(?,?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['periode'],
			$data['keterangan'],
			$data['pic']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function add_stok_opname($data) {
		$sql 	= 'CALL stockopname_insert(?)';

		$query 	=  $this->db->query($sql, array(
				$data['periode']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_perubahan($id_opname) {
		$sql 	= 'SELECT a.*, b.no_bc, b.stock_code, b.stock_name FROM t_stock_opname a JOIN m_material b ON b.id = a.id_material WHERE a.id_stock_opname = ?';
		$query 	=  $this->db->query($sql,
			array($id_opname)
		);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function mutasi_add($data) {
		$sql 	= 'CALL mutasi_add(?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id_stock_awal'],
				$data['id_stock_akhir'],
				$data['tanggal_perubahan'],
				$data['amount_mutasi'],
				$data['type_mutasi']
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function perubahan_update($data) {
		$sql 	= 'CALL stockopname_update(?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id_opname'],
				$data['perubahan'],
				$data['keterangan_perubahan'],
				$data['tanggal_perubahan'],
				$data['qty_awal'],
				$data['qty_akhir'],
				$data['pic']
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function stockopname_delete($data) {
		$sql 	= 'CALL stockopname_delete(?,?)';
		$query 	=  $this->db->query($sql,array(
			$data['id'],
			$data['periode']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}