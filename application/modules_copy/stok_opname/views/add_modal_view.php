<!--Parsley-->
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/gentelella-master/vendors/parsleyjs/dist/parsley.min.js"></script>
<style>
	table {font-size: 10px;}

	.add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
	.add_item:hover{color:#ff8c00}

	#periode_loading-us{display:none}
	#periode_tick{display:none}
</style>
	
<form class="form-horizontal form-label-left" id="add_form" role="form" action="<?php echo base_url('stok_opname/save_periode');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="periode">Periode <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="periode" name="periode" placeholder="Periode Perubahan" autocomplete="off" required>
			<span id="periode_loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking Periode...</span>
			<span id="periode_tick"></span>
			<input type="hidden" id="existPeriode" name="existPeriode" value="1">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="keterangan">Keterangan <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<textarea class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan" required="required"></textarea>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Stok Opname</button>
		</div>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();

		$('#periode').datepicker({
			format: "d-M-yyyy",
			autoclose: true,
			todayHighlight: true
		});
	});

	var last_periode = $('#periode').val();
	$('#periode').on('change',function(event) {
		periode_check();
	});

	function periode_check() {
		var periode = $('#periode').val();
		if(periode.length > 3) {
			var mydate = new Date(periode);
			var sendDate = mydate.getFullYear()+'-'+(mydate.getMonth()+1)+'-'+mydate.getDate()

			var post_data = {
				'periode': sendDate
			};

			$('#periode_tick').empty();
			$('#periode_tick').hide();
			$('#periode_loading-us').show();
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url('stok_opname/check_periode');?>",
				data: post_data,
				cache: false,
				success: function(response){
					if(response.success == true){
						$('#periode').css('border', '3px #090 solid');
						$('#periode_loading-us').hide();
						$('#periode_tick').empty();
						$('#existPeriode').val(1);
						$("#periode_tick").append('<span class="fa fa-check"> '+response.message+'</span>');
						$('#periode_tick').show();
					}else {
						$('#periode').css('border', '3px #C33 solid');
						$('#periode_loading-us').hide();
						$('#periode_tick').empty();
						$('#existPeriode').val(0);
						$("#periode_tick").append('<span class="fa fa-close"> '+response.message+'</span>');
						$('#periode_tick').show();
					}
				}
			});
		}else {
			$('#periode').css('border', '3px #C33 solid');
			$('#periode_loading-us').hide();
			$('#existPeriode').val(1);
			 
			$("#periode_tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
			$('#periode_tick').show();
		}
	}

	$('#add_form').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		var periode_exists = $('#existPeriode').val();
		var url = $(this).attr('action');
		e.preventDefault();

		var formData = new FormData();
		formData.set('periode',		$('#periode').val());
		formData.set('keterangan',	$('#keterangan').val());

		if(periode_exists == 0) {
			swal({
				title: 'Periode Sudah Ada!',
				text: 'Apaka Yakin untuk di ganti?',
				type: 'warning',
				showCancelButton: true,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "Ya!",
				showCancelButton: true,
				cancelButtonClass: "btn-default",
				cancelButtonText: "Tidak!"
			}).then(function(confirm) {
				if(confirm) {
					formData.set('periode_exists', true);
					save_form(url, formData);
				}
			}, function(dismiss) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Tambah Stok Opname");
			});
		}else {
			formData.set('periode_exists', false);
			save_form(url, formData);
		}
	}));

	function save_form(url, formData) {
		$.ajax({
			type:'POST',
			url: url,
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						window.location.href = "<?php echo base_url('stok_opname');?>";
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Tambah Stok Opname");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah Stok Opname");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}
</script>