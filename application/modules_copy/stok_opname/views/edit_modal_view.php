<!--Parsley-->
<!-- <script type="text/javascript" src="<?php echo base_url('assets'); ?>/gentelella-master/vendors/parsleyjs/dist/parsley.min.js"></script> -->
<style>
	table {font-size: 10px;}
	#no_po_loading-us{display:none}
	#no_po_tick{display:none}

	.add_edit_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
	.add_edit_item:hover{color:#ff8c00}
</style>

<form class="form-horizontal form-label-left" id="edit_form" role="form" action="<?php echo base_url('stok_opname/save_edit_opname');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="stok_code">Stok Code</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="stok_code" name="stok_code" class="form-control" value="<?php if(isset($opname[0]['stock_code'])){ echo $opname[0]['stock_code']; }?>" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="stok_name">Tanggal Perubahan</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="periode_perubahan" name="periode_perubahan" class="form-control" value="<?php if(isset($opname[0]['tanggal_perubahan'])){ echo date('d-M-Y', strtotime($opname[0]['tanggal_perubahan'])); }?>" >
		</div>
	</div>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="stok_name">Stok Name</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="stok_name" name="stok_name" class="form-control" value="<?php if(isset($opname[0]['stock_name'])){ echo $opname[0]['stock_name']; }?>" readonly>
		</div>
	</div>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty_awal">Qty Awal</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="qty_awal" name="qty_awal" class="form-control" value="<?php if(isset($opname[0]['qty_akhir'])) echo number_format(round($opname[0]['qty_akhir']),0,',','.')?>" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty_akhir">Qty Akhir <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="qty_akhir" name="qty_akhir" class="form-control" placeholder="Qty Perubahan" value="0" autocomplete="off" required="required">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="ket_perubahan">Keterangan <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<textarea class="form-control" id="ket_perubahan" name="ket_perubahan" placeholder="Keterangan" required="required"><?php
				if(isset($opname[0]['keterangan_perubahan'])) echo trim($opname[0]['keterangan_perubahan']);
			?></textarea>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Perubahan Stok Opname</button>
			<input type="hidden" id="id_opname" name="id_opname" value="<?php if(isset($opname[0]['id_stock_opname'])){ echo $opname[0]['id_stock_opname']; }?>">
			<input type="hidden" id="periode" name="periode" value="<?php if(isset($opname[0]['periode'])){ echo date('d-M-Y', strtotime($opname[0]['periode'])); }?>">
		</div>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
		$('#periode_perubahan').datepicker({
			format: "dd-M-yyyy",
			autoclose: true,
			todayHighlight: true
		});

		$('#qty_perubahan').on('focus', function() {
			var jmlh = this.value.replace(/[^0-9]/g, "");
			if(jmlh == '') this.value = 0;
			else this.value = formatNumber(parseInt(jmlh));
		});
		$('#qty_perubahan').on('keyup', function() {
			var jmlh = this.value.replace(/[^0-9]/g, "");
			if(jmlh == '') this.value = 0;
			else this.value = formatNumber(parseInt(jmlh));
		});
		$('#qty_perubahan').on('change', function() {
			if(this.value == '') this.value = 0;
			else this.value = formatNumber(this.value.replace(/[^0-9]/g, ""));
		});
	});

	$('#edit_form').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		var formData = new FormData(this);
		
		$.ajax({
			type:'POST',
			url: $(this).attr('action'),
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Perubahan Stok Opname");
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						//window.location.href = "<?php echo base_url('stok_opname');?>";
						tableOpname.fnDraw();
						$('#panel-modal').modal('toggle');
					});
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Perubahan Stok Opname");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Perubahan Stok Opname");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}));
</script>