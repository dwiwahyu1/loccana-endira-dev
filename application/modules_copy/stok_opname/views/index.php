<style type="text/css">
	.dt-body-center {
		text-align: center;
	}
	.dt-body-right {
		text-align: right;
	}
</style>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title" id="title_menu">Stok Opname</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="card-box" id="div_opname_master">
				<table id="list_periode" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="text-align: center; width: 5%;">No</th>
							<th>Periode</th>
							<th>Ket</th>
							<th>Group</th>
							<th style="width: 8%;"></th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="card-box" id="div_list_opname" style="display: none;"></div>
		</div>
	</div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-dialog-scrollable" style="width: 60%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
	var tableOpname;
	var buttonCommon = {
			exportOptions: {
				format: {
					body: function ( data, row, column, node ) {
						return data;
					}
				}
			}
		};

	$(document).ready(function(){
		get_list_periode();
	});

	function get_list_periode() {
		$("#list_periode").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'stok_opname/list_periode/';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip',
			"initComplete": function(){
				$("div.toolbar").prepend(
					'<div class="btn-group pull-left">'+
						'<a class="btn btn-primary" onClick="add_opname()"><i class="fa fa-calendar-plus-o"></i> Tambah Stok Opname</a>'+
					'</div>');
			},
			"columnDefs": [{
				targets: [0, 4],
				className: 'dt-body-center'
			}]
		});
	}

	function add_opname(){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('stok_opname/add_opname');?>');
		$('#panel-modal  .panel-title').html('<i class="fa fa-building-o"></i>Tambah Stok Opname');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function perubahan_opname(id){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('stok_opname/perubahan_opname/');?>'+"/"+id);
		$('#panel-modal  .panel-title').html('<i class="fa fa-building-o"></i> Perubahan Stok Opname');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function list_by_periode(periode) {
		$('#div_list_opname').show();
		$('#div_opname_master').hide();
		
		$('#div_list_opname').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#div_list_opname').load('<?php echo base_url('stok_opname/detail_stok_opname/');?>'+"/"+periode);

		var jsPeriode = new Date(periode);
		var titlePeriode = jsPeriode.getDate() +'-'+ months[jsPeriode.getMonth()] +'-'+ jsPeriode.getFullYear();
		$('#title_menu').html('Stok Opname Periode : ' + titlePeriode);
	}

	function delete_periode(id, periode) {
		swal({
			title: 'Yakin akan Menghapus ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Ya!",
			cancelButtonClass: "btn-default",
			cancelButtonText: "Tidak!"
		}).then(function () {
			var datapost = {
				'id'		: id,
				'periode'	: periode
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>stok_opname/delete_periode",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
				   swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						window.location.href = "<?php echo base_url('stok_opname');?>";
					})

					if (response.status != "success") swal("Failed!", response.message, "error");
				}
			});
		})
	}
</script>