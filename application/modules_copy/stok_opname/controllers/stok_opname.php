<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Stok_Opname extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('stok_opname/stok_opname_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'stok_opname/views/index');
	}

	function list_periode() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

		$order_fields = array('', 'a.periode', 'a.keterangan', 'a.pic', 'b.nama', 'd.group');
		$search = $this->input->get_post('search');
		$search_val = (!empty($search['value'])) ? $search['value'] : null;
		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->stok_opname_model->list_periode($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = $params['offset']+1;
		foreach ($list['data'] as $k => $v) {
			if($i == 0 || $i == 1) {
				$strBtn =
					'<div class="btn-group">'.
						'<a class="btn btn-success btn-sm" title="Detail" onClick="list_by_periode(\''.$v['periode'].'\')">'.
							'<i class="fa fa-search"></i>'.
						'</a>'.
					/*
					'</div>'.
					'<div class="btn-group">'.
						'<a class="btn btn-danger btn-sm" title="Delete" onClick="delete_periode(\''.$v['id_so_master'].'\',\''.$v['periode'].'\')">'.
							'<i class="fa fa-trash"></i>'.
						'</a>'.
					*/
					'</div>';
			}else {
				$strBtn =
					'<div class="btn-group">'.
						'<a class="btn btn-success btn-sm" title="Detail" disabled="disabled">'.
							'<i class="fa fa-search"></i>'.
						'</a>'.
					/*
					'</div>'.
					'<div class="btn-group">'.
						'<a class="btn btn-danger btn-sm" title="Delete" onClick="delete_periode(\''.$v['id_so_master'].'\',\''.$v['periode'].'\')">'.
							'<i class="fa fa-trash"></i>'.
						'</a>'.
					*/
					'</div>';
			}
			array_push($data, array(
				$i++,
				date('d-M-Y', strtotime($v['periode'])),
				$v['keterangan'],
				$v['group'],
				$strBtn
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function detail_stok_opname($periode) {
		// echo "<pre>";
		// print_r($periode);die;
		// $result_opname = $this->stok_opname_model->edit_perubahan($id_opname);

		$data = array(
			'periode' => (string)$periode
		);

		$this->load->view('detail_view', $data);
	}

	public function add_opname() {
		$result_material = $this->stok_opname_model->get_material();

		$data = array(
			'material' => $result_material
		);

		$this->load->view('add_modal_view', $data);
	}

	public function check_periode() {
		$this->form_validation->set_rules('periode', 'Periode', 'trim|required|is_unique[t_stockopname_master.periode]');
		$this->form_validation->set_message('is_unique', 'Periode Already Exists.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'Periode is Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}

	public function save_periode() {
		$this->form_validation->set_rules('periode', 'Periode', 'trim|required');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$periode 			= $this->Anti_sql_injection($this->input->post('periode', TRUE));
			$keterangan 		= $this->Anti_sql_injection($this->input->post('keterangan', TRUE));
			$periode_exists 	= $this->Anti_sql_injection($this->input->post('periode_exists', TRUE));
			
			$data = array(
				'periode'			=> date('Y-m-d', strtotime($periode)),
				'keterangan'		=> $keterangan,
				'periode_exists' 	=> $periode_exists,
				'pic'				=> $this->session->userdata['logged_in']['user_id']
			);

			if($periode_exists == false || $periode_exists == 'false') $result_opname_master = $this->stok_opname_model->add_stok_opname_master($data);
			else $result_opname_master = 1;

			if($result_opname_master > 0) {
				$result_opname = $this->stok_opname_model->add_stok_opname($data);
				if ($result_opname > 0) {
					$this->log_activity->insert_activity('insert', 'Insert Stok Opname Periode = '.$periode);
					$result = array('success' => true, 'message' => 'Berhasil menambahkan Stok Opname ke database');
				}else {
					$this->log_activity->insert_activity('insert', 'Gagal Insert Stok Opname Periode = '.$periode);
					$result = array('success' => false, 'message' => 'Gagal menambahkan Stok Opname ke database');
				}
			}else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert Stok Opname Periode = '.$periode);
				$result = array('success' => false, 'message' => 'Gagal menambahkan Stok Opname ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function delete_periode() {
		$data 	= file_get_contents("php://input");
		$params 	= json_decode($data,true);

		$result = $this->stok_opname_model->stockopname_delete($params);

		if($result > 0) {
			$this->log_activity->insert_activity('delete', 'Delete Opname Master id : '.$params['id']);
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}else {
			$this->log_activity->insert_activity('delete', 'Gagal Delete Opname Master id : '.$params['id']);
			$res = array('status' => 'failed', 'message' => 'Data gagal di hapus');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	function list_opname() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

		$order_fields = array('', 'stock_code', 'stock_name');
		$search = $this->input->get_post('search');
		$search_val = (!empty($search['value'])) ? $search['value'] : null;
		$search_value = $this->Anti_sql_injection($search_val);

		$periode = ($this->input->get_post('periode') != FALSE) ? $this->input->get_post('periode') : NULL;

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		if($periode != null || $periode != '') $params['filterPeriode'] = $periode;
		else $params['filterPeriode'] = '';

		if($this->input->get_post('lengthDownload') != FALSE) {
			$lengthDownload = (int)$this->input->get_post('lengthDownload');
			if($lengthDownload < 0) $limitRecord = $this->stok_opname_model->count_opname($params['filterPeriode']);
			else $limitRecord = $lengthDownload;
			$params['limit'] = $limitRecord;
		}else if($params['limit'] < 0) $params['limit'] = $this->stok_opname_model->count_opname($params['filterPeriode']);

		$list = $this->stok_opname_model->list_opname($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = $params['offset']+1;

		if($this->input->get_post('lengthDownload') != FALSE) {
			foreach ($list['data'] as $k => $v) {
				if($v['periode'] != NULL || $v['periode'] != '') $periode = date('d-M-Y', strtotime($v['periode']));
				else $periode = '';
	
				if($v['tanggal_perubahan'] != NULL || $v['tanggal_perubahan'] != '') $tgl_perubahan = date('d-M-Y', strtotime($v['tanggal_perubahan']));
				else $tgl_perubahan = '';
	
				array_push($data, array(
					$i++,
					$v['no_bc'],
					$v['stock_code'],
					$v['stock_name'],
					$v['uom_name'],
					$v['type_material_name'],
					round($v['qty_awal']),
					round($v['perubahan']),
					round($v['qty_akhir']),
					$v['keterangan_perubahan'],
					$tgl_perubahan
				));
			}
		}else {
			foreach ($list['data'] as $k => $v) {
				if($v['id_stock_opname'] != NULL || $v['id_stock_opname'] != '') {
					$strOption = 
						'<div class="btn-group">'.
							'<button class="btn btn-warning btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Perubahan"
								onClick="perubahan_opname(\'' . $v['id_stock_opname'] . '\')">'.
								'<i class="fa fa-edit"></i>'.
							'</button>'.
						'</div>';
				}else $strOption = '';
	
				if($v['periode'] != NULL || $v['periode'] != '') $periode = date('d-M-Y', strtotime($v['periode']));
				else $periode = '';
	
				if($v['tanggal_perubahan'] != NULL || $v['tanggal_perubahan'] != '') $tgl_perubahan = date('d-M-Y', strtotime($v['tanggal_perubahan']));
				else $tgl_perubahan = '';
	
				array_push($data, array(
					$i++,
					$v['no_bc'],
					$v['stock_code'],
					$v['stock_name'],
					$v['uom_name'],
					$v['type_material_name'],
					round($v['qty_awal']),
					round($v['perubahan']),
					round($v['qty_akhir']),
					$v['keterangan_perubahan'],
					$tgl_perubahan,
					$strOption
				));
			}
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function perubahan_opname($id_opname) {
		$result_opname = $this->stok_opname_model->edit_perubahan($id_opname);

		$data = array(
			'opname' => $result_opname
		);

		$this->load->view('edit_modal_view', $data);
	}

	public function save_edit_opname() {
		$this->form_validation->set_rules('qty_akhir', 'Qty Akhir', 'trim|required');
		$this->form_validation->set_rules('ket_perubahan', 'Keterangan Perubahan', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$id_opname			= $this->Anti_sql_injection($this->input->post('id_opname', TRUE));
			$qty_awal			= $this->Anti_sql_injection($this->input->post('qty_awal', TRUE));
			$qty_akhir			= $this->Anti_sql_injection($this->input->post('qty_akhir', TRUE));
			$ket_perubahan		= $this->Anti_sql_injection($this->input->post('ket_perubahan', TRUE));
			$periode			= $this->Anti_sql_injection($this->input->post('periode', TRUE));
			$tanggal_perubahan			= $this->Anti_sql_injection($this->input->post('periode_perubahan', TRUE));

			$result_opname = $this->stok_opname_model->edit_perubahan($id_opname);
			if(sizeof($result_opname) > 0) {
				$dataPerubahan = array(
					'id_opname'				=> $id_opname,
					'keterangan_perubahan'	=> $ket_perubahan,
					'tanggal_perubahan'		=> date('Y-m-d',strtotime($tanggal_perubahan)),
					'periode'				=> date('Y-m-d', strtotime($periode)),
					'qty_awal'				=> $qty_awal,
					'qty_akhir'				=> $qty_akhir,
					'pic'					=> $this->session->userdata['logged_in']['user_id']
				);

				$dataMutasi = array(
					'id_stock_awal'		=> $result_opname[0]['id_material'],
					'id_stock_akhir'	=> $result_opname[0]['id_material'],
					'tanggal_perubahan' =>$dataPerubahan['tanggal_perubahan']
				);

				if($qty_akhir > $qty_awal) {
					$dataPerubahan['perubahan'] = $qty_akhir - $qty_awal;
					$dataMutasi['amount_mutasi'] = $qty_akhir - $qty_awal;
					$dataMutasi['type_mutasi'] = 3;
				}else {
					$dataPerubahan['perubahan'] = $qty_awal - $qty_akhir;
					$dataMutasi['amount_mutasi'] = $qty_awal - $qty_akhir;
					$dataMutasi['type_mutasi'] = 4;
				}
				$resultPerubahan = $this->stok_opname_model->perubahan_update($dataPerubahan);
				if($resultPerubahan > 0) {
					$resultMutasiAdd = $this->stok_opname_model->mutasi_add($dataMutasi);
					if ($resultMutasiAdd > 0) {
						$this->log_activity->insert_activity('update', 'Update Stok Opname id : '.$id_opname);
						$result = array('success' => true, 'message' => 'Berhasil mengubah Stok Opname ke database');
					}else {
						$this->log_activity->insert_activity('update', 'Gagal Update Stok Opname id : '.$id_opname);
						$result = array('success' => false, 'message' => 'Gagal mengubah Stok Opname ke database');
					}
				}else {
					$this->log_activity->insert_activity('update', 'Gagal Update Stok Opname id : '.$id_opname);
					$result = array('success' => false, 'message' => 'Gagal mengubah Stok Opname ke database');
				}
			}else $result = array('success' => false, 'message' => 'Invalid respon, silahkan cek koneksi.');

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}
}