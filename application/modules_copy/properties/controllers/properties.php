<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Properties extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('properties/properties_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'properties/views/index');
	}

	function list_properties() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;
		$order_fields = array('', 'properties_name');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->properties_model->list_properties($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$status_akses =
				'<div class="btn-group">'.
					'<button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
						onClick="edit_properties(\'' . $v['id_properties'] . '\')">'.
						'<i class="fa fa-edit"></i>'.
					'</button>'.
				'</div>'.
				'<div class="btn-group">'.
					'<button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete"
						onClick="delete_properties(\'' . $v['id_properties'] . '\')">'.
						'<i class="fa fa-trash"></i>'.
					'</button>'.
				'</div>'.
				'<div class="btn-group">'.
					'<button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Details"
						onClick="details_properties(\'' . $v['id_properties'] . '\')">'.
						'<i class="fa fa-list-alt"></i>'.
					'</button>'.
				'</div>';

			array_push($data, array(
				$i,
				ucwords($v['properties_name']),
				$status_akses
			));
		}

		$result["data"] = $data;
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_properties() {
		$this->load->view('add_modal_view');
	}

	public function check_propname(){
		$this->form_validation->set_rules('propname', 'Properties', 'trim|required|min_length[4]|max_length[100]|is_unique[m_properties.properties_name]');
		$this->form_validation->set_message('is_unique', 'Properties Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'Nama Properties Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}

	public function save_properties() {
		$this->form_validation->set_rules('propname', 'Properties', 'trim|required|min_length[4]|max_length[100]|is_unique[m_properties.properties_name]',array('is_unique' => 'This %s Properties already exists.'));

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$properties = ucwords($this->Anti_sql_injection($this->input->post('propname', TRUE)));

			$data = array(
				'properties_name' => $properties
			);

			$result = $this->properties_model->add_properties($data);

			if ($result > 0) {
				$this->log_activity->insert_activity('insert', 'Insert Propertie');
				$result = array('success' => true, 'message' => 'Berhasil menambahkan Propertie ke database');
			} else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert Propertie');
				$result = array('success' => false, 'message' => 'Gagal menambahkan Propertie ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function edit_properties($id) {
		$result = $this->properties_model->edit_properties($id);

		$data = array(
			'properties' => $result
		);

		$this->load->view('edit_modal_view', $data);
	}

	public function save_edit_properties() {
		$this->form_validation->set_rules('propname', 'Properties', 'trim|required|min_length[4]|max_length[100]|is_unique[m_properties.properties_name]',array('is_unique' => 'This %s Properties already exists.'));

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array( 'success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$id_properties = $this->Anti_sql_injection($this->input->post('prop_id'));
			$properties = ucwords($this->Anti_sql_injection($this->input->post('propname', TRUE)));

			$data = array(
				'id_properties' => $id_properties,
				'properties_name' => $properties
			);

			$result = $this->properties_model->save_edit_properties($data);

			if ($result > 0) {
				$this->log_activity->insert_activity('update', 'Update Propertie id : '.$id_properties);
				$result = array('success' => true, 'message' => 'Berhasil mengubah Propertie ke database');
			} else {
				$this->log_activity->insert_activity('update', 'Gagal Update Propertie id : '.$id_properties);
				$result = array('success' => false, 'message' => 'Gagal mengubah Propertie ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function delete_properties() {
		$data 	= file_get_contents("php://input");
		$params = json_decode($data,true);
		$result = $this->properties_model->delete_properties($params['id']);
		
		if($result > 0) {
			$this->log_activity->insert_activity('delete', 'Delete Propertie id : '. $params['id']);
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}else {
			$this->log_activity->insert_activity('delete', 'Gagal Delete Propertie id : '. $params['id']);
			$res = array('status' => 'failed', 'message' => 'Data gagal di hapus');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function detail_properties($id) {
		$data = array('d_prop_id' => $id);

		$this->load->view('detail_prop_view', $data);
	}

	public function list_detail_properties($id) 
	{
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;
		$order_fields = array('', 'detail_name','id_properties','param_detail_name');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;
		$params['id'] = $id;

		$list = $this->properties_model->detail_properties($params);
		$result_prop = $this->properties_model->edit_properties($id);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$status_akses =
				'<div class="btn-group">'.
					'<button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
						onClick="edit_detail_prop(\'' . $v['id_detail_prop'] . '\')">'.
						'<i class="fa fa-edit"></i>'.
					'</button>'.
				'</div>'.
				'<div class="btn-group">'.
					'<button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete"
						onClick="delete_detail_prop(\'' . $v['id_detail_prop'] . '\')">'.
						'<i class="fa fa-trash"></i>'.
					'</button>'.
				'</div>';

			array_push($data, array(
				$i,
				ucwords($v['detail_name']),
				ucwords($v['param_detail_name']),
				$result_prop[0]['properties_name'],
				$status_akses
			));
		}

		$result["data"] = $data;
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_detail_prop() {
		$result_properties = $this->properties_model->properties();
		$data = array(
			'properties' => $result_properties
		);

		$this->load->view('add_modal_d_prop_view', $data);
	}

	public function check_d_propname(){
		$this->form_validation->set_rules('d_propname', 'DetailPropName', 'trim|required|min_length[4]|max_length[100]|is_unique[d_detail_properties.detail_name]');
		$this->form_validation->set_message('is_unique', 'Detail Properties Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'Nama Detail Properties Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}

	public function save_detail() {
		$this->form_validation->set_rules('d_propname', 'DetailPropName', 'trim|required|min_length[4]|max_length[100]|is_unique[d_detail_properties.detail_name]',array('is_unique' => 'This %s Detail Properties already exists.'));
		$this->form_validation->set_rules('param_d_prop', 'ParamName', 'trim|required|max_length[100]');
		// $this->form_validation->set_rules('prop_id', 'Properties', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$detail_name = ucwords($this->Anti_sql_injection($this->input->post('d_propname', TRUE)));
			$param_detail_name = ucwords($this->Anti_sql_injection($this->input->post('param_d_prop', TRUE)));
			$id_properties = $this->Anti_sql_injection($this->input->post('prop_id', TRUE));

			$data = array(
				'detail_name' => $detail_name,
				'param_detail_name' => $param_detail_name,
				'id_properties' => $id_properties
			);

			$result = $this->properties_model->add_detail_properties($data);

			if ($result > 0) {
				$this->log_activity->insert_activity('insert', 'Insert Detail Properties');
				$result = array('success' => true, 'message' => 'Berhasil menambahkan Detail Properties ke database');
			}else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert Detail Properties');
				$result = array('success' => false, 'message' => 'Gagal menambahkan Detail Properties ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function edit_detail_prop($id) {
		$result = $this->properties_model->edit_detail_prop($id);
		$result_properties = $this->properties_model->properties();

		$data = array(
			'detail' => $result,
			'properties' => $result_properties
		);

		$this->load->view('edit_modal_d_prop_view', $data);
	}

	public function save_edit_detail() {
		$this->form_validation->set_rules('d_propname', 'DetailPropName', 'trim|required|min_length[4]|max_length[100]|is_unique[d_detail_properties.detail_name]',array('is_unique' => 'This %s Detail Properties already exists.'));
		$this->form_validation->set_rules('param_d_prop', 'ParamName', 'trim|required|max_length[100]');
		// $this->form_validation->set_rules('prop_id', 'Properties', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$id_detail_prop = $this->Anti_sql_injection($this->input->post('d_prop_id', TRUE));
			$detail_name = ucwords($this->Anti_sql_injection($this->input->post('d_propname', TRUE)));
			$param_detail_name = ucwords($this->Anti_sql_injection($this->input->post('param_d_prop', TRUE)));
			$id_properties = $this->Anti_sql_injection($this->input->post('prop_id', TRUE));

			$data = array(
				'id_detail_prop' => $id_detail_prop,
				'detail_name' => $detail_name,
				'param_detail_name' => $param_detail_name,
				'id_properties' => $id_properties
			);

			$result = $this->properties_model->save_edit_detail($data);

			if ($result > 0) {
				$this->log_activity->insert_activity('update', 'Update Detail Properties id : '. $id_detail_prop);
				$result = array('success' => true, 'message' => 'Berhasil mengubah Detail Properties ke database');
			} else {
				$this->log_activity->insert_activity('update', 'Gagal Update Detail Properties id : '. $id_detail_prop);
				$result = array('success' => false, 'message' => 'Gagal mengubah Detail Properties ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function delete_detail_prop() {
		$data 	= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		$result = $this->properties_model->delete_detail_prop($params['id']);
		if($result > 0) {
			$this->log_activity->insert_activity('delete', 'Delete Detail Properties id : '.$params['id']);
			$res = array('status' => 'success','message' => 'Data telah di hapus');
		}else {
			$this->log_activity->insert_activity('delete', 'Gagal Delete Detail Properties id : '.$params['id']);
			$res = array('status' => 'failed', 'message' => 'Data gagal di hapus');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}
}