<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Properties_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function properties() {
		$sql 	= 'SELECT * FROM m_properties;';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function list_properties($params = array()) {
		// print_r($params);die;
		$sql 	= 'CALL prop_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();
		// print_r($result);die;

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();
		// print_r($total);die;

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);
		// print_r($return);die;

		return $return;
	}

	public function add_properties($data) {
		$sql 	= 'CALL prop_add(?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['properties_name']
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_properties($id) {
		$sql 	= 'CALL prop_search_id(?)';

		$query 	=  $this->db->query($sql,
			array($id)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function save_edit_properties($data) {
		$sql 	= 'CALL prop_update(?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id_properties'],
				$data['properties_name']
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function delete_properties($data) {
		$sql 	= 'CALL prop_delete(?)';
		$query 	=  $this->db->query($sql,
			array($data)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function detail_properties($params = array()) {
		$sql 	= 'CALL d_prop_search_id_prop(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['id'],
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function add_detail_properties($data) {
		$sql 	= 'CALL d_prop_add(?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['detail_name'],
				$data['param_detail_name'],
				$data['id_properties'],
				'2'
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_detail_prop($id) {
		$sql 	= 'CALL d_prop_search_id(?)';

		$query 	=  $this->db->query($sql,
			array($id)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function save_edit_detail($data) {
		$sql 	= 'CALL d_prop_update(?,?,?,?,?)';
		
		$query 	=  $this->db->query($sql,
			array(
				$data['id_detail_prop'],
				$data['detail_name'],
				$data['param_detail_name'],
				$data['id_properties'],
				'2'
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function delete_detail_prop($data) {
		$sql 	= 'CALL d_prop_delete(?)';
		$query 	=  $this->db->query($sql,
			array($data)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}