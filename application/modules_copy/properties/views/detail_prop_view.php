<style>
	#loading-us{display:none}
	#tick{display:none}

	#loading-mail{display:none}
	#cross{display:none}
</style>

<div class="btn-group pull-left">
	<button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Back" onClick="back_properties()">
		<i class="fa fa-arrow-left"></i>
	</button>
</div>

<table id="list_detail_properties" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th style="width: 5px;">No</th>
			<th>Nama Detail</th>
			<th>Parameter Detail</th>
			<th>Properties</th>
			<th style="width: 200px;">Option</th>
		</tr>
	</thead>
	<tbody>
		<td style="width: 5px;">No</td>
		<th>Nama Detail</th>
		<th>Parameter Detail</th>
		<th>Properties</th>
		<td style="width: 200px;">Option</td>
	</tbody>
</table>

<script type="text/javascript">
	var id_prop = "<?php if(isset($d_prop_id)){ echo $d_prop_id; } ?>";
	var table_d_prop;
	$(document).ready(function() {
		get_detail_properties();
	});

	function get_detail_properties() {
		table_d_prop = $("#list_detail_properties").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'properties/list_detail_properties/'; ?>" + id_prop,
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip',
			"initComplete": function(){
				$("#div_detail_properties div.toolbar").prepend(
					'<div class="btn-group pull-left">'+
						'<a class="btn btn-primary" onClick="add_detail_prop()">'+
							'<i class="fa fa-cogs"></i> Add Detail Properties'+
						'</a>'+
					'</div>'
				);
			}
		});
	}

	function add_detail_prop() {
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('properties/add_detail_prop');?>');
		$('#panel-modal  .panel-title').html('<i class="fa fa-cogs"></i> Add Detail Properties');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function edit_detail_prop(id){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('properties/edit_detail_prop/');?>'+"/"+id);
		$('#panel-modal  .panel-title').html('<i class="fa fa-cogs"></i> Edit Detail Properties');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function delete_detail_prop(id){
		swal({
			title: 'Yakin akan Menghapus ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost={
				"id" : id
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>properties/delete_detail_prop",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
				   swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						redrawData();
					})

					if (response.status == "success") {

					}else {
						swal("Failed!", response.message, "error");
					}
				}
			});
		})
	}

	function redrawData() {
		table_d_prop.fnClearTable();
		table_d_prop.fnDestroy();
		get_detail_properties();
	}

	function back_properties() {
		$('#title_Menu').html('Properties');
		$('#div_list_properties').show();
		$('#div_detail_properties').hide();
		$('#div_detail_properties').html('');
	}
</script>