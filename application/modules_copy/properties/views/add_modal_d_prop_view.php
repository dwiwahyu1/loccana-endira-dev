<!--Parsley-->
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/gentelella-master/vendors/parsleyjs/dist/parsley.min.js"></script>
<style>
	#loading-us{display:none}
	#tick{display:none}

	#loading-mail{display:none}
	#cross{display:none}
</style>
	
<form class="form-horizontal form-label-left" id="add_detail_properties" role="form" action="<?php echo base_url('properties/save_detail');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="d_propname">Nama Detail Properties <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-minlength="4" data-parsley-maxlength="100" type="text" id="d_propname" name="d_propname" class="form-control col-md-7 col-xs-12" placeholder="nama properties minimal 4 karakter" required="required">
			<span id="loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking detail properties...</span>
			<span id="tick"></span>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="param_d_prop">Parameter Detail Properties <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="100" type="text" id="param_d_prop" name="param_d_prop" class="form-control col-md-7 col-xs-12" placeholder="nama parameter minimal 4 karakter" required="required">
			<span id="loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking properties...</span>
			<span id="tick"></span>
		</div>
	</div>

	<!-- <div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="prop_id">Properties <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="prop_id" name="prop_id" style="width: 100%" required>
				<option value="" >-- Select Properties --</option>
				<?php foreach($properties as $key) { ?>
					<option value="<?php echo $key['id_properties']; ?>" ><?php echo $key['properties_name']; ?></option>
				<?php } ?>
			</select>
		</div>;p0;99;p
	</div> -->

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Detail Properties</button>
			<input type="hidden" id="prop_id" name="prop_id" value="">
		</div>
	</div>

</form>
<!-- /page content -->

<script type="text/javascript">
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
		// $("#prop_id").val(id_prop).change();
		$('#prop_id').val(id_prop);
	});

	var lastd_propname = $('#d_propname').val();
	$('#d_propname').on('input',function(event) {
		if($('#d_propname').val() != lastd_propname) {
			d_propname_check();
		}
	});

	function d_propname_check() {
		var d_propname = $('#d_propname').val();
		if(d_propname.length > 3) {
			var post_data = {
				'd_propname': d_propname
			};

			$('#tick').empty();
			$('#tick').hide();
			$('#loading-us').show();
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url('properties/check_d_propname');?>",
				data: post_data,
				cache: false,
				success: function(response){
					if(response.success == true){
						$('#d_propname').css('border', '3px #090 solid');
						$('#loading-us').hide();
						$('#tick').empty();
						$("#tick").append('<span class="fa fa-check"> '+response.message+'</span>');
						$('#tick').show();
					}else {
						$('#d_propname').css('border', '3px #C33 solid');
						$('#loading-us').hide();
						$('#tick').empty();
						$("#tick").append('<span class="fa fa-close"> '+response.message+'</span>');
						$('#tick').show();
					}
				}
			});
		}else {
			$('#d_propname').css('border', '3px #C33 solid');
			$('#loading-us').hide();
			$('#tick').empty();
			$("#tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
			$('#tick').show();
		}
	}

	$('#add_detail_properties').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		var formData = new FormData(this);

		$.ajax({
			type:'POST',
			url: $(this).attr('action'),
			data:formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						redrawData();
						$('#panel-modal').modal('hide');
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Tambah Detail Properties");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah Detail Properties");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}));
</script>
