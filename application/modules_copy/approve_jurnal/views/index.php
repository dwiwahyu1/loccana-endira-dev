<style>
	.dt-body-left {text-align:left;}
	.dt-body-right {text-align:right;}
	.dt-body-center {text-align:center; vertical-align: middle;}
	.force-overflow {height: 650px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow {min-height: 650px}
	.filter_separator{padding:0px 10px;}
</style>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Approve Jurnal</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<table id="listApproveJurnal" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="width: 5px;">No</th>
							<th>Perkiraan</th>
							<th>COA</th>
							<th>Keterangan</th>
							<th>Tanggal</th>
							<th>Valas</th>
							<th>Nilai</th>
							<th>Tipe</th>
							<th>Status</th>
							<th style="width: 150px;">Option</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="width: 5px;">No</td>
							<td>Perkiraan</td>
							<td>COA</td>
							<td>Keterangan</td>
							<td>Tanggal</td>
							<td>Valas</td>
							<td>Nilai</td>
							<td>Tipe</td>
							<td>Status</td>
							<td style="width: 150px;">Option</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$(document).ready(function(){
		listApproveJurnal();
	});

	function listApproveJurnal(){
		var coa = (typeof $('.select_coa').val() !== "undefined")?$('.select_coa').val():'0';
		var type = (typeof $('.select_tipe').val() !== "undefined")?$('.select_tipe').val():'2';
		var valas = (typeof $('.select_valas').val() !== "undefined")?$('.select_valas').val():'0';
		
      $("#listApproveJurnal").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'approve_jurnal/lists_approve_jurnal?coa="+coa+"&type="+type+"&valas="+valas+"';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false,
			"destroy": true,
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip',
			"initComplete": function(result){
				$("#listjurnalapprove_filter").css("padding-top","50px");

				$("div.toolbar").prepend(element);

				var selecttype0 = (type==='0')?'selected':'';
				var selecttype1 = (type==='1')?'selected':'';
				var element = '<div class="btn-group pull-left">';
					element += '	<label>Filter</label>';
					element += '	<label class="filter_separator">|</label>';
					element += '	<label>Perkiraan:';
					element += '		<select class="form-control select_coa" onChange="listjurnal()" style="height:30px;">';
					element += '			<option value="0">-- Semua Perkiraan --</option>';
					$.each( result.json.coafilter, function( key, val ) {
						var selectcoa = (val.id_coa===coa)?'selected':'';
			            element += '		<option value="'+val.id_coa+'" '+selectcoa+'>'+val.keterangan+'</option>';
			        });
		        	element += '		</select>';
		        	element += '	</label>';
		        	element += '	<label class="filter_separator">|</label>';
		        	element += '	<label>Tipe:';
					element += '		<select class="form-control select_tipe" onChange="listjurnal()" style="height:30px;">';
					element += '			<option value="2">-- Semua Tipe --</option>';
					element += '			<option value="0" '+selecttype0+'>Pemasukan</option>';
					element += '			<option value="1" '+selecttype1+'>Pengeluaran</option>';
		        	element += '		</select>';
		        	element += '	</label>';
		        	element += '	<label class="filter_separator">|</label>';
					element += '	<label>Valas:';
					element += '		<select class="form-control select_valas" onChange="listjurnal()" style="height:30px;">';
					element += '			<option value="0">-- Semua Valas --</option>';
					$.each( result.json.valasfilter, function( key, val ) {
						var selectvalas = (val.valas_id===valas)?'selected':'';
			            element += '		<option value="'+val.valas_id+'" '+selectvalas+'>'+val.nama_valas+'</option>';
			        });
		        	element += '		</select>';
		        	element += '	</label>';
					element += '</div>';

		        $("#listjurnalapprove_filter").prepend(element);
			},
			"columnDefs": [{
				"targets": [8],
				"searchable": false,
				"className": 'dt-body-center',
				"width": 90
			},{
				"targets": [9],
				"searchable": false,
				"className": 'dt-body-center',
				"width": 80
			}]
		});
    }

	function coa(level,id){
		$('#loading').show();
		$.ajax({
	        type:'GET',
	        url: "<?php echo base_url();?>jurnal/change_coa",
	        data : {id:id},
	        success: function(response) {
	        	if(response.length){
	        		var element = '';
		            $.each( response, function( key, val ) {
		            	element += '<option value="'+val.id_coa+'" data-coa="'+val.coa+'">'+val.keterangan+'</option>';
		            });
		            
		            if(level === 'level1'){
		            	$('#level2').html(element);
		            	$('#level2_temp').show();

		            	coa('level2',response[0].coa);
		            }else if(level === 'level2'){
		            	$('#level3').html(element);
		            	$('#level3_temp').show();

		            	coa('level3',response[0].coa);
		            }else{
		            	$('#level4').html(element);
		            	$('#level4_temp').show();
		            	$('#loading').hide();
		            }
	        	}else{
	        		if(level === 'level1'){
		            	$('#level2_temp,#level3_temp,#level4_temp').hide();
		            }else if(level === 'level2'){
		            	$('#level3_temp,#level4_temp').hide();
		            }else{
		            	$('#level4_temp').hide();
		            }
		            $('#loading').hide();
	        	}
	        }
	    });
	}
	
	function approve_jurnal(id){
		swal({
			title: 'Silahkan Pilih Status',
			text: 'Approve Jurnal',
			input: 'select',
			inputClass: 'form-control',
			inputPlaceholder: 'Please Select',
			inputOptions: {
			  '1' : 'Approve',
			  '2' : 'Reject'
			},
			inputValidator: (value) => {
				return new Promise((resolve) => {
					if (value === '') {
						resolve('Pilih Status!')
					} else {
						resolve()
					}
				})
			}
		}).then(function (value) {
			var datapost = {
				"id"   		:   id,
				"status"  	:   value
			};
			
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('approve_jurnal/jurnal_update_status');?>",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
					if(response.success == true) {
					   swal({
							title: 'Apakah anda yakin!',
							text: response.title,
							type: 'warning',
							showCancelButton: true,
							confirmButtonColor: '#3085d6',
							cancelButtonColor: '#d33',
							confirmButtonText: 'Ok'
						}).then(function () {
							listApproveJurnal();
						})
					}else{
						swal("Warning!", response.message, "info");
					}
				}
			});
		})
	}
</script>