<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Approve_Jurnal_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
	}

	/**
      * This function is get the list data in coa_value table
      * @param : $params is where condition for select query
      */

	public function lists($params = array())
	{
		$sql_all 	= 'CALL coavalue_list_all_temporary(?, ?, ?, ?, ?, ?, ?, ?,?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				'',
				$params['coa'],
				$params['type'],
				$params['valas'],
				NULL
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL coavalue_list_temporary(?, ?, ?, ?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter'],
				$params['coa'],
				$params['type'],
				$params['valas'],
				NULL
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total_row,
			'total' => $total_row,
		);

		return $return;
	}

	/**
      * This function is get the list data in coa table
      * @param : $params is where condition for select query
      */
	public function coa($search,$id)
	{
		$this->db->select('id_coa,id_parent,coa,keterangan');
		$this->db->from('t_coa');
		$this->db->where($search, $id);

		$query 	= $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function coa_list($id_coa)
	{
		$this->db->select('keterangan');
		$this->db->from('t_coa');
		$this->db->where($id_coa);

		$query 	= $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	/**
      * This function is get the list data in valas table
      */
	public function valas()
	{
		$this->db->select('valas_id,nama_valas');
		$this->db->from('m_valas');

		$query 	= $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is used to Insert Record in coa_value_temporary table
      * @param : $data - record array 
      */

			public function update_status($data) {
				$sql 	= 'CALL coavalue_add_temporary(?,?,?,?,?,?,?,?,?,?)';
		
				$query = $this->db->query($sql,array(
					$data['id_coa'],
					$data['id_parent'],
					$data['date'],
					$data['id_valas'],
					$data['value'],
					0,
					$data['type_cash'],
					$data['note'],
					$data['rate'],
					$data['bukti']

					// IN `pin_id_coa` INT(11),
					// IN `pin_id_parent` INT(11),
					// IN `pin_date` DATE,
					// IN `pin_id_valas` INT(11),
					// IN `pin_value` FLOAT,
					// IN `pin_adjusment` VARCHAR(255),
					// IN `pin_type_cash` INT(11),
					// IN `pin_note` VARCHAR(255),
					// IN `pin_rate` DECIMAL(20,4),
					// IN `pin_bukti` VARCHAR(255),
					// IN `pin_pin_pic` INT(11),
					// IN `pin_approve` INT(11),
					// IN `pin_status` INT(11)
				));
		
				$result	= $this->db->affected_rows();
		
				$this->db->close();
				$this->db->initialize();
		
				return $result;
			}

	/**
      * This function is used to Insert Record in coa_value table
      * @param : $data - record array 
      */

	public function add_jurnal($data) {
		$sql 	= 'CALL coavalue_add3(?,?,?,?,?,?,?,?,?,?,?)';

		$query = $this->db->query($sql,array(
			$data['id_coa'],
			$data['id_parent'],
			$data['date'],
			$data['id_valas'],
			$data['value'],
			0,
			$data['type_cash'],
			$data['note'],
			$data['rate'],
			$data['bukti'],
			$data['idtemp']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	/**
      * This function is used to Insert Record in coa_value table
      * @param : $data - record array 
      */

			public function update_status_temp($data) {
				$sql 	= 'CALL coavalue_temporary_update(?,?,?)';
		
				$query = $this->db->query($sql,array(
					$data['id'],
					$data['userid'],
					$data['status']
				));
		
				$result	= $this->db->affected_rows();
		
				$this->db->close();
				$this->db->initialize();
		
				return $result;
			}


	/**
     * This function is used to delete coa_value
     * @param: $id - id of coa_value table
     */
	function delete_jurnal($id) {
		$this->db->where('id', $id);  
		$this->db->delete('t_coa_value'); 

		$this->db->close();
		$this->db->initialize();
	}

	/**
      * This function is get data in coa_value table by id
      * @param : $id is where condition for select query
      */

	public function jurnal_id($id)
	{
		$sql 	= 'CALL coavalue_search_id_temporary(?)';

		$query 	= $this->db->query($sql,
			array(
				$id
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function delete_approve_jurnal($id)
	{
		$sql 	= 'CALL coavalue_delete_temporary(?)';

		$query 	= $this->db->query($sql,
			array(
				$id
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
}