<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Approve_Jurnal extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('approve_jurnal/approve_jurnal_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	 * This function is redirect to index jurnal page
	 * @return Void
	 */
	public function index() {
		$this->template->load('maintemplate', 'approve_jurnal/views/index');
	}

	/**
	 * This function is used for showing jurnal list
	 * @return Array
	 */

	function lists_approve_jurnal() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;
		$order_fields = array('', 'id', 'date');

		$search = $this->input->get_post('search');
		$coa = $this->input->get_post('coa');
		$type = $this->input->get_post('type');
		$valas = $this->input->get_post('valas');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;
		$params['coa'] = $coa;
		$params['type'] = $type;
		$params['valas'] = $valas;

		$list = $this->approve_jurnal_model->lists($params);

		// echo "<pre>";print_r($list);die;

		$status = 0;

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["coafilter"] = $this->approve_jurnal_model->coa("id_parent", 0);
		$result["valasfilter"] = $this->approve_jurnal_model->valas();
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$actions = '';
			if ($v['status'] == 0) {
				$actions = '<div class="btn-group">';
				$actions .= '   <button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Approve Jurnal" onClick="approve_jurnal(\'' . $v['id'] . '\')">';
				$actions .= '       <i class="fa fa-check"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';

				$lblstatus = '<div class="btn-group">';
				$lblstatus .= '   <label class="label label-warning">Need Approve</label>';
				$lblstatus .= '</div>';
			}

			if ($v['status'] == 1) {
				$lblstatus = '<div class="btn-group">';
				$lblstatus .= '   <label class="label label-success">Approved</label>';
				$lblstatus .= '</div>';
			}
			if ($v['status'] == 2) {
				$lblstatus = '<div class="btn-group">';
				$lblstatus .= '   <label class="label label-danger">Rejected</label>';
				$lblstatus .= '</div>';
			}

			array_push($data, array(
				// $v['id'],
				$i,
				$v['perkiraan'],
				$v['coa'],
				$v['keterangan'],
				$v['date'],
				$v['nama_valas'],
				$v['value'],
				$v['type'],
				$lblstatus,
				$actions
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function jurnal_update_status() {
		$data 	= file_get_contents("php://input");
		$params = json_decode($data, true);

		$params['userid'] = $this->session->userdata['logged_in']['user_id'];

		$list = $this->approve_jurnal_model->jurnal_id($params['id']);
		// var_dump($list,$params['id']);die;
		$list[0]['idtemp'] = $params['id'];
		if ($params['status'] ==1) $add_jurnal = $this->approve_jurnal_model->add_jurnal($list[0]);
		$add_jurnal = $this->approve_jurnal_model->update_status_temp($params);

		if ($add_jurnal > 0) {
			//$delete_jurnal = $this->approve_jurnal_model->delete_approve_jurnal($params['id']);
			$msg = 'Data Jurnal berhasil di approve';
			$title = 'Akan mengapprove data Jurnal ini ?';

			$results = array(
				'success' => true,
				'status' => 'success',
				'title' => $title,
				'message' => $msg
			);

			$this->log_activity->insert_activity('approve', $msg . ' dengan Status ' . $params['status']);
		} else {
			$msg = 'Data Jurnal Gagal di approve';

			$results = array(
				'success' => false,
				'status' => 'error',
				'message' => $msg
			);

			$this->log_activity->insert_activity('reject', $msg . ' dengan status ' . $params['status']);
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($results);
	}
}
