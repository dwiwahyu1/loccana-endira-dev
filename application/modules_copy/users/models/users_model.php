<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function group()
	{
		$sql 	= 'SELECT * FROM u_group;';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
    
    public function edit($id)
	{
		$sql 	= 'select a.id,
                            a.nama,
                            a.image,
                            a.email,
                            a.email_code,
                            a.created_by,
                            a.created_at,
							b.username,
							b.id_role
                    from u_user a
					left join u_user_group b on a.id = b.id_user
                    where a.id = '.$id;

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
    public function roles()
	{
		$sql 	= 'select *
                    from u_group';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
    public function dinas()
	{
		$sql 	= 'SELECT * FROM tbl_dinas';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	public function cek_level($data)
	{
		$sql 	= 'SELECT level FROM u_group WHERE level=? AND id_lokasi=?;';
		
		$query 	= $this->db->query($sql,
			array(
				$data['level'],
				$data['lokasi_id'],
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	public function lokasi()
	{
		$sql 	= 'SELECT * FROM t_lokasi;';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function check_userid($params = array())
	{
		$sql 	= 'SELECT COUNT(id) AS jumlah FROM u_user WHERE id = ? AND status_akses = 1;';

		$query 	= $this->db->query($sql,
			array(
				$params['user_id']
			));
		$return = $query->row_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function lists($params = array())
	{
		// print_r($params);die;
		$sql 	= 'CALL user_lists(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		//print_r($result);die;

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		//print_r($total);die;

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}
        
	public function lists_log($params = array())
	{
		$sql 	= 'CALL users_lists_log(? , ? , ? , ? , ? , @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}
        
	public function lists_ekalasi($params = array())
	{
		// print_r($params);die;
		$sql 	= 'CALL ekalasi_lists(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function list_users($params = array())
	{
		// print_r($params);die;
		$sql 	= 'CALL user_list_name(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function add_user($data)
	{
		$sql 	= 'CALL user_add(?,?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['user_id'],
				$data['picture'],
				$data['nama'],
				$data['username'],
				$data['email'],
				$data['group_id'],
				$data['password_hash'],
				$data['message']
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function create_username($data)
	{
		$sql 	= 'CALL user_create_username(?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['user_id'],
				$data['pemilik_id'],
				$data['username'],
				$data['group_id'],
				$data['password_hash'],
				$data['message']
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	public function create_ekalasi($data)
	{
		$sql 	= 'CALL user_create_eskalasi(?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['title'],
				$data['keterangan'],
				$data['lokasi_id'],
				$data['level'],
				$data['user_id']
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	public function edit_ekalasi($data)
	{
		$sql 	= 'CALL user_edit_eskalasi(?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['title'],
				$data['keterangan'],
				$data['lokasi_id'],
				$data['level'],
				$data['user_id'],
				$data['id']
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	public function detail_eskalasi($params = array())
	{
		$sql 	= 'SELECT * FROM u_group WHERE id = ? ';

		$query 	= $this->db->query($sql,
			array(
				$params['sid']
			));
		$return = $query->row_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	public function deleted_eskalasi($data)
	{
		$sql 	= 'CALL user_delete_eskalasi(?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['sid']
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	public function reset_password($data)
	{
		$sql 	= 'CALL user_reset_password(?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['username'],
				$data['password_hash'],
				$data['message']
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}


	/* GROUPS FUNCTION */

	public function listgroup($params = array()) {
		// print_r($params);die;
		$sql 	= 'CALL group_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();
		// print_r($result);die;

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();
		// print_r($total);die;

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function add_groups($data) {
		$sql 	= 'CALL group_add(?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['group'],
				$data['keterangan'],
				$data['id_lokasi'],
				$data['created_by']
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_groups($id) {
		$sql 	= 'CALL group_search_id(?)';

		$query 	=  $this->db->query($sql,
			array($id)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function save_edit_groups($data) {
		$sql 	= 'CALL group_update(?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['group_id'],
				$data['group'],
				$data['keterangan'],
				$data['id_lokasi'],
				$data['created_by']
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function delete_group($data) {
		$sql 	= 'CALL group_delete(?)';
		$query 	=  $this->db->query($sql,
			array($data)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}