<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Users extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('users/users_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function mail_service($result) {
		$name = 'Cashier App';
		$from = 'mail@google.com';
		$to = $result['mail_to']; //change this
		$subject = $result['mail_subject']; //change this

		$header = $result['mail_header']; //change this
		$body = $result['mail_body']; //change this
		$footer = $result['mail_footer']; //change this
		// Timpa isi email dengan data
		$a = array('xxheaderxx', 'xxbodyxx', 'xxfooterxx');
		$b = array($header, $body, $footer);

		$template = APPPATH . 'modules/template/email_default.html';
		$fd = fopen($template, "r");
		$message = fread($fd, filesize($template));
		fclose($fd);
		$message = str_replace($a, $b, $message);


		$this->load->library('email'); // load email library
		$this->email->from($from, $name);
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($message);
		if ($this->email->send()) return 1;
		else return 0;
	}

	public function index() {
		$this->template->load('maintemplate', 'users/views/index');
	}

	function lists() {

		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 7;

		$order_fields = array('', 'us.image', 'uug.username', 'ug.group', 'us.nama', 'us.email', 'tps.param_value', 'uug.created_at');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->users_model->lists($params);
		// print_r($list);die;

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$status_akses = '
				<div class="btn-group">
					<button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Reset password" disabled>
						<i class="fa fa-key"></i>
					</button>
				</div> 
				<div class="btn-group">
					<button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edituser(\'' . $v['user_id'] . '\')">
						<i class="fa fa-edit"></i>
					</button>
				</div>';
			if ($v['status_akses'] == 1 OR $v['status_akses'] == 2) {
				$status_akses = '
					<div class="btn-group">
						<button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Reset password" onClick="reset_username(\'' . $v['username'] . '\')">
							<i class="fa fa-key"></i>
						</button>
					</div> 
					<div class="btn-group">
						<button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edituser(\'' . $v['user_id'] . '\')">
							<i class="fa fa-edit"></i>
						</button>
					</div>';
			}
			if ($v['username'] == $username) {
				$status_akses = '
				<div class="btn-group">
					<button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Reset Password" disabled>
						<i class="fa fa-key"></i>
					</button>
				</div>
				<div class="btn-group">
					<button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edituser(\'' . $v['user_id'] . '\')">
						<i class="fa fa-edit"></i>
					</button>
				</div>';
			}
			array_push($data, array(
				$i,
				'<img src="' . base_url() . $v['picture'] . '" alt="picture" class="img-circle profile_img" style="width: 70px; height:70px">',
				$v['username'],
				ucwords($v['group']),
				ucwords($v['nama']),
				$v['email'],
				ucwords($v['status_name']),
				$v['created_at'],
				$status_akses
					)
			);
		}

		$result["data"] = $data;
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_username() {
		$this->template->load('maintemplate', 'users/views/add_username_view');
	}

	public function username_add() {
		$user_id = $this->uri->segment('3');

		$result_group = $this->users_model->group();

		$data = array(
			'user_id' => $user_id,
			'group' => $result_group
		);

		$result = $this->users_model->check_userid($data);

		if ($result['jumlah'] > 0) $this->load->view('add_modal_username_view', $data);
		else echo '<p style="text-align:center;">Data Not Found!</p>';
	}

	function list_users() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 4;

		$order_fields = array('', 'us.image', 'us.nama', 'us.email', 'us.created_at');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->users_model->list_users($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$status_akses = '<div class="btn-group"><button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Add username" onClick="add_username(' . $v['user_id'] . ',\'' . ucwords($v['nama']) . '\')"><i class="fa fa-user-plus"></i></button></div>';
			array_push($data, array(
				$i,
				'<img src="' . base_url() . $v['picture'] . '" alt="picture" class="img-circle profile_img">',
				ucwords($v['nama']),
				$v['email'],
				$v['created_at'],
				$status_akses
					)
			);
		}

		$result["data"] = $data;
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function reset_password() {
		$_POST = json_decode(file_get_contents("php://input"), true);
		$username = $this->input->get_post('sid');
		$newpassword = $this->input->get_post('newpassword');

		if ($username) {
			// $password_generated = substr(md5(mt_rand(100000, 999999)), 0, 8);
      $password_generated = $newpassword;
			$password_hash = password_hash($password_generated, PASSWORD_BCRYPT);
			$message = "Gunakan password anda yang baru dibawah ini, <br/>Username : " . $username . "<br/>Password : " . $password_generated;

			$data = array(
				'username' => $username,
				'password_hash' => $password_hash,
				'message' => base64_encode($message)
			);

			$result = $this->users_model->reset_password($data);

			if ($result > 0) {
				$msg = 'Berhasil mereset password, silahkan cek email untuk memvalidasi password.';
				$return = array('success' => TRUE, 'message' => $msg);
				$this->output->set_content_type('application/json')->set_output(json_encode($return));
			} else {
				$msg = 'Gagal mereset password!';
				$return = array('success' => FALSE, 'message' => $msg);
				$this->output->set_content_type('application/json')->set_output(json_encode($return));
			}
		} else {
			$msg = 'Username is empty.';
			$return = array('success' => FALSE, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}

	public function add() {
		$result = $this->users_model->group();

		$data = array(
			'group' => $result
		);

		$this->load->view('add_modal_view', $data);
	}
	public function edit($id) {
		$result = $this->users_model->edit($id);
		$roles = $this->users_model->roles($id);

		$data = array(
			'group' => $result,
			'roles' => $roles
		);

		$this->load->view('edit_modal_view', $data);
	}

	public function check_username() {
		$this->form_validation->set_rules('username', 'Username', 'trim|alpha_numeric|required|min_length[4]|max_length[100]|is_unique[u_user_group.username]');
		$this->form_validation->set_message('is_unique', 'Username Already Registered.');

		if ($this->form_validation->run() == FALSE) {

			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {

			$return = array('success' => true, 'message' => 'Username Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}

	public function check_email() {
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[u_user.email]');
		$this->form_validation->set_message('is_unique', 'Email Already Registered.');

		if ($this->form_validation->run() == FALSE) {

			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {

			$return = array('success' => true, 'message' => 'Email Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}

	public function check_level() {
		$lokasi_id = $this->Anti_sql_injection($this->input->post('lokasi_id', TRUE));
		$level = $this->Anti_sql_injection($this->input->post('level', TRUE));
		$data = array(
			'lokasi_id' => $lokasi_id,
			'level' => $level
		);
		$level = $this->users_model->cek_level($data);

		if ($level) {
			$return = array('success' => false, 'message' => 'Level sudah digunakan di lokasi ini');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else {
			$return = array('success' => true, 'message' => 'Level Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}

	public function add_user() {
		$this->form_validation->set_rules('nama', 'Nama', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric|min_length[4]|max_length[100]|is_unique[u_user_group.username]', array('is_unique' => 'This %s username already exists.'));
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[100]|is_unique[u_user.email]', array('is_unique' => 'This %s email already exists.'));
		$this->form_validation->set_rules('group_id', 'Group', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$upload_error = NULL;
			$picture = NULL;

			if ($_FILES['picture']['name']) {
				$this->load->library('upload');
				$config = array(
					'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/profile",
					'upload_url' => base_url() . "uploads/profile",
					'encrypt_name' => TRUE,
					'overwrite' => FALSE,
					'allowed_types' => 'jpg|jpeg|png',
					'max_size' => '10000'
				);

				$this->upload->initialize($config);

				if ($this->upload->do_upload("picture")) { // Success
					// General result data
					$result = $this->upload->data();

					// Load resize library
					$this->load->library('image_lib');

					// Resizing parameters large
					$resize = array
						(
						'source_image' => $result['full_path'],
						'new_image' => $result['full_path'],
						'maintain_ratio' => TRUE,
						'width' => 300,
						'height' => 300
					);

					// Do resize
					$this->image_lib->initialize($resize);
					$this->image_lib->resize();
					$this->image_lib->clear();

					// Add our stuff
					$picture = $result['file_name'];
				}else {
					$pesan = $this->upload->display_errors();
					$upload_error = strip_tags(str_replace("\n", '', $pesan));

					$result = array(
						'success' => false,
						'message' => $upload_error
					);
				}
			}

			if (!isset($upload_error)) {
				$user_id = $this->session->userdata['logged_in']['user_id'];
				$nama = strtolower($this->Anti_sql_injection($this->input->post('nama', TRUE)));
				$username = strtolower($this->Anti_sql_injection($this->input->post('username', TRUE)));
				$email = strtolower($this->input->post('email', TRUE));
				$group_id = $this->Anti_sql_injection($this->input->post('group_id', TRUE));
				$dinas_id = $this->Anti_sql_injection($this->input->post('dinas_id', TRUE));
				$password_generated = substr(md5(strtolower($this->Anti_sql_injection($this->input->post('password', TRUE)))), 0, 8);
			   // echo $this->input->post('password', TRUE);
			   // echo $password_generated; 
				$password_hash = password_hash($this->Anti_sql_injection($this->input->post('password', TRUE)), PASSWORD_BCRYPT);
				  // echo $password_hash; die;
				  
				 // if (password_verify($this->Anti_sql_injection($this->input->post('password', TRUE)), $password_hash)) {
					 // echo "berhasil";
				 // }else{
					 // echo "gagal";
				 // }
				 // die;
				$message = "Akun anda dibawah ini sudah terdaftar, <br/>Username : " . $username . "<br/>Password : " . $password_generated;

				$data = array(
					'user_id' => $user_id,
					'picture' => $picture,
					'nama' => $nama,
					'username' => $username,
					'email' => $email,
					'group_id' => $group_id,
					'dinas_id' => $dinas_id,
					'password_hash' => $password_hash,
					'message' => base64_encode($message)
				);
				// $data = $this->security->xss_clean($data);

				$result = $this->users_model->add_user($data);

				if ($result > 0) {
					$this->log_activity->insert_activity('insert', 'Insert User');
					$result = array('success' => true, 'message' => 'Berhasil menambahkan User ke database');
				} else {
					$this->log_activity->insert_activity('insert', 'Gagal Insert User');
					$result = array('success' => false, 'message' => 'Gagal menambahkan User ke database');
				}
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function create_username() {

		$this->form_validation->set_rules('sid', 'Pemilik', 'required');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric|min_length[4]|max_length[100]|is_unique[u_user_group.username]', array('is_unique' => 'This %s username already exists.'));
		$this->form_validation->set_rules('group_id', 'Group', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$user_id = $this->session->userdata['logged_in']['user_id'];
			$pemilik_id = $this->Anti_sql_injection($this->input->post('sid', TRUE));
			$username = strtolower($this->Anti_sql_injection($this->input->post('username', TRUE)));
			$group_id = $this->Anti_sql_injection($this->input->post('group_id', TRUE));
			$password_generated = substr(md5(mt_rand(100000, 999999)), 0, 8);
			$password_hash = password_hash($password_generated, PASSWORD_BCRYPT);
			$message = "Akun anda dibawah ini sudah terdaftar, <br/>Username : " . $username . "<br/>Password : " . $password_generated;

			$data = array(
				'user_id' => $user_id,
				'pemilik_id' => $pemilik_id,
				'username' => $username,
				'group_id' => $group_id,
				'password_hash' => $password_hash,
				'message' => base64_encode($message)
			);
			// print_r($data);die;

			$data = $this->security->xss_clean($data);
			$result = $this->users_model->create_username($data);

			if ($result > 0) {
				$this->log_activity->insert_activity('insert', 'Insert User');
				$result = array('success' => true, 'message' => 'Berhasil menambahkan User ke database');
			} else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert User');
				$result = array('success' => false, 'message' => 'Gagal menambahkan User ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function log() {
		$this->template->load('maintemplate', 'users/views/log_view');
	}
	
	public function eskalasi() {
		$this->template->load('maintemplate', 'users/views/eskalasi');
	}

	function lists_log() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'DESC';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 8;

		$order_fields = array('log_id', 'user_id', 'nama', 'group', 'activity_type', 'activity_desc', 'object_name',
			'object_url', 'activity_date');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : NULL;
 
		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->users_model->lists_log($params);
//        print_r($list);die;

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		foreach ($list['data'] as $k => $v) {
			array_push($data, array(
				$v['log_id'],
				$v['user_id'],
				$v['nama'],
				$v['group'],
				$v['activity_type'],
				$v['activity_desc'],
				$v['object_name'],
				$v['object_url'],
				$v['activity_date']
					)
			);
		}
		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	function lists_ekalasi() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 5;

		$order_fields = array('ug.id', 'ug.group', 'ug.keterangan', 'tl.namalokasi', 'ug.level', 'ug.id');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->users_model->lists_ekalasi($params);
		// print_r($list);die;

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			array_push($data, array(
				$i,
				$v['group'],
				$v['keterangan'],
				$v['namalokasi'],
				$v['level'],
				'<button type="button" class="btn btn-primary" data-toggle="tooltip" data-placement="top" data-origina-title="Edit Eskalasi" title="Edit Eskalasi" onClick="edit_param(' . $v['id'] . ')"><i class="fa fa-pencil-square-o"></i></button>&nbsp;<button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" data-origina-title="Delete!" title="Delete!" onClick="del_eskalasi(' . $v['id'] . ')"><i class="fa fa-trash"></i></button>'
					)
			);
		}

		$result["data"] = $data;
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_ekalasi_view() {
		$result = $this->users_model->lokasi();

		$data = array(
			'lokasi' => $result
		);

		$this->load->view('add_modal_eskalasi', $data);
	}

	public function add_eskalasi() {
		$lokasi_id = $this->Anti_sql_injection($this->input->post('lokasi_id', TRUE));
		$level = $this->Anti_sql_injection($this->input->post('level', TRUE));
		$data = array(
			'lokasi_id' => $lokasi_id,
			'level' => $level
		);
		$level = $this->users_model->cek_level($data);

		if ($level) {
			$return = array('success' => false, 'message' => 'Level sudah digunakan di lokasi ini');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else {
			$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
			$this->form_validation->set_rules('lokasi_id', 'Lokasi', 'required');
			$this->form_validation->set_rules('level', 'level', 'required');

			if ($this->form_validation->run() == FALSE) {

				$pesan = validation_errors();
				$msg = strip_tags(str_replace("\n", '', $pesan));

				$result = array(
					'success' => false,
					'message' => $msg
				);

				$this->output->set_content_type('application/json')->set_output(json_encode($result));
			} else {

				$user_id = $this->session->userdata['logged_in']['user_id'];
				$title = $this->Anti_sql_injection($this->input->post('title', TRUE));
				$keterangan = $this->Anti_sql_injection($this->input->post('keterangan', TRUE));
				$lokasi_id = $this->Anti_sql_injection($this->input->post('lokasi_id', TRUE));
				$level = $this->Anti_sql_injection($this->input->post('level', TRUE));


				$data = array(
					'user_id' => $user_id,
					'title' => $title,
					'keterangan' => $keterangan,
					'lokasi_id' => $lokasi_id,
					'level' => $level
				);


				$data = $this->security->xss_clean($data);

				$result = $this->users_model->create_ekalasi($data);

				if ($result > 0) {
					$this->log_activity->insert_activity('insert', 'Insert Eskalasi User');
					$result = array('success' => true, 'message' => 'Berhasil menambahkan Eskalasi User ke database');
				} else {
					$this->log_activity->insert_activity('insert', 'Gagal Insert Eskalasi User');
					$result = array('success' => false, 'message' => 'Gagal menambahkan Eskalasi User ke database');
				}

				$this->output->set_content_type('application/json')->set_output(json_encode($result));
			}
		}
	}

	public function edit_ekalasi_view($sid) {
		if($sid) {
			$data = array(
				'sid' => $sid
			);
			$result = $this->users_model->detail_eskalasi($data);

			if ($result) {
				$data['sid'] = $sid;
				$data['detail_eskalasi'] = $result;

				$data['lokasi'] = $this->users_model->lokasi();
				$this->load->view('editeskalasi_modal_view', $data);
			} else {
				echo '<h2 style="text-align:center;">Parameter tidak ditemukan!</h2>';
			}
		}else echo '<h2 style="text-align:center;">Tidak ada parameter!</h2>';
	}

	public function edit_eskalasi() {
		$lokasi_id = $this->Anti_sql_injection($this->input->post('lokasi_id', TRUE));
		$level = $this->Anti_sql_injection($this->input->post('level', TRUE));
		$data = array(
			'lokasi_id' => $lokasi_id,
			'level' => $level
		);
		$level = $this->users_model->cek_level($data);

		if ($level) {
			$return = array('success' => false, 'message' => 'Level sudah digunakan di lokasi ini');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else {
			$this->form_validation->set_rules('title', 'Title', 'required');
			$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
			$this->form_validation->set_rules('lokasi_id', 'Lokasi', 'required');
			$this->form_validation->set_rules('level', 'level', 'required');

			if ($this->form_validation->run() == FALSE) {
				$pesan = validation_errors();
				$msg = strip_tags(str_replace("\n", '', $pesan));

				$result = array(
					'success' => false,
					'message' => $msg
				);

				$this->output->set_content_type('application/json')->set_output(json_encode($result));
			}else {
				$user_id = $this->session->userdata['logged_in']['user_id'];
				$title = $this->Anti_sql_injection($this->input->post('title', TRUE));
				$id = $this->Anti_sql_injection($this->input->post('sid', TRUE));
				$keterangan = $this->Anti_sql_injection($this->input->post('keterangan', TRUE));
				$lokasi_id = $this->Anti_sql_injection($this->input->post('lokasi_id', TRUE));
				$level = $this->Anti_sql_injection($this->input->post('level', TRUE));


				$data = array(
					'id' => $id,
					'user_id' => $user_id,
					'title' => $title,
					'keterangan' => $keterangan,
					'lokasi_id' => $lokasi_id,
					'level' => $level
				);

				$data = $this->security->xss_clean($data);

				$result = $this->users_model->edit_ekalasi($data);

				if ($result > 0) {
					$this->log_activity->insert_activity('update', 'Update Eskalasi User id : '.$id);
					$result = array('success' => true, 'message' => 'Berhasil mengubah Eskalasi User ke database');
				} else {
					$this->log_activity->insert_activity('update', 'Gagal Update Eskalasi User id : '.$id);
					$result = array('success' => false, 'message' => 'Gagal mengubah Eskalasi User');
				}

				$this->output->set_content_type('application/json')->set_output(json_encode($result));
			}
		}
	}

	public function deleted_eskalasi() {
		$_POST = json_decode(file_get_contents("php://input"), true);
		$sid = $this->Anti_sql_injection($this->input->get_post('sid'));

		if ($sid) {
			$data = array(
				'sid' => $sid
			);

			$result = $this->users_model->deleted_eskalasi($data);

			if ($result > 0) {
				$this->log_activity->insert_activity('delete', 'Delete Eskalasi User id : '.$sid);
				$return = array('success' => true, 'message' => 'Berhasil menghapus Eskalasi User dari database');
				$this->output->set_content_type('application/json')->set_output(json_encode($return));
			} else {
				$this->log_activity->insert_activity('delete', 'Gagal Delete Eskalasi User id : '.$sid);
				$return = array('success' => false, 'message' => 'Gagal menghapus Eskalasi User dari database');
				$this->output->set_content_type('application/json')->set_output(json_encode($return));
			}
		} else {
			$return = array('success' => false, 'message' => 'Eskalasi Use is empty');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}


	/* FUNCTION GROUPS */
	public function groups() {
		$this->template->load('maintemplate', 'users/views/groups');
	}

	function listgroup() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 6;
		$order_fields = array('', 'group', 'keterangan', 'id_lokasi', 'level', 'created_date', 'created_by');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->users_model->listgroup($params);
		// print_r($list);die;

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$status_akses =
				'<div class="btn-group">'.
					'<button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
						onClick="edit_group(\'' . $v['id'] . '\')">'.
						'<i class="fa fa-edit"></i>'.
					'</button>'.
				'</div>'.
				'<div class="btn-group">'.
					'<button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete"
						onClick="deletegroup(\'' . $v['id'] . '\')">'.
						'<i class="fa fa-trash"></i>'.
					'</button>'.
				'</div>';

			array_push($data, array(
				$i,
				ucwords($v['group']),
				ucwords($v['keterangan']),
				date('d M Y H:i:s', strtotime($v['created_date'])),
				$status_akses
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_groups() {
		$this->load->view('add_modal_groups_view');
	}

	public function check_groupname(){
		$this->form_validation->set_rules('groupname', 'Group', 'trim|alpha_numeric|required|min_length[4]|max_length[100]|is_unique[u_group.group]');
		$this->form_validation->set_message('is_unique', 'Group Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'Groupname Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}

	public function save_group() {
		$this->form_validation->set_rules('groupname', 'Group', 'trim|alpha_numeric|required|min_length[4]|max_length[100]|is_unique[u_group.group]',array('is_unique' => 'This %s group already exists.'));
		$this->form_validation->set_rules('group_ket', 'Keterangan', 'trim|required|max_length[255]');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$user_id = $this->session->userdata['logged_in']['user_id'];
			$group = ucwords($this->Anti_sql_injection($this->input->post('groupname', TRUE)));
			$keterangan = ucwords($this->Anti_sql_injection($this->input->post('group_ket', TRUE)));

			$data = array(
				'group' => $group,
				'keterangan' => $keterangan,
				'id_lokasi' => '1',
				'created_by' => $user_id
			);

			$result = $this->users_model->add_groups($data);

			if ($result > 0) {
				$this->log_activity->insert_activity('insert', 'Insert Group');
				$result = array('success' => true, 'message' => 'Berhasil menambahkan Group ke database');
			} else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert Group');
				$result = array('success' => false, 'message' => 'Gagal menambahkan Group ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function edit_groups($id) {
		$result = $this->users_model->edit_groups($id);

		$data = array(
			'group' => $result
		);
		$this->load->view('edit_modal_groups_view', $data);
	}

	public function save_edit_group() {
		$this->form_validation->set_rules('groupname', 'Group', 'trim|alpha_numeric|required|min_length[4]|max_length[100]|is_unique[u_group.group]',array('is_unique' => 'This %s group already exists.'));
		$this->form_validation->set_rules('group_ket', 'Keterangan', 'trim|required|max_length[255]');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$user_id = $this->session->userdata['logged_in']['user_id'];
			$group_id = $this->Anti_sql_injection($this->input->post('group_id'));
			$group = ucwords($this->Anti_sql_injection($this->input->post('groupname', TRUE)));
			$keterangan = ucwords($this->Anti_sql_injection($this->input->post('group_ket', TRUE)));

			$data = array(
				'group_id' => $group_id,
				'group' => $group,
				'keterangan' => $keterangan,
				'id_lokasi' => '2',
				'created_by' => $user_id
			);

			$result = $this->users_model->save_edit_groups($data);

			if ($result > 0) {
				$this->log_activity->insert_activity('update', 'Update Group id : '.$group_id);
				$result = array('success' => true, 'message' => 'Berhasil mengubah Group ke database');
			} else {
				$this->log_activity->insert_activity('update', 'Gagal Update Group id : '.$group_id);
				$result = array('success' => false, 'message' => 'Gagal mengubah Group ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function delete_group() {
		$data 	= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		$result = $this->users_model->delete_group($params['id']);
		if($result > 0) {
			$this->log_activity->insert_activity('delete', 'Delete Group id : '.$params['id']);
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}else {
			$this->log_activity->insert_activity('delete', 'Gagal Delete Group id : '.$params['id']);
			$res = array('status' => 'success', 'message' => 'Data gagal di hapus');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}
}