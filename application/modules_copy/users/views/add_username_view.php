 <div class="row">
     <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Add Username</h2>

                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                      <div class="clearfix margin-bottom-10px">

                      </div>

                    <table id="listpemohon" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th width="80px">Picture</th>
                          <th>Pemilik</th>
                          <th>Email</th>
                          <th>Dibuat Tanggal</th>
                          <th>Option</th>
                        </tr>
                      </thead>
                      <tbody>
                        <td>No</td>
                        <td width="80px">Picture</td>
                        <td>Pemilik</td>
                        <td>Email</td>
                        <td>Dibuat Tanggal</td>
                        <td>Option</td>
                      </tbody>
                    </table>


                  </div>
                </div>
              </div>
            </div>

    <div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content p-0 b-0">
                <div class="panel panel-color panel-primary panel-filled">
                    <div class="panel-heading">
                        <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="panel-title"></h3>
                    </div>
                    <div class="panel-body">
                        <p></p>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

        <script type="text/javascript">

        function add_username(sid,sn){
            $('#panel-modal').removeData('bs.modal');
            $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
            $('#panel-modal  .panel-body').load('<?php echo base_url('users/username_add');?>/'+sid);
            $('#panel-modal  .panel-title').html('<i class="fa fa-user-plus"></i> Add username for '+sn);
            $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
        }

        $(document).ready(function(){

        	$("#listpemohon").dataTable({
        				"processing": true,
        				"serverSide": true,
        				"ajax": "<?php echo base_url().'users/list_users/';?>",
        				"searchDelay": 700,
        				"responsive": true,
        				"lengthChange": false,
        				"info": false,
                        "bSort": false
        	});


        });


        </script>
