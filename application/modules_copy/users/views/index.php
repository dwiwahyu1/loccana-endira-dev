<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Management Users</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">

                    <table id="listpemohon" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Picture</th>
                          <th>Username</th>
                          <th>Group</th>
                          <th>Nama</th>
                          <th>Email</th>
                          <th>Status Akses</th>
                          <th>Dibuat Tanggal</th>
                          <th>Option</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>

            </div>
        </div><!-- end col -->
    </div>
</div>


    <div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content p-0 b-0">
                <div class="panel panel-color panel-primary panel-filled">
                    <div class="panel-heading">
                        <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="panel-title"></h3>
                    </div>
                    <div class="panel-body">
                        <p></p>
                    </div>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

        <script type="text/javascript">

        function add_user(){
            $('#panel-modal').removeData('bs.modal');
            $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
            $('#panel-modal  .panel-body').load('<?php echo base_url('users/add');?>');
            $('#panel-modal  .panel-title').html('<i class="fa fa-user-plus"></i> Add user');
            $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
        }

        function edituser(id){
            $('#panel-modal').removeData('bs.modal');
            $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
            $('#panel-modal  .panel-body').load('<?php echo base_url('users/edit/');?>'+"/"+id);
            $('#panel-modal  .panel-title').html('<i class="fa fa-user"></i> Edit user');
            $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
        }

        function reset_username(sid){

            swal({
              title: 'Password Reset!',
              text: "Input Password Baru!",
              type: 'warning',
              input: 'text',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes!'
            }).then(function () {
                var form_data = {
                    sid : sid,
                    newpassword : $('.swal2-input').val()
                }

                $.ajax({
                    type	: "POST",
                    url		: "<?php echo base_url();?>" + "users/reset_password/",
                    data	: JSON.stringify(form_data),
					dataType	: 'json',
					contentType	: 'application/json; charset=utf-8',
                    success	: function(response) {
                        if (response.success) {
                            $("#listpemohon").DataTable().ajax.reload();
                            swal({
                                title	: "Success!",
                                text	: response.message,
                                type	: "success"
                            })
                        } else {
                            swal({
                                title	: "Error!",
                                text	: response.message,
                                type	: "error"
                            })
                        }
                    }
                }).fail(function(xhr, status, message) {
                    swal("Gagal!", "Invalid respon, silahkan cek koneksi.", "error");
                })
            });

    	}

        $(document).ready(function(){

        	$("#listpemohon").dataTable({
        				"processing": true,
        				"serverSide": true,
        				"ajax": "<?php echo base_url().'users/lists/';?>",
        				"searchDelay": 700,
        				"responsive": true,
        				"lengthChange": false,
        				"info": false,
                        "bSort": false,
                        "dom": 'l<"toolbar">frtip',
                        "initComplete": function(){
                           $("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-default" onClick="add_user()"><i class="fa fa-user-plus"></i> Add User</a></div>');
                        }
        	});


        });


        </script>
