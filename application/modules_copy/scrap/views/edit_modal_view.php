<style>
	#loading-us {
		display: none
	}

	#tick {
		display: none
	}

	#loading-mail {
		display: none
	}

	#cross {
		display: none
	}
</style>

<form class="form-horizontal form-label-left" id="edit_scrap" role="form" action="<?php echo base_url('scrap/edit_scrap'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_po">Nomor PO <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="id_po" id="id_po" style="width: 100%" required>
				<option value="" disabled='disabled' selected='selected'>-- Pilih PO --</option>
				<?php foreach ($po_customer_name as $key) { ?>
					<option value="<?php echo $key['id_po'] . "," . $key['name_eksternal']; ?>" <?php if ($stok[0]['id_po_quot'] == $key['id_po'])
																																													echo 'selected="selected"';
																																												?>><?php echo $key['no_po']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Customer <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" readonly id="nama_customer" name="nama_customer" class="form-control" placeholder="Nama Customer" autocomplete="off" required="required" value="<?php echo $stok[0]['name_eksternal']; ?>">
		</div>
	</div>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Kode Stok <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="text" id="kode_stok" name="kode_stok" class="form-control" placeholder="Kode Stok" value="<?php if (isset($stok[0]['stock_code'])) {
																																																																						echo $stok[0]['stock_code'];
																																																																					} ?>" autocomplete="off" required="required">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Stok <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="text" id="nama_stok" name="nama_stok" class="form-control" placeholder="Nama Stok" value="<?php if (isset($stok[0]['stock_name'])) {
																																																																						echo $stok[0]['stock_name'];
																																																																					} ?>" autocomplete="off" required="required">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Deskripsi Stok <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<textarea data-parsley-maxlength="255" id="desk_stok" name="desk_stok" class="form-control"><?php if (isset($stok[0]['stock_description'])) {
																																																		echo $stok[0]['stock_description'];
																																																	} ?></textarea>
		</div>
	</div>

	<input data-parsley-maxlength="255" type="hidden" id="type_material" name="type_material" class="form-control" placeholder="Harga" value="<?php echo $stok[0]['type']; ?>" autocomplete="off" readOnly>
	<!-- <div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_material">Tipe Stok <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="type_material" id="type_material" style="width: 100%" required>
				<option value="" selected="selected">-- Pilih Tipe Stok --</option>
			<?php foreach ($type_material as $key) { ?>
				<option value="<?php echo $key['id_type_material']; ?>" <?php if (isset($stok[0]['id_type_material'])) {
																																		if ($stok[0]['id_type_material'] == $key['id_type_material']) echo 'selected="selected"';
																																	} ?>><?php echo $key['type_material_name']; ?></option>
			<?php } ?>
			</select>
		</div>
	</div> -->

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_material">Unit <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="unit" id="unit" style="width: 100%" required>
				<option value="" selected='selected'>-- Pilih Unit --</option>
				<?php foreach ($unit as $unit) { ?>
					<option value="<?php echo $unit['id_uom']; ?>" <?php if (isset($stok[0]['unit'])) {
																															if ($stok[0]['unit'] == $unit['id_uom']) echo 'selected="selected"';
																														} ?>><?php echo $unit['uom_name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<input data-parsley-maxlength="255" type="hidden" id="valas" name="valas" class="form-control" placeholder="Harga" value="0" autocomplete="off" readOnly>
	<!-- <div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_material">Valas <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="valas" id="valas" style="width: 100%" required>
				<option value="" disabled='disabled' selected='selected' >-- Pilih Valas --</option>
			<?php foreach ($valas as $valas) { ?>
				<option value="<?php echo $valas['valas_id']; ?>" ><?php echo $valas['nama_valas']; ?></option>
			<?php } ?>
			</select>
		</div>
	</div> -->

	<input type="hidden" value="<?php if (isset($stok[0]['base_price'])) {
																echo (int) $stok[0]['base_price'];
															} ?>" id="harga" name="harga" class="form-control" placeholder="Harga" autocomplete="off" required="required">

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Quantity <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="number" id="qty" name="qty" class="form-control" placeholder="Quantity" value="<?php if (isset($stok[0]['base_qty'])) {
																																																		echo number_format($stok[0]['base_qty'], 4);
																																																	} ?>" step=".0001" autocomplete="off" required>
		</div>
	</div>

	<input type="hidden" value="<?php if (isset($stok[0]['weight'])) {
																echo (int) $stok[0]['weight'];
															} ?>" id="weight" name="weight" class="form-control" placeholder="Weight" autocomplete="off" required="required">

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_material">Gudang <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="gudang" id="gudang" style="width: 100%" required>
				<option value="" disabled="disabled">-- Pilih Gudang Penyimpanan --</option>
				<?php foreach ($Gudang as $Gudang) { ?>
					<option value="<?php echo $Gudang['id_gudang']; ?>" <?php if (isset($stok[0]['id_gudang'])) {
																																	if ($stok[0]['id_gudang'] == $Gudang['id_gudang']) echo 'selected="selected"';
																																} ?>><?php echo $Gudang['nama_gudang']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<!-- <div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_prop">Detail <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="detail" id="detail" style="width: 100%" required>
				<option value="" disabled='disabled' selected='selected' >-- Pilih Detail --</option>
			<?php foreach ($detail as $detail) { ?>
				<option value="<?php echo $detail['id_properties']; ?>" <?php if (isset($stok[0]['id_properties'])) {
																																		if ($stok[0]['id_properties'] == $detail['id_properties']) echo 'selected="selected"';
																																	} ?>><?php echo $detail['properties_name']; ?></option>
			<?php } ?>
			</select>
		</div>
	</div> -->

	<div id='detail_div'></div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Material</button>
			<input hidden type="text" id="id_material" name="id_material" value="<?php if (isset($stok[0]['id'])) {
																																							echo $stok[0]['id'];
																																						} ?>" required>
			<input hidden type="text" id="id_poscrap" name="id_poscrap" value="<?php if (isset($stok[0]['id_poscrap'])) {
																																							echo $stok[0]['id_poscrap'];
																																						} ?>" required>
		</div>
	</div>
</form><!-- /page content -->

<script type="text/javascript">
	$('#id_po').on('change', (function(e) {
		$('#nama_customer').val($('#id_po').val().split(",")[1]);
	}));
	$(document).ready(function() {
		// $('#type_material').select2();
		$('#unit').select2();

		/*$('#qty').on('keyup', function() {
			$(this).val($(this).val().replace(/[^0-9.]/g, ''));
		})*/
	});
	$('#detail').on('change', (function(e) {
		var formData = new FormData();
		formData.append('id', $('#detail').val());

		$.ajax({
			type: 'POST',
			url: '<?php echo base_url('material/get_properties'); ?>',
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					var data = response.data;
					var length_data = data.length;

					if (length_data > 0) {
						var form_add = '';
						for (var i = 0; i < length_data; i++) {
							form_add += ' ' + create_form_element(data[i].type_properties, data[i].detail_name, data[i].param_detail_name);
						}

						$('#detail_div').html(form_add);
					} else $('#detail_div').html('');
				}
			}
		});
	}));

	function setDetail() {
		// body...
	}

	function create_form_element(type, name, name_param) {
		var html = '';
		if (type == '1') {
			html += '<div class="item form-group">';
			html += '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">' + name + '</label>';
			html += '<div class="col-md-8 col-sm-6 col-xs-12">';
			html += '<input data-parsley-maxlength="255" type="text" id="' + name_param + '" name="' + name_param + '" class="form-control col-md-7 col-xs-12" placeholder="' + name + '" required="required">';
			html += '</div></div>';
		} else if (type == '2') {
			html += '<div class="item form-group">';
			html += '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">' + name + '</label>';
			html += '<div class="col-md-8 col-sm-6 col-xs-12">';
			html += '<input data-parsley-maxlength="255" type="text" id="' + name_param + '" name="' + name_param + '" class="form-control col-md-7 col-xs-12" placeholder="' + name + '" required="required">';
			html += '</div></div>';
		}
		return html;
	}

	$('#edit_scrap').on('submit', (function(e) {
		$('#btn-submit').attr('disabled', 'disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		var formData = new FormData(this);
		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function() {
						window.location.href = "<?php echo base_url('scrap'); ?>";
					})
				} else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Edit Scrap");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Edit Scrap");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}));
</script>