    <!--Parsley-->
    <script type="text/javascript" src="<?php echo base_url('assets'); ?>/gentelella-master/vendors/parsleyjs/dist/parsley.min.js"></script>

	<?php $message = $this->session->flashdata('msg'); if ($message): ?>
		<script>swal("<?php echo $message['status']; ?>", "<?php echo $message['message']; ?>", "<?php echo $message['status']; ?>")</script>
	<?php endif; ?>

	<style>
	#loading-us{display:none}
	#tick{display:none}

	#loading-mail{display:none}
	#cross{display:none}
	</style>

                    <form class="form-horizontal form-label-left" id="add_user" role="form" action="<?php echo base_url('users/create_username');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

                      <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="username">Username <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input data-parsley-type="alphanum" data-parsley-minlength="4" data-parsley-maxlength="100" type="text" id="username" name="username" class="form-control col-md-7 col-xs-12" placeholder="username untuk login minimal 4 karakter" required="required">
                          <input type="hidden" id="sid" name="sid" value="<?php echo $user_id;?>">
                          <span id="loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking username...</span>
						  <span id="tick"></span>
						</div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="group_id">Group <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
							<select class="form-control" name="group_id" id="group_id" style="width: 100%" required>
								<option value="" >-- Select Group User --</option>
								<?php foreach($group as $key) { ?>
								<option value="<?php echo $key['id']; ?>" ><?php echo $key['group']; ?></option>
								<?php } ?>
							</select>
                        </div>
                      </div>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Username</button>
                        </div>
                      </div>

                    </form>

        <!-- /page content -->

<script type="text/javascript">
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
	});

var lastusername = $('#username').val();

$('#username').on('input',function(event) {
   if($('#username').val() != lastusername) {
       username_check();
   }
});

function username_check(){
    var username = $('#username').val();
    if(username.length > 3) {
        var post_data = {
          'username': username
        };

    	$('#tick').empty();
    	$('#tick').hide();
    	$('#loading-us').show();
    	jQuery.ajax({
    	   type: "POST",
    	   url: "<?php echo base_url('users/check_username');?>",
    	   data: post_data,
    	   cache: false,
    	   success: function(response){
    			if(response.success == true){
    				$('#username').css('border', '3px #090 solid');
    				$('#loading-us').hide();
    				$('#tick').empty();
    				$("#tick").append('<span class="fa fa-check"> '+response.message+'</span>');
    				$('#tick').show();
    				}else{
    				$('#username').css('border', '3px #C33 solid');
    				$('#loading-us').hide();
    				$('#tick').empty();
    				$("#tick").append('<span class="fa fa-close"> '+response.message+'</span>');
    				$('#tick').show();
    			}
    		}
    	});
    } else {
        $('#username').css('border', '3px #C33 solid');
        $('#loading-us').hide();
        $('#tick').empty();
        $("#tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
        $('#tick').show();
    }
}

$('#add_user').on('submit',(function(e) {
    $('#btn-submit').attr('disabled','disabled');
    $('#btn-submit').text("Memasukkan data...");
    e.preventDefault();
    var formData = new FormData(this);

    $.ajax({
        type:'POST',
        url: $(this).attr('action'),
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(response) {
            if (response.success == true) {
                swal({
                  title: 'Success!',
                  text: response.message,
                  type: 'success',
                  showCancelButton: false,
                  confirmButtonText: 'Ok'
                }).then(function () {
                  window.location.href = "<?php echo base_url('users');?>";
                })
            } else{
                $('#btn-submit').removeAttr('disabled');
                $('#btn-submit').text("Tambah User");
                swal("Failed!", response.message, "error");
            }
        }
    }).fail(function(xhr, status, message) {
        $('#btn-submit').removeAttr('disabled');
        $('#btn-submit').text("Tambah User");
        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
    });
}));
</script>
