<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Scrap_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
    
	
	public function detail_prop_full($id)
	{
		$sql_all 	= 'CALL d_prop_search_id_prop(?)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				$id
			));

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
	public function detail_prop($params = array())
	{
		$sql_all 	= 'CALL prop_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				1000,
				0,
				NULL,
				NULL,
				''
			));

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	public function po_customer_name($params = array())
	{
		$sql_all 	= 'CALL po_customer_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				1000,
				0,
				NULL,
				NULL,
				''
			));

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}	
	public function type_scrap($params = array())
	{
		$sql_all 	= 'CALL type_mate_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				1000,
				0,
				NULL,
				NULL,
				''
			));

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}	
	
	public function gudang($params = array())
	{
		$sql_all 	= 'CALL gudang_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				1000,
				0,
				NULL,
				NULL,
				''
			));

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
	public function unit($params = array())
	{
		$sql_all 	= 'CALL uom_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				1000,
				0,
				NULL,
				NULL,
				''
			));

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
	public function valas($params = array())
	{
		$sql_all 	= 'CALL valas_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				1000,
				0,
				NULL,
				NULL,
				''
			));

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}
	
    public function edit($id) {
    	$sql = 'CALL poscrap_search_id(?)';

    	$query 	=  $this->db->query($sql, array(
    		$id
    	));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function lists($params = array()) {
		$sql 	= 'CALL po_scrap_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function add_scrap($data) {
		$sql 	= 'CALL material_add(?,?,?,?,?,?,?,?,?,?,?,?,?)';
		$query 	=  $this->db->query($sql, array(
			"",
			$data['kode_stok'],
			$data['nama_stok'],
			$data['desk_stok'],
			$data['unit'],
			$data['type_material'],
			$data['qty'],
			5, // tipe material pasti scrap
			$data['detail'],
			$data['gudang'],
			1,
			0,
			0
		));
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		
		//$id = $this->db->insert_id();
		//print_r($lastid);die;
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		
		return $arr_result;
	}

	public function jenis_material()
	{
		$sql 	= 'SELECT id_coa,id_parent,coa,keterangan FROM t_coa WHERE id_parent = 80300 order by coa';

		$query_all 	=  $this->db->query($sql);

		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}	
	public function new_add_material($data) {
		$sql 	= 'CALL material_add(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
		
		$query 	=  $this->db->query($sql,
			array(
				$data['no_bc'],
				$data['kode_stok'],
				$data['nama_stok'],
				$data['desk_stok'],
				$data['unit'],
				$data['type_material'],
				$data['qty'],
				$data['weight'],
				$data['treshold'],
				$data['detail'],
				$data['gudang'],
				$data['status'],
				$data['base_price'],
				$data['base_qty'],
        $data['user_id'],
				$data['jenis_material']
        // IN `pin_no_bc` VARCHAR(50),
        // IN `pin_stock_code` VARCHAR(50),
        // IN `pin_stock_name` VARCHAR(255),
        // IN `pin_stock_description` VARCHAR(255),
        // IN `pin_unit` INT(11),
        // IN `pin_type` INT(11),
        // IN `pin_qty` DECIMAL(20,10),
        // IN `pin_weight` DECIMAL(20,10),
        // IN `pin_treshold` INT(11),
        // IN `pin_id_properties` INT(11),
        // IN `pin_id_gudang` INT(11),
        // IN `pin_status` INT(11),
        // IN `pin_base_price` DECIMAL(20,4),
        // IN `pin_base_qty` DECIMAL(20,10),
        // IN `pin_cust_id` INT(11),
        // IN `pin_id_coa` INT(11)
        #OUT pout_id INT(11)
		));
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		
		return $arr_result;
	}
	public function new_add_scrap($data) {
		$sql 	= 'CALL poscrap_add(?,?)';
		
		// IN `pin_id_po_quot` INT(11),
		// IN `pin_id_material` INT(11)
		$query 	=  $this->db->query($sql,
			array(
				$data['id_po'],
				$data['id_material']
		));
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		
		return $arr_result;
	}
	public function get_komponen($id) {
		$sql 	= 'CALL get_komponen(?)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$id['detail']
			));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
		
	}

	public function edit_scrap($data) {
		$sql 	= 'CALL poscrap_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';
		
		$query 	=  $this->db->query($sql,
			array(
				// IN `pin_id` INT(10),
				// IN `pin_stock_code` VARCHAR(50),
				// IN `pin_stock_name` VARCHAR(255),
				// IN `pin_stock_description` VARCHAR(255),
				// IN `pin_unit` INT(11),
				// IN `pin_id_valas` INT(11),
				// IN `pin_price` DECIMAL(20,4),
				// IN `pin_id_gudang` INT(11),
				// IN `pin_type` INT(11),
				// IN `pin_qty` DECIMAL(20,10),
				// IN `pin_weight` DECIMAL(20,10),
				// IN `pin_treshold` INT(11),
				// IN `pin_id_properties` INT(11),
				// IN `pin_status` INT(11),
				// IN `pin_base_price` DECIMAL(20,4),
				// IN `pin_base_qty` DECIMAL(20,10),
				// IN `pin_cust_id` INT(11),
				// IN `pin_id_poscrap` INT(11),
				// IN `pin_id_po_quot` INT(11)
				$data['id_material'],
				$data['kode_stok'],
				$data['nama_stok'],
				$data['desk_stok'],
				$data['unit'],
				$data['valas'],
				$data['harga'],
				$data['gudang'],
				$data['type_material'],
				$data['qty'],
				$data['weight'],
				$data['treshold'],
				$data['id_properties'],
				$data['status'],
				$data['base_price'],
				$data['base_qty'],
				$data['user_id'],
				$data['id_poscrap'],
				$data['id_po']
		));
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function add_dvalues($id_last,$val_komp,$id_det_komp) {
		$sql 	= 'CALL dvalues_add(?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$id_last,
				$id_det_komp,
				$val_komp
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}	
	
	public function add_price($data,$id_komp)
	{
		$sql 	= 'CALL price_add(?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['detail'],
				$data['harga'],
				$data['valas'],
				$id_komp, 
				$data['harga_satuan']
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}	
	
	public function add_mutasi($data)
	{
		$sql 	= 'CALL mutasi_add(?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id_material'],
				$data['id_material'],
				$data['qty']
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function delete_scrap($id_poscrap,$id_material) {
		$sql 	= 'CALL poscrap_delete(?,?)';

		$query 	=  $this->db->query($sql, array(
			$id_poscrap,
			$id_material	
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}


}
