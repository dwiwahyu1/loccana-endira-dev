<style type="text/css">
	@media print {
		body {
			page-break-before: avoid;
			width:100%;
			height:100%;
			-webkit-transform: rotate(-90deg) scale(.68,.68); 
			-moz-transform:rotate(-90deg) scale(.58,.58);
			zoom: 200%;
		}
	}

	/*table, th, td {
		border: 1px solid black;
	}*/

	.bodyReport {
		padding: 5px 20px;
	}

	.reportBox {
		border: 1px solid black;
		page-break-before: always;
	}

	.reportTitle {
		margin-top: 20px;
		page-break-before: always;
	}

	.reportBodyBox {
		border-top: 0;
		border-left: 1px;
		border-right: 1px;
		border-bottom: 0;
		border-style: solid;
		border-color: black;
		page-break-before: always;
	}

	.titleReport {
		padding: 0;
		margin: 20px 0;
	}

	.separatorReport {
		padding: 0;
		margin: 10px 0;
	}

	.subHrTitle {
		border-top: 1px;
		border-left: 0;
		border-right: 0;
		border-bottom: 1px;
		border-style: solid;
	}

	.tableReport {
		box-sizing: border-box;
		text-align: left;
		width: 90%;
		margin: 10px auto;
		page-break-before: always;
	}

	.tableReport th, td {
		padding: 5px 0;
	}

	.tableReportTitle {
		box-sizing: border-box;
		text-align: left;
		width: 90%;
		margin: 5px auto;
		page-break-before: always;
	}

	.tableReportTitle th, td {
		padding: 5px 0;
	}

	.tableCellSumTOP {
		border-top: 1px;
		border-left: 0;
		border-right: 0;
		border-bottom: 0;
		border-style: solid;
		border-color: black;
	}

	.tableCellSumBottom {
		border-top: 0;
		border-left: 0;
		border-right: 0;
		border-bottom: 1px;
		border-style: solid;
		border-color: black;
	}

	.tableCellSumTopBottom {
		border-top: 1px;
		border-left: 0;
		border-right: 0;
		border-bottom: 1px;
		border-style: solid;
		border-color: black;
	}

	.labelTitle {
		margin-left: -20px;
		margin-bottom: 0 !important;
		padding: 0 !important;
	}

	.h4TitleNoMargin {
		margin-top: 0 !important;
		margin-bottom: 0 !important;
		padding: 0 !important;
		font-weight: 600;
	}
</style>

<div class="card-box">
	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" id="btn_download">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>

	<div class="bodyReport">
		<div class="row">
			<div class="col-md-12 text-center titleReport">
				<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
				<h3 id="titleJenisReport">Laporan Laba Rugi</h3>
				<h3 id="titleTanggal">Per, <?php if(isset($dateSearch)) echo $dateSearch; else echo date('d-M-Y'); ?></h3>
			</div>

			<div id="divBodyReport">
				<div class="col-md-12 text-center reportBox">
					<table class="tableReportTitle">
						<tr>
							<td align="center">
								<h4 class="h4TitleNoMargin">KETERANGAN</h4></td>
							<td style="width: 20%;" align="center">
								<h4 class="h4TitleNoMargin">%</h4></dt>
						</tr>
					</table>
				</div>
				<div class="col-md-12 text-center reportBodyBox">
					<table class="tableReport" id="t_penjualan">
						<thead>
							<tr>
								<th align="left" colspan="4">PENJUALAN</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$totalPenjualan = 0;
							foreach($penjualan as $kp) {
								$totalPenjualan = $totalPenjualan + $kp['total'];
						?>
							<tr>
								<td style="width: 5%;"></td>
								<td align="left"><?php echo $kp['name_field']; ?></td>
								<td style="width: 20%;" align="right">Rp. <?php echo number_format($kp['total'],2,",","."); ?></td>
								<td style="width: 20%;"></td>
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td style="width: 5%;"></td>
								<th align="left">PENJUALAN BERSIH</th>
								<td style="width: 20%;" class="tableCellSumTOP" align="right"></td>
								<th style="width: 20%;text-align: right;" align="right">Rp. <?php echo number_format($totalPenjualan,2,",","."); ?></th>
							</tr>
						</tfoot>
					</table>

					<?php
						$persedian_per 			= 0;
						$harga_pokok_produksi	= 0;
						$brg_siap_dijual		= 0;
						$saldo_persediaan_per	= 0;
						$harga_pokok_penjualan	= 0;
						$laba_rugi_kotor		= 0;
						if(isset($hargaPokokPenjualan)) {
							$persedian_per 			= $hargaPokokPenjualan['persedian_per'];
							$harga_pokok_produksi	= $hargaPokokPenjualan['hpp'];
							$brg_siap_dijual		= $persedian_per + $harga_pokok_produksi;
							$saldo_persediaan_per	= $hargaPokokPenjualan['saldo_persediaan_per'];
							$harga_pokok_penjualan	= $brg_siap_dijual - $saldo_persediaan_per;
							$laba_rugi_kotor		= $harga_pokok_penjualan - $totalPenjualan;
						}?>

					<table class="tableReport" id="t_hpp">
						<thead>
							<tr>
								<th align="left" colspan="4">HARGA POKOK PENJUALAN</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="width: 5%;"></td>
								<td align="left">Persediaan per, <?php if(isset($startDate)) echo $startDate; else echo date('d-M-Y'); ?></td>
								<td style="width: 20%;" align="right">Rp. <?php echo number_format($persedian_per,2,",","."); ?></td>
								<td style="width: 20%;" align="right"></td>
							</tr>
							<tr>
								<td style="width: 5%;"></td>
								<td align="left">Harga Pokok Produksi</td>
								<td style="width: 20%;" class="tableCellSumBottom" align="right">Rp. <?php echo number_format($harga_pokok_produksi,2,",","."); ?></td>
								<td style="width: 20%;" align="right"></td>
							</tr>
							<tr>
								<td style="width: 5%;"></td>
								<td align="left">Barang Siap untuk Dijual</td>
								<th style="width: 20%; text-align: right;" align="right">Rp. <?php echo number_format($brg_siap_dijual,2,",","."); ?></th>
								<td style="width: 20%;" align="right"></td>
							</tr>
							<tr>
								<td style="width: 5%;"></td>
								<td align="left">Saldo Persediaan per, <?php if(isset($dateSearch)) echo $dateSearch; else echo date('d-M-Y'); ?></td>
								<td style="width: 20%;" align="right">Rp. <?php echo number_format($saldo_persediaan_per,2,",","."); ?></td>
								<td style="width: 20%;" align="right"></td>
							</tr>
						</tbody>
						<tfoot>
							<tr>
								<td style="width: 5%;"></td>
								<th style="text-align: left;" align="left">HARGA POKOK PENJUALAN</th>
								<td style="width: 20%;" class="tableCellSumTOP" align="right"></td>
								<th style="width: 20%; text-align: right;" align="right">Rp. <?php echo number_format($harga_pokok_penjualan,2,",","."); ?></th>
							</tr>
							<tr>
								<td colspan="4"></td>
							</tr>
							<tr>
								<td style="width: 5%;"></td>
								<th style="text-align: left;" align="left">LABA (RUGI) KOTOR</th>
								<td style="width: 20%;" align="right"></td>
								<th style="width: 20%; text-align: right;" class="tableCellSumTOP"
									align="right">Rp. <?php echo number_format($laba_rugi_kotor,2,",","."); ?></th>
							</tr>
						</tfoot>
					</table>

					<table class="tableReport" id="t_bo">
						<thead>
							<tr>
								<th align="left" colspan="4">BIAYA OPERASIONAL</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$totalBO				= 0;
							$laba_rugi_operasional	= 0;
							foreach($bO as $kbo) {
								$totalBO = $totalBO + $kbo['total'];
						?>
							<tr>
								<td style="width: 5%;"></td>
								<td align="left"><?php echo $kbo['name_field']; ?></td>
								<td style="width: 20%;" align="right">Rp. <?php echo number_format($kbo['total'],2,",","."); ?></td>
								<td style="width: 20%;"></td>
							</tr>
						<?php }
							$laba_rugi_operasional = $laba_rugi_kotor - $totalBO;
						?>
						</tbody>
						<tfoot>
							<tr>
								<td style="width: 5%;"></td>
								<th style="text-align: left;" align="left">TOTAL BIAYA OPERASIONAL</th>
								<td style="width: 20%;" class="tableCellSumTOP" align="right"></td>
								<th style="width: 20%; text-align: right;" align="right">Rp. <?php echo number_format($totalBO,2,",","."); ?></th>
							</tr>
							<tr>
								<td colspan="4"></td>
							</tr>
							<tr>
								<td style="width: 5%;"></td>
								<th style="text-align: left;" align="left">LABA (RUGI) OPERASIONAL</th>
								<td style="width: 20%" align="right"></td>
								<th style="width: 20%; text-align: right;" class="tableCellSumTOP"
									align="right">Rp. <?php echo number_format($laba_rugi_operasional,2,",","."); ?></th>
							</tr>
						</tfoot>
					</table>

					<table class="tableReport" id="t_pll">
						<thead>
							<tr>
								<th align="left" colspan="4">PENDAPATAN (BEBAN) LAIN-LAIN</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$totalPLL = 0;
							foreach($pLL as $kpll) {
								$totalPLL = $totalPLL + $kpll['total'];
						?>
							<tr>
								<td style="width: 5%;"></td>
								<td align="left"><?php echo $kpll['name_field']; ?></td>
								<td style="width: 20%;" align="right">
									Rp. <?php echo number_format($kpll['total'],2,",","."); ?></td>
								<td style="width: 20%;"></td>
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td style="width: 5%;"></td>
								<th style="text-align: left;" align="left">TOTAL PENDAPATAN (BEBAN) LAIN-LAIN</th>
								<td style="width: 20%;" class="tableCellSumTOP" align="right"></td>
								<th style="width: 20%; text-align: right;" class="tableCellSumBottom"
									align="right">Rp. <?php echo number_format($totalPLL,2,",","."); ?></th>
							</tr>
						</tfoot>
					</table>

					<?php
						$laba_rugi_bsp	= $laba_rugi_operasional + $totalPLL;
						$pph_badan		= 0;
						$laba_bersih	= $laba_rugi_bsp - $pph_badan;
					?>

					<table class="tableReport" id="t_total">
						<tfoot>
							<tr>
								<th style="text-align: left;" align="left" colspan="3">LABA (RUGI) BERSIH SEBELUM PAJAK </th>
								<th style="width: 20%; text-align: right;" align="right">Rp. <?php echo number_format($laba_rugi_bsp,2,",","."); ?></th>
							</tr>
							<tr>
								<td style="width: 5%;"></td>
								<th style="text-align: left;" align="left">PPH BADAN </th>
								<td style="width: 20%;" align="right"></td>
								<th style="width: 20%; text-align: right;"
									align="right"><?php echo number_format($pph_badan,2,",","."); ?></th>
							</tr>
							<tr>
								<td style="width: 5%;"></td>
								<th style="text-align: left;" align="left">LABA BERSIH SETELAH PAJAK</th>
								<td style="width: 20%;" align="right"></td>
								<th style="width: 20%; text-align: right;" class="tableCellSumTopBottom"
									align="right">Rp. <?php echo number_format($laba_bersih,2,",","."); ?></th>
							</tr>
						</tfoot>
					</table>
				</div>
				<div class="col-md-12 text-center reportBox"></div>

			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){

		$('#btn_download').click(function () {
			var doc = new jsPDF('l');
			var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
			var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
			
			// FOOTER
			doc.setTextColor(100);
			doc.setFontSize(16);
			doc.text($('#titlePerusahaan').html(), pageWidth / 2, 20, 'center');
			doc.setFontSize(12);
			doc.text($('#titleJenisReport').html(), pageWidth / 2, 30, 'center');
			doc.setFontSize(14);
			doc.text($('#titleTanggal').html(), pageWidth / 2, 40, 'center');

			doc.setFontSize(12);
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_penjualan', 50));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_hpp', doc.autoTable.previous.finalY + 3));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_bo', doc.autoTable.previous.finalY + 3));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_pll', doc.autoTable.previous.finalY + 3));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_total', doc.autoTable.previous.finalY + 3));

			doc.save('Laba_Rugi.pdf');
		});
	});

	function objectSize(obj) {
		var size = 0;
		for (key in obj) {
			if (obj.hasOwnProperty(key)) size++;
		}
		return size;
	}

	function setAutoTable(typeTable, pageWidth, pageHeight, idTable, setStart) {
		var data = {};
		if(typeTable == 0) {
			data = {
				html 			: idTable,
				theme			: 'plain',
				styles			: {fontSize : 10},
				margin 			: 20,
				tableWidth		: (pageWidth - 40),
				headStyles		: {halign : 'left'},
				rowPageBreak	: 'auto',
				showHead 		: 'firstPage',
				showFoot		: 'lastPage',
				startY			: setStart
			};
		}else if(typeTable == 1) {
			data = {
				html 			: idTable,
				theme			: 'plain',
				styles			: {fontSize : 10},
				margin 			: 20,
				tableWidth		: (pageWidth - 40),
				headStyles		: {halign : 'left'},
				didParseCell	: function (data) {
					if(data.table.body.length > 0) {
						for (var i = 0; i < data.table.body.length; i++) {
							var objSize = objectSize(data.table.body[i].cells);
							if(objSize > 0) {
								if(data.table.body[i].cells[1]) data.table.body[i].cells[1].styles.halign		= 'left';
								if(data.table.body[i].cells[2]) data.table.body[i].cells[2].styles.halign		= 'right';
								if(data.table.body[i].cells[3]) data.table.body[i].cells[3].styles.halign		= 'right';
							}
						}
					}
					if(data.table.foot.length > 0) {
						for (var i = 0; i < data.table.foot.length; i++) {
							var objSize = objectSize(data.table.foot[i].cells);
							if(objSize > 0) {
								if(data.table.foot[i].cells[1]) data.table.foot[i].cells[1].styles.halign		= 'left';
								if(data.table.foot[i].cells[2]) data.table.foot[i].cells[2].styles.halign		= 'right';
								if(data.table.foot[i].cells[3]) data.table.foot[i].cells[3].styles.halign		= 'right';
							}
						}
					}
				},
				columnStyles	: {
					0: {halign: 'left'},
					1: {halign: 'left', cellWidth: 90},
					2: {halign: 'right', cellWidth: 40},
					3: {halign: 'right', cellWidth: 40}
				},
				rowPageBreak	: 'auto',
				showHead 		: 'firstPage',
				showFoot		: 'lastPage',
				startY			: setStart
			};
		}

		return data;
	}
</script>