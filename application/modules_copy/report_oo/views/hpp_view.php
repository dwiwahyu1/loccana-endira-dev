<style type="text/css">
	@media print {
		body {
			page-break-before: avoid;
			width:100%;
			height:100%;
			-webkit-transform: rotate(-90deg) scale(.68,.68); 
			-moz-transform:rotate(-90deg) scale(.58,.58);
			zoom: 200%;
		}
	}

	/*table, th, td {
		border: 1px solid black;
	}*/

	.bodyReport {
		padding: 5px 20px;
	}

	.reportBox {
		border: 1px solid black;
		page-break-before: always;
	}

	.reportTitle {
		margin-top: 20px;
		page-break-before: always;
	}

	.reportBodyBox {
		border-top: 0;
		border-left: 1px;
		border-right: 1px;
		border-bottom: 0;
		border-style: solid;
		border-color: black;
		page-break-before: always;
	}

	.reportBodyBoxNoTittle {
		margin-top: 20px;
		border-top: 1px;
		border-left: 1px;
		border-right: 1px;
		border-bottom: 0;
		border-style: solid;
		border-color: black;
		page-break-before: always;
	}

	.reportBodyBoxNoTittleWBottom {
		margin-top: 20px;
		border-top: 1px;
		border-left: 1px;
		border-right: 1px;
		border-bottom: 1px;
		border-style: solid;
		border-color: black;
		page-break-before: always;
	}

	.titleReport {
		padding: 0;
		margin: 20px 0;
	}

	.separatorReport {
		padding: 0;
		margin: 10px 0;
	}

	.subHrTitle {
		border-top: 1px;
		border-left: 0;
		border-right: 0;
		border-bottom: 1px;
		border-style: solid;
	}

	.tableReport {
		box-sizing: border-box;
		text-align: left;
		width: 90%;
		margin: 10px auto;
		page-break-before: always;
	}

	.tableReport th, td {
		padding: 5px 0;
	}

	.tableCellSumTOP {
		border-top: 1px;
		border-left: 0;
		border-right: 0;
		border-bottom: 0;
		border-style: solid;
		border-color: black;
	}

	.tableCellSumBottom {
		border-top: 0;
		border-left: 0;
		border-right: 0;
		border-bottom: 1px;
		border-style: solid;
		border-color: black;
	}

	.tableCellSumTopBottom {
		border-top: 1px;
		border-left: 0;
		border-right: 0;
		border-bottom: 1px;
		border-style: solid;
		border-color: black;
	}

	.labelTitle {
		margin-left: -20px;
		margin-bottom: 0 !important;
		padding: 0 !important;
	}

	.labelTitleNoMargin {
		margin-bottom: 0 !important;
		padding: 0 !important;
	}
</style>

<div class="card-box">
	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" id="btn_download">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>

	<div class="bodyReport">
		<div class="row">
			<div class="col-md-12 text-center titleReport">
				<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
				<h3 id="titleJenisReport">HARGA POKOK PRODUKSI</h3>
				<h3 id="titleTanggal">Per, <?php if(isset($dateSearch)) echo $dateSearch; else echo date('d-M-Y'); ?></h3>
			</div>

			<?php
				$persedian_per 			= 0;
				$pembelian 				= 0;
				$bahan_baku_tersedia	= 0;
				$saldo_persediaan_per	= 0;
				$pemakaian_bahan_baku	= 0;
				$awal_wip				= 0;
				$akhir_wip				= 0;
				if(isset($pemakaianBahanBaku[0])) {
					$persedian_per 			= $pemakaianBahanBaku[0]['persedian_per'];
					$pembelian 				= $pemakaianBahanBaku[0]['pembelian'];
					$bahan_baku_tersedia	= $persedian_per + $pembelian;
					$saldo_persediaan_per	= $pemakaianBahanBaku[0]['saldo_persediaan_per'];
					$pemakaian_bahan_baku	= $bahan_baku_tersedia - $saldo_persediaan_per;
					$awal_wip				= $pemakaianBahanBaku[0]['awal_wip'];
					$akhir_wip				= $pemakaianBahanBaku[0]['akhir_wip'];
				}?>

			<div id="divBodyReport">
				<div class="col-md-12 text-center reportBodyBoxNoTittleWBottom">
					<table class="tableReport" id="t_pemakaianBahanBaku">
						<tbody>
							<tr>
								<td style="width: 5%;"></td>
								<td align="left">Persediaan per, <?php if(isset($startDate)) echo $startDate; else echo date('d-M-Y'); ?></td>
								<td style="width: 20%;" align="right">Rp. <?php echo number_format($persedian_per,2,",","."); ?></td>
								<td style="width: 20%;" align="right"></td>
							</tr>
							<tr>
								<td></td>
								<td align="left">Pembelian</td>
								<td style="width: 20%;" class="tableCellSumBottom" align="right"><?php echo number_format($pembelian,2,",","."); ?></td>
								<td style="width: 20%;" align="right"></td>
							</tr>
							<tr>
								<td></td>
								<td align="left">Bahan baku yang tersedia untuk diproduksi</td>
								<td style="width: 20%;" align="right">Rp. <?php echo number_format($bahan_baku_tersedia,2,",","."); ?></td>
								<td style="width: 20%;" align="right"></td>
							</tr>
							<tr>
								<td></td>
								<td align="left">Saldo Persediaan per, <?php if(isset($dateSearch)) echo $dateSearch; else echo date('d-M-Y'); ?></td>
								<td style="width: 20%;" align="right">Rp. <?php echo number_format($saldo_persediaan_per,2,",","."); ?></td>
								<td style="width: 20%;" align="right"></td>
							</tr>
						</tbody>
						<tfoot>
							<tr>
								<td style="width: 5%;"></td>
								<th align="left">PEMAKAIAN BAHAN BAKU</th>
								<td style="width: 20%;" class="tableCellSumTOP"></td>
								<td style="width: 20%;" align="right">
									<label class="labelTitleNoMargin">Rp. <?php echo number_format($pemakaian_bahan_baku,2,",","."); ?></label></td>
							</tr>
						</tfoot>
					</table>

					<table class="tableReport" id="t_bTKL">
						<?php
						$totalBTKL = 0;
						foreach($bTKL as $kBTKL) {
							$totalBTKL = $totalBTKL + $kBTKL['total'];
						}
						?>
						<tr>
							<th style="width: 80%;">BIAYA TENAGA KERJA LANGSUNG</th>
							<td style="width: 20%;" align="right">
								<label class="labelTitleNoMargin">Rp. <?php echo number_format($totalBTKL,2,",","."); ?></label></td>
						</tr>
					</table>

					<table class="tableReport" id="t_bPTL">
						<thead>
							<tr>
								<th colspan="4" align="left">BIAYA PRODUKSI TIDAK LANGSUNG</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$totalBPTL = 0;
						foreach($bPTL as $kBPTL) {
							$totalBPTL = $totalBPTL + $kBPTL['total'];
						?>
							<tr>
								<td></td>
								<td><?php echo $kBPTL['name_field']; ?></td>
								<td style="width: 20%;" align="right">
									Rp. <?php echo number_format($kBPTL['total'],2,",","."); ?></td>
								<td style="width: 20%;"></td>
							</tr>
						<?php } ?>
							<tr>
								<td style="width: 5%;"></td>
								<th align="left">TOTAL BIAYA PRODUKSI TIDAK LANGSUNG</th>
								<td style="width: 20%;" class="tableCellSumTOP"></td>
								<td style="width: 20%;" align="right">
									<label class="labelTitleNoMargin">Rp. <?php echo number_format($totalBPTL,2,",","."); ?></label></td>
							</tr>
							<tr>
								<td style="width: 5%;"></td>
								<td align="left">Persediaan Awal  WIP</td>
								<td style="width: 20%;"></td>
								<td style="width: 20%;" align="right">Rp. <?php echo number_format($awal_wip,2,",","."); ?></td>
							</tr>
							<tr>
								<td style="width: 5%;"></td>
								<td align="left">Persediaan Akhir WIP</td>
								<td style="width: 20%;"></td>
								<td style="width: 20%;" align="right">Rp. <?php echo number_format($akhir_wip,2,",","."); ?></td>
							</tr>
						</tbody>
						<tfoot>
						<?php
							$totalHPP = 0;
							$totalHPP = $totalHPP + $pemakaian_bahan_baku + $totalBTKL + $totalBPTL + $awal_wip + $akhir_wip;
						?>
							<tr>
								<th align="left" colspan="3">HARGA POKOK PRODUKSI</th>
								<td style="width: 20%;" class="tableCellSumTopBottom" align="right">
										<label class="labelTitleNoMargin">Rp. <?php echo number_format($totalHPP,2,",","."); ?></label></td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#btn_download').click(function() {
			var doc = new jsPDF('l');
			var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
			var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
			
			doc.setTextColor(100);
			doc.setFontSize(16);
			doc.text($('#titlePerusahaan').html(), pageWidth / 2, 20, 'center');
			doc.setFontSize(12);
			doc.text($('#titleJenisReport').html(), pageWidth / 2, 30, 'center');
			doc.setFontSize(14);
			doc.text($('#titleTanggal').html(), pageWidth / 2, 40, 'center');

			doc.setFontSize(12);
			doc.autoTable(setAutoTable(2, pageWidth, pageHeight, '#t_pemakaianBahanBaku', 50));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_bTKL', doc.autoTable.previous.finalY + 3));
			doc.autoTable(setAutoTable(2, pageWidth, pageHeight, '#t_bPTL', doc.autoTable.previous.finalY + 3));

			doc.save('Harga_Pokok_Produksi.pdf');
		});
	});

	function objectSize(obj) {
		var size = 0;
		for (key in obj) {
			if (obj.hasOwnProperty(key)) size++;
		}
		return size;
	}

	function setAutoTable(typeTable, pageWidth, pageHeight, idTable, setStart) {
		var data = {};
		if(typeTable == 0) {
			data = {
				html 			: idTable,
				theme			: 'plain',
				styles			: {fontSize : 10},
				margin 			: 20,
				tableWidth		: (pageWidth - 40),
				headStyles		: {halign : 'left'},
				rowPageBreak	: 'auto',
				showHead 		: 'firstPage',
				showFoot		: 'lastPage',
				startY			: setStart
			};
		}else if(typeTable == 1) {
			data = {
				html 			: idTable,
				theme			: 'plain',
				styles			: {fontSize : 10},
				margin 			: 20,
				tableWidth		: (pageWidth - 40),
				headStyles		: {halign : 'left'},
				didParseCell	: function (data) {
					if(data.table.body.length > 0) {
						for (var i = 0; i < data.table.body.length; i++) {
							var objSize = objectSize(data.table.body[i].cells);
							if(objSize > 0) {
								if(data.table.body[i].cells[1]) data.table.body[i].cells[1].styles.halign		= 'right';
							}
						}
					}
					if(data.table.foot.length > 0) {
						for (var i = 0; i < data.table.foot.length; i++) {
							var objSize = objectSize(data.table.foot[i].cells);
							if(objSize > 0) {
								if(data.table.foot[i].cells[1]) data.table.foot[i].cells[1].styles.halign		= 'right';
							}
						}
					}
				},
				columnStyles	: {
					0: {halign: 'left'},
					1: {halign: 'right', cellWidth: 40}
				},
				rowPageBreak	: 'auto',
				showHead 		: 'firstPage',
				showFoot		: 'lastPage',
				startY			: setStart
			};
		}else if(typeTable == 2) {
			data = {
				html 			: idTable,
				theme			: 'plain',
				styles			: {fontSize : 10},
				margin 			: 20,
				tableWidth		: (pageWidth - 40),
				headStyles		: {halign : 'left'},
				didParseCell	: function (data) {
					if(data.table.body.length > 0) {
						for (var i = 0; i < data.table.body.length; i++) {
							var objSize = objectSize(data.table.body[i].cells);
							if(objSize > 0) {
								if(data.table.body[i].cells[1]) data.table.body[i].cells[1].styles.halign		= 'left';
								if(data.table.body[i].cells[2]) data.table.body[i].cells[2].styles.halign		= 'right';
								if(data.table.body[i].cells[3]) data.table.body[i].cells[3].styles.halign		= 'right';
							}
						}
					}
					if(data.table.foot.length > 0) {
						for (var i = 0; i < data.table.foot.length; i++) {
							var objSize = objectSize(data.table.foot[i].cells);
							if(objSize > 0) {
								if(data.table.foot[i].cells[1]) data.table.foot[i].cells[1].styles.halign		= 'left';
								if(data.table.foot[i].cells[2]) data.table.foot[i].cells[2].styles.halign		= 'right';
								if(data.table.foot[i].cells[3]) data.table.foot[i].cells[3].styles.halign		= 'right';
							}
						}
					}
				},
				columnStyles	: {
					0: {halign: 'left'},
					1: {halign: 'left', cellWidth: 90},
					2: {halign: 'right', cellWidth: 40},
					3: {halign: 'right', cellWidth: 40}
				},
				rowPageBreak	: 'auto',
				showHead 		: 'firstPage',
				showFoot		: 'lastPage',
				startY			: setStart
			};
		}

		return data;
	}
</script>