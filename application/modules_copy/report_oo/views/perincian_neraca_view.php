<style type="text/css">
	@media print {
		body {
			page-break-before: avoid;
			width:100%;
			height:100%;
			-webkit-transform: rotate(-90deg) scale(.68,.68); 
			-moz-transform:rotate(-90deg) scale(.58,.58);
			zoom: 200%;
		}
	}

	/*table, th, td {
		border: 1px solid black;
	}*/

	.bodyReport {
		padding: 5px 20px;
	}

	.reportBox {
		border: 1px solid black;
		page-break-before: always;
	}

	.reportTitle {
		margin-top: 20px;
		page-break-before: always;
	}

	.reportBodyBox {
		border-top: 0;
		border-left: 1px;
		border-right: 1px;
		border-bottom: 0;
		border-style: solid;
		border-color: black;
		page-break-before: always;
	}

	.titleReport {
		padding: 0;
		margin: 20px 0;
	}

	.separatorReport {
		padding: 0;
		margin: 10px 0;
	}

	.subHrTitle {
		border-top: 1px;
		border-left: 0;
		border-right: 0;
		border-bottom: 1px;
		border-style: solid;
	}

	.tableReport {
		box-sizing: border-box;
		text-align: left;
		width: 90%;
		margin: 10px auto;
		page-break-before: always;
	}

	.tableReport th, td {
		padding: 5px 0;
	}

	.tableCellSumTOP {
		border-top: 1px;
		border-left: 0;
		border-right: 0;
		border-bottom: 0;
		border-style: solid;
		border-color: black;
	}

	.tableCellSumBottom {
		border-top: 0;
		border-left: 0;
		border-right: 0;
		border-bottom: 1px;
		border-style: solid;
		border-color: black;
	}

	.tableCellSumTopBottom {
		border-top: 1px;
		border-left: 0;
		border-right: 0;
		border-bottom: 1px;
		border-style: solid;
		border-color: black;
	}

	.labelTitle {
		margin-left: -20px;
		margin-bottom: 0 !important;
		padding: 0 !important;
	}

	.labelTitleNoMargin {
		margin-bottom: 0 !important;
		padding: 0 !important;
	}
</style>

<div class="card-box">
	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" id="btn_download">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>

	<div class="bodyReport">
		<div class="row">
			<div class="col-md-12 text-center titleReport">
				<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
				<h3 id="titleJenisReport">Perincian Neraca</h3>
				<h3 id="titleTanggal">Per, <?php if(isset($dateSearch)) echo $dateSearch; else echo date('d-M-Y'); ?></h3>
			</div>

			<div id="divBodyReport">
				<div class="col-md-12 text-center reportBox">
					<h4 id="titleAktiva">AKTIVA</h4>
				</div>
				<div class="col-md-12 text-center reportBodyBox">
					<table class="tableReport" id="t_tittle_aktiva_lancar">
						<tr>
							<td colspan="3" align="left">
								<label class="labelTitle">AKTIVA LANCAR</label>
							</td>
						</tr>
					</table>
					<table class="tableReport" id="t_kas">
						<thead>
							<tr>
								<th colspan="3" align="left">KAS</th>
							</tr>
						</thead>
						<tbody>
						<?php 
						$totalKas = 0;
						foreach($kasDetail as $kd) {
							$totalKas = $totalKas + $kd['total'];
						?>
							<tr>
								<td style="width: 10%;"></td>
								<td><?php echo trim($kd['name_field']); ?></td>
								<td style="width: 20%;" align="right">
									Rp. <?php echo number_format(trim($kd['total']),2,",","."); ?></td>
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td style="width: 10%;"></td>
								<th>TOTAL KAS</th>
								<td style="width: 20%;" class="tableCellSumTOP" align="right">
									<label class="labelTitleNoMargin">Rp. <?php echo number_format(trim($totalKas),2,",","."); ?></label></td>
							</tr>
						</tfoot>
					</table>

					<table class="tableReport" id="t_bank">
						<thead>
							<tr>
								<th colspan="3" align="left">BANK</th>
							</tr>
						</thead>
						<tbody>
						<?php 
						$totalBank = 0;
						foreach($bankDetail as $bd) {
							$totalBank = $totalBank + $bd['total'];
						?>
							<tr>
								<td style="width: 10%;"></td>
								<td><?php echo trim($bd['name_field']); ?></td>
								<td style="width: 20%;" align="right">
									Rp. <?php echo number_format(trim($bd['total']),2,",","."); ?></td>
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td style="width: 10%;"></td>
								<th>TOTAL BANK</th>
								<td style="width: 20%;" class="tableCellSumTOP" align="right">
									<label class="labelTitleNoMargin">Rp. <?php echo number_format(trim($totalBank),2,",","."); ?></label></td>
							</tr>
						</tfoot>
					</table>

					<table class="tableReport" id="t_ar">
						<thead>
							<tr>
								<th colspan="3" align="left">ACCOUNT RECEIVABLE</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$totalAr = 0;
						foreach($arDetail as $ad) {
							$totalAr = $totalAr + $ad['total'];
						?>
							<tr>
								<td style="width: 10%;"></td>
								<td><?php echo trim($ad['name_field']); ?></td>
								<td style="width: 20%" align="right">
									Rp. <?php echo number_format(trim($ad['total']),2,",","."); ?></td>
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td style="width: 10%;"></td>
								<th>TOTAL ACCOUNT RECEIVABLE</th>
								<td style="width: 20%;" class="tableCellSumTOP" align="right">
									<label class="labelTitleNoMargin">Rp. <?php echo number_format(trim($totalAr),2,",","."); ?></label></td>
							</tr>
						</tfoot>
					</table>

					<table class="tableReport" id="t_pbd">
						<thead>
							<tr>
								<th colspan="3" align="left">PAJAK BAYAR DIMUKA</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$totalPbd = 0;
						foreach($pbdDetail as $pbd) {
							$totalPbd = $totalPbd + $pbd['total'];
						?>
							<tr>
								<td style="width: 10%;"></td>
								<td><?php echo trim($pbd['name_field']); ?></td>
								<td style="width: 20%" align="right">
									Rp. <?php echo number_format(trim($pbd['total']),2,",","."); ?></td>
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td style="width: 10%;"></td>
								<th>TOTAL PAJAK BAYAR DIMUKA</th>
								<td style="width: 20%;" class="tableCellSumTOP" align="right">
									<label class="labelTitleNoMargin">Rp. <?php echo number_format(trim($totalPbd),2,",","."); ?></label></td>
							</tr>
						</tfoot>
					</table>

					<table class="tableReport" id="t_inventory">
						<thead>
							<tr>
								<th colspan="3" align="left">INVENTORY</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$totalInventory = 0;
						foreach($inventory as $inv) {
							$totalInventory = $totalInventory + $inv['total'];
						?>
							<tr>
								<td style="width: 10%;"></td>
								<td><?php echo trim($inv['name_field']); ?></td>
								<td style="width: 20%" align="right">
									Rp. <?php echo number_format(trim($inv['total']),2,",","."); ?></td>
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td style="width: 10%;"></td>
								<th>TOTAL INVENTORY</th>
								<td style="width: 20%;" class="tableCellSumTOP" align="right">
									<label class="labelTitleNoMargin">Rp. <?php echo number_format(trim($totalInventory),2,",","."); ?></label></td>
							</tr>
						</tfoot>
					</table>

					<table class="tableReport" id="t_total_aktiva_lancar">
						<?php
							$totalAktivaLancar = $totalKas + $totalBank + $totalAr + $totalPbd + $totalInventory;
						?>
						<tr>
							<th colspan="2" align="left">TOTAL AKTIVA LANCAR</th>
							<td style="width: 20%;" class="tableCellSumTopBottom" align="right">
								<label class="labelTitleNoMargin">Rp. <?php echo number_format(trim($totalAktivaLancar),2,",","."); ?></label></td>
						</tr>
					</table>

					<table class="tableReport" id="t_tittle_aktiva_tetap">
						<tr>
							<td colspan="3" align="left">
								<label class="labelTitle">AKTIVA TETAP</label>
							</td>
						</tr>
					</table>

					<table class="tableReport" id="t_fa">
						<thead>
							<tr>
								<th colspan="3" align="left">FIXED ASSETS</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$totalFa = 0;
						foreach($fixedAsset as $fa) {
							$totalFa = $totalFa + $fa['total'];
						?>
							<tr>
								<td style="width: 10%;"></td>
								<td><?php echo trim($fa['name_field']); ?></td>
								<td style="width: 20%" align="right">
									Rp. <?php echo number_format(trim($fa['total']),2,",","."); ?></td>
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td style="width: 10%;"></td>
								<th>TOTAL FIXED ASSETS</th>
								<td style="width: 20%;" class="tableCellSumTOP" align="right">
									<label class="labelTitleNoMargin">Rp. <?php echo number_format(trim($totalFa),2,",","."); ?></label></td>
							</tr>
						</tfoot>
					</table>

					<table class="tableReport" id="t_total_aktiva_tetap">
						<?php
						$totalPenyusutan = 0;
						foreach($penyusutan as $kp) {
							$totalPenyusutan = $totalPenyusutan + $kp['total'];
						}
						?>
						<tr>
							<th colspan="2">AKUMULASI PENYUSUTAN</th>
							<td style="width: 20%;" align="right">
								Rp. <?php echo number_format(trim($totalPenyusutan),2,",","."); ?></td>
						</tr>
						<?php
							$totalAktivaTetap = $totalFa + $totalPenyusutan;
						?>
						<tr>
							<th colspan="2" align="left">TOTAL AKTIVA TETAP</th>
							<td style="width: 20%;" class="tableCellSumTopBottom" align="right">
								<label class="labelTitleNoMargin">Rp. <?php echo number_format(trim($totalAktivaTetap),2,",","."); ?></label></td>
						</tr>
					</table>
				</div>

				<div class="col-sm-12 text-center reportBox">
					<table class="tableReport" id="t_total_aktiva">
						<?php
							$totalAktiva = $totalAktivaLancar + $totalAktivaTetap;
						?>
						<tr>
							<td style="width: 10%;"></td>
							<td style="width: 70%;" align="center">
								<label>TOTAL AKTIVA</label></td>
							<td style="width: 20%;" align="right">
								<label class="labelTitleNoMargin">Rp. <?php echo number_format(trim($totalAktiva),2,",","."); ?></label></td>
						</tr>
					</table>
				</div>

				<div class="col-md-12 text-center reportBox reportTitle">
					<h4 id="titlePasiva">PASIVA</h4>
				</div>
				<div class="col-md-12 text-center reportBodyBox">
					<table class="tableReport" id="t_hutang_dagang">
						<thead>
							<tr>
								<th colspan="3" align="left">HUTANG DAGANG</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$totalHd = 0;
						foreach($hutangDagang as $hd) {
							$totalHd = $totalHd + $hd['total'];
						?>
							<tr>
								<td style="width: 10%;"></td>
								<td><?php echo trim($hd['name_field']); ?></td>
								<td style="width: 20%;" align="right">
									Rp. <?php echo number_format(trim($hd['total']),2,",","."); ?></td>
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td style="width: 10%;"></td>
								<th>TOTAL HUTANG DAGANG</th>
								<td style="width: 20%;" class="tableCellSumTOP" align="right">
									<label class="labelTitleNoMargin">Rp. <?php echo number_format(trim($totalHd),2,",","."); ?></label></td>
							</tr>
						</tfoot>
					</table>

					<table class="tableReport" id="t_hutang_lain">
						<thead>
							<tr>
								<th colspan="3" align="left">HUTANG LAIN_LAIN</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$totalHl = 0;
						foreach($hutangLain as $hl) {
							$totalHl = $totalHl + $hl['total'];
						?>
							<tr>
								<td style="width: 10%;"></td>
								<td><?php echo trim($hl['name_field']); ?></td>
								<td style="width: 20%;" align="right">
									Rp. <?php echo number_format(trim($hl['total']),2,",","."); ?></td>
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td style="width: 10%;"></td>
								<th>TOTAL HUTANG LAIN-LAIN</th>
								<td style="width: 20%;" class="tableCellSumTOP" align="right">
									<label class="labelTitleNoMargin">Rp. <?php echo number_format(trim($totalHl),2,",","."); ?></label></td>
							</tr>
						</tfoot>
					</table>

					<table class="tableReport" id="t_total_huutang">
						<?php 
							$totalHutang = $totalHd + $totalHl;

						?>
						<tr>
							<th colspan="2" align="left">TOTAL HUTANG</th>
							<td style="width: 20%;" class="tableCellSumTopBottom" align="right">
								<label class="labelTitleNoMargin">Rp. <?php echo number_format(trim($totalHutang),2,",","."); ?></label></td>
						</tr>
					</table>

					<table class="tableReport" id="t_modal">
						<thead>
							<tr>
								<th colspan="3" align="left">MODAL</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$totalModal = 0;
						foreach($modal as $md) {
							$totalModal = $totalModal + $md['total'];
						?>
							<tr>
								<td style="width: 10%;"></td>
								<td><?php echo trim($md['name_field']); ?></td>
								<td style="width: 20%" align="right">
									Rp. <?php echo number_format(trim($md['total']),2,",","."); ?></td>
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td style="width: 10%;"></td>
								<th>TOTAL MODAL</th>
								<td style="width: 20%;" class="tableCellSumTOP" align="right">
									<label class="labelTitleNoMargin">Rp. <?php echo number_format(trim($totalModal),2,",","."); ?></label></td>
							</tr>
						</tfoot>
					</table>
				</div>

				<div class="col-sm-12 text-center reportBox">
					<table class="tableReport" id="t_total_pasiva">
						<?php
							$totalPasiva = $totalHutang + $totalModal;
						?>
						<tr>
							<td style="width: 10%;"></td>
							<td style="width: 70%;" align="center">
								<label>TOTAL PASIVA</label></td>
							<td style="width: 20%;" align="right">
								<label class="labelTitleNoMargin">Rp. <?php echo number_format(trim($totalPasiva),2,",","."); ?></label></td>
						</tr>
					</table>
				</div>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('#btn_download').click(function() {
			var doc = new jsPDF();
			var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
			var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
			
			doc.setTextColor(100);
			doc.setFontSize(16);
			doc.text($('#titlePerusahaan').html(), pageWidth / 2, 20, 'center');
			doc.setFontSize(12);
			doc.text($('#titleJenisReport').html(), pageWidth / 2, 30, 'center');
			doc.setFontSize(14);
			doc.text($('#titleTanggal').html(), pageWidth / 2, 40, 'center');

			doc.setFontSize(12);
			doc.text($('#titleAktiva').html(), pageWidth / 2, 55, 'center');
			doc.autoTable(setAutoTable(0, pageWidth, pageHeight, '#t_tittle_aktiva_lancar', 60));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_kas', doc.autoTable.previous.finalY + 3));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_bank', doc.autoTable.previous.finalY + 3));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_ar', doc.autoTable.previous.finalY + 3));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_pbd', doc.autoTable.previous.finalY + 3));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_inventory', doc.autoTable.previous.finalY + 3));
			
			doc.autoTable(setAutoTable(0, pageWidth, pageHeight, '#t_tittle_aktiva_tetap', doc.autoTable.previous.finalY + 3));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_fa', doc.autoTable.previous.finalY + 3));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_total_aktiva_tetap', doc.autoTable.previous.finalY + 3));

			doc.text($('#titlePasiva').html(), pageWidth / 2, doc.autoTable.previous.finalY + 10, 'center');
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_hutang_dagang', doc.autoTable.previous.finalY + 15));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_hutang_lain', doc.autoTable.previous.finalY + 3));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_modal', doc.autoTable.previous.finalY + 3));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_total_pasiva', doc.autoTable.previous.finalY + 3));

			doc.save('Report_Perincian_Neraca.pdf');
		});
	});

	function objectSize(obj) {
		var size = 0;
		for (key in obj) {
			if (obj.hasOwnProperty(key)) size++;
		}
		return size;
	}

	function setAutoTable(typeTable, pageWidth, pageHeight, idTable, setStart) {
		var data = {};
		if(typeTable == 0) {
			data = {
				html 			: idTable,
				theme			: 'plain',
				styles			: {fontSize : 10, cellWidth: 50},
				margin 			: 20,
				tableWidth		: (pageWidth - 40),
				headStyles		: {halign : 'left'},
				rowPageBreak	: 'auto',
				showHead 		: 'firstPage',
				showFoot		: 'lastPage',
				startY			: setStart
			};
		}else if(typeTable == 1) {
			data = {
				html 			: idTable,
				theme			: 'plain',
				styles			: {fontSize : 10, cellWidth: 50},
				margin 			: 20,
				tableWidth		: (pageWidth - 40),
				headStyles		: {halign : 'left'},
				didParseCell	: function (data) {
					if(data.table.foot.length > 0) {
						for (var i = 0; i < data.table.foot.length; i++) {
							var objSize = objectSize(data.table.foot[i].cells);
							if(objSize > 0) {
								if(data.table.foot[i].cells[2]) data.table.foot[i].cells[2].styles.halign = 'right';
							}
						}
					}
				},
				columnStyles	: {
					0: {halign: 'left'},
					1: {halign: 'left'},
					2: {halign: 'right'}
				},
				rowPageBreak	: 'auto',
				showHead 		: 'firstPage',
				showFoot		: 'lastPage',
				startY			: setStart
			};
		}

		return data;
	}
</script>