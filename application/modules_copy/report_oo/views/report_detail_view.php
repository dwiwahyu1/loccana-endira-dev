<style type="text/css">
	.row-search{
		padding: 5px;
	}
	#loading{
		position: absolute;
		background: #FFFFFF;
		opacity: 0.5;
		width: 93%;
		height: 80%;
		z-index: 3;
		text-align: center;
		display:none;
	}
</style>
<div id="loading"><img src="<?php echo base_url();?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/preloader.gif"/></div>
<div class="row row-search">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nopengajuan">Jenis Report</label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<select class="form-control" id="jenis_report" name="jenis_report" required>
			<option value="">-- Select Jenis --</option>
			<option value="nrc">Neraca</option>
			<option value="prn_nrc">Perincian Neraca</option>
			<option value="hpp">Harga Pokok Produksi</option>
			<option value="rl">Laba Rugi</option>
			<option value="prn_rl">Perincian Laba Rugi</option> 
			<option value="gl">General Ledger</option>
			<option value="bbk">Buku Bank / Kas</option>
		</select>
	</div>
</div>

<div class="row row-search" id="inputCOA">
	<div class="spinner-border spinner-border-sm" role="status">
		<span class="sr-only">Loading...</span>
	</div>
</div>

<div class="row row-search">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tgl_report">Tanggal</label>
	<div class="col-md-8 col-sm-6 col-xs-12" id="divDateSearch">
		<input type="text" class="form-control" id="date_search" name="date_search" required>
	</div>
</div>

<div class="row row-search">
	<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<button id="btn-search" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Cari</button>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
		jQuery('#date_search').datepicker({
			format: "dd/M/yyyy",
			autoclose: true,
			toggleActive: true
		}).focus(function() {
			$(this).prop("autocomplete", "off");
		});

		jQuery('#jenis_report').on('change', function() {
			var tanggalStr = '';
			$('#divDateSearch').html('');
			if(this.value != 'hpp' && this.value != 'rl' && this.value != 'gl' && this.value != 'bbk') {
				setLedger('', 0);
				tanggalStr = '<input type="text" class="form-control" id="date_search" name="date_search" required>';
				$('#divDateSearch').html(tanggalStr);
				jQuery('#date_search').datepicker({
					format: "dd/M/yyyy",
					autoclose: true,
					toggleActive: true
				}).focus(function() {
					$(this).prop("autocomplete", "off");
				});
			}else {
				if(this.value != 'gl' && this.value != 'bbk') setLedger('', 0);
				else if(this.value == 'gl') setLedger('', 1);
				else if(this.value == 'bbk') setBBK('', 1);

				tanggalStr = '<div class="input-daterange input-group" id="date-range">'+
					'<input type="text" class="form-control" id="start_date" name="start_date" autocomplete="off">'+
					'<span class="input-group-addon bg-primary b-0 text-white">to</span>'+
					'<input type="text" class="form-control" id="end_date" name="end_date" autocomplete="off">'+
				'</div>';
				$('#divDateSearch').html(tanggalStr);
				jQuery('#date-range').datepicker({
					format: "dd/M/yyyy",
					toggleActive: true
				}).focus(function() {
					$(this).prop("autocomplete", "off");
				});
			}
		});

		$('#btn-search').on('click', function () {
			var jenisReport = $('#jenis_report').val();
			$('#btn-search').attr('disabled','disabled');
			$('#btn-search').text("Loading...");
			$('#div_report').removeData('');
			$('#div_report').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');

			if(jenisReport != '') {
				if($('#date_search').length > 0) {
					if($('#date_search').val() != '') sendData(1);
					else searchFailed(0, null);
				}else if($('#start_date').length > 0 && $('#end_date').length > 0) {
					if($('#start_date').val() != '' && $('#end_date').val() != '') {
						if(jenisReport == 'gl') sendData(3);
						else if(jenisReport == 'bbk') sendData(4);
						else sendData(2);
					}else searchFailed(0, null);
				}
			}else searchFailed(0, null);
		})
	});

	function setLedger(id, val) {
		var strHtml = '';
		if (val == 0) $('#inputCOA').html(strHtml);
		else if (val == 1) {
			$('#loading').show();
			getDataCOA(id, val);
			strHtml =
				'<label class="control-label col-md-3 col-sm-3 col-xs-12" for="coa_lv1">COA</label>'+
				'<div class="col-md-8 col-sm-6 col-xs-12">'+
					'<select class="form-control" id="coa_lv1" name="coa_lv1" required>'+
						'<option value="">-- Select COA --</option>'+
					'</select>'+
				'</div>'+
				'<div id="divCoa_lv2"></div>';
			$('#inputCOA').html(strHtml);
			$('#coa_lv1').select2();
		}else if (val == 2) {
			$('#loading').show();
			getDataCOA(id, val);
			strHtml =
				'<div class="col-md-3 col-sm-3 col-xs-12"></div>'+
				'<div style="padding-top: 10px;" class="col-md-8 col-sm-6 col-xs-12">'+
					'<select class="form-control" id="coa_lv2" name="coa_lv2" required>'+
						'<option value="">-- Semua COA --</option>'+
					'</select>'+
				'</div>'+
				'<div id="divCoa_lv3"></div>';
			$('#divCoa_lv2').html(strHtml);
			$('#coa_lv2').select2();
		}else if (val == 3) {
			$('#loading').show();
			getDataCOA(id, val);
			strHtml =
				'<div class="col-md-3 col-sm-3 col-xs-12"></div>'+
				'<div style="padding-top: 10px;" class="col-md-8 col-sm-6 col-xs-12">'+
					'<select class="form-control" id="coa_lv3" name="coa_lv3" required>'+
						'<option value="">-- Semua COA --</option>'+
					'</select>'+
				'</div>';
			$('#divCoa_lv3').html(strHtml);
			$('#coa_lv3').select2();
		}
	}

	function getDataCOA(id, level) {
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>report/getDataCOA",
			data : JSON.stringify({"id" : id, "level" : level }),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				if (response.status == "success") {
					var data = response.coa;
					if(level == 1) {
						var strId = '#coa_lv1';
						jQuery('#coa_lv1').on('change', function() {
							if(this.value != '') setLedger(this.value, 2);
							else $('#divCoa_lv2').html('');
						});
					}else if(level == 2) {
						var strId = '#coa_lv2';
						jQuery('#coa_lv2').on('change', function() {
							if(this.value != '') setLedger(this.value, 3);
							else $('#divCoa_lv3').html('');
						});
					}else if(level == 3) {
						var strId = '#coa_lv3';
						jQuery('#coa_lv3').on('change', function() {
							if(this.value != '') setLedger(this.value, 4);
						});
					}
					for (var i = 0; i < data.length; i++) {
						$(strId).append(new Option(data[i].keterangan, data[i].id_coa));
					}
					$('#loading').hide();
				}
			}
		});
	}

	function setBBK(id, val) {
		var strHtml = '';
		if (val == 0) $('#inputCOA').html(strHtml);
		else if (val == 1) {
			$('#loading').show();
			getDataBBK(id, val);
			strHtml =
				'<label class="control-label col-md-3 col-sm-3 col-xs-12" for="bbk_coa">COA</label>'+
				'<div class="col-md-8 col-sm-6 col-xs-12">'+
					'<label class="control-label">Bank</label>'+
					'<select class="form-control" id="bbk_coa" name="bbk_coa" required>'+
						'<option value="">-- Select COA --</option>'+
					'</select>'+
				'</div>'+
				'<div id="divCoa"></div>';
			$('#inputCOA').html(strHtml);
		}
	}

	function getDataBBK(id, level) {
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>report/getDataBBK",
			data : JSON.stringify({"id" : id, "level" : level }),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				if (response.status == "success") {
					var data = response.coa;
					for (var i = 0; i < data.length; i++) {
						$('#bbk_coa').append(new Option(data[i].keterangan, data[i].id_coa));
					}
					$('#loading').hide();
				}
			}
		});
	}

	function sendData(val) {
		var data = {
			'jenis_report'	: $('#jenis_report').val()
		};
		if(val == 1) {
			data.date_search = $('#date_search').val();
			searchFailed(1, data);
		}else if(val == 2) {
			data.start_date = $('#start_date').val();
			data.end_date = $('#end_date').val();
			searchFailed(1, data);
		}else if(val == 3) {
			data.start_date = $('#start_date').val();
			data.end_date = $('#end_date').val();

			if($('#coa_lv1').length > 0 && $('#coa_lv1').val() != '') data.coa = $('#coa_lv1').val();
			if($('#coa_lv2').length > 0 && $('#coa_lv2').val() != '') data.coa = $('#coa_lv2').val();
			if($('#coa_lv3').length > 0 && $('#coa_lv3').val() != '') data.coa = $('#coa_lv3').val();
			searchFailed(1, data);
		}else if(val == 4) {
			data.start_date = $('#start_date').val();
			data.end_date = $('#end_date').val();

			if($('#bbk_coa').length > 0 && $('#bbk_coa').val() != '') data.coa = $('#bbk_coa').val();
			searchFailed(1, data);
		}else searchFailed(0, null);
	}

	function searchFailed(val, data) {
		if(val == 1) {
			$('#div_report').load('<?php echo base_url('report/view_neraca/');?>', data, function(response, status, xhr) {
				if (status != "error") {
					$('#btn-search').removeAttr('disabled');
					$('#btn-search').text("Cari");
					$('#panel-modal').modal('toggle');
				}else {
					$('#btn-search').removeAttr('disabled');
					$('#btn-search').text("Cari");
					$('#div_report').removeData('');
					$('#div_report').html('');
					swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
				}
			});
		}else {
			$('#btn-search').removeAttr('disabled');
			$('#btn-search').text("Cari");
			$('#div_report').removeData('');
			$('#div_report').html('');
			swal("Failed!", "Invalid inputan, silahkan cek kembali.", "error");
		}
	}
</script>