<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Report extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('report/Report_model');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'report/views/index');

		// $startDate	= '01/Aug/2019';
		// $endDate	= '31/Aug/2019';

		// $start_date = $this->setFormatDate(0, $startDate);
		// $end_date = $this->setFormatDate(0, $endDate);
		// $coa = 9;

		// $data = array(
		// 	'dataCoa'		=> $this->Report_model->getIdBBK($coa),
		// 	'saldoAwal'		=> $this->Report_model->getSaldoAwal(array('start_date' => $start_date, 'end_date' => $end_date, 'coa' => $coa)),
		// 	'dataBBK'		=> $this->Report_model->dataGeneralLedger(array('start_date' => $start_date, 'end_date' => $end_date, 'coa' => $coa))
		// );
		// $this->template->load('maintemplate', 'report/views/buku_bank_kas_view', $data);
	}

	public function report_detail() {
		$this->load->view('report_detail_view');
	}

	public function view_neraca() {
		$jenis_report = $this->Anti_sql_injection($this->input->post('jenis_report', TRUE));
		if($jenis_report != 'hpp' && $jenis_report != 'rl' && $jenis_report != 'gl' && $jenis_report != 'bbk') {
			$dateSearch = $this->Anti_sql_injection($this->input->post('date_search', TRUE));
			$date_search = $this->setFormatDate(0, $dateSearch);
		}else {
			$startDate	= $this->Anti_sql_injection($this->input->post('start_date', TRUE));
			$endDate	= $this->Anti_sql_injection($this->input->post('end_date', TRUE));

			$start_date = $this->setFormatDate(0, $startDate);
			$end_date = $this->setFormatDate(0, $endDate);
		}

		if($jenis_report == 'nrc') {	
			$data = array(
				'dateSearch'			=> $this->setFormatDate(1, $dateSearch),
				'aktiva_lancar'			=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 2)),
				'aktiva_tetap'			=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 3)),
				'penyusutan'			=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 4)),
				'pasivaHutangDagang'	=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 5)),
				'pasivaHutangLain'		=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 6)),
				'pasivaModal'			=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 7))
			);
			$this->load->view('neraca_view', $data);
		}else if($jenis_report == 'prn_nrc') {
			$data = array(
				'dateSearch'		=> $this->setFormatDate(1, $dateSearch),
				'kasDetail'			=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 8)),
				'bankDetail'		=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 9)),
				'arDetail'			=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 10)),
				'pbdDetail'			=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 11)),
				'inventory'			=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 12)),
				'fixedAsset'		=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 3)),
				'penyusutan'		=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 4)),
				'hutangDagang'		=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 13)),
				'hutangLain'		=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 6)),
				'modal'				=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 7))
			);
			$this->load->view('perincian_neraca_view', $data);
		}else if($jenis_report == 'hpp') {
			$data = array(
				'dateStart'				=> $this->setFormatDate(1, $startDate),
				'dateSearch'			=> $this->setFormatDate(1, $endDate),
				'pemakaianBahanBaku'	=> $this->getReportFinanceHppUp(array('start_date' => $start_date, 'end_date' => $end_date)),
				'bTKL'					=> $this->getReportFinance(array('date_search' => $end_date, 'report_finance' => 14)),
				'bPTL'					=> $this->getReportFinance(array('date_search' => $end_date, 'report_finance' => 15))
			);
			$this->load->view('hpp_view', $data);
		}else if($jenis_report == 'rl') {
			$data = array(
				'dateStart'				=> $this->setFormatDate(1, $startDate),
				'dateSearch'			=> $this->setFormatDate(1, $endDate),
				'penjualan'				=> $this->getReportFinance(array('date_search' => $end_date, 'report_finance' => 16)),
				'hargaPokokPenjualan'	=> $this->sumHPP($start_date, $end_date),
				'bO'					=> $this->getReportFinance(array('date_search' => $end_date, 'report_finance' => 17)),
				'pLL'					=> $this->getReportFinance(array('date_search' => $end_date, 'report_finance' => 18))
			);
			$this->load->view('laba_rugi_view', $data);
		}else if($jenis_report == 'prn_rl') {
			$data = array(
				'dateSearch'			=> $this->setFormatDate(1, $dateSearch),
				'penjualan'				=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 16)),
				'pembelian'				=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 19)),
				'bTKL'					=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 14)),
				'bPTL'					=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 15)),
				'bO'					=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 17)),
				'bAU'					=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 20)),
				'pLL'					=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 18)),
				'bLL'					=> $this->getReportFinance(array('date_search' => $date_search, 'report_finance' => 21))
			);
			$this->load->view('perincian_laba_rugi_view', $data);
		}else if($jenis_report == 'gl') {
			$coa					= $this->Anti_sql_injection($this->input->post('coa', TRUE));

			$data = array(
				'dataCoa'				=> $this->Report_model->getIdCOA($coa),
				'saldoAwal'				=> $this->Report_model->getSaldoAwal(array('start_date' => $start_date, 'end_date' => $end_date, 'coa' => $coa)),
				'dataGeneralLedger'		=> $this->Report_model->dataGeneralLedger(array('start_date' => $start_date, 'end_date' => $end_date, 'coa' => $coa))
			);
			$this->load->view('general_ledger_view', $data);
		}else if($jenis_report == 'bbk') {
			$coa					= $this->Anti_sql_injection($this->input->post('coa', TRUE));

			$data = array(
				'dataCoa'				=> $this->Report_model->getIdBBK($coa),
				'saldoAwal'				=> $this->Report_model->getSaldoAwal(array('start_date' => $start_date, 'end_date' => $end_date, 'coa' => $coa)),
				'dataBBK'		=> $this->Report_model->dataGeneralLedger(array('start_date' => $start_date, 'end_date' => $end_date, 'coa' => $coa))
			);
			$this->load->view('buku_bank_kas_view', $data);
		}else{
			$result = array(
				'success' => false,
			);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function getReportFinance($data) {
		$result = $this->Report_model->get_report($data);

		return $result;
	}

	public function getReportFinanceHppUp($data) {
		$result = $this->Report_model->finance_hpp_up($data);

		return $result;
	}

	public function setFormatDate($val, $valDate) {
		$temptgl = explode("/", $valDate);
		if($val == 0) $format = 'Y-m-d';
		else $format = 'd-F-Y';
		$dateFormated = date($format, strtotime($temptgl[2].'-'.$temptgl[1].'-'.$temptgl[0])); 

		return $dateFormated;
	}

	public function sumHPP($start_date, $end_date) {
		$persedian_per 			= 0;
		$pembelian 				= 0;
		$bahan_baku_tersedia	= 0;
		$saldo_persediaan_per	= 0;
		$pemakaian_bahan_baku	= 0;
		$awal_wip				= 0;
		$akhir_wip				= 0;
		$totalBTKL				= 0;
		$totalBPTL				= 0;
		$totalHPP				= 0;

		$pemakaianBahanBaku = $this->getReportFinanceHppUp(array('start_date' => $start_date, 'end_date' => $end_date));
		$bTKL				= $this->getReportFinance(array('date_search' => $end_date, 'report_finance' => 14));
		$bPTL				= $this->getReportFinance(array('date_search' => $end_date, 'report_finance' => 15));
		
		if(isset($pemakaianBahanBaku[0])) {
			$persedian_per 			= $pemakaianBahanBaku[0]['persedian_per'];
			$pembelian 				= $pemakaianBahanBaku[0]['pembelian'];
			$bahan_baku_tersedia	= $persedian_per + $pembelian;
			$saldo_persediaan_per	= $pemakaianBahanBaku[0]['saldo_persediaan_per'];
			$pemakaian_bahan_baku	= $bahan_baku_tersedia - $saldo_persediaan_per;
			$awal_wip				= $pemakaianBahanBaku[0]['awal_wip'];
			$akhir_wip				= $pemakaianBahanBaku[0]['akhir_wip'];
		}
		
		foreach($bTKL as $kBTKL) {
			$totalBTKL = $totalBTKL + $kBTKL['total'];
		}
		
		foreach($bPTL as $kBPTL) {
			$totalBPTL = $totalBPTL + $kBPTL['total'];
		}

		$totalHPP = $totalHPP + $pemakaian_bahan_baku + $totalBTKL + $totalBPTL + $awal_wip + $akhir_wip;
		$result = array(
			'persedian_per'			=> $persedian_per,
			'hpp'					=> $totalHPP,
			'saldo_persediaan_per'	=> $saldo_persediaan_per
		);

		return $result;
	}

	public function getDataCOA() {
		$data		= file_get_contents("php://input");
		$params		= json_decode($data,true);
		$strQuery	= '';

		if(isset($params['level'])) {
			if($params['level'] == 1) $strQuery = 'id_parent = 0';
			else {
				if(isset($params['id'])) {
					$noCoa		= $this->Report_model->getIdCOA($params['id']);
					$strQuery	= 'id_parent = '.$noCoa[0]['coa'];
				}
			}

			if($strQuery != '') {
				$data = $this->Report_model->getDataCOA($strQuery);
				$res = array(
					'status'	=> 'success',
					'coa'		=> $data
				);
			}else $res = array('status' => 'failed');
		}else $res = array('status' => 'failed');

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function getDataBBK() {
		$data		= file_get_contents("php://input");
		$params		= json_decode($data,true);

		$data = $this->Report_model->getDataBBK();
		if(sizeof($data) > 0) {
			$res = array(
				'status'	=> 'success',
				'coa'		=> $data
			);
		}else $res = array('status' => 'failed');

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}
}