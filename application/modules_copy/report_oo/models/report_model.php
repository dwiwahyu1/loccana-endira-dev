<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Report_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function get_report($data) {
		$sql 	= 'CALL report_finance(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['date_search'],
			$data['report_finance']
		));
		
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function finance_hpp_up($data) {
		$sql 	= 'CALL report_finance_hpp_up(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['start_date'],
			$data['end_date']
		));
		
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function getDataCOA($strQry) {
		$sql = 'SELECT id_coa, keterangan FROM t_coa WHERE '.$strQry;
		$query 	=  $this->db->query($sql);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function getDataBBK() {
		$sql = 'SELECT id_coa, keterangan FROM t_coa WHERE id_parent = 10120';
		$query 	=  $this->db->query($sql);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function getIdCOA($id) {
		$sql = 'SELECT coa, keterangan FROM t_coa WHERE id_coa = '.$id;
		$query 	=  $this->db->query($sql);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function getIdBBK($id) {
		$sql = 'SELECT a.id_coa,a.coa, a.keterangan,b.nama_bank,b.no_rek FROM t_coa a JOIN t_bank b ON b.id_coa = a.id_coa WHERE a.id_coa ='.$id;
		$query 	=  $this->db->query($sql);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function getSaldoAwal($data) {
		$sql 	= 'CALL general_ledger_saldo_awal(?,?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['coa'],
			$data['start_date'],
			$data['end_date']
		));
		
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function dataGeneralLedger($data) {
		$sql 	= 'CALL general_ledger(?,?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['coa'],
			$data['start_date'],
			$data['end_date']
		));
		
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}