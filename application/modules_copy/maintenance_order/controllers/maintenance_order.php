<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Maintenance_Order extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('maintenance_order/maintenance_order_model');
    $this->load->library('sequence');
    $this->load->library('log_activity');
  }

  /**
   * anti sql injection
   */
  public function Anti_sql_injection($string)
  {
    $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
    return $string;
  }

  public function index()
  {
    $this->template->load('maintemplate', 'maintenance_order/views/index');
  }

  function lists()
  {
    $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
    $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
    $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
    $order = $this->input->get_post('order');
    $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
    $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

    $order_fields = array(
      'a.no_work_order', 'a.delivery_date', 'a.tujuan', 'a.term_of_payment', 'a.total_amount',
      'b.name_eksternal',
      'c.nama',
      'e.group'
    );

    $search = $this->input->get_post('search');

    $search_val = (!empty($search['value'])) ? $search['value'] : null;

    $search_value = $this->Anti_sql_injection($search_val);

    // Build params for calling model
    $params['limit'] = (int) $length;
    $params['offset'] = (int) $start;
    $params['order_column'] = $order_fields[$order_column];
    $params['order_dir'] = $order_dir;
    $params['filter'] = $search_value;

    $list = $this->maintenance_order_model->lists($params);

    /*echo "<pre>";
		print_r($list);
		echo "</pre>";
		die;*/

    $result["recordsTotal"] = $list['total'];
    $result["recordsFiltered"] = $list['total_filtered'];
    $result["draw"] = $draw;

    $data = array();
    $strStatus = '';
    $no = $start;
    foreach ($list['data'] as $k => $v) {
      if ($v['id_mo'] != NULL) {
        $no++;
        $strButton =
          '<div class="btn-group">' .
          '<button class="btn btn-info btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Details" onclick="detail_mainOrder(\'' . $v['id_mo'] . '\')">' .
          '<i class="fa fa-search"></i>' .
          '</button>' .
          '</div>';

        $strButtonEdit =
          '<div class="btn-group">' .
          '<button class="btn btn-warning btn-sm"title="Edit" onClick="edit_mainOrder(\'' . $v['id_mo'] . '\')">' .
          '<i class="fa fa-edit"></i>' .
          '</button>' .
          '</div>';

        $strButtonDel =
          '<div class="btn-group">' .
          '<button class="btn btn-danger btn-sm"title="Delete" onClick="delete_mainOrder(\'' . $v['id_mo'] . '\')">' .
          '<i class="fa fa-trash"></i>' .
          '</button>' .
          '</div>';

        $strButtonApproval =
          '<div class="btn-group">' .
          '<button class="btn btn-success btn-sm"title="Approval" onClick="approve_mainOrder(\'' . $v['id_mo'] . '\')">' .
          '<i class="fa fa-check"></i>' .
          '</button>' .
          '</div>';

        if ($v['status'] == 0) {
          $strStatus = 'Preparing';
          $strButton .= $strButtonEdit . $strButtonDel . $strButtonApproval;
        } else if ($v['status'] == 1) $strStatus = 'Done';
        else {
          $strStatus = 'Rejected';
          $strButton .= $strButtonEdit . $strButtonDel;
        }

        array_push($data, array(
          $no,
          $v['no_main_order'],
          $v['name_eksternal'],
          date('d-M-Y', strtotime($v['delivery_date'])),
          $v['term_of_payment'],
          $v['symbol_valas'] . ' ' . number_format($v['total_amount'], 2, ',', '.'),
          $strStatus,
          $strButton
        ));
      }
    }

    $result["data"] = $data;

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function get_maintenance()
  {
    $result = $this->maintenance_order_model->get_maintenance();

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($result);
  }

  public function get_maintenanceBy($id)
  {
    $result = $this->maintenance_order_model->get_maintenanceBy($id);

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($result);
  }

  public function add()
  {
    $customer     = $this->maintenance_order_model->customer();
    $valas       = $this->maintenance_order_model->valas();
    $maintenance   = $this->maintenance_order_model->get_maintenance();

    $data = array(
      'customer'    => $customer,
      'valas'      => $valas,
      'maintenance'  => $maintenance
    );

    /*echo "<pre>";
		// print_r($customer);
		// print_r($valas);
		// print_r($maintenance);
		print_r($data);
		echo "</pre>";
		die;*/

    $this->load->view('add_modal_view', $data);
  }

  public function add_mainOrder()
  {
    $this->form_validation->set_rules('delivery_date', 'Delivery Date', 'trim|required');
    // $this->form_validation->set_rules('cust_id', 'Customer Name', 'trim|required');
    $this->form_validation->set_rules('term_of_pay', 'Term of Payment', 'trim|required');
    $this->form_validation->set_rules('valas', 'Valas', 'trim|required');
    $this->form_validation->set_rules('rate', 'Rate', 'trim|required');
    $this->form_validation->set_rules('diskon', 'Diskon', 'trim|required');
    $this->form_validation->set_rules('tujuan', 'Tujuan', 'trim|required');

    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));

      $result = array('success' => false, 'message' => $msg);
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      $no           = $this->sequence->get_no('maintenance');
      $tgl_delivery_date    = $this->Anti_sql_injection($this->input->post('delivery_date', TRUE));
      $temp_delivery_date   = explode("-", $tgl_delivery_date);
      $cust_id        = 0;
      $term_of_pay      = $this->Anti_sql_injection($this->input->post('term_of_pay', TRUE));
      $valas          = $this->Anti_sql_injection($this->input->post('valas', TRUE));
      $rate          = $this->Anti_sql_injection($this->input->post('rate', TRUE));
      $diskon          = $this->Anti_sql_injection($this->input->post('diskon', TRUE));
      $tujuan          = $this->Anti_sql_injection($this->input->post('tujuan', TRUE));
      $total_amount      = $this->Anti_sql_injection($this->input->post('total_amount', TRUE));

      $tempNoDm        = $this->Anti_sql_injection($this->input->post('arrNoDm', TRUE));
      $arrNoDm        = explode(',', $tempNoDm);

      $tempPrice        = $this->Anti_sql_injection($this->input->post('arrPrice', TRUE));
      $arrPrice        = explode(',', $tempPrice);

      $tempRemark        = $this->Anti_sql_injection($this->input->post('arrRemark', TRUE));
      $arrRemark        = explode(',', $tempRemark);

      $dataMainOrder = array(
        'no'      => $no,
        'delivery_date'  => date('Y-m-d', strtotime($temp_delivery_date[2] . '-' . $temp_delivery_date[1] . '-' . $temp_delivery_date[0])),
        'tujuan'    => ucwords($tujuan),
        'cust_id'    => $cust_id,
        'term_of_pay'  => $term_of_pay,
        'diskon'    => $diskon,
        'total_amount'  => $total_amount,
        'valas'      => $valas,
        'rate'      => $rate
      );

      /*echo "<pre>";
			print_r($dataMainOrder);
			print_r($arrNoDm);
			print_r($arrPrice);
			print_r($arrRemark);
			echo "</pre>";
			die;*/

      $resultDetail = false;
      $resultAddMainOrder = $this->maintenance_order_model->add_mainOrder($dataMainOrder);
      if ($resultAddMainOrder['result'] > 0) {
        for ($i = 0; $i < sizeof($arrNoDm); $i++) {
          if ($arrRemark[$i] != '' && $arrRemark[$i] != 'NULL') $remark = ucfirst($arrRemark[$i]);
          else $remark = NULL;

          $dataDetail = array(
            'id_mo'      => $resultAddMainOrder['lastid'],
            'id_dm'      => $arrNoDm[$i],
            'unit_price'  => $arrPrice[$i],
            'remark'    => $remark
          );

          $resultAddDetailMainOrder = $this->maintenance_order_model->add_detail_mainOrder($dataDetail);
          if ($resultAddDetailMainOrder > 0) $resultDetail = true;
          else {
            $resultDetail = false;
            $msg = 'Gagal menambahkan detail Maintenance Order yang ke-' . ($i + 1);
            $this->log_activity->insert_activity('insert', $msg);
            $result = array('success' => false, 'message' => $msg);
            break;
          }
        }

        if ($resultDetail == true) {
          $msg = 'Berhasil menambahkan Maintenance Order ke database';
          $this->log_activity->insert_activity('insert', $msg . ' dengan ID : ' . $resultAddMainOrder['lastid']);
          $result = array('success' => true, 'message' => $msg);
        }
      } else {
        $msg = 'Gagal menambahkan Maintenance Order ke database';

        $this->log_activity->insert_activity('insert', $msg);
        $result = array('success' => false, 'message' => $msg);
      }
    }

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function edit($id_mainOrder)
  {
    $mainOrder     = $this->maintenance_order_model->get_mainOrder($id_mainOrder);
    $detail     = $this->maintenance_order_model->get_detail_mainOrder($id_mainOrder);
    $customer     = $this->maintenance_order_model->customer();
    $valas       = $this->maintenance_order_model->valas();
    $maintenance   = $this->maintenance_order_model->get_maintenance();

    $data = array(
      'mainOrder'    => $mainOrder,
      'detail'    => $detail,
      'customer'    => $customer,
      'valas'      => $valas,
      'maintenance'  => $maintenance
    );

    /*echo "<pre>";
		// print_r($mainOrder);
		// print_r($detail);
		// print_r($customer);
		// print_r($valas);
		// print_r($maintenance);
		echo "</pre>";
		die;*/

    $this->load->view('edit_modal_view', $data);
  }

  public function edit_mainOrder()
  {
    $this->form_validation->set_rules('delivery_date', 'Delivery Date', 'trim|required');
    $this->form_validation->set_rules('cust_id', 'Customer Name', 'trim|required');
    $this->form_validation->set_rules('term_of_pay', 'Term of Payment', 'trim|required');
    $this->form_validation->set_rules('valas', 'Valas', 'trim|required');
    $this->form_validation->set_rules('rate', 'Rate', 'trim|required');
    $this->form_validation->set_rules('diskon', 'Diskon', 'trim|required');
    $this->form_validation->set_rules('tujuan', 'Tujuan', 'trim|required');

    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));

      $result = array('success' => false, 'message' => $msg);
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      $id_mo           = $this->Anti_sql_injection($this->input->post('id_mo', TRUE));
      $tgl_delivery_date    = $this->Anti_sql_injection($this->input->post('delivery_date', TRUE));
      $temp_delivery_date   = explode("-", $tgl_delivery_date);
      $cust_id        = $this->Anti_sql_injection($this->input->post('cust_id', TRUE));
      $term_of_pay      = $this->Anti_sql_injection($this->input->post('term_of_pay', TRUE));
      $valas          = $this->Anti_sql_injection($this->input->post('valas', TRUE));
      $rate          = $this->Anti_sql_injection($this->input->post('rate', TRUE));
      $diskon          = $this->Anti_sql_injection($this->input->post('diskon', TRUE));
      $tujuan          = $this->Anti_sql_injection($this->input->post('tujuan', TRUE));
      $total_amount      = $this->Anti_sql_injection($this->input->post('total_amount', TRUE));

      $tempIdItem        = $this->Anti_sql_injection($this->input->post('arrIdItem', TRUE));
      $arrIdItem        = explode(',', $tempIdItem);

      $tempNoDm        = $this->Anti_sql_injection($this->input->post('arrNoDm', TRUE));
      $arrNoDm        = explode(',', $tempNoDm);

      $tempPrice        = $this->Anti_sql_injection($this->input->post('arrPrice', TRUE));
      $arrPrice        = explode(',', $tempPrice);

      $tempRemark        = $this->Anti_sql_injection($this->input->post('arrRemark', TRUE));
      $arrRemark        = explode(',', $tempRemark);

      $mainOrder       = $this->maintenance_order_model->get_mainOrder($id_mo)[0];
      $dataEditMainOrder  = array(
        'id_mo'      => $id_mo,
        'delivery_date'  => date('Y-m-d', strtotime($temp_delivery_date[2] . '-' . $temp_delivery_date[1] . '-' . $temp_delivery_date[0])),
        'cust_id'    => $cust_id,
        'term_of_pay'  => $term_of_pay,
        'valas'      => $valas,
        'rate'      => $rate,
        'diskon'    => $diskon,
        'tujuan'    => ucwords($tujuan),
        'total_amount'  => $total_amount
      );

      /*echo "<pre>";
			print_r($mainOrder);
			print_r($dataEditMainOrder);
			print_r($arrIdItem);
			print_r($arrNoDm);
			print_r($arrPrice);
			print_r($arrRemark);
			echo "</pre>";
			die;*/

      if (
        $dataEditMainOrder['delivery_date'] != $mainOrder['delivery_date'] ||
        $dataEditMainOrder['cust_id'] != $mainOrder['id_cust'] ||
        $dataEditMainOrder['term_of_pay'] != $mainOrder['term_of_payment'] ||
        $dataEditMainOrder['valas'] != $mainOrder['id_valas'] ||
        $dataEditMainOrder['rate'] != $mainOrder['rate'] ||
        $dataEditMainOrder['diskon'] != $mainOrder['diskon'] ||
        $dataEditMainOrder['tujuan'] != $mainOrder['tujuan'] ||
        $dataEditMainOrder['total_amount'] != $mainOrder['total_amount']
      ) {
        $resultEditMainOrder = $this->maintenance_order_model->edit_mainOrder($dataEditMainOrder);
      } else $resultEditMainOrder = 1;
      $resultDetail = false;

      for ($i = 0; $i < sizeof($arrIdItem); $i++) {
        if ($arrRemark[$i] != '' && $arrRemark[$i] != 'NULL') $remark = $arrRemark[$i];
        else $remark = NULL;

        if ($arrIdItem[$i] != 'new') {
          $detail = $this->maintenance_order_model->get_detail_mainOrder_id_dm($arrIdItem[$i])[0];
          $dataDetail = array(
            'id_mo_dm'      => $arrIdItem[$i],
            'id_dm'        => $arrNoDm[$i],
            'unit_price'    => $arrPrice[$i],
            'remark'      => $remark
          );

          if (
            $dataDetail['id_dm'] != $detail['id_dm'] ||
            $dataDetail['unit_price'] != $detail['unit_price'] ||
            $dataDetail['remark'] != $detail['remark']
          ) {
            $resultEditDetailWorkOrder = $this->maintenance_order_model->edit_detail_mainOrder($dataDetail);
          } else $resultEditDetailWorkOrder = 1;
          if ($resultEditDetailWorkOrder > 0) $resultDetail = true;
          else {
            $msg = 'Gagal update detail Maintenance Order yang ke-' . ($i + 1) . ' database';

            $this->log_activity->insert_activity('update', $msg . ' dengan ID : ' . $arrIdItem[$i]);
            $result = array('success' => false, 'message' => $msg);
            $resultDetail = false;
            break;
          }
        } else {
          $dataDetail = array(
            'id_mo'      => $id_mo,
            'id_dm'      => $arrNoDm[$i],
            'unit_price'  => $arrPrice[$i],
            'remark'    => $remark
          );

          $resultAddDetailMainOrder = $this->maintenance_order_model->add_detail_mainOrder($dataDetail);
          if ($resultAddDetailMainOrder > 0) $resultDetail = true;
          else {
            $resultDetail = false;
            $msg = 'Gagal mengubah detail Maintenance Order yang ke-' . ($i + 1) . ' database';

            $this->log_activity->insert_activity('insert', 'Gagal menambahkan baru detail Maintenance Order');
            $result = array('success' => false, 'message' => $msg);
            break;
          }
        }
      }

      if ($resultEditMainOrder > 0 && $resultDetail == true) {
        $msg = 'Berhasil mengubah Maintenance Order ke database';

        $this->log_activity->insert_activity('update', $msg . ' dengan ID : ' . $id_mo);
        $result = array('success' => true, 'message' => $msg);
      } else {
        if ($resultDetail != false) {
          $msg = 'Gagal mengubah Maintenance Order ke database';

          $this->log_activity->insert_activity('update', $msg . ' dengan ID : ' . $id_mo);
          $result = array('success' => false, 'message' => $msg);
        }
      }
    }

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function detail_mainOrder($id_mainOrder)
  {
    $mainOrder     = $this->maintenance_order_model->get_mainOrder($id_mainOrder);
    $detail     = $this->maintenance_order_model->get_detail_mainOrder($id_mainOrder);
    $customer     = $this->maintenance_order_model->customer();
    $valas       = $this->maintenance_order_model->valas();
    $maintenance   = $this->maintenance_order_model->get_maintenance();

    $data = array(
      'mainOrder'    => $mainOrder,
      'detail'    => $detail,
      'customer'    => $customer,
      'valas'      => $valas,
      'maintenance'  => $maintenance
    );

    $this->load->view('detail_modal_view', $data);
  }

  public function approve_mainOrder($id_mainOrder)
  {
    $mainOrder     = $this->maintenance_order_model->get_mainOrder($id_mainOrder);
    $detail     = $this->maintenance_order_model->get_detail_mainOrder($id_mainOrder);
    $customer     = $this->maintenance_order_model->customer();
    $valas       = $this->maintenance_order_model->valas();
    $maintenance   = $this->maintenance_order_model->get_maintenance();

    $data = array(
      'mainOrder'    => $mainOrder,
      'detail'    => $detail,
      'customer'    => $customer,
      'valas'      => $valas,
      'maintenance'  => $maintenance
    );

    $this->load->view('approval_modal_view', $data);
  }

  public function status_mainOrder()
  {
    $this->form_validation->set_rules('status', 'Status', 'trim|required');

    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));

      $result = array('success' => false, 'message' => $msg);
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      // echo "<pre>";
      // print_r($this->input->post());
      // echo "</pre>";
      // die;
      $id_mo     = $this->Anti_sql_injection($this->input->post('id_mo', TRUE));
      $no_mo     = $this->Anti_sql_injection($this->input->post('no_mo', TRUE));
      $status   = $this->Anti_sql_injection($this->input->post('status', TRUE));
      $notes     = $this->Anti_sql_injection($this->input->post('notes', TRUE));
      $delivery_date     = $this->Anti_sql_injection($this->input->post('delivery_date', TRUE));
      $delivery_date   = explode("-", $delivery_date);
      $delivery_date   =date('Y-m-d', strtotime($delivery_date[2] . '-' . $delivery_date[1] . '-' . $delivery_date[0]));
      $term_of_pay   = $this->Anti_sql_injection($this->input->post('term_of_pay', TRUE));
      $valas     = $this->Anti_sql_injection($this->input->post('valas', TRUE));
      $rate     = $this->Anti_sql_injection($this->input->post('rate', TRUE));
      $diskon   = $this->Anti_sql_injection($this->input->post('diskon', TRUE));
      $tujuan     = $this->Anti_sql_injection($this->input->post('tujuan', TRUE));
      $total_amount_last     = $this->Anti_sql_injection($this->input->post('total_amount_last', TRUE));
      $total_amount_first   = $this->Anti_sql_injection($this->input->post('total_amount_first', TRUE));
      $arrIdItem   = ($this->input->post('id_material', TRUE));
      $arrQty   = ($this->input->post('qty', TRUE));

      $data = array(
        'id_mo'    => $id_mo,
        'status'  => $status,
        'notes'    => $notes
      );

      $resultEditStatus = $this->maintenance_order_model->status_mainOrder($data);

      $data_kartu_hp = array(
        'ref'       => NULL,
        'source'     => NUll,
        'keterangan'   => 'Pembayaran Maintenance Order ' . $no_mo,
        'status'     => 0,
        'saldo'     => $total_amount_first,
        'saldo_akhir'   =>   $total_amount_last,
        'id_valas'    => $valas,
        'type_kartu'  => 0,
        'type_master'  => 0,
        'delivery_date'  => $delivery_date,
        'payment_coa'  => 487 // coa maintenance
      );

      $data_coa = array(
        'id_coa'  => 487,
        'id_parent'  => 0,
        'id_valas'  => $valas,
        'value'    => $total_amount_last,
        'adjusment'  => 0,
        'type_cash'  => 0,
        'note'    => $data_kartu_hp['keterangan'],
        'rate'    => $rate
      );

      $result_add_coa = $this->maintenance_order_model->add_coa_values($data_coa);
      $data_kartu_hp['id_master'] = $result_add_coa['lastid'];
      $this->maintenance_order_model->add_kartu_hp($data_kartu_hp);
      for ($i = 0; $i < sizeof($arrIdItem); $i++) {
        $dataDetail = array(
          'id_detail'  => $arrIdItem[$i],
          'qty'        => $arrQty[$i],
          'date'       => $delivery_date
        );
        $resultDetail = $this->maintenance_order_model->add_mutasi_maintenance($dataDetail);
      }
      if ($resultEditStatus > 0) {
        $msg = 'Berhasil mengubah status Maintenance Order ke database';

        $this->log_activity->insert_activity('update', $msg . ' dengan ID : ' . $id_mo);
        $result = array('success' => true, 'message' => $msg);
      } else {
        $msg = 'Gagal mengubah status Maintenance Order ke database';

        $this->log_activity->insert_activity('update', $msg . ' dengan ID : ' . $id_mo);
        $result = array('success' => false, 'message' => $msg);
      }
    }

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function delete_mainOrder()
  {
    $data     = file_get_contents("php://input");
    $params     = json_decode($data, true);

    /*echo "<pre>";
		print_r($params);
		echo "</pre>";
		die;*/

    $resultDelete = $this->maintenance_order_model->delete_mainOrder($params);
    if ($resultDelete > 0) {
      $msg = 'Berhasil menghapus Data Maintenance Order';
      $this->log_activity->insert_activity('delete', $msg . ' dengan ID : ' . $params['id']);
      $result = array('success' => true, 'message' => $msg);
    } else {
      $msg = 'Gagal menghapus Data Maintenance Order';
      $this->log_activity->insert_activity('delete', $msg . ' dengan ID : ' . $params['id']);
      $result = array('success' => false, 'message' => $msg);
    }

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function delete_detail_mainOrder()
  {
    $data     = file_get_contents("php://input");
    $params     = json_decode($data, true);

    /*echo "<pre>";
		print_r($params);
		echo "</pre>";
		die;*/

    $resultDelete = $this->maintenance_order_model->delete_detail_mainOrder($params);
    if ($resultDelete > 0) {
      $msg = 'Berhasil menghapus Detail Maintenance Order';
      $this->log_activity->insert_activity('delete', $msg . ' dengan ID : ' . $params['id']);
      $result = array('success' => true, 'message' => $msg);
    } else {
      $msg = 'Gagal menghapus Detail Maintenance Order';
      $this->log_activity->insert_activity('delete', $msg . ' dengan ID : ' . $params['id']);
      $result = array('success' => false, 'message' => $msg);
    }

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }
}
