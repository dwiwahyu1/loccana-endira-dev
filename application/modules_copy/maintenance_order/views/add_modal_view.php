<style>
	.dt-body-left {text-align: left;}
	.dt-body-center {text-align: center;}
	.dt-body-right {text-align: right;}

	#listItemMaintenance {
		counter-reset: rowNumber;
	}

	#listItemMaintenance tbody tr > td:first-child {
		counter-increment: rowNumber;
		text-align: center;
	}

	#listItemMaintenance tbody tr td:first-child::before {
		content: counter(rowNumber);
		min-width: 1em;
		margin-right: 0.5em;
	}
</style>

<form class="form-horizontal form-label-left" id="add_mainOrder" role="form" action="<?php echo base_url('maintenance_order/add_mainOrder'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="delivery_date">Delivery Date <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="delivery_date" name="delivery_date" value="<?php
				echo date('d-M-Y'); ?>" placeholder="Work Date" required>
		</div>
	</div>

	<div class="item form-group" style="display:none">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="cust_id">Customer Name <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="cust_id" name="cust_id" style="width: 100%" >
				<option value="" selected>-- Pilih Customer --</option>
		<?php
			foreach ($customer as $ck => $cv) { ?>
				<option value="<?php echo $cv['id']; ?>"><?php echo $cv['name_eksternal']; ?></option>
		<?php
			}
		?>
			</select>
	  </div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="term_of_pay">Term of Payment <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="term_of_pay" name="term_of_pay" style="width: 100%" required>
				<option value="45 Day"> 45 Day</option>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="valas">Valas <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="valas" name="valas" style="width: 100%" required>
				<option value="" selected>-- Pilih Valas --</option>
				<?php foreach($valas as $vd) { ?>
					<option value="<?php echo $vd['valas_id']; ?>" ><?php echo $vd['nama_valas']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="rate">Rate <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="number" class="form-control" id="rate" name="rate" placeholder="Rate" autocomplete="off" step=".0001" value='1' required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="diskon">Diskon <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="number" class="form-control" id="diskon" name="diskon" placeholder="Diskon" autocomplete="off" step=".0001" value='0' required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tujuan">No BTB <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="tujuan" name="tujuan" placeholder="Tujuan" autocomplete="off" required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Daftar Maintenance : </label>
		<div class="col-md-8 col-sm-6 col-xs-12"></div>
	</div>

	<div class="item form-group">
		<table id="listItemMaintenance" class="table table-striped table-bordered dt-responsive nowrap" width="100%">
			<thead>
				<tr>
					<th class="dt-body-center" style="width: 5%;">No</th>
					<th style="width: 15%;">No Maintenance</th>
					<th>Nama Maintenance</th>
					<th>Mesin</th>
					<th style="width: 5%;">QTY</th>
					<th style="width: 15%;">Harga</th>
					<th style="width: 15%;">Amount</th>
					<th style="width: 20%;">Remark</th>
					<th style="width: 5%;"><a id="btn_item_add" class="btn btn-primary"><i class="fa fa-plus"></i></a></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td></td>
					<td>
						<select class="form-control" id="no_dm1" name="no_dm[]">
							<option value="" selected>-- Pilih Maintenance --</option>
					<?php
						foreach ($maintenance as $mk => $mv) { ?>
							<option value="<?php echo $mv['id_detail_main']; ?>"><?php echo $mv['no_main'].' - '.$mv['name_eksternal'].' - '.$mv['stock_name']; ?></option>
					<?php
						}
					?>
						</select>
					</td>
					<td>
						<label id="nama_main1"></label>
					</td>
					<td>
						<label id="uom_main1"></label>
					</td>
					<td>
						<label id="qty_main1"></label>
					</td>
					<td>
						<input type="number" class="form-control" id="dm_price1" name="dm_price[]" placeholder="Harga" autocomplete="off" step=".0001" disabled>
					</td>
					<td class="dt-body-right">
						<label id="rowAmount1">0.0000</label>
						<input type="hidden" id="amount1" name="amount[]" value="0">
					</td>
					<td>
						<textarea class="form-control" id="desc_main1" name="desc_main[]" placeholder="Deskripsi" rows="3" disabled></textarea>
					</td>
					<td></td>
				</tr>
			</tbody>
			<tfoot>
				<tr>
					<td class="dt-body-right" colspan="5" rowspan="3"></td>
					<td class="dt-body-right">Total</td>
					<td class="dt-body-right"><label id="totalAmountRow">0.0000</label></td>
					<td colspan="2" rowspan="3"></td>
				</tr>
				<tr>
					<td class="dt-body-right">Diskon</td>
					<td class="dt-body-right"><label id="totalAmountDiskon">0</label></td>
				</tr>
				<tr>
					<td class="dt-body-right">Total Amount</td>
					<td class="dt-body-right"><label id="totalAmount">0.0000</label></td>
				</tr>
			</tfoot>
		</table>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Maintenance Order</button>
			<input type="hidden" id="total_amount" name="total_amount" value="0">
		</div>
	</div>
</form>

<script type="text/javascript">
	var idRowItem 		= 1;
	var arrNoDm 		= [];
	var arrPrice		= [];
	var arrRemark		= [];

	$(document).ready(function() {
		$("#delivery_date").datepicker({
			format: 'dd-M-yyyy',
			autoclose: true,
			todayHighlight: true,
		});

		$('#cust_id').select2();
		$('#term_of_pay').select2();
		$('#valas').select2();
		$('#diskon').on('keyup', function() {
			cal_amount();
		})
		$('#diskon').on('change', function() {
			cal_amount();
		})

		$('#no_dm1').select2();
		$('#no_dm1').on('change', function() {
			set_itemValue(this.value, this.id);
			cal_amount();
		})

		$('#dm_price1').on('keyup', function() {
			cal_amount();
		})
		$('#dm_price1').on('change', function() {
			cal_amount();
		})
	});

	$('#btn_item_add').on('click', function() {
		idRowItem++;
		$('#listItemMaintenance tbody').append(
			'<tr id="trRowItem'+idRowItem+'">'+
				'<td></td>'+
				'<td>'+
					'<select class="form-control" id="no_dm'+idRowItem+'" name="no_dm[]" disabled>'+
						'<option value="" selected>-- Pilih Maintenance --</option>'+
					'</select>'+
				'</td>'+
				'<td>'+
					'<label id="nama_main'+idRowItem+'"></label>'+
				'</td>'+
				'<td>'+
					'<label id="uom_main'+idRowItem+'"></label>'+
				'</td>'+
				'<td>'+
					'<label id="qty_main'+idRowItem+'"></label>'+
				'</td>'+
				'<td>'+
					'<input type="number" class="form-control" id="dm_price'+idRowItem+'" name="dm_price[]" placeholder="Harga" autocomplete="off" step=".0001" disabled>'+
				'</td>'+
				'<td class="dt-body-right">'+
					'<label id="rowAmount'+idRowItem+'">0.0000</label>'+
					'<input type="hidden" id="amount'+idRowItem+'" name="amount[]" value="0">'+
				'</td>'+
				'<td>'+
					'<textarea class="form-control" id="desc_main'+idRowItem+'" name="desc_main[]" placeholder="Deskripsi" rows="3" disabled></textarea>'+
				'</td>'+
				'<td>'+
					'<a class="btn btn-danger" onclick="removeRow('+idRowItem+')"><i class="fa fa-minus"></i></a>'+
				'</td>'+
			'</tr>'
		);
		$("#no_dm"+idRowItem).select2();
		$('#no_dm'+idRowItem).on('change', function() {
			set_itemValue(this.value, this.id);
			cal_amount();
		})
		$('#dm_price'+idRowItem).on('keyup', function() {
			cal_amount();
		})
		$('#dm_price'+idRowItem).on('change', function() {
			cal_amount();
		})

		$.ajax({
			type: "GET",
			url: "<?php echo base_url('maintenance_order/get_maintenance');?>",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					for (var i = 0; i < r.length; i++) {
						var newOption = new Option(r[i].no_main+' - '+r[i].name_eksternal+' - '+r[i].stock_name, r[i].id_detail_main, false, false);
						$('#no_dm'+idRowItem).append(newOption);
					}
					$('#no_dm'+idRowItem).removeAttr('disabled');
				}else $('#no_dm'+idRowItem).removeAttr('disabled');
			}
		});
	})

	function set_itemValue(dm, rowSelect) {
		var row = rowSelect.replace(/[^0-9]+/g, "");
		if(dm != '' && dm != null) {
			$.ajax({
				type: "GET",
				url: "<?php echo base_url('maintenance_order/get_maintenanceBy');?>"+'/'+dm,
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(r) {
					if(r.length > 0) {
						$('#nama_main'+row).html(r[0].nama_main);
						$('#qty_main'+row).html(formatCurrencyComaNull(r[0].qty));
						$('#uom_main'+row).html(r[0].stock_name);
						$('#dm_price'+row).val(0).removeAttr('disabled');
						$('#amount'+row).val(0);
						$('#desc_main'+row).val(r[0].deskripsi).removeAttr('disabled');
					}
				}
			});
		}else {
			$('#nama_main'+row).html('');
			$('#qty_main'+row).html('');
			$('#uom_main'+row).html('');
			$('#dm_price'+row).val('').attr('disabled', 'disabled');
			$('#amount'+row).val(0);
			$('#desc_main'+row).val('').attr('disabled', 'disabled');
		}
	}

	function removeRow(rowItem) {
		$('#trRowItem'+rowItem).remove();
		cal_amount();
	}

	function cal_amount() {
		var diskon 			= $('#diskon').val();
		var tempDiskon		= 0;
		var qtyRow			= 0;
		var price 			= 0;
		var totalRow		= 0;
		var tempTotal 		= 0;
		var totalAmount 	= 0;
		if(diskon == '' || diskon == undefined || diskon == null) diskon = 0;

		$('input[name="dm_price[]"]').each(function() {
			var idRow 		= this.id.replace(/[^0-9]+/g, "");

			var tempQty		= $('#qty_main'+idRow).html().replace(/[^0-9.]+/g, "");
			if(tempQty != '' && tempQty != null && tempQty != undefined) qtyRow = parseFloat(tempQty);
			if(this.value != '' && this.value != null && this.value != undefined) price = parseFloat(this.value);

			totalRow = (price * qtyRow);
			if(this.value != '' && this.value != undefined && this.value != null) {
				$('#rowAmount'+idRow).html(formatCurrencyComa(totalRow));
				$('#amount'+idRow).val(totalRow);
			}else {
				$('#rowAmount'+idRow).html(formatCurrencyComa(0));
				$('#amount'+idRow).val(0);
			}
		})

		$('input[name="amount[]"]').each(function() {
			var amountRow = 0;
			if(this.value != '' && this.value != null && this.value != undefined) amountRow = parseFloat(this.value);
			tempTotal = (tempTotal + amountRow);
		});

		if(diskon > 100) {
			totalAmount = tempTotal - diskon;
			$('#totalAmountDiskon').html(formatCurrencyComa(parseFloat(diskon)));
		}else {
			var tempTotalvDiskon = 0;
			tempDiskon			= diskon / 100;
			tempTotalvDiskon	= tempTotal * tempDiskon;
			totalAmount 		= (tempTotal - tempTotalvDiskon);
			$('#totalAmountDiskon').html(diskon+'%');
		}

		$('#totalAmountRow').html(formatCurrencyComa(parseFloat(tempTotal)));		
		$('#totalAmount').html(formatCurrencyComa(parseFloat(totalAmount)));
		$('#total_amount').val(totalAmount);
	}

	$('#add_mainOrder').on('submit', (function(e) {
		cal_amount();
		arrNoDm 	= [];
		arrPrice 	= [];
		arrRemark 	= [];

		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();

		$('select[name="no_dm[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '')  arrNoDm.push(this.value);
			}
		})

		$('input[name="dm_price[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '')  arrPrice.push(this.value);
			}
		})

		$('textarea[name="desc_main[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arrRemark.push(this.value);
				else arrRemark.push('NULL');
			}else arrRemark.push('NULL');
		})

		if(arrNoDm.length > 0 && arrPrice.length > 0 && arrRemark.length > 0) {
			var formData = new FormData();
			formData.set('delivery_date',	$('#delivery_date').val());
			formData.set('cust_id',			$('#cust_id').val());
			formData.set('term_of_pay',		$('#term_of_pay').val());
			formData.set('valas',			$('#valas').val());
			formData.set('rate',			$('#rate').val());
			formData.set('diskon',			$('#diskon').val());
			formData.set('tujuan',			$('#tujuan').val());
			formData.set('total_amount',	$('#total_amount').val());
			formData.set('arrNoDm',			arrNoDm);
			formData.set('arrPrice',		arrPrice);
			formData.set('arrRemark',		arrRemark);

			$.ajax({
				type:'POST',
				url: $(this).attr('action'),
				data: formData,
				cache:false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function () {
							$('#panel-modal').modal('toggle');
							listMainOrder();
						})
					}else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Tambah Maintenance Order");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Tambah Maintenance Order");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}else {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah Maintenance Order");
			swal("Failed!", "Invalid Inputan List Maintenance, silahkan cek kembali.", "error");
		}
	}));
  </script>