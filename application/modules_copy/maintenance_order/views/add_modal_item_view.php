	<style>
		#loading-us {
			display: none
		}

		#tick {
			display: none
		}

		.form-item {
			margin-top: 15px;
			overflow: auto;
		}

		.items-add {
			cursor: pointer;
		}

		.items-add:hover {
			color: #ff8c00
		}
	</style>

	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<br>
	<!-- <div class="item form-group form-item">
			<div class="col-md-8 col-sm-6 col-xs-12 items-add" onclick="add_new_item()">
				<button type="button" class="btn btn-icon waves-effect waves-light m-b-5"><i class="fa fa-plus"></i></button> Tambah Barang Baru
			</div>
		</div> -->
	<hr>
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Tipe Barang <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control type_item" name="type_item" id="type_item" style="width: 100%" required>
				<option value="">Tidak ada Type Material</option>	
				<?php foreach ($type_material as $key) { ?>
					<option value="<?php echo $key['id_type_material'] ; ?>"><?php echo $key['type_material_name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Barang <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control item_name" name="item_name" id="item_name" style="width: 100%" required>
			</select>
		</div>
	</div>
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty">Qty <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="text" id="qty" name="qty" class="form-control col-md-7 col-xs-12" placeholder="Qty" min="1" maxlength="7" value="1" onkeypress="javascript:return isNumber(event)" required="required">
		</div>
	</div>


	<div class="item form-group form-item" style="overflow-y: hidden;height: 37.99px;">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_diperlukan">Tanggal Diperlukan </label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<div class="input-group date">
				<input placeholder="Tanggal Diperlukan" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="tanggal_diperlukan" name="tanggal_diperlukan" value="<?php echo date('Y-m-d'); ?>">
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
	</div>

	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="gudang_id">Gudang <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="gudang_id" id="gudang_id" style="width: 100%" required>
				<?php foreach ($gudang as $key) { ?>
					<option value="<?php echo $key['id_gudang'] . ',' . $key['nama_gudang']; ?>"><?php echo $key['nama_gudang']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<!-- <div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_bc">No BC Keluar <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="no_bc" id="no_bc" style="width: 100%" required>
				<option value="">Tidak ada No BC Masuk</option>	
				<?php foreach ($bcs as $pend =>$bc) { ?>
					<option value="<?php echo $bc['id'];?>"><?php echo $bc['no_pendaftaran']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_bc">No BC Keluar <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="no_bc" id="no_bc" style="width: 100%" required>
				<option value="">Tidak ada No BC Keluar</option>	
				<?php foreach ($bcs as $pend =>$bc) { ?>
					<option value="<?php echo $bc['id'];?>"><?php echo $bc['no_pendaftaran']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div> -->
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="keterangan">Keterangan</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<textarea data-parsley-maxlength="255" type="text" id="desc" name="desc" class="form-control col-md-7 col-xs-12" placeholder="Keterangan"></textarea>
		</div>
	</div>

	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submititem" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Barang</button>
		</div>
	</div>

	<script type="text/javascript">
		var addNew = [];
		$(document).ready(function() {
			$('form').parsley();
			$('[data-toggle="tooltip"]').tooltip();

			$("#tanggal_diperlukan").datepicker({
				format: 'yyyy-mm-dd',
				autoclose: true,
				todayHighlight: true,
			});

			$('#type_item').select2();
			$('#item_name').select2();
			$('#gudang_id').select2();
			
		});
		$('#type_item').on('change', (function(e) {
			$('#item_name').find('option').remove();
			$.ajax({
					type:'POST',
					url: '<?php echo base_url('maintenance/search_material');?>'+'/'+$('#type_item').val(),
					cache:false,
					contentType: false,
					processData: false,
					success: function(response) {
						var textOption = "<option value=''>Pilih Material !</option>"
						response.forEach(function(response){
							// $key['id'] . ',' . $key['stock_name'] . ',' . $key['id_uom'] . ',' . $key['uom_name'];
							textOption+="<option value='"+response.id+","+response.stock_name+","+response.id_uom+","+response.uom_name+"'>"+
								response.stock_code+"|"+response.stock_name+"|"+response.uom_name+"|"+response.qty+"|"+
								"</option>"
						})
						$('#item_name').append(textOption); 
					}
			});
		}));
		$('#btn-submititem').on('click', (function(e) {
			if ($('#item_name').val() === '') {
				swal("Warning", "Nama Barang harus diisi.", "error");
			} else {
				$(this).attr('disabled', 'disabled');
				$(this).text("Memasukkan data...");

				var vals = $('#item_name').val();
				var vals2 = $('#gudang_id').val();

				var splits = vals.split(',');
				console.log(splits);
				var splits2 = vals2.split(',');

				var iditems = splits[0];
				var itemname = splits[1];
				var satuans = splits[2];
				var nama_satuan = splits[3];

				var idgudang = splits2[0];
				var nama_gudang = splits2[1];

				var iditems = iditems,
					item_real_name = itemname,
					item_name = iditems,
					unit = satuans,
					qty = $('#qty').val(),
					tgl_diperlukan = $('#tanggal_diperlukan').val(),
					desc = $('#desc').val(),
					gudang_id = idgudang,
					gudang = nama_gudang,
					unittext = nama_satuan;

				if ($('#qty').val() === '') {
					qty = $('#qty').val(1);
				}

				var dataitem = {
					iditems,
					item_real_name,
					item_name,
					unit,
					qty,
					tgl_diperlukan,
					gudang_id,
					gudang,
					desc,
					unittext
				};
				items.push(dataitem);
				$('#panel-modalchild .panel-heading button').trigger('click');
				// reloaditems();
				itemsElem();
			}

		}));

		function addNewItems(items) {
			var kode_stok = items["kode_stok"];
			var nama_stok = items["nama_stok"];
			var uomId = items["uomId"];
			var namaUom = items["namaUom"];
			var qty2 = items["qty2"];
			var qty_null = items["qty"];
			var desk_stok = items["desk_stok"];
			var gudangId = items["gudangId"];
			var namaGudang = items["namaGudang"];

			$("#item_name").val(nama_stok + ',' + uomId + ',' + namaUom);
			if (qty2 != '') {
				$("#qty").val(qty2);
			} else {
				$("#qty").val(1);
			}
			$("#gudang_id").val(gudangId + ',' + namaGudang);
			$("#desc").val(desk_stok);
			$("#notes").val(desk_stok);

		}

		function isNumber(evt) {
			var iKeyCode = (evt.which) ? evt.which : evt.keyCode
			if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
				return false;

			return true;
		}
	</script>