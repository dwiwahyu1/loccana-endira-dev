<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-center {
    text-align: center;
  }

  .dt-body-right {
    text-align: right;
  }

  #listItemMaintenance {
    counter-reset: rowNumber;
  }

  #listItemMaintenance tbody tr>td:first-child {
    counter-increment: rowNumber;
    text-align: center;
  }

  #listItemMaintenance tbody tr td:first-child::before {
    content: counter(rowNumber);
    min-width: 1em;
    margin-right: 0.5em;
  }
</style>

<form class="form-horizontal form-label-left" id="status_mainOrder" role="form" action="<?php echo base_url('maintenance_order/status_mainOrder'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_mo">No Maintenance Order</label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="text" class="form-control" id="no_mo" name="no_mo" placeholder="No Maintenance Order" autocomplete="off"  value="<?php
                                                                                                                                if (isset($mainOrder[0]['no_main_order'])) echo $mainOrder[0]['no_main_order'];
                                                                                                                                ?>" readonly>
    </div>
  </div>
  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="delivery_date">Delivery Date</label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="text" class="form-control" id="delivery_date" name="delivery_date" value="<?php
                                                                                              if (isset($mainOrder[0]['delivery_date'])) echo date('d-M-Y', strtotime($mainOrder[0]['delivery_date']));
                                                                                              ?>" placeholder="Work Date" readonly>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cust_id">Customer Name</label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <select class="form-control" id="cust_id" name="cust_id" style="width: 100%" disabled>
        <option value="">-- Pilih Customer --</option>
        <?php
        foreach ($customer as $ck => $cv) { ?>
          <option value="<?php echo $cv['id']; ?>" <?php
                                                    if (isset($mainOrder[0]['id_cust'])) {
                                                      if ($cv['id'] == $mainOrder[0]['id_cust']) echo "selected";
                                                    } ?>><?php echo $cv['name_eksternal']; ?></option>
        <?php
        }
        ?>
      </select>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="term_of_pay">Term of Payment</label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <select class="form-control" id="term_of_pay" name="term_of_pay" style="width: 100%" readonly>
        <option value="<?php echo $mainOrder[0]['term_of_payment']; ?>"><?php echo $mainOrder[0]['term_of_payment']; ?></option>
      </select>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="valas">Valas</label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <select class="form-control" id="valas" name="valas" style="width: 100%" readonly>
        <?php foreach ($valas as $vd) {
          if (isset($mainOrder[0]['id_valas'])) {
            if ($vd['valas_id'] == $mainOrder[0]['id_valas']) { ?>
              <option value="<?php echo $vd['valas_id']; ?>" selected><?php echo $vd['nama_valas']; ?></option>
        <?php }
          }
        } ?>
      </select>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rate">Rate</label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="number" class="form-control" id="rate" name="rate" placeholder="Rate" autocomplete="off" step=".0001" value="<?php
                                                                                                                                if (isset($mainOrder[0]['rate'])) echo $mainOrder[0]['rate'];
                                                                                                                                ?>" readonly>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="diskon">Diskon</label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="number" class="form-control" id="diskon" name="diskon" placeholder="Diskon" autocomplete="off" step=".0001" value="<?php
                                                                                                                                      if (isset($mainOrder[0]['diskon'])) echo $mainOrder[0]['diskon'];
                                                                                                                                      ?>" readonly>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tujuan">No BTB</label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="text" class="form-control" id="tujuan" name="tujuan" placeholder="Tujuan" value="<?php
                                                                                                    if (isset($mainOrder[0]['tujuan'])) echo $mainOrder[0]['tujuan'];
                                                                                                    ?>" autocomplete="off" readonly>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Daftar Maintenance : </label>
    <div class="col-md-8 col-sm-6 col-xs-12"></div>
  </div>

  <div class="item form-group">
    <table id="listItemMaintenance" class="table table-striped table-bordered dt-responsive nowrap" width="100%">
      <thead>
        <tr>
          <th class="dt-body-center" style="width: 5%;">No</th>
          <th style="width: 15%;">No Maintenance</th>
          <th>Nama Maintenance</th>
          <th>Nama Mesin</th>
          <th class="dt-body-center" style="width: 5%;">QTY</th>
          <th class="dt-body-center" style="width: 5%;">UOM</th>
          <th class="dt-body-right" style="width: 15%;">Harga</th>
          <th class="dt-body-right" style="width: 15%;">Amount</th>
          <th style="width: 20%;">Remark</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $i = 1;
        $rowAmount       = 0;
        $totalAmountRow    = 0;
        $totalAmount     = 0;
        if (sizeof($detail) > 0) {
          foreach ($detail as $dk) {
            $rowAmount     = (floatval($dk['unit_price'])) * (floatval($dk['qty']));
            $totalAmountRow = $totalAmountRow + $rowAmount; ?>
            <tr id="trRowItem<?php echo $i; ?>">
              <input type="hidden" id="id_material" name="id_material[]" value="<?php echo $dk['id_material'];?>">
              <input type="hidden" id="qty" name="qty[]" value="<?php echo $dk['qty'];?>">
              <td></td>
              <td><?php echo $dk['no_main']; ?></td>
              <td><?php echo $dk['nama_main']; ?></td>
              <td><?php echo $dk['stock_name']; ?></td>
              <td class="dt-body-center"><?php echo number_format($dk['qty'], 0); ?></td>
              <td class="dt-body-center"><?php echo $dk['uom_name']; ?></td>
              <td class="dt-body-right"><?php echo $dk['symbol_valas'] . ' ' . number_format($dk['unit_price'], 4, ',', '.'); ?></td>
              <td class="dt-body-right"><?php echo $dk['symbol_valas'] . ' ' . number_format($rowAmount, 4, '.', ',') ?></td>
              <td><?php echo $dk['remark']; ?></td>
            </tr>
          <?php
            $i++;
          }
        } else { ?>
          <tr>
            <td></td>
            <td></td>
            <td></td>
            <td class="dt-body-center"></td>
            <td class="dt-body-center"></td>
            <td class="dt-body-right"></td>
            <td class="dt-body-right"></td>
            <td></td>
          </tr>
        <?php
        }

        if ($dk['diskon'] >= 100) {
          $totalAmount = $totalAmountRow - $dk['diskon'];
        } else {
          $tempDiskon   = $dk['diskon'] / 100;
          $tempTotal     = $totalAmountRow * $tempDiskon;
          $totalAmount   = ($totalAmountRow - $tempTotal);
        }
        ?>
      </tbody>
      <tfoot>
        <tr>
          <td class="dt-body-right" colspan="5" rowspan="3"></td>
          <td class="dt-body-right">Total</td>
          <td class="dt-body-right"><label><?php echo $dk['symbol_valas'] . ' ' . number_format($totalAmountRow, 4, ',', '.'); ?></label></td>
          <td colspan="2" rowspan="3"></td>
        </tr>
        <tr>
          <td class="dt-body-right">Diskon</td>
          <td class="dt-body-right"><?php
                                    if ($dk['diskon'] >= 100) echo $dk['symbol_valas'] . ' ' . number_format($dk['diskon'], 4, ',', '.');
                                    else echo $dk['diskon'] . '%';
                                    ?></td>
        </tr>
        <tr>
          <td class="dt-body-right">Total Amount</td>
          <td class="dt-body-right">
            <label><?php echo $dk['symbol_valas'] . ' ' . number_format($totalAmount, 4, ',', '.'); ?></label>
            <input type="hidden" id="total_amount_last" name="total_amount_last" value="<?php echo $totalAmount ?>">
            <input type="hidden" id="total_amount_first" name="total_amount_first" value="<?php echo $totalAmountRow ?>">
          </td>
        </tr>
      </tfoot>
    </table>
  </div>

  <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Status <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <select class="form-control" id="status" name="status" required>
        <option value="1">Accept</option>
        <option value="2">Reject</option>
      </select>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="notes">Notes</label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <textarea class="form-control" id="notes" name="notes"></textarea>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Status</button>
      <input type="hidden" id="id_mo" name="id_mo" value="<?php if (isset($mainOrder[0]['id_mo'])) echo $mainOrder[0]['id_mo']; ?>">
    </div>
  </div>
</form>

<script type="text/javascript">
  $(document).ready(function() {
    $("#delivery_date").datepicker({
      format: 'dd-M-yyyy',
      autoclose: true,
      todayHighlight: true,
    });

    $('#cust_id').select2();
    $('#term_of_pay').select2();
    $('#valas').select2();
  })

  $('#status_mainOrder').on('submit', (function(e) {
    $('#btn-submit').attr('disabled', 'disabled');
    $('#btn-submit').text("Memasukkan data...");
    e.preventDefault();

    if ($('#id_po_spb').val() != '') {
      var formData = new FormData(this);
      $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
          if (response.success == true) {
            swal({
              title: 'Success!',
              text: response.message,
              type: 'success',
              showCancelButton: false,
              confirmButtonText: 'Ok'
            }).then(function() {
              $('#panel-modal').modal('toggle');
              listMainOrder();
            })
          } else {
            $('#btn-submit').removeAttr('disabled');
            $('#btn-submit').text("Edit Status");
            swal("Failed!", response.message, "error");
          }
        }
      }).fail(function(xhr, status, message) {
        $('#btn-submit').removeAttr('disabled');
        $('#btn-submit').text("Edit Status");
        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
      });
    } else {
      $('#btn-submit').removeAttr('disabled');
      $('#btn-submit').text("Edit Status");
      swal("Failed!", "Barang tidak ada yang di pilih, silahkan cek kembali.", "error");
    }
  }))
</script>