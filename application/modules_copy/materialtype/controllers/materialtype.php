<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Materialtype extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('materialtype/materialtype_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function mail_service($result) {
		$name = 'Cashier App';
		$from = 'mail@google.com';
		$to = $result['mail_to']; //change this
		$subject = $result['mail_subject']; //change this

		$header = $result['mail_header']; //change this
		$body = $result['mail_body']; //change this
		$footer = $result['mail_footer']; //change this
		// Timpa isi email dengan data
		$a = array('xxheaderxx', 'xxbodyxx', 'xxfooterxx');
		$b = array($header, $body, $footer);

		$template = APPPATH . 'modules/template/email_default.html';
		$fd = fopen($template, "r");
		$message = fread($fd, filesize($template));
		fclose($fd);
		$message = str_replace($a, $b, $message);


		$this->load->library('email'); // load email library
		$this->email->from($from, $name);
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($message);
		if ($this->email->send()) {
			return 1;
		} else {
			return 0;
		}
	}

	public function index() {
		$this->template->load('maintemplate', 'materialtype/views/index');
	}

	function lists() {

		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

		$order_fields = array('', 'type_material_name', 'type_material_description');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->materialtype_model->lists($params);
		//print_r($list);die;

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$status_akses = 
				'<div class="btn-group"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="editmaterial(\'' . $v['id_type_material'] . '\')">'.
					'<i class="fa fa-edit"></i></button>'.
				'</div>'.
				'<div class="btn-group"><button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deletematerial(\'' . $v['id_type_material'] . '\')">'.
					'<i class="fa fa-trash"></i></button>'.
				'</div>';
			array_push($data, array(
				$i,
				$v['type_material_name'],
				$v['type_material_description'],
				$status_akses
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add() {
		//$result = $this->materialtype_model->group();

		$data = array(
			'group' => ''
		);

		$this->load->view('add_modal_view', $data);
	}
	
	public function edit($id) {
		$result = $this->materialtype_model->edit($id);
		
		$data = array(
			'uom' => $result
		);

		$this->load->view('edit_modal_view', $data);
	}

	public function deletes() {
		$data 	= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		
		$result = $this->materialtype_model->deletes($params['id']);
		if($result > 0) {
			$this->log_activity->insert_activity('delete', 'Delete Type Material id : '.$params['id']);
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}else {
			$this->log_activity->insert_activity('delete', 'Gagal Delete Type Material id : '.$params['id']);
			$res = array('status' => 'failed', 'message' => 'Data gagal di hapus');
		}
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function edit_material() {
		$this->form_validation->set_rules('nama_tipe_material', 'nama_tipe_material', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('desc_tipe_material', 'desc_tipe_material', 'trim|required|max_length[255]');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$id_type_material = $this->Anti_sql_injection($this->input->post('id_type_material', TRUE));
			$type_material_name = $this->Anti_sql_injection($this->input->post('nama_tipe_material', TRUE));
			$type_material_description = $this->Anti_sql_injection($this->input->post('desc_tipe_material', TRUE));
			$message = "";

			$data = array(
				'id_type_material' => $id_type_material,
				'type_material_name' => $type_material_name,
				'type_material_description' => $type_material_description
			);
			// $data = $this->security->xss_clean($data);

			$result = $this->materialtype_model->edit_material($data);
			if ($result > 0) {
				$this->log_activity->insert_activity('update', 'Update Type Material id : '.$id_type_material);
				$result = array('success' => true, 'message' => 'Berhasil mengubah Type Material ke database');
			}else {
				$this->log_activity->insert_activity('update', 'Gagal Update Type Material id : '.$id_type_material);
				$result = array('success' => false, 'message' => 'Gagal mengubah Type Material ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function add_material() {
		$this->form_validation->set_rules('nama_tipe_material', 'nama_tipe_material', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('desc_tipe_material', 'desc_tipe_material', 'trim|required|max_length[255]');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$user_id = $this->session->userdata['logged_in']['user_id'];
			$nama_tipe_material = $this->Anti_sql_injection($this->input->post('nama_tipe_material', TRUE));
			$desc_tipe_material = $this->Anti_sql_injection($this->input->post('desc_tipe_material', TRUE));
			$message = "";

			$data = array(
				'user_id' => $user_id,
				'nama_tipe_material' => $nama_tipe_material,
				'desc_tipe_material' => $desc_tipe_material
			);
			// $data = $this->security->xss_clean($data);

			$result = $this->materialtype_model->add_material($data);

			if ($result > 0) {
				$this->log_activity->insert_activity('insert', 'Insert Type Stok');
				$result = array('success' => true, 'message' => 'Berhasil menambahkan Type Stok ke database');
			}else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert Type Stok');
				$result = array('success' => false, 'message' => 'Gagal menambahkan Type Stok ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

}
