<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Materialtype_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
    
    public function edit($id)
	{
		$sql 	= 'CALL type_mate_search_id(?)';
		$query 	=  $this->db->query($sql,array($id));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function lists($params = array())
	{
		// print_r($params);die;
		$sql 	= 'CALL type_mate_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
		
		// $sql_all 	= 'CALL type_mate_list_all(?, ?, ?, ?, ?, @total_filtered, @total)';

		// $out2 = array();
		// $query_all 	=  $this->db->query($sql_all,
			// array(
				// NULL,
				// NULL,
				// NULL,
				// NULL,
				// ''
			// ));

		// $result_all = $query_all->result_array();
		// $total_row = $result_all[0]['count_all'];
		
		// $this->load->helper('db');
		// free_result($this->db->conn_id);
		// //print_r($result_all[0]['count_all']);die;

		// $sql 	= 'CALL type_mate_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		// $out = array();
		// $query 	=  $this->db->query($sql,
			// array(
				// $params['limit'],
				// $params['offset'],
				// $params['order_column'],
				// $params['order_dir'],
				// $params['filter']
			// ));

		// $result = $query->result_array();

		// //print_r();die;

		// $this->load->helper('db');
		// free_result($this->db->conn_id);

		// $total = $this->db->query('select @total_filtered, @total')->row_array();
		// //print_r($total);die;


		// $return = array(
			// 'data' => $result,
			// 'total_filtered' => $total['@total_filtered'],
			// 'total' => $total['@total'],
		// );

		// return $return;
	}
	
	public function edit_material($data)
	{
		$sql 	= 'CALL type_mate_update(?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id_type_material'],
				$data['type_material_name'],
				$data['type_material_description']
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function add_material($data)
	{
		$sql 	= 'CALL type_mate_add(?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['nama_tipe_material'],
				$data['desc_tipe_material']
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	
	public function deletes($data)
	{
		$sql 	= 'CALL type_mate_delete(?)';

		$query 	=  $this->db->query($sql,
			array(
				$data
			));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}


}
