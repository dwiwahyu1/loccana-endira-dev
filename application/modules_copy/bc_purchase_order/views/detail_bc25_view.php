<style type="text/css">
	.separator {
		display: flex;
		align-items: center;
		text-align: center;
	}
	
	.separator::before, .separator::after {
		content: '';
		flex: 1;
		border-bottom: 1px solid #000;
		border-style: dashed;
	}

	.separator::before {
		margin-right: .25em;
	}

	.separator::after {
		margin-left: .25em;
	}

	h4 { line-height: 20px !important; }
	.table {border-color: black !important}
	.table thead tr td, th {border-color: black !important}
	.table tbody tr td, th {border-color: black !important}
	.table tfoot tr td, th {border-color: black !important}
</style>

<!-- Start Lembar 1 -->
<div class="card-box" style="margin-top: 20px;">
	<div class="btn-group pull-left" style="padding-bottom: 20px;">
		<a class="btn btn-info btn-sm" id="back_bc" name="back_bc" data-toggle="tooltip" data-placement="top" title="Back">
			<i class="fa fa-arrow-left"></i>
		</a>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border: 0px;">
				<tr style="font-size: 20px;">
					<th style="border: 0px; padding: 0px;" class="dt-body-center"><?php
						if(isset($bc[0]['desc_bc'])) echo strtoupper($bc[0]['desc_bc']);
					?></th>
				</tr>
				<tr>
					<th style="border: 0px; padding: 0px;" class="dt-body-right"><?php
						if(isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']);
					?></th>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<tr>
					<td style="border: 0px; width: 15%;">Kantor Pabean</td>
					<td style="border: 0px;">: &nbsp; KPPC BANDUNG</td>
					<th style="border: 0px; width: 15%;" rowspan="3">Halaman Ke-1 dari 4</th>
				</tr>
				<tr>
					<td style="border: 0px; width: 15%;">Nomor Pengajuaan</td>
					<td style="border: 0px;">: &nbsp; <?php if(isset($bc[0]['no_pengajuan'])) echo $bc[0]['no_pengajuan']; ?></td>
				</tr>
				<tr>
					<td style="border: 0px; width: 15%;">A. Jenis TPB</td>
					<td style="border: 0px;">: &nbsp; 1. Kawasan Berikat &nbsp; 2. Gudang Serikat &nbsp; 3. TPPB &nbsp; 4.TBB &nbsp; 5. TLB &nbsp; 6. KDUB &nbsp; 7. Lainnya</td>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<tr>
					<td style="width: 50%;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
							<thead>
								<tr>
									<th style="padding-bottom: 0px;" colspan="2">B. DATA PEMBERITAHUAN PENYELENGGARA/PENGUSAHA TPB</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="border: 0px; width: 25%;">1. NPWP</td>
									<td style="border: 0px;">: &nbsp;</td>
								</tr>
								<tr>
									<td style="border: 0px; width: 25%;">2. Nama</td>
									<td style="border: 0px;">: &nbsp;</td>
								</tr>
								<tr>
									<td style="border: 0px; width: 25%;">3. Alamat</td>
									<td style="border: 0px;">: &nbsp;</td>
								</tr>
								<tr>
									<td style="border: 0px; width: 25%;">4. No Izin TPB</td>
									<td style="border: 0px;">: &nbsp;</td>
								</tr>
								<tr>
									<td style="border: 0px; width: 25%;">5. API</td>
									<td style="border: 0px;">: &nbsp;</td>
								</tr>
							</tbody>
						</table>
					</td>
					<td style="width: 50%;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
							<thead>
								<tr>
									<th style="padding-bottom: 0px;" colspan="2">D. DIISI OLEH BEA DAN CUKAI</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="border: 0px; width: 30%;">Nomor Pendaftaraan</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($bc[0]['no_pendaftaran'])) echo $bc[0]['no_pendaftaran'];
									?></td>
								</tr>
								<tr>
									<td style="border: 0px; width: 30%;">Tanggal</td>
									<td style="border: 0px;">: &nbsp; <?php
										if(isset($bc[0]['tanggal_pengajuan'])) echo date('d-m-Y', strtotime($bc[0]['tanggal_pengajuan']));
									?></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<tr>
					<th style="padding-left: 15px; padding-bottom: 0px; width: 50%;">B. DATA PEMBERITAHUAN PENYELENGGARA/PENGUSAHA TPB</th>
					<th style="padding-left: 15px; padding-bottom: 0px; width: 50%;">D. DIISI OLEH BEA DAN CUKAI</th>
				</tr>
				<tr>
					<td style="border-top: 0px; border-bottom: 0px; padding-top: 0px; padding-bottom: 0px;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; padding-top: 0px;">
							<tr>
								<td style="border: 0px; width: 30%;">1. NPWP</td>
								<td style="border: 0px;">: &nbsp;</td>
							</tr>
						</table>
					</td>
					<td style="border-top: 0px; border-bottom: 0px; padding-top: 0px; padding-bottom: 0px;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; padding-top: 0px;">
							<tr>
								<td style="border: 0px; width: 30%;">Nomor Pendaftaraan</td>
								<td style="border: 0px;">: &nbsp; <?php
									if(isset($bc[0]['no_pendaftaran'])) echo $bc[0]['no_pendaftaran'];
								?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="border-top: 0px; border-bottom: 0px; padding-top: 0px; padding-bottom: 0px;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; padding-top: 0px;">
							<tr>
								<td style="border: 0px; width: 30%;">2. Nama</td>
								<td style="border: 0px;">: &nbsp;</td>
							</tr>
						</table>
					</td>
					<td style="border-top: 0px; border-bottom: 0px; padding-top: 0px; padding-bottom: 0px;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; padding-top: 0px;">
							<tr>
								<td style="border: 0px; width: 30%;">Tanggal</td>
								<td style="border: 0px;">: &nbsp; <?php
									if(isset($bc[0]['tanggal_pengajuan'])) echo date('d-m-Y', strtotime($bc[0]['tanggal_pengajuan']));
								?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="border-top: 0px; border-bottom: 0px; padding-top: 0px; padding-bottom: 0px;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; padding-top: 0px;">
							<tr>
								<td style="border: 0px; width: 30%;">3. Alamat</td>
								<td style="border: 0px;">: &nbsp;</td>
							</tr>
						</table>
					</td>
					<td style="border-bottom: 0px; padding-top: 0px; padding-bottom: 0px;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; padding-top: 0px;">
							<tr>
								<td style="border: 0px; width: 46%;">14. Invoice</td>
								<td style="border: 0px; width: 34%;">: &nbsp;</td>
								<td style="border: 0px;">Tgl. </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="border-top: 0px; border-bottom: 0px; padding-top: 0px; padding-bottom: 0px;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; padding-top: 0px;">
							<tr>
								<td style="border: 0px; width: 30%;">4. No Izin TPB</td>
								<td style="border: 0px;">: &nbsp;</td>
							</tr>
						</table>
					</td>
					<td style="border-top: 0px; border-bottom: 0px; padding-top: 0px; padding-bottom: 0px;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; padding-top: 0px;">
							<tr>
								<td style="border: 0px; width: 46%;">15. Packing List</td>
								<td style="border: 0px; width: 34%;">: &nbsp;</td>
								<td style="border: 0px;">Tgl. </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="border-top: 0px; border-bottom: 0px; padding-top: 0px; padding-bottom: 0px;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; padding-top: 0px;">
							<tr>
								<td style="border: 0px; width: 30%;">5. API</td>
								<td style="border: 0px;">: &nbsp;</td>
							</tr>
						</table>
					</td>
					<td style="border-top: 0px; border-bottom: 0px; padding-top: 0px; padding-bottom: 0px;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; padding-top: 0px;">
							<tr>
								<td style="border: 0px; width: 46%;">16. Kontrak</td>
								<td style="border: 0px; width: 34%;">: &nbsp;</td>
								<td style="border: 0px;">Tgl. </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<th style="border-bottom: 0px; padding-left: 15px; padding-bottom: 0px;">PEMILIK BARANG</th>
					<td style="border-top: 0px; border-bottom: 0px; padding-top: 0px; padding-bottom: 0px;" rowspan="3">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; padding-top: 0px;">
							<tr>
								<td style="border: 0px; width: 46%;">17. Fasilitas Impor</td>
								<td style="border: 0px; width: 34%;">: &nbsp;</td>
								<td style="border: 0px;">Tgl. </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="border-top: 0px; border-bottom: 0px; padding-top: 0px; padding-bottom: 0px;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; padding-top: 0px;">
							<tr>
								<td style="border: 0px; width: 30%;">6. NPWP</td>
								<td style="border: 0px;">: &nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="border-top: 0px; border-bottom: 0px; padding-top: 0px; padding-bottom: 0px;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; padding-top: 0px;">
							<tr>
								<td style="border: 0px; width: 30%;">7. Nama</td>
								<td style="border: 0px;">: &nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="border-top: 0px; border-bottom: 0px; padding-top: 0px; padding-bottom: 0px;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; padding-top: 0px;">
							<tr>
								<td style="border: 0px; width: 30%;">8. Alamat</td>
								<td style="border: 0px;">: &nbsp;</td>
							</tr>
						</table>
					</td>
					<td style="border-top: 0px; padding-top: 0px; padding-bottom: 0px;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; padding-top: 0px;">
							<tr>
								<td style="border: 0px; width: 46%;">18. Surat Keputusan / Dokumen Lainnya</td>
								<td style="border: 0px; width: 34%;">: &nbsp;</td>
								<td style="border: 0px;">Tgl. </td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<tr>
					<td style="border-top: 0px; border-bottom: 0px; width: 49.99%;" rowspan="3">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
							<thead>
								<tr>
									<th style="padding-top: 0px; padding-bottom: 0px; border: 0px;" colspan="2">PENERIMA BARANG</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="border: 0px; width: 30%;">9. NPWP</td>
									<td style="border: 0px;">: &nbsp;</td>
								</tr>
								<tr>
									<td style="border: 0px; width: 30%;">10. Nama</td>
									<td style="border: 0px;">: &nbsp;</td>
								</tr>
								<tr>
									<td style="border: 0px; width: 30%;">11. Alamat</td>
									<td style="border: 0px;">: &nbsp;</td>
								</tr>
								<tr>
									<td style="border: 0px; width: 30%;">12. NIPER</td>
									<td style="border: 0px;">: &nbsp;</td>
								</tr>
								<tr>
									<td style="border: 0px; width: 30%;">13. API</td>
									<td style="border: 0px;">: &nbsp;</td>
								</tr>
							</tbody>
						</table>
					</td>
					<td style="width: 25%; padding-bottom: 29px;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
							<tr>
								<td style="border: 0px;  width: 35%;">19. Valuta</td>
								<td style="border: 0px;">: &nbsp;</td>
							</tr>
						</table>
					</td>
					<td style="width: 25%; padding-bottom: 29px;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
							<tr>
								<td style="border: 0px; width: 35%;">20. NDPBM</td>
								<td style="border: 0px;">: &nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="width: 25%;" colspan="2">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
							<tr>
								<td style="border: 0px; width: 35%;">21. Nilai CIF</td>
								<td style="border: 0px;">: &nbsp;</td>
							</tr>
							<tr>
								<td style="border: 0px; width: 35%;">22. Harga Penyerahan</td>
								<td style="border: 0px;">: &nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td style="width: 25%;" colspan="2">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
							<tr>
								<td style="border: 0px; width: 35%;">23. Jenis Sarana Pengangkut</td>
								<td style="border: 0px;">: &nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<tr>
					<td style="width: 75%">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
							<thead>
								<tr>
									<td style="border: 0px; width: 50%;">24. Nomor, Ukuran dan Tipe Peti Kemas</td>
									<td style="border: 0px; width: 50%;">25. Jumlah, Jenis dan Merek Kemasan</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="border: 0px;"></td>
									<td style="border: 0px;"></td>
								</tr>
							</tbody>
						</table>
					</td>
					<td style="width: 25%">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
							<tr>
								<td style="border: 0px; width: 51%;">26. Berat Kotor (Kg)</td>
								<td style="border: 0px;">: &nbsp;</td>
							</tr>
							<tr>
								<td style="border: 0px; width: 51%;">27. Berat Bersih (Kg)</td>
								<td style="border: 0px;">: &nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<td style="width: 5%; padding-left: 16px;">28. No</td>
						<td style="width: 22%; padding-left: 16px;">29. <ul style="margin: -20px 0px 0px 0px; padding: 0px 0px 0px 45px;">
							<li>Pos Tarif/HS</li>
							<li>Kode Barang</li>
							<li>Uraian barang secara lengkap, merk, tipe, ukuran, spesifikasi lain</li>
							<li>Fasilitas Impor</li>
							<li>Surat Keputusan/Dokumen Lainnya</li>
						</ul></td>
						<td style="width: 10%; padding-left: 16px;">30. <ul style="margin: -20px 0px 0px 0px; padding: 0px 0px 0px 45px;">
							<li>Kategori Barang</li>
							<li>Kondisi Barang</li>
						</ul></td>
						<td style="width: 12%; padding-left: 16px;">31. <ul style="margin: -20px 0px 0px 0px; padding: 0px 0px 0px 45px;">
							<li>Tarif dan Fasilitas</li>
							<li>BM</li>
							<li>BMT</li>
							<li>Cukai</li>
							<li>PPN</li>
							<li>PPnBM</li>
							<li>PPh</li>
						</ul></td>
						<td style="width: 12%; padding-left: 16px;">32. <ul style="margin: -20px 0px 0px 0px; padding: 0px 0px 0px 45px;">
							<li>Jumlah dan Jenis Satuan</li>
							<li>Berat Bersih (Kg)</li>
							<li>Jumlah dan Jenis Kemasan</li>
						</ul></td>
						<td style="width: 10%; padding-left: 16px;">33. <ul style="margin: -20px 0px 0px 0px; padding: 0px 0px 0px 45px;">
							<li>Nilai CIF</li>
							<li>Harga Penyerahan</li>
						</ul></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="7">
							<div style="height: 70px;" class="col-sm-12 separator">
								<h5>0 Jenis Barang. Lihat lembar lanjutan</h5>
							</div>
						</td>
					</tr>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<td class="dt-body-center" colspan="2">Jenis Pungutan</td>
						<td style="width: 17%;" class="dt-body-center">Dibayar (Rp)</td>
						<td style="width: 17%;" class="dt-body-center">Dibebaskan (Rp)</td>
						<td style="width: 17%;" class="dt-body-center">Ditanggung Pemerintah (Rp)</td>
						<td style="width: 17%;" class="dt-body-center">Sudah Dilunasi (Rp)</td>
					</tr>
				</thead>
				<tbody>
					<tr>
					<td style="width: 5%;" class="dt-body-center">34.</td>
					<td>BM</td>
					<td class="dt-body-right">0</td>
					<td class="dt-body-right">0</td>
					<td class="dt-body-right">0</td>
					<td class="dt-body-right">0</td>
				</tr>
				<tr>
					<td style="width: 5%;" class="dt-body-center">35</td>
					<td>BMT</td>
					<td class="dt-body-right">0</td>
					<td class="dt-body-right">0</td>
					<td class="dt-body-right">0</td>
					<td class="dt-body-right">0</td>
				</tr>
				<tr>
					<td style="width: 5%;" class="dt-body-center">36.</td>
					<td>Cukai</td>
					<td class="dt-body-right">0</td>
					<td class="dt-body-right">0</td>
					<td class="dt-body-right">0</td>
					<td class="dt-body-right">0</td>
				</tr>
				<tr>
					<td style="width: 5%;" class="dt-body-center">37.</td>
					<td>PPN</td>
					<td class="dt-body-right">0</td>
					<td class="dt-body-right">0</td>
					<td class="dt-body-right">0</td>
					<td class="dt-body-right">0</td>
				</tr>
				<tr>
					<td style="width: 5%;" class="dt-body-center">38.</td>
					<td>PPnBM</td>
					<td class="dt-body-right">0</td>
					<td class="dt-body-right">0</td>
					<td class="dt-body-right">0</td>
					<td class="dt-body-right">0</td>
				</tr>
				<tr>
					<td style="width: 5%;" class="dt-body-center">39.</td>
					<td>PPh</td>
					<td class="dt-body-right">0</td>
					<td class="dt-body-right">0</td>
					<td class="dt-body-right">0</td>
					<td class="dt-body-right">0</td>
				</tr>
				<tr>
					<td style="width: 5%;" class="dt-body-center">40.</td>
					<td>TOTAL</td>
					<td class="dt-body-right">0</td>
					<td class="dt-body-right">0</td>
					<td class="dt-body-right">0</td>
					<td class="dt-body-right">0</td>
				</tr>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
				<tr>
					<td style="width: 50%;" rowspan="2">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
							<thead>
								<tr>
									<td style="border: 0px;" colspan="2">C. PENGESAHAN PENGUSAHA TPB</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="border: 0px;" colspan="2">Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberitahukan dalam pemberitahuan pabean ini</td>
								</tr>
								<tr>
									<td style="border: 0px; width: 30%;">Tempat, Tanggal</td>
									<td style="border: 0px;">: &nbsp; BANDUNG, <?php if(isset($bc[0]['tanggal_pengajuan'])) echo date('d F Y', strtotime($bc[0]['tanggal_pengajuan'])); ?></td>
								</tr>
								<tr>
									<td style="border: 0px; width: 30%;">Nama Lengkap</td>
									<td style="border: 0px;">: &nbsp;</td>
								</tr>
								<tr>
									<td style="border: 0px; width: 30%;">Jabatan</td>
									<td style="border: 0px;">: &nbsp;</td>
								</tr>
								<tr>
									<td style="border: 0px;" colspan="2">Tanda Tangan dan Stempel Perusahaan :</td>
								</tr>
							</tbody>
						</table>
					</td>
					<td style="padding-bottom: 0px; width: 50%;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; padding-bottom: 0px;">
							<thead>
								<tr>
									<td style="border: 0px;" colspan="3">E. UNTUK PEMBAYARAN</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="border: 0px;">Pembayaran</td>
									<td style="border: 0px;"></td>
									<td style="border: 0px;">&nbsp; 1. Bank &nbsp; 2. Pos &nbsp; 3. Kantor Pabean</td>
								</tr>
								<tr>
									<td style="border: 0px;">Wajib Bayar</td>
									<td style="border: 0px;"></td>
									<td style="border: 0px;">&nbsp; 1. Pengusaha TPB &nbsp; 3. Kantor Penerima</td>
								</tr>
								<tr>
									<td style="border: 0px;" colspan="3" class="dt-body-center">Tanggal :</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td style="padding-top: 0px; padding-bottom: 0px; width: 50%;">
						<table class="table dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; padding-bottom: 0px;">
							<tr>
								<th style="border: 0px;" class="dt-body-center">Nama/Stempel instansi</th>
							</tr>
							<tr>
								<td style="border: 0px; height: 120px;"></td>
							</tr>
							<tr>
								<td style="border: 0px;" class="dt-body-center">Nama/Stempel</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<!-- End Lembar 1 -->

<!-- Start Lembar 2 -->
<div class="card-box" style="margin-top: 20px;">
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border: 0px;">
				<tr style="font-size: 20px;">
					<th style="border: 0px; padding: 0px;" class="dt-body-center">LEMBAR LANJUTAN DATA BARANG<br/><?php
						if(isset($bc[0]['desc_bc'])) echo strtoupper($bc[0]['desc_bc']);
					?></th>
				</tr>
				<tr>
					<th style="border: 0px; padding: 0px;" class="dt-body-right"><?php
						if(isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']);
					?></th>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<tr>
					<td style="border: 0px; width: 15%;">Kantor Pabean</td>
					<td style="border: 0px;">: &nbsp; KPPC BANDUNG</td>
					<th style="border: 0px; width: 15%;" rowspan="3">Halaman Ke-2 dari 4</th>
				</tr>
				<tr>
					<td style="border: 0px; width: 15%;">Nomor Pengajuaan</td>
					<td style="border: 0px;">: &nbsp; <?php if(isset($bc[0]['no_pengajuan'])) echo $bc[0]['no_pengajuan']; ?></td>
				</tr>
				<tr>
					<td style="border: 0px; width: 15%;">A. Jenis TPB</td>
					<td style="border: 0px;">: &nbsp; 1. Kawasan Berikat &nbsp; 2. Gudang Serikat &nbsp; 3. TPPB &nbsp; 4.TBB &nbsp; 5. TLB &nbsp; 6. KDUB &nbsp; 7. Lainnya</td>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<td style="width: 5%; padding-left: 16px;">28. No</td>
						<td style="width: 22%; padding-left: 16px;">29. <ul style="margin: -20px 0px 0px 0px; padding: 0px 0px 0px 45px;">
							<li>Pos Tarif/HS</li>
							<li>Kode Barang</li>
							<li>Uraian barang secara lengkap, merk, tipe, ukuran, spesifikasi lain</li>
							<li>Fasilitas Impor</li>
							<li>Surat Keputusan/Dokumen Lainnya</li>
						</ul></td>
						<td style="width: 10%; padding-left: 16px;">30. <ul style="margin: -20px 0px 0px 0px; padding: 0px 0px 0px 45px;">
							<li>Kategori Barang</li>
							<li>Kondisi Barang</li>
						</ul></td>
						<td style="width: 12%; padding-left: 16px;">31. <ul style="margin: -20px 0px 0px 0px; padding: 0px 0px 0px 45px;">
							<li>Tarif dan Fasilitas</li>
							<li>BM</li>
							<li>BMT</li>
							<li>Cukai</li>
							<li>PPN</li>
							<li>PPnBM</li>
							<li>PPh</li>
						</ul></td>
						<td style="width: 12%; padding-left: 16px;">32. <ul style="margin: -20px 0px 0px 0px; padding: 0px 0px 0px 45px;">
							<li>Jumlah dan Jenis Satuan</li>
							<li>Berat Bersih (Kg)</li>
							<li>Jumlah dan Jenis Kemasan</li>
						</ul></td>
						<td style="width: 10%; padding-left: 16px;">33. <ul style="margin: -20px 0px 0px 0px; padding: 0px 0px 0px 45px;">
							<li>Nilai CIF</li>
							<li>Harga Penyerahan</li>
						</ul></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="border-top: 0px; border-bottom: 0px;" class="dt-body-center"></td>
						<td style="border-top: 0px; border-bottom: 0px;"></td>
						<td style="border-top: 0px; border-bottom: 0px;"></td>
						<td style="border-top: 0px; border-bottom: 0px;"></td>
						<td style="border-top: 0px; border-bottom: 0px;"></td>
						<td style="border-top: 0px; border-bottom: 0px;"></td>
					</tr>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
				<thead>
					<tr>
						<td style="border: 0px;" colspan="2">C. PENGESAHAN PENGUSAHA TPB</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="border: 0px;" colspan="2">Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberitahukan dalam pemberitahuan pabean ini</td>
					</tr>
					<tr>
						<td style="border: 0px; width: 15%;">Tempat, Tanggal</td>
						<td style="border: 0px;">: &nbsp; BANDUNG, <?php if(isset($bc[0]['tanggal_pengajuan'])) echo date('d F Y', strtotime($bc[0]['tanggal_pengajuan'])); ?></td>
					</tr>
					<tr>
						<td style="border: 0px; width: 15%;">Nama Lengkap</td>
						<td style="border: 0px;">: &nbsp;</td>
					</tr>
					<tr>
						<td style="border: 0px; width: 15%;">Jabatan</td>
						<td style="border: 0px;">: &nbsp;</td>
					</tr>
					<tr>
						<td style="border: 0px;" colspan="2">Tanda Tangan dan Stempel Perusahaan :</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- End Lembar 2 -->

<!-- Start Lembar 3 -->
<div class="card-box" style="margin-top: 20px;">
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border: 0px;">
				<tr style="font-size: 20px;">
					<th style="border: 0px; padding: 0px;" class="dt-body-center">LEMBAR LANJUTAN DOKUMEN PELENGKAP<br/><?php
						if(isset($bc[0]['desc_bc'])) echo strtoupper($bc[0]['desc_bc']);
					?></th>
				</tr>
				<tr>
					<th style="border: 0px; padding: 0px;" class="dt-body-right"><?php
						if(isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']);
					?></th>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<tr>
					<td style="border: 0px; width: 15%;">Kantor Pabean</td>
					<td style="border: 0px;">: &nbsp; KPPC BANDUNG</td>
					<th style="border: 0px; width: 15%;" rowspan="3">Halaman Ke-3 dari 4</th>
				</tr>
				<tr>
					<td style="border: 0px; width: 15%;">Nomor Pengajuaan</td>
					<td style="border: 0px;">: &nbsp; <?php if(isset($bc[0]['no_pengajuan'])) echo $bc[0]['no_pengajuan']; ?></td>
				</tr>
				<tr>
					<td style="border: 0px; width: 15%;">A. Jenis TPB</td>
					<td style="border: 0px;">: &nbsp; 1. Kawasan Berikat &nbsp; 2. Gudang Serikat &nbsp; 3. TPPB &nbsp; 4.TBB &nbsp; 5. TLB &nbsp; 6. KDUB &nbsp; 7. Lainnya</td>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<td style="width: 5%;">No.</td>
						<td class="dt-body-center">Jenis Dokumen</td>
						<td class="dt-body-center">Nomor Dokumen</td>
						<td class="dt-body-center">Tanggal Dokumen</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="border-top: 0px; border-bottom: 0px;" class="dt-body-center">1</td>
						<td style="border-top: 0px; border-bottom: 0px;">PACKING LIST</td>
						<td style="border-top: 0px; border-bottom: 0px;"></td>
						<td style="border-top: 0px; border-bottom: 0px;" class="dt-body-center"></td>
					</tr>
					<tr>
						<td style="border-top: 0px; border-bottom: 0px;" class="dt-body-center">2</td>
						<td style="border-top: 0px; border-bottom: 0px;">INVOICE</td>
						<td style="border-top: 0px; border-bottom: 0px;"></td>
						<td style="border-top: 0px; border-bottom: 0px;" class="dt-body-center"></td>
					</tr>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
				<thead>
					<tr>
						<td style="border: 0px;" colspan="2">C. PENGESAHAN PENGUSAHA TPB</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="border: 0px;" colspan="2">Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberitahukan dalam pemberitahuan pabean ini</td>
					</tr>
					<tr>
						<td style="border: 0px; width: 15%;">Tempat, Tanggal</td>
						<td style="border: 0px;">: &nbsp; BANDUNG, <?php if(isset($bc[0]['tanggal_pengajuan'])) echo date('d F Y', strtotime($bc[0]['tanggal_pengajuan'])); ?></td>
					</tr>
					<tr>
						<td style="border: 0px; width: 15%;">Nama Lengkap</td>
						<td style="border: 0px;">: &nbsp;</td>
					</tr>
					<tr>
						<td style="border: 0px; width: 15%;">Jabatan</td>
						<td style="border: 0px;">: &nbsp;</td>
					</tr>
					<tr>
						<td style="border: 0px;" colspan="2">Tanda Tangan dan Stempel Perusahaan :</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- End Lembar 3 -->

<!-- Start Lembar 4 -->
<div class="card-box" style="margin-top: 20px;">
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border: 0px;">
				<tr style="font-size: 20px;">
					<th style="border: 0px; padding: 0px;" class="dt-body-center">LEMBAR LAMPIRAN<br/>DATA PENGGUNAAN BARANG DAN/ATAU BAHAN IMPOR<?php
						if(isset($bc[0]['desc_bc'])) echo strtoupper($bc[0]['desc_bc']);
					?></th>
				</tr>
				<tr>
					<th style="border: 0px; padding: 0px;" class="dt-body-right"><?php
						if(isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']);
					?></th>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<tr>
					<td style="border: 0px; width: 15%;">Kantor Pabean</td>
					<td style="border: 0px;">: &nbsp; KPPC BANDUNG</td>
					<th style="border: 0px; width: 15%;" rowspan="3">Halaman Ke-4 dari 4</th>
				</tr>
				<tr>
					<td style="border: 0px; width: 15%;">Nomor Pengajuaan</td>
					<td style="border: 0px;">: &nbsp; <?php if(isset($bc[0]['no_pengajuan'])) echo $bc[0]['no_pengajuan']; ?></td>
				</tr>
				<tr>
					<td style="border: 0px; width: 15%;">A. Jenis TPB</td>
					<td style="border: 0px;">: &nbsp; 1. Kawasan Berikat &nbsp; 2. Gudang Serikat &nbsp; 3. TPPB &nbsp; 4.TBB &nbsp; 5. TLB &nbsp; 6. KDUB &nbsp; 7. Lainnya</td>
				</tr>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px; border-bottom: 0px;">
				<thead>
					<tr>
						<td style="width: 5%; padding-left: 16px;">No Urut Barang</td>
						<td style="width: 20%; padding-left: 16px;"><ul style="padding: 0px 0px 0px 16px;">
							<li>Kode Kantor</li>
							<li>No/Tgl Daftar BC 2.3, BC 2.7, Lainnya *)</li>
						</ul></td>
						<td style="width: 8%; padding-left: 16px;">No Urut Dalam<ul style="padding: 0px 0px 0px 16px;">
							<li>BC 2.3</li>
							<li>BC 2.7</li>
							<li>Lainnya *)</li>
						</ul></td>
						<td style="width: 20%; padding-left: 16px;"><ul style="padding: 0px 0px 0px 16px;">
							<li>Pos tarif/HS, uraian jumlah dan jenis barang secara lengkap, kode barang merk, tipe, ukuran, dan spesifikasi lain</li>
							<li>Perijinan/Fasilitas</li>
						</ul></td>
						<td style="width: 5%; padding-left: 16px;"><ul style="padding: 0px 0px 0px 16px;">
							<li>Jumlah</li>
							<li>Satuan</li>
						</ul></td>
						<td style="width: 5%; padding-left: 16px;">Nilai<ul style="padding: 0px 0px 0px 16px;">
							<li>CIF</li>
							<li>Harga Penyerahan (Rp)</li>
						</ul></td>
						<td style="width: 5%; padding-left: 16px;">Nilai (Rp)<ul style="padding: 0px 0px 0px 16px;">
							<li>BM</li>
							<li>BMT</li>
							<li>Cukai</li>
							<li>PPN</li>
							<li>PPnBM</li>
							<li>PPh 22</li>
						</ul></td>
					</tr>
					<tr>
						<td style="width: 5%;" class="dt-body-center">(1)</td>
						<td style="width: 20%;" class="dt-body-center">(2)</td>
						<td style="width: 8%;" class="dt-body-center">(3)</td>
						<td style="width: 20%;" class="dt-body-center">(4)</td>
						<td style="width: 5%;" class="dt-body-center">(5)</td>
						<td style="width: 5%;" class="dt-body-center">(6)</td>
						<td style="width: 5%;" class="dt-body-center">(7)</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="width: 5%; border-top: 0px; border-bottom: 0px;" class="dt-body-center"></td>
						<td style="width: 20%; border-top: 0px; border-bottom: 0px;"></td>
						<td style="width: 8%; border-top: 0px; border-bottom: 0px;"></td>
						<td style="width: 20%; border-top: 0px; border-bottom: 0px;"></td>
						<td style="width: 5%; border-top: 0px; border-bottom: 0px;"></td>
						<td style="width: 5%; border-top: 0px; border-bottom: 0px;"></td>
						<td style="width: 5%; border-top: 0px; border-bottom: 0px;"></td>
					</tr>
				</tbody>
			</table>

			<table class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom: 0px;">
				<thead>
					<tr>
						<td style="border: 0px;" colspan="2">C. PENGESAHAN PENGUSAHA TPB</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="border: 0px;" colspan="2">Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberitahukan dalam pemberitahuan pabean ini</td>
					</tr>
					<tr>
						<td style="border: 0px; width: 15%;">Tempat, Tanggal</td>
						<td style="border: 0px;">: &nbsp; BANDUNG, <?php if(isset($bc[0]['tanggal_pengajuan'])) echo date('d F Y', strtotime($bc[0]['tanggal_pengajuan'])); ?></td>
					</tr>
					<tr>
						<td style="border: 0px; width: 15%;">Nama Lengkap</td>
						<td style="border: 0px;">: &nbsp;</td>
					</tr>
					<tr>
						<td style="border: 0px; width: 15%;">Jabatan</td>
						<td style="border: 0px;">: &nbsp;</td>
					</tr>
					<tr>
						<td style="border: 0px;" colspan="2">Tanda Tangan dan Stempel Perusahaan :</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- End Lembar 4 -->

<script type="text/javascript">
	var id_bc = "<?php if(isset($bc[0])){ echo $bc[0]['id']; } ?>";

	$(document).ready(function() {
		$('#back_bc').on('click', function() {
			$('#title_Menu').html('Bea Cukai');
			$('#div_list_bc').show();
			$('#div_detail_bc').hide();
			$('#div_detail_bc').html('');
		})

		$('#downloadDetail').on('click', function() {

		})
	});
</script>