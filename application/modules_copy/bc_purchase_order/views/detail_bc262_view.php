<style type="text/css">
	.tombol-kembali {
		margin-top: -5px;
	}

	.text-kembali {
		margin-top: -5px;
		margin-left: 50px;
	}

	.header-jenis-bc {
		vertical-align: middle;
		width: 10%;
	}

	.header-deskripsi-bc {
		vertical-align: middle;
		width: 90%;
	}

	.label-header {
		color: #505458;
		font-size: 20px;
		font-weight: bold;
	}
</style>
<div class="card-box" style="height:60px;">
	<div class="row">
		<div class="btn-group pull-left tombol-kembali">
			<a class="btn btn-info btn-sm" id="back_bc" name="back_bc" data-toggle="tooltip" data-placement="top" title="Back">
				<i class="fa fa-arrow-left"></i>
			</a>
		</div>
		<h3 class="text-kembali"><label> Kembali </label></h3>
	</div>
</div>
<div class="card-box" style="margin-bottom:30px;margin-top:20px;">
	<div class="row">
		<div class="col-md-12">
			<table id="TablePrintHeaderPage1" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td colspan="4">
							<h4 id="titleCelebit" style="text-align:center;"> Pemberitahuan Pemasukan Kembali Barang yang Dikeluarkan dari</h4>
						</td>
					</tr>
					<tr>
						<td colspan="4">
							<h4 id="titlePerusahaan" style="text-align:center;">Tempat Penimbunan Berikat dengan Jaminan</h4>
						</td>
					</tr>
					<tr>
						<td colspan="4" style="text-align: right;"><?php echo $bc[0]['jenis_bc'] ?></td>
					</tr>
					<tr>
						<td width="25%">Kantor Pabean : </td>
						<td width="25%"></td>
						<td width="25%"><?php if (sizeof($header)>1) echo $header[0]['KPPBC'] ?></td>
						<td width="25%"></td>
					</tr>
					<tr>
						<td>Nomor Pengajuan : </td>
						<td><?php echo $bc[0]['no_pengajuan']; ?></td>
					</tr>
					<tr>
						<td>TUJUAN PENGIRIMAN : </td>
						<td><?php if (sizeof($header)>1) echo $header[0]['KODE_TUJUAN_PENGIRIMAN']; ?></td>
						<td colspan="2">1. Diperbaiki 2.Disubkontraktorkan 3.Dipinjamkan 4.Lainnya</td>
					</tr>
				</tbody>
			</table>
			<table id="tablePrintTopPage1" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td colspan="2">B. Data Pemberitahuan</td>
					</tr>
					<tr>
						<td>Pengusaha TPB
							<br>1. NPWP : <?php if (sizeof($header)>1) echo $header[0]['NPWP_BILLING'] ?>
							<br>2. Nama :<?php if (sizeof($header)>1)  echo $header[0]['ID_PENGUSAHA'] ?>
							<br>3. Alamat : <?php if (sizeof($header)>1) echo $header[0]['ALAMAT_PENGUSAHA'] ?>
							<br>4. Nomor dan Tanggal Izin TPB : <?php if (sizeof($header)>1) echo $header[0]['NOMOR_IJIN_TPB'] ?> Tanggal : <?php if (sizeof($header)>1) echo $header[0]['TANGGAL_IJIN_TPB'] ?>
						</td>
						<td>D. DIISI OLEH BEA DAN CUKAI
							<br>Nomor Pendaftaran : <?php  echo $bc[0]['no_pendaftaran'] ?>
							<br>Tanggal :
						</td>
					</tr>
					<tr>
						<td>Penerima Barang
							<br>5. NPWP :
							<br>6. Nama : <?php if (sizeof($header)>1) echo $header[0]['ID_PENERIMA_BARANG'] ?>
							<br>7. Alamat : <?php if (sizeof($header)>1) echo $header[0]['ALAMAT_PENERIMA_BARANG'] ?>
						</td>
						<td>Dokumen Pelengkap Pabean
							<br>8. Packing List No : Tgl :
							<br>9. Pemenuhan Persyaratan/Fasilitas Impor
							<br>No : Tgl:
							<br>10. Surat Keputusan Dokumen Lainnya :
							<br>No : Tgl:
						</td>
					</tr>
					<tr>
						<td colspan="2">11. Valuta : <?php if (sizeof($header)>1) echo  $header[0]['KODE_VALUTA'] ?>
							<br>12. NDPBM : <?php if (sizeof($header)>1) echo $header[0]['NDPBM'] ?>
							<br>13. Nilai CIF : <?php if (sizeof($header)>1) echo $header[0]['CIF'] ?>
						</td>
					</tr>
					<tr>
						<td colspan="2">14. Jenis Sarana Pengangkut : <?php if (sizeof($header)>1) echo $header[0]['NAMA_PENGANGKUT'] ?>
						</td>
					</tr>
					<tr>
						<td>15. Nomor, Ukuran dan Tipe Peti Kemas &nbsp; 16. Jumlah, Jenis dan Merek Kemasan 1 0 Unpacked or unpackaged
						</td>
						<td>17. Berat Kotor (kg) :
							<br><?php if (sizeof($header)>1) echo $header[0]['BRUTO'] ?>
							<br>18. Berat Bersih (kg) :
							<br><?php if (sizeof($header)>1) echo $header[0]['NETTO'] ?>
						</td>
					</tr>
				</tbody>
			</table>
			<table id="tablePrintBottomPage1" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td>19<br>No.</td>
						<td>20
							<br>-Pos Tarif/HS
							<br>Uraian Jumlah Barang Secara Lengkap, Merek, Tipe, Ukuran dan Spesifikasi Lain.
						</td>
						<td>21 Jumlah dan
							<br>Jenis Satuan
							<br>Berat Bersih (kg)
						</td>
						<td>22 Nilai CIF
						</td>
					</tr>
					<tr>
						<td>1</td>
						<td>
							<br>- Pos Tarif/HS :
							<br>- Kode Barang : TTD
							<br>- SOFT TOOLING DN24C336-42, Merk: , Tipe:
							<br>- 0 Unpacked or unpackaged (NE)
						</td>
						<td></td>
						<td>Satuan : 1,0000
							<br>NIU(Number Of International Units)
							<br>Berat Bersih :
							<br>135,50000
						</td>
						<td><?php if (sizeof($header)>1) echo $header[0]['CIF_RUPIAH'] ?></td>
					</tr>
				</tbody>
			</table>

			<table id="tablePrintLastBottomPage1" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td colspan="2">Data Perhitungan Jaminan</td>
						<td colspan="2">Data Jaminan</td>
					</tr>
					<tr>
						<td>Jenis Pungutan</td>
						<td>Jumlah</td>
						<td colspan="2">
							32. Jaminan :
						</td>
					</tr>
					<tr>
						<td>25.Bea Masuk</td>
						<td></td>
						<td colspan="2">33. Nomor Jaminan : &nbsp; Tanggal :</td>
					</tr>
					<tr>
						<td>26.Bea Masuk Tambahan</td>
						<td></td>
						<td colspan="2">34. Nilai Jaminan :
					</tr>
					<tr>
						<td>27.Cukai</td>
						<td></td>
						<td colspan="2">35. Tanggal Jatuh Tempo : <?php if (sizeof($header)>1) echo $header[0]['TANGGAL_JATUH_TEMPO'] ?>
					</tr>
					<tr>
						<td>28.PPN</td>
						<td></td>
						<td colspan="2">36. Penjamin :
					</tr>
					<tr>
						<td>29.PPn BM</td>
						<td></td>
						<td colspan="2">37. Nomor dan tanggal :
					</tr>
					<tr>
						<td>30.PPh</td>
						<td></td>
						<td colspan="2">Bukti Penerimaan Jaminan : &nbsp; Tanggal :</td>
					</tr>
					<tr>
						<td>31.Jumlah Total</td>
						<td></td>
						<td colspan="2"></td>
					</tr>
					<tr>
						<td colspan="3">
							C. Pengesahan Pengusaha TPB
							<br>Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberitahukan
							<br>dalam pemberitahuan pabean ini.
							<br>Tempat, Tanggal :
							<br>Nama Lengkap :<?php if (sizeof($header)>1) echo $header[0]['NAMA_PENERIMA_BARANG'] ?>
							<br>Jabatan :
							<br>Tanda Tangan dan Stempel Perusahaan :
						</td>
						<td>
							E. Untuk Pejabat BEA DAN CUKAI
						</td>
					</tr>
				</tbody>
			</table>

			<table id="Dokumen"  class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" >
				<tbody>
					<tr>
						<td> No </td>
						<td> Jenis Dokumen </td>
						<td> Nomor Dokumen </td>
						<td> Tanggal Dokumen </td>
					</tr>

					<?php
					$i=1;
					foreach ($dokumen as $row) {
						// var_dump($row); 
						unset($row['id_report_dokumen']);
						// var_dump($row); 
						?>
						<tr>
							<td><?php echo  $i++;?></td>
							<td><?php echo  $row['TIPE_DOKUMEN'];?></td>
							<td><?php echo  $row['NOMOR_DOKUMEN'];?></td>
							<td><?php echo  $row['TANGGAL_DOKUMEN'];?></td>
						</tr>
					<?php
					}
					?>
				</tbody>
			</table>
			<table id="tablePrintTopPage2" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td colspan="4">
							C. Pengesahan Pengusaha TPB
							<br>Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberitahukan dalam pemberitahuan pabean ini
							<br>Tempat, Tanggal :<?php if (sizeof($header)>1) echo $header[0]['KOTA_TTD'] . " , " ?> <?php if (sizeof($header)>1) echo $header[0]['TANGGAL_TTD'] ?>
							<br>Nama Lengkap : <?php if (sizeof($header)>1) echo $header[0]['NAMA_TTD'] ?>
							<br>Jabatan :<?php if (sizeof($header)>1) echo $header[0]['JABATAN_TTD'] ?>
							<br>Tanda Tangan dan Stempel Perusahaan :
						</td>
					</tr>
				</tbody>
			</table>
			

		</div>
	</div>
</div>
	<script type="text/javascript">
		var id_bc = "<?php if (isset($bc[0])) {
										echo $bc[0]['id'];
									} ?>";

		$(document).ready(function() {
			$('#back_bc').on('click', function() {
				$('#title_Menu').html('Bea Cukai');
				$('#div_list_bc').show();
				$('#div_detail_bc').hide();
				$('#div_detail_bc').html('');
			})
		});
	</script>