<style>
	#nopendaftaran_loading-us{display:none}
	#nopendaftaran_tick{display:none}

	#nopengajuan_loading-us{display:none}
	#nopengajuan_tick{display:none}
	.x-hidden{display:none;}
	.sk-circle {
		  margin: 0px 250px;
		  width: 30px;
		  height: 30px;
		  position: relative;
		}
		.sk-circle .sk-child {
		  width: 100%;
		  height: 100%;
		  position: absolute;
		  left: 0;
		  top: 0;
		}
		.sk-circle .sk-child:before {
		  content: '';
		  display: block;
		  margin: 0 auto;
		  width: 15%;
		  height: 15%;
		  background-color: #6a6e6d;
		  border-radius: 100%;
		  -webkit-animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
				  animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
		}
		.sk-circle .sk-circle2 {
		  -webkit-transform: rotate(30deg);
			  -ms-transform: rotate(30deg);
				  transform: rotate(30deg); }
		.sk-circle .sk-circle3 {
		  -webkit-transform: rotate(60deg);
			  -ms-transform: rotate(60deg);
				  transform: rotate(60deg); }
		.sk-circle .sk-circle4 {
		  -webkit-transform: rotate(90deg);
			  -ms-transform: rotate(90deg);
				  transform: rotate(90deg); }
		.sk-circle .sk-circle5 {
		  -webkit-transform: rotate(120deg);
			  -ms-transform: rotate(120deg);
				  transform: rotate(120deg); }
		.sk-circle .sk-circle6 {
		  -webkit-transform: rotate(150deg);
			  -ms-transform: rotate(150deg);
				  transform: rotate(150deg); }
		.sk-circle .sk-circle7 {
		  -webkit-transform: rotate(180deg);
			  -ms-transform: rotate(180deg);
				  transform: rotate(180deg); }
		.sk-circle .sk-circle8 {
		  -webkit-transform: rotate(210deg);
			  -ms-transform: rotate(210deg);
				  transform: rotate(210deg); }
		.sk-circle .sk-circle9 {
		  -webkit-transform: rotate(240deg);
			  -ms-transform: rotate(240deg);
				  transform: rotate(240deg); }
		.sk-circle .sk-circle10 {
		  -webkit-transform: rotate(270deg);
			  -ms-transform: rotate(270deg);
				  transform: rotate(270deg); }
		.sk-circle .sk-circle11 {
		  -webkit-transform: rotate(300deg);
			  -ms-transform: rotate(300deg);
				  transform: rotate(300deg); }
		.sk-circle .sk-circle12 {
		  -webkit-transform: rotate(330deg);
			  -ms-transform: rotate(330deg);
				  transform: rotate(330deg); }
		.sk-circle .sk-circle2:before {
		  -webkit-animation-delay: -1.1s;
				  animation-delay: -1.1s; }
		.sk-circle .sk-circle3:before {
		  -webkit-animation-delay: -1s;
				  animation-delay: -1s; }
		.sk-circle .sk-circle4:before {
		  -webkit-animation-delay: -0.9s;
				  animation-delay: -0.9s; }
		.sk-circle .sk-circle5:before {
		  -webkit-animation-delay: -0.8s;
				  animation-delay: -0.8s; }
		.sk-circle .sk-circle6:before {
		  -webkit-animation-delay: -0.7s;
				  animation-delay: -0.7s; }
		.sk-circle .sk-circle7:before {
		  -webkit-animation-delay: -0.6s;
				  animation-delay: -0.6s; }
		.sk-circle .sk-circle8:before {
		  -webkit-animation-delay: -0.5s;
				  animation-delay: -0.5s; }
		.sk-circle .sk-circle9:before {
		  -webkit-animation-delay: -0.4s;
				  animation-delay: -0.4s; }
		.sk-circle .sk-circle10:before {
		  -webkit-animation-delay: -0.3s;
				  animation-delay: -0.3s; }
		.sk-circle .sk-circle11:before {
		  -webkit-animation-delay: -0.2s;
				  animation-delay: -0.2s; }
		.sk-circle .sk-circle12:before {
		  -webkit-animation-delay: -0.1s;
				  animation-delay: -0.1s; }

		@-webkit-keyframes sk-circleBounceDelay {
		  0%, 80%, 100% {
			-webkit-transform: scale(0);
					transform: scale(0);
		  } 40% {
			-webkit-transform: scale(1);
					transform: scale(1);
		  }
		}

		@keyframes sk-circleBounceDelay {
		  0%, 80%, 100% {
			-webkit-transform: scale(0);
					transform: scale(0);
		  } 40% {
			-webkit-transform: scale(1);
					transform: scale(1);
		  }
		}
</style>
	
<form class="form-horizontal form-label-left" id="add_form" role="form" action="<?php echo base_url('bc_purchase_order/save_bc');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jenis_bc">Jenis BC <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="jenis_bc" name="jenis_bc" style="width: 100%" required>
				<option value="" >-- Select Jenis --</option>
				<?php foreach($jenis_bc as $key) { ?>
					<option value="<?php echo $key['id']; ?>" ><?php echo $key['jenis_bc']; ?></option>
				<?php } ?>
			</select>
			<input type="hidden" id="type_text" name="type_text" value="">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nopendaftaran">No Pendaftaran</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="nopendaftaran" name="nopendaftaran" class="form-control col-md-7 col-xs-12" placeholder="No Pendaftaran" autocomplete="off" data-parsley-maxlength="100" autocomplete="off">
			<!-- <span id="nopendaftaran_loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking No Pendaftaran...</span>
			<span id="nopendaftaran_tick"></span> -->
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nopengajuan">No Pengajuan <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="nopengajuan" name="nopengajuan" class="form-control col-md-7 col-xs-12" placeholder="No Pengajuan" data-parsley-maxlength="100" autocomplete="off">
			<!-- <span id="nopengajuan_loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking No Pengajuan...</span>
			<span id="nopengajuan_tick"></span> -->
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tglpengajuan">Tanggal Pengajuan <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="100" type="text" id="tglpengajuan" name="tglpengajuan" class="form-control col-md-7 col-xs-12" placeholder="Tanggal Pengajuan" autocomplete="off" required>
		</div>
	</div>

	<div class="item form-group has-feedback">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_po">Nomor PO <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="no_po" name="no_po" onchange="getBTB();" required>
				<option value="">-- Pilih PO --</option>
			</select>
		</div>
	</div>
	
	<div class="sk-circle x-hidden" id="loaders">
		<div class="sk-circle1 sk-child"></div>
		<div class="sk-circle2 sk-child"></div>
		<div class="sk-circle3 sk-child"></div>
		<div class="sk-circle4 sk-child"></div>
		<div class="sk-circle5 sk-child"></div>
		<div class="sk-circle6 sk-child"></div>
		<div class="sk-circle7 sk-child"></div>
		<div class="sk-circle8 sk-child"></div>
		<div class="sk-circle9 sk-child"></div>
		<div class="sk-circle10 sk-child"></div>
		<div class="sk-circle11 sk-child"></div>
		<div class="sk-circle12 sk-child"></div>
	</div>
		
	<div class="item form-group has-feedback x-hidden" id="btb">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_btb">No BTB <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="id_btb" name="id_btb" required>
				<option value="">-- Pilih BTB --</option>
			</select>
		</div>
	</div>

	<!-- <div class="item form-group has-feedback">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="file_bc">File</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="file" class="form-control" id="file_bc" name="file_bc" data-height="110" accept=".pdf, .txt, .doc, .docx"/>
			<span id="file_bc_tick">Hanya format file pdf,txt,doc,docx dengan besaran max 10Mb yang diterima.</span>
		</div>
	</div> -->

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="hidden" id="menus" name="menus" value="">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah BC</button>
		</div>
	</div>
</form>

<script type="text/javascript">
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
		$('#tglpengajuan').datepicker({
			format: "dd/M/yyyy",
			autoclose: true,
			todayHighlight: true
		});

		$('#no_po').select2();
		get_purchaseOrder();

		/*$('#jenis_bc').change(function() {
			if(this.options[this.selectedIndex].value != '') $('#type_text').val(this.options[this.selectedIndex].text);
			else $('#type_text').val('');
		});

		$('#file_bc').bind('change', function() {
			if(this.files[0].size >= 9437184) {
				$('#file_bc').css('border', '3px #C33 solid');
				$('#file_bc_tick').empty();
				$("#file_bc_tick").append('<span class="fa fa-close"> Ukuran File Lebih dari 9Mb</span>');
				$('#file_bc_tick').show();
			}else {
				$('#file_bc').removeAttr("style");
				$('#file_bc_tick').empty();
				$("#file_bc_tick").append('Hanya format file pdf,txt,doc,docx dengan besaran max 10Mb yang diterima.');
				$('#file_bc_tick').show();
			}
		});*/

		$('#jenis_bc').on('change', function() {
			if(this.value) {
				var post_data = {
					'jenis_bc': $(this).find("option:selected").text().replace(/\D/g,'')
				};

				jQuery.ajax({
					type: "POST",
					url: "<?php echo base_url('bc_purchase_order/get_no_ajul');?>",
					data: post_data,
					cache: false,
					success: function(response){
						if(response.success == true) {
							// $('#nopengajuan').val(response.no_pengajuan);
							$('#menus').val(response.menus);
						}else {
							$('#nopengajuan').val('');
							$('#menus').val('');
						}
					}
				});
			}else {
				// $('#nopengajuan').val('');
				$('#menus').val('');
			}
		})
	});

	/*var last_nopendaftaran = $('#nopendaftaran').val();
	$('#nopendaftaran').on('input',function(event) {
		if($('#nopendaftaran').val() != last_nopendaftaran) {
			nopendaftaran_check();
		}
	});

	function nopendaftaran_check() {
		var nopendaftaran = $('#nopendaftaran').val();
		if(nopendaftaran.length > 3) {
			var post_data = {
				'nopendaftaran': nopendaftaran
			};

			$('#nopendaftaran_tick').empty();
			$('#nopendaftaran_tick').hide();
			$('#nopendaftaran_loading-us').show();
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url('bc_purchase_order/check_nopendaftaran');?>",
				data: post_data,
				cache: false,
				success: function(response){
					if(response.success == true){
						$('#nopendaftaran').css('border', '3px #090 solid');
						$('#nopendaftaran_loading-us').hide();
						$('#nopendaftaran_tick').empty();
						$("#nopendaftaran_tick").append('<span class="fa fa-check"> '+response.message+'</span>');
						$('#nopendaftaran_tick').show();
					}else {
						$('#nopendaftaran').css('border', '3px #C33 solid');
						$('#nopendaftaran_loading-us').hide();
						$('#nopendaftaran_tick').empty();
						$("#nopendaftaran_tick").append('<span class="fa fa-close"> '+response.message+'</span>');
						$('#nopendaftaran_tick').show();
					}
				}
			});
		}else {
			$('#nopendaftaran').css('border', '3px #C33 solid');
			$('#nopendaftaran_loading-us').hide();
			$('#nopendaftaran_tick').empty();
			$("#nopendaftaran_tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
			$('#nopendaftaran_tick').show();
		}
	}

	var last_nopengajuan = $('#nopengajuan').val();
	$('#nopengajuan').on('input',function(event) {
		if($('#nopengajuan').val() != last_nopengajuan) {
			nopengajuan_check();
		}
	});

	function nopengajuan_check() {
		var nopengajuan = $('#nopengajuan').val();
		if(nopengajuan.length > 3) {
			var post_data = {
				'nopengajuan': nopengajuan
			};

			$('#nopengajuan_tick').empty();
			$('#nopengajuan_tick').hide();
			$('#nopengajuan_loading-us').show();
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url('bc_purchase_order/check_nopengajuan');?>",
				data: post_data,
				cache: false,
				success: function(response){
					if(response.success == true){
						$('#nopengajuan').css('border', '3px #090 solid');
						$('#nopengajuan_loading-us').hide();
						$('#nopengajuan_tick').empty();
						$("#nopengajuan_tick").append('<span class="fa fa-check"> '+response.message+'</span>');
						$('#nopengajuan_tick').show();
					}else {
						$('#nopengajuan').css('border', '3px #C33 solid');
						$('#nopengajuan_loading-us').hide();
						$('#nopengajuan_tick').empty();
						$("#nopengajuan_tick").append('<span class="fa fa-close"> '+response.message+'</span>');
						$('#nopengajuan_tick').show();
					}
				}
			});
		}else {
			$('#nopengajuan').css('border', '3px #C33 solid');
			$('#nopengajuan_loading-us').hide();
			$('#nopengajuan_tick').empty();
			$("#nopengajuan_tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
			$('#nopengajuan_tick').show();
		}
	}*/

	function getBTB() {
		var value_no = $("#no_po").val();
		
		if(value_no != '' || value_no != null) {
			sendNo($("#no_po option:selected").val()+','+value_no);
		}else{
			
		}
	}

	function sendNo(id) 
	{
		var purchase_order = id.split(',');
		var html = '';
		id_po = purchase_order[0];
		
		$('#loaders').removeClass('x-hidden');
		$('#btb').addClass('x-hidden');
		
		var datapost={
			"id_po" :   id_po
		};
			
		$.ajax({
			type:'POST',
			url: '<?php echo base_url('bc_purchase_order/get_btb_by_po');?>',
			data:JSON.stringify(datapost),
			cache:true,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					var btb = response.btb;
					for (var i = 0; i < btb.length; i++) {
						var option_val = btb[i]['id_btb']; 
						var option_no = btb[i]['no_btb']; 
						
						html += "<option value='"+option_val+"'>"+option_no+"</option>"; 
					} 
					$('#id_btb').val(null).trigger('change');
					$('#id_btb').html(html);
					$('#btb').removeClass('x-hidden');
					$('#loaders').addClass('x-hidden');
				} else{
					swal("Perhatian!", "Data BTB tidak ditemukan di dalam PO ini", "info");
					$('#btb').addClass('x-hidden');
					$('#loaders').addClass('x-hidden');
				}
			}
		}).fail(function(xhr, status, message) {
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}

	function get_purchaseOrder() {
		$('#no_po').attr('disabled', 'disabled');

		$.ajax({
			type: "GET",
			url: "<?php echo base_url('bc_purchase_order/get_purchaseOrder');?>",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					for (var i = 0; i < r.length; i++) {
						var name_eksternal ="";
						if (r[i].name_eksternal !== undefined)
							name_eksternal = " - "+r[i].name_eksternal;
						var newOption = new Option(r[i].no_po+name_eksternal, r[i].id_po, false, false);
						$('#no_po').append(newOption);
					}
					$('#no_po').removeAttr('disabled');
				}else $('#no_po').removeAttr('disabled');
			}
		});
	}

	$('#add_form').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		var formData = new FormData(this);

		/*if($('#file_bc')[0].files.length > 0) {
			if($('#file_bc')[0].files[0].size <= 9437184) {
				save_Form(formData, $(this).attr('action'));
			}else {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Tambah BC");
				swal("Failed!", "Ukuran File Terlalu besar, silahkan cek kembali", "error");
			}
		}else {
			save_Form(formData, $(this).attr('action'));
		}*/
		save_Form(formData, $(this).attr('action'));
	}));

	function save_Form(formData, url) {
		$.ajax({
			type:'POST',
			url: url,
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						$('#panel-modal').modal('toggle');
						get_list_bc();
						// window.location.href = "<?php echo base_url('bc_purchase_order');?>";
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Tambah BC");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah BC");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}
</script>
