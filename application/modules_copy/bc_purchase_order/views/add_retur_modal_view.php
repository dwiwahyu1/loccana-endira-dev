<style>
	#nopendaftaran_loading-us{display:none}
	#nopendaftaran_tick{display:none}

	#nopengajuan_loading-us{display:none}
	#nopengajuan_tick{display:none}
	.x-hidden{display:none;}
	.sk-circle {
		  margin: 0px 250px;
		  width: 30px;
		  height: 30px;
		  position: relative;
		}
		.sk-circle .sk-child {
		  width: 100%;
		  height: 100%;
		  position: absolute;
		  left: 0;
		  top: 0;
		}
		.sk-circle .sk-child:before {
		  content: '';
		  display: block;
		  margin: 0 auto;
		  width: 15%;
		  height: 15%;
		  background-color: #6a6e6d;
		  border-radius: 100%;
		  -webkit-animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
				  animation: sk-circleBounceDelay 1.2s infinite ease-in-out both;
		}
		.sk-circle .sk-circle2 {
		  -webkit-transform: rotate(30deg);
			  -ms-transform: rotate(30deg);
				  transform: rotate(30deg); }
		.sk-circle .sk-circle3 {
		  -webkit-transform: rotate(60deg);
			  -ms-transform: rotate(60deg);
				  transform: rotate(60deg); }
		.sk-circle .sk-circle4 {
		  -webkit-transform: rotate(90deg);
			  -ms-transform: rotate(90deg);
				  transform: rotate(90deg); }
		.sk-circle .sk-circle5 {
		  -webkit-transform: rotate(120deg);
			  -ms-transform: rotate(120deg);
				  transform: rotate(120deg); }
		.sk-circle .sk-circle6 {
		  -webkit-transform: rotate(150deg);
			  -ms-transform: rotate(150deg);
				  transform: rotate(150deg); }
		.sk-circle .sk-circle7 {
		  -webkit-transform: rotate(180deg);
			  -ms-transform: rotate(180deg);
				  transform: rotate(180deg); }
		.sk-circle .sk-circle8 {
		  -webkit-transform: rotate(210deg);
			  -ms-transform: rotate(210deg);
				  transform: rotate(210deg); }
		.sk-circle .sk-circle9 {
		  -webkit-transform: rotate(240deg);
			  -ms-transform: rotate(240deg);
				  transform: rotate(240deg); }
		.sk-circle .sk-circle10 {
		  -webkit-transform: rotate(270deg);
			  -ms-transform: rotate(270deg);
				  transform: rotate(270deg); }
		.sk-circle .sk-circle11 {
		  -webkit-transform: rotate(300deg);
			  -ms-transform: rotate(300deg);
				  transform: rotate(300deg); }
		.sk-circle .sk-circle12 {
		  -webkit-transform: rotate(330deg);
			  -ms-transform: rotate(330deg);
				  transform: rotate(330deg); }
		.sk-circle .sk-circle2:before {
		  -webkit-animation-delay: -1.1s;
				  animation-delay: -1.1s; }
		.sk-circle .sk-circle3:before {
		  -webkit-animation-delay: -1s;
				  animation-delay: -1s; }
		.sk-circle .sk-circle4:before {
		  -webkit-animation-delay: -0.9s;
				  animation-delay: -0.9s; }
		.sk-circle .sk-circle5:before {
		  -webkit-animation-delay: -0.8s;
				  animation-delay: -0.8s; }
		.sk-circle .sk-circle6:before {
		  -webkit-animation-delay: -0.7s;
				  animation-delay: -0.7s; }
		.sk-circle .sk-circle7:before {
		  -webkit-animation-delay: -0.6s;
				  animation-delay: -0.6s; }
		.sk-circle .sk-circle8:before {
		  -webkit-animation-delay: -0.5s;
				  animation-delay: -0.5s; }
		.sk-circle .sk-circle9:before {
		  -webkit-animation-delay: -0.4s;
				  animation-delay: -0.4s; }
		.sk-circle .sk-circle10:before {
		  -webkit-animation-delay: -0.3s;
				  animation-delay: -0.3s; }
		.sk-circle .sk-circle11:before {
		  -webkit-animation-delay: -0.2s;
				  animation-delay: -0.2s; }
		.sk-circle .sk-circle12:before {
		  -webkit-animation-delay: -0.1s;
				  animation-delay: -0.1s; }

		@-webkit-keyframes sk-circleBounceDelay {
		  0%, 80%, 100% {
			-webkit-transform: scale(0);
					transform: scale(0);
		  } 40% {
			-webkit-transform: scale(1);
					transform: scale(1);
		  }
		}

		@keyframes sk-circleBounceDelay {
		  0%, 80%, 100% {
			-webkit-transform: scale(0);
					transform: scale(0);
		  } 40% {
			-webkit-transform: scale(1);
					transform: scale(1);
		  }
		}
</style>
	
<form class="form-horizontal form-label-left" id="add_form" role="form" action="<?php echo base_url('bc_purchase_order/save_bc_retur');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jenis_bc">Jenis BC</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<!-- <input type="text" class="form-control" id="jenis_bc" name="jenis_bc" value="BC Retur" placeholder="Jenis BC" readonly> -->
			<select class="form-control" id="jenis_bc" name="jenis_bc" style="width: 100%" required>
				<option value="" >-- Select Jenis --</option>
				<?php foreach($jenis_bc as $key) { ?>
					<option value="<?php echo $key['id']; ?>" ><?php echo $key['jenis_bc']; ?></option>
				<?php } ?>
			</select>
			<!-- <input type="hidden" id="type_text" name="type_text" value=""> -->
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nopendaftaran">No Pendaftaran</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="nopendaftaran" name="nopendaftaran" class="form-control col-md-7 col-xs-12" placeholder="No Pendaftaran" autocomplete="off" data-parsley-maxlength="100" autocomplete="off">
			<!-- <span id="nopendaftaran_loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking No Pendaftaran...</span>
			<span id="nopendaftaran_tick"></span> -->
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nopengajuan">No Pengajuan <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="nopengajuan" name="nopengajuan" class="form-control col-md-7 col-xs-12" placeholder="No Pengajuan" data-parsley-maxlength="100" autocomplete="off">
			<!-- <span id="nopengajuan_loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking No Pengajuan...</span>
			<span id="nopengajuan_tick"></span> -->
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tglpengajuan">Tanggal Pengajuan <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="100" type="text" id="tglpengajuan" name="tglpengajuan" class="form-control col-md-7 col-xs-12" placeholder="Tanggal Pengajuan" autocomplete="off" required>
		</div>
	</div>

	<div class="item form-group has-feedback">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_po">Nomor PO <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="no_po" name="no_po" required>
				<option value="" selected>-- Pilih PO --</option>
			</select>
		</div>
	</div>
	
	<div class="sk-circle x-hidden" id="loaders">
		<div class="sk-circle1 sk-child"></div>
		<div class="sk-circle2 sk-child"></div>
		<div class="sk-circle3 sk-child"></div>
		<div class="sk-circle4 sk-child"></div>
		<div class="sk-circle5 sk-child"></div>
		<div class="sk-circle6 sk-child"></div>
		<div class="sk-circle7 sk-child"></div>
		<div class="sk-circle8 sk-child"></div>
		<div class="sk-circle9 sk-child"></div>
		<div class="sk-circle10 sk-child"></div>
		<div class="sk-circle11 sk-child"></div>
		<div class="sk-circle12 sk-child"></div>
	</div>

	<div class="item form-group has-feedback x-hidden" id="div_kode_stok">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="kode_stok">Kode Stok</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="kode_stok" name="kode_stok" placeholder="Kode Stok" readonly>
			<input type="hidden" id="id_stok" name="id_stok">
		</div>
	</div>

	<div class="item form-group has-feedback x-hidden" id="div_nama_stok">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_stok">Nama Stok</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="nama_stok" name="nama_stok" placeholder="Nama Stok" readonly>
		</div>
	</div>

	<div class="item form-group has-feedback x-hidden" id="div_harga_dasar">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="harga_dasar">Harga Dasar</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="harga_dasar" name="harga_dasar" value="0" placeholder="Harga Dasar" readonly>
		</div>
	</div>

	<div class="item form-group has-feedback x-hidden" id="div_qty">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty">Qty <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="number" class="form-control" id="qty" name="qty" value="0" placeholder="Qty" autocomplete="off" required>
		</div>
	</div>

	<div class="item form-group has-feedback x-hidden" id="div_retur">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="retur">Pilih Retur <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="retur" name="retur" required>
				<option value="4" selected>Finish Good</option>
				<option value="5">Scrap</option>
			</select>
		</div>
	</div>
	
	<div class="item form-group has-feedback x-hidden" id="div_material">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="material">Material <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="material" name="material" >
				<option value="" selected>-- Pilih Material--</option>
				<option value="0">Input Baru</option>
			</select>
		</div>
	</div>
	
	<div class="item form-group has-feedback x-hidden" id="div_kode">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="stock_code">Stock Code <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="stock_code" name="stock_code" value="" placeholder="Stock Code" autocomplete="off" >
		</div>
	</div>
	
	<div class="item form-group has-feedback x-hidden" id="div_name">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="stock_name">Stock Name <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="stock_name" name="stock_name" value="" placeholder="Stock Name" autocomplete="off" >
		</div>
	</div>
	
	<div class="item form-group has-feedback x-hidden" id="div_desc">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="stock_desc">Stock Description <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<textarea class="form-control" id="stock_desc" name="stock_desc" value="" placeholder="Stock Description" autocomplete="off" ></textarea>
		</div>
	</div>
	
	<!-- <div class="item form-group has-feedback">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="file_bc">File</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="file" class="form-control" id="file_bc" name="file_bc" data-height="110" accept=".pdf, .txt, .doc, .docx"/>
			<span id="file_bc_tick">Hanya format file pdf,txt,doc,docx dengan besaran max 10Mb yang diterima.</span>
		</div>
	</div> -->

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah BC</button>
		</div>
	</div>
</form>

<script type="text/javascript">
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
		$('#tglpengajuan').datepicker({
			format: "dd/M/yyyy",
			autoclose: true,
			todayHighlight: true
		});

		$('#no_po').select2();
		get_purchaseOrder();

		/*$('#jenis_bc').change(function() {
			if(this.options[this.selectedIndex].value != '') $('#type_text').val(this.options[this.selectedIndex].text);
			else $('#type_text').val('');
		});

		$('#file_bc').bind('change', function() {
			if(this.files[0].size >= 9437184) {
				$('#file_bc').css('border', '3px #C33 solid');
				$('#file_bc_tick').empty();
				$("#file_bc_tick").append('<span class="fa fa-close"> Ukuran File Lebih dari 9Mb</span>');
				$('#file_bc_tick').show();
			}else {
				$('#file_bc').removeAttr("style");
				$('#file_bc_tick').empty();
				$("#file_bc_tick").append('Hanya format file pdf,txt,doc,docx dengan besaran max 10Mb yang diterima.');
				$('#file_bc_tick').show();
			}
		});*/

		/*$('#jenis_bc').on('change', function() {
			if(this.value) {
				var post_data = {
					'jenis_bc': $(this).find("option:selected").text().replace(/\D/g,'')
				};

				jQuery.ajax({
					type: "POST",
					url: "<?php echo base_url('bc_purchase_order/get_no_ajul');?>",
					data: post_data,
					cache: false,
					success: function(response){
						if(response.success == true) {
							// $('#nopengajuan').val(response.no_pengajuan);
							$('#menus').val(response.menus);
						}else {
							$('#nopengajuan').val('');
							$('#menus').val('');
						}
					}
				});
			}else {
				// $('#nopengajuan').val('');
				$('#menus').val('');
			}
		})*/

		$('#no_po').on('change', function() {
			if(this.value != '') {
				$('#div_kode_stok').show();
				$('#div_nama_stok').show();
				$('#div_harga_dasar').show();
				$('#div_qty').show();
				$('#div_retur').show();
				get_stok_by_po();
			}else {
				$('#kode_stok').val('');
				$('#div_kode_stok').hide();
				$('#div_nama_stok').hide();
				$('#div_harga_dasar').hide();
				$('#div_qty').hide();
				$('#div_retur').hide();
				$('#retur').val('');
				$('#div_material').hide();
				$('#div_kode').hide();
				$('#div_name').hide();
				$('#div_desc').hide();
			}
			// alert('test');
		})
		
		$('#retur').on('change', function() {
			if(this.value != '' && this.value == 5) {
				$('#div_material').show();
				get_type_material(this.value);
			}else{
				$('#div_material').hide();
				$('#material').removeAttr('required');
			}
			// alert('test');
		})
		
		$('#material').on('change', function() {
			if(this.value != '' && this.value == 0) {
				$('#div_kode').show();
				$('#div_name').show();
				$('#div_desc').show();
				$('#stock_code').addAttr('required');
				$('#stock_name').addAttr('required');
				$('#stock_desc').addAttr('required');
			}else{
				$('#div_kode').hide();
				$('#div_name').hide();
				$('#div_desc').hide();
				$('#stock_code').removeAttr('required');
				$('#stock_name').removeAttr('required');
				$('#stock_desc').removeAttr('required');
			}
			// alert('test');
		})
	});

	/*var last_nopendaftaran = $('#nopendaftaran').val();
	$('#nopendaftaran').on('input',function(event) {
		if($('#nopendaftaran').val() != last_nopendaftaran) {
			nopendaftaran_check();
		}
	});

	function nopendaftaran_check() {
		var nopendaftaran = $('#nopendaftaran').val();
		if(nopendaftaran.length > 3) {
			var post_data = {
				'nopendaftaran': nopendaftaran
			};

			$('#nopendaftaran_tick').empty();
			$('#nopendaftaran_tick').hide();
			$('#nopendaftaran_loading-us').show();
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url('bc_purchase_order/check_nopendaftaran');?>",
				data: post_data,
				cache: false,
				success: function(response){
					if(response.success == true){
						$('#nopendaftaran').css('border', '3px #090 solid');
						$('#nopendaftaran_loading-us').hide();
						$('#nopendaftaran_tick').empty();
						$("#nopendaftaran_tick").append('<span class="fa fa-check"> '+response.message+'</span>');
						$('#nopendaftaran_tick').show();
					}else {
						$('#nopendaftaran').css('border', '3px #C33 solid');
						$('#nopendaftaran_loading-us').hide();
						$('#nopendaftaran_tick').empty();
						$("#nopendaftaran_tick").append('<span class="fa fa-close"> '+response.message+'</span>');
						$('#nopendaftaran_tick').show();
					}
				}
			});
		}else {
			$('#nopendaftaran').css('border', '3px #C33 solid');
			$('#nopendaftaran_loading-us').hide();
			$('#nopendaftaran_tick').empty();
			$("#nopendaftaran_tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
			$('#nopendaftaran_tick').show();
		}
	}

	var last_nopengajuan = $('#nopengajuan').val();
	$('#nopengajuan').on('input',function(event) {
		if($('#nopengajuan').val() != last_nopengajuan) {
			nopengajuan_check();
		}
	});

	function nopengajuan_check() {
		var nopengajuan = $('#nopengajuan').val();
		if(nopengajuan.length > 3) {
			var post_data = {
				'nopengajuan': nopengajuan
			};

			$('#nopengajuan_tick').empty();
			$('#nopengajuan_tick').hide();
			$('#nopengajuan_loading-us').show();
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url('bc_purchase_order/check_nopengajuan');?>",
				data: post_data,
				cache: false,
				success: function(response){
					if(response.success == true){
						$('#nopengajuan').css('border', '3px #090 solid');
						$('#nopengajuan_loading-us').hide();
						$('#nopengajuan_tick').empty();
						$("#nopengajuan_tick").append('<span class="fa fa-check"> '+response.message+'</span>');
						$('#nopengajuan_tick').show();
					}else {
						$('#nopengajuan').css('border', '3px #C33 solid');
						$('#nopengajuan_loading-us').hide();
						$('#nopengajuan_tick').empty();
						$("#nopengajuan_tick").append('<span class="fa fa-close"> '+response.message+'</span>');
						$('#nopengajuan_tick').show();
					}
				}
			});
		}else {
			$('#nopengajuan').css('border', '3px #C33 solid');
			$('#nopengajuan_loading-us').hide();
			$('#nopengajuan_tick').empty();
			$("#nopengajuan_tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
			$('#nopengajuan_tick').show();
		}
	}*/

	function get_purchaseOrder() {
		$('#no_po').attr('disabled', 'disabled');

		$.ajax({
			type: "GET",
			url: "<?php echo base_url('bc_purchase_order/get_PO_retur');?>",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					for (var i = 0; i < r.length; i++) {
						var newOption = new Option(r[i].str_no_po, r[i].id_order, false, false);
						$('#no_po').append(newOption);
					}
					$('#no_po').removeAttr('disabled');
				}else $('#no_po').removeAttr('disabled');
			}
		});
	}

	function get_type_material(type_material) {
		$('#material').attr('disabled', 'disabled');
		
		var datapost = {
			"type_material" : type_material
		};
		
		$.ajax({
			type: 'POST',
			url: '<?php echo base_url('bc_purchase_order/get_type_material');?>',
			data: JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					for (var i = 0; i < r.length; i++) {
						var newOption = new Option(r[i].stock_code + ' - ' + r[i].stock_name, r[i].id_material, false, false);
						$('#material').append(newOption);
					}
					$('#material').removeAttr('disabled');
				}else $('#material').removeAttr('disabled');
			}
		});
	}
	
	function get_stok_by_po() {
		$('#kode_stok').val('');
		$('#id_stok').val('');
		$('#nama_stok').val('');

		var datapost={
			"po_id_order" : $('#no_po').val()
		};

		$.ajax({
			type: 'POST',
			url: '<?php echo base_url('bc_purchase_order/get_stok_by_po');?>',
			data: JSON.stringify(datapost),
			cache: true,
			contentType: false,
			processData: false,
			success: function(r) {
				if(r.length > 0) {
					for (var i = 0; i < r.length; i++) {
						$('#kode_stok').val(r[i].stock_code);
						$('#id_stok').val(r[i].id_stok);
						$('#nama_stok').val(r[i].part_no);
						$('#harga_dasar').val(r[i].symbol+' '+parseFloat(r[i].unit_price).toFixed(0));
						$('#qty').val(0);
					}
				}else {
					$('#kode_stok').val('');
					$('#id_stok').val('');
					$('#nama_stok').val('');
					$('#harga_dasar').val(0);
					$('#qty').val(0);
				}
			}
		});
	}

	function set_stok_by_id(id_stok) {
		var datapost={
			"po_id_order" : id_stok
		};

		$.ajax({
			type: 'POST',
			url: '<?php echo base_url('bc_purchase_order/set_stok_by_id');?>',
			data: JSON.stringify(datapost),
			cache: true,
			contentType: false,
			processData: false,
			success: function(r) {
				if(r.length > 0) {
					for (var i = 0; i < r.length; i++) {
						var newOption = new Option(r[i].stock_code, r[i].id_stok, false, false);
						$('#kode_stok').append(newOption);
						$('#kode_stok').val(r[i].id_stok).trigger('change');
						$('#nama_stok').val(r[i].part_no);
						$('#harga_dasar').val(r[i].symbol+' '+parseFloat(r[i].unit_price).toFixed(0));
						$('#qty').val(0);
					}
					$('#kode_stok').removeAttr('disabled');
				}else {
					$('#kode_stok').val('').trigger('change');
					$('#nama_stok').val('');
					$('#harga_dasar').val(0);
					$('#qty').val(0);
					$('#kode_stok').removeAttr('disabled');
				}
			}
		});
	}

	$('#add_form').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		var formData = new FormData(this);

		/*if($('#file_bc')[0].files.length > 0) {
			if($('#file_bc')[0].files[0].size <= 9437184) {
				save_Form(formData, $(this).attr('action'));
			}else {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Tambah BC");
				swal("Failed!", "Ukuran File Terlalu besar, silahkan cek kembali", "error");
			}
		}else {
			save_Form(formData, $(this).attr('action'));
		}*/
		save_Form(formData, $(this).attr('action'));
	}));

	function save_Form(formData, url) {
		$.ajax({
			type:'POST',
			url: url,
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						$('#panel-modal').modal('toggle');
						get_list_bc();
						// window.location.href = "<?php echo base_url('bc_purchase_order');?>";
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Tambah BC");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah BC");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}
</script>
