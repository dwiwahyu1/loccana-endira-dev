<style type="text/css">
	.reportBoxAllSide {
		border: 1px solid black;
		page-break-before: always;
	}

	.reportBoxNoTop {
		border-bottom: 1px solid;
		border-left: 1px solid;
		border-right: 1px solid;
		border-color: black;
		page-break-before: always;
	}

	.reportBoxNoTopLeft {
		border-bottom: 1px solid;
		border-right: 1px solid;
		border-color: black;
		page-break-before: always;
	}

	.reportBoxNoTopRight {
		border-bottom: 1px solid;
		border-left: 1px solid;
		border-color: black;
		page-break-before: always;
	}

	.reportBoxNoLeft {
		border-top: 1px solid;
		border-bottom: 1px solid;
		border-right: 1px solid;
		border-color: black;
		page-break-before: always;
	}

	.reportBoxNoRight {
		border-top: 1px solid;
		border-bottom: 1px solid;
		border-left: 1px solid;
		border-color: black;
		page-break-before: always;
	}

	.reportBoxNoBottom {
		border-top: 1px solid;
		border-left: 1px solid;
		border-right: 1px solid;
		border-color: black;
		page-break-before: always;
	}

	.separator {
		display: flex;
		align-items: center;
		text-align: center;
	}
	
	.separator::before, .separator::after {
		content: '';
		flex: 1;
		border-bottom: 1px solid #000;
		border-style: dashed;
	}

	.separator::before {
		margin-right: .25em;
	}

	.separator::after {
		margin-left: .25em;
	}

	.tableTitle {
		border: 1px solid;
		border-color: black;
		box-sizing: border-box;
		text-align: left;
		page-break-before: always;
	}

	.tableTitle tr td {
		border: 1px solid;
		border-color: black;
		padding-left: 10px;
		padding-right: 10px;
	}

	.tableReport {
		box-sizing: border-box;
		text-align: left;
		margin: 10px auto;
		page-break-before: always;
	}

	.tablewOnlyBorder {
		box-sizing: border-box;
		page-break-before: always;
	}

	.tablewOnlyBorder tr td{
		border-right: 1px solid black;
		border-left: 1px solid black;
		border-bottom: 1px solid black;
	}

	.tableReportwBorder {
		box-sizing: border-box;
		text-align: left;
		margin: 10px auto;
		page-break-before: always;
	}

	.tableReportwBorder tr td {
		border-left: 1px solid;
		border-right: 1px solid;
		border-bottom: 1px solid;
		border-color: black;
		padding-left: 20px;
		padding-right: 20px;
	}

	.tableReportwBorder2 {
		border-left: 1px solid;
		border-right: 1px solid;
		border-bottom: 1px solid;
		border-color: black;
		box-sizing: border-box;
		text-align: left;
		margin: 10px auto;
		page-break-before: always;
	}

	.tableReportwBorder2 thead tr td {
		border-left: 1px solid;
		border-right: 1px solid;
		border-bottom: 1px solid;
		border-color: black;
		padding-left: 10px;
		padding-right: 5px;
	}

	.tableReportwBorder2 tbody tr td {
		border-left: 1px solid;
		border-right: 1px solid;
		border-color: black;
		padding-left: 10px;
	}

	.tableReportvAlign tr td {
		vertical-align: baseline;
	}

	.hrBox {
		margin-top: 0px;
		margin-left: -10px;
		margin-right: -10px;
		margin-bottom: 0px;
		border: 0.5px solid;
		border-color: black;
	}

	.hrinTable {
		margin-top: 0px;
		margin-left: -21px;
		margin-right: -21px;
		margin-bottom: 0px;
		border: 0.5px solid;
		border-color: black;
	}

	.boxTipeBC {
		position: absolute;
		padding-right: 20px;
		margin-top: 55px;
	}

	.boxPage {
		position: absolute;
		padding-right: 50px;
		margin-top: 40px;
	}

	.boxPage2 {
		position: absolute;
		padding-right: 60px;
		margin-top: 10px;
	}

	h4 { line-height: 20px !important; }
</style>

<!-- Start Lembar 1 -->
<div class="card-box" style="margin-top: 20px;">
	<div class="btn-group pull-left" style="padding-bottom: 20px;">
		<a class="btn btn-info btn-sm" id="back_bc" name="back_bc" data-toggle="tooltip" data-placement="top" title="Back">
			<i class="fa fa-arrow-left"></i>
		</a>
	</div>

	<div class="row">
		<table class="tableTitle" cellpadding="0" width="100%">
			<tr>
				<td class="dt-body-center" style="width: 16%;">
					<h3><label><?php if (isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']); ?></label></h3>
				</td>
				<td class="dt-body-center">
					<h4><label><?php if(isset($bc[0]['desc_bc'])) echo wordwrap(strtoupper($bc[0]['desc_bc']), 70, "<br>\n"); ?></label></h4>
				</td>
			</tr>
		</table>

		<div class="col-md-12 reportBoxNoTop">
			<div class="col-sm-12">
				<h5><label style="margin-bottom: 0px;">HEADER</label></h5>
			</div>
			<div class="col-sm-12 text-right boxPage">
				<label>Halaman Ke-1 dari 5</label>
			</div>
		</div>

		<div class="col-md-6 reportBoxNoTopRight" style="margin-right: -1px;">
			<div class="col-sm-6">
				<table class="tableReport" cellpadding="0" width="100%">
					<tr>
						<td style="width: 60%;">NOMOR PENGAJUAN</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td colspan="2">A. KANTOR PABEAN</td>
					</tr>
					<tr>
						<td style="padding-left: 15px;">1. Kantor Asli</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td style="padding-left: 15px;">2. Kantor Tujuan</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td>B. JENIS TPB ASAL</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td>C. JENIS TPB TUJUAN</td>
						<td>: &nbsp;</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="col-md-6 reportBoxNoTopLeft" style="height: 65px;">
			<div class="col-sm-12">
				<table class="tableReport" cellpadding="0" width="100%" style="margin-top: 25px;">
					<tr>
						<td style="width: 30%;">D. TUJUAN PENGIRIMAN</td>
						<td>: &nbsp;</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="col-md-6 reportBoxNoTop" style="height: 76px;">
			<div class="col-sm-12">
				<table class="tableReport" cellpadding="0" width="100%">
					<thead>
						<tr>
							<td colspan="2">G. KOLOM KHUSUS BEA DAN CUKAI</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="padding-left: 20px; width: 30%">Nomor Pendaftaran</td>
							<td>: &nbsp;</td>
						</tr>
						<tr>
							<td style="padding-left: 20px; width: 30%">Tanggal</td>
							<td>: &nbsp;</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="col-sm-12 reportBoxNoTop">
			<div class="col-sm-12">
				<h5><label style="margin-bottom: 0px;">E. DATA PEMBERITAHUAN :</label></h5>
			</div>
		</div>

		<div class="col-md-6 reportBoxNoTop">
			<div class="col-sm-12">
				<h5>TPB ASAL BARANG</h5>
			</div>
		</div>

		<div class="col-md-6 reportBoxNoTopLeft">
			<div class="col-sm-12">
				<h5>TPB TUJUAN BARANG</h5>
			</div>
		</div>

		<div class="col-md-12 reportBoxNoTop">
			<div class="col-sm-6">
				<table class="tableReport" cellpadding="0" width="100%">
					<tr>
						<td style="width: 29%;">1. NPWP</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td>2. NAMA</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td>3. ALAMAT</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td>4. No Izin TPB</td>
						<td>: &nbsp;</td>
					</tr>
				</table>
			</div>

			<div class="col-sm-6">
				<table class="tableReport" cellpadding="0" width="100%">
					<tr>
						<td style="width: 31%;">5. NPWP</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td>6. NAMA</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td>7. ALAMAT</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td>8. No Izin TPB</td>
						<td>: &nbsp;</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="col-md-12 reportBoxNoTop">
			<div class="col-sm-12">
				<h5><label style="margin-bottom: 0px;">DOKUMEN PELENGKAP PABEAN</label></h5>
			</div>
		</div>

		<div class="col-md-12 reportBoxNoTop">
			<div class="col-sm-6">
				<table class="tableReport" cellpadding="0" width="100%">
					<tr>
						<td style="width: 29%;">9. Invoice</td>
						<td style="width: 40%;">: &nbsp;</td>
						<td>tgl. </td>
					</tr>
					<tr>
						<td>10. Packing List</td>
						<td>: &nbsp;</td>
						<td>tgl. </td>
					</tr>
					<tr>
						<td>11. Kontrak</td>
						<td>: &nbsp;</td>
						<td>tgl. </td>
					</tr>
				</table>
			</div>

			<div class="col-sm-6">
				<table class="tableReport" cellpadding="0" width="100%">
					<tr>
						<td style="width: 31%;">12. Surat Jalan</td>
						<td style="width: 40%;">: &nbsp;</td>
						<td>tgl. </td>
					</tr>
					<tr>
						<td>10. Surat Keputusan/Persetujuan</td>
						<td>: &nbsp;</td>
						<td>tgl. </td>
					</tr>
					<tr>
						<td>11. Lainnya</td>
						<td>: &nbsp;</td>
						<td>tgl. </td>
					</tr>
				</table>
			</div>
		</div>

		<div class="col-md-12 reportBoxNoTop">
			<div class="col-sm-12">
				<h5><label style="margin-bottom: 0px;">RIWAYAT BARANG</label></h5>
			</div>
		</div>

		<div class="col-md-12 reportBoxNoTop">
			<div class="col-sm-12">
				<table class="tableReport" cellpadding="0" width="100%">
					<tr>
						<td rowspan="3" style="width: 35px;">15.</td>
					</tr>
					<tr>
						<td style="width: 20%;">a. Nomor dan Tanggal BC 2.7 Asal</td>
						<td style="width: 45%;">: &nbsp;</td>
						<td>tgl.</td>
					</tr>
					<tr>
						<td>b. Nomor dan Tanggal BC 2.3</td>
						<td>: &nbsp;</td>
						<td>tgl.</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="col-md-12 reportBoxNoTop">
			<div class="col-sm-12">
				<h5><label style="margin-bottom: 0px;">DATA PERDAGANGAN</label></h5>
			</div>
		</div>

		<div class="col-md-12 reportBoxNoTop">
			<div class="col-sm-6">
				<table class="tableReport" cellpadding="0" width="100%">
					<tr>
						<td style="width: 30%;">16. Jenis Valuta Asing</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td>17. CIF</td>
						<td>: &nbsp;</td>
					</tr>
				</table>
			</div>

			<div class="col-sm-6">
				<table class="tableReport" cellpadding="0" width="100%">
					<tr>
						<td style="width: 30%;">16. Harga Penyerahan</td>
						<td>: &nbsp;</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="col-md-8 reportBoxNoTop">
			<div class="col-sm-12">
				<h5><label style="margin-bottom: 0px;">DATA PENGANGKUTAN</label></h5>
			</div>
		</div>

		<div class="col-md-4 reportBoxNoTopLeft">
			<div class="col-sm-12">
				<h5><label style="margin-bottom: 0px;">SEGEL (DIISI OLEH BEA DAN CUKAI)</label></h5>
			</div>
		</div>

		<div class="col-md-8 reportBoxNoTop">
			<div class="col-sm-6">
				<table class="tableReport" cellpadding="0" width="100%">
					<tr>
						<td>19. Jenis Sarana Pengangkut Darat</td>
					</tr>
				</table>
			</div>

			<div class="col-sm-6">
				<table class="tableReport" cellpadding="0" width="100%">
					<tr>
						<td>20. No Polisi</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="col-md-4">
			<table class="tablewOnlyBorder" cellpadding="0" style="padding-right: 0px; padding-left: 0px; margin-top: 0px; margin-left: -10.5px; width: 105.5%;">
				<thead>
					<tr>
						<td class="dt-body-center" colspan="2">BC ASAL</td>
						<td class="dt-body-center">23. Catatan BC Tujuan</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="dt-body-center">21. No Segel</td>
						<td class="dt-body-center">22. Jenis</td>
						<td class="dt-body-center" style="border-bottom: 0px;"></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td style="height: 120px;"></td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="col-md-8 reportBoxNoTop" style="margin-top: -121px;">
			<div class="col-sm-12">
				<h5><label style="margin-bottom: 0px;">DATA PETI KEMAS DAN PENGEMAS</label></h5>
			</div>
		</div>

		<div class="col-md-8 reportBoxNoTopRight" style="margin-top: -84px; height: 84px;">
			<div class="col-sm-12">
				<table class="tableReport" cellpadding="0" width="100%">
					<tr>
						<td>24. Merek dan No Kemasan/Peti Kemas dan Jumlah</td>
						<td>25. Jumlah dan Jenis Kemasan</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="col-md-12 reportBoxNoTop">
			<div class="col-sm-12">
				<table class="tableReport" cellpadding="0" width="100%">
					<tr>
						<td style="width: 33%;">26. Volume (m3) :</td>
						<td style="width: 33%;">27. Berat Kotor (Kg) :</td>
						<td style="width: 33%;">28. Berat Bersih (Kg) :</td>
					</tr>
				</table>
			</div>
		</div>

		<table class="tableReportwBorder tableReportvAlign" cellpadding="0" width="100%" style="margin-bottom: 0px;">
			<tr>
				<td style="width: 5%;">29. No</td>
				<td style="width: 40%;">30. Pos tarif/HS, uraian jumlah dan jenis barang secara lengkap, kode barang, merk, tipe, ukuran, dan spesifikasi lain</td>
				<td>31. <ul style="margin-top: -20px;">
						<li>Jumlah & Jenis Satuan</li>
						<li>Berat Bersih (Kg)</li>
						<li>Volume (m3)</li>
				</ul></td>
				<td>32. <ul style="margin-top: -20px;">
					<li>Nilai CIF</li>
					<li>Harga penyerahan</li>
				</ul></td>
			</tr>
		</table>

		<div class="col-md-12 reportBoxNoTop" style="margin-top: -10px;">
			<div class="col-sm-12 separator" style="padding-top: 40px; padding-bottom: 40px;">
				<h5>2 Jenis Barang. Lihat lembar lanjutan</h5>
			</div>
		</div>

		<div class="col-md-6 reportBoxNoTop">
			<div class="col-sm-12">
				<h5><label style="margin-bottom: 0px;">F. TANDA TANGAN PENGUSAHA TPB</label></h5>
			</div>
		</div>

		<div class="col-md-6 reportBoxNoTopLeft">
			<div class="col-sm-12">
				<h5><label style="margin-bottom: 0px;">H. UNTUK PEJABAT BEA DAN CUKAI</label></h5>
			</div>
		</div>

		<div class="col-md-6 reportBoxNoTop">
			<div class="col-sm-12">Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberikan dalam pemberitahuan pabean ini</div>
			<div class="col-sm-12 text-center" style="height: 100px;">BANDUNG, <?php
				if(isset($bc[0]['tanggal_pengajuan'])) echo date('d F Y', strtotime($bc[0]['tanggal_pengajuan']));
			?></div>
			<div class="col-sm-12 text-center">NAMA</div>
		</div>

		<div class="col-md-6 reportBoxNoTopLeft">
			<div class="col-sm-12">
				<table class="tablewOnlyBorder" cellpadding="0" style="padding-right: 0px; padding-left: 0px; margin-top: 0px; margin-left: -21.5px; width: 107.5%; margin-bottom: -1.5px;">
					<thead>
						<tr>
							<td class="dt-body-center">Kantor Pabean Asal</td>
							<td class="dt-body-center">Kantor Pabean Tujuan</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="height: 100px; border-bottom: 0px"></td>
							<td style="height: 100px; border-bottom: 0px"></td>
						</tr>
						<tr>
							<td style="padding-left: 10px; border-top: 0px; border-bottom: 0px;">Nama &emsp;: </td>
							<td style="padding-left: 10px; border-top: 0px; border-bottom: 0px;">Nama &emsp;: </td>
						</tr>
						<tr>
							<td style="padding-left: 10px;">NIP &emsp;&emsp;: </td>
							<td style="padding-left: 10px;">NIP &emsp;&emsp;: </td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<!-- End Lembar 1 -->

<!-- Start Lembar 2 -->
<div class="card-box">
	<div class="row">
		<table class="tableTitle" cellpadding="0" width="100%">
			<tr>
				<td class="dt-body-center" style="width: 16%;">
					<h3><label><?php if (isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']); ?></label></h3>
				</td>
				<td class="dt-body-center">
					<h4><label>LEMBAR LANJUTAN<br/>DATA BARANG</label></h4>
				</td>
			</tr>
		</table>

		<div class="col-md-12 reportBoxNoTop">
			<div class="col-sm-12">
				<h5><label style="margin-bottom: 0px;">HEADER</label></h5>
			</div>
			<div class="col-sm-12 text-right boxPage">
				<label>Halaman Ke-2 dari 5</label>
			</div>
		</div>

		<div class="col-md-6 reportBoxNoTopRight" style="margin-right: -1px;">
			<div class="col-sm-6">
				<table class="tableReport" cellpadding="0" width="100%">
					<tr>
						<td style="width: 60%;">NOMOR PENGAJUAN</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td colspan="2">A. KANTOR PABEAN</td>
					</tr>
					<tr>
						<td style="padding-left: 15px;">1. Kantor Asli</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td style="padding-left: 15px;">2. Kantor Tujuan</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td>B. JENIS TPB ASAL</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td>C. JENIS TPB TUJUAN</td>
						<td>: &nbsp;</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="col-md-6 reportBoxNoTopLeft" style="height: 65px;">
			<div class="col-sm-12">
				<table class="tableReport" cellpadding="0" width="100%" style="margin-top: 25px;">
					<tr>
						<td style="width: 30%;">D. TUJUAN PENGIRIMAN</td>
						<td>: &nbsp;</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="col-md-6 reportBoxNoTop" style="height: 76px;">
			<div class="col-sm-12">
				<table class="tableReport" cellpadding="0" width="100%">
					<thead>
						<tr>
							<td colspan="2">G. KOLOM KHUSUS BEA DAN CUKAI</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="padding-left: 20px; width: 30%">Nomor Pendaftaran</td>
							<td>: &nbsp;</td>
						</tr>
						<tr>
							<td style="padding-left: 20px; width: 30%">Tanggal</td>
							<td>: &nbsp;</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<table class="tableReportwBorder tableReportvAlign" cellpadding="0" width="100%" style="margin-bottom: 0px;">
			<tr>
				<td style="width: 5%;">29. No</td>
				<td style="width: 40%;">30. Pos tarif/HS, uraian jumlah dan jenis barang secara lengkap, kode barang, merk, tipe, ukuran, dan spesifikasi lain</td>
				<td>31. <ul style="margin-top: -20px;">
						<li>Jumlah & Jenis Satuan</li>
						<li>Berat Bersih (Kg)</li>
						<li>Volume (m3)</li>
				</ul></td>
				<td>32. <ul style="margin-top: -20px;">
					<li>Nilai CIF</li>
					<li>Harga penyerahan</li>
				</ul></td>
			</tr>
		</table>

		<div class="col-md-6 reportBoxNoTop">
			<div class="col-sm-12">
				<h5><label style="margin-bottom: 0px;">F. TANDA TANGAN PENGUSAHA TPB</label></h5>
			</div>
		</div>

		<div class="col-md-6 reportBoxNoTopLeft">
			<div class="col-sm-12">
				<h5><label style="margin-bottom: 0px;">H. UNTUK PEJABAT BEA DAN CUKAI</label></h5>
			</div>
		</div>

		<div class="col-md-6 reportBoxNoTop">
			<div class="col-sm-12">Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberikan dalam pemberitahuan pabean ini</div>
			<div class="col-sm-12 text-center" style="height: 100px;">BANDUNG, <?php
				if(isset($bc[0]['tanggal_pengajuan'])) echo date('d F Y', strtotime($bc[0]['tanggal_pengajuan']));
			?></div>
			<div class="col-sm-12 text-center">NAMA</div>
		</div>

		<div class="col-md-6 reportBoxNoTopLeft">
			<div class="col-sm-12">
				<table class="tablewOnlyBorder" cellpadding="0" style="padding-right: 0px; padding-left: 0px; margin-top: 0px; margin-left: -21.5px; width: 107.5%; margin-bottom: -1.5px;">
					<thead>
						<tr>
							<td class="dt-body-center">Kantor Pabean Asal</td>
							<td class="dt-body-center">Kantor Pabean Tujuan</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="height: 100px; border-bottom: 0px"></td>
							<td style="height: 100px; border-bottom: 0px"></td>
						</tr>
						<tr>
							<td style="padding-left: 10px; border-top: 0px; border-bottom: 0px;">Nama &emsp;: </td>
							<td style="padding-left: 10px; border-top: 0px; border-bottom: 0px;">Nama &emsp;: </td>
						</tr>
						<tr>
							<td style="padding-left: 10px;">NIP &emsp;&emsp;: </td>
							<td style="padding-left: 10px;">NIP &emsp;&emsp;: </td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<!-- End Lembar 2 -->

<!-- Start Lembar 3 -->
<div class="card-box">
	<div class="row">
		<table class="tableTitle" cellpadding="0" width="100%">
			<tr>
				<td class="dt-body-center" style="width: 16%;">
					<h3><label><?php if (isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']); ?></label></h3>
				</td>
				<td class="dt-body-center">
					<h4><label>LEMBAR LANJUTAN<br/>DOKUMEN PELENGKAP PABEAN</label></h4>
				</td>
			</tr>
		</table>

		<div class="col-md-12 reportBoxNoTop">
			<div class="col-sm-12">
				<h5><label style="margin-bottom: 0px;">HEADER</label></h5>
			</div>
			<div class="col-sm-12 text-right boxPage">
				<label>Halaman Ke-3 dari 5</label>
			</div>
		</div>

		<div class="col-md-6 reportBoxNoTopRight" style="margin-right: -1px;">
			<div class="col-sm-6">
				<table class="tableReport" cellpadding="0" width="100%">
					<tr>
						<td style="width: 60%;">NOMOR PENGAJUAN</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td colspan="2">A. KANTOR PABEAN</td>
					</tr>
					<tr>
						<td style="padding-left: 15px;">1. Kantor Asli</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td style="padding-left: 15px;">2. Kantor Tujuan</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td>B. JENIS TPB ASAL</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td>C. JENIS TPB TUJUAN</td>
						<td>: &nbsp;</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="col-md-6 reportBoxNoTopLeft" style="height: 65px;">
			<div class="col-sm-12">
				<table class="tableReport" cellpadding="0" width="100%" style="margin-top: 25px;">
					<tr>
						<td style="width: 30%;">D. TUJUAN PENGIRIMAN</td>
						<td>: &nbsp;</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="col-md-6 reportBoxNoTop" style="height: 76px;">
			<div class="col-sm-12">
				<table class="tableReport" cellpadding="0" width="100%">
					<thead>
						<tr>
							<td colspan="2">G. KOLOM KHUSUS BEA DAN CUKAI</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="padding-left: 20px; width: 30%">Nomor Pendaftaran</td>
							<td>: &nbsp;</td>
						</tr>
						<tr>
							<td style="padding-left: 20px; width: 30%">Tanggal</td>
							<td>: &nbsp;</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<table class="tableReportwBorder2 tableReportvAlign" cellpadding="0" width="100%" style="margin-bottom: 0px;">
			<thead>
				<tr>
					<td class="dt-body-center" style="width: 5%;">No.</td>
					<td class="dt-body-center">JENIS DOKUMEN</td>
					<td class="dt-body-center">NOMOR</td>
					<td class="dt-body-center">TANGGAL</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>1</td>
					<td>INVOICE</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>2</td>
					<td>PACKING LIST</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>3</td>
					<td>SURAT JALAN</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>4</td>
					<td>BC 2.7</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>5</td>
					<td>FAKTUR PAJAk</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>6</td>
					<td>BC 2.3</td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>

		<div class="col-md-12 reportBoxNoTop">
			<div class="col-sm-12 text-center">
				<h5><label style="margin-bottom: 0px;">E. TANDA TANGAN PENGUSAHA TPB</label></h5>
			</div>
		</div>

		<div class="col-md-12 reportBoxNoTop">
			<div class="col-sm-12 text-center">Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberikan dalam pemberitahuan pabean ini</div>
			<div class="col-sm-12 text-center" style="height: 100px;">BANDUNG, <?php
				if(isset($bc[0]['tanggal_pengajuan'])) echo date('d F Y', strtotime($bc[0]['tanggal_pengajuan']));
			?></div>
			<div class="col-sm-12 text-center">NAMA</div>
		</div>
	</div>
</div>
<!-- End Lembar 3 -->

<!-- Start Lembar 4 -->
<div class="card-box">
	<div class="row">
		<table class="tableTitle" cellpadding="0" width="100%">
			<tr>
				<td class="dt-body-center" style="width: 16%;">
					<h3><label><?php if (isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']); ?></label></h3>
				</td>
				<td class="dt-body-center">
					<h4><label>LEMBAR LAMPIRAN<br/>KONVERSI PEMAKAIAN BAHAN (DIJUAL)</label></h4>
				</td>
			</tr>
		</table>

		<div class="col-md-12 reportBoxNoTop">
			<div class="col-sm-12">
				<h5><label style="margin-bottom: 0px;">HEADER</label></h5>
			</div>
			<div class="col-sm-12 text-right boxPage">
				<label>Halaman Ke-4 dari 5</label>
			</div>
		</div>

		<div class="col-md-6 reportBoxNoTopRight" style="margin-right: -1px;">
			<div class="col-sm-6">
				<table class="tableReport" cellpadding="0" width="100%">
					<tr>
						<td style="width: 60%;">NOMOR PENGAJUAN</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td colspan="2">A. KANTOR PABEAN</td>
					</tr>
					<tr>
						<td style="padding-left: 15px;">1. Kantor Asli</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td style="padding-left: 15px;">2. Kantor Tujuan</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td>B. JENIS TPB ASAL</td>
						<td>: &nbsp;</td>
					</tr>
					<tr>
						<td>C. JENIS TPB TUJUAN</td>
						<td>: &nbsp;</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="col-md-6 reportBoxNoTopLeft" style="height: 65px;">
			<div class="col-sm-12">
				<table class="tableReport" cellpadding="0" width="100%" style="margin-top: 25px;">
					<tr>
						<td style="width: 30%;">D. TUJUAN PENGIRIMAN</td>
						<td>: &nbsp;</td>
					</tr>
				</table>
			</div>
		</div>

		<div class="col-md-6 reportBoxNoTop" style="height: 76px;">
			<div class="col-sm-12">
				<table class="tableReport" cellpadding="0" width="100%">
					<thead>
						<tr>
							<td colspan="2">G. KOLOM KHUSUS BEA DAN CUKAI</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="padding-left: 20px; width: 30%">Nomor Pendaftaran</td>
							<td>: &nbsp;</td>
						</tr>
						<tr>
							<td style="padding-left: 20px; width: 30%">Tanggal</td>
							<td>: &nbsp;</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

		<div class="col-sm-6 reportBoxNoTop text-center">
			<div class="col-sm-12">
				<h5><label style="margin-bottom: 0px;">Barang Jadi</label></h5>
			</div>
		</div>

		<div class="col-sm-6 reportBoxNoTopLeft text-center">
			<div class="col-sm-12">
				<h5><label style="margin-bottom: 0px;">Bahan Baku yang Digunakan</label></h5>
			</div>
		</div>

		<table class="tableReportwBorder tableReportvAlign" cellpadding="0" width="100%" style="margin-bottom: 0px;">
			<thead>
				<tr>
					<td style="width: 5%;">No</td>
					<td>Pos tarif/HS, uraian jumlah dan jenis barang secara lengkap, kode barang, merk, tipe, ukuran, dan spesifikasi lain</td>
					<td>Jumlah</td>
					<td>Satuan</td>
					<td>Pos tarif/HS, uraian jumlah dan jenis barang secara lengkap, kode barang, merk, tipe, ukuran, dan spesifikasi lain</td>
					<td>Jumlah</td>
					<td>Satuan</td>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>

		<div class="col-md-6 reportBoxNoTop" style="height: 198px;">
			<div class="col-sm-12"></div>
		</div>

		<div class="col-md-6 reportBoxNoTopLeft">
			<div class="col-sm-12">
				<h5><label style="margin-bottom: 0px;">F. TANDA TANGAN PENGUSAHA TPB</label></h5>
			</div>
		</div>

		<div class="col-md-6 reportBoxNoTopLeft">
			<div class="col-sm-12">Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberikan dalam pemberitahuan pabean ini</div>
			<div class="col-sm-12 text-center" style="height: 100px;">BANDUNG, <?php
				if(isset($bc[0]['tanggal_pengajuan'])) echo date('d F Y', strtotime($bc[0]['tanggal_pengajuan']));
			?></div>
			<div class="col-sm-12 text-center">NAMA</div>
		</div>
	</div>
</div>
<!-- End Lembar 4 -->

<!-- Start Lembar 5 -->
<div class="card-box">
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-12 text-center">
				<h4><label>LEMBAR LAMPIRAN<br/>DATA BARANG DAN/ATAU BAHAN IMPOR<br/><?php
					if(isset($bc[0]['desc_bc'])) echo str_replace('PEMBERITAHUAN PENGELUARAN BARANG ', '', strtoupper($bc[0]['desc_bc']));
				?></label></h4>
			</div>

			<div class="col-md-12 text-right boxTipeBC">
				<h5><?php if (isset($bc[0]['jenis_bc'])) echo strtoupper($bc[0]['jenis_bc']); ?></h5>
			</div>
		</div>

		<div class="col-md-12 reportBoxAllSide">
			<div class="col-sm-12 text-right boxPage2">
				<label>Halaman Ke-5 dari 5</label>
			</div>

			<div class="col-sm-12">
				<table class="tableReport" cellspacing="0" width="100%">
					<tr>
						<td style="width: 13%;">Kantor Pabean</td>
						<td style="width: 40%;">: &nbsp; KPPBC Bandung</td>
						<td style="width: 13%;"></td>
						<td></td>
					</tr>
					<tr>
						<td>Nomor Pengajuaan</td>
						<td>: &nbsp; <?php if(isset($bc[0]['no_pengajuan'])) echo $bc[0]['no_pengajuan']; ?></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>Nomor Pendaftaran</td>
						<td>: &nbsp; <?php if(isset($bc[0]['no_pendaftaran'])) echo $bc[0]['no_pendaftaran']; ?></td>
						<td>Tanggal Pendaftaran</td>
						<td>: &nbsp; <?php if(isset($bc[0]['tanggal_pengajuan'])) echo date('d-m-Y', strtotime($bc[0]['tanggal_pengajuan'])); ?></td>
					</tr>
				</table>
			</div>
		</div>

		<table class="tableReportwBorder tableReportvAlign" cellpadding="0" width="100%" style="margin-bottom: 0px;">
			<thead>
				<tr>
					<td style="width: 5%;">No Urut Barang</td>
					<td>
						- Kode Kantor<br/>
						- No/Tgl Daftar BC 2.3, BC 2.7
					</td>
					<td>No. Urut Dalam<br/>
						- BC 2.3<br/>
						- BC 2.7
					</td>
					<td>
						- Pos tarif/HS, uraian jumlah dan jenis barang secara lengkap, kode barang, merk, tipe, ukuran, dan spesifikasi lain<br/>
						- Perijinan/Fasilitas
					</td>
					<td>
						- Jumlah<br/>
						- Satuan
					</td>
					<td>Nilai<br/>
						- CIF<br/>
						- Harga Penyerahan (Rp)
					</td>
					<td>Nilai (Rp) BM, BMT, Cukai, PPN, PPnBM, PPh 22</td>
				</tr>
				<tr>
					<td class="dt-body-center">1</td>
					<td class="dt-body-center">2</td>
					<td class="dt-body-center">3</td>
					<td class="dt-body-center">4</td>
					<td class="dt-body-center">5</td>
					<td class="dt-body-center">6</td>
					<td class="dt-body-center">7</td>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>

		<div class="col-md-12 reportBoxNoTop" style="margin-top: -10px; height: 250px;">
			<div class="col-sm-12">
				<table class="tableReport" cellpadding="0" width="100%">
					<thead>
						<tr>
							<td colspan="2">C. PENGESAHAN PENGUSAHA TPB</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td colspan="2">Dengan ini saya menyatakan bertanggung jawab atas kebenaran hal-hal yang diberitahukan dalam pemberitahuan pabean ini</td>
						</tr>
						<tr>
							<td style="width: 12%;">Tempat, Tanggal</td>
							<td>: &nbsp; BANDUNG, <?php if(isset($bc[0]['tanggal_pengajuan'])) echo date('d F Y', strtotime($bc[0]['tanggal_pengajuan'])); ?></td>
						</tr>
						<tr>
							<td>Nama Lengkap</td>
							<td>: &nbsp;</td>
						</tr>
						<tr>
							<td>Jabatan</td>
							<td>: &nbsp;</td>
						</tr>
						<tr>
							<td colspan="2">Tanda Tangan dan Stempel Perusahaan :</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<!-- End Lembar 5 -->

<script type="text/javascript">
	var id_bc = "<?php if(isset($bc[0])){ echo $bc[0]['id']; } ?>";

	$(document).ready(function() {
		$('#back_bc').on('click', function() {
			$('#title_Menu').html('Bea Cukai');
			$('#div_list_bc').show();
			$('#div_detail_bc').hide();
			$('#div_detail_bc').html('');
		})

		$('#downloadDetail').on('click', function() {

		})
	});
</script>