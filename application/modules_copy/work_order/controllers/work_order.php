<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Work_Order extends MX_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('work_order/work_order_model');
		$this->load->model('pekerjaan/pekerjaan_model');
		$this->load->library('sequence');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'work_order/views/index');
	}

	function lists() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

		$order_fields = array(
			'a.no_work_order', 'a.delivery_date', 'a.tujuan', 'a.term_of_payment', 'a.total_amount',
			'b.name_eksternal',
			'c.nama',
			'e.group');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->work_order_model->lists($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$strStatus = '';
		$no = $start;
		foreach ($list['data'] as $k => $v) {
			if($v['id_wo'] != NULL) {
				$no++;
				$strButton =
					'<div class="btn-group">'.
						'<button class="btn btn-success btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Download" onclick="report_workOrder(\''.$v['id_wo'].'\')">'.
							'<i class="fa fa-file-pdf-o"></i>'.
						'</button>'.
					'</div>'.
					'<div class="btn-group">'.
						'<button class="btn btn-info btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Details" onclick="detail_workOrder(\''.$v['id_wo'].'\')">'.
							'<i class="fa fa-search"></i>'.
						'</button>'.
					'</div>';

				$strButtonEdit =
					'<div class="btn-group">'.
						'<button class="btn btn-warning btn-sm"title="Edit" onClick="edit_workOrder(\''.$v['id_wo'].'\')">'.
							'<i class="fa fa-edit"></i>'.
						'</button>'.
					'</div>';

				$strButtonDel =
					'<div class="btn-group">'.
						'<button class="btn btn-danger btn-sm"title="Delete" onClick="delete_workOrder(\''.$v['id_wo'].'\')">'.
							'<i class="fa fa-trash"></i>'.
						'</button>'.
					'</div>';

				$strButtonApproval =
					'<div class="btn-group">'.
						'<button class="btn btn-success btn-sm"title="Approval" onClick="approve_workOrder(\''.$v['id_wo'].'\')">'.
							'<i class="fa fa-check"></i>'.
						'</button>'.
					'</div>';

				if($v['status'] == 0) {
					$strStatus = 'Preparing';
					$strButton .= $strButtonEdit.$strButtonDel.$strButtonApproval;
				}else if($v['status'] == 1) $strStatus = 'Done';
				else {
					$strStatus = 'Rejected';
					$strButton .= $strButtonEdit.$strButtonDel;
				}

				array_push($data, array(
					$no,
					$v['no_work_order'],
					$v['name_eksternal'],
					date('d-M-Y', strtotime($v['delivery_date'])),
					$v['term_of_payment'],
					$v['symbol_valas'].' '.number_format($v['total_amount'], 2, ',', '.'),
					$strStatus,
					$strButton
				));
			}
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function get_permintaan() {
		$result = $this->work_order_model->get_permintaan();

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result);
	}

	public function get_permintaanBy($id) {
		$result = $this->work_order_model->get_permintaanBy($id);

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result);
	}

	public function add() {
		$customer 		= $this->work_order_model->customer();
		$valas 			= $this->work_order_model->valas();
		$permintaan 	= $this->work_order_model->get_permintaan();

		$data = array(
			'customer'		=> $customer,
			'valas'			=> $valas,
			'permintaan'	=> $permintaan
		);

		$this->load->view('add_modal_view', $data);
	}

	public function add_workOrder() {
		$this->form_validation->set_rules('delivery_date', 'Delivery Date', 'trim|required');
		$this->form_validation->set_rules('cust_id', 'Customer Name', 'trim|required');
		$this->form_validation->set_rules('term_of_pay', 'Term of Payment', 'trim|required');
		$this->form_validation->set_rules('valas', 'Valas', 'trim|required');
		$this->form_validation->set_rules('rate', 'Rate', 'trim|required');
		$this->form_validation->set_rules('diskon', 'Diskon', 'trim|required');
		$this->form_validation->set_rules('tujuan', 'Tujuan', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$no 					= $this->sequence->get_no('work_order');
			$tgl_delivery_date		= $this->Anti_sql_injection($this->input->post('delivery_date', TRUE));
			$temp_delivery_date 	= explode("-", $tgl_delivery_date);
			$cust_id				= $this->Anti_sql_injection($this->input->post('cust_id', TRUE));
			$term_of_pay			= $this->Anti_sql_injection($this->input->post('term_of_pay', TRUE));
			$valas					= $this->Anti_sql_injection($this->input->post('valas', TRUE));
			$rate					= $this->Anti_sql_injection($this->input->post('rate', TRUE));
			$diskon					= $this->Anti_sql_injection($this->input->post('diskon', TRUE));
			$tujuan					= $this->Anti_sql_injection($this->input->post('tujuan', TRUE));
			$total_amount			= $this->Anti_sql_injection($this->input->post('total_amount', TRUE));

			$tempNoDpp				= $this->Anti_sql_injection($this->input->post('arrNoDpp', TRUE));
			$arrNoDpp				= explode(',', $tempNoDpp);

			$tempPrice				= $this->Anti_sql_injection($this->input->post('arrPrice', TRUE));
			$arrPrice				= explode(',', $tempPrice);

			$tempRemark				= $this->Anti_sql_injection($this->input->post('arrRemark', TRUE));
			$arrRemark				= explode(',', $tempRemark);

			$dataWorkOrder = array(
				'no'			=> $no,
				'delivery_date'	=> date('Y-m-d', strtotime($temp_delivery_date[2].'-'.$temp_delivery_date[1].'-'.$temp_delivery_date[0])),
				'cust_id'		=> $cust_id,
				'term_of_pay'	=> $term_of_pay,
				'valas'			=> $valas,
				'rate'			=> $rate,
				'diskon'		=> $diskon,
				'tujuan'		=> ucwords($tujuan),
				'total_amount'	=> $total_amount
			);

			$resultDetail = false;
			$resultAddWorkOrder = $this->work_order_model->add_workOrder($dataWorkOrder);
			if($resultAddWorkOrder['result'] > 0) {
				for ($i = 0; $i < sizeof($arrNoDpp); $i++) {
					if($arrRemark[$i] != '' && $arrRemark[$i] != 'NULL') $remark = $arrRemark[$i];
					else $remark = NULL;
					$dataDetail = array(
						'id_wo'			=> $resultAddWorkOrder['lastid'],
						'id_dpp'		=> $arrNoDpp[$i],
						'unit_price'	=> $arrPrice[$i],
						'remark'		=> $remark
					);
					$resultAddDetailWorkOrder = $this->work_order_model->add_detail_workOrder($dataDetail);
					if($resultAddDetailWorkOrder > 0) $resultDetail = true;
					else {
						$resultDetail = false;
						$msg = 'Gagal menambahkan detail Work Order yang ke-'.($i+1);
						$this->log_activity->insert_activity('insert', $msg);
						$result = array('success' => false, 'message' => $msg);
						break;
					}
				}

				if($resultDetail == true) {
					$msg = 'Berhasil menambahkan Work Order ke database';
					$this->log_activity->insert_activity('insert', $msg.' dengan ID : '.$resultAddWorkOrder['lastid']);
					$result = array('success' => true, 'message' => $msg);
				}
			}else {
				$msg = 'Gagal menambahkan Work Order ke database';

				$this->log_activity->insert_activity('insert', $msg);
				$result = array('success' => false, 'message' => $msg);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function edit($id_workOrder) {
		$workOrder 		= $this->work_order_model->get_workOrder($id_workOrder);
		$detail 		= $this->work_order_model->get_detail_workOrder($id_workOrder);
		$customer 		= $this->work_order_model->customer();
		$valas 			= $this->work_order_model->valas();
		$permintaan 	= $this->work_order_model->get_permintaan();

		$data = array(
			'workOrder'		=> $workOrder,
			'detail'		=> $detail,
			'customer'		=> $customer,
			'valas'			=> $valas,
			'permintaan'	=> $permintaan
		);

		$this->load->view('edit_modal_view', $data);
	}

	public function edit_workOrder() {
		$this->form_validation->set_rules('delivery_date', 'Delivery Date', 'trim|required');
		$this->form_validation->set_rules('cust_id', 'Customer Name', 'trim|required');
		$this->form_validation->set_rules('term_of_pay', 'Term of Payment', 'trim|required');
		$this->form_validation->set_rules('valas', 'Valas', 'trim|required');
		$this->form_validation->set_rules('rate', 'Rate', 'trim|required');
		$this->form_validation->set_rules('diskon', 'Diskon', 'trim|required');
		$this->form_validation->set_rules('tujuan', 'Tujuan', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$id_wo 					= $this->Anti_sql_injection($this->input->post('id_wo', TRUE));
			$tgl_delivery_date		= $this->Anti_sql_injection($this->input->post('delivery_date', TRUE));
			$temp_delivery_date 	= explode("-", $tgl_delivery_date);
			$cust_id				= $this->Anti_sql_injection($this->input->post('cust_id', TRUE));
			$term_of_pay			= $this->Anti_sql_injection($this->input->post('term_of_pay', TRUE));
			$valas					= $this->Anti_sql_injection($this->input->post('valas', TRUE));
			$rate					= $this->Anti_sql_injection($this->input->post('rate', TRUE));
			$diskon					= $this->Anti_sql_injection($this->input->post('diskon', TRUE));
			$tujuan					= $this->Anti_sql_injection($this->input->post('tujuan', TRUE));
			$total_amount			= $this->Anti_sql_injection($this->input->post('total_amount', TRUE));

			$tempIdItem				= $this->Anti_sql_injection($this->input->post('arrIdItem', TRUE));
			$arrIdItem				= explode(',', $tempIdItem);

			$tempNoDpp				= $this->Anti_sql_injection($this->input->post('arrNoDpp', TRUE));
			$arrNoDpp				= explode(',', $tempNoDpp);

			$tempPrice				= $this->Anti_sql_injection($this->input->post('arrPrice', TRUE));
			$arrPrice				= explode(',', $tempPrice);

			$tempRemark				= $this->Anti_sql_injection($this->input->post('arrRemark', TRUE));
			$arrRemark				= explode(',', $tempRemark);

			$workOrder 			= $this->work_order_model->get_workOrder($id_wo)[0];
			$dataEditWorkOrder	= array(
				'id_wo'			=> $id_wo,
				'delivery_date'	=> date('Y-m-d', strtotime($temp_delivery_date[2].'-'.$temp_delivery_date[1].'-'.$temp_delivery_date[0])),
				'cust_id'		=> $cust_id,
				'term_of_pay'	=> $term_of_pay,
				'valas'			=> $valas,
				'rate'			=> $rate,
				'diskon'		=> $diskon,
				'tujuan'		=> ucwords($tujuan),
				'total_amount'	=> $total_amount
			);

			if(
			$dataEditWorkOrder['delivery_date'] != $workOrder['delivery_date'] ||
			$dataEditWorkOrder['cust_id'] != $workOrder['id_cust'] ||
			$dataEditWorkOrder['term_of_pay'] != $workOrder['term_of_payment'] ||
			$dataEditWorkOrder['valas'] != $workOrder['id_valas'] ||
			$dataEditWorkOrder['rate'] != $workOrder['rate'] ||
			$dataEditWorkOrder['diskon'] != $workOrder['diskon'] ||
			$dataEditWorkOrder['tujuan'] != $workOrder['tujuan'] ||
			$dataEditWorkOrder['total_amount'] != $workOrder['total_amount']) {
				$resultEditWorkOrder = $this->work_order_model->edit_workOrder($dataEditWorkOrder);
			}
			else $resultEditWorkOrder = 1;
			$resultDetail = false;
			for ($i = 0; $i < sizeof($arrIdItem); $i++) {
				if($arrRemark[$i] != '' && $arrRemark[$i] != 'NULL') $remark = $arrRemark[$i];
				else $remark = NULL;

				if($arrIdItem[$i] != 'new') {
					$detail = $this->work_order_model->get_detail_workOrder_id_dpp($arrIdItem[$i])[0];
					$dataDetail = array(
						'id_wo_dpp'			=> $arrIdItem[$i],
						'id_dpp'			=> $arrNoDpp[$i],
						'unit_price'		=> $arrPrice[$i],
						'remark'			=> $remark
					);
					
					if(
					$dataDetail['id_dpp'] != $detail['id_dpp'] ||
					$dataDetail['unit_price'] != $detail['unit_price'] ||
					$dataDetail['remark'] != $detail['remark']
					) {
						$resultEditDetailWorkOrder = $this->work_order_model->edit_detail_workOrder($dataDetail);
					}else $resultEditDetailWorkOrder = 1;
					if($resultEditDetailWorkOrder > 0) $resultDetail = true;
					else {
						$msg = 'Gagal update detail Work Order yang ke-'.($i+1).' database';

						$this->log_activity->insert_activity('update', $msg.' dengan ID : ' .$id_wo_dpp);
						$result = array('success' => false, 'message' => $msg);
						$resultDetail = false;
						break;
					}
				}else {
					$dataDetail = array(
						'id_wo'			=> $id_wo,
						'id_dpp'		=> $arrNoDpp[$i],
						'unit_price'	=> $arrPrice[$i],
						'remark'		=> $remark
					);

					$resultAddDetailWorkOrder = $this->work_order_model->add_detail_workOrder($dataDetail);
					if($resultAddDetailWorkOrder > 0) $resultDetail = true;
					else {
						$msg = 'Gagal update detail Work Order yang ke-'.($i+1).' database';

						$this->log_activity->insert_activity('insert', 'Gagal Insert baru detail Work Order');
						$result = array('success' => false, 'message' => $msg);
						$resultDetail = false;
						break;
					}
				}
			}

			if($resultEditWorkOrder > 0 && $resultDetail == true) {
				$msg = 'Berhasil mengubah Work Order ke database';

				$this->log_activity->insert_activity('update', $msg.' dengan ID : ' .$id_wo);
				$result = array('success' => true, 'message' => $msg);
			}else {
				if($resultDetail != false) {
					$msg = 'Gagal mengubah Order ke database';

					$this->log_activity->insert_activity('update', $msg.' dengan ID : ' .$id_wo);
					$result = array('success' => false, 'message' => $msg);
				}
			}	
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function detail_workOrder($id_workOrder) {
		$workOrder 		= $this->work_order_model->get_workOrder($id_workOrder);
		$detail 		= $this->work_order_model->get_detail_workOrder($id_workOrder);
		$customer 		= $this->work_order_model->customer();
		$valas 			= $this->work_order_model->valas();
		$permintaan 	= $this->work_order_model->get_permintaan();

		$data = array(
			'workOrder'		=> $workOrder,
			'detail'		=> $detail,
			'customer'		=> $customer,
			'valas'			=> $valas,
			'permintaan'	=> $permintaan
		);

		$this->load->view('detail_modal_view', $data);
	}

	public function approve_workOrder($id_workOrder) {
		$workOrder 		= $this->work_order_model->get_workOrder($id_workOrder);
		$detail 		= $this->work_order_model->get_detail_workOrder($id_workOrder);
		$customer 		= $this->work_order_model->customer();
		$valas 			= $this->work_order_model->valas();
		$permintaan 	= $this->work_order_model->get_permintaan();

		$data = array(
			'workOrder'		=> $workOrder,
			'detail'		=> $detail,
			'customer'		=> $customer,
			'valas'			=> $valas,
			'permintaan'	=> $permintaan
		);

		$this->load->view('approval_modal_view', $data);
	}

	public function status_workOrder() {
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		//$this->form_validation->set_rules('notes', 'Notes', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$id_wo 		= $this->Anti_sql_injection($this->input->post('id_wo', TRUE));
			$status 	= $this->Anti_sql_injection($this->input->post('status', TRUE));
			$notes 		= $this->Anti_sql_injection($this->input->post('notes', TRUE));

			$data = array(
				'id_wo'		=> $id_wo,
				'status'	=> $status,
				'notes'		=> $notes
			);

			$resultEditStatus = $this->work_order_model->status_workOrder($data);
			if($resultEditStatus > 0) {
				$msg = 'Berhasil mengubah status Work Order ke database';

				$this->log_activity->insert_activity('update', $msg.' dengan ID : '.$id_wo);
				$result = array('success' => true, 'message' => $msg);
			}else {
				$msg = 'Gagal mengubah status Work Order ke database';

				$this->log_activity->insert_activity('update', $msg.' dengan ID : '.$id_wo);
				$result = array('success' => false, 'message' => $msg);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function report_workOrder($id_workOrder) {
		$result_wo 		= $this->work_order_model->download_workOrder($id_workOrder);

		$data = array(
			'workOrder'		=> $result_wo
		);

		$this->load->view('report_modal_view', $data);
	}

	public function download_workOrder() {
		$data   = file_get_contents("php://input");
		$params = json_decode($data, true);

		$result_wo = $this->work_order_model->download_workOrder($params['id_wo']);

		/*echo "<pre>";
		print_r($params);
		print_r($result_btb);*/

		if(sizeof($result_wo) > 0) $result = array('success' => true, 'data' => $result_wo);
		else $result = array('success' => false);

		/*print_r($result);
		echo "</pre>";
		die;*/

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function delete_workOrder() {
		$data 		= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$resultDelete = $this->work_order_model->delete_workOrder($params);
		if($resultDelete > 0) {
			$msg = 'Berhasil menghapus Data Work Order';
			$this->log_activity->insert_activity('delete', $msg.' dengan ID : ' .$params['id']);
			$result = array('success' => true, 'message' => $msg);
		}else {
			$msg = 'Gagal menghapus Data Work Order';
			$this->log_activity->insert_activity('delete', $msg.' dengan ID : ' .$params['id']);
			$result = array('success' => false, 'message' => $msg);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function delete_detail_workOrder() {
		$data 		= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$resultDelete = $this->work_order_model->delete_detail_workOrder($params);
		if($resultDelete > 0) {
			$msg = 'Berhasil menghapus Detail Work Order';
			$this->log_activity->insert_activity('delete', $msg.' dengan ID : ' .$params['id']);
			$result = array('success' => true, 'message' => $msg);
		}else {
			$msg = 'Gagal menghapus Detail Work Order';
			$this->log_activity->insert_activity('delete', $msg.' dengan ID : ' .$params['id']);
			$result = array('success' => false, 'message' => $msg);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
}
