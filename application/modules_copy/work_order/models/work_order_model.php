<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Work_Order_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function lists($params = array()) {
		$sql 	= 'CALL work_order_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query(
			$sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			)
		);

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total']
		);

		return $return;
	}

	public function customer() {
		$query =   $this->db->query("SELECT id,name_eksternal,kode_eksternal FROM t_eksternal
			WHERE type_eksternal = 2 OR type_eksternal = 3");

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function valas() {
		$sql 	= "SELECT * FROM m_valas";

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_permintaan() {
		$sql 	= 'CALL work_order_list_dpp_all';

		$query 	= $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_permintaanBy($id) {
		$sql 	= 'CALL work_order_list_dpp_by_id(?)';

		$query 	= $this->db->query($sql, array(
			$id
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_workOrder($id) {
		$sql 	= 'CALL work_order_search_id(?)';

		$query 	= $this->db->query($sql, array(
			$id
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function add_workOrder($data) {
		$sql 	= 'CALL work_order_add(?,?,?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['no'],
			$data['delivery_date'],
			$data['tujuan'],
			$data['cust_id'],
			$data['term_of_pay'],
			$data['diskon'],
			$data['total_amount'],
			$data['valas'],
			$data['rate']
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];
		
		$result['result'] = $this->db->affected_rows();
		$result['lastid'] = $lastid;

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function add_detail_workOrder($data) {
		$sql 	= 'CALL work_order_dpp_add(?,?,?,?)';
		
		$query 	=  $this->db->query($sql, array(
			$data['id_wo'],
			$data['id_dpp'],
			$data['unit_price'],
			$data["remark"]
		));
		
		$result = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_workOrder($data) {
		$sql 	= 'CALL work_order_update(?,?,?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['id_wo'],
			$data['delivery_date'],
			$data['tujuan'],
			$data['cust_id'],
			$data['term_of_pay'],
			$data['diskon'],
			$data['total_amount'],
			$data['valas'],
			$data['rate']
		));
		
		$result = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_detail_workOrder($data) {
		$sql 	= 'CALL work_order_dpp_update(?,?,?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['id_wo_dpp'],
			$data['id_dpp'],
			$data['unit_price'],
			$data["remark"]
		));
		
		$result = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function detail_pekerjaan($id) {
		$sql 	= 'CALL permintaan_pekerjaan_detail_search_id_permintaan(?)';

		$query 	= $this->db->query($sql, array(
			$id
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_detail_workOrder($id) {
		$sql 	= 'CALL work_order_search_dpp_id(?)';

		$query 	= $this->db->query($sql, array(
			$id
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_detail_workOrder_id_dpp($id) {
		$sql 	= 'CALL work_order_search_dpp_id_dpp(?)';

		$query 	= $this->db->query($sql, array(
			$id
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function status_workOrder($data) {
		$sql 	=
			'UPDATE `t_work_order` SET
				`status` = '.$data['status'].',
				`notes` = \''.$data['notes'].'\'
			WHERE `id_wo` = '.$data['id_wo'];

		$query 	= $this->db->query($sql);

		$result = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function download_workOrder($id_wo) {
		$sql 	= 'CALL work_order_view_report_by_id(?)';

		$query 	= $this->db->query($sql,array(
			$id_wo
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	function delete_workOrder($data) {
		$sql 	= 'CALL work_order_delete(?)';

		$query 	= $this->db->query($sql, array(
			$data['id']
		));

		$result = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function delete_detail_workOrder($data) {
		$sql 	= 'CALL work_order_dpp_delete(?)';

		$query 	= $this->db->query($sql, array(
			$data['id']
		));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
}
