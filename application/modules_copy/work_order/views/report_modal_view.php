<style>
	.dt-body-left {text-align: left; vertical-align: middle;}
	.dt-body-center {text-align: center; vertical-align: middle;}
	.dt-body-right {text-align: right; vertical-align: middle;}

	.dt-noBorder thead tr td { border: 0px; }
	.dt-noBorder thead tr th { border: 0px; }
	.dt-noBorder tbody tr td { border: 0px; }
	.dt-noBorder tbody tr th { border: 0px; }

	img {
		width	: 60%;
		height	: auto;
	}
	/*.margin-row{
		margin-top: 45px;
		margin-bottom: 45px;
	}
	.titleReport{
		text-align: center;
	}
	.left-header{
		width: 30%;
		float: left;
		position: relative;
		text-align: center;
		padding-left: 35px;
		padding-right: 35px;
	}
	.right-header{
		position: relative;
		text-align: center;
		padding-left: 35px;
		padding-right: 35px;
	}*/

	.border-packing{
		border: 1px solid #ebeff2;
	}

	/*.border-bottom{
		border-bottom: 1px solid #ebeff2;
	}*/
</style>

<div class="row">
	<div class="col-md-12" style="position: absolute; padding-top: 10px; padding-right: 70px; z-index: 1;">
		<a class="btn btn-primary pull-right" id="btn-download">
			<i class="fa fa-download"></i>
		</a>
	</div>
</div>

<div class="row" style="border: 1px solid #ebeff2;">
	<div class="col-md-2 text-center" style="margin-top: 5px;">
		<img src="<?php echo site_url(); ?>assets/images/logo-celebit.jpg" alt="logo-celebit">
	</div>
	<div class="col-md-10" style="margin-top: 10px;">
		<h1 id="titleCelebit">CELEBIT</h1>
		<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
		<h4 id="titleAlamat">BANDUNG FACTORY : JL.BUAH DUA RT.01/RW.04 RANCAEKEK - BANDUNG-INDONESIA</h4>
		<h4 id="titleTlp">TEL 62-22-7798 561/7798 542, FAX : 62-22-7798 562 E-MAIL : celebit@celebit.id</h4>
	</div>

	<div class="col-md-12 text-center" style="border-top: 1px solid #ebeff2; margin-bottom: 15px;">
		<h2 id="titleWorkOrder" style="font-weight:900;">WORK ORDER</h2>
	</div>

	<table id="header_wo" class="table dt-noBorder dt-responsive nowrap" cellspacing="0" width="100%">
		<tr>
			<th class="dt-body-left" style="width: 5%;">M/S</th>
			<td class="dt-body-center" style="vertical-align: middle; width: 1%;">:</td>
			<td colspan="4"><label style="font-weight: normal;" id="name_eksternal"><?php if(isset($workOrder[0])) echo $workOrder[0]['name_eksternal']; ?></label></td>
		</tr>
		<tr>
			<th class="dt-body-left" style="idth: 5%;">Address</th>
			<td class="dt-body-center" style="width: 1%;">:</td>
			<td colspan="4"><label style="font-weight: normal;" id="eksternal_address"><?php if(isset($workOrder[0])) echo $workOrder[0]['eksternal_address']; ?></label></td>
		</tr>
		<tr>
			<th class="dt-body-left" style="width: 5%;">Attn</th>
			<td class="dt-body-center" style="width: 1%;">:</td>
			<td colspan="4"></td>
		</tr>
		<tr>
			<th class="dt-body-left" style="width: 5%;">Telp</th>
			<td class="dt-body-center" style="width: 1%;">:</td>
			<td><label style="font-weight: normal;" id="phone"><?php if(isset($workOrder[0])) echo $workOrder[0]['phone_1'].' / '.$workOrder[0]['phone_2']; ?></label></td>
			<td class="dt-body-left" style="width: 5%;">No. WO</td>
			<td class="dt-body-center" style="width: 1%;">:</td>
			<td style="width: 20%;"><label id="no_wo" style="font-weight: normal;"><?php if(isset($workOrder[0])) echo $workOrder[0]['no_work_order']; ?></label></td>
		</tr>
		<tr>
			<th class="dt-body-left" width: 5%;">Fax</th>
			<td class="dt-body-center" style="width: 1%;">:</td>
			<td><label style="font-weight: normal;" id="fax"><?php if(isset($workOrder[0])) echo $workOrder[0]['fax']; ?></label></td>
			<td class="dt-body-left" style="width: 5%;">Date</td>
			<td class="dt-body-center" style="width: 1%;">:</td>
			<td style="width: 20%;"><input style="width: 85%; margin-top: -10px;" type="text" class="form-control" id="tgl_wo" name="tgl_wo" placeholder="<?php echo date('d-M-Y'); ?>" value="<?php echo date('d-M-Y'); ?>" autocomplete="off"></td>
		</tr>
	</table>

	<table id="list_wo" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class="dt-body-center" style="width: 5%;">NO</th>
				<th class="dt-body-center" style="width: 8%;">NO WR</th>
				<th class="dt-body-center">DESCRIPTION</th>
				<th class="dt-body-center" style="width: 5%;">QTY</th>
				<th class="dt-body-center" style="width: 5%;">UOM</th>
				<th class="dt-body-center" style="width: 10%;">UNIT PRICE</th>
				<th class="dt-body-center" style="width: 10%;">AMOUNT</th>
				<th class="dt-body-center">Remark</th>
			</tr>
		</thead>
		<tbody>
<?php
	if(isset($workOrder) && sizeof($workOrder) > 0) {
		$no 	= 1;
		$total 	= 0;
		foreach ($workOrder as $wkk => $wkv) {
			$totalRow = floatval($wkv['qty']) * floatval($wkv['unit_price']); ?>
			<tr>
				<td class="dt-body-center"><?php echo $no; ?></td>
				<td class="dt-body-center"><?php echo $wkv['no_pekerjaan']; ?></td>
				<td class="dt-body-left"><?php echo $wkv['nama_pekerjaan']; ?></td>
				<td class="dt-body-center"><?php echo $wkv['qty']; ?></td>
				<td class="dt-body-center"><?php echo $wkv['uom_symbol']; ?></td>
				<td class="dt-body-right"><?php echo $wkv['symbol_valas'].' '.number_format($wkv['unit_price'], 4, ',', '.'); ?></td>
				<td class="dt-body-right"><?php echo $wkv['symbol_valas'].' '.number_format($totalRow, 4, ',', '.'); ?></td>
				<td class="dt-body-left"><?php echo $wkv['remark']; ?></td>
			</tr>
<?php
			$total = $total + floatval($totalRow);
			$no++;
			$strTotal = $wkv['symbol_valas'].' '.number_format($total, 4, ',', '.');
		}
	}
?>
		</tbody>
		<tfoot>
			<tr>
				<th class="dt-body-center" colspan="6">TOTAL</th>
				<th class="dt-body-right" id="footTotal"><?php if(isset($strTotal)) echo $strTotal; ?></th>
				<td class="dt-body-center"></td>
			</tr>
		</tfoot>
	</table>

	<table id="footer_wo" class="table dt-noBorder dt-responsive nowrap" cellspacing="0" width="100%">
		<tr>
			<td style="width: 10%;">Delivery Date</td>
			<td style="width: 1%;">:</td>
			<td><label style="font-weight: normal;" id="delivery_date"><?php if(isset($workOrder[0])) echo date('d-M-Y', strtotime($workOrder[0]['delivery_date'])); ?></label></td>
		</tr>
		<tr>
			<td style="width: 10%;">Destination</td>
			<td style="width: 1%;">:</td>
			<td><label style="font-weight: normal;" id="tujuan"><?php if(isset($workOrder[0])) echo $workOrder[0]['tujuan']; ?></label></td>
		</tr>
		<tr>
			<td style="width: 10%;">Term of Payment</td>
			<td style="width: 1%;">:</td>
			<td><label style="font-weight: normal;" id="term_of_payment"><?php if(isset($workOrder[0])) echo $workOrder[0]['term_of_payment']; ?></label></td>
		</tr>
	</table>
	
	<table id="approval_wo" class="table dt-noBorder dt-responsive nowrap" cellspacing="0" width="100%" style="margin-top: 25px; margin-bottom: 25px;">
		<thead>
			<tr>
				<td class="dt-body-center" style="width: 10%; border: 1px solid #ebeff2;">Received by.</td>
				<td></td>
				<td class="dt-body-center" style="width: 10%; border: 1px solid #ebeff2;">Prepared by.</td>
				<td class="dt-body-center" style="width: 10%; border: 1px solid #ebeff2;">Approved by.</td>
				<td class="dt-body-center" style="width: 10%; border: 1px solid #ebeff2;">Approved by.</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td style="height: 100px; border: 1px solid #ebeff2;"></td>
				<td style="height: 100px;"></td>
				<td style="height: 100px; border: 1px solid #ebeff2;"></td>
				<td style="height: 100px; border: 1px solid #ebeff2;"></td>
				<td style="height: 100px; border: 1px solid #ebeff2;"></td>
			</tr>
		</tbody>
	</table>
</div>

<script type="text/javascript">
	var dataImage = null;
	
	$(document).ready(function() {
		$("#tgl_wo").datepicker({
			format: 'dd-M-yyyy',
			autoclose: true,
			todayHighlight: true,
		});

		$('#btn-download').on('click', function() {
			var doc = new jsPDF('p', 'mm', 'letter');
			var imgData = dataImage;
			var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
			var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

			doc.setTextColor(100);
			doc.setFontStyle('helvetica','arial','sans-serif','bold');
			doc.addImage(imgData, 'JPEG', 20, 5, 22, 22);
			doc.setFontSize(10);
			doc.text($('#titleCelebit').html(), 50, 10, 'left');
			doc.setFontSize(8);
			doc.text($('#titlePerusahaan').html(), 50, 15, 'left');
			doc.setFontSize(8);
			doc.text($('#titleAlamat').html(), 50, 20, 'left');
			doc.setFontSize(8);
			doc.text($('#titleTlp').html(), 50, 25, 'left');
			doc.line(pageWidth - 5, 28, 5, 28);

			doc.setFontSize(12);
			doc.text($('#titleWorkOrder').html(), pageWidth / 2, 33, 'center');

			doc.setFontSize(10);
			doc.text('M/S', 5, 43, 'left');
			doc.text(': '+$('#name_eksternal').html(), 25, 43, 'left');
			doc.text('Address', 5, 48, 'left');
			doc.text(': '+$('#eksternal_address').html(), 25, 48, 'left');
			doc.text('Attn', 5, 53, 'left');
			doc.text(': ', 25, 53, 'left');
			doc.text('Telp', 5, 58, 'left');
			doc.text(': '+$('#phone').html(), 25, 58, 'left');
			doc.text('Fax', 5, 63, 'left');
			doc.text(': '+$('#fax').html(), 25, 63, 'left');

			doc.text('No. WO', pageWidth - 50, 58, 'left');
			doc.text(': '+$('#no_wo').html(), pageWidth - 35, 58, 'left');
			doc.text('Date', pageWidth - 50, 63, 'left');
			doc.text(': '+$('#tgl_wo').val(), pageWidth - 35, 63, 'left');

			doc.autoTable({
				html 			: '#list_wo',
				theme			: 'plain',
				styles			: {
					fontSize 	: 8, 
					lineColor	: [116, 119, 122],
					lineWidth	: 0.1,
					cellWidth 	: 'auto',
					
				},
				margin 			: 5,
				tableWidth		: (pageWidth - 10),
				headStyles		: {valign : 'middle', halign : 'center'},
				columns 		: [0,1,2,3,4,5,6,7],
				didParseCell	: function (data) {
					if(data.table.foot[0]) {
						if(data.table.foot[0].cells[0]) data.table.foot[0].cells[0].styles.halign = 'right';
						if(data.table.foot[0].cells[6]) data.table.foot[0].cells[6].styles.halign = 'right';
					}
				},
				columnStyles	: {
					0: {halign: 'center'},
					1: {halign: 'center'},
					2: {halign: 'left'},
					3: {halign: 'center'},
					4: {halign: 'center'},
					5: {halign: 'right'},
					6: {halign: 'right'},
					7: {halign: 'left'}
				},
				rowPageBreak	: 'auto',
				showHead 		: 'firstPage',
				showFoot		: 'lastPage',
				startY			: 65
			});

			var lastTable = doc.autoTable.previous.finalY;

			doc.setFontSize(8);
			doc.text('Delivery Date', 5, lastTable + 5);
			doc.text(': '+$('#delivery_date').html(), 30, lastTable + 5);
			doc.text('Destination', 5, lastTable + 10);
			doc.text(': '+$('#tujuan').html(), 30, lastTable + 10);
			doc.text('Term of Payment', 5, lastTable + 15);
			doc.text(': '+$('#term_of_payment').html(), 30, lastTable + 15);

			doc.setFontSize(12);
			doc.autoTable({
				html 			: '#approval_wo',
				theme			: 'plain',
				styles			: {
					fontSize 	: 8, 
					lineColor	: [116, 119, 122],
					lineWidth	: 0.1,
					cellWidth 	: 'auto',
					
				},
				margin 			: 5,
				tableWidth		: (pageWidth - 10),
				headStyles		: {valign : 'middle', halign : 'center'},
				columns 		: [0,1,2,3,4],
				didParseCell	: function (data) {
					if(data.table.head[0]) {
						if(data.table.head[0].cells[1]) data.table.head[0].cells[1].styles.lineWidth = 0;
					}

					if(data.table.body[0]) {
						data.table.body[0].height = 20;
						if(data.table.body[0].cells[1]) data.table.body[0].cells[1].styles.lineWidth = 0;
					}
				},
				columnStyles	: {
					0: {halign: 'center', cellWidth: 20},
					1: {halign: 'center', cellWidth: 75},
					2: {halign: 'center', cellWidth: 20},
					3: {halign: 'center', cellWidth: 20},
					4: {halign: 'center', cellWidth: 20}
				},
				rowPageBreak	: 'auto',
				showHead 		: 'firstPage',
				showFoot		: 'lastPage',
				startY			: lastTable + 20
			});

			doc.save('Report '+$('#no_wo').html()+'.pdf');
		})
	});
	
	function toDataURL(url, callback) {
		var xhr = new XMLHttpRequest();
		xhr.onload = function() {
			var reader = new FileReader();
			reader.onloadend = function() {
				callback(reader.result);
			}
			reader.readAsDataURL(xhr.response);
		};
		xhr.open('GET', url);
		xhr.responseType = 'blob';
		xhr.send();
	}

	toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
		dataImage = dataUrl;
	})
</script>