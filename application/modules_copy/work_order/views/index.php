<style>
	.changed_status{cursor:pointer;text-decoration: underline;color:#96b6e8;}
	.changed_status:hover{color:#ff8c00}
	.dt-body-left {text-align:left;}
	.dt-body-right {text-align:right;}
	.dt-body-center {text-align:center; vertical-align: middle;}
	.force-overflow {height: 450px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow {min-height: 350px;}

	.force-overflow-report {height: 650px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow-report {min-height: 550px;}

	#modal-child::-webkit-scrollbar-track {
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	#modal-modalchild::-webkit-scrollbar {
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-modalchild::-webkit-scrollbar-thumb {
		background-image: -webkit-gradient(linear,
		left bottom,
		left top,
		color-stop(0.44, rgb(122,153,217)),
		color-stop(0.72, rgb(73,125,189)),
		color-stop(0.86, rgb(28,58,148)));
	}

	.x-hidden {
		/*display: none;*/
	}
</style>

<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Work Order</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<table id="listWorkOrder" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="width: 5%;">No</th>
							<th style="width: 11%;">No Work Order</th>
							<th>Customer Name</th>
							<th>Delivery Date</th>
							<th>Term of Payment</th>
							<th>Total Amount</th>
							<th>Status</th>
							<th style="width: 15%;"></th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div><!-- end col -->
	</div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:90%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-modal">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalReport" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:90%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow-report" id="modal-modal">
					<div class="scroll-overflow-report">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:70%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-modalchild">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild-barang" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:92%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$(document).ready(function(){
		listWorkOrder();
	});

	function add_workOrder(){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('work_order/add');?>');
		$('#panel-modal  .panel-title').html('<i class="fa fa-plus"></i> Add Work Order');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function edit_workOrder(id_workOrder){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('work_order/edit/');?>'+"/"+id_workOrder);
		$('#panel-modal  .panel-title').html('<i class="fa fa-pencil"></i> Edit Work Order');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function detail_workOrder(id_workOrder){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('work_order/detail_workOrder');?>'+"/"+id_workOrder);
		$('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Detail Work Order');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function report_workOrder(id_workOrder) {
		$('#panel-modalReport').removeData('bs.modal');
		$('#panel-modalReport  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modalReport  .panel-body').load('<?php echo base_url('work_order/report_workOrder');?>'+"/"+id_workOrder);
		$('#panel-modalReport  .panel-title').html('<i class="fa fa-download"></i> Report Work Order');
		$('#panel-modalReport').modal({backdrop:'static',keyboard:false},'show');
	}
	
	function approve_workOrder(id_workOrder){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('work_order/approve_workOrder');?>'+"/"+id_workOrder);
		$('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Detail Work Order');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function listWorkOrder(){
		$("#listWorkOrder").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'work_order/lists/';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false,
			"destroy": true,
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip',
			"initComplete": function(){
				$("div.toolbar").prepend(
					'<div class="btn-group pull-left">'+
						'<a class="btn btn-primary" onClick="add_workOrder()">'+
							'<i class="fa fa-plus"></i> Add Work Order'+
						'</a>'+
					'</div>'
				);
			}
		});
	}

	function delete_workOrder(id_workOrder) {
		swal({
			title: 'Yakin akan Menghapus ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost={
				"id" : id_workOrder
			};

			$.ajax({
				type:'POST',
				url: "<?php echo base_url().'work_order/delete_workOrder';?>",
				data:JSON.stringify(datapost),
				cache:false,
				contentType: false,
				processData: false,
				success: function(r) {
					if(r.success == true) {
						swal({
							title: 'Success!',
							text: r.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function () {
							listWorkOrder();
						})
					}else {
						swal("Failed!", r.message, "error");
					}
				}
			});
		});
	}
</script>