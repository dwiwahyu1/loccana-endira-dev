<style>
	.dt-body-left {text-align: left;}
	.dt-body-center {text-align: center;}
	.dt-body-right {text-align: right;}

	#listItemPekerjaan {
		counter-reset: rowNumber;
	}

	#listItemPekerjaan tbody tr > td:first-child {
		counter-increment: rowNumber;
		text-align: center;
	}

	#listItemPekerjaan tbody tr td:first-child::before {
		content: counter(rowNumber);
		min-width: 1em;
		margin-right: 0.5em;
	}
</style>

<form class="form-horizontal form-label-left" id="edit_workOrder" role="form" action="<?php echo base_url('work_order/edit_workOrder'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="delivery_date">Delivery Date <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="delivery_date" name="delivery_date" value="<?php
				if(isset($workOrder[0]['delivery_date'])) echo date('d-M-Y', strtotime($workOrder[0]['delivery_date']));
			?>" placeholder="Work Date" required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="cust_id">Customer Name <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="cust_id" name="cust_id" style="width: 100%" required>
				<option value="">-- Pilih Customer --</option>
		<?php
			foreach ($customer as $ck => $cv) { ?>
				<option value="<?php echo $cv['id']; ?>" <?php
				if(isset($workOrder[0]['id_cust'])) {
					if($cv['id'] == $workOrder[0]['id_cust']) echo "selected";
				} ?>><?php echo $cv['name_eksternal']; ?></option>
		<?php
			}
		?>
			</select>
	  </div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="term_of_pay">Term of Payment <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="term_of_pay" name="term_of_pay" style="width: 100%" required>
				<option value="">-- Select Term --</option>
				<option value="1 Day" <?php
					if(isset($workOrder[0]['term_of_payment'])) if($workOrder[0]['term_of_payment'] == '1 Day') echo "selected";
				?>>1 Day</option>
				<option value="15 Day" <?php
					if(isset($workOrder[0]['term_of_payment'])) if($workOrder[0]['term_of_payment'] == '15 Day') echo "selected";
				?>>15 Day</option>
				<option value="30 Day" <?php
					if(isset($workOrder[0]['term_of_payment'])) if($workOrder[0]['term_of_payment'] == '30 Day') echo "selected";
				?>> 30 Day</option>
				<option value="60 Day" <?php
					if(isset($workOrder[0]['term_of_payment'])) if($workOrder[0]['term_of_payment'] == '60 Day') echo "selected";
				?>> 60 Day</option>
				<option value="90 Day" <?php
					if(isset($workOrder[0]['term_of_payment'])) if($workOrder[0]['term_of_payment'] == '90 Day') echo "selected";
				?>> 90 Day</option>
				<option value="120 Day" <?php
					if(isset($workOrder[0]['term_of_payment'])) if($workOrder[0]['term_of_payment'] == '120 Day') echo "selected";
				?>> 120 Day</option>
				<option value="150 Day" <?php
					if(isset($workOrder[0]['term_of_payment'])) if($workOrder[0]['term_of_payment'] == '120 Day') echo "selected";
				?>> 150 Day</option>
				<option value="180 Day" <?php
					if(isset($workOrder[0]['term_of_payment'])) if($workOrder[0]['term_of_payment'] == '150 Day') echo "selected";
				?>> 180 Day</option>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="valas">Valas <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="valas" name="valas" style="width: 100%" required>
				<option value="" selected>-- Pilih Valas --</option>
				<?php foreach($valas as $vd) { ?>
					<option value="<?php echo $vd['valas_id']; ?>" <?php
					if(isset($workOrder[0]['id_valas'])) {
						if($vd['valas_id'] == $workOrder[0]['id_valas']) echo "selected";
					}
					?>><?php echo $vd['nama_valas']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="rate">Rate <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="number" class="form-control" id="rate" name="rate" placeholder="Rate" autocomplete="off" step=".0001" value="<?php
				if(isset($workOrder[0]['rate'])) echo $workOrder[0]['rate'];
			?>" required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="diskon">Diskon <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="number" class="form-control" id="diskon" name="diskon" placeholder="Diskon" autocomplete="off" step=".0001" value="<?php
				if(isset($workOrder[0]['diskon'])) echo $workOrder[0]['diskon'];
			?>" required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tujuan">Tujuan <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="tujuan" name="tujuan" placeholder="Tujuan" value="<?php
				if(isset($workOrder[0]['tujuan'])) echo $workOrder[0]['tujuan'];
			?>" autocomplete="off" required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Daftar Pekerjaan : </label>
		<div class="col-md-8 col-sm-6 col-xs-12"></div>
	</div>

	<div class="item form-group">
		<table id="listItemPekerjaan" class="table table-striped table-bordered dt-responsive nowrap" width="100%">
			<thead>
				<tr>
					<th class="dt-body-center" style="width: 5%;">No</th>
					<th style="width: 15%;">No Pekerjaan</th>
					<th>Nama Pekerjaan</th>
					<th style="width: 5%;">QTY</th>
					<th style="width: 5%;">UOM</th>
					<th style="width: 15%;">Harga</th>
					<th style="width: 15%;">Amount</th>
					<th style="width: 20%;">Remark</th>
					<th style="width: 5%;"><a id="btn_item_add" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a></th>
				</tr>
			</thead>
			<tbody>
		<?php
			$i=1;
			if(sizeof($detail) > 0) {
				foreach($detail as $dk) { ?>
					<tr id="trRowItem<?php echo $i; ?>">
						<td></td>
						<td>
							<select class="form-control" id="no_dpp<?php echo $i; ?>" name="no_dpp[]">
								<option value="">-- Pilih Pekerjaan --</option>
						<?php
							if($dk['id_detail_permintaan'] != '' && $dk['id_detail_permintaan'] != NULL) { ?>
								<option value="<?php echo $dk['id_detail_permintaan']; ?>" selected><?php echo $dk['no_pekerjaan']; ?></option>
						<?php
							}

							foreach ($permintaan as $pk => $pv) { ?>
								<option value="<?php echo $pv['id_detail_permintaan']; ?>"><?php echo $pv['no_pekerjaan']; ?></option>
						<?php
							} ?>
							</select>
						</td>
						<td>
							<label id="nama_pekerjaan<?php echo $i; ?>"><?php echo $dk['nama_pekerjaan']; ?></label>
						</td>
						<td>
							<label id="qty_pekerjaan<?php echo $i; ?>"><?php echo number_format($dk['qty'], 0); ?></label>
						</td>
						<td>
							<label id="uom_pekerjaan<?php echo $i; ?>"><?php echo $dk['uom_name']; ?></label>
						</td>
						<td>
							<input type="number" class="form-control" id="dpp_price<?php echo $i; ?>" name="dpp_price[]" placeholder="Harga" value="<?php
								echo $dk['unit_price'];
							?>" autocomplete="off" step=".0001">
						</td>
						<td class="dt-body-right">
						<?php
							$rowAmount = (floatval($dk['unit_price'])) * (floatval($dk['qty']));
						?>
							<label id="rowAmount<?php echo $i; ?>"><?php echo number_format($rowAmount, 4, '.', ',') ?></label>
							<input type="hidden" id="amount<?php echo $i; ?>" name="amount[]" value="<?php echo number_format($rowAmount, 4, '.', ''); ?>">
						</td>
						<td>
							<textarea class="form-control" id="desc_pekerjaan<?php echo $i; ?>" name="desc_pekerjaan[]" placeholder="Deskripsi" rows="3"><?php
								echo $dk['remark'];
							?></textarea>
						</td>
						<td>
							<input type="hidden" id="id_wo_dpp<?php echo $i; ?>" name="id_wo_dpp[]" value="<?php echo $dk['id_wo_dpp']; ?>">
							<a id="btn_item_remove" class="btn btn-danger btn-sm" title="Hapus" onclick="delItem(this, '<?php
								echo $dk['id_wo_dpp']; ?>')">
								<i class="fa fa-trash"></i>
							</a>
						</td>
					</tr>
		<?php
					$i++;
				}
			}else { ?>
				<tr>
					<td></td>
					<td>
						<select class="form-control" id="no_dpp<?php echo $i; ?>" name="no_dpp[]">
							<option value="" selected>-- Pilih Pekerjaan --</option>
					<?php
						foreach ($permintaan as $pk => $pv) { ?>
							<option value="<?php echo $pv['id_detail_permintaan']; ?>"><?php echo $pv['no_pekerjaan']; ?></option>
					<?php
						}
					?>
						</select>
					</td>
					<td>
						<label id="nama_pekerjaan<?php echo $i; ?>"></label>
					</td>
					<td>
						<label id="qty_pekerjaan<?php echo $i; ?>"></label>
					</td>
					<td>
						<label id="uom_pekerjaan<?php echo $i; ?>"></label>
					</td>
					<td>
						<input type="number" class="form-control" id="dpp_price<?php echo $i; ?>" name="dpp_price[]" placeholder="Harga" autocomplete="off" step=".0001" disabled>
					</td>
					<td class="dt-body-right">
						<label id="rowAmount<?php echo $i; ?>">0.0000</label>
						<input type="hidden" id="amount<?php echo $i; ?>" name="amount[]" value="0">
					</td>
					<td>
						<textarea class="form-control" id="desc_pekerjaan<?php echo $i; ?>" name="desc_pekerjaan[]" placeholder="Deskripsi" rows="3" disabled></textarea>
					</td>
					<td>
						<input type="hidden" id="id_wo_dpp<?php echo $i; ?>" name="id_wo_dpp[]" value="new">
					</td>
				</tr>
		<?php
			} ?>
			</tbody>
			<tfoot>
				<tr>
					<td class="dt-body-right" colspan="5" rowspan="3"></td>
					<td class="dt-body-right">Total</td>
					<td class="dt-body-right"><label id="totalAmountRow">0.0000</label></td>
					<td colspan="2" rowspan="3"></td>
				</tr>
				<tr>
					<td class="dt-body-right">Diskon</td>
					<td class="dt-body-right"><label id="totalAmountDiskon">0</label></td>
				</tr>
				<tr>
					<td class="dt-body-right">Total Amount</td>
					<td class="dt-body-right"><label id="totalAmount">0.0000</label></td>
				</tr>
			</tfoot>
		</table>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Simpan Work Order</button>
			<input type="hidden" id="total_amount" name="total_amount" value="0">
			<input type="hidden" id="id_wo" name="id_wo" value="<?php if(isset($workOrder[0]['id_wo'])) echo $workOrder[0]['id_wo']; ?>">
		</div>
	</div>
</form>

<script type="text/javascript">
	var idRowItem 		= '<?php echo $i; ?>';
	var arrIdItem		= [];
	var arrNoDpp 		= [];
	var arrPrice		= [];
	var arrRemark		= [];

	$(document).ready(function() {
		$("#delivery_date").datepicker({
			format: 'dd-M-yyyy',
			autoclose: true,
			todayHighlight: true,
		});

		$('#cust_id').select2();
		$('#term_of_pay').select2();
		$('#valas').select2();
		$('#diskon').on('keyup', function() {
			cal_amount();
		})
		$('#diskon').on('change', function() {
			cal_amount();
		})

		for (var i = 1; i <= idRowItem; i++) {
			$('#no_dpp'+i).select2();
				$('#no_dpp'+i).on('change', function() {
				set_itemValue(this.value, this.id);
				cal_amount();
			})
			$('#dpp_price'+i).on('keyup', function() {
				cal_amount();
			})
			$('#dpp_price'+i).on('change', function() {
				cal_amount();
			})
		}

		cal_amount();
	});

	$('#btn_item_add').on('click', function() {
		idRowItem++;
		$('#listItemPekerjaan tbody').append(
			'<tr id="trRowItem'+idRowItem+'">'+
				'<td></td>'+
				'<td>'+
					'<select class="form-control" id="no_dpp'+idRowItem+'" name="no_dpp[]" disabled>'+
						'<option value="" selected>-- Pilih Pekerjaan --</option>'+
					'</select>'+
				'</td>'+
				'<td>'+
					'<label id="nama_pekerjaan'+idRowItem+'"></label>'+
				'</td>'+
				'<td>'+
					'<label id="qty_pekerjaan'+idRowItem+'"></label>'+
				'</td>'+
				'<td>'+
					'<label id="uom_pekerjaan'+idRowItem+'"></label>'+
				'</td>'+
				'<td>'+
					'<input type="number" class="form-control" id="dpp_price'+idRowItem+'" name="dpp_price[]" placeholder="Harga" autocomplete="off" step=".0001" disabled>'+
				'</td>'+
				'<td class="dt-body-right">'+
					'<label id="rowAmount'+idRowItem+'">0.0000</label>'+
					'<input type="hidden" id="amount'+idRowItem+'" name="amount[]" value="0">'+
				'</td>'+
				'<td>'+
					'<textarea class="form-control" id="desc_pekerjaan'+idRowItem+'" name="desc_pekerjaan[]" placeholder="Deskripsi" rows="3" disabled></textarea>'+
				'</td>'+
				'<td>'+
					'<input type="hidden" id="id_wo_dpp'+idRowItem+'" name="id_wo_dpp[]" value="new">'+
					'<a class="btn btn-danger btn-sm" onclick="removeRow('+idRowItem+')"><i class="fa fa-minus"></i></a>'+
				'</td>'+
			'</tr>'
		);
		$("#no_dpp"+idRowItem).select2();
		$('#no_dpp'+idRowItem).on('change', function() {
			set_itemValue(this.value, this.id);
			cal_amount();
		})
		$('#dpp_price'+idRowItem).on('keyup', function() {
			cal_amount();
		})
		$('#dpp_price'+idRowItem).on('change', function() {
			cal_amount();
		})

		$.ajax({
			type: "GET",
			url: "<?php echo base_url('work_order/get_permintaan');?>",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					for (var i = 0; i < r.length; i++) {
						var newOption = new Option(r[i].no_pekerjaan, r[i].id_detail_permintaan, false, false);
						$('#no_dpp'+idRowItem).append(newOption);
						cal_amount();
					}
					$('#no_dpp'+idRowItem).removeAttr('disabled');
				}else $('#no_dpp'+idRowItem).removeAttr('disabled');
			}
		});
	})

	function set_itemValue(dpp, rowSelect) {
		var row = rowSelect.replace(/[^0-9]+/g, "");
		if(dpp != '' && dpp != null) {
			$.ajax({
				type: "GET",
				url: "<?php echo base_url('work_order/get_permintaanBy');?>"+'/'+dpp,
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(r) {
					if(r.length > 0) {
						$('#nama_pekerjaan'+row).html(r[0].nama_pekerjaan);
						$('#qty_pekerjaan'+row).html(formatCurrencyComaNull(r[0].qty));
						$('#uom_pekerjaan'+row).html(r[0].uom_name);
						$('#dpp_price'+row).val(0).removeAttr('disabled');
						$('#amount'+row).val(0);
						$('#desc_pekerjaan'+row).val(r[0].deskripsi).removeAttr('disabled');
					}
				}
			});
		}else {
			$('#nama_pekerjaan'+row).html('');
			$('#qty_pekerjaan'+row).html('');
			$('#uom_pekerjaan'+row).html('');
			$('#dpp_price'+row).val('').attr('disabled', 'disabled');
			$('#amount'+row).val(0);
			$('#desc_pekerjaan'+row).val('').attr('disabled', 'disabled');
		}
	}

	function removeRow(rowItem) {
		$('#trRowItem'+rowItem).remove();
		cal_amount();
	}

	function cal_amount() {
		var diskon 			= $('#diskon').val();
		var tempDiskon		= 0;
		var qtyRow			= 0;
		var price 			= 0;
		var totalRow		= 0;
		var tempTotal 		= 0;
		var totalAmount 	= 0;
		if(diskon == '' || diskon == undefined || diskon == null) diskon = 0;

		$('input[name="dpp_price[]"]').each(function() {
			var idRow 		= this.id.replace(/[^0-9]+/g, "");

			var tempQty		= $('#qty_pekerjaan'+idRow).html().replace(/[^0-9.]+/g, "");
			if(tempQty != '' && tempQty != null && tempQty != undefined) qtyRow = parseFloat(tempQty);
			if(this.value != '' && this.value != null && this.value != undefined) price = parseFloat(this.value);

			totalRow = (price * qtyRow);
			if(this.value != '' && this.value != undefined && this.value != null) {
				$('#rowAmount'+idRow).html(formatCurrencyComa(totalRow));
				$('#amount'+idRow).val(totalRow);
			}else {
				$('#rowAmount'+idRow).html(formatCurrencyComa(0));
				$('#amount'+idRow).val(0);
			}
		})

		$('input[name="amount[]"]').each(function() {
			var amountRow = 0;
			if(this.value != '' && this.value != null && this.value != undefined) amountRow = parseFloat(this.value);
			tempTotal = (tempTotal + amountRow);
		});

		if(diskon > 100) {
			totalAmount = tempTotal - diskon;
			$('#totalAmountDiskon').html(formatCurrencyComa(parseFloat(diskon)));
		}else {
			var tempTotalvDiskon = 0;
			tempDiskon			= diskon / 100;
			tempTotalvDiskon	= tempTotal * tempDiskon;
			totalAmount 		= (tempTotal - tempTotalvDiskon);
			$('#totalAmountDiskon').html(diskon+'%');
		}

		$('#totalAmountRow').html(formatCurrencyComa(parseFloat(tempTotal)));		
		$('#totalAmount').html(formatCurrencyComa(parseFloat(totalAmount)));
		$('#total_amount').val(totalAmount);
	}

	function delItem(btnDel, idDetal) {
		var rowParent = $(btnDel).parents('tr');

		swal({
			title: 'Yakin akan Menghapus Detail yang sudah ada ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost = {
				"id"		: parseInt(idDetal)
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>work_order/delete_detail_workOrder",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function () {
							rowParent.remove();
							cal_amount();
						})
					}else swal("Failed!", response.message, "error");
				}
			});
		});
	}

	$('#edit_workOrder').on('submit', (function(e) {
		cal_amount();
		arrIdItem		= [];
		arrNoDpp 		= [];
		arrPrice		= [];
		arrRemark		= [];

		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();

		$('input[name="id_wo_dpp[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '')  arrIdItem.push(this.value);
			}
		})

		$('select[name="no_dpp[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '')  arrNoDpp.push(this.value);
			}
		})

		$('input[name="dpp_price[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '')  arrPrice.push(this.value);
			}
		})

		$('textarea[name="desc_pekerjaan[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arrRemark.push(this.value);
				else arrRemark.push('NULL');
			}else arrRemark.push('NULL');
		})

		if(arrIdItem.length >0 && arrNoDpp.length > 0 && arrPrice.length > 0 && arrRemark.length > 0) {
			var formData = new FormData();
			formData.set('id_wo',			$('#id_wo').val());
			formData.set('delivery_date',	$('#delivery_date').val());
			formData.set('cust_id',			$('#cust_id').val());
			formData.set('term_of_pay',		$('#term_of_pay').val());
			formData.set('valas',			$('#valas').val());
			formData.set('rate',			$('#rate').val());
			formData.set('diskon',			$('#diskon').val());
			formData.set('tujuan',			$('#tujuan').val());
			formData.set('total_amount',	$('#total_amount').val());
			formData.set('arrIdItem',		arrIdItem);
			formData.set('arrNoDpp',		arrNoDpp);
			formData.set('arrPrice',		arrPrice);
			formData.set('arrRemark',		arrRemark);

			$.ajax({
				type:'POST',
				url: $(this).attr('action'),
				data: formData,
				cache:false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function () {
							$('#panel-modal').modal('toggle');
							listWorkOrder();
						})
					}else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Simpan Work Order");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Simpan Work Order");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}else {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Simpan Work Order");
			swal("Failed!", "Invalid Inputan List Pekerjaan, silahkan cek kembali.", "error");
		}
	}));
</script>