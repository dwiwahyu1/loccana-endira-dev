<style>
	.dt-body-left {text-align:left;}
	.dt-body-right {text-align:right;}
	.dt-body-center {text-align:center;}

	.force-overflow {height: 650px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow {min-height: 650px}
	#modal-stbj::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	#modal-stbj::-webkit-scrollbar
	{
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-stbj::-webkit-scrollbar-thumb
	{
		background-image: -webkit-gradient(linear,
		   left bottom,
		   left top,
		   color-stop(0.44, rgb(122,153,217)),
		   color-stop(0.72, rgb(73,125,189)),
		   color-stop(0.86, rgb(28,58,148)));
	}
</style>

<div class="container" style="height: auto;">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Penerimaan Barang Jadi</h4>
		</div>
	</div>

	<div class="row">
        <div class="col-sm-12">
            <div class="card-box">
				<table id="liststbj" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
                        <tr>
							<th style="width: 5%;">No</th>
							<th>Nomor STBJ</th>
							<th>Tanggal STBJ</th>
							<th>Detail Box</th>
							<th>Remark</th>
							<th>Status</th>
							<th>Option</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
            </div>
        </div><!-- end col -->
    </div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-dialog-scrollable" role="document" style="width:90%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-stbj">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal-detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" role="document" style="width:70%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-stbj">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
function stbj_add(){
	$('#panel-modal').removeData('bs.modal');
	$('#panel-modal .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal .panel-body').load('<?php echo base_url('stbj/stbj_add');?>');
	$('#panel-modal .panel-title').html('<i class="fa fa-plus"></i> Tambah STBJ');
	$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}

function stbj_detail(id){
	$('#panel-modal').removeData('bs.modal');
	$('#panel-modal .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal .panel-body').load('<?php echo base_url('stbj/stbj_detail');?>'+'/'+id);
	$('#panel-modal .panel-title').html('<i class="fa fa-search"></i> Detail STBJ');
	$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}

function stbj_edit(id){
	$('#panel-modal').removeData('bs.modal');
	$('#panel-modal .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal .panel-body').load('<?php echo base_url('stbj/stbj_edit');?>'+'/'+id);
	$('#panel-modal .panel-title').html('<i class="fa fa-pencil"></i> Edit STBJ');
	$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}

function stbj_approve(id_stbj,no_stbj){
	swal({
		title: 'Silahkan Pilih Status',
		text: 'Approve STBJ',
		input: 'select',
		inputClass: 'form-control',
		inputPlaceholder: 'Please Select',
		inputOptions: {
		  '1' : 'Approve',
		  '2' : 'Reject'
		},
		inputValidator: (value) => {
			return new Promise((resolve) => {
				if (value === '') {
					resolve('Pilih Status !')
				} else {
					resolve()
				}
			})
		}
	}).then(function (value) {
		var datapost = {
			"id_stbj"   :   id_stbj,
			"no_stbj"   :   no_stbj,
			"status"  	:   value
		};
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('stbj/stbj_approve');?>",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				if(response.value == 1) {
				   swal({
						title: 'Apakah anda yakin!',
						text: response.title,
						type: 'warning',
						showCancelButton: true,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Ok'
					}).then(function () {
						swal("Success!", response.message, "info");
						window.location.href = "<?php echo base_url('stbj');?>";
					})
				}else if(response.value == 2){
					swal("Warning!", response.message, "info");
					window.location.href = "<?php echo base_url('stbj');?>";
				}else{
					swal("Warning!", response.message, "info");
				}
			}
		});
	})
}

function stbj_delete(id){
	swal({
		title: 'Yakin akan Menghapus ?',
		text: 'data tidak dapat dikembalikan bila sudah dihapus !',
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Ya',
		cancelButtonText: 'Tidak'
	}).then(function () {
		var datapost={
			"id"  :   id
		};
		$.ajax({
			type: "POST",
			url: "<?php echo base_url('stbj/stbj_delete');?>",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				
			   swal({
					title: 'Success!',
					text: response.message,
					type: 'success',
					showCancelButton: false,
					confirmButtonText: 'Ok'
				}).then(function () {
					window.location.href = "<?php echo base_url('stbj');?>";
				})
				if (response.status == "success") {
				} else{
                    swal("Failed!", response.message, "error");
				}
			}
		});
	})
}

function liststbj(){
	$("#liststbj").dataTable({
		"processing": true,
		"serverSide": true,
		"ajax": "<?php echo base_url().'stbj/lists';?>",
		"searchDelay": 700,
		"responsive": true,
		"lengthChange": false,
		"destroy": true,
		"info": false,
		"bSort": false,
		"dom": 'l<"toolbar">frtip',
		"initComplete": function(){
			var element = '<div class="btn-group pull-left">';
				element += '  	<a class="btn btn-primary" onClick="stbj_add()">';
				element += '    	<i class="fa fa-plus"></i> Add Penerimaan Barang Jadi';
				element += '  	</a>';
				element += '</div>';
			$("div.toolbar").prepend(element);
		},
		"columnDefs": [{
			"targets": [0],
			"searchable": false,
			"width": 10,
			"className": 'dt-body-center'
		},{
			"targets": [1],
			"searchable": false,
			"width": 120,
			"className": 'dt-body-left'
		},{
			"targets": [2],
			"searchable": false,
			"width": 120,
			"className": 'dt-body-left'
		},{
			"targets": [3],
			"searchable": false,
			"width": 90,
			"className": 'dt-body-left'
		},{
			"targets": [4],
			"searchable": false,
			"width": 90,
			"className": 'dt-body-left'
		},{
			"targets": [5],
			"searchable": false,
			"width": 20,
			"className": 'dt-body-center'
		},{
			"targets": [6],
			"searchable": false,
			"width": 120,
			"className": 'dt-body-center'
		}]
    });
}
  
function stbj_delete(id){
	swal({
		title: 'Yakin akan Menghapus ?',
		text: 'data tidak dapat dikembalikan bila sudah dihapus !',
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Ya',
		cancelButtonText: 'Tidak'
    }).then(function () {
		var datapost={
			"id" : id
		};

      $.ajax({
        type: "POST",
        url: "<?php echo base_url();?>stbj/stbj_delete",
        data : JSON.stringify(datapost),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function(response) {
           $('.panel-heading button').trigger('click');
				listdelivery();
				swal({
					title: 'Success!',
					text: response.message,
					type: 'success',
					showCancelButton: false,
					confirmButtonText: 'Ok'
				}).then(function () {
				})
			}
		});
    })
}

$(document).ready(function(){
	liststbj();
});
</script>