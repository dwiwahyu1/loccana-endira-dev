	<style>
		#loading-us{display:none}
		#tick{display:none}
		.x-hidden{display:none}

		#loading-mail{display:none}
		#cross{display:none}
		.add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
		.add_item:hover{color:#ff8c00}
		.right-text{text-align:right}
		
		.dt-body-left {text-align:left;}
		.dt-body-right {text-align:right;}
		.dt-body-center {text-align:center;}
	</style>

	<form class="form-horizontal form-label-left" id="stbj_detail_packing_edit" role="form" action="<?php echo base_url('stbj/edit_stbj');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

  		<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
		<br>
        <div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_stbj">No STBJ <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input placeholder="No. STBJ" type="text" class="form-control" id="no_stbj" name="no_stbj" value="<?php if(isset($stbj[0]['no_stbj'])){ echo $stbj[0]['no_stbj']; }?>" readonly required autocomplete="off">
				<span id="loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking No STBJ...</span>
				<span id="tick"></span>
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_stbj">Tanggal STBJ <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<div class="input-group date">
					<input placeholder="<?php echo date('Y-m-d'); ?>" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="tanggal_stbj" name="tanggal_stbj" placeholder="" value="<?php if(isset($stbj[0]['tanggal_stbj'])){ echo $stbj[0]['tanggal_stbj']; }?>" required autocomplete="off">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-th"></span>
					</div>
				</div>
			</div>
		</div>
		
		
		<div class="item form-group x-hidden">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="detail_box">Detail Box <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<textarea placeholder="Detail Box" class="form-control" id="detail_box" name="detail_box" autocomplete="off"><?php if(isset($stbj[0]['detail_box'])){ echo $stbj[0]['detail_box']; }?></textarea>
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_pi">No PI <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input placeholder="No. PI" type="text" class="form-control" id="no_pi" name="no_pi" value="<?php if(isset($stbj[0]['no_pi'])){ echo $stbj[0]['no_pi']; }?>" required autocomplete="off">
			</div>
		</div>
		
		

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="remark">Remark <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<textarea placeholder="Remark" class="form-control" id="remark" name="remark" required autocomplete="off"><?php if(isset($stbj[0]['remark'])){ echo $stbj[0]['remark']; }?></textarea>
			</div>
		</div>
		
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Barang : </label>
			<div class="col-md-8 col-sm-6 col-xs-12 add_item">
				<a class="btn btn-primary" onclick="stbj_edit_item()">
					<i class="fa fa-plus"></i> Tambah Barang
				</a> 
				<input type="hidden" id="tambah_barang" name="tambah_barang" value="0">
			</div>
		</div>

	
		<div class="item form-group">
			<div class="col-md-12">
				<table id="listeditpacking" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th></th>
							<th>No Packing</th>
							<th>Date Packing</th>
							<th>Qty Box</th>
							<th>Qty Array</th>
							<th>Net Weight</th>
							<th>Gross Waight</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	
		<hr>
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Simpan</button>
				<input type="hidden" id="id_stbj" name="id_stbj" value="<?php if(isset($stbj[0]['id_stbj'])){ echo $stbj[0]['id_stbj']; }?>">
				<input type="hidden" id="id_packing" name="id_packing" value="<?php if(isset($stbj[0]['id_packing'])){ echo $stbj[0]['id_packing']; }?>">
			</div>
		</div>
	</form>
	
	<script type="text/javascript">
	var t_editBarang;
	
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
		
		$('#tanggal_stbj').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true,
			changeYear: true,
			minDate: '-3M',
			maxDate: '+30D',
		});
		
		dtPacking();
	});

	function dtPacking() {
		t_editPacking = $('#listeditpacking').DataTable( {
			"processing": true,
			"searching": false,
			"paging": false,
			"responsive": true,
			"lengthChange": false,
			"ajax": {
				"type" : "GET",
				"url" : "<?php echo base_url().'stbj/stbj_detail_packing/'.$stbj[0]['id_stbj'];?>"
            },
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0],
				"visible": false,
				"searchable": false
			},{
				"targets": [1, 2],
				"searchable": false,
				"className": 'dt-body-left',
				"width": 100
			},{
				"targets": [3, 4, 5, 6],
				"searchable": false,
				"className": 'dt-body-right',
				"width": 90
			},{
				"targets": [7],
				"className": 'dt-body-center',
				"width": 50
			}]
		});
	}
	
	function stbj_edit_item(){
		$('#panel-modal-detail').removeData('bs.modal');
		$('#panel-modal-detail  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal-detail  .panel-body').load('<?php echo base_url('stbj/stbj_edit_item');?>');
		$('#panel-modal-detail  .panel-title').html('<i class="fa fa-plus"></i> Edit Packing Barang');
		$('#panel-modal-detail').modal({backdrop:'static',keyboard:false},'show');
	}

	$('#listeditpacking').on("click", "button", function(){
		var delStat = false;
		var dataPacking = t_editPacking.row($(this).parents('tr')).data();
		if(dataPacking[0] != "") {
			swal({
				title: 'Yakin akan Menghapus Barang yang sudah ada ?',
				text: 'data tidak dapat dikembalikan bila sudah dihapus !',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak'
			}).then(function () {
				var datapost={
					"id_packing" : parseInt(dataPacking[0])
				};
				
				$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>stbj/delete_stbj_detail",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
						if (response.status == "success") {
							swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () { })
						}else {
							swal("Failed!", response.message, "error");
							delStat = false;
						}
					},
					error: function (xhr, ajaxOptions, thrownError) {
						swal("Error deleting!", "Silahkan coba lagi", "error");
					}
				});
			})
			t_editPacking.row($(this).parents('tr')).remove().draw(false);
		}else t_editPacking.row($(this).parents('tr')).remove().draw(false);

		for (var i = 0; i < t_editPacking.rows().data().length; i++) {
			var rowData = t_editPacking.row(i).data();
		}
	});
	
	$('#stbj_detail_packing_edit').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		var formData = new FormData(this);
		
		$.ajax({
			type:'POST',
			url: $(this).attr('action'),
			data:formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					save_packing(response.id_packing);
				}else{
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Simpan");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Simpan");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}));
	
	function save_packing(id_packing) {
		
		var arrTemp = [];
		var id_stbj = '<?php echo $stbj[0]['id_stbj']; ?>';
		for (var i = 0; i < t_editPacking.rows().data().length; i++) {
			var rowData = t_editPacking.row(i).data();
			arrTemp.push(rowData); 
		}

		var datapost = {
			"id_stbj"		: id_stbj,
			"id_packing"	: id_packing,
			"listpacking"	: arrTemp
		};
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>stbj/edit_detail_stbj",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						$('#panel-modal').modal('toggle');
						liststbj();
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Simpan");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Simpan");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}
	
	function isNumber(evt) {
		var iKeyCode = (evt.which) ? evt.which : evt.keyCode
		if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
			return false;

		return true;
	}
	</script>