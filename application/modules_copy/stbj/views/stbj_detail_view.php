	<div class="row">
		<div class="col-md-12">
			<div class="col-md-6 pull-left">
				<div class="row">
					<label class="col-md-3">Nomor STBJ </label>
					<label class="col-md-1 pull-left">:</label>
					<label class="col-md-8 pull-left"><?php if(isset($dp[0]['no_stbj'])){ echo $dp[0]['no_stbj']; }?></label>
				</div>
				
				<div class="row">
					<label class="col-md-3">Tanggal STBJ </label>
					<label class="col-md-1 pull-left">:</label>
					<label class="col-md-8 pull-left"><?php if(isset($dp[0]['tanggal_stbj'])){ 
						$date_stbj = date_create($dp[0]['tanggal_stbj']);
						echo date_format($date_stbj,"d F Y"); }?>
					</label>
				</div>
			</div>
		</div>
		<br>
		<br>
		<hr>
		
		<div class="col-md-12">
			<div class="col-md-6 pull-left">
				<div class="row">
					<label class="col-md-3">Detail Box </label>
					<label class="col-md-1">:</label>
					<label class="col-md-8"><?php if(isset($dp[0]['detail_box'])){ echo $dp[0]['detail_box']; }?></label>
				</div>

				<div class="row">
					<label class="col-md-3">No PI </label>
					<label class="col-md-1">:</label>
					<label class="col-md-8"><?php if(isset($dp[0]['no_pi'])){ echo $dp[0]['no_pi']; }?></label>
				</div>

				<div class="row">
					<label class="col-md-3">Remark </label>
					<label class="col-md-1">:</label>
					<label class="col-md-8"><?php if(isset($dp[0]['remark'])){ echo $dp[0]['remark']; }?></label>
				</div>

				<div class="row">
					<label class="col-md-3" for="nama">Daftar Packing </label>
					<label class="col-md-1">:</label>
					<div class="col-md-12">
						
					</div>
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-12">
				<table id="listdetailpacking" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>No PO</th>
							<th>Part Number</th>
							<th>Date Packing</th>
							<th>Qty Box</th>
							<th>Qty Array</th>
							<th>Net Weight</th>
							<th>Gross Weight</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
<script type="text/javascript">
	$(document).ready(function(){
		dt_packing();
	});

	function dt_packing() {
		$('#listdetailpacking').DataTable( {
			"processing": true,
			"searching": false,
			"paging": false,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"ajax": {
				"type" : "GET",
				"url" : "<?php echo base_url().'stbj/detail_stbj/'.$dp[0]['id_stbj'];?>"
			},
		});
	}
	
	function delivery_detail_order_delete(id_packing){
		swal({
			title: 'Yakin akan Menghapus ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost={
				"id_packing"  :   id_packing
			};
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('stbj/delivery_detail_order_delete');?>",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
					
				   swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						listdelivery();
					})
					if (response.status == "success") {
					} else{
						swal("Failed!", response.message, "error");
					}
				}
			});
		})
	}

</script>