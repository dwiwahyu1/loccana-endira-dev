	<style>
		#loading-us {
			display: none
		}

		#tick {
			display: none
		}
		
		.x-hidden {
			display: none
		}

		#loading-mail {
			display: none
		}

		#cross {
			display: none
		}

		.add_item {
			cursor: pointer;
			text-decoration: underline;
			color: #96b6e8;
			padding-top: 6px;
		}

		.add_item:hover {
			color: #ff8c00
		}

		.right-text {
			text-align: right
		}

		.modal-body {
			overflow-y: auto;
		}
	</style>

	<form class="form-horizontal form-label-left" id="stbj_add" role="form" action="<?php echo base_url('stbj/insert_stbj'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>

		<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
		<br>
		<!--<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_stbj">No STBJ <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input placeholder="No. STBJ" type="text" class="form-control" id="no_stbj" name="no_stbj" readonly value="<?php //echo $STBJNo; ?>">
			</div>
		</div>-->

		<input placeholder="No. STBJ" type="hidden" class="form-control" id="no_stbj" name="no_stbj" readonly value="<?php //echo $STBJNo; ?>" autocomplete="off">
		
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_stbj">Tanggal STBJ <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<div class="input-group date">
					<input placeholder="<?php echo date('Y-m-d'); ?>" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="tanggal_stbj" name="tanggal_stbj" placeholder="" autocomplete="off" required>
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-th"></span>
					</div>
				</div>
			</div>
		</div>

		<input placeholder="No. PI" type="hidden" class="form-control" id="detail_box" name="detail_box" value="">
		<input placeholder="No. PI" type="hidden" class="form-control" id="no_pi" name="no_pi" value="">

		<div class="item form-group x-hidden">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="detail_box">Detail Box <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<textarea placeholder="Detail Box" class="form-control" id="detail_box" name="detail_box" autocomplete="off"></textarea>
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_pi">No PI <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input placeholder="No. PI" type="text" class="form-control" id="no_pi" name="no_pi" required autocomplete="off">
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="remark">Remark <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<textarea placeholder="Remark" class="form-control" id="remark" name="remark" required autocomplete="off"></textarea>
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Barang : </label>
			<div class="col-md-8 col-sm-6 col-xs-12 add_item">
				<a class="btn btn-primary" onclick="stbj_add_item()">
					<i class="fa fa-plus"></i> Tambah Barang
				</a>
				<input type="hidden" id="tambah_barang" name="tambah_barang" value="0">
			</div>
		</div>


		<div class="item form-group">
			<table id="listaddpacking" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th></th>
						<th>No Packing</th>
						<th>Date Packing</th>
						<th>Qty Box</th>
						<th>Qty Array</th>
						<th>Net Weight</th>
						<th>Gross Weight</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>

		<hr>
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Simpan</button>
			</div>
		</div>
	</form>

	<script type="text/javascript">
		var dataBarang = [];
		var t_addPacking;
		// var last_no_stbj = $('#no_stbj').val();

		$(document).ready(function() {
			$('form').parsley();
			$('[data-toggle="tooltip"]').tooltip();

			$('#tanggal_stbj').datepicker({
				format: "yyyy-mm-dd",
				autoclose: true,
				todayHighlight: true,
				changeYear: true,
				minDate: '-3M',
				maxDate: '+30D',
			});

			dtPacking();
		});

		// $('#no_stbj').on('input',function(event) {
		// 	if($('#no_stbj').val() != last_no_stbj) {
		// 		no_stbj_check();
		// 	}
		// });

		// function no_stbj_check() {
		// 	var no_stbj = $('#no_stbj').val();
		// 	if(no_stbj.length > 3) {
		// 		var post_data = {
		// 			'no_stbj': no_stbj
		// 		};

		// 		$('#tick').empty();
		// 		$('#tick').hide();
		// 		$('#loading-us').show();
		// 		jQuery.ajax({
		// 			type: "POST",
		// 			url: "<?php echo base_url('stbj/check_no_stbj'); ?>",
		// 			data: post_data,
		// 			cache: false,
		// 			success: function(response){
		// 				if(response.success == true){
		// 					$('#no_stbj').css('border', '3px #090 solid');
		// 					$('#loading-us').hide();
		// 					$('#tick').empty();
		// 					$("#tick").append('<span class="fa fa-check"> '+response.message+'</span>');
		// 					$('#tick').show();
		// 				}else {
		// 					$('#no_stbj').css('border', '3px #C33 solid');
		// 					$('#loading-us').hide();
		// 					$('#tick').empty();
		// 					$("#tick").append('<span class="fa fa-close"> '+response.message+'</span>');
		// 					$('#tick').show();
		// 				}
		// 			}
		// 		});
		// 	}else {
		// 		$('#no_stbj').css('border', '3px #C33 solid');
		// 		$('#loading-us').hide();
		// 		$('#tick').empty();
		// 		$("#tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
		// 		$('#tick').show();
		// 	}
		// }

		function dtPacking() {
			t_addPacking = $('#listaddpacking').DataTable({
				"processing": true,
				"searching": false,
				"paging": false,
				"responsive": true,
				"lengthChange": false,
				"info": false,
				"bSort": false,
				"columnDefs": [{
					"targets": [0],
					"visible": false,
					"searchable": false
				}]
			});
		}

		function stbj_add_item() {
			$('#panel-modal-detail').removeData('bs.modal');
			$('#panel-modal-detail  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
			$('#panel-modal-detail  .panel-body').load('<?php echo base_url('stbj/stbj_add_item'); ?>');
			$('#panel-modal-detail  .panel-title').html('<i class="fa fa-plus"></i> Tambah Packing Barang');
			$('#panel-modal-detail').modal({
				backdrop: 'static',
				keyboard: false
			}, 'show');
		}

		$('#listaddpacking').on("click", "button", function() {
			t_addPacking.row($(this).parents('tr')).remove().draw(false);
			for (var i = 0; i < t_addPacking.rows().data().length; i++) {
				var rowData = t_addPacking.row(i).data();
			}
		});

		$('#stbj_add').on('submit', (function(e) {
			if ($('#stbj').val() !== '') {
				$('#btn-submit').attr('disabled', 'disabled');
				$('#btn-submit').text("Memasukkan data...");
				e.preventDefault();
				var formData = new FormData(this);

				$.ajax({
					type: 'POST',
					url: $(this).attr('action'),
					data: formData,
					cache: false,
					contentType: false,
					processData: false,
					success: function(response) {
						if (response.success == true) {
							save_packing(response.lastid);
						} else {
							$('#btn-submit').removeAttr('disabled');
							$('#btn-submit').text("Simpan");
							swal("Failed!", response.message, "error");
						}
					}
				}).fail(function(xhr, status, message) {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Simpan");
					swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
				});
			} else {
				swal("Failed!", "Maaf kolom Keterangan harus diisi", "error");
			}
			return false;
		}));

		function save_packing(lastid) {

			var arrTemp = [];
			for (var i = 0; i < t_addPacking.rows().data().length; i++) {
				var rowData = t_addPacking.row(i).data();
				arrTemp.push(rowData);
			}

			var datapost = {
				"id_stbj"		: lastid,
				'tanggal_stbj'	: $('#tanggal_stbj').val(),
				'listpacking'	: arrTemp
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>stbj/insert_detail_stbj",
				data: JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function() {
							$('#panel-modal').modal('toggle');
							liststbj();
						})
					} else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Simpan");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Simpan");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}

		function isNumber(evt) {
			var iKeyCode = (evt.which) ? evt.which : evt.keyCode
			if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
				return false;

			return true;
		}
	</script>