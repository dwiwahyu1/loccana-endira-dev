	<style>
		#loading-us{display:none}
		#tick{display:none}

		#loading-mail{display:none}
		#cross{display:none}
		.add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
		.add_item:hover{color:#ff8c00}
		.right-text{text-align:right}
		.form-item{margin-top: 15px;overflow: auto;}
	</style>

	
  	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<br>
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_po">Nomor Order<span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="no_po" id="no_po" style="width: 100%" required autocomplete="off">
				<option value="">Select Nomor Order</option>
				<?php foreach($packing as $pack => $p) {
					if($p['id_order'] != '') { ?>
						<option value="<?php echo $p['id_order']; ?>" ><?php echo $p['stock_name']; ?></option>
				<?php
					}
				} ?>
			</select>
		</div>
	</div>
	
	<div class="item form-group form-item">
		<table id="listpacking" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th></th>
					<th>No Packing</th>
					<th>Date Packing</th>
					<th>Qty Box</th>
					<th>Qty Array</th>
					<th>Net Weight</th>
					<th>Gross Weight</th>
					<th>Pilih</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>

	<hr>
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="btn_submit_simpan"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn_submit_simpan" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Simpan</button>
		</div>
	</div>
	
	<script type="text/javascript">
	var table_packing;

	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();

		resetDt();
		$("#no_po").select2();

		$('#no_po').on('change', function() {
			if(this.value != '' && this.value != null) {
				sendNo(this.value);
			}else table_packing.clear().draw();
		})
	});
	
	function resetDt() {
		table_packing = $('#listpacking').DataTable( {
			"processing": true,
			"searching": false,
			"paging": false,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0],
				"visible": false,
				"searchable": false
			}]
		});
	}
	
	function sendNo(id_order) {
		var tempPacking = [];
		if(t_editPacking.rows().data().length > 0) {
			for (var i = 0; i < t_editPacking.rows().data().length; i++) {
				tempPacking.push(t_editPacking.rows().data()[i]);
			}
		}
		
		var datapost = {
			"id_order"		: id_order,
			"tempPacking"	: tempPacking
		};
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>stbj/stbj_packing_search_id",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.success == true) {
					table_packing.clear().draw();
					var data = r.data;
					for (var i = 0; i < data.length; i++) {
						table_packing.row.add(data[i]).draw(false);
					}
				}else table_packing.clear().draw();
			}
		});
	}
	
	$('#btn_submit_simpan').click(function() {
		var tempArr = [];
		var statusBtn = 0;

		$('input[type=checkbox]').each(function () {
			if(this.checked == true) {
				var row = parseInt($(this).val());
				var rowData = table_packing.row(row).data();
				var qtyBox = $('#qty_box' + row).val();
				var qtyArray = $('#qty_array' + row).val();
				var netWeight = $('#net_weight' + row).val();
				var grossWeight = $('#gross_weight' + row).val();
				
				var option =
					'<button class="btn btn-icon waves-effect waves-light btn-danger">'+
						'<i class="fa fa-trash"></i>'+
					'</button>';
				if(qtyBox != '' && qtyBox != 0) {
					var arrRow = [rowData[0], rowData[1], rowData[2], rowData[3], rowData[4], rowData[5], rowData[6], option];
					tempArr.push(arrRow);
					arrRow = [];
					statusBtn = 1;
				}else if(qtyBox == 0) {
					swal('Warning', 'Data qty box baris ke-'+ (row+1) +' tidak boleh kosong');
					statusBtn = 0;
				}else if(qtyArray == 0) {
					swal('Warning', 'Data qty array baris ke-'+ (row+1) +' tidak boleh kosong');
					statusBtn = 0;
				}else {
					swal('Warning', 'Data qty baris ke-'+ (row+1) +' harus diisi');
					statusBtn = 0;
				}
			}
		});
		
		if(statusBtn == 1) {
			for (var i = 0; i < tempArr.length; i++) {
				t_editPacking.row.add(tempArr[i]).draw();
			}
			$('#panel-modal-detail').modal('toggle');
		}

		for (var i = 0; i < t_editPacking.rows().data().length; i++) {
			var rowData = t_editPacking.row(i).data();
		}
	})
	
	function isNumber(evt) {
		var iKeyCode = (evt.which) ? evt.which : evt.keyCode
		if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
			return false;

		return true;
	}
	</script>