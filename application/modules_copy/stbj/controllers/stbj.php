<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Stbj extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('stbj/stbj_model');
		$this->load->library('log_activity');
		$this->load->library('sequence');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'stbj/views/index');
	}

	function lists() {
        $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
        $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
        $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
        $order = $this->input->get_post('order');
        $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'DESC';
        $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

        $order_fields = array('', 'no_stbj');

        $search = $this->input->get_post('search');

        $search_val = (!empty($search['value'])) ? $search['value'] : null;

        $search_value = $this->Anti_sql_injection($search_val);

        // Build params for calling model
        $params['limit'] = (int) $length;
        $params['offset'] = (int) $start;
        $params['order_column'] = $order_fields[$order_column];
        $params['order_dir'] = $order_dir;
        $params['filter'] = $search_value;
		
        $list = $this->stbj_model->lists($params);
		
        $result["recordsTotal"] = $list['total'];
        $result["recordsFiltered"] = $list['total_filtered'];
        $result["draw"] = $draw;
		
        $data = array();
		$no=1;
        foreach ($list['data'] as $k => $v) {
            $no = $k+1;
			
			if($v['status'] == 0) {
				$status = '<span class="label label-warning">Pending</span>';
				
				$actions = '<div class="btn-group">';
				$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit STBJ" onClick="stbj_edit(\'' . $v['id_stbj'] . '\')">';
				$actions .= '       <i class="fa fa-pencil"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
				$actions .= '<div class="btn-group">';
				$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Detail STBJ" onClick="stbj_detail(\'' . $v['id_stbj'] . '\')">';
				$actions .= '       <i class="fa fa-search"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
				$actions .= '<div class="btn-group">';
				$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Approve STBJ" onClick="stbj_approve(\'' . $v['id_stbj'] . '\',\'' . $v['no_stbj'] .'\')">';
				$actions .= '       <i class="fa fa-check"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
				
            }elseif($v['status'] == 1){
				$status = '<span class="label label-success">Approve</span>';
				
				$actions = '<div class="btn-group">';
				$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Detail STBJ" onClick="stbj_detail(\'' . $v['id_stbj'] . '\')">';
				$actions .= '       <i class="fa fa-search"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
			}else{
				$status = '<span class="label label-danger">Reject</span>';
				
				$actions = '<div class="btn-group">';
				$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Detail STBJ" onClick="stbj_detail(\'' . $v['id_stbj'] . '\')">';
				$actions .= '       <i class="fa fa-search"></i>';
				$actions .= '   </button>';
				$actions .= '</div>';
			}
			
            array_push($data, array(
                $no,
                $v['no_stbj'],
                $v['tanggal'],
				$v['detail_boxs'],
				//$v['no_pi'],
                $v['remark'],
                $status,
                $actions
				)
            );
        }
		
        $result["data"] = $data;
		
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	public function stbj_add() {
		$data = array(
			// 'STBJNo' => $this->sequence->get_no('stbj')
		);
		
		$this->load->view('stbj_add_view',$data);
	}
	
	public function stbj_add_item() {
		$results = $this->stbj_model->stbj_list_packing();
		
		$data = array(
			'packing' => $results
		);

		$this->load->view('stbj_add_item_view',$data);
	}
	
	public function stbj_detail($id_stbj) {
		$results = $this->stbj_model->stbj_detail($id_stbj);
		
		$data = array(
			'dp' => $results
		);
		
		$this->load->view('stbj_detail_view',$data);
	}
	
	public function stbj_edit($id_stbj) {
		$results = $this->stbj_model->stbj_detail($id_stbj);
		
		$data = array(
			'stbj' => $results
		);

		$this->load->view('stbj_edit_view',$data);
	}
	
	public function stbj_edit_item() {
		$results =  $this->stbj_model->stbj_list_packing();
		
		$data = array(
			'packing' => $results
		);
		
		$this->load->view('stbj_edit_item_view',$data);
	}
	
	public function stbj_packing_search_id() {
		$data		= file_get_contents("php://input");
		$params		= json_decode($data,true);

		$var = array('id_order' => $params['id_order']);

		$tempPacking = $params['tempPacking'];
		$list = $this->stbj_model->stbj_packing_search_id($var);

		$data = array();
		$i = 0;
		if(sizeof($list) > 0) {
			foreach ($list as $lk => $lv) {
				$tempStat = true;
				if(sizeof($tempPacking) > 0) {
					for ($j=0; $j < sizeof($tempPacking); $j++) {
						if($lv['id_packing'] == $tempPacking[$j][0]) $tempStat = false;
					}
				}

				if(($lv['id_packing'] != null || $lv['id_packing'] != '') && $tempStat == true) {
					$strOpt =
						'<div class="checkbox">'.
							'<input id="option['.$i.']" name="tempOption[]" type="checkbox" value="'.$i.'">'.
							'<label for="option['.$i.']"></label>'.
						'</div>';
					array_push($data, array(
						$lv['id_packing'],
						$lv['no_packing'],
						date('d-M-Y', strtotime($lv['date_packing'])),
						$lv['qty_box'],
						$lv['qty_array'],
						$lv['net_weight'],
						$lv['gross_weight'],
						$strOpt
					));
					$i++;
				}
			}
			$result = array('success' => true, 'data' => $data);
		}else $result = array('success'	=> false, 'data' => $data);

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result);
	}
	
	public function detail_stbj($id_stbj) {
		$list = $this->stbj_model->delivery_packing_search_idstbj($id_stbj);
		
		$data = array();
		$i = 0;
		$no = 0;
		
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list as $k => $v) {

			$actions = '<div class="btn-group">';
            $actions .= '   <button class="btn btn-icon waves-effect waves-light btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Hapus" onClick="stbj_order_delete(\'' . $v['id_packing'] . '\')">';
            $actions .= '       <i class="fa fa-trash"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';

			array_push($data, array(
				$no = $k+1,
				$v['no_po'],
				$v['stock_name'],
				$v['date_packing'],
				$v['qty_box'],
				$v['qty_array'],
				$v['net_weight'],
				$v['gross_weight']
				//$actions
			));
			$i++;
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function stbj_detail_packing($id_stbj) {
		$list = $this->stbj_model->stbj_detail($id_stbj);
		
		$data = array();
		$i = 0;
		
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list as $k => $v) {
			
			$actions = '<div class="btn-group">';
            $actions .=   	'<button class="btn btn-icon waves-effect waves-light btn-danger">';
			$actions .=			'<i class="fa fa-trash"></i>';
			$actions .=		'</button>';
            $actions .= '</div>';

			array_push($data, array(
				$v['id_packing'],
				$v['no_packing'],
				$v['date_packing'],
				$v['qty_box'],
				$v['qty_array'],
				$v['net_weight'],
				$v['gross_weight'],
				$actions
			));
			$i++;
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function insert_stbj() {
		$this->form_validation->set_rules('tanggal_stbj', 'Tanggal PO', 'trim|required');
		$this->form_validation->set_rules('remark', 'Remark', 'trim|required');
		//$this->form_validation->set_rules('detail_box', 'ID STBJ', 'trim|required');
		$this->form_validation->set_rules('no_pi', 'No Delivery Order', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$results = array(
				'success' => false,
				'message' => $msg
			);
			
			$this->log_activity->insert_activity('update', $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($results));
		}else {
			$no_stbj		= $this->sequence->get_max_no('stbj');
			$tanggal_stbj	= $this->Anti_sql_injection($this->input->post('tanggal_stbj', TRUE));
			// $detail_box		= $this->Anti_sql_injection($this->input->post('detail_box', TRUE));
			$no_pi 			= $this->Anti_sql_injection($this->input->post('no_pi', TRUE));
			$remark 		= $this->Anti_sql_injection($this->input->post('remark', TRUE));

			$data = array(
				'no_stbj'		=> $no_stbj,
				'tanggal_stbj'	=> $tanggal_stbj,
				// 'detail_box'	=> $detail_box,
				'no_pi'			=> $no_pi,
				'remark'		=> $remark,
				'status'		=> 0
			);

			$results = $this->stbj_model->insert_stbj($data);
			if ($results['result'] > 0) {
				$this->sequence->save_max_no('stbj');
				$msg = 'Berhasil menambahkan data STBJ';

				$results = array('success' => true, 'message' => $msg, 'lastid' => $results['lastid']);
				$this->log_activity->insert_activity('insert', $msg. ' dengan No STBJ ' .$no_stbj);
			}else {
				$msg = 'Gagal menambahkan data STBJ ke database';

				$results = array('success' => false, 'message' => $msg);
				$this->log_activity->insert_activity('insert', $msg. ' dengan No STBJ ' .$no_stbj);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}
	
	public function insert_detail_stbj() {
		$data   	= file_get_contents("php://input");
		$no_stbj	= $this->Anti_sql_injection($this->input->post('no_stbj', TRUE));
		$params     = json_decode($data,true);

		$getStbjMaterial = $this->stbj_model->get_stbj_material($params['id_stbj']);
		
		foreach ($params['listpacking'] as $k) {
			$arrTemp = array(
				'id_stbj'		=> $params['id_stbj'],
				'id_packing'	=> $k[0]
			);
			
			$results = $this->stbj_model->insert_detail_packing($arrTemp);
			$results_update = $this->stbj_model->update_detail_box($arrTemp);
		}
		
		if ($results > 0) {
			// $getStbjMaterial = $this->stbj_model->get_stbj_material($params['id_stbj']);

			// foreach ($getStbjMaterial as $smk => $smv) {
				// $dataMutasi = array(
					// 'id_stock_awal'		=> $smv['id'],
					// 'id_stock_akhir'	=> $smv['id'],
					// 'date_mutasi'		=> date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s'))),
					// 'amount_mutasi'		=> $smv['qty'],
					// 'type_mutasi'		=> 0
				// );

				// $dataCoaVlue = array(
					// 'id_coa'		=> 122,
					// 'id_parent'		=> 0,
					// 'stbj_date'		=> date('Y-m-d', strtotime($params['tanggal_stbj'])),
					// 'id_valas'		=> $smv['valas_id'],
					// 'stbj_value'	=> $smv['stbj_value'],
					// 'adjusment'		=> 0,
					// 'type_cash'		=> 0,
					// 'note'			=> 'Finish Good PO '.$smv['no_po'],
					// 'rate'			=> $smv['rate'],
					// 'bukti'			=> NULL,
					// 'id_coa_temp'	=> NULL
				// );

				// $dataQtyMaterial = array(
					// 'qty'			=> $smv['qty_material'] + $smv['qty'],
					// 'id_material'	=> $smv['id']
				// );
				
				// $resultMutasi = $this->stbj_model->insertMutasi($dataMutasi);
				// $resultCoaValue = $this->stbj_model->insertCoaValue($dataCoaVlue);
				// $resultMaterial = $this->stbj_model->update_qtyMaterial($dataQtyMaterial);
			// }

			$msg = 'Berhasil menambahkan STBJ packing';

			$results = array('success' => true, 'message' => $msg);
			$this->log_activity->insert_activity('insert', $msg. ' dengan No STBJ ' .$no_stbj);
		}else {
			$msg = 'Gagal menambahkan STBJ packing ke database';

			$results = array('success' => false, 'message' => $msg);
			$this->log_activity->insert_activity('insert', $msg. ' dengan ID STBJ ' .$params['id_stbj']);
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($results);
	}
	
	public function stbj_approve() {
		$data   = file_get_contents("php://input");
		$params = json_decode($data,true);
		
		$title 	= 'Akan mengubah status STBJ';
		
		$date_mutasi = date('Y-m-d H:i:s');
		
		$data_update = array(
			'id_stbj' 	=> $params['id_stbj'],
			'status'	=> $params['status']
		);
		
		if($params['status'] == 1) {
			$update_status = $this->stbj_model->update_status_stbj($data_update);
			$getStbjMaterial = $this->stbj_model->get_stbj_material($params['id_stbj']);
			$totalQty = 0;
			
			foreach ($getStbjMaterial as $smk => $smv) {
				$dataMutasi = array(
					'id_stock_awal'		=> $smv['id'],
					'id_stock_akhir'	=> $smv['id'],
					'date_mutasi'		=> date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s'))),
					'amount_mutasi'		=> $smv['qty'],
					'type_mutasi'		=> 0
				);

				$dataCoaVlue = array(
					'id_coa'		=> 122,
					'id_parent'		=> 0,
					'stbj_date'		=> date('Y-m-d', strtotime($getStbjMaterial[0]['tanggal_stbj'])),
					'id_valas'		=> $smv['valas_id'],
					'stbj_value'	=> $smv['stbj_value'],
					'adjusment'		=> 0,
					'type_cash'		=> 0,
					'note'			=> 'Finish Good PO '.$smv['no_po'],
					'rate'			=> $smv['rate'],
					'bukti'			=> NULL,
					'id_coa_temp'	=> NULL
				);
				
				$totalQty = $totalQty + $smv['qty'];
				
				$dataQtyMaterial = array(
					'qty'			=> $smv['qty_material'] + $totalQty,
					'id_material'	=> $smv['id']
				);
				
				$resultMutasi = $this->stbj_model->insertMutasi($dataMutasi);
				$resultCoaValue = $this->stbj_model->insertCoaValue($dataCoaVlue);
			}
			
			$resultMaterial = $this->stbj_model->update_qtyMaterial($dataQtyMaterial);
			
			$msg = 'Status STBJ berhasil di Approve';

			$results = array('success' => true, 'message' => $msg, 'value' => $params['status'], 'title' => $title);
			$this->log_activity->insert_activity('insert', $msg. ' dengan No STBJ ' .$params['no_stbj']);	
			
		}elseif($params['status'] == 2) {
			
			$update_status = $this->stbj_model->update_status_stbj($data_update);
			
			$msg = 'Status STBJ berhasil di Reject';

			$results = array('success' => true, 'message' => $msg, 'value' => $params['status'], 'title' => $title);
			$this->log_activity->insert_activity('insert', $msg. ' dengan No STBJ ' .$params['no_stbj']);	
		}else{
			$msg = 'Gagal Merubah status STBJ';

			$results = array('false' => true, 'message' => $msg, 'value' => $params['status'], 'title' => $title);
		}
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($results);
	}
	
	public function edit_stbj() {
		$this->form_validation->set_rules('id_stbj', 'ID STBJ', 'trim|required');
		$this->form_validation->set_rules('no_stbj', 'No Delivery Order', 'trim|required|min_length[4]|max_length[100]');
		$this->form_validation->set_rules('tanggal_stbj', 'Tanggal PO', 'trim|required');
		//$this->form_validation->set_rules('detail_box', 'ID STBJ', 'trim|required');
		$this->form_validation->set_rules('no_pi', 'No Delivery Order', 'trim|required');
		$this->form_validation->set_rules('remark', 'Remark', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);
			
			$this->log_activity->insert_activity('update', $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$id_stbj 		= $this->Anti_sql_injection($this->input->post('id_stbj', TRUE));
			$no_stbj 		= $this->Anti_sql_injection($this->input->post('no_stbj', TRUE));
			// $no_stbj 		= $this->sequence->get_no('stbj');
			$tanggal_stbj	= $this->Anti_sql_injection($this->input->post('tanggal_stbj', TRUE));
			//$detail_box		= $this->Anti_sql_injection($this->input->post('detail_box', TRUE));
			$no_pi 			= $this->Anti_sql_injection($this->input->post('no_pi', TRUE));
			$remark 		= $this->Anti_sql_injection($this->input->post('remark', TRUE));

			$data = array(
				'id_stbj'		=> $id_stbj,
				'no_stbj'		=> $no_stbj,
				'tanggal_stbj'	=> $tanggal_stbj,
				//'detail_box'	=> $detail_box,
				'no_pi'			=> $no_pi,
				'remark'		=> $remark
			);
			
			$get_stbj = $this->stbj_model->stbj_list($id_stbj);
			
			//if($get_stbj[0]['tanggal'] != $tanggal_stbj || $get_stbj[0]['detail_box'] != $detail_box || $get_stbj[0]['no_pi'] != $no_pi || $get_stbj[0]['remark'] != $remark){
				$result = $this->stbj_model->insert_stbj_edit($data);
				
				$msg = 'Berhasil merubah data STBJ packing';
				
				$result = array(
					'success'	=> true,
					'message'	=> $msg,
					'no_stbj'	=> $no_stbj
				);
				
				$this->log_activity->insert_activity('update', $msg. ' dengan No STBJ ' .$no_stbj);
				
			//}else{
				// $msg = 'Gagal merubah data STBJ. karena tidak ada data yang dirubah';
				
				// $result = array(
					// 'success' => false,
					// 'message' => $msg
				// );
				
				// $this->log_activity->insert_activity('update', $msg. ' dengan No STBJ ' .$no_stbj);
			//}
			
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}
	
	
	public function edit_detail_stbj() {
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data,true);

		$arrData = array();	
		$statSave = false;
		$this->db->delete('t_stbj_packing', array('id_stbj' => $params['id_stbj']));
		foreach ($params['listpacking'] as $k => $v) {
			if($v[0] != '') {
				
				$arrTemp = array(
					'id_stbj'		=> $params['id_stbj'],
					'id_packing'	=> $v[0]
				);

				$results = $this->stbj_model->insert_detail_packing($arrTemp);
				$results_update = $this->stbj_model->update_detail_box($arrTemp);
				
				if ($results > 0) $statSave = true;
				else $statSave = false;
			}
		}

		if($statSave == true) {
			$msg = 'Berhasil merubah STBJ packing';

			$result = array('success' => true, 'message' => $msg);
			$this->log_activity->insert_activity('update', $msg. ' dengan ID STBJ ' .$params['id_stbj']);
			
		}else {
			$msg = 'Gagal merubah STBJ packing';
			
			$result = array('success' => false, 'message' => $msg);
			$this->log_activity->insert_activity('update', $msg. ' dengan No STBJ ' .$params['id_stbj']);
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result);
	}
	
	public function packing_edit() {
		
		$data   	= file_get_contents("php://input");
        $params     = json_decode($data,true);
		
		$jml = sizeof($params['no_pack']);

		$add_packing = $this->packing_model->insert_packing($params);
		
        $msg = 'Berhasil mengubah data packing';

        $result = array(
            'success' => true,
            'message' => $msg
        );
		
		$this->log_activity->insert_activity('update', $msg. ' dengan No Packing ' .$params['no_pack']);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function packing_insert($id) {
		$data   = file_get_contents("php://input");
        $params     = json_decode($data,true);
		
		$insert = $this->packing_model->insert_packing($params,$id);
		
		if($insert['code'] == 1) {
			
			$dataPack = array (
				'id_po_quot' => $id,
				'id_schedule' => 0,
				'id_packing' => $insert['lastid'],
				'date_packing' => $params['date_pack'],
			);
			
			$this->packing_model->insert_po_packing($dataPack);
			
			$msg = 'Data packing berhasil disimpan';
			
			$results = array(
				'success' => true,
				'message' => $msg
			);
			
			$this->log_activity->insert_activity('insert', $msg. ' dengan No Packing ' .$params['no_pack']);
		}else{
			$msg = 'Data packing gagal disimpan ke database';
			
			$results = array(
				'success' => true,
				'message' => $msg
			);
			
			$this->log_activity->insert_activity('insert', $msg. ' dengan No Packing ' .$params['no_pack']);
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($results));
	}

	public function delete_stbj_detail() {
		
		$data 	= file_get_contents("php://input");
		$params = json_decode($data,true);
		
		$list = $this->stbj_model->delete_stbj_detail($params);
		
		$msg = 'Data detail STBJ order berhasil di hapus';
		
		$results = array(
			'status' => 'success',
			'message' => $msg
		);
		
		$this->log_activity->insert_activity('delete', $msg. ' dengan No Packing ' .$params['id_packing']);
		
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
        echo json_encode($results);
    }
	
	public function check_no_stbj(){
		$this->form_validation->set_rules('no_stbj', 'NoStbj', 'trim|required|min_length[4]|max_length[20]|is_unique[t_stbj.no_stbj]');
		$this->form_validation->set_message('is_unique', 'No STBJ Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'No STBJ Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}
}