<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Stbj_model extends CI_Model
{

	public function __construct()
	{

		parent::__construct();
	}

	public function lists($params = array())
	{
		$sql_all 	= 'CALL stbj_list_all(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query(
			$sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				''
			)
		);

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL stbj_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query(
			$sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			)
		);

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => count($result),
			'total' => $total_row,
		);

		return $return;
	}

	public function stbj_list($id_stbj)
	{
		$sql 	= 'CALL stbj_search_id(?)';

		$query 	= $this->db->query(
			$sql,
			array(
				$id_stbj
			)
		);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_stbj_material($id_stbj) {
		$sql 	= 'CALL stbj_material_search_id(?)';

		$query 	=  $this->db->query(
			$sql, array(
				$id_stbj
			)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function stbj_list_packing() {
		$sql 	= 'CALL stbj_packing_list_not_same()';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function stbj_packing_search_id($data) {
		$sql 	= 'CALL packing_list3(?)';

		$query 	=  $this->db->query($sql, array(
				$data['id_order']
			)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function stbj_do_search_id($data)
	{
		$sql 	= 'CALL stbj_do_list(?,?)';

		$query 	=  $this->db->query(
			$sql,
			array(
				$data['no_packing'],
				$data['id_po_quot']
			)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function delivery_packing_search_idstbj($id_stbj)
	{
		$sql 	= 'CALL stbj_detail_list(?)';

		$query 	=  $this->db->query(
			$sql,
			array(
				$id_stbj
			)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function insert_stbj($data)
	{
		$sql 	= 'CALL stbj_add(?,?,?,?,?)';

		$query 	= $this->db->query($sql, array(
			$data['no_stbj'],
			$data['tanggal_stbj'],
			$data['no_pi'],
			$data['remark'],
			$data['status']
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function insertMutasi($data) {
		$sql 	= 'CALL mutasi_add(?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id_stock_awal'],
				$data['id_stock_akhir'],
				$data['date_mutasi'],
				$data['amount_mutasi'],
				$data['type_mutasi']
			)
		);
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function insertCoaValue($data) {
		$sql 	= 'CALL coavalue_add4(?,?,?,?,?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id_coa'],
				$data['id_parent'],
				$data['stbj_date'],
				$data['id_valas'],
				$data['stbj_value'],
				$data['adjusment'],
				$data['type_cash'],
				$data['note'],
				$data['rate'],
				$data['bukti'],
				$data['id_coa_temp']
			)
		);
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function insert_detail_stbj_packing($data) {
		$sql 	= 'CALL stbj_packing_add(?,?)';

		$query 	= $this->db->query($sql, array(
			$data['id_stbj'],
			$data['id_packing']
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function insert_detail_packing($data)
	{
		$sql 	= 'CALL stbj_packing_add(?,?)';

		$query 	= $this->db->query(
			$sql,
			array(
				$data['id_stbj'],
				$data['id_packing']
			)
		);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function update_status_stbj($data) {
		$sql 	= 'UPDATE t_stbj SET status = ? WHERE id_stbj = ?';

		$query 	= $this->db->query($sql,array(
			$data['status'],
			$data['id_stbj']
		));

		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function update_detail_box($data)
	{
		$sql 	= 'CALL stbj_update_detailbox_stbj(?)';

		$query 	= $this->db->query($sql,array(
			$data['id_stbj']
		));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function insert_stbj_edit($data)
	{
		$sql 	= 'CALL stbj_update(?,?,?,?,?)';
		$query 	=  $this->db->query(
			$sql,
			array(
				$data['id_stbj'],
				$data['no_stbj'],
				$data['tanggal_stbj'],
				//$data['detail_box'],
				$data['no_pi'],
				$data['remark']
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function stbj_detail($id_stbj)
	{
		$sql 	= 'CALL stbj_detail_list(?)';

		$query 	= $this->db->query(
			$sql,
			array(
				$id_stbj
			)
		);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function delivery_order_detail_packing($id_stbj)
	{
		$sql 	= 'CALL stbj_detail_list(?)';

		$query 	= $this->db->query(
			$sql,
			array(
				$id_stbj
			)
		);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function detail_packing_list($id)
	{
		$sql 	= 'CALL packing_list(?)';

		$query 	= $this->db->query($sql, array(
			$id
		));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function delete_stbj_detail($data)
	{
		$sql 	= 'CALL stbj_packing_delete(?)';
		$query 	=  $this->db->query(
			$sql,
			array(
				$data['id_packing']
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function update_qtyMaterial($data)
	{
		$sql 	= 'UPDATE m_material SET qty = ? WHERE id = ?';
		$query 	=  $this->db->query(
			$sql,
			array(
				$data['qty'],
				$data['id_material']
			)
		);

		$result['code']	= $this->db->affected_rows();
		$result['status'] = 1;

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}
