<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kontra_Bon_model extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
	}

	public function count_kontra_bon($params) {
		$sql 	= 'CALL kb_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql, array(NULL, NULL, NULL, NULL, NULL));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		return $total['@total'];
	}

	public function list_kontra_bon($params) {
		$sql 	= 'CALL kb_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function get_purchase_order() {
		$sql 	= "SELECT e.*,h.name_eksternal FROM t_purchase_order e 
					  LEFT JOIN t_po_spb b ON e.id_po=b.id_po
					  LEFT JOIN t_btb_po c ON b.id_spb=c.id_spb
					  LEFT JOIN t_kontra_bon g ON c.`id_btb`=g.id_btb
					  LEFT JOIN t_eksternal h ON e.id_distributor=h.id
					  WHERE g.id_kontra_bon IS NULL";

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_distributor() {
		$sql 	= 'SELECT `id`, `kode_eksternal`, `name_eksternal` FROM `t_eksternal` WHERE `type_eksternal` = 2';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_purchase_order_by_id2($id_po,$id_btb) {
		$sql 	= '
			SELECT *,SUM(e.`qty_diterima`*d.`unit_price`)+(SUM(e.`qty_diterima`*d.`unit_price`)*a.ppn/100) AS btb_amount 
FROM t_purchase_order a
				LEFT JOIN t_po_spb d ON a.id_po=d.id_po
				LEFT JOIN t_btb_po e ON d.id_spb=e.id_spb
				LEFT JOIN t_btb fb ON e.`id_btb` = fb.`id_btb`
				LEFT JOIN t_spb zz ON d.id_spb=zz.id
				LEFT JOIN m_valas c ON a.id_valas = c.valas_id
				LEFT JOIN t_eksternal f ON a.id_distributor = f.id
			WHERE a.id_po = '.$id_po.' AND fb.`id_btb` = '.$id_btb.'
			GROUP BY a.id_po';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_purchase_order_by_id2_edit($id_po) {
		$sql 	= '
			SELECT *,SUM(e.`qty_diterima`*d.`unit_price`) AS btb_amount FROM t_purchase_order a
				LEFT JOIN t_po_spb d ON a.id_po=d.id_po
				LEFT JOIN t_btb_po e ON d.id_spb=e.id_spb
				LEFT JOIN t_btb fb ON e.`id_btb` = fb.`id_btb`
				LEFT JOIN t_spb zz ON d.id_spb=zz.id
				LEFT JOIN m_valas c ON a.id_valas = c.valas_id
				LEFT JOIN t_eksternal f ON a.id_distributor = f.id
			WHERE a.id_po = '.$id_po.'
			GROUP BY a.id_po';
		// WHERE a.id_po = '.$id_po.' AND fb.`id_btb` = '.$id_btb.'

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_purchase_order_by_id($id_po,$id_btb) {
		$sql 	= '
			SELECT *,SUM(e.`qty_diterima`*d.`unit_price`) AS btb_amount FROM t_purchase_order a 
				LEFT JOIN t_po_spb d ON a.id_po=d.id_po
				LEFT JOIN t_btb_po e ON d.id_spb=e.id_spb
				LEFT JOIN t_btb fb ON e.`id_btb` = fb.`id_btb`
				JOIN t_spb zz ON d.id_spb=zz.id
				JOIN m_valas c ON a.id_valas = c.valas_id 
				JOIN t_eksternal f ON a.id_distributor = f.id 
			WHERE a.id_po = '.$id_po.' 
			GROUP BY a.id_po';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function set_btb_po($id_po) 
	{
		$sql 	= 'SELECT * FROM t_purchase_order a 
					LEFT JOIN t_po_spb b ON a.id_po=b.id_po
					LEFT JOIN t_btb_po c ON b.id_spb=c.id_spb
					LEFT JOIN t_kontra_bon g ON c.`id_btb`=g.id_btb
					JOIN t_btb d ON c.id_btb=d.id_btb
				  WHERE a.`id_po`='.$id_po.' AND g.id_kontra_bon IS NULL
				  GROUP BY c.id_btb';

		$query 	= $this->db->query($sql);
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function set_btb_non_po() {
		$sql =
			'SELECT
				a.`id_btb_non` AS id_btb,
				a.`no_btb`
			FROM `t_btb_non` a
			LEFT JOIN `t_kontra_bon` b ON b.`id_btb` = a.`id_btb_non`
			WHERE b.`id_btb` IS NULL
			GROUP BY a.`id_btb_non`';

		$query 	= $this->db->query($sql);
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_coa_btb($id_btb) 
	{
		$sql 	= 'SELECT b.id_coa_value FROM t_btb a
					LEFT JOIN btb_coa b ON a.id_btb=b.id_btb
					LEFT JOIN t_coa_value c ON b.id_coa_value=c.id
					WHERE a.id_btb='.$id_btb.'
					ORDER BY c.id DESC';

		$query 	= $this->db->query($sql);
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function kb_get_po($id_kontrabon) {
		$sql 	= "SELECT
				a.*,
				b.`id_kontra_bon`, b.`id_purchase_order`, b.`no_kb`, b.`no_faktur`, b.`date_kb`, b.`coa`, b.`amount`, b.`jatuh_tempo`,
				g.`id_coa` AS dist_coa,
				SUM(c.qty_diterima*(c.unit_price-c.diskon)) AS total_amount_fix
			FROM t_purchase_order a 
			LEFT JOIN t_kontra_bon b ON a.id_po=b.id_purchase_order
			LEFT JOIN t_po_spb c ON a.id_po=c.id_po
			LEFT JOIN t_coa g ON a.`id_distributor` = g.`id_eksternal`
			WHERE b.id_kontra_bon = ".$id_kontrabon."
			GROUP BY b.id_kontra_bon";

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_coa($id_eksternal) {
		$sql = 'SELECT * FROM `t_coa` WHERE `id_eksternal` = '.$id_eksternal;

		$query = $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_kartu_hp($data)
	{
		$sql 	= 'CALL hp_add(?,?,?,?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['ref'],
			$data['source'],
			$data['keterangan'],
			$data['status'],
			$data['saldo'],
			$data['saldo_akhir'],
			$data['id_valas'],
			$data['type_kartu'],
			$data['id_master'],
			$data['type_master']
		));
		
		$return['code'] = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function add_kartu_hp2($data)
	{
		$sql 	= 'CALL hp_add_nn(?,?,?,?,?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['ref'],
			$data['source'],
			$data['keterangan'],
			$data['status'],
			$data['saldo'],
			$data['saldo_akhir'],
			$data['id_valas'],
			$data['type_kartu'],
			$data['id_master'],
			$data['type_master'],
			$data['jatuh_tempo']
		));
		
		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function kb_add_coa($data) {
		$sql 	= 'CALL kb_add_coa(?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_kontra_bon'],
			$data['coa_value']
		));
		
		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_id_coa($params)
	{
		$sql 	= 'CALL btb_get_id_coa2(?,?)';

		$query 	= $this->db->query($sql,array(
			$params[0]['id_po'],
			$params[0]['id_distributor']
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_coa_values($data) {
		$sql 	= 'CALL coavalue_add4(?,?,?,?,?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_coa'],
			$data['id_parent'],
			$data['date_kb'],
			$data['id_valas'],
			$data['value'],
			$data['adjusment'],
			$data['type_cash'],
			$data['note'],
			$data['rate'],
			$data['bukti'],
			$data['id_coa_temp']
		));
		
		$return['result'] = $this->db->affected_rows();
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$return['lastid'] = $lastid;

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_coa_values_cash($data) {
		$sql 	= 'CALL coavalue_add2(?,?,?,?,?,?,?,?)';
		
		$query 	= $this->db->query($sql,array(
			$data['payment_coa'],
			0,
			$data['delivery_date'],
			$data['id_valas'],
			$data['saldo'],
			0,
			1,
			$data['keterangan'],
		));

		$this->db->close();
		$this->db->initialize();
	}
	
	public function add_po_spb_coa($id_coa_value, $id_po_spb) {
		$sql 	= 'CALL hp_po_spb_coa_add(?,?)';

		$query 	= $this->db->query($sql, array(
			$id_coa_value,
			$id_po_spb
		));
		
		$return['code'] = $this->db->affected_rows();
		$return['array'] = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function hp_po_spb_coa_update($id_coa_value, $id_po_spb) {
		$sql 	= 'CALL hp_po_spb_coa_update(?,?)';

		$query 	= $this->db->query($sql, array(
			$id_coa_value,
			$id_po_spb
		));
		
		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function kb_add($data) {
		$sql 	= 'CALL kb_add(?,?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['id_po'],
			$data['no_kb'],
			$data['no_faktur'],
			$data['tgl_kb'],
			$data['id_coa_masuk'],
			$data['amount'],
			$data['jatuh_tempo'],
			$data['id_btb']
		));

		$return['result'] = $this->db->affected_rows();
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$return['lastid'] = $lastid;

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function kb_add_non_po($data) {
		$sql 	= 'CALL kb_add_non_po(?,?,?,?,?,?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['id_po'],
			$data['no_kb'],
			$data['no_faktur'],
			$data['tgl_kb'],
			$data['id_coa_masuk'],
			$data['amount'],
			$data['jatuh_tempo'],
			$data['rate'],
			$data['valas'],
			$data['id_distributor'],
			$data['type'],
			$data['id_btb']
		));

		$return['result'] = $this->db->affected_rows();
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$return['lastid'] = $lastid;

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_id_kontrabon($id) {
		$sql = 'SELECT * FROM `t_kontra_bon` WHERE `id_kontra_bon` = ?';
		
		$query = $this->db->query($sql,
			$id
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function get_list_btb_non() {
		$sql = 'SELECT id_btb_non AS id_btb, no_btb FROM `t_btb_non` WHERE `status` = 1';
		
		$query = $this->db->query($sql);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_btb_by_po($id_po) {
		$sql =
			'SELECT
				a.`id_btb`, a.`no_btb`
			FROM `t_btb` a
			LEFT JOIN `t_btb_po` b ON b.`id_btb` = a.`id_btb`
			LEFT JOIN `t_po_spb` c ON c.`id_spb` = b.`id_spb`
			WHERE c.`id_po` = ?
			GROUP BY a.`id_btb`';
		
		$query = $this->db->query($sql, array(
			$id_po
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_spb_btb_non($id_btb) {
		$sql =
			'SELECT
				a.*,
				b.`id_spb`,
				SUM(b.`unit_price` * b.`qty_diterima`) AS total_amount,
				c.`no_spb`, c.`tanggal_spb`, c.`nama_material`, c.`tanggal_diperlukan`,
				d.`name_eksternal`,
				e.`symbol`, e.`symbol_valas`
			FROM `t_btb_non` a
			LEFT JOIN `t_btb_spb_non` b ON b.`id_btb_non` = a.`id_btb_non`
			LEFT JOIN `t_spb` c ON c.`id` = b.`id_spb`
			LEFT JOIN `t_eksternal` d ON d.`id` = a.`id_cust`
			LEFT JOIN `m_valas` e ON e.`valas_id` = a.`valas_id`
			WHERE a.`id_btb_non` = ?';
		
		$query = $this->db->query($sql, array(
			$id_btb
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_po_btb($id_po) {
		$sql =
			'SELECT
				a.*,
				b.name_eksternal,
				c.`symbol`, c.`symbol_valas`
			FROM `t_purchase_order` a
			LEFT JOIN `t_eksternal` b ON b.`id` = a.`id_distributor`
			LEFT JOIN `m_valas` c ON c.`valas_id` = a.`id_valas`
			WHERE a.`id_po` = ?';
		
		$query = $this->db->query($sql, array(
			$id_po
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function get_type_kb($id) {
		$sql = 'SELECT `type`,`id_btb` FROM `t_kontra_bon` WHERE `id_kontra_bon` = ?';
		
		$query = $this->db->query($sql,array(
			$id
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function get_type_kb1($id, $id_btb) {
		$sql = 'SELECT `type`,`id_btb` FROM `t_kontra_bon` WHERE `id_kontra_bon` = ? AND id_btb = ?';
		
		$query = $this->db->query($sql,array(
			$id,
			$id_btb
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function get_type_kb2($id) {
		$sql = 'SELECT `type`,`id_btb` FROM `t_kontra_bon` WHERE `id_kontra_bon` = ?';
		
		$query = $this->db->query($sql,array(
			$id
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_kb_coavalue($id) {
		$sql = 'SELECT * FROM `t_kontra_bon_coa` WHERE `id_kontra_bon` = ?';
		
		$query = $this->db->query($sql,
			$id
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_kontra_bon($id) {
		$sql 	= 'CALL kb_search_id(?)';

		$query 	=  $this->db->query($sql,
			array($id)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_kontra_bon_new($id, $id_btb) {
		$sql =
			'SELECT
				a.`id_kontra_bon`, a.`no_kb`, a.`no_faktur`, a.`date_kb`, a.`coa`,
				b.`id_po`, b.`no_po`, b.`date_po`, b.`term_of_payment`,
				SUM(h.`unit_price` * g.`qty_diterima`) AS total_amount,
				b.`id_valas`, b.`rate`, b.`id_distributor`,
				c.`id`, c.`stock_code`, c.`stock_name`,
				d.`symbol`, d.`symbol_valas`,
				e.`name_eksternal`,
				f.`id_btb`, f.`no_btb`
			FROM `t_kontra_bon` a
			LEFT JOIN `t_purchase_order` b ON b.`id_po` = a.`id_purchase_order`
			LEFT JOIN `m_material` c ON b.`id_distributor` = c.`id`
			LEFT JOIN `m_valas` d ON b.`id_valas` = d.`valas_id`
			LEFT JOIN `t_eksternal` e ON  b.`id_distributor` = e.`id`
			LEFT JOIN `t_btb` f ON f.`id_btb` = a.`id_btb`
			LEFT JOIN `t_btb_po` g ON g.`id_btb` = f.`id_btb`
			LEFT JOIN `t_po_spb` h ON h.`id_spb` = g.`id_spb`
			WHERE a.id_kontra_bon = ? AND a.id_btb = ?';

		$query 	=  $this->db->query($sql,array(
			$id,
			$id_btb
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function edit_kontra_bon2($id,$id_btb) {
		$sql 	= 'SELECT
						a.`id_kontra_bon`, a.`no_kb`, a.`no_faktur`, a.`date_kb`, a.`coa`,
						b.`id_po`, b.`no_po`, b.`date_po`, b.`term_of_payment`, aa.`unit_price`*i.`qty_diterima` AS `total_amount`, b.`id_valas`, b.`rate`, b.`id_distributor`,
						c.`id`, c.`stock_code`, c.`stock_name`,
						d.`symbol`, d.`symbol_valas`,
						e.`name_eksternal`,j.`id_btb`,j.`no_btb`
					FROM t_kontra_bon a
					LEFT JOIN t_purchase_order b ON a.id_purchase_order = b.id_po
					LEFT JOIN m_material c ON b.id_distributor = c.id
					LEFT JOIN m_valas d ON b.id_valas = d.valas_id
					LEFT JOIN `t_eksternal`e ON  b.id_distributor =e.id
					LEFT JOIN t_po_spb aa ON b.`id_po`=aa.`id_po`
					LEFT JOIN t_btb_po i ON aa.`id_spb`=i.`id_spb`
					LEFT JOIN t_btb j ON i.`id_btb`=j.`id_btb`
					WHERE a.id_kontra_bon = ? AND a.id_btb = ?
					ORDER BY j.`id_btb` DESC';

		$query 	=  $this->db->query($sql,array(
			$id,
			$id_btb
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_kontra_bon_non_po($id) {
		$sql 	= 'CALL kb_non_po_search_id(?)';

		$query 	=  $this->db->query($sql,
			array($id)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_kontra_bon_non_po2($id,$id_btb) {
		$sql 	= 'SELECT
						a.`id_kontra_bon`, a.`id_purchase_order`, a.`no_kb`, a.`no_faktur`, a.`date_kb`, a.`coa`, a.`amount`, a.`jatuh_tempo`,a.`id_btb`,
						a.`rate`, a.`valas` AS id_valas, a.`id_distributor`, a.`type`,
						b.`coa_value`,
						d.`name_eksternal`, d.`eksternal_address`, d.`phone_1`, d.`phone_2`,
						e.`symbol`, e.`symbol_valas`
					FROM t_kontra_bon a
					LEFT JOIN `t_kontra_bon_coa` b ON a.`id_kontra_bon` = b.`id_kontra_bon`
					LEFT JOIN `t_coa_value` c ON b.`coa_value` = c.`id`
					LEFT JOIN `t_eksternal` d ON a.`id_distributor` = d.`id`
					LEFT JOIN `m_valas` e ON a.`valas` = e.`valas_id`
					LEFT JOIN `t_btb` f ON a.`id_btb` = f.`id_btb`
					WHERE a.id_kontra_bon = ? AND a.id_btb = ?
					ORDER BY a.`id_btb` DESC';

		$query 	=  $this->db->query($sql,array(
			$id,
			$id_btb
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function kb_update($data) {
		$sql 	= 'CALL kb_update3(?,?,?,?,?,?,?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql, array(
				$data['id_kontra_bon'],
				$data['id_purchase_order'],
				$data['no_kb'],
				$data['no_faktur'],
				$data['date_kb'],
				$data['coa'],
				$data['amount'],
				$data['jatuh_tempo'],
				$data['rate'],
				$data['valas'],
				$data['id_distributor'],
				$data['type'],
				$data['id_btb']
			)
		);
		
		$result['code']	= $this->db->affected_rows();
		$result['status'] = 1;

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function kb_update_non_po($data) {
		$sql 	= 'CALL kb_update_non_po(?,?,?,?,?,?,?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id_kontra_bon'],
				$data['id_purchase_order'],
				$data['no_kb'],
				$data['no_faktur'],
				$data['date_kb'],
				$data['coa'],
				$data['amount'],
				$data['jatuh_tempo'],
				$data['rate'],
				$data['valas'],
				$data['id_distributor'],
				$data['type'],
				$data['id_btb']
			)
		);

		$result['code']	= $this->db->affected_rows();
		$result['status'] = 1;

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function coavalue_update($data) {
		$sql 	= 'CALL coavalue_update2(?,?,?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql, array(
				$data['id'],
				$data['id_coa'],
				$data['id_parent'],
				$data['date_kb'],
				$data['id_valas'],
				$data['value'],
				$data['rate'],
				$data['bukti'],
				$data['id_coa_temp']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function coavalue_update_non_po($data) {
		$sql 	= 'CALL coavalue_update_non_po(?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql, array(
				$data['id'],
				$data['id_coa'],
				$data['id_valas'],
				$data['value'],
				$data['value_real'],
				$data['rate']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function hp_update_non_po($data) {
		$sql 	= 'CALL hp_update_non_po(?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['tanggal'],
				$data['saldo'],
				$data['saldo_akhir'],
				$data['id_valas'],
				$data['id_master'],
				$data['durasi']
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function kb_coa_delete($id) {
		$sql = 'DELETE FROM `t_kontra_bon_coa` WHERE `id_kontra_bon` = ?';

		$query = $this->db->query($sql,array(
			$id
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function kartu_hp_delete($id) {
		$sql = 'DELETE FROM `t_kartu_hp` WHERE `id_master` = ?';

		$query = $this->db->query($sql,array(
			$id
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function delete_po_spb_coa($id) {
		$sql = 'DELETE FROM `t_po_spb_coa` WHERE `id_coa_value` = ?';

		$query = $this->db->query($sql,array(
			$id
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function coavalue_delete($id) {
		$sql = 'CALL coavalue_delete(?)';

		$query = $this->db->query($sql,array(
			$id
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function kb_delete($data) {
		$sql 	= 'CALL kb_delete(?)';
		$query 	=  $this->db->query($sql,array(
			$data['id']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}