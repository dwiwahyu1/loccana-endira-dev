<style type="text/css">
	.dt-body-center {
		text-align: center;
	}
	.dt-body-right {
		text-align: right;
	}
	.x-hidden { 
		display: none;
	}
	
	/** BEGIN CSS **/
			@keyframes rotate-loading {
				0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
				100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
			}

			@-moz-keyframes rotate-loading {
				0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
				100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
			}

			@-webkit-keyframes rotate-loading {
				0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
				100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
			}

			@-o-keyframes rotate-loading {
				0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
				100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
			}

			@keyframes rotate-loading {
				0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
				100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
			}

			@-moz-keyframes rotate-loading {
				0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
				100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
			}

			@-webkit-keyframes rotate-loading {
				0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
				100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
			}

			@-o-keyframes rotate-loading {
				0%  {transform: rotate(0deg);-ms-transform: rotate(0deg); -webkit-transform: rotate(0deg); -o-transform: rotate(0deg); -moz-transform: rotate(0deg);}
				100% {transform: rotate(360deg);-ms-transform: rotate(360deg); -webkit-transform: rotate(360deg); -o-transform: rotate(360deg); -moz-transform: rotate(360deg);}
			}

			@keyframes loading-text-opacity {
				0%  {opacity: 0}
				20% {opacity: 0}
				50% {opacity: 1}
				100%{opacity: 0}
			}

			@-moz-keyframes loading-text-opacity {
				0%  {opacity: 0}
				20% {opacity: 0}
				50% {opacity: 1}
				100%{opacity: 0}
			}

			@-webkit-keyframes loading-text-opacity {
				0%  {opacity: 0}
				20% {opacity: 0}
				50% {opacity: 1}
				100%{opacity: 0}
			}

			@-o-keyframes loading-text-opacity {
				0%  {opacity: 0}
				20% {opacity: 0}
				50% {opacity: 1}
				100%{opacity: 0}
			}
			.loading-container,
			.loading {
				height: 45px;
				position: relative;
				width: 45px;
				margin-left: -45px;
				border-radius: 100%;
			}


			.loading-container { margin: 0px auto }

			.loading {
				border: 2px solid transparent;
				border-color: transparent #f2f2f2 transparent #FFF;
				-moz-animation: rotate-loading 1.5s linear 0s infinite normal;
				-moz-transform-origin: 50% 50%;
				-o-animation: rotate-loading 1.5s linear 0s infinite normal;
				-o-transform-origin: 50% 50%;
				-webkit-animation: rotate-loading 1.5s linear 0s infinite normal;
				-webkit-transform-origin: 50% 50%;
				animation: rotate-loading 1.5s linear 0s infinite normal;
				transform-origin: 50% 50%;
			}

			.loading-container:hover .loading {
				border-color: transparent #E45635 transparent #E45635;
			}
			.loading-container:hover .loading,
			.loading-container .loading {
				-webkit-transition: all 0.5s ease-in-out;
				-moz-transition: all 0.5s ease-in-out;
				-ms-transition: all 0.5s ease-in-out;
				-o-transition: all 0.5s ease-in-out;
				transition: all 0.5s ease-in-out;
			}

			#loading-text {
				-moz-animation: loading-text-opacity 2s linear 0s infinite normal;
				-o-animation: loading-text-opacity 2s linear 0s infinite normal;
				-webkit-animation: loading-text-opacity 2s linear 0s infinite normal;
				animation: loading-text-opacity 2s linear 0s infinite normal;
				color: #5c5c5c;
				font-family: "Helvetica Neue, "Helvetica", ""arial";
				font-size: 10px;
				font-weight: bold;
				margin-top: 15px;
				margin-left: 13px;
				opacity: 0;
				position: absolute;
				text-align: center;
				top: 0;
				width: 100px;
			}
</style>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title" id="title_menu">Kontra Bon</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<table id="list_kontra_bon" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="text-align: center; width: 5%;">No</th>
							<th>No Kontra Bon</th>
							<th>No Faktur</th>
							<th>Tanggal Kontra Bon</th>
							<th>Supplier</th>
							<th>Jatuh Tempo</th>
							<th>Jumlah</th>
							<th style="width: 12%;"></th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-dialog-scrollable" style="width: 60%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	var tableKontraBon;
	$(document).ready(function(){
		get_list_kontra_bon();
	});

	function get_list_kontra_bon() {
		tableKontraBon = $("#list_kontra_bon").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'kontra_bon/list_kontra_bon/';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false,
			"destroy": true,
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">Bfrtip',
			"buttons" : [{
				extend: 'excelHtml5',
				text: '<i class="fa fa-file-excel-o" title="Excel"></i>',
				filename : 'Kontra Bon',
				header : true,
				action: function (e, dt, button, node, config) {
					$('.buttons-excel').attr('disabled', 'disabled');
					var excelButtonConfig 				= $.fn.DataTable.ext.buttons.excelHtml5;
					excelButtonConfig.filename 			= 'Kontra Bon';
					excelButtonConfig.exportOptions 	= {
						columns: [0, 1, 2, 3, 4, 5, 6]
					};
					
					$.ajax({
						type: "POST",
						url: "<?php echo base_url().'kontra_bon/list_kontra_bon?length=-1';?>",
						dataType: 'json',
						contentType: 'application/json; charset=utf-8',
						success: function(response) {
							tableKontraBon.api().rows().remove();
							tableKontraBon.api().rows.add(response.data);
							excelButtonConfig.action(e, dt, button, excelButtonConfig);
							$('.buttons-excel').removeAttr('disabled');
						}
					});
				}
			}, {
				extend 			: 'pdfHtml5',
				text 			: '<i class="fa fa-file-pdf-o" title="Pdf"></i>',
				action : function(e, dt, button, node, config) {
					$('.buttons-pdf').attr('disabled', 'disabled');
					var pdfButtonConfig 			= $.fn.DataTable.ext.buttons.pdfHtml5;
					pdfButtonConfig.filename 		= 'Kontra Bon';
					pdfButtonConfig.title 			= 'Kontra Bon';
					pdfButtonConfig.orientation 	= 'landscape';
					pdfButtonConfig.pageSize 		= 'A4';
					pdfButtonConfig.exportOptions 	= {
						columns: [0, 1, 2, 3, 4, 5, 6]
					};
					$.ajax({
						type: "POST",
						url: "<?php echo base_url().'kontra_bon/list_kontra_bon?length=-1';?>",
						dataType: 'json',
						contentType: 'application/json; charset=utf-8',
						success: function(response) {
							tableKontraBon.api().rows().remove();
							tableKontraBon.api().rows.add(response.data);
							pdfButtonConfig.action(e, dt, button, pdfButtonConfig);
							$('.buttons-pdf').removeAttr('disabled');
						}
					});
				}
			}],
			"initComplete": function(){
				$("div.toolbar").prepend(
					'<div class="btn-group pull-left">'+
						'<a class="btn btn-primary" onClick="add_kontra_bon()"><i class="fa fa-plus"></i> Tambah Kontra Bon</a>'+
					'</div>');
			},
			"columnDefs": [{
				targets: [0],
				className: 'dt-body-center'
			},{
				targets: [6],
				className: 'dt-body-right'
			}]
		});
	}

	function add_kontra_bon(){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('kontra_bon/add_kontra_bon');?>');
		$('#panel-modal  .panel-title').html('<i class="fa fa-plus"></i> Tambah Kontra Bon');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function edit_kontra_bon(id){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('kontra_bon/edit_kontra_bon/');?>'+"/"+id);
		$('#panel-modal  .panel-title').html('<i class="fa fa-edit"></i> Edit Kontra Bon');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function detail_kontra_bon(id) {
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('kontra_bon/detail_kontra_bon/');?>'+"/"+id);
		$('#panel-modal  .panel-title').html('<i class="fa fa-edit"></i> Detail Kontra Bon');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function delete_kontra_bon(id) {
		swal({
			title: 'Yakin akan Menghapus ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Ya!",
			showCancelButton: true,
			cancelButtonClass: "btn-default",
			cancelButtonText: "Tidak!"
		}).then(function () {
			var datapost = {'id' : id};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>kontra_bon/delete_kontra_bon",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
				   swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						window.location.href = "<?php echo base_url('kontra_bon');?>";
					})

					if (response.status != "success") swal("Failed!", response.message, "error");
				}
			});
		})
	}
</script>