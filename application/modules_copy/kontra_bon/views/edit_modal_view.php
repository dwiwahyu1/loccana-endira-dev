<form class="form-horizontal form-label-left" id="add_form" role="form" action="<?php echo base_url('kontra_bon/save_edit_kontra_bon');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_kontra_bon">No Kontra Bon <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="no_kontra_bon" name="no_kontra_bon" class="form-control" placeholder="No Kontra Bon" value="<?php if(isset($kontra_bon[0]['no_kb'])){ echo $kontra_bon[0]['no_kb']; }?>" autocomplete="off" readonly>
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_faktur">No Faktur <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="no_faktur" name="no_faktur" class="form-control" placeholder="No Faktur" value="<?php if(isset($kontra_bon[0]['no_faktur'])){ echo $kontra_bon[0]['no_faktur']; }?>" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tgl_kontra_bon">Tanggal Kontra Bon <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="tgl_kontra_bon" name="tgl_kontra_bon" class="form-control" placeholder="Tanggal Kontra Bon" value="<?php if(isset($kontra_bon[0]['date_kb'])){
				echo date('d-M-Y', strtotime($kontra_bon[0]['date_kb']));
			}?>" autocomplete="off" required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_invoice">No PO <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="no_invoice" name="no_invoice" style="width: 100%" required>
				<?php if(isset($type_kb) && $type_kb == 1) {
						echo '<option value="non_po" selected>Non PO</option>';
				}else {
					echo '<option value="">... Pilih PO ...</option>';
					echo '<option value="'.$get_po_kb[0]['id_po'].'" selected>'.$get_po_kb[0]['no_po'].' - '.$get_po_kb[0]['name_eksternal'].'</option>';
					foreach ($po as $pk) {
						echo '<option value="'.$pk['id_po'].'">'.$pk['no_po'].' - '.$pk['name_eksternal'].'</option>';
					}
				} ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="btb">BTB</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="btb" name="btb" style="width: 100%">
				<option value="">... Pilih BTB ...</option>
				<?php foreach ($list_btb as $lbk) {
					if(isset($kontra_bon[0]['id_btb']) && $kontra_bon[0]['id_btb'] == $lbk['id_btb']) $strSelected = 'selected';
					else $strSelected = '';
					echo '<option value="'.$lbk['id_btb'].'" '.$strSelected.'>'.$lbk['no_btb'].'</option>';
				} ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_coa_masuk">Coa <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control select-control" id="id_coa_masuk" name="id_coa_masuk" style="width: 100%" required>
			<?php foreach ($coa_all as $ck) {
				if(isset($kontra_bon[0]['coa']) && $kontra_bon[0]['coa'] == $ck['id_coa']) $strSelected = 'selected';
				else $strSelected = '';
				echo '<option value="'.$ck['id_coa'].'" '.$strSelected.'>'.$ck['coa'].' - '.$ck['keterangan'].'</option>';
			} ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_supplier">Nama Supplier</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="nama_supplier" name="nama_supplier">
				<option value="">... Pilih Supplier ...</option>
		<?php if(isset($distributor)) {
			foreach ($distributor as $dk) { ?>
				<option value="<?php echo $dk['id']; ?>" <?php if(isset($kontra_bon[0]['id_distributor']) && $dk['id'] == $kontra_bon[0]['id_distributor']) echo "selected"; ?>><?php echo strtoupper($dk['name_eksternal']); ?></option>
		<?php
			}
		} ?>
			</select>
			<input type="hidden" id="str_nama_supplier" name="str_nama_supplier" value="<?php if(isset($kontra_bon[0]['name_eksternal'])) echo $kontra_bon[0]['name_eksternal']; ?>" autocomplete="off">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="valas">Valas</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="valas" name="valas">
				<option value="" selected>... Pilih Valas ...</option>
		<?php if(isset($valas)) {
			foreach ($valas as $vk) {
				if(isset($kontra_bon[0]['valas']) && $kontra_bon[0]['valas'] == $vk['valas_id']) $strSelected = 'selected';
				else $strSelected = '';
				echo '<option value="'.$vk['valas_id'].'" '.$strSelected.'>'.$vk['nama_valas'].'</option>';
			}
		} ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="rate">Rate</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="number" id="rate" name="rate" min="1" class="form-control" step="1" value="<?php
				if(isset($kontra_bon[0]['rate'])) echo number_format($kontra_bon[0]['rate'], 0);
			?>" placeholder="Rate" value="0" autocomplete="off" <?php if(isset($type_kb) && $type_kb != 1) echo "readonly"; ?>>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jatuh_tempo">Jatuh Tempo</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="jatuh_tempo" name="jatuh_tempo" class="form-control" placeholder="Jatuh Tempo" value="<?php
				if(isset($kontra_bon[0]['jatuh_tempo'])) echo $kontra_bon[0]['jatuh_tempo'];
			?>" autocomplete="off">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jumlah">Jumlah</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="jumlah" name="jumlah" class="form-control" placeholder="Jumlah" value="<?php
			if(isset($get_po_kb[0]['symbol'])) echo $get_po_kb[0]['symbol'].' ';
			if(isset($kontra_bon[0]['amount'])) {
					echo number_format(floatval($kontra_bon[0]['amount']), 0, ',', '.');
			} ?>" autocomplete="off">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Update Kontra Bon</button>
			<input type="hidden" id="id_kontra_bon" name="id_kontra_bon" value="<?php if(isset($kontra_bon[0]['id_kontra_bon'])) echo $kontra_bon[0]['id_kontra_bon']; ?>">
			<input type="hidden" id="type_kb" name="type_kb" value="<?php if(isset($type_kb)) echo $type_kb; ?>">
		</div>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	var setFirst	= true;
	var idPO 		= '<?php if(isset($kontra_bon[0]['id_purchase_order'])) echo $kontra_bon[0]['id_purchase_order']; ?>';
	var idBTB 		= '<?php if(isset($kontra_bon[0]['id_btb'])) echo $kontra_bon[0]['id_btb']; ?>';
	
	$(document).ready(function() {
		$('#tgl_kontra_bon').datepicker({
			format: "d-M-yyyy",
			autoclose: true,
			todayHighlight: true
		});
		
		$('#jatuh_tempo').datepicker({
			format: "d-M-yyyy",
			autoclose: true,
			todayHighlight: true
		});

		$('#no_invoice').select2();
		$('#id_coa_masuk').select2();
		$('#valas').select2();
		$('#nama_supplier').select2();
		$('#btb').select2();
		
		
	<?php if(isset($type_kb) && $type_kb == 0) { ?>
		$('#nama_supplier').select2('readonly', true);
		$('#valas').select2('readonly', true);
		$('#rate').prop('readonly', true);
		$('#jumlah').prop('readonly', true);
	<?php } ?>

		$('#no_invoice').on('change', function() {
			$('#nama_supplier').select2('readonly', true);
			$('#valas').select2('readonly', true);
			$('#rate').prop('readonly', true);
			$('#jumlah').prop('readonly', true);

			if(this.value != '' && this.value != 'non_po') {
				$('#nama_supplier').val('').trigger('change');
				$('#btb').val('').trigger('change');
				$('#valas').val('').trigger('change');
				$('#rate').val('1');
				$('#jumlah').val('');

				setBTBPO(this.value);
			}else if(this.value == 'non_po') {
				$('#btb').find('option').not(':first').remove();
				
				$('#nama_supplier').val('').trigger('change');
				$('#nama_supplier').select2('readonly', false);
				$('#btb').val('').trigger('change');
				$('#valas').val('').trigger('change');
				$('#valas').select2('readonly', false);
				$('#rate').val('1');
				$('#rate').prop('readonly', false);
				$('#jumlah').val('');
				$('#jumlah').prop('readonly', false);
			}else {
				$('#btb').find('option').not(':first').remove();

				$('#nama_supplier').val('').trigger('change');
				$('#btb').val('').trigger('change');
				$('#jatuh_tempo').val('');
				$('#valas').val('').trigger('change');
				$('#rate').val('1');
				$('#jumlah').val('');
			}
		});

		$('#nama_supplier').on('change', function() {
			if(this.value != '' && this.value != null) $('#str_nama_supplier').val($(this).find('option:selected').text());
			else $('#str_nama_supplier').val('');
		});

		$('#btb').on('change', function() {
			var id_po = $('#no_invoice').val();
			
			if(this.value != '' && id_po != 'non_po') {
				$('#nama_supplier').val('').trigger('change');
				$('#jatuh_tempo').val('');
				$('#valas').val('').trigger('change');
				$('#rate').val('1');
				$('#jumlah').val('');

				setInvoice(id_po, this.value);
			}else {
				$('#nama_supplier').val('').trigger('change');
				$('#jatuh_tempo').val('');
				$('#valas').val('').trigger('change');
				$('#rate').val('1');
				$('#jumlah').val('');

				setInvoiceNoPO(this.value);
			}
		});

		$('#btb').val(idBTB).trigger('change');
		$('#jatuh_tempo').val('<?php echo $kontra_bon[0]['jatuh_tempo']; ?>');
	});

	function getInvoice() {
		$('#no_invoice').attr('disabled', 'disabled');
		$.ajax({
			type: "GET",
			url: "<?php echo base_url('kontra_bon/get_invoice');?>",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					for (var i = 0; i < r.length; i++) {
						var newOption = new Option(r[i].no_po, r[i].id_po, false, false);
						$('#no_invoice').append(newOption);
					}
					$('#no_invoice').removeAttr('disabled');
				}
			}
		});
	}

	function setInvoice(id_po, id_btb) {
		var datapost = {"id_po" : id_po, "id_btb" : id_btb};

		$.ajax({
			type: "POST",
			url: "<?php echo base_url('kontra_bon/set_invoice');?>", 
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					$('#nama_supplier').val(r[0].id_distributor).trigger('change');
					$('#str_nama_supplier').val(r[0]['name_eksternal']);
					$('#valas').val(r[0].valas_id).trigger('change');
					$('#rate').val(r[0].rate);
					$('#jumlah').val(r[0].total_amount);
				}else {
					$('#nama_supplier').val('').trigger('change');
					$('#str_nama_supplier').val('');
					$('#valas').val('').trigger('change');
					$('#rate').val('1');
					$('#jatuh_tempo').val();
					$('#jumlah').val();
				}
			}
		});
	}

	function setInvoiceNoPO(id_btb) {
		var datapost = {"id_btb" : id_btb};

		$.ajax({
			type: "POST",
			url: "<?php echo base_url('kontra_bon/set_invoice_non_po');?>", 
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					$('#nama_supplier').val(r[0].id_cust).trigger('change');
					$('#str_nama_supplier').val(r[0]['name_eksternal']);
					$('#valas').val(r[0].valas_id).trigger('change');
					$('#rate').val(r[0].rate);
					$('#jumlah').val(r[0].total_amount);
				}else {
					$('#nama_supplier').val('').trigger('change');
					$('#str_nama_supplier').val('');
					$('#valas').val('').trigger('change');
					$('#rate').val('1');
					$('#jatuh_tempo').val();
					$('#jumlah').val();
				}
			}
		});
	}

	function setBTBPO(id_po) {
		var datapost = {"id_po" : id_po};

		$.ajax({
			type: "POST",
			url: "<?php echo base_url('kontra_bon/set_btb_po');?>",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				var data_btb = response.data;
				$('#btb').find('option').not(':first').remove();

				if(response.status == true) {
					for(var i=0; i < data_btb.length;i++)	{
						var id_btb 		= data_btb[i]['id_btb'];
						var no_btb 		= data_btb[i]['no_btb'];

						var newOption = new Option(no_btb, id_btb, false, false);
						$('#btb').append(newOption);
					}
				}else $('#btb').val('').trigger('change');
			}
		});
	}

	$('#add_form').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		var periode_exists = $('#existPeriode').val();
		var url = $(this).attr('action');
		e.preventDefault();

		var formData = new FormData(this);

		$.ajax({
			type:'POST',
			url: $(this).attr('action'),
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						window.location.href = "<?php echo base_url('kontra_bon');?>";
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Tambah Kontra Bon");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah Kontra Bon");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}));
</script>