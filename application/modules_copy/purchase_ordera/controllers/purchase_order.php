<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Purchase_Order extends MX_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('purchase_order/purchase_order_model');
		$this->load->library('excel');
		$this->load->library('log_activity');
		$this->load->library('sequence');
		$this->load->library('numbering');
	}

	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'purchase_order/views/index');
	}

	function list_po() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;
		$order_fields = array('no_po', 'date_po', 'delivery_date', 'id_distributor', 'term_of_payment', 'total_amount', 'status');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->purchase_order_model->list_po($params);
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		
		/*echo "<pre>";
		print_r($list);
		echo "</pre>";
		die;*/

		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$status_akses =
				'<div class="btn-group">'.
					'<a class="btn btn-info btn-sm" title="Details" onClick="details_po(\'' . $v['id_po'] . '\')">'.
						'<i class="fa fa-search"></i>'.
					'</a>'.
				'</div>';
			$strStat = "";
			$checkBarang = $this->purchase_order_model->check_approve_po_spb($v['id_po']);
			$checkBarang_accepted = $this->purchase_order_model->check_approve_po_spb_accepted($v['id_po']);
			$checkBarang_rejected = $this->purchase_order_model->check_approve_po_spb_rejected($v['id_po']);
			
			if($checkBarang['total'] > 0 && $checkBarang_accepted['total'] == 0 && $checkBarang_rejected['total'] == 0) {
				$strStat = "Preparing";
				$status_akses .=
					'<div class="btn-group">'.
						'<a class="btn btn-warning btn-sm" title="Edit" onClick="edit_po(\'' . $v['id_po'] . '\')">'.
							'<i class="fa fa-edit"></i>'.
						'</a>'.
					'</div>'.

					'<div class="btn-group">'.
						'<a class="btn btn-danger btn-sm" title="Delete" onClick="delete_po(\'' . $v['id_po'] . '\')">'.
							'<i class="fa fa-trash"></i>'.
						'</a>'.
					'</div>';
			}else if ($checkBarang_accepted['total'] > 0 && $checkBarang_rejected['total'] == 0) {
				if($v['status'] == 3) $strStat = "<span style='color:green'>Close</span>";
				else $strStat = "<span style='color:green'>Done</span>";
				
				if($v['status'] == 0) {
					$status_akses .=
						'<div class="btn-group">'.
							'<a class="btn btn-primary btn-sm" title="Close PO" onClick="close_po(\'' . $v['id_po'] . '\')">'.
								'<i class="fa fa-close"></i>'.
							'</a>'.
						'</div>';
				}
			}else if ($checkBarang_accepted['total'] > 0 && $checkBarang_rejected['total'] > 0) {
				$strStat = "<span style='color:red'>Need Respond</span>";
				$status_akses .=
					'<div class="btn-group">'.
						'<a class="btn btn-warning btn-sm" title="Edit" onClick="edit_po(\'' . $v['id_po'] . '\')">'.
							'<i class="fa fa-edit"></i>'.
						'</a>'.
					'</div>' .

					'<div class="btn-group">'.
						'<a class="btn btn-danger btn-sm" title="Delete" onClick="delete_po(\'' . $v['id_po'] . '\')">' .
							'<i class="fa fa-trash"></i>'.
						'</a>'.
					'</div>';
			}else {
				if ($checkBarang['total'] == 0 && $checkBarang_rejected['total'] > 0) $strStat = "<span style='color:red'>Need Respond</span>";
				else $strStat = "Preparing";
				$status_akses .=
					'<div class="btn-group">'.
						'<a class="btn btn-warning btn-sm" title="Edit" onClick="edit_po(\'' . $v['id_po'] . '\')">'.
							'<i class="fa fa-edit"></i>'.
						'</a>'.
					'</div>'.

					'<div class="btn-group">'.
						'<a class="btn btn-danger title="Delete" onClick="delete_po(\'' . $v['id_po'] . '\')">'.
							'<i class="fa fa-trash"></i>'.
						'</a>'.
					'</div>';
			}

			if($checkBarang['total'] > 0) {
				$status_akses .=
					'<div class="btn-group">'.
						'<a class="btn btn-success btn-sm" title="Approval" onClick="approve_po(\'' . $v['id_po'] . '\')">'.
							'<i class="fa fa-check"></i>'.
						'</a>'.
					'</div>';
			}
			
			if($v['valas_id']==1) $coma = 2;
			else $coma = 5;

			array_push($data, array(
				$v['no_po'],
				date('d M Y', strtotime($v['date_po'])),
				$v['name_eksternal'],
				$v['symbol_valas'] . " " . number_format($v['total_amount'], $coma, ",", "."),
				date('d M Y', strtotime($v['delivery_date'])),
				ucwords($v['term_of_payment']),
				$strStat,
				$status_akses
			));
		}

		$result["data"] = $data;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_po()
	{
		$result_dist = $this->purchase_order_model->distributor();
		$result_coa = $this->purchase_order_model->coa();
		$coa = $this->purchase_order_model->coa_list();
		$valas = $this->purchase_order_model->valas_list();
		$no_po_temp = $this->sequence->get_max_no('purchase_order');
		$data = array(
			'distributor' => $result_dist,
			'coa' => $result_coa,
			'coa_list' => $coa,
			'valas_list' => $valas,
			'no_po_temp' =>$no_po_temp
		);

		$this->load->view('add_modal_view', $data);
	}

	public function add_item()
	{
		$result_spb = $this->purchase_order_model->spb();

		$data = array(
			'spb' => $result_spb
		);

		$this->load->view('add_modal_item_view', $data);
	}

	public function get_spb() {
		$data		= file_get_contents("php://input");
		$params		= json_decode($data, true);

		$list		= $this->purchase_order_model->get_spb_id($params['id_spb']);
		$result_uom	= $this->purchase_order_model->uom();

		$data = array();
		$tempObj = new stdClass();
		$uom = "";

		if (sizeof($params['tempBarang']) > 0) {
			foreach ($params['tempBarang'] as $p => $pk) {
				for ($i = 0; $i <= sizeof($list); $i++) {
					if (!empty($list[$i])) {
						if ($list[$i]['no_spb'] == $pk[1] && $list[$i]['stock_name'] == $pk[2]) {
							unset($list[$i]);
						}
					}
				}
			}
		}

		$i = 0;
		foreach ($list as $k => $v) {
			foreach ($result_uom as $k_o => $v_o) {
				if ($v_o['id_uom'] == $v['id_uom']) $uom = $v_o['uom_symbol'];
			}
			$strUnitPrice =
				'<input type="text" class="form-control " id="unit_price'.$i.'" name="unit_price[]" style="height:25px; width: 100px;" value="0">';
			$strAmount =
				'<input type="text" class="form-control" id="amount'.$i.'" name="amount[]" style="height:25px; width: 100px;" value="0" readonly>';
			$strDiskon =
				'<input type="text" class="form-control" max="100" id="diskon'.$i.'" name="diskon[]" style="height:25px; width: 100px;" value="0">';
			$strRemark =
				'<input type="text" class="form-control" id="remark'.$i.'" name="remark[]" style="height:25px; width: 100px;">';
			$strOption =
				'<div class="checkbox">'.
					'<input id="option'.$i.'" name="option[]" type="checkbox" value="'.$i.'">'.
					'<label for="option['.$i.']"></label>'.
				'</div>';

			array_push($data, array(
				$v['id'],
				$v['no_spb'],
				$v['stock_name'],
				$v['uom_name'],
				number_format($v['qty'], 2, '.', ''),
				$strUnitPrice,
				$strDiskon,
				$strAmount,
				$strRemark,
				$strOption
			));
			$i++;
		}

		$res = array('status' => 'success', 'data' => $data);

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function save_po() {
		// $no_po 			= $this->sequence->get_no('purchase_order');
		// $no_po 			= $this->sequence->get_max_no('purchase_order');
		$no_po 			= $this->Anti_sql_injection($this->input->post('no_po', TRUE));
		$tglpo			= $this->Anti_sql_injection($this->input->post('tgl_po', TRUE));
		$dist 			= $this->Anti_sql_injection($this->input->post('dist', TRUE));
		$tgldelivery 	= $this->Anti_sql_injection($this->input->post('tgl_delivery', TRUE));
		$term_of_pay 	= ucwords($this->Anti_sql_injection($this->input->post('term_of_pay', TRUE)));
		$amount_barang 	= $this->Anti_sql_injection($this->input->post('amount_barang', TRUE));
		$valas 			= $this->Anti_sql_injection($this->input->post('valas', TRUE));
		$rate 			= $this->Anti_sql_injection($this->input->post('rate', TRUE));
		$ppn 			= $this->Anti_sql_injection($this->input->post('ppn', TRUE));

		$temp_tgl_po		= explode("/", $tglpo);
		$tgl_po 			= date('Y-m-d', strtotime($temp_tgl_po[2] . '-' . $temp_tgl_po[1] . '-' . $temp_tgl_po[0]));
		$temp_tgl_delivery	= explode("/", $tgldelivery);
		$tgl_delivery 		= date('Y-m-d', strtotime($temp_tgl_delivery[2] . '-' . $temp_tgl_delivery[1] . '-' . $temp_tgl_delivery[0]));

		if ($term_of_pay == 'CASH') {
			$coa_s = $this->Anti_sql_injection($this->input->post('id_coa', TRUE));
		} else {
			$coa_s = 0;
		}

		$data = array(
			'no_po'			=> $no_po,
			'tgl_po'		=> $tgl_po,
			'dist'			=> $dist,
			'tgl_delivery'	=> $tgl_delivery,
			'term_of_pay'	=> $term_of_pay,
			'amount'		=> $amount_barang,
			'pay_coa'		=> $coa_s,
			'valas'			=> $valas,
			'rate'			=> $rate,
			'ppn'				=> $ppn
		);
		$result = $this->purchase_order_model->add_po($data);
		if ($result['result'] > 0) {
			$this->sequence->save_max_no('purchase_order');

			$this->log_activity->insert_activity('insert', 'Insert Purchase Order');
			$result = array('success' => true, 'message' => 'Berhasil menambahkan Purchase Order ke database', 'lastid' => $result['lastid']);
		} else {
			$this->log_activity->insert_activity('insert', 'Gagal Insert Purchase Order');
			$result = array('success' => false, 'message' => 'Gagal menambahkan Purchase Order ke database');
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function save_barang_po() {
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data, true);
		$arrData	= array();

		foreach ($params['listbarang'] as $k => $v) {
			$arrTemp = array(
				'id_po'			=> $params['id_po'],
				'id_spb'		=> $v[0],
				'unit_price'	=> preg_replace("/[^0-9. ]/", '', $v[5]),
				'diskon'		=> preg_replace("/[^0-9. ]/", '', $v[6]),
				'price'			=> preg_replace("/[^0-9. ]/", '', $v[7]),
				'remark'		=> $v[8],
				'status'		=> '0'
			);
			
			$result = $this->purchase_order_model->add_barang($arrTemp);
			$this->purchase_order_model->update_status_spb($arrTemp);
		}

		if ($result > 0) {
			$this->log_activity->insert_activity('insert', 'Insert Barang Purchase Order');
			$result = array('success' => true, 'message' => 'Berhasil menambahkan Purchase Order ke database');
		} else {
			$this->log_activity->insert_activity('insert', 'Gagal Insert Barang Purchase Order');
			$result = array('success' => false, 'message' => 'Gagal menambahkan Purchase Order ke database');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result);
	}

	public function edit_po($id) {
		$result_po = $this->purchase_order_model->edit_po($id);
		$result_dist = $this->purchase_order_model->distributor();
		$result_coa = $this->purchase_order_model->coa();
		$valas_list = $this->purchase_order_model->valas_list();

		$result_po[0]['date_po'] = date('d/M/Y', strtotime($result_po[0]['date_po']));
		$result_po[0]['delivery_date'] = date('d/M/Y', strtotime($result_po[0]['delivery_date']));

		$data = array(
			'po' => $result_po,
			'distributor' => $result_dist,
			'coa' => $result_coa,
			'valas_list' => $valas_list
		);
		
		$this->load->view('edit_modal_view', $data);
	}

	public function get_po_spb($id_po) {
		$list = $this->purchase_order_model->get_po_spb($id_po);
		$listuom = $this->purchase_order_model->uom();

		$data = array();
		$i = 0;
		$tempTotal = 0;
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list as $k => $v) {
			$amount = (float) $v['price'] * (float) $v['qty'];
			if ($v['diskon'] > 0 && $v['diskon'] <= 100) {
				$diskon = $v['diskon'] / 100;
				$amountVdiskon = $amount * $diskon;
				$amount = $amount - $amountVdiskon;
			} else {
				$amount = $amount - $v['diskon'];
			}
			$tempTotal = $tempTotal + $amount;

			$strUnitPrice =
				'<input type="text" class="form-control" id="edit_unit_price'.$i.'" name="edit_unit_price[]" style="height:25px; width: 100px;" value="'.$v['price'].'">';
			$strAmount =
				'<input type="text" class="form-control" id="edit_amount'.$i.'" name="edit_amount[]" style="height:25px; width: 100px;" value="'.$amount.'" readonly>';

			$strDiskon =
				'<input type="text" class="form-control" id="edit_diskon'.$i.'" name="edit_diskon[]" style="height:25px; width: 100px;" value="'.$v['diskon'].'">';

			$strRemark =
				'<input type="text" class="form-control" id="edit_remark'.$i.'" name="edit_remark[]" style="height:25px; width: 100px;" value="'.$v['remarks'].'">';

			if($v['status'] == 1) {
				$strStatus = 'Accepted';
				$strOption = '';
			}else if($v['status'] == 2) {
				$strStatus = 'Rejected';
				$strOption =
					'<div class="text-center">'.
						'<a class="btn btn-icon waves-effect waves-light btn-danger">'.
							'<i class="fa fa-trash"></i>'.
						'</a>'.
					'</div>';
			}else {
				$strStatus = 'Preparing';
				$strOption =
					'<div class="text-center">'.
						'<a class="btn btn-icon waves-effect waves-light btn-danger">'.
							'<i class="fa fa-trash"></i>'.
						'</a>'.
					'</div>';
			}
			$uom = "";
			foreach ($listuom as $l => $o) {
				if ($v['id_uom'] == $o['id_uom']) $uom = $o['uom_symbol'];
			}

			array_push($data, array(
				$v['id'],
				$v['id_spb'],
				$v['no_spb'],
				$v['stock_name'],
				$v['uom_name'],
				$v['qty'],
				$strUnitPrice,
				$v['price'],
				$strDiskon,
				$v['diskon'],
				$strAmount,
				$amount,
				$strRemark,
				$v['remarks'],
				$strStatus,
				$strOption
			));
			$i++;
		}

		$result["data"] = $data;
		$result["total"] = $tempTotal;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_edit_item()
	{
		$result_spb = $this->purchase_order_model->spb();

		$data = array(
			'spb' => $result_spb
		);

		$this->load->view('edit_modal_item_view', $data);
	}

	public function get_edit_spb() {
		$data		= file_get_contents("php://input");
		$params		= json_decode($data, true);

		$list		= $this->purchase_order_model->get_spb($params['id_spb']);
		$result_uom	= $this->purchase_order_model->uom();

		$data = array();
		$tempObj = new stdClass();
		$uom = "";

		if (sizeof($params['tempBarang']) > 0) {
			foreach ($params['tempBarang'] as $p => $pk) {
				for ($i = 0; $i <= sizeof($list); $i++) {
					if (!empty($list[$i])) {
						if ($list[$i]['id'] == $pk[1] && $list[$i]['no_spb'] == $pk[2]) unset($list[$i]);
					}
				}
			}
		}

		$i = 0;
		foreach ($list as $k => $v) {
			foreach ($result_uom as $k_o => $v_o) {
				// if($v_o['id_uom'] == $v['id_uom']) $uom = $v_o['uom_symbol'];
				if ($v_o['id_uom'] == $v['id_uom']) $uom = $v_o['uom_symbol'];
			}
			$strUnitPrice =
				'<input type="text" class="form-control" step=".0001" id="item_unit_price'.$i.'" name="item_unit_price[]" style="height:25px; width: 100px;" min="0" autocomplete="off" value="0">';
			$strAmount =
				'<input type="text" class="form-control" step=".01" id="item_amount'.$i.'" name="item_amount[]" style="height:25px; width: 100px;" min="0" autocomplete="off" value="0" readonly>';
			$strDiskon =
				'<input type="text" class="form-control" step=".01" id="item_diskon'.$i.'" name="item_diskon[]" style="height:25px; width: 100px;" min="0" autocomplete="off" value="0">';
			$strRemark =
				'<input type="text" class="form-control" id="item_remark'.$i.'" name="item_remark[]" style="height:25px; width: 100px;" autocomplete="off">';
			$strOption =
				'<div class="checkbox">'.
					'<input id="option['.$i.']" type="checkbox" value="'.$i.'">'.
					'<label for="option['.$i.']"></label>' .
				'</div>';
			array_push($data, array(
				$v['id'],
				$v['no_spb'],
				$v['nama_barang'],
				$uom,
				$this->numbering->format_qty($v['qty']),
				$strUnitPrice,
				$strDiskon,
				$strAmount,
				$strRemark,
				$strOption
			));
			$i++;
		}

		$res = array(
			'status'	=> 'success',
			'data'		=> $data
		);

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function get_edit_po_spb($id_po) {
		$result_po = $this->purchase_order_model->edit_po($id_po);
		$list = $this->purchase_order_model->get_po_spb($id_po);
		$listuom = $this->purchase_order_model->uom();

		$data = array();
		$i = 0;
		$tempTotal = 0;
		$username = $this->session->userdata['logged_in']['username'];

		foreach ($list as $k => $v) {
			$amount = (float) $v['unit_price'] * (float) $v['qty'];
			if ($v['diskon'] > 0 && $v['diskon'] <= 100) {
				$diskon = $v['diskon'] / 100;
				$amountVdiskon = $amount * $diskon;
				$amount = $amount - $amountVdiskon;
			} else {
				$amount = $amount - $v['diskon'];
			}

			$tempTotal = $tempTotal + $amount;
			
			$strUnitPrice =
				'<input type="number" class="form-control" step=".0001" id="edit_unit_price'.$i.'" name="edit_unit_price[]" style="height:25px; width: 100px;" min="0" autocomplete="off" value="'.$this->numbering->format_unit_price($v['unit_price'], $result_po[0]['valas_id']).'">';
			$strAmount =
				'<input type="number" class="form-control" step=".01" id="edit_amount'.$i.'" name="edit_amount[]" style="height:25px; width: 100px;" min="0" autocomplete="off" value="'.$this->numbering->format_amount($amount).'" readonly>';
			$strDiskon =
				'<input type="number" class="form-control" step=".01" id="edit_diskon'.$i.'" name="edit_diskon[]" style="height:25px; width: 100px;" min="0" autocomplete="off" value="'.$v['diskon'].'">';
			$strRemark =
				'<input type="text" class="form-control" id="edit_remark'.$i.'" name="edit_remark[]" style="height:25px; width: 100px;" autocomplete="off" value="'.$v['remarks'].'">';
			if ($v['status'] == 1) {
				$strStatus = 'Accepted';
				$strOption = '';
			} else if ($v['status'] == 2) {
				$strStatus = 'Rejected';
				$strOption =
					'<div class="text-center">'.
						'<a class="btn btn-icon waves-effect waves-light btn-danger">'.
							'<i class="fa fa-trash"></i>'.
						'</a>'.
					'</div>';
			} else {
				$strStatus = 'Preparing';
				$strOption =
					'<div class="text-center">'.
						'<a class="btn btn-icon waves-effect waves-light btn-danger">'.
							'<i class="fa fa-trash"></i>'.
						'</a>'.
					'</div>';
			}
			/*$uom = "";
			foreach ($listuom as $l => $o) {
				if($v['id_uom'] == $o['id_uom']) $uom = $o['uom_name'];
			}*/

			array_push($data, array(
				$v['id_pospb'],
				$v['id_spb'],
				$v['no_spb'],
				$v['stock_name'],
				$v['uom_name'],
				$this->numbering->format_qty($v['qty']),
				$strUnitPrice,
				$strDiskon,
				$strAmount,
				$strRemark,
				$strStatus,
				$strOption
			));
			$i++;
		}

		$result["data"] = $data;
		$result["total"] = $this->numbering->format_amount($tempTotal);

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function delete_po_spb()
	{
		$data 	= file_get_contents("php://input");
		$params 	= json_decode($data, true);
		$result = $this->purchase_order_model->delete_po_spb($params['id']);

		if ($result > 0) {
			$this->log_activity->insert_activity('delete', 'Delete Purchase Order id : ' . $params['id']);
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		} else {
			$this->log_activity->insert_activity('delete', 'Gagal Delete Purchase Order id : ' . $params['id']);
			$res = array('status' => 'failed', 'message' => 'Data gagal di hapus');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function save_edit_po()
	{
		$this->form_validation->set_rules('no_po', 'No Purchase Order', 'trim|required|min_length[4]|max_length[100]');
		$this->form_validation->set_rules('tgl_po', 'Tanggal PO', 'trim|required');
		//$this->form_validation->set_rules('coa', 'COA', 'trim|required');
		$this->form_validation->set_rules('dist', 'Distributor', 'trim|required');
		$this->form_validation->set_rules('valas', 'Valas', 'trim|required');
		$this->form_validation->set_rules('rate', 'Rate', 'trim|required');
		$this->form_validation->set_rules('tgl_delivery', 'Tanggal Delivery', 'trim|required');
		$this->form_validation->set_rules('term_of_pay', 'Term Of Payment', 'trim|required');
		$this->form_validation->set_rules('ppn', 'PPN', 'trim');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			$id_po 			= $this->Anti_sql_injection($this->input->post('id_po', TRUE));
			$no_po			= $this->Anti_sql_injection($this->input->post('no_po', TRUE));
			$tglpo			= $this->Anti_sql_injection($this->input->post('tgl_po', TRUE));
			//$coa 			= $this->Anti_sql_injection($this->input->post('coa', TRUE));
			$valas 			= $this->Anti_sql_injection($this->input->post('valas', TRUE));
			$rate 			= $this->Anti_sql_injection($this->input->post('rate', TRUE));
			$dist 			= $this->Anti_sql_injection($this->input->post('dist', TRUE));
			$tgldelivery 	= $this->Anti_sql_injection($this->input->post('tgl_delivery', TRUE));
			$term_of_pay 	= ucwords($this->Anti_sql_injection($this->input->post('term_of_pay', TRUE)));
			$amount_barang 	= $this->Anti_sql_injection($this->input->post('amount_barang', TRUE));
			$ppn 	= $this->Anti_sql_injection($this->input->post('ppn', TRUE));

			$temp_tgl_po		= explode("/", $tglpo);
			$tgl_po 			= date('Y-m-d', strtotime($temp_tgl_po[2] . '-' . $temp_tgl_po[1] . '-' . $temp_tgl_po[0]));
			$temp_tgl_delivery	= explode("/", $tgldelivery);
			$tgl_delivery 		= date('Y-m-d', strtotime($temp_tgl_delivery[2] . '-' . $temp_tgl_delivery[1] . '-' . $temp_tgl_delivery[0]));

			$data = array(
				'id_po'			=> $id_po,
				'no_po'			=> $no_po,
				'tgl_po'		=> $tgl_po,
				//'coa'			=> $coa,
				'dist'			=> $dist,
				'valas'			=> $valas,
				'rate'			=> $rate,
				'tgl_delivery'	=> $tgl_delivery,
				'term_of_pay'	=> $term_of_pay,
				'amount'		=> $amount_barang,
				'ppn'		=> $ppn
			);

			$result = $this->purchase_order_model->save_edit_po($data);

			// if ($result['result'] > 0) {
			$this->log_activity->insert_activity('update', 'Update Purchase Order id : ' . $id_po);
			$result = array('success' => true, 'message' => 'Berhasil mengubah Purchase Order ke database', 'id_po' => $id_po);
			// }else {
			// 	$this->log_activity->insert_activity('update', 'Update Purchase Order id : '.$id_po);
			// 	$result = array('success' => false, 'message' => 'Gagal mengubah Purchase Order ke database');
			// }

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function save_edit_barang_po() {
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data, true);

		$arrData = array();
		$statSave = false;
		foreach ($params['listbarang'] as $k => $v) {
			if($v[0] != '') {
				if($v[10] != 'Accepted') {
					$arrTemp = array(
						'id'			=> $v[0],
						'id_po'			=> $params['id_po'],
						'id_spb'		=> $v[1],
						'unit_price'	=> floatval($v[6]),
						'diskon'		=> floatval($v[7]),
						'price'			=> floatval($v[8]),
						'remark'		=> $v[9],
						'status'		=> '0'
					);
					$resultBarang = $this->purchase_order_model->edit_barang($arrTemp);

					$get_material = $this->purchase_order_model->get_material($v[0]);
					if (sizeof($get_material) > 0) {
						if ($get_material[0]['nama_material']) {
							$dataMaterial = array(
								'id_material'	=> $get_material[0]['nama_material'],
								'status'		=> NULL
							);
							$resultMaterial = $this->purchase_order_model->edit_status_material($dataMaterial);

							if ($resultBarang['status'] > 0 || $resultMaterial['status'] > 0) $statSave = true;
							else $statSave = false;
						}
					}
				}
			}else {
				$arrTemp = array(
					'id_po'			=> $params['id_po'],
					'id_spb'		=> $v[1],
					'unit_price'	=> floatval($v[6]),
					'diskon'		=> floatval($v[7]),
					'price'			=> floatval($v[8]),
					'remark'		=> $v[9],
					'status'		=> '0'
				);
				$result = $this->purchase_order_model->add_barang($arrTemp);
				if ($result > 0) $statSave = true;
				else $statSave = false;
			}
		}

		if ($statSave == true) {
			$this->log_activity->insert_activity('update', 'Update Barang Purchase Order id : ' . $params['id_po']);
			$result = array('success' => true, 'message' => 'Berhasil mengubah barang Purchase Order ke database');
		} else {
			$this->log_activity->insert_activity('update', 'Gagal Update Barang Purchase Order id : ' . $params['id_po']);
			$result = array('success' => false, 'message' => 'Gagal mengubah barang Purchase Order ke database');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result);
	}

	public function close_po() {
		$data			= file_get_contents("php://input");
		$params			= json_decode($data, true);

		$result = $this->purchase_order_model->close_po($params['id']);
		if ($result > 0) {
			$this->log_activity->insert_activity('update', 'Close Purchase Order id : ' . $params['id']);
			$res = array('status' => 'success', 'message' => 'Purchase Order telah di close');
		} else {
			$this->log_activity->insert_activity('update', 'Gagal Close Purchase Order id : ' . $params['id']);
			$res = array('status' => 'success', 'message' => 'Purchase Order gagal di close');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function delete_po() {
		$data			= file_get_contents("php://input");
		$params			= json_decode($data, true);
		$result_pospb	= $this->purchase_order_model->delete_pospb_by_po($params['id']);
		$result_po 		= $this->purchase_order_model->delete_po($params['id']);

		if ($result_po > 0 && $result_pospb > 0) {
			$this->log_activity->insert_activity('delete', 'Delete Purchase Order id : ' . $params['id']);
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		} else {
			$this->log_activity->insert_activity('delete', 'Gagal Delete Purchase Order id : ' . $params['id']);
			$res = array('status' => 'success', 'message' => 'Data gagal di hapus');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function detail_po($id) {
		$po_dist = array();
		$result_po = $this->purchase_order_model->edit_po($id);
		$result_dist = $this->purchase_order_model->distributor();
		$result_coa = $this->purchase_order_model->coa();
		$result_po[0]['date_po'] = date('d/M/Y', strtotime($result_po[0]['date_po']));
		$result_po[0]['delivery_date'] = date('d/M/Y', strtotime($result_po[0]['delivery_date']));

		foreach ($result_dist as $kd => $vd) {
			if ($vd['id'] == $result_po[0]['id_distributor']) {
				$po_dist = $vd;
				$result_po[0]['id_distributor'] = $vd['name_eksternal'];
			}
		}

		foreach ($result_coa as $kc => $vc) {
			if ($vc['id_coa'] == $result_po[0]['id_coa']) {
				$result_po[0]['id_coa'] = $vc['keterangan'];
			}
		}

		$list_po = $this->purchase_order_model->get_po_spb($result_po[0]['id_po']);
		$total = 0;
		foreach ($list_po as $lpk) {
			$total = $total + $lpk['price'];
		}

		$ppn = ($total * ($result_po[0]['ppn'] / 100));
		$total = $total + $ppn;

		$data = array(
			'po'		=> $result_po,
			'po_dist'	=> $po_dist,
			'ppn'		=> $ppn,
			'total'		=> $total
		);

		$this->load->view('detail_po_view', $data);
	}

	public function list_detail_po_barang($id_po) {
		$list = $this->purchase_order_model->get_po_spb($id_po);
		$result_uom	= $this->purchase_order_model->uom();

		$data = array();
		$i = 0;
		$tempTotal = 0;
		$symbol_valas = '';
		$username = $this->session->userdata['logged_in']['username'];

		foreach ($list as $k => $v) {
			$amount = (float) $v['unit_price'] * (float) $v['qty'];
			if ($v['diskon'] > 0 && $v['diskon'] <= 100) {
				$diskon = $v['diskon'] / 100;
				$amountVdiskon = $amount * $diskon;
				$amount = $amount - $amountVdiskon;
				$strDiskon = number_format($v['diskon'], 2, ',', '.') . '%';
			} else {
				$amount = $amount - $v['diskon'];
				$strDiskon = $v['symbol_valas'] . ' ' . number_format($v['diskon'], 2, ',', '.');
			}
			
			$symbol_valas = $v['symbol_valas'];
			
			$tempTotal = $tempTotal + $amount;

			if ($v['status'] == '0') $strStatus = 'Preparing';
			else if ($v['status'] == '1') $strStatus = 'Accepted';
			else if ($v['status'] == '2') $strStatus = 'Rejected';
			else $strStatus = 'Unknown';

			array_push($data, array(
				"",
				$v['no_spb'],
				$v['stock_code'],
				$v['stock_name'],
				$v['uom_name'],
				$this->numbering->format_qty($v['qty']),
				$v['symbol_valas'] . ' ' . $this->numbering->format_unit_price($v['unit_price'], $v['valas_id']),
				$strDiskon,
				$v['symbol_valas'] . ' ' . $this->numbering->format_amount($amount),
				$v['remarks'],
				$strStatus
			));
			$i++;
		}

		$result["data"] = $data;
		$result['symbol_valas'] = $symbol_valas;
		$result["total"] = $symbol_valas . ' ' . number_format($tempTotal, 2, ",", ".");

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function approval_po($id_po) {
		$result_po = $this->purchase_order_model->edit_po($id_po);
		$result_dist = $this->purchase_order_model->distributor();
		$result_coa = $this->purchase_order_model->coa();

		$temptgl_po = explode('-', $result_po[0]['date_po']);
		$temptgl_delivery = explode('-', $result_po[0]['delivery_date']);

		$strPO = array(
			'date_po'		=> date('d/M/Y', strtotime($temptgl_po[0] . '-' . $temptgl_po[1] . '-' . $temptgl_po[2])),
			'delivery_date'	=> date('d/M/Y', strtotime($temptgl_delivery[0] . '-' . $temptgl_delivery[1] . '-' . $temptgl_delivery[2]))
		);

		foreach ($result_dist as $kd => $vd) {
			if ($vd['id'] == $result_po[0]['id_distributor']) {
				$strPO['distributor'] = $vd['name_eksternal'];
			}
		}

		foreach ($result_coa as $kc => $vc) {
			if ($vc['id_coa'] == $result_po[0]['id_coa']) {
				$strPO['coa'] = $vc['keterangan'];
			}
		}

		$list_po = $this->purchase_order_model->get_po_spb($result_po[0]['id_po']);
		$total = 0;
		foreach ($list_po as $lpk) $total = $total + $lpk['price'];
		$ppn = ($total * ($result_po[0]['ppn'] / 100));
		$total = $total + $ppn;

		$checkBarang = $this->purchase_order_model->check_approve_po_spb($id_po);

		$data = array(
			'po'			=> $result_po,
			'strPo'			=> $strPO,
			'barangApprove'	=> $checkBarang,
			'ppn'			=> $ppn,
			'total'			=> $total
		);

		$this->load->view('approval_view', $data);
	}

	public function list_approval_po_barang($id_po)
	{
		$list = $this->purchase_order_model->get_po_spb($id_po);
		$result_uom	= $this->purchase_order_model->uom();

		$data = array();
		$i = 0;
		$tempTotal = 0;
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list as $k => $v) {
			$amount = (float) $v['unit_price'] * (float) $v['qty'];
			if ($v['diskon'] > 0 && $v['diskon'] <= 100) {
				$diskon = $v['diskon'] / 100;
				$amountVdiskon = $amount * $diskon;
				$amount = $amount - $amountVdiskon;
				$strDiskon = number_format($v['diskon'], 2, ',', '.') . '%';
			} else {
				$amount = $amount - $v['diskon'];
				$strDiskon = $v['symbol_valas'] . ' ' . number_format($v['diskon'], 2, ',', '.');
			}
			$tempTotal = $tempTotal + $amount;

			if ($v['status'] == '0') {
				$strOption =
					'<div class="checkbox">' .
					'<input id="option[' . $i . ']" type="checkbox" value="' . $v['id'] . '" onClick="setList()">' .
					'<label for="option[' . $i . ']"></label>' .
					'</div>';
			} else $strOption = '';

			if ($v['status'] == '0') $strStatus = 'Preparing';
			else if ($v['status'] == '1') $strStatus = 'Accepted';
			else if ($v['status'] == '2') $strStatus = 'Rejected';
			else $strStatus = 'Unknown';

			array_push($data, array(
				$v['no_spb'],
				$v['stock_code'],
				$v['uom_name'],
				number_format($v['qty'], 2, '.', ''),
				$v['symbol_valas'] . ' ' . number_format($v['unit_price'], 0, ",", "."),
				$strDiskon,
				$v['symbol_valas'] . ' ' . number_format($amount, 0, ",", "."),
				$v['remarks'],
				$strStatus,
				$strOption
			));
			$i++;
		}

		$result["data"] = $data;
		$result['symbol_valas'] = $v['symbol_valas'];
		$result["total"] = $v['symbol_valas'] . ' ' . number_format($tempTotal, 0, ",", ".");

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function change_status() {
		$this->form_validation->set_rules('status', 'Status', 'trim|required');

		if($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$id_po 			= $this->Anti_sql_injection($this->input->post('id_po', TRUE));
			$status			= $this->Anti_sql_injection($this->input->post('status', TRUE));
			$notes			= $this->Anti_sql_injection($this->input->post('notes', TRUE));

			$statSave = false;

			$po_spb = $this->purchase_order_model->get_po_spb($id_po);
			foreach ($po_spb as $kps) {
				$data = array(
					'stat_approve'	=> 2,
					'id_po_spb'		=> $kps['id_spb'],
					'status'		=> $status,
					'approval_date'	=> date('Y-m-d'),
					'notes'			=> $notes
				);

				$resultApprove = $this->purchase_order_model->approval_po($data);

				/*
				$get_material = $this->purchase_order_model->get_material_approve($kps);
				if ($get_material[0]['nama_material']) {
					if($status == 1) {
						$dataMaterial = array(
							'id_material'	=> $get_material[0]['nama_material'],
							'status'		=> 1
						);
					}else {
						$dataMaterial = array(
							'id_material'	=> $get_material[0]['nama_material'],
							'status'		=> 1
						);
					}
					$resultMaterial = $this->purchase_order_model->edit_status_material($dataMaterial);
				}
				if ($resultApprove > 0 && $resultMaterial > 0) $statSave = true;
				*/
				if($resultApprove > 0 ) $statSave = true;
				else $statSave = false;
			};

			if($statSave == true) {
				$this->log_activity->insert_activity('update', 'Update Status Purchase Order id : ' . $id_po);
				$result = array('success' => true, 'message' => 'Berhasil mengubah status Purchase Order', 'id_po' => $id_po);
			}else {
				$this->log_activity->insert_activity('update', 'Gagal Update Status Purchase Order id : ' . $id_po);
				$result = array('success' => false, 'message' => 'Gagal mengubah status Purchase Order');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function export_excel($id) {
		$po_dist = array();
		$result_po = $this->purchase_order_model->edit_po($id);
		$result_dist = $this->purchase_order_model->distributor();
		$result_coa = $this->purchase_order_model->coa();
		$list = $this->purchase_order_model->get_po_spb($id);
		$result_uom	= $this->purchase_order_model->uom();

		$temptgl_po = explode('-', $result_po[0]['date_po']);
		$result_po[0]['temp_date_po'] = $result_po[0]['date_po'];
		$result_po[0]['date_po'] = date('d/M/Y', strtotime($temptgl_po[0] . '-' . $temptgl_po[1] . '-' . $temptgl_po[2]));
		$temptgl_delivery = explode('-', $result_po[0]['delivery_date']);
		$result_po[0]['temp_delivery_date'] = $result_po[0]['delivery_date'];
		$result_po[0]['delivery_date'] = date('d/M/Y', strtotime($temptgl_delivery[0] . '-' . $temptgl_delivery[1] . '-' . $temptgl_delivery[2]));

		foreach ($result_dist as $kd => $vd) {
			if ($vd['id'] == $result_po[0]['id_distributor']) {
				$po_dist = $vd;
				$result_po[0]['id_distributor'] = $vd['name_eksternal'];
			}
		}

		foreach ($result_coa as $kc => $vc) {
			if ($vc['id_coa'] == $result_po[0]['id_coa']) {
				$result_po[0]['id_coa'] = $vc['keterangan'];
			}
		}

		$data = array();
		$tempTotal = 0;
		foreach ($list as $k => $v) {
			$amount = (float) $v['unit_price'] * (float) $v['qty'];
			if ($v['diskon'] > 0 && $v['diskon'] <= 100) {
				$diskon = $v['diskon'] / 100;
				$amountVdiskon = $amount * $diskon;
				$amount = $amount - $amountVdiskon;
				$strDiskon = $v['diskon'] . '%';
			} else {
				$amount = $amount - $v['diskon'];
				$strDiskon = $v['symbol_valas'] . ' ' . number_format($v['diskon'], 0, ',', '.');
			}
			$tempTotal = $tempTotal + $amount;

			array_push($data, array(
				'',
				$v['no_spb'],
				$v['stock_code'],
				$v['stock_name'],
				$v['uom_name'],
				number_format($v['qty'], 2, '.', ''),
				$v['symbol_valas'] . ' ' . number_format($v['unit_price'], 2, ',', '.'),
				$strDiskon,
				$v['symbol_valas'] . ' ' . number_format($amount, 2, ',', '.'),
				$v['remarks']
			));
		}

		$result["po"]			= $result_po[0];
		$result["po_dist"]		= $po_dist;
		$result["itemPO"]		= $data;
		$result['symbol_valas'] = $v['symbol_valas'];
		$result["total"]		= $v['symbol_valas'] . ' ' . number_format($tempTotal, 2, ",", ".");

		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getActiveSheet()->setTitle("Purchase Order");
		$objPHPExcel->getProperties()->setCreator("System")
			->setLastModifiedBy("System")
			->setTitle("Purchase Order")
			->setSubject("Report Bea Cukai")
			->setDescription("Report Pembelian Lokal")
			->setKeywords("Pembelian Lokal")
			->setCategory("Report");

		$objPHPExcel->setActiveSheetIndex(0)
			// ->setCellValue('A1', 'Purchase Order')
			->setCellValue('A2', 'PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA')
			->setCellValue('A3', '');

		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A3', 'M/S')
			->setCellValue('B3', ': ' . $result['po_dist']['name_eksternal'])

			->setCellValue('A4', 'Address')
			->setCellValue('B4', ': ' . $result['po_dist']['eksternal_address'])

			->setCellValue('A5', 'Attn')
			->setCellValue('B5', ': ' . $result['po_dist']['pic'])

			->setCellValue('A6', 'Telp')
			->setCellValue('B6', ': ' . $result['po_dist']['phone_1'])

			->setCellValue('A7', 'Fax')
			->setCellValue('B7', ': ' . $result['po_dist']['fax'])

			->setCellValue('A8', 'PPN')
			->setCellValue('B8', ': ' . $result_po[0]['ppn'] . '%')

			->setCellValue('I7', 'PO No')
			->setCellValue('I8', 'Date')

			->setCellValue('A10', 'No')
			->setCellValue('B10', 'No SPB')
			->setCellValue('C10', 'Kode Material')
			->setCellValue('D10', 'Nama Material')
			->setCellValue('E10', 'UOM')
			->setCellValue('F10', 'QTY')
			->setCellValue('G10', 'Unit Price')
			->setCellValue('H10', 'Diskon')
			->setCellValue('I10', 'Amount')
			->setCellValue('J10', 'Remark');

		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('J7', ': ' . $result['po']['no_po'])->getStyle('J7')->getAlignment();

		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('J8', ': ' . date('d-M-Y', strtotime($result['po']['temp_date_po'])))->getStyle('J8')->getAlignment();


		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);

		$i = 11;
		$no = 1;
		foreach ($result['itemPO'] as $itemPO) {
			// $objPHPExcel->setActiveSheetIndex()->getStyle('A5:W'.$i)->applyFromArray($styleArray);
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A' . $i, $no)
				->setCellValue('B' . $i, $itemPO[1])
				->setCellValue('C' . $i, $itemPO[2])
				->setCellValue('D' . $i, $itemPO[3])
				->setCellValue('E' . $i, $itemPO[4])
				->setCellValue('F' . $i, $itemPO[5])
				->setCellValue('G' . $i, $itemPO[6])
				->setCellValue('H' . $i, $itemPO[7])
				->setCellValue('I' . $i, $itemPO[8])
				->setCellValue('J' . $i, $itemPO[9]);

			//CUSTOM COLUMN -->
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A' . $i, $no)->getStyle('A' . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('B' . $i, $itemPO[1])->getStyle('B' . $i)->getAlignment();
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('C' . $i, $itemPO[2])->getStyle('C' . $i)->getAlignment();
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('D' . $i, $itemPO[3])->getStyle('D' . $i)->getAlignment();
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('E' . $i, $itemPO[4])->getStyle('E' . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('F' . $i, $itemPO[5])->getStyle('F' . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('G' . $i, $itemPO[6])->getStyle('G' . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('H' . $i, $itemPO[7])->getStyle('H' . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('I' . $i, $itemPO[8])->getStyle('I' . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('J' . $i, $itemPO[9])->getStyle('J' . $i)->getAlignment();

			$i++;
			$no++;
		}

		$styleArray = array();
		$styleArray['borders'] = array();
		$styleArray['borders']['allborders'] = array();
		$styleArray['borders']['allborders']['style'] = PHPExcel_Style_Border::BORDER_THIN;
		$styleArray['borders']['allborders']['color'] = array('argb' => '000000');

		$objPHPExcel->setActiveSheetIndex(0)->getStyle('A10:J' . ($i - 1))->applyFromArray($styleArray);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:J1');
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:W1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:J2');
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:W2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:J3');
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:J3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B4:J4');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:J5');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B6:J6');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B7:H7');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B8:H8');

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A9:J9');

		$objPHPExcel->setActiveSheetIndex(0)->getStyle('A10:J10')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		// Footer
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G' . $i, 'Total');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('G' . $i . ':H' . $i)->getStyle('G' . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I' . $i, $result['po']['symbol_valas'] . " " . number_format($result['po']['total_amount'], 0, ",", "."));
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('I' . $i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('G' . $i . ':I' . $i)->applyFromArray($styleArray);

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $i, 'Delivery Date');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $i . ':B' . $i);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $i, ': ' . date('d-M-Y', strtotime($result['po']['temp_delivery_date'])));
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C' . $i . ':D' . $i);
		$i++;
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $i, 'Destination');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $i . ':B' . $i);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $i, ': Celebit');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C' . $i . ':D' . $i);
		$i++;
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $i, 'Term of Payment');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $i . ':B' . $i);
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $i, ': ' . $result['po']['term_of_payment']);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C' . $i . ':D' . $i);

		$i = $i + 2;
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $i, 'Received by.');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $i . ':B' . $i);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('A' . $i . ':B' . $i)->applyFromArray($styleArray);

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H' . $i, 'Prepared by.');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I' . $i, 'Approved by.');
		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('J' . $i, 'Approved by.');
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('H' . $i . ':J' . $i)->applyFromArray($styleArray);

		$i++;
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $i . ':B' . $i);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A' . $i . ':A' . ($i + 2));
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('A' . $i . ':B' . ($i + 2))->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('H' . $i . ':H' . ($i + 2));
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('H' . $i . ':H' . ($i + 2))->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I' . $i . ':I' . ($i + 2));
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('I' . $i . ':I' . ($i + 2))->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J' . $i . ':J' . ($i + 2));
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('J' . $i . ':J' . ($i + 2))->applyFromArray($styleArray);

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="Purchase Order_' . $result['po']['no_po'] . '.xls"');

		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;
	}
}
