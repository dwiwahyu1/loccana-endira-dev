<style type="text/css">
	.dt-body-right {
		text-align: right;
	}
	.force-overflow {height: 550px; overflow-y: auto;overflow-x: auto}
	.force-items-overflow {height: 500; overflow-y: auto;overflow-x: auto}
	.scroll-overflow {min-height: 550px;}
	.scroll-items-overflow {min-height: 230px;}
	#modal-purchase::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	#modal-purchase::-webkit-scrollbar
	{
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-purchase::-webkit-scrollbar-thumb
	{
		background-image: -webkit-gradient(linear,
		   left bottom,
		   left top,
		   color-stop(0.44, rgb(122,153,217)),
		   color-stop(0.72, rgb(73,125,189)),
		   color-stop(0.86, rgb(28,58,148)));
	}
</style>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title" id="title_menu">Purchase Order</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="card-box" id="div_list_po">
				<table id="list_po" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No PO</th>
							<th>Tanggal PO</th>
							<th>Distributor</th>
							<th>Total Amount</th>
							<th>Delivery Date</th>
							<th>Term of Payment</th>
							<th>Status</th>
							<th style="width: 200px;">Option</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>

			<div class="card-box" id="div_detail_po" style="display: none;"></div>
		</div>
	</div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-dialog-scrollable" style="width: 85%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-purchase">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div id="panel-modaldetails" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-dialog-scrollable" style="width: 85%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-items-overflow" id="modal-purchase">
					<div class="scroll-items-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-dialog-scrollable" style="width: 85%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-items-overflow" id="modal-purchase">
					<div class="scroll-items-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$(document).ready(function(){
		$('#div_detail_po').hide();
		get_list_po();
	});

	function get_list_po() {
		$("#list_po").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'purchase_order/list_po/';?>",
			"searchDelay": 700,
			"responsive": true,
			"destroy": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip',
			"initComplete": function(){
				$("div.toolbar").prepend(
					'<div class="btn-group pull-left"><a class="btn btn-primary" onClick="add_po()"><i class="fa fa-plus"></i> Add Purchase Order</a></div>'
				);
			},
			"columnDefs" : [{
				"targets" : [3],
				"className" : 'dt-body-right'
			}]
		});
	}

	function add_po(){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('purchase_order/add_po');?>');
		$('#panel-modal  .panel-title').html('<i class="fa fa-plus"></i> Add Purchase Order');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function edit_po(id){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('purchase_order/edit_po/');?>'+"/"+id);
		$('#panel-modal  .panel-title').html('<i class="fa fa-edit"></i> Edit Purchase Order');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function details_po(id_po) {
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('purchase_order/detail_po/');?>'+"/"+id_po);
		$('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Details Purchase Order');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function approve_po(id_po) {
		$('#panel-modaldetails').removeData('bs.modal');
		$('#panel-modaldetails  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modaldetails  .panel-body').load('<?php echo base_url('purchase_order/approval_po/');?>'+"/"+id_po);
		$('#panel-modaldetails  .panel-title').html('<i class="fa fa-check"></i> Approval Purchase Order');
		$('#panel-modaldetails').modal({backdrop:'static',keyboard:false},'show');
	}

	function close_po(id) {
		swal({
			title: 'Yakin Untuk Menutup Purchase Order ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost={
				"id" : id
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>purchase_order/close_po",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
				   swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						get_list_po();
					})

					if (response.status == "success") {

					}else swal("Failed!", response.message, "error");
				}
			});
		})
	}
		
	function delete_po(id){
		swal({
			title: 'Yakin akan Menghapus ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost={
				"id" : id
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>purchase_order/delete_po",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
				   swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						get_list_po();
					})

					if (response.status == "success") {

					}else {
						swal("Failed!", response.message, "error");
					}
				}
			});
		})
	}
</script>