<style type="text/css">
	tbody .dt-body-left{text-align: left;}
	tbody .dt-body-right{text-align: right;}
	#listdetailbarang {
		counter-reset: rowNumber;
	}

	#listdetailbarang tr > td:first-child {
		counter-increment: rowNumber;
	}

	#listdetailbarang tr td:first-child::before {
		content: counter(rowNumber);
		min-width: 1em;
		margin-right: 0.5em;
	}
</style>
<div class="row">
	<div class="col-md-12">
		<div class="pull-right">
			<a class="btn btn-icon waves-effect waves-light btn-success m-b-5" data-toggle="tooltip" data-placement="top" title="Export to Excel" id="export_excel">
				<i class="fa fa-file-excel-o"></i>
			</a>
			<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download PDF" id="btn_download">
				<i class="fa fa-download"></i>
			</a>
		</div>
	</div>
</div>

<div class="row" style="display: none;">
	<div class="col-md-12" style="display: none;">
		<div class="col-md-2 logo-place">
			<img src="<?php echo site_url(); ?>assets/images/logo-celebit.jpg" alt="logo-celebit">
		</div>
		<div class="col-md-10">
			<div class="col-md-12 titleReport">
				<h1 id="titleCelebit">CELEBIT</h1>
				<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
				<h4 id="titleAlamat">BANDUNG FACTORY : JL.BUAH DUA RT.01/RW.04 RANCAEKEK - BANDUNG-INDONESIA</h4>
				<h4 id="titleTlp">TEL 62-22-7798 561/7798 542, FAX : 62-22-7798 562 E-MAIL : celebit@celebit.com</h4>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="row">
			<label class="col-md-2">No PO :</label>
			<label class="col-md-10"><?php if(isset($po[0]['no_po'])){ echo $po[0]['no_po']; }?></label>
		</div>

		<div class="row">
			<label class="col-md-2">Tanggal PO :</label>
			<label class="col-md-10"><?php if(isset($po[0]['date_po'])){ echo $po[0]['date_po']; }?></label>
		</div>

		<!--<div class="row">
			<label class="col-md-2">COA :</label>
			<label class="col-md-10"><?php if(isset($po[0]['id_coa'])){ echo $po[0]['id_coa']; }?></label>
		</div>-->

		<div class="row">
			<label class="col-md-2">Distributor :</label>
			<label class="col-md-10"><?php if(isset($po[0]['id_distributor'])){ echo $po[0]['id_distributor']; }?></label>
		</div>

		<div class="row">
			<label class="col-md-2">Tanggal Delivery :</label>
			<label class="col-md-10"><?php if(isset($po[0]['delivery_date'])){ echo $po[0]['delivery_date']; }?></label>
		</div>

		<div class="row">
			<label class="col-md-2">Term of Payment :</label>
			<label class="col-md-10"><?php if(isset($po[0]['term_of_payment'])){ echo $po[0]['term_of_payment']; }?></label>
		</div>
		
		<div class="row">
			<label class="col-md-2">Valas :</label>
			<label class="col-md-10"><?php if(isset($po[0]['nama_valas'])){ echo $po[0]['nama_valas']; }?></label>
		</div>
		
		<div class="row">
			<label class="col-md-2">Rate :</label>
			<label class="col-md-10"><?php if(isset($po[0]['rate'])){ echo $po[0]['symbol_valas'].'. '.number_format($po[0]['rate'],2,'.',','); }?></label>
		</div>
		<div class="row">
			<label class="col-md-2">PPN :</label>
			<label class="col-md-10"><?php if(isset($po[0]['ppn'])){ echo number_format($po[0]['ppn'],2,'.',','); }?> %</label>
			<input id="ppn-hidden" type="hidden" value ="<?php if(isset($po[0]['ppn'])) echo $po[0]['ppn']?>">
		</div>

		<div class="row">
			<label class="col-md-12" for="nama">Daftar Barang :</label>
			<div class="col-md-12">
				
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<table id="listdetailbarang" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th style="width: 5%;">No</th>
					<th>No SPB</th>
					<th>Kode Material</th>
					<th>Nama Material</th>
					<th>UOM</th>
					<th>QTY</th>
					<th>Unit Price</th>
					<th>Diskon</th>
					<th>Amount</th>
					<th>Remark</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody></tbody>
			<tfoot>
				<tr>
					<th colspan="8" style="text-align: right;">PPN</th>
					<td style="text-align: right;" id="td_PPN"><?php
						if(isset($ppn)) echo number_format($ppn, 2, '.', ',');
					?></td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<th colspan="8" style="text-align: right;">Total</th>
					<td style="text-align: right;" id="td_detailPO_Total"><?php
						if(isset($total)) echo number_format($total, 2, '.', ',');
					?></td>
					<td colspan="2"></td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>

<script type="text/javascript">
	var dataImage = null;

	$(document).ready(function(){
		dt_barang();
	});

	function dt_barang() {
		$('#listdetailbarang').DataTable( {
			"processing": true,
			"searching": false,
			"responsive": true,
			"paging": false,
			"lengthChange": false,
			"destroy": true,
			"paging": false,
			"info": false,
			"bSort": false,
			"ajax": "<?php echo base_url().'purchase_order/list_detail_po_barang/'.$po[0]['id_po'];?>",
			"columnDefs": [{
				"targets": [4, 5, 6],
				"className": 'dt-body-right'
			}]
		});
	}

	function toDataURL(url, callback) {
		var xhr = new XMLHttpRequest();
		xhr.onload = function() {
			var reader = new FileReader();
			reader.onloadend = function() {
				callback(reader.result);
			}
			reader.readAsDataURL(xhr.response);
		};
		xhr.open('GET', url);
		xhr.responseType = 'blob';
		xhr.send();
	}
	
	toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
		dataImage = dataUrl;
	})

	$('#btn_download').on('click', function() {
		var doc = new jsPDF("a4");
		var imgData = dataImage;
		var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
		var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

		doc.setTextColor(50);
		doc.addImage(imgData, 'JPEG', 5, 5, 25, 25)
		doc.setFontSize(12);
		doc.text($('#titleCelebit').html(), 33, 10, 'left');
		doc.setFontSize(10);
		doc.text($('#titlePerusahaan').html(), 33, 15, 'left');
		doc.setFontSize(11);
		doc.text($('#titleAlamat').html(), 33, 20, 'left');
		doc.setFontSize(11);
		doc.text($('#titleTlp').html(), 33, 25, 'left');

		doc.setFontSize(16);
		doc.setFontStyle('helvetica','arial','sans-serif','bold');
		doc.line(pageWidth - 5, 30, 5, 30);
		doc.text('PURCHASE ORDER', pageWidth / 2, 40, 'center');

		doc.setFontSize(12);
		doc.text('M/S', 5, 50, 'left');
		doc.text(': <?php if(isset($po_dist['name_eksternal'])) echo $po_dist['name_eksternal']; ?>', 25, 50, 'left');
		
		doc.text('Address', 5, 55, 'left');
		doc.text(': <?php if(isset($po_dist['eksternal_address'])) echo str_replace(array( "\n", "\r" ),'',$po_dist['eksternal_address']); ?>', 25, 55, 'left');
		
		doc.text('Attn', 5, 60, 'left');
		doc.text(': <?php if(isset($po_dist['pic'])) echo str_replace(array( "\n", "\r" ),'',$po_dist['pic']); ?>', 25, 60, 'left');
		
		doc.text('Telp', 5, 65, 'left');
		doc.text(': <?php if(isset($po_dist['phone_1'])) echo str_replace(array( "\n", "\r" ),'',$po_dist['phone_1']); ?>', 25, 65, 'left');
		doc.text('PO No', pageWidth -55, 65, 'left');
		doc.text(': <?php if(isset($po[0]['no_po'])) echo $po[0]['no_po']; ?>', pageWidth - 35, 65, 'left');
		
		doc.text('Fax', 5, 70, 'left');
		doc.text(': <?php if(isset($po_dist['fax'])) echo str_replace(array( "\n", "\r" ),'',$po_dist['fax']); ?>', 25, 70, 'left');
		doc.text('Date', pageWidth - 55, 70, 'left');
		doc.text(': <?php if(isset($po[0]['temp_date_po'])) echo date('d-M-Y', strtotime($po[0]['temp_date_po'])); ?>', pageWidth - 35, 70, 'left');
		// doc.setFont("helvetica");
		doc.autoTable({
			html 			: '#listdetailbarang',
			theme			: 'plain',
			styles			: {
				fontSize 	: 10, 
				lineColor	: [116, 119, 122],
				font: "arial",
				lineWidth	: 0.1,
				cellWidth 	: 'auto',
				
			},
			margin 			: 4.5,
			tableWidth		: (pageWidth - 10),
			headStyles		: {
				valign		: 'middle', 
				halign		: 'center',
			},
			columns 		: [0,1,2,3,4,5,6,7,8,9],
			didParseCell	: function (data) {
				if(data.table.foot[0]) {
					if(data.table.foot[0].cells[0]) {
						data.table.foot[0].cells[0].styles.halign = 'right';
					}
					if(data.table.foot[0].cells[8]) {
						data.table.foot[0].cells[8].styles.halign = 'right';
					}
					if(data.table.foot[0].cells[9]) {
						data.table.foot[0].cells[9].colSpan = 1;
					}
				}
			},
			columnStyles	: {
				0: {halign: 'center'},
				1: {halign: 'center'},
				2: {halign: 'left'},
				3: {halign: 'left'},
				4: {halign: 'center'},
				5: {halign: 'center'},
				6: {halign: 'right'},
				7: {halign: 'center'},
				8: {halign: 'right'},
				9: {halign: 'left'}
			},
			rowPageBreak	: 'auto',
			showHead 		: 'firstPage',
			showFoot		: 'lastPage',
			startY			: 75
		});

		doc.text('Delivery Date', 5, doc.autoTable.previous.finalY + 5, 'left');
		doc.text(': <?php if(isset($po[0]['temp_delivery_date'])) echo date('d-M-Y', strtotime($po[0]['temp_delivery_date'])); ?>', 40, doc.autoTable.previous.finalY + 5, 'left');

		doc.text('Destination', 5, doc.autoTable.previous.finalY + 10, 'left');
		//doc.text(': <?php if(isset($po[0]['name_eksternal'])) echo $po[0]['name_eksternal']; ?>', 40, doc.autoTable.previous.finalY + 10, 'left');
		doc.text(': PT. CELEBIT', 40, doc.autoTable.previous.finalY + 10, 'left');

		doc.text('Term of Payment', 5, doc.autoTable.previous.finalY + 15, 'left');
		doc.text(': <?php if(isset($po[0]['term_of_payment'])) echo $po[0]['term_of_payment']; ?>', 40, doc.autoTable.previous.finalY + 15, 'left');

		var startYPO = doc.autoTable.previous.finalY + 20;

		doc.autoTable({
			theme			: 'plain',
			styles			: {fontSize : 8, lineColor : [116, 119, 122], lineWidth : 0.1, cellWidth : 'auto'},
			margin 			: 4.5,
			tableWidth		: 30,
			headStyles		: {valign : 'middle', halign : 'center'},
			head: [['Received by.']],
			body: [['']],
			columnStyles	: {
				0: {minCellHeight: 20}
			},
			rowPageBreak	: 'auto',
			showHead 		: 'firstPage',
			showFoot		: 'lastPage',
			startY			: startYPO + 3
		});
		/*doc.autoTable({
			theme			: 'plain',
			styles			: {fontSize : 8, lineColor : [116, 119, 122], lineWidth : 0.1, cellWidth : 'auto'},
			margin 			: 4.5,
			tableWidth		: 30,
			headStyles		: {valign : 'middle', halign : 'center'},
			head: [['']],
			columnStyles	: {
				0: {minCellHeight: 20}
			},
			rowPageBreak	: 'auto',
			showHead 		: 'firstPage',
			showFoot		: 'lastPage',
			startY			: doc.autoTable.previous.finalY
		});*/

		doc.autoTable({
			theme			: 'plain',
			styles			: {fontSize : 8, lineColor : [116, 119, 122], lineWidth : 0.1, cellWidth : 'auto'},
			margin 			: {left: pageWidth - 105.5},
			tableWidth		: 100,
			headStyles		: {valign : 'middle', halign : 'center'},
			head 			: [['prepared by,', 'Approved by,', 'Approved by,']],
			body 			: [['', '', '']],
			columnStyles	: {
				0: {minCellHeight: 20, cellWidth: 5}
			},
			rowPageBreak	: 'auto',
			showHead 		: 'firstPage',
			showFoot		: 'lastPage',
			startY			: startYPO + 3
		});

		/*doc.autoTable({
			theme			: 'plain',
			styles			: {fontSize : 8, lineColor : [116, 119, 122], lineWidth : 0.1, cellWidth : 'auto'},
			margin 			: {left: pageWidth - 105.5},
			tableWidth		: 100,
			headStyles		: {valign : 'middle', halign : 'center'},
			head 			: [['', '', '']],
			columnStyles	: {
				0: {cellWidth: 5}
			},
			rowPageBreak	: 'auto',
			showHead 		: 'firstPage',
			showFoot		: 'lastPage',
			startY			: doc.autoTable.previous.finalY
		});*/

		doc.save('Purchase Order_<?php if(isset($po[0]['no_po'])) echo $po[0]['no_po'];?>.pdf');
	})

	$('#export_excel').on('click', function() {
		window.open("<?php echo base_url().'purchase_order/export_excel/'.$po[0]['id_po'];?>");
		/*$.ajax({
			type: "GET",
			url : "<?php echo base_url().'purchase_order/export_excel/'.$po[0]['id_po'];?>",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					for (var i = 0; i < r.length; i++) {
						var newOption = new Option(r[i].stock_code+' | '+r[i].stock_name, r[i].id, false, false);
						$('#item_id'+idRowItem).append(newOption);
					}
					$('#item_id'+idRowItem).removeAttr('disabled');
				}else $('#item_id'+idRowItem).removeAttr('disabled');
				idRowItem++;
			}
		});*/

		/*var url = '<?php echo base_url(); ?>purchase_order/export_excel'; 
		var tanggal_awal = '<?php echo $tanggal_masuk; ?>';
		var tanggal_akhir = '<?php echo $tanggal_keluar; ?>';
		var report = '<?php echo $report ?>';
		//"url" : "<?php echo base_url().'purchase_order/list_detail_po_barang/'.$po[0]['id_po'];?>",
		  
		var form = $("<form action='" + url + "' method='post' target='_blank'>" +
			"<input type='hidden' name='tanggal_awal' value='" + tanggal_awal + "' />" +
			"<input type='hidden' name='tanggal_akhir' value='" + tanggal_akhir + "' />" +
			"<input type='hidden' name='report' value='" + report + "' />" +
			"</form>");
		$('body').append(form);
		form.submit();*/
	})
</script>