<style>
	#loading-us{display:none}
	#tick{display:none}

	#loading-mail{display:none}
	#cross{display:none}
	.form-item{margin-top: 15px;overflow: auto;}
</style>

<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

<div class="item form-group">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_spb">No SPB <span class="required"><sup>*</sup></span></label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<select class="form-control" name="no_spb" id="no_spb" style="width: 100%" required>
			<option value="" selected>-- Select SPB --</option>
		<?php foreach($spb as $key => $v) { ?>
			<option value="<?php echo $v['id']; ?>" ><?php echo $v['no_spb']; ?></option>
		<?php } ?>
		</select>
	</div>
</div>

<div class="item form-group">
	<table id="listspb" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th></th>
				<th>No SPB</th>
				<th>Deskripsi</th>
				<th>UOM</th>
				<th>QTY</th>
				<th>Unit Price</th>
				<th>Diskon</th>
				<th>Amount</th>
				<th>Remark</th>
				<th></th>
			</tr>
		</thead>
		<tbody></tbody>
		<tfoot>
			<tr>
				<th colspan="7" style="text-align: right;">Total</th>
				<td style="text-align: right;" id="tdTotal"></td>
				<td colspan="2"></td>
			</tr>
		</tfoot>
	</table>
</div>

<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="edit_btn_add_item"></label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<button id="edit_btn_add_item" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Barang</button>
	</div>
</div>

<script type="text/javascript">
	var table_spb;

	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();

		$("#tanggal_diperlukan").datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});
		resetDt();
		$('#no_spb').select2();

		$('#no_spb').on('change', function() {
			getBarang();
		});
	});

	function resetDt() {
		table_spb = $('#listspb').DataTable( {
			"processing": true,
			"searching": false,
			"paging": false,
			"responsive": true,
			"lengthChange": false,
			"destroy": true,
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0],
				"visible": false,
				"searchable": false
			}]
		});
	}

	function getBarang() {
		var value_spb = $("#no_spb").val();
		if(value_spb != '') sendNo($("#no_spb option:selected").text()+ ',' +value_spb);
		else table_spb.clear().draw();
	}

	function cal_price(row) {
		var rowData = table_spb.row(row).data();
		var qty = parseFloat(rowData[4]);
		var unitPrice = $('#item_unit_price'+row).val();
		var amount = unitPrice * qty;

		if($('#item_diskon'+row).val() > 0 && $('#item_diskon'+row).val() <= 100) {
			var diskon = ($('#item_diskon'+row).val() / 100);
			var amountVdiskon = amount * diskon;
			amount = amount - amountVdiskon;
		}else {
			var diskon = $('#item_diskon'+row).val();
			amount = amount - diskon;
		}

		$('#item_amount' + row).val(amount);

		var tempTotal = 0;
		$('input[name="item_amount[]"]').each(function() {
			tempTotal = tempTotal + parseInt(this.value);
		})
		$('#tdTotal').html(formatNumber(tempTotal));
	}

	function sendNo(spb) {
		var spb = spb.split(',');
		var idspb = spb[1];
		var nospb = spb[0];
		
		var tempBarang = [];
		if(t_editBarang.rows().data().length > 0) {
			for (var i = 0; i < t_editBarang.rows().data().length; i++) {
				tempBarang.push(t_editBarang.rows().data()[i]);
			}
		}
		var datapost = {
			"id_spb" : idspb,
			"no_spb" : nospb,
			"tempBarang" : tempBarang
		};

		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>purchase_order/get_edit_spb",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				table_spb.clear().draw();
				for (var i = 0; i < r.data.length; i++) {
					table_spb.row.add(r.data[i]).draw();
				}

				$('input[name="item_unit_price[]"]').on('keyup', function() {
					var idRow = $(this).attr('id').replace(/\D/g,'');
					cal_price(idRow);
				});

				$('input[name="item_diskon[]"]').on('keyup', function() {
					var idRow = $(this).attr('id').replace(/\D/g,'');
					cal_price(idRow);
				});
			}
		});
	}

	$('#edit_btn_add_item').click(function() {
		var tempArr = [];
		var tempTotal = 0;
		var statusBtn = 0;

		$('input[type=checkbox]').each(function () {
			if(this.checked == true) {
				var row 		= parseInt($(this).val());
				var rowData 	= table_spb.row(row).data();
				var unitPrice 	= $('#item_unit_price' + row).val();
				var diskon 		= $('#item_diskon' + row).val();
				var amount 		= $('#item_amount' + row).val();
				var remark 		= $('#item_remark' + row).val();

				var strUnitPrice =
					'<input type="number" class="form-control" id="edit_unit_price'+last_item+'" name="edit_unit_price[]" style="height:25px; width: 100px;" min="0" autocomplete="off" step=".0001" value="'+unitPrice+'" readonly>';
				var strAmount =
					'<input type="number" class="form-control" id="edit_amount'+last_item+'" name="edit_amount[]" style="height:25px; width: 100px;" autocomplete="off" step=".0001" value="'+amount+'" readonly>';
				var strDiskon =
					'<input type="number" class="form-control" id="edit_diskon'+last_item+'" name="edit_diskon[]" style="height:25px; width: 100px;" autocomplete="off" step=".0001" value="'+diskon+'" readonly>';
				var strRemark =
					'<input type="text" class="form-control" id="edit_remark'+last_item+'" name="edit_remark[]" style="height:25px; width: 100px;" autocomplete="off" step=".0001" value="'+remark+'" readonly>';
				var option =
					'<div class="text-center">'+
						'<a class="btn btn-icon waves-effect waves-light btn-danger">'+
							'<i class="fa fa-trash"></i>'+
						'</a>'+
					'</div>';
				if(unitPrice != '' && unitPrice != 0) {
					var arrRow = [
						'',
						rowData[0],
						rowData[1],
						rowData[2],
						rowData[3],
						rowData[4],
						strUnitPrice,
						strDiskon,
						strAmount,
						strRemark,
						'',
						option
					];
					tempArr.push(arrRow);
					arrRow = [];
					statusBtn = 1;
				}else if(unitPrice == 0) {
					swal('Warning', 'Unit Price baris ke-'+ (row+1) +' tidak boleh kosong');
					statusBtn = 0;
				}else {
					swal('Warning', 'Unit Price baris ke-'+ (row+1) +' harus diisi');
					statusBtn = 0;
				}
			}
		});

		if(statusBtn == 1) {
			for (var i = 0; i < tempArr.length; i++) {
				t_editBarang.row.add(tempArr[i]).draw();
			}

			refreshTotal();
			$('#panel-modalchild').modal('toggle');
		}
	})
</script>
