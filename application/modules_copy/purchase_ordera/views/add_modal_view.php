<!--Parsley-->
<style>
	#no_po_loading-us {
		display: none
	}

	#no_po_tick {
		display: none
	}

	.add_item {
		cursor: pointer;
		text-decoration: underline;
		color: #96b6e8;
		padding-top: 6px;
	}

	.add_item:hover {
		color: #ff8c00
	}
</style>

<form class="form-horizontal form-label-left" id="add_form" role="form" action="<?php echo base_url('purchase_order/save_po'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_po">No PO <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-minlength="4" data-parsley-maxlength="100" type="text" id="no_po" name="no_po" class="form-control col-md-7 col-xs-12" placeholder="no PO minimal 4 karakter" value="<?php
				if(isset($no_po_temp)) echo $no_po_temp;
			?>" readonly>
			<span id="no_po_loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking No Purchase Order...</span>
			<span id="no_po_tick"></span>
		</div>
	</div>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tgl_po">Tanggal PO <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="100" type="text" id="tgl_po" name="tgl_po" class="form-control col-md-7 col-xs-12" placeholder="Tanggal purchase order" autocomplete="off" required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="dist">Distributor <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="dist" name="dist" style="width: 100%" required>
				<option value="">-- Select Distributor --</option>
				<?php foreach ($distributor as $dk) { ?>
					<option value="<?php echo $dk['id']; ?>"><?php echo $dk['name_eksternal']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tgl_delivery">Tanggal Delivery <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="100" type="text" id="tgl_delivery" name="tgl_delivery" class="form-control col-md-7 col-xs-12" placeholder="Tanggal purchase order" autocomplete="off" required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="term_of_pay">Term of Payment <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="term_of_pay" name="term_of_pay" style="width: 100%" required>
				<option value="" disabled="disabled" selected>-- Select Term --</option>
				<option value="Telex Transfer (TT)">Telex Transfer (TT)</option>
				<option value="1 Day">1 Day</option>
				<option value="15 Day">15 Day</option>
				<option value="30 Day"> 30 Day</option>
				<option value="45 Day"> 45 Day</option>
				<option value="60 Day"> 60 Day</option>
				<option value="90 Day"> 90 Day</option>
				<option value="120 Day"> 120 Day</option>
				<option value="150 Day"> 150 Day</option>
				<option value="180 Day"> 180 Day</option>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="valas">Valas <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="valas" name="valas" style="width: 100%" required>
				<option value="" disabled="disabled" SELECTED>-- Valas --</option>
				<?php foreach ($valas_list as $valass) { ?>
					<option value="<?php echo $valass['valas_id']; ?>"><?php echo $valass['nama_valas']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="rate">Rate <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="number" id="rate" name="rate" class="form-control col-md-7 col-xs-12" placeholder="Rate" min="0" step=".0001" autocomplete="off" value='1' required>
			<!--<span id="no_po_loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking No Purchase Order...</span>
			<span id="no_po_tick"></span>-->
		</div>
	</div>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="ppn">PPN <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="number" class="form-control" id="ppn" name="ppn"  placeholder="PPN" min="0" max="100" step="1" autocomplete="off" value="10" required>
		</div>
		<label class="control-label" style="text-align:left" for="ppn">%</label>
	</div>
	<div class="item form-group" id="jenis_bayar" style="display:none">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_coa">Jenis Pembayaran <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="id_coa" name="id_coa" style="width: 100%" required>
				<option value="" disabled="disabled" SELECTED>-- Jenis Pembayaran --</option>
				<?php foreach ($coa_list as $coas) { ?>
					<option value="<?php echo $coas['id_coa']; ?>"><?php echo $coas['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Barang : </label>
		<div class="col-md-8 col-sm-6 col-xs-12 add_item">
			<a class="btn btn-primary" onclick="add_item()">
				<i class="fa fa-plus"></i> Tambah Barang
			</a>
			<input type="hidden" id="amount_barang" name="amount_barang" value="0">
		</div>
	</div>

	<div class="item form-group">
		<table id="listaddbarang" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th></th>
					<th>No SPB</th>
					<th>Deskripsi</th>
					<th>UOM</th>
					<th>QTY</th>
					<th>Unit Price</th>
					<th>Diskon</th>
					<th>Amount</th>
					<th>Remark</th>
					<th>Option</th>
				</tr>
			</thead>
			<tbody></tbody>
			<tfoot>
				<tr>
					<th colspan="7" style="text-align: right;">PPN</th>
					<td style="text-align: right;" id="td_PPN"></td>
					<td colspan="2"></td>
				</tr>
				<tr>
					<th colspan="7" style="text-align: right;">Total</th>
					<td style="text-align: right;" id="tdPO_Total"></td>
					<td colspan="2"></td>
				</tr>
			</tfoot>
		</table>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Purchase Order</button>
		</div>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	var dataBarang = [];
	var t_addBarang;
	
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
		$('#tgl_po').datepicker({
			format: "dd/M/yyyy",
			autoclose: true,
			todayHighlight: true
		});

		$('#tgl_delivery').datepicker({
			format: "dd/M/yyyy",
			autoclose: true,
			todayHighlight: true
		});

		$("#dist").select2();
		$("#coa").select2();
		dtBarang();
	});

	function dtBarang() {
		t_addBarang = $('#listaddbarang').DataTable({
			"processing": true,
			"searching": false,
			"responsive": true,
			"paging": false,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0],
				"visible": true,
				"searchable": false
			}]
		});
	}

	function add_item() {
		$('#panel-modalchild').removeData('bs.modal');
		$('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modalchild  .panel-body').load('<?php echo base_url('purchase_order/add_item'); ?>');
		$('#panel-modalchild  .panel-title').html('<i class="fa fa-plus"></i> Tambah Barang');
		$('#panel-modalchild').modal({backdrop: 'static', keyboard: false}, 'show');
	}

	function refreshTotal() {
		var tempTotal = 0;

		for (var i = 0; i < t_addBarang.rows().data().length; i++) {
			var rowData = t_addBarang.row(i).data();
			var tempAmount = rowData[7].toString().replace(/[^0-9\.]/g, '');
			tempTotal = tempTotal + parseFloat(tempAmount);
		}

		var ppn = (tempTotal * parseFloat($("#ppn").val()) / 100);
		tempTotal = tempTotal + ppn;
		$('#td_PPN').html(formatCurrencyComa(ppn));
		$('#tdPO_Total').html(formatCurrencyComa(tempTotal));
		$('#amount_barang').val(tempTotal);
	}

	$('#listaddbarang').on('click', 'a', function() {
		t_addBarang.row($(this).parents('tr')).remove().draw(false);
		refreshTotal();
	});

	$("#ppn").on("keyup", function() {
		refreshTotal();
	});

	$('#term_of_pay').on('change', function() {
		var tv = $('#term_of_pay').val();
		if (tv == 'CASH') $('#jenis_bayar').show();
		else $('#jenis_bayar').hide();
	});

	$('#add_form').on('submit', (function(e) {
		$('#btn-submit').attr('disabled', 'disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		var formData = new FormData(this);

		$.ajax({
			type: 'POST',
			url: $(this).attr('action'),
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					save_barang(response.lastid);
				} else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Tambah Purchase Order");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah Purchase Order");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}));

	function save_barang(lastid) {
		var arrTemp = [];
		for (var i = 0; i < t_addBarang.rows().data().length; i++) {
			var rowData = t_addBarang.row(i).data();
			arrTemp.push(rowData);
		}

		var datapost = {
			"id_po": lastid,
			'listbarang': arrTemp
		};

		$.ajax({
			type: "POST",
			url: "<?php echo base_url(); ?>purchase_order/save_barang_po",
			data: JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function() {
						window.location.href = "<?php echo base_url('purchase_order'); ?>";
					})
				} else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Tambah Purchase Order");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah Purchase Order");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}
</script>