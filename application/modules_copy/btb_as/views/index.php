<style>
	.dt-body-left {text-align:left;}
	.dt-body-right {text-align:right;}
	.dt-body-center {text-align:center; vertical-align: middle;}
	.force-overflow {height: 650px; overflow-y: auto;overflow-x: auto}
	.force-report-overflow {height: 230px; overflow-y: auto;overflow-x: auto}
	.force-view-report-overflow {height: 600px; overflow-y: auto;overflow-x: auto}
	.forces-overflow {height: 500px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow {min-height: 650px;}
	.scroll-report-overflow {min-height: 230px;}
	.scroll-view-report-overflow {min-height: 600px;}
	.scrollss-overflow {min-height: 500px; min-width: 600px;}
	#modal-btb::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	#modal-btb::-webkit-scrollbar
	{
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-btb::-webkit-scrollbar-thumb
	{
		background-image: -webkit-gradient(linear,
		   left bottom,
		   left top,
		   color-stop(0.44, rgb(122,153,217)),
		   color-stop(0.72, rgb(73,125,189)),
		   color-stop(0.86, rgb(28,58,148)));
	}
  .changed_status{cursor:pointer;text-decoration: underline;color:#96b6e8;}
  .changed_status:hover{color:#ff8c00}
  .custom-tables, th{text-align:center;vertical-align:middle;}
  .custom-tables.align-text, th{vertical-align:middle;}
  .custom-tables.text-center {align-text: center;}
  .text-left {align-text: left;}
  .text-right {align-text: right;}
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Penerimaan Barang</h4>
        </div>
    </div>
	
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">

				<table id="listbtb" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th class="custom-tables text-center">No</th>
							<th class="custom-tables text-center">No BTB</th>
							<th class="custom-tables text-center">Nama Supplier</th>
							<th class="custom-tables text-center">Tanggal BTB</th>
							<th class="custom-tables text-center">Type BTB</th>
							<th class="custom-tables text-center">Status BTB</th>
							<th class="custom-tables text-center">Actions</th>
						</tr>
					</tbody>
				</table>

            </div>
        </div><!-- end col -->
    </div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width:1200px;">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-btb">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal-report" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" style="width:85%;">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-report-overflow" id="modal-btb">
					<div class="scroll-report-overflow">
						<p></p>
					</div>
				</div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal-view-report" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width:1200px;">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-view-report-overflow" id="modal-btb">
					<div class="scroll-view-report-overflow">
						<p></p>
					</div>
				</div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal-detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width:1200px;">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body forces-overflow" id="modal-btb">
					<div class="scrolls-overflow">
						<p></p>
					</div>
				</div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
	$(document).ready(function(){
		listbtb();
	});
	
	function btb_add(){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('btb/btb_add/');?>');
		$('#panel-modal  .panel-title').html('<i class="fa fa-plus"></i> Add BTB');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function btb_report(){
		$('#panel-modal-report').removeData('bs.modal');
		$('#panel-modal-report  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal-report  .panel-body').load('<?php echo base_url('btb/btb_report/');?>');
		$('#panel-modal-report  .panel-title').html('<i class="fa fa-search"></i> Filter Report BTB');
		$('#panel-modal-report').modal({backdrop:'static',keyboard:false},'show');
	}

	function btb_view_report(){
		$('#panel-modal-view-report').removeData('bs.modal');
		$('#panel-modal-view-report  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal-view-report  .panel-body').load('<?php echo base_url('btb/btb_view_report/');?>');
		$('#panel-modal-view-report  .panel-title').html('<i class="fa fa-file-pdf-o"></i> View Report BTB');
		$('#panel-modal-view-report').modal({backdrop:'static',keyboard:false},'show');
	}

	function btb_detail(id_btb){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('btb/btb_detail');?>'+"/"+id_btb);
		$('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Detail Barang');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function btb_edit(id_btb){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('btb/btb_edit');?>'+"/"+id_btb);
		$('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Detail Barang');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	// function btb_delete(id_btb){
	// 	$('#panel-modal').removeData('bs.modal');
	// 	$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	// 	$('#panel-modal  .panel-body').load('<?php echo base_url('btb/btb_detail');?>'+"/"+id_btb);
	// 	$('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Detail Barang');
	// 	$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	// }

	function btb_approve(id,id_po_spb){
		swal({
			title: 'Silahkan Pilih Status',
			text: 'Approve Jurnal',
			input: 'select',
			inputClass: 'form-control',
			inputPlaceholder: 'Please Select',
			inputOptions: {
			  '1' : 'Approve',
			  '2' : 'Reject'
			},
			inputValidator: (value) => {
				return new Promise((resolve) => {
					if (value === '') {
						resolve('Pilih Status!')
					} else {
						resolve()
					}
				})
			}
		}).then(function (value) {
			var datapost = {
				"id_btb"   	:   id,
				"id_po_spb" :   id_po_spb,
				"status"  	:   value
			};
			
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('btb/approve_btb');?>",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
					if(response.success == true) {
					   swal({
							title: 'Apakah anda yakin!',
							text: response.title,
							type: 'warning',
							showCancelButton: true,
							confirmButtonColor: '#3085d6',
							cancelButtonColor: '#d33',
							confirmButtonText: 'Ok'
						}).then(function () {
							swal("Success!", response.message, "info");
							window.location.href = "<?php echo base_url('btb');?>";
						})
					}else{
						swal("Warning!", response.message, "info");
					}
				}
			});
		})
	}

	function listbtb(){
		$("#listbtb").DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'btb/lists/';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false,
			"destroy": true,
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip',
			"initComplete": function(){
				var element =
					'<div class="btn-group pull-left">'+
						'<a class="btn btn-primary" onClick="btb_add()">'+
							'<i class="fa fa-plus"></i> Add BTB'+
						'</a>'+
					'</div>'+
					'<div class="btn-group pull-left">'+
						'<a class="btn btn-success" onClick="btb_report()">'+
							'<i class="fa fa-file-pdf-o"></i> Cetak Laporan BTB'+
						'</a>'+
					'</div>';
				$("div.toolbar").prepend(element);
			},
			"columnDefs": [{
				targets: [0],
				className: 'dt-body-center',
				width:	15
			},{
				targets: [1],
				className: 'dt-body-left',
				width:	150
			},{
				targets: [2],
				className: 'dt-body-left',
				width:	200
			},{
				targets: [3],
				className: 'dt-body-center',
				width:	150
			},{
				targets: [4,5],
				className: 'dt-body-center',
				width:	60
			},{
				targets: [6],
				className: 'dt-body-center',
				width:	70
			}]
		});
	}
</script>