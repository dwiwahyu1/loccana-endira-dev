	<style>
		#loading-us{display:none}
		#tick{display:none}

		#loading-mail{display:none}
		#cross{display:none}
		.dt-body-center{text-align:center;}
		.dt-body-left{text-align:left;}
		.dt-body-right{text-align:right;}
		tbody .dt-body-left{text-align: left;}
		tbody .dt-body-right{text-align: right;}
		#listdetailbtb {
			counter-reset: rowNumber;
		}

		#listdetailbtb tr > td:first-child {
			counter-increment: rowNumber;
		}

		#listdetailbtb tr td:first-child::before {
			content: counter(rowNumber);
			min-width: 1em;
			margin-right: 0.5em;
		}
	</style>
	
<div class="row">
	<div class="col-md-12">
		<div class="pull-right">
			<a class="btn btn-icon waves-effect waves-light btn-success m-b-5" data-toggle="tooltip" data-placement="top" title="Export to Excel" id="export_excel">
				<i class="fa fa-file-excel-o"></i>
			</a>
			<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download PDF" id="btn_download">
				<i class="fa fa-download"></i>
			</a>
		</div>
	</div>
</div>

<div class="row" style="display: none;">
	<div class="col-md-12" style="display: none;">
		<div class="col-md-2 logo-place">
			<img src="<?php echo site_url(); ?>assets/images/logo-celebit.jpg" alt="logo-celebit">
		</div>
		<div class="col-md-10">
			<div class="col-md-12 titleReport">
				<h1 id="titleCelebit">CELEBIT</h1>
				<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
				<h4 id="titleAlamat">BANDUNG FACTORY : JL.BUAH DUA RT.01/RW.04 RANCAEKEK - BANDUNG-INDONESIA</h4>
				<h4 id="titleTlp">TEL 62-22-7798 561/7798 542, FAX : 62-22-7798 562 E-MAIL : celebit@melsa.net.id</h4>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="row">
			<label class="col-md-2">No BTB :</label>
			<label class="col-md-10"><?php if(isset($btb[0]['no_btb'])){echo $btb[0]['no_btb'];}?></label>
		</div>

		<div class="row">
			<label class="col-md-2">Tanggal BTB :</label>
			<label class="col-md-10"><?php if(isset($btb[0]['tanggal_btb'])){echo $tanggal_btb;}?></label>
		</div>

		<div class="row">
			<label class="col-md-2">Type BTB :</label>
			<label class="col-md-10"><?php if(isset($btb[0]['type_btb'])){echo $btb[0]['type_btb'];}?></label>
		</div>
		
	</div>
	<hr>
	
	<div class="col-md-12">
		<div class="row">
			<label class="col-md-12" for="nama">Daftar Terima Barang :</label>
			<div class="col-md-12">
				<input type="hidden" id="id_btb" name="id_btb" class="form-control col-md-7 col-xs-12" placeholder="ID BTB"  value="<?php if(isset($btb[0]['id_btb'])){echo $btb[0]['id_btb'];}?>" readonly>
			</div>
		</div>
		
		<div class="row">
			<table id="listdetailbtb" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>No</th>
						<th class="text-center">No PO</th>
						<th class="text-center">Kode Barang</th>
						<th class="text-center">Nama Barang</th>
						<th class="text-center">Satuan</th>
						<th class="text-center">Qty</th>
						<th class="text-center">Qty Diterima</th>
						<th class="text-center">Keterangan</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>
	

<script type="text/javascript">
$(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();
	listdetailbtb();
});

function listdetailbtb(){
	$("#listdetailbtb").dataTable({
		"processing": true,
		"serverSide": true,
		"ajax": "<?php echo base_url().'btb/lists_detail/'.$btb[0]['id_btb'];?>",
		"searchDelay": 700,
		"responsive": true,
		"lengthChange": false,
		"destroy": true,
		"info": false,
		"bSort": false,
		"dom": 'l<"toolbar">frtip',
		"columnDefs": [{
			targets: [0],
			className: 'dt-body-center',
			width:	25
		},{
			targets: [1],
			className: 'dt-body-left',
			width:	175
		},{
			targets: [4],
			className: 'dt-body-center',
			width:	80
		},{
			targets: [5],
			className: 'dt-body-right',
			width:	100
		},{
			targets: [6],
			className: 'dt-body-right',
			width:	100
		},{
			targets: [7],
			className: 'dt-body-left',
			width:	150
		}]
	});
}

function toDataURL(url, callback) {
	var xhr = new XMLHttpRequest();
	xhr.onload = function() {
		var reader = new FileReader();
		reader.onloadend = function() {
			callback(reader.result);
		}
		reader.readAsDataURL(xhr.response);
	};
	xhr.open('GET', url);
	xhr.responseType = 'blob';
	xhr.send();
}

toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
	dataImage = dataUrl;
})

$('#btn_download').on('click', function() {
	var doc = new jsPDF("a4");
	var imgData = dataImage;
	var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
	var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

	doc.setTextColor(50);
	doc.addImage(imgData, 'JPEG', 5, 5, 25, 25)
	doc.setFontSize(12);
	doc.text($('#titleCelebit').html(), 33, 10, 'left');
	doc.setFontSize(10);
	doc.text($('#titlePerusahaan').html(), 33, 15, 'left');
	doc.setFontSize(11);
	doc.text($('#titleAlamat').html(), 33, 20, 'left');
	doc.setFontSize(11);
	doc.text($('#titleTlp').html(), 33, 25, 'left');

	doc.setFontSize(16);
	doc.setFontStyle('helvetica','arial','sans-serif','bold');
	doc.line(pageWidth - 5, 30, 5, 30);
	doc.text('BUKTI TERIMA BARANG', pageWidth / 2, 40, 'center');

	doc.setFontSize(12);
	doc.text('No BTB', 5, 55, 'left');
	doc.text(': <?php if(isset($btb[0]['no_btb'])){echo $btb[0]['no_btb'];}?>', 30, 55, 'left');
	
	doc.text('Tanggal BTB', 5, 60, 'left');
	doc.text(': <?php if(isset($btb[0]['tanggal_btb'])){echo $tanggal_btb;}?>', 30, 60, 'left');
	
	doc.text('Typpe BTB', 5, 65, 'left');
	doc.text(': <?php if(isset($btb[0]['type_btb'])){echo $btb[0]['type_btb'];}?>', 30, 65, 'left');
	
	doc.text('Tanggal Terima', pageWidth - 65, 70, 'left');
	doc.text(': <?php if(isset($btb[0]['tanggal_btb'])){echo date('d-M-Y', strtotime($tanggal_btb));}?>', pageWidth - 35, 70, 'left');

	doc.autoTable({
		html 			: '#listdetailbtb',
		theme			: 'plain',
		styles			: {
			fontSize 	: 8, 
			lineColor	: [116, 119, 122],
			lineWidth	: 0.1,
			cellWidth 	: 'auto',
			
		},
		margin 			: 4.5,
		tableWidth		: (pageWidth - 10),
		headStyles		: {
			valign		: 'middle', 
			halign		: 'center',
		},
		columns 		: [0,1,2,3,4,5,6],
		didParseCell	: function (data) {
			if(data.table.foot[0]) {
				if(data.table.foot[0].cells[0]) {
					data.table.foot[0].cells[0].styles.halign = 'right';
				}
				if(data.table.foot[0].cells[8]) {
					data.table.foot[0].cells[8].styles.halign = 'right';
				}
				if(data.table.foot[0].cells[9]) {
					data.table.foot[0].cells[9].colSpan = 1;
				}
			}
		},
		columnStyles	: {
			0: {halign: 'center'},
			1: {halign: 'left'},
			2: {halign: 'left'},
			3: {halign: 'left'},
			4: {halign: 'right'},
			5: {halign: 'right'},
			6: {halign: 'left'}
		},
		rowPageBreak	: 'auto',
		showHead 		: 'firstPage',
		showFoot		: 'lastPage',
		startY			: 75
	});

	/*doc.text('Delivery Date', 5, doc.autoTable.previous.finalY + 5, 'left');
	doc.text(': <?php if(isset($po[0]['temp_delivery_date'])) echo date('d-M-Y', strtotime($po[0]['temp_delivery_date'])); ?>', 40, doc.autoTable.previous.finalY + 5, 'left');

	doc.text('Destination', 5, doc.autoTable.previous.finalY + 10, 'left');
	doc.text(': <?php if(isset($po[0]['temp_delivery_date'])) echo date('d-M-Y', strtotime($po[0]['temp_delivery_date'])); ?>', 40, doc.autoTable.previous.finalY + 10, 'left');

	doc.text('Term of Payment', 5, doc.autoTable.previous.finalY + 15, 'left');
	doc.text(': <?php if(isset($po[0]['term_of_payment'])) echo $po[0]['term_of_payment']; ?>', 40, doc.autoTable.previous.finalY + 15, 'left');*/

	var startYPO = doc.autoTable.previous.finalY + 20;

	doc.autoTable({
		theme			: 'plain',
		styles			: {fontSize : 8, lineColor : [116, 119, 122], lineWidth : 0.1, cellWidth : 'auto'},
		margin 			: 4.5,
		tableWidth		: 30,
		headStyles		: {valign : 'middle', halign : 'center'},
		head: [['Received by.']],
		body: [['']],
		columnStyles	: {
			0: {minCellHeight: 20}
		},
		rowPageBreak	: 'auto',
		showHead 		: 'firstPage',
		showFoot		: 'lastPage',
		startY			: startYPO + 3
	});

	doc.autoTable({
		theme			: 'plain',
		styles			: {fontSize : 8, lineColor : [116, 119, 122], lineWidth : 0.1, cellWidth : 'auto'},
		margin 			: {left: pageWidth - 105.5},
		tableWidth		: 100,
		headStyles		: {valign : 'middle', halign : 'center'},
		head 			: [['prepared by,', 'Approved by,', 'Approved by,']],
		body 			: [['', '', '']],
		columnStyles	: {
			0: {minCellHeight: 20, cellWidth: 5}
		},
		rowPageBreak	: 'auto',
		showHead 		: 'firstPage',
		showFoot		: 'lastPage',
		startY			: startYPO + 3
	});

	doc.save('BTB Report_<?php if(isset($btb[0]['no_btb'])) echo $btb[0]['no_btb'];?>.pdf');
})

$('#btn_download2').on('click', function() {
	var doc = new jsPDF("a4");
	var imgData = dataImage;
	var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
	var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

	doc.setTextColor(50);
	doc.addImage(imgData, 'JPEG', 5, 5, 25, 25)
	doc.setFontSize(12);
	doc.text($('#titleCelebit').html(), 33, 10, 'left');
	doc.setFontSize(10);
	doc.text($('#titlePerusahaan').html(), 33, 15, 'left');
	doc.setFontSize(11);
	doc.text($('#titleAlamat').html(), 33, 20, 'left');
	doc.setFontSize(11);
	doc.text($('#titleTlp').html(), 33, 25, 'left');

	doc.setFontSize(16);
	doc.setFontStyle('helvetica','arial','sans-serif','bold');
	doc.line(pageWidth - 5, 30, 5, 30);
	doc.text('BUKTI TERIMA BARANG', pageWidth / 2, 40, 'center');

	doc.setFontSize(12);
	doc.text('No BTB', 5, 55, 'left');
	doc.text(': <?php if(isset($btb[0]['no_btb'])){echo $btb[0]['no_btb'];}?>', 30, 55, 'left');
	
	doc.text('Tanggal BTB', 5, 60, 'left');
	doc.text(': <?php if(isset($btb[0]['tanggal_btb'])){echo $tanggal_btb;}?>', 30, 60, 'left');
	
	doc.text('Typpe BTB', 5, 65, 'left');
	doc.text(': <?php if(isset($btb[0]['type_btb'])){echo $btb[0]['type_btb'];}?>', 30, 65, 'left');
	
	doc.text('Tanggal Terima', pageWidth - 65, 70, 'left');
	doc.text(': <?php if(isset($btb[0]['tanggal_btb'])){echo date('d-M-Y', strtotime($tanggal_btb));}?>', pageWidth - 35, 70, 'left');

	doc.autoTable({
		html 			: '#listdetailbtb',
		theme			: 'plain',
		styles			: {
			fontSize 	: 8, 
			lineColor	: [116, 119, 122],
			lineWidth	: 0.1,
			cellWidth 	: 'auto',
			
		},
		margin 			: 4.5,
		tableWidth		: (pageWidth - 10),
		headStyles		: {
			valign		: 'middle', 
			halign		: 'center',
		},
		columns 		: [0,1,2,3,4,5,6],
		didParseCell	: function (data) {
			if(data.table.foot[0]) {
				if(data.table.foot[0].cells[0]) {
					data.table.foot[0].cells[0].styles.halign = 'right';
				}
				if(data.table.foot[0].cells[8]) {
					data.table.foot[0].cells[8].styles.halign = 'right';
				}
				if(data.table.foot[0].cells[9]) {
					data.table.foot[0].cells[9].colSpan = 1;
				}
			}
		},
		columnStyles	: {
			0: {halign: 'center'},
			1: {halign: 'left'},
			2: {halign: 'left'},
			3: {halign: 'left'},
			4: {halign: 'right'},
			5: {halign: 'right'},
			6: {halign: 'left'}
		},
		rowPageBreak	: 'auto',
		showHead 		: 'firstPage',
		showFoot		: 'lastPage',
		startY			: 75
	});

	/*doc.text('Delivery Date', 5, doc.autoTable.previous.finalY + 5, 'left');
	doc.text(': <?php if(isset($po[0]['temp_delivery_date'])) echo date('d-M-Y', strtotime($po[0]['temp_delivery_date'])); ?>', 40, doc.autoTable.previous.finalY + 5, 'left');

	doc.text('Destination', 5, doc.autoTable.previous.finalY + 10, 'left');
	doc.text(': <?php if(isset($po[0]['temp_delivery_date'])) echo date('d-M-Y', strtotime($po[0]['temp_delivery_date'])); ?>', 40, doc.autoTable.previous.finalY + 10, 'left');

	doc.text('Term of Payment', 5, doc.autoTable.previous.finalY + 15, 'left');
	doc.text(': <?php if(isset($po[0]['term_of_payment'])) echo $po[0]['term_of_payment']; ?>', 40, doc.autoTable.previous.finalY + 15, 'left');*/

	var startYPO = doc.autoTable.previous.finalY + 20;

	doc.autoTable({
		theme			: 'plain',
		styles			: {fontSize : 8, lineColor : [116, 119, 122], lineWidth : 0.1, cellWidth : 'auto'},
		margin 			: 4.5,
		tableWidth		: 30,
		headStyles		: {valign : 'middle', halign : 'center'},
		head: [['Received by.']],
		body: [['']],
		columnStyles	: {
			0: {minCellHeight: 20}
		},
		rowPageBreak	: 'auto',
		showHead 		: 'firstPage',
		showFoot		: 'lastPage',
		startY			: startYPO + 3
	});

	doc.autoTable({
		theme			: 'plain',
		styles			: {fontSize : 8, lineColor : [116, 119, 122], lineWidth : 0.1, cellWidth : 'auto'},
		margin 			: {left: pageWidth - 105.5},
		tableWidth		: 100,
		headStyles		: {valign : 'middle', halign : 'center'},
		head 			: [['prepared by,', 'Approved by,', 'Approved by,']],
		body 			: [['', '', '']],
		columnStyles	: {
			0: {minCellHeight: 20, cellWidth: 5}
		},
		rowPageBreak	: 'auto',
		showHead 		: 'firstPage',
		showFoot		: 'lastPage',
		startY			: startYPO + 3
	});

	doc.save('BTB Report_<?php if(isset($btb[0]['no_btb'])) echo $btb[0]['no_btb'];?>.pdf');
})

$('#export_excel').on('click', function() {
	window.open("<?php echo base_url().'btb/export_excel/'.$btb[0]['id_btb'];?>");
})
</script>
