	<style>
		#loading-us{display:none}
		#tick{display:none}

		#loading-mail{display:none}
		#cross{display:none}
		.dt-body-center{text-align:center;}
		.dt-body-left{text-align:left;}
		.dt-body-right{text-align:right;}
		tbody .dt-body-left{text-align: left;}
		tbody .dt-body-right{text-align: right;}
		/*#listdetailbtb {
			counter-reset: rowNumber;
		}

		#listdetailbtb tbody tr > td:first-child {
			counter-increment: rowNumber;
		}

		#listdetailbtb tbody tr td:first-child::before {
			content: counter(rowNumber);
			min-width: 1em;
			margin-right: 0.5em;
		}*/
		.margin-bawah{
			margin-bottom: 5px;
		}
		.margin-bawah-akhir{
			margin-bottom: 30px;
		}
		.table .text-tengah{
			vertical-align: middle;
		}
		.table .bawah-kanan{
			border-bottom-style: hidden;
			border-right-style: hidden;
		}
		.table .bawah-kiri{
			border-bottom-style: hidden;
			border-left-style: hidden;
		}
		.table .hilang-border-bawah{
			border-bottom-style: hidden;
		}
		.table .hilang-border-kanan{
			border-right-style: hidden;
		}
		.table .lebar-bawah{
			height: 50px;
		}
	</style>
	
<div class="row">
	<div class="col-md-12">
		<div class="col-md-12 text-center">
			<div class="col-md-12">
				<h3>BUKTI TERIMA BARANG</h3>
				<h4><?php if(isset($btb[0]['type_btb'])){echo strtoupper($btb[0]['type_btb']);}?></h4>
			</div>
		</div>
		<div class="pull-right">
			<a class="btn btn-icon waves-effect waves-light btn-success m-b-5" data-toggle="tooltip" data-placement="top" title="Export to Excel" id="export_excel">
				<i class="fa fa-file-excel-o"></i>
			</a>
			<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download PDF" id="btn_download">
				<i class="fa fa-download"></i>
			</a>
		</div>
	</div>
</div>

<div class="row" style="display: none;">
	<div class="col-md-12" style="display: none;">
		<div class="col-md-10">
			<div class="col-md-12 titleReport">
				<h1 id="titleMenu">BUKTI TERIMA BARANG</h1>
				<h2 id="titleDetail"><?php if(isset($btb[0]['type_btb'])){echo strtoupper($btb[0]['type_btb']);}?></h2>
			</div>
		</div>
	</div>
</div>

<hr>

<div class="row">
	<div class="col-md-12">
		<div class="row margin-bawah">
			<div class="col-md-6">
				<label class="col-md-6 col-sm-6 col-xs-12" style="" id="titleSupplier">Diterima dari Supplier</label>
				<label class="col-md-6 col-sm-6 col-xs-12" style="" id="namaSupplier">: <?php if(isset($btb[0]['name_eksternal'])){echo $btb[0]['name_eksternal'];}?></label>
			</div>
			<div class="col-md-6">
				<label class="col-md-2 col-sm-2 col-xs-12" style="" id="titleNoBTB">No. BTB</label>
				<label class="col-md-10 col-sm-10 col-xs-12" style="" id="noBTB">: <?php if(isset($btb[0]['no_btb'])){echo $btb[0]['no_btb'];}?></label>
			</div>
		</div>
		<div class="row margin-bawah">
			<div class="col-md-6">
				<div class="item form-group">
					<label class="col-md-6 col-sm-6 col-xs-12" style="" id="titleKendaraan">Kendaraan</label>
					<label class="col-md-6 col-sm-6 col-xs-12" style="" id="kendaraan">: <?php if(isset($btb[0]['no_pol'])){ echo trim($btb[0]['no_pol']);}?></label>
				</div>
			</div>
			<div class="col-md-6">
				<label class="col-md-2 col-sm-2 col-xs-12" style="" id="titleTanggal">TGL</label>
				<label class="col-md-10 col-sm-10 col-xs-12" style="" id="tanggal">: <?php if(isset($btb[0]['tanggal_btb'])){ $date = date_create($btb[0]['tanggal_btb']); echo date_format($date,'d-m-Y');}?></label>
			</div>
		</div>
		<div class="row margin-bawah-akhir">
			<div class="col-md-6">
				<div class="item form-group">
					<label class="col-md-6 col-sm-6 col-xs-12" style="" id="titleSuratJalan">No. Surat Jalan/Faktur Supplier</label>
					<label class="col-md-6 col-sm-6 col-xs-12" style="" id="suratJalan">: <?php if(isset($btb[0]['faktur_supplier'])){ echo trim($btb[0]['faktur_supplier']);}?></label>
				</div>
			</div>
		</div>
	</div>
	
	<div class="col-md-12">
		<hr>
	</div>
	
	<div class="col-md-12">
		<div class="row">
			<table id="listdetailbtb" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th class="text-center text-tengah" rowspan="2">No</th>
						<th class="text-center text-tengah" rowspan="2">JENIS BARANG</th>
						<th class="text-center text-tengah" rowspan="2">GUDANG QTY</th>
						<th class="text-center" colspan="3">BAG. PEMBELIAN</th>
						<th class="text-center text-tengah" rowspan="2">No. PR</th>
						<th class="text-center text-tengah" rowspan="2">No. PO</th>
						<th class="text-center text-tengah" rowspan="2">CODE BARANG</th>
						<th class="text-center text-tengah" rowspan="2">KET</th>
					</tr>
					<tr>
						<th class="text-center">HRG SAT</th>
						<th class="text-center">PPN/ Disc</th>
						<th class="text-center">HRG TOTAL</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
				<tfoot>
					<tr>
						<td class="text-center bawah-kiri" colspan="3"></td>
						<td class="text-center">TOTAL</td>
						<td class="text-center"></td>
						<td class="text-center" id="total_amount"></td>
						<td class="text-center bawah-kanan" colspan="4"></td>
					</tr>
				</tfoot>
			</table>
			
			<table id="approval" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th class="text-center">CATATAN :</th>
						<th class="text-center">KEUANGAN</th>
						<th class="text-center">Diperiksa Oleh,</th>
						<th class="text-center" colspan="2">GUDANG</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-center" rowspan="4"></td>
						<td class="hilang-border-bawah" rowspan="">No. BKK : </td>
						<td class="text-center hilang-border-bawah" rowspan="3"></td>
						<td class="text-center lebar-bawah" rowspan="4">KA. GUDANG TGL</td>
						<td class="text-center lebar-bawah" rowspan="4">PENERIMA TGL</td>
					</tr>
					<tr>
						<td class="hilang-border-bawah" rowspan="">BANK-GB/CH : </td>
					</tr>
					<tr>
						<td class="hilang-border-bawah" rowspan="">TGL : </td>
					</tr>
					<tr>
						<td class="" rowspan="">JML : </td>
						<td class="text-center" rowspan="">PEMBELIAN</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td class="" colspan="5">Note :   White : Fin, Pink : Acct, Yellow : Purch, Green : Exim, Blue : Store</td>
					</tr>
				</tfoot>
			</table>
			<div class="row">
				<div class="col-md-12">
					<label class="control-label pull-right" id="kode_report">F_MCI-002</label>
				</div>
				<div class="col-md-12">
					<label class="control-label pull-right" id="kode_report_detail">REV : 00</label>
				</div>
			</div>
		</div>
	</div>
</div>
	

<script type="text/javascript">
var t_btb_detail;

$(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();
	dt_btb();
});

function dt_btb(){
	t_btb_detail = $("#listdetailbtb").dataTable({
		"processing": true,
		"searching": false,
		"responsive": true,
		"lengthChange": false,
		"ajax": {
			"type" : "GET",
			"url" : "<?php echo base_url().'btb/get_lists_detail/'.$btb[0]['id_btb'];?>",
			"dataSrc": function(response) {
				console.log(response);
				$('#total_amount').html(formatCurrencyComa(response.total));
				return response.data;
			}
		},
		"info": false,
		"bSort": false,
		"columnDefs": [{
			"targets": [0],
			"searchable": false,
			"className": 'dt-body-center',
			"width":	10
		},{
			"targets": [1],
			"searchable": false,
			"className": 'dt-body-left',
			"width":	175
		},{
			"targets": [2],
			"searchable": false,
			"className": 'dt-body-right',
			"width":	80
		},{
			"targets": [3],
			"searchable": false,
			"className": 'dt-body-left',
			"width":	80
		},{
			"targets": [4],
			"searchable": false,
			"className": 'dt-body-right',
			"width":	80
		},{
			"targets": [5],
			"searchable": false,
			"className": 'dt-body-right',
			"width":	80
		},{
			"targets": [6],
			"searchable": false,
			"className": 'dt-body-left',
			"width":	70
		},{
			"targets": [7],
			"searchable": false,
			"className": 'dt-body-left',
			"width":	70
		},{
			"targets": [8],
			"searchable": false,
			"className": 'dt-body-left',
			"width":	90
		},{
			"targets": [9],
			"searchable": false,
			"className": 'dt-body-left',
			"width":	90
		}]
	});
}

function toDataURL(url, callback) {
	var xhr = new XMLHttpRequest();
	xhr.onload = function() {
		var reader = new FileReader();
		reader.onloadend = function() {
			callback(reader.result);
		}
		reader.readAsDataURL(xhr.response);
	};
	xhr.open('GET', url);
	xhr.responseType = 'blob';
	xhr.send();
}

toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
	dataImage = dataUrl;
})

$('#btn_download').on('click', function() {
	var doc = new jsPDF("a4");
	var imgData = dataImage;
	var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
	var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

	doc.setFontSize(12);
	doc.text($('#titleMenu').html(), 105, 10, 'center');
	doc.setFontSize(10);
	doc.text($('#titleDetail').html(), 105, 15, 'center');
	doc.line(pageWidth - 5, 20, 5, 20);

	doc.setFontSize(16);
	doc.setFontStyle('helvetica','arial','sans-serif','bold');

	doc.setFontSize(12);
	doc.text('Diterima dari Supplier', 5, 30, 'left');
	doc.text($('#namaSupplier').html(), 70, 30, 'left');
	
	doc.text('No. BTB', 160, 30, 'left');
	doc.text($('#noBTB').html(), 180, 30, 'left');
	
	doc.text('Kendaraan', 5, 35, 'left');
	doc.text($('#kendaraan').html(), 70, 35, 'left');
	
	doc.text('TGL', 160, 35, 'left');
	doc.text($('#tanggal').html(), 180, 35, 'left');
	
	doc.text('No. Surat Jalan/ Faktur Suppplier', 5, 40, 'left');
	doc.text($('#suratJalan').html(), 70, 40, 'left');

	doc.autoTable({
		html 			: '#listdetailbtb',
		theme			: 'plain',
		styles			: {
			fontSize 	: 8, 
			lineColor	: [116, 119, 122],
			lineWidth	: 0.1,
			cellWidth 	: 'auto',
			
		},
		margin 			: 4.5,
		tableWidth		: (pageWidth - 10),
		headStyles		: {
			valign		: 'middle', 
			halign		: 'center',
		},
		columns 		: [0,1,2,3,4,5,6,7,8,9],
		didParseCell	: function (data) {
			if(data.table.foot[0]) {
				if(data.table.foot[0].cells[3]) {
					data.table.foot[0].cells[3].styles.halign = 'right';
				}
				if(data.table.foot[0].cells[4]) {
					data.table.foot[0].cells[4].styles.balign = 'right';
				}
				if(data.table.foot[0].cells[5]) {
					data.table.foot[0].cells[5].styles.balign = 'right';
				}
			}
		},
		columnStyles	: {
			0: {halign: 'center'},
			1: {halign: 'center'},
			2: {halign: 'center'},
			3: {halign: 'center'},
			4: {halign: 'center'},
			5: {halign: 'center'},
			6: {halign: 'center'},
			7: {halign: 'center'},
			8: {halign: 'center'},
			9: {halign: 'center'},
		},
		rowPageBreak	: 'auto',
		showHead 		: 'firstPage',
		showFoot		: 'lastPage',
		startY			: 50
	});

	var startYPO = doc.autoTable.previous.finalY + 20;
	var startApprove = pageHeight - 150;
	console.log(startApprove);
	doc.autoTable({
		html 			: '#approval',
		theme			: 'plain',
		styles			: {
			fontSize 	: 8, 
			lineColor	: [116, 119, 122],
			lineWidth	: 0.1,
			cellWidth 	: 'auto',
			
		},
		margin 			: 4.5,
		tableWidth		: (pageWidth - 10),
		headStyles		: {
			valign		: 'middle', 
			halign		: 'center',
		},
		columns 		: [0,1,2,3,4],
		columnStyles	: {
			0: {halign: 'left'},
			1: {halign: 'left'},
			2: {halign: 'center'},
			3: {halign: 'center'},
			4: {halign: 'center'}
		},
		rowPageBreak	: 'auto',
		showHead 		: 'firstPage',
		showFoot		: 'lastPage',
		startY			: startYPO + 3
	});

	doc.text($('#kode_report').html(), 205, pageHeight -5, 'right');
	doc.text($('#kode_report_detail').html(), 205, pageHeight -10, 'right');
	
	doc.save('BTB Report_<?php if(isset($btb[0]['no_btb'])) echo $btb[0]['no_btb'];?>.pdf');
})

$('#export_excel').on('click', function() {
	window.open("<?php echo base_url().'btb/export_excel/'.$btb[0]['id_btb'];?>");
})
</script>
