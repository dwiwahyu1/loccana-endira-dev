<style>
	.changed_status{cursor:pointer;text-decoration: underline;color:#96b6e8;}
	.changed_status:hover{color:#ff8c00}
	.dt-body-center{
		text-align: center;
	}
	.dt-body-left{
		text-align: left;
	}
	.dt-body-right{
		text-align: right;
	}
	.force-overflow {height: 650px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow {min-height: 650px;}
	.force-overflow-report {height: 850px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow-report {min-height: 850px;}
	#modal-spb::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	#modal-spb::-webkit-scrollbar
	{
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-spb::-webkit-scrollbar-thumb
	{
		background-image: -webkit-gradient(linear,
		   left bottom,
		   left top,
		   color-stop(0.44, rgb(122,153,217)),
		   color-stop(0.72, rgb(73,125,189)),
		   color-stop(0.86, rgb(28,58,148)));
	}
  .changed_status{cursor:pointer;text-decoration: underline;color:#96b6e8;}
  .changed_status:hover{color:#ff8c00}
  .custom-tables, th{text-align:center;vertical-align:middle;}
  .custom-tables.align-text, th{vertical-align:middle;}
  .custom-tables.text-center {align-text: center;}
</style>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Purchase Request</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">

				<table id="listspb" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>Username Login</th>
							<th>Date</th>
							<th>Department</th>
							<th>Part Numbers</th>
							<th>Total Items</th>
							<th>Status</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>

			</div>
		</div><!-- end col -->
	</div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;; z-index: 1053">
	<div class="modal-dialog" style="width:70%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-spb">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;z-index: 1053;">
	<div class="modal-dialog" style="width:80%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-spb">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild-barang" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true" style="display: none; z-index: 1052;">
	<div class="modal-dialog" style="width:70%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-spb">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-report-spb" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel4" aria-hidden="true" style="display: none;  z-index: 1050;">
	<div class="modal-dialog modal-lg" role="document" style="width:90%;">
		<div class="modal-content p-0 b-0" style="min-height: 100%;">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow-report" id="modal-spb">
					<div class="scroll-overflow-report">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
function add_spb(){
	$('#panel-modal').removeData('bs.modal');
	$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal  .panel-body').load('<?php echo base_url('spb/add');?>');
	$('#panel-modal  .panel-title').html('<i class="fa fa-plus"></i> Add Purchase Request');
	$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}

function add_item(){
	$('#panel-modalchild').removeData('bs.modal');
	$('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modalchild  .panel-body').load('<?php echo base_url('spb/add_item');?>');
	$('#panel-modalchild  .panel-title').html('<i class="fa fa-plus"></i> Tambah Barang');
	$('#panel-modalchild').modal({backdrop:'static',keyboard:false},'show');
}

function add_new_item(){
	$('#panel-modalchild-barang').removeData('bs.modal');
	$('#panel-modalchild-barang  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modalchild-barang  .panel-body').load('<?php echo base_url('spb/add_new_item');?>');
	$('#panel-modalchild-barang  .panel-title').html('<i class="fa fa-plus"></i> Tambah Barang Baru');
	$('#panel-modalchild-barang').modal({backdrop:'static',keyboard:false},'show');
}

function edit_spb(idspb){
	var no_spb = idspb.replace(" ","%20");
	
	$('#panel-report-spb').removeData('bs.modal');
	$('#panel-report-spb  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-report-spb  .panel-body').load('<?php echo base_url('spb/edit/');?>'+"/"+no_spb);
	$('#panel-report-spb  .panel-title').html('<i class="fa fa-pencil"></i> Edit Purchase Request');
	$('#panel-report-spb').modal({backdrop:'static',keyboard:false},'show');
}

function edit_item(){
	$('#panel-modalchild').removeData('bs.modal');
	$('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modalchild  .panel-body').load('<?php echo base_url('spb/edit_item');?>');
	$('#panel-modalchild  .panel-title').html('<i class="fa fa-plus"></i> Tambah Barang');
	$('#panel-modalchild').modal({backdrop:'static',keyboard:false},'show');
}

function approval_spb(idspb){
	no_spb = idspb.replace(" ","%20");
	
	$('#panel-report-spb').removeData('bs.modal');
	$('#panel-report-spb  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-report-spb  .panel-body').load('<?php echo base_url('spb/edit_approval');?>'+"/"+no_spb);
	$('#panel-report-spb  .panel-title').html('<i class="fa fa-pencil"></i> Edit Purchase Request status');
	$('#panel-report-spb').modal({backdrop:'static',keyboard:false},'show');
}

function detail_spb(idspb){
	no_spb = idspb.replace(" ","%20");
	
	$('#panel-report-spb').removeData('bs.modal');
	$('#panel-report-spb  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-report-spb  .panel-body').load('<?php echo base_url('spb/detail');?>'+"/"+no_spb);
	$('#panel-report-spb  .panel-title').html('<i class="fa fa-search"></i> Detail Purchase Request');
	$('#panel-report-spb').modal({backdrop:'static',keyboard:false},'show');
}

function print_spb(idspb){
	no_spb = idspb.replace(" ","%20");
	
	$('#panel-report-spb').removeData('bs.modal');
	$('#panel-report-spb  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-report-spb  .panel-body').load('<?php echo base_url('spb/detail');?>'+"/"+no_spb);
	$('#panel-report-spb  .panel-title').html('<i class="fa fa-search"></i> Report Purchase Request');
	$('#panel-report-spb').modal({backdrop:'static',keyboard:false},'show');
}

function detail_status(idspb){
	$('#panel-modal').removeData('bs.modal');
	$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal  .panel-body').load('<?php echo base_url('spb/detail_status');?>'+"/"+idspb);
	$('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Detail Purchase Request Status');
	$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}

function deletespb(idspb){
	console.log(idspb);

	no_spb = idspb.replace(" ","%20");
	swal({
		title: 'Yakin akan Menghapus ?',
		text: 'data tidak dapat dikembalikan bila sudah dihapus !',
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Ya',
		cancelButtonText: 'Tidak'
	}).then(function () {
		var datapost  = {
			"no_spb"  :   no_spb,
			'idspb'   : idspb
		};

		$.ajax({
			type:'POST',
			url: "<?php echo base_url().'spb/delete_spb';?>",
			data:JSON.stringify(datapost),
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if(response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						listspb();
						// window.location.href = "<?php echo base_url('spb');?>";
					});
				}else{
					swal("Perhatian !!!", response.message,"error");
					// window.location.href = "<?php echo base_url('spb');?>";
				}
			}
		});
	});
}
function delRow(rowParent) {
	t_spb.row(rowParent).remove().draw(false);
}
	
function delete_spb_item(idspb, thisval){
	var rowParent = $(thisval).parents('tr');
	
	if(idspb != 0) {
		swal({
			title: 'Yakin akan Menghapus ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost={
				"idspb"  :   idspb
			};

			$.ajax({
				type:'POST',
				url: "<?php echo base_url().'spb/delete_spb_item';?>",
				data:JSON.stringify(datapost),
				cache:false,
				contentType: false,
				processData: false,
				success: function(response) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						t_spb.row($(thisval).parents('tr')).remove().draw();
						listspb();
					})
				}
			});
		});
	}else {
		t_spb.row($(thisval).parents('tr')).remove().draw();
		listspb();
	}
}

function listspb(){
	$("#listspb").dataTable({
		"processing": true,
		"serverSide": true,
		"ajax": "<?php echo base_url().'spb/lists/';?>",
		"searchDelay": 700,
		"responsive": true,
		"lengthChange": false,
		"destroy": true,
		"info": false,
		"bSort": false,		
		"dom": 'l<"toolbar">Bfrtip',
		buttons: [
						'excel'
        ],
		"initComplete": function(){
			var element = '<div class="btn-group pull-left">';
				element += '  <a class="btn btn-primary" onClick="add_spb()">';
				element += '    <i class="fa fa-plus"></i> Add Purchase Request';
				element += '  </a>';
				element += '</div>';
			$("div.toolbar").prepend(element);
		},
		"columnDefs": [{
			"targets": [6],
			"searchable": false,
			"className": 'dt-body-center',
			"width": 120
		},{
			"targets": [7],
			"searchable": false,
			"className": 'dt-body-center',
			"width": 200
		}],
		/*createdRow: function(row, data, dataIndex){
			if(data[8] !== ''){
				$('td:eq(7)', row).attr('colspan', 4);
				$('td:eq(7)', row).attr('align', 'center');
				
				$('td:eq(8)', row).css('border', 'none');
				$('td:eq(8)', row).css('display', 'none');
				$('td:eq(9)', row).css('border', 'none');
				$('td:eq(9)', row).css('display', 'none');
				$('td:eq(10)', row).css('border', 'none');
				$('td:eq(10)', row).css('display', 'none');

				// Update cell data
				this.api().cell($('td:eq(7)', row)).data(data[7]);
			}
		}*/
	});
}

$(document).ready(function(){
	listspb();
});
</script>