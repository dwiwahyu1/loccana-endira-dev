  <style>
		#loading-us{display:none}
		#tick{display:none}

		#loading-mail{display:none}
		#cross{display:none}
		.add_item{cursor:pointer;#96b6e8;padding-top: 6px;}
		.add_item:hover{color:#ff8c00}
		.add_item a:hover{color:#ff8c00}
  </style>
  <form class="form-horizontal form-label-left" id="add_spb" role="form" action="<?php echo base_url('spb/add_spb');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

		<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
		<br>   
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">PR Date
				<span class="required">
					<sup>*</sup>
				</span>
			</label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<div class="input-group date">
				   <input placeholder="PR Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="spb_date" name="spb_date" required="required" value="<?php echo date('Y-m-d');?>" autocomplete="off">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-th"></span>
					</div>
				</div>
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Username Login 
			  <span class="required">
				<sup>*</sup>
			  </span>
			</label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input data-parsley-maxlength="255" type="text" id="cust_name" name="cust_name" class="form-control col-md-7 col-xs-12" placeholder="Customer Name" autocomplete="off" required="required" disabled="disabled" value="<?php echo $this->session->userdata['logged_in']['username'];?>">
				<input type="hidden" id="id_cust" name="id_cust" value="<?php echo $this->session->userdata['logged_in']['user_id'];?>">     
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Department 
				<span class="required">
					<sup>*</sup>
				</span>
			</label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input data-parsley-maxlength="255" type="text" id="department" name="department" class="form-control col-md-7 col-xs-12" placeholder="Department" autocomplete="off" required="required" disabled="disabled" value="<?php echo $this->session->userdata['logged_in']['name_role'];?>">
						
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Barang : </label>
			<div class="col-md-8 col-sm-6 col-xs-12 add_item" onclick="add_item()">
				<a type="button" class="btn btn-default btn-icon waves-effect waves-light m-b-5">
					<i class="fa fa-plus"></i>
				</a>
				Tambah Barang
			</div>
		</div>

		 <div class="item form-group">
			<table id="listaddedmaterial" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th style="display:none">ID SPB</th>
						<th style="display:none">NO SPB</th>
						<th>Kode Barang</th>
						<th>Nama Barang</th>
						<th>Satuan Unit</th>
						<th>Satuan</th>
						<th>Qty</th>
						<th>Qty Update</th>
						<th>Tanggal Diperlukan</th>
						<th>Tanggal Diperlukan Update</th>
						<th>Gudang ID</th>
						<th>Gudang</th>
						<th>Keterangan</th>
						<th>Option</th>
					</tr>
				</thead>
			  <tbody>
			  </tbody>
			</table>
		 </div>

		 <div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Add Purchase Request</button>
			</div>
		 </div>
  </form><!-- /page content -->

<script type="text/javascript">
var items = [];
var t_spb;
var last_items = <?php if(isset($detail) && sizeof($detail) > 0) echo sizeof($detail); else echo 0; ?>;

$(document).ready(function() {
	$('form').parsley();
	$('[data-toggle="tooltip"]').tooltip();

	$("#spb_date").datepicker({
		format: 'yyyy-mm-dd',
		autoclose: true,
		todayHighlight: true,
	});
	dtSPB();
});

function dtSPB() {
	t_spb = $("#listaddedmaterial").DataTable({
		//"ajax": "<?php echo base_url().'spb/lists/';?>",
		"responsive": true,
		"lengthChange": false,
		"paging": false,
		"destroy": true,
		"info": false,
		"bSort": false,
		"columnDefs": [{
			"targets": [0,1,4,7,9,10],
			"searchable": false,
			"visible": false,
			"className": 'dt-body-center',
			"width": 120
		}],
	});
}

function redrawTable(row) {
	var rowData = t_spb.row(row).data();

	rowData[6] = $('#qty'+row).val();
	rowData[9] = $('#tanggal_diperlukan'+row).val();

	t_spb.draw();
}

$('#add_spb').on('submit',(function(e) {
	if($('#id_cust').val() !== ''){
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();

		var	no_spb 		= $('#no_spb').val();
		var	spb_date 	= $('#spb_date').val();
		var	id_cust 	= $('#id_cust').val();
		var	departemen 	= $('#department').val();

		for(var i = 0; i < t_spb.rows().data().length;i++) {
			var rowData		= t_spb.row(i).data();

			if($(rowData[6]).val()) var qty = $(rowData[6]).val();
			else var qty = rowData[6];

			var div_tgl		= $(rowData[9]);

			/*var id_tgl		= $(rowData[9]).find('input').attr('id');
			var tgl 		= $('#'+id_tgl).val();*/
			
			var dataItem = {
				'id_spb'			: parseInt(rowData[0]),
				'no_spb'			: rowData[1],
				'iditems'			: rowData[2],
				'item_name'			: rowData[3],
				'unit'				: parseInt(rowData[4]),
				'qty'				: qty,
				'tgl_diperlukan'	: $(div_tgl).selector,
				'gudang_id'			: parseInt(rowData[10]),
				'gudang'			: rowData[11],
				'desc'				: rowData[12],
				'unittext'			: rowData[5]
			};
			
			items.push(dataItem);
		}
		
		var datapost = {
			"no_spb"      :   no_spb,
			"spb_date"    :   spb_date,
			"id_cust"     :   id_cust,
			"departemen"  :   departemen,
			"items"       :   items
		};

		$.ajax({
			type:'POST',
			url: $(this).attr('action'),
			data:JSON.stringify(datapost),
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						$('.panel-heading button').trigger('click');
						window.location.href = "<?php echo base_url('spb');?>";
					})
				} else{
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Add Purchase Request");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Add Purchase Request");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}else{
	  swal("Failed!", "Harus menambah barang dahulu", "error");
	}
	return false;
}));

// function deleteaddeditem(id){
	// items.splice(id,1)

	// itemsElem();
// }

// function itemsElem(){
	// var itemslen = items.length; 
	
	// var element ='<thead>';
		// element +='   <tr>';
		// element +='     <th>ID SPB</th>';
		// element +='     <th>No SPB</th>';
		// element +='     <th>Kode Barang</th>';
		// element +='     <th>Nama Barang</th>';
		// element +='     <th>Satuan</th>';
		// element +='     <th>Qty</th>';
		// element +='     <th>Tanggal Diperlukan</th>';
		// element +='     <th>Gudang</th>';
		// element +='     <th>Keterangan</th>';
		// element +='     <th>Option</th>';
		// element +='   </tr>';
		// element +='</thead>';
		// element +='<tbody>';
	  // for(var i=0;i<itemslen;i++){
		// element +='   <tr>';
		// element +='     <td>'+items[i]["id_spb"]+'</td>';
		// element +='     <td>'+items[i]["no_spb"]+'</td>';
		// element +='     <td>'+items[i]["iditems"]+'</td>';
		// element +='     <td>'+items[i]["item_name"]+'</td>';
		// element +='     <td>'+items[i]["unit"]+'</td>';
		// element +='     <td>'+items[i]["qty"]+'</td>';
		// element +='     <td>'+items[i]["tgl_diperlukan"]+'</td>';
		// element +='     <td>'+items[i]["gudang"]+'</td>';
		// element +='     <td>'+items[i]["desc"]+'</td>';
		// element +='     <td>';
		// element +='       <div class="btn-group">';
		// element +='         <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteaddeditem('+i+')">';
		// element +='         <i class="fa fa-trash"></i>';
		// element +='       </div>';
		// element +='     </td>';
		// element +='   </tr>';
	// }
	// element +='</tbody>';
	
	// $('#listaddedmaterial').html(element);
// }
</script>
