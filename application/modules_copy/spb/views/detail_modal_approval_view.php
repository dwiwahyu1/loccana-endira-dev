  <style>
	  #loading-us{display:none}
	  #tick{display:none}

	  #loading-mail{display:none}
	  #cross{display:none}
    #add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
    #add_item:hover{color:#ff8c00}
  </style>
  <form class="form-horizontal form-label-left" id="edit_status" role="form" action="<?php echo base_url('spb/change_status');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

  		<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
		<br>
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Customer Name 
              <span class="required">
                <sup>*</sup>
              </span>
            </label>
            <div class="col-md-8 col-sm-6 col-xs-12">
                <input data-parsley-maxlength="255" type="text" id="cust_name" name="cust_name" class="form-control col-md-7 col-xs-12" placeholder="Customer Name" required="required"  value="<?php if(isset($detail[0]['cust_name'])){ echo $detail[0]['cust_name']; }?>" disabled="disabled">   
            </div>
        </div>
            
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Approval Date
            	<span class="required">
            		<sup>*</sup>
            	</span>
            </label>
            <div class="col-md-8 col-sm-6 col-xs-12">
				<div class="input-group date">
					<input placeholder="Approval Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="approval_date" name="approval_date" required="required" value="<?php if(isset($detail[0]['approval_date'])){ echo $detail[0]['approval_date']; }?>" readonly>
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-th"></span>
					</div>
				</div>
			</div>
       	</div>

        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">PIC
              <span class="required">
                <sup>*</sup>
              </span>
            </label>
            <div class="col-md-8 col-sm-6 col-xs-12">
                <input data-parsley-maxlength="255" type="text" id="pic_name" name="pic_name" class="form-control col-md-7 col-xs-12" placeholder="Customer Name" required="required"  value="<?php if(isset($detail[0]['pic_name'])){ echo $detail[0]['pic_name']; }?>" disabled="disabled">   
            </div>
        </div>

         <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Status 
            	<span class="required">
            		<sup>*</sup>
            	</span>
            </label>
            <div class="col-md-8 col-sm-6 col-xs-12">
                <input data-parsley-maxlength="255" type="text" id="status" name="status" class="form-control col-md-7 col-xs-12" placeholder="Customer Name" required="required"  value="<?php if(isset($detail[0]['status'])){ echo $detail[0]['status']; }?>" disabled="disabled"> 
            </div>
        </div>

        <div class="item form-group">
	      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Notes <span class="required"><sup>*</sup></span>
	      </label>
	      <div class="col-md-8 col-sm-6 col-xs-12">
	        <textarea data-parsley-maxlength="255" type="text" id="notes" name="notes" class="form-control col-md-7 col-xs-12" placeholder="Notes" disabled="disabled"><?php if(isset($detail[0]['notes'])){ echo $detail[0]['notes']; }?></textarea>
	      </div>
	    </div>
  </form><!-- /page content -->

<script type="text/javascript">
  var items = [];
  $(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();

    $("#approval_date").datepicker({
		  format: 'yyyy-mm-dd',
		  autoclose: true,
		  todayHighlight: true,
	  });
  });
</script>
