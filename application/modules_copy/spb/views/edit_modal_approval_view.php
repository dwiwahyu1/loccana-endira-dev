<style>
  	#loading-us{display:none}
  	#tick{display:none}
  	#loading-mail{display:none}
  	#cross{display:none}
	#add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
	#add_item:hover{color:#ff8c00}
</style>

<form class="form-horizontal form-label-left" id="edit_status" role="form" action="<?php echo base_url('spb/change_status');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

		<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
		<br>
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">PR No
				<span class="required">
					<sup>*</sup>
				</span>
			</label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input data-parsley-maxlength="255" type="text" id="no_spb" name="no_spb" class="form-control col-md-7 col-xs-12" placeholder="Spb No" value="<?php if(isset($detail[0]['no_spb'])){ echo $detail[0]['no_spb']; }?>" readonly autocomplete="off">
			</div>
		</div>
			
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">PR Date
				<span class="required">
					<sup>*</sup>
				</span>
			</label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<div class="input-group date">
					<input placeholder="SPB Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="spb_date" name="spb_date" required="required" value="<?php if(isset($detail[0]['tanggal_spb'])){ echo $detail[0]['tanggal_spb']; }?>" readonly autocomplete="off">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-th"></span>
					</div>
				</div>
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Detail Customer : </label>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Customer Name 
			  <span class="required">
				<sup>*</sup>
			  </span>
			</label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input data-parsley-maxlength="255" type="text" id="cust_name" name="cust_name" class="form-control col-md-7 col-xs-12" placeholder="Customer Name" autocomplete="off" required="required"  value="<?php if(isset($detail[0]['cust_name'])){ echo $detail[0]['cust_name']; }?>" disabled="disabled">
				<input type="hidden" id="id_cust" name="id_cust" value="<?php if(isset($detail[0]['id_cust'])){ echo $detail[0]['id_cust']; }?>">     
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Department 
				<span class="required">
					<sup>*</sup>
				</span>
			</label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input data-parsley-maxlength="255" type="text" id="department" name="department" class="form-control col-md-7 col-xs-12" placeholder="Department" autocomplete="off" required="required" value="<?php if(isset($detail[0]['departemen'])){ echo $detail[0]['departemen']; }?>" disabled="disabled">
						
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Barang : </label>
		</div>

		 <div class="item form-group">
			<table id="listaddedmaterial" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			  <thead>
				<tr>
					<th>No</th>
					<th>Kode Barang</th>
					<th>Nama Barang</th>
					<th>Satuan</th>
					<th>Qty</th>
					<th>Tanggal Datang</th>
					<th>Gudang</th>
					<th>Keterangan</th>
				 </tr>
			  </thead>
			  <tbody>
				<?php
					$items = $detail;
					$itemsLen = count($items);

					if($itemsLen){
						$no = 1;
						for($i=0;$i<$itemsLen;$i++){
				?>
						<tr>
							<td><?php echo $no; ?></td>
							<td><?php echo $detail[$i]['nama_material']; ?></td>
							<td><?php echo $detail[$i]['nama_barang']; ?></td>
							<td><?php echo $detail[$i]['uom_symbol']; ?></td>
							<td><?php echo $detail[$i]['qty']; ?></td>
							<td><?php echo $detail[$i]['tanggal_diperlukan']; ?></td>
							<td><?php echo $detail[$i]['gudang']; ?></td>
							<td><?php echo $detail[$i]['desc']; ?></td>
						</tr>
				<?php $no++;
						}
					}	
				?>
			  </tbody>
			</table>
		 </div>

		 <div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Status 
				<span class="required">
					<sup>*</sup>
				</span>
			</label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<select class="form-control" name="status" id="status" style="width: 100%" required>
					<option value="1">Accept</option>
					<option value="2">Reject</option>
				</select>
			</div>
		</div>

		<div class="item form-group">
		  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Notes <span class="required"><sup>*</sup></span>
		  </label>
		  <div class="col-md-8 col-sm-6 col-xs-12">
			<textarea data-parsley-maxlength="255" type="text" id="notes" name="notes" class="form-control col-md-7 col-xs-12" placeholder="Notes"></textarea>
		  </div>
		</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-change" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Status</button>
		</div>
		<input type="hidden" id="id_spb" name="id_spb" value="<?php if(isset($detail[0]['id'])) echo $detail[0]['id']; ?>">
	</div>
</form><!-- /page content -->

<script type="text/javascript">
  var items = [];
  $(document).ready(function() {
	$('form').parsley();
	$('[data-toggle="tooltip"]').tooltip();

	$("#spb_date").datepicker({
		  format: 'yyyy-mm-dd',
		  autoclose: true,
		  todayHighlight: true,
	  });
  });

  $('#edit_status').on('submit',(function(e) {
		$('#btn-change').attr('disabled','disabled');
		$('#btn-change').text("Mengubah status...");
		e.preventDefault();

		var no_spb = $('#no_spb').val(),
			status = $('#status').val(),
			notes  = $('#notes').val(),
			id_spb = $('#id_spb').val();

		var datapost={
			  "no_spb"      : no_spb,
			  "status"    	: status,
			  "notes"    	: notes,
			  'id_spb'		: id_spb
			};

		$.ajax({
			type:'POST',
			url: $(this).attr('action'),
			data:JSON.stringify(datapost),
			cache:false,
			contentType: false,
			processData: false,
			success: function(r) {
				if (r.success == true) {
					swal({
					  title: 'Success!',
					  text: r.message,
					  type: 'success',
					  showCancelButton: false,
					  confirmButtonText: 'Ok'
					}).then(function () {
						listspb();
						$('#panel-report-spb').modal('toggle');
						// $('.panel-heading button').trigger('click');
					})
				}else {
					$('#btn-change').removeAttr('disabled');
					$('#btn-change').text("Edit Status");
					swal("Failed!", r.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-change').removeAttr('disabled');
			$('#btn-change').text("Edit Status");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
  }));
</script>
