<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spb extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('spb/spb_model');
		$this->load->model('material/material_model');
		$this->load->library('log_activity');
		$this->load->library('sequence');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	  * This function is redirect to index spb page
	  * @return Void
	  */
	public function index() {
		$this->template->load('maintemplate', 'spb/views/index');
	}

	/**
	  * This function is used for showing spb list
	  * @return Array
	  */
	function lists() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

		$order_fields = array('', 'no_spb','cust_name');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->spb_model->lists($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();

		/*echo "<pre>";
		print_r($list);
		echo "</pre>";
		die;*/

		foreach ($list['data'] as $k => $v) {
			$actions =
				'<div class="btn-group">'.
					'<button class="btn btn-info" type="button" title="Detail" onClick="detail_spb(\'' . $v['no_spb'] . '\')">'.
						'<i class="fa fa-info"></i>';
					'</button>'.
				'</div>';

			//Status Pending
			if($v['status_spb'] == 1) {
				$status_spb = '<span class="label label-success text-center">'.$v['status_name'].'</span>';

				$actions .=
					'<div class="btn-group">'.
						'<button class="btn btn-warning" type="button" title="Edit" onClick="edit_spb(\'' . $v['no_spb'] . '\')">'.
							'<i class="fa fa-edit"></i>'.
						'</button>'.
					'</div>'.

					'<div class="btn-group">'.
						'<button class="btn btn-success" type="button" title="Approval" onClick="approval_spb(\'' . $v['id'] . '\')">'.
							'<i class="fa fa-check"></i>'.
						'</button>'.
					'</div>'.

					'<div class="btn-group">'.
						'<button class="btn btn-danger" type="button" title="Delete" onClick="deletespb(\'' . $v['id'] . '\')">'.
							'<i class="fa fa-trash"></i>'.
						'</button>'.
					'</div>';
			}else {
				$status_spb = '<span class="label label-info text-center">'.$v['status_name'].'</span>';

				$actions .=
					'<div class="btn-group">'.
						'<button class="btn btn-primary" type="button" title="Print PR" onClick="print_spb(\'' . $v['no_spb'] . '\')">'.
							'<i class="fa fa-file-pdf-o"></i>'.
						'</button>'.
					'</div>';
			}
			
			array_push($data, array(
				$v['no_spb'],
				$v['cust_name'],
				$v['tanggal_spb'],
				$v['departemen'],
				$v['part_number'],
				$v['total_products'],
				$status_spb,
				$actions
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	/**
	  * This function is redirect to add spb page
	  * @return Void
	  */
	public function add() {
		$this->load->view('add_modal_view');
	}

	/**
	  * This function is redirect to add spb page
	  * @return Void
	  */
	public function add_item() {
		$result = $this->spb_model->uom();
		$type_material = $this->spb_model->type_material();
		$result_brg = $this->spb_model->search_material();
		$result_gudang = $this->spb_model->search_gudang();
		$unit = $this->material_model->unit();

		$data = array(
			'uom' => $result,
			'unit' => $unit,
			'material' => $result_brg,
			'gudang' => $result_gudang,
			'type_material' => $type_material
		);

		$this->load->view('add_modal_item_view',$data);
	}
	
	public function add_new_item() {
		$type_material = $this->material_model->type_material();
		$valas = $this->material_model->valas();
		$unit = $this->material_model->unit();
		$detail = $this->material_model->detail_prop();
		$gudang = $this->material_model->gudang();
		
		$data = array(
			'type_material' => $type_material,
			'valas' => $valas,
			'unit' => $unit,
			'detail' => $detail,
			'gudang' => $gudang
		);

		$this->load->view('add_new_item_modal_view',$data);
	}
	
	/**
	  * This function is used to delete spb data
	  * @return Array
	  */
	public function add_spb() {
		$data   = file_get_contents("php://input");
		$params     = json_decode($data,true);
		$user_id	= $this->session->userdata['logged_in']['user_id'];

		/*echo "<pre>";
		print_r($params);
		echo "</pre>";
		die;*/
		
		$no_spb = $this->sequence->get_max_no('purchase_request');
		
		$items = $params['items'];
		$itemsLen = count($items);
		
		
		for($i=0;$i<$itemsLen;$i++){
			$add_spb = $this->spb_model->add_spb($no_spb,$params,$i,$user_id);
			if($add_spb['result'] > 0) {
				$this->sequence->save_max_no('purchase_request');
				
				$data_spb = array(
					'id_spb' 		=> $add_spb['lastid'],
					'status_spb' 	=> 1,
					'status_name' 	=> 'Pending',
					'date_insert' 	=> date('Y-m-d H:i:s')
				);
				
				$add_spb_status = $this->spb_model->add_spb_status($data_spb);
			}
		}
		
		$msg = 'Data SPB berhasil disimpan';
		
		$result = array('success' => true, 'message' => $msg);
		$this->log_activity->insert_activity('insert', $msg. ' dengan NO SPB ' .$no_spb);
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function get_material() 
	{
		$data   = file_get_contents("php://input");
		$params     = json_decode($data,true);
		
		$material = $this->spb_model->get_type_material($params);
		
		if(sizeof($material) > 0) {
			$result = array(
				'success' => true,
				'material' => $material
			);
		}else{
			$msg = 'Data barang tidak ditemukan di database, Silahkan pilih tipe barang kembali !!!';
			
			$result = array(
				'success' => false,
				'message' => $msg
			);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function get_satuan_by_material() 
	{
		$data   = file_get_contents("php://input");
		$params     = json_decode($data,true);
		
		$material = $this->spb_model->get_satuan_material($params);
		
		if(sizeof($material) > 0) {
			$result = array(
				'success' => true,
				'material' => $material
			);
		}else{
			$result = array(
				'success' => false
			);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function check_kd_stok(){
		$this->form_validation->set_rules('kode_stok', 'KodeStok', 'trim|required|min_length[4]|max_length[20]|is_unique[m_material.stock_code]');
		$this->form_validation->set_message('is_unique', 'Kode Stok Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'Kode Stok Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}
	
	public function save_new_items() {
		$data   = file_get_contents("php://input");
		$params     = json_decode($data,true);
		
		$this->spb_model->add_material($params);

		$msg = 'Berhasil menambah data material';

		$result = array(
			'success' => true,
			'message' => $msg
		);

		$this->log_activity->insert_activity('insert', $msg. ' dengan Kode Stok ' .$params['kode_stok']);
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function get_properties() {
		 
		$id = $this->Anti_sql_injection($this->input->post('id', TRUE));
		$detail_prop = $this->material_model->detail_prop_full($id);
		 
		$result = array(
			'success' => true,
			'message' => '',
			'data' => $detail_prop
		);
					
		$this->output->set_content_type('application/json')->set_output(json_encode($result,true));
	}
	
	/**
	  * This function is used to delete spb data
	  * @return Array
	  */
	public function delete_spb() {
		$data   = file_get_contents("php://input");
		$params     = json_decode($data,true);

		$result = $this->spb_model->delete_spbByID($params['idspb']);

		$msg = 'Berhasil menghapus data Purchase Request';

		$result = array('success' => true, 'message' => $msg);
		$this->log_activity->insert_activity('delete', $msg. ' dengan ID PR ' .$params['idspb']);
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function delete_spb_item() {
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);

		$this->spb_model->delete_spb_item($params['idspb']);

		$msg = 'Berhasil menghapus data Purchase Request';

		$result = array(
			'success' => true,
			'message' => $msg
		);

		$this->log_activity->insert_activity('delete', $msg. ' dengan ID PR ' .$params['idspb']);
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	/**
	  * This function is redirect to edit customer page
	  * @return Void
	  */
	public function edit($idspb) {
		$idspb = str_replace("%20"," ",$idspb);
		$result = $this->spb_model->detail_bynospb($idspb);

		$data = array(
			'detail' => $result
		);

		$this->load->view('edit_modal_view', $data);
	}

	/**
	  * This function is redirect to add spb page
	  * @return Void
	  */
	public function edit_approval($idspb) {
		$result = $this->spb_model->detail($idspb);
		$detail = $this->spb_model->detail_bynospb($result[0]['no_spb']);

		$data = array(
			// 'detail' => $result
			'detail' => $detail
		);

		$this->load->view('edit_modal_approval_view',$data);
	}

	public function change_status() {
		$data   = file_get_contents("php://input");
		$params     = json_decode($data,true);

		$user_id 		= $this->session->userdata['logged_in']['user_id'];
		$getSpbByNo		= $this->spb_model->getSpbByNo($params['no_spb']);

		if($params['status'] == 1) {
			$status_spb 	= 2;
			$status_name	= 'PO';
		}else {
			$status_spb 	= 4;
			$status_name	= 'Reject';
		}

		if(sizeof($getSpbByNo) > 0) {
			for ($i = 0; $i < sizeof($getSpbByNo); $i++) { 
				$dataStatus = array(
					'id_spb'		=> $getSpbByNo[$i]['id'],
					'no_spb'		=> $params['no_spb'],
					'status_spb'	=> $status_spb,
					'status_name'	=> $status_name,
					'notes'			=> $params['notes']
				);

				$status = $this->spb_model->change_status($dataStatus);
				if($status > 0) $this->spb_model->update_approval_spb($dataStatus);
			}

			if($status > 0) {
				$msg = 'Berhasil mengubah status SPB';

				$result = array('success' => true, 'message' => $msg);
				$this->log_activity->insert_activity('update', $msg. ' dengan No SPB ' .$params['no_spb']);
			}else {
				$msg = 'Gagal mengubah status SPB';

				$result = array('success' => false, 'message' => $msg);
				$this->log_activity->insert_activity('update', $msg. ' dengan No SPB ' .$params['no_spb']);
			}
		}else {
			$msg = 'Gagal mengubah status SPB';
			$result = array('success' => false, 'message' => $msg);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function detail($idspb) {
		$idspb = str_replace("%20"," ",$idspb);
		$result = $this->spb_model->detail_bynospb($idspb);

		$data = array(
			'detail' => $result
		);

		$this->load->view('detail_modal_view', $data);
	}
	
	public function detail_no_spb($idspb)  {
		$idspb = str_replace("%20"," ",$idspb);
		
		$data = array();
		$list = $this->spb_model->detail_bynospb($idspb);

		$i = 0;
		foreach ($list as $k => $v) {
			$tgl_diperlukan = date('d-m-Y', strtotime($v['tanggal_diperlukan']));
			
			array_push($data, array(
				$i+1,
				$v['stock_code'],
				$v['nama_barang'],
				number_format($v['qty'], 2),
				$v['uom_symbol'],
				$tgl_diperlukan,
				$v['desc'],
				'',
				'',
				$v['cust_name']
			));

			$i++;
		}
		
		$result['data'] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function detail_status($idspb) {
		$result = $this->spb_model->detail_status($idspb);

		$data = array(
			'detail' => $result
		);

		$this->load->view('detail_modal_approval_view', $data);
	}

	public function lists_detail_spb($idspb) 
	{
		$idspb = str_replace("%20"," ",$idspb);
		
		$data = array();
		$list = $this->spb_model->detail_bynospb($idspb);
		
		$i=0;
		
		foreach ($list as $k => $v) {
			if($v['valas_id'] == 1) {
				$qty = number_format(floatval($v['qty']), 2, '.', '');
				$step = '.01';
			}else {
				$qty = number_format(floatval($v['qty']), 5, '.', '');
				$step = '.00001';
			}
			
			$strQty = '<input type="number" class="form-control text-right" id="qty'.$i.'" name="qty'.$i.'" value="'.$qty.'" onchange="redrawTable('.$i.')" step="'.$step.'">';
			$strTanggal = '<div class="input-group date">';
			$strTanggal .='		<input placeholder="PR Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="tanggal_diperlukan'.$i.'" name="tanggal_diperlukan'.$i.'" required="required" value="'.$v['tanggal_diperlukan'].'" autocomplete="off">';
			$strTanggal .='		<div class="input-group-addon">';
			$strTanggal .='			<span class="glyphicon glyphicon-th"></span>';
			$strTanggal .='		</div>';
			$strTanggal .='</div>';
			
			$actions = '<div class="btn-group">';
			$actions .= '   <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="delete_spb_item(\'' . $v['id'] . '\',this)">';
			$actions .= '       <i class="fa fa-trash"></i>';
			$actions .= '   </button>';
			$actions .= '</div>';
			
			array_push($data, array(
				$v['id'],
				$v['no_spb'],
				$v['nama_material'],
				$v['nama_barang'],
				$v['id_uom'],
				$v['uom_name'],
				$strQty,
				$qty,
				$strTanggal,
				$v['tanggal_diperlukan'],
				$v['id_gudang'],
				$v['gudang'],
				$v['desc'],
				$actions
			));
			$i++;
		}
		
		$result['data'] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function edit_spb() 
	{
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		$user_id	= $this->session->userdata['logged_in']['user_id'];
		$no_spb 	= $params['no_spb'];
		
		// echo"<pre>";print_r($params['items']);echo "</pre>";die;

		if(sizeof($params['items']) > 0){
			for($i = 0; $i < sizeof($params['items']);$i++) {
				$data = array(
					'id_spb_item' 	=> $params['items'][$i]['id_spb'],
					'no_spb_item' 	=> $params['items'][$i]['no_spb'],
					'no_spb'	 	=> $no_spb,
					'id_cust' 		=> $params['id_cust'],
					'spb_date' 		=> $params['spb_date'],
					'nama_material' => $params['items'][$i]['iditems'],
					'id_uom' 		=> $params['items'][$i]['unit'],
					'qty_update' 	=> floatval($params['items'][$i]['qty']),
					'tgl_update' 	=> $params['items'][$i]['tgl_diperlukan'],
					'desc'			=> $params['items'][$i]['desc'],
					'status'		=> 1,
					'pic'			=> $user_id,
					'gudang_id'		=> $params['items'][$i]['gudang_id'],
				);
				
				/*$data = array(
					'id_spb_item' 	=> $params['items'][$i][0],
					'no_spb_item' 	=> $params['items'][$i][1],
					'no_spb'	 	=> $no_spb,
					'id_cust' 		=> $params['id_cust'],
					'spb_date' 		=> $params['spb_date'],
					'nama_material' => $params['items'][$i][2],
					'id_uom' 		=> $params['items'][$i][4],
					'qty_update' 	=> floatval($params['items'][$i][7]),
					'tgl_update' 	=> $params['items'][$i][9],
					'desc'			=> $params['items'][$i][12],
					'status'		=> 1,
					'pic'			=> $user_id,
					'gudang_id'		=> $params['items'][$i][10]
				);*/
				
				if($data['id_spb_item'] != 0){
					$this->spb_model->edit_spb($data);
				}else{
					
					$data_spb = $this->spb_model->add_spb_items($data);
					
					$data_spb_status = array(
						'id_spb'		=> $data_spb['lastid'],
						'status_spb'	=> $data['status'],
						'status_name'	=> 'Pending',
						'date_insert'	=> $data['spb_date']
					);
					
					$this->spb_model->add_spb_status($data_spb_status);
				}
				
			}
			
			$msg = 'Berhasil mengubah data Purchase Request';

			$result = array(
				'success' => true,
				'message' => $msg
			);
			
			$this->log_activity->insert_activity('update', $msg. ' dengan No PR ' .$no_spb);
			
		}else{
			$msg = 'Gagal menyimpan data Purchase Request, items barang tidak ditemukan';
			
			$result = array(
				'success' => false,
				'message' => $msg
			);
			
			$this->log_activity->insert_activity('update', $msg. ' dengan No PR ' .$params['items']);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function get_edit_material_spb($nospb) 
	{
		$no_spb = str_replace("%20"," ",$nospb);
		$list = $this->spb_model->detail_bynospb($no_spb);

		$data = array();
		$i = 0;
		$tempTotalQty = 0;
		$username = $this->session->userdata['logged_in']['username'];
		
		foreach ($list as $k => $v) {
			$amountQty = (int)$v['qty'];

			$tempTotalQty = $tempTotalQty + $amountQty;
			$strQty =
				'<input type="number" class="form-control" min="0" id="edit_qty'.$i.'" name="edit_qty'.$i.'" style="height:25px; width: 100px;" onInput="cal_qty('.$i.')" onChange="redrawTable('.$i.')" value="'.number_format($v['qty'], 4,".",".").'" step=".0001">';
			$strTanggal =
				'<input type="text" class="form-control" id="edit_tanggal'.$i.'" name="edit_tanggal'.$i.'" style="height:25px; width: 100%;" value="'.$v['tanggal_spb'].'">';
			$strOption =
				'<a class="btn btn-icon waves-effect waves-light btn-danger">'.
					'<i class="fa fa-trash"></i>'.
				'</a>';
				
			array_push($data, array(
				$v['id'],
				$v['no_spb'],
				$v['stock_code'],
				$v['nama_barang'],
				$v['id_uom'],
				'('.$v['uom_symbol'].') -'.$v['uom_name'],
				number_format($v['qty'],0,'',''),
				$strQty,
				$v['tanggal_spb'],
				$strTanggal,
				$v['id_gudang'],
				$v['gudang'],
				$v['desc'],
				$strOption
			));
			$i++;
		}

		$result["data"] = $data;
		$result["total"] = $tempTotalQty;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function save_edit_spb() {
		$this->form_validation->set_rules('no_spb', 'No Purchase Request', 'trim|required|min_length[4]|max_length[100]');
		$this->form_validation->set_rules('spb_date', 'Tanggal SPB', 'trim|required');
		$this->form_validation->set_rules('cust_name', 'Nama Customer', 'trim|required');
		$this->form_validation->set_rules('department', 'Departemen', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			$id_spb 		= $this->Anti_sql_injection($this->input->post('id_spb', TRUE));
			$no_spb			= $this->Anti_sql_injection($this->input->post('no_spb', TRUE));
			$tgl_spb		= $this->Anti_sql_injection($this->input->post('spb_date', TRUE));
			$id_cust		= $this->Anti_sql_injection($this->input->post('id_cust', TRUE));
			$nama_customer 	= $this->Anti_sql_injection($this->input->post('cust_name', TRUE));
			$departemen		= $this->Anti_sql_injection($this->input->post('departement', TRUE));
			$user_id		= $this->session->userdata['logged_in']['user_id'];

			$data = array(
				'id_spb'		=> $id_spb,
				'no_spb'		=> $no_spb,
				'tgl_spb'		=> $tgl_spb,
				'id_cust'		=> $id_cust,
				'nama_customer'	=> $nama_customer,
				'departemen'	=> $departemen,
				'pic'			=> $user_id,
			);

			$result = $this->spb_model->save_edit_spb($data);

			if ($data > 0) {
				$result = array(
					'success' 	=> true, 
					'message' 	=> 'Berhasil mengubah Purchase Request ke database', 
					'id_spb'	=> $id_spb,
					'data'		=> $data
				);
			}else {
				$this->log_activity->insert_activity('update', 'Update Purchase Request id : '.$id_spb);
				$result = array('success' => false, 'message' => 'Gagal mengubah Purchase Request ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function save_edit_barang_spb() 
	{
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		
		$arrData = array();
		$statSave = false;
		//echo"<pre>";print_r($params);die;
		foreach ($params['listbarang'] as $k => $v) {
			if($v[0] != '') {
				$arrTemp = array(
					'id_spb'		=> $v[0],
					'no_spb'		=> $v[1],
					'stock_code'	=> $v[2],
					'stock_name'	=> $v[3],
					'id_uom'		=> $v[4],
					'uom_name'		=> $v[5],
					'qty'			=> $v[6],
					'tanggal_spb'	=> $v[8],
					'id_gudang'		=> $v[10],
					'gudang'		=> $v[11],
					'desc'			=> $v[12],
					'status'		=> '0',
					'id_cust'		=> $params['spb']['id_cust'],
					'nama_customer'	=> $params['spb']['nama_customer'],
					'departemen'	=> $params['spb']['departemen'],
					'pic'			=> $params['spb']['pic'],
				);
				
				$resultBarang = $this->spb_model->save_edit_spb($arrTemp);
				if($resultBarang) {
					$statSave = true;
				}else{
					$statSave = false;
				}
			} 
		}

		if ($statSave == true) {
			$this->log_activity->insert_activity('update', 'Update Barang Purchase Request');
			$result = array('success' => true, 'message' => 'Berhasil mengubah barang Purchase Request ke database');
		}else {
			$this->log_activity->insert_activity('update', 'Gagal Update Barang Purchase Request');
			$result = array('success' => false, 'message' => 'Gagal mengubah barang Purchase Request ke database');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result);
	}
	
	public function delete_spb_detail() {
		$data 	= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		$result = $this->spb_model->delete_spb_detail($params['id_spb']);

		if($result > 0) {
			$this->log_activity->insert_activity('delete', 'Delete Purchase Order id : '.$params['id']);
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}else {
			$this->log_activity->insert_activity('delete', 'Gagal Delete Purchase Order id : '.$params['id']);
			$res = array('status' => 'failed', 'message' => 'Data gagal di hapus');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}
}