<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Spb_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	/**
      * This function is get the list data in uom table
      */
	public function uom()
	{
		$sql 	= 'SELECT id_uom, uom_symbol FROM m_uom ORDER BY uom_symbol ASC;';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function type_material()
	{
		$sql 	= 'SELECT id_type_material, type_material_name FROM m_type_material ORDER BY type_material_name ASC;';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_type_material($data)
	{
		$sql 	= 'SELECT a.id as id_material,a.stock_code,a.stock_name,a.unit,b.uom_name,b.uom_symbol FROM m_material a 
					LEFT JOIN m_uom b ON a.unit = b.id_uom where a.type = ? 
					ORDER BY a.stock_name ASC';

		$query 	= $this->db->query($sql, array(
			$data['type_barang']
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_satuan_material($data)
	{
		$sql 	= 'SELECT a.id as id_material,a.stock_code,a.stock_name,a.unit,b.uom_name,b.uom_symbol FROM m_material a 
					LEFT JOIN m_uom b ON a.unit = b.id_uom where a.id = ?';

		$query 	= $this->db->query($sql, array(
			$data['id_material']
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function search_material()
	{
		$sql 	= 'SELECT DISTINCT a.id,a.stock_code,a.stock_name,a.stock_description,b.id_uom,b.uom_name FROM m_material AS a LEFT JOIN m_uom AS b ON a.unit = b.id_uom ORDER by a.id DESC';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function search_gudang()
	{
		$sql 	= 'SELECT a.id_gudang,a.nama_gudang,c.uom_name,c.uom_symbol FROM t_gudang a
					LEFT JOIN m_material b ON a.id_gudang = b.id_gudang
					LEFT JOIN m_uom c ON b.unit = c.id_uom
					GROUP BY a.id_gudang';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	/**
      * This function is get the list data in spb table
      * @param : $params is where condition for select query
      */

	public function lists($params = array())
	{
		$sql_all 	= 'CALL spb_list_all(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				''
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL spb_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	/**
      * This function is used to Insert Record in spb table
      * @param : $data - record array 
      */

	public function add_spb($no_spb,$data,$i,$user_id)
	{
		$items = $data['items'];
		$itemsLen = count($items);

		//for($i=0;$i<$itemsLen;$i++){
			$sql 	= 'CALL spb_add(?,?,?,?,?,?,?,?,?,?,?)';

			$query 	=  $this->db->query($sql,
				array(
					$no_spb,
					$data['id_cust'],
					$data['spb_date'],
					$items[$i]["iditems"],
					0,
					$items[$i]["qty"],
					$items[$i]["tgl_diperlukan"],
					$items[$i]['desc'],
					1,
					$user_id,
					$items[$i]['gudang_id']
				)
			);
		//}
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		
		return $arr_result;
	}

	public function add_spb_status($data) {
		$sql 	= 'INSERT INTO t_spb_status (id_spb,status_spb,status_name,date_insert) VALUES (?,?,?,?)';
		
		$query 	=  $this->db->query($sql,array(
			$data['id_spb'],
			$data['status_spb'],
			$data['status_name'],
			$data['date_insert']
		));
			
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		
		return $arr_result;
	}
	
	public function add_material($data)
	{
		$sql 	= 'CALL material_add(?,?,?,?,?,?,?,?,?,?,?)';
		
		$query 	=  $this->db->query($sql,
			array(
				"",
				$data['kode_stok'],
				$data['nama_stok'],
				$data['desk_stok'],
				$data['uomId'],
				$data['type_material'],
				$data['qty2'],
				10,
				$data['detail'],
				$data['gudangId'],
				4
			));

		//print_r($query);die;
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		
		return $arr_result;
	}
	
	/**
     * This function is used to delete spb
     * @param: $nospb - nospb of spb table
     */
	function delete_spbByID($idspb) {
		$sql 	= 'CALL spb_delete(?)';
		
		$query 	= $this->db->query($sql,array(
			$idspb
		));
		
		$return = $query->result_array();
		
		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	function delete_spb($no_spb) {
		$sql 	= 'DELETE t_spb, t_spb_status FROM t_spb LEFT JOIN t_spb_status ON t_spb.id = t_spb_status.id_spb WHERE t_spb.no_spb = ? ';
		$this->db->where('no_spb', $no_spb);  
		$this->db->delete('t_spb'); 

		$return = $this->db->affected_rows();
		
		$this->db->close();
		$this->db->initialize();
		
		return $return;
	}
	
	function delete_spb_item($id_spb) {
		$sql 	= 'DELETE t_spb, t_spb_status FROM t_spb LEFT JOIN t_spb_status ON t_spb.id = t_spb_status.id_spb WHERE t_spb.id = ? ';

		$query 	= $this->db->query($sql,array(
			$id_spb
		));
		
		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	/**
      * This function is get data in spb table by no_spb
      * @param : $id is where condition for select query
      */

	public function detail_bynospb($no_spb)
	{
		$sql 	= 'CALL spb_search_nospb(?)';
		
		$query 	= $this->db->query($sql,array(
			$no_spb
		));
		
		$return = $query->result_array();
		
		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	/**
      * This function is get data in spb table by no_spb
      * @param : $id is where condition for select query
      */

	public function detail($idspb)
	{
		$sql 	= 'CALL spb_search_id(?)';

		$query 	= $this->db->query($sql,array(
				$idspb
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is used to Update Record in customer table
      * @param : $data - updated array 
      */

	public function change_status($data) {
		$sql 	=
			'UPDATE t_spb_status SET
				status_spb = ?,
				status_name = ?
			WHERE id_spb = ?;';

		$query = $this->db->query($sql, array(
			$data['status_spb'],
			$data['status_name'],
			$data['id_spb']
		));

		$return = $this->db->affected_rows();
		
		$this->db->close();
		$this->db->initialize();
		
		return $return;
	}

	public function getSpbByNo($no_spb) {
		$sql 	= 'SELECT * FROM t_spb WHERE no_spb = ?;';

		$query = $this->db->query($sql, array(
			$no_spb
		));

		$return = $query->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $return;
	}

	public function update_approval_spb($data) {
		$sql 	=
			'UPDATE t_spb SET
				notes = ?,
				approval_date = NOW()
			WHERE id = ?;';

		$query = $this->db->query($sql, array(
			$data['notes'],
			$data['id_spb']
		));

		$return = $this->db->affected_rows();
		
		$this->db->close();
		$this->db->initialize();
		
		return $return;
	}

	/**
      * This function is get data in spb table by no_spb
      * @param : $id is where condition for select query
      */
	public function detail_status($idspb)
	{
		$sql 	= 'SELECT 
							customer.nama AS cust_name,
							`t_spb`.approval_date,
							 pic.nama AS pic_name,
							 CASE
	                            WHEN status = 0 THEN "Preparing"
	                            WHEN status = 1 THEN "Accepted"
	                            ELSE "Rejected"
	                         END AS `status`,
	                         notes
					FROM 
							`t_spb`
        					LEFT JOIN u_user customer ON t_spb.id_user=customer.id
        					LEFT JOIN u_user pic ON t_spb.id_pic=pic.id  
        			WHERE 
        					id=?';

		$query 	= $this->db->query($sql,array(
				$idspb
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is used to Insert Record in spb table
      * @param : $id is where condition for select query
      */
	public function edit_spb($data)
	{
		if($data['tgl_update'] == '') $data['tgl_update'] = NULL;
		
		$sql 	= 'UPDATE t_spb SET qty = ?, tanggal_spb = ?, tanggal_diperlukan = ? WHERE id = ? AND no_spb = ?';

		$query 	=  $this->db->query($sql,
			array(
				$data['qty_update'],
				$data['spb_date'],
				$data['tgl_update'],
				$data['id_spb_item'],
				$data['no_spb_item']
			));	
		
		$return	= $this->db->affected_rows();
		
		return $return;
	}
	
	public function add_spb_items($data)
	{
		$sql 	= 'CALL spb_add(?,?,?,?,?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['no_spb'],
				$data['id_cust'],
				$data['spb_date'],
				$data['nama_material'],
				$data['id_uom'],
				$data['qty_update'],
				$data['tgl_update'],
				$data['desc'],
				$data['status'],
				$data['pic'],
				$data['gudang_id']
			)
		);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		
		return $arr_result;
	}
}