  <style>
  #loading-us{display:none}
  #tick{display:none}

  #loading-mail{display:none}
  #cross{display:none}
  .input-sep{width: 65px;float: left;text-align: center;font-size: 30px;}
  .add_list{cursor:pointer;#96b6e8;padding-top: 6px;}
  .add_list:hover{color:#ff8c00}
  .add_list a:hover{color:#ff8c00}
  </style>

  <form class="form-horizontal form-label-left" id="detail_wip" role="form" action="<?php echo base_url('wip/detail_wip');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

    <p style="text-align: center;">Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">PO No <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="order_no" name="order_no" class="form-control col-md-7 col-xs-12" placeholder="PO No" required="required" value="<?php if(isset($detail[0]['order_no'])){ echo $detail[0]['order_no']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Customer Name <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="cust_name" name="cust_name" class="form-control col-md-7 col-xs-12" placeholder="Customer Name" required="required" value="<?php if(isset($detail[0]['name_eksternal'])){ echo $detail[0]['name_eksternal']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Item <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="stock_name" name="stock_name" class="form-control col-md-7 col-xs-12" placeholder="Item" required="required" value="<?php if(isset($detail[0]['stock_name'])){ echo $detail[0]['stock_name']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Unit <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="unit" name="unit" class="form-control col-md-7 col-xs-12" placeholder="Unit" required="required" value="<?php if(isset($detail[0]['unit'])){ echo $detail[0]['unit']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Panel/Array <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="pcs_array" name="pcs_array" class="form-control col-md-7 col-xs-12" placeholder="Panel/Array" required="required" value="<?php if(isset($detail[0]['pcs_array'])){ echo $detail[0]['pcs_array']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">M&sup2/PNL <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="panel_m2" name="panel_m2" class="form-control col-md-7 col-xs-12" placeholder="Item" required="required" value="<?php if(isset($detail[0]['panel_m2'])){ echo $detail[0]['panel_m2']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Wip : </label>
    </div>

    <div class="item form-group">
      <table id="listaddedwip" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Date</th>
            <th>Process Flow</th>
            <th>Qty In</th>
            <th>Qty Out</th>
            <th>M&sup2</th>
          </tr>
        </thead>
        <tbody>
          <?php
            $itemsLen = count($listwip);
            if($itemsLen){
              for($i=0;$i<$itemsLen;$i++){
          ?>
                <tr>
                  <td><?php echo $listwip[$i]['date']; ?></td>
                  <td><?php echo $listwip[$i]['name']; ?></td>
                  <td><?php echo $listwip[$i]['qty_in']; ?></td>
                  <td><?php echo $listwip[$i]['qty_out']; ?></td>
                  <td><?php echo $listwip[$i]['m2']; ?></td>
                </tr>
          <?php
              }
            } 
          ?>
        </tbody>
      </table>
    </div>

  <input type="hidden" id="id_po_quotation" name="id_po_quotation" value="<?php if(isset($detail[0]['id_po_quotation'])){ echo $detail[0]['id_po_quotation']; }?>">
  <input type="hidden" id="id_produk" name="id_produk" value="<?php if(isset($detail[0]['id_produk'])){ echo $detail[0]['id_produk']; }?>">
</form><!-- /page content -->

<script type="text/javascript">
  $(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>
