  <style>
  #loading-us{display:none}
  #tick{display:none}

  #loading-mail{display:none}
  #cross{display:none}
  .input-sep{width: 65px;float: left;text-align: center;font-size: 30px;}
  .add_list{cursor:pointer;#96b6e8;padding-top: 6px;}
  .add_list:hover{color:#ff8c00}
  .add_list a:hover{color:#ff8c00}
  </style>

  <form class="form-horizontal form-label-left" id="edit_wip" role="form" action="<?php echo base_url('wip/edit_wip');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

    <p style="text-align: center;">Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">PO No <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="order_no" name="order_no" class="form-control col-md-7 col-xs-12" placeholder="PO No" required="required" value="<?php if(isset($detail[0]['order_no'])){ echo $detail[0]['order_no']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Customer Name <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="cust_name" name="cust_name" class="form-control col-md-7 col-xs-12" placeholder="Customer Name" required="required" value="<?php if(isset($detail[0]['name_eksternal'])){ echo $detail[0]['name_eksternal']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Item <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="stock_name" name="stock_name" class="form-control col-md-7 col-xs-12" placeholder="Item" required="required" value="<?php if(isset($detail[0]['stock_name'])){ echo $detail[0]['stock_name']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Unit <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="unit" name="unit" class="form-control col-md-7 col-xs-12" placeholder="Unit" required="required" value="<?php if(isset($detail[0]['unit'])){ echo $detail[0]['unit']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Panel/Array <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="pcs_array" name="pcs_array" class="form-control col-md-7 col-xs-12" placeholder="Panel/Array" required="required" value="<?php if(isset($detail[0]['pcs_array'])){ echo $detail[0]['pcs_array']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">M&sup2/PNL <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="panel_m2" name="panel_m2" class="form-control col-md-7 col-xs-12" placeholder="Item" required="required" value="<?php if(isset($detail[0]['panel_m2'])){ echo $detail[0]['panel_m2']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Wip : </label>
      <div class="col-md-8 col-sm-6 col-xs-12 add_list" onclick="add_list()">
        <a type="button" class="btn btn-default btn-icon waves-effect waves-light m-b-5">
          <i class="fa fa-plus"></i>
        </a>
        Tambah Wip
      </div>
    </div>

    <div class="item form-group">
      <table id="listaddedwip" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Date</th>
            <th>Process Flow</th>
            <th>Qty In</th>
            <th>Qty Out</th>
            <th>M&sup2</th>
            <th>Option</th>
          </tr>
        </thead>
        <tbody>
          <?php
            $itemsLen = count($listwip);
            if($itemsLen){
              for($i=0;$i<$itemsLen;$i++){
          ?>
                <tr>
                  <td><?php echo $listwip[$i]['date']; ?></td>
                  <td><?php echo $listwip[$i]['name']; ?></td>
                  <td><?php echo $listwip[$i]['qty_in']; ?></td>
                  <td><?php echo $listwip[$i]['qty_out']; ?></td>
                  <td><?php echo $listwip[$i]['m2']; ?></td>
                  <td>
                    <div class="btn-group">
                      <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteaddedwip('<?php echo $i;?>')">
                      <i class="fa fa-trash"></i>
                    </div>
                  </td>
                </tr>
          <?php
              }
            } 
          ?>
        </tbody>
      </table>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <button id="btn-submitdata" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Wip</button>
      </div>
    </div>

  <input type="hidden" id="id_po_quotation" name="id_po_quotation" value="<?php if(isset($detail[0]['id_po_quotation'])){ echo $detail[0]['id_po_quotation']; }?>">
  <input type="hidden" id="id_produk" name="id_produk" value="<?php if(isset($detail[0]['id_produk'])){ echo $detail[0]['id_produk']; }?>">
</form><!-- /page content -->

<script type="text/javascript">
  var addWip = [];

  "<?php
    for($i=0;$i<$itemsLen;$i++){
      $id_po_quotation = $detail[0]['id_po_quotation'];
      $id_produk = $detail[0]['id_produk'];
      $id_process_flow = $listwip[$i]['id_process_flow'];
      $date = $listwip[$i]['date'];
      $qtyin = $listwip[$i]['qty_in'];
      $qtyout = $listwip[$i]['qty_out'];
      $m2 = $listwip[$i]['m2'];
      $text_process_flow = $listwip[$i]['name'];
  ?>"
      var id_po_quotation = "<?php echo $id_po_quotation; ?>";
      var id_produk = "<?php echo $id_produk; ?>";
      var id_process_flow = "<?php echo $id_process_flow; ?>";
      var date = "<?php echo $date; ?>";
      var qtyin = "<?php echo $qtyin; ?>";
      var qtyout = "<?php echo $qtyout; ?>";
      var m2 = "<?php echo $m2; ?>";
      var text_process_flow = "<?php echo $text_process_flow; ?>";

      var dataitem = {
          id_po_quotation,
          id_produk,
          id_process_flow,
          date,
          qtyin,
          qtyout,
          m2,
          text_process_flow
      };

      addWip.push(dataitem);
  "<?php    
    }
  ?>"

  $(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();
  });

  $('#edit_wip').on('submit',(function(e) {
    if(addWip.length){
      $('#btn-submitdata').attr('disabled','disabled');
      $('#btn-submitdata').text("Mengubah data...");
      e.preventDefault();

      var formData = new FormData(this);

      $.ajax({
          type:'POST',
          url: $(this).attr('action'),
          data : JSON.stringify(addWip),
          dataType: 'json',
          contentType: 'application/json; charset=utf-8',
          success: function(response) {
              if (response.success == true) {
                $('.panel-heading button').trigger('click');
                  listwip();
                  swal({
                    title: 'Success!',
                    text: response.message,
                    type: 'success',
                    showCancelButton: false,
                    confirmButtonText: 'Ok'
                  }).then(function () {
                  });
              } else{
                  $('#btn-submitdata').removeAttr('disabled');
                  $('#btn-submitdata').text("Edit Wip");
                  swal("Failed!", response.message, "error");
              }
          }
      }).fail(function(xhr, status, message) {
          $('#btn-submitdata').removeAttr('disabled');
          $('#btn-submitdata').text("Edit Wip");
          swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
      });
    }else{
      swal("Failed!", "Harus menambah wip dahulu", "error");
    }
    return false;
  }));

  function deleteaddedwip(id){
    addWip.splice(id,1)

    itemsElem();
  }

  function itemsElem(){
    var itemslen = addWip.length; 
    
    var element ='<thead>';
        element +='   <tr>';
        element +='     <th>Date</th>';
        element +='     <th>Process Flow</th>';
        element +='     <th>Qty In</th>';
        element +='     <th>Qty Out</th>';
        element +='     <th>M&sup2</th>';
        element +='     <th>Option</th>';
        element +='   </tr>';
        element +='</thead>';
        element +='<tbody>';
    for(var i=0;i<itemslen;i++){
        element +='   <tr>';
        element +='     <td>'+addWip[i]["date"]+'</td>';
        element +='     <td>'+addWip[i]["text_process_flow"]+'</td>';
        element +='     <td>'+addWip[i]["qtyin"]+'</td>';
        element +='     <td>'+addWip[i]["qtyout"]+'</td>';
        element +='     <td>'+addWip[i]["m2"]+'</td>';
        element +='     <td>';
        element +='       <div class="btn-group">';
        element +='         <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteaddedwip('+i+')">';
        element +='         <i class="fa fa-trash"></i>';
        element +='       </div>';
        element +='     </td>';
        element +='   </tr>';
    }
    element +='</tbody>';

    $('#listaddedwip').html(element);
  }
</script>
