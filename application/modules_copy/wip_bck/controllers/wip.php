<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wip extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('wip/wip_model');
		$this->load->library('log_activity');
    }

    /**
     * anti sql injection
     * @return string
     */
    public function Anti_sql_injection($string) {
        $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
        return $string;
    }

    /**
      * This function is redirect to index esf page
      * @return Void
      */
    public function index() {
        $this->template->load('maintemplate', 'wip/views/index');
    }

    /**
      * This function is used for showing esf list
      * @return Array
      */
    function lists() {
        $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
        $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
        $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
        $order = $this->input->get_post('order');
        $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
        $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

        $order_fields = array('', 'id', 'name_eksternal', 'stock_name');

        $search = $this->input->get_post('search');

        $search_val = (!empty($search['value'])) ? $search['value'] : null;

        $search_value = $this->Anti_sql_injection($search_val);

        // Build params for calling model
        $params['limit'] = (int) $length;
        $params['offset'] = (int) $start;
        $params['order_column'] = $order_fields[$order_column];
        $params['order_dir'] = $order_dir;
        $params['filter'] = $search_value;

        $list = $this->wip_model->lists($params);

        $result["recordsTotal"] = $list['total'];
        $result["recordsFiltered"] = $list['total_filtered'];
        $result["draw"] = $draw;

        $data = array();

        $i = $params['offset'];
        foreach ($list['data'] as $k => $v) {
            $i++;

            $actions = '<div class="btn-group">';
            $actions .= '   <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="editwip(\'' . $v['id'] . '~' . $v['id_produk'] . '\')">';
            $actions .= '       <i class="fa fa-edit"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';
            $actions .= '<div class="btn-group">';
            $actions .= '   <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deletewip(\'' . $v['id'] . '~' . $v['id_produk'] . '\')">';
            $actions .= '       <i class="fa fa-trash"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';
            $actions .= '<div class="btn-group">';
            $actions .= '   <button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="detailwip(\'' . $v['id'] . '~' . $v['id_produk'] . '\')">';
            $actions .= '       <i class="fa fa-search"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';
            
            array_push($data, array(
                $i,
                $v['order_no'],
                $v['name_eksternal'],
                $v['stock_name'],
                $v['process_name'],
                $v['date'],
                $v['qty_in'],
                $v['qty_out'],
                $v['m2'],
                $actions
                    )
            );
        }

        $result["data"] = $data;

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is redirect to add wip page
      * @return Void
      */
    public function add() {;
        $this->load->view('add_modal_view');
    }

    /**
      * This function is used to add wip data
      * @return Array
      */
    public function get_po() {
        $data   = file_get_contents("php://input");
        $params = json_decode($data,true);

        $list   = $this->wip_model->get_po($params);

        $data = array();

        $i = 0;
        foreach ($list as $k => $v) {
            $i++;

            $index = $i-1;

            $strOption =
                '<div class="radiobutton">'.
                    '<input id="option['.$index.']" type="radio" value="'.$v['id'].'~'.$v['id_produk'].'">'.
                    '<label for="option['.$index.']"></label>'.
                '</div>';
            
            array_push($data, array(
                $i,
                $v['order_no'],
                $v['issue_date'],
                $v['name_eksternal'],
                $v['stock_name'],
                $strOption
            ));
        }
        
        $res = array(
            'status'    => 'success',
            'data'      => $data
        );

        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
        echo json_encode($res);
    }

    /**
      * This function is redirect to add wip page
      * @return Void
      */
    public function add_list() {
        $process_flow = $this->wip_model->process_flow();

        $data = array(
            'process_flow'    => $process_flow
        );

        $this->load->view('add_modal_wip_view',$data);
    }

    /**
      * This function is redirect to add wip page
      * @return Void
      */
    public function new($id) {
      $detail = $this->wip_model->detail($id);

      $data = array(
          'detail'    => $detail
      );

      $this->load->view('new_modal_view',$data);
    }

    /**
      * This function is used to add wip data
      * @return Array
      */
    public function add_wip() {
      $data     = file_get_contents("php://input");
      $params   = json_decode($data,true);

      $this->wip_model->add_wip($params);

      $msg = 'Berhasil menambah data work in progress';

      $result = array(
          'success' => true,
          'message' => $msg
      );

		$this->log_activity->insert_activity('insert', $msg. ' dengan ID PO Quotation ' .$params['id_po_quotation']);
		
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is used to delete wip data
      * @return Array
      */
    public function delete_wip() {
        $data   = file_get_contents("php://input");
        $params = json_decode($data,true);

        $idarray = explode('~', $params['id']);
        $id_po_quotation = $idarray[0];
        $id_produk = $idarray[1];

        $this->wip_model->delete_wip($id_po_quotation,$id_produk);

        $msg = 'Berhasil menghapus data enginering specification';

        $result = array(
            'success' => true,
            'message' => $msg
        );

		$this->log_activity->insert_activity('delete', $msg. ' dengan ID PO Quotation ' .$id_po_quotation);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is redirect to edit wip page
      * @return Void
      */
    public function edit($id) {
      $detail = $this->wip_model->detail($id);
      $listwip = $this->wip_model->listwip($id);

      $data = array(
          'detail'    => $detail,
          'listwip'   => $listwip
      );

      $this->load->view('edit_modal_view',$data);
    }

    /**
      * This function is used to edit wip data
      * @return Array
      */
    public function edit_wip() {
      $data     = file_get_contents("php://input");
      $params   = json_decode($data,true);

      $this->wip_model->edit_wip($params);

      $msg = 'Berhasil mengubah data work in progress';

      $result = array(
          'success' => true,
          'message' => $msg
      );

		$this->log_activity->insert_activity('update', $msg. ' dengan ID PO Quotation ' .$params['id_po_quotation']);
		
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is redirect to detail wip page
      * @return Void
      */
    public function detail($id) {
      $detail = $this->wip_model->detail($id);
      $listwip = $this->wip_model->listwip($id);

      $data = array(
          'detail'    => $detail,
          'listwip'   => $listwip
      );

      $this->load->view('detail_modal_view',$data);
    }
}