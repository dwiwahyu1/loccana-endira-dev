	<style>
		#loading-us{display:none}
		#tick{display:none}

		#loading-mail{display:none}
		#cross{display:none}
		.add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
		.add_item:hover{color:#ff8c00}
	</style>
	
	<form class="form-horizontal form-label-left" id="approve_btb" role="form" action="<?php echo base_url('btb/approve_btb');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

  		<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

        <div class="item form-group">
        	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_po">No PO <span class="required"><sup>*</sup></span></label>
            <div class="col-md-8 col-sm-6 col-xs-12">
               	<input type="text" id="no_po" name="no_po" class="form-control col-md-7 col-xs-12" placeholder="No PO"  value="<?php echo $approve[0]['no_po'];?>" readonly>
               	<input type="hidden" id="id_po" name="id_po" class="form-control col-md-7 col-xs-12" placeholder="ID PO"  value="<?php echo $approve[0]['id_po'];?>" readonly>
               	<input type="hidden" id="id_po_spb" name="id_po_spb" class="form-control col-md-7 col-xs-12" placeholder="ID PO SPB"  value="<?php echo $approve[0]['id_po_spb'];?>" readonly>
               	<input type="hidden" id="id_material" name="id_material" class="form-control col-md-7 col-xs-12" placeholder="ID Material"  value="<?php echo $approve[0]['id_material'];?>" readonly>
            </div>
        </div>
            
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_spb">No SPB <span class="required"><sup>*</sup></span></label>
            <div class="col-md-8 col-sm-6 col-xs-12">
				  <div class="input-group date">
					<input placeholder="No SPB" type="text" class="form-control col-md-7 col-xs-12" id="no_spb" name="no_spb" required="required" value="<?php echo $approve[0]['no_spb'];?>" readonly>
					<input placeholder="ID SPB" type="hidden" class="form-control col-md-7 col-xs-12" id="id_spb" name="id_spb" required="required" value="<?php echo $approve[0]['id_spb'];?>" readonly>
					<input placeholder="Status" type="hidden" class="form-control col-md-7 col-xs-12" id="status" name="status" required="required" value="2" readonly>
					<input placeholder="Status Material" type="hidden" class="form-control col-md-7 col-xs-12" id="status_material" name="status_material" required="required" value="1" readonly>
				</div>
			</div>
       	</div>

        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="keterangan">Keterangan <span class="required"><sup>*</sup></span></label>
            <div class="col-md-8 col-sm-6 col-xs-12">
                <textarea data-parsley-maxlength="255" type="text" id="keterangan" name="keterangan" class="form-control col-md-7 col-xs-12" placeholder="Keterangan"></textarea>     
            </div>
        </div>
		
		  <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_btb">No Btb <span class="required"><sup>*</sup></span></label>
            <div class="col-md-8 col-sm-6 col-xs-12">
                <input data-parsley-maxlength="255" type="text" id="no_btb" name="no_btb" class="form-control col-md-7 col-xs-12" placeholder="No BTB" />     
            </div>
        </div>
		
		 <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_bc">No BC <span class="required"><sup>*</sup></span></label>
            <div class="col-md-8 col-sm-6 col-xs-12">
                <select id="no_bc" name="no_bc" class="form-control col-md-7 col-xs-12" >
					<option value='0' >Tanpa BC</option>
					<!--<option value='Baru' >Buat BC Baru</option>-->
					<?php foreach($jenis_bc as $key) { ?>
						<option value="<?php echo $key['id']; ?>" ><?php echo $key['jb']; ?> - <?php echo $key['no_pendaftaran']; ?> - <?php echo $key['tanggal_pengajuan']; ?></option>
					<?php } ?>
				</select>
            </div>
        </div>
		
		<!--
		<div id='new_bc' style="display:none">
			<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jenis_bc">Jenis BC <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="jenis_bc" name="jenis_bc" style="width: 100%" required>
				<option value="" >-- Select Jenis --</option>
				<?php foreach($list_bc_all as $key) { ?>
					<option value="<?php echo $key['id']; ?>" ><?php echo $key['jenis_bc']; ?></option>
				<?php } ?>
			</select>
			<input type="hidden" id="type_text" name="type_text" value="">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nopendaftaran">No Pendaftaran <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-minlength="4" data-parsley-maxlength="100" type="number" id="nopendaftaran" name="nopendaftaran" class="form-control col-md-7 col-xs-12" placeholder="no pendaftaran minimal 4 karakter" required="required">
			<!--<span id="nopendaftaran_loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking No Pendaftaran...</span>
			<span id="nopendaftaran_tick"></span>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nopengajuan">No Pengajuan <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-minlength="4" data-parsley-maxlength="100" type="text" id="nopengajuan" name="nopengajuan" class="form-control col-md-7 col-xs-12" placeholder="no pengajuan minimal 4 karakter" required="required">
			<!-- <span id="nopengajuan_loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking No Pengajuan...</span>
			<span id="nopengajuan_tick"></span>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tglpengajuan">Tanggal Pengajuan <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="100" type="text" id="tglpengajuan" name="tglpengajuan" class="form-control col-md-7 col-xs-12" placeholder="tanggal pengajuan" required="required">
		</div>
	</div>

	<div class="item form-group has-feedback">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="file_bc">File</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="file" class="form-control" id="file_bc" name="file_bc" data-height="110" accept=".pdf, .txt, .doc, .docx"/>
			<span id="file_bc_tick">Hanya format file pdf,txt,doc,docx dengan besaran max 10Mb yang diterima.</span>
		</div>
	</div>-->
		</div>
		
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
            <div class="col-md-8 col-sm-6 col-xs-12">
            	<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Approve BTB</button>
            </div>
        </div>
  </form><!-- /page content -->

<script type="text/javascript">
$(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();
	
		$('#tglpengajuan').datepicker({
			format: "dd/M/yyyy",
			autoclose: true,
			todayHighlight: true
		});
	
});

	$('#no_bc').on('change', function() {
		
		var tv = $('#no_bc').val();
		//alert(tv);
		if( tv == 'Baru' ){
			$('#new_bc').show();
		}else{
			$('#new_bc').hide();
		}
	  //alert( this.value );
	});

  $('#approve_btb').on('submit',(function(e) {
    if($('#keterangan').val() !== ''){
        $('#btn-submit').attr('disabled','disabled');
        $('#btn-submit').text("Memasukkan data...");
        e.preventDefault();

        var id_po_spb = $('#id_po_spb').val(),
            id_material = $('#id_material').val(),
            id_spb = $('#id_spb').val(),
            status = $('#status').val(),
            status_material = $('#status_material').val(),
            keterangan = $('#keterangan').val(),
            no_btb = $('#no_btb').val(),
            no_bc = $('#no_bc').val();

        var datapost={
			"id_po_spb"			:   id_po_spb,
			"id_material"		:   id_material,
			"id_spb"			:   id_spb,
			"status"			:   status,
			"status_material"	:   status_material,
			"keterangan"		:   keterangan,
			"no_btb"			:   no_btb,
			"no_bc"				:   no_bc 
		};

        $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:JSON.stringify(datapost),
            cache:false,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response.success == true) {
                    swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
                    }).then(function () {
						$('.panel-heading button').trigger('click');
						listbtb();
                    })
                } else{
                    $('#btn-submit').removeAttr('disabled');
                    $('#btn-submit').text("Approve BTB");
                    swal("Failed!", response.message, "error");
                }
            }
        }).fail(function(xhr, status, message) {
            $('#btn-submit').removeAttr('disabled');
            $('#btn-submit').text("Approve BTB");
            swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
        });
    }else{
		swal("Failed!", "Maaf kolom Keterangan harus diisi", "error");
    }
    return false;
  }));
</script>
