<style>
  .changed_status{cursor:pointer;text-decoration: underline;color:#96b6e8;}
  .changed_status:hover{color:#ff8c00}
  .custom-tables, th{text-align:center;vertical-align:middle;}
  .custom-tables.align-text, th{vertical-align:middle;}
  .dt-body-right{text-align:right;}
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">BTB</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">

				<table id="listbtb" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th rowspan="2" class="custom-tables align-text">No</th>
							<th rowspan="2" class="custom-tables align-text">Jenis Barang</th>
							<th class="custom-tables">Gudang</th>
							<th colspan="3" class="custom-tables align-text">Bagian Pembelian</th>
							<th rowspan="2" class="custom-tables align-text">No. PR</th>
							<th rowspan="2" class="custom-tables align-text">No. PO</th>
							<th rowspan="2" class="custom-tables align-text">Kode Barang</th>
							<th rowspan="2" class="custom-tables align-text">Status</th>
							<th rowspan="2" class="custom-tables align-text">Ket</th>
							<th rowspan="2" class="custom-tables align-text">Actions</th>
						</tr>
						<tr>
							<th class="custom-tables align-text">Qty</th>
							<th class="custom-tables align-text">HRG Sat</th>
							<th class="custom-tables align-text">PPN /Disc</th>
							<th class="custom-tables align-text">HRG Total</th>
						</tr>
					</tbody>
				</table>

            </div>
        </div><!-- end col -->
    </div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width:850px;">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <p></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
function reject_btb(idpospb){
	$('#panel-modal').removeData('bs.modal');
	$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal  .panel-body').load('<?php echo base_url('btb/reject/');?>'+"/"+idpospb);
	$('#panel-modal  .panel-title').html('<i class="fa fa-times"></i> Reject Barang');
	$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}

function approve_btb(idpospb){
	$('#panel-modal').removeData('bs.modal');
	$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal  .panel-body').load('<?php echo base_url('btb/approve');?>'+"/"+idpospb);
	$('#panel-modal  .panel-title').html('<i class="fa fa-pencil"></i> Terima Barang');
	$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}
  
function detail_status(idpospb){
	$('#panel-modal').removeData('bs.modal');
	$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal  .panel-body').load('<?php echo base_url('btb/detail');?>'+"/"+idpospb);
	$('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Detail Barang');
	$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}

function listbtb(){
	$("#listbtb").dataTable({
		"processing": true,
		"serverSide": true,
		"ajax": "<?php echo base_url().'btb/lists/';?>",
		"searchDelay": 700,
		"responsive": true,
		"lengthChange": false,
		"destroy": true,
		"info": false,
		"bSort": false,
		"dom": 'l<"toolbar">frtip',
		"columnDefs": [{
			targets: [2,3,4,5,7],
			className: 'dt-body-right'
		}]
	});
}

$(document).ready(function(){
	listbtb();
});
</script>