	<style>
		#loading-us{display:none}
		#tick{display:none}

		#loading-mail{display:none}
		#cross{display:none}
		.dt-body-right{text-align:right;}
	</style>
	
	<form class="form-horizontal form-label-left" role="form" action="<?php echo base_url('btb/detail');?>" method="POST" enctype="multipart/form-data" data-parsley-validate>
	
        <div class="item form-group">
        	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_po">No PO <span class="required"><sup>*</sup></span></label>
            <div class="col-md-8 col-sm-6 col-xs-12">
               	<input type="text" id="no_po" name="no_po" class="form-control col-md-7 col-xs-12" placeholder="No PO"  value="<?php echo $status[0]['no_po'];?>" readonly>
               	<input type="hidden" id="id_po_spb" name="id_po_spb" class="form-control col-md-7 col-xs-12" placeholder="ID PO"  value="<?php echo $status[0]['id_po_spb'];?>" readonly>
            </div>
        </div>
            
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_spb">No SPB <span class="required"><sup>*</sup></span></label>
            <div class="col-md-8 col-sm-6 col-xs-12">
				  <div class="input-group date">
					<input placeholder="No SPB" type="text" class="form-control col-md-7 col-xs-12" id="no_spb" name="no_spb" required="required" value="<?php echo $status[0]['no_spb'];?>" readonly>
					<input placeholder="ID SPB" type="hidden" class="form-control col-md-7 col-xs-12" id="id_spb" name="id_spb" required="required" value="<?php echo $status[0]['id_spb'];?>" readonly>
				</div>
			</div>
       	</div>
		
		<div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Status </label>
            <div class="col-md-8 col-sm-6 col-xs-12">
				<div class="input-group date">
					<?php if($status[0]['status_po'] == 2) { 
							$stat = "Approved"; 
							echo "<input placeholder='Status' type='text' class='form-control col-md-7 col-xs-12' id='status' name='status' required='required' value='".$stat."' readonly>";
						}elseif($status[0]['status_po'] == 1){ 
							$stat = 'Process';
							echo "<input placeholder='Status' type='text' class='form-control col-md-7 col-xs-12' id='status' name='status' required='required' value='".$stat."' readonly>";
						} 
					?>
				</div>
			</div>
       	</div>

        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="keterangan">Keterangan <span class="required"><sup>*</sup></span></label>
            <div class="col-md-8 col-sm-6 col-xs-12">
				<?php if($status[0]['notes'] != '') { $ket = $status[0]['notes']; }else{ $ket = 'Tidak ada keterangan'; } ?>
                <textarea data-parsley-maxlength="255" type="text" id="keterangan" name="keterangan" class="form-control col-md-7 col-xs-12" placeholder="Keterangan"><?php echo $ket; ?></textarea>     
            </div>
        </div>
		
		<div class="item form-group">
         	<table id="listdetailbtb" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>Kode Barang</th>
						<th>Jenis Barang</th>
						<th>Qty</th>
						<th>Harga Sat</th>
						<th>Tanggal Diterima</th>
						<th>Keterangan</th>
					</tr>
				</thead>
				<tbody>
					<?php
						$items = $status;
						$itemsLen = count($items);
				
						if($itemsLen){
							for($i=0;$i<$itemsLen;$i++){
								if($status[$i]['approval_date'] != '') { $tgl_terima = $status[$i]['approval_date']; }else{ $tgl_terima = '-'; }
								if($status[$i]['notes'] != '') { $ket = $status[$i]['notes']; }else{ $ket = 'Tidak ada keterangan'; }
					?>
					<tr>
						<td><?php echo $status[$i]['stock_code']; ?></td>
						<td><?php echo $status[$i]['stock_name']; ?></td>
						<td><?php echo $status[$i]['qty']; ?></td>
						<td><?php echo number_format($status[$i]['price'],0,'.','.'); ?></td>
						<td><?php echo $tgl_terima; ?></td>
						<td><?php echo $ket; ?></td>
					</tr>
					<?php } } ?>
				</tbody>
            </table>
        </div>
		
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
            <div class="col-md-8 col-sm-6 col-xs-12">
            	<button id="btn-keluar" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" data-dismiss="modal">Keluar</button>
            </div>
        </div>
	</form><!-- /page content -->

<script type="text/javascript">
$(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();
});
</script>
