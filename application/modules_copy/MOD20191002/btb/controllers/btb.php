<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Btb extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('btb/btb_model');
    }

    /**
     * anti sql injection
     */
    public function Anti_sql_injection($string) {
        $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
        return $string;
    }

    /**
      * This function is redirect to index btb page
      * @return Void
      */
    public function index() {
        $this->template->load('maintemplate', 'btb/views/index');
    }

    /**
      * This function is used for showing btb list
      * @return Array
      */
    function lists() {
        $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
        $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
        $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
        $order = $this->input->get_post('order');
        $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
        $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

        $order_fields = array('', 'id');

        $search = $this->input->get_post('search');

        $search_val = (!empty($search['value'])) ? $search['value'] : null;

        $search_value = $this->Anti_sql_injection($search_val);

        // Build params for calling model
        $params['limit'] = (int) $length;
        $params['offset'] = (int) $start;
        $params['order_column'] = $order_fields[$order_column];
        $params['order_dir'] = $order_dir;
        $params['filter'] = $search_value;

        $list = $this->btb_model->lists($params);
		
        $result["recordsTotal"] = $list['total'];
        $result["recordsFiltered"] = $list['total_filtered'];
        $result["draw"] = $draw;
		
        $data = array();
		
		$no=1;
        foreach ($list['data'] as $k => $v) {
			$no = $k+1;
            $no_btb = '<div class="changed_status" onClick="status_btb(\'' . $v['id_po_spb'] . '\')">';
            $no_btb .=      $v['id_po'];
            $no_btb .= '</div>';

            $status = $v['status_po'];
            if($status == 1){
				$stat_btb	= 'Processed';
                $status = '		<div class="changed_status" onClick="detail_status(\'' . $v['id_po_spb'] . '\')">';
                $status .=    		$stat_btb;
                $status .= '	</div>';
            }else{
				$stat_btb	= 'Approved';
                $status = '		<div class="changed_status" onClick="detail_status(\'' . $v['id_po_spb'] . '\')">';
                $status .=    		$stat_btb;
                $status .= '	</div>';
			}
			
            $actions = '';

            if($stat_btb == 'Processed'){
                $actions .= '	<div class="text-center">';
                $actions .= '	<div class="btn-group">';
				$actions .= '		<button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Reject Barang" onClick="reject_btb(\'' . $v['id_po_spb'] . '\')">';
                $actions .= '       	<i class="fa fa-times"></i>';
                $actions .= '   	</button>';
                $actions .= '	</div>';
                $actions .= '	<div class="btn-group">';
                $actions .= '   	<button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Terima Barang" onClick="approve_btb(\'' . $v['id_po_spb'] . '\')">';
                $actions .= '       	<i class="fa fa-check"></i>';
                $actions .= '   	</button>';
                $actions .= '	</div>';
				$actions .= '	<div class="btn-group">';
                $actions .= '   	<button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Detail Barang" onClick="detail_status(\'' . $v['id_po_spb'] . '\')">';
                $actions .= '       	<i class="fa fa-search"></i>';
                $actions .= '   	</button>';
                $actions .= '	</div>';
                $actions .= '	</div>';
            }else{
				$actions .= '	<div class="text-center">';
				$actions .= '	<div class="btn-group">';
                $actions .= '   	<button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Detail Barang" onClick="detail_status(\'' . $v['id_po_spb'] . '\')">';
                $actions .= '       	<i class="fa fa-search"></i>';
                $actions .= '   	</button>';
                $actions .= '	</div>';
                $actions .= '	</div>';
			}
            
			$harga_total = ($v['price'] - ($v['price'] * $v['diskon'] / 100)) * $v['qty'];
			
			if($v['diskon'] != 0 || $v['diskon'] != '') { $diskon = $v['diskon']; }else{ $diskon = 0; }
			if($v['no_po'] != 0 || $v['no_po'] != '') { $po = $v['no_po']; }else{ $po = '-'; }
			
            array_push($data, 
				array(
					$no,
					$v['stock_name'],
					number_format($v['qty'],0,'.','.'),
					$v['symbol_valas'].' '.number_format($v['unit_price'],0,'.','.'),
					$diskon .'%',
					$v['symbol_valas'].' '.number_format($v['price'],0,'.','.'),
					$v['no_spb'],
					$v['no_po'],
					$v['stock_code'],
					$status,
					$v['notes_po'],
					$actions
                )
            );
        }

        $result["data"] = $data;

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

	/**
      * This function is redirect to edit customer page
      * @return Void
      */
    public function reject($idpospb) {
		
		$results = $this->btb_model->reject_detail($idpospb);

        $data = array(
            'reject' => $results
        );
		
        $this->load->view('reject_btb_modal_view',$data);
    }
	
	/**
      * This function is redirect to edit customer page
      * @return Void
      */
    public function reject_btb() {
		$data   = file_get_contents("php://input");
        $params     = json_decode($data,true);
		
        $this->btb_model->reject_btb($params);

        $msg = 'Barang di reject karena, '.$params['keterangan'];

        $result = array(
            'success' => true,
            'message' => $msg
        );

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	/**
      * This function is redirect to edit customer page
      * @return Void
      */
    public function approve($idpospb) {
		
		$results = $this->btb_model->approve_detail($idpospb);
		$result_type = $this->btb_model->type_bc('0');
		$result_type2 = $this->btb_model->list_bc_all('0');
		
        $data = array(
            'approve' => $results,
			'jenis_bc' => $result_type,
			'list_bc_all' => $result_type2
        );
		
        $this->load->view('approve_btb_modal_view',$data);
    }
	
	/**
      * This function is redirect to edit customer page
      * @return Void
      */
    public function approve_btb() {
		$data   = file_get_contents("php://input");
        $params     = json_decode($data,true);
	
		if(count($params) > 0) {
			$get_key = $this->btb_model->get_data($params);
			
			$total_harga = ($get_key[0]['harga'] - ($get_key[0]['harga'] * $get_key[0]['diskon'] / 100)) * $get_key[0]['qty'];
			$base_price = ($get_key[0]['harga'] - ($get_key[0]['harga'] * $get_key[0]['diskon'] / 100));

			$this->btb_model->insert_no_btb($params,$get_key[0]['qty']);
			
			$approve = $this->btb_model->approve_btb($params,$get_key[0]['id_material'],$get_key[0]['qty'],$base_price);
			echo"<pre>";print_r($approve);die;
			if($approve['code'] == '1') {
 
				$data_kartu_hp = array(
					'ref' 			=> NULL, 
					'source' 		=> NUll,
					'keterangan' 	=> 'Pembelian PO '.$get_key[0]['no_po'], 
					'status' 		=> 0,
					'saldo' 		=> $get_key[0]['total_amount'],
					'saldo_akhir' 	=> $get_key[0]['total_amount'], 
					'id_valas'		=> $get_key[0]['valas_id'], 
					'type_kartu'	=> 0,
					'id_master'		=> $get_key[0]['id_po'],
					'type_master'	=> 0,
					'delivery_date'	=> $get_key[0]['delivery_date'],
					'payment_coa'	=> $get_key[0]['payment_coa']
				);
				
				if($get_key[0]['term_of_payment'] == 'CASH'){
						
					$this->btb_model->add_coa_values_cash($data_kartu_hp);
						
				}else{
					
					$check_po = $this->btb_model->check_po($get_key[0]['id_po']);
					
					if($check_po[0]['cnt_po'] == 0){
						$this->btb_model->add_kartu_hp($data_kartu_hp);
						
						$get_data_id = $this->btb_model->get_id_coa($get_key);
						
						$data_coa = array (
							'id_coa'	=> $get_data_id[0]['id_coa'],
							'id_parent'	=> 0,
							'id_valas'	=> $get_key[0]['valas_id'],
							'value'		=> $get_key[0]['total_amount'],
							'adjusment'	=> 0,
							'type_cash'	=> 0,
							'note'		=> $data_kartu_hp['keterangan']
						);
						
						$add_coa = $this->btb_model->add_coa_values($data_coa);
						
						$insert_val_d = $this->btb_model->add_po_spb_coa($add_coa['lastid'],$get_key[0]['id_po']);
					}
				}
				
				$this->btb_model->mutasi_add($get_key[0]);
				
			} else {
				
				$msg = 'Barang Gagal di approve karena, Data belum lengkap silahkan cek kembali.';
			
				$result = array(
					'success' => false,
					'message' => $msg
				);
			
			}
			
			$msg = 'Barang berhasil di approve karena, '.$params['keterangan'];

			$result = array(
				'success' => true,
				'message' => $msg
			);
			
		}else{
			
			$msg = 'Barang Gagal di approve karena, Data belum lengkap silahkan cek kembali.';
			
			$result = array(
				'success' => false,
				'message' => $msg
			);
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	/**
      * This function is redirect to edit customer page
      * @return Void
      */
    public function detail($idpospb) {
		
		$results = $this->btb_model->detail_status($idpospb);
		
        $data = array(
            'status' => $results
        );
		
        $this->load->view('detail_btb_modal_view',$data);
    }

}