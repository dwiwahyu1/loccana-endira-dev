<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Btb_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	/**
      * This function is get the list data in spb table
      * @param : $params is where condition for select query
      */
	public function add_coa_values_cash($data)
	{
		$sql 	= 'CALL coavalue_add2(?,?,?,?,?,?,?,?)';
		
		$query 	= $this->db->query($sql,array(
			$data['payment_coa'],
			0,
			$data['delivery_date'],
			$data['id_valas'],
			$data['saldo'],
			0,
			1,
			$data['keterangan'],
		));
		
		// $return['code'] = $this->db->affected_rows();
		// $return['list'] = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		//return $return;
	}

	public function lists($params = array())
	{
		$sql_all 	= 'CALL btb_list_all(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				''
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL btb_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => count($result),
			'total' => $total_row,
		);
		
		return $return;
	}

	/**
      * This function is get data in spb table by no_spb
      * @param : $id is where condition for select query
      */

	public function reject_btb($data)
	{
		$sql 	= 'CALL btb_update_status(?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_po_spb'],
			$data['id_material'],
			$data['status'],
			$data['status_material'],
			$data['keterangan']
		));
		
		$return = $query->result_array();
		
		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is get data in spb table by no_spb
      * @param : $id is where condition for select query
      */

	public function reject_detail($idpospb)
	{
		$sql 	= 'CALL btb_search_idpo(?)';

		$query 	= $this->db->query($sql,array(
			$idpospb
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	/**
      * This function is get data in spb table by no_spb
      * @param : $id is where condition for select query
      */

	public function approve_btb($data,$id_material,$qty,$base_price)
	{
		$sql 	= 'CALL btb_update_status(?,?,?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_po_spb'],
			$id_material,
			$data['status'],
			$data['status_material'],
			$data['keterangan'],
			$qty,
			$data['no_btb'],
			$data['no_bc'],
			$base_price
		));
		
		$return['code'] = $this->db->affected_rows();
		$return['array'] = $query->result_array();
		
		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function insert_no_btb($data,$qty)
	{
		$sql 	= ' UPDATE `t_spb`
			SET	`t_spb`.`no_btb` = "'.$data['no_btb'].'"
			WHERE  `t_spb`.`id` = (SELECT id_spb FROM t_po_spb WHERE `t_po_spb`.`id` = '.$data['id_po_spb'].' ) ';

		$query 	= $this->db->query($sql);
		//echo "<pre>"; print_r($query);die;
		 
		// $return['code'] = $this->db->affected_rows();
		// $return['array'] = $query->result_array();
		
		$this->db->close();
		$this->db->initialize();

		// return $return;
	}

	/**
      * This function is get data in spb table by no_spb
      * @param : $id is where condition for select query
      */

	public function approve_detail($idpospb)
	{
		$sql 	= 'CALL btb_search_idpo(?)';

		$query 	= $this->db->query($sql,array(
			$idpospb
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function detail_status($idpospb)
	{
		$sql 	= 'CALL btb_search_idpo(?)';

		$query 	= $this->db->query($sql,array(
			$idpospb
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_data($params)
	{
		$sql 	= 'CALL btb_get_data(?)';

		$query 	= $this->db->query($sql,array(
			$params['id_po_spb']
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function check_po($params)
	{
		$sql 	= 'select count(*) as cnt_po from `t_purchase_order` a
		left join `t_po_spb` b on a.`id_po` = b.`id_po`
		where a.`id_po` = ? and b.`status` <> 2';

		$query 	= $this->db->query($sql,array(
			$params
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_id_eksternal($params)
	{
		$sql 	= 'CALL btb_get_id_eksternal(?)';

		$query 	= $this->db->query($sql,array(
			$params['id_distributor']
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function type_bc($type_bc)
	{
		$sql 	= 'SELECT a.*,b.`jenis_bc` AS jb FROM `t_bc` a
					LEFT JOIN `m_type_bc` b ON
					a.`jenis_bc` = b.`id`
					WHERE b.`type_bc` = ?
					GROUP BY b.`jenis_bc`, a.no_pendaftaran';

		$query 	= $this->db->query($sql,array(
			$type_bc
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function list_bc_all($type_bc)
	{
		$sql 	= 'SELECT * FROM `m_type_bc` b
					WHERE b.`type_bc` = ?
					';

		$query 	= $this->db->query($sql,array(
			$type_bc
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_id_coa($params)
	{
		$sql 	= 'CALL btb_get_id_coa(?,?)';

		$query 	= $this->db->query($sql,array(
			$params[0]['id_po'],$params[0]['id_distributor']
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_kartu_hp($data)
	{
		$sql 	= 'CALL hp_add(?,?,?,?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['ref'],
			$data['source'],
			$data['keterangan'],
			$data['status'],
			$data['saldo'],
			$data['saldo_akhir'],
			$data['id_valas'],
			$data['type_kartu'],
			$data['id_master'],
			$data['type_master']
		));
		
		//echo $query;die;
		
		$return['code'] = $this->db->affected_rows();
		//$return['array'] = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function mutasi_add($data)
	{
		$sql 	= 'CALL mutasi_add(?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_material'],
			$data['id_material'],
			$data['qty'],
			0
		));
		
		//echo $query;die;
		
		$return['code'] = $this->db->affected_rows();
		//$return['array'] = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_po_spb_coa($id_coa_value,$id_po_spb)
	{
		$sql 	= 'CALL hp_po_spb_coa_add(?,?)';

		$query 	= $this->db->query($sql,array(
			$id_coa_value,
			$id_po_spb
		));
		
		$return['code'] = $this->db->affected_rows();
		$return['array'] = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_coa_values($data)
	{
		$sql 	= 'CALL coavalue_add(?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_coa'],
			$data['id_parent'],
			$data['id_valas'],
			$data['value'],
			$data['adjusment'],
			$data['type_cash'],
			$data['note']
		));
		
		$return['result'] = $query->result_array();
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$return['lastid'] = $lastid;

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

}