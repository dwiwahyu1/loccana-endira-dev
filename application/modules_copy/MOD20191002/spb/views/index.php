<style>
  .changed_status{cursor:pointer;text-decoration: underline;color:#96b6e8;}
  .changed_status:hover{color:#ff8c00}
</style>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Permintaan Barang</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">

                    <table id="listspb" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Customer Name</th>
                          <th>Date</th>
                          <th>Department</th>
                          <th>Total Items</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>No</td>
                          <td>Customer Name</td>
                          <td>Date</td>
                          <td>Department</td>
                          <td>Total Items</td>
                          <td>Actions</td>
                        </tr>
                      </tbody>
                    </table>

            </div>
        </div><!-- end col -->
    </div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width:950px;">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <p></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width:950px;">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <p></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild-barang" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width:950px;">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <p></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
  function add_spb(){
      $('#panel-modal').removeData('bs.modal');
      $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modal  .panel-body').load('<?php echo base_url('spb/add');?>');
      $('#panel-modal  .panel-title').html('<i class="fa fa-plus"></i> Add SPB');
      $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
  }

  function add_item(){
      $('#panel-modalchild').removeData('bs.modal');
      $('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modalchild  .panel-body').load('<?php echo base_url('spb/add_item');?>');
      $('#panel-modalchild  .panel-title').html('<i class="fa fa-plus"></i> Tambah Barang');
      $('#panel-modalchild').modal({backdrop:'static',keyboard:false},'show');
  }

  function add_new_item(){
      $('#panel-modalchild-barang').removeData('bs.modal');
      $('#panel-modalchild-barang  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modalchild-barang  .panel-body').load('<?php echo base_url('spb/add_new_item');?>');
      $('#panel-modalchild-barang  .panel-title').html('<i class="fa fa-plus"></i> Tambah Barang Baru');
      $('#panel-modalchild-barang').modal({backdrop:'static',keyboard:false},'show');
  }
  
  function edit_spb(nospb){
      $('#panel-modal').removeData('bs.modal');
      $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modal  .panel-body').load('<?php echo base_url('spb/edit/');?>'+"/"+nospb);
      $('#panel-modal  .panel-title').html('<i class="fa fa-pencil"></i> Edit SPB');
      $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
  }

  function edit_item(){
      $('#panel-modalchild').removeData('bs.modal');
      $('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modalchild  .panel-body').load('<?php echo base_url('spb/edit_item');?>');
      $('#panel-modalchild  .panel-title').html('<i class="fa fa-plus"></i> Tambah Barang');
      $('#panel-modalchild').modal({backdrop:'static',keyboard:false},'show');
  }

  function approval_spb(nospb){
      $('#panel-modal').removeData('bs.modal');
      $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modal  .panel-body').load('<?php echo base_url('spb/edit_approval');?>'+"/"+nospb);
      $('#panel-modal  .panel-title').html('<i class="fa fa-pencil"></i> Edit status');
      $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
  }

  function detail_spb(nospb){
      $('#panel-modal').removeData('bs.modal');
      $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modal  .panel-body').load('<?php echo base_url('spb/detail');?>'+"/"+nospb);
      $('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Detail SPB');
      $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
  }

  function detail_status(nospb){
      $('#panel-modal').removeData('bs.modal');
      $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modal  .panel-body').load('<?php echo base_url('spb/detail_status');?>'+"/"+nospb);
      $('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Detail Status');
      $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
  }

  function deletespb(nospb){
    swal({
        title: 'Yakin akan Menghapus ?',
        text: 'data tidak dapat dikembalikan bila sudah dihapus !',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak'
      }).then(function () {
          var datapost={
            "nospb"  :   nospb
          };

          $.ajax({
                type:'POST',
                url: "<?php echo base_url().'spb/delete_spb';?>",
                data:JSON.stringify(datapost),
                cache:false,
                contentType: false,
                processData: false,
                success: function(response) {
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                        listspb();
                    })
                }
            });
      });
  }

function listspb(){
	$("#listspb").dataTable({
		"processing": true,
		"serverSide": true,
		"ajax": "<?php echo base_url().'spb/lists/';?>",
		"searchDelay": 700,
		"responsive": true,
		"lengthChange": false,
		"destroy": true,
		"info": false,
		"bSort": false,
		"dom": 'l<"toolbar">frtip',
		"initComplete": function(){
			var element = '<div class="btn-group pull-left">';
				element += '  <a class="btn btn-primary" onClick="add_spb()">';
				element += '    <i class="fa fa-plus"></i> Add SPB';
				element += '  </a>';
				element += '</div>';
			$("div.toolbar").prepend(element);
		}
	});
}

	$(document).ready(function(){
      listspb();
  });
</script>