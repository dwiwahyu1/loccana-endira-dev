<script type="text/javascript" src="<?php echo base_url('assets'); ?>/gentelella-master/vendors/parsleyjs/dist/parsley.min.js"></script>
<style>
	#loading-us{display:none}
	#tick{display:none}

	#loading-mail{display:none}
	#cross{display:none}
	.form-item{margin-top: 15px;overflow: auto;}
</style>

		<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
		<br>
		<div class="item form-group form-item">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="kode_stok">Kode Stok <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input data-parsley-minlength="4" data-parsley-maxlength="20" type="text" id="kode_stok" name="kode_stok" class="form-control col-md-7 col-xs-12" placeholder="Kode Stok" required="required">
				<span id="loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking kode stok...</span>
				<span id="tick"></span>
			</div>
		</div>
					  
		<div class="item form-group form-item">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Stok <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input data-parsley-maxlength="255" type="text" id="nama_stok" name="nama_stok" class="form-control col-md-7 col-xs-12" placeholder="Nama Stok" required="required">
			</div>
		</div>
				  
		<div class="item form-group form-item">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Deskripsi Stok <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<textarea data-parsley-maxlength="255" id="desk_stok" name="desk_stok" class="form-control col-md-7 col-xs-12" placeholder="Deskripsi Stok" required="required"></textarea>
			</div>
		</div>

		<div class="item form-group form-item">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_material">Tipe Stok <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<select class="form-control" name="type_material" id="type_material" style="width: 100%" required>
					<option value="" disabled='disabled' selected='selected' >-- Pilih Tipe Stok --</option>
					<?php foreach($type_material as $key) { ?>
						<option value="<?php echo $key['id_type_material']; ?>" ><?php echo $key['type_material_name']; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
				  
		<div class="item form-group form-item">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_material">Unit <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<select class="form-control" name="unit" id="unit" style="width: 100%" required>
					<option value="" disabled='disabled' selected='selected' >-- Pilih Unit --</option>
					<?php foreach($unit as $unit) { ?>
						<option value="<?php echo $unit['id_uom'].','.$unit['uom_name']; ?>" ><?php echo $unit['uom_name']; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
				  
	<!--	<div class="item form-group form-item">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_material">Valas <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<select class="form-control" name="valas" id="valas" style="width: 100%" required>
					<option value="" disabled='disabled' selected='selected' >-- Pilih Valas --</option>
					<?php foreach($valas as $valas) { ?>
						<option value="<?php echo $valas['valas_id']; ?>" ><?php echo $valas['nama_valas']; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>

		<div class="item form-group form-item">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Harga <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input data-parsley-maxlength="255" type="text" id="harga" name="harga" class="form-control col-md-7 col-xs-12" placeholder="Harga" onkeypress="javascript:return isNumber(event)" required="required">
			</div>
		</div>

		<div class="item form-group form-item">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty">Qty <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input data-parsley-maxlength="255" type="text" id="qty2" name="qty2" class="form-control col-md-7 col-xs-12" placeholder="Qty" min="1" maxlength="7" onkeypress="javascript:return isNumber(event)" required="required">
			</div>
		</div> -->

		<div class="item form-group form-item">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_material">Gudang <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<select class="form-control" name="gudang" id="gudang" style="width: 100%" required>
					<option value="" disabled='disabled' selected='selected' >-- Pilih Gudang Penyimpanan --</option>
					<?php foreach($gudang as $gudang) { ?>
						<option value="<?php echo $gudang['id_gudang'].','.$gudang['nama_gudang']; ?>" ><?php echo $gudang['nama_gudang']; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>

		<div class="item form-group form-item">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_prop">Detail <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<select class="form-control" name="detail" id="detail" style="width: 100%" required>
					<option value="" disabled='disabled' selected='selected' >-- Pilih Detail --</option>
					<option value="0" >Tanpa Detail</option>
					<?php foreach($detail as $detail) { ?>
						<option value="<?php echo $detail['id_properties']; ?>" ><?php echo $detail['properties_name']; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
				  
				  
		<div id="detail_div" class="form-item"></div>

		<div class="item form-group form-item">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<button id="btn-submites" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Stok</button>
			</div>
		</div>

<!-- /page content -->

<script type="text/javascript">
$(document).ready(function() {
	$('form').parsley();
	$('[data-toggle="tooltip"]').tooltip();
});

var last_kd_stok = $('#kode_stok').val();
$('#kode_stok').on('input',function(event) {
	if($('#kode_stok').val() != last_kd_stok) {
		kd_stok_check();
	}
});

function kd_stok_check() {
	var kd_stok = $('#kode_stok').val();
	if(kd_stok.length > 3) {
		var post_data = {
			'kode_stok': kd_stok
		};

		$('#tick').empty();
		$('#tick').hide();
		$('#loading-us').show();
		jQuery.ajax({
			type: "POST",
			url: "<?php echo base_url('spb/check_kd_stok');?>",
			data: post_data,
			cache: false,
			success: function(response){
				if(response.success == true){
					$('#kode_stok').css('border', '3px #090 solid');
					$('#loading-us').hide();
					$('#tick').empty();
					$("#tick").append('<span class="fa fa-check"> '+response.message+'</span>');
					$('#tick').show();
				}else {
					$('#kode_stok').css('border', '3px #C33 solid');
					$('#loading-us').hide();
					$('#tick').empty();
					$("#tick").append('<span class="fa fa-close"> '+response.message+'</span>');
					$('#tick').show();
				}
			}
		});
	}else {
		$('#kode_stok').css('border', '3px #C33 solid');
		$('#loading-us').hide();
		$('#tick').empty();
		$("#tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
		$('#tick').show();
	}
}
	
var lastusername = $('#username').val();

$('#username').on('input',function(event) {
   if($('#username').val() != lastusername) {
       username_check();
   }
});

function username_check(){
    var username = $('#username').val();
    if(username.length > 3) {
        var post_data = {
          'username': username
        };

    	$('#tick').empty();
    	$('#tick').hide();
    	$('#loading-us').show();
    	jQuery.ajax({
    	   type: "POST",
    	   url: "<?php echo base_url('users/check_username');?>",
    	   data: post_data,
    	   cache: false,
    	   success: function(response){
    			if(response.success == true){
    				$('#username').css('border', '3px #090 solid');
    				$('#loading-us').hide();
    				$('#tick').empty();
    				$("#tick").append('<span class="fa fa-check"> '+response.message+'</span>');
    				$('#tick').show();
    				}else{
    				$('#username').css('border', '3px #C33 solid');
    				$('#loading-us').hide();
    				$('#tick').empty();
    				$("#tick").append('<span class="fa fa-close"> '+response.message+'</span>');
    				$('#tick').show();
    			}
    		}
    	});
    } else {
        $('#username').css('border', '3px #C33 solid');
        $('#loading-us').hide();
        $('#tick').empty();
        $("#tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
        $('#tick').show();
    }
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

var lastemail = $('#email').val();

$('#email').on('input',function(event) {
   if($('#email').val() != lastemail) {
       email_check();
   }
});

function email_check() {
    var email = $("#email").val();

    $('#cross').empty();
    $('#cross').hide();
    $('#loading-mail').show();

    if (validateEmail(email)) {
        var post_data = {
          'email': email
        };

        jQuery.ajax({
    	   type: "POST",
    	   url: "<?php echo base_url('users/check_email');?>",
    	   data: post_data,
    	   cache: false,
    	   success: function(response){
    			if(response.success == true){
    				$('#email').css('border', '3px #090 solid');
    				$('#loading-mail').hide();
    				$('#cross').empty();
    				$("#cross").append('<span class="fa fa-check"> '+response.message+'</span>');
    				$('#cross').show();
    				}else{
    				$('#email').css('border', '3px #C33 solid');
    				$('#loading-mail').hide();
    				$('#cross').empty();
    				$("#cross").append('<span class="fa fa-close"> '+response.message+'</span>');
    				$('#cross').show();
    			}
    		}
    	});
    } else {
        $('#email').css('border', '3px #C33 solid');
        $('#loading-mail').hide();
        $('#cross').empty();
        $("#cross").append('<span class="fa fa-close"> Not valid email.</span>');
        $('#cross').show();
    }
    return false;
}

$('#btn-submites').on('click',(function(e) {
    if($('#kode_stok').val() === ''){
        swal("Warning", "Kode Barang harus diisi.", "error");
	}else if($('#gudang').val() === '') {
		swal("Warning", "Data Gudang harus diisi.", "error");
    }else{
		$(this).attr('disabled','disabled');
        $(this).text("Memasukkan data...");
		
		var vals = $('#gudang').val();
		var vals_uom = $('#unit').val();
		
		var splits = vals.split(',');
		var splits_uom = vals_uom.split(',');
		
		var gudang_id = splits[0];
		var nama_gudang = splits[1];
		
		var id_uom = splits_uom[0];
		var uom_name = splits_uom[1];
		
		
		var kode_stok     	= $('#kode_stok').val(),
			nama_stok     	= $('#nama_stok').val(),
		    uomId			= id_uom,
			namaUom			= uom_name,
            qty2           	= 0,
            desk_stok       = $('#desk_stok').val(),
            type_material   = $('#type_material').val(),
            detail			= $('#detail').val(),
            gudangId       	= gudang_id,
            namaGudang     	= nama_gudang;
			
		if($('#qty2').val() == '') {
			qty2 = 1;
		}
			
        var newData = {
            kode_stok,
            nama_stok,
			uomId,
			namaUom,
            qty2,
            desk_stok,
			type_material,
			detail,
			gudangId,
			namaGudang
        };
		
		$.ajax({
            type:'POST',
            url: "<?php echo base_url().'spb/save_new_items';?>",
            data:JSON.stringify(newData),
            cache:false,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response.success == true) {
                    swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
                    }).then(function () {
						$('#panel-modalchild-barang').modal('toggle');
						addNewItems(newData);
                    })
                } else{
                    $('#btn-submites').removeAttr('disabled');
                    $('#btn-submites').text("Tambah Barang Baru");
                    swal("Failed!", response.message, "error");
                }
            }
        }).fail(function(xhr, status, message) {
            $('#btn-submites').removeAttr('disabled');
            $('#btn-submites').text("Tambah Barang Baru");
            swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
        });
		
	}

}));

$('#detail').on('change',(function(e) {

    var formData = new FormData();
	formData.append('id', $('#detail').val());

    $.ajax({
        type:'POST',
        url: '<?php echo base_url('spb/get_properties');?>',
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success: function(response) {
            if (response.success == true) {
				var data = response.data;
				var length_data = data.length;
				
				if(length_data > 0 ){
					var form_add = '';
					for(var i=0;i<length_data;i++){
						form_add += ' '+ create_form_element(data[i].type_properties,data[i].detail_name,data[i].param_detail_name);
					}
					$('#detail_div').html(form_add);
					
				}else{
					$('#detail_div').html('');
				}

            } else{
                // $('#btn-submit').removeAttr('disabled');
                // $('#btn-submit').text("Tambah Tipe Material");
                // swal("Failed!", response.message, "error");
            }
        }
    }).fail(function(xhr, status, message) {
        // $('#btn-submit').removeAttr('disabled');
        // $('#btn-submit').text("Tambah Tipe Material");
        // swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
    });
}));

function create_form_element(type,name,name_param) {
	
	var html = '';
	if(type == '1'){
		html += '<div class="item form-group form-item">';
        html += '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">'+name+'</label>';
        html += '<div class="col-md-8 col-sm-6 col-xs-12">';
        html += '<input data-parsley-maxlength="255" type="text" id="'+name_param+'" name="'+name_param+'" class="form-control col-md-7 col-xs-12" placeholder="'+name+'" required="required">';
		html += '</div></div>';
	}else if(type == '2'){
		html += '<div class="item form-group form-item">';
        html += '<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">'+name+'</label>';
        html += '<div class="col-md-8 col-sm-6 col-xs-12">';
        html += '<input data-parsley-maxlength="255" type="text" id="'+name_param+'" name="'+name_param+'" class="form-control col-md-7 col-xs-12" placeholder="'+name+'" required="required">';
		html += '</div></div>';
	}
	
	return html;
}

function isNumber(evt) {
	var iKeyCode = (evt.which) ? evt.which : evt.keyCode
	if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
		return false;

	return true;
} 

</script>
