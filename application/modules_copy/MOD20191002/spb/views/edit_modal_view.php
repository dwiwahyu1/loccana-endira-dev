  <style>
	  #loading-us{display:none}
	  #tick{display:none}

	  #loading-mail{display:none}
	  #cross{display:none}
    #add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
    #add_item:hover{color:#ff8c00}
  </style>
  <form class="form-horizontal form-label-left" id="edit_spb" role="form" action="<?php echo base_url('spb/edit_spb');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

  		<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
		<br>
        <div class="item form-group">
        	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Spb No
        		<span class="required">
        			<sup>*</sup>
        		</span>
            </label>
            <div class="col-md-8 col-sm-6 col-xs-12">
               	<input data-parsley-maxlength="255" type="text" id="no_spb" name="no_spb" class="form-control col-md-7 col-xs-12" placeholder="Spb No" value="<?php if(isset($detail[0]['no_spb'])){ echo $detail[0]['no_spb']; }?>" readonly>
            </div>
        </div>
            
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">SPB Date
            	<span class="required">
            		<sup>*</sup>
            	</span>
            </label>
            <div class="col-md-8 col-sm-6 col-xs-12">
				      <div class="input-group date">
					       <input placeholder="SPB Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="spb_date" name="spb_date" required="required" value="<?php if(isset($detail[0]['tanggal_spb'])){ echo $detail[0]['tanggal_spb']; }?>">
					       <div class="input-group-addon">
						        <span class="glyphicon glyphicon-th"></span>
					       </div>
				      </div>
			     </div>
       	</div>

       	<div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Detail Customer : </label>
        </div>

        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Customer Name 
              <span class="required">
                <sup>*</sup>
              </span>
            </label>
            <div class="col-md-8 col-sm-6 col-xs-12">
                <input data-parsley-maxlength="255" type="text" id="cust_name" name="cust_name" class="form-control col-md-7 col-xs-12" placeholder="Customer Name" required="required"  value="<?php if(isset($detail[0]['cust_name'])){ echo $detail[0]['cust_name']; }?>" disabled="disabled">
                <input type="hidden" id="id_cust" name="id_cust" value="<?php if(isset($detail[0]['id_cust'])){ echo $detail[0]['id_cust']; }?>">     
            </div>
        </div>

        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Department 
            	<span class="required">
            		<sup>*</sup>
            	</span>
            </label>
            <div class="col-md-8 col-sm-6 col-xs-12">
                <input data-parsley-maxlength="255" type="text" id="department" name="department" class="form-control col-md-7 col-xs-12" placeholder="Department" required="required" value="<?php if(isset($detail[0]['departemen'])){ echo $detail[0]['departemen']; }?>" disabled="disabled">
                        
            </div>
        </div>

        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Barang : </label>
            <div class="col-md-8 col-sm-6 col-xs-12" id="add_item" onclick="add_item()">
              Tambah Barang
            </div>
        </div>

         <div class="item form-group">
         	<table id="listaddedmaterial" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
	          <thead>
	            <tr>
                  	<th>Nama Barang</th>
	                <th>Satuan</th>
	                <th>Qty</th>
                  	<th>Tanggal Diperlukan</th>
                  	<th>Gudang</th>
                  	<th>Keterangan</th>
	                <th>Option</th>
	             </tr>
	          </thead>
              <tbody>
              	<?php
					$itemsLen = count($detail);
					if($itemsLen){
	              		for($i=0;$i<$itemsLen;$i++){
              	?>
		              	<tr>
			                <td><?php echo $detail[$i]['nama_material']; ?></td>
			                <td><?php echo $detail[$i]['uom_symbol']; ?></td>
			                <td><?php echo $detail[$i]['qty']; ?></td>
			                <td><?php echo $detail[$i]['tanggal_diperlukan']; ?></td>
			                <td><?php echo $detail[$i]['gudang']; ?></td>
			                <td><?php echo $detail[$i]['desc']; ?></td>
			                <td>
			                	<div class="btn-group">
			                		<button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteaddeditem('<?php echo $i;?>')">
			                		<i class="fa fa-trash"></i>
			                	</div>
			                </td>
		              	</tr>
              	<?php
              			}
              		}	
              	?>
              </tbody>
            </table>
         </div>

         <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
            <div class="col-md-8 col-sm-6 col-xs-12">
            	<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Spb</button>
            </div>
         </div>
  </form><!-- /page content -->

<script type="text/javascript">
  var items = [];
  $(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();

    $("#spb_date").datepicker({
		  format: 'yyyy-mm-dd',
		  autoclose: true,
		  todayHighlight: true,
	  });
  });

  $('#edit_spb').on('submit',(function(e) {
    if(items.length && $('#id_cust').val() !== ''){
        $('#btn-submit').attr('disabled','disabled');
        $('#btn-submit').text("Mengubah data...");
        e.preventDefault();

        var no_spb = $('#no_spb').val(),
            spb_date = $('#spb_date').val(),
            id_cust = $('#id_cust').val();

        var datapost={
              "no_spb"      :   no_spb,
              "spb_date"    :   spb_date,
              "id_cust"     :   id_cust,
              "items"       :   items
            };

        $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:JSON.stringify(datapost),
            cache:false,
            contentType: false,
            processData: false,
            success: function(response) {
                if (response.success == true) {
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                      $('.panel-heading button').trigger('click');
                      listspb();
                    })
                } else{
                    $('#btn-submit').removeAttr('disabled');
                    $('#btn-submit').text("Edit SPB");
                    swal("Failed!", response.message, "error");
                }
            }
        }).fail(function(xhr, status, message) {
            $('#btn-submit').removeAttr('disabled');
            $('#btn-submit').text("Edit SPB");
            swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
        });
    }else{
      swal("Failed!", "Harus menambah barang dahulu", "error");
    }
    return false;
  }));

  "<?php
    for($i=0;$i<$itemsLen;$i++){
      $nama_material = $detail[$i]['nama_material'];
      $id_uom = $detail[$i]['id_uom'];
      $qty = $detail[$i]['qty'];
      $tgl_diperlukan = $detail[$i]['tanggal_diperlukan'];
      $gudang_id = $detail[$i]['id_gudang'];
      $gudang = $detail[$i]['gudang'];
      $desc = $detail[$i]['desc'];
      $uom_symbol = $detail[$i]['uom_symbol'];
      $notes = $detail[$i]['notes'];
  ?>"
      var item_name = "<?php echo $nama_material; ?>";
      var unit = "<?php echo $id_uom; ?>";
      var unittext = "<?php echo $uom_symbol; ?>";
      var qty = "<?php echo $qty; ?>";
      var tgl_diperlukan = "<?php echo $tgl_diperlukan; ?>";
      var gudang_id = "<?php echo $gudang_id; ?>";
      var gudang = "<?php echo $gudang; ?>";
      var desc = "<?php echo $desc; ?>";
      var notes = "<?php echo $notes; ?>";

      var dataitem = {
          item_name,
          unit,
          qty,
          tgl_diperlukan,
          notes,
          gudang_id,
          gudang,
          desc,
          unittext
      };

      items.push(dataitem);
  "<?php    
    }
  ?>"

  function deleteaddeditem(id){
      items.splice(id,1)

      itemsElem();
  }

  function itemsElem(){
      var itemslen = items.length; 
      var element ='<thead>';
          element +='   <tr>';
          element +='     <th>Nama Barang</th>';
          element +='     <th>Satuan</th>';
          element +='     <th>Qty</th>';
          element +='     <th>Tanggal Diperlukan</th>';
          element +='     <th>Gudang</th>';
          element +='     <th>Keterangan</th>';
          element +='     <th>Option</th>';
          element +='   </tr>';
          element +='</thead>';
          element +='<tbody>';
      for(var i=0;i<itemslen;i++){
          element +='   <tr>';
          element +='     <td>'+items[i]["item_name"]+'</td>';
          element +='     <td>'+items[i]["unittext"]+'</td>';
          element +='     <td>'+items[i]["qty"]+'</td>';
          element +='     <td>'+items[i]["tgl_diperlukan"]+'</td>';
          element +='     <td>'+items[i]["gudang"]+'</td>';
          element +='     <td>'+items[i]["desc"]+'</td>';
          element +='     <td>';
          element +='       <div class="btn-group">';
          element +='         <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteaddeditem('+i+')">';
          element +='         <i class="fa fa-trash"></i>';
          element +='       </div>';
          element +='     </td>';
          element +='   </tr>';
      }
          element +='</tbody>';

      $('#listaddedmaterial').html(element);
  }
</script>
