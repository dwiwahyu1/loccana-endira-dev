  <style>
	  #loading-us{display:none}
	  #tick{display:none}

	  #loading-mail{display:none}
	  #cross{display:none}
    #add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
    #add_item:hover{color:#ff8c00}
  </style>
  <form class="form-horizontal form-label-left" id="add_spb" role="form" action="<?php echo base_url('spb/edit_spb');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

  		<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
		<br>
        <div class="item form-group">
        	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Spb No
        		<span class="required">
        			<sup>*</sup>
        		</span>
            </label>
            <div class="col-md-8 col-sm-6 col-xs-12">
               	<input data-parsley-maxlength="255" type="text" id="no_spb" name="no_spb" class="form-control col-md-7 col-xs-12" placeholder="Spb No" value="<?php if(isset($detail[0]['no_spb'])){ echo $detail[0]['no_spb']; }?>" readonly>
            </div>
        </div>
            
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">SPB Date
            	<span class="required">
            		<sup>*</sup>
            	</span>
            </label>
            <div class="col-md-8 col-sm-6 col-xs-12">
				<div class="input-group date">
					<input placeholder="SPB Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="spb_date" name="spb_date" required="required" value="<?php if(isset($detail[0]['tanggal_spb'])){ echo $detail[0]['tanggal_spb']; }?>" readonly>
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-th"></span>
					</div>
				</div>
			</div>
       	</div>
		<hr>
       	<div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Detail Customer : </label>
        </div>
		<hr>
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Customer Name 
              <span class="required">
                <sup>*</sup>
              </span>
            </label>
            <div class="col-md-8 col-sm-6 col-xs-12">
                <input data-parsley-maxlength="255" type="text" id="cust_name" name="cust_name" class="form-control col-md-7 col-xs-12" placeholder="Customer Name" required="required"  value="<?php if(isset($detail[0]['cust_name'])){ echo $detail[0]['cust_name']; }?>" disabled="disabled">
                <input type="hidden" id="id_cust" name="id_cust" value="<?php if(isset($detail[0]['id_cust'])){ echo $detail[0]['id_cust']; }?>">     
            </div>
        </div>

        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Department 
            	<span class="required">
            		<sup>*</sup>
            	</span>
            </label>
            <div class="col-md-8 col-sm-6 col-xs-12">
                <input data-parsley-maxlength="255" type="text" id="department" name="department" class="form-control col-md-7 col-xs-12" placeholder="Department" required="required" value="<?php if(isset($detail[0]['departemen'])){ echo $detail[0]['departemen']; }?>" disabled="disabled">
                        
            </div>
        </div>

        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Barang : </label>
        </div>

        <div class="item form-group">
         	<table id="listaddedmaterial" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
	          <thead>
	            <tr>
                  <th>Nama Barang</th>
	              <th>Satuan</th>
	              <th>Qty</th>
                  <th>Tanggal Diperlukan</th>
				  <th>Gudang</th>
                  <th>Keterangan</th>
				   <th>Status</th>
	             </tr>
	          </thead>
              <tbody>
              	<?php
	              	$items = $detail;
					$itemsLen = count($items);

					if($itemsLen){
	              		for($i=0;$i<$itemsLen;$i++){
              	?>
		              	<tr>
			                <td><?php echo $detail[$i]['nama_barang']; ?></td>
			                <td><?php echo $detail[$i]['uom_symbol']; ?></td>
			                <td><?php echo $detail[$i]['qty']; ?></td>
			                <td><?php echo $detail[$i]['tanggal_diperlukan']; ?></td>
			                <td><?php echo $detail[$i]['gudang']; ?></td>
			                <td><?php echo $detail[$i]['desc']; ?></td>
							<td><?php 
							if($detail[$i]['status'] == 1){
								echo "Menunggu";
							}elseif($detail[$i]['status'] == 2){
								echo "Ditolak";
							}elseif($detail[$i]['status'] == 0){
								echo "Diterima";
							}
							
						
							?></td> 
		              	</tr>
              	<?php
              			}
              		}	
              	?>
				</tbody>
            </table>
        </div>
  </form><!-- /page content -->

<script type="text/javascript">
  var items = [];
  $(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();

    $("#spb_date").datepicker({
		  format: 'yyyy-mm-dd',
		  autoclose: true,
		  todayHighlight: true,
	  });
});
</script>
