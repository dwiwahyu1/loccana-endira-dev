<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Spb_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}

	/**
      * This function is get the list data in uom table
      */
	public function uom()
	{
		$sql 	= 'SELECT id_uom,uom_symbol FROM m_uom;';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function search_material()
	{
		$sql 	= 'SELECT DISTINCT a.id,a.stock_code,a.stock_name,a.stock_description,b.id_uom,b.uom_name FROM m_material AS a LEFT JOIN m_uom AS b ON a.unit = b.id_uom ORDER by a.id DESC';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function search_gudang()
	{
		$sql 	= 'SELECT id_gudang,nama_gudang FROM t_gudang';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	/**
      * This function is get the list data in spb table
      * @param : $params is where condition for select query
      */

	public function lists($params = array())
	{
		$sql_all 	= 'CALL spb_list_all(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				''
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL spb_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => count($result),
			'total' => $total_row,
		);

		return $return;
	}

	/**
      * This function is used to Insert Record in spb table
      * @param : $data - record array 
      */

	public function add_spb($data)
	{
		$items = $data['items'];
		$itemsLen = count($items);

		for($i=0;$i<$itemsLen;$i++){
			$sql 	= 'CALL spb_add(?,?,?,?,?,?,?,?,?,?,?,?)';

			$query 	=  $this->db->query($sql,
				array(
					$data['no_spb'],
					$data['id_cust'],
					$data['spb_date'],
					$items[$i]["iditems"],
					0,
					$items[$i]["qty1"],
					$items[$i]["tgl_diperlukan"],
					$items[$i]['desc'],
					1,
					0,
					0,
					$items[$i]['gudang_id']
				)
			);
		}
		
		$this->db->close();
		$this->db->initialize();
	}

	public function add_material($data)
	{
		$sql 	= 'CALL material_add(?,?,?,?,?,?,?,?,?,?,?)';
		
		$query 	=  $this->db->query($sql,
			array(
				"",
				$data['kode_stok'],
				$data['nama_stok'],
				$data['desk_stok'],
				$data['uomId'],
				$data['type_material'],
				$data['qty2'],
				10,
				$data['detail'],
				$data['gudangId'],
				4
			));

		//print_r($query);die;
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		
		return $arr_result;
	}
	
	/**
     * This function is used to delete spb
     * @param: $nospb - nospb of spb table
     */
	function delete_spb($nospb) {
		$this->db->where('no_spb', $nospb);  
		$this->db->delete('t_spb'); 

		$this->db->close();
		$this->db->initialize();
	}

	/**
      * This function is get data in spb table by no_spb
      * @param : $id is where condition for select query
      */

	public function detail($nospb)
	{
		$sql 	= 'CALL spb_search_id(?)';

		$query 	= $this->db->query($sql,array(
				$nospb
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is used to Update Record in customer table
      * @param : $data - updated array 
      */

	public function change_status($data)
	{
		$user_id = $this->session->userdata['logged_in']['user_id'];

		$sql 	= 'UPDATE t_spb SET status=?, notes=?, id_pic=?, approval_date=NOW() WHERE no_spb=?;';

		$this->db->query($sql,
			array(
				$data['status'],
				$data['notes'],
				$user_id,
				$data['no_spb']
			));

		$this->db->close();
		$this->db->initialize();
	}

	/**
      * This function is get data in spb table by no_spb
      * @param : $id is where condition for select query
      */
	public function detail_status($nospb)
	{
		$sql 	= 'SELECT 
							customer.nama AS cust_name,
							`t_spb`.approval_date,
							 pic.nama AS pic_name,
							 CASE
	                            WHEN status = 0 THEN "Preparing"
	                            WHEN status = 1 THEN "Accepted"
	                            ELSE "Rejected"
	                         END AS `status`,
	                         notes
					FROM 
							`t_spb`
        					LEFT JOIN u_user customer ON t_spb.id_user=customer.id
        					LEFT JOIN u_user pic ON t_spb.id_pic=pic.id  
        			WHERE 
        					no_spb=?';

		$query 	= $this->db->query($sql,array(
				$nospb
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is used to Insert Record in spb table
      * @param : $id is where condition for select query
      */
	public function edit_spb($data)
	{
		$this->db->where('no_spb', $data['no_spb']);  
		$this->db->delete('t_spb');

		$items = $data['items'];
		$itemsLen = count($items);

		for($i=0;$i<$itemsLen;$i++){
			$sql 	= 'CALL spb_add(?,?,?,?,?,?,?,?,?,?,?,?,?)';

			$query 	=  $this->db->query($sql,
				array(
					$data['no_spb'],
					$data['id_cust'],
					$data['spb_date'],
					$items[$i]["item_name"],
					$items[$i]["unit"],
					$items[$i]["qty"],
					$items[$i]["tgl_diperlukan"],
					$items[$i]['desc'],
					0,
					$items[$i]['notes'],
					NULL,
					NULL,
					$items[$i]['gudang_id']
				));
		}
		
		$this->db->close();
		$this->db->initialize();
	}
}