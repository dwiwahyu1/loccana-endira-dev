<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spb extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('spb/spb_model');
        $this->load->model('material/material_model');
		$this->load->library('log_activity');
    }

    /**
     * anti sql injection
     */
    public function Anti_sql_injection($string) {
        $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
        return $string;
    }

    /**
      * This function is redirect to index spb page
      * @return Void
      */
    public function index() {
        $this->template->load('maintemplate', 'spb/views/index');
    }

    /**
      * This function is used for showing spb list
      * @return Array
      */
    function lists() {
        $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
        $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
        $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
        $order = $this->input->get_post('order');
        $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
        $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

        $order_fields = array('', 'id');

        $search = $this->input->get_post('search');

        $search_val = (!empty($search['value'])) ? $search['value'] : null;

        $search_value = $this->Anti_sql_injection($search_val);

        // Build params for calling model
        $params['limit'] = (int) $length;
        $params['offset'] = (int) $start;
        $params['order_column'] = $order_fields[$order_column];
        $params['order_dir'] = $order_dir;
        $params['filter'] = $search_value;

        $list = $this->spb_model->lists($params);

        $result["recordsTotal"] = $list['total'];
        $result["recordsFiltered"] = $list['total_filtered'];
        $result["draw"] = $draw;

        $data = array();

        foreach ($list['data'] as $k => $v) {
            $no_spb = '<div class="changed_status" onClick="detail_spb(\'' . $v['no_spb'] . '\')">';
            $no_spb .=      $v['no_spb'];
            $no_spb .= '</div>';

            $status = $v['status'];
            if($v['status'] != "Preparing"){
                $status = '<div class="changed_status" onClick="detail_status(\'' . $v['no_spb'] . '\')">';
                $status .=      $v['status'];
                $status .= '</div>';
            }

            $actions = '';
            if($v['status'] == "Preparing"){
                $actions .= '<div class="btn-group">';
                $actions .= '   <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edit_spb(\'' . $v['no_spb'] . '\')">';
                $actions .= '       <i class="fa fa-edit"></i>';
                $actions .= '   </button>';
                $actions .= '</div>';
            }

            $actions .= '<div class="btn-group">';
            $actions .= '   <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deletespb(\'' . $v['no_spb'] . '\')">';
            $actions .= '       <i class="fa fa-trash"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';

            if($v['status'] == "Preparing"){
                $actions .= '<div class="btn-group">';
                $actions .= '   <button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Approval" onClick="approval_spb(\'' . $v['no_spb'] . '\')">';
                $actions .= '       <i class="fa fa-check"></i>';
                $actions .= '   </button>';
                $actions .= '</div>';
            }
            
            array_push($data, array(
                $no_spb,
                $v['cust_name'],
                $v['tanggal_spb'],
                $v['departemen'],
                $v['total_products'],
                $actions
                    )
            );
        }

        $result["data"] = $data;

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is redirect to add spb page
      * @return Void
      */
    public function add() {
        

        $this->load->view('add_modal_view');
    }

    /**
      * This function is redirect to add spb page
      * @return Void
      */
    public function add_item() {
        $result = $this->spb_model->uom();
        $result_brg = $this->spb_model->search_material();
		//print_r($result_brg);die;
        $result_gudang = $this->spb_model->search_gudang();
		$unit = $this->material_model->unit();



        $data = array(
            'uom' => $result,
            'unit' => $unit,
            'material' => $result_brg,
            'gudang' => $result_gudang
        );

        $this->load->view('add_modal_item_view',$data);
    }
	
	public function add_new_item() {
		$type_material = $this->material_model->type_material();
        $valas = $this->material_model->valas();
        $unit = $this->material_model->unit();
        $detail = $this->material_model->detail_prop();
        $gudang = $this->material_model->gudang();
		
        $data = array(
            'type_material' => $type_material,
            'valas' => $valas,
            'unit' => $unit,
            'detail' => $detail,
            'gudang' => $gudang
        );

        $this->load->view('add_new_item_modal_view',$data);
    }
	
    /**
      * This function is used to delete spb data
      * @return Array
      */
    public function add_spb() {
        $data   = file_get_contents("php://input");
        $params     = json_decode($data,true);
		
        $this->spb_model->add_spb($params);

        $msg = 'Berhasil menambah data SPB';

        $result = array(
            'success' => true,
            'message' => $msg
        );

		$this->log_activity->insert_activity('insert', $msg. ' dengan No SPB ' .$params['no_spb']);
		
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	public function check_kd_stok(){
		$this->form_validation->set_rules('kode_stok', 'KodeStok', 'trim|required|min_length[4]|max_length[20]|is_unique[m_material.stock_code]');
		$this->form_validation->set_message('is_unique', 'Kode Stok Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'Kode Stok Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}
	
	public function save_new_items() {
        $data   = file_get_contents("php://input");
        $params     = json_decode($data,true);
        
		$this->spb_model->add_material($params);

        $msg = 'Berhasil menambah data material';

        $result = array(
            'success' => true,
            'message' => $msg
        );

		$this->log_activity->insert_activity('insert', $msg. ' dengan Kode Stok ' .$params['kode_stok']);
		
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	public function get_properties() {
		 
		$id = $this->Anti_sql_injection($this->input->post('id', TRUE));
		$detail_prop = $this->material_model->detail_prop_full($id);
		 
		$result = array(
            'success' => true,
            'message' => '',
			'data' => $detail_prop
        );
					
		$this->output->set_content_type('application/json')->set_output(json_encode($result,true));
	}
	
    /**
      * This function is used to delete spb data
      * @return Array
      */
    public function delete_spb() {
        $data   = file_get_contents("php://input");
        $params     = json_decode($data,true);

        $this->spb_model->delete_spb($params['nospb']);

        $msg = 'Berhasil menghapus data SPB';

        $result = array(
            'success' => true,
            'message' => $msg
        );

		$this->log_activity->insert_activity('delete', $msg. ' dengan No SPB ' .$params['nospb']);
		
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is redirect to edit customer page
      * @return Void
      */
    public function edit($nospb) {
        $result = $this->spb_model->detail($nospb);

        $data = array(
            'detail' => $result
        );

        $this->load->view('edit_modal_view', $data);
    }

    /**
      * This function is redirect to add spb page
      * @return Void
      */
    public function edit_approval($nospb) {
        $result = $this->spb_model->detail($nospb);

        $data = array(
            'detail' => $result
        );

        $this->load->view('edit_modal_approval_view',$data);
    }

    /**
      * This function is used to change status spb data
      * @return Array
      */
    public function change_status() {
        $data   = file_get_contents("php://input");
        $params     = json_decode($data,true);

        $this->spb_model->change_status($params);

        $msg = 'Berhasil mengubah status SPB';

        $result = array(
            'success' => true,
            'message' => $msg
        );

		$this->log_activity->insert_activity('update', $msg. ' dengan No SPB ' .$params['no_spb']);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is redirect to edit customer page
      * @return Void
      */
    public function detail($nospb) {
        $result = $this->spb_model->detail($nospb);

		//print_r($result);die;

        $data = array(
            'detail' => $result
        );

        $this->load->view('detail_modal_view', $data);
    }

    /**
      * This function is redirect to edit customer page
      * @return Void
      */
    public function detail_status($nospb) {
        $result = $this->spb_model->detail_status($nospb);

        $data = array(
            'detail' => $result
        );

        $this->load->view('detail_modal_approval_view', $data);
    }

    /**
      * This function is used to update spb data
      * @return Array
      */
    public function edit_spb() {
        $data   = file_get_contents("php://input");
        $params = json_decode($data,true);

        $this->spb_model->edit_spb($params);

        $msg = 'Berhasil mengubah data SPB';

        $result = array(
            'success' => true,
            'message' => $msg
        );
		
		$this->log_activity->insert_activity('update', $msg. ' dengan No SPB ' .$params['no_spb']);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
}