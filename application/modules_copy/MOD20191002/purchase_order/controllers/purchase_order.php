<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Purchase_Order extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('purchase_order/purchase_order_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'purchase_order/views/index');
	}

	function list_po() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;
		$order_fields = array('no_po', 'date_po', 'delivery_date', 'id_distributor', 'term_of_payment', 'total_amount', 'status');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->purchase_order_model->list_po($params);
		$listdist = $this->purchase_order_model->distributor();

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		//print_r($list['data']);die;

		$data = array();
		$i = 0;
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$status_akses = "";
			$strStat = "";
			$checkBarang = $this->purchase_order_model->check_approve_po_spb($v['id_po']);
			$checkBarang_accepted = $this->purchase_order_model->check_approve_po_spb_accepted($v['id_po']);
			$checkBarang_rejected = $this->purchase_order_model->check_approve_po_spb_rejected($v['id_po']);
			if($checkBarang['total'] > 0 && $checkBarang_accepted['total'] == 0 && $checkBarang_rejected['total'] == 0) {
				$strStat = "Preparing";
				$status_akses .=
					'<div class="btn-group">'.
						'<button class="btn btn-warning btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
							onClick="edit_po(\'' . $v['id_po'] . '\')">'.
							'<i class="fa fa-edit"></i>'.
						'</button>'.
					'</div>'.
					'<div class="btn-group">'.
						'<button class="btn btn-danger btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Delete"
							onClick="delete_po(\'' . $v['id_po'] . '\')">'.
							'<i class="fa fa-trash"></i>'.
						'</button>'.
					'</div>';
			}else if($checkBarang_accepted['total'] > 0 && $checkBarang_rejected['total'] == 0) {
				$strStat = "<span style='color:green'>Done</span>";
			}else if($checkBarang_accepted['total'] > 0 && $checkBarang_rejected['total'] > 0) {
				$strStat = "<span style='color:red'>Need Respond</span>";
				$status_akses .=
					'<div class="btn-group">'.
						'<button class="btn btn-warning btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
							onClick="edit_po(\'' . $v['id_po'] . '\')">'.
							'<i class="fa fa-edit"></i>'.
						'</button>'.
					'</div>'.
					'<div class="btn-group">'.
						'<button class="btn btn-danger btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Delete"
							onClick="delete_po(\'' . $v['id_po'] . '\')">'.
							'<i class="fa fa-trash"></i>'.
						'</button>'.
					'</div>';
			}else {
				if($checkBarang['total'] == 0 && $checkBarang_rejected['total'] > 0) $strStat = "<span style='color:red'>Need Respond</span>";
				else $strStat = "Preparing";
				$status_akses .=
					'<div class="btn-group">'.
						'<button class="btn btn-warning btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
							onClick="edit_po(\'' . $v['id_po'] . '\')">'.
							'<i class="fa fa-edit"></i>'.
						'</button>'.
					'</div>'.
					'<div class="btn-group">'.
						'<button class="btn btn-danger btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Delete"
							onClick="delete_po(\'' . $v['id_po'] . '\')">'.
							'<i class="fa fa-trash"></i>'.
						'</button>'.
					'</div>';
			}

			$status_akses .= 
				'<div class="btn-group">'.
					'<button class="btn btn-info btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Details"
						onClick="details_po(\'' . $v['id_po'] . '\')">'.
						'<i class="fa fa-search"></i>'.
					'</button>'.
				'</div>';

			if($checkBarang['total'] > 0) {
				$status_akses .=
					'<div class="btn-group">'.
						'<button class="btn btn-success btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Approval"
							onClick="approve_po(\'' . $v['id_po'] . '\')">'.
							'<i class="fa fa-check"></i>'.
						'</button>'.
					'</div>';
			}

			$strDist = "";
			foreach ($listdist as $l => $o) {
				if($v['id_distributor'] == $o['id']) $strDist = $o['name_eksternal'];
			}

			array_push($data, array(
				$v['no_po'],
				date('d M Y', strtotime($v['date_po'])),
				$strDist,
				$v['symbol_valas']." ".number_format($v['total_amount'],0,",","."),
				date('d M Y', strtotime($v['delivery_date'])),
				ucwords($v['term_of_payment']),
				$strStat,
				$status_akses
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_po() {
		$result_dist = $this->purchase_order_model->distributor();
		$result_coa = $this->purchase_order_model->coa();
		$coa = $this->purchase_order_model->coa_list();
		$valas = $this->purchase_order_model->valas_list();

		$data = array(
			'distributor' => $result_dist,
			'coa' => $result_coa,
			'coa_list' => $coa,
			'valas_list' => $valas
		);

		$this->load->view('add_modal_view', $data);
	}

	public function add_item() {
		$result_spb = $this->purchase_order_model->spb();

		$data = array(
			'spb' => $result_spb
		);

		$this->load->view('add_modal_item_view',$data);
	}

	public function get_spb() {
		$data		= file_get_contents("php://input");
		$params		= json_decode($data,true);
		$list		= $this->purchase_order_model->get_spb_no($params['no_spb']);
		$result_uom	= $this->purchase_order_model->uom();
		
		$data = array();
		$tempObj = new stdClass();
		$uom = "";

		if(sizeof($params['tempBarang']) > 0) {
			foreach ($params['tempBarang'] as $p => $pk) {
				for ($i=0; $i <= sizeof($list); $i++) {
					if(!empty($list[$i])) {
						if($list[$i]['no_spb'] == $pk[1] && $list[$i]['nama_material'] == $pk[2]) {
							unset($list[$i]);
						}
					}
				}
			}
		}

		$i = 0;
		foreach ($list as $k => $v) {
			foreach ($result_uom as $k_o => $v_o) {
				if($v_o['id_uom'] == $v['id_uom']) $uom = $v_o['uom_symbol'];
			}
			$strUnitPrice =
				'<input type="number" class="form-control " min="0" id="unit_price'.$i.'" name="unit_price'.$i.'" style="height:25px; width: 100px;" onInput="cal_price('.$i.')" value="0">';
			$strAmount =
				'<input type="number" class="form-control" min="0" id="amount" name="amount'.$i.'" style="height:25px; width: 100px;" value="0" readonly>';
			$strDiskon =
				'<input type="number" class="form-control" min="0" max="100" id="diskon'.$i.'" name="dsikon'.$i.'" style="height:25px; width: 100px;" onInput="cal_price('.$i.')" value="0">';
			$strRemark =
				'<input type="text" class="form-control" id="remark'.$i.'" name="remark'.$i.'" style="height:25px; width: 100px;">';
			$strOption =
				'<div class="checkbox">'.
					'<input id="option['.$i.']" type="checkbox" value="'.$i.'">'.
					'<label for="option['.$i.']"></label>'.
				'</div>';

			array_push($data, array(
				$v['id'],
				$v['no_spb'],
				$v['stock_name'],
				$v['uom_name'],
				number_format((int)$v['qty'],0,'',''),
				$strUnitPrice,
				$strDiskon,
				$strAmount,
				$strRemark,
				$strOption
			));
			$i++;
		}
		
		$res = array('status' => 'success', 'data' => $data);

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function check_nopo() {
		$this->form_validation->set_rules('no_po', 'No Purchase Order', 'trim|required|min_length[4]|max_length[100]|is_unique[t_purchase_order.no_po]');
		$this->form_validation->set_message('is_unique', 'No Purchase Order Already Exists.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'No Purchase Order Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}

	public function save_po() {
		// $this->form_validation->set_rules('no_po', 'No Purchase Order', 'trim|required|min_length[4]|max_length[100]|is_unique[t_purchase_order.no_po]');
		// $this->form_validation->set_rules('tgl_po', 'Tanggal PO', 'trim|required');
		// //$this->form_validation->set_rules('coa', 'COA', 'trim|required');
		// $this->form_validation->set_rules('dist', 'Distributor', 'trim|required');
		// $this->form_validation->set_rules('tgl_delivery', 'Tanggal Delivery', 'trim|required');
		// $this->form_validation->set_rules('term_of_pay', 'Term Of Payment', 'trim|required');

		// if ($this->form_validation->run() == FALSE) {
			// $pesan = validation_errors();
			// $msg = strip_tags(str_replace("\n", '', $pesan));

			// $result = array(
				// 'success' => false,
				// 'message' => $msg
			// );

			// $this->output->set_content_type('application/json')->set_output(json_encode($result));
		// }else {
			$no_po			= $this->Anti_sql_injection($this->input->post('no_po', TRUE));
			$tglpo			= $this->Anti_sql_injection($this->input->post('tgl_po', TRUE));
			//$coa 			= $this->Anti_sql_injection($this->input->post('coa', TRUE));
			$dist 			= $this->Anti_sql_injection($this->input->post('dist', TRUE));
			$tgldelivery 	= $this->Anti_sql_injection($this->input->post('tgl_delivery', TRUE));
			$term_of_pay 	= ucwords($this->Anti_sql_injection($this->input->post('term_of_pay', TRUE)));
			$amount_barang 	= $this->Anti_sql_injection($this->input->post('amount_barang', TRUE));
			$valas 			= $this->Anti_sql_injection($this->input->post('valas', TRUE));
			
			//echo $amount_barang;die;

			$temp_tgl_po		= explode("/", $tglpo);
			$tgl_po 			= date('Y-m-d', strtotime($temp_tgl_po[2].'-'.$temp_tgl_po[1].'-'.$temp_tgl_po[0]));
			$temp_tgl_delivery	= explode("/", $tgldelivery);
			$tgl_delivery 		= date('Y-m-d', strtotime($temp_tgl_delivery[2].'-'.$temp_tgl_delivery[1].'-'.$temp_tgl_delivery[0]));

			if ($term_of_pay == 'CASH') {
				$coa_s = $this->Anti_sql_injection($this->input->post('id_coa', TRUE));
			}else {
				$coa_s = 0;
			}

			$data = array(
				'no_po'			=> $no_po,
				'tgl_po'		=> $tgl_po,
				//'coa'			=> $coa,
				'dist'			=> $dist,
				'tgl_delivery'	=> $tgl_delivery,
				'term_of_pay'	=> $term_of_pay,
				'amount'		=> $amount_barang,
				'pay_coa'		=> $coa_s, 
				'valas'			=> $valas 
			);

			$result = $this->purchase_order_model->add_po($data);
			
			//$this->purchase_order_model->add_qty_material($data);

			if ($result['result'] > 0) {
				$this->log_activity->insert_activity('insert', 'Insert Purchase Order');
				$result = array('success' => true, 'message' => 'Berhasil menambahkan Purchase Order ke database', 'lastid' => $result['lastid']);
			}else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert Purchase Order');
				$result = array('success' => false, 'message' => 'Gagal menambahkan Purchase Order ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
	//	}
	}

	public function save_barang_po() {
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		$arrData = array();

		foreach ($params['listbarang'] as $k => $v) {
			$arrTemp = array(
				'id_po'			=> $params['id_po'],
				'id_spb'		=> $v[0],
				'unit_price'	=> preg_replace('/\D/', '', $v[5]),
				'diskon'		=> $v[6],
				'price'			=> preg_replace('/\D/', '', $v[7]),
				'remark'		=> $v[8],
				'status'		=> '0'
			);

			$result = $this->purchase_order_model->add_barang($arrTemp);
		}
		
		if ($result > 0) {
			$this->log_activity->insert_activity('insert', 'Insert Barang Purchase Order');
			$result = array('success' => true, 'message' => 'Berhasil menambahkan Purchase Order ke database');
		}else {
			$this->log_activity->insert_activity('insert', 'Gagal Insert Barang Purchase Order');
			$result = array('success' => false, 'message' => 'Gagal menambahkan Purchase Order ke database');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result);
	}

	public function edit_po($id) {
		$result_po = $this->purchase_order_model->edit_po($id);
		$result_dist = $this->purchase_order_model->distributor();
		$result_coa = $this->purchase_order_model->coa();

		$temptgl_po = explode('-', $result_po[0]['date_po']);
		$result_po[0]['date_po'] = date('d/M/Y', strtotime($temptgl_po[0].'-'.$temptgl_po[1].'-'.$temptgl_po[2]));
		$temptgl_delivery = explode('-', $result_po[0]['delivery_date']);
		$result_po[0]['delivery_date'] = date('d/M/Y', strtotime($temptgl_delivery[0].'-'.$temptgl_delivery[1].'-'.$temptgl_delivery[2]));

		$data = array(
			'po' => $result_po,
			'distributor' => $result_dist,
			'coa' => $result_coa
		);
		// print_r($result_po);die;

		$this->load->view('edit_modal_view', $data);
	}

	public function get_po_spb($id_po) {
		$list = $this->purchase_order_model->get_po_spb($id_po);
		$listuom = $this->purchase_order_model->uom();

		$data = array();
		$i = 0;
		$tempTotal = 0;
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list as $k => $v) {
			$amount = (int)$v['price'] * (int)$v['qty'];
			if($v['diskon'] > 0) {
				$diskon = $v['diskon'] / 100;
				$amountVdiskon = $amount * $diskon;
				$amount = $amount - $amountVdiskon;
			}
			$tempTotal = $tempTotal + $amount;
			$strUnitPrice =
				'<input type="number" class="form-control" min="0" id="edit_unit_price'.$i.'" name="edit_unit_price'.$i.'" style="height:25px; width: 100px;" onInput="cal_price_edit('.$i.')" onChange="redrawTable('.$i.')" value="'.$v['price'].'">';
			$strAmount =
				'<input type="number" class="form-control" min="0" id="edit_amount" name="edit_amount'.$i.'" style="height:25px; width: 100px;" value="'.$amount.'" readonly>';
			$strDiskon =
				'<input type="number" class="form-control" min="0" id="edit_diskon'.$i.'" name="edit_diskon'.$i.'" style="height:25px; width: 100px;" onInput="cal_price_edit('.$i.')" onChange="redrawTable('.$i.')" value="'.$v['diskon'].'">';
			$strRemark =
				'<input type="text" class="form-control" id="edit_remark'.$i.'" name="edit_remark'.$i.'" style="height:25px; width: 100px;" onChange="redrawTable('.$i.')" value="'.$v['remarks'].'">';
			if($v['status'] == 1) {
				$strStatus = 'Accepted';
				$strOption = '';
			}else if($v['status'] == 2) {
				$strStatus = 'Rejected';
				$strOption =
					'<button class="btn btn-icon waves-effect waves-light btn-danger">'.
						'<i class="fa fa-trash"></i>'.
					'</button>';
			}else {
				$strStatus = 'Preparing';
				$strOption =
					'<button class="btn btn-icon waves-effect waves-light btn-danger">'.
						'<i class="fa fa-trash"></i>'.
					'</button>';
			}
			$uom = "";
			foreach ($listuom as $l => $o) {
				if($v['id_uom'] == $o['id_uom']) $uom = $o['uom_symbol'];
			}

			array_push($data, array(
				$v['id'],
				$v['id_spb'],
				$v['no_spb'],
				$v['nama_material'],
				$uom,
				$v['qty'],
				$strUnitPrice,
				$v['price'],
				$strDiskon,
				$v['diskon'],
				$strAmount,
				$amount,
				$strRemark,
				$v['remarks'],
				$strStatus,
				$strOption
			));
			$i++;
		}

		$result["data"] = $data;
		$result["total"] = $tempTotal;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_edit_item() {
		$result_spb = $this->purchase_order_model->spb();

		$data = array(
			'spb' => $result_spb
		);

		$this->load->view('edit_modal_item_view',$data);
	}

	public function get_edit_spb() {
		$data		= file_get_contents("php://input");
		$params		= json_decode($data,true);
		$list		= $this->purchase_order_model->get_spb($params['no_spb']);
		$result_uom	= $this->purchase_order_model->uom();
		
		$data = array();
		$tempObj = new stdClass();
		$uom = "";

		if(sizeof($params['tempBarang']) > 0) {
			foreach ($params['tempBarang'] as $p => $pk) {
				for ($i=0; $i <= sizeof($list); $i++) {
					if(!empty($list[$i])) {
						if($list[$i]['id'] == $pk[1] && $list[$i]['no_spb'] == $pk[2]) {
							unset($list[$i]);
						}
					}
				}
			}
		}

		$i = 0;
		foreach ($list as $k => $v) {
			foreach ($result_uom as $k_o => $v_o) {
				if($v_o['id_uom'] == $v['id_uom']) $uom = $v_o['uom_symbol'];
			}
			$strUnitPrice =
				'<input type="number" class="form-control" min="0" id="unit_price'.$i.'" name="unit_price'.$i.'" style="height:25px; width: 100px;" onInput="cal_price('.$i.')" value="0">';
			$strAmount =
				'<input type="number" class="form-control" min="0" id="amount" name="amount'.$i.'" style="height:25px; width: 100px;" value="0" readonly>';
			$strDiskon =
				'<input type="number" class="form-control" min="0" id="edit_item_diskon'.$i.'" name="edit_item_diskon'.$i.'" style="height:25px; width: 100px;" onInput="cal_price('.$i.')" value="0">';
			$strRemark =
				'<input type="text" class="form-control" id="remark'.$i.'" name="remark'.$i.'" style="height:25px; width: 100px;">';
			$strOption =
				'<div class="checkbox">'.
					'<input id="option['.$i.']" type="checkbox" value="'.$i.'">'.
					'<label for="option['.$i.']"></label>'.
				'</div>';
			array_push($data, array(
				$v['id'],
				$v['no_spb'],
				$v['nama_material'],
				$uom,
				number_format($v['qty'],0,'',''),
				$strUnitPrice,
				$strDiskon,
				$strAmount,
				$strRemark,
				$strOption
			));
			$i++;
		}
		
		$res = array(
			'status'	=> 'success',
			'data'		=> $data
		);

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function get_edit_po_spb($id_po) {
		$list = $this->purchase_order_model->get_po_spb($id_po);
		$listuom = $this->purchase_order_model->uom();

		$data = array();
		$i = 0;
		$tempTotal = 0;
		$username = $this->session->userdata['logged_in']['username'];
		// print_r($list);die;
		foreach ($list as $k => $v) {
			$amount = (int)$v['unit_price'] * (int)$v['qty'];
			if($v['diskon'] > 0) {
				$unit_price = (int)$v['unit_price'] * (int)$v['qty'];
				$diskon = $v['diskon'] / 100;
				$amountVdiskon = $unit_price * $diskon;
				$amount = $amount - $amountVdiskon;
			}
			$tempTotal = $tempTotal + $amount;
			$strUnitPrice =
				'<input type="number" class="form-control" min="0" id="edit_unit_price'.$i.'" name="edit_unit_price'.$i.'" style="height:25px; width: 100px;" onInput="cal_price_edit('.$i.')" onChange="redrawTable('.$i.')" value="'.number_format($v['unit_price'],0,"","").'">';
			$strAmount =
				'<input type="number" class="form-control" min="0" id="edit_amount" name="edit_amount'.$i.'" style="height:25px; width: 100px;" value="'.$amount.'" readonly>';
			$strDiskon =
				'<input type="number" class="form-control" min="0" id="edit_diskon'.$i.'" name="edit_diskon'.$i.'" style="height:25px; width: 100px;" onInput="cal_price_edit('.$i.')" onChange="redrawTable('.$i.')" value="'.number_format($v['diskon'],0,"","").'">';
			$strRemark =
				'<input type="text" class="form-control" id="edit_remark'.$i.'" name="edit_remark'.$i.'" style="height:25px; width: 100px;" onChange="redrawTable('.$i.')" value="'.$v['remarks'].'">';
			if($v['status'] == 1) {
				$strStatus = 'Accepted';
				$strOption = '';
			}else if($v['status'] == 2) {
				$strStatus = 'Rejected';
				$strOption =
					'<button class="btn btn-icon waves-effect waves-light btn-danger">'.
						'<i class="fa fa-trash"></i>'.
					'</button>';
			}else {
				$strStatus = 'Preparing';
				$strOption =
					'<button class="btn btn-icon waves-effect waves-light btn-danger">'.
						'<i class="fa fa-trash"></i>'.
					'</button>';
			}
			$uom = "";
			foreach ($listuom as $l => $o) {
				if($v['id_uom'] == $o['id_uom']) $uom = $o['uom_symbol'];
			}

			array_push($data, array(
				$v['id_pospb'],
				$v['id_spb'],
				$v['no_spb'],
				$v['nama_material'],
				$uom,
				number_format($v['qty'],0,"",""),
				$strUnitPrice,
				$v['unit_price'],
				$strDiskon,
				$v['diskon'],
				$strAmount,
				$amount,
				$strRemark,
				$v['remarks'],
				$strStatus,
				$strOption
			));
			$i++;
		}

		$result["data"] = $data;
		$result["total"] = $tempTotal;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function delete_po_spb() {
		$data 	= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		$result = $this->purchase_order_model->delete_po_spb($params['id']);

		if($result > 0) {
			$this->log_activity->insert_activity('delete', 'Delete Purchase Order id : '.$params['id']);
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}else {
			$this->log_activity->insert_activity('delete', 'Gagal Delete Purchase Order id : '.$params['id']);
			$res = array('status' => 'failed', 'message' => 'Data gagal di hapus');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function save_edit_po() {
		$this->form_validation->set_rules('no_po', 'No Purchase Order', 'trim|required|min_length[4]|max_length[100]');
		$this->form_validation->set_rules('tgl_po', 'Tanggal PO', 'trim|required');
		$this->form_validation->set_rules('coa', 'COA', 'trim|required');
		$this->form_validation->set_rules('dist', 'Distributor', 'trim|required');
		$this->form_validation->set_rules('tgl_delivery', 'Tanggal Delivery', 'trim|required');
		$this->form_validation->set_rules('term_of_pay', 'Term Of Payment', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$id_po 			= $this->Anti_sql_injection($this->input->post('id_po', TRUE));
			$no_po			= $this->Anti_sql_injection($this->input->post('no_po', TRUE));
			$tglpo			= $this->Anti_sql_injection($this->input->post('tgl_po', TRUE));
			$coa 			= $this->Anti_sql_injection($this->input->post('coa', TRUE));
			$dist 			= $this->Anti_sql_injection($this->input->post('dist', TRUE));
			$tgldelivery 	= $this->Anti_sql_injection($this->input->post('tgl_delivery', TRUE));
			$term_of_pay 	= ucwords($this->Anti_sql_injection($this->input->post('term_of_pay', TRUE)));
			$amount_barang 	= $this->Anti_sql_injection($this->input->post('amount_barang', TRUE));

			$temp_tgl_po		= explode("/", $tglpo);
			$tgl_po 			= date('Y-m-d', strtotime($temp_tgl_po[2].'-'.$temp_tgl_po[1].'-'.$temp_tgl_po[0]));
			$temp_tgl_delivery	= explode("/", $tgldelivery);
			$tgl_delivery 		= date('Y-m-d', strtotime($temp_tgl_delivery[2].'-'.$temp_tgl_delivery[1].'-'.$temp_tgl_delivery[0]));

			$data = array(
				'id_po'			=> $id_po,
				'no_po'			=> $no_po,
				'tgl_po'		=> $tgl_po,
				'coa'			=> $coa,
				'dist'			=> $dist,
				'tgl_delivery'	=> $tgl_delivery,
				'term_of_pay'	=> $term_of_pay,
				'amount'		=> $amount_barang
			);

			$result = $this->purchase_order_model->save_edit_po($data);

			if ($result['result'] > 0) {
				$this->log_activity->insert_activity('update', 'Update Purchase Order id : '.$id_po);
				$result = array('success' => true, 'message' => 'Berhasil mengubah Purchase Order ke database', 'id_po' => $id_po);
			}else {
				$this->log_activity->insert_activity('update', 'Update Purchase Order id : '.$id_po);
				$result = array('success' => false, 'message' => 'Gagal mengubah Purchase Order ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function save_edit_barang_po() {
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		
		$arrData = array();
		$statSave = false;
		foreach ($params['listbarang'] as $k => $v) {
			if($v[0] != '') {
				if($v[14] != 'Accepted') {
					$arrTemp = array(
						'id'			=> $v[0],
						'id_po'			=> $params['id_po'],
						'id_spb'		=> $v[1],
						'unit_price'	=> $v[7],
						'diskon'		=> $v[9],
						'price'			=> $v[11],
						'remark'		=> $v[13],
						'status'		=> '0'
					);
					$resultBarang = $this->purchase_order_model->edit_barang($arrTemp);

					$get_material = $this->purchase_order_model->get_material($v[0]);
					if(sizeof($get_material) > 0) {
						if($get_material[0]['nama_material']) {
							$dataMaterial = array(
								'id_material'	=> $get_material[0]['nama_material'],
								'status'		=> NULL
							);
							$resultMaterial = $this->purchase_order_model->edit_status_material($dataMaterial);

							if ($resultBarang > 0 || $resultMaterial > 0) $statSave = true;
							else $statSave = false;
						}
					}
				}
			}else {
				$arrTemp = array(
					'id_po'			=> $params['id_po'],
					'id_spb'		=> $v[1],
					'unit_price'	=> $v[7],
					'diskon'		=> $v[9],
					'price'			=> $v[11],
					'remark'		=> $v[13],
					'status'		=> '0'
				);

				$result = $this->purchase_order_model->add_barang($arrTemp);
				if ($result > 0) $statSave = true;
				else $statSave = false;
			}
		}

		if ($statSave == true) {
			$this->log_activity->insert_activity('update', 'Update Barang Purchase Order id : '.$params['id_po']);
			$result = array('success' => true, 'message' => 'Berhasil mengubah barang Purchase Order ke database');
		}else {
			$this->log_activity->insert_activity('update', 'Gagal Update Barang Purchase Order id : '.$params['id_po']);
			$result = array('success' => false, 'message' => 'Gagal mengubah barang Purchase Order ke database');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result);
	}

	public function delete_po() {
		$data			= file_get_contents("php://input");
		$params			= json_decode($data,true);
		$result_pospb	= $this->purchase_order_model->delete_pospb_by_po($params['id']);
		$result_po 		= $this->purchase_order_model->delete_po($params['id']);

		if($result_po > 0 && $result_pospb > 0) {
			$this->log_activity->insert_activity('delete', 'Delete Purchase Order id : '.$params['id']);
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}else {
			$this->log_activity->insert_activity('delete', 'Gagal Delete Purchase Order id : '.$params['id']);
			$res = array('status' => 'success', 'message' => 'Data gagal di hapus');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function detail_po($id) {
		$result_po = $this->purchase_order_model->edit_po($id);
		$result_dist = $this->purchase_order_model->distributor();
		$result_coa = $this->purchase_order_model->coa();
		$temptgl_po = explode('-', $result_po[0]['date_po']);
		$result_po[0]['date_po'] = date('d/M/Y', strtotime($temptgl_po[0].'-'.$temptgl_po[1].'-'.$temptgl_po[2]));
		$temptgl_delivery = explode('-', $result_po[0]['delivery_date']);
		$result_po[0]['delivery_date'] = date('d/M/Y', strtotime($temptgl_delivery[0].'-'.$temptgl_delivery[1].'-'.$temptgl_delivery[2]));

		foreach ($result_dist as $kd => $vd) {
			if($vd['id'] == $result_po[0]['id_distributor']) {
				$result_po[0]['id_distributor'] = $vd['name_eksternal'];
			}
		}

		foreach ($result_coa as $kc => $vc) {
			if($vc['id_coa'] == $result_po[0]['id_coa']) {
				$result_po[0]['id_coa'] = $vc['keterangan'];
			}
		}

		$data = array(
			'po'	=> $result_po
		);

		$this->load->view('detail_po_view', $data);
	}

	public function list_detail_po_barang($id_po) {
		$list = $this->purchase_order_model->get_po_spb($id_po);
		$result_uom	= $this->purchase_order_model->uom();
		
		$data = array();
		$i = 0;
		$tempTotal = 0;
		$username = $this->session->userdata['logged_in']['username'];
		// print_r($list);die;
		foreach ($list as $k => $v) {
			$amount = (int)$v['unit_price'] * (int)$v['qty'];
			if($v['diskon'] > 0) {
				$diskon 		= $v['diskon'] / 100;
				$unit_price 	= (int)$v['unit_price'] * (int)$v['qty'];
				$amountVdiskon 	= $unit_price * $diskon;
				$amount 		= $amount - $amountVdiskon;
			}
			$tempTotal = $tempTotal + $amount;
			$uom = "";
			foreach ($result_uom as $l => $o) {
				if($v['id_uom'] == $o['id_uom']) $uom = $o['uom_symbol'];
			}

			if($v['status'] == '0') $strStatus = 'Preparing';
			else if($v['status'] == '1') $strStatus = 'Accepted';
			else if($v['status'] == '2') $strStatus = 'Rejected';
			else $strStatus = 'Unknown';

			array_push($data, array(
				$v['no_spb'],
				$v['nama_material'],
				$uom,
				number_format($v['qty'],0,',','.'),
				$v['symbol_valas'].' '.number_format($v['unit_price'],0,',','.'),
				$v['diskon'].'%',
				$v['symbol_valas'].' '.number_format($amount,0,',','.'),
				$v['remarks'],
				$strStatus
			));
			$i++;
		}

		$result["data"] = $data;
		$result['symbol_valas'] = $v['symbol_valas'];
		$result["total"] = $v['symbol_valas'].' '.number_format($tempTotal,0,",",".");

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function approval_po($id_po) {
		$result_po = $this->purchase_order_model->edit_po($id_po);
		$result_dist = $this->purchase_order_model->distributor();
		$result_coa = $this->purchase_order_model->coa();

		$temptgl_po = explode('-', $result_po[0]['date_po']);
		$temptgl_delivery = explode('-', $result_po[0]['delivery_date']);

		$strPO = array(
			'date_po'		=> date('d/M/Y', strtotime($temptgl_po[0].'-'.$temptgl_po[1].'-'.$temptgl_po[2])),
			'delivery_date'	=> date('d/M/Y', strtotime($temptgl_delivery[0].'-'.$temptgl_delivery[1].'-'.$temptgl_delivery[2]))
		);

		foreach ($result_dist as $kd => $vd) {
			if($vd['id'] == $result_po[0]['id_distributor']) {
				$strPO['distributor'] = $vd['name_eksternal'];
			}
		}

		foreach ($result_coa as $kc => $vc) {
			if($vc['id_coa'] == $result_po[0]['id_coa']) {
				$strPO['coa'] = $vc['keterangan'];
			}
		}

		$checkBarang = $this->purchase_order_model->check_approve_po_spb($id_po);

		$data = array(
			'po'			=> $result_po,
			'strPo'			=> $strPO,
			'barangApprove'	=> $checkBarang
		);

		$this->load->view('approval_view', $data);
	}

	public function list_approval_po_barang($id_po) {
		$list = $this->purchase_order_model->get_po_spb($id_po);
		$result_uom	= $this->purchase_order_model->uom();
		
		$data = array();
		$i = 0;
		$tempTotal = 0;
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list as $k => $v) {
			$amount = (int)$v['unit_price'] * (int)$v['qty'];
			if($v['diskon'] > 0) {
				$diskon 		= $v['diskon'] / 100;
				$unit_price 	= (int)$v['unit_price'] * (int)$v['qty'];
				$amountVdiskon 	= $unit_price * $diskon;
				$amount 		= $amount - $amountVdiskon;
			}
			$tempTotal = $tempTotal + $amount;
			$uom = "";
			foreach ($result_uom as $l => $o) {
				if($v['id_uom'] == $o['id_uom']) $uom = $o['uom_symbol'];
			}

			if($v['status'] == '0') {
				$strOption =
					'<div class="checkbox">'.
						'<input id="option['.$i.']" type="checkbox" value="'.$v['id'].'" onClick="setList()">'.
						'<label for="option['.$i.']"></label>'.
					'</div>';
			}else $strOption = '';

			if($v['status'] == '0') $strStatus = 'Preparing';
			else if($v['status'] == '1') $strStatus = 'Accepted';
			else if($v['status'] == '2') $strStatus = 'Rejected';
			else $strStatus = 'Unknown';

			array_push($data, array(
				$v['no_spb'],
				$v['nama_material'],
				$uom,
				number_format($v['qty'],0,',','.'),
				$v['symbol_valas'].' '.number_format($v['unit_price'],0,",","."),
				$v['diskon'].'%',
				$v['symbol_valas'].' '.number_format($amount,0,",","."),
				$v['remarks'],
				$strStatus,
				$strOption
			));
			$i++;
		}

		$result["data"] = $data;
		$result['symbol_valas'] = $v['symbol_valas'];
		$result["total"] = $v['symbol_valas'].' '.number_format($tempTotal,0,",",".");

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function change_status() {
		$this->form_validation->set_rules('status', 'Status', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$id_po 			= $this->Anti_sql_injection($this->input->post('id_po', TRUE));
			$id_po_spb		= $this->Anti_sql_injection($this->input->post('id_po_spb', TRUE));
			$status			= $this->Anti_sql_injection($this->input->post('status', TRUE));
			$notes			= $this->Anti_sql_injection($this->input->post('notes', TRUE));

			$statSave = false;
			$arr_po_spb = explode(';', $id_po_spb);
			foreach ($arr_po_spb as $kps) {
				$get_material = $this->purchase_order_model->get_material_approve($kps);
				if($get_material[0]['nama_material']) {
					$data = array(
						'stat_approve'	=> 2,
						'id_po_spb'		=> $kps,
						'status'		=> $status,
						'approval_date'	=> date('Y-m-d'),
						'notes'			=> $notes
					);
					if($status == 1) {
						$dataMaterial = array(
							'id_material'	=> $get_material[0]['nama_material'],
							'status'		=> 5
						);
					}else {
						$dataMaterial = array(
							'id_material'	=> $get_material[0]['nama_material'],
							'status'		=> 2
						);
					}
					$resultApprove = $this->purchase_order_model->approval_po($data);
					$resultMaterial = $this->purchase_order_model->edit_status_material($dataMaterial);
				}
				if ($resultApprove > 0 && $resultMaterial > 0) $statSave = true;
			};

			if ($statSave == true) {
				$this->log_activity->insert_activity('update', 'Update Status Purchase Order id : '.$id_po_spb);
				$result = array('success' => true, 'message' => 'Berhasil mengubah status Purchase Order', 'id_po' => $id_po);
			}else {
				$this->log_activity->insert_activity('update', 'Gagal Update Status Purchase Order id : '.$id_po_spb);
				$result = array('success' => false, 'message' => 'Gagal mengubah status Purchase Order');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}
}