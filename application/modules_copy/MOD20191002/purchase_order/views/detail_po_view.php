<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script> -->
<style type="text/css">
	tbody .dt-body-left{text-align: left;}
	tbody .dt-body-right{text-align: right;}
</style>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<label class="col-md-2">No PO :</label>
			<label class="col-md-10"><?php if(isset($po[0]['no_po'])){ echo $po[0]['no_po']; }?></label>
		</div>

		<div class="row">
			<label class="col-md-2">Tanggal PO :</label>
			<label class="col-md-10"><?php if(isset($po[0]['date_po'])){ echo $po[0]['date_po']; }?></label>
		</div>

		<div class="row">
			<label class="col-md-2">COA :</label>
			<label class="col-md-10"><?php if(isset($po[0]['id_coa'])){ echo $po[0]['id_coa']; }?></label>
		</div>

		<div class="row">
			<label class="col-md-2">Distributor :</label>
			<label class="col-md-10"><?php if(isset($po[0]['id_distributor'])){ echo $po[0]['id_distributor']; }?></label>
		</div>

		<div class="row">
			<label class="col-md-2">Tanggal Delivery :</label>
			<label class="col-md-10"><?php if(isset($po[0]['delivery_date'])){ echo $po[0]['delivery_date']; }?></label>
		</div>

		<div class="row">
			<label class="col-md-2">Term of Payment :</label>
			<label class="col-md-10"><?php if(isset($po[0]['term_of_payment'])){ echo $po[0]['term_of_payment']; }?></label>
		</div>

		<div class="row">
			<label class="col-md-12" for="nama">Daftar Barang :</label>
			<div class="col-md-12">
				
			</div>
		</div>
	</div>
</div>

<table id="listdetailbarang" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th>No SPB</th>
			<th>Deskripsi</th>
			<th>UOM</th>
			<th>QTY</th>
			<th>Unit Price</th>
			<th>Diskon</th>
			<th>Amount</th>
			<th>Remark</th>
			<th>Status</th>
		</tr>
	</thead>
	<tbody></tbody>
	<tfoot>
		<tr>
			<th colspan="6" style="text-align: right;">Total</th>
			<td style="text-align: right;" id="td_detailPO_Total">Rp. </td>
			<td colspan="2"></td>
		</tr>
	</tfoot>
</table>

<script type="text/javascript">
	$(document).ready(function(){
		dt_barang();
	});

	function dt_barang() {
		$('#listdetailbarang').DataTable( {
			"processing": true,
			"searching": false,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"ajax": {
				"type" : "GET",
				"url" : "<?php echo base_url().'purchase_order/list_detail_po_barang/'.$po[0]['id_po'];?>",
				"dataSrc": function(response) {
					$('#td_detailPO_Total').html(response.total);
					return response.data;
				}
			},
			"columnDefs": [
				{"className": 'dt-body-right', "targets": [3,4,5,6]}
			]
		});
	}
</script>