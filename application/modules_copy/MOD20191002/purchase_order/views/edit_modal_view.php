<!--Parsley-->
<!-- <script type="text/javascript" src="<?php echo base_url('assets'); ?>/gentelella-master/vendors/parsleyjs/dist/parsley.min.js"></script> -->
<style>
	table {font-size: 10px;}
	#no_po_loading-us{display:none}
	#no_po_tick{display:none}

	.add_edit_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
	.add_edit_item:hover{color:#ff8c00}
</style>
	
<form class="form-horizontal form-label-left" id="edit_form" role="form" action="<?php echo base_url('purchase_order/save_edit_po');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_po">No PO <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-minlength="4" data-parsley-maxlength="100" type="text" id="no_po" name="no_po" class="form-control col-md-7 col-xs-12" placeholder="no PO minimal 4 karakter" value="<?php if(isset($po[0]['no_po'])){ echo $po[0]['no_po']; }?>" readonly>
			<span id="no_po_loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking No Purchase Order...</span>
			<span id="no_po_tick"></span>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tgl_po">Tanggal PO <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="100" type="text" id="tgl_po" name="tgl_po" class="form-control col-md-7 col-xs-12" placeholder="tanggal purchase order" value="<?php if(isset($po[0]['date_po'])){ echo $po[0]['date_po']; }?>" required="required">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="coa">COA <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="coa" name="coa" style="width: 100%" required>
				<option value="" >-- Select COA --</option>
				<?php foreach($coa as $kc) { ?>
					<option value="<?php echo $kc['id_coa']; ?>" <?php if(isset($po[0]['id_coa'])) { if( $po[0]['id_coa'] == $kc['id_coa'] ) { echo "selected"; } }?>>
						<?php echo $kc['keterangan']; ?>
					</option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="dist">Distributor <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="dist" name="dist" style="width: 100%" required>
				<option value="" >-- Select Distributor --</option>
				<?php foreach($distributor as $dk) { ?>
					<option value="<?php echo $dk['id']; ?>" <?php if(isset($po[0]['id_distributor'])) { if( $po[0]['id_distributor'] == $dk['id'] ) { echo "selected"; } }?>>
						<?php echo $dk['name_eksternal']; ?>
					</option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tgl_delivery">Tanggal Delivery <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="100" type="text" id="tgl_delivery" name="tgl_delivery" class="form-control col-md-7 col-xs-12" placeholder="tanggal purchase order" value="<?php if(isset($po[0]['delivery_date'])){ echo $po[0]['delivery_date']; }?>" required="required">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="term_of_pay">Term of Payment <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="term_of_pay" name="term_of_pay" style="width: 100%" required>
				<option value="" >-- Select Term --</option>
				<option value="1 Day" <?php if($po[0]['term_of_payment'] == '1 Day') echo "selected"; ?>>1 Day</option>
				<option value="15 Day" <?php if($po[0]['term_of_payment'] == '15 Day') echo "selected"; ?>>15 Day</option>
				<option value="30 Day" <?php if($po[0]['term_of_payment'] == '30 Day') echo "selected"; ?>>30 Day</option>
				<option value="60 Day" <?php if($po[0]['term_of_payment'] == '60 Day') echo "selected"; ?>> 60 Day</option>
				<option value="90 Day" <?php if($po[0]['term_of_payment'] == '90 Day') echo "selected"; ?>>90 Day</option>
				<option value="120 Day" <?php if($po[0]['term_of_payment'] == '120 Day') echo "selected"; ?>>120 Day</option>
				<option value="150 Day" <?php if($po[0]['term_of_payment'] == '150 Day') echo "selected"; ?>>150 Day</option>
				<option value="180 Day" <?php if($po[0]['term_of_payment'] == '180 Day') echo "selected"; ?>>180 Day</option>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Barang : </label>
		<div class="col-md-8 col-sm-6 col-xs-12 add_edit_item">
			<a class="btn btn-primary" onclick="add_edit_item()">
				<i class="fa fa-plus"></i> Tambah Barang
			</a>
			<input type="hidden" id="amount_barang" name="amount_barang" value="0">
		</div>
	</div>

	<div class="item form-group">
		<table id="listeditbarang" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th></th>
					<th></th>
					<th>No SPB</th>
					<th>Deskripsi</th>
					<th>UOM</th>
					<th>QTY</th>
					<th>Unit Price</th>
					<th></th>
					<th>Diskon</th>
					<th></th>
					<th>Amount</th>
					<th></th>
					<th>Remark</th>
					<th></th>
					<th>Status</th>
					<th></th>
				</tr>
			</thead>
			<tbody></tbody>
			<tfoot>
				<tr>
					<th colspan="10" style="text-align: right;">Total</th>
					<td style="text-align: right;" id="tdPO_Total"></td>
					<td colspan="5"></td>
				</tr>
			</tfoot>
		</table>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Purchase Order</button>
			<input type="hidden" id="id_po" name="id_po" value="<?php if(isset($po[0]['id_po'])){ echo $po[0]['id_po']; }?>">
		</div>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	var t_editBarang;

	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
		$('#tgl_po').datepicker({
			format: "dd/M/yyyy",
			autoclose: true,
			todayHighlight: true
		});

		$('#tgl_delivery').datepicker({
			format: "dd/M/yyyy",
			autoclose: true,
			todayHighlight: true
		});

		$("#dist").select2();
		$("#coa").select2();
		dtBarang();
	});

	function dtBarang() {
		t_editBarang = $('#listeditbarang').DataTable( {
			"processing": true,
			"searching": false,
			"responsive": true,
			"lengthChange": false,
			"ajax": {
				"type" : "GET",
				"url" : "<?php echo base_url().'purchase_order/get_edit_po_spb/'.$po[0]['id_po'];?>",
				"dataSrc": function(response) {
					$('#tdPO_Total').html(formatNumber(response.total));
					$('#amount_barang').val(response.total);
                	return response.data;
                }
            },
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0],
				"visible": false,
				"searchable": false
			},{
				"targets": [1],
				"visible": false,
				"searchable": false
			},{
				"targets": [7],
				"visible": false,
				"searchable": false
			},{
				"targets": [9],
				"visible": false,
				"searchable": false
			},{
				"targets": [11],
				"visible": false,
				"searchable": false
			},{
				"targets": [13],
				"visible": false,
				"searchable": false
			}]
		});
	}

	function add_edit_item(){
		$('#panel-modalchild').removeData('bs.modal');
		$('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modalchild  .panel-body').load('<?php echo base_url('purchase_order/add_edit_item');?>');
		$('#panel-modalchild  .panel-title').html('<i class="fa fa-at"></i> Tambah Barang');
		$('#panel-modalchild').modal({backdrop:'static',keyboard:false},'show');
	}

	function cal_price_edit(row) {
		var rowData = t_editBarang.row(row).data();
		var qty = rowData[5];
		var unitPrice = $('#edit_unit_price'+row).val();
		var amount = unitPrice * qty;
		if($('#edit_diskon'+row).val() > 0) {
			var diskon = ($('#edit_diskon'+row).val()/100);
			var amountVdiskon = amount * diskon;
			amount = amount - amountVdiskon;
		}

		$('input[name=edit_amount'+row+']').val(amount);

		var tempTotal = 0;
		$('input[id=edit_amount]').each(function() {
			tempTotal = tempTotal + parseInt(this.value);
		})
		$('#tdPO_Total').html(formatNumber(tempTotal));
		$('#amount_barang').val(tempTotal);
		// t_editBarang.draw();
	}

	function redrawTable(row) {
		var rowData = t_editBarang.row(row).data();
		
		rowData[7] = $('#edit_unit_price'+row).val();
		rowData[9] = $('#edit_diskon'+row).val();
		rowData[11] = $('input[name=edit_amount'+row+']').val();
		rowData[13] = $('#edit_remark'+row).val();
		t_editBarang.draw();
	}

	$('#listeditbarang').on("click", "button", function(){
		var delStat = false;
		var dataBarang = t_editBarang.row($(this).parents('tr')).data();
		if(dataBarang[0] != "") {
			swal({
				title: 'Yakin akan Menghapus Barang yang sudah ada ?',
				text: 'data tidak dapat dikembalikan bila sudah dihapus !',
				type: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak'
			}).then(function () {
				var datapost={
					"id" : parseInt(dataBarang[0])
				};

				$.ajax({
					type: "POST",
					url: "<?php echo base_url();?>purchase_order/delete_po_spb",
					data : JSON.stringify(datapost),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
						if (response.status == "success") {
							swal({
								title: 'Success!',
								text: response.message,
								type: 'success',
								showCancelButton: false,
								confirmButtonText: 'Ok'
							}).then(function () { })
						}else {
							swal("Failed!", response.message, "error");
							delStat = false;
						}
					}
				});
			})
			t_editBarang.row($(this).parents('tr')).remove().draw(false);
		}else t_editBarang.row($(this).parents('tr')).remove().draw(false);

		var tempTotal = 0;

		for (var i = 0; i < t_editBarang.rows().data().length; i++) {
			var rowData = t_editBarang.row(i).data();
			var tempAmount = rowData[11];
			tempTotal = tempTotal + parseInt(tempAmount);
		}
		$('#tdPO_Total').html(formatNumber(tempTotal));
		$('#amount_barang').val(tempTotal);
	});

	var last_noPO = $('#no_po').val();
	$('#no_po').on('input',function(event) {
		if($('#no_po').val().toUpperCase() != last_noPO) nopo_check();
		else {
			$('#no_po').removeAttr("style");
			$('#no_po_tick').empty();
			$('#no_po_tick').hide();
			$('#no_po_loading-us').hide();
		}
	});

	function nopo_check() {
		var nopo = $('#no_po').val();
		if(nopo.length > 3) {
			var post_data = {
				'no_po': nopo
			};

			$('#no_po_tick').empty();
			$('#no_po_tick').hide();
			$('#no_po_loading-us').show();
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url('purchase_order/check_nopo');?>",
				data: post_data,
				cache: false,
				success: function(response){
					if(response.success == true){
						$('#no_po').css('border', '3px #090 solid');
						$('#no_po_loading-us').hide();
						$('#no_po_tick').empty();
						$("#no_po_tick").append('<span class="fa fa-check"> '+response.message+'</span>');
						$('#no_po_tick').show();
					}else {
						$('#no_po').css('border', '3px #C33 solid');
						$('#no_po_loading-us').hide();
						$('#no_po_tick').empty();
						$("#no_po_tick").append('<span class="fa fa-close"> '+response.message+'</span>');
						$('#no_po_tick').show();
					}
				}
			});
		}else {
			$('#no_po').css('border', '3px #C33 solid');
			$('#no_po_loading-us').hide();
			$('#no_po_tick').empty();
			$("#no_po_tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
			$('#no_po_tick').show();
		}
	}

	$('#edit_form').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		var formData = new FormData(this);
		// save_barang(70);
		
		$.ajax({
			type:'POST',
			url: $(this).attr('action'),
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					save_barang(response.id_po);
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Edit Purchase Order");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Edit Purchase Order");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}));

	function save_barang(id_po) {
		var arrTemp = [];
		for (var i = 0; i < t_editBarang.rows().data().length; i++) {
			var rowData = t_editBarang.row(i).data();
			arrTemp.push(rowData);
		}

		var datapost = {
			"id_po"			: id_po,
			'listbarang'	: arrTemp
		};

		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>purchase_order/save_edit_barang_po",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						window.location.href = "<?php echo base_url('purchase_order');?>";
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Edit Purchase Order");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Edit Purchase Order");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}
</script>