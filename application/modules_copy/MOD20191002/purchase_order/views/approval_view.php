<style type="text/css">
	tbody .dt-body-left{text-align: left;}
	tbody .dt-body-right{text-align: right;}
</style>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<label class="col-md-2">No PO :</label>
			<label class="col-md-10"><?php if(isset($po[0]['no_po'])){ echo $po[0]['no_po']; }?></label>
		</div>

		<div class="row">
			<label class="col-md-2">Tanggal PO :</label>
			<label class="col-md-10"><?php if(isset($strPo['date_po'])){ echo $strPo['date_po']; }?></label>
		</div>

		<div class="row">
			<label class="col-md-2">COA :</label>
			<label class="col-md-10"><?php if(isset($strPo['coa'])){ echo $strPo['coa']; }?></label>
		</div>

		<div class="row">
			<label class="col-md-2">Distributor :</label>
			<label class="col-md-10"><?php if(isset($strPo['distributor'])){ echo $strPo['distributor']; }?></label>
		</div>

		<div class="row">
			<label class="col-md-2">Tanggal Delivery :</label>
			<label class="col-md-10"><?php if(isset($strPo['delivery_date'])){ echo $strPo['delivery_date']; }?></label>
		</div>

		<div class="row">
			<label class="col-md-2">Term of Payment :</label>
			<label class="col-md-10"><?php if(isset($po[0]['term_of_payment'])){ echo $po[0]['term_of_payment']; }?></label>
		</div>
		
		<div class="row">
			<label class="col-md-2">Valas :</label>
			<label class="col-md-10"><?php if(isset($po[0]['nama_valas'])){ echo $po[0]['nama_valas']; }?></label>
		</div>

		<div class="row">
			<label class="col-md-12" for="nama">Daftar Barang :</label>
			<div class="col-md-12">
				
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<table id="listdetailbarang" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>No SPB</th>
					<th>Deskripsi</th>
					<th>UOM</th>
					<th>QTY</th>
					<th>Unit Price</th>
					<th>Diskon</th>
					<th>Amount</th>
					<th>Remark</th>
					<th>Status</th>
					<th style="width: 6px;"></th>
				</tr>
			</thead>
			<tbody></tbody>
			<tfoot>
				<tr>
					<th colspan="6" style="text-align: right;">Total</th>
					<td style="text-align: right;" id="td_approve_PO_Total"></td>
					<td colspan="3"></td>
				</tr>
			</tfoot>
		</table>
	</div>
</div>

<?php if(isset($barangApprove)) {
	if($barangApprove['total'] > 0) { ?>
		<div class="row" style="margin-top: 10px;">
			<form class="form-horizontal form-label-left" id="edit_status" role="form" action="<?php echo base_url('purchase_order/change_status');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
				<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

				<div class="item form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Status <span class="required"><sup>*</sup></span></label>
					<div class="col-md-8 col-sm-6 col-xs-12">
						<select class="form-control" name="status" id="status" style="width: 100%" required>
							<option value="1">Accept</option>
							<option value="2">Reject</option>
						</select>
					</div>
				</div>

				<div class="item form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="notes">Notes </label>
					<div class="col-md-8 col-sm-6 col-xs-12">
						<textarea data-parsley-maxlength="255" type="text" id="notes" name="notes" class="form-control col-md-7 col-xs-12" placeholder="Notes"></textarea>
					</div>
				</div>

				<div class="item form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
					<div class="col-md-8 col-sm-6 col-xs-12">
						<button id="btn-change" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Status</button>
						<input type="hidden" id="id_po" name="id_po" value="<?php if(isset($po[0]['id_po'])){ echo $po[0]['id_po']; }?>">
						<input type="hidden" id="id_po_spb" name="id_po_spb" value="">
					</div>
				</div>
			</form>
		</div>
	<?php }
} ?>
<script type="text/javascript">
	$(document).ready(function() {
		dt_barang();
	});

	function dt_barang() {
		$('#listdetailbarang').DataTable( {
			"processing": true,
			"searching": false,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"ajax": {
				"type" : "GET",
				"url" : "<?php echo base_url().'purchase_order/list_approval_po_barang/'.$po[0]['id_po'];?>",
				"dataSrc": function(response) {
					$('#td_approve_PO_Total').html(response.total);
					return response.data;
				}
			},
			"columnDefs": [
				{"className": 'dt-body-right', "targets": [3,4,5,6]}
			]
		});
	}

	function setList() {
		$('#id_po_spb').val('');
		$('input[type=checkbox]').each(function () {
			if(this.checked == true) {
				var po_spb = $('#id_po_spb').val();
				if(po_spb == '') $('#id_po_spb').val(this.value);
				else {
					po_spb += ';' + this.value;
					$('#id_po_spb').val(po_spb);
				}
			}
		});
	}

	$('#edit_status').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();

		if($('#id_po_spb').val() != '') {
			var formData = new FormData(this);

			$.ajax({
				type:'POST',
				url: $(this).attr('action'),
				data: formData,
				cache:false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function () {
							window.location.href = "<?php echo base_url('purchase_order');?>";
						})
					}else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Edit Status");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Edit Status");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}else {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Edit Status");
			swal("Failed!", "Barang tidak ada yang di pilih, silahkan cek kembali.", "error");
		}
	}));
</script>