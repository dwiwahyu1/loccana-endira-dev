<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Purchase_Order_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function coa_list() {
		$sql 	= "SELECT * FROM `m_list_coa` a
				LEFT JOIN t_coa b ON a.id_coa = b.id_coa
				WHERE a.type = 'Payment'";

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function valas_list() {
		$sql 	= "SELECT * FROM m_valas ";

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function distributor() {
		$sql 	= 'SELECT * FROM t_eksternal WHERE type_eksternal = 2';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function coa() {
		$sql 	= 'SELECT * FROM t_coa';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function spb() {
		// $sql 	= 'SELECT id, no_spb FROM t_spb WHERE status = "1" GROUP BY no_spb';
		$sql = 'SELECT a.id, a.no_spb FROM t_spb a LEFT JOIN t_po_spb b ON a.id = b.id_spb WHERE b.id_spb IS NULL AND a.STATUS = "1" GROUP BY no_spb';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_spb($no_spb) {
		$sql 	= 'CALL spb_search_id(?)';

		$query 	=  $this->db->query($sql,
			array($no_spb)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_spb_no($no_spb) {
		$sql 	= 'CALL pospb_search_no_spb(?)';

		$query 	=  $this->db->query($sql,
			array($no_spb)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function uom() {
		$sql 	= 'SELECT id_uom,uom_symbol FROM m_uom;';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function check_userid($params = array()) {
		$sql 	= 'SELECT COUNT(id) AS jumlah FROM u_user WHERE id = ? AND status_akses = 1;';
		$query 	= $this->db->query($sql,
			array($params['user_id'])
		);
		$return = $query->row_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function list_po($params = array()) {
		$sql 	= 'CALL porder_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function add_po($data) {
		$sql 	= 'CALL porder_add(?,?,?,?,?,?,?,?,?)';
		$query 	=  $this->db->query($sql,
			array(
				$data['no_po'],
				$data['tgl_po'],
				$data['tgl_delivery'],
				$data['dist'],
				$data['term_of_pay'],
				$data['amount'],
				'0',
				$data['pay_coa'],
				$data['valas']
			)
		);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_barang($data) {
		$sql 	= 'CALL pospb_add(?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id_po'],
				$data['id_spb'],
				$data['unit_price'],
				$data['price'],
				$data['diskon'],
				$data['remark'],
				$data['status']
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_po($id) {
		$sql 	= 'CALL porder_search_id(?)';

		$query 	=  $this->db->query($sql,
			array($id)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_po_spb($id) {
		$sql 	= 'CALL pospb_search_id(?)';

		$query 	=  $this->db->query($sql,
			array($id)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function delete_po_spb($data) {
		$sql 	= 'CALL pospb_delete(?)';
		$query 	=  $this->db->query($sql,
			array($data)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function save_edit_po($data) {
		$sql 	= 'CALL porder_update(?,?,?,?,?,?,?,?,?)';
		$query 	=  $this->db->query($sql,
			array(
				$data['id_po'],
				$data['no_po'],
				$data['tgl_po'],
				$data['tgl_delivery'],
				$data['dist'],
				$data['term_of_pay'],
				$data['amount'],
				'0',
				$data['coa']
			)
		);
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit_barang($data) {
		$sql 	= 'CALL pospb_update(?,?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['id'],
				$data['id_po'],
				$data['id_spb'],
				$data['unit_price'],
				$data['price'],
				$data['diskon'],
				$data['remark'],
				$data['status']
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function delete_po($data) {
		$sql 	= 'CALL porder_delete(?)';
		$query 	=  $this->db->query($sql,
			array($data)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function delete_pospb_by_po($data) {
		$sql 	= 'CALL pospb_delete_by_po(?)';
		$query 	=  $this->db->query($sql,
			array($data)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function check_approve_po_spb($id_po) {
		$sql	= 'SELECT COUNT(*) AS total FROM t_po_spb WHERE id_po=? AND status = \'0\'';
		$query 	= $this->db->query($sql,
			array($id_po)
		);
		$return = $query->row_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function check_approve_po_spb_accepted($id_po) {
		$sql	= 'SELECT COUNT(*) AS total FROM t_po_spb WHERE id_po=? AND status = \'1\'';
		$query 	= $this->db->query($sql,
			array($id_po)
		);
		$return = $query->row_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function check_approve_po_spb_rejected($id_po) {
		$sql	= 'SELECT COUNT(*) AS total FROM t_po_spb WHERE id_po=? AND status = \'2\'';
		$query 	= $this->db->query($sql,
			array($id_po)
		);
		$return = $query->row_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_material($id_po_spb) {
		$sql 	= 'SELECT b.* FROM t_po_spb a JOIN t_spb b ON a.id_spb = b.id WHERE a.id = ?';
		$query 	=  $this->db->query($sql,
			array($id_po_spb)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_material_approve($id_po_spb) {
		$sql 	= 'SELECT b.* FROM t_po_spb a JOIN t_spb b ON a.id_spb = b.id WHERE a.id_spb = ?';
		$query 	=  $this->db->query($sql,
			array($id_po_spb)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function approval_po($data) {
		$user_id = $this->session->userdata['logged_in']['user_id'];
		if($data['stat_approve'] == 1) {
			$sql 	= 'UPDATE t_purchase_order SET status=? WHERE id_po=?;';

			$result 	= $this->db->query($sql, array(
				$data['status'],
				$data['id_po']
			));
		}else if($data['stat_approve'] == 2) {
			$sql 	= 'UPDATE t_po_spb SET status=?, approval_date=?, notes=? WHERE id_spb=?;';

			$result 	= $this->db->query($sql, array(
				$data['status'],
				$data['approval_date'],
				$data['notes'],
				$data['id_po_spb']
			));
		}

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_status_material($data) {
		$user_id = $this->session->userdata['logged_in']['user_id'];
		$sql 	= 'UPDATE m_material SET status=? WHERE id=?;';
		$result 	= $this->db->query($sql, array(
			$data['status'],
			$data['id_material']
		));

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}