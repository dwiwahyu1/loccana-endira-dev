    <style>
		#loading-us{display:none}
		#tick{display:none}

		#loading-mail{display:none}
		#cross{display:none}
	</style>

    <form class="form-horizontal form-label-left" id="edit_contractor" role="form" action="<?php echo base_url('contractor/edit_contractor');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

      <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

      <div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="kode_kontraktor">Kode Kontraktor <span class="required"><sup>*</sup></span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
          <input data-parsley-maxlength="255" type="text" id="kode_kontraktor" name="kode_kontraktor" class="form-control col-md-7 col-xs-12" placeholder="Kode Kontraktor" readonly required="required" value="<?php if(isset($detail[0]['kode_eksternal'])){ echo $detail[0]['kode_eksternal']; }?>">
		</div>
      </div>
	  
	  <div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama <span class="required"><sup>*</sup></span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
          <input data-parsley-maxlength="255" type="text" id="cont_name" name="cont_name" class="form-control col-md-7 col-xs-12" placeholder="Nama" required="required" value="<?php if(isset($detail[0]['name_eksternal'])){ echo $detail[0]['name_eksternal']; }?>">
		</div>
      </div>
	  
	  <div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Alamat <span class="required"><sup>*</sup></span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
          <textarea data-parsley-maxlength="255" type="text" id="cont_address" name="cont_address" class="form-control col-md-7 col-xs-12" placeholder="Alamat" required="required"><?php if(isset($detail[0]['eksternal_address'])){ echo $detail[0]['eksternal_address']; }?></textarea>
		</div>
      </div>
	  
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="ship_address">Ship Address </label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<textarea data-parsley-maxlength="255" type="text" id="ship_address" name="ship_address" class="form-control col-md-7 col-xs-12" placeholder="Ship Address"><?php if(isset($detail[0]['ship_address'])){ echo $detail[0]['ship_address']; }?></textarea>
			</div>
		</div>
		
      <div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Phone 1 <span class="required"><sup>*</sup></span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
          <input data-parsley-maxlength="255" type="text" id="phone_1" name="phone_1" class="form-control col-md-7 col-xs-12" placeholder="Phone 1" required="required" value="<?php if(isset($detail[0]['phone_1'])){ echo $detail[0]['phone_1']; }?>">
		</div>
      </div>

      <div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Phone 2</label>
        <div class="col-md-8 col-sm-6 col-xs-12">
          <input data-parsley-maxlength="255" type="text" id="phone_2" name="phone_2" class="form-control col-md-7 col-xs-12" placeholder="Phone 2" value="<?php if(isset($detail[0]['phone_2'])){ echo $detail[0]['phone_2']; }?>">
		</div>
      </div>
      
      <div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Fax</label>
        <div class="col-md-8 col-sm-6 col-xs-12">
          <input data-parsley-maxlength="255" type="text" id="fax" name="fax" class="form-control col-md-7 col-xs-12" placeholder="Fax"  value="<?php if(isset($detail[0]['fax'])){ echo $detail[0]['fax']; }?>">
		</div>
      </div>

      <div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Email<span class="required"><sup>*</sup></span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
          <input data-parsley-maxlength="255" type="email" id="email" name="email" class="form-control col-md-7 col-xs-12" placeholder="Email" required="required" value="<?php if(isset($detail[0]['email'])){ echo $detail[0]['email']; }?>" readonly>
		</div>
      </div>

      <div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">PIC <span class="required"><sup>*</sup></span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
          <input data-parsley-maxlength="255" type="text" id="pic" name="pic" class="form-control col-md-7 col-xs-12" placeholder="PIC" required="required" value="<?php if(isset($detail[0]['pic'])){ echo $detail[0]['pic']; }?>">
		</div>
      </div>

      <div class="item form-group form-item">
	    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Lokasi 
	      <span class="required">
	        <sup>*</sup>
	      </span>
	    </label>
	    <div class="col-md-8 col-sm-6 col-xs-12">
	      <select class="form-control" name="location" id="location" style="width: 100%" required>
	        <?php foreach($location as $key) { ?>
	          <option value="<?php echo $key['id']; ?>" <?php if(isset($detail[0]['eksternal_loc'])){ if( $detail[0]['eksternal_loc'] == $key['id'] ){ echo "selected"; } }?>><?php echo $key['name_eksternal_loc']; ?></option>
	        <?php } ?>
	      </select>
	    </div>
	  </div>

      <div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
        <div class="col-md-8 col-sm-6 col-xs-12">
          <input data-parsley-maxlength="255" type="hidden" id="id" name="id" class="form-control col-md-7 col-xs-12" placeholder="Distributor Id" required="required"  value="<?php if(isset($detail[0]['id'])){ echo $detail[0]['id']; }?>" readonly>
          <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Sub Kontraktor</button>
        </div>
      </div>

    </form><!-- /page content -->

<script type="text/javascript">
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
	});

	$('#edit_contractor').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Mengubah data...");
		e.preventDefault();
		var formData = new FormData(this);

		$.ajax({
	        type:'POST',
	        url: $(this).attr('action'),
	        data:formData,
	        cache:false,
	        contentType: false,
	        processData: false,
	        success: function(response) {
	            if (response.success == true) {
	            	$('.panel-heading button').trigger('click');
	                listcont();
	                swal({
	                  title: 'Success!',
	                  text: response.message,
	                  type: 'success',
	                  showCancelButton: false,
	                  confirmButtonText: 'Ok'
	                }).then(function () {
	                });
	            } else{
	                $('#btn-submit').removeAttr('disabled');
	                $('#btn-submit').text("Edit Sub Kontraktor");
	                swal("Failed!", response.message, "error");
	            }
	        }
	    }).fail(function(xhr, status, message) {
	        $('#btn-submit').removeAttr('disabled');
	        $('#btn-submit').text("Edit Sub Kontraktor");
	        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
	    });
	}));
</script>
