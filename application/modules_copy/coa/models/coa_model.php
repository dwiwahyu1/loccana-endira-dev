<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Coa_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	/**
      * This function is get the list data in coa table
      * @param : $params is where condition for select query
      */

	public function lists($params = array())
	{
		$sql_all 	= 'CALL coa_list_all(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				''
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL coa_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total_row,
			'total' => $total_row,
		);

		return $return;
	}

	/**
      * This function is get the list data in coa table
      * @param : $params is where condition for select query
      */
	public function coa($search,$id)
	{
		$this->db->select('id_parent,coa,keterangan');
		$this->db->from('t_coa');
		$this->db->where($search, $id);

		$query 	= $this->db->get();

		if($query->num_rows()){
			$return = $query->result_array();	
		}else{
			$return = array();
		}

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is get the list data in eksternal table
      * @type : $type is where condition for select query
      */
	public function eksternal()
	{
		$this->db->select('id,name_eksternal');
		$this->db->from('t_eksternal');

		$query 	= $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is used to delete Record in coa table
      * @param : $data - record array 
      */
	public function delete_coa($data)
	{
		$sql 	= 'CALL coa_delete(?)';

		$this->db->query($sql,
			array(
				$data['id']
			));

		$this->db->close();
		$this->db->initialize();
	}

	/**
      * This function is get data in coa table by id
      * @param : $id is where condition for select query
      */

	public function detail($id)
	{
		$sql 	= 'CALL coa_search_id(?)';

		$query 	= $this->db->query($sql,array(
				$id
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is used to delete Record in coa table
      * @param : $data - record array 
      */
	public function add_coa($data)
	{
		$id_eksternal = ($data['id_eksternal']=='0')?NULL:$data['id_eksternal'];
		$id_parent = intval($data['id_parent']);

		$sql 	= 'CALL coa_add(?,?,?,?)';

		$this->db->query($sql,
			array(
				$id_parent,
				$data['keterangan'],
				NULL,
				$id_eksternal
			));

		$this->db->close();
		$this->db->initialize();
	}

	/**
      * This function is used to edit Record in coa table
      * @param : $data - record array 
      */
	public function edit_coa($data)
	{
		$id_eksternal = ($data['id_eksternal']=='0')?NULL:$data['id_eksternal'];
		$id_parent = intval($data['id_parent']);
		
		$sql 	= 'CALL coa_update(?,?,?,?,?)';

		$this->db->query($sql,
			array(
				$data['id_coa'],
				$id_parent,
				$data['keterangan'],
				NULL,
				$id_eksternal
			));

		$this->db->close();
		$this->db->initialize();
	}
}