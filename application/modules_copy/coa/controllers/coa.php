<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Coa extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('coa/coa_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	* This function is redirect to index coa page
	* @return Void
	*/
	public function index() {
		$this->template->load('maintemplate', 'coa/views/index');
	}

	/**
	* This function is used for showing coa list
	* @return Array
	*/
	function lists() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;
		$order_fields = array('', 'id_coa', 'keterangan');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->coa_model->lists($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$actions = '<div class="btn-group">';
			$actions .= '   <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edit_coa(\'' . $v['id_coa'] . ','.$v['id_parent'].'\')">';
			$actions .= '       <i class="fa fa-edit"></i>';
			$actions .= '   </button>';
			$actions .= '</div>';
			$actions .= '<div class="btn-group">';
			$actions .= '   <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="delete_coa(\'' . $v['id_coa'] . '\')">';
			$actions .= '       <i class="fa fa-trash"></i>';
			$actions .= '   </button>';
			$actions .= '</div>';

			array_push($data, array(
				$i,
				$v['coa'],
				$v['keterangan'],
				$v['eksternal'],
				$actions
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	/**
	* This function is redirect to add coa page
	* @return Void
	*/
  public function add() {
	  $coa = $this->coa_model->coa("id_parent",0);

	  $eksternal = $this->coa_model->eksternal();

	  $data = array(
		  'coa' => $coa,
		  'eksternal' => $eksternal
	  );

	  $this->load->view('add_modal_view',$data);
  }

  /**
	* This function is used to delete coa data
	* @return Array
	*/
  public function delete_coa() {
	  $data   = file_get_contents("php://input");
	  $params = json_decode($data,true);

	  $this->coa_model->delete_coa($params);

	  $msg = 'Berhasil menghapus coa.';

	  $result = array(
		   'success' => true,
		   'message' => $msg
	  );

	  $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  /**
	* This function is used to change coa list
	* @return Array
	*/
  public function change_coa() {
	  $id = $this->input->get('id');
	  $result = $this->coa_model->coa("id_parent",$id);

	  $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  /**
	* This function is redirect to edit coa page
	* @return Void
	*/
  public function edit($id) {
	  $detail = $this->coa_model->detail($id);
	  
	  if($detail[0]['id_parent'] == '0'){
		  $level1_id = $detail[0]['coa'];
		  $level2    = array();
		  $level2_id = '';
		  $level3    = array();  
		  $level3_id = '';
		  $level4_id = '';
	  }else{
		  $level3 = $this->coa_model->coa("coa",$detail[0]['id_parent']);
		  $level2 = $this->coa_model->coa("coa",$level3[0]['id_parent']);

		  if(count($level2)){

			  $level1 = $this->coa_model->coa("coa",$level2[0]['id_parent']);

			  if(count($level1) == 0){
				  $level1_id = $level2[0]['coa'];

				  $level2_id = $level3[0]['coa'];
				  $level2 = $this->coa_model->coa("id_parent",$level3[0]['id_parent']);
				  
				  $level3 = array();
				  $level3_id = $level2[0]['coa'];
				  $level4_id = '';

			  }else{
				  $level1_id = $level2[0]['id_parent'];
				  
				  $level2_id = $level2[0]['coa'];
				  $level2 = $this->coa_model->coa("id_parent",$level2[0]['id_parent']);
				  
				  $level3 = $this->coa_model->coa("id_parent",$level2[0]['coa']);
				  $level3_id = $level3[0]['coa'];

				  $level4_id = $detail[0]['coa'];
			  }
			  
		  }else{
			  $level1_id = $detail[0]['id_parent'];
			  $level2_id = $detail[0]['coa'];
			  $level3    = array();
			  $level3_id = '';
			  $level4_id = '';
		  }
	  }

	  $level1 = $this->coa_model->coa("id_parent",0);

	  $eksternal = $this->coa_model->eksternal();


	  $data = array(
		  'level1'    => $level1,
		  'level1_id' => $level1_id,
		  'level2'    => $level2,
		  'level2_id' => $level2_id,
		  'level3'    => $level3,
		  'level3_id' => $level3_id,
		  'level4_id' => $level4_id,
		  'eksternal' => $eksternal,
		  'detail'    => $detail
	  );

	  $this->load->view('edit_modal_view',$data);
  }

  /**
	* This function is used to add coa data
	* @return Array
	*/
  public function add_coa() {
	  $data   = file_get_contents("php://input");
	  $params = json_decode($data,true);

	  $this->coa_model->add_coa($params);

	  $msg = 'Berhasil menambah coa.';

	  $result = array(
		   'success' => true,
		   'message' => $msg
	  );

	  $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  /**
	* This function is used to edit coa data
	* @return Array
	*/
  public function edit_coa() {
	  $data   = file_get_contents("php://input");
	  $params = json_decode($data,true);

	  $this->coa_model->edit_coa($params);

	  $msg = 'Berhasil mengubah coa.';

	  $result = array(
		   'success' => true,
		   'message' => $msg
	  );

	  $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }
}