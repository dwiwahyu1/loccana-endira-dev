<style>
	#loading{
		position: absolute;
	    background: #FFFFFF;
	    opacity: 0.5;
	    width: 93%;
	    height: 80%;
	    z-index: 3;
	    text-align: center;
		display:none;
	}
</style>
<div id="loading"><img src="<?php echo base_url();?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/preloader.gif"/></div>
<form class="form-horizontal form-label-left" id="add_coa" role="form" action="<?php echo base_url('coa/add_coa');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id">Coa <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control select-control coa-select" id="level1" name="level1" style="width: 100%" required>
				<option value="0">-- Tambah COA --</option>
				<?php foreach($coa as $key) { ?>
					<option value="<?php echo $key['coa']; ?>"><?php echo $key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group" id="level2_temp" style="display:none">
		<div class="col-md-8 col-sm-6 col-xs-12" style="margin-left: 145px;">
			<select class="form-control select-control coa-select" id="level2" name="level2" style="width: 100%">
				<option value="0">-- Tambah COA --</option>
			</select>
		</div>
	</div>

	<div class="item form-group" id="level3_temp" style="display:none">
		<div class="col-md-8 col-sm-6 col-xs-12" style="margin-left: 145px;">
			<select class="form-control coa-select" id="level3" name="level3" style="width: 100%">
				<option value="0">-- Tambah COA --</option>
			</select>
		</div>
	</div>

	<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Keterangan
        	<span class="required">
        		<sup>*</sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
		      <textarea data-parsley-maxlength="255" type="text" id="keterangan" name="keterangan" class="form-control col-md-7 col-xs-12" placeholder="Keterangan" required="required"></textarea>
	     </div>
   	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id">Eksternal</span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="id_eksternal" name="id_eksternal" style="width: 100%">
				<option value="0">-- Pilih Eksternal --</option>
				<?php foreach($eksternal as $key) { ?>
					<option value="<?php echo $key['id']; ?>" ><?php echo $key['name_eksternal']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah COA</button>
		</div>
	</div>

</form>
<!-- /page content -->

<script type="text/javascript">
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
		$('#id_eksternal').select2();
	});

	$('.select-control').on('change',(function(e) {
		var id = $(this).val(),
			level = $(this).attr('id');
			
			if(id === '0' && level === 'level1'){
				$('#level2').html('<option value="0">-- Tambah COA --</option>');
				$('#level2_temp').hide();
				$('#level3').html('<option value="0">-- Tambah COA --</option>');
				$('#level3_temp').hide();
			}else if(id === '0' && level === 'level2'){
				$('#level3').html('<option value="0">-- Tambah COA --</option>');
				$('#level3_temp').hide();
			}
			if(id !== '0')coa(level,id);
	}));

	function coa(level,id){
		$('#loading').show();
		$.ajax({
	        type:'GET',
	        url: "<?php echo base_url();?>coa/change_coa",
	        data : {id:id},
	        success: function(response) {
	        	var element = '<option value="0">-- Tambah COA --</option>';
	        	if(response.length){
		            $.each( response, function( key, val ) {
		            	element += '<option value="'+val.coa+'">'+val.keterangan+'</option>';
		            });
		            
		            if(level === 'level1'){
		            	$('#level2').html(element);
		            	$('#level2_temp').show();
		            }else{
		            	$('#level3').html(element);
		            	$('#level3_temp').show();
		            }
	        	}else{
	        		if(level === 'level1'){
	        			$('#level2').html(element);
		            	$('#level2_temp').hide();
		            	$('#level3').html(element);
						$('#level3_temp').hide();
		            }else{
		            	$('#level3').html(element);
		            	$('#level3_temp').hide();
		            }
	        	}
	            $('#loading').hide();
	        }
	    });
	}

	$('#add_coa').on('submit',(function(e) {
		var keterangan   	 = $('#keterangan').val(),
			id_eksternal 	 = $('#id_eksternal').val(),
			level1 		 	 = $('#level1').val(),
			level2 		 	 = $('#level2').val(),
			level3 		 	 = $('#level3').val();

			var id_parent = '';
			if(level1 === '0' || level2 === '0'){
				id_parent = level1;
			}else if(level3 === '0'){
				id_parent = level2;
			}else{
				id_parent = level3;
			}

		var datapost={
				"id_parent"     : id_parent,
				"keterangan"    : keterangan,
				"id_eksternal"  : id_eksternal
			};

	    $('#btn-submit').attr('disabled','disabled');
	    $('#btn-submit').text("Memasukkan data...");
	    e.preventDefault();

	    $.ajax({
	        type:'POST',
	        url: $(this).attr('action'),
	        data : JSON.stringify(datapost),
	        dataType: 'json',
			contentType: 'application/json; charset=utf-8',
	        success: function(response) {
	            if (response.success == true) {
	              $('.panel-heading button').trigger('click');
	                listcoa();
	                swal({
	                  title: 'Success!',
	                  text: response.message,
	                  type: 'success',
	                  showCancelButton: false,
	                  confirmButtonText: 'Ok'
	                }).then(function () {
	                });
	            } else{
	                $('#btn-submit').removeAttr('disabled');
	                $('#btn-submit').text("Tambah COA");
	                swal("Failed!", response.message, "error");
	            }
	        }
	    }).fail(function(xhr, status, message) {
	        $('#btn-submit').removeAttr('disabled');
	        $('#btn-submit').text("Tambah COA");
	        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
	    });
	  }));
</script>
