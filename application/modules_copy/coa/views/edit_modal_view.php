<style>
	#loading{
		position: absolute;
	    background: #FFFFFF;
	    opacity: 0.5;
	    width: 93%;
	    height: 80%;
	    z-index: 3;
	    text-align: center;
		display:none;
	}
</style>
<div id="loading"><img src="<?php echo base_url();?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/preloader.gif"/></div>
<form class="form-horizontal form-label-left" id="edit_coa" role="form" action="<?php echo base_url('coa/edit_coa');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group" id="level1_temp" <?php if(isset($level2_id)){ if( $level2_id == ''){ echo 'style="display:none"'; } }?>>
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id">Coa <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control select-control" id="level1" name="level1" style="width: 100%" required>
				<?php foreach($level1 as $key) { ?>
					<option value="<?php echo $key['coa']; ?>" <?php if(isset($level1_id)){ if( $level1_id == $key['coa']){ echo "selected"; } }?>><?php echo $key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
		</div>
	<div class="item form-group" id="level2_temp" <?php if(isset($level3)){ if( $level3_id == ''){ echo 'style="display:none"'; } }?>>
		<div class="col-md-8 col-sm-6 col-xs-12" style="margin-left: 145px;">
			<select class="form-control select-control" id="level2" name="level2" style="width: 100%">
				<?php foreach($level2 as $key) { ?>
					<option value="<?php echo $key['coa']; ?>" <?php if(isset($level2_id)){ if( $level2_id == $key['coa']){ echo "selected"; } }?>><?php echo $key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group" id="level3_temp" <?php if(isset($level4_id)){ if( $level4_id == ''){ echo 'style="display:none"'; } }?>>
		<div class="col-md-8 col-sm-6 col-xs-12" style="margin-left: 145px;">
			<select class="form-control" id="level3" name="level3" style="width: 100%">
				<?php foreach($level3 as $key) { ?>
					<option value="<?php echo $key['coa']; ?>" <?php if(isset($level3_id)){ if( $level3_id == $key['coa']){ echo "selected"; } }?>><?php echo $key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Keterangan
        	<span class="required">
        		<sup>*</sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
		      <textarea data-parsley-maxlength="255" type="text" id="keterangan" name="keterangan" class="form-control col-md-7 col-xs-12" placeholder="Keterangan" required="required"><?php if(isset($detail[0]['keterangan'])){ echo $detail[0]['keterangan']; }?></textarea>
	     </div>
   	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id">Eksternal</span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="id_eksternal" name="id_eksternal" style="width: 100%">
				<option value="0">-- Pilih Eksternal --</option>
				<?php foreach($eksternal as $key) { ?>
					<option value="<?php echo $key['id']; ?>" <?php if(isset($detail[0]['id_eksternal'])){ if( $detail[0]['id_eksternal'] == $key['id']){ echo "selected"; } }?>><?php echo $key['name_eksternal']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Ubah COA</button>
		</div>
	</div>

	<input type="hidden" id="id_coa" value="<?php if(isset($detail[0]['id_coa'])){ echo $detail[0]['id_coa']; }?>">
</form>
<!-- /page content -->

<script type="text/javascript">
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();

		$('.select-control').on('change',(function(e) {
			var id = $(this).val(),
				level = $(this).attr('id');
				
				coa(level,id);
		}));
		$('#id_eksternal').select2();
	});

	function coa(level,id){
		$('#loading').show();
		$.ajax({
	        type:'GET',
	        url: "<?php echo base_url();?>coa/change_coa",
	        data : {id:id},
	        success: function(response) {
	        	if(response.length){
	        		var element = '';
		            $.each( response, function( key, val ) {
		            	element += '<option value="'+val.coa+'">'+val.keterangan+'</option>';
		            });
		            
		            if(level === 'level1'){
		            	$('#level2').html(element);
		            	$('#level2_temp').show();

		            	coa('level2',response[0].coa);
		            }else{
		            	$('#level3').html(element);
		            	$('#level3_temp').show();
		            	$('#loading').hide();
		            }
	        	}else{
	        		if(level === 'level1'){
		            	$('#level2_temp').hide();
						$('#level3_temp').hide();
		            }else{
		            	$('#level3_temp').hide();
		            }
		            $('#loading').hide();
	        	}
	        }
	    });
	}

	$('#edit_coa').on('submit',(function(e) {
		var id_coa 	 		 = $('#id_coa').val(),
			keterangan   	 = $('#keterangan').val(),
			id_eksternal 	 = $('#id_eksternal').val(),
			level1 		 	 = $('#level1').val(),
			level2 		 	 = $('#level2').val(),
			level3 		 	 = $('#level3').val();

			var id_parent = '';
			if($('#level3_temp').is(":visible")){
				id_parent = level3;
			}else if($('#level2_temp').is(":visible")){
				id_parent = level2;
			}else if($('#level1_temp').is(":visible")){
				id_parent = level1;
			}else{
				id_parent = '0';
			}

		var datapost={
				"id_coa"     	: id_coa,
				"id_parent"     : id_parent,
				"keterangan"    : keterangan,
				"id_eksternal"  : id_eksternal
			};
			
	    $('#btn-submit').attr('disabled','disabled');
	    $('#btn-submit').text("Mengubah data...");
	    e.preventDefault();

	    $.ajax({
	        type:'POST',
	        url: $(this).attr('action'),
	        data : JSON.stringify(datapost),
	        dataType: 'json',
			contentType: 'application/json; charset=utf-8',
	        success: function(response) {
	            if (response.success == true) {
	              $('.panel-heading button').trigger('click');
	                listcoa();
	                swal({
	                  title: 'Success!',
	                  text: response.message,
	                  type: 'success',
	                  showCancelButton: false,
	                  confirmButtonText: 'Ok'
	                }).then(function () {
	                });
	            } else{
	                $('#btn-submit').removeAttr('disabled');
	                $('#btn-submit').text("Ubah COA");
	                swal("Failed!", response.message, "error");
	            }
	        }
	    }).fail(function(xhr, status, message) {
	        $('#btn-submit').removeAttr('disabled');
	        $('#btn-submit').text("Ubah COA");
	        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
	    });
	  }));
</script>
