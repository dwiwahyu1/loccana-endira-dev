<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">COA</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<table id="listcoa" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="width: 5px;">No</th>
							<th>COA</th>
							<th>Keterangan</th>
							<th>Eksternal</th>
							<th style="width: 150px;">Option</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="width: 5px;">No</td>
							<th>COA</th>
							<th>Keterangan</th>
							<th>Eksternal</th>
							<td style="width: 150px;">Option</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$(document).ready(function(){
		listcoa();
	});

	function listcoa(){
      $("#listcoa").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'coa/lists/';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false,
			"destroy": true,
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip',
			"initComplete": function(){
				var element = '<div class="btn-group pull-left">';
					element += '	<a class="btn btn-primary" onClick="add_coa()">';
					element += '		<i class="fa fa-plus"></i> Add COA';
					element += '	</a>';
					element += '</div>';

				$("div.toolbar").prepend(element);
			}
		});
    }

	function add_coa(){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('coa/add');?>');
		$('#panel-modal  .panel-title').html('<i class="fa fa-plus"></i> Add COA');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function edit_coa(id){
		var idarray = id.split(",");

		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('coa/edit/');?>'+"/"+idarray[0]);
		$('#panel-modal  .panel-title').html('<i class="fa fa-edit"></i> Edit COA');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}
		
	function delete_coa(id){
		swal({
			title: 'Yakin akan Menghapus ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost={
				"id" : id
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>coa/delete_coa",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
				   $('.panel-heading button').trigger('click');
                    listcoa();
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                    })
				}
			});
		})
	}
</script>