<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bom_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	/**
      * This function is get the list data in coa table
      * @param : $params is where condition for select query
      */

	public function lists($params = array())
	{
			$sql 	= 'CALL material_list_finish(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	/**
      * This function is get data in bom table by id
      * @param : $id is where condition for select query
      */

	public function detail_main($id)
	{
		//$sql 	= 'CALL bom_search_id(?)';
		$sql 	= '
			SELECT c.`id_bom`,a.id,a.`stock_code`,a.`stock_name`,a.`stock_description`,a.`cust_id`,b.`name_eksternal`,c.* 
			FROM m_material a 
			LEFT JOIN t_eksternal b ON a.`cust_id` = b.`id`
			LEFT JOIN t_bom_m c ON a.`id` = c.`id_stock`
			WHERE a.`id` = ?
			AND c.`material` = "MAIN"
		';

		$query 	= $this->db->query($sql,array(
				$id
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	
	public function detail_main2($id)
	{
		//$sql 	= 'CALL bom_search_id(?)';
		$sql 	= '
			SELECT c.`id_bom`,a.id,a.`stock_code`,a.`stock_name`,a.`stock_description`,a.`cust_id`,b.`name_eksternal`,c.* 
			FROM m_material a 
			LEFT JOIN t_eksternal b ON a.`cust_id` = b.`id`
			LEFT JOIN t_bom_m c ON a.`id` = c.`id_stock`
			WHERE a.`id` = ?
			AND c.`material` = "MAIN2"
		';

		$query 	= $this->db->query($sql,array(
				$id
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	public function get_bom_list_1st_table($id_stock) {
		$sql 	= 'call bom_list_1st_table('.$id_stock.')';
		// $sql 	= 'SELECT * FROM `t_report_header`';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	public function get_supp($id_stock) {
		$sql 	= 'call get_supp('.$id_stock.')';
		// $sql 	= 'SELECT * FROM `t_report_header`';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	public function detail($id)
	{
		//$sql 	= 'CALL bom_search_id(?)';
		$sql 	= '
			SELECT c.`id_bom`,a.id,a.`stock_code`,a.`stock_name`,a.`stock_description`,a.`cust_id`,b.`name_eksternal`,c.* , 
	variable, (t.`rec_pcb`*variable)/t.`rec_yield` as use_gm
			FROM m_material a
			LEFT join t_esf t on a.`id` = t.`id_produk`
			LEFT JOIN t_eksternal b ON a.`cust_id` = b.`id`
			LEFT JOIN t_bom_m c ON a.`id` = c.`id_stock`
			JOIN (SELECT material,variable,seq from `t_supp` group by `material`) oo on c.`material` = oo.material
			WHERE a.`id` = ?
			ORDER BY oo.seq
		';

		$query 	= $this->db->query($sql,array(
				$id
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is get data in production_schedule table by id
      * @param : $id is where condition for select query
      */

	public function edit_bom($data)
	{
			$split_data = explode("|",$data);
			
			// var_dump($split_data[2],$data);die;
			//if(count($split_data) > 1){

			//if(count($split_data) > 1){
				if($split_data[0] == 'main'){
					if($split_data[2] == 0){
						$sql 	= 'update t_bom_m set id_material_comp = NULL where material = "MAIN" and id_stock = '.$split_data[1].' ';
					}else{
						$sql 	= 'update t_bom_m set id_material_comp = '.$split_data[2].' where material = "MAIN" and id_stock = '.$split_data[1].' ';
					}
					
				}else if($split_data[0] == 'main2'){
					if($split_data[2] == 0){
						$sql 	= 'update t_bom_m set id_material_comp = NULL where material = "MAIN2" and id_stock = '.$split_data[1].' ';
					}else{
						$sql 	= 'update t_bom_m set id_material_comp = '.$split_data[2].' where material = "MAIN2" and id_stock = '.$split_data[1].' ';
					}
				}else{
					if ($split_data[0]=="blank"){
						$bom = 'null';
					}
					else {
						$bom = $split_data[0];
					}
					$sql 	= 'update t_bom_m set id_material_comp = '.$bom.' where id_bom = '.$split_data[1].' ';
				}
				
				// var_dump($sql);
				
				
				
			
			//}else{
				
			//}
				$this->db->query($sql);

					// $this->db->query($sql,array(
						// $data[$j]['bom_no'],
						// $data[$j]['id_po_quot'],
						// $data[$j]['id_produk'],
						// $data[$j]['id_mainproduk'],
						// $data[$j]['qty'],
						// $data[$j]['qty_main']
					// ));
				// }

				$this->db->close();
				$this->db->initialize();
			
			
	}

	/**
      * This function is get the list data in material table
      */
	public function item($type)
	{
		// $this->db->select('id,stock_code,stock_name,stock_description');
		// $this->db->from('m_material');
		// $this->db->where('type', $type);
		// $this->db->where('status', 1);

		// $query 	= $this->db->get();
		
		$sql = '
		SELECT a.*,b.`stock_name`,b.`stock_code` FROM `t_supp` a
		JOIN m_material b ON a.`id_material` = b.`id`
		ORDER BY seq,id_material 
		';
		
		$query 	= $this->db->query($sql);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function mainitem($type)
	{
		// $this->db->select('id,stock_code,stock_name,stock_description');
		// $this->db->from('m_material');
		// $this->db->where('type', $type);
		// $this->db->where('status', 1);

		// $query 	= $this->db->get();
		
		$sql = '
		SELECT a.* FROM m_material a 
		where a.type = 1
		ORDER BY stock_code 
		';
		
		$query 	= $this->db->query($sql);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
}