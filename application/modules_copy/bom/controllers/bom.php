<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Bom extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('bom/bom_model');
		$this->load->model('quotation/quotation_model');
		$this->load->model('material/material_model');
		$this->load->library('log_activity');
		$this->load->library('excel');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string)
	{
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	 * This function is redirect to index bom page
	 * @return Void
	 */
	public function index()
	{
		$this->template->load('maintemplate', 'bom/views/index');
	}

	/**
	 * This function is used for showing bom list
	 * @return Array
	 */
	function lists()
	{
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;
		$order_fields = array('', 'date_create', 'stock_code', 'stock_name');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);
		$cust_id = $this->input->get_post('cust_id');

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;
		//$params['cust_id'] = $cust_id;

		$list = $this->bom_model->lists($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["customer"] = $this->quotation_model->customer();
		$result["draw"] = $draw;

		$data = array();
		$i = $start;
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$actions = '<div class="btn-group">';
			$actions .= '   <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edit_bom(\'' . $v['id'] . '\')">';
			$actions .= '       <i class="fa fa-edit"></i>';
			$actions .= '   </button>';
			$actions .= '</div>' ;
			$actions .= '<div class="btn-group">';
			$actions .= 	'<button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Download" onClick="download_bom(\'' . $v['id'] . '\')">' .'<i class="fa fa-file-excel-o"></i>';
			$actions .= 	'</button>';
			$actions .= '</div>';

			array_push($data, array(
				$i,
				$v['stock_code'],
				$v['name_eksternal'],
				$v['stock_name'],
				$actions
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	/**
	 * This function is redirect to edit bom page
	 * @return Void
	 */
	public function edit($id)
	{
		$item = $this->bom_model->item(3);
		$mainitem = $this->bom_model->mainitem(1);
		$detail = $this->bom_model->detail($id);
		$detail_main = $this->bom_model->detail_main($id);
		$detail_main2 = $this->bom_model->detail_main2($id);


		$data = array(
			'item' => $item,
			'mainitem' => $mainitem,
			'detail' => $detail,
			'detail_main' => $detail_main,
			'detail_main2' => $detail_main2
		);

		// echo "<pre>";print_r($detail);print_r($item);echo "</pre>";die;
		$this->load->view('edit_modal_view', $data);
	}

	/**
	 * This function is redirect to edit bom page
	 * @return Void
	 */
	public function detail_item($id)
	{
		$result = $this->material_model->edit($id);

		$data = array(
			'uom' => $result,
		);

		$this->load->view('edit_modal_material_view', $data);
	}

	/**
	 * This function is used to edit bom data
	 * @return Array
	 */
	public function edit_bom()
	{
		$data   = file_get_contents("php://input");
		$params = json_decode($data, true);

		// echo"<pre>";print_r($params[0]);echo"</pre>";die;

		foreach ($params[0] as $paramss) {
			$schedule = $this->bom_model->edit_bom($paramss);
		}

		$msg = 'Berhasil mengubah bom';

		$result = array(
			'success' => true,
			'message' => $msg
		);

		$this->log_activity->insert_activity('update', $msg);

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	public function download_bom($id_bom)
	{
		$detail_bom = $this->bom_model->detail($id_bom)[0];
		// $data_bc 		= $this->bc_purchase_order_model->edit_bc($id_bc)[0];
		$exportList		= array();
		$data_key 		= array();
		$exportList = array('Sheet-1');


		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("System")
			->setLastModifiedBy("System")
			->setTitle($detail_bom['stock_name'])
			->setSubject("Laporan BOM")
			->setDescription("")
			->setKeywords("BOM")
			->setCategory("Report");
		$data_key 	= array();
		// $no_aju 	= preg_replace('/\D/', '', $data_bc['no_pengajuan']);
		// var_dump($detail_bom['id_stock']);die;
		$data_bom = $this->bom_model->get_bom_list_1st_table($detail_bom['id_stock']);
		// if (sizeof($data_bom) > 0) {
		// 	for ($i = 0; $i < sizeof($data_bom); $i++) //unset($data[$i]['id_report_header']);
		// 		foreach ($data_bom[0] as $key => $value) array_push($data_key, $key);
		// }
		$objPHPExcel->setActiveSheetIndex(0)->setTitle("ENG LAYOUT & BOM");

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:D3');
		$objPHPExcel->getActiveSheet()->setCellValue('A2', "ENGINEERING  LAY OUT Aktif UNTUK INPUT BC");
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B4:C4');
		$objPHPExcel->getActiveSheet()->setCellValue('B4', "3/1/2019");
		$objPHPExcel->getActiveSheet()->setCellValue('AJ3', "BILL  OF  MATERIAL ");
		$objPHPExcel->getActiveSheet()->setCellValue('AI5', "Periode  : 28 Maret  2013");
		$objPHPExcel->getActiveSheet()->setCellValue('AI6', "MAIN  MATERIAL USE ( grams/M2. )");
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AI6:AR6');
		$objPHPExcel->getActiveSheet()->setCellValue('AS6', "SUPPORT MATERIAL  USE ( Gram/M2 )");
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('AS6:BJ6');
		$objPHPExcel->getActiveSheet()->setCellValue('CB6', "KOMPOSISI  GRAM / M2");
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('CB6:CK6');
		$objPHPExcel->getActiveSheet()->setCellValue('CM6', "KOMPOSISI  KILOGRAM / M2");
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('CM6:CV6');
		$objPHPExcel->getActiveSheet()->setCellValue('CX6', "KOMPOSISI MM/M2");
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('CX6:CY6');
		$objPHPExcel->getActiveSheet()->setCellValue('CX4', "CODE MATERIAL TAMBAHAN");
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('CX4:DG4');
		$objPHPExcel->getActiveSheet()->setCellValue('BH4', "Engineering Dept.");
		$objPHPExcel->getActiveSheet()->setCellValue('BH5', "Draft Sugito");
		$objPHPExcel->getActiveSheet()->setCellValue('Z5', "0.975");
		$objPHPExcel->getActiveSheet()->setCellValue('AI5', "Periode : 28 Maret  2013");

		//set merge for header cells
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A6:A7');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B6:B7');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C6:C7');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('D6:D7');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('E6:G7');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J6:L7');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('o6:q7');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('aa6:aa7');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('aj7:ak7');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('am7:an7');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('as7:at7');
		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('bi7:bj7');
		$datakey1_top = array(
			" KODE BC ",
			"	CUSTOMER	",
			"	PART NO.	",
			"	UNIT	",
			"	ARY SIZE	",
			"		",
			"		",
			"	M2 / 	",
			"	ARY/	",
			"	PANEL SIZE	",
			"		",
			"		",
			"	M2 / 	",
			"	PNL/	",
			"	SHEET SIZE	",
			"		",
			"		",
			"	SHT/	",
			"	THICKNESS	",
			"	MTR'L	",
			"	MTR'L	",
			"	SHEET/	",
			"		",
			"		",
			"	M2	",
			"		",
			"	YIELD	",
			"	GROSS	",
			"	GROSS	",
			"	GROSS	",
			"	CCL	",
			"	WEIGHT	",
			"	REMARK	",
		);
		$datakey1_bottom = array(
			"		",
			"		",
			"		",
			"		",
			"		",
			"		",
			"		",
			"	ARY	",
			"	PCS	",
			"		",
			"		",
			"		",
			"	PNL	",
			"	ARY	",
			"		",
			"		",
			"		",
			"	PNL	",
			"	& OZ	",
			"	TYPE	",
			"	CODE	",
			"	PCS	",
			"		",
			"		",
			"	UNIT	",
			"	Unit	",
			"		",
			"	PANEL	",
			"	ARRAY	",
			"	PCS	",
			"	CODE	",
			"	grm / Arrys.	",
			"		",
		);
		$datakey2_top = array(
			"	TSSPP	",
			"	TSSLG	",
			"		",
			"	TSSSM	",
			"		",
			"	UC2	",
			"	TCUCP	",
			"	CHCL	",
			"	CEHHO	",
			"		",
			"	CPTNR	",
			"	CPTNR	",
			"	CPTBH	",
			"	CEHHO	",
			"		",
			"		",
			"	CPTNR	",
			"	COSP	",
			"	CPTBH	",
			"	COSP	",
			"	CEHHO	",
			"	CEHHO	",
			"	COSP	",
			"	CSRM	",
			"	CSRM	",
			"	CSRM	",
			"	COSP	",
			"	COSP	",
			"		",
			"		",
			"		",
			"		",
			"		",
			"		",
			"		",
			"		",
			"		",
			"		",
			"		",
			"		",
			"		",
			"		",
			"		",
			"		",
			"		",
			"	TSSPP	",
			"	TSSLG	",
			"	TSSSM	",
			"	CHCL	",
			"	CEHHO	",
			"	CPTNR	",
			"	CPTBH	",
			"	COSP	",
			"	CSRM	",
			"	TCUCP	",
			"		",
			"	TSSPP	",
			"	TSSLG	",
			"	TSSSM	",
			"	CHCL	",
			"	CEHHO	",
			"	CPTNR	",
			"	CPTBH	",
			"	COSP	",
			"	CSRM	",
			"	TCUCP	",
			"		",
			"	TSSSQG70	",
			"	TSSSQG85	",
			"	TSSSQG70	",
			"	TSSSQG85	",
			"	TDSCPLT	",
			"	TSSSCR	",
			"	TSSSCR	",
			"	12	",
			"	18	",
			"	TFLM	",
		);
		$datakey2_bottom = array(
			"	PATTERN INK	",
			"	L. COMP. INK	",
			"	L. CIRCUIT INK	",
			"	S/M INK	",
			"	UC1	",
			"	UC2	",
			"	CARBON INK	",
			"	HCL	",
			"	ETCH ENC.	",
			"	GOLD	",
			"	ACETON	",
			"	BUTHYL  CELL	",
			"	CAUSTIC  FLAK	",
			"	HI - CEM  DEFOAMER  DF - 200	",
			"	HARTER  930	",
			"	KIWOBOND	",
			"	IPA 	",
			"	THINER  TULOL  YELOW  GLUE	",
			"	SULPHURIC  ACID  ( H2S04 )	",
			"	SODIUM PERSHULPHATE	",
			"	COUGULANT TAC-01	",
			"	DEFOAMER 	",
			"	ENTEK CLEANER SC 101 DE	",
			"	EMULSION REMOVER KEE-1001	",
			"	DIAZO CP 2 EMULSION	",
			"	MESH RED GLUE HARDENER (NANPAO 104)	",
			"	OSP F8	",
			"	Micro Etch	",

		);
		$datakey3_top = array(
			"	KOMPOSISI MM/M2	",
			"		",
			"	KOMPOSISI ROLL/M2	",
			"		",
			"	KOMPOSISI KG/M2	",
			"	KOMPOSISI MTR/M2	",
			"	KOMPOSISI ROLL/M2	",
			"	KOMPOSISI ROLL/M2	",
			"		",
			"	KOMPOSISI SHT/M2	",
		);
		$objPHPExcel->getActiveSheet()->fromArray($datakey1_top, null, 'A6');
		$objPHPExcel->getActiveSheet()->fromArray($datakey1_bottom, null, 'A7');
		$objPHPExcel->getActiveSheet()->fromArray($datakey2_top, null, 'AI7');
		$objPHPExcel->getActiveSheet()->fromArray($datakey2_bottom, null, 'AI8');
		$objPHPExcel->getActiveSheet()->fromArray($datakey3_top, null, 'CX6');
		$no = 10;
		for ($i = 0; $i < sizeof($data_bom); $i++) {
			$objPHPExcel->getActiveSheet()->fromArray($data_bom[$i], null, 'A' . $no);
			$no++;
		}

		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN
				)
			)
		);
		$objPHPExcel->getDefaultStyle()->applyFromArray($styleArray);
		$objPHPExcel->setActiveSheetIndex(0);
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $detail_bom['category'] . '.xls"');

		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;
	}
}
