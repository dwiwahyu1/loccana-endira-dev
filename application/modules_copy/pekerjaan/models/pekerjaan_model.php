<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Pekerjaan_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function lists($params = array()) {
		$sql 	= 'CALL permintaan_pekerjaan_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query(
			$sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			)
		);

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total']
		);

		return $return;
	}

	public function uom() {
		$sql 	= 'CALL m_uom(?)';

		$out = array();
		$query 	=  $this->db->query($sql, array(
				NULL
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_permintaan($id) {
		$sql 	= 'CALL permintaan_pekerjaan_search_id(?)';

		$query 	= $this->db->query($sql, array(
			$id
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function add_pekerjaan($data) {
		$sql 	= 'CALL permintaan_pekerjaan_add(?,?)';
		$query 	=  $this->db->query($sql, array(
			$data['work_date'],
			$data["pic"]
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];
		
		$result['result'] = $this->db->affected_rows();
		$result['lastid'] = $lastid;

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function add_detail_pekerjaan($data) {
		$sql 	= 'CALL permintaan_pekerjaan_detail_add(?,?,?,?,?,?)';
		$query 	=  $this->db->query($sql, array(
			$data['id_permintaan'],
			$data['no_pekerjaan'],
			$data['nama_pekerjaan'],
			$data['qty'],
			$data["unit"],
			$data['deskripsi']
		));
		
		$result = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_pekerjaan($data) {
		$sql 	= 'CALL permintaan_pekerjaan_update(?,?,?)';
		$query 	=  $this->db->query($sql, array(
			$data['id_permintaan'],
			$data['work_date'],
			$data["pic"]
		));
		
		$result = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_detail_pekerjaan($data) {
		$sql 	= 'CALL permintaan_pekerjaan_detail_update(?,?,?,?,?)';
		$query 	=  $this->db->query($sql, array(
			$data['id_detail'],
			$data['nama_pekerjaan'],
			$data['qty'],
			$data["unit"],
			$data['deskripsi']
		));
		
		$result = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function detail_pekerjaan($id) {
		$sql 	= 'CALL permintaan_pekerjaan_detail_search_id_permintaan(?)';

		$query 	= $this->db->query($sql, array(
			$id
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_detail_pekerjaan($id) {
		$sql 	= 'CALL permintaan_pekerjaan_detail_search_id_detail(?)';

		$query 	= $this->db->query($sql, array(
			$id
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	function delete_pekerjaan($data) {
		$sql 	= 'CALL permintaan_pekerjaan_delete(?)';

		$query 	= $this->db->query($sql, array(
			$data['id']
		));

		$result = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function delete_detail_pekerjaan($data) {
		$sql 	= 'CALL permintaan_pekerjaan_detail_delete(?)';

		$query 	= $this->db->query($sql, array(
			$data['id']
		));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
}
