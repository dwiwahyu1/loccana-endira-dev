<style>
	#listItemPekerjaan {
		counter-reset: rowNumber;
	}

	#listItemPekerjaan tr > td:first-child {
		counter-increment: rowNumber;
	}

	#listItemPekerjaan tr td:first-child::before {
		content: counter(rowNumber);
		min-width: 1em;
		margin-right: 0.5em;
	}
</style>

<form class="form-horizontal form-label-left" id="add_pekerjaan" role="form" action="<?php echo base_url('pekerjaan/add_pekerjaan'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="work_date">Worker Date <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="work_date" name="work_date" value="<?php
				echo date('d-M-Y'); ?>" placeholder="Work Date" required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="pic">PIC</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="pic" name="pic" value="<?php
				echo ucwords($this->session->userdata['logged_in']['nama']); ?>" placeholder="PIC" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="department">Department</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="department" name="department" value="<?php
				echo ucwords($this->session->userdata['logged_in']['name_role']); ?>" placeholder="Department" readonly>
		</div>
	</div>

	<div class="item form-group">
		<table id="listItemPekerjaan" class="table table-striped table-bordered dt-responsive nowrap" width="100%">
			<thead>
				<tr>
					<th style="width: 5%;">No</th>
					<th>Nama Pekerjaan</th>
					<th style="width: 10%;">QTY</th>
					<th style="width: 15%;">UOM</th>
					<th>Deskripsi</th>
					<th style="width: 5%;"><a id="btn_item_add" class="btn btn-primary"><i class="fa fa-plus"></i></a></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td></td>
					<td>
						<input type="text" class="form-control" id="nama_pekerjaan1" name="nama_pekerjaan[]" placeholder="Nama Pekerjaan" autocomplete="off" required>
					</td>
					<td>
						<input type="number" class="form-control" id="qty_pekerjaan1" name="qty_pekerjaan[]" placeholder="Qty" value="1" step=".0001" autocomplete="off" required>
					</td>
					<td>
						<select class="form-control" id="uom_pekerjaan1" name="uom_pekerjaan[]">
							<option value="" selected >-- Pilih UOM --</option>
					<?php
						foreach ($uom as $uk => $uv) { ?>
							<option value="<?php echo $uv['id_uom']; ?>"><?php echo $uv['uom_name']; ?></option>
					<?php
						} ?>
						</select>
					</td>
					<td>
						<textarea class="form-control" id="desc_pekerjaan1" name="desc_pekerjaan[]" placeholder="Deskripsi"></textarea>
					</td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Pekerjaan</button>
		</div>
	</div>
</form>

<script type="text/javascript">
	var idRowItem 		= 1;
	var arrNama 		= [];
	var arrQty			= [];
	var arrUom			= [];
	var arrDesc			= [];

	$(document).ready(function() {
		$("#work_date").datepicker({
			format: 'dd-M-yyyy',
			autoclose: true,
			todayHighlight: true,
		});

		$('#uom_pekerjaan1').select2();
	});

	function add_detail_pekerjaan(){
		$('#panel-modalchild').removeData('bs.modal');
		$('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modalchild  .panel-body').load('<?php echo base_url('pekerjaan/add_detail_pekerjaan');?>');
		$('#panel-modalchild  .panel-title').html('<i class="fa fa-plus"></i> Tambah Pekerjaan');
		$('#panel-modalchild').modal({backdrop:'static',keyboard:false},'show');
	}

	$('#btn_item_add').on('click', function() {
		idRowItem++;
		$('#listItemPekerjaan tbody').append(
			'<tr id="trRowItem'+idRowItem+'">'+
				'<td></td>'+
				'<td>'+
					'<input type="text" class="form-control" id="nama_pekerjaan'+idRowItem+'" name="nama_pekerjaan[]" placeholder="Nama Pekerjaan" autocomplete="off" required>'+
				'</td>'+
				'<td>'+
					'<input type="number" class="form-control" id="qty_pekerjaan'+idRowItem+'" name="qty_pekerjaan[]" placeholder="Qty" value="1" step=".0001" autocomplete="off" required>'+
				'</td>'+
				'<td>'+
					'<select class="form-control" id="uom_pekerjaan'+idRowItem+'" name="uom_pekerjaan[]">'+
						'<option value="" selected >-- Pilih UOM --</option>'+
					'</select>'+
				'</td>'+
				'<td>'+
					'<textarea class="form-control" id="desc_pekerjaan'+idRowItem+'" name="desc_pekerjaan[]" placeholder="Deskripsi"></textarea>'+
				'</td>'+
				'<td>'+
					'<a class="btn btn-danger" onclick="removeRow('+idRowItem+')"><i class="fa fa-minus"></i></a>'+
				'</td>'+
			'</tr>'
		);
		$("#uom_pekerjaan"+idRowItem).select2();
		$('#uom_pekerjaan'+idRowItem).attr('readonly', 'readonly');

		$.ajax({
			type: "GET",
			url: "<?php echo base_url('pekerjaan/get_uom');?>",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					for (var i = 0; i < r.length; i++) {
						var newOption = new Option(r[i].uom_name, r[i].id_uom, false, false);
						$('#uom_pekerjaan'+idRowItem).append(newOption);
					}
					$('#uom_pekerjaan'+idRowItem).removeAttr('readonly');
				}else $('#uom_pekerjaan'+idRowItem).removeAttr('readonly');
			}
		});
	})

	function removeRow(rowItem) {
		$('#trRowItem'+rowItem).remove();
	}

	$('#add_pekerjaan').on('submit', (function(e) {
		arrNama		= [];
		arrQty		= [];
		arrUom		= [];
		arrDesc		= [];

		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();

		$('input[name="nama_pekerjaan[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '')  arrNama.push(this.value);
			}
		})

		$('input[name="qty_pekerjaan[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '')  arrQty.push(this.value);
			}
		})

		$('select[name="uom_pekerjaan[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arrUom.push(this.value);
			}
		})

		$('textarea[name="desc_pekerjaan[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arrDesc.push(this.value);
				else arrDesc.push('NULL');
			}else arrDesc.push('NULL');
		})

		if(arrNama.length > 0 && arrQty.length > 0 && arrUom.length > 0 && arrDesc.length > 0) {
			var formData = new FormData();
			formData.set('work_date',	$('#work_date').val());
			formData.set('arrNama',	arrNama);
			formData.set('arrQty',	arrQty);
			formData.set('arrUom',	arrUom);
			formData.set('arrDesc',	arrDesc);

			$.ajax({
				type:'POST',
				url: $(this).attr('action'),
				data: formData,
				cache:false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function () {
							$('#panel-modal').modal('toggle');
							listpekerjaan();
						})
					}else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Tambah Pekerjaan");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Tambah Pekerjaan");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}else {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah Pekerjaan");
			swal("Failed!", "Invalid Inputan List Pekerjaan, silahkan cek kembali.", "error");
		}
	}));
  </script>