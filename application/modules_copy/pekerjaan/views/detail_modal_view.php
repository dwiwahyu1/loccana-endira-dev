<style>
	#listItemPekerjaan {
		counter-reset: rowNumber;
	}

	#listItemPekerjaan tr > td:first-child {
		counter-increment: rowNumber;
	}

	#listItemPekerjaan tr td:first-child::before {
		content: counter(rowNumber);
		min-width: 1em;
		margin-right: 0.5em;
	}
</style>

<form class="form-horizontal form-label-left" id="add_maintenance" role="form" action="<?php echo base_url('maintenance/edit_maintenance'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="work_date">Worker Date <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="work_date" name="work_date" value="<?php
				if(isset($permintaan[0]['tanggal_pekerjaan'])) echo date('d-M-Y', strtotime($permintaan[0]['tanggal_pekerjaan']));
			?>" placeholder="Work Date" disabled>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="pic">PIC</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="pic" name="pic" value="<?php
				if(isset($permintaan[0]['nama'])) echo ucwords($permintaan[0]['nama']); ?>" placeholder="PIC" disabled>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="department">Department</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="department" name="department" value="<?php
				if(isset($permintaan[0]['group'])) echo ucwords($permintaan[0]['group']);?>" placeholder="Department" disabled>
		</div>
	</div>

	<div class="item form-group">
		<table id="listItemPekerjaan" class="table table-striped table-bordered dt-responsive nowrap" width="100%">
			<thead>
				<tr>
					<th style="width: 5%;">No</th>
					<th style="width: 10%;">No Pekerjaan</th>
					<th>Nama Pekerjaan</th>
					<th style="width: 10%;">QTY</th>
					<th style="width: 15%;">UOM</th>
					<th>Deskripsi</th>
				</tr>
			</thead>
			<tbody>
		<?php
			foreach ($detail as $dk => $dv) { ?>
				<tr>
					<td></td>
					<td><?php echo $dv['no_pekerjaan']; ?></td>
					<td><?php echo $dv['nama_pekerjaan']; ?></td>
					<td><?php echo number_format($dv['qty'], 4, ',', '.'); ?></td>
					<td><?php echo $dv['uom_name']; ?></td>
					<td><?php
						if(($dv['deskripsi'] != '' && $dv['deskripsi'] != 'NULL') && $dv['deskripsi'] != NULL) echo $dv['deskripsi'];
					?></td>
				</tr>
		<?php
			} ?>
			</tbody>
		</table>
	</div>
</form>