<style>
	.changed_status{cursor:pointer;text-decoration: underline;color:#96b6e8;}
	.changed_status:hover{color:#ff8c00}
</style>

<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Permintaan Pekerjaan</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<table id="listpekerjaan" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="width: 5%;">No</th>
							<th>PIC</th>
							<th>Department</th>
							<th>Date</th>
							<th style="width: 5%;">Worklists</th>
							<th style="width: 15%;">Actions</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div><!-- end col -->
	</div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:90%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:70%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild-barang" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:92%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$(document).ready(function(){
		listpekerjaan();
	});

	function add_pekerjaan(){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('pekerjaan/add');?>');
		$('#panel-modal  .panel-title').html('<i class="fa fa-plus"></i> Add Pekerjaan');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	<?php /*
	function edit_pekerjaan(id_permintaan){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('pekerjaan/edit/');?>'+"/"+id_permintaan);
		$('#panel-modal  .panel-title').html('<i class="fa fa-pencil"></i> Edit Pekerjaan');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function detail_pekerjaan(id_permintaan){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('pekerjaan/detail_pekerjaan');?>'+"/"+id_permintaan);
		$('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Detail Pekerjaan');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}
	*/ ?>
	function detail_pekerjaan(id_permintaan){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('pekerjaan/detail_pekerjaan');?>'+"/"+id_permintaan);
		$('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Detail Pekerjaan');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}
	
	function edit_pekerjaan(id_permintaan){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('pekerjaan/edit/');?>'+"/"+id_permintaan);
		$('#panel-modal  .panel-title').html('<i class="fa fa-pencil"></i> Edit Pekerjaan');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function listpekerjaan(){
		$("#listpekerjaan").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'pekerjaan/lists/';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false,
			"destroy": true,
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip',
			"initComplete": function(){
				$("div.toolbar").prepend(
					'<div class="btn-group pull-left">'+
						'<a class="btn btn-primary" onClick="add_pekerjaan()">'+
							'<i class="fa fa-plus"></i> Add Pekerjaan'+
						'</a>'+
					'</div>'
				);
			}
		});
	}

	
	function deletepekerjaan(id_permintaan) {
		swal({
			title: 'Yakin akan Menghapus ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost={
				"id" : id_permintaan
			};

			$.ajax({
				type:'POST',
				url: "<?php echo base_url().'pekerjaan/delete_pekerjaan';?>",
				data:JSON.stringify(datapost),
				cache:false,
				contentType: false,
				processData: false,
				success: function(r) {
					if(r.success == true) {
						swal({
							title: 'Success!',
							text: r.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function () {
							listpekerjaan();
						})
					}else {
						swal("Failed!", r.message, "error");
					}
				}
			});
		});
	}
	
</script>