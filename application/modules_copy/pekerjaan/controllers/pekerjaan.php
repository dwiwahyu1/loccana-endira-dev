<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pekerjaan extends MX_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('pekerjaan/pekerjaan_model');
		$this->load->model('material/material_model');
		$this->load->model('delivery_order/do_model');
		$this->load->library('sequence');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'pekerjaan/views/index');
	}

	function lists() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

		$order_fields = array('a.no_pekerjaan', 'a.tanggal_pekerjaan', 'c.nama', 'e.group');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->pekerjaan_model->lists($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$strStatus = '';
		$no = $start;
		foreach ($list['data'] as $k => $v) {
			if($v['id'] != NULL) {
				$no++;
				$strButton =
					'<div class="btn-group">'.
						'<button class="btn btn-info btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Details" onclick="detail_pekerjaan(\''.$v['id'].'\')">'.
							'<i class="fa fa-search"></i>'.
						'</button>'.
					'</div>';

				if($this->session->userdata['logged_in']['user_id'] == $v['id_pic'] || $this->session->userdata['logged_in']['id_role'] == 99) {
					$strButtonEdit =
						'<div class="btn-group">'.
							'<button class="btn btn-warning btn-sm"title="Edit" onClick="edit_pekerjaan(\''.$v['id'].'\')">'.
								'<i class="fa fa-edit"></i>'.
							'</button>'.
						'</div>';
				}else $strButtonEdit = '';

				if($this->session->userdata['logged_in']['user_id'] == $v['id_pic'] || $this->session->userdata['logged_in']['id_role'] == 99) {
					$strButtonDel =
						'<div class="btn-group">'.
							'<button class="btn btn-danger btn-sm"title="Delete" onClick="deletepekerjaan(\''.$v['id'].'\')">'.
								'<i class="fa fa-trash"></i>'.
							'</button>'.
						'</div>';
				}else $strButtonDel = '';
				$strButton .= $strButtonEdit.$strButtonDel;
				
				array_push($data, array(
					$no,
					ucwords($v['nama']),
					ucwords($v['group']),
					date('d-M-Y', strtotime($v['tanggal_pekerjaan'])),
					$v['worklists'],
					$strButton
				));
			}
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add() {
		$uom 		= $this->pekerjaan_model->uom();

		$data = array(
			'uom'		=> $uom
		);

		$this->load->view('add_modal_view', $data);
	}

	public function get_uom() {
		$result = $this->pekerjaan_model->uom();

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result);
	}

	public function add_pekerjaan() {
		$this->form_validation->set_rules('work_date', 'Work Date', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$tgl_work_date		= $this->Anti_sql_injection($this->input->post('work_date', TRUE));
			$temp_work_date 	= explode("-", $tgl_work_date);
			$pic				= $this->session->userdata['logged_in']['user_id'];

			$tempNama		= $this->Anti_sql_injection($this->input->post('arrNama', TRUE));
			$arrNama		= explode(',', $tempNama);

			$tempQty		= $this->Anti_sql_injection($this->input->post('arrQty', TRUE));
			$arrQty			= explode(',', $tempQty);

			$tempUom		= $this->Anti_sql_injection($this->input->post('arrUom', TRUE));
			$arrUom			= explode(',', $tempUom);

			$tempDesc		= $this->Anti_sql_injection($this->input->post('arrDesc', TRUE));
			$arrDesc		= explode(',', $tempDesc);

			$dataPekerjaan = array(
				'work_date'		=> date('Y-m-d', strtotime($temp_work_date[2].'-'.$temp_work_date[1].'-'.$temp_work_date[0])),
				'pic'			=> $pic
			);

			$resultDetail = false;
			$resultAddPekerjaan = $this->pekerjaan_model->add_pekerjaan($dataPekerjaan);
			if($resultAddPekerjaan['result'] > 0) {
				for ($i = 0; $i < sizeof($arrNama); $i++) {
					$no = $this->sequence->get_no('work_req');

					if($arrDesc[$i] != '' && $arrDesc[$i] != 'NULL') $deskripsi = $arrDesc[$i];
					else $deskripsi = NULL;

					$dataDetail = array(
						'id_permintaan'		=> $resultAddPekerjaan['lastid'],
						'no_pekerjaan'		=> $no,
						'nama_pekerjaan'	=> $arrNama[$i],
						'qty'				=> $arrQty[$i],
						'unit'				=> $arrUom[$i],
						'deskripsi'			=> $deskripsi
					);
					$resultAddDetailPekerjaan = $this->pekerjaan_model->add_detail_pekerjaan($dataDetail);
					if($resultAddDetailPekerjaan > 0) $resultDetail = true;
					else {
						$resultDetail = false;
						$msg = 'Gagal menambahkan Detail Permintaan Pekerjaan No.'($i+1).' ke database';
						$this->log_activity->insert_activity('insert', $msg);
						$result = array('success' => false, 'message' => $msg);
						break;
					}
				}

				if($resultDetail == true) {
					$msg = 'Berhasil menambahkan Permintaan Pekerjaan ke database';
					$this->log_activity->insert_activity('insert', $msg);
					$result = array('success' => true, 'message' => $msg);
				}
			}else {
				$msg = 'Gagal menambahkan Permintaan Pekerjaan ke database';
				$this->log_activity->insert_activity('insert', $msg);
				$result = array('success' => false, 'message' => $msg);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function edit($id_permintaan) {
		$permintaan 	= $this->pekerjaan_model->get_permintaan($id_permintaan);
		$detail 		= $this->pekerjaan_model->detail_pekerjaan($id_permintaan);
		$uom 			= $this->pekerjaan_model->uom();

		$data = array(
			'permintaan'	=> $permintaan,
			'detail'		=> $detail,
			'uom'			=> $uom
		);

		$this->load->view('edit_modal_view', $data);
	}

	public function edit_pekerjaan() {
		$this->form_validation->set_rules('work_date', 'Work Date', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$id_permintaan		= $this->Anti_sql_injection($this->input->post('id_permintaan', TRUE));
			$tgl_work_date		= $this->Anti_sql_injection($this->input->post('work_date', TRUE));
			$temp_work_date 	= explode("-", $tgl_work_date);
			$pic				= $this->session->userdata['logged_in']['user_id'];

			$tempIdItem		= $this->Anti_sql_injection($this->input->post('arrIdItem', TRUE));
			$arrIdItem		= explode(',', $tempIdItem);

			$tempNama		= $this->Anti_sql_injection($this->input->post('arrNama', TRUE));
			$arrNama		= explode(',', $tempNama);

			$tempQty		= $this->Anti_sql_injection($this->input->post('arrQty', TRUE));
			$arrQty			= explode(',', $tempQty);

			$tempUom		= $this->Anti_sql_injection($this->input->post('arrUom', TRUE));
			$arrUom			= explode(',', $tempUom);

			$tempDesc		= $this->Anti_sql_injection($this->input->post('arrDesc', TRUE));
			$arrDesc		= explode(',', $tempDesc);
			
			$dataPermintaan 	= $this->pekerjaan_model->get_permintaan($id_permintaan)[0];
			$dataPekerjaan = array(
				'id_permintaan'	=> $id_permintaan,
				'work_date'		=> date('Y-m-d', strtotime($temp_work_date[2].'-'.$temp_work_date[1].'-'.$temp_work_date[0])),
				'pic'			=> $pic
			);

			if($dataPekerjaan['work_date'] != $dataPermintaan['tanggal_pekerjaan'] || $dataPekerjaan['pic'] != $dataPermintaan['id_pic']) $resultEditPekerjaan = $this->pekerjaan_model->edit_pekerjaan($dataPekerjaan);
			else $resultEditPekerjaan = 1;
			$resultDetail = false;
			for ($i = 0; $i < sizeof($arrIdItem); $i++) {
				if($arrDesc[$i] != '' && $arrDesc[$i] != 'NULL') $deskripsi = $arrDesc[$i];
				else $deskripsi = NULL;

				if($arrIdItem[$i] != 'new') {
					$dataDetail = array(
						'id_detail'			=> $arrIdItem[$i],
						'nama_pekerjaan'	=> $arrNama[$i],
						'qty'				=> $arrQty[$i],
						'unit'				=> $arrUom[$i],
						'deskripsi'			=> $deskripsi
					);
					$detail = $this->pekerjaan_model->get_detail_pekerjaan($arrIdItem[$i])[0];
					
					if($dataDetail['nama_pekerjaan'] != $detail['nama_pekerjaan'] || $dataDetail['qty'] != $detail['qty'] || $dataDetail['unit'] != $detail['unit'] || $dataDetail['deskripsi'] != $detail['deskripsi']) $resultEditDetailPekerjaan = $this->pekerjaan_model->edit_detail_pekerjaan($dataDetail);
					else $resultEditDetailPekerjaan = 1;
					if($resultEditDetailPekerjaan > 0) $resultDetail = true;
					else {
						$msg = 'Gagal update Detail Permintaan Pekerjaan yang ke-'.($i+1).' database';

						$this->log_activity->insert_activity('update', $msg.' dengan ID : ' .$id_permintaan);
						$result = array('success' => false, 'message' => $msg);
						$resultDetail = false;
						break;
					}
				}else {
					$no = $this->sequence->get_no('work_req');

					$dataDetail = array(
						'id_permintaan'		=> $id_permintaan,
						'nama_pekerjaan'	=> $arrNama[$i],
						'no_pekerjaan'		=> $no,
						'qty'				=> $arrQty[$i],
						'unit'				=> $arrUom[$i],
						'deskripsi'			=> $deskripsi
					);
					
					$resultAddDetailPekerjaan = $this->pekerjaan_model->add_detail_pekerjaan($dataDetail);
					if($resultAddDetailPekerjaan > 0) $resultDetail = true;
					else {
						$msg = 'Gagal update Detail Permintaan Pekerjaan yang ke-'.($i+1).' database';

						$this->log_activity->insert_activity('update', $msg.' dengan ID : ' .$id_permintaan);
						$result = array('success' => false, 'message' => $msg);
						$resultDetail = false;
						break;
					}
				}
			}

			if($resultEditPekerjaan > 0 && $resultDetail == true) {
				$msg = 'Berhasil update Permintaan Pekerjaan ke database';

				$this->log_activity->insert_activity('update', $msg.' dengan ID : ' .$id_permintaan);
				$result = array('success' => true, 'message' => $msg);
			}else {
				if($resultDetail != false) {
					$msg = 'Gagal update Permintaan Pekerjaan ke database';

					$this->log_activity->insert_activity('update', $msg.' dengan ID : ' .$id_permintaan);
					$result = array('success' => false, 'message' => $msg);
				}
			}	
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function detail_pekerjaan($id_permintaan) {
		$permintaan 	= $this->pekerjaan_model->get_permintaan($id_permintaan);
		$detail 		= $this->pekerjaan_model->detail_pekerjaan($id_permintaan);

		$data = array(
			'permintaan'	=> $permintaan,
			'detail'		=> $detail
		);

		$this->load->view('detail_modal_view', $data);
	}

	public function delete_pekerjaan() {
		$data 		= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$resultDelete = $this->pekerjaan_model->delete_pekerjaan($params);
		if($resultDelete > 0) {
			$msg = 'Berhasil menghapus Data Permintaan Pekerjaan';
			$this->log_activity->insert_activity('delete', $msg.' dengan ID : ' .$params['id']);
			$result = array('success' => true, 'message' => $msg);
		}else {
			$msg = 'Gagal menghapus Data Permintaan Pekerjaan';
			$this->log_activity->insert_activity('delete', $msg.' dengan ID : ' .$params['id']);
			$result = array('success' => false, 'message' => $msg);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function delete_detail_pekerjaan() {
		$data 		= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$resultDelete = $this->pekerjaan_model->delete_detail_pekerjaan($params);
		if($resultDelete > 0) {
			$msg = 'Berhasil menghapus Detail Permintaan Pekerjaan';
			$this->log_activity->insert_activity('delete', $msg.' dengan ID : ' .$params['id']);
			$result = array('success' => true, 'message' => $msg);
		}else {
			$msg = 'Gagal menghapus Detail Permintaan Pekerjaan';
			$this->log_activity->insert_activity('delete', $msg.' dengan ID : ' .$params['id']);
			$result = array('success' => false, 'message' => $msg);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
}
