<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Quotation_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	/**
      * This function is get the list data in po_quotation table
      * @param : $params is where condition for select query
      */



	public function doedit_schedule($data)
	{
		$this->db->where('id_order', $data[0]['id_order']);  
		$this->db->delete('t_schedule_shipping'); 

		$sql 	= 'CALL schedule_add(?, ?, ?, ?, ?, ?)';
		
		$dataLen = count($data);
		$delivery = '';
		for($i=0;$i<$dataLen;$i++){
			$date_shipping_plan = ($data[$i]['date_shipping_plan']!='')?$data[$i]['date_shipping_plan']:NULL;
			$this->db->query($sql,array(
				$data[$i]['id_order'],
				$data[$i]['qty_shipping'],
				$data[$i]['type_shipping'],
				$data[$i]['note_shipping'],
				$date_shipping_plan,
				NULL
			));

			if(intval($data[$i]['type_shipping'])==0){
				$delivery = 'ASAP';
			}else if(intval($data[$i]['type_shipping'])==1){
				$delivery = 'Menunggu info';
			}else{
				if($i==0){
					$delivery = $date_shipping_plan;
				}else{
					$delivery .= ','.$date_shipping_plan;
				}
			}
		}

        $return = array(
		        'delivery' => $delivery
		);

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function lists($params = array()) {
		$sql 	= 'CALL req_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql, array (
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data'				=> $result,
			'total_filtered'	=> $total['@total_filtered'],
			'total'				=> $total['@total']
		);

		return $return;
	}

	/**
      * This function is get the list data in valas table
      */
	public function valas()
	{
		$this->db->select('valas_id,nama_valas');
		$this->db->from('m_valas');

		$query 	= $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is get the list data in eksternal table
      */
	public function customer() {
		// $this->db->select('id,name_eksternal,kode_eksternal');
		// $this->db->from('t_eksternal');
		// $this->db->where('type_eksternal', 1);
		// $this->db->where('kode_eksternal', 1);

		// $query 	= $this->db->get();
		
		$query =   $this->db->query("SELECT id,name_eksternal,kode_eksternal from t_eksternal where
		type_eksternal =1 and kode_eksternal IS NOT NULL ");
		
		//$resultas = $queryas->result_array();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is get the list data in material table
      */
	public function item($cust_id=null) {
		// $userid = $this->session->userdata('user_id');
		$this->db->select('id, stock_name, stock_code,cust_id');
		$this->db->from('m_material');
		// $this->db->where('status', 1);
		$this->db->where('type', 4);
		if($cust_id!=null)
			$this->db->where('cust_id', $cust_id);
		// $this->db->where('cust_id', $userid);

		$query 	= $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_quotation($data) {
		$sql 	= 'CALL po_add(?,?,?,?,?,?,?,?,?,?,?)';

		$query = $this->db->query($sql, array(
			$data['cust_id'],
			$data['issue_date'],
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			4,
			$data['no'],
			$data['term_of_pay']
		));
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];
		
		$return['result'] = $query->result_array();
		$return['code'] = $this->db->affected_rows();
		$return['lastid'] = $lastid;

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_material($data) {
		$sql 	= 'CALL material_add(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

		$query = $this->db->query($sql, array(
			$data['no_bc'],
			$data['stock_code'],
			$data['stock_name'],
			$data['stock_description'],
			$data['unit'],
			$data['type'],
			$data['qty'],
			$data['weight'],
			$data['treshold'],
			$data['id_properties'],
			$data['id_gudang'],
			$data['status'],
			$data['base_price'],
			$data['base_qty'],
			$data['cust_id'],
			NULL
		));
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];
		
		$return['result'] = $query->result_array();
		$return['code'] = $this->db->affected_rows();
		$return['lastid'] = $lastid;

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function update_eksternal($last_seq,$cust_id) {
		$sql 	= 'UPDATE t_eksternal SET last_seq = ? where id = ?';

		$query = $this->db->query($sql, array(
			$last_seq,
			$cust_id
		));
		
		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_customer_spec($id_material,$cust_id) {
		$sql 	= 'INSERT INTO t_cust_spec(id_produk,id_eksternal)VALUES (?,?)';

		$query = $this->db->query($sql, array(
			$id_material,
			$cust_id
		));
		
		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_esf($id_material,$cust_id) {
		$sql 	= 'INSERT INTO t_esf(id_produk,id_po_quotation) VALUES (?,?)';

		$query = $this->db->query($sql, array(
			$id_material,
			$cust_id
		));
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];
		
		$return['result'] = $query->result_array();
		$return['code'] = $this->db->affected_rows();
		$return['lastid'] = $lastid;

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_esf_layout($id_esf) {
		$sql 	= 'INSERT INTO t_esf_layout(id_esf) VALUES (?)';

		$query = $this->db->query($sql, array(
			$id_esf
		));
		
		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_esft($id_esf) {
		$sql 	= 'INSERT INTO t_esft(id_cust_spec) VALUES (?)';

		$query = $this->db->query($sql, array(
			$id_esf
		));
		
		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_bom_supp($id_material) {
		$sql 	= 'INSERT INTO t_bom_m SELECT NULL,'.$id_material.' AS ID,category,material,NULL,0 FROM `t_supp` GROUP BY material ORDER BY seq';

		$query = $this->db->query($sql);
		
		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_bom($id_material) {
		$sql 	= 'INSERT INTO t_bom_m (id_stock,category,material,id_material_comp) VALUES (?,"MAIN","MAIN",NULL)';

		$query = $this->db->query($sql,array(
			$id_material
		));
		
		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_bom2($id_material) {
		$sql 	= 'INSERT INTO t_bom_m (id_stock,category,material,id_material_comp) VALUES (?,"MAIN 2","MAIN 2",NULL)';

		$query = $this->db->query($sql,array(
			$id_material
		));
		
		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_process_mat($id_material) {
		$sql 	= 'INSERT INTO t_process_mat SELECT NULL AS AAS,'.$id_material.' AS IDP,id,1,"" as os,"" ink, "" as lot FROM t_process_flow ORDER BY level';

		$query = $this->db->query($sql);
		
		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_prod_layout($id_material) {
		$sql 	= 'INSERT INTO t_prod_layout (id_material) VALUES (?)';

		$query = $this->db->query($sql,array(
			$id_material
		));
		
		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_request_item($id_po_quotation,$id_material,$arrQty) {
		$sql 	= 'INSERT INTO t_request_item(id_po_quotation,id_produk,qty,status) VALUES (?,?,?,"Waiting")';

		$query = $this->db->query($sql,array(
			$id_po_quotation,
			$id_material,
			$arrQty
		));
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];
		
		$return['result'] = $query->result_array();
		$return['code'] = $this->db->affected_rows();
		$return['lastid'] = $lastid;


		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function add_order($data) {
		$sql 	= 'CALL order_add_req(?,?,?,?,?,?,?,?,?)';

		$query = $this->db->query($sql,array(
			$data['id_po_quotation'],
			$data['id_product'],
			$data['id_product'],
			$data['qty'],
			$data['price'],
			$data['diskon'],
			$data['disc_price'],
			$data['amount_price'],
			$data['no_po']
		));
		
		$return['result'] = $query->result_array();
		$return['code'] = $this->db->affected_rows();
		
		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_eksternal($cust_id) {
		$sql = 'SELECT * FROM t_eksternal WHERE id = ?';

		$query 	= $this->db->query($sql,array(
			$cust_id
		));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_request_item($id_po_quotation) {
		$sql = 'SELECT * FROM t_request_item WHERE id_po_quotation = ?';

		$query 	= $this->db->query($sql,array(
			$id_po_quotation
		));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function update_order($arrQty,$id_order,$id_po_quotation) {
		$sql 	= 'UPDATE t_order SET qty = ? WHERE id_order = ? AND id_po_quotation = ?';

		$query = $this->db->query($sql,array(
			$arrQty,
			$id_order,
			$id_po_quotation
		));
		
		$return = $this->db->affected_rows();
		
		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function update_request_item($arrQty,$id_po_quotation,$id_material) {
		$sql 	= 'UPDATE t_request_item SET qty = ? WHERE id_po_quotation = ? AND id_produk = ?';

		$query = $this->db->query($sql,array(
			$arrQty,
			$id_po_quotation,
			$id_material
		));
		
		$return = $this->db->affected_rows();
		
		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	function delete_item($data) {
		$sql 	= 'CALL order_request_delete(?, ?, ?)';

		$query 	=  $this->db->query($sql, array (
			$data['id'],
			$data['idProduk'],
			$data['idQuot']
		));

		// $result = $query->affected_rows();
		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	function delete_quotation($id) {
		$this->db->where('id', $id);  
		$this->db->delete('t_po_quotation');
		
		$this->db->where('id_po_quotation', $id);  
		$this->db->delete('t_order');

		$this->db->where('id_po_quot', $id);  
		$this->db->delete('t_bom');  

		$this->db->close();
		$this->db->initialize();
	}	
	
	function close_quotation($id) {
		$this->db->query("update t_po_quotation set status = 3 where id = ".$id." ");
		
		
		// $this->db->query("
		// INSERT INTO t_order
		// SELECT NULL,id_po_quotation,id_produk,id_produk,qty,0 as prc,0 as prc1,0 as prc2,0 as prc3 from t_request_item
		// where id_po_quotation = ".$id." 
		
		// ");

		$this->db->close();
		$this->db->initialize();
	} 

	/**
      * This function is get data in po_quotation table by id
      * @param : $id is where condition for select query
      */

	public function po_detail($id) {
		$sql 	= 'CALL po_search_id(?)';

		$query 	= $this->db->query($sql,array(
			$id
		));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function delete_order_request($id_po_quotation) {
		$sql 	= 'DELETE FROM t_order WHERE id_po_quotation = ?';

		$query 	= $this->db->query($sql,array(
			$id_po_quotation
		));
		
		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function order_detail($id) {
		$sql = 'SELECT a.id_order, a.id_produk, a.qty, b.stock_code, b.stock_name,a.no_po, b.id AS id_material FROM t_order a
			LEFT JOIN m_material b ON a.id_produk = b.id
			WHERE a.id_po_quotation = '.$id;

		$query =   $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function check_po($id) {
		$sql = 'SELECT COUNT(DISTINCT(a.no_po)) AS check_po , no_po 
			FROM t_order a
			LEFT JOIN m_material b ON a.id_produk = b.id
			WHERE a.id_po_quotation = '.$id;

		$query =   $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function edit_quotation($data) {
		$sql = 'CALL order_request_update(?,?,?)';

		$query = $this->db->query($sql, array(
			$data['id_quotation'],
			$data['issue_date'],
			$data['term_of_pay']
		));
		
		$return = $this->db->affected_rows();
		
		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function edit_quotation1($data, $arridPoQuot, $arrItemId, $arrPartName, $arrQty, $arrNoPO) {
		$sql = 'CALL order_request_update(?,?,?)';

		$query = $this->db->query($sql, array(
			$data['id_quotation'],
			$data['issue_date'],
			$data['term_of_pay']
		));

		$resultQuery = $this->db->affected_rows();

		for ($i=0; $i < sizeof($arrItemId); $i++) {
			if($arridPoQuot[$i] != 0) {
				$sqlOrder	= 'UPDATE t_order
					SET qty = '.$arrQty[$i].' WHERE id_order = '.$arridPoQuot[$i].' AND id_po_quotation = '.$data['id_quotation'];
				$sqlRequest = 'UPDATE t_request_item 
					SET qty = '.$arrQty[$i].' WHERE id_po_quotation = '.$data['id_quotation'].' AND id_produk = '.$arrItemId[$i];

				$this->db->query($sqlOrder);
				$this->db->query($sqlRequest);
				$resultQuery = $this->db->affected_rows();
			}else {
				if($arrItemId[$i] == 'new') {
					$queryas	= $this->db->query("SELECT * from t_eksternal where id = ".$data['cust_id']." ");
					$resultas	= $queryas->result_array();
					$oos		= $resultas[0];
					$new_seq 	= $oos['last_seq'] + 1;
					$length_seq = strlen($new_seq);

					if($length_seq < 2) $new_seq_t = '0'.$new_seq;
					else $new_seq_t = $new_seq;

					$m_code = $oos['kode_eksternal']."-PSS-P-".$new_seq_t;
					$sqlm 	= 'INSERT INTO m_material(
						stock_code,
						stock_name,
						stock_description,
						unit,
						`type`,
						treshold,
						id_properties,
						id_gudang,
						`status`,
						cust_id,
						date_create
					)VALUES (
						"'.$m_code.'",
						"'.$arrPartName[$i].'",
						"",
						10,
						4,
						10,
						3,
						5,
						4,
						'.$data['cust_id'].',
						NOW()
					)';

					$querym = $this->db->query($sqlm);

					$queryld 		= $this->db->query('SELECT LAST_INSERT_ID()');
					$rowld 			= $queryld->row_array();
					$id_material 	= $rowld['LAST_INSERT_ID()'];
					
					$this->db->query("update t_eksternal set last_seq = ".$new_seq." where id = ".$data['cust_id']." ");
					
					$sqlms 	= 'INSERT INTO t_cust_spec(
						id_produk,
						id_eksternal
					)VALUES (
						"'.$id_material.'",
						'.$data['cust_id'].'							
					)';

					$queryms 	= $this->db->query($sqlms);

					$sqlms2 	= 'INSERT INTO t_esf(
						id_produk,
						id_po_quotation
					)VALUES (
						"'.$id_material.'",
						'.$data['cust_id'].'							
					)';

					$queryms2 	= $this->db->query($sqlms2);

					$queryld2	= $this->db->query('SELECT LAST_INSERT_ID()');
					$rowld2		= $queryld2->row_array();
					$id_esft	= $rowld2['LAST_INSERT_ID()'];

					$sqlms 	= 'INSERT INTO t_esft(
						id_cust_spec
					)VALUES (
						"'.$id_esft.'"
					)';

					$queryms 	= $this->db->query($sqlms);

					$sql = 'INSERT INTO t_bom_m SELECT NULL,'.$id_material.' AS ID,category,material,NULL,0 FROM `t_supp` GROUP BY material ORDER BY seq';

					$query 	= $this->db->query($sql);
					
					$sql2 	= 'INSERT INTO t_bom_m (
						id_stock,
						category,
						material,
						id_material_comp
					)VALUES (
						'.$id_material.',
						"MAIN",
						"MAIN",
						NULL
					)';

					$query2 = $this->db->query($sql2);

					$sql3 	= "INSERT INTO `t_process_mat` SELECT NULL AS AAS,".$id_material." AS IDP,id,1,'' as os,'' ink, '' as lot
						FROM `t_process_flow` ORDER BY `level`";

					$query3 	= $this->db->query($sql3);

					$sql4 	= 'INSERT INTO t_prod_layout (
						id_material
					)VALUES (
						'.$id_material.'
					)';

					$query4 	= $this->db->query($sql4);
					
					$sql5 = 'INSERT INTO t_order
						SELECT NULL, id_po_quotation, id_produk, id_produk, qty, 0 as prc, 0 as prc1, 0 as prc2, 0 as prc3, "'.$arrNoPO[$i].'" AS no_po,
						FROM t_request_item WHERE id_po_quotation = '.$data['id_quotation'];
					$query5 	= $this->db->query($sql5);
				
				}else $id_material = $arrItemId[$i];

				$sql = 'INSERT INTO t_request_item(
					id_po_quotation,
					id_produk,
					qty,
					status
				)values (
					'.$data['id_quotation'].',
					'.$id_material.',
					'.$arrQty[$i].',
					"Waiting"
				)';
				$query 	= $this->db->query($sql);
				
				$sql2 = 'INSERT INTO t_order
					SELECT NULL, id_po_quotation, id_produk, id_produk, qty, 0 as prc, 0 as prc1, 0 as prc2, 0 as prc3, "'.$arrNoPO[$i].'" AS no_po,
					FROM t_request_item WHERE id_po_quotation = '.$data['id_quotation'];
				$query2 	= $this->db->query($sql2);
			}
			
			$sql = 'INSERT INTO t_order
				SELECT NULL, id_po_quotation, id_produk, id_produk, qty, 0 as prc, 0 as prc1, 0 as prc2, 0 as prc3, "'.$data['no_po'].'" AS no_po,
				FROM t_request_item WHERE id_po_quotation = '.$data['id_quotation'];
			$query = $this->db->query($sql);
		}

		$this->db->where('id_po_quotation', $data['id_quotation']);  
		$this->db->delete('t_order');

		$sql = 'INSERT INTO t_order
			SELECT NULL, id_po_quotation, id_produk, id_produk, qty, 0 as prc, 0 as prc1, 0 as prc2, 0 as prc3, "'.$arrNoPO[$i].'" AS no_po,
			FROM t_request_item WHERE id_po_quotation = '.$data['id_quotation'];
		$query = $this->db->query($sql);
		$resultQuery = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $resultQuery;
	}
	public function detail_req($id) {
		$queryas =   $this->db->query("SELECT a.*,b.`stock_code`,b.`stock_name` FROM `t_request_item` a
				LEFT JOIN `m_material` b
				ON a.`id_produk` = b.`id` 
				where id_po_quotation = ".$id."
				");
		
		$resultas = $queryas->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $resultas;
		
	}
	
	public function detail($id)
	{
		$this->db->select("name_eksternal,
							order_no,
							order_date,
							transport,
							packing,
							issue_date,
							`price_term`,
							GROUP_CONCAT(t_order.id_order SEPARATOR '~') AS id_order,
							GROUP_CONCAT(id_produk SEPARATOR '~') AS id_produk,
							GROUP_CONCAT(price SEPARATOR '~') AS price,
							GROUP_CONCAT(diskon SEPARATOR '~') AS diskon,
							GROUP_CONCAT(disc_price SEPARATOR '~') AS disc_price,
							nama_valas,
							GROUP_CONCAT(stock_name SEPARATOR '~') AS stock_name,
							GROUP_CONCAT(t_order.qty SEPARATOR '~') AS qty,
							GROUP_CONCAT(amount_price SEPARATOR '~') AS amount_price,
							t_status_g.`status`,
	                        approval_date,
	                        notes");
		$this->db->from('t_po_quotation');
		$this->db->join('t_eksternal', 't_po_quotation.cust_id=t_eksternal.id');
		$this->db->join('t_order', 't_po_quotation.id=t_order.id_po_quotation');
		$this->db->join('m_valas', 't_po_quotation.valas_id=m_valas.valas_id');
		$this->db->join('m_material', 't_order.id_produk=m_material.id');
		$this->db->join('t_status_g', 't_po_quotation.status=t_status_g.id_status');
		$this->db->where('t_po_quotation.id', $id);

		$query 	= $this->db->get();

		$return = $query->result_array();

		$orderArray = explode('~',$return[0]['id_order']);
		$idArray = explode('~',$return[0]['id_produk']);
		$stockArray = explode('~',$return[0]['stock_name']);
		$priceArray = explode('~',$return[0]['price']);
		$diskonArray = explode('~',$return[0]['diskon']);
		$disc_priceArray = explode('~',$return[0]['disc_price']);
		$qtyArray = explode('~',$return[0]['qty']);
		$amount_priceArray = explode('~',$return[0]['amount_price']);

		$itemCount = count($stockArray);

		$data = array();
		for($i=0;$i<$itemCount;$i++){
			$this->db->select('type_shipping,date_shipping_plan');
			$this->db->from('t_schedule_shipping');
			$this->db->where('id_order', $orderArray[$i]);

			$query_shipping = $this->db->get();

			$shipping = $query_shipping->result_array();

			if(count($shipping)){
				if($shipping[0]["type_shipping"] == '0'){
					$delivery = 'ASAP';
				}else if($shipping[0]["type_shipping"] == '1'){
					$delivery = 'Menunggu info';
				}else{
					$delivery = '';
					$j = 0;
					foreach($shipping as $key) {
						if($j==0){
							$delivery = $key['date_shipping_plan'];
						}else{
							$delivery .= ','.$key['date_shipping_plan'];
						}
						$j++;
					}
				}
			}else{
				$delivery = '-';
			}

			$qty = (!isset($qtyArray[$i]) || $qtyArray[$i]=='')?'0':$qtyArray[$i];
			$amount_price = (!isset($amount_priceArray[$i]) || $amount_priceArray[$i]=='')?'0':$amount_priceArray[$i];

			$data[] = array(
						"id_order" => $orderArray[$i],
						"id_produk" => $idArray[$i],
						"stock_name" => $stockArray[$i],
						"price" => $priceArray[$i],
						"diskon" => $diskonArray[$i],
						"disc_price" => $disc_priceArray[$i],
						"qty" => $qty,
						"amount_price" => $amount_price,
						"delivery" => $delivery
					  );
		}

		$approval_date = ($return[0]['approval_date']=='')?'-':date_format(date_create($return[0]['approval_date']),'d F Y');
		$notes = ($return[0]['notes']=='')?'-':$return[0]['notes'];

		$result = array(
					"id" => $id,
					"name_eksternal" => $return[0]['name_eksternal'],
					"issue_date" => date_format(date_create($return[0]['issue_date']),'d F Y'),
					"nama_valas" => $return[0]['nama_valas'],
					"price_term" => $return[0]['price_term'],
					"status" => $return[0]['status'],
					"approval_date" => $approval_date,
					"notes" => $notes,
					"order_no" => $return[0]['order_no'],
					"order_date" => $return[0]['order_date'],
					"transport" => $return[0]['transport'],
					"packing" => $return[0]['packing'],
					"item" => $data
				  );

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	/**
      * This function is used to Update Record in po_quotation table
      * @param : $data - updated array 
      */

	public function change_status($data)
	{
		
		$kk = explode('|',$data);
		
		$sql 	= '
				UPDATE t_request_item set status = "'.$kk[1].'" where id_order =  '.$kk[0].'
			';

			$query 	= $this->db->query($sql);
			
		$this->db->close();
		$this->db->initialize();
	}
}