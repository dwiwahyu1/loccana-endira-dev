<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Request_order extends MX_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('request_order/quotation_model');
		$this->load->library('log_activity');
		$this->load->library('sequence');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'request_order/views/index');
	}

	function lists() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;
		$order_fields = array('', 'id', 'name_eksternal');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->quotation_model->lists($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		foreach ($list['data'] as $k => $v) {
			$id = '<div class="changed_status" onClick="detail_quotation(\'' . $v['id'] . '\')">';
			$id .=      $v['req_no'];
			$id .= '</div>';

			$actions = '';
			if($v['status'] == "On Quotation"){
				$actions = 'Close';
				/*$actions .=
					'<div class="btn-group">'.
						'<button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edit_quotation(\'' . $v['id'] .'\')">'.
							'<i class="fa fa-edit"></i>'.
						'</button>'.
					'</div>';*/
			}

			if($v['status'] == "On Request"){
				$actions .=
					'<div class="btn-group">'.
						'<button class="btn btn-sm btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edit_quotation(\'' . $v['id'] . '\')">'.
							'<i class="fa fa-edit"></i>'.
						'</button>'.
					'</div>'.
					'<div class="btn-group">'.
						'<button class="btn btn-sm btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="delete_quotation(\'' . $v['id'] . '\')">'.
							'<i class="fa fa-trash"></i>'.
						'</button>'.
					'</div>'.

					'<div class="btn-group">'.
						'<button class="btn btn-sm btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Approval" onClick="approval_quotation(\'' . $v['id'] . '\')">'.
							'<i class="fa fa-check"></i>'.
						'</button>'.
					'</div>'.

					'<div class="btn-group">'.
						'<button class="btn btn-sm btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Close" onClick="close_quotation(\'' . $v['id'] . '\')">'.
							'<i class="fa fa-close"></i>'.
						'</button>'.
					'</div>';
			}

			array_push($data, array(
				$id,
				$v['name_eksternal'],
				$v['issue_date'],
				$v['part_number'],
				$v['total_products'],
				$v['total_items'],
				$v['prod_term'].' Days',
				$v['status'],
				$actions
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	public function add() {
		$customer 	= $this->quotation_model->customer();
		$item 		= $this->quotation_model->item();

		$data = array(
			'customer'	=> $customer,
			// 'item'		=> $item
		);

		$this->load->view('add_modal_view',$data);
	}

	public function get_item() {
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);
		$result = $this->quotation_model->item($params['cust_id']);
		// var_dump($params['cust_id'],$result);die;
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result);
	}


	public function add_quotation() {
		$this->form_validation->set_rules('issue_date', 'Issue Date', 'trim|required');
		$this->form_validation->set_rules('cust_id', 'Customer', 'trim|required');
		$this->form_validation->set_rules('term_of_pay', 'Term of Production', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$no 				= $this->sequence->get_no('req_id');
			$issue_date			= $this->Anti_sql_injection($this->input->post('issue_date', TRUE));
			$temp_issue_date	= explode("/", $issue_date);
			$cust_id			= $this->Anti_sql_injection($this->input->post('cust_id', TRUE));
			$term_of_pay		= $this->Anti_sql_injection($this->input->post('term_of_pay', TRUE));
			$tempItem			= $this->Anti_sql_injection($this->input->post('arrItemId', TRUE));
			$arrItemId			= explode(',', $tempItem);
			$tempPartName		= $this->Anti_sql_injection($this->input->post('arrPartName', TRUE));
			$arrPartName		= explode(',', $tempPartName);
			$tempQty			= $this->Anti_sql_injection($this->input->post('arrQty', TRUE));
			$arrQty				= explode(',', $tempQty);
			$tempNoPO			= $this->Anti_sql_injection($this->input->post('arrNoPO', TRUE));
			$arrNoPO			= explode(',', $tempNoPO);
			
			$dataQuotation = array(
				'no'				=> $no,
				'cust_id'			=> $cust_id,
				'issue_date'		=> date('Y-m-d', strtotime($temp_issue_date[2].'-'.$temp_issue_date[1].'-'.$temp_issue_date[0])),
				'term_of_pay' 		=> $term_of_pay
			);

			
			$result_quotation = $this->quotation_model->add_quotation($dataQuotation);
			if($result_quotation['result'] > 0) {
				
				$id_po_quotation = $result_quotation['lastid'];
				
				for ($i=0; $i < sizeof($arrItemId); $i++) {
					if($arrItemId[$i] == 'new') {
						$getEksternal = $this->quotation_model->get_eksternal($cust_id);
						
						$new_seq = $getEksternal[0]['last_seq'] + 1;
						$length_seq = strlen($new_seq);

						if($length_seq < 2) { $new_seq_t = '0'.$new_seq; } 
						else { $new_seq_t = $new_seq; }
						
						$kode_material = $getEksternal[0]['kode_eksternal']."-PSS-P-".$new_seq_t;
			
						$data_material = array(
							'no_bc' 			=> NULL,
							'stock_code' 		=> $kode_material,
							'stock_name' 		=> $arrPartName[$i],
							'stock_description'	=> NULL,
							'unit'				=> 10,
							'type'				=> 4,
							'qty'				=> 0,
							'weight'			=> 0,
							'treshold'			=> 10,
							'id_properties'		=> 3,
							'id_gudang'			=> 5,
							'status'			=> 4,
							'base_price'		=> 0,
							'base_qty'			=> 0,
							'cust_id'			=> $cust_id
						);
						
						$result_material 		= $this->quotation_model->add_material($data_material);
						$id_material 			= $result_material['lastid'];
						
						$result_update_material = $this->quotation_model->update_eksternal($new_seq,$cust_id);
						$result_customer_spec 	= $this->quotation_model->add_customer_spec($id_material,$cust_id);
						$result_esf 			= $this->quotation_model->add_esf($id_material,$cust_id);
						$id_esf					= $result_esf['lastid'];
						
						$result_esf_layout 		= $this->quotation_model->add_esf_layout($id_esf);
						$result_esft	 		= $this->quotation_model->add_esft($id_esf);
						$result_bom_supp 		= $this->quotation_model->add_bom_supp($id_material);
						$result_bom		 		= $this->quotation_model->add_bom($id_material);
						$result_bom2		 	= $this->quotation_model->add_bom2($id_material);
						//$result_process_mat	 	= $this->quotation_model->add_process_mat($id_material);
						//$result_prod_layout	 	= $this->quotation_model->add_prod_layout($id_material);
						$request_item		 	= $this->quotation_model->add_request_item($id_po_quotation,$id_material,$arrQty[$i]);
						$id_request 			= $request_item['lastid'];
						
						$get_request_item 		= $this->quotation_model->get_request_item($id_po_quotation);
						
						$data_order = array(
							'id_po_quotation' 	=> $id_po_quotation,
							'id_product' 		=> $get_request_item[$i]['id_produk'],
							'qty' 				=> $get_request_item[$i]['qty'],
							'price' 			=> 0,
							'diskon' 			=> 0,
							'disc_price'		=> 0,
							'amount_price' 		=> 0,
							'no_po' 			=> $arrNoPO[$i],
						);
						
						$result_order_request	= $this->quotation_model->add_order($data_order);
						
					} else {
						$id_material = $arrItemId[$i];
						
						$request_item		 	= $this->quotation_model->add_request_item($id_po_quotation,$id_material,$arrQty[$i]);
						$id_request 			= $request_item['lastid'];
						
						$get_request_item 		= $this->quotation_model->get_request_item($id_po_quotation);
						
						$data_order = array(
							'id_po_quotation' 	=> $id_po_quotation,
							'id_product' 		=> $get_request_item[$i]['id_produk'],
							'qty' 				=> $get_request_item[$i]['qty'],
							'price' 			=> 0,
							'diskon' 			=> 0,
							'disc_price'		=> 0,
							'amount_price' 		=> 0,
							'no_po' 			=> $arrNoPO[$i]
						);
						
						$result_order_request	= $this->quotation_model->add_order($data_order);
						
					}
				}
			}
			
			if($result_quotation['result'] > 0) {
				$this->log_activity->insert_activity('insert', 'Berhasil Insert Request Order');
				$result = array('success' => true, 'message' => 'Berhasil menambahkan Request Order ke database');
			}else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert List Request Order');
				$result = array('success' => false, 'message' => 'Gagal menambahkan Request Order ke database');
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function edit($id) {
		$customer 		= $this->quotation_model->customer();
		$po_detail 		= $this->quotation_model->po_detail($id);
		$item 			= $this->quotation_model->item();
		$order_detail 	= $this->quotation_model->order_detail($id);
		$check_po 	= $this->quotation_model->check_po($id);

		$data = array(
			'customer'		=> $customer,
			'item'			=> $item,
			'po_detail'		=> $po_detail,
			'order_detail'	=> $order_detail,
			'check_po'		=> $check_po[0]
		);
		
		//print_r($po_detail);die;

		$this->load->view('edit_modal_view',$data);
	}

	public function delete_item() {
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$result = $this->quotation_model->delete_item($params);

		if($result > 0) {
			$result = array('success' => true, 'message' => 'Berhasil Hapus item');
			$this->log_activity->insert_activity('delete', 'Berhasil Delete item Request Order id : '.$params['id']);
		}else $result = array('success' => false, 'message' => 'Gagal Hapus item');

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function delete_quotation() {
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);

		$this->quotation_model->delete_quotation($params['id']);

		$msg = 'Berhasil menghapus data Request';

		$result = array(
			'success' => true,
			'message' => $msg
		);
		
		$this->log_activity->insert_activity('delete', $msg. ' dengan id ' .$params['id']);

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}    
	
	public function close_quotation() {
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);

		$this->quotation_model->close_quotation($params['id']);

		$msg = 'Berhasil close request ';

		$result = array(
			'success' => true,
			'message' => $msg
		);
		
		$this->log_activity->insert_activity('close', $msg. ' dengan ID ' .$params['id']);

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function edit_quotation() {
		$this->form_validation->set_rules('issue_date', 'Issue Date', 'trim|required');
		$this->form_validation->set_rules('cust_id', 'Customer', 'trim|required');
		$this->form_validation->set_rules('term_of_pay', 'Term of Production', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$id_quotation		= $this->Anti_sql_injection($this->input->post('id_quotation', TRUE));
			$id_po_quotation 	= $id_quotation;
			$issue_date			= $this->Anti_sql_injection($this->input->post('issue_date', TRUE));
			$temp_issue_date	= explode("/", $issue_date);
			$cust_id			= $this->Anti_sql_injection($this->input->post('cust_id', TRUE));
			$term_of_pay		= $this->Anti_sql_injection($this->input->post('term_of_pay', TRUE));
			$tempidPoQuot		= $this->Anti_sql_injection($this->input->post('arridPoQuot', TRUE));
			$arridPoQuot		= explode(',', $tempidPoQuot);
			$tempidMaterial		= $this->Anti_sql_injection($this->input->post('arridMaterial', TRUE));
			$arridMaterial		= explode(',', $tempidMaterial);
			$tempItem			= $this->Anti_sql_injection($this->input->post('arrItemId', TRUE));
			$arrItemId			= explode(',', $tempItem);
			$tempPartName		= $this->Anti_sql_injection($this->input->post('arrPartName', TRUE));
			$arrPartName		= explode(',', $tempPartName);
			$tempQty			= $this->Anti_sql_injection($this->input->post('arrQty', TRUE));
			$arrQty				= explode(',', $tempQty);
			$tempNoPO			= $this->Anti_sql_injection($this->input->post('arrNoPO', TRUE));
			$arrNoPO			= explode(',', $tempNoPO);
			
			//print_r($tempItem);die;
			
			$dataQuotation = array(
				'id_quotation'		=> $id_quotation,
				'cust_id'			=> $cust_id,
				'issue_date'		=> date('Y-m-d', strtotime($temp_issue_date[2].'-'.$temp_issue_date[1].'-'.$temp_issue_date[0])),
				'term_of_pay' 		=> $term_of_pay
			);
			
			$result_quotation = $this->quotation_model->edit_quotation($dataQuotation);
			$result_order_request = '';
			$delete_order_request	= $this->quotation_model->delete_order_request($id_po_quotation);
			
			for ($i=0; $i < sizeof($arrItemId); $i++) {
				if($arridPoQuot[$i] != 0) {
					$sqlOrder 	= $this->quotation_model->update_order($arrQty[$i],$arridPoQuot[$i],$id_quotation);
					$sqlRequest = $this->quotation_model->update_request_item($arrQty[$i],$id_quotation,$arridMaterial[$i]);
				}else{
					if($arrItemId[$i] == 'new') {
						$getEksternal = $this->quotation_model->get_eksternal($cust_id);
					
						$new_seq = $getEksternal[0]['last_seq'] + 1;
						$length_seq = strlen($new_seq);

						if($length_seq < 2) { $new_seq_t = '0'.$new_seq; } 
						else { $new_seq_t = $new_seq; }
						
						$kode_material = $getEksternal[0]['kode_eksternal']."-PSS-P-".$new_seq_t;
						
						$data_material = array(
							'no_bc' 			=> '',
							'stock_code' 		=> $kode_material,
							'stock_name' 		=> $arrPartName[$i],
							'stock_description'	=> NULL,
							'unit'				=> 10,
							'type'				=> 4,
							'qty'				=> 0,
							'weight'			=> 0,
							'treshold'			=> 10,
							'id_properties'		=> 3,
							'id_gudang'			=> 5,
							'status'			=> 4,
							'base_price'		=> 0,
							'base_qty'			=> 0,
							'cust_id'			=> $cust_id
						);
						
						$result_material 		= $this->quotation_model->add_material($data_material);
						$id_material 			= $result_material['lastid'];
						
						$result_update_material = $this->quotation_model->update_eksternal($new_seq,$cust_id);
						$result_customer_spec 	= $this->quotation_model->add_customer_spec($arridMaterial[$i],$cust_id);
						$result_esf 			= $this->quotation_model->add_esf($arridMaterial[$i],$cust_id);
						$id_esf					= $result_esf['lastid'];
					
						$result_esf_layout 		= $this->quotation_model->add_esf_layout($id_esf);
						$result_esft	 		= $this->quotation_model->add_esft($id_esf);
						$result_bom_supp 		= $this->quotation_model->add_bom_supp($arridMaterial[$i]);
						$result_bom		 		= $this->quotation_model->add_bom($arridMaterial[$i]);
						$result_bom2		 	= $this->quotation_model->add_bom2($arridMaterial[$i]);
						$result_process_mat	 	= $this->quotation_model->add_process_mat($arridMaterial[$i]);
						$result_prod_layout	 	= $this->quotation_model->add_prod_layout($arridMaterial[$i]);
						$request_item		 	= $this->quotation_model->add_request_item($id_quotation,$id_material,$arrQty[$i]);
						$id_request 			= $request_item['lastid'];
						
						$get_request_item 		= $this->quotation_model->get_request_item($id_quotation);
						
						$data_order = array(
							'id_po_quotation' 	=> $id_po_quotation,
							'id_product' 		=> $get_request_item[$i]['id_produk'],
							'qty' 				=> $get_request_item[$i]['qty'],
							'price' 			=> 0,
							'diskon' 			=> 0,
							'disc_price'		=> 0,
							'amount_price' 		=> 0,
							'no_po' 			=> $arrNoPO[$i],
						);
						
						$result_order_request	= $this->quotation_model->add_order($data_order);
					
					} else { 
						$id_material = $arrItemId[$i];
						
						$request_item		 	= $this->quotation_model->add_request_item($id_quotation,$id_material,$arrQty[$i]);
						$id_request 			= $request_item['lastid'];
						
						$get_request_item 		= $this->quotation_model->get_request_item($id_quotation);
						
						$data_order = array(
							'id_po_quotation' 	=> $id_po_quotation,
							'id_product' 		=> $get_request_item[$i]['id_produk'],
							'qty' 				=> $get_request_item[$i]['qty'],
							'price' 			=> 0,
							'diskon' 			=> 0,
							'disc_price'		=> 0,
							'amount_price' 		=> 0,
							'no_po' 			=> $arrNoPO[$i],
						);
						
						$result_order_request	= $this->quotation_model->add_order($data_order);
					
					}
				}
			}
			
			$get_request_item 		= $this->quotation_model->get_request_item($id_quotation);
						
			$data_order = array(
				'id_po_quotation' 	=> $id_po_quotation,
				'id_product' 		=> $get_request_item[0]['id_produk'],
				'qty' 				=> $get_request_item[0]['qty'],
				'price' 			=> 0,
				'diskon' 			=> 0,
				'disc_price'		=> 0,
				'amount_price' 		=> 0,
				'no_po' 			=> $arrNoPO[0],
			);
			
			
			$result_order_request	= $this->quotation_model->add_order($data_order);
				
			if($result_order_request['result'] > 0) {
				$this->log_activity->insert_activity('insert', 'Berhasil Insert Request Order');
				$result = array('success' => true, 'message' => 'Berhasil menambahkan Request Order ke database');
			}else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert List Request Order');
				$result = array('success' => false, 'message' => 'Gagal menambahkan Request Order ke database');
			}
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

  /**
	* This function is redirect to detail quotation page
	* @return Void
	*/
  public function detail($id) {
	  $detail = $this->quotation_model->detail_req($id);

	  $data = array(
			'detail' => $detail
	  );
	  
	 // print_r($data);die;

	  $this->load->view('detail_modal_view',$data);
  }

  /**
	  * This function is redirect to approval quotation page
	  * @return Void
	  */
	public function edit_approval($id) {
		$result = $this->quotation_model->detail_req($id);

		//print_r($result);die;

		$data = array(
			'detail' => $result
		);

		$this->load->view('edit_modal_approval_view',$data);
	}

	/**
	  * This function is used to change status quotation data
	  * @return Array
	  */
	public function doedit_schedule() {
		$data   = file_get_contents("php://input");
		$params = json_decode($data,true);
		// var_dump($params);die;
		$schedule = $this->quotation_model->doedit_schedule($params);

		$msg = 'Berhasil mengubah schedule po';

		$dataLen = count($params);

		$result = array(
			'success' => true,
			'message' => $msg,
			'schedule' => $schedule
		);
		for($i=0;$i<$dataLen;$i++)
			$this->log_activity->insert_activity('update', $msg. ' dengan ID Order ' .$params[$i]['id_order']);
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	  
	public function change_status() {
		
		//echo "asasa";die;
		
		$data   = file_get_contents("php://input");
		$params = json_decode($data,true);

		//print_r($params);die;
		 $msg = 'Berhasil mengubah status request';
		foreach($params as $paramsss){
			 $this->quotation_model->change_status($paramsss);
			 
			 $kk = explode('|',$data);
			 
			 $this->log_activity->insert_activity('update', $msg. ' dengan status '.$kk[1]);
		}

	   

		$result = array(
			'success' => true,
			'message' => $msg
		);
		
		

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	/**
	  * This function is redirect to edit approval quotation page
	  * @return Void
	  */
	public function detail_status($id) {
		$result = $this->quotation_model->detail_status($id);

		$data = array(
			'detail' => $result
		);

		$this->load->view('detail_modal_approval_view', $data);
	}
}