<style>
  .changed_status{cursor:pointer;text-decoration: underline;color:#96b6e8;}
  .changed_status:hover{color:#ff8c00}
</style>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Request Order</h4>
		</div>
	</div>

	<div class="row"  >
		<div class="col-sm-12" >
			<div class="card-box">
				<table id="listquotation" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No Request</th>
							<th>Customer</th>
							<th>Issue Date</th>
							<th>Part Numbers</th>
							<th>Total Part Numbers</th>
							<th>Total Items</th>
							<th>Production Time</th>
							<th>Status</th>
							<th style="width:140px;">Option</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th>No Request</th>
							<th>Customer</th>
							<th>Issue Date</th>
							<th>Part Numbers</th>
							<th>Total Part Numbers</th>
							<th>Total Items</th>
							<th>Production Time</th>
							<th>Status</th>
							<td>Option</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:1000px;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$(document).ready(function(){
		listquotation();
	});

	function listquotation(){
      $("#listquotation").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'request_order/lists/';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false,
			"destroy": true,
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip',
			"initComplete": function(){
				var element = '<div class="btn-group pull-left">';
					element += '	<a class="btn btn-primary" onClick="add_quotation()">';
					element += '		<i class="fa fa-plus"></i> Request Order Baru';
					element += '	</a>';
					element += '</div>';

				$("div.toolbar").prepend(element);
			}
		});
    }

	function add_quotation(){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('request_order/add');?>');
		$('#panel-modal  .panel-title').html('<i class="fa fa-plus"></i> Request Order Baru ');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function edit_quotation(id){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('request_order/edit/');?>'+"/"+id);
		$('#panel-modal  .panel-title').html('<i class="fa fa-pencil"></i> Edit Request Order');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function detail_quotation(id){
        $('#panel-modal').removeData('bs.modal');
        $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
        $('#panel-modal  .panel-body').load('<?php echo base_url('request_order/detail');?>'+"/"+id);
        $('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Detail Request Order');
        $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
  	}

  	function approval_quotation(id){
	    $('#panel-modal').removeData('bs.modal');
	    $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	    $('#panel-modal  .panel-body').load('<?php echo base_url('request_order/edit_approval');?>'+"/"+id);
	    $('#panel-modal  .panel-title').html('<i class="fa fa-pencil"></i> Edit Status');
	    $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function detail_status(id){
	     $('#panel-modal').removeData('bs.modal');
	     $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	     $('#panel-modal  .panel-body').load('<?php echo base_url('request_order/detail_status');?>'+"/"+id);
	     $('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Detail Status');
	     $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}
		
	function delete_quotation(id){
		swal({
			title: 'Yakin akan Menghapus ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost={
				"id" : id
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>request_order/delete_quotation",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
				   $('.panel-heading button').trigger('click');
                    listquotation();
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                    })
				}
			});
		})
	}
	
		function close_quotation(id){
		swal({
			title: 'Lanjutkan ke Quotation ?',
			text: 'Request tidak dapat dikembalikan bila sudah dilanjutkan ke Quotation !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost={
				"id" : id
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>request_order/close_quotation",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
				   $('.panel-heading button').trigger('click');
                    listquotation();
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                    })
				}
			});
		})
	}
</script>