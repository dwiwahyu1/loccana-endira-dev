<style type="text/css">
	#listadditem {
		counter-reset: rowNumber;
	}

	#listadditem tr>td:first-child {
		counter-increment: rowNumber;
	}

	#listadditem tr td:first-child::before {
		content: counter(rowNumber);
		min-width: 1em;
		margin-right: 0.5em;
	}

	.x-hidden {
		display: none;
	}
</style>

<form class="form-horizontal form-label-left" id="add_quotation" role="form" action="<?php echo base_url('request_order/add_quotation'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p style="text-align:center;">Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="issue_date">Issue Date <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<div class="input-group date">
				<input placeholder="Issue Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="issue_date" name="issue_date" autocomplete="off" required>
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="cust_id">Customer <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="cust_id" name="cust_id" style="width: 100%" required>
				<option value="" selected>-- Select Customer --</option>
				<?php foreach ($customer as $dk) { ?>
					<option value="<?php echo $dk['id']; ?>"><?php echo $dk['kode_eksternal'] . ' - ' . $dk['name_eksternal']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="term_of_pay">Term of Production <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input placeholder="Production Days" type="number" class="form-control" id="term_of_pay" name="term_of_pay" autocomplete="off" required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="pilih">PO <span class="required"><sup>*</sup></span></label>
		<div class="col-md-4 col-sm-6 col-xs-12">
			<div class="checkbox checkbox-success">
				<input type="checkbox" class="form-control" id="check_no_po" name="check_no_po"><label for="no_po">PO Sama</label>
			</div>
		</div>
		<div class="col-md-4 col-sm-6 col-xs-12 disabled" id="isi">
			<input placeholder="No PO" type="text" class="form-control" id="no_po_isi" name="no_po_check" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group" id="judul_tbl" style="display:none">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Item : </label>
	</div>

	<div class="item form-group">
		<table id="listadditem" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="display:none">
			<thead>
				<tr>
					<th style="width: 5%;">No</th>
					<th style="width: 30%;">Items</th>
					<th style="width: 25%;">Part Number</th>
					<th style="width: 10%;">Qty</th>
					<th style="width: 25%;">No PO</th>
					<th style="width: 5%;"><a id="btn_item_add" class="btn btn-primary"><i class="fa fa-plus"></i></a></th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td></td>
					<td>
						<select class="form-control select-control item-control" id="item_id1" name="item_id[]" required>
							<option value="new" selected>Part Number Baru</option>
							<!-- <?php foreach ($item as $key) { ?>
								<option value="<?php echo $key['id']; ?>"><?php echo $key['stock_code'] . ' | ' . $key['stock_name']; ?></option>
							<?php } ?> -->
						</select>
					</td>
					<td>
						<input type="text" class="form-control part-control" id="part_name1" name="part_name[]" value="">
					</td>
					<td>
						<input type="number" class="form-control qty-control" min="0" id="qty" name="qty[]" value="0">
					</td>
					<td>
						<input type="text" class="form-control no_po-control" id="no_po" name="no_po[]" value="" placeholder="No PO">
					</td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Request</button>
		</div>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	var idRowItem = 1;
	var arrItemId = [];
	var arrPartName = [];
	var arrQty = [];
	var arrNoPO = [];

	$(document).ready(function() {
		function reloadItem() {
			$('#item_id' + idRowItem)
				.find('option')
				.remove()
				.end()
				.append('<option value="new" selected>Part Number Baru</option>')
				.val('whatever');
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('request_order/get_item/'); ?>",
				dataType: 'json',
				data: JSON.stringify({
					"cust_id": $("#cust_id").val()
				}),
				contentType: 'application/json; charset=utf-8',
				success: function(r) {
					if (r.length > 0) {
						for (var i = 0; i < r.length; i++) {
							var newOption = new Option(r[i].stock_code + ' | ' + r[i].stock_name, r[i].id, false, false);
							$('#item_id' + idRowItem).append(newOption);
						}
						$('#item_id' + idRowItem).removeAttr('readonly');
					} else $('#item_id' + idRowItem).removeAttr('readonly');
				}
			});
		}
		$("#issue_date").datepicker({
			format: 'dd/M/yyyy',
			autoclose: true,
			todayHighlight: true
		});

		$("#cust_id").select2();
		$(".select-control").select2();

		$("#cust_id").on("click", function() {
			if ($("#cust_id").val() !== "") {
				$("#listadditem").show('block');
				$("#judul_tbl").show('block');
				reloadItem();
			} else {
				$("#listadditem").hide();
				$("#judul_tbl").hide();
			}
		});

		$("#item_id1").on("change", function() {
			if (this.value == 'new') {
				$('#part_name1').val('');
				$('#part_name1').removeAttr('readonly');
			} else {

				// $(this).val();

				$('#part_name1').val($("#item_id1"+' option:selected').text().split(" ")[0]);
				$('#part_name1').attr('readonly', 'readonly');
			}
		});

		$('#btn_item_add').on('click', function() {
			var get_no_po_isi = $('#no_po_isi').val();
			var checked_po = $('input:checkbox[name=check_no_po]');

			var readonly = '';
			if (checked_po.is(':checked') === true) {
				readonly = 'readonly';
				get_no_po = get_no_po_isi;

				$('input[name="no_po[]"]').each(function() {
					this.value = get_no_po_isi;
					$(this).attr('readonly', 'readonly');
				})
			} else {
				readonly = '';
				get_no_po = '';

				$('input[name="no_po[]"]').each(function() {
					this.value = $(this).val();
					$(this).removeAttr('readonly', 'readonly');
				})
			}

			idRowItem++;
			$('#listadditem tbody').append(
				'<tr id="trRowItem' + idRowItem + '">' +
				'<td></td>' +
				'<td>' +
				'<select class="form-control select-control item-control" id="item_id' + idRowItem + '" name="item_id[]" required>' +
				'<option value="new" selected >Part Number Baru</option>' +
				/*<?php foreach ($item as $key) { ?>
					<option value="<?php echo $key['id']; ?>"><?php echo $key['stock_code'] . ' | ' . $key['stock_name']; ?></option>
				<?php } ?>*/
				'</select>' +
				'</td>' +
				'<td>' +
				'<input type="text" class="form-control part-control" id="part_name' + idRowItem + '" name="part_name[]" value="">' +
				'</td>' +
				'<td>' +
				'<input type="number" class="form-control qty-control" min="0" id="qty' + idRowItem + '" name="qty[]" value="0">' +
				'</td>' +
				'<td>' +
				'<input type="text" class="form-control no_po-control" id="no_po' + idRowItem + '" name="no_po[]" ' + readonly + ' placeholder="No PO" value="' + get_no_po + '">' +
				'</td>' +
				'<td>' +
				'<a class="btn btn-danger" onclick="removeRow(' + idRowItem + ')"><i class="fa fa-minus"></i></a>' +
				'</td>' +
				'</tr>'
			);
			$("#item_id" + idRowItem).select2();
			$('#item_id' + idRowItem).attr('readonly', 'readonly');


			$('#item_id' + idRowItem).on('change', function() {
				if (this.value == 'new') {
					$('#part_name' + idRowItem).val('');
					$('#part_name' + idRowItem).removeAttr('readonly');
				} else {
					$('#part_name' + idRowItem).val($('#item_id' + idRowItem +' option:selected' ).text().split(" ")[0]);
					$('#part_name' + idRowItem).attr('readonly', 'readonly');
				}
			});
			reloadItem()
		});
	});

	function removeRow(rowItem) {
		$('#trRowItem' + rowItem).remove();
	}

	$('#no_po_isi').on('keyup', function() {
		var inputNoPO = $('#no_po_isi').val();
		$('input[name="no_po[]"]').each(function() {
			this.value = inputNoPO;
			$(this).attr('readonly', 'readonly');
		})
	});

	$('#check_no_po').on('change', function() {
		if (this.checked) {
			$('#isi').removeClass('disabled');
			$('#no_po_isi').removeAttr('readonly', 'readonly');

			$('input[name="no_po[]"]').each(function() {
				this.value = '';
				$(this).attr('readonly', 'readonly');
			})
		} else {
			$('#isi').addClass('disabled');
			$('#no_po_isi').val('');
			$('#no_po_isi').attr('readonly', 'readonly');

			$('#no_po').val('');
			$('#no_po').removeAttr('readonly', 'readonly');

			$('input[name="no_po[]"]').each(function() {
				this.value = '';
				$(this).removeAttr('readonly', 'readonly');
			})
		}
	});

	$('#add_quotation').on('submit', (function(e) {
		arrItemId = [];
		arrPartName = [];
		arrQty = [];
		arrNoPO = [];

		$('#btn-submit').attr('disabled', 'disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();

		$('select[name="item_id[]"]').each(function() {
			if (this.value) {
				if (this.value != undefined && this.value != '') arrItemId.push(this.value);
			}
		})

		$('input[name="part_name[]"]').each(function() {
			if (this.value) {
				if (this.value != undefined && this.value != '') arrPartName.push(this.value);
				else swal("Warning!", "Maaf, anda melewatkan inputan Part Name", "info");
			}
		})

		$('input[name="qty[]"]').each(function() {
			if (this.value) {
				if (this.value != undefined && this.value != '' && this.value > 0) arrQty.push(this.value);
				else swal("Warning!", "Maaf, Qty harus diisi", "info");
			}
		})

		$('input[name="no_po[]"]').each(function() {
			if (this.value) {
				if (this.value != undefined && this.value != '') arrNoPO.push(this.value);
				else swal("Warning!", "Maaf, anda melewatkan inputan No PO", "info");
			}
		})

		if (arrItemId.length > 0 && arrPartName.length > 0 && arrQty.length > 0 && arrNoPO.length > 0) {
			var formData = new FormData();
			formData.set('issue_date', $('#issue_date').val());
			formData.set('cust_id', $('#cust_id').val());
			formData.set('term_of_pay', $('#term_of_pay').val());
			formData.set('arrItemId', arrItemId);
			formData.set('arrPartName', arrPartName);
			formData.set('arrQty', arrQty);
			formData.set('arrNoPO', arrNoPO);

			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: formData,
				cache: false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function() {
							window.location.href = "<?php echo base_url('request_order'); ?>";
						})
					} else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Tambah Quotation");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Tambah Quotation");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		} else {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah Quotation");
			swal("Failed!", "Invalid Inputan Item, silahkan cek kembali.", "error");
		}
	}));
</script>