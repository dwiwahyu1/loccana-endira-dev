<form class="form-horizontal form-label-left" id="change_status" role="form" action="<?php echo base_url('request_order/change_status');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Item : </label>
    </div>

    <div class="item form-group">
		<table id="listedititem" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
				<th>No</th>
					<th>Kode</th>
					<th>Part Number</th>
					<th>Qty</th>
					<th>Approval</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$id_produkArray = array(); 
				$i=1; 
				foreach($detail as $orderkey) {
					$id_produkArray[] = $orderkey['id_produk'];
				?>
					<tr>
						<td style="padding-top: 17px;"><?php echo $i; ?></td>
	                  	<td>
	                  		<?php if(isset($orderkey['stock_code'])){ echo $orderkey['stock_code']; }?>
	                  	</td>
		                <td>
		                	<?php if(isset($orderkey['stock_name'])){ echo $orderkey['stock_name']; }?>
		                </td>
		                <td>
		                	<?php if(isset($orderkey['qty'])){ echo $orderkey['qty']; }?>
		                </td>
						  <td>
		                	<select class="form-control" id="app_<?php echo $orderkey['id_order']; ?>" name="app_<?php echo $orderkey['id_order']; ?>" style="width: 100%" required>
								<option value="" selected disabled>-- Status --</option>
								<option value="<?php echo $orderkey['id_order']; ?>|Approve" <?php if($orderkey['status'] == "Approve"){ echo "Selected"; } ?> >Approve</option>
								<option value="<?php echo $orderkey['id_order']; ?>|Reject" <?php if($orderkey['status'] == "Reject"){ echo "Selected"; } ?>>Reject</option>
								<option value="<?php echo $orderkey['id_order']; ?>|Waiting" <?php if($orderkey['status'] == "Waiting"){ echo "Selected"; } ?>>Waiting</option>
								
							</select>
		                </td>
		            </tr>
	            <?php $i++;} ?>
			</tbody>
		</table>
	</div>

	<!--<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Status 
        	<span class="required">
        		<sup>*</sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
            <select class="form-control" name="status" id="status" style="width: 100%" required>
              	<option value="5">Accept</option>
              	<option value="2">Reject</option>
            </select>
        </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Notes</label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <textarea data-parsley-maxlength="255" type="text" id="notes" name="notes" class="form-control col-md-7 col-xs-12" placeholder="Notes"></textarea>
      </div>
    </div>-->

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Status</button>
		</div>
	</div>

	<input type="hidden" id="id_quotation" value="<?php if(isset($detail['id'])){ echo $detail['id']; }?>">
	<input type="hidden" id="id_produk" value="<?php echo implode(',',$id_produkArray);?>">
</form>
<!-- /page content -->

<script type="text/javascript">
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
	});

	$('#change_status').on('submit',(function(e) {
		
		var datapost={
		<?php foreach($detail as $orderkeys) { ?>
			"app_<?php echo $orderkeys['id_order']; ?>": $('#app_<?php echo $orderkeys['id_order']; ?>').val(),
		<?php } ?>

			
		};
		
		//console.log(datapost);
			
	    $('#btn-submit').attr('disabled','disabled');
	    $('#btn-submit').text("Mengubah status...");
	    e.preventDefault();

	    $.ajax({
	        type:'POST',
	        url: $(this).attr('action'),
	        data : JSON.stringify(datapost),
	        dataType: 'json',
			contentType: 'application/json; charset=utf-8',
	        success: function(response) {
	            if (response.success == true) {
	              $('.panel-heading button').trigger('click');
	                listquotation();
	                swal({
	                  title: 'Success!',
	                  text: response.message,
	                  type: 'success',
	                  showCancelButton: false,
	                  confirmButtonText: 'Ok'
	                }).then(function () {
	                });
	            } else{
	                $('#btn-submit').removeAttr('disabled');
	                $('#btn-submit').text("Edit Status");
	                swal("Failed!", response.message, "error");
	            }
	        }
	    }).fail(function(xhr, status, message) {
	        $('#btn-submit').removeAttr('disabled');
	        $('#btn-submit').text("Edit Status");
	        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
	    });
	  }));
</script>
