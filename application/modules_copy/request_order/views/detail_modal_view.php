<form class="form-horizontal form-label-left" id="detail_quotation" role="form" action="" method="post" enctype="multipart/form-data" data-parsley-validate>


	<div class="item form-group">
        <label class="col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Item : </label>
    </div>

    <div class="item form-group">
		<table id="listedititem" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>No</th>
					<th>Kode</th>
					<th>Part Number</th>
					<th>Qty</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$i=1; 
				foreach($detail as $orderkey) {?>
					<tr>
						<td style="padding-top: 17px;"><?php echo $i; ?></td>
	                  	<td>
	                  		<?php if(isset($orderkey['stock_code'])){ echo $orderkey['stock_code']; }?>
	                  	</td>
		                <td>
		                	<?php if(isset($orderkey['stock_name'])){ echo $orderkey['stock_name']; }?>
		                </td>
		                <td>
		                	<?php if(isset($orderkey['qty'])){ echo $orderkey['qty']; }?>
		                </td>
		            </tr>
	            <?php $i++;} ?>
			</tbody>
		</table>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
	});
</script>
