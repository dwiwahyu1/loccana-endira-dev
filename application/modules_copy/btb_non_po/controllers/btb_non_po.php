<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Btb_Non_Po extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('btb_non_po/btb_non_po_model');
		$this->load->library('log_activity');
		$this->load->library('sequence');
		$this->load->library('excel');
	}

	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'btb_non_po/views/index');
	}

	function lists() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'DESC';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;
		
		$order_fields = array('a.no_btb', 'a.tanggal_btb','a.type_btb','a.no_pol');
		
		$search = $this->input->get_post('search');
		$search_val = (!empty($search['value'])) ? $search['value'] : null;
		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->btb_non_po_model->lists($params);
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$data = array();
		
		$no = 1;
		foreach ($list['data'] as $k => $v) {
			if($v['status'] == 0) {
				$status = '<span class="label label-warning">Pending</span>';
				
				$actions =
					'<div class="text-center">'.
						'<div class="btn-group">'.
							'<button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Detail PO" onClick="btb_detail(\'' . $v['id_btb_non'] . '\')">'.
								'<i class="fa fa-search"></i>'.
							'</button>'.
						'</div>'.
						'<div class="btn-group">'.
							'<a class="btn btn-warning" title="Edit BTB" onclick="btb_edit(\''.$v['id_btb_non'].'\')">'.
								'<i class="fa fa-edit"></i>'.
							'</a>'.
						'</div>'.
						'<div class="btn-group">'.
							'<button class="btn btn-danger" title="Delete" onclick="btb_delete(\''.$v['id_btb_non'].'\')">'.
								'<i class="fa fa-trash"></i>'.
							'</button>'.
						'</div>'.
						'<div class="btn-group">'.
							'<a class="btn btn-success" title="Approve BTB" onclick="btb_approve(\''.$v['id_btb_non'].'\')">'.
								'<i class="fa fa-check"></i>'.
							'</a>'.
						'</div>'.
					'</div>';
			}else if($v['status'] == 1) {
				$status = '<span class="label label-success">Approve</span>';
				
				$actions =
					'<div class="text-center">'.
						'<div class="btn-group">'.
							'<button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Detail PO" onClick="btb_detail(\'' . $v['id_btb_non'] . '\')">'.
								'<i class="fa fa-search"></i>'.
							'</button>'.
						'</div>'.
					'</div>';
			}else {
				$status = '<span class="label label-danger">Reject</span>';
				
				$actions =
					'<div class="text-center">'.
						'<div class="btn-group">'.
							'<button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Detail PO" onClick="btb_detail(\'' . $v['id_btb_non'] . '\')">'.
								'<i class="fa fa-search"></i>'.
							'</button>'.
						'</div>'.
						'<div class="btn-group">'.
							'<button class="btn btn-danger" title="Delete" onclick="btb_delete(\''.$v['id_btb_non'].'\')">'.
								'<i class="fa fa-trash"></i>'.
							'</button>'.
						'</div>'.
					'</div>';
			}
			
			array_push($data, array(
				$no,
				$v['no_btb'],
				$v['name_eksternal'],
				date('d F Y', strtotime($v['tanggal_btb'])),
				ucwords($v['type_btb']),
				$status,
				$actions
			));

			$no++;
		}

		$result["data"] = $data;
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function btb_add() {
		$eksternal 	= $this->btb_non_po_model->get_eksternal();
		$valas 		= $this->btb_non_po_model->get_valas();
		// $no_btb = $this->sequence->get_no('btb_lokal');
		
		$data = array(
			'eksternal' => $eksternal,
			'valas' 	=> $valas
			// 'no_btb' => $no_btb,
		);

		$this->load->view('btb_add_modal_view',$data);
	}

	public function btb_add_item() {
		$results = $this->btb_non_po_model->btb_list_spb('', TRUE);
		
		$data = array(
			'spb' => $results
		);
		
		$this->load->view('btb_add_item_modal_view',$data);
	}

	public function btb_insert_spb() {
		$this->form_validation->set_rules('type_btb',		'Type BTB',			'trim|required');
		$this->form_validation->set_rules('tanggal_btb',	'Tanggal BTB',		'trim|required');
		$this->form_validation->set_rules('id_cust',		'Nama Supplier',	'trim|required');
		$this->form_validation->set_rules('id_valas',		'Valas',			'trim|required');
		$this->form_validation->set_rules('rate',			'Rate',				'trim|required');
		$this->form_validation->set_rules('no_pol',			'No Polisi',		'trim|required');
		$this->form_validation->set_rules('surat_jalan',	'Surat Jalan',		'trim|required');

		if($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$results = array('success' => false, 'message' => $msg);
		}else {
			$id_cust		= $this->Anti_sql_injection($this->input->post('id_cust', TRUE));
			$type_btb		= $this->Anti_sql_injection($this->input->post('type_btb', TRUE));
			$tanggal_btb	= $this->Anti_sql_injection($this->input->post('tanggal_btb', TRUE));
			$valas_id		= $this->Anti_sql_injection($this->input->post('id_valas', TRUE));
			$rate			= $this->Anti_sql_injection($this->input->post('rate', TRUE));
			$spb_number		= $this->Anti_sql_injection($this->input->post('tambah_spb', TRUE));
			$no_pol 		= $this->Anti_sql_injection($this->input->post('no_pol', TRUE));
			$surat_jalan	= $this->Anti_sql_injection($this->input->post('surat_jalan', TRUE));
			
			if($type_btb == 'lokal') $no_btb = $this->sequence->get_max_no('btb_lokal');
			else if($type_btb == 'import') $no_btb = $this->sequence->get_max_no('btb_impor');
			else $no_btb = $this->sequence->get_max_no('btb_kb');
			
			$data = array(
				'id_cust'		=> $id_cust,
				'no_btb'		=> $no_btb,
				'tanggal_btb'	=> date('Y-m-d', strtotime($tanggal_btb)),
				'type_btb'		=> $type_btb,
				'valas_id'		=> $valas_id,
				'rate'			=> $rate,
				'surat_jalan'	=> $surat_jalan,
				'no_pol'		=> $no_pol
			);

			$results = $this->btb_non_po_model->btb_non_add($data);
			if($results > 0) {
				if($type_btb == 'lokal') $this->sequence->save_max_no('btb_lokal');
				else if($type_btb == 'import') $this->sequence->save_max_no('btb_impor');
				else $this->sequence->save_max_no('btb_kb');
				
				$msg = 'Berhasil menambahkan data BTB';

				$results = array('success' => true, 'message' => $msg, 'lastid' => $results['lastid']);
				$this->log_activity->insert_activity('insert', $msg. ' dengan No BTB ' .$no_btb);
			}else {
				$msg = 'Gagal menambahkan data BTB ke database';

				$results = array('success' => false, 'message' => $msg);
				$this->log_activity->insert_activity('insert', $msg. ' dengan No BTB ' .$no_btb);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}

	public function btb_insert_spb_detail() {
		$data   = file_get_contents("php://input");
		$params = json_decode($data,true);

		if(isset($params['list_spb']) && (is_array($params['list_spb']) || is_object($params['list_spb']))) {
			foreach($params['list_spb'] as $k => $v) {
				$arrTemp = array(
					'id_btb_non'	=> $params['id_btb'],
					'id_spb'		=> $v[0],
					'id_material'	=> $v[1],
					'qty_diterima'	=> $v[7],
					'unit_price'	=> $v[8],
					'date_mutasi'	=> date('Y-m-d H:i:s', strtotime($params['tanggal_btb'].' '.date('H:i:s')))
				);

				$results = $this->btb_non_po_model->btb_non_add_detail_spb($arrTemp);
				// $upPoSpb = $this->btb_non_po_model->btb_update_qty($arrTemp); NOT USED
				$material = $this->btb_non_po_model->btb_update_qty_material($arrTemp);
				$mutasi = $this->btb_non_po_model->btb_mutasi_add($arrTemp);
			}
			 // $this->btb_non_po_model->btb_mutasi_add_hutang($arrTemp);
		}
		
		
		if ($results > 0) {
			$msg = 'Berhasil menambahkan data SPB';

			$results = array('success' => true, 'message' => $msg);
			$this->log_activity->insert_activity('insert', $msg. ' dengan ID BTB ' .$params['id_btb']);
		}else {
			$msg = 'Gagal menambahkan data detail SPB ke database';

			$results = array('success' => false, 'message' => $msg);
			$this->log_activity->insert_activity('insert', $msg. ' dengan ID BTB ' .$params['id_btb']);
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($results);
	}

	public function btb_edit($id_btb) {
		$results = $this->btb_non_po_model->btb_non_detail($id_btb);

		$eksternal 	= $this->btb_non_po_model->get_eksternal();
		$valas 		= $this->btb_non_po_model->get_valas();

		$data = array(
			'btb'			=> $results,
			'eksternal'		=> $eksternal,
			'valas'			=> $valas
		);

		$this->load->view('btb_edit_modal_view',$data);
	}

	public function btb_edit_item() {
		$results = $this->btb_non_po_model->btb_list_spb('', TRUE);
		
		$data = array(
			'spb' => $results
		);
		
		$this->load->view('btb_edit_item_modal_view',$data);
	}

	public function btb_update_spb() {
		$this->form_validation->set_rules('type_btb',		'Type BTB',			'trim|required');
		$this->form_validation->set_rules('tanggal_btb',	'Tanggal BTB',		'trim|required');
		$this->form_validation->set_rules('id_cust',		'Nama Supplier',	'trim|required');
		$this->form_validation->set_rules('id_valas',		'Valas',			'trim|required');
		$this->form_validation->set_rules('rate',			'Rate',				'trim|required');
		$this->form_validation->set_rules('no_pol',			'No Polisi',		'trim|required');
		$this->form_validation->set_rules('surat_jalan',	'Surat Jalan',		'trim|required');

		if($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$results = array('success' => false, 'message' => $msg);
		}else {
			$id_btb_non		= $this->Anti_sql_injection($this->input->post('id_btb_non', TRUE));
			$no_btb			= $this->Anti_sql_injection($this->input->post('no_btb', TRUE));
			$id_cust		= $this->Anti_sql_injection($this->input->post('id_cust', TRUE));
			$type_btb		= $this->Anti_sql_injection($this->input->post('type_btb', TRUE));
			$tanggal_btb	= $this->Anti_sql_injection($this->input->post('tanggal_btb', TRUE));
			$valas_id		= $this->Anti_sql_injection($this->input->post('id_valas', TRUE));
			$rate			= $this->Anti_sql_injection($this->input->post('rate', TRUE));
			$spb_number		= $this->Anti_sql_injection($this->input->post('tambah_spb', TRUE));
			$no_pol 		= $this->Anti_sql_injection($this->input->post('no_pol', TRUE));
			$surat_jalan	= $this->Anti_sql_injection($this->input->post('surat_jalan', TRUE));
			
			$data = array(
				'id_btb_non'	=> $id_btb_non,
				'id_cust'		=> $id_cust,
				'no_btb'		=> $no_btb,
				'tanggal_btb'	=> date('Y-m-d', strtotime($tanggal_btb)),
				'type_btb'		=> $type_btb,
				'valas_id'		=> $valas_id,
				'rate'			=> $rate,
				'no_pol'		=> $no_pol,
				'surat_jalan'	=> $surat_jalan
			);

			$result_update = $this->btb_non_po_model->btb_non_update($data);
			$delete_detail = $this->btb_non_po_model->btb_spb_non_delete($id_btb_non);

			if($result_update > 0) $msg = 'Berhasil mengubah data BTB';
			else $msg = 'Gagal mengubah data BTB ke database';

			$results = array('success' => true);
			$this->log_activity->insert_activity('update', $msg. ' dengan No BTB ' .$no_btb);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}

	public function btb_update_spb_detail() {
		$data   = file_get_contents("php://input");
		$params = json_decode($data,true);

		if(isset($params['list_spb']) && (is_array($params['list_spb']) || is_object($params['list_spb']))) {
			foreach($params['list_spb'] as $pk => $pv) {
				$arrTemp = array(
					'id_btb_non'	=> $params['id_btb_non'],
					'id_spb'		=> $pv[1],
					'id_material'	=> $pv[2],
					'qty_diterima'	=> preg_replace("/[^0-9\.]/", "", $pv[8]),
					'unit_price'	=> preg_replace("/[^0-9\.]/", "", $pv[9]),
					'date_mutasi'	=> date('Y-m-d H:i:s', strtotime($params['tanggal_btb'].' '.date('H:i:s')))
				);

				$results = $this->btb_non_po_model->btb_non_add_detail_spb($arrTemp);
				$material = $this->btb_non_po_model->btb_update_qty_material($arrTemp);
				$mutasi = $this->btb_non_po_model->btb_mutasi_add($arrTemp);
			}
			 // $this->btb_non_po_model->btb_mutasi_add_hutang($arrTemp);
		}
		
		
		if ($results > 0) {
			$msg = 'Berhasil mengubah data SPB';

			$results = array('success' => true, 'message' => $msg);
			$this->log_activity->insert_activity('insert', $msg. ' dengan ID BTB ' .$params['id_btb_non']);
		}else {
			$msg = 'Gagal mengubah data detail SPB ke database';

			$results = array('success' => false, 'message' => $msg);
			$this->log_activity->insert_activity('insert', $msg. ' dengan ID BTB ' .$params['id_btb_non']);
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($results);
	}

	public function btb_search_spb() {
		$data   = file_get_contents("php://input");
		$params = json_decode($data, true);

		$list_spb = $this->btb_non_po_model->btb_search_spb($params['no_spb']);

		if(sizeof($list_spb) > 0){
			$data = array();
			$tempObj = new stdClass();

			if(sizeof($params['tempSPB']) > 0) {
				for ($i = 0; $i < sizeof($params['tempSPB']); $i++) {
					for ($j = 0; $j < sizeof($list_spb); $j++) {
						if(isset($list_spb[$j])) {
							if(
								((int)$list_spb[$j]['id'] == (int)$params['tempSPB'][$i][0]) && 
								(trim($list_spb[$j]['no_spb']) == trim($params['tempSPB'][$i][2]))
							) unset($list_spb[$j]);
						}
					}
				}
			}
			
			$i = 0;
			foreach ($list_spb as $k => $v) {
				$strQty =
					'<input type="number" class="form-control" id="qty_diterima'.$i.'" name="qty_diterima'.$i.'" onInput="valQty('.$i.')" style="height:25px; width: 100px;" value="0">';
				$strUnitPrice =
					'<input type="number" class="form-control" id="unit_price'.$i.'" name="unit_price'.$i.'" onInput="valQty('.$i.')" style="height:25px; width: 100px;" value="0">';
				
				$strOption =
					'<div class="checkbox">'.
						'<input id="option['.$i.']" type="checkbox" value="'.$i.'">'.
						'<label for="option['.$i.']"></label>'.
					'</div>';

				array_push($data, array(
					($i+1),
					$v['id'],
					$v['id_material'],
					$v['stock_code'],
					$v['stock_name'],
					'('.$v['uom_symbol'].') - '.$v['uom_name'],
					number_format($v['qty'], 2, ',', '.'),
					$strQty,
					$strUnitPrice,
					$strOption
				));
				$i++;
			}
			
			$res = array(
				'success'	=> true,
				'status'	=> 'success',
				'data'		=> $data
			);
		}else{
			$data = array();
			$res = array(
				'success'	=> false,
				'message'	=> 'Data SPB tidak ditemukan',
				'data'		=> $data
			);
		}
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function btb_search_edit_spb() {
		$data   = file_get_contents("php://input");
		$params = json_decode($data, true);

		$list_spb = $this->btb_non_po_model->btb_search_spb($params['no_spb']);

		if(sizeof($list_spb) > 0){
			$data = array();
			$tempObj = new stdClass();

			if(sizeof($params['tempSPB']) > 0) {
				for ($i = 0; $i < sizeof($params['tempSPB']); $i++) {
					for ($j = 0; $j < sizeof($list_spb); $j++) {
						if(isset($list_spb[$j])) {
							if(
								((int)$list_spb[$j]['id'] == (int)$params['tempSPB'][$i][1]) && 
								(trim($list_spb[$j]['no_spb']) == trim($params['tempSPB'][$i][3]))
							)unset($list_spb[$j]);
						}
					}
				}
			}
			
			$i = 0;
			foreach ($list_spb as $k => $v) {
				$strQty =
					'<input type="number" class="form-control" id="qty_diterima'.$i.'" name="qty_diterima'.$i.'" onInput="valQty('.$i.')" style="height:25px; width: 100px;" value="0">';
				$strUnitPrice =
					'<input type="number" class="form-control" id="unit_price'.$i.'" name="unit_price'.$i.'" onInput="valQty('.$i.')" style="height:25px; width: 100px;" value="0">';
				
				$strOption =
					'<div class="checkbox">'.
						'<input id="option['.$i.']" type="checkbox" value="'.$i.'">'.
						'<label for="option['.$i.']"></label>'.
					'</div>';

				array_push($data, array(
					($i+1),
					$v['id'],
					$v['id_material'],
					$v['stock_code'],
					$v['stock_name'],
					'('.$v['uom_symbol'].') - '.$v['uom_name'],
					number_format($v['qty'], 2, ',', '.'),
					$strQty,
					$strUnitPrice,
					$strOption
				));
				$i++;
			}
			
			$res = array('success' => true, 'status' => 'success', 'data' => $data);
		}else {
			$data = array();
			$res = array('success' => false, 'message' => 'Data SPB tidak ditemukan', 'data' => $data);
		}
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function get_no_spb_by() {
		$data   = file_get_contents("php://input");
		$params = json_decode($data, true);

		if($params['stock_name'] != '' && $params['stock_name'] != NULL) $group = FALSE;
		else $group = TRUE;
		$no_spb = $this->btb_non_po_model->btb_list_spb($params['stock_name'], $group);
		$this->output->set_content_type('application/json')->set_output(json_encode($no_spb));
	}
	
	public function get_lists_detail($id_btb)  {
		$list = $this->btb_non_po_model->lists_detail($id_btb);
	
		$data = array();
		
		$i = 0;
		$total_amount = 0;
		foreach ($list as $k => $v) {
			$amount = floatval($v['total_amount']);
			
			$total_amount = $total_amount + $amount;
			array_push($data, 
				array(
					$i+1,
					$v['stock_name'],
					number_format($v['qty_diterima'],2,',','.'),
					number_format($v['unit_price'], 2,',','.'),
					'',
					number_format($v['total_amount'], 2,',','.'),
					$v['no_spb'],
					$v['no_po'],
					$v['stock_code'],
					$v['notes']
				)
			);
		}

		$result["data"] = $data;
		$result["total"] = $total_amount;
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function btb_report() {
		$data = array(
			'msg' => '',
			'tanggal_awal' => '',
			'tanggal_akhir' => ''
		);
		
		$this->load->view('btb_report_modal_view',$data);
	}
	
	public function btb_non_view_report() {
		$jenis_report = $this->Anti_sql_injection($this->input->post('jenis_report', TRUE));
		$tanggalAwal = $this->Anti_sql_injection($this->input->post('tanggal_awal', TRUE));
		$tanggalAkhir = $this->Anti_sql_injection($this->input->post('tanggal_akhir', TRUE));

		$tglAwl = explode("/", $tanggalAwal);
		$tglAkhr = explode("/", $tanggalAkhir);
		$tanggal_awal = date('Y-m-d', strtotime($tglAwl[2].'-'.$tglAwl[1].'-'.$tglAwl[0]));
		$tanggal_akhir = date('Y-m-d', strtotime($tglAkhr[2].'-'.$tglAkhr[1].'-'.$tglAkhr[0]));
		
		if($jenis_report == 'lokal') {
			//LOKAL
			$lokal = array('jenis_report' => $jenis_report, 'tanggal_awal' => $tanggal_awal, 'tanggal_akhir' => $tanggal_akhir);
			$result_lokal = $this->btb_non_po_model->btb_non_view_report($lokal);
			
			$data = array(
				'report'			=> 'Bukti Terima Barang Lokal',
				'tanggal_awal'		=> date('d-F-Y', strtotime($tglAwl[2].'-'.$tglAwl[1].'-'.$tglAwl[0])),
				'tanggal_akhir'		=> date('d-F-Y', strtotime($tglAkhr[2].'-'.$tglAkhr[1].'-'.$tglAkhr[0])),
				'hasil'				=> $result_lokal
			);
			
			if(count($result_lokal) > 0) $data['msg'] = array('message' => 0, 'success' => true, 'status' => 'success');
			else $data['msg'] = array('message' => 1, 'success' => false, 'status' => 'error');
			
			$this->load->view('btb_list_report_modal_view', $data);
		}else if($jenis_report == 'import') {
			//IMPORT
			$import = array('jenis_report' => $jenis_report,'tanggal_awal' => $tanggal_awal, 'tanggal_akhir' => $tanggal_akhir);
			$result_import = $this->btb_non_po_model->btb_non_view_report($import);
			
			$data = array(
				'report'			=> 'Bukti Terima Barang Import',
				'tanggal_awal'		=> date('d-F-Y', strtotime($tglAwl[2].'-'.$tglAwl[1].'-'.$tglAwl[0])),
				'tanggal_akhir'		=> date('d-F-Y', strtotime($tglAkhr[2].'-'.$tglAkhr[1].'-'.$tglAkhr[0])),
				'hasil'				=> $result_import
			);
			
			if(count($result_import) > 0) $data['msg'] = array('message' => 0, 'success' => true, 'status' => 'success');
			else $data['msg'] = array('message' => 1, 'success' => false, 'status' => 'error');
			
			$this->load->view('btb_list_report_modal_view', $data);
		}else if($jenis_report == 'antar_kb') {
			//ANTAR KB
			$kb = array('jenis_report' => $jenis_report, 'tanggal_awal' => $tanggal_awal, 'tanggal_akhir' => $tanggal_akhir);
			$result_kb = $this->btb_non_po_model->btb_non_view_report($kb);
			
			$data = array (
				'report'			=> 'Bukti Terima Barang Antar KB',
				'tanggal_awal'		=> date('d-F-Y', strtotime($tglAwl[2].'-'.$tglAwl[1].'-'.$tglAwl[0])),
				'tanggal_akhir'		=> date('d-F-Y', strtotime($tglAkhr[2].'-'.$tglAkhr[1].'-'.$tglAkhr[0])),
				'hasil'				=> $result_kb
			);
			
			if(count($result_kb) > 0) $data['msg'] = array('message' => 0, 'success' => true, 'status' => 'success');
			else $data['msg'] = array('message' => 1, 'success' => false, 'status' => 'error');
			
			$this->load->view('btb_list_report_modal_view', $data);
		}else {
			$result = array('success' => false);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function approve($idpospb) {
		
		$results = $this->btb_non_po_model->approve_detail($idpospb);
		$result_type = $this->btb_non_po_model->type_bc('0');
		$result_type2 = $this->btb_non_po_model->list_bc_all('0');
		
		$data = array(
			'approve' => $results,
			'jenis_bc' => $result_type,
			'list_bc_all' => $result_type2
		);
		
		$this->load->view('approve_btb_modal_view',$data);
	}

	public function approve_btb() {
		$data   = file_get_contents("php://input");
		$params = json_decode($data, true);
		$title	= 'Akan mengubah status BTB Non PO';
		$get_data_id = $this->btb_non_po_model->get_id_coa($params['id_btb_non']);
		
		if(count($params) > 0) {
			if($params['status'] == 1) {
				if(sizeof($get_data_id) > 0) {
					foreach ($get_data_id as $ik) {
						$data_coa = array (
							'id_coa'		=> $ik['id_coa'],
							'id_parent'		=> 0,
							'tgl_btb'		=> $ik['tanggal_btb'],
							'valas'			=> $ik['valas_id'],
							'value'			=> $ik['unit_price'] * $ik['qty_diterima'],
							'adjusment'		=> 0,
							'type_cash'		=> 0,
							'note'			=> 'Pembelian dengan No BTB : ' .$ik['no_btb'],
							'bukti'			=> '',
							'rate'			=> $ik['rate'],
							'id_coa_temp'	=> NULL		
						);
						$add_coa = $this->btb_non_po_model->add_coa_values($data_coa);
						
						$data_status = array (
							'id_btb_non'	=> $params['id_btb_non'],
							'status'		=> $params['status']
						);
						$update_status_btb = $this->btb_non_po_model->update_status_btb_non_po($data_status);
						
						if($add_coa['result'] > 0) {
							$dataBtbCoa = array(
								'id_btb_non'	=> $params['id_btb_non'],
								'id_coa_value'	=> $add_coa['lastid']
							);

							$addBtbCoa = $this->btb_non_po_model->add_btb_non_po_coa($dataBtbCoa);
						}
						
						$date_mutasi = date('Y-m-d H:i:s');
						$this->btb_non_po_model->mutasi_add($ik, $date_mutasi);
						$this->btb_non_po_model->edit_material($ik);
					}

					$msg = 'Barang berhasil di approve dengan No BTB : ' .$get_data_id[0]['no_btb'];
					$result = array('success' => true, 'message' => $msg, 'title' => $title);
				}else {
					$msg = 'Maaf data customer dengan No BTB : ' .$get_data_id[0]['no_btb']. 'ini tidak lengkap silahkan hubungi admin untuk pengajuan approval';
					$result = array('success' => false, 'message' => $msg, 'title' => $title);
				}
			}else {
				$update_status_btb = $this->btb_non_po_model->update_status_btb_non_po($data_status);

				$msg = 'Barang berhasil di approve dengan No BTB : ' .$get_data_id[0]['no_btb'];
				$result = array('success' => true, 'message' => $msg, 'title' => $title);
			}
		}else {
			$msg = 'Barang Gagal di approve karena, Data belum lengkap silahkan cek kembali.';
			$result = array('success' => false, 'message' => $msg, 'title' => $title);
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function delete_btb_non() {
		$data   = file_get_contents("php://input");
		$params = json_decode($data, true);

		/*echo "<pre>";
		print_r($params);
		echo "</pre>";
		die;*/

		$delete_btb_non = $this->btb_non_po_model->delete_btb_non($params['id']);
		$delete_btb_spb = $this->btb_non_po_model->delete_btb_spb_by_id_btb($params['id']);

		if($delete_btb_non > 0 && $delete_btb_spb > 0) {
			$msg = 'Berhasil menghapus data BTB di database';
			$result = array('success' => true, 'message' => $msg);
		}else {
			$msg = 'Gagal menghapus data BTB di database';
			$result = array('success' => false, 'message' => $msg);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function delete_btb_spb() {
		$data   = file_get_contents("php://input");
		$params = json_decode($data, true);

		/*echo "<pre>";
		print_r($params);
		echo "</pre>";
		die;*/

		$results = $this->btb_non_po_model->delete_btb_spb($params['id']);

		if($results > 0) {
			$msg = 'Berhasil menghapus data detail BTB di database';
			$result = array('success' => true, 'message' => $msg);
		}else {
			$msg = 'Gagal menghapus data detail BTB di database';
			$result = array('success' => false, 'message' => $msg);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function btb_non_detail($id_btb)  {
		$results = $this->btb_non_po_model->btb_non_detail($id_btb);
		$tanggal = $results[0]['tanggal_btb'];
		
		$tglbtb = explode("-", $tanggal);
		$tanggal_btb = date('d-F-Y', strtotime($tglbtb[2].'-'.$tglbtb[1].'-'.$tglbtb[0]));
		
		$data = array(
			'btb' => $results
		);

		$this->load->view('btb_detail_modal_view',$data);
	}

	public function export_excel($id_btb) {
		$result_btb		= $this->btb_non_po_model->btb_non_detail($id_btb);
		$type_btb		= strtoupper($result_btb[0]['type_btb']);
		$tanggal_btb	= date('d/M/Y', strtotime($result_btb[0]['tanggal_btb']));
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getActiveSheet()->setTitle("Bukti Terima Barang");
		$objPHPExcel->getProperties()->setCreator("System")
			->setLastModifiedBy("System")
			->setTitle("Bukti Terima Barang")
			->setSubject("Laporan Bukti Terima Barang")
			->setDescription("Report Bukti Terima Barang")
			->setKeywords("Bukti Terima Barang")
			->setCategory("Report");

		$bordersAll = array(
			'borders' => array(
				'allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			)
		);

		$bordersExceptBottom = array(
			'borders' => array(
				'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			)
		);

		$bordersExceptTop = array(
			'borders' => array(
				'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
				'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			)
		);

		$bordersBottomOnly = array(
			'borders' => array(
				'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			)
		);

		$bordersLeftOnly = array(
			'borders' => array(
				'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			)
		);

		$bordersRightOnly = array(
			'borders' => array(
				'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN)
			)
		);
		
		$objPHPExcel->setActiveSheetIndex(0)
			->mergeCells('A2:I2')
			->setCellValue('A2', 'BUKTI TERIMA BARANG')
			->getStyle('A2:I2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:I2')->applyFromArray($bordersExceptBottom);

		$objPHPExcel->setActiveSheetIndex(0)
			->mergeCells('A3:I3')
			->setCellValue('A3', $type_btb)
			->getStyle('A3:I3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:I3')->applyFromArray($bordersExceptTop);

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:B4')->setCellValue('A4', 'Diterima dari Supplier');

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('C4:I4')->setCellValue('C4', ': '.trim($result_btb[0]['name_eksternal']));

		$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:B5')->setCellValue('A5', 'Kendaraan');

		$objPHPExcel->setActiveSheetIndex(0)
			->mergeCells('C5:G5')
			->setCellValue('C5', ': '.trim($result_btb[0]['no_pol']));

		$objPHPExcel->setActiveSheetIndex(0)
			->mergeCells('A6:B6')
			->setCellValue('A6', 'No. Surat Jalan/Faktur Jalan');
		$objPHPExcel->setActiveSheetIndex(0)
			->mergeCells('C6:G6')
			->setCellValue('C6', ': '.trim($result_btb[0]['surat_jalan']));

		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('H5', 'No. BTB')
			->setCellValue('I5', ': '.trim($result_btb[0]['no_btb']));

		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('H6', 'Tgl.')
			->setCellValue('I6', ': '.$tanggal_btb);

		// START HEADER TABLE
		$objPHPExcel->setActiveSheetIndex(0)
			->mergeCells('A8:A9')
			->setCellValue('A8', 'NO')
			->getStyle('A8:A9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
			->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->setActiveSheetIndex(0)
			->mergeCells('B8:B9')
			->setCellValue('B8', 'JENIS BARANG')
			->getStyle('B8:B9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
			->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->setActiveSheetIndex(0)
			->mergeCells('C8:C9')
			->setCellValue('C8', 'GUDANG QTY')
			->getStyle('C8:C9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
			->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->setActiveSheetIndex(0)
			->mergeCells('D8:F8')
			->setCellValue('D8', 'BAG PEMBELIAN')
			->getStyle('D8:F8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
			->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('D9', 'HRG SAT')
			->getStyle('B9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
			->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('E9', 'PPN/Disc')
			->getStyle('E9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
			->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('F9', 'HRG TOTAL')
			->getStyle('F9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
			->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->setActiveSheetIndex(0)
			->mergeCells('G8:G9')
			->setCellValue('G8', 'NO. PR')
			->getStyle('G8:G9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
			->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->setActiveSheetIndex(0)
			->mergeCells('H8:H9')
			->setCellValue('H8', 'CODE BARANG')
			->getStyle('H8:H9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
			->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->setActiveSheetIndex(0)
			->mergeCells('I8:I9')
			->setCellValue('I8', 'KET')
			->getStyle('I8:I9')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
			->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

		$objPHPExcel->setActiveSheetIndex(0)->getStyle('A8:I9')->applyFromArray($bordersAll);
		// END HEADER TABLE

		// START BODY TABLE
		$i = 10;
		$no = 1;
		$totalHrg = 0;
		foreach ($result_btb as $k => $v) {
			$totalHrgRow = floatval($v['unit_price']) * floatval($v['qty']);
			$totalHrg = $totalHrg + $totalHrgRow;

			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$i, $no)
				->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('B'.$i, $v['stock_name'])
				->getStyle('B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('C'.$i, $v['qty'])
				->getStyle('C'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('D'.$i, number_format($v['unit_price']), 2)
				->getStyle('D'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('E'.$i, '')
				->getStyle('E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('F'.$i, number_format($totalHrgRow, 2, ',', '.'))
				->getStyle('F'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('G'.$i, $v['no_spb'])
				->getStyle('G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('H'.$i, $v['stock_code'])
				->getStyle('H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('I'.$i, $v['desc'])
				->getStyle('I'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$i.':I'.$i)->applyFromArray($bordersAll);

			$no++;
			$i++;
		}
		// END BODY TABLE

		$i++;
		$objPHPExcel->setActiveSheetIndex(0)
			->mergeCells('A'.$i.':B'.$i)
			->setCellValue('A'.$i, 'Received by.');
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$i.':B'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('A'.$i.':B'.$i)->applyFromArray($bordersAll);

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('G'.$i, 'Prepared by.');
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('G'.$i)->applyFromArray($bordersAll);

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('H'.$i, 'Approved by.');
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('H'.$i)->applyFromArray($bordersAll);

		$objPHPExcel->setActiveSheetIndex(0)->setCellValue('I'.$i, 'Approved by.');
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('I'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->setActiveSheetIndex(0)->getStyle('I'.$i)->applyFromArray($bordersAll);

		$i++;
		$objPHPExcel->setActiveSheetIndex(0)
			->mergeCells('A'.$i.':B'.($i+2))
			->getStyle('A'.$i.':B'.($i+2))->applyFromArray($bordersAll);

		$objPHPExcel->setActiveSheetIndex(0)
			->mergeCells('G'.$i.':G'.($i+2))
			->getStyle('G'.$i.':G'.($i+2))->applyFromArray($bordersAll);

		$objPHPExcel->setActiveSheetIndex(0)
			->mergeCells('H'.$i.':H'.($i+2))
			->getStyle('H'.$i.':H'.($i+2))->applyFromArray($bordersAll);

		$objPHPExcel->setActiveSheetIndex(0)
			->mergeCells('I'.$i.':I'.($i+2))
			->getStyle('I'.$i.':I'.($i+2))->applyFromArray($bordersAll);

		$objPHPExcel->setActiveSheetIndex(0)
			->getStyle('A2:A'.($i+2))->applyFromArray($bordersLeftOnly);
		$objPHPExcel->setActiveSheetIndex(0)
			->getStyle('C'.($i+2).':F'.($i+2))->applyFromArray($bordersBottomOnly);
		$objPHPExcel->setActiveSheetIndex(0)
			->getStyle('I2:I'.($i+2))->applyFromArray($bordersRightOnly);

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="BTB Report_'.$result_btb[0]['no_btb'].'.xls"');
		
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		exit;
	}
}