<style type="text/css">
	.dt-body-left thead tr th{text-align: left; vertical-align: middle;}
	.dt-body-center thead tr th{text-align: center; vertical-align: middle;}
	.dt-body-right thead tr th{text-align: right; vertical-align: middle;}
	.dt-body-left tbody tr th{text-align: left; vertical-align: middle;}
	.dt-body-center tbody tr th{text-align: center; vertical-align: middle;}
	.dt-body-right tbody tr th{text-align: right; vertical-align: middle;}
	.dt-body-left tfoot tr th{text-align: left; vertical-align: middle;}
	.dt-body-center tfoot tr th{text-align: center; vertical-align: middle;}
	.dt-body-right tfoot tr th{text-align: right; vertical-align: middle;}

	.dt-body-left thead tr td{text-align: left; vertical-align: middle;}
	.dt-body-center thead tr td{text-align: center; vertical-align: middle;}
	.dt-body-right thead tr td{text-align: right; vertical-align: middle;}
	.dt-body-left tbody tr td{text-align: left; vertical-align: middle;}
	.dt-body-center tbody tr td{text-align: center; vertical-align: middle;}
	.dt-body-right tbody tr td{text-align: right; vertical-align: middle;}
	.dt-body-left tfoot tr td{text-align: left; vertical-align: middle;}
	.dt-body-center tfoot tr td{text-align: center; vertical-align: middle;}
	.dt-body-right tfoot tr td{text-align: right; vertical-align: middle;}

	.dt-noBorder thead tr td { border: 0px; }
	.dt-noBorder thead tr th { border: 0px; }
	.dt-noBorder tbody tr td { border: 0px; }
	.dt-noBorder tbody tr th { border: 0px; }

	img {
		width	: 60%;
		height	: auto;
	}

	.border-packing{
		border: 1px solid #ebeff2;
	}
</style>

<div class="row">
	<div class="col-md-12" style="position: absolute; padding-top: 10px; padding-right: 70px; z-index: 1;">
		<div class="pull-right">
			<a class="btn btn-icon waves-effect waves-light btn-success m-b-5" data-toggle="tooltip" data-placement="top" title="Export to Excel" id="export_excel">
				<i class="fa fa-file-excel-o"></i>
			</a>
			<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download PDF" id="btn_export_pdf">
				<i class="fa fa-download"></i>
			</a>
		</div>
	</div>
</div>

<div class="row" style="border: 1px solid #ebeff2;">
	<div class="col-md-2 text-center" style="margin-top: 5px; display: none;">
		<img src="<?php echo site_url(); ?>assets/images/logo-celebit.jpg" alt="logo-celebit">
	</div>

	<div class="col-md-10" style="margin-top: 10px; display: none;">
		<h1 id="titleCelebit">CELEBIT</h1>
		<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
		<h4 id="titleAlamat">BANDUNG FACTORY : JL.BUAH DUA RT.01/RW.04 RANCAEKEK - BANDUNG-INDONESIA</h4>
		<h4 id="titleTlp">TEL 62-22-7798 561/7798 542, FAX : 62-22-7798 562 E-MAIL : celebit@celebit.id</h4>
	</div>

	<div class="col-md-12 text-center" style="border-top: 1px solid #ebeff2; border-bottom: 1px solid #ebeff2; margin-bottom: 15px;">
		<h3 id="titleBuktiTerima">BUKTI TERIMA BARANG</h3>
		<h4 id="titleTypeBtb"><?php if(isset($btb[0]['type_btb'])){echo strtoupper($btb[0]['type_btb']);}?></h4>
	</div>

	<table id="header_spb" class="table dt-noBorder dt-responsive nowrap" cellspacing="0" width="100%">
		<tr>
			<th class="dt-body-left" style="width: 12%;">Diterima dari Supplier</th>
			<td class="dt-body-center" style="vertical-align: middle; width: 1%;">:</td>
			<td colspan="4"><label style="font-weight: normal;" id="name_eksternal"><?php if(isset($btb[0]['name_eksternal'])) echo $btb[0]['name_eksternal']; ?></label></td>
		</tr>
		<tr>
			<th class="dt-body-left" style="idth: 12%;">Kendaraan</th>
			<td class="dt-body-center" style="width: 1%;">:</td>
			<td style="width: 70%;"><label style="font-weight: normal;" id="no_pol"><?php if(isset($btb[0]['no_pol'])) echo $btb[0]['no_pol']; ?></label></td>
			<td class="dt-body-left" style="width: 5%;">No. BTB</td>
			<td class="dt-body-center" style="width: 1%;">:</td>
			<td style="width: 20%;"><label style="font-weight: normal;" id="no_btb"><?php if(isset($btb[0]['no_btb'])) echo $btb[0]['no_btb']; ?></label></td>
		</tr>
		<tr>
			<th class="dt-body-left" style="width: 12%;">No. Surat Jalan/ Faktur Supplier</th>
			<td class="dt-body-center" style="width: 1%;">:</td>
			<td style="width: 70%;"><label style="font-weight: normal;" id="surat_jalan"><?php if(isset($btb[0]['surat_jalan'])) echo $btb[0]['surat_jalan']; ?></label></td>
			<td class="dt-body-left" style="width: 5%;">Tgl</td>
			<td class="dt-body-center" style="width: 1%;">:</td>
			<td style="width: 20%;"><label style="font-weight: normal;"><?php if(isset($btb[0]['tanggal_btb'])) echo $btb[0]['tanggal_btb']; ?></label></td>
		</tr>
	</table>

	<table id="list_spb" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		<thead>
			<tr>
				<th class="dt-body-center" rowspan="2">NO</th>
				<th class="dt-body-center" rowspan="2">JENIS BARANG</th>
				<th class="dt-body-center" rowspan="2">QTY DITERIMA</th>
				<th class="dt-body-center" colspan="3">BAG PEMBELIAN</th>
				<th class="dt-body-center" rowspan="2">NO PR</th>
				<th class="dt-body-center" rowspan="2">CODE BARANG</th>
				<th class="dt-body-center" rowspan="2">KET</th>
			</tr>
			<tr>
				<th class="dt-body-center">HRG SAT</th>
				<th class="dt-body-center">PPN/Disc</th>
				<th class="dt-body-center">HRG TOTAL</th>
			</tr>
		</thead>
		<tbody>
<?php
	if(isset($btb) && sizeof($btb) > 0) {
		$no = 1;
		$hrgTotal = 0;
		foreach ($btb as $k => $v) {
			$hrgSatTotal = floatval($v['qty_diterima']) * floatval($v['unit_price']);
			$hrgTotal = $hrgTotal + $hrgSatTotal;
			?>
			<tr>
				<td class="dt-body-center"><?php echo $no; ?></td>
				<td class="dt-body-left"><?php echo trim($v['stock_name']); ?></td>
				<td class="dt-body-center"><?php echo number_format($v['qty_diterima'], 2, ',', '.'); ?></td>
				<td class="dt-body-right"><?php echo number_format($v['unit_price'], 2, ',', '.'); ?></td>
				<td class="dt-body-center"></td>
				<td class="dt-body-right"><?php echo number_format($hrgSatTotal, 2, ',', '.'); ?></td>
				<td class="dt-body-center"><?php echo trim($v['no_spb']); ?></td>
				<td class="dt-body-center"><?php echo trim($v['stock_code']); ?></td>
				<td class="dt-body-left"><?php echo trim($v['desc']); ?></td>
			</tr>
<?php
			$no++;
		}
	}
?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3"></td>
				<th class="dt-body-center">TOTAL</th>
				<td></td>
				<th class="dt-body-right" id="footTotal"><?php echo number_format($hrgTotal, 4, ',', '.'); ?></th>
				<td colspan="3"></td>
			</tr>
		</tfoot>
	</table>

	<table id="footer_spb" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-top: 50px;">
		<thead>
			<tr>
				<th class="text-center">CATATAN :</th>
				<th class="text-center">KEUANGAN</th>
				<th class="text-center">Diperiksa Oleh,</th>
				<th class="text-center" colspan="2">GUDANG</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="text-center" rowspan="4"></td>
				<td class="hilang-border-bawah" rowspan="">No. BKK : </td>
				<td class="text-center hilang-border-bawah" rowspan="3"></td>
				<td class="text-center lebar-bawah" rowspan="4">KA. GUDANG TGL</td>
				<td class="text-center lebar-bawah" rowspan="4">PENERIMA TGL</td>
			</tr>
			<tr>
				<td class="hilang-border-bawah" rowspan="">BANK-GB/CH : </td>
			</tr>
			<tr>
				<td class="hilang-border-bawah" rowspan="">TGL : </td>
			</tr>
			<tr>
				<td class="" rowspan="">JML : </td>
				<td class="text-center" rowspan="">PEMBELIAN</td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td class="" colspan="5">Note :   White : Fin, Pink : Acct, Yellow : Purch, Green : Exim, Blue : Store</td>
			</tr>
		</tfoot>
	</table>

	<div class="col-md-12 text-right" style="margin-top: 10px; margin-bottom: 10px;">
		<label id="kode_report">F_MCI-002</label>
		<br/>
		<label id="kode_report_detail">REV : 00</label>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
	});

	function toDataURL(url, callback) {
		var xhr = new XMLHttpRequest();
		xhr.onload = function() {
			var reader = new FileReader();
			reader.onloadend = function() {
				callback(reader.result);
			}
			reader.readAsDataURL(xhr.response);
		};
		xhr.open('GET', url);
		xhr.responseType = 'blob';
		xhr.send();
	}

	toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
		dataImage = dataUrl;
	})

	$('#btn_export_pdf').on('click', function() {
		var doc = new jsPDF("a4");
		var imgData = dataImage;
		var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
		var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

		doc.setFontSize(12);
		doc.setFontStyle('helvetica','arial','sans-serif','bold');
		doc.text($('#titleBuktiTerima').html(), 105, 10, 'center');
		doc.setFontSize(10);
		doc.text($('#titleTypeBtb').html(), 105, 15, 'center');
		doc.line(pageWidth - 5, 20, 5, 20);

		doc.setFontSize(10);
		doc.text('Diterima dari Supplier', 5, 25, 'left');
		doc.text(': '+$('#name_eksternal').html(), 55, 25, 'left');
		doc.text('Kendaraan', 5, 30, 'left');
		doc.text(': '+$('#no_pol').html(), 55, 30, 'left');
		doc.text('No. Surat Jalan/ Faktur Supplier', 5, 35, 'left');
		doc.text(': '+$('#surat_jalan').html(), 55, 35, 'left');

		doc.text('No. BTB', pageWidth - 50, 30, 'left');
		doc.text(': '+$('#no_btb').html(), pageWidth - 35, 30, 'left');
		doc.text('TGL', pageWidth - 50, 35, 'left');
		doc.text(': '+$('#tanggal_btb').html(), pageWidth - 35, 35, 'left');

		doc.autoTable({
			html 			: '#list_spb',
			theme			: 'plain',
			styles			: {
				fontSize 	: 8, 
				lineColor	: [116, 119, 122],
				lineWidth	: 0.1,
				cellWidth 	: 'auto',
				
			},
			margin 			: 4.5,
			tableWidth		: (pageWidth - 10),
			headStyles		: {
				valign		: 'middle', 
				halign		: 'center',
			},
			columns 		: [0,1,2,3,4,5,6,7,8],
			/*didParseCell	: function (data) {
				if(data.table.foot[0]) {
					if(data.table.foot[0].cells[3]) {
						data.table.foot[0].cells[3].styles.halign = 'right';
					}
					if(data.table.foot[0].cells[4]) {
						data.table.foot[0].cells[4].styles.balign = 'right';
					}
					if(data.table.foot[0].cells[5]) {
						data.table.foot[0].cells[5].styles.balign = 'right';
					}
				}
			},*/
			columnStyles	: {
				0: {halign: 'center'},
				1: {halign: 'center'},
				2: {halign: 'center'},
				3: {halign: 'center'},
				4: {halign: 'center'},
				5: {halign: 'center'},
				6: {halign: 'center'},
				7: {halign: 'center'},
				8: {halign: 'center'},
			},
			rowPageBreak	: 'auto',
			showHead 		: 'firstPage',
			showFoot		: 'lastPage',
			startY			: 40
		});

		var lastTable = doc.autoTable.previous.finalY;

		doc.autoTable({
			html 			: '#footer_spb',
			theme			: 'plain',
			styles			: {
				fontSize 	: 8, 
				lineColor	: [116, 119, 122],
				lineWidth	: 0.1,
				cellWidth 	: 'auto',
				
			},
			margin 			: 4.5,
			tableWidth		: (pageWidth - 10),
			headStyles		: {
				valign		: 'middle', 
				halign		: 'center',
			},
			columns 		: [0,1,2,3,4],
			columnStyles	: {
				0: {halign: 'left'},
				1: {halign: 'left'},
				2: {halign: 'center'},
				3: {halign: 'center'},
				4: {halign: 'center'}
			},
			rowPageBreak	: 'auto',
			showHead 		: 'firstPage',
			showFoot		: 'lastPage',
			startY			: lastTable + 10
		});

		lastTable = doc.autoTable.previous.finalY;

		doc.text($('#kode_report').html(), pageWidth - 5, lastTable + 5, 'right');
		doc.text($('#kode_report_detail').html(), pageWidth - 5, lastTable + 10, 'right');
		
		doc.save('BTB Report_<?php if(isset($btb[0]['no_btb'])) echo $btb[0]['no_btb'];?>.pdf');
	})

	$('#export_excel').on('click', function() {
		window.open("<?php echo base_url().'btb_non_po/export_excel/'.$btb[0]['id_btb_non'];?>");
	})
</script>