<style type="text/css">
	#loading-us{display:none}
	#tick{display:none}

	#loading-mail{display:none}
	#cross{display:none}
	.add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
	.add_item:hover{color:#ff8c00}
	.right-text{text-align:right}
	.dt-body-center{text-align: center;}
	.dt-body-left{text-align: left;}
	.dt-body-right{text-align: right;}
</style>

<form class="form-horizontal form-label-left" id="btb_add" role="form" action="<?php echo base_url('btb_non_po/btb_insert_spb');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
    <div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_btb">Type BTB <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="type_btb" id="type_btb" style="width: 100%" required>
				<option value="" selected>Select Type BTB</option>
				<option value="lokal">Lokal</option>
				<option value="import">Import</option>
				<option value="antar_kb">Antar KB</option>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_btb">Tanggal BTB <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<div class="input-group date">
				<input type="text" class="form-control col-md-7 col-xs-12 datepicker" id="tanggal_btb" name="tanggal_btb" value="<?php echo date('d-M-Y'); ?>" autocomplete="off" placeholder="<?php echo date('d-M-Y'); ?>" required>
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_cust">Nama Supplier <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="id_cust" name="id_cust" required>
				<option value="" selected>Select Supplier</option>
				<?php
					if(isset($eksternal) && sizeof($eksternal) > 0) {
						foreach ($eksternal as $ek => $ev) { ?>
							<option value="<?php echo $ev['id']; ?>"><?php echo trim($ev['name_eksternal']); ?></option>
				<?php
						}
					}
				?>
			</select>
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="id_valas">Valas <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="id_valas" name="id_valas" required>
				<option value="" selected>... Pilih Valas ...</option>
				<?php foreach($valas as $valas => $v) { ?>
					<option value="<?php echo $v['valas_id']; ?>"><?php echo $v['nama_valas'].' ('.$v['symbol_valas'].')'; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="rate">Rate <span class="required"><sup></sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input placeholder="1" type="text" class="form-control" id="rate" name="rate" style="width: 100%" value="1" style="text-align:right;">
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_btb">No Polisi <span class="required"><sup></sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input placeholder="" type="text" class="form-control" id="no_pol" name="no_pol" style="width: 100%">
		</div>
	</div>
	
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_btb">Surat Jalan <span class="required"><sup></sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input placeholder="" type="text" class="form-control" id="surat_jalan" name="surat_jalan" style="width: 100%">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar SPB : </label>
		<div class="col-md-8 col-sm-6 col-xs-12 add_item">
			<a class="btn btn-primary" style="margin-top: -5px;" id="btn_add_item">
				<i class="fa fa-plus"></i> Tambah SPB
			</a> 
			<input type="hidden" id="tambah_spb" name="tambah_spb" value="0">
		</div>
	</div>
	
	<div class="item form-group">
		<div style="overflow-x:auto;">
			<table id="listAddSPB" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>ID SPB</th>
						<th>ID Material</th>
						<th class="custom-tables align-text" style="width: 8%;">No SPB</th>
						<th class="custom-tables align-text" style="width: 8%;">Kode Barang</th>
						<th class="custom-tables align-text">Nama Barang</th>
						<th class="custom-tables align-text" style="width: 8%;">Satuan Barang</th>
						<th class="custom-tables align-text" style="width: 5%;">Qty</th>
						<th class="custom-tables align-text" style="width: 8%;">Qty Diterima</th>
						<th class="custom-tables align-text" style="width: 8%;">Harga Unit</th>
						<th class="custom-tables align-text">Ket</th>
						<th class="custom-tables align-text" style="width: 5%;">Actions</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>

	<hr>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Simpan</button>
		</div>
	</div>
</form>
	
<script type="text/javascript">
	var dataSPB = [];
	var t_addSPB;
	
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();

		$('#id_cust').select2();
		
		$('#tanggal_btb').datepicker({
			format: "dd-M-yyyy",
			autoclose: true,
			todayHighlight: true,
			changeYear: true,
			minDate: '-3M',
			maxDate: '+30D',
		});

		$('#btn_add_item').on('click', function() {
			btb_add_item();
		})
		
		$("#type_btb").select2();
		dtsSPB();
	});
	
	function dtsSPB() {
		t_addSPB = $('#listAddSPB').DataTable( {
			"processing": true,
			"searching": false,
			"paging": false,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0, 1],
				"visible": false,
				"searchable": false
			}, {
				"targets": [2, 3, 8],
				"className": 'dt-body-center',
				"width": 90
			}, {
				"targets": [5, 7],
				"className": 'dt-body-center',
				"width": 115
			}, {
				"targets": [6],
				"className": 'dt-body-center',
				"width": 60
			}, {
				"targets": [10],
				"className": 'dt-body-center'
			}]
		});
	}

	$('#listaddPO').on("click", "button", function(){
		t_addSPB.row($(this).parents('tr')).remove().draw(false);
		for (var i = 0; i < t_addSPB.rows().data().length; i++) {
			var rowData = t_addSPB.row(i).data();
		}
	});

	function del_item(valThis) {
		var row = t_addSPB.row($(valThis).parents('tr')).remove().draw(false);
	}
	
	function redrawTable(row) {
		var rowData = t_addSPB.row(row).data();
		rowData[8] = $('#qty_diterima'+row).val();
		t_addSPB.draw();
	}
	
	$('#btb_add').on('submit',(function(e) {
		$('#tambah_spb').val(t_addSPB.rows().data().length);
		var listSPB = $('#tambah_spb').val();

		if(parseInt(listSPB) > 0) {
			$('#btn-submit').attr('disabled','disabled');
			$('#btn-submit').text("Memasukkan data...");
			e.preventDefault();
			var formData = new FormData(this);
			
			$.ajax({
				type:'POST',
				url: $(this).attr('action'),
				data:formData,
				cache:false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						save_PO(response.lastid);
					} else{
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Simpan");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Simpan");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}else swal("Failed!", "Maaf Daftar SPB belum ada", "error");

		return false;
	}));
	
	function save_PO(lastid) {
		var arrTemp = [];
		for (var i = 0; i < t_addSPB.rows().data().length; i++) {
			var rowData = t_addSPB.row(i).data();
			arrTemp.push(rowData);
		}

		var datapost = {
			'id_btb'		: lastid,
			'tanggal_btb'	: $('#tanggal_btb').val(),
			'list_spb'		: arrTemp
		};
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>btb_non_po/btb_insert_spb_detail",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						$('#panel-modal').modal('toggle');
						listbtb();
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Simpan");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Simpan");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}
	
	function isNumber(evt) {
		var iKeyCode = (evt.which) ? evt.which : evt.keyCode
		if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57)) return false;

		return true;
	}
	</script>