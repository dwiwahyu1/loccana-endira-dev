<style>
	#loading-us{display:none}
	#tick{display:none}

	#loading-mail{display:none}
	#cross{display:none}
	.add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
	.add_item:hover{color:#ff8c00}
	.right-text{text-align:right}
	.form-item{margin-top: 15px;overflow: auto;}
	.dt-body-center{text-align: center;}
	.dt-body-left{text-align: left;}
	.dt-body-right{text-align: right;}
</style>

<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
<br>
<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="stock_name">Nama Barang</label>wasd
	<div class="col-md-8 col-sm-6 col-xs-12">
		<input type="text" class="form-control" id="stock_name" name="stock_name" placeholder="Nama Barang" autocomplete="off">
	</div>
</div>
<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_spb">Nomor SPB<span class="required"><sup>*</sup></span></label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<select class="form-control" name="no_spb" id="no_spb" style="width: 100%" required>
			<option value="" selected>Select Nomor SPB</option>
			<?php
				if(isset($spb) && sizeof($spb) > 0) {
					foreach ($spb as $sk => $sv) { ?>
						<option value="<?php echo $sv['id']; ?>"><?php echo $sv['no_spb']; ?></option>
			<?php
					}
				}
			?>
		</select>
	</div>
</div>
		
<div class="item form-group form-item">
	<div style="overflow-x:auto;">
		<table id="listSPB" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th class="dt-body-center" style="width: 5%;">No.</th>
					<th>ID SPB</th>
					<th>ID MATERIAL</th>
					<th class="dt-body-center">Kode Barang</th>
					<th class="dt-body-center">Nama Barang</th>
					<th class="dt-body-center" style="width: 95px;">Satuan Barang</th>
					<th class="dt-body-center" style="width: 5%;">Qty</th>
					<th class="dt-body-center" style="width: 12%;">Qty Diterima</th>
					<th class="dt-body-center" style="width: 12%;">Harga Unit</th>
					<th class="dt-body-center" style="width: 5%;">Action</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>

<hr>
<div class="item form-group form-item">
	<label class="control-label col-md-3 col-sm-3 col-xs-12" for="btn_submit_simpan"></label>
	<div class="col-md-8 col-sm-6 col-xs-12">
		<a id="btn_item_simpan" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Simpan</a>
	</div>
</div>
	
<script type="text/javascript">
	var t_addItemSPB;

	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
	
		resetDt();
		$("#no_spb").select2();

		$('#no_spb').on('change', function() {
			if(this.value != '') {
				var arrNoSpb = $('#no_spb option:selected').text().trim().split('-');
				var no_spb = arrNoSpb[0].trim();
				var id_spb = this.value;
				getSPB(this.value, no_spb);
			}else t_addItemSPB.clear().draw();
		})

		$('#stock_name').on('keyup', function() {
			set_no_spb(this.value);
		})
	});
	
	function resetDt() {
		t_addItemSPB = $('#listSPB').DataTable( {
			"processing": true,
			"searching": false,
			"paging": false,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets" : [0, 3, 5, 6, 9],
				"className": 'dt-body-center'
			}, {
				"targets" : [1, 2],
				"visible" : false,
				"searchable" : false
			}]
		});
	}

	function set_no_spb(val_stock) {
		$('#no_spb').find('option').not(':first').remove();
		$('#no_spb option:first-child').trigger('change');
		$('#no_spb').prop('disabled', true);

		var datapost = {
			'stock_name' : val_stock
		}

		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>btb_non_po/get_no_spb_by",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				$('#no_spb').find('option').not(':first').remove();
				if(r.length > 0) {
					for (var i = 0; i < r.length; i++) {
						if($('#stock_name').val() != '' && $('#stock_name').val() != null) {
							var newOption = new Option(r[i].no_spb+' - '+r[i].stock_name, r[i].id, false, false);
						}else {
							var newOption = new Option(r[i].no_spb, r[i].id, false, false);
						}
						$('#no_spb').append(newOption);
					}
					$('#no_spb').removeAttr('disabled');
				}else $('#no_spb').removeAttr('disabled');
			}
		});
	}

	function getSPB(id_spb, no_spb) {
		var tempArr = [];
		if(t_addSPB.rows().data().length > 0) {
			for (var i = 0; i < t_addSPB.rows().data().length; i++) {
				tempArr.push(t_addSPB.rows().data()[i]);
			}
		}

		var datapost = {
			'id_spb'	: id_spb,
			'no_spb'	: no_spb,
			'tempSPB'	: tempArr
		}

		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>btb_non_po/btb_search_edit_spb",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.success == true) {
					t_addItemSPB.clear().draw();
					for (var i = 0; i < r.data.length; i++) {
						t_addItemSPB.row.add(r.data[i]).draw();
					}
				}else{
					t_addItemSPB.clear().draw();
					swal("Perhatian!", r.message, "info");
				}
			}
		});
	}
	
	function valQty(row) {
		var rowData = t_addItemSPB.row(row).data();
		var qty = rowData[7];
		var qty_diterima = $('#qty_diterima'+row).val();
		var hargaUnit = qty_diterima * rowData[10];
		
		/*if(qty_diterima > qty) {
			swal('Warning', 'Data qty diterima tidak boleh lebih besar dari qty yang telah ditentukan', 'info');
		}*/
	}
	
	$('#btn_item_simpan').on('click', function() {
		var tempArr = [];
		var statusBtn = 0;
		
		$('input[type=checkbox]').each(function () {
			if(this.checked == true) {
				var row = parseInt($(this).val());
				var rowData = t_addItemSPB.row(row).data();

				var qtyDiterima 	= $('#qty_diterima'+row).val();
				var unitPrice		= $('#unit_price'+row).val();
				var option =
					'<a class="btn btn-icon waves-effect waves-light btn-danger" onclick="del_item(this)">'+
						'<i class="fa fa-trash"></i>'+
					'</a>';

				if((qtyDiterima != '' && qtyDiterima != 0) && (unitPrice != '' && unitPrice != 0)) {
					var arrRow = [
						'new',
						rowData[1],
						rowData[2],
						$('#no_spb option:selected').text(),
						rowData[3],
						rowData[4],
						rowData[5],
						rowData[6],
						qtyDiterima,
						unitPrice,
						'',
						option
					];

					tempArr.push(arrRow);
					arrRow = [];
					statusBtn = 1;
				}else if(qtyDiterima == 0) {
					swal('Warning', 'Data Qty Diterima kolom ke-'+ (row+8) +' tidak boleh kosong');
					statusBtn = 0;
				}else if(unitPrice == 0) {
					swal('Warning', 'Data Harga Unit kolom ke-'+ (row+8) +' tidak boleh kosong');
					statusBtn = 0;
				}
			}
		});
	
	
		if(statusBtn == 1) {
			for (var i = 0; i < tempArr.length; i++) {
				tempArr[i][8] = formatCurrencyComaNull(tempArr[i][8]);
				t_addSPB.row.add(tempArr[i]).draw();
			}
			$('#panel-modal-detail').modal('toggle');
		}else if(tempArr.length == 0) swal('Warning', 'Belum ada Data yang dipilih!');
	})
</script>