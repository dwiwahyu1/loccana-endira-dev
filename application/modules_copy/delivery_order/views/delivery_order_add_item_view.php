	<style>
		#loading-us{display:none}
		#tick{display:none}

		#loading-mail{display:none}
		#cross{display:none}
		.add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
		.add_item:hover{color:#ff8c00}
		.right-text{text-align:right}
		.form-item{margin-top: 15px;overflow: auto;}
	</style>

	
  	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<br>
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_po">Nomor STBJ<span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="no_po" id="no_po" style="width: 100%" onchange="getSTBJ();" required autocomplete="off">
				<option value="">Select Nomor STBJ</option>
				<?php foreach($stbj as $st => $s) { ?>
					<option value="<?php echo $s['id_stbjorder']; ?>" ><?php echo $s['no_stbj'].' - '.$s['no_po']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	
	<div class="item form-group form-item">
		<table id="liststbj" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th></th>
					<th>No STBJ</th>
					<th>Tanggal STBJ</th>
					<th>Part No.</th>
					<th>Detail Box</th>
					<th>No PI</th>
					<th>Remark</th>
					<th>Pilih</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>

	<hr>
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="btn_submit_simpan"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn_submit_simpan" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Simpan</button>
		</div>
	</div>
	
	<script type="text/javascript">
	var table_stbj;
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
		
		$('#tanggal_do').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true,
			changeYear: true,
			minDate: '-3M',
			maxDate: '+30D',
		});
		resetDt();
		$("#no_po").select2();
	});
	
	function resetDt() {
		table_stbj = $('#liststbj').DataTable( {
			"processing": true,
			"searching": false,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0],
				"visible": false,
				"searchable": false
			}]
		});
	}

	function getSTBJ() {
		var value_no = $("#no_po").val();
		if(value_no != '') {
			sendNo($("#no_po option:selected").text()+','+value_no);
		}else {
			table_stbj.clear();
			table_stbj.destroy();
			resetDt();
		}
	}
	
	function sendNo(id_stbj) {
		var tempSTBJ = [];
		if(t_addSTBJ.rows().data().length > 0) {
			for (var i = 0; i < t_addSTBJ.rows().data().length; i++) {
				tempSTBJ.push(t_addSTBJ.rows().data()[i]);
			}
		}
		var datapost = {
			"stbj" : id_stbj,
			"tempSTBJ" : tempSTBJ
		};
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>delivery_order/delivery_stbj_search_id",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				table_stbj.clear();
				table_stbj.destroy();
				table_stbj = $('#liststbj').DataTable( {
					"processing": true,
					"searching": false,
					"responsive": true,
					"lengthChange": false,
					"info": false,
					"bSort": false,
					"data": r.data,
					"columnDefs": [{
						"targets": [0],
						"visible": false,
						"searchable": false
					}]
				});
			}
		});
	}
	
	$('#btn_submit_simpan').click(function() {
		var tempArr = [];
		var statusBtn = 0;

		$('input[type=checkbox]').each(function () {
			if(this.checked == true) {
				var row = parseInt($(this).val());
				var rowData = table_stbj.row(row).data();
				var noStbj = $('#no_stbj' + row).val();
				var tanggalStbj = $('#tanggal_stbj' + row).val();
				var detailBox = $('#detail_box' + row).val();
				var noPI = $('#no_pi' + row).val();
				var remark = $('#remark' + row).val();
				
				var option =
					'<button class="btn btn-icon waves-effect waves-light btn-danger">'+
						'<i class="fa fa-trash"></i>'+
					'</button>';
				if(noStbj != '') {
					var arrRow = [rowData[0], rowData[1], rowData[2], rowData[3], rowData[4], rowData[5],rowData[6], option];
					tempArr.push(arrRow);
					arrRow = [];
					statusBtn = 1;
				}else if(detailBox == '') {
					swal('Warning', 'Data detail box baris ke-'+ (row+3) +' tidak boleh kosong');
					statusBtn = 0;
				}else if(noPI == '') {
					swal('Warning', 'Data No PI baris ke-'+ (row+4) +' tidak boleh kosong');
					statusBtn = 0;
				}else {
					swal('Warning', 'Data remark baris ke-'+ (row+5) +' harus diisi');
					statusBtn = 0;
				}
			}
		});
		
		if(statusBtn == 1) {
			for (var i = 0; i < tempArr.length; i++) {
				t_addSTBJ.row.add(tempArr[i]).draw();
			}
			$('#panel-modal-detail').modal('toggle');
		}

		for (var i = 0; i < t_addSTBJ.rows().data().length; i++) {
			var rowData = t_addSTBJ.row(i).data();
		}
	})
	
	function isNumber(evt) {
		var iKeyCode = (evt.which) ? evt.which : evt.keyCode
		if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
			return false;

		return true;
	}
	</script>