	<style>
		.col-customer {
			border: solid 1px #b2b8b7;
		}

		.dt-body-left {
			text-align: left;
			vertical-align: middle;
		}

		.dt-body-right {
			text-align: right;
		}

		.dt-body-center {
			text-align: center;
		}

		.titleReport {
			text-align: center;
			margin-left: -116px;
		}

		img {
			width: 95%;
			max-width: 85%;
			height: auto;
		}

		.nama-perusahaan-margin {
			margin-top: 30px;

		}

		.margin-row {
			margin-bottom: 30px
		}

		.margin-ttd {
			margin-bottom: 75px
		}

		.margin-nama {
			margin-bottom: 55px
		}

		.padding-box-nama {
			border: 1px solid #ebf2f2;
			padding: 10px 5px 10px 5px;
		}
	</style>

	<form class="form-horizontal" id="form-invoice" role="form" action="<?php echo base_url('delivery_order/save_invoice'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
		<div class="row">
			<div class="col-md-12">
				<div class="pull-right">
					<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download Invoice" id="btn_download">
						<i class="fa fa-download"></i>
					</a>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="col-md-2 logo-place">
					<img src="<?php echo site_url(); ?>assets/images/logo-celebit.jpg" alt="logo-celebit">
				</div>
				<div class="col-md-10">
					<div class="col-md-12 titleReport">
						<h1 id="titleCelebit">CELEBIT</h1>
						<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
						<h4 id="titleAlamat">BANDUNG FACTORY : JL.BUAH DUA RT.01/RW.04 RANCAEKEK - BANDUNG-INDONESIA</h4>
						<h4 id="titleTlp">TEL 62-22-7798 561/7798 542, FAX : 62-22-7798 562 E-MAIL : invoicing@celebit.com</h4>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-md-12 text-center">
				<h4 id="titleInvoice" style="font-weight:900;">PROFORMA INVOICE</h4>
			</div>
		</div>

		<div class="row margin-row">
			<div class="col-md-12">
				<div class="col-md-8 col-customer">
					<div class="row">
						<div class="col-md-12">
							<table id="print_table_customer">
								<tr>
									<td>
										<label class="control-label" id="namaCustomer"><?php if (isset($invoice[0]['name_eksternal'])) { echo $invoice[0]['name_eksternal']; } ?></label><br>
										<label class="control-label" id="lokasiCustomer"><?php if (isset($invoice[0]['eksternal_address'])) { echo $invoice[0]['eksternal_address']; } ?></label><br>
										<label class="control-label" id="alamatCustomer"><?php if (isset($invoice[0]['phone_1']) || isset($invoice[0]['phone_2']) || isset($invoice[0]['fax']) || isset($invoice[0]['email'])) { echo 'Telp : ' . $invoice[0]['phone_1'] . ' / ' . $invoice[0]['phone_2'] . ' FAX : ' . $invoice[0]['fax'] . ' E-MAIL : ' . $invoice[0]['email']; } ?>
										</label><br>
										<label class="control-label" id="divCustomer"></label><br><label class="control-label col-md-2 pull-left" id="att1">ATTN :</label>
										<div class="col-md-5">
											<input type="text" class="form-control" id="attn" name="attn" value="" placeholder="ATTN"><br>
											<label class="control-label" id="attn-label" style="display:none;"></label>
                    </div>
                    
									</td>
								</tr>
							</table>

						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="noInvoice1">No P Invoice</label>
							<label class="control-label col-md-1" id="noInvoice2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="noInvoice" name="noInvoice">
							</div>
						</div>
					</div>
          <div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="noInvoice1">Rate</label>
							<label class="control-label col-md-1" id="noInvoice2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="rate" name="rate" value="<?php if (isset($invoice[0]['rate'])) { echo $invoice[0]['rate']; } ?>">
							</div>
						</div>
					</div>
					<?php if ($invoice[0]['valas_id'] != 1) { 
						$tanggal_do = date_create($invoice[0]['tanggal_do']);
						$due_dates	= date_create($due_date); 
						$tanggal_invoice  = date_format($tanggal_do, "F d, Y");
						$due_date_invoice = date_format($due_dates, "F d, Y");
					} else { 
						$tanggal_do = date_create($invoice[0]['tanggal_do']); 
						$due_dates	= date_create($due_date); 
						$tanggal_invoice  = date_format($tanggal_do, "d M Y"); 
						$due_date_invoice = date_format($due_dates, "d M Y");
					} ?>
				
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="tanggalInvoice1">Date</label>
							<label class="control-label col-md-1" id="tanggalInvoice2">:</label>
							<div class="col-md-8">
								<div class="input-group date">
									<input placeholder="<?php echo $tanggal_invoice; ?>" type="text" class="form-control datepicker" id="tanggalInvoice" name="tanggalInvoice" required="required" value="<?php if (isset($invoice[0]['tanggal_do'])) { echo $tanggal_invoice; } ?>">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-th"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="poNo1">PO Number</label>
							<label class="control-label col-md-1" id="poNo2">:</label>
							<div class="col-md-8">
								<input placeholder="PO No" type="text" class="form-control" id="poNo" name="poNo" required="required" value="">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="shipmentBy1">Shipment By</label>
							<label class="control-label col-md-1" id="shipmentBy2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="shipmentBy" name="shipmentBy" required="required" value="" placeholder="Shipment By">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="etd1">ETD</label>
							<label class="control-label col-md-1" id="etd2">:</label>
							<div class="col-md-8">
								<div class="input-group">
									<input placeholder="<?php echo date('F d, Y'); ?>" type="text" class="form-control" id="etd" name="etd" required="required" value="<?php if (isset($invoice[0]['tanggal_do'])) { $date = date_create($invoice[0]['tanggal_do']); echo date_format($date, 'F d, Y'); } ?>">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-th"></span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="deliveryTerm1">Delivery Term</label>
							<label class="control-label col-md-1" id="deliveryTerm2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="deliveryTerm" name="deliveryTerm" required="required" value="" placeholder="Delivery Term">
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="col-md-4 col-md-offset-8">
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="netWeight1">Net Weight</label>
							<label class="control-label col-md-1" id="netWeight2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="netWeight" name="netWeight" placeholder="Net Weight">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="shipForm1">Ship Form</label>
							<label class="control-label col-md-1" id="shipForm2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="shipForm" name="shipForm" required="required" value="Jakarta" placeholder="Ship Form">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="shipTo1">Ship To</label>
							<label class="control-label col-md-1" id="shipTo2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="shipTo" name="shipTo" required="required" value="" placeholder="Ship To">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-3" id="blNumber1">BL Number</label>
							<label class="control-label col-md-1" id="blNumber2">:</label>
							<div class="col-md-8">
								<input type="text" class="form-control" id="blNumber" name="blNumber" required="required" value="" placeholder="BL Number">
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="row" style="float: left">
			<label class="control-label col-md-12" id="being">Being Charge for Delivery of Goods Period:</label>
		</div>

		<div class="row margin-row">
			<div class="col-md-12">
				<table id="listinvoice" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th rowspan="2" style="vertical-align: middle;">NO</th>
							<th rowspan="2">ID D INVOICE</th>
							<th colspan="2" style="text-align:center">Description</th>
							<th rowspan="2" style="vertical-align: middle;">Qty (Pcs)</th>
							<th rowspan="2" style="vertical-align: middle;">Unit Price (<?php if (isset($invoice[0]['symbol_valas'])) { echo $invoice[0]['symbol_valas']; } ?>)</th>
							<th rowspan="2" style="vertical-align: middle;">Unit Price Hid(<?php if (isset($invoice[0]['symbol_valas'])) { echo $invoice[0]['symbol_valas']; } ?>)</th>
							<th rowspan="2" style="vertical-align: middle;">Amount (<?php if (isset($invoice[0]['symbol_valas'])) { echo $invoice[0]['symbol_valas']; } ?>)</th>
							<th rowspan="2" style="vertical-align:middle">Amount Hid(<?php if (isset($invoice[0]['symbol_valas'])) { echo $invoice[0]['symbol_valas']; } ?>)</th>
							<th rowspan="2">STBJ Hid</th>
						</tr>
						<tr>
							<th>Part No</th>
							<th>PO</th>
						</tr>
					</thead>
					<tbody></tbody>
					<tfoot>
						<tr>
							<th colspan="7" style="text-align: left;">TOTAL <?php if (isset($invoice[0]['symbol_valas'])) { echo $invoice[0]['symbol_valas']; } ?></th>
							<th id="tdAmount_Total">
								<!--<label id="symbol-amount_total" name="symbol-amount_total"><?php if(isset($invoice[0]['symbol'])) echo $invoice[0]['symbol']; ?>.  </label>-->
								<label id="amount_total_label" name="amount_total_label" style="min-width: 115px; padding-left: 5%;"></label>
								<input type="hidden" id="total" name="total" value="">
								<input type="hidden" id="ppn_harga" name="ppn_harga" value="">
								<input type="hidden" id="total_amount" name="total_amount" value="">
								<input type="hidden" id="id_valas" name="id_valas" value="<?php echo $invoice[0]['valas_id']; ?>">
								<input type="hidden" id="id_invoice" name="id_invoice" value="<?php echo $invoice[0]['id_invoice']; ?>">
							</th>
						</tr>
						<tr>
							<th colspan="8" id="totalBilangan" name="totalBilangan"></th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>

		<div class="row col-md-12">
			<i><label class="col-md-12" id="note_comm">NO COMMERCIAL VALUE</label></i>
		</div>

		<div class="row col-md-12">
			<label class="col-md-12" id="noted">Noted :</label>
		</div>

		<div class="row margin-row">
			<div class="col-md-12">
				<table id="tandatangan_page" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="width:70%;border: 1px solid #ebf2f2;padding: 10px 5px 10px 5px;text-align: left;">
								<br>Approved by Customer : <input type="text" class="form-control" id="approved" name="approved" required="required" value="" placeholder="Approved by Customer" autocomplete="off"><label class="control-label" id="approved-label" name="approved-label" style="display:none;"></label>
								<input type="hidden" class="form-control" id="approved2">
								<br>Date : <label class="control-label" id="dateCust-label" name="dateCust-label" style="display:none;"></label>
								<input type="hidden" class="form-control" id="dateCust2">
								<div class="input-group date datexx">
									<input placeholder="<?php echo date('F d, Y'); ?>" type="text" class="form-control datepicker" id="dateCust" name="dateCust" required="required" value="<?php if (isset($invoice[0]['tanggal_do'])) { $date = date_create($invoice[0]['tanggal_do']); echo date_format($date, 'F d, Y'); } ?>">
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-th"></span>
									</div>
								</div>
								<br><br><br><br><br>
								<br>Name : <input type="text" class="form-control" id="name" name="name" required="required" value="" placeholder="Name"><label class="control-label" id="name-label" name="name-label" style="display:none;"></label>
								<input type="hidden" class="form-control" id="name2">
							</th>
							<th class="text-center" style="width:30%;border-top-style:hidden;border-bottom-style:hidden;border-right-style:hidden;">
								<br><label class="control-label margin-ttd text-center" style="margin-right: 0px;" id="tandatangan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</label>
								<br><br><br><br><br><br>
								<input class="form-control text-center" id="penandaTangan" style="margin-top: 30px;" name="penandaTangan" value="<?php if(isset($invoiced[0]['sign'])) echo $invoiced[0]['sign']; ?>"><label class="control-label margin-ttd text-center" id="penandaTangan-label" style="display:none"></label>
							</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
		
		<hr>
		<div class="row">
			<div class="item form-group">
				<div class="col-md-2 col-md-offset-10">
					<div class="col-md-12 pull-right">
						<input type="hidden" class="form-control" id="id_eks" name="id_eks" required="required" value="<?php if(isset($invoice[0]['id_eks'])) echo $invoice[0]['id_eks']; ?>" placeholder="ID Eksternal">
						<input type="hidden" class="form-control" id="id_do" name="id_do" required="required" value="<?php if(isset($invoice[0]['id_do'])) echo $invoice[0]['id_do']; ?>" placeholder="ID DO">
						<input type="hidden" id="id_valas" name="id_valas" value="<?php echo $invoice[0]['valas_id']; ?>">
						<input type="hidden" id="id_invoice" name="id_invoice" value="<?php echo $invoice[0]['id_invoice']; ?>">
						<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Edit</button>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<input type="hidden" id="totHide" name="totHide">
		</div>
		<div class="row">
			<div class="col-md-12">
				<label class="control-label pull-right" id="kode_report">F_MCI-002</label>
			</div>
			<div class="col-md-12">
				<label class="control-label pull-right" id="kode_report_detail">REV : 00</label>
			</div>
		</div>
	</form>
	
	<script type="text/javascript">
		var dataImage = null;
		var invoice = <?php echo count($invoice); ?>;
		var nama_valas = '<?php if (isset($invoice[0]['nama_valas'])) { echo $invoice[0]['nama_valas']; } ?>';

		var t_invoice_list;

		$(document).ready(function() {
			dtInvoice();
			$("#tanggalInvoice").datepicker({
				format: 'MM dd, yyyy',
				autoclose: true,
				todayHighlight: true,
			});
			
			$("#etd").datepicker({
				format: 'MM dd, yyyy',
				autoclose: true,
				todayHighlight: true,
			}).on("change", function() {
				$("#etd-label").text(this.value);
			});
			
			$(".datexx").datepicker({
				format: 'MM dd, yyyy',
				autoclose: true,
				todayHighlight: true,
			});
			
			$(".datex").datepicker({
				format: 'yyyy-mm-dd',
				autoclose: true,
				todayHighlight: true,
			});
			
			$("#penandaTangan").on("keyup", function() {
				$("#penandaTangan-label").text($("#penandaTangan").val());
			});
			
			$("#approved").on("keyup", function() {
				$("#approved-label").text($("#approved").val());
			})
			
			$("#dateCust").on("keyup", function() {
				$("#dateCust-label").text($("#dateCust").val());
			})
			
			$("#name").on("keyup", function() {
				$("#name-label").text($("#name").val());
			})
			
			for (var i = 0; i < invoice.length; i++) {
				$("#unit_price" + i).on("keyup", function() {
					total = parseFloat($("#total-label").text().replace(/[^\d.-]/g, ''));

					// console.log(parseFloat($("#total-label").text())+parseFloat($("#ppn-label").text()),parseFloat($("#total-label").text()),parseFloat($("#ppn-label").text()));
					$('#amount_total_label').html(formatCurrencyComaUSD(total));
					//$("#tdAmount_Total").html(formatCurrencyComaUSD(total));
					$("#totalBilangan").html(terbilang(total));
				})
			}
			
			$('#btn_download').click(function() {
				var doc = new jsPDF('p', 'mm', 'letter');
				var imgData = dataImage;
				var pdf_pages = doc.internal.pages
				var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
				var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
				// console.log($("#notes-label").val());
				
				// FOOTER
				doc.setFontType('bold');
				doc.setTextColor(0, 0, 0);
				doc.addImage(imgData, 'JPEG', 5, 5, 25, 25)
				doc.setFontSize(12);
				doc.text($('#titleCelebit').html(), 33, 10, 'left');
				doc.setFontSize(10);
				doc.text($('#titlePerusahaan').html(), 33, 15, 'left');
				doc.setFontSize(11);
				doc.text($('#titleAlamat').html(), 33, 20, 'left');
				doc.setFontSize(11);
				doc.text($('#titleTlp').html(), 33, 25, 'left');

				doc.setDrawColor(116, 119, 122);
				doc.setLineWidth(0.7);
				doc.line(4, 30, 500, 30);

				doc.setFontSize(11);
				doc.text($('#titleInvoice').html(), pageWidth / 2, 40, 'center');

				doc.setFontSize(10);

				doc.autoTable({
					html: '#print_table_customer',
					theme: 'plain',
					styles: {
						fontSize: 10,
						fontStyle: 'bold',
						lineColor: [116, 119, 122],
						lineWidth: 0.7,
						cellWidth: 'auto',

					},
					margin: 20,
					tableWidth: ((pageWidth / 2) - 20),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					columnStyles: {
						0: {
							halign: 'left'
						},
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 45
				});

				doc.setFontSize(10);
				doc.text($('#noInvoice1').html(), 135, 50, 'left');
				doc.text($('#noInvoice2').html(), 165, 50, 'left');
				doc.text($('#noInvoice').val(), 170, 50, 'left');

				doc.text($('#tanggalInvoice1').html(), 135, 57, 'left');
				doc.text($('#tanggalInvoice2').html(), 165, 57, 'left');
				doc.text($('#tanggalInvoice').val(), 170, 57, 'left');

				doc.text($('#poNo1').html(), 135, 64, 'left');
				doc.text($('#poNo2').html(), 165, 64, 'left');
				doc.text($('#poNo').val(), 170, 64, 'left');

				doc.text($('#shipmentBy1').html(), 135, 71, 'left');
				doc.text($('#shipmentBy2').html(), 165, 71, 'left');
				doc.text($('#shipmentBy').val(), 170, 71, 'left');

				doc.text($('#etd1').html(), 135, 78, 'left');
				doc.text($('#etd2').html(), 165, 78, 'left');
				doc.text($('#etd').val(), 170, 78, 'left');
				
				doc.text($('#deliveryTerm1').html(), 135, 85, 'left');
				doc.text($('#deliveryTerm2').html(), 165, 85, 'left');
				doc.text($('#deliveryTerm').val(), 170, 85, 'left');
				
				doc.text($('#netWeight1').html(), 135, 95, 'left');
				doc.text($('#netWeight2').html(), 165, 95, 'left');
				doc.text($('#netWeight').val(), 170, 95, 'left');
				
				doc.text($('#shipForm1').html(), 135, 102, 'left');
				doc.text($('#shipForm2').html(), 165, 102, 'left');
				doc.text($('#shipForm').val(), 170, 102, 'left');
				
				doc.text($('#shipTo1').html(), 135, 109, 'left');
				doc.text($('#shipTo2').html(), 165, 109, 'left');
				doc.text($('#shipTo').val(), 170, 109, 'left');
				
				doc.text($('#blNumber1').html(), 135, 116, 'left');
				doc.text($('#blNumber2').html(), 165, 116, 'left');
				doc.text($('#blNumber').val(), 170, 116, 'left');

				doc.text($('#being').html(), 20, 123, 'left');
				
				doc.autoTable({
					html: '#listinvoice',
					theme: 'plain',
					styles: {
						fontSize: 10,
						fontStyle: 'bold',
						lineColor: [116, 119, 122],
						lineWidth: 0.7,
						cellWidth: 'auto',
						overflow: 'linebreak'
					},
					margin: 20,
					tableWidth: (pageWidth - 30),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					didParseCell: function(data) {
						if (data.table.foot[0]) {
							if (data.table.foot[0].cells[4]) {
								data.table.foot[0].cells[4].styles.halign = 'right';
							}
							if (data.table.foot[0].cells[5]) {
								data.table.foot[0].cells[5].styles.halign = 'right';
								// data.table.foot[0].cells[5].text = $('#amount_total_label').html();
							}
						}
						if (data.table.foot[1]) {
							if (data.table.foot[1].cells[5]) {
								data.table.foot[1].cells[5].styles.halign = 'right';
								data.table.foot[1].cells[5].styles.falign = 'right';
								data.table.foot[1].cells[5].text = $('#amount_total_label').html();
							}
						}
					},
					columnStyles: {
						0: {
							tableWidth: 10,
							halign: 'center'
						},
						1: {
							tableWidth: 10,
							halign: 'center'
						},
						2: {
							halign: 'center'
						},
						3: {
							halign: 'right'
						},
						4: {
							halign: 'right'
						},
						5: {
							halign: 'right'
						}
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 128
				});
				
				var startYPZ = doc.autoTable.previous.finalY + 5
				
				doc.text($('#note_comm').html(), 20, startYPZ + 5, 'left');
				doc.text($('#noted').html(), 20, startYPZ + 10, 'left');
				
				var x = pageWidth * 70 / 100;
				var y = pageHeight * 75 / 100;
				
				doc.autoTable({
					html: '#tandatangan_page',
					theme: 'plain',
					styles: {
						fontSize: 10,
						fontStyle: 'bold',
						lineWidth: 0,
						cellWidth: 'auto',
						overflow: 'linebreak'
					},
					margin: 20,
					tableWidth: (pageWidth - 30),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					didParseCell: function(data) {
						if (data.table.head[0]) {
							if (data.table.head[0].cells[0]) {
								data.table.head[0].cells[0].styles.halign = 'left';
								var approve = data.table.head[0].cells[0].text[1];
								var datecust = data.table.head[0].cells[0].text[2];
								var names = data.table.head[0].cells[0].text[8];
								
								var txt_app = approve.split(':');
								var date_app = datecust.split(':');
								var name_app = names.split(':');
								
								$('#approved2').val(txt_app[1]);
								$('#dateCust2').val(date_app[1]+''+data.table.head[0].cells[0].text[3]);
								$('#name2').val(name_app[1]);
								data.table.head[0].cells[0].styles.halign = 'left';
							}
						}
					},
					pageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: doc.autoTable.previous.finalY + 15
				});
			
				doc.setFontSize(10);
				var startYPO = doc.autoTable.previous.finalY + 5
				
				doc.page = 1;
				
				var myFooter1 = $('#kode_report').html();
				var myFooter2 = $('#kode_report_detail').html();
				
				for (var i = 1; i < pdf_pages.length; i++) {
					doc.setPage(i)
					doc.text(myFooter1, 185, pageHeight - 5)
					doc.text(myFooter2, 185, pageHeight - 10)
					doc.page ++;
				}
				
				doc.save('INVOICE <?php echo $invoice[0]['name_eksternal'].'-'.$invoice[0]['tanggal_do']; ?>.pdf');
			});
		});

		$('#form-invoice').on('submit',(function(e) {
			if($('#no_invoice').val() !== ''){
				$('#btn-submit').attr('disabled','disabled');
				$('#btn-submit').text("Memasukkan data...");
				e.preventDefault();
				var formData = new FormData(this);
				
				$.ajax({
					type:'POST',
					url: $(this).attr('action'),
					data:formData,
					cache:false,
					contentType: false,
					processData: false,
					success: function(response) {
						if (response.success == true) {
							save_invoice(response.lastid,response.id_valas,response.id_eksternal,response.total_amount,response.ppn_harga,response.ppn);
						} else{
							$('#btn-submit').removeAttr('disabled');
							$('#btn-submit').text("Simpan");
							swal("Failed!", response.message, "error");
						}
					}
				}).fail(function(xhr, status, message) {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Simpan");
					swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
				});
			}else{
				swal("Failed!", "Maaf kolom Keterangan harus diisi", "error");
			}
			return false;
		}));
		
		function save_invoice(lastid,id_valas,id_eks,total_amount,ppn_harga,ppn) {
			
			var arrTemp = [];
			for (var i = 0; i < t_invoice_list.rows().data().length; i++) {
				var rowData = t_invoice_list.row(i).data();
				arrTemp.push(rowData); 
			}
			
			var datapost = {
				'id_invoice' 	 : lastid,
				'id_valas'		 : id_valas,
				'id_eksternal'	 : id_eks,
				'total_amount'	 : total_amount,
				'ppn_harga'	 	 : ppn_harga,
				'ppn'		 	 : ppn,
				'list_invoice'	 : arrTemp
			};
			
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>delivery_order/insert_detail_invoice",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function () {
							window.location.href = "<?php echo base_url('delivery_order');?>";
						})
					}else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Simpan");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Simpan");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}
	
		function toDataURL(url, callback) {
			var xhr = new XMLHttpRequest();
			xhr.onload = function() {
				var reader = new FileReader();
				reader.onloadend = function() {
					callback(reader.result);
				}
				reader.readAsDataURL(xhr.response);
			};
			xhr.open('GET', url);
			xhr.responseType = 'blob';
			xhr.send();
		}

		toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
			dataImage = dataUrl;
		})

		function dtInvoice() {
			t_invoice_list = $('#listinvoice').DataTable({
				"processing": true,
				"searching": false,
				"paging": false,
				"responsive": true,
				"lengthChange": false,
				"info": false,
				"bSort": false,
				"ajax": {
					"type": "GET",
					"url": "<?php echo base_url() . 'delivery_order/delivery_list_invoice/' . $id_do; ?>",
					"dataSrc": function(response) {
						$('#tdAmount_Total').html(formatCurrencyComaUSD(response.total_amounts));
						return response.data;
					}
				},
				"columnDefs": [{
					"targets": [0],
					"searchable": false,
					"className": 'dt-body-center',
					"width": 10
				}, {
					"targets": [2],
					"searchable": false,
					"className": 'dt-body-center',
					"width": 250
				}, {
					"targets": [3],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 150
				}, {
					"targets": [4],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 150
				}, {
					"targets": [5],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 60
				},{
					"targets": [1,6,8,9],
					"searchable": false,
					"visible": false,
					"className": 'dt-body-right',
					"width": 60
				}, {
					"targets": [7],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 150
				}],
				"footerCallback": function(row, data, start, end, display) {
					var api = this.api(),
						data;
					var total = 0;
					// Remove the formatting to get integer data for summation
					var intVal = function(i) {
						return typeof i === 'string' ?
							i.replace(/[\$,]/g, '') * 1 :
							typeof i === 'number' ?
							i : 0;
					};

					// Total over all pages
					total = api
						.column(8)
						.data()
						.reduce(function(a, b) {
							total = total + intVal(a) + intVal(b);
							return (intVal(a) + intVal(b));
						}, 0);

					// Total over this page
					pageTotal = api
						.column(8, {
							page: 'current'
						})
						.data()
						.reduce(function(a, b) {
							total = total + intVal(a) + intVal(b);
							return (intVal(a) + intVal(b));
						}, 0);

					// Update footer
					$(api.column(8).footer()).html(
						'<?php $invoice[0]['symbol_valas']; ?>. ' + formatCurrencyComaUSD(total)
					)
					
					var totals = total;
					
					$("#penandaTangan-label").text($("#penandaTangan").val());
					$("#approved-label").text($("#approved").val());
					$("#dateCust-label").text($("#dateCust").val());
					$("#name-label").text($("#name").val());
					
					$("#total-label").text(formatCurrencyComaUSD(pageTotal));
					$('#amount_total_label').html(formatCurrencyComaIDR(totals));
					$('#total_amount').val(totals);
					$('#total').val(totals);
					$('#totHide').val(pageTotal.toFixed(2));

					terbilangENG();
				}
			});
		}

		// System for American Numbering 
		var th_val = ['', 'Thousand', 'Million', 'Billion', 'Trillion'];
		var dg_val = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];
		var tn_val = ['Ten', 'eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
		var tw_val = ['Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];

		function toWordsconver(s) {
			s = s.toString();
			s = s.replace(/[\, ]/g, '');
			if (s != parseFloat(s))
				return 'not a number ';
			var x_val = s.indexOf('.');
			if (x_val == -1)
				x_val = s.length;
			if (x_val > 15)
				return 'too big';
			var n_val = s.split('');
			var str_val = '';
			var sk_val = 0;
			for (var i = 0; i < x_val; i++) {
				if ((x_val - i) % 3 == 2) {
					if (n_val[i] == '1') {
						str_val += tn_val[Number(n_val[i + 1])] + ' ';
						i++;
						sk_val = 1;
					} else if (n_val[i] != 0) {
						str_val += tw_val[n_val[i] - 2] + ' ';
						sk_val = 1;
					}
				} else if (n_val[i] != 0) {
					str_val += dg_val[n_val[i]] + ' ';
					if ((x_val - i) % 3 == 0)
						str_val += 'hundred ';
					sk_val = 1;
				}
				if ((x_val - i) % 3 == 1) {
					if (sk_val)
						str_val += th_val[(x_val - i - 1) / 3] + ' ';
					sk_val = 0;
				}
			}
			if (x_val != s.length) {
				var y_val = s.length;
				str_val += 'And Cents ';
				for (var i = x_val + 1; i < y_val; i++) {
					// str_val += dg_val[n_val[i]] + ' ';
					if ((y_val - i) % 3 == 2) {
						if (n_val[i] == '1') {
							str_val += tn_val[Number(n_val[i + 1])] + ' ';
							i++;
							sk_val = 1;
						}else if (n_val[i] != 0) {
							str_val += tw_val[n_val[i] - 2] + ' ';
							sk_val = 1;
						}
					}else if (n_val[i] != 0) {
						str_val += dg_val[n_val[i]] + ' ';
						if ((x_val - i) % 3 == 0)
							str_val += 'Hundred ';
						sk_val = 1;
					}

					if ((y_val - i) % 3 == 1) {
						if (sk_val)
							str_val += th_val[(y_val - i - 1) / 3] + ' ';
						sk_val = 0;
					}
				}
			}
			return "Says : ( " + str_val.replace(/\s+/g, ' ') + " Only )";
		}

		function terbilangENG() {
			var bilangan = document.getElementById("totHide").value;
			var kalimat = toWordsconver(bilangan);

			document.getElementById("totalBilangan").innerHTML = kalimat;
		}

		function cal_price(row) {
			var totAmount = 0;
			var rowData = t_invoice_list.row(row).data();
			var amount = rowData[6];

			var qtykoma = rowData[4];
			var qty = qtykoma.replace(/[^\d.-]/g, '');
			var unitPrice = $('#unit_price' + row).val();
			amount = parseFloat(unitPrice) * parseFloat(qty);
			
			$('input[id="amount'+row+'"]').val(parseFloat(amount));
			$("#amount-label"+row).html(formatCurrencyRupiah(amount));
			$("#unit_price-label"+row).html(formatCurrencyRupiah(parseFloat(unitPrice)));

			totAmount = formatCurrencyComa(amount);
			$('input[id="amount' + row + '"]').val(amount);

			var tempTotal = 0;
			$('input[name="amount[]"]').each(function() {
				if (this.value !== '' && this.value !== null)
					tempTotal = tempTotal + parseFloat(this.value);
				else
					tempTotal = tempTotal + 0;
			})

			$('#total-label').text(formatCurrencyComaUSD(tempTotal));
			$('#amount_total_label').html(formatCurrencyComaIDR(tempTotal));
			// $('#tdAmount_Total').html(formatCurrencyComaIDR(tempTotal + (tempTotal * (ppn_total / 100))));
			// $("#totalBilangan").html(terbilang(tempTotal));

			document.getElementById("totalBilangan").innerHTML = toWordsconver(tempTotal);
		}

		function redrawTable(row) {
			var rowData = t_invoice_list.row(row).data();

			rowData[6] = $('#unit_price' + row).val();
			rowData[8] = $('#amount' + row).val();
			t_invoice_list.draw();
		}
		
		function numberWithCommas(x) {
			var parts = x.toString().split(".");
			parts[0] = parts[0].replace(",", "");
			return parts.join(".");
		}
	</script>