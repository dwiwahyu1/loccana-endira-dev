	<style>
		.col-customer {
			border: solid 1px #b2b8b7;
		}

		.dt-body-left {
			text-align: left;
			vertical-align: middle;
		}

		.dt-body-right {
			text-align: right;
			vertical-align: middle;
		}

		.dt-body-center {
			text-align: center;
			vertical-align: middle;
		}

		.titleReport {
			text-align: center;
			margin-left: -116px;
		}

		img {
			width: 50%;
			max-width: 85%;
			height: auto;
		}

		.margin-row {
			margin-top: 45px;
			margin-bottom: 45px;
		}

		.left-header {
			width: 30%;
			float: left;
			position: relative;
			text-align: center;
			padding-left: 35px;
			padding-right: 35px;
		}

		.right-header {
			position: relative;
			text-align: center;
			padding-left: 35px;
			padding-right: 357px;
		}

		.border-packing {
			border: 1px solid #ebeff2;
		}
	</style>

	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download Packing List" id="btn_download">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>

	<form class="form-horizontal form-label-left" id="form_packing" role="form" action="<?php echo base_url('delivery_order/save_packing');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
		<div class="row border-packing">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="left-header">
							<img src="<?php echo site_url(); ?>assets/images/logo-celebit.jpg" alt="logo-celebit">
						</div>
						<div class="right-header">
							<h1 id="titleCelebit">CELEBIT</h1>
							<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
							<h4 id="titleAlamat">BANDUNG FACTORY : JL.BUAH DUA RT.01/RW.04 RANCAEKEK - BANDUNG-INDONESIA</h4>
							<h4 id="titleTlp">TEL 62-22-7798 561/7798 542, FAX : 62-22-7798 562 E-MAIL : invoicing@celebit.com</h4>
						</div>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-12 text-center">
					<h3 id="titleInvoice" style="font-weight:bold;">PACKING LIST</h3>
				</div>
			</div>

			
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-8 col-customer">
						<div class="row">
							<div class="col-md-12">
								<table class="table" id="tableCustomer">
									<tbody>
										<tr>
											<td>
												<label class="control-label" id="namaCustomer"><?php if (isset($packing[0]['name_eksternal'])) { echo $packing[0]['name_eksternal']; } ?></label><br>
												<label class="control-label" id="lokasiCustomer"><?php if (isset($packing[0]['eksternal_address'])) { echo $packing[0]['eksternal_address']; } ?></label><br>
												<label class="control-label" id="alamatCustomer"> <?php if (isset($packing[0]['phone_1']) || isset($packing[0]['phone_2']) || isset($packing[0]['fax']) || isset($packing[0]['email'])) { echo 'Telp : ' . $packing[0]['phone_1'] . ' / ' . $packing[0]['phone_2'] . ' FAX : ' . $packing[0]['fax'] . ' E-MAIL : ' . $packing[0]['email']; } ?></label><br>
												<label class="control-label" id="divCustomer"><?php if (isset($packing[0]['pic'])) { echo $packing[0]['pic']; } ?></label><br>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div> 
					<div class="col-md-4">
						<div class="row">
							<div class="col-md-12">
								<label class="control-label col-md-3" id="noInvoice1">No. Invoice</label>
								<label class="control-label col-md-1" id="noInvoice2">:</label>
								<div class="col-md-8">
									<input class="form-control" id="noInvoice" name="noInvoice" placeholder="No Invoice" value="<?php if (isset($packing[0]['no_invoice'])) { echo $packing[0]['no_invoice']; } ?>" required>
								</div>
								<br><br>
								<?php if ($packing[0]['valas_id'] != 1) { 
									$tanggal_do = date_create($packing[0]['tanggal_do']);
									$tanggal_invoice  = date_format($tanggal_do, "F d, Y");
								} else { 
									$tanggal_do = date_create($packing[0]['tanggal_do']); 
									$tanggal_invoice  = date_format($tanggal_do, "d M Y"); 
								} ?>

								<label class="control-label col-md-3" id="tanggalInvoice1">Date</label>
								<label class="control-label col-md-1" id="tanggalInvoice2">:</label>
								<div class="col-md-8">
									<div class="input-group date">
										<input placeholder="<?php echo $tanggal_invoice; ?>" type="text" class="form-control datepicker" id="tanggalInvoice" name="tanggalInvoice" required="required" value="<?php if (isset($packing[0]['tanggal_do'])) { echo $tanggal_invoice; } ?>">
										<label class="control-label" id="noInvoice-label" style="display:none;"></label><div class="input-group-addon">
											<span class="glyphicon glyphicon-th"></span>
										</div>
									</div>
								</div><br><br>
								<label class="control-label col-md-3" id="attn1">Attn</label>
								<label class="control-label col-md-1" id="attn2">:</label>
								<div class="col-md-8">
									<input class="form-control" id="attn" name="attn" value="<?php if (isset($packing[0]['attention'])) { echo $packing[0]['attention']; } ?>" placeholder="Attention" required>
									<label style="display:none;" class="control-label margin-row" id="attn11"><?php if(isset($attn)) { echo str_replace('<br />','',$attn); }else{ $attn; }; ?></label>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">

						</div>
					</div>
				</div>
			</div>
		</div>
		<hr style="margin-top: 35px;"></br>
		<div class="row">
			<div class="col-md-12">
				<table id="listPackingList" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th class="text-center" rowspan="2" style="vertical-align: middle;">No.</th>
							<th class="text-center" rowspan="2" style="vertical-align: middle;">CTN NO.</th>
							<th class="text-center" rowspan="2" style="vertical-align: middle;">CTN NO.Hid</th>
							<th class="text-center" rowspan="2" style="vertical-align: middle;">PO NO</th>
							<th class="text-center" rowspan="2" style="vertical-align: middle;">SKU</th>
							<th class="text-center" rowspan="2" style="vertical-align: middle;">DESCRIPTION</th>
							<th class="text-center" rowspan="">QUANTITY</th>
							<th class="text-center" rowspan="">NETT WEIGHT</th>
							<th class="text-center" rowspan="2" style="vertical-align: middle;">GROSS WEIGHT (KGM)</th>
							<th class="text-center" rowspan="2" style="vertical-align: middle;">ID D INVOICE</th>
						</tr>
						<tr>
							<th class="text-right" rowspan="">(PCS)<?php //if(isset($packing[0]['uom_symbol'])) echo '('.$packing[0]['uom_symbol'].')'; 
																											?></th>
							<th class="text-right" rowspan="">(KGM)<?php //if(isset($packing[0]['uom_symbol'])) echo '('.$packing[0]['uom_symbol'].')'; 
																											?></th>
						</tr>
					</thead>
					<tbody></tbody>
					<tfoot>
						<tr>
							<th colspan="6" style="text-align: left;">TOTAL</th>
							<th id="tdQty"></th>
							<th id="tdNet"></th>
							<th id="tdGross"></th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
		
		<div class="row margin-row">
			<div class="col-md-12">
				<div class="col-md-3 col-md-offset-9 text-center">
					<label class="control-label margin-row" id="namaPerusahaan">PT. CELEBIT CIRCUIT TECH. IND</label>
					<input type="text" class="form-control text-center" id="nama_ttd" name="nama_ttd" placeholder="NAMA TANDA TANGAN" value="">
					<label style="display:none;" class="control-label margin-row" id="nama_ttd2"><?php if(isset($attn)) { echo str_replace('<br />','',$attn); }else{ $attn; }; ?></label>
				</div> 
			</div>
		</div>
		
		<hr>
		<div class="row">
			<div class="col-md-2 col-md-offset-10">
				<div class="col-md-12 pull-right">
					<input type="hidden" class="form-control" id="id_eks" name="id_eks" required="required" value="<?php if(isset($packing[0]['id_eks'])) echo $packing[0]['id_eks']; ?>" placeholder="ID Eksternal">
					<input type="hidden" class="form-control" id="id_do" name="id_do" required="required" value="<?php if(isset($packing[0]['id_do'])) echo $packing[0]['id_do']; ?>" placeholder="ID DO">
					<input type="hidden" id="total_qty" name="total_qty" value="">
					<input type="hidden" id="total_net" name="total_net" value="">
					<input type="hidden" id="total_gross" name="total_gross" value="">
					<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Simpan</button>
				</div>
			</div>
		</div>
	</form>
	
	<script type="text/javascript">
		var dataImage = null;
		var t_packingList;
		var packing = "<?php echo count($packing); ?>";
		var valas_id = "<?php if(isset($packing[0]['valas_id'])) echo $packing[0]['valas_id'];?>";
		var id_d_packing = "<?php if(isset($packing[0]['id_d_packing'])) echo $packing[0]['id_d_packing']; ?>";
		var id_do = "<?php if(isset($packing[0]['id_do'])) echo $packing[0]['id_do'];?>";
		
		$(document).ready(function() {
			dtPacking();
			$('form').parsley();
			$('[data-toggle="tooltip"]').tooltip();
			
			$("#tanggalInvoice").datepicker({
				format: 'MM dd, yyyy',
				autoclose: true,
				todayHighlight: true,
			});
			
			$(".datex").datepicker({
				format: 'yyyy-mm-dd',
				autoclose: true,
				todayHighlight: true,
			});
			
			$("#noInvoice3").on('keyup',function(){
				$("#labelNoInvoice").text($("#noInvoice3").val());
			})
			
			$('#btn_download').click(function() {
				var doc = new jsPDF('p', 'mm', 'letter');
				var imgData = dataImage;
				var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
				var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

				// FOOTER
				doc.setFontType('bold');
				doc.setTextColor(0, 0, 0);
				doc.addImage(imgData, 'JPEG', 5, 5, 25, 25)
				doc.setFontSize(12);
				doc.text($('#titleCelebit').html(), 33, 10, 'left');
				doc.setFontSize(11);
				doc.text($('#titlePerusahaan').html(), 33, 15, 'left');
				doc.setFontSize(11);
				doc.text($('#titleAlamat').html(), 33, 20, 'left');
				doc.setFontSize(11);
				doc.text($('#titleTlp').html(), 33, 25, 'left');

				doc.setDrawColor(116, 119, 122);
				doc.setLineWidth(1);
				doc.line(4, 30, 210, 30);

				doc.setFontSize(10);
				doc.text($('#titleInvoice').html(), pageWidth / 2, 40, 'center');

				doc.setFontSize(9);
				doc.autoTable({
					html: '#tableCustomer',
					theme: 'plain',
					styles: {
						fontSize: 9,
						fontStyle: 'bold',
						lineColor: [116, 119, 122],
						lineWidth: 1,
						cellWidth: 'auto',

					},
					margin: 20,
					tableWidth: ((pageWidth / 2) - 20),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 45
				});
				
				var startYPZ = doc.autoTable.previous.finalY
				
				doc.setFontSize(9);
				doc.text($('#noInvoice1').html(), 120, 47, 'left');
				doc.text($('#noInvoice2').html(), 145, 47, 'left');
				doc.text($('#noInvoice').val(), 155, 47, 'left');

				doc.text($('#tanggalInvoice1').html(), 120, 54, 'left');
				doc.text($('#tanggalInvoice2').html(), 145, 54, 'left');
				doc.text($('#tanggalInvoice').val(), 155, 54, 'left');
				
				doc.text($('#attn1').html(), 120, 61, 'left');
				doc.text($('#attn2').html(), 145, 61, 'left');
				doc.text($('#attn').val(), 155, 61, 'left');

				doc.autoTable({
					html: '#listPackingList',
					theme: 'plain',
					styles: {
						fontSize: 9,
						fontStyle: 'bold',
						lineColor: [116, 119, 122],
						lineWidth: 1,
						cellWidth: 'auto',

					},
					margin: 20,
					tableWidth: (pageWidth - 30),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					didParseCell: function(data) {
						if (data.table.foot[0]) {
							if (data.table.foot[0].cells[5]) {
								data.table.foot[0].cells[5].styles.halign = 'right';
							}
							if (data.table.foot[0].cells[6]) {
								data.table.foot[0].cells[6].styles.halign = 'right';
							}
							if (data.table.foot[0].cells[7]) {
								data.table.foot[0].cells[7].styles.halign = 'right';
							}
						}
					},
					columnStyles: {
						0: {
							cellWidth: 10,
							halign: 'center'
						},
						1: {
							tableWidth: 25,
							halign: 'center'
						},
						2: {
							tableWidth: 40,
							halign: 'left'
						},
						3: {
							tableWidth: 40,
							halign: 'left',
							falign: 'left'
						},
						4: {
							tableWidth: 40,
							halign: 'left',
							falign: 'left'
						},
						5: {
							cellWidth: 25,
							halign: 'right',
							falign: 'right'
						},
						6: {
							cellWidth: 25,
							halign: 'right',
							falign: 'right'
						},
						7: {
							cellWidth: 25,
							halign: 'right',
							falign: 'right'
						}
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: startYPZ + 5
				});
				
				var startYPX = doc.autoTable.previous.finalY + 5
				var x = pageWidth * 80 / 100;
				var y = pageHeight * 35 / 100;

				doc.setFontType('bold');
				doc.setFontSize(9);
				doc.text($('#namaPerusahaan').html(), x, startYPX, 'center');
				// doc.text($('#nama_ttd').val(), x, startYPX + 25, 'center');
				doc.text($('#nama_ttd').val(), x, startYPX + 25, 'center');

				doc.save('PACKING LIST <?php echo $packing[0]['name_eksternal'].' - '.date('d-M-Y'); ?>.pdf');
			});

		});

		$('#form_packing').on('submit',(function(e) {
			$('#btn-submit').attr('disabled','disabled');
			$('#btn-submit').text("Memasukkan data...");
			e.preventDefault();
			var formData = new FormData(this);
			
			$.ajax({
				type:'POST',
				url: $(this).attr('action'),
				data: formData,
				cache:false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						save_packing(response.lastid,response.id_eksternal,response.attention);
					}else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Simpan Packing List");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Simpan Packing List");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}));
	
		function save_packing(lastid,id_eks,attention) {
			var arrTemp = [];
			for (var i = 0; i < t_packingList.rows().data().length; i++) {
				var rowData = t_packingList.row(i).data();
				arrTemp.push(rowData); 
			}
			
			var datapost = {
				'id_do' 		 : id_do,
				'id_d_packing'	 : id_d_packing,
				'id_invoice' 	 : lastid,
				'id_valas'		 : valas_id,
				'id_eksternal'	 : id_eks,
				'attention'	 	 : attention,
				'list_packing'	 : arrTemp
			};
			
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>delivery_order/insert_detail_packing",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function () {
							window.location.href = "<?php echo base_url('delivery_order');?>";
							// $('#panel-modal').modal('toggle');
							// listdelivery();
						})
					}else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Simpan");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Simpan");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}
	
		function toDataURL(url, callback) {
			var xhr = new XMLHttpRequest();
			xhr.onload = function() {
				var reader = new FileReader();
				reader.onloadend = function() {
					callback(reader.result);
				}
				reader.readAsDataURL(xhr.response);
			};
			xhr.open('GET', url);
			xhr.responseType = 'blob';
			xhr.send();
		}

		toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
			dataImage = dataUrl;
		})

		function dtPacking() {
			
			t_packingList = $('#listPackingList').DataTable({
				"processing": true,
				"searching": false,
				"paging": false,
				"responsive": true,
				"lengthChange": false,
				"info": false,
				"bSort": false,
				"ajax": {
					"type": "GET",
					"url": "<?php echo base_url() . 'delivery_order/detail_delivery_order_packing/' . $packing[0]['id_do']; ?>",
					"dataSrc": function(response) {
							$('#tdQty').html(response.total_qty.toFixed(0));
							$('#tdNet').html(formatCurrencyComaIDR(response.total_net));
							$('#tdGross').html(formatCurrencyComaIDR(response.total_gross));
							$('#total_qty').val(response.total_qty.toFixed(0));
							$('#total_net').val(formatCurrencyComaIDR(response.total_net));
							$('#total_gross').val(formatCurrencyComaIDR(response.total_gross));
						return response.data;
					}
				},
				"columnDefs": [{
					"targets": [0],
					"searchable": false,
					"visible": true,
					"className": 'dt-body-center',
					"width": 10
				}, {
					"targets": [1],
					"searchable": false,
					"className": 'dt-body-center',
					"width": 90
				}, {
					"targets": [2,9],
					"searchable": false,
					"visible": false,
					"className": 'dt-body-left',
					"width": 175
				}, {
					"targets": [3,4,5],
					"searchable": false,
					"className": 'dt-body-left',
					"width": 175
				}, {
					"targets": [6,7,8],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 150
				}]
			})
		}
		
		function cal_price(row) {
			var rowData = t_packingList.row(row).data();
			var no_kontainer = $('#no_kontainer' + row).val();
			
			var no_kontainers = '';
			$('input[name="no_kontainer' + row + '"]').each(function() {
				if (this.value !== '' && this.value !== null) {
					$('#no_kontainer_label'+row).text(this.value);
				} else {
					$('#no_kontainer_label'+row).text(no_kontainers);
				}
			})
		}
	
		function redrawTable(row) {
			var rowData = t_packingList.row(row).data();
			
			rowData[2] = $('#no_kontainer'+row).val();
			t_packingList.draw();
		}
	</script>