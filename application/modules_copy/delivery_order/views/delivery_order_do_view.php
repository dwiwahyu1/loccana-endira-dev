	<style>
		.col-customer {
			border: solid 1px #b2b8b7;
		}

		.dt-body-left {
			text-align: left;
			vertical-align: middle;
		}

		.dt-body-right {
			text-align: right;
			vertical-align: middle;
		}

		.dt-body-center {
			text-align: center;
			vertical-align: middle;
		}

		.dt-body-left th {
			text-align: left;
			vertical-align: middle;
		}

		.dt-body-right th {
			text-align: right;
			vertical-align: middle;
		}

		.dt-body-center th {
			text-align: center;
			vertical-align: middle;
		}

		.titleReport {
			text-align: center;
			margin-left: -116px;
		}

		img {
			width: 50%;
			max-width: 85%;
			height: auto;
		}

		.margin-row {
			margin-top: 45px;
			margin-bottom: 45px;
		}

		.left-header {
			width: 30%;
			float: left;
			position: relative;
			text-align: center;
			padding-left: 35px;
			padding-right: 35px;
		}

		.right-header {
			position: relative;
			text-align: center;
			padding-left: 35px;
			padding-right: 357px;
		}

		.border-packing {
			border: 1px solid #ebeff2;
		}

		.border-bottom {
			border-bottom: 1px solid #ebeff2;
		}
	</style>

	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download Delivery Order" id="btn_download">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>

	<div class="row border-packing">
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="left-header">
						<img src="<?php echo site_url(); ?>assets/images/logo-celebit.jpg" alt="logo-celebit">
					</div>
					<div class="right-header">
						<h1 id="titleCelebit">CELEBIT</h1>
						<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
						<h4 id="titleAlamat">BANDUNG FACTORY : JL.BUAH DUA RT.01/RW.04 RANCAEKEK - BANDUNG-INDONESIA</h4>
						<h4 id="titleTlp">TEL 62-22-7798 561/7798 542, FAX : 62-22-7798 562 E-MAIL : celebit@celebit.id</h4>
					</div>
				</div>
			</div>
		</div>
		<hr style="margin-top: 35px;">
		<div class="row" style="margin-bottom: 15px;">
			<div class="col-md-12 text-center">
				<h2 id="titleInvoice" style="font-weight:900;">DELIVERY ORDER</h2>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="col-md-1" style="width: 30px;">
					<label class="control-label" id="ms">M/S</label>
				</div>
				<div class="col-md-4">
					<div class="col-md-12 border-bottom">
						<label class="control-label" id="namaCustomer"><?php if (isset($do[0]['name_eksternal'])) { echo $do[0]['name_eksternal']; } ?></label>
					</div>
					<div class="col-md-12 border-bottom">
						<label class="control-label" id="lokasiCustomer"><?php if (isset($do[0]['eksternal_address'])) { echo $do[0]['eksternal_address']; } ?></label>
					</div>
					<div class="col-md-12 border-bottom">
						<label class="control-label" id="alamatCustomer"><?php if (isset($do[0]['phone_1']) || isset($do[0]['phone_2']) || isset($do[0]['fax']) || isset($do[0]['email'])) { echo 'Telp : ' . $do[0]['phone_1'] . ' / ' . $do[0]['phone_2'] . ' FAX : ' . $do[0]['fax'] . ' E-MAIL : ' . $do[0]['email']; } ?></label>
					</div>
					<div class="col-md-12 border-bottom">
						<label class="control-label" id="divCustomer"><?php if (isset($do[0]['pic'])) { echo $do[0]['pic']; } ?></label><br>
					</div>
				</div>
				<div class="col-md-5 pull-right">
					<div class="row">
						<div class="col-md-12" style="margin-bottom: 10px;">
							<label class="control-label col-md-4" id="noInvoice1" style="font-size: 15px;font-weight: bold;">Delivery Order No.</label>
							<label class="control-label col-md-1" id="noInvoice2">:</label>
							<input type="text" class="form-control" id="no_do" name="no_do" style="width: 50%" placeholder="Masukan No DO" value="<?php echo( $do[0]['no_do'] );?>">
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-4" id="tanggalInvoice1a">Date</label>
							<label class="control-label col-md-1" id="tanggalInvoice2a">:</label>
							<label class="control-label col-md-7" id="tanggalInvoice3a"><?php if (isset($do[0]['tanggal_do'])) { $tanggal_do = date_create($do[0]['tanggal_do']); echo trim(date_format($tanggal_do, "d F Y")); } ?></label>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<label class="control-label col-md-4" id="tanggalInvoice1b">PO No</label>
							<label class="control-label col-md-1" id="tanggalInvoice2b">:</label>
							<label class="control-label col-md-7" id="tanggalInvoice3b">As Mentioned</label>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12" style="margin-bottom: 10px;">
							<label class="control-label col-md-4" id="noInvoicea">No. Invoice</label>
							<label class="control-label col-md-1" id="noInvoiceb">:</label>
							<input style="width: 50%;" class="form-control" type="text" id="no_invoice" name="no_invoice" value="<?php if(isset($do[0]['no_invoice'])) echo $do[0]['no_invoice']; ?>" placeholder="No Invoice">
						</div>
					</div>

					<div class="row">
						<div class="col-md-12" style="margin-bottom: 10px;">
							<label class="control-label col-md-4" id="noBC1">No. BC</label>
							<label class="control-label col-md-1" id="noBC2">:</label>
							<input style="width: 50%;" class="form-control" type="text" id="no_bc" name="no_bc" placeholder="No BC" value="<?php if(isset($do[0]['no_pendaftaran'])) echo $do[0]['no_pendaftaran']; ?>">
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-12" style="margin-bottom: 10px;">
							<label class="control-label col-md-4" id="attn1">Attn</label>
							<label class="control-label col-md-1" id="attn2">:</label>
							<input style="width: 50%;" class="form-control" id="attn" name="attn" placeholder="Attention" value="<?php if(isset($do[0]['attention'])) echo $do[0]['attention']; ?>" required></input>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<table id="listDOList" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th class="dt-body-center" rowspan="2">ITEM</th>
							<th class="dt-body-center" colspan="2">DESCRIPTION</th>
							<th class="dt-body-center" rowspan="2">QUANTITY</th>
							<th class="dt-body-center" rowspan="2">Remark</th>
						</tr>
						<tr>
							<th class="dt-body-center">PCB PART NO</th>
							<th class="dt-body-center">PO NO</th>
						</tr>
					</thead>
					<tbody></tbody>
					<tfoot>
						<tr>
							<th style="text-align: right;" colspan="3">TOTAL</th>
							<th style="text-align: center;" class="dt-body-center" id="idQty"><?php if (isset($totalQty)) echo $totalQty; ?></th>
							<td style="text-align: center;" class="dt-body-center" id="idRmk"><?php if (isset($totalRemark)) echo $totalRemark; ?></td>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>

		<div class="row margin-row">
			<div class="col-md-12">
				<div class="col-md-4">
					<label class="control-label" id="note_foot1">RECEIVED THE ABOVE MENTIONED GOODS</label>
				</div>
				<div class="col-md-4"></div>
				<div class="col-md-4 text-center">
					<label class="control-label" id="note_for_foot">for</label>
				</div>
				<div class="col-md-4"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-4">
					<label class="control-label" id="note_foot2">IN GOODS ORDER AND CONDITION</label>
				</div>
				<div class="col-md-4"></div>
				<div style="height: 150px;" class="col-md-4 text-center">
					<label class="control-label" id="note_pt_foot">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</label>
				</div>
			</div>
			<div class="col-md-12 text-center">
				<div class="col-md-4"></div>
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<input type="text" class="form-control text-center" id="nama_ttd" name="nama_ttd" placeholder="NAMA TANDA TANGAN" value="">
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<label class="control-label pull-right" id="kode_report">F_MCI-002</label>
			</div>
			<div class="col-md-12">
				<label class="control-label pull-right" id="kode_report_detail">REV : 00</label>
			</div>
		</div>
	</div>
	<table id="tableCustomer" style="display:none;">
		<tbody>
			<tr>
				<td>
					<label class="control-label" id="namaCustomer"><?php if (isset($do[0]['name_eksternal'])) {
																														echo $do[0]['name_eksternal'];
																													} ?></label><br>
					<label class="control-label" id="lokasiCustomer"><?php if (isset($do[0]['eksternal_address'])) {
																															echo $do[0]['eksternal_address'];
																														} ?></label><br>
					<label class="control-label" id="alamatCustomer"><?php
																														if (isset($do[0]['phone_1']) || isset($do[0]['phone_2']) || isset($do[0]['fax']) || isset($do[0]['email'])) {
																															echo 'Telp : ' . $do[0]['phone_1'] . ' / ' . $do[0]['phone_2'] . ' FAX : ' . $do[0]['fax'] . ' E-MAIL : ' . $do[0]['email'];
																														}
																														?></label><br>
					<label class="control-label" id="divCustomer"><?php if (isset($do[0]['pic'])) {
																													echo $do[0]['pic'];
																												} ?></label><br>
				</td>
			</tr>
		</tbody>
	</table>
	<script type="text/javascript">
		var dataImage = null;
		var t_doList;

		$(document).ready(function() {
			dtPacking();

			$('#btn_download').click(function() {
				var doc = new jsPDF('p', 'mm', 'a4');
				var imgData = dataImage;
				var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
				var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

				// FOOTER
				doc.setFontType('bold');
				doc.setTextColor(0, 0, 0);
				doc.addImage(imgData, 'JPEG', 5, 5, 25, 25)
				doc.setFontSize(12);
				doc.text($('#titleCelebit').html(), 33, 10, 'left');
				doc.setFontSize(11);
				doc.text($('#titlePerusahaan').html(), 33, 15, 'left');
				doc.setFontSize(11);
				doc.text($('#titleAlamat').html(), 33, 20, 'left');
				doc.setFontSize(11);
				doc.text($('#titleTlp').html(), 33, 25, 'left');

				doc.setDrawColor(116, 119, 122);
				doc.setLineWidth(1);
				doc.line(4, 30, 500, 30);

				doc.setFontSize(11);
				doc.text($('#titleInvoice').html(), pageWidth / 2, 40, 'center');

				// doc.setDrawColor(116, 119, 122);
				// doc.setLineWidth(0.1);
				// doc.line(4, 45, 110, 45);
				// doc.setLineWidth(0.1);
				// doc.line(4, 45, 4, 68);
				// doc.setLineWidth(0.1);
				// doc.line(110, 45, 110, 68);
				// doc.setLineWidth(0.1);
				// doc.line(4, 68, 110, 68);

				doc.setFontSize(10);
				// doc.setFontType('bold');
				// doc.text($('#namaCustomer').html(), 6, 50);
				// doc.text($('#lokasiCustomer').html(), 6, 55);
				// doc.text($('#alamatCustomer').html(), 6, 50);
				// doc.text($('#divCustomer').html(), 6, 65);
				doc.autoTable({
					html: '#tableCustomer',
					theme: 'plain',
					styles: {
						fontSize: 10,
						fontStyle: 'bold',
						lineColor: [116, 119, 122],
						lineWidth: 1,
						cellWidth: 'auto'

					},
					margin: 20,
					tableWidth: ((pageWidth / 2) - 20),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 48
				});
				
				doc.text($('#noInvoice1').html(), 135, 50, 'left');
				// doc.text($('#noInvoice2').html(), 160, 50, 'left');
				doc.text($('#no_do').val(), 175, 50, 'left');

				doc.text($('#tanggalInvoice1a').html(), 135, 57, 'left');
				doc.text($('#tanggalInvoice2a').html(), 160, 57, 'left');
				doc.text($('#tanggalInvoice3a').html(), 165, 57, 'left');

				doc.text($('#tanggalInvoice1b').html(), 135, 64, 'left');
				doc.text($('#tanggalInvoice2b').html(), 160, 64, 'left');
				doc.text($('#tanggalInvoice3b').html(), 165, 64, 'left');

				doc.text($('#noInvoicea').html(), 135, 71, 'left');
				doc.text($('#noInvoiceb').html(), 160, 71, 'left');
				doc.text($('#no_invoice').val(), 165, 71, 'left');

				doc.text($('#noBC1').html(), 135, 78, 'left');
				doc.text($('#noBC2').html(), 160, 78, 'left');
				doc.text($('#no_bc').val(), 165, 78, 'left');

				doc.text($('#attn1').html(), 135, 85, 'left');
				doc.text($('#attn2').html(), 160, 85, 'left');
				doc.text($('#attn').val(), 165, 85, 'left');
			
				doc.autoTable({
					html: '#listDOList',
					theme: 'plain',
					styles: {
						fontSize: 10,
						fontStyle: 'bold',
						lineColor: [116, 119, 122],
						lineWidth: 1,
						//cellWidth: 'auto',

					},
					margin: 20,
					tableWidth: (pageWidth - 30),
					headStyles: {
						valign: 'middle',
						halign: 'center'
					},
					didParseCell: function(data) {
						if (data.table.foot[0]) {
							if (data.table.foot[0].cells[0]) {
								data.table.foot[0].cells[0].styles.halign = 'left';
							}
							if (data.table.foot[0].cells[3]) {
								data.table.foot[0].cells[3].styles.halign = 'center';
							}
							if (data.table.foot[0].cells[4]) {
								data.table.foot[0].cells[4].styles.halign = 'center';
							}
						}
					},
					columnStyles: {
						0: {
							tableWidth: 10,
							halign: 'center',
							falign: 'center'
						},
						2: {
							halign: 'center',
							falign: 'left'
						},
						3: {
							halign: 'center',
							falign: 'right'
						},
						4: {
							halign: 'center',
							falign: 'center',
							cellWidth: 49
						},
						5: {
							halign: 'center',
							falign: 'center'
						},
						6: {
							halign: 'center',
							falign: 'center'
						},
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 90
				});

				var x = pageWidth * 70 / 100;
				var y = pageHeight * 35 / 100;
				var z = x + y;

				/*doc.setFontSize(10);
				doc.setFontType('bold');
				doc.text('PROV INV', (pageWidth / 2) + 10, 81);
				doc.text('BC', (pageWidth / 2) + 10, 86);
				doc.text(': '+$('#no_invoice').val(), (pageWidth / 2) + 30, 81);
				doc.text(': '+$('#no_bc').val(), (pageWidth / 2) + 30, 86);*/

				var lastTable = doc.autoTable.previous.finalY;

				doc.setFontSize(10);
				doc.setFontType("bold");
				doc.text($('#note_foot1').html(), 20, lastTable + 5);
				doc.text($('#note_foot2').html(), 20, lastTable + 10);

				doc.setFontSize(10);
				doc.setFontType("bold")
				doc.text($('#note_for_foot').html(), x, lastTable + 5, "center");
				doc.text($('#note_pt_foot').html(), x, lastTable + 10, "center");
				doc.text($('#nama_ttd').val(), x, lastTable + 50, 'center');

				doc.setFontSize(10);
				doc.setFontType('bold');
				doc.text($('#kode_report').html(), 205, pageHeight - 5, 'right');
				doc.text($('#kode_report_detail').html(), 205, pageHeight - 10, 'right');

				doc.setFontSize(10);
				
				var x = pageWidth * 80 / 100;
				var y = pageHeight * 35 / 100;
				// doc.text($('#nama_ttd').val(), 205, pageHeight - z, 'center');

				doc.save('DELIVERY ORDER <?php echo date('d-M-Y'); ?>.pdf');
			});

		});

		function toDataURL(url, callback) {
			var xhr = new XMLHttpRequest();
			xhr.onload = function() {
				var reader = new FileReader();
				reader.onloadend = function() {
					callback(reader.result);
				}
				reader.readAsDataURL(xhr.response);
			};
			xhr.open('GET', url);
			xhr.responseType = 'blob';
			xhr.send();
		}

		toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
			dataImage = dataUrl;
		})

		function numberCNT(row) {
			var rowData = t_doList.row(row).data();
			var no_cnt = rowData[1];
			var tot_qty = rowData[7];
			var tot_net = rowData[8];
			var tot_gross = rowData[9];

			var no_kont = $('#no_kontainer' + row).val();

			$('#tdQty').html(tot_qty);
			$('#tdNet').html(tot_net);
			$('#tdGross').html(tot_gross);
		}

		function dtPacking() {
			t_doList = $('#listDOList').DataTable({
				"processing": true,
				"searching": false,
				"paging": false,
				"responsive": true,
				"lengthChange": false,
				"info": false,
				"bSort": false,
				"ajax": {
					"type": "GET",
					"url": "<?php echo base_url() . 'delivery_order/detail_delivery_order_do/' . $id_do; ?>"
				},
				"columnDefs": [{
					"targets": [0],
					"searchable": false,
					"className": 'dt-body-center',
					"width": 50
				}, {
					"targets": [1, 2],
					"searchable": false,
					"className": 'dt-body-left',
				}, {
					"targets": [3],
					"searchable": false,
					"className": 'dt-body-center',
				}, {
					"targets": [4],
					"searchable": false,
					"className": 'dt-body-center',
				}]
			})
		}
	</script>