	<style>
		@primary-color: #242222;

		.col-customer {
			border: solid 1px #b2b8b7;
		}

		.dt-body-left {
			text-align: left;
			vertical-align: middle;
		}

		.dt-body-right {
			text-align: right;
			vertical-align: middle;
		}

		.dt-body-center {
			text-align: center;
			vertical-align: middle;
		}

		.titleReport {
			
		}

		.text-celebit{
			font-size: 18px;
		}
		
		.header-test{
			float: right;
		}

		#header-celebit .width-img {
			width: 25%;
			border-right-style: hidden;
		}

		.nama-perusahaan-margin {
			margin-top: 30px;
		}

		.margin-ttd {
			margin-bottom: 65px
		}

		#header-celebit .border-none {
			border: none;
			border-bottom: none;


			#header-invoice-num .border-none {
				border: none;
				border-bottom-style: hidden;
			}

			#header-celebit-invoice,
			#header-invoice-num .border-none {
				border: none;
				border-bottom-style: hidden;
			}

			#header-invoice-num .border-right-table {
				border-right-style: hidden;
			}

			.border-table {
				border: 1px solid #242222;
			}

			.border-table-invoice {
				border: 1px solid #242222;
				border-left-style: initial;
				border-right-style: initial;
			}

			.border-table-supplier {
				border: 1px solid #242222;
				border-top-style: initial;
				border-left-style: initial;
			}

			.border-table-bill {
				border: 1px solid #242222;
				border-top-style: initial;
				border-right-style: initial;
				border-left-style: initial;
			}

			.border-luar-table {
				border-style: double;
				border: 1px solid #242222;
			}
			#padding-celebit {
				padding-left: 27px;
			}
	</style>


	<form class="form-horizontal" id="form-invoice" role="form" action="<?php echo base_url('delivery_order/save_invoice'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	
		<div class="row">
			<div class="col-md-12">
				<div class="pull-right">
					<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download Invoice" id="btn_download">
						<i class="fa fa-download"></i>
					</a>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="col-md-2 logo-place">
					<img src="<?php echo site_url(); ?>assets/images/logo-celebit.jpg" alt="logo-celebit">
				</div>
				<div class="col-md-10">
					<div class="col-md-12 titleReport">
						<h1 id="titleCelebit">CELEBIT</h1>
						<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
						<h4 id="titleAlamat">BANDUNG FACTORY : JL.BUAH DUA RT.01/RW.04 RANCAEKEK - BANDUNG-INDONESIA</h4>
						<h4 id="titleTlp">TEL 62-22-7798 561/7798 542, FAX : 62-22-7798 562 E-MAIL : invoicing@celebit.com</h4>
					</div>
				</div>
			</div>
		</div>
	
		<div class="row">
			<div class="col-md-12">
				<table id="header-invoice" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom:0;">
					<thead>
						<tr>
							<th colspan="2" class="text-center">
								<h4 id="titleInvoice" style="font-weight:bold;">INVOICE</h4>
							</th>
						</tr>
					</thead>
					<tbody>
						<?php if ($invoice[0]['valas_id'] != 1) { 
							$tanggal_do = date_create($invoice[0]['tanggal_do']);
							$due_dates	= date_create($due_date); 
							$tanggal_invoice  = date_format($tanggal_do, "F d, Y");
							$due_date_invoice = date_format($due_dates, "F d, Y");
						} else { 
							$tanggal_do = date_create($invoice[0]['tanggal_do']); 
							$due_dates	= date_create($due_date); 
							$tanggal_invoice  = date_format($tanggal_do, "d M Y"); 
							$due_date_invoice = date_format($due_dates, "d M Y");
						} ?>
						<tr style="width:100%">
							<td style="width:50%; text-align: center;">
								<label>Invoice Number</label>&nbsp;<label>:</label>&nbsp;<br>
								<input type="text" class="form-control text-center" id="noInvoice" name="noInvoice" placeholder="Invoice Number" value="<?php if(isset($invoiced[0]['no_invoice'])) echo $invoiced[0]['no_invoice']; ?>"><label class="control-label" id="noInvoice-label" style="display:none;"></label>
							</td>
							<td style="width:50%; text-align: center;">
								<label>Invoice Date</label>&nbsp;<label>:</label>&nbsp;<br> 
								<label class="text-center" id="tanggalInvoice-label" name="tanggalInvoice-label" style="text-align:center;display:none;"></label>
								<div class="input-group date">
									<input placeholder="<?php echo $tanggal_invoice; ?>" style="text-align: center;" type="text" class="form-control datepicker" id="tanggalInvoice" name="tanggalInvoice" value="<?php if (isset($invoice[0]['tanggal_do'])) { echo $tanggal_invoice; } ?>" required>
									<div class="input-group-addon">
										<span class="glyphicon glyphicon-th"></span>
									</div>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<div class="col-md-12">
				<table id="header-celebit-invoice" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom:0;">
					<thead>
						<tr>
							<th class="text-center">
								<label class="control-label" id="titleAddressSupplier">Suppliers Address :</label>
							</th>
							<th class="text-center">
								<label class="control-label" id="titleAddressBill">Bill To Address :</label>
							</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td rowspan="3" style="width:50%">
								<label>Singapore Office :</label><br>
								<label>CELEBIT CIRCUIT TECHNOLOGY S.P. Ltd</label><br>
								<label>35 Kallang Puding Road</label><br>
								<label>349314 09-04 Tong Lee Building</label><br>
								<label>SG 349314- SINGAPORE</label><br>
								<label>Phone : 0065 66 786 2148</label><br>
								<label>Attn : </label><input type="text" class="form-control" id="attn" name="attn" value="<?php if (isset($invoice[0]['attention'])) echo $invoice[0]['attention']; ?>" placeholder="Attention"><label class="control-label" id="attn-label" name="attn-label" style="display:none;"></label><br><br>
								<label>Bandung Factory</label><br>
								<label>PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</label><br>
								<label>Jl. Buah Dua No. 168 RT.01/RW.04</label><br>
								<label>Rancaekek - Bandung</label><br>
								<label>Jawa Barat - Indonesia</label><br>
								<label>Phone : 006222 779 8561; Fax # 006222 779 8562</label><br>
								<label>Attn :  </label>&nbsp;<input type="text" class="form-control" id="attn1" name="attn1" value="" placeholder="Attention" width="100%"> <label class="control-label" id="attn1-label" style="display:none;"></label><br>
								<label>Rate :  </label>&nbsp;<input type="text" class="form-control" id="rate" name="rate" value="<?php if (isset($invoice[0]['rate'])) echo $invoice[0]['rate']; ?>" placeholder="Attention" width="100%"> <label class="control-label" id="rate-label" ></label><br>
							</td>
							<td style="width:50%">
								<label><?php if (isset($invoice[0]['name_eksternal'])) echo $invoice[0]['name_eksternal']; ?></label><br>
								<label><?php if (isset($invoice[0]['eksternal_address'])) echo $invoice[0]['eksternal_address']; ?></label>
							</td>
						</tr>
						<tr>
							<td colspan="1" class="text-center" style="width:50%;vertical-align:middle;font-weight:bold;">
								<label>Ship To Address :</label><br>
							</td>
						</tr>
						<tr>
							<th style="width:50%">
								<?php if (isset($invoice[0]['ship_address'])) echo $invoice[0]['ship_address']; ?>
								<label>Phone : </label>&nbsp;<input type="text" class="form-control" id="phone2" name="phone2" value="<?php if (isset($invoice[0]['phone_1'])) echo $invoice[0]['phone_1']; ?>" placeholder="Phone"><label class="control-label" id="phone2-label" style="display:none;"></label><br>
								<label>Attn  : </label>&nbsp;<input type="text" class="form-control" id="attn2" name="attn2" value="" placeholder="Attention"> <label class="control-label" id="attn2-label" style="display:none;"></label><br>
							</th>
						</tr>
						<tr>
							<th colspan="2"><label class="control-label" id="titleBeing"><label>Being Charge for Delivery of Goods Period :</label></th>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="col-md-12">
				<table id="listinvoice" class="table table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="margin-bottom:0;">
					<thead>
						<tr>
							<th rowspan="2">No</th>
							<th rowspan="2">ID D INVOICE</th>
							<th colspan="2" style="text-align:center;">Description</th>
							<th rowspan="2">Qty (Pcs)</th>
							<th rowspan="2">Unit Price (<?php if (isset($invoice[0]['symbol_valas'])) { echo $invoice[0]['symbol_valas']; } ?>)</th>
							<th rowspan="2">Unit Price Hid(<?php if (isset($invoice[0]['symbol_valas'])) { echo $invoice[0]['symbol_valas']; } ?>)</th>
							<th rowspan="2">Amount (<?php if (isset($invoice[0]['symbol_valas'])) { echo $invoice[0]['symbol_valas']; } ?>)</th>
							<th rowspan="2">Amount Hid(<?php if (isset($invoice[0]['symbol_valas'])) { echo $invoice[0]['symbol_valas']; } ?>)</th>
							<th rowspan="2">STBJ Hid</th>
						</tr>
						<tr>
							<th class="text-center">Part No</th>
							<th class="text-center">PO</th>
						</tr>
					</thead>
					<tbody></tbody>
					<tfoot>
						<tr>
							<td colspan="8"></td>
						</tr>
						<tr>
							<th colspan="7" style="text-align: left;">TOTAL <?php if (isset($invoice[0]['symbol_valas'])) { echo $invoice[0]['symbol_valas']; } ?></th>
							<th id="tdAmount_Total" style="text-align: right;">
								<!--<label id="symbol-amount_total" name="symbol-amount_total"><?php if(isset($invoice[0]['symbol'])) echo $invoice[0]['symbol']; ?>.  </label>-->
								<label id="amount_total_label" name="amount_total_label" style="min-width: 115px; padding-left: 5%;text-align:right;"></label>
								<input type="hidden" id="total" name="total" value="">
								<input type="hidden" id="ppn_harga" name="ppn_harga" value="">
							</th>
						</tr>
						<tr>
							<th colspan="8" style="text-align: left;" id="totalBilangan" name="totalBilangan"></th>
						</tr>
						<tr>
							<td colspan="4" style="width:50%;border-right-style:none;border-bottom-style:none;">
								<label>Term Of Payment</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>:</label>&nbsp;<input type="text" style="width:100%" class="form-control" id="top" name="top" placeholder="Days Net" value="<?php if(isset($invoice[0]['price_term'])) echo $invoice[0]['price_term']; ?>" lang="en-150" required><label class="control-label" id="top-label" name="top-label" style="display:none;"></label><br>
								<label>Term Of Delivery</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>:</label>&nbsp;<input type="text" style="width:100%" class="form-control" id="tod" name="tod" placeholder="Term Of Delivery" value="" required><label class="control-label" id="tod-label" name="tod-label" style="display:none;"></label><br>
								<label>PO Number</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>:</label>&nbsp;<input type="text" style="width:100%" class="form-control" id="PONumber" name="PONumber" placeholder="As Mentioned" value="As Mentioned" required><label class="control-label" id="PONumber-label" name="PONumber-label" style="display:none;"></label><br>
								<label>Harmony System #</label>&nbsp;&nbsp;<label> :</label>&nbsp;<input type="text" style="width:100%" class="form-control" id="harmonySystem" name="harmonySystem" placeholder="8534.00.1000" value="8534.00.1000" required><label class="control-label" id="harmonySystem-label" name="harmonySystem-label" style="display:none;"></label><br>
								<label>Shippment Form</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label> :</label>&nbsp;<input type="text" style="width:100%" class="form-control" id="shippmentForm" name="shippmentForm" placeholder="Jakarta Port - Indonesia" value="Jakarta Port - Indonesia" required><label class="control-label" id="shippmentForm-label" name="shippmentForm-label" style="display:none;"></label><br>
								<label>Shippment To</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>:</label>&nbsp;<input type="text" style="width:100%" class="form-control" id="shippmentTo" name="shippmentTo" placeholder="Shippment To" value="" required><label class="control-label" id="shippmentTo-label" name="shippmentTo-label" style="display:none;"></label>
							</td>
							<td colspan="4" style="width:50%;border-bottom-style:none;">
								<label>Delivery By</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>:</label>&nbsp;<input type="text" style="width:100%" class="form-control" id="deliveryBy" name="deliveryBy" placeholder="Courier" value="" required><label class="control-label" id="deliveryBy-label" name="deliveryBy-label" style="display:none;"></label><br>
								<label>Delivery Method</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>:</label>&nbsp;<input type="text" style="width:100%" class="form-control" id="delveryMethod" name="delveryMethod" placeholder="Delivery Method" value="" required><label class="control-label" id="delveryMethod-label" name="delveryMethod-label" style="display:none;"></label><br>
								<label>ETD</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>:</label>&nbsp;<label id="etd-label" name="etd-label" style="display:none;"></label> <div class="input-group date" style="width:100%"><input placeholder="<?php echo date('F d, Y'); ?>" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="etd" name="etd" value="<?php echo date('F d, Y'); ?>" required><div class="input-group-addon"><span class="glyphicon glyphicon-th"></span></div></div><br>
								<label>Vessel Name</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>:</label>&nbsp;<input type="text" style="width:100%" class="form-control" id="vesselName" name="vesselName" placeholder="Vessel Name" value="" required><label class="control-label" id="vesselName-label" name="vesselName-label" style="display:none;"></label><br>
								<label>AWB Number</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>:</label>&nbsp;<input type="text" style="width:100%" class="form-control" id="awbNumber" name="awbNumber" placeholder="AWB Number" value="" required><label class="control-label" id="awbNumber-label" name="awbNumber-label" style="display:none;"></label><br>
								<label>Nett/ Gross Weight</label>&nbsp;&nbsp;<label>:</label>&nbsp;<input type="text" style="width:100%" class="form-control" id="nettGross" name="nettGross" placeholder="Nett / Gross Weight" value="" required><label class="control-label" id="nettGross-label" name="nettGross-label" style="display:none;"></label>
							</td>
						</tr>
						<tr>
							<td colspan="4" style="width:50%; border: 1px solid #ebf2f2">
								<label>BANK TRANSFER</label>&nbsp;<label>:</label><br>
								<label>BANK NAME</label>&nbsp;&nbsp;<label>:</label>&nbsp;<label>BANK NEGARA INDONESIA (BNI)</label><br>
								&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label> Jl. Buah Batu No. 189 D Bandung</label><br>
								<label>Phone</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>:</label><label>022-7313371</label><br>
								<label>Account Number (US $)</label>&nbsp;&nbsp;<label>:</label>&nbsp;<input type="text" style="width:100%" class="form-control" id="accountNumber" name="accountNumber" placeholder="Account Number" value="" required>&nbsp;<label class="control-label" id="accountNumber-label" name="accountNumber-label" style="display:none;"></label><br>
								<label>Swift Code</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label>:</label><input type="text" style="width:100%" class="form-control" id="swiftCode" name="swiftCode" placeholder="Swift Code" value="" required>&nbsp;<label class="control-label" id="swiftCode-label" name="swiftCode-label" style="display:none;"></label><br>
								<label>Account Name</label>&nbsp;&nbsp;<label>:</label>&nbsp;<label>PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</label>
							</td>
							<td class="text-center" colspan="4" style="width:50%;text-align: center;">
								<label class="text-center">Signature</label>
								<br><br><br><br><br>
								<input type="text" style="width:100%" class="form-control text-center" id="signature" name="signature" placeholder="Signature Name" value="" required>
								<br><br><br>
								<label class="text-center" id="signature-label" name="signature-label" style="display:none;"></label>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="item col-md-12">
				<div class="col-md-2 col-md-offset-10">
					<div class="col-md-12 pull-right">
						<input type="hidden" class="form-control" id="id_eks" name="id_eks" required="required" value="<?php if(isset($invoice[0]['id_eks'])) echo $invoice[0]['id_eks']; ?>" placeholder="ID Eksternal">
						<input type="hidden" class="form-control" id="id_do" name="id_do" required="required" value="<?php if(isset($invoice[0]['id_do'])) echo $invoice[0]['id_do']; ?>" placeholder="ID DO">
						<input type="hidden" class="form-control" id="id_invoice" name="id_invoice" required="required" value="<?php if(isset($invoice[0]['id_invoice'])) echo $invoice[0]['id_invoice']; ?>" placeholder="ID Invoice">
						<input type="hidden" class="form-control" id="id_valas" name="id_valas" required="required" value="<?php if(isset($invoice[0]['valas_id'])) echo $invoice[0]['valas_id']; ?>" placeholder="ID Valas">
						<input type="hidden" id="total_amount" name="total_amount" value="">
						<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Edit</button>
					</div>
				</div>
			</div>
		</div>
	</form>
	
	<div class="row">
		<input type="hidden" id="totHide" name="totHide">
	</div>
	<div class="row">
		<div class="col-md-12">
			<label class="control-label pull-right" id="kode_report">F_MCI-002</label>
		</div>
		<div class="col-md-12">
			<label class="control-label pull-right" id="kode_report_detail">REV : 00</label>
		</div>
	</div>
	<script type="text/javascript">
		var dataImage = null;
		var invoice = <?php echo count($invoice); ?>;
		var nama_valas = '<?php if (isset($invoice[0]['nama_valas'])) {echo $invoice[0]['nama_valas'];} ?>';

		var t_invoice_list;

		$(document).ready(function() {
			dtInvoice();
			$("#tanggalInvoice").datepicker({
				format: 'MM dd, yyyy',
				autoclose: true,
				todayHighlight: true
			}).on("change", function() {
				$("#tanggalInvoice-label").text(this.value);
				$("#dueDateInvoice-label").text(this.value);
			});
			
			$("#etd").datepicker({
				format: 'MM dd, yyyy',
				autoclose: true,
				todayHighlight: true,
			}).on("change", function() {
				$("#etd-label").text(this.value);
			});
			
			$(".datexx").datepicker({
				format: 'MM dd, yyyy',
				autoclose: true,
				todayHighlight: true,
			});
			
			$(".datex").datepicker({
				format: 'yyyy-mm-dd',
				autoclose: true,
				todayHighlight: true,
			});
			
			$("#notes-input").on("keyup", function() {
				$("#notes-label").text($("#notes-input").val());
			})

			$("#noInvoice").on("keyup", function() {
				$("#noInvoice-label").text($("#noInvoice").val());
			})
			$("#attn").on("keyup", function() {
				$("#attn-label").text($("#attn").val());
			})
			$("#attn1").on("keyup", function() {
				$("#attn1-label").text($("#attn1").val());
			})
			$("#phone2").on("keyup", function() {
				$("#phone2-label").text($("#phone2").val());
			})
			$("#attn2").on("keyup", function() {
				$("#attn2-label").text($("#attn2").val());
			})
			$("#top").on("keyup", function() {
				$("#top-label").text($("#top").val());
			})
			$("#deliveryBy").on("keyup", function() {
				$("#deliveryBy-label").text($("#deliveryBy").val());
			})
			$("#tod").on("keyup", function() {
				$("#tod-label").text($("#tod").val());
			})
			$("#delveryMethod").on("keyup", function() {
				$("#delveryMethod-label").text($("#delveryMethod").val());
			})
			$("#PONumber").on("keyup", function() {
				$("#PONumber-label").text($("#PONumber").val());
			})
			$("#etd").on("keyup", function() {
				$("#etd-label").text($("#etd").val());
			})
			$("#harmonySystem").on("keyup", function() {
				$("#harmonySystem-label").text($("#harmonySystem").val());
			})
			$("#vesselName").on("keyup", function() {
				$("#vesselName-label").text($("#vesselName").val());
			})
			$("#shippmentForm").on("keyup", function() {
				$("#shippmentForm-label").text($("#shippmentForm").val());
			})
			$("#shippmentTo").on("keyup", function() {
				$("#shippmentTo-label").text($("#shippmentTo").val());
			})
			$("#awbNumber").on("keyup", function() {
				$("#awbNumber-label").text($("#awbNumber").val());
			})
			$("#nettGross").on("keyup", function() {
				$("#nettGross-label").text($("#nettGross").val());
			})
			$("#accountNumber").on("keyup", function() {
				$("#accountNumber-label").text($("#accountNumber").val());
			})
			$("#swiftCode").on("keyup", function() {
				$("#swiftCode-label").text($("#swiftCode").val());
			})
			$("#signature").on("keyup", function() {
				$("#signature-label").text($("#signature").val());
			})
		
			for (var i = 0; i < invoice.length; i++) {
				$("#unit_price" + i).on("keyup", function() {
					total = parseFloat($("#total-label").text().replace(/[^\d.-]/g, ''));

					// console.log(parseFloat($("#total-label").text())+parseFloat($("#ppn-label").text()),parseFloat($("#total-label").text()),parseFloat($("#ppn-label").text()));
					$('#amount_total_label').html(formatCurrencyComaIDR(total));
					$('#total_amount').val(total);
					//$("#tdAmount_Total").html(formatCurrencyComaUSD(total));
					$("#totalBilangan").html(terbilang(total));
				})
			}

			$('#btn_download').click(function() {
				var doc = new jsPDF('p', 'mm', 'letter');
				var imgData = dataImage;
				var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
				var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
				
				// FOOTER
				doc.setFontType('bold');
				doc.setTextColor(0, 0, 0);
				doc.addImage(imgData, 'JPEG', 20, 15, 25, 25)
				doc.setFontSize(11);
				doc.text($('#titleCelebit').html(), 50, 20, 'left');
				doc.setFontSize(11);
				doc.text($('#titlePerusahaan').html(), 50, 25, 'left');
				doc.setFontSize(10);
				doc.text($('#titleAlamat').html(), 50, 30, 'left');
				doc.setFontSize(10);
				doc.text($('#titleTlp').html(), 50, 35, 'left');

				doc.setDrawColor(116, 119, 122);
				doc.setLineWidth(0.7);
				doc.line(20, 10, 206, 10);
				doc.line(20, 40, 20, 10);
				doc.line(206, 40, 206, 10);
			
				doc.autoTable({
					html: '#header-invoice',
					theme: 'plain',
					styles: {
						fontSize: 10,
						fontStyle: 'bold',
						lineColor: [116, 119, 122],
						lineWidth: 0.7
					},
					margin: 20,
					tableWidth: (pageWidth - 30),
					headStyles: {
						halign: 'center'
					},
					columns: [
						{ header: "Invoice Number", dataKey: "inv_numb" },
						{ header: "Invoice Date", dataKey: "inv_date" }
					],
					columnStyles: {
						inv_numb: { cellWidth: 50 },
						inv_date: { cellWidth: 50, halign: 'center', balign: 'center', falign: 'center'},
					},
					bodyStyles: {
						fontStyle: 'bold',
						halign: 'center',
						falign: 'center'
					},
					didParseCell: function(data) {
						console.log(data.table.body[0]);
						if (data.table.body[0]) {
							if (data.table.body[0].cells['inv_numb']) {
								data.table.body[0].cells['inv_numb'].styles.halign = 'center';
								data.table.body[0].cells['inv_numb'].styles.balign = 'center';
								data.table.body[0].cells['inv_numb'].styles.falign = 'center';
							}
							if (data.table.body[0].cells['inv_date']) {
								data.table.body[0].cells['inv_date'].styles.halign = 'center';
								data.table.body[0].cells['inv_date'].styles.balign = 'center';
								data.table.body[0].cells['inv_date'].styles.falign = 'center';
							}
						}
					},
					pageBreak: 'auto',
					startY: 40
				});
				
				var startYPIN = doc.autoTable.previous.finalY;
				
				doc.autoTable({
					html: '#header-celebit-invoice',
					theme: 'plain',
					styles: {
						fontSize: 10,
						fontStyle: 'bold',
						lineColor: [116, 119, 122],
						lineWidth: 0.7,
						cellWidth: 'auto'
					},
					columns: [
						{ header: "Supplier Address", dataKey: "supp_addr" },
						{ header: "Bill To Address", dataKey: "bill_addr" }
					],
					columnStyles: {
						supp_addr: { cellWidth: 50 },
						bill_addr: { cellWidth: 50, balign: 'center'},
					},
					margin: 20,
					tableWidth: (pageWidth - 30),
					headStyles: {
						valign: 'middle',
						halign: 'center'
					},
					bodyStyles: {
						valign: 'middle',
						halign: 'left'
					},
					rowPageBreak: 'auto',
					startY: startYPIN
				});
				
				var startYPB = doc.autoTable.previous.finalY;
				
				doc.autoTable({
					html: '#listinvoice',
					theme: 'plain',
					styles: {
						fontSize: 10,
						fontStyle: 'bold',
						lineColor: [116, 119, 122],
						lineWidth: 0.7,
						cellWidth: 'auto',
						// overflow: 'linebreak'
					},
					margin: 20,
					tableWidth: (pageWidth - 30),
					headStyles: {
						valign: 'middle',
						halign: 'center'
					},
					didParseCell: function(data) {
						if (data.table.foot[0]) {
							if (data.table.foot[0].cells[4]) {
								data.table.foot[0].cells[4].styles.halign = 'right';
							}
							if (data.table.foot[0].cells[5]) {
								data.table.foot[0].cells[5].styles.halign = 'right';
								data.table.foot[1].cells[5].styles.overflow = 'linebreak';
								data.table.foot[0].cells[5].text = formatCurrencyComaIDR($('#amount_total_label').html());
							}
						}
						if (data.table.foot[1]) {
							if (data.table.foot[1].cells[5]) {
								data.table.foot[1].cells[5].styles.halign = 'right';
								data.table.foot[1].cells[5].styles.falign = 'right';
								data.table.foot[1].cells[5].styles.overflow = 'linebreak';
								data.table.foot[1].cells[5].text = formatCurrencyComaIDR($('#amount_total_label').html());
							}
						}
						if (data.table.foot[4]) {
							if (data.table.foot[4].cells[3]) {
								data.table.foot[4].cells[3].styles.halign = 'center';
								data.table.foot[4].cells[3].styles.balign = 'center';
								data.table.foot[4].cells[3].styles.falign = 'center';
							}
						}
					},
					columnStyles: {
						0: {halign: 'center', cellWidth: 15},
						1: {halign: 'left', cellWidth: 52},
						2: {halign: 'center', cellWidth: 30},
						3: {halign: 'right', cellWidth: 20},
						4: {halign: 'right', cellWidth: 33},
						5: {halign: 'right',falign: 'left', cellWidth: 33},
						6: {halign: 'right',falign: 'left', cellWidth: 33},
						7: {halign: 'right',falign: 'left', cellWidth: 33},
						8: {halign: 'right',falign: 'left', cellWidth: 33}
					},
					pageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: startYPB
				});
				
				var startYPO = doc.autoTable.previous.finalY;
				
				doc.save('INVOICE <?php echo $invoice[0]['name_eksternal'].'-'.$invoice[0]['tanggal_do']; ?>.pdf');
			});
		});
		
		$('#form-invoice').on('submit',(function(e) {
			if($('#no_invoice').val() !== ''){
				$('#btn-submit').attr('disabled','disabled');
				$('#btn-submit').text("Memasukkan data...");
				e.preventDefault();
				var formData = new FormData(this);
				
				$.ajax({
					type:'POST',
					url: $(this).attr('action'),
					data:formData,
					cache:false,
					contentType: false,
					processData: false,
					success: function(response) {
						if (response.success == true) {
							save_invoice(response.lastid,response.id_valas,response.id_eksternal,response.total_amount,response.ppn_harga,response.ppn);
						} else{
							$('#btn-submit').removeAttr('disabled');
							$('#btn-submit').text("Simpan");
							swal("Failed!", response.message, "error");
						}
					}
				}).fail(function(xhr, status, message) {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Simpan");
					swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
				});
			}else{
				swal("Failed!", "Maaf kolom Keterangan harus diisi", "error");
			}
			return false;
		}));
		
		function save_invoice(lastid,id_valas,id_eks,total_amount,ppn_harga,ppn) {
			var arrTemp = [];
			for (var i = 0; i < t_invoice_list.rows().data().length; i++) {
				var rowData = t_invoice_list.row(i).data();
				arrTemp.push(rowData); 
			}

			var datapost = {
				'id_invoice' 	 : lastid,
				'id_valas'		 : id_valas,
				'id_eksternal'	 : id_eks,
				'total_amount'	 : total_amount,
				'ppn_harga'	 	 : ppn_harga,
				'ppn'		 	 : ppn,
				'list_invoice'	 : arrTemp
			};
			
			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>delivery_order/insert_detail_invoice",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function () {
							window.location.href = "<?php echo base_url('delivery_order');?>";
						})
					}else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Simpan");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Simpan");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}
	
		function toDataURL(url, callback) {
			var xhr = new XMLHttpRequest();
			xhr.onload = function() {
				var reader = new FileReader();
				reader.onloadend = function() {
					callback(reader.result);
				}
				reader.readAsDataURL(xhr.response);
			};
			xhr.open('GET', url);
			xhr.responseType = 'blob';
			xhr.send();
		}

		toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
			dataImage = dataUrl;
		})

		function dtInvoice() {
			t_invoice_list = $('#listinvoice').DataTable({
				"processing": true,
				"searching": false,
				"responsive": true,
				"paging": false,
				"lengthChange": false,
				"info": false,
				"bSort": false,
				"ajax": {
					"type": "GET",
					"url": "<?php echo base_url() . 'delivery_order/delivery_list_invoice/' . $id_do; ?>"
				},
				"columnDefs": [{
					"targets": [0],
					"searchable": false,
					"className": 'dt-body-center',
					"width": 10
				}, {
					"targets": [2],
					"searchable": false,
					"className": 'dt-body-center',
					"width": 250
				}, {
					"targets": [3],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 150
				}, {
					"targets": [4],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 150
				}, {
					"targets": [5],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 60
				}, {
					"targets": [1,6,8,9],
					"searchable": false,
					"visible": false,
					"className": 'dt-body-right',
					"width": 60
				}, {
					"targets": [7],
					"searchable": false,
					"className": 'dt-body-right',
					"width": 150
				}],
				"footerCallback": function(row, data, start, end, display) {
					var api = this.api(),
						data;
					var total = 0;
					var intVal = function(i) {
						return typeof i === 'string' ?
							i.replace(/[\$,]/g, '') * 1 :
							typeof i === 'number' ?
							i : 0;
					};

					// Total over all pages
					total = api
						.column(8)
						.data()
						.reduce(function(a, b) {
							total = total + intVal(a) + intVal(b);
							return (intVal(a) + intVal(b));
						}, 0);

					// Total over this page
					pageTotal = api
						.column(8, {
							page: 'current'
						})
						.data()
						.reduce(function(a, b) {
							total = total + intVal(a) + intVal(b);
							return (intVal(a) + intVal(b));
						}, 0);
					
					// Update footer
					$(api.column(8).footer()).html(
						'<?php $invoice[0]['symbol_valas']; ?>. ' + formatCurrencyComaUSD(pageTotal)
					)
					
					$("#total-label").text(formatCurrencyComaIDR(pageTotal));
					$("#noInvoice-label").text($("#noInvoice").val());
					$("#tanggalInvoice-label").text($("#tanggalInvoice").val());
					$("#dueDateInvoice-label").text($("#tanggalInvoice").val());
					$("#attn-label").text($("#attn").val());
					$("#attn1-label").text($("#attn1").val());
					$("#phone2-label").text($("#phone2").val());
					$("#attn2-label").text($("#attn2").val());
					$("#top-label").text($("#top").val());
					$("#delveryBy-label").text($("#delveryBy").val());
					$("#tod-label").text($("#tod").val());
					$("#delveryMethod-label").text($("#delveryMethod").val());
					$("#PONumber-label").text($("#PONumber").val());
					$("#etd-label").text($("#etd").val());
					$("#harmonySystem-label").text($("#harmonySystem").val());
					$("#vesselName-label").text($("#vesselName").val());
					$("#shippmentForm-label").text($("#shippmentForm").val());
					$("#shippmentTo-label").text($("#shippmentTo").val());
					$("#awbNumber-label").text($("#awbNumber").val());
					$("#nettGross-label").text($("#nettGross").val());
					$("#accountNumber-label").text($("#accountNumber").val());
					$("#swiftCode-label").text($("#swiftCode").val());
					$("#signature-label").text($("#signature").val());
					
					$('#amount_total_label').html(formatCurrencyComaIDR(pageTotal));
					$('#total_amount').val(formatCurrencyComaIDR(pageTotal));
					// $("#tdAmount_Total").html(formatCurrencyRupiah(total));
					$('#totHide').val(pageTotal);
					
					<?php if ($invoice[0]['valas_id'] == 1) { ?>
						terbilangIND();
					<?php } else { ?>
						terbilangENG();
					<?php } ?>
				}
			});
		}

		function terb_depan(uang) {
			var sub = '';
			if (uang == 1) {
				sub = 'Satu '
			} else
			if (uang == 2) {
				sub = 'Dua '
			} else
			if (uang == 3) {
				sub = 'Tiga '
			} else
			if (uang == 4) {
				sub = 'Empat '
			} else
			if (uang == 5) {
				sub = 'Lima '
			} else
			if (uang == 6) {
				sub = 'Enam '
			} else
			if (uang == 7) {
				sub = 'Tujuh '
			} else
			if (uang == 8) {
				sub = 'Delapan '
			} else
			if (uang == 9) {
				sub = 'Sembilan '
			} else
			if (uang == 0) {
				sub = '  '
			} else
			if (uang == 10) {
				sub = 'Sepuluh '
			} else
			if (uang == 11) {
				sub = 'Sebelas '
			} else
			if ((uang >= 11) && (uang <= 19)) {
				sub = terb_depan(uang % 10) + 'Belas ';
			} else
			if ((uang >= 20) && (uang <= 99)) {
				sub = terb_depan(Math.floor(uang / 10)) + 'Puluh ' + terb_depan(uang % 10);
			} else
			if ((uang >= 100) && (uang <= 199)) {
				sub = 'Seratus ' + terb_depan(uang - 100);
			} else
			if ((uang >= 200) && (uang <= 999)) {
				sub = terb_depan(Math.floor(uang / 100)) + 'Ratus ' + terb_depan(uang % 100);
			} else
			if ((uang >= 1000) && (uang <= 1999)) {
				sub = 'Seribu ' + terb_depan(uang - 1000);
			} else
			if ((uang >= 2000) && (uang <= 999999)) {
				sub = terb_depan(Math.floor(uang / 1000)) + 'Ribu ' + terb_depan(uang % 1000);
			} else
			if ((uang >= 1000000) && (uang <= 999999999)) {
				sub = terb_depan(Math.floor(uang / 1000000)) + 'Juta ' + terb_depan(uang % 1000000);
			} else
			if ((uang >= 100000000) && (uang <= 999999999999)) {
				sub = terb_depan(Math.floor(uang / 1000000000)) + 'Milyar ' + terb_depan(uang % 1000000000);
			} else
			if ((uang >= 1000000000000)) {
				sub = terb_depan(Math.floor(uang / 1000000000000)) + 'Triliun ' + terb_depan(uang % 1000000000000);
			}
			return sub;
		}

		function terb_belakang(t) {
			if (t.length == 0) {
				return '';
			}
			return t
				.split('0').join('Kosong ')
				.split('1').join('Satu ')
				.split('2').join('Dua ')
				.split('3').join('Tiga ')
				.split('4').join('Empat ')
				.split('5').join('Lima ')
				.split('6').join('Enam ')
				.split('7').join('Tujuh ')
				.split('8').join('Delapan ')
				.split('9').join('Dembilan ');
		}

		function terbilang(nangka) {
			var
				v = 0,
				sisa = 0,
				tanda = '',
				tmp = '',
				sub = '',
				subkoma = '',
				p1 = '',
				p2 = '',
				pkoma = 0;
			// nangka = nangka.replace(/[^\d.-]/g, '');
			if (nangka > 999999999999999999) {
				return 'Buset dah buanyak amat...';
			}
			v = nangka;
			if (v < 0) {
				tanda = 'Minus ';
			}
			v = Math.abs(v);
			tmp = v.toString().split('.');
			p1 = tmp[0];
			p2 = '';
			// console.log(parseFloat(tmp[1]), 0, parseFloat(tmp[1]) > 0)
			if (parseFloat(tmp[1]) > 0) {
				p2 = tmp[1];
			}
			v = parseFloat(p1);
			sub = terb_depan(v);
			/* sisa = parseFloat('0.'+p2);
			subkoma = terb_belakang(sisa); */
			subkoma = 'Koma ' + terb_belakang(p2).replace('  ', ' ');;
			if (subkoma == 'Koma ')
				subkoma = '';
			sub = tanda + sub.replace('  ', ' ') + subkoma;
			return sub.replace('  ', ' ') + " Rupiah";
		}

		function terbilangIND() {
			// var bilangan = document.getElementById("totHide").value;
			var bilangan = formatCurrencyComaIDR($("#amount_total_label").val());
			var kalimat = terbilang(bilangan);
			
			document.getElementById("totalBilangan").innerHTML = kalimat;
		}

		function toWordsconver(s) 
		{
			var th_val = ['', 'Thousand', 'Million', 'Billion', 'Trillion'];
			var dg_val = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];
			var tn_val = ['Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
			var tw_val = ['Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];
			s = s.toString();
			s = s.replace(/[\, ]/g, '');
			if (s != parseFloat(s))
				return 'not a number ';
			var x_val = s.indexOf('.');
			if (x_val == -1)
				x_val = s.length;
			if (x_val > 15)
				return 'too big';
			var n_val = s.split('');
			var str_val = '';
			var sk_val = 0;
			for (var i = 0; i < x_val; i++) {
				if ((x_val - i) % 3 == 2) {
					if (n_val[i] == '1') {
						str_val += tn_val[Number(n_val[i + 1])] + ' ';
						i++;
						sk_val = 1;
					} else if (n_val[i] != 0) {
						str_val += tw_val[n_val[i] - 2] + ' ';
						sk_val = 1;
					}
				} else if (n_val[i] != 0) {
					str_val += dg_val[n_val[i]] + ' ';
					if ((x_val - i) % 3 == 0)
						str_val += 'Hundred ';
					sk_val = 1;
				}
				if ((x_val - i) % 3 == 1) {
					if (sk_val)
						str_val += th_val[(x_val - i - 1) / 3] + ' ';
					sk_val = 0;
				}
			}
			// console.log(dg_val, n_val);
			if (x_val != s.length) {
				var y_val = s.length;
				str_val += 'And Cents ';
				for (var i = x_val + 1; i < y_val; i++) {
					// str_val += dg_val[n_val[i]] + ' ';
					if ((y_val - i) % 3 == 2) {
						if (n_val[i] == '1') {
							str_val += tn_val[Number(n_val[i + 1])] + ' ';
							i++;
							sk_val = 1;
						}else if (n_val[i] != 0) {
							str_val += tw_val[n_val[i] - 2] + ' ';
							sk_val = 1;
						}
					}else if (n_val[i] != 0) {
						str_val += dg_val[n_val[i]] + ' ';
						if ((x_val - i) % 3 == 0)
							str_val += 'Hundred ';
						sk_val = 1;
					}

					if ((y_val - i) % 3 == 1) {
						if (sk_val)
							str_val += th_val[(y_val - i - 1) / 3] + ' ';
						sk_val = 0;
					}
				}
			}
			return "Says : ( " + str_val.replace(/\s+/g, ' ') + " Only )";
		}

		function terbilangENG() {
			// var bilangan = $("#tdAmount_Total").html();
			// var bilangan = document.getElementById("totHide").value;
			var bilangan = $("#total_amount").val();
			//console.log(bilangan);
			var kalimat = toWordsconver(bilangan);
			
			document.getElementById("totalBilangan").innerHTML = kalimat;
		}


		function cal_price(row) {
			var total = 0;
			var rowData = t_invoice_list.row(row).data();
			var amount = rowData[6];

			var qtykoma = rowData[4];
			var qty = qtykoma.replace(/[^\d.-]/g, '');
			var unitPrice = $('#unit_price' + row).val();
			var total = numberWithCommas($('#total-label').html());

			amount = parseFloat(unitPrice) * parseFloat(qty);

			$('input[id="amount' + row + '"]').val(parseFloat(amount).toFixed(2));
			<?php if ($invoice[0]['valas_id'] == 1) { ?>
				$("#amount-label" + row).html(formatCurrencyRupiah(amount));
				$("#unit_price-label" + row).html(formatCurrencyRupiah(parseFloat(unitPrice)));
			<?php } else { ?>
				$("#amount-label" + row).html(formatCurrencyComaUSD(amount));
				$("#unit_price-label" + row).html(formatCurrencyComaUSD(parseFloat(unitPrice)));
			<?php } ?>
			var tempTotal = 0;
			var totalSeluruh = 0;
			$('input[name="amount[]"]').each(function() {
				if (this.value !== '' && this.value !== null) {
					tempTotal = tempTotal + parseFloat(this.value);
					totalSeluruh = parseFloat(tempTotal);
				} else {
					tempTotal = tempTotal + 0;
					totalSeluruh = tempTotal + 0;
				}
			})
			
			$('#total-label').text(formatCurrencyComaIDR(tempTotal));
			$('#amount_total_label').html(formatCurrencyComaIDR(tempTotal));
			// $('#tdAmount_Total').html(formatCurrencyComaIDR(tempTotal + (tempTotal * (ppn_total / 100))));
			// $("#totalBilangan").html(terbilang(tempTotal));
			
			document.getElementById("totalBilangan").innerHTML = toWordsconver(tempTotal);
		}

		function redrawTable(row) {
			var rowData = t_invoice_list.row(row).data();

			rowData[6] = $('#unit_price' + row).val();
			rowData[8] = $('#amount' + row).val();
			t_invoice_list.draw();
		}
		
		function numberWithCommas(x) {
			var parts = x.toString().split(".");
			parts[0] = parts[0].replace(",", "");
			return parts.join(".");
		}
	</script>