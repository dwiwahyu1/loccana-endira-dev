	<style>
		#loading-us{display:none}
		#tick{display:none}

		#loading-mail{display:none}
		#cross{display:none}
		.add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
		.add_item:hover{color:#ff8c00}
		.right-text{text-align:right}
		.x-hidden{display:none}
	</style>

	<form class="form-horizontal form-label-left" id="delivery_order_add" role="form" action="<?php echo base_url('delivery_order/insert_delivery_order');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

  		<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
		<br>
        <div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_do">No DO <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input placeholder="No. DO" type="text" class="form-control" id="no_do" name="no_do" required autocomplete="off">
				<span id="loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking No DO...</span>
				<span id="tick"></span>
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_do">Tanggal Delivery <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<div class="input-group date">
					<input placeholder="<?php echo date('Y-m-d'); ?>" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="tanggal_do" autocomplete="off" name="tanggal_do" placeholder="<?php echo date('Y-m-d'); ?>" required>
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-th"></span>
					</div>
				</div>
			</div>
		</div>
		
		<div class="item form-group x-hidden">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="detail_box">Detail Box <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<textarea placeholder="Detail Box" class="form-control" id="detail_box" name="detail_box" autocomplete="off"></textarea>
			</div>
		</div>

		<div class="item form-group x-hidden">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_pi">No PI <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input placeholder="No. PI" type="text" class="form-control" id="no_pi" name="no_pi" autocomplete="off">
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="remark">Remark <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<textarea placeholder="Remark" class="form-control" id="remark" name="remark" required autocomplete="off"></textarea>
			</div>
		</div>
		
		<div class="item form-group x-hidden">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_bc">No BC <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<select class="form-control" id="no_bc" name="no_bc" style="width: 100%" autocomplete="off">
					<option value="">Tidak ada No Pendaftaran</option>
					<?php foreach($pendaftaran as $pend => $bc) { ?>
						<option value="<?php echo $bc['id'];?>"><?php echo $bc['no_pendaftaran']; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>
		
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Barang : </label>
			<div class="col-md-8 col-sm-6 col-xs-12 add_item">
				<a class="btn btn-primary" onclick="delivery_order_add_item()">
					<i class="fa fa-plus"></i> Tambah Barang STBJ
				</a> 
				<input type="hidden" id="tambah_barang" name="tambah_barang" value="0">
			</div>
		</div>

	
		<div class="item form-group">
			<table id="listaddstbj" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th></th>
						<th>No STBJ</th>
						<th>Tanggal STBJ</th>
						<th>Part No</th>
						<th>Detail Box</th>
						<th>No PI</th>
						<th>Remark</th>
						<th></th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	
		<hr>
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Simpan</button>
			</div>
		</div>
	</form>
	
	<script type="text/javascript">
	var dataBarang = [];
	var t_addSTBJ;
	var last_no_do = $('#no_do').val();
	
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
		
		$('#tanggal_do').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true,
			changeYear: true,
			minDate: '-3M',
			maxDate: '+30D',
		});
		
		// $('#no_bc').select2();
		dtPacking();
	});
	
	$('#no_do').on('input',function(event) {
		if($('#no_do').val() != last_no_do) {
			no_do_check();
		}
	});

	function no_do_check() {
		var no_do = $('#no_do').val();
		if(no_do.length > 3) {
			var post_data = {
				'no_do': no_do
			};

			$('#tick').empty();
			$('#tick').hide();
			$('#loading-us').show();
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url('delivery_order/check_no_do');?>",
				data: post_data,
				cache: false,
				success: function(response){
					if(response.success == true){
						$('#no_do').css('border', '3px #090 solid');
						$('#loading-us').hide();
						$('#tick').empty();
						$("#tick").append('<span class="fa fa-check"> '+response.message+'</span>');
						$('#tick').show();
					}else {
						$('#no_do').css('border', '3px #C33 solid');
						$('#loading-us').hide();
						$('#tick').empty();
						$("#tick").append('<span class="fa fa-close"> '+response.message+'</span>');
						$('#tick').show();
					}
				}
			});
		}else {
			$('#no_do').css('border', '3px #C33 solid');
			$('#loading-us').hide();
			$('#tick').empty();
			$("#tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
			$('#tick').show();
		}
	}

	function dtPacking() {
		t_addSTBJ = $('#listaddstbj').DataTable( {
			"processing": true,
			"searching": false,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"visible": false,
				"targets": [0],
				"searchable": false
			}]
		});
	}
	
	function delivery_order_add_item(){
		$('#panel-modal-detail').removeData('bs.modal');
		$('#panel-modal-detail  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal-detail  .panel-body').load('<?php echo base_url('delivery_order/delivery_order_add_item');?>');
		$('#panel-modal-detail  .panel-title').html('<i class="fa fa-plus"></i> Tambah Packing Barang');
		$('#panel-modal-detail').modal({backdrop:'static',keyboard:false},'show');
	}

	$('#listaddstbj').on("click", "button", function(){
		t_addSTBJ.row($(this).parents('tr')).remove().draw(false);
		for (var i = 0; i < t_addSTBJ.rows().data().length; i++) {
			var rowData = t_addSTBJ.row(i).data();
		}
	});
	
	$('#delivery_order_add').on('submit',(function(e) {
		if($('#no_do').val() !== ''){
			$('#btn-submit').attr('disabled','disabled');
			$('#btn-submit').text("Memasukkan data...");
			e.preventDefault();
			var formData = new FormData(this);
			var data_stbj = t_addSTBJ.rows().data().length;
			
			formData.set('data_stbj',data_stbj);
			
			$.ajax({
				type:'POST',
				url: $(this).attr('action'),
				data:formData,
				cache:false,
				contentType: false,
				processData: false,
				success: function(response) {
					if(response.data_stbj > 0) {
						if (response.success == true) {
							save_packing(response.lastid,response.lastid_inv);
						} else{
							$('#btn-submit').removeAttr('disabled');
							$('#btn-submit').text("Simpan");
							swal("Failed!", response.message, "error");
						}
					}else{
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Simpan");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Simpan");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}else{
			swal("Failed!", "Maaf kolom Keterangan harus diisi", "error");
		}
		return false;
	}));
	
	function save_packing(lastid,id_inv) {
		
		var arrTemp = [];
		for (var i = 0; i < t_addSTBJ.rows().data().length; i++) {
			var rowData = t_addSTBJ.row(i).data();
			arrTemp.push(rowData); 
		}

		var datapost = {
			"id_do"				: lastid,
			"id_invoice"		: id_inv,
			'liststbj'			: arrTemp
		};
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>delivery_order/insert_detail_delivery_order",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						$('#panel-modal').modal('toggle');
						listdelivery();
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Simpan");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Simpan");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}
	
	function isNumber(evt) {
		var iKeyCode = (evt.which) ? evt.which : evt.keyCode
		if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
			return false;

		return true;
	}
	</script>