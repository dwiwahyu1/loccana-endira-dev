<style>
	.col-customer {
		border: solid 1px #b2b8b7;
	}

	.dt-body-left {
		text-align: left;
		vertical-align: middle;
	}

	.dt-body-right {
		text-align: right;
		vertical-align: middle;
	}

	.dt-body-center {
		text-align: center;
		vertical-align: middle;
	}
	
	.titleReport {
		text-align: center;
		margin-left: -116px;
	}

	img {
		width: 95%;
		max-width: 85%;
		height: auto;
	}

	.nama-perusahaan-margin {
		margin-top: 30px;

	}

	.margin-ttd {
		margin-bottom: 65px
	}
</style>

<form class="form-horizontal" id="form-invoice" role="form" action="<?php echo base_url('delivery_order/save_invoice'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download Invoice" id="btn_download">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="col-md-2 logo-place">
				<img src="<?php echo site_url(); ?>assets/images/logo-celebit.jpg" alt="logo-celebit">
			</div>
			<div class="col-md-10">
				<div class="col-md-12 titleReport">
					<h1 id="titleCelebit">CELEBIT</h1>
					<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
					<h4 id="titleAlamat">BANDUNG FACTORY : JL.BUAH DUA RT.01/RW.04 RANCAEKEK - BANDUNG-INDONESIA</h4>
					<h4 id="titleTlp">TEL 62-22-7798 561/7798 542, FAX : 62-22-7798 562 E-MAIL : invoicing@celebit.com</h4>
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-12 text-center">
			<h2 id="titleInvoice" style="font-weight:900;">INVOICE</h2>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="col-md-8 col-customer">
				<div class="row">
					<div class="col-md-12">
						<table id="print_table_customer">
							<tr>
								<td>
									<label class="control-label" id="namaCustomer"><?php if (isset($invoice[0]['name_eksternal'])) { echo $invoice[0]['name_eksternal']; } ?></label><br>
									<label class="control-label" id="lokasiCustomer"><?php if (isset($invoice[0]['eksternal_address'])) { echo $invoice[0]['eksternal_address']; } ?></label><br>
									<label class="control-label" id="alamatCustomer"><?php if (isset($invoice[0]['phone_1']) || isset($invoice[0]['phone_2']) || isset($invoice[0]['fax']) || isset($invoice[0]['email'])) { echo 'Telp : ' . $invoice[0]['phone_1'] . ' / ' . $invoice[0]['phone_2'] . ' FAX : ' . $invoice[0]['fax'] . ' E-MAIL : ' . $invoice[0]['email']; } ?>
									</label><br>
									<label class="control-label" id="divCustomer"><?php //if (isset($invoice[0]['eksternal_loc'])) { echo $invoice[0]['eksternal_loc']; } ?></label><br>
								</td>
							</tr>
						</table>

					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-3" id="noInvoice1">No.</label>
						<label class="control-label col-md-1" id="noInvoice2">:</label>
						<div class="col-md-8">
							<input class="form-control" id="noInvoice" name="noInvoice" placeholder="No Invoice" value="<?php if (isset($invoiced[0]['no_invoice'])) { echo $invoiced[0]['no_invoice']; } ?>" required>
						</div>
					</div>
				</div>
				
				<?php if ($invoice[0]['valas_id'] != 1) { 
					$tanggal_do = date_create($invoice[0]['tanggal_do']);
					$due_dates	= date_create($due_date); 
					$tanggal_invoice  = date_format($tanggal_do, "F d, Y");
					$due_date_invoice = date_format($due_dates, "F d, Y");
				} else { 
					$tanggal_do = date_create($invoice[0]['tanggal_do']); 
					$due_dates	= date_create($due_date); 
					$tanggal_invoice  = date_format($tanggal_do, "d M Y"); 
					$due_date_invoice = date_format($due_dates, "d M Y");
				} ?>
				
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-3" id="tanggalInvoice1">Date</label>
						<label class="control-label col-md-1" id="tanggalInvoice2">:</label>
						<div class="col-md-8">
							<div class="input-group date">
								<input placeholder="<?php echo $tanggal_invoice; ?>" type="text" class="form-control datepicker" id="tanggalInvoice" name="tanggalInvoice" required="required" value="<?php if (isset($invoice[0]['tanggal_do'])) { echo $tanggal_invoice; } ?>">
								<label class="control-label" id="noInvoice-label" style="display:none;"></label><div class="input-group-addon">
									<span class="glyphicon glyphicon-th"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-3" id="dueDateInvoice1">Due Date</label>
						<label class="control-label col-md-1" id="dueDateInvoice2">:</label>
						<div class="col-md-8">
							<div class="input-group date">
								<input placeholder="<?php echo $due_date_invoice; ?>" type="text" class="form-control datepicker" id="dueDateInvoice" name="dueDateInvoice" required="required" value="<?php if (isset($due_date)) { echo $due_date_invoice; } ?>">
								<div class="input-group-addon">
									<span class="glyphicon glyphicon-th"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
					
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-3" id="attn1">Attn</label>
						<label class="control-label col-md-1" id="attn2">:</label>
						<div class="col-md-8">
							<?php /*if(isset($invoiced[0]['attention'])) {
								$attention = explode(' ',$invoiced[0]['attention']);
									if($attention != 0){
										$x = 6;
										$z = 0;
										for($i=0;$i < sizeof($attention);$i++){
											if($x > 5 ){
												$attns = $attention[$i];
												print_r($attns);
											}else{
												$attns = $attention[$i];
											}
											$attn = $attns;
										} die;
										
										// $attn = $attention[0].' '.$attention[1].' '.$attention[2].' '.$attention[3].' '.$attention[4].' '.$attention[5].' "\n" '.$attention[6];
									}
								}print_r($attns);*/
							?>
							<input class="form-control" id="attn" name="attn" placeholder="Attention" value="<?php echo $attn; ?>" required></input>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-3" id="attn1">Rate</label>
						<label class="control-label col-md-1" id="attn2">:</label>
						<div class="col-md-8">
							<?php /*if(isset($invoiced[0]['attention'])) {
								$attention = explode(' ',$invoiced[0]['attention']);
									if($attention != 0){
										$x = 6;
										$z = 0;
										for($i=0;$i < sizeof($attention);$i++){
											if($x > 5 ){
												$attns = $attention[$i];
												print_r($attns);
											}else{
												$attns = $attention[$i];
											}
											$attn = $attns;
										} die;
										
										// $attn = $attention[0].' '.$attention[1].' '.$attention[2].' '.$attention[3].' '.$attention[4].' '.$attention[5].' "\n" '.$attention[6];
									}
								}print_r($attns);*/
							?>
							<input class="form-control" id="rate" name="rate" placeholder="Attention" value="<?php if (isset($invoice[0]['rate'])) { echo $invoice[0]['rate']; } ?>" required></input>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<table id="listinvoice" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th rowspan="2">No DO</th>
						<th rowspan="2">ID D INVOICE</th>
						<th colspan="2" style="text-align:center;">Description</th>
						<th rowspan="2">Qty (Pcs)</th>
						<th rowspan="2">Unit Price (<?php if (isset($invoice[0]['symbol_valas'])) { echo $invoice[0]['symbol_valas']; } ?>)</th>
						<th rowspan="2">Unit Price Hid(<?php if (isset($invoice[0]['symbol_valas'])) { echo $invoice[0]['symbol_valas']; } ?>)</th>
						<th rowspan="2">Amount (<?php if (isset($invoice[0]['symbol_valas'])) { echo $invoice[0]['symbol_valas']; } ?>)</th>
						<th rowspan="2">Amount Hid(<?php if (isset($invoice[0]['symbol_valas'])) { echo $invoice[0]['symbol_valas']; } ?>)</th>
						<th rowspan="2">STBJ Hid</th>
					</tr>
					<tr>
						<th>Part No</th>
						<th>PO</th>
					</tr>
				</thead>
				<tbody></tbody>
				<tfoot>
					<tr>
						<td colspan="7" style="margin-left:10px;text-align: left;">
							<label class="control-label">TOTAL</label>
							<br>
								<label class="control-label">PPN : </label> <label id="ppn-percentage-label" name="ppn-percentage-label" style="display: none;"></label><label>%</label>
								<input class="form-control text-right" style="width:50%;" type="text" id="ppn-input" name="ppn-input" value="<?php if(isset($invoiced[0]['ppn'])) echo $invoiced[0]['ppn']; else echo 10; ?>" 
								placeholder="Insert PPN" style="width: 10%; margin-right: 8px; display: inherit;" required>

							<br><br>
								
								<h4>Bank Transfer :</h4>
								<br>BANK : BANK NEGARA INDONESIA (BNI)
								<br>SWIFT CODE : BNINIDJA
								<br>Alamat : Jl. Buah Batu No 189 D Bandung Telp 022-7313371
								<br>No Rekening (Rp) : 77-1688-1688 (Rp)
								<br>No Rekening (USD) : 1688-1688-72(USD)
								<br>Account Name : PT CELEBIT CIRCUIT TECHNOLOGY INDONESIA
								
							<br><br>
								
								<label class="control-label">NOTES : </label> <label id="notes-label" name="notes-label" style="display:none;"></label>	
								<input class="form-control " type="text" id="notes-input" name="notes-input" value="<?php if(isset($invoiced[0]['note'])) echo $invoiced[0]['note'];
								?>" placeholder="Note" style="width: 50%; margin-right: 8px; display: inherit;">
						</td>
						<td class="text-right">
							<br>
								<label id="total-label" name="total-label" style="min-width: 0px; padding-left: 5%;"></label>
							<br>
								<label id="ppn-label" name="ppn-label" style="min-width: 0px; padding-left: 5%;"></label>
						</td>
					</tr>
					<tr>
						<th colspan="7" style="text-align: left;">TOTAL <?php if(isset($invoice[0]['symbol_valas'])) echo $invoice[0]['symbol_valas']; ?></th>
						<th id="tdAmount_Total" style="text-align:right;">
							<label style="text-align: right;" id="amount_total_label" name="amount_total_label"></label>
						</th>
					</tr>
					<tr>
						<th colspan="8" id="totalBilangan" name="totalBilangan"></th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>

	
	<div class="row">
		<div class="item form-group">
			<div class="col-md-2 col-md-offset-10">
				<div class="col-md-12 pull-right">
					<input type="hidden" class="form-control" id="id_eks" name="id_eks" required="required" value="<?php if(isset($invoice[0]['id_eks'])) echo $invoice[0]['id_eks']; ?>" placeholder="ID Eksternal">
					<input type="hidden" class="form-control" id="id_do" name="id_do" required="required" value="<?php if(isset($invoice[0]['id_do'])) echo $invoice[0]['id_do']; ?>" placeholder="ID DO">
					<input type="hidden" id="total" name="total" value="">
					<input type="hidden" id="ppn_harga" name="ppn_harga" value="">
					<input type="hidden" id="total_amount" name="total_amount" value="">
					<input type="hidden" id="id_valas" name="id_valas" value="<?php echo $invoice[0]['valas_id']; ?>">
					<input type="hidden" id="id_invoice" name="id_invoice" value="<?php echo $invoice[0]['id_invoice']; ?>">
					<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Edit</button>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<table id="tandatangan_page" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th style="width:70%;border:hidden;">
						</th>
						<th style="width:30%;border:hidden;">
							<div class="col-md-12 text-center">
								<label class="control-label margin-ttd text-center" style="margin-right: 20px;" id="tandatangan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</label>
								
								<br><br><br><br><br><br>
								
								<input class="form-control text-center" id="penandaTangan" style="margin-top: 30px;" name="penandaTangan" value="<?php if(isset($invoiced[0]['sign'])) echo $invoiced[0]['sign']; ?>">
								<label class="control-label margin-ttd text-center" id="penandaTangan-label" style="display:none"></label>
							</div>
						</th>
					</tr>
				</thead>
			</table>
		</div>
	</div>

	<br><br><br><br><br><br><br><br>

	<div class="row">
		<input type="hidden" id="totHide" name="totHide">
		<label id="totHide2" name="totHide2" style="display:none;";></label>
	</div>

	<div class="row" id="kode">
		<div class="col-md-12">
			<label class="control-label pull-right" id="kode_report">F_MCI-002</label>
		</div>
		<div class="col-md-12">
			<label class="control-label pull-right" id="kode_report_detail">REV : 00</label>
		</div>
	</div>
</form>

<script type="text/javascript">
	var dataImage = null;
	var t_invoice_list;

	$(document).ready(function() {
		dtInvoice();
		$("#tanggalInvoice").datepicker({
			format: 'MM dd, yyyy',
			autoclose: true,
			todayHighlight: true,
		});
		
		$("#dueDateInvoice").datepicker({
			format: 'MM dd, yyyy',
			autoclose: true,
			todayHighlight: true,
		});
		
		$(".datexx").datepicker({
			format: 'MM dd, yyyy',
			autoclose: true,
			todayHighlight: true,
		});
		
		$(".datex").datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});
		
		$("#penandaTangan").on("keyup", function() {
			$("#penandaTangan-label").text($("#penandaTangan").val());
		});
		
		$("#notes-input").on("keyup", function() {
			$("#notes-label").text($("#notes-input").val());
		});
		
		$("#ppn-input").on("keyup", function() {
			total = parseFloat($("#total-label").text().replace(/[^\d.-]/g, ''));
			ppn_percentage = parseFloat($("#ppn-input").val());
			$("#ppn-percentage-label").text($("#ppn-input").val());

			$("#ppn-label").text(formatCurrencyComaIDR(total * (ppn_percentage / 100)));
			ppn = ppn_percentage * total / 100;
			var totppn = total + ppn;
			
			$('#amount_total_label').html(formatCurrencyComaIDR(totppn));
			$('#total_amount').val(totppn);
			$('#ppn_harga').val(total * (ppn_percentage / 100));
			$('#total').val(total);
			
			$('#totHide').val(total + ppn);
			$('#totHide2').html(total + ppn);
			<?php if ($invoice[0]['valas_id'] == 1) { ?>
				terbilangIND();
			<?php } else { ?>
				terbilangENG();
			<?php } ?>
		})

		$('#btn_download').click(function() {
			var doc = new jsPDF('p', 'mm', 'letter');
			var number_of_pages = doc.internal.getNumberOfPages()
			var pdf_pages = doc.internal.pages
			
			var imgData = dataImage;
			var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
			var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
			
			// FOOTER
			doc.setFont("helvetica");
			doc.setFontType('bold');
			doc.setTextColor(0, 0, 0);
			doc.addImage(imgData, 'JPEG', 5, 5, 25, 25)
			doc.setFontSize(12);
			doc.text($('#titleCelebit').html(), 33, 10, 'left');
			doc.setFontSize(11);
			doc.text($('#titlePerusahaan').html(), 33, 15, 'left');
			doc.setFontSize(11);
			doc.text($('#titleAlamat').html(), 33, 20, 'left');
			doc.setFontSize(11);
			doc.text($('#titleTlp').html(), 33, 25, 'left');

			doc.setDrawColor(116, 119, 122);
			doc.setLineWidth(0.7);
			doc.line(4, 30, 210, 30);

			doc.setFontSize(11);
			doc.text($('#titleInvoice').html(), pageWidth / 2, 40, 'center');

			doc.setFontSize(11);

			doc.autoTable({
				html: '#print_table_customer',
				theme: 'plain',
				styles: {
					fontSize: 10,
					fontStyle: 'bold',
					lineColor: [116, 119, 122],
					lineWidth: 0.7,
					cellWidth: 'auto',
					overflow: 'linebreak'
				},
				margin: 20,
				tableWidth: ((pageWidth / 2) - 20),
				headStyles: {
					valign: 'middle',
					halign: 'center',
				},
				columnStyles: {
					0: {
						halign: 'left'
					},
				},
				rowPageBreak: 'auto',
				showHead: 'firstPage',
				showFoot: 'lastPage',
				startY: 45
			});

			doc.setFontSize(10);
			doc.text($('#noInvoice1').html(), 125, 50, 'left');
			doc.text($('#noInvoice2').html(), 150, 50, 'left');
			doc.text($('#noInvoice').val(), 155, 50, 'left');

			doc.text($('#tanggalInvoice1').html(), 125, 57, 'left');
			doc.text($('#tanggalInvoice2').html(), 150, 57, 'left');
			doc.text($('#tanggalInvoice').val(), 155, 57, 'left');

			doc.text($('#dueDateInvoice1').html(), 125, 64, 'left');
			doc.text($('#dueDateInvoice2').html(), 150, 64, 'left');
			doc.text($('#dueDateInvoice').val(), 155, 64, 'left');
			
			doc.text($('#attn1').html(), 125, 71, 'left');
			doc.text($('#attn2').html(), 150, 71, 'left');
			// doc.text($('#attn').val(), 155, 71, 'left');
			doc.fromHTML($('#attn').val(), 154, 64,  {
				'text-align': 'left',
				'font-family': 'helvetica',
				'font': 'helvetica',
				'font-weight': 'bold'
			});

			doc.autoTable({
				html: '#listinvoice',
				theme: 'plain',
				styles: {
					fontSize: 10,
					fontStyle: 'bold',
					lineColor: [116, 119, 122],
					lineWidth: 0.7,
					cellWidth: 'auto',
					overflow: 'linebreak'
				},
				margin: 20,
				tableWidth: (pageWidth - 30),
				headStyles: {
					valign: 'middle',
					halign: 'center',
				},
				footStyles: {
					rowPageBreak: 'auto',
					overflow: 'linebreak'
				},
				didParseCell: function(data) {
					console.log(data.table.foot[1]);
					if (data.table.foot[0]) {
						if (data.table.foot[0].cells[5]) {
							data.table.foot[0].cells[5].styles.halign = 'right';
							data.table.foot[0].cells[5].styles.falign = 'right';
						}
					}
					if (data.table.foot[1]) {
						if (data.table.foot[1].cells[5]) {
							data.table.foot[1].cells[5].styles.halign = 'right';
							data.table.foot[1].cells[5].styles.falign = 'right';
						}
					}
				},
				columnStyles: {
					0: {halign: 'left', cellWidth: 15},
					1: {halign: 'left', cellWidth: 52},
					2: {halign: 'center', cellWidth: 30},
					3: {halign: 'right', cellWidth: 20},
					4: {halign: 'right', cellWidth: 33},
					5: {halign: 'right',falign: 'left', cellWidth: 33},
					6: {halign: 'right',falign: 'left', cellWidth: 33},
					7: {halign: 'right',falign: 'left', cellWidth: 33},
					8: {halign: 'right',falign: 'left', cellWidth: 33}
				},
				footStyles: {
					1: {halign: 'right',valign: 'right',falign: 'right'}
				},
				pageBreak: 'auto',
				showHead: 'firstPage',
				showFoot: 'lastPage',
				startY: doc.autoTable.previous.finalY + 10
			});
			
			doc.autoTable({
				html: '#tandatangan_page',
				theme: 'plain',
				styles: {
					fontSize: 10,
					fontStyle: 'bold',
					lineWidth: 0,
					cellWidth: 'auto',
					overflow: 'linebreak'
				},
				margin: 10,
				tableWidth: (pageWidth - 30),
				headStyles: {
					valign: 'middle',
					halign: 'center',
					falign: 'center',
				},
				columnStyles: {
					0: {halign: 'center', cellWidth: 15},
					1: {halign: 'center', cellWidth: 15},
				},
				pageBreak: 'auto',
				showHead: 'firstPage',
				showFoot: 'lastPage',
				startY: doc.autoTable.previous.finalY + 10
			});
			
			doc.setFontSize(10);
			var x = pageWidth * 70 / 100;
			var y = pageHeight * 75 / 100;
			var startYPO = doc.autoTable.previous.finalY + 5
			var pageHeight2 = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
			
			doc.page = 1;
			
			var myFooter1 = $('#kode_report').html();
			var myFooter2 = $('#kode_report_detail').html();
			
			for (var i = 1; i < pdf_pages.length; i++) {
				// if(startYPO > (pageHeight - 20.4)) {
					// doc.addPage();
					// startYP = 0;
					
					// doc.setFontType('bold');
					// doc.text($('#tandatangan').html(), x - 40, startYP + 20, {
						// 'overflow': 'linebreak',
						// 'pageBreak': 'always',
						// 'margin-top': 40,
						// 'falign': 'center',
						// 'halign': 'center',
						// 'font-family': 'Arial',
						// 'font-weight': 'bold',
						// 'fontSize': 10
					// });
					// doc.text($('#penandaTangan').val(), x - 40, startYP + 50, {
						// 'overflow': 'linebreak',
						// 'falign': 'center',
						// 'margin-top': 70,
						// 'halign': 'center',
						// 'text-align': 'center',
						// 'font-family': 'Arial',
						// 'font-weight': 'bold',
						// 'fontSize': 10
					// });
				// }else{
					// doc.setFontType('bold');
					// doc.fromHTML($('#tandatangan').html(), x - 40, startYPO,  {
						// 'overflow': 'linebreak',
						// 'pageBreak': 'always',
						// 'falign': 'center',
						// 'halign': 'center',
						// 'font-family': 'Arial',
						// 'font-weight': 'bold',
						// 'fontSize': 10
					// });
					// doc.fromHTML($('#penandaTangan').val(), x - 40, startYPO + 30,  {
						// 'overflow': 'linebreak',
						// 'falign': 'center',
						// 'text-align': 'center',
						// 'halign': 'center',
						// 'font-family': 'Arial',
						// 'font-weight': 'bold',
						// 'fontSize': 10
					// });
				// }
				
				doc.setPage(i)
				doc.text(myFooter1, 185, pageHeight - 5)
				doc.text(myFooter2, 185, pageHeight - 10)
				doc.page ++;
			}
			
			doc.save('INVOICE <?php echo $invoice[0]['name_eksternal'].'-'.$invoice[0]['tanggal_do']; ?>.pdf');
		});
	});

	$('#form-invoice').on('submit',(function(e) {
		var id_invoices = '<?php echo $invoice[0]['id_invoice']; ?>';
		var id_valases = '<?php echo $invoice[0]['valas_id']; ?>';
		
		if($('#no_invoice').val() !== ''){
			$('#btn-submit').attr('disabled','disabled');
			$('#btn-submit').text("Memasukkan data...");
			e.preventDefault();
			var formData = new FormData(this);
			
			$.ajax({
				type:'POST',
				url: $(this).attr('action'),
				data:formData,
				cache:false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						save_invoice(response.lastid,id_invoices,id_valases,response.id_eksternal,response.total_amount,response.ppn_harga,response.ppn);
					} else{
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Simpan");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Simpan");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}else{
			swal("Failed!", "Maaf kolom Keterangan harus diisi", "error");
		}
		return false;
	}));
	
	function save_invoice(lastid,id_invoices,id_valas,id_eks,total_amount,ppn_harga,ppn) {
		
		var arrTemp = [];
		for (var i = 0; i < t_invoice_list.rows().data().length; i++) {
			var rowData = t_invoice_list.row(i).data();
			arrTemp.push(rowData); 
		}
		
		var datapost = {
			'id_invoice' 	 : id_invoices,
			'id_valas'		 : id_valas,
			'id_eksternal'	 : id_eks,
			'total_amount'	 : total_amount,
			'ppn_harga'	 	 : ppn_harga,
			'ppn'		 	 : ppn,
			'list_invoice'	 : arrTemp
		};
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>delivery_order/insert_detail_invoice",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						window.location.href = "<?php echo base_url('delivery_order');?>";
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Simpan");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Simpan");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}
	
	function toDataURL(url, callback) {
		var xhr = new XMLHttpRequest();
		xhr.onload = function() {
			var reader = new FileReader();
			reader.onloadend = function() {
				callback(reader.result);
			}
			reader.readAsDataURL(xhr.response);
		};
		xhr.open('GET', url);
		xhr.responseType = 'blob';
		xhr.send();
	}

	toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
		dataImage = dataUrl;
	})

	function dtInvoice() {
		t_invoice_list = $('#listinvoice').DataTable({
			"processing": true,
			"searching": false,
			"responsive": true,
			"paging": false,
			"lengthChange": false,
			"destroy": true,
			"info": false,
			"bSort": false,
			"ajax": {
				"type": "GET",
				"url": "<?php echo base_url() . 'delivery_order/delivery_list_invoice/' . $id_do; ?>"
			},
			"columnDefs": [{
				"targets": [0],
				"searchable": true,
				"className": 'dt-body-left',
				"width": 40
			}, {
				"targets": [2],
				"searchable": false,
				"className": 'dt-body-left',
				"width": 80
			}, {
				"targets": [3],
				"searchable": false,
				"className": 'dt-body-right',
				"width": 80
			}, {
				"targets": [4],
				"searchable": false,
				"className": 'dt-body-right',
				"width": 60
			}, {
				"targets": [5],
				"searchable": false,
				"className": 'dt-body-right',
				"width": 40
			}, {
				"targets": [1,6,8,9],
				"searchable": false,
				"visible": false,
				"className": 'dt-body-right',
				"width": 40
			}, {
				"targets": [7],
				"searchable": false,
				"className": 'dt-body-right',
				"width": 40
			}],
			"footerCallback": function(row, data, start, end, display) {
				var api = this.api(),
					data;
				var total = 0;
				// Remove the formatting to get integer data for summation
				var intVal = function(i) {
					return typeof i === 'string' ?
						i.replace(/[\$,]/g, '') * 1 :
						typeof i === 'number' ?
						i : 0;
				};

				// Total over all pages
				total = api
					.column(8)
					.data()
					.reduce(function(a, b) {
						// total = total + parseFloat(a) + parseFloat(b);
						return (parseFloat(a) + parseFloat(b));
					}, 0);

				// Total over this page
				pageTotal = api
					.column(8, {
						page: 'current'
					})
					.data()
					.reduce(function(a, b) {
						// total = total + parseFloat(a) + parseFloat(b);
						return (parseFloat(a) + parseFloat(b));
					}, 0);
				
				// Update footer
				$(api.column(8).footer()).html(
					'<?php $invoice[0]['symbol_valas']; ?>. ' + formatCurrencyComaIDR(total * 110 / 100)
				)

				var percent = parseFloat($("#ppn-input").val());
				
				var totals = (total * percent / 100) + total;
				
				$("#total-label").text(formatCurrencyComaIDR(total));
				$("#ppn-percentage-label").text($("#ppn-input").val());
				$("#notes-label").text($("#notes-input").val());
				$("#penandaTangan-label").text($("#penandaTangan").val());
				$("#ppn-label").text(formatCurrencyComaIDR(total * percent / 100));
				$('#totHide2').html(formatCurrencyComaIDR(totals));
				$('#total_amount').val(totals);
				$('#ppn_harga').val(total * percent / 100);
				$('#total').val(total);
				
				var total2 = Math.floor(totals);
				<?php if ($invoice[0]['valas_id'] == 1) { ?>
					$('#totHide').val(total2);
					$('#amount_total_label').html(formatCurrencyComaIDRX(totals));
					terbilangIND();
				<?php } else { ?>
					$('#totHide').val(totals.toFixed(2));
					$('#amount_total_label').html(formatCurrencyComaIDR(totals));
					terbilangENG();
				<?php } ?>
				
			}
		});
	}

	function terb_depan(uang) {
		var sub = '';
		if (uang == 1) {
			sub = 'Satu '
		} else
		if (uang == 2) {
			sub = 'Dua '
		} else
		if (uang == 3) {
			sub = 'Tiga '
		} else
		if (uang == 4) {
			sub = 'Empat '
		} else
		if (uang == 5) {
			sub = 'Lima '
		} else
		if (uang == 6) {
			sub = 'Enam '
		} else
		if (uang == 7) {
			sub = 'Tujuh '
		} else
		if (uang == 8) {
			sub = 'Delapan '
		} else
		if (uang == 9) {
			sub = 'Sembilan '
		} else
		if (uang == 0) {
			sub = '  '
		} else
		if (uang == 10) {
			sub = 'Sepuluh '
		} else
		if (uang == 11) {
			sub = 'Sebelas '
		} else
		if ((uang >= 11) && (uang <= 19)) {
			sub = terb_depan(uang % 10) + 'Belas ';
		} else
		if ((uang >= 20) && (uang <= 99)) {
			sub = terb_depan(Math.floor(uang / 10)) + 'Puluh ' + terb_depan(uang % 10);
		} else
		if ((uang >= 100) && (uang <= 199)) {
			sub = 'Seratus ' + terb_depan(uang - 100);
		} else
		if ((uang >= 200) && (uang <= 999)) {
			sub = terb_depan(Math.floor(uang / 100)) + 'Ratus ' + terb_depan(uang % 100);
		} else
		if ((uang >= 1000) && (uang <= 1999)) {
			sub = 'Seribu ' + terb_depan(uang - 1000);
		} else
		if ((uang >= 2000) && (uang <= 999999)) {
			sub = terb_depan(Math.floor(uang / 1000)) + 'Ribu ' + terb_depan(uang % 1000);
		} else
		if ((uang >= 1000000) && (uang <= 999999999)) {
			sub = terb_depan(Math.floor(uang / 1000000)) + 'Juta ' + terb_depan(uang % 1000000);
		} else
		if ((uang >= 100000000) && (uang <= 999999999999)) {
			sub = terb_depan(Math.floor(uang / 1000000000)) + 'Milyar ' + terb_depan(uang % 1000000000);
		} else
		if ((uang >= 1000000000000)) {
			sub = terb_depan(Math.floor(uang / 1000000000000)) + 'Triliun ' + terb_depan(uang % 1000000000000);
		}
		return sub;
	}

	function terb_belakang(t) {
		if (t.length == 0) {
			return '';
		}
		return t
			.split('0').join('Kosong ')
			.split('1').join('Satu ')
			.split('2').join('Dua ')
			.split('3').join('Tiga ')
			.split('4').join('Empat ')
			.split('5').join('Lima ')
			.split('6').join('Enam ')
			.split('7').join('Tujuh ')
			.split('8').join('Delapan ')
			.split('9').join('Sembilan ');
	}

	function terbilang(nangka) {
		var
			v = 0,
			sisa = 0,
			tanda = '',
			tmp = '',
			sub = '',
			subkoma = '',
			p1 = '',
			p2 = '',
			pkoma = 0;
		// nangka = nangka.replace(/[^\d.-]/g, '');
		if (nangka > 999999999999999999) {
			return 'Buset dah buanyak amat...';
		}
		v = nangka;
		if (v < 0) {
			tanda = 'Minus ';
		}
		v = Math.abs(v);
		tmp = v.toString().split('.');
		p1 = tmp[0];
		p2 = '';
		// console.log(parseFloat(tmp[1]), 0, parseFloat(tmp[1]) > 0)
		if (parseFloat(tmp[1]) > 0) {
			p2 = tmp[1];
		}
		v = parseFloat(p1);
		sub = terb_depan(v);
		/* sisa = parseFloat('0.'+p2);
		subkoma = terb_belakang(sisa); */
		subkoma = 'Koma ' + terb_belakang(p2).replace('  ', ' ');;
		if (subkoma == 'Koma ')
			subkoma = '';
		sub = tanda + sub.replace('  ', ' ') + subkoma;
		return sub.replace('  ', ' ') + " Rupiah";
	}

	function terbilangIND() {
		var bilangan = document.getElementById('totHide').value;
		var kalimat = terbilang(bilangan);

		document.getElementById("totalBilangan").innerHTML = kalimat;
	}

	function toWordsconver(s) {
		// System for American Numbering
		// System for uncomment this line for Number of English

		var dg_val = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine'];
		var tn_val = ['Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen'];
		var tw_val = ['Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety'];
		var th_val = ['', 'Thousand', 'Million', 'Billion', 'Trillion'];

		s = s.toString();
		s = s.replace(/[\, ]/g, '');
		if (s != parseFloat(s))
			return 'not a number ';
		var x_val = s.indexOf('.');
		if (x_val == -1)
			x_val = s.length;
		if (x_val > 15)
			return 'too big';
		var n_val = s.split('');
		var str_val = '';
		var sk_val = 0;

		for (var i = 0; i < x_val; i++) {
			// console.log(x_val);
			// console.log(n_val[i]);

			if ((x_val - i) % 3 == 2) {
				if (n_val[i] == '1') {
					str_val += tn_val[Number(n_val[i + 1])] + ' ';
					i++;
					sk_val = 1;
				}else if (n_val[i] != 0) {
					str_val += tw_val[n_val[i] - 2] + ' ';
					sk_val = 1;
				}
			}else if (n_val[i] != 0) {
				str_val += dg_val[n_val[i]] + ' ';
				if ((x_val - i) % 3 == 0)
					str_val += 'Hundred ';
				sk_val = 1;
			}

			if ((x_val - i) % 3 == 1) {
				if (sk_val)
					str_val += th_val[(x_val - i - 1) / 3] + ' ';
				sk_val = 0;
			}
		}
		
		if (x_val != s.length) {
			var y_val = s.length;
			str_val += 'And Cents ';

			for (var i = x_val + 1; i < y_val; i++) {
				// str_val += dg_val[n_val[i]] + ' ';
				if ((y_val - i) % 3 == 2) {
					if (n_val[i] == '1') {
						str_val += tn_val[Number(n_val[i + 1])] + ' ';
						i++;
						sk_val = 1;
					}else if (n_val[i] != 0) {
						str_val += tw_val[n_val[i] - 2] + ' ';
						sk_val = 1;
					}
				}else if (n_val[i] != 0) {
					str_val += dg_val[n_val[i]] + ' ';
					if ((x_val - i) % 3 == 0)
						str_val += 'Hundred ';
					sk_val = 1;
				}

				if ((y_val - i) % 3 == 1) {
					if (sk_val)
						str_val += th_val[(y_val - i - 1) / 3] + ' ';
					sk_val = 0;
				}
			}
		}
		
		return "Says : ( US Dollars " + str_val.replace(/\s+/g, ' ') + " Only )";
	}

	function terbilangENG() {
		// var bilangan = $("#tdAmount_Total").html();
		var bilangan = $("#amount_total_label").html();
		var kalimat = toWordsconver(bilangan);

		document.getElementById("totalBilangan").innerHTML = kalimat;
	}


	function cal_price(row) {
		var total = 0;
		var rowData = t_invoice_list.row(row).data();
		
		var amount = rowData[6];
		var qtykoma = rowData[4];
		
		var qty = qtykoma.replace(/[^\d.-]/g, '');
		var unitPrice = $('#unit_price' + row).val();
		var total = numberWithCommas($('#total-label').html());
		var ppn_total = numberWithCommas($('#ppn-input').val());
		
		tot_amount = parseFloat(unitPrice) * parseFloat(qty);
		
		$('input[id="amount' + row + '"]').val(parseFloat(tot_amount).toFixed(2));
		<?php if ($invoice[0]['valas_id'] == 1) { ?>
			$("#amount-label" + row).html(formatCurrencyComaIDR(tot_amount));
			$("#unit_price-label" + row).html(formatCurrencyComaIDR(parseFloat(unitPrice)));
		<?php } else { ?>
			$("#amount-label" + row).html(formatCurrencyComaUSD(tot_amount));
			$("#unit_price-label" + row).html(formatCurrencyComaUSD(parseFloat(unitPrice)));
		<?php } ?>
		var tempTotal = 0;
		var totalSeluruh = 0;
		$('input[name="amount[]"]').each(function() {
			if (this.value !== '' && this.value !== null) {
				tempTotal = tempTotal + parseFloat(this.value);
				totalSeluruh = parseFloat(tempTotal) + parseFloat(ppn_total);
			} else {
				tempTotal = tempTotal + 0;
				totalSeluruh = tempTotal + 0;
			}
		})
		
		$('#total-label').text(formatCurrencyComaIDR(tempTotal));
		$("#ppn-label").text(formatCurrencyComaIDR(tempTotal * (ppn_total / 100)));
		$('#amount_total_label').html(formatCurrencyComaIDR(tempTotal + (tempTotal * (ppn_total / 100))));
		// $('#tdAmount_Total').html(formatCurrencyComaIDR(tempTotal + (tempTotal * (ppn_total / 100))));
		$("#totalBilangan").html(terbilang(tempTotal + (tempTotal * (ppn_total / 100))));
	}

	function redrawTable(row) {
		var rowData = t_invoice_list.row(row).data();
		
		rowData[6] = $('#unit_price' + row).val();
		rowData[8] = $('#amount' + row).val();
		t_invoice_list.draw();
	}

	function numberWithCommas(x) {
		var parts = x.toString().split(".");
		parts[0] = parts[0].replace(",", "");
		return parts.join(".");
	}
</script>