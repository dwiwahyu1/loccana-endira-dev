  <style>
  	#loading-us {
  		display: none
  	}

  	#tick {
  		display: none
  	}

  	#loading-mail {
  		display: none
  	}

  	#cross {
  		display: none
  	}

  	#add_item {
  		cursor: pointer;
  		text-decoration: underline;
  		color: #96b6e8;
  		padding-top: 6px;
  	}

  	#add_item:hover {
  		color: #ff8c00
  	}
  </style>
  <form class="form-horizontal form-label-left" id="edit_status" role="form" action="<?php echo base_url('maintenance/change_status'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>

  	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
  	<br>
  	<div class="item form-group">
  		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Maintenance No
  			<span class="required">
  				<sup>*</sup>
  			</span>
  		</label>
  		<div class="col-md-8 col-sm-6 col-xs-12">
  			<input data-parsley-maxlength="255" type="text" id="no_maintenance" name="no_maintenance" class="form-control col-md-7 col-xs-12" placeholder="Maintenance No" value="<?php if (isset($detail[0]['no_maintenance'])) {
																																																																																				echo $detail[0]['no_maintenance'];
																																																																																			} ?>" readonly>
  		</div>
  	</div>

  	<div class="item form-group">
  		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Maintenance Date
  			<span class="required">
  				<sup>*</sup>
  			</span>
  		</label>
  		<div class="col-md-8 col-sm-6 col-xs-12">
  			<div class="input-group date">
  				<input placeholder="Maintenance Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="maintenance_date" name="maintenance_date" required="required" value="<?php if (isset($detail[0]['tanggal_maintenance'])) {
																																																																																													echo $detail[0]['tanggal_maintenance'];
																																																																																												} ?>" readonly>
  				<div class="input-group-addon">
  					<span class="glyphicon glyphicon-th"></span>
  				</div>
  			</div>
  		</div>
  	</div>

  	<div class="item form-group">
  		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Detail Customer : </label>
  	</div>

  	<div class="item form-group">
  		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Customer Name
  			<span class="required">
  				<sup>*</sup>
  			</span>
  		</label>
  		<div class="col-md-8 col-sm-6 col-xs-12">
  			<input data-parsley-maxlength="255" type="text" id="cust_name" name="cust_name" class="form-control col-md-7 col-xs-12" placeholder="Customer Name" required="required" value="<?php if (isset($detail[0]['cust_name'])) {
																																																																																													echo $detail[0]['cust_name'];
																																																																																												} ?>" disabled="disabled">
  			<input type="hidden" id="id_cust" name="id_cust" value="<?php if (isset($detail[0]['id_cust'])) {
																																	echo $detail[0]['id_cust'];
																																} ?>">
  		</div>
  	</div>

  	<div class="item form-group">
  		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Department
  			<span class="required">
  				<sup>*</sup>
  			</span>
  		</label>
  		<div class="col-md-8 col-sm-6 col-xs-12">
  			<input data-parsley-maxlength="255" type="text" id="department" name="department" class="form-control col-md-7 col-xs-12" placeholder="Department" required="required" value="<?php if (isset($detail[0]['departemen'])) {
																																																																																												echo $detail[0]['departemen'];
																																																																																											} ?>" disabled="disabled">

  		</div>
  	</div>

  	<div class="item form-group">
  		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Barang : </label>
  	</div>

  	<div class="item form-group">
  		<table id="listaddedmaterial" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
  			<thead>
  				<tr>
  					<th>Nama Barang</th>
  					<th>Satuan</th>
  					<th>Qty</th>
  					<th>Tanggal Datang</th>
  					<th>Gudang</th>
  					<th>Keterangan</th>
  				</tr>
  			</thead>
  			<tbody>
  				<?php
					$items = $detail;
					$itemsLen = count($items);

					if ($itemsLen) {
						for ($i = 0; $i < $itemsLen; $i++) {
							?>
  						<tr>
  							<td><?php echo $detail[$i]['nama_barang']; ?></td>
  							<td><?php echo $detail[$i]['uom_symbol']; ?></td>
  							<td><?php echo $detail[$i]['qty']; ?></td>
  							<td><?php echo $detail[$i]['tanggal_diperlukan']; ?></td>
  							<td><?php echo $detail[$i]['gudang']; ?></td>
  							<td><?php echo $detail[$i]['desc']; ?></td>
  						</tr>
  				<?php
						}
					}
					?>
  			</tbody>
  		</table>
  	</div>

  	<div class="item form-group">
  		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Status
  			<span class="required">
  				<sup>*</sup>
  			</span>
  		</label>
  		<div class="col-md-8 col-sm-6 col-xs-12">
  			<select class="form-control" name="status" id="status" style="width: 100%" required>
  				<option value="1">Accept</option>
  				<option value="5">Reject</option>
  			</select>
  		</div>
  	</div>
  	<!-- <div class="item form-group form-item class_bc" style="display:none">
  		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_bc" >No BC<span class="required"><sup>*</sup></span></label>
  		<div class="col-md-8 col-sm-6 col-xs-12">
  			<select class="form-control" name="no_bc" id="no_bc" style="width: 100%" required>
  				<option value="">Tidak ada No BC</option>
  				<?php foreach ($bcs as $pend => $bc) { ?>
  					<option value="<?php echo $bc['id']; ?>"><?php echo $bc['no_pendaftaran']; ?></option>
  				<?php } ?>
  			</select>
  		</div>
  	</div> -->
  	<div class="item form-group">
  		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Notes <span class="required"><sup>*</sup></span>
  		</label>
  		<div class="col-md-8 col-sm-6 col-xs-12">
  			<textarea data-parsley-maxlength="255" type="text" id="notes" name="notes" class="form-control col-md-7 col-xs-12" placeholder="Notes"></textarea>
  		</div>
  	</div>

  	<div class="item form-group">
  		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
  		<div class="col-md-8 col-sm-6 col-xs-12">
  			<button id="btn-change" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Status</button>
  		</div>
  	</div>
  </form><!-- /page content -->

  <script type="text/javascript">
  	var items = [];
  	$(document).ready(function() {
  		$('form').parsley();
  		$('[data-toggle="tooltip"]').tooltip();

  		$("#maintenance_date").datepicker({
  			format: 'yyyy-mm-dd',
  			autoclose: true,
  			todayHighlight: true,
  		});
  	});
		// $('#status').on('change',(function(e){
		// 	if($(this).val()==1)//if value status is Accepted
		// 		$('.class_bc').show();
		// 	else
		// 		$('.class_bc').hide();
		// }));
  	$('#edit_status').on('submit', (function(e) {
  		$('#btn-change').attr('disabled', 'disabled');
  		$('#btn-change').text("Mengubah status...");
  		e.preventDefault();

  		var no_maintenance = $('#no_maintenance').val(),
  			status = $('#status').val(),
  			notes = $('#notes').val();

  		var datapost = {
  			"no_maintenance": no_maintenance,
  			"status": status,
  			"notes": notes
  		};

  		$.ajax({
  			type: 'POST',
  			url: $(this).attr('action'),
  			data: JSON.stringify(datapost),
  			cache: false,
  			contentType: false,
  			processData: false,
  			success: function(response) {
  				if (response.success == true) {
  					swal({
  						title: 'Success!',
  						text: response.message,
  						type: 'success',
  						showCancelButton: false,
  						confirmButtonText: 'Ok'
  					}).then(function() {
  						$('.panel-heading button').trigger('click');
  						listmaintenance();
  					})
  				} else {
  					$('#btn-submit').removeAttr('disabled');
  					$('#btn-submit').text("Ubah Status");
  					swal("Failed!", response.message, "error");
  				}
  			}
  		}).fail(function(xhr, status, message) {
  			$('#btn-submit').removeAttr('disabled');
  			$('#btn-submit').text("Ubah Status");
  			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
  		});
  	}));
  </script>