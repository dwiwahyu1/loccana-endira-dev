  <style>
  #loading-us{display:none}
	#tick{display:none}

	#loading-mail{display:none}
	#cross{display:none}
	.form-item{margin-top: 15px;overflow: auto;}
  </style>
  <form class="form-horizontal form-label-left" id="save_bc_masuk_detail" role="form" action="<?php echo base_url('maintenance/save_bc_masuk'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>

    <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
    <br>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Maintenance No
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="no_maintenance" name="no_maintenance" class="form-control col-md-7 col-xs-12" placeholder="Maintenance No" value="<?php if (isset($detail[0]['no_maintenance'])) {
                                                                                                                                                                                echo $detail[0]['no_maintenance'];
                                                                                                                                                                              } ?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Maintenance Date
        <span class="required" disabled="disabled">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <div class="input-group date">
          <input placeholder="Maintenance Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="maintenance_date" name="maintenance_date" required="required" value="<?php if (isset($detail[0]['tanggal_maintenance'])) {
                                                                                                                                                                                          echo $detail[0]['tanggal_maintenance'];
                                                                                                                                                                                        } ?>">
          <div class="input-group-addon">
            <span class="glyphicon glyphicon-th"></span>
          </div>
        </div>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Detail Customer : </label>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Customer Name
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="cust_name" name="cust_name" class="form-control col-md-7 col-xs-12" placeholder="Customer Name" required="required" value="<?php if (isset($detail[0]['cust_name'])) {
                                                                                                                                                                                          echo $detail[0]['cust_name'];
                                                                                                                                                                                        } ?>" disabled="disabled">
        <input type="hidden" id="id_cust" name="id_cust" value="<?php if (isset($detail[0]['id_cust'])) {
                                                                  echo $detail[0]['id_cust'];
                                                                } ?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Department
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="department" name="department" class="form-control col-md-7 col-xs-12" placeholder="Department" required="required" value="<?php if (isset($detail[0]['departemen'])) {
                                                                                                                                                                                        echo $detail[0]['departemen'];
                                                                                                                                                                                      } ?>" disabled="disabled">

      </div>
    </div>
    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Material
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="nama_material" name="nama_material" class="form-control col-md-7 col-xs-12" placeholder="Nama Material" value="<?php if (isset($detail[0]['nama_barang'])) {
                                                                                                                                                                              echo $detail[0]['nama_barang'];
                                                                                                                                                                            } ?>" readonly>
      </div>
    </div>
    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Total Quantity
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="qty" name="qty" class="form-control col-md-7 col-xs-12" placeholder="Jumlah Barang" value="<?php echo $detail[0]['qty']; ?>" disabled="disabled">
      </div>
    </div>
    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Satuan
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <!-- <input data-parsley-maxlength="255" type="text" id="qty" name="qty" class="form-control col-md-7 col-xs-12" placeholder="Jumlah Barang" value="<?php echo $detail[0]['qty']; ?>" disabled="disabled"> -->
        <input data-parsley-maxlength="255" type="text" id="uom_symbol" name="uom_symbol" class="form-control col-md-7 col-xs-12" placeholder="Satuan Barang" value="<?php echo $detail[0]['uom_symbol']; ?>" disabled="disabled">
      </div>
    </div>
    <!-- <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Barang : </label>
      <div class="col-md-8 col-sm-6 col-xs-12" id="add_item" onclick="add_item()">
        Tambah Barang
      </div>
    </div> -->

    <div class="item form-group">
      <table id="listaddedmaterial-detail" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>BC Masuk</th>
            <th>Qty Masuk</th>
            <th>Option</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $itemsLen = count($result_bc_masuk);
          for ($i = 0; $i < $itemsLen + 1; $i++) {
            ?>
            <tr>
              <td>
                <select <?php if ($i<$itemsLen) { echo 'disabled="disabled"';}?> class="form-control col-md-7 col-xs-12 bc_masuk" name="bc_masuk[]" id="bc_masuk<?php echo $i ?>"   >
                  <option value="">-- Tidak Ada BC --</option>
                  <?php foreach ($result_bc as $pend => $bc) { ?>
                    <option value="<?php echo $bc['id']; ?>" <?php 
                    if (isset($result_bc_masuk[$i]['id_bc'])) if ($result_bc_masuk[$i]['id_bc'] == $bc['id']) echo 'selected="selected"';
                    ?>> <?php echo $bc['no_pendaftaran'] . '-' . $bc['no_pengajuan'] . '-' . $bc['tanggal_pengajuan']; ?></option>
                  <?php } ?>
                </select>
              </td>
              <td>
                <!-- <?php echo $result_bc_masuk[$i]['qty']; ?> -->
                <input <?php if ($i<$itemsLen) { echo 'disabled="disabled"';}?> step="0.0001" data-parsley-maxlength="255" min="0" class="form-control normal-control col-md-7 col-xs-12" type="number" id="qty_masuk<?php echo $i ?>" name="qty_masuk[]" placeholder="Jumlah Barang Masuk" value="<?php if (isset($result_bc_masuk[$i]['qty'])) {
                                                                                                                                                                                                                                echo $result_bc_masuk[$i]['qty'];
                                                                                                                                                                                                                              } ?>">
              </td>
              <td>
                <?php if ($i==$itemsLen) { ?>
                <div  class="btn-group">
                  <button class="btn btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Tambah Baru" onClick="add_item()">
                    <i class="fa fa-plus"></i>
                  </button>
                  <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Hapus BC Masuk" >
                    <i class="fa fa-trash"></i>
                  </button>
                </div>
                <?php }?>
              </td>
            </tr>
          <?php

          }
          ?>
        </tbody>
      </table>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <button id="btn-submit-bc-masuk" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Maintenance</button>
      </div>
    </div>
  </form><!-- /page content -->

  <script type="text/javascript">
    var items = [],
      itemsLen = 1;
    $(document).ready(function() {
      $('form').parsley();
      $('[data-toggle="tooltip"]').tooltip();

      $("#maintenance_date").datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
      });
      for (i = 0; i < itemsLen; i++)
        $('.bc_masuk').select2();
    });

    $('#save_bc_masuk_detail').on('submit', (function(e) {
    var items = [];
      var qty_masuk_vals = $("input[name='qty_masuk[]']").map(function() {
        return $(this).val();
      }).get();
      var bc_masuk_vals = $("select[name='bc_masuk[]']").map(function() {
        return $(this).val();
      }).get();
      // function for adding two numbers. Easy!
      var add = (a, b) => parseInt(a) + parseInt(b);
      // use reduce to sum our array
      var sum = qty_masuk_vals.reduce(add);
      console.log(qty_masuk_vals.length);
      for (i = 0; i < qty_masuk_vals.length; i++) {
        var id_maintenance = "<?php echo $detail[0]['id']; ?>";
        var no_maintenance = "<?php $detail[0]['no_maintenance']; ?>";
        var iditems = "<?php echo $detail[0]['nama_material']; ?>";
        var id_bc = bc_masuk_vals[i];
        var qty = qty_masuk_vals[i];
        

        var dataitem = {
          id_maintenance,
          iditems,
          id_bc,
          qty
        };
        items.push(dataitem);
      }
      console.log(qty_masuk_vals, bc_masuk_vals, items.length, $('#id_cust').val(), items);
      if (items.length && $('#id_cust').val() !== '') {
        $('#btn-submit-bc-masuk').attr('disabled', 'disabled');
        $('#btn-submit-bc-masuk').text("Mengubah data...");
        e.preventDefault();
        var datapost = {
          "sum_qty": sum,
          "qty_keluar": parseInt($('#qty').val()),
          "items": items
        };

        $.ajax({
          type: 'POST',
          url: $(this).attr('action'),
          data: JSON.stringify(datapost),
          cache: false,
          contentType: false,
          processData: false,
          success: function(response) {
            if (response.success == true) {
              swal({
                title: 'Success!',
                text: response.message,
                type: 'success',
                showCancelButton: false,
                confirmButtonText: 'Ok'
              }).then(function() {
                $('.panel-heading button').trigger('click');
                listmaintenance();
              })
            } else {
              $('#btn-submit-bc-masuk').removeAttr('disabled');
              $('#btn-submit-bc-masuk').text("Edit Maintenance");
              swal("Failed!", response.message, "error");
            }
          }
        }).fail(function(xhr, status, message) {
          $('#btn-submit-bc-masuk').removeAttr('disabled');
          $('#btn-submit-bc-masuk').text("Edit Maintenance");
          swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
        });
      } else {
        swal("Failed!", "Harus menambah barang dahulu", "error");
      }
      return false;
    }));

    function add_item() {
      var itemsBC = [];
      "<?php

        $itemsBCLen = count($result_bc);
        // UPDATE t_maintenance 
        // SET status=?, bc_keluar=?, id_pic=?
        // WHERE id=? AND no_maintenance=?;
        for ($i = 0; $i < $itemsBCLen; $i++) {
          $id = $result_bc[$i]['id'];
          $no_pendaftaran = $result_bc[$i]['no_pendaftaran'];
          $no_pengajuan = $result_bc[$i]['no_pengajuan'];
          $tanggal_pengajuan = $result_bc[$i]['tanggal_pengajuan'];
          ?>"
      var id = "<?php echo $id; ?>";
      var no_pendaftaran = "<?php echo $no_pendaftaran; ?>";
      var no_pengajuan = "<?php echo $no_pengajuan; ?>";
      var tanggal_pengajuan = "<?php echo $tanggal_pengajuan; ?>";

      var dataitemBC = {
        id,
        no_pendaftaran,
        no_pengajuan,
        tanggal_pengajuan
      };
      itemsBC.push(dataitemBC);
      "<?php
        }
        ?>"

      var itemCount = items.length,
        itemBCCount = itemsBC.length,
        itemCur = itemsLen;
      itemsLen++;
      var element = '<tr>';
      element += '	<td>';

      element += '		<select class="form-control col-md-7 col-xs-12 bc_masuk" name="bc_masuk[]" id="bc_masuk' + itemCur + '" >';
      element += '			<option value="">-- Tidak Ada BC --</option>';
      for (var i = 0; i < itemBCCount; i++) {
        element += '		<option value="' + itemsBC[i]['id'] + '">';
        element += itemsBC[i]['no_pendaftaran'] + '-';
        element += itemsBC[i]['no_pengajuan'] + '-'
        element += itemsBC[i]['tanggal_pengajuan'] + '</option>';
      }
      element += '		</select>';
      element += '	</td>';
      element += '	<td>';
      element += '		<input type="number" class="form-control normal-control" min="0" id="qty_masuk' + itemCur + '" name="qty_masuk[]" style="" value="0">';
      element += '	</td>';
      element += '	<td>';
      element += '		<div class="btn-group">';
      element += '			<button class="btn btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Tambah Baru" onClick="add_item()">';
      element += '				<i class="fa fa-plus"></i>';
      element += '			</button>';
      element += '      <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Hapus" >';
      element += '        <i class="fa fa-trash"></i>';
      element += '      </button>';
      element += '		</div>';
      element += '	</td>';
      element += '</tr>';
      $('#listaddedmaterial-detail tbody').append(element);

      $("select").select2();
      $('[data-toggle="tooltip"]').tooltip();
      
      $(".btn-danger").on("click", function() {
        $(this).parent().parent().parent().remove();
        itemsLen--;
        looptable();
      });
    }
    $(".btn-danger").on("click", function() {
      $(this).parent().parent().parent().remove();
      itemsLen--;
      looptable();
    });
    function looptable() {
      $('#listadditem tbody tr').each(function(index, value) {
        $(this).children('td:first-child').html(index + 1);
      });
    }
  </script>