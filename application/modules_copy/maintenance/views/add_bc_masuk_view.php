  <style>
    #loading-us {
      display: none
    }

    #tick {
      display: none
    }

    #loading-mail {
      display: none
    }

    #cross {
      display: none
    }

    #add_item {
      cursor: pointer;
      text-decoration: underline;
      color: #96b6e8;
      padding-top: 6px;
    }

    #add_item:hover {
      color: #ff8c00
    }
  </style>
  <form class="form-horizontal form-label-left" id="save_bc_masuk" role="form" enctype="multipart/form-data" data-parsley-validate>

    <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
    <br>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Maintenance No
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="no_maintenance" name="no_maintenance" class="form-control col-md-7 col-xs-12" placeholder="Maintenance No" value="<?php if (isset($detail[0]['no_maintenance'])) {
                                                                                                                                                                                echo $detail[0]['no_maintenance'];
                                                                                                                                                                              } ?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Maintenance Date
        <span class="required" disabled="disabled">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <div class="input-group date">
          <input placeholder="Maintenance Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="maintenance_date" name="maintenance_date" required="required" value="<?php if (isset($detail[0]['tanggal_maintenance'])) {
                                                                                                                                                                                          echo $detail[0]['tanggal_maintenance'];
                                                                                                                                                                                        } ?>">
          <div class="input-group-addon">
            <span class="glyphicon glyphicon-th"></span>
          </div>
        </div>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Detail Customer : </label>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Customer Name
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="cust_name" name="cust_name" class="form-control col-md-7 col-xs-12" placeholder="Customer Name" required="required" value="<?php if (isset($detail[0]['cust_name'])) {
                                                                                                                                                                                          echo $detail[0]['cust_name'];
                                                                                                                                                                                        } ?>" disabled="disabled">
        <input type="hidden" id="id_cust" name="id_cust" value="<?php if (isset($detail[0]['id_cust'])) {
                                                                  echo $detail[0]['id_cust'];
                                                                } ?>">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Department
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="department" name="department" class="form-control col-md-7 col-xs-12" placeholder="Department" required="required" value="<?php if (isset($detail[0]['departemen'])) {
                                                                                                                                                                                        echo $detail[0]['departemen'];
                                                                                                                                                                                      } ?>" disabled="disabled">

      </div>
    </div>

    <!-- <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Barang : </label>
      <div class="col-md-8 col-sm-6 col-xs-12" id="add_item" onclick="add_item()">
        Tambah Barang
      </div>
    </div> -->

    <div class="item form-group">
      <table id="listaddedmaterial" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>Nama Barang</th>
            <th>Satuan</th>
            <th>Qty</th>
            <th>Harga</th>
            <th>Discount</th>
            <th>Harga Akhir</th>
            <th>Tanggal Diperlukan</th>
            <th>Gudang</th>
            <th>Pihak Pengerja</th>
            <th>Keterangan</th>
            <th>BC Keluar</th>
            <th>BC Masuk</th>
            <!-- <th>Option</th> -->
          </tr>
        </thead>
        <tbody>
          <?php
          $itemsLen = count($detail);
          if ($itemsLen) {
            for ($i = 0; $i < $itemsLen; $i++) {
              ?>
              <div class="item form-group" style="display:none">
                <input data-parsley-maxlength="255" type="text" id="id<?php echo $i ?>" name="id<?php echo $i ?>" class="form-control col-md-7 col-xs-12" placeholder="ID" value="<?php if (isset($detail[0]['id'])) {
                                                                                                                                                        echo $detail[0]['id'];
                                                                                                                                                      } ?>" readonly>
              </div>
              <tr>
                <td>
                  <input data-parsley-maxlength="255" type="text" id="cust_name" name="cust_name" class="form-control col-md-7 col-xs-12" placeholder="Customer Name" value="<?php echo $detail[$i]['nama_barang']; ?>" disabled="disabled">
                </td>
                <td>
                  <!-- <?php echo $detail[$i]['uom_symbol']; ?> -->
                  <input data-parsley-maxlength="255" type="text" id="uom_symbol" name="uom_symbol" class="form-control col-md-7 col-xs-12" placeholder="Satuan Barang" value="<?php echo $detail[$i]['uom_symbol']; ?>" disabled="disabled">
                </td>
                <td>
                  <!-- <?php echo $detail[$i]['qty']; ?> -->
                  <input data-parsley-maxlength="255" type="text" id="qty" name="qty" class="form-control col-md-7 col-xs-12" placeholder="Jumlah Barang" value="<?php echo $detail[$i]['qty']; ?>" disabled="disabled">
                </td>
                <td>
                  <!-- <?php echo $detail[$i]['price']; ?> -->
                  <input data-parsley-maxlength="255" type="text" id="price<?php echo $i ?>" name="price" class="form-control col-md-7 col-xs-12" placeholder="Harga Satuan Barang" required="required" value="<?php echo $detail[$i]['price']; ?>" disabled="disabled">
                </td>
                <td>
                  <!-- <?php echo $detail[$i]['discount']; ?> -->
                  <input data-parsley-max="100" type="text" id="discount<?php echo $i ?>" name="discount" class="form-control col-md-7 col-xs-12" placeholder="Discount Barang" required="required" value="<?php echo $detail[$i]['discount']; ?>" disabled="disabled">
                </td>
                <td>
                  <!-- <?php echo $detail[$i]['last_price']; ?> -->
                  <input data-parsley-maxlength="255" type="text" id="last_price<?php echo $i ?>" name="last_price" class="form-control col-md-7 col-xs-12" placeholder="Harga Akhir" required="required" value="<?php echo $detail[$i]['last_price']; ?>" disabled="disabled">
                </td>
                <td>
                  <!-- <?php echo $detail[$i]['tanggal_diperlukan']; ?> -->
                  <input data-parsley-maxlength="255" type="text" id="tanggal_diperlukan" name="tanggal_diperlukan" class="form-control col-md-7 col-xs-12" placeholder="Tanggal Diperlukan" value="<?php echo $detail[$i]['tanggal_diperlukan']; ?>" disabled="disabled">
                </td>
                <td>
                  <!-- <?php echo $detail[$i]['gudang']; ?> -->
                  <input data-parsley-maxlength="255" type="text" id="gudang" name="gudang" class="form-control col-md-7 col-xs-12" placeholder="Gudang" value="<?php echo $detail[$i]['gudang']; ?>" disabled="disabled">
                </td>
                <td>
                  <!-- <?php echo $detail[$i]['worker_name']; ?> -->
                  <input data-parsley-maxlength="255" type="text" id="worker_name" name="worker_name" class="form-control col-md-7 col-xs-12" placeholder="Pihak Pekerja" required="required" value="<?php echo $detail[$i]['worker_name']; ?>" disabled="disabled">
                  <!-- <select class="form-control col-md-7 col-xs-12 worker_name" name="worker_name" id="worker_name<?php echo $i ?>" required>
                    <option value="">Belum ada Worker</option>
                    <?php foreach ($list_worker_name as $workind => $worker)?>
                      <option value="<?php echo $worker['id']; ?>" 
                        <?php if ($worker['id'] == $detail[$i]['worker_name']) echo 'choosen';?>
                        ><?php echo $worker['name_eksternal']; ?>
                      </option>
                  </select> -->
                </td>
                <td>
                  <!-- <?php echo $detail[$i]['desc']; ?> -->
                  <input data-parsley-maxlength="255" type="text" id="desc" name="desc" class="form-control col-md-7 col-xs-12" placeholder="Deskripsi" value="<?php echo $detail[$i]['desc']; ?>" disabled="disabled">
                </td>
                <td>
                  <!-- <?php echo $detail[$i]['worker_name']; ?> -->
                  <input data-parsley-maxlength="255" type="text" id="bc_keluar" name="bc_keluar" class="form-control col-md-7 col-xs-12" placeholder="BC Keluar" required="required" value="<?php 
                   foreach ($result_bc as $pend=> $bc)
                    if ($bc['id']==$detail[$i]['bc_keluar']) echo $bc['no_pendaftaran'].'|'.$bc['no_pengajuan'].'|'.$bc['tanggal_pengajuan'];
                                                                                                                              
                   
                   ?>" disabled="disabled">
                </td>
                <td>
                  <!-- <select class="form-control col-md-7 col-xs-12 bc_masuk" name="bc_masuk" id="bc_masuk<?php echo $i ?>" required>
                    <option value="">Tidak ada No BC</option>
                    <?php foreach ($result_bc as $pend => $bc) { ?>
                      <option value="<?php echo $bc['id']; ?>"> <?php echo $bc['no_pendaftaran'].'-'.$bc['no_pengajuan'].'-'.$bc['tanggal_pengajuan']; ?></option>
                    <?php } ?>
                  </select> -->
                  <div class="btn-group">
                    <button class="btn btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Set BC Masuk" onClick="add_bc_masuk_detail( <?php echo 
                      '\''.$detail[$i]['id'].'\''.','.
                      '\''.$detail[$i]['nama_material'].'\''
                      ;?>)">
                      <i class="fa fa-sign-in"></i>
                    </button>
                  </div>
                </td>
                <!-- <td>
                  <div class="btn-group">
                    <button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="deleteaddeditem('<?php echo $i; ?>')">
                      <i class="fa fa-pencil"></i>
                  </div>
                </td> -->
              </tr>
          <?php
            }
          }
          ?>
        </tbody>
      </table>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Maintenance</button>
      </div>
    </div>
  </form><!-- /page content -->

  <script type="text/javascript">
    var items = [];
    $(document).ready(function() {
      $('form').parsley();
      $('[data-toggle="tooltip"]').tooltip();

      $("#maintenance_date").datepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        todayHighlight: true,
      });

      // $('.bc_masuk').select2();
    });
    
    $('#save_bc_masuk').on('submit', (function(e) {
      $('.panel-heading button').trigger('click');
      listmaintenance();
    }));
  </script>