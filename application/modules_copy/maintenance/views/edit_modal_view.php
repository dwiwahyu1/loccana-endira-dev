<style>
  #listItemMaintenance {
    counter-reset: rowNumber;
  }

  #listItemMaintenance tr>td:first-child {
    counter-increment: rowNumber;
  }

  #listItemMaintenance tr td:first-child::before {
    content: counter(rowNumber);
    min-width: 1em;
    margin-right: 0.5em;
  }
</style>

<form class="form-horizontal form-label-left" id="edit_main" role="form" action="<?php echo base_url('maintenance/edit_maintenance'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
  <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="main_date">Maintenance Date <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="text" class="form-control" id="main_date" name="main_date" value="<?php
                                                                                      if (isset($permintaan[0]['tanggal_main'])) echo date('d-M-Y', strtotime($permintaan[0]['tanggal_main']));
                                                                                      ?>" placeholder="Work Date" required>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pic">PIC</label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="text" class="form-control" id="pic" name="pic" value="<?php
                                                                          if (isset($permintaan[0]['nama'])) echo ucwords($permintaan[0]['nama']); ?>" placeholder="PIC" readonly>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="department">Department</label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="text" class="form-control" id="department" name="department" value="<?php
                                                                                        if (isset($permintaan[0]['group'])) echo ucwords($permintaan[0]['group']); ?>" placeholder="Department" readonly>
    </div>
  </div>
  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="department">Sub Kontraktor</label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <select class="form-control uom" id="subkon_id" name="subkon_id[]">
        <option value="">-- Pilih Sub Kontraktor --</option>
        <?php
        foreach ($customer as $uk => $uv) { ?>
          <option value="<?php echo $uv['id']; ?>" <?php
                                                    if (isset($detail[0]['subkon_id'])) {
                                                      if ($uv['id'] == $detail[0]['subkon_id']) echo "selected";
                                                    }
                                                    ?>><?php echo $uv['name_eksternal']; ?></option>
        <?php
        } ?>
      </select>
    </div>
  </div>

  <div class="item form-group">
    <input type="hidden" id="id_main" name="id_main" value="<?php if (isset($permintaan[0]['group'])) echo $permintaan[0]['id_main']; ?>">
    <table id="listItemMaintenance" class="table table-striped table-bordered dt-responsive nowrap" width="100%">
      <thead>
        <tr>
          <th style="width: 5%;">No</th>
          <th style="width: 10%;">No Maintenance</th>
          <th>Nama Maintenance</th>
          <th>Mesin</th>
          <th style="width: 10%;">QTY</th>
          <!-- <th style="width: 20%;">Sub Kontraktor</th> -->
          <th>Deskripsi</th>
          <th style="width: 5%;"><a id="btn_item_add" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a></th>
        </tr>
      </thead>
      <tbody>
        <?php
        $i = 1;
        if (sizeof($detail) > 0) {
          foreach ($detail as $dk) { ?>
            <tr id="trRowItem<?php echo $i; ?>">
              <td></td>
              <td><?php if (isset($dk['no_main'])) echo $dk['no_main']; ?></td>
              <td>
                <input type="text" class="form-control" id="nama_main<?php echo $i; ?>" name="nama_main[]" value="<?php
                                                                                                                  if (isset($dk['nama_main'])) echo $dk['nama_main']; ?>" placeholder="Nama Maitenance" autocomplete="off" required>
              </td>
              <td>
                <select class="form-control uom" id="uom_main<?php echo $i; ?>" name="uom_main[]">
                  <option value="">-- Pilih MESIN --</option>
                  <?php
                  foreach ($material as $uk => $uv) { ?>
                    <option value="<?php echo $uv['id']; ?>" <?php
                                                              if (isset($dk['qty'])) {
                                                                if ($uv['id'] == $dk['unit']) echo "selected";
                                                              }
                                                              ?>><?php echo $uv['stock_name']; ?></option>
                  <?php
                  } ?>
                </select>
              </td>
              <td>
                <input type="number" class="form-control" id="qty_main<?php echo $i; ?>" name="qty_main[]" value="<?php
                                                                                                                  if (isset($dk['qty'])) echo $dk['qty']; ?>" placeholder="Qty" value="1" step=".0001" autocomplete="off" required>
              </td>
              <!-- <td>
                <select class="form-control uom" id="subkon_id<?php echo $i; ?>" name="subkon_id[]">
                  <option value="">-- Pilih Sub Kontraktor --</option>
                  <?php
                  foreach ($customer as $uk => $uv) { ?>
                    <option value="<?php echo $uv['id']; ?>" <?php
                                                              if (isset($dk['qty'])) {
                                                                if ($uv['id'] == $dk['subkon_id']) echo "selected";
                                                              }
                                                              ?>><?php echo $uv['name_eksternal']; ?></option>
                  <?php
                  } ?>
                </select>
              </td> -->
              <td>
                <textarea class="form-control" id="desc_main<?php echo $i; ?>" name="desc_main[]" placeholder="Deskripsi"><?php
                                                                                                                          if (isset($dk['deskripsi'])) {
                                                                                                                            if (($dk['deskripsi'] != '' && $dk['deskripsi'] != 'NULL') && $dk['deskripsi'] != NULL) echo $dk['deskripsi'];
                                                                                                                          }
                                                                                                                          ?></textarea>
              </td>
              <td>
                <input type="hidden" id="id_detail_main<?php echo $i; ?>" name="id_detail_main[]" value="<?php
                                                                                                          if (isset($dk['id_detail_main'])) echo $dk['id_detail_main']; ?>">
                <a id="btn_item_remove" class="btn btn-danger btn-sm" title="Hapus" onclick="delItem(this, '<?php
                                                                                                            echo $dk['id_detail_main']; ?>')">
                  <i class="fa fa-trash"></i>
                </a>
              </td>
            </tr>
          <?php
            $i++;
          }
        } else { ?>
          <tr>
            <td></td>
            <td></td>
            <td>
              <input type="text" class="form-control" id="nama_main<?php echo $i; ?>" name="nama_main[]" placeholder="Nama Maintenance" autocomplete="off" required>
            </td>
            <td>
              <select class="form-control uom" id="uom_main<?php echo $i; ?>" name="uom_main[]">
                <option value="" selected>-- Pilih UOM --</option>
                <?php
                foreach ($material as $uk => $uv) { ?>
                  <option value="<?php echo $uv['id']; ?>"><?php echo $uv['stock_name']; ?></option>
                <?php
                } ?>
              </select>
            </td>
            <td>
              <input type="number" class="form-control" id="qty_main<?php echo $i; ?>" name="qty_main[]" placeholder="Qty" value="1" step=".0001" autocomplete="off" required>
            </td>
            <!-- <td>
              <select class="form-control uom" id="subkon_id<?php echo $i; ?>" name="subkon_id[]">
                <option value="">-- Pilih Sub Kontraktor --</option>
                <?php
                foreach ($customer as $uk => $uv) { ?>
                  <option value="<?php echo $uv['id']; ?>"><?php echo $uv['name_eksternal']; ?></option>
                <?php
                } ?>
              </select>
            </td> -->
            <td>
              <textarea class="form-control" id="desc_main<?php echo $i; ?>" name="desc_main[]" placeholder="Deskripsi"></textarea>
            </td>
            <td>
              <input type="hidden" id="id_detail_main<?php echo $i; ?>" name="id_detail_main[]" value="new">
            </td>
          </tr>
        <?php
        } ?>
      </tbody>
    </table>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Simpan Maintenance</button>
    </div>
  </div>
</form>

<script type="text/javascript">
  var idRowItem = '<?php echo $i; ?>';
  var arrIdItem = [];
  var arrNama = [];
  var arrQty = [];
  var arrUom = [];
  var arrDesc = [];

  $(document).ready(function() {
    $("#main_date").datepicker({
      format: 'dd-M-yyyy',
      autoclose: true,
      todayHighlight: true,
    });
    $(".uom").select2();
  });

  $('#btn_item_add').on('click', function() {
    idRowItem++;
    $('#listItemMaintenance tbody').append(
      '<tr id="trRowItem' + idRowItem + '">' +
      '<td></td>' +
      '<td>-</td>' +
      '<td>' +
      '<input type="text" class="form-control" id="nama_main' + idRowItem + '" name="nama_main[]" placeholder="Nama Maintenance" autocomplete="off" required>' +
      '</td>' +
      '<td>' +
      '<select class="form-control" id="uom_main' + idRowItem + '" name="uom_main[]">' +
      '<option value="" selected >-- Pilih Mesin --</option>' +
      '</select>' +
      '</td>' +
      '<td>' +
      '<input type="number" class="form-control" id="qty_main' + idRowItem + '" name="qty_main[]" placeholder="Qty" value="1" step=".0001" autocomplete="off" required>' +
      '</td>' +
      '<td>' +
      '<textarea class="form-control" id="desc_main' + idRowItem + '" name="desc_main[]" placeholder="Deskripsi"></textarea>' +
      '</td>' +
      '<td>' +
      '<input type="hidden" id="id_detail_main' + idRowItem + '" name="id_detail_main[]" value="new">' +
      '<a class="btn btn-danger" onclick="removeRow(' + idRowItem + ')"><i class="fa fa-minus"></i></a>' +
      '</td>' +
      '</tr>'
    );
    $("#uom_main" + idRowItem).select2();
    $('#uom_main' + idRowItem).attr('readonly', 'readonly');

    $.ajax({
      type: "GET",
      url: "<?php echo base_url('maintenance/get_material'); ?>",
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      success: function(r) {
        if (r.length > 0) {
          for (var i = 0; i < r.length; i++) {
            var newOption = new Option(r[i].stock_name, r[i].id, false, false);
            $('#uom_main' + idRowItem).append(newOption);
          }
          $('#uom_main' + idRowItem).removeAttr('readonly');
        } else $('#uom_main' + idRowItem).removeAttr('readonly');
      }
    });
  })

  function delItem(btnDel, idDetal) {
    var rowParent = $(btnDel).parents('tr');

    swal({
      title: 'Yakin akan Menghapus Detail yang sudah ada ?',
      text: 'data tidak dapat dikembalikan bila sudah dihapus !',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak'
    }).then(function() {
      var datapost = {
        "id": parseInt(idDetal)
      };

      $.ajax({
        type: "POST",
        url: "<?php echo base_url(); ?>maintenance/delete_detail_maintenance",
        data: JSON.stringify(datapost),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function(response) {
          if (response.success == true) {
            swal({
              title: 'Success!',
              text: response.message,
              type: 'success',
              showCancelButton: false,
              confirmButtonText: 'Ok'
            }).then(function() {
              rowParent.remove();
            })
          } else swal("Failed!", response.message, "error");
        }
      });
    });
  }

  $('#edit_main').on('submit', (function(e) {
    arrIdItem = [];
    arrNama = [];
    arrQty = [];
    arrUom = [];
    arrDesc = [];

    $('#btn-submit').attr('disabled', 'disabled');
    $('#btn-submit').text("Memasukkan data...");
    e.preventDefault();

    $('input[name="id_detail_main[]"]').each(function() {
      if (this.value) {
        if (this.value != undefined && this.value != '') arrIdItem.push(this.value);
      }
    })

    $('input[name="nama_main[]"]').each(function() {
      if (this.value) {
        if (this.value != undefined && this.value != '') arrNama.push(this.value);
      }
    })

    $('input[name="qty_main[]"]').each(function() {
      if (this.value) {
        if (this.value != undefined && this.value != '') arrQty.push(this.value);
      }
    })

    $('select[name="uom_main[]"]').each(function() {
      if (this.value) {
        if (this.value != undefined && this.value != '') arrUom.push(this.value);
      }
    })

    $('textarea[name="desc_main[]"]').each(function() {
      if (this.value) {
        if (this.value != undefined && this.value != '') arrDesc.push(this.value);
        else arrDesc.push('NULL');
      } else arrDesc.push('NULL');
    })

    if (arrIdItem.length > 0 && arrNama.length > 0 && arrQty.length > 0 && arrUom.length > 0 && arrDesc.length > 0) {
      var formData = new FormData();
      formData.set('id_main', $('#id_main').val());
      formData.set('main_date', $('#main_date').val());
      formData.set('subkon_id', $('#subkon_id').val());
      formData.set('arrIdItem', arrIdItem);
      formData.set('arrNama', arrNama);
      formData.set('arrQty', arrQty);
      formData.set('arrUom', arrUom);
      formData.set('arrDesc', arrDesc);

      $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
          if (response.success == true) {
            swal({
              title: 'Success!',
              text: response.message,
              type: 'success',
              showCancelButton: false,
              confirmButtonText: 'Ok'
            }).then(function() {
              $('#panel-modal').modal('toggle');
              listmaintenance();
            })
          } else {
            $('#btn-submit').removeAttr('disabled');
            $('#btn-submit').text("Simpan Maintenance");
            swal("Failed!", response.message, "error");
          }
        }
      }).fail(function(xhr, status, message) {
        $('#btn-submit').removeAttr('disabled');
        $('#btn-submit').text("Simpan Maintenance");
        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
      });
    } else {
      $('#btn-submit').removeAttr('disabled');
      $('#btn-submit').text("Simpan Maintenance");
      swal("Failed!", "Invalid Inputan List Maintenance, silahkan cek kembali.", "error");
    }
  }));
</script>