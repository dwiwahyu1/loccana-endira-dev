<style>
  #listItemMaintenance {
    counter-reset: rowNumber;
  }

  #listItemMaintenance tr>td:first-child {
    counter-increment: rowNumber;
  }

  #listItemMaintenance tr td:first-child::before {
    content: counter(rowNumber);
    min-width: 1em;
    margin-right: 0.5em;
  }
</style>

<form class="form-horizontal form-label-left" id="edit_main" role="form" action="<?php echo base_url('maintenance/approve_maintenance'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
  <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="main_date">Maintenance Date <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="text" class="form-control" id="main_date" name="main_date" value="<?php
                                                                                      if (isset($permintaan[0]['tanggal_main'])) echo date('d-M-Y', strtotime($permintaan[0]['tanggal_main']));
                                                                                      ?>" placeholder="Work Date" required>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pic">PIC</label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="text" class="form-control" id="pic" name="pic" value="<?php
                                                                          if (isset($permintaan[0]['nama'])) echo ucwords($permintaan[0]['nama']); ?>" placeholder="PIC" readonly>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="department">Department</label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="text" class="form-control" id="department" name="department" value="<?php
                                                                                        if (isset($permintaan[0]['group'])) echo ucwords($permintaan[0]['group']); ?>" placeholder="Department" readonly>
    </div>
  </div>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="department">Sub Kontraktor</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="group" name="group" value="<?php
				if(isset($permintaan[0])) echo $permintaan[0]['name_eksternal'];
			?>" disabled>
		</div>
	</div>
  <div class="item form-group">
    <input type="hidden" id="id_main" name="id_main" value="<?php if (isset($permintaan[0]['group'])) echo $permintaan[0]['id_main']; ?>">
    <table id="listItemMaintenance" class="table table-striped table-bordered dt-responsive nowrap" width="100%">
      <thead>
        <tr>
          <th style="width: 5%;">No</th>
          <th style="width: 10%;">No Maintenance</th>
          <th>Nama Maintenance</th>
          <th>Mesin</th>
          <th style="width: 10%;">QTY</th>
          <th>Deskripsi</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $i = 1;
        if (sizeof($detail) > 0) {
          foreach ($detail as $dk) { ?>
            <tr id="trRowItem<?php echo $i; ?>">
              <td></td>
              <td><?php if (isset($dk['no_main'])) echo $dk['no_main']; ?></td>
              <td>
                <input type="text" class="form-control" id="nama_main<?php echo $i; ?>" name="nama_main[]" value="<?php
                                                                                                                  if (isset($dk['nama_main'])) echo $dk['nama_main']; ?>" placeholder="Nama Maitenance" autocomplete="off" readonly>
              </td>
              <td>
                <select class="form-control uom" id="uom_main<?php echo $i; ?>" name="uom_main[]" readonly>
                  <option value="">-- Pilih MESIN --</option>
                  <?php
                  foreach ($material as $uk => $uv) { ?>
                    <option value="<?php echo $uv['id']; ?>" <?php
                                                              if (isset($dk['qty'])) {
                                                                if ($uv['id'] == $dk['unit']) echo "selected";
                                                              }
                                                              ?>><?php echo $uv['stock_name']; ?></option>
                  <?php
                  } ?>
                </select>
              </td>
              <td>
                <input type="number" class="form-control" id="qty_main<?php echo $i; ?>" name="qty_main[]" value="<?php
                                                                                                                  if (isset($dk['qty'])) echo $dk['qty']; ?>" placeholder="Qty" value="1" step=".0001" autocomplete="off" readonly>
              </td>

              <td>
                <textarea class="form-control" id="desc_main<?php echo $i; ?>" name="desc_main[]" placeholder="Deskripsi" readonly><?php
                                                                                                                                    if (isset($dk['deskripsi'])) {
                                                                                                                                      if (($dk['deskripsi'] != '' && $dk['deskripsi'] != 'NULL') && $dk['deskripsi'] != NULL) echo $dk['deskripsi'];
                                                                                                                                    }
                                                                                                                                    ?></textarea>
              </td>
            </tr>
          <?php
            $i++;
          }
        } else { ?>
          <tr>
            <td></td>
            <td></td>
            <td>
              <input type="text" class="form-control" id="nama_main<?php echo $i; ?>" name="nama_main[]" placeholder="Nama Maintenance" autocomplete="off" readonly>
            </td>
            <td>
              <select class="form-control uom" id="uom_main<?php echo $i; ?>" name="uom_main[]" readonly>
                <option value="" selected>-- Pilih UOM --</option>
                <?php
                foreach ($material as $uk => $uv) { ?>
                  <option value="<?php echo $uv['id']; ?>"><?php echo $uv['stock_name']; ?></option>
                <?php
                } ?>
              </select>
            </td>
            <td>
              <input type="number" class="form-control" id="qty_main<?php echo $i; ?>" name="qty_main[]" placeholder="Qty" value="1" step=".0001" autocomplete="off" readonly>
            </td>
            <td>
              <textarea class="form-control" id="desc_main<?php echo $i; ?>" name="desc_main[]" placeholder="Deskripsi" readonly></textarea>
            </td>
            <td>
              <input type="hidden" id="id_detail_main<?php echo $i; ?>" name="id_detail_main[]" value="new">
            </td>
          </tr>
        <?php
        } ?>
      </tbody>
    </table>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Approve Maintenance</button>
    </div>
  </div>
</form>

<script type="text/javascript">
  var idRowItem = '<?php echo $i; ?>';
  var arrIdItem = [];
  var arrNama = [];
  var arrQty = [];
  var arrUom = [];
  var arrDesc = [];

  $(document).ready(function() {
    $("#main_date").datepicker({
      format: 'dd-M-yyyy',
      autoclose: true,
      todayHighlight: true,
    });
    $(".uom").select2();
  });


  $('#edit_main').on('submit', (function(e) {


    $('#btn-submit').attr('disabled', 'disabled');
    $('#btn-submit').text("Memasukkan data...");
    e.preventDefault();

    var formData = new FormData(this);

    $.ajax({
      type: 'POST',
      url: $(this).attr('action'),
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success: function(response) {
        if (response.success == true) {
          swal({
            title: 'Success!',
            text: response.message,
            type: 'success',
            showCancelButton: false,
            confirmButtonText: 'Ok'
          }).then(function() {
            $('#panel-modal').modal('toggle');
            listmaintenance();
          })
        } else {
          $('#btn-submit').removeAttr('disabled');
          $('#btn-submit').text("Simpan Maintenance");
          swal("Failed!", response.message, "error");
        }
      }
    }).fail(function(xhr, status, message) {
      $('#btn-submit').removeAttr('disabled');
      $('#btn-submit').text("Simpan Maintenance");
      swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
    });
  }));
</script>