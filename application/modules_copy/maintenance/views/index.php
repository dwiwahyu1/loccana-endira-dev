<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 450px;
    overflow-y: auto;
    overflow-x: auto
  }

  .scroll-overflow {
    min-height: 350px;
  }

  #panel-modal::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #panel-modal::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #panel-modal::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }
</style>

<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <h4 class="page-title">Maintenance</h4>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-12">
      <div class="card-box">
        <table id="listmaintenance" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th style="width: 5%;">No</th>
              <th style="width: 5%;">No Maintenance</th>
              <th>PIC</th>
              <th>Department</th>
              <th>Date</th>
              <th style="width: 5%;">Worklists</th>
              <th style="width: 15%;">Actions</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>
    </div><!-- end col -->
  </div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog" style="width:90%;">
    <div class="modal-content p-0 b-0">
      <div class="panel panel-color panel-primary panel-filled">
        <div class="panel-heading">
          <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 class="panel-title"></h3>
        </div>
        <div class="panel-body">
          <p></p>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;">
  <div class="modal-dialog" style="width:70%;">
    <div class="modal-content p-0 b-0">
      <div class="panel panel-color panel-primary panel-filled">
        <div class="panel-heading">
          <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 class="panel-title"></h3>
        </div>
        <div class="panel-body">
          <p></p>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild-barang" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true" style="display: none;">
  <div class="modal-dialog" style="width:92%;">
    <div class="modal-content p-0 b-0">
      <div class="panel panel-color panel-primary panel-filled">
        <div class="panel-heading">
          <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 class="panel-title"></h3>
        </div>
        <div class="panel-body">
          <p></p>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
  $(document).ready(function() {
    listmaintenance();
  });

  function add_maintenance() {
    $('#panel-modal').removeData('bs.modal');
    $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    $('#panel-modal  .panel-body').load('<?php echo base_url('maintenance/add'); ?>');
    $('#panel-modal  .panel-title').html('<i class="fa fa-plus"></i> Add Maintenance');
    $('#panel-modal').modal({
      backdrop: 'static',
      keyboard: false
    }, 'show');
  }

  function edit_maintenance(id_permintaan) {
    $('#panel-modal').removeData('bs.modal');
    $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    $('#panel-modal  .panel-body').load('<?php echo base_url('maintenance/edit/'); ?>' + "/" + id_permintaan);
    $('#panel-modal  .panel-title').html('<i class="fa fa-pencil"></i> Edit Maintenance');
    $('#panel-modal').modal({
      backdrop: 'static',
      keyboard: false
    }, 'show');
  }


  function approve_maintenance(id_permintaan) {
    $('#panel-modal').removeData('bs.modal');
    $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    $('#panel-modal  .panel-body').load('<?php echo base_url('maintenance/approve/'); ?>' + "/" + id_permintaan);
    $('#panel-modal  .panel-title').html('<i class="fa fa-pencil"></i> Approve Maintenance');
    $('#panel-modal').modal({
      backdrop: 'static',
      keyboard: false
    }, 'show');
  }

  function detail_maintenance(id_permintaan) {
    $('#panel-modal').removeData('bs.modal');
    $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    $('#panel-modal  .panel-body').load('<?php echo base_url('maintenance/detail_maintenance'); ?>' + "/" + id_permintaan);
    $('#panel-modal  .panel-title').html('<i class="fa fa-search"></i> Detail Maintenance');
    $('#panel-modal').modal({
      backdrop: 'static',
      keyboard: false
    }, 'show');
  }

  function listmaintenance() {
    $("#listmaintenance").dataTable({
      "processing": true,
      "serverSide": true,
      "ajax": "<?php echo base_url() . 'maintenance/lists/'; ?>",
      "searchDelay": 700,
      "responsive": true,
      "lengthChange": false,
      "destroy": true,
      "info": false,
      "bSort": false,
      "dom": 'l<"toolbar">frtip',
      "initComplete": function() {
        $("div.toolbar").prepend(
          '<div class="btn-group pull-left">' +
          '<a class="btn btn-primary" onClick="add_maintenance()">' +
          '<i class="fa fa-plus"></i> Add Maintenance' +
          '</a>' +
          '</div>'
        );
      },
      "columnDefs": [{
        "targets": [0],
        "className": 'dt-body-center'
      }]
    });
  }

  function delete_maintenance(id_main) {
    swal({
      title: 'Yakin akan Menghapus ?',
      text: 'data tidak dapat dikembalikan bila sudah dihapus !',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak'
    }).then(function() {
      var datapost = {
        "id": id_main
      };

      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'maintenance/delete_maintenance'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(r) {
          if (r.success == true) {
            swal({
              title: 'Success!',
              text: r.message,
              type: 'success',
              showCancelButton: false,
              confirmButtonText: 'Ok'
            }).then(function() {
              listmaintenance();
            })
          } else {
            swal("Failed!", r.message, "error");
          }
        }
      });
    });
  }
</script>