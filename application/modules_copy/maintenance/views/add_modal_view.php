<style>
  #listItemMaintenance {
    counter-reset: rowNumber;
  }

  #listItemMaintenance tbody tr>td:first-child {
    counter-increment: rowNumber;
  }

  #listItemMaintenance tbody tr td:first-child::before {
    content: counter(rowNumber);
    min-width: 1em;
    margin-right: 0.5em;
  }
</style>

<form class="form-horizontal form-label-left" id="add_maintenance" role="form" action="<?php echo base_url('maintenance/add_maintenance'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
  <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="main_date">Maintenance Date <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="text" class="form-control" id="main_date" name="main_date" value="<?php
                                                                                      echo date('d-M-Y'); ?>" placeholder="Work Date" required>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="pic">PIC</label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="text" class="form-control" id="pic" name="pic" value="<?php
                                                                          echo ucwords($this->session->userdata['logged_in']['nama']); ?>" placeholder="PIC" readonly>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="department">Department</label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="text" class="form-control" id="department" name="department" value="<?php
                                                                                        echo ucwords($this->session->userdata['logged_in']['name_role']); ?>" placeholder="Department" readonly>
    </div>
  </div>
  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="department">Sub Kontraktor</label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <select class="form-control" id="subkon_id" name="subkon_id">
        <option value="" selected>-- Pilih Sub Kontraktor --</option>
        <?php
        foreach ($customer as $ck => $cv) { ?>
          <option value="<?php echo $cv['id']; ?>"><?php echo $cv['name_eksternal']; ?></option>
        <?php
        } ?>
      </select>
    </div>
  </div>
  <div class="item form-group">
    <table id="listItemMaintenance" class="table table-striped table-bordered dt-responsive nowrap" width="100%">
      <thead>
        <tr>
          <th style="width: 5%;">No</th>
          <th>Nama Maintenance</th>
          <th>Mesin</th>
          <th style="width: 10%;">QTY</th>
          <!-- <th style="width: 20%;">Sub Kontraktor</th> -->
          <th>Deskripsi</th>
          <th style="width: 5%;"><a id="btn_item_add" class="btn btn-primary"><i class="fa fa-plus"></i></a></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td></td>
          <td>
            <input type="text" class="form-control" id="nama_maintenance1" name="nama_maintenance[]" placeholder="Nama Maintenance" autocomplete="off" required>
          </td>
          <td>
            <select class="form-control" id="uom_maintenance1" name="uom_maintenance[]">
              <option value="" selected>-- Pilih Mesin --</option>
              <?php
              foreach ($material as $uk => $uv) { ?>
                <option value="<?php echo $uv['id']; ?>"><?php echo $uv['stock_name']; ?></option>
              <?php
              } ?>
            </select>
          </td>
          <td>
            <input type="number" class="form-control" id="qty_maintenance1" name="qty_maintenance[]" placeholder="Qty" value="1" step=".0001" autocomplete="off" required>
          </td>
          <!-- <td>
            <select class="form-control" id="subkon_id1" name="subkon_id[]">
              <option value="" selected>-- Pilih Sub Kontraktor --</option>
              <?php
              foreach ($customer as $ck => $cv) { ?>
                <option value="<?php echo $cv['id']; ?>"><?php echo $cv['name_eksternal']; ?></option>
              <?php
              } ?>
            </select>
          </td> -->
          <td>
            <textarea class="form-control" id="desc_maintenance1" name="desc_maintenance[]" placeholder="Deskripsi"></textarea>
          </td>
          <td></td>
        </tr>
      </tbody>
    </table>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Maintenance</button>
    </div>
  </div>
</form>

<script type="text/javascript">
  var idRowItem = 1;
  var arrNama = [];
  var arrQty = [];
  var arrUom = [];
  var arrDesc = [];

  $(document).ready(function() {
    $("#main_date").datepicker({
      format: 'dd-M-yyyy',
      autoclose: true,
      todayHighlight: true,
    });

    $('#uom_maintenance1').select2();
    $('#subkon_id').select2();
  });

  $('#btn_item_add').on('click', function() {
    idRowItem++;
    $('#listItemMaintenance tbody').append(
      '<tr id="trRowItem' + idRowItem + '">' +
      '<td></td>' +
      '<td>' +
      '<input type="text" class="form-control" id="nama_maintenance' + idRowItem + '" name="nama_maintenance[]" placeholder="Nama Pekerjaan" autocomplete="off" required>' +
      '</td>' +
      '<td>' +
      '<select class="form-control" id="uom_maintenance' + idRowItem + '" name="uom_maintenance[]">' +
      '<option value="" selected >-- Pilih Mesin --</option>' +
      '</select>' +
      '</td>' +
      '<td>' +
      '<input type="number" class="form-control" id="qty_maintenance' + idRowItem + '" name="qty_maintenance[]" placeholder="Qty" value="1" step=".0001" autocomplete="off" required>' +
      '</td>' +
      // '<td>' +
      // '<select class="form-control" id="subkon_id' + idRowItem + '" name="subkon_id[]">' +
      // '<option value="" selected >-- Pilih Sub Kontraktor --</option>' +
      // '</select>' +
      // '</td>' +
      '<td>' +
      '<textarea class="form-control" id="desc_maintenance' + idRowItem + '" name="desc_maintenance[]" placeholder="Deskripsi"></textarea>' +
      '</td>' +
      '<td>' +
      '<a class="btn btn-danger" onclick="removeRow(' + idRowItem + ')"><i class="fa fa-minus"></i></a>' +
      '</td>' +
      '</tr>'
    );
    $("#uom_maintenance" + idRowItem).select2();
    // $("#subkon_id" + idRowItem).select2();
    $('#uom_maintenance' + idRowItem).attr('readonly', 'readonly');

    $.ajax({
      type: "GET",
      url: "<?php echo base_url('maintenance/get_material'); ?>",
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      success: function(r) {
        if (r.length > 0) {
          for (var i = 0; i < r.length; i++) {
            var newOption = new Option(r[i].stock_name, r[i].id, false, false);
            $('#uom_maintenance' + idRowItem).append(newOption);
          }
          $('#uom_maintenance' + idRowItem).removeAttr('readonly');
        } else $('#uom_maintenance' + idRowItem).removeAttr('readonly');
      }
    });
    // $.ajax({
    //   type: "GET",
    //   url: "<?php echo base_url('maintenance/get_subkon'); ?>",
    //   dataType: 'json',
    //   contentType: 'application/json; charset=utf-8',
    //   success: function(r) {
    //     if (r.length > 0) {
    //       for (var i = 0; i < r.length; i++) {
    //         var newOption = new Option(r[i].name_eksternal, r[i].id, false, false);
    //         $('#subkon_id' + idRowItem).append(newOption);
    //       }
    //       $('#subkon_id' + idRowItem).removeAttr('readonly');
    //     } else $('#subkon_id' + idRowItem).removeAttr('readonly');
    //   }
    // });
  })

  function removeRow(rowItem) {
    $('#trRowItem' + rowItem).remove();
  }

  $('#add_maintenance').on('submit', (function(e) {
    arrNama = [];
    arrQty = [];
    arrUom = [];
    arrDesc = [];

    $('#btn-submit').attr('disabled', 'disabled');
    $('#btn-submit').text("Memasukkan data...");
    e.preventDefault();

    $('input[name="nama_maintenance[]"]').each(function() {
      if (this.value) {
        if (this.value != undefined && this.value != '') arrNama.push(this.value);
      }
    })

    $('input[name="qty_maintenance[]"]').each(function() {
      if (this.value) {
        if (this.value != undefined && this.value != '') arrQty.push(this.value);
      }
    })

    $('select[name="uom_maintenance[]"]').each(function() {
      if (this.value) {
        if (this.value != undefined && this.value != '') arrUom.push(this.value);
      }
    })
    // $('select[name="subkon_id[]"]').each(function() {
    //   if (this.value) {
    //     if (this.value != undefined && this.value != '') arrSubkon.push(this.value);
    //   }
    // })

    $('textarea[name="desc_maintenance[]"]').each(function() {
      if (this.value) {
        if (this.value != undefined && this.value != '') arrDesc.push(this.value);
        else arrDesc.push('NULL');
      } else arrDesc.push('NULL');
    })

    if (arrNama.length > 0 && arrQty.length > 0 && arrUom.length > 0 && arrDesc.length > 0) {
      var formData = new FormData();
      formData.set('main_date', $('#main_date').val());
      formData.set('subkon_id', $('#subkon_id').val());
      formData.set('arrNama', arrNama);
      formData.set('arrQty', arrQty);
      formData.set('arrUom', arrUom);
      formData.set('arrDesc', arrDesc);

      $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
          if (response.success == true) {
            swal({
              title: 'Success!',
              text: response.message,
              type: 'success',
              showCancelButton: false,
              confirmButtonText: 'Ok'
            }).then(function() {
              $('#panel-modal').modal('toggle');
              listmaintenance();
            })
          } else {
            $('#btn-submit').removeAttr('disabled');
            $('#btn-submit').text("Tambah Maintenance");
            swal("Failed!", response.message, "error");
          }
        }
      }).fail(function(xhr, status, message) {
        $('#btn-submit').removeAttr('disabled');
        $('#btn-submit').text("Tambah Maintenance");
        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
      });
    } else {
      $('#btn-submit').removeAttr('disabled');
      $('#btn-submit').text("Tambah Maintenance");
      swal("Failed!", "Invalid Inputan List Maintenance, silahkan cek kembali.", "error");
    }
  }));
</script>