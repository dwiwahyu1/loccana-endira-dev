<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Maintenance_Model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }

  public function lists($params = array())
  {
    $sql   = 'CALL maintenance_list(?, ?, ?, ?, ?, @total_filtered, @total)';

    $out = array();
    $query   =  $this->db->query(
      $sql,
      array(
        $params['limit'],
        $params['offset'],
        $params['order_column'],
        $params['order_dir'],
        $params['filter']
      )
    );

    $result = $query->result_array();

    $this->load->helper('db');
    free_result($this->db->conn_id);

    $total = $this->db->query('select @total_filtered, @total')->row_array();

    $return = array(
      'data' => $result,
      'total_filtered' => $total['@total_filtered'],
      'total' => $total['@total']
    );

    return $return;
  }

  public function uom()
  {
    $sql   = 'CALL m_uom(?)';

    $out = array();
    $query   =  $this->db->query($sql, array(
      NULL
    ));

    $result = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function customer()
  {
    $query =   $this->db->query("SELECT id,name_eksternal,kode_eksternal 
      FROM t_eksternal
      WHERE type_eksternal = 3
      ");

    $result = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }
  public function get_maintenance($id)
  {
    $sql   = 'CALL maintenance_search_id(?)';

    $query   = $this->db->query($sql, array(
      $id
    ));

    $result = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function add_maintenance($data)
  {
    $sql   = 'CALL maintenance_add(?,?,?)';
    $query   =  $this->db->query($sql, array(
      $data['main_date'],
      $data["pic"],
      $data["subkon_id"],
    ));

    $query = $this->db->query('SELECT LAST_INSERT_ID()');
    $row = $query->row_array();
    $lastid = $row['LAST_INSERT_ID()'];

    $result['result'] = $this->db->affected_rows();
    $result['lastid'] = $lastid;

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function add_detail_maintenance($data)
  {
    $sql   = 'CALL maintenance_detail_add(?,?,?,?,?,?)';
    $query   =  $this->db->query($sql, array(
      $data['id_main'],
      $data['no_main'],
      $data['nama_main'],
      $data['deskripsi'],
      $data['qty'],
      $data["unit"],
      // $data["subkon_id"]
    ));

    $result = $this->db->affected_rows();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function edit_maintenance($data)
  {
    $sql   = 'CALL maintenance_edit(?,?,?,?)';
    $query   =  $this->db->query($sql, array(
      $data['id_main'],
      $data['main_date'],
      $data["pic"],
      $data["subkon_id"]
    ));

    $result = $this->db->affected_rows();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function edit_detail_maintenance($data)
  {
    $sql   = 'CALL maintenance_detail_edit(?,?,?,?,?)';
    $query   =  $this->db->query($sql, array(
      $data['id_detail'],
      $data['nama_main'],
      $data['deskripsi'],
      $data['qty'],
      $data["unit"]
    ));

    $result = $this->db->affected_rows();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function detail_maintenance($id_main)
  {
    $sql   = 'CALL maintenance_detail_search_id_main(?)';

    $query   = $this->db->query($sql, array(
      $id_main
    ));

    $result = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function detail_maintenance_by_id_detail($id_main)
  {
    $sql   = 'CALL maintenance_detail_search_id_detail(?)';

    $query   = $this->db->query($sql, array(
      $id_main
    ));

    $result = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }
  public function get_detail_pekerjaan($id)
  {
    $sql   = 'CALL permintaan_pekerjaan_detail_search_id_detail(?)';

    $query   = $this->db->query($sql, array(
      $id
    ));

    $result = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  function delete_maintenance($data)
  {
    $sql   = 'CALL maintenance_delete(?)';

    $query   = $this->db->query($sql, array(
      $data['id']
    ));

    $result = $this->db->affected_rows();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function delete_detail_pekerjaan($data)
  {
    $sql   = 'CALL permintaan_pekerjaan_detail_delete(?)';

    $query   = $this->db->query($sql, array(
      $data['id']
    ));
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }
  public function get_material()
  {
    $sql   = 'select id,stock_name
    from m_material
    where type IN (9,12)';

    $query   = $this->db->query($sql, array());

    $result = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }
  
  public function get_subkon()
  {
    $sql   = 'select id,name_eksternal
    from t_eksternal
    where type_eksternal IN (3)';

    $query   = $this->db->query($sql, array());

    $result = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }
  public function approve_maintenance($data)
  {
    $sql   =
      'UPDATE `t_maintenance` SET
          `status` = 1
        WHERE `id_main` = ' . $data['id_main'];

    $query   = $this->db->query($sql);

    $result = $this->db->affected_rows();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }
  public function add_mutasi_maintenance($data)
  {
    // $this->save_edit_material($data);
    $sql   = 'CALL mutasi_add(?,?,?,?,?)';
    $query   =  $this->db->query(
      $sql,
      array(
        $data['id_detail'],
        $data['id_detail'],
        $data['date'],
        $data['qty'],
        1
      )
    );

    $result  = $this->db->affected_rows();
    return $result;
  }
}
