<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Maintenance extends MX_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('maintenance/maintenance_model');
    $this->load->library('sequence');
    $this->load->library('log_activity');
  }

  /**
   * anti sql injection
   */
  public function Anti_sql_injection($string)
  {
    $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
    return $string;
  }

  public function index()
  {
    $this->template->load('maintemplate', 'maintenance/views/index');
  }

  function lists()
  {
    $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
    $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
    $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
    $order = $this->input->get_post('order');
    $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
    $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

    $order_fields = array('a.tanggal_main', 'b.nama', 'd.group');

    $search = $this->input->get_post('search');

    $search_val = (!empty($search['value'])) ? $search['value'] : null;

    $search_value = $this->Anti_sql_injection($search_val);

    // Build params for calling model
    $params['limit'] = (int) $length;
    $params['offset'] = (int) $start;
    $params['order_column'] = $order_fields[$order_column];
    $params['order_dir'] = $order_dir;
    $params['filter'] = $search_value;

    $list = $this->maintenance_model->lists($params);

    /*echo "<pre>";
		print_r($list);
		echo "</pre>";
		die;*/

    $result["recordsTotal"] = $list['total'];
    $result["recordsFiltered"] = $list['total_filtered'];
    $result["draw"] = $draw;

    $data = array();
    $strStatus = '';
    $no = $start;
    foreach ($list['data'] as $k => $v) {
      if ($v['id_main'] != NULL) {
        $no++;
        $strButton =
          '<div class="btn-group">' .
          '<button class="btn btn-info btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Details" onclick="detail_maintenance(\'' . $v['id_main'] . '\')">' .
          '<i class="fa fa-search"></i>' .
          '</button>' .
          '</div>';

        if ($this->session->userdata['logged_in']['user_id'] == $v['id_pic'] || $this->session->userdata['logged_in']['id_role'] == 99) {
          $strButtonEdit =
            '<div class="btn-group">' .
            '<button class="btn btn-warning btn-sm"title="Edit" onClick="edit_maintenance(\'' . $v['id_main'] . '\')">' .
            '<i class="fa fa-edit"></i>' .
            '</button>' .
            '</div>';
        } else $strButtonEdit = '';

        if ($this->session->userdata['logged_in']['user_id'] == $v['id_pic'] || $this->session->userdata['logged_in']['id_role'] == 99) {
          $strButtonDel =
            '<div class="btn-group">' .
            '<button class="btn btn-danger btn-sm"title="Delete" onClick="delete_maintenance(\'' . $v['id_main'] . '\')">' .
            '<i class="fa fa-trash"></i>' .
            '</button>' .
            '</div>';
        } else $strButtonDel = '';

        if ($v['status'] != 1) {
          $strButtonApprove =
            '<div class="btn-group">' .
            '<button class="btn btn-success btn-sm"title="Approve" onClick="approve_maintenance(\'' . $v['id_main'] . '\')">' .
            '<i class="fa fa-check"></i>' .
            '</button>' .
            '</div>';
          $strButton .= $strButtonEdit . $strButtonDel . $strButtonApprove;
        } else
          $strButton = $strButton;
        array_push($data, array(
          $no,
          $v['no_main'],
          ucwords($v['nama']),
          ucwords($v['group']),
          date('d-M-Y', strtotime($v['tanggal_main'])),
          $v['worklists'],
          $strButton
        ));
      }
    }

    $result["data"] = $data;

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }
  public function customer()
  {
    $query =   $this->db->query("SELECT id,name_eksternal,kode_eksternal 
      FROM t_eksternal
      WHERE id = 195
      ");

    $result = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }
  public function add()
  {
    $customer     = $this->maintenance_model->customer();
    $uom = $this->maintenance_model->uom();
    $material = $this->maintenance_model->get_material();

    $data = array(
      'customer'    => $customer,
      'uom' => $uom,
      'material' => $material
    );

    $this->load->view('add_modal_view', $data);
  }

  public function get_uom()
  {
    $result = $this->maintenance_model->uom();

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($result);
  }
  public function get_material()
  {
    $result = $this->maintenance_model->get_material();

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($result);
  }
  public function get_subkon()
  {
    $result = $this->maintenance_model->customer();

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($result);
  }
  public function add_maintenance()
  {
    $this->form_validation->set_rules('main_date', 'Maintenance Date', 'trim|required');

    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));

      $result = array('success' => false, 'message' => $msg);
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      $tgl_main_date    = $this->Anti_sql_injection($this->input->post('main_date', TRUE));
      $temp_main_date   = explode("-", $tgl_main_date);
      $pic        = $this->session->userdata['logged_in']['user_id'];

      $tempNama    = $this->Anti_sql_injection($this->input->post('arrNama', TRUE));
      $arrNama    = explode(',', $tempNama);

      $tempQty    = $this->Anti_sql_injection($this->input->post('arrQty', TRUE));
      $arrQty      = explode(',', $tempQty);

      $tempUom    = $this->Anti_sql_injection($this->input->post('arrUom', TRUE));
      $arrUom      = explode(',', $tempUom);

      $tempDesc    = $this->Anti_sql_injection($this->input->post('arrDesc', TRUE));
      $arrDesc    = explode(',', $tempDesc);

      $subkon_id    = $this->Anti_sql_injection($this->input->post('subkon_id', TRUE));
      // $arrSubkon    = explode(',', $tempSubKon);

      $dataMaintenance = array(
        'main_date'    => date('Y-m-d', strtotime($temp_main_date[2] . '-' . $temp_main_date[1] . '-' . $temp_main_date[0])),
        'pic'      => $pic,
        'subkon_id' => $subkon_id
      );

      /*echo "<pre>";
			print_r($dataMaintenance);
			print_r($arrNama);
			print_r($arrQty);
			print_r($arrUom);
			print_r($arrDesc);
			echo "</pre>";
			die;*/

      $resultDetail = false;
      $resultAddMain = $this->maintenance_model->add_maintenance($dataMaintenance);
      if ($resultAddMain['result'] > 0) {
        $no = $this->sequence->get_no('maintenance_req');
        for ($i = 0; $i < sizeof($arrNama); $i++) {

          if ($arrDesc[$i] != '' && $arrDesc[$i] != 'NULL') $deskripsi = $arrDesc[$i];
          else $deskripsi = NULL;

          $dataDetail = array(
            'id_main'    => $resultAddMain['lastid'],
            'no_main'    => $no,
            'nama_main'    => $arrNama[$i],
            'qty'      => $arrQty[$i],
            'unit'      => $arrUom[$i],
            'deskripsi'    => $deskripsi,
            // 'subkon_id'    => $subkon_id
          );

          $resultAddDetail = $this->maintenance_model->add_detail_maintenance($dataDetail);
          if ($resultAddDetail > 0) $resultDetail = true;
          else {
            $resultDetail = false;
            $msg = 'Gagal menambahkan Detail Maintenance No.' . ($i + 1) . ' ke database';
            $this->log_activity->insert_activity('insert', $msg);
            $result = array('success' => false, 'message' => $msg);
            break;
          }
        }

        if ($resultDetail == true) {
          $msg = 'Berhasil menambahkan Maintenance ke database';
          $this->log_activity->insert_activity('insert', $msg);
          $result = array('success' => true, 'message' => $msg);
        }
      } else {
        $msg = 'Gagal menambahkan Detail Maintenance ke database';
        $this->log_activity->insert_activity('insert', $msg);
        $result = array('success' => false, 'message' => $msg);
      }
    }

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function edit($id_permintaan)
  {
    $permintaan   = $this->maintenance_model->get_maintenance($id_permintaan);
    $detail     = $this->maintenance_model->detail_maintenance($id_permintaan);
    $uom       = $this->maintenance_model->uom();
    $material       = $this->maintenance_model->get_material();
    $customer     = $this->maintenance_model->customer();
    // var_dump($detail);die;
    $data = array(
      'permintaan'  => $permintaan,
      'detail'    => $detail,
      'uom'      => $uom,
      'material' => $material,
      'customer' => $customer
    );

    // $this->load->view('edit_modal_view_backup', $data);
    $this->load->view('edit_modal_view', $data);
  }

  public function edit_maintenance()
  {
    // $this->form_validation->set_rules('work_date', 'Work Date', 'trim|required');


    $id_main    = $this->Anti_sql_injection($this->input->post('id_main', TRUE));
    $tgl_main    = $this->Anti_sql_injection($this->input->post('main_date', TRUE));
    $subkon_id    = $this->Anti_sql_injection($this->input->post('subkon_id', TRUE));
    $temp_main_date   = explode("-", $tgl_main);
    $pic        = $this->session->userdata['logged_in']['user_id'];

    $tempIdItem    = $this->Anti_sql_injection($this->input->post('arrIdItem', TRUE));
    $arrIdItem    = explode(',', $tempIdItem);

    $tempNama    = $this->Anti_sql_injection($this->input->post('arrNama', TRUE));
    $arrNama    = explode(',', $tempNama);

    $tempQty    = $this->Anti_sql_injection($this->input->post('arrQty', TRUE));
    $arrQty      = explode(',', $tempQty);

    $tempUom    = $this->Anti_sql_injection($this->input->post('arrUom', TRUE));
    $arrUom      = explode(',', $tempUom);

    $tempDesc    = $this->Anti_sql_injection($this->input->post('arrDesc', TRUE));
    $arrDesc    = explode(',', $tempDesc);

    $dataDetailMaintenance   = $this->maintenance_model->get_maintenance($id_main)[0];
    // echo"<pre>";print_r($dataDetailMaintenance);die;
    $dataMaintenance = array(
      'id_main'  => $id_main,
      'main_date'    => date('Y-m-d', strtotime($temp_main_date[2] . '-' . $temp_main_date[1] . '-' . $temp_main_date[0])),
      'pic'      => $pic,
      'subkon_id'      => $subkon_id
    );
    if ($dataMaintenance['main_date'] != $dataDetailMaintenance['tanggal_main'] || $dataMaintenance['pic'] != $dataDetailMaintenance['id_pic'] || $dataMaintenance['subkon_id'] != $dataDetailMaintenance['subkon_id']) $resultEditMaintenance = $this->maintenance_model->edit_maintenance($dataMaintenance);
    else $resultEditMaintenance = 1;
    $resultDetail = false;
    for ($i = 0; $i < sizeof($arrIdItem); $i++) {
      if ($arrDesc[$i] != '' && $arrDesc[$i] != 'NULL') $deskripsi = $arrDesc[$i];
      else $deskripsi = NULL;

      if ($arrIdItem[$i] != 'new') {
        $dataDetail = array(
          'id_detail'      => $arrIdItem[$i],
          'nama_main'  => $arrNama[$i],
          'qty'        => $arrQty[$i],
          'unit'        => $arrUom[$i],
          'deskripsi'      => $deskripsi
        );
        $detail = $this->maintenance_model->detail_maintenance_by_id_detail($arrIdItem[$i])[0];

        if ($dataDetail['nama_main'] != $detail['nama_main'] || $dataDetail['qty'] != $detail['qty'] || $dataDetail['unit'] != $detail['unit'] || $dataDetail['deskripsi'] != $detail['deskripsi']) $resultEditMaintenance = $this->maintenance_model->edit_detail_maintenance($dataDetail);
        else $resultEditMaintenance = 1;
        if ($resultEditMaintenance > 0) $resultDetail = true;
        else {
          $msg = 'Gagal update Detail Maintenance yang ke-' . ($i + 1) . ' database';

          $this->log_activity->insert_activity('update', $msg . ' dengan ID : ' . $id_main);
          $result = array('success' => false, 'message' => $msg);
          $resultDetail = false;
          break;
        }
      } else {
        $no = $dataDetailMaintenance['no_main'];

        $dataDetail = array(
          'id_main'    => $id_main,
          'nama_main'  => $arrNama[$i],
          'no_main'    => $no,
          'qty'        => $arrQty[$i],
          'unit'        => $arrUom[$i],
          'deskripsi'      => $deskripsi,
        );

        $resultAddDetailmaintenance = $this->maintenance_model->add_detail_maintenance($dataDetail);
        if ($resultAddDetailmaintenance > 0) $resultDetail = true;
        else {
          $msg = 'Gagal update Detail Maintenance yang ke-' . ($i + 1) . ' database';

          $this->log_activity->insert_activity('update', $msg . ' dengan ID : ' . $id_main);
          $result = array('success' => false, 'message' => $msg);
          $resultDetail = false;
          break;
        }
      }
    }

    if ($resultEditMaintenance > 0 && $resultDetail == true) {
      $msg = 'Berhasil update Maintenance ke database';

      $this->log_activity->insert_activity('update', $msg . ' dengan ID : ' . $id_main);
      $result = array('success' => true, 'message' => $msg);
    } else {
      if ($resultDetail != false) {
        $msg = 'Gagal update Maintenance ke database';

        $this->log_activity->insert_activity('update', $msg . ' dengan ID : ' . $id_main);
        $result = array('success' => false, 'message' => $msg);
      }
    }


    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function detail_maintenance($id_main)
  {
    $maintenance   = $this->maintenance_model->get_maintenance($id_main);
    $detail     = $this->maintenance_model->detail_maintenance($id_main);

    $data = array(
      'maintenance'  => $maintenance,
      'detail'    => $detail
    );

    /*echo "<pre>";
		print_r($data);
		echo "</pre>";
		die;*/

    $this->load->view('detail_modal_view', $data);
  }

  public function delete_maintenance()
  {
    $data     = file_get_contents("php://input");
    $params     = json_decode($data, true);

    /*echo "<pre>";
		print_r($params);
		echo "</pre>";
		die;*/

    $resultDelete = $this->maintenance_model->delete_maintenance($params);
    if ($resultDelete > 0) {
      $msg = 'Berhasil menghapus Data Maintenance';
      $this->log_activity->insert_activity('delete', $msg . ' dengan ID : ' . $params['id']);
      $result = array('success' => true, 'message' => $msg);
    } else {
      $msg = 'Gagal menghapus Data Maintenance';
      $this->log_activity->insert_activity('delete', $msg . ' dengan ID : ' . $params['id']);
      $result = array('success' => false, 'message' => $msg);
    }

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function delete_detail_maintenance()
  {
    $data     = file_get_contents("php://input");
    $params     = json_decode($data, true);

    $resultDelete = $this->maintenance_model->delete_detail_pekerjaan($params);
    if ($resultDelete > 0) {
      $msg = 'Berhasil menghapus Detail Permintaan Pekerjaan';
      $this->log_activity->insert_activity('delete', $msg . ' dengan ID : ' . $params['id']);
      $result = array('success' => true, 'message' => $msg);
    } else {
      $msg = 'Gagal menghapus Detail Permintaan Pekerjaan';
      $this->log_activity->insert_activity('delete', $msg . ' dengan ID : ' . $params['id']);
      $result = array('success' => false, 'message' => $msg);
    }

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }
  public function approve($id_permintaan)
  {
    $permintaan   = $this->maintenance_model->get_maintenance($id_permintaan);
    $detail     = $this->maintenance_model->detail_maintenance($id_permintaan);
    $uom       = $this->maintenance_model->uom();
    $material       = $this->maintenance_model->get_material();
    // var_dump($detail);die;
    $data = array(
      'permintaan'  => $permintaan,
      'detail'    => $detail,
      'uom'      => $uom,
      'material' => $material
    );

    // $this->load->view('edit_modal_view_backup', $data);
    $this->load->view('approval_modal_view', $data);
  }
  public function approve_maintenance()
  {
    $id_main    = $this->Anti_sql_injection($this->input->post('id_main', TRUE));
    $tgl_main    = $this->Anti_sql_injection($this->input->post('main_date', TRUE));
    $temp_main_date   = explode("-", $tgl_main);
    $pic        = $this->session->userdata['logged_in']['user_id'];

    $arrIdItem    = ($this->input->post('uom_main', TRUE));

    $arrQty    = ($this->input->post('qty_main', TRUE));
    $dataMaintenance = array(
      'id_main'  => $id_main,
      'main_date'    => date('Y-m-d', strtotime($temp_main_date[2] . '-' . $temp_main_date[1] . '-' . $temp_main_date[0])),
      'pic'      => $pic
    );
    $this->maintenance_model->approve_maintenance($dataMaintenance);
    for ($i = 0; $i < sizeof($arrIdItem); $i++) {

      $dataDetail = array(
        'id_detail'  => $arrIdItem[$i],
        'qty'        => $arrQty[$i],
        'date'       => $dataMaintenance['main_date']
      );
      $resultDetail = $this->maintenance_model->add_mutasi_maintenance($dataDetail);
    }

    if ($resultDetail == true) {
      $msg = 'Berhasil update Maintenance ke database';

      $this->log_activity->insert_activity('update', $msg . ' dengan ID : ' . $id_main);
      $result = array('success' => true, 'message' => $msg);
    } else {
      $msg = 'Gagal update Maintenance ke database';

      $this->log_activity->insert_activity('update', $msg . ' dengan ID : ' . $id_main);
      $result = array('success' => false, 'message' => $msg);
    }
    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }
}
