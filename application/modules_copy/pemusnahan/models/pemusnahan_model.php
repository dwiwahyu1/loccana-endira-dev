<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pemusnahan_model extends CI_Model {
	
	public function __construct() {
		
		parent::__construct();
		
	}

	public function lists($params = array())
	{
		$sql 	= 'CALL pemusnahan_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();
	
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);
		
		return $return;
	}

	public function pemusnahan_list_material()
	{
		$sql 	= 'CALL pemusnahan_list_material()';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function pemusnahan_stok_list_by_type($data)
	{
		$sql 	= 'CALL pemusnahan_stok_list_by_type(?)';

		$query 	= $this->db->query($sql,
			array(
				$data['id_type_material']
			));
			
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function insert_pemusnahan($data)
	{
		$sql 	= 'CALL pemusnahan_add(?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['no_pemusnahan'],
			$data['tanggal_pengajuan'],
			$data['tanggal_pemusnahan'],
			$data['file_pemusnahan'],
			$data['keterangan']
		));
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function insert_pemusnahan_detail($data)
	{	
		$sql 	= 'CALL pemusnahan_detail_add(?,?,?,?)';

		$query 	= $this->db->query($sql,
			array(
				$data['id_pemusnahan'],
				$data['id_material'],
				$data['jml_qty'],
				$data['keterangan']
		));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function update_pemusnahan_qty($data)
	{	
		$sql 	= 'CALL pemusnahan_update_qty(?,?)';

		$query 	= $this->db->query($sql,
			array(
				$data['id_material'],
				$data['jml_qty']
		));
		
		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function pemusnahan_detail_id($id_pemusnahan)
	{
		$sql 	= 'CALL pemusnahan_detail_id(?)';

		$query 	= $this->db->query($sql,
			array(
				$id_pemusnahan
			));
			
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function pemusnahan_list_material_id($id_pemusnahan)
	{
		$sql 	= 'CALL pemusnahan_list_material_id(?)';

		$query 	= $this->db->query($sql,
			array(
				$id_pemusnahan
			));
			
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function update_pemusnahan($data) {
		$sql 	= 'CALL pemusnahan_update(?,?,?,?,?,?)';
		$query 	=  $this->db->query($sql,
			array(
				$data['id_pemusnahan'],
				$data['no_pemusnahan'],
				$data['tanggal_pengajuan'],
				$data['tanggal_pemusnahan'],
				$data['file_pemusnahan'],
				$data['keterangan']
			)
		);
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function pemusnahan_delete($data)
	{
		$sql 	= 'CALL pemusnahan_delete(?)';
		$query 	= $this->db->query($sql,
			array(
				$data['id_pemusnahan']
			)
		);
			
		$return	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function pemusnahan_delete1($data)
	{
		$sql 	= 'CALL pemusnahan_delete1(?)';
		$query 	= $this->db->query($sql,
			array(
				$data['id_pemusnahan']
			)
		);
			
		$return	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function pemusnahan_delete2($data)
	{
		$sql 	= 'CALL pemusnahan_delete2(?)';
		$query 	= $this->db->query($sql,
			array(
				$data['id_pemusnahan']
			)
		);
			
		$return	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
}