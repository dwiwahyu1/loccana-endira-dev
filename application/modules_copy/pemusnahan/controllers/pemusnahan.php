<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pemusnahan extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('pemusnahan/pemusnahan_model');
		$this->load->library('log_activity');
		$this->load->library('sequence');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'pemusnahan/views/index');
	}

	function lists() {
        $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
        $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
        $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
        $order = $this->input->get_post('order');
        $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'DESC';
        $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

        $order_fields = array('','no_pemusnahan', 'tanggal_input');

        $search = $this->input->get_post('search');

        $search_val = (!empty($search['value'])) ? $search['value'] : null;

        $search_value = $this->Anti_sql_injection($search_val);

        // Build params for calling model
        $params['limit'] = (int) $length;
        $params['offset'] = (int) $start;
        $params['order_column'] = $order_fields[$order_column];
        $params['order_dir'] = $order_dir;
        $params['filter'] = $search_value;
		
        $list = $this->pemusnahan_model->lists($params);
		
        $result["recordsTotal"] = $list['total'];
        $result["recordsFiltered"] = $list['total_filtered'];
        $result["draw"] = $draw;
		
        $data = array();
		$no=1;
        foreach ($list['data'] as $k => $v) {
            $no = $k+1;
			
			if($v['file'] != '' || $v['file'] != NULL) {
				$downloadBtn =
					'<div class="btn-group">'.
						'<a href="'. $v['file'] .'" class="btn btn-info btn-sm" target="_blank" title="Download File" download>'.
							'<i class="fa fa-file"></i>'.
						'</a>'.
					'</div>';
			}
			
			$actions = '<div class="btn-group">';
			$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit Pemusnahan" onClick="pemusnahan_edit(\'' . $v['id_pemusnahan'] . '\')">';
			$actions .= '       <i class="fa fa-edit"></i>';
			$actions .= '   </button>';
			$actions .= '</div>';
			$actions .= '<div class="btn-group">';
			$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Detail Pemusnahan" onClick="pemusnahan_detail(\'' . $v['id_pemusnahan'] . '\')">';
			$actions .= '       <i class="fa fa-search"></i>';
			$actions .= '   </button>';
			$actions .= '</div>';
			$actions .= '<div class="btn-group">';
			$actions .= '   <button class="btn btn-icon waves-effect waves-light btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Hapus Data Pemusnahan" onClick="pemusnahan_delete(\'' . $v['id_pemusnahan'] . '\')">';
			$actions .= '       <i class="fa fa-trash"></i>';
			$actions .= '   </button>';
			$actions .= '</div>';
			
            array_push($data, array(
                $no,
                $v['no_pemusnahan'],
                date('d F Y', strtotime($v['tanggal_input'])),
                date('d F Y', strtotime($v['tanggal_pengajuan'])),
                date('d F Y', strtotime($v['tanggal_pemusnahan'])),
                $downloadBtn,
                $v['keterangan'],
                $actions
				)
            );
        }
		
        $result["data"] = $data;
		
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
	
	public function pemusnahan_add() {
		$this->load->view('pemusnahan_add_view');
	}
	
	public function pemusnahan_add_item() {
		$results = $this->pemusnahan_model->pemusnahan_list_material();
		
		$data = array(
			'pemusnahan' => $results
		);

		$this->load->view('pemusnahan_add_item_view',$data);
	}
	
	public function pemusnahan_edit($id_pemusnahan) {
		$results = $this->pemusnahan_model->pemusnahan_detail_id($id_pemusnahan);
		
		$file = explode('.',$results[0]['file']);
		$files = explode('/',$results[0]['file']);
		$filetype = $file[1];
		$fileupload = $files[2];
		$dir_image = $results[0]['file'];
		
		$data = array(
			'dir_image' => $dir_image,
			'pemusnahan' => $results,
			'filetype' => $filetype,
			'fileupload' => $fileupload
		);
		
		$this->load->view('pemusnahan_edit_view',$data);
	}
	
	public function pemusnahan_edit_item() {
		$results =  $this->pemusnahan_model->pemusnahan_list_material();
	
		$data = array(
			'pemusnahan' => $results
		);
		
		$this->load->view('pemusnahan_edit_item_view',$data);
	}
	
	public function pemusnahan_detail($id_pemusnahan) {
		$results = $this->pemusnahan_model->pemusnahan_list_material_id($id_pemusnahan);
		
		$data = array(
			'pemusnahan' => $results
		);
		
		$this->load->view('pemusnahan_detail_view',$data);
	}

	public function pemusnahan_material_search_id_type() {
		$data		= file_get_contents("php://input");
		$params		= json_decode($data,true);
	
		$var = array (
			'id_type_material' => $params['material']
		);
		
		$list = $this->pemusnahan_model->pemusnahan_stok_list_by_type($var);
		
		if(empty($list)){
			$data = array();
			$res = array(
				'success'	=> false,
				'message'	=> 'Data material tidak ditemukan',
				'data'		=> $data
			);
		}else{
			$data = array();
			$tempObj = new stdClass();

			if(sizeof($params['tempPemusnahan']) > 0) {
				foreach ($params['tempPemusnahan'] as $p => $pk) {
					for ($i=0; $i <= sizeof($list); $i++) {
						if(!empty($list[$i])) {
							if($list[$i]['stock_code'] == $pk[1] && $list[$i]['stock_code'] == $pk[2]) {
								unset($list[$i]);
							}
						}
					}
				}
			}

			$i = 0;
			foreach ($list as $k => $v) {
				
				$strQty =
				'<input type="number" class="form-control" min="0" id="jml_qty'.$i.'" name="jml_qty'.$i.'" onChange="redrawTable('.$i.')" style="height:25px; width: 100px;" value="0">';
				
				$strKeterangan =
				'<textarea type="text" class="form-control" id="keterangan'.$i.'" name="keterangan'.$i.'" style="height:25px; width: 100px;" placeholder="Keterangan"></textarea>';
			
				$strOption =
					'<div class="checkbox">'.
						'<input id="option['.$i.']" type="checkbox" value="'.$i.'">'.
						'<label for="option['.$i.']"></label>'.
					'</div>';
				
				array_push($data, array(
					$v['id_material'],
					$v['stock_code'],
					$v['stock_name'],
					$v['type_material_name'],
					$strQty,
					$v['qty'],
					$strKeterangan,
					$strOption
				));
				$i++;
			}
			
			$res = array(
				'success'	=> true,
				'message'	=> 'Data material ditemukan',
				'data'		=> $data
			);
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}
	
	public function pemusnahan_list_detail($id_pemusnahan) {
		$list = $this->pemusnahan_model->pemusnahan_list_material_id($id_pemusnahan);
		
		$data = array();
		$i = 0;
		$no = 0;
		
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list as $k => $v) {

			$actions = '<div class="btn-group">';
            $actions .= '   <button class="btn btn-icon waves-effect waves-light btn-primary" type="button" data-toggle="tooltip" data-placement="top" title="Hapus" onClick="pemusnahan_detail_delete(\'' . $v['id_pemusnahan'] . '\')">';
            $actions .= '       <i class="fa fa-trash"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';

			array_push($data, array(
				$no = $k+1,
				$v['id_material'],
				$v['stock_code'],
				$v['stock_name'],
				$v['type_material_name'],
				$v['jml_qty'],
				$v['keterangan_detail'],
				//$actions
			));
			$i++;
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function pemusnahan_edit_list() {
		$data		= file_get_contents("php://input");
		$params		= json_decode($data,true);
	
		$var = array (
			'id_type_material' => $params['material']
		);
		
		$list = $this->pemusnahan_model->pemusnahan_stok_list_by_type($var);

		$data = array();
		$tempObj = new stdClass();
		
		if(sizeof($params['tempPemusnahan']) > 0) {
			foreach ($params['tempPemusnahan'] as $p => $pk) {
				for ($i=0; $i <= sizeof($list); $i++) {
					if(!empty($list[$i])) {
						if($list[$i]['stock_code'] == $pk[1] && $list[$i]['stock_code'] == $pk[2]) {
							unset($list[$i]);
						}
					}
				}
			}
		}
		
		$i = 0;
			
		foreach ($list as $k => $v) {
				
			$strQty =
			'<input type="number" class="form-control" min="0" id="jml_qty'.$i.'" name="jml_qty'.$i.'" style="height:25px; width: 100px;" value="0">';
			
			$strKeterangan =
			'<textarea type="text" class="form-control" id="keterangan'.$i.'" name="keterangan'.$i.'" style="height:25px; width: 150px;" placeholder="Keterangan"></textarea>';

			$strOption =
				'<div class="checkbox">'.
					'<input id="option['.$i.']" type="checkbox" value="'.$i.'">'.
					'<label for="option['.$i.']"></label>'.
				'</div>';
			
			array_push($data, array(
				$v['id_material'],
				$v['stock_code'],
				$v['stock_name'],
				$v['type_material_name'],
				$strQty,
				$v['jml_qty'],
				$strKeterangan,
				$strOption
			));
			$i++;
		}
	
		$results = array(
			'success'	=> true,
			'message'	=> 'Data material ditemukan',
			'data'		=> $data
		);
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($results);
	}
	
	public function pemusnahan_edit_list_detail($id_pemusnahan) {
		$list = $this->pemusnahan_model->pemusnahan_list_material_id($id_pemusnahan);
		
		$data = array();
		$i = 0;
		$tempTotal = 0;
		$username = $this->session->userdata['logged_in']['username'];

		foreach ($list as $k => $v) {
			
			$strQty =
			'<input type="number" class="form-control" min="0" id="edit_jml_qty'.$i.'" name="edit_jml_qty'.$i.'" style="height:25px; width: 100px;" onChange="redrawTable('.$i.')" value="'.$v['jml_qty'].'">';
			
			$strKeterangan =
			'<textarea type="text" class="form-control" id="edit_keterangan'.$i.'" name="edit_keterangan'.$i.'" style="height:25px; width: 150px;" onChange="redrawTable('.$i.')" placeholder="Keterangan">'.$v['keterangan_detail'].'</textarea>';
		
			$strOption = '<button class="btn btn-icon waves-effect waves-light btn-danger">';
			$strOption .=	'<i class="fa fa-trash"></i>';
			$strOption .='</button>';
			
			array_push($data, array(
				$v['id_material'],
				$v['stock_code'],
				$v['stock_name'],
				$v['type_material_name'],
				$strQty,
				$v['jml_qty'],
				$v['qty'],
				$strKeterangan,
				$v['keterangan_detail'],
				$strOption
			));
			$i++;
		}
		
		$results["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}
	
	public function insert_pemusnahan() {
		$no_pemusnahan			= $this->sequence->get_no('pemusnahan');
		$tanggal_pengajuan		= $this->Anti_sql_injection($this->input->post('tanggal_pengajuan', TRUE));
		$tanggal_pemusnahan		= $this->Anti_sql_injection($this->input->post('tanggal_pemusnahan', TRUE));
		$keterangan				= $this->Anti_sql_injection($this->input->post('keterangan', TRUE));
		$tanggal_input			= date('Y-m-d');
		$tmpTgl					= explode('-',$tanggal_input);
		$upload_error 			= NULL;
		$file_pemusnahan		= NULL;
		
		if ($_FILES['file_pemusnahan']['name']) {
			$this->load->library('upload');
			$new_filename =
				preg_replace('/\s/', '', $no_pemusnahan) .' '.
				$no_pemusnahan .' '.
				date('d-m-Y', strtotime($tmpTgl[2].'-'.$tmpTgl[1].'-'.$tmpTgl[0])).
				'.'.pathinfo($_FILES['file_pemusnahan']['name'], PATHINFO_EXTENSION);

			$config = array(
				'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/pemusnahan",
				'upload_url' => base_url() . "uploads/pemusnahan",
				'encrypt_name' => FALSE,
				'max_filename' => 100,
				'file_name' => $new_filename,
				'overwrite' => FALSE,
				'allowed_types' => 'pdf|txt|doc|docx',
				'max_size' => '10000'
			);
			$this->upload->initialize($config);

			if ($this->upload->do_upload("file_pemusnahan")) {
				// General result data
				$results = $this->upload->data();

				// Add our stuff
				$file_pemusnahan = 'uploads/pemusnahan/'.$results['file_name'];
			}else {
				$pesan = $this->upload->display_errors();
				$upload_error = strip_tags(str_replace("\n", '', $pesan));
				
				$msg = $upload_error;
				
				$result = array(
					'success' => false,
					'message' => $msg
				);
				
				$this->log_activity->insert_activity('insert', $msg. ' dengan No Pemusnahan ' .$no_pemusnahan);
			}
		}
		
		if (!isset($upload_error)) {
			$data = array(
				'no_pemusnahan'			=> $no_pemusnahan,
				'tanggal_pengajuan'		=> $tanggal_pengajuan,
				'tanggal_pemusnahan'	=> $tanggal_pemusnahan,
				'file_pemusnahan'		=> $file_pemusnahan,
				'keterangan'			=> $keterangan
			);
			
			$results = $this->pemusnahan_model->insert_pemusnahan($data);
			
			if ($results > 0) {
				$msg = 'Berhasil menambahkan data pemusnahan';

				$results = array(
					'success' => true,
					'message' => $msg,
					'lastid' => $results['lastid']
				);
				
				$this->log_activity->insert_activity('insert', $msg. ' dengan No Pemusnahan ' .$no_pemusnahan);
			}else {
				$msg = 'Gagal menambahkan data pemusnahan barang ke database';

				$results = array(
					'success' => false,
					'message' => $msg
				);
				
				$this->log_activity->insert_activity('insert', $msg. ' dengan No Pemusnahan ' .$no_pemusnahan);
			}
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}
	
	public function insert_pemusnahan_detail() {
		$data   	= file_get_contents("php://input");
        $params     = json_decode($data,true);
		
		foreach ($params['listpemusnahan'] as $k => $v) {
			$arrTemp = array(
				'id_pemusnahan'	=> $params['id_pemusnahan'],
				'id_material'	=> $v[0],
				'jml_qty'		=> $v[4],
				'keterangan'	=> $v[6],
			);
			
			$results = $this->pemusnahan_model->insert_pemusnahan_detail($arrTemp);
			$results_qty = $this->pemusnahan_model->update_pemusnahan_qty($arrTemp);
		}
		
		if ($results > 0) {
			$msg = 'Berhasil menambahkan data detail pemusnahan barang';

			$results = array(
				'success' => true,
				'message' => $msg
			);
			
			$this->log_activity->insert_activity('insert', $msg. ' dengan ID Pemusnahan ' .$params['id_pemusnahan']);
		}else {
			$msg = 'Gagal menambahkan data detail pemusnahan barang ke database';

			$results = array(
				'success' => false,
				'message' => $msg
			);
			
			$this->log_activity->insert_activity('insert', $msg. ' dengan ID Pemusnahan ' .$params['id_pemusnahan']);
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($results);
	}
	
	public function update_pemusnahan() {
		$this->form_validation->set_rules('no_pemusnahan', 'No Pemusnahan', 'trim|required|min_length[4]|max_length[100]');
		$this->form_validation->set_rules('tanggal_pengajuan', 'Tanggal Pengajuan', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('tanggal_pemusnahan', 'Tanggal Pemusnahan', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('keterangan', 'Keterangan', 'trim|required|max_length[100]');
		
		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			$id_pemusnahan			= ucwords($this->Anti_sql_injection($this->input->post('id_pemusnahan', TRUE)));
			$no_pemusnahan			= ucwords($this->Anti_sql_injection($this->input->post('no_pemusnahan', TRUE)));
			$tanggal_pengajuan		= $this->Anti_sql_injection($this->input->post('tanggal_pengajuan', TRUE));
			$tanggal_pemusnahan		= $this->Anti_sql_injection($this->input->post('tanggal_pemusnahan', TRUE));
			$keterangan				= $this->Anti_sql_injection($this->input->post('keterangan', TRUE));
			$old_file				= $this->Anti_sql_injection($this->input->post('old_file', TRUE));

			$tglInput				= date('Y-m-d');
			$tmpTgl					= explode('-',$tglInput);
			$upload_error 			= NULL;
			$file_pemusnahan		= NULL;
			
			if ($_FILES['file_pemusnahan']['name']) {
				$this->load->library('upload');
				$new_filename =
					preg_replace('/\s/', '', $tglInput) .' '.
					$no_pemusnahan .' '.
					date('d-m-Y', strtotime($tmpTgl[2].'-'.$tmpTgl[1].'-'.$tmpTgl[0])).
					'.'.pathinfo($_FILES['file_pemusnahan']['name'], PATHINFO_EXTENSION);

				$config = array(
					'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/pemusnahan",
					'upload_url' => base_url() . "uploads/pemusnahan",
					'encrypt_name' => FALSE,
					'max_filename' => 100,
					'file_name' => $new_filename,
					'overwrite' => FALSE,
					'allowed_types' => 'pdf|txt|doc|docx',
					'max_size' => '10000'
				);
				$this->upload->initialize($config);

				if ($this->upload->do_upload("file_pemusnahan")) {
					// General result data
					$result = $this->upload->data();

					// Add our stuff
					$pemusnahan = 'uploads/pemusnahan/'.$result['file_name'];
				} else {
					$pesan = $this->upload->display_errors();
					$upload_error = strip_tags(str_replace("\n", '', $pesan));

					$msg = $upload_error;
					
					$result = array(
						'success' => false,
						'message' => $msg
					);
					
					$this->log_activity->insert_activity('update', $msg. ' dengan No Pemusnahan ' .$no_pemusnahan);
				}
			}
			
			if (!isset($upload_error)) {
				if($file_pemusnahan == '' || $file_pemusnahan == NULL) {
					$file_pemusnahan = $old_file;
				} else {
					$filepath = dirname($_SERVER["SCRIPT_FILENAME"]) . '' .$old_file;
					if (is_file($filepath)) {
						unlink($filepath);
					}
				}
				
				$data = array(
					'id_pemusnahan'			=> $id_pemusnahan,
					'no_pemusnahan'			=> $no_pemusnahan,
					'tanggal_pengajuan'		=> $tanggal_pengajuan,
					'tanggal_pemusnahan'	=> $tanggal_pemusnahan,
					'file_pemusnahan'		=> $file_pemusnahan,
					'keterangan'			=> $keterangan
				);
				
				$results = $this->pemusnahan_model->update_pemusnahan($data);
				
				if ($results > 0) {
					$msg = 'Berhasil mengubah data pemusnahan';

					$results = array(
						'success' => true,
						'message' => $msg,
						'id_pemusnahan' => $id_pemusnahan,
					);
					
					$this->log_activity->insert_activity('update', $msg. ' dengan No Pemusnahan ' .$no_pemusnahan);
					
				}else {
					$msg = 'Gagal mengubah data pemusnahan barang ke database';

					$results = array(
						'success' => false,
						'message' => $msg
					);
					
					$this->log_activity->insert_activity('update', $msg. ' dengan No Pemusnahan ' .$no_pemusnahan);
				}
			}
			
			$this->output->set_content_type('application/json')->set_output(json_encode($results));
		}
	}
	
	public function update_pemusnahan_detail() {
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		
		$this->pemusnahan_model->pemusnahan_delete1($params);
		foreach ($params['listpemusnahan'] as $k => $v) {
			if($v[0] != '') {
				
				$arrTemp = array(
					'id_pemusnahan'	=> $params['id_pemusnahan'],
					'id_material'	=> $v[0],
					'jml_qty'		=> $v[5],
					'keterangan'	=> $v[7]
				);
				
				$results_qty = $this->pemusnahan_model->update_pemusnahan_qty($arrTemp);
				$results = $this->pemusnahan_model->insert_pemusnahan_detail($arrTemp);
				
				if ($results > 0) $statSave = true;
				else $statSave = false;
			}
		}

		if ($statSave == true) {
			$msg = 'Berhasil merubah data barang pemusnahan';

			$result = array(
				'success'	=> true,
				'message'	=> $msg
			);
			
			$this->log_activity->insert_activity('update', $msg. ' dengan ID Pemusnahan ' .$params['id_pemusnahan']);
			
		}else {
			$msg = 'Gagal merubah data barang pemusnahan ke database';
			
			$result = array(
				'success' => false,
				'message' => $msg
			);
			
			$this->log_activity->insert_activity('update', $msg. ' dengan ID Pemusnahan ' .$params['id_pemusnahan']);
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($result);
	}
	
	public function pemusnahan_delete() {
		
		$data 	= file_get_contents("php://input");
		$params = json_decode($data,true);
		
		$get_list = $this->pemusnahan_model->pemusnahan_detail_id($params['id_pemusnahan']);	
		if($get_list[0]['id_pemusnahan'] != $get_list[0]['id_pemusnahan_detail']){
			$list = $this->pemusnahan_model->pemusnahan_delete($params);
		}else{
			$list = $this->pemusnahan_model->pemusnahan_delete2($params);
		}
		
		$msg = 'Data detail pemusnahan berhasil di hapus';
		
		$results = array(
			'success' => true,
			'status' => 'success',
			'message' => $msg
		);
		
		$this->log_activity->insert_activity('delete', $msg. ' dengan ID Pemusnahan ' .$params['id_pemusnahan']);
		
		header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
        echo json_encode($results);
    }
	
	public function check_no_pemusnahan(){
		$this->form_validation->set_rules('no_pemusnahan', 'NoDo', 'trim|required|min_length[4]|max_length[20]|is_unique[t_pemusnahan.no_pemusnahan]');
		$this->form_validation->set_message('is_unique', 'No Pemusnahan Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'No Pemusnahan Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}
}