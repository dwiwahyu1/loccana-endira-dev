	<div class="row">
		<div class="col-md-12">
			<div class="col-md-9 pull-left">
				<div class="row">
					<label class="col-md-4">Nomor Pemusnahan </label>
					<label class="col-md-1 pull-left">:</label>
					<label class="col-md-7 pull-left"><?php if(isset($pemusnahan[0]['no_pemusnahan'])){ echo $pemusnahan[0]['no_pemusnahan']; }?></label>
				</div>
				
				<div class="row">
					<label class="col-md-4">Tanggal Input </label>
					<label class="col-md-1 pull-left">:</label>
					<label class="col-md-7 pull-left"><?php if(isset($pemusnahan[0]['tanggal_input'])){ 
						$date_input = date_create($pemusnahan[0]['tanggal_input']);
						echo date_format($date_input,"d F Y"); }?>
					</label>
				</div>
				
				<div class="row">
					<label class="col-md-4">Tanggal Pengajuan </label>
					<label class="col-md-1 pull-left">:</label>
					<label class="col-md-7 pull-left"><?php if(isset($pemusnahan[0]['tanggal_pengajuan'])){ 
						$date_pengajuan = date_create($pemusnahan[0]['tanggal_pengajuan']);
						echo date_format($date_pengajuan,"d F Y"); }?>
					</label>
				</div>
				
				<div class="row">
					<label class="col-md-4">Tanggal Pemusnahan </label>
					<label class="col-md-1 pull-left">:</label>
					<label class="col-md-7 pull-left"><?php if(isset($pemusnahan[0]['tanggal_pemusnahan'])){ 
						$date_pemusnahan = date_create($pemusnahan[0]['tanggal_pemusnahan']);
						echo date_format($date_pemusnahan,"d F Y"); }?>
					</label>
				</div>
				
				<div class="row">
					<label class="col-md-4">File </label>
					<label class="col-md-1 pull-left">:</label>
					<label class="col-md-7 pull-left"><?php if(isset($pemusnahan[0]['file'])){ echo $pemusnahan[0]['file']; }?></label>
				</div>
				
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-12">
				<table id="listdetailpemusnahan" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>Material</th>
							<th>Kode Material</th>
							<th>Nama Material</th>
							<th>Jenis Material</th>
							<th>Qty</th>
							<th>Keterangan</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	
<script type="text/javascript">
	$(document).ready(function(){
		dt_pemusnahan();
	});

	function dt_pemusnahan() {
		$('#listdetailpemusnahan').DataTable( {
			"processing": true,
			"searching": false,
			"paging": false,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"ajax": {
				"type" : "GET",
				"url" : "<?php echo base_url().'pemusnahan/pemusnahan_list_detail/'.$pemusnahan[0]['id_pemusnahan'];?>"
			},
			"columnDefs": [{
				"targets": [5],
				"className": 'dt-body-right',
			}]
		});
	}
	
	function pemusnahan_detail_delete(id_pemusnahan){
		swal({
			title: 'Yakin akan Menghapus ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost={
				"id_pemusnahan"  :   id_pemusnahan
			};
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('pemusnahan/pemusnahan_detail_delete');?>",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
					
				   swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						listdelivery();
					})
					if (response.status == "success") {
					} else{
						swal("Failed!", response.message, "error");
					}
				}
			});
		})
	}

</script>