	<style>
		#loading-us{display:none}
		#tick{display:none}

		#loading-mail{display:none}
		#cross{display:none}
		.add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
		.add_item:hover{color:#ff8c00}
		.right-text{text-align:right}
		.form-item{margin-top: 15px;overflow: auto;}
	</style>

	
  	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<br>
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_material">Type Material <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" name="type_material" id="type_material" style="width: 100%" onchange="getPemusnahan();" required autocomplete="off">
				<option value="">Select Type Material</option>
				<?php foreach($pemusnahan as $pem => $p) { ?>
					<option value="<?php echo $p['id_type_material']; ?>"><?php echo $p['type_material_name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>
	
	<div class="item form-group form-item">
		<table id="listmaterial" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th></th>
					<th>Kode Material</th>
					<th>Nama Material</th>
					<th>Jenis Material</th>
					<th>Jumlah Qty</th>
					<th>Sisa Qty</th>
					<th>Keterangan</th>
					<th>Pilih</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>

	<hr>
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="btn_submit_simpan"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn_submit_simpan" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Simpan</button>
		</div>
	</div>
	
	<script type="text/javascript">
	var table_pemusnahan_material;
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
		
		resetDt();
		$("#type_material").select2();
	});
	
	function resetDt() {
		table_pemusnahan_material = $('#listmaterial').DataTable( {
			"processing": true,
			"searching": false,
			"paging": false,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0],
				"visible": false,
				"searchable": false
			}]
		});
	}

	function getPemusnahan() {
		var value_no = $("#type_material").val();
		if(value_no != '') {
			sendNo(value_no);
		}else {
			table_pemusnahan_material.clear();
			table_pemusnahan_material.destroy();
			resetDt();
		}
	}
	
	function sendNo(id_type_material) {
		var tempPemusnahan = [];
		if(t_editPemusnahan.rows().data().length > 0) {
			for (var i = 0; i < t_editPemusnahan.rows().data().length; i++) {
				tempPemusnahan.push(t_editPemusnahan.rows().data()[i]);
			}
		}
		var datapost = {
			"material" : id_type_material,
			"tempPemusnahan" : tempPemusnahan
		};
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>pemusnahan/pemusnahan_edit_list",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.success == true) {
					table_pemusnahan_material.clear();
					table_pemusnahan_material.destroy();
					table_pemusnahan_material = $('#listmaterial').DataTable( {
						"processing": true,
						"searching": false,
						"responsive": true,
						"lengthChange": false,
						"info": false,
						"bSort": false,
						"data": r.data,
						"columnDefs": [{
							"targets": [0],
							"visible": false,
							"searchable": false
						}]
					});
				}else{
					table_pemusnahan_material.clear();
					table_pemusnahan_material.destroy();
					table_pemusnahan_material = $('#listmaterial').DataTable( {
						"processing": true,
						"searching": false,
						"responsive": true,
						"lengthChange": false,
						"info": false,
						"bSort": false,
						"data": r.data,
						"columnDefs": [{
							"targets": [0],
							"visible": false,
							"searchable": false
						}]
					});
					swal("Perhatian!", r.message, "info");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Simpan");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}
	
	$('#btn_submit_simpan').click(function() {
		var tempArr = [];
		var statusBtn = 0;
		
		$('input[type=checkbox]').each(function () {
			if(this.checked == true) {
				var row = parseInt($(this).val());
				var rowData = table_pemusnahan_material.row(row).data();
				var stockCode = rowData[1];
				var jumlahQty = $('#jml_qty' + row).val();
				var keterangan = $('#keterangan' + row).val();
				
				var strJumlahQty =
					'<input type="number" class="form-control" min="0" style="height:25px; width: 100px;" value="'+jumlahQty+'">';
					
				var strKeterangan =
					'<textarea type="text" class="form-control" style="height:25px; width: 150px;" placeholder="keterangan">'+keterangan+'</textarea>';
					
				var option =
					'<button class="btn btn-icon waves-effect waves-light btn-danger">'+
						'<i class="fa fa-trash"></i>'+
					'</button>';
					
				if(jumlahQty > rowData[5]) {
					swal('Warning', 'Data jumlah Qty tidak boleh lebih besar dari sisa qty');
					statusBtn = 0;
				}else if(jumlahQty <= 0) {
					swal('Warning', 'Data jumlah Qty tidak boleh lebih kecil dari 0 atau tidak berjumlah 0');
					statusBtn = 0;
				}
				
				if(stockCode != '' && jumlahQty != 0) {
					var arrRow = [
						rowData[0], 
						rowData[1], 
						rowData[2], 
						rowData[3], 
						strJumlahQty, 
						jumlahQty, 
						rowData[5], 
						strKeterangan, 
						keterangan, 
						option
					];
					
					tempArr.push(arrRow);
					arrRow = [];
					statusBtn = 1;
				}else if(jumlahQty == 0) {
					swal('Warning', 'Data jumlah Qty baris ke-'+ (row+5) +' tidak boleh kosong');
					statusBtn = 0;
				}else {
					swal('Warning', 'Data jumlah Qty baris ke-'+ (row+5) +' harus diisi');
					statusBtn = 0;
				}
			}
		});
		
		if(statusBtn == 1) {
			for (var i = 0; i < tempArr.length; i++) {
				t_editPemusnahan.row.add(tempArr[i]).draw();
			}
			$('#panel-modal-detail').modal('toggle');
		}

		for (var i = 0; i < t_editPemusnahan.rows().data().length; i++) {
			var rowData = t_editPemusnahan.row(i).data();
		}
	})
	
	function isNumber(evt) {
		var iKeyCode = (evt.which) ? evt.which : evt.keyCode
		if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
			return false;

		return true;
	}
	</script>