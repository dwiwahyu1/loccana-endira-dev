<style>
	.dt-body-left {text-align:left;}
	.dt-body-right {text-align:right;}
	.dt-body-center {text-align:center; vertical-align: middle;}
	.force-overflow {height: 650px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow {min-height: 650px}
	#modal-pemusnahan::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	#modal-pemusnahan::-webkit-scrollbar
	{
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-pemusnahan::-webkit-scrollbar-thumb
	{
		background-image: -webkit-gradient(linear,
		   left bottom,
		   left top,
		   color-stop(0.44, rgb(122,153,217)),
		   color-stop(0.72, rgb(73,125,189)),
		   color-stop(0.86, rgb(28,58,148)));
	}
</style>

<div class="container" style="height: auto;">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Pemusnahan</h4>
		</div>
	</div>

	<div class="row">
        <div class="col-sm-12">
            <div class="card-box">
				<table id="listpemusnahan" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
                        <tr>
							<th style="width: 5%;">No</th>
							<th>Nomor Pemusnahan</th>
							<th>Tanggal Input</th>
							<th>Tanggal Pengajuan</th>
							<th>Tanggal Pemusnahan</th>
							<th>File</th>
							<th>Keterangan</th>
							<th style="width: 15%;">Option</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
            </div>
        </div><!-- end col -->
    </div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-pemusnahan">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal-detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body force-overflow" id="modal-pemusnahan">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
function pemusnahan_add(){
	$('#panel-modal').removeData('bs.modal');
	$('#panel-modal .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal .panel-body').load('<?php echo base_url('pemusnahan/pemusnahan_add');?>');
	$('#panel-modal .panel-title').html('<i class="fa fa-plus"></i> Tambah Data Pemusnahan');
	$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}

function pemusnahan_detail(id){
	$('#panel-modal').removeData('bs.modal');
	$('#panel-modal .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal .panel-body').load('<?php echo base_url('pemusnahan/pemusnahan_detail');?>'+'/'+id);
	$('#panel-modal .panel-title').html('<i class="fa fa-search"></i> Detail Data Pemusnahan');
	$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}

function pemusnahan_edit(id){
	$('#panel-modal').removeData('bs.modal');
	$('#panel-modal .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal .panel-body').load('<?php echo base_url('pemusnahan/pemusnahan_edit');?>'+'/'+id);
	$('#panel-modal .panel-title').html('<i class="fa fa-pencil"></i> Edit Data Pemusnahan');
	$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

function listpemusnahan(){
	$("#listpemusnahan").dataTable({
		"processing": true,
		"serverSide": true,
		"ajax": "<?php echo base_url().'pemusnahan/lists';?>",
		"searchDelay": 700,
		"responsive": true,
		"lengthChange": false,
		"destroy": true,
		"info": false,
		"bSort": false,
		"dom": 'l<"toolbar">frtip',
		"initComplete": function(){
			var element = '<div class="btn-group pull-left">';
				element += '  	<a class="btn btn-primary" onClick="pemusnahan_add()">';
				element += '    	<i class="fa fa-plus"></i> Tambah Data Pemusnahan';
				element += '  	</a>';
				element += '</div>';
			$("div.toolbar").prepend(element);
		},
		"columnDefs": [{
			"targets": [0],
			"searchable": false,
			"className": 'dt-body-center',
			"width": 10
		},{
			"targets": [1],
			"searchable": false,
			"className": 'dt-body-left',
			"width": 180
		},{
			"targets": [2],
			"searchable": false,
			"className": 'dt-body-left',
			"width": 150,
		},{
			"targets": [3],
			"searchable": false,
			"className": 'dt-body-left',
			"width": 150
		},{
			"targets": [4],
			"searchable": false,
			"className": 'dt-body-left',
			"width": 150
		},{
			"targets": [5],
			"searchable": false,
			"className": 'dt-body-center',
			"width": 30
		},{
			"targets": [6],
			"searchable": false,
			"className": 'dt-body-left',
			"width": 250
		},{
			"targets": [7],
			"searchable": false,
			"className": 'dt-body-center',
			"width": 115
		}]
    });
}
  
function pemusnahan_delete(id){
	swal({
		title: 'Yakin akan Menghapus ?',
		text: 'data tidak dapat dikembalikan bila sudah dihapus !',
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Ya',
		cancelButtonText: 'Tidak'
    }).then(function () {
		var datapost={
			"id_pemusnahan" : id
		};

		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>pemusnahan/pemusnahan_delete",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				if(response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						$('.panel-heading button').trigger('click');
						listpemusnahan();
					})
				}else{
					swal("Failed!", response.message, "error");
				}
			}
		});
    })
}

$(document).ready(function(){
	listpemusnahan();
});
</script>