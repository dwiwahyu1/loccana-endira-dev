	<style>
		#loading-us{display:none}
		#tick{display:none}

		#loading-mail{display:none}
		#cross{display:none}
		.add_item{cursor:pointer;text-decoration: underline;color:#96b6e8;padding-top: 6px;}
		.add_item:hover{color:#ff8c00}
		.right-text{text-align:right}
	</style>

	<form class="form-horizontal form-label-left" id="pemusnahan_edit" role="form" action="<?php echo base_url('pemusnahan/update_pemusnahan');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

  		<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
		<br>
        <div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_pemusnahan">No Pemusnahan <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input placeholder="No. Pemusnahan" type="text" class="form-control" id="no_pemusnahan" name="no_pemusnahan" required readonly autocomplete="off" value="<?php if(isset($pemusnahan[0]['no_pemusnahan'])) {echo $pemusnahan[0]['no_pemusnahan'];} ?>">
				<span id="loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking No Pemusnahan...</span>
				<span id="tick"></span>
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_pengajuan">Tanggal Pengajuan <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<div class="input-group date">
					<input type="text" class="form-control col-md-7 col-xs-12 datepicker" id="tanggal_pengajuan" name="tanggal_pengajuan" autocomplete="off" placeholder="<?php echo date('Y-m-d'); ?>" required value="<?php if(isset($pemusnahan[0]['tanggal_pengajuan'])) {echo $pemusnahan[0]['tanggal_pengajuan'];} ?>">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-th"></span>
					</div>
				</div>
			</div>
		</div>
		
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tanggal_pemusnahan">Tanggal Pemusnahan <span class="required"><sup>*</sup></span></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<div class="input-group date">
					<input type="text" class="form-control col-md-7 col-xs-12 datepicker" id="tanggal_pemusnahan" name="tanggal_pemusnahan" placeholder="<?php echo date('Y-m-d'); ?>" autocomplete="off" required value="<?php if(isset($pemusnahan[0]['tanggal_pemusnahan'])) {echo $pemusnahan[0]['tanggal_pemusnahan'];} ?>">
					<div class="input-group-addon">
						<span class="glyphicon glyphicon-th"></span>
					</div>
				</div>
			</div>
		</div>
		
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="file_pemusnahan">File </label>
			<div style="margin-left: 5px;">
				<?php if(isset($pemusnahan[0]['file']) && $pemusnahan[0]['file'] != '') {
					if($filetype == 'pdf') { ?>
					<a href="<?php echo base_url().$dir_image;?>" title="Download File" target="_blank" download>
						<!-- Gambar hanya contoh, path tergantung proyek masing2 -->
						<img src="<?php echo base_url() ."uploads/file-icon/pdf96px.png";?>" id="panel_file_temp"/>
					</a>
				<?php }elseif($filetype == 'docx' || $filetype == 'doc'){?>
					<a href="<?php echo base_url().$dir_image;?>" target="_blank" title="Download File" target="_blank" download>
						<!-- Gambar hanya contoh, path tergantung proyek masing2 -->
						<img src="<?php echo base_url() ."uploads/file-icon/word2019-96px.png";?>" id="panel_file_temp"/>
					</a>
				<?php }elseif($filetype == 'xlsx' || $filetype == 'xls') { ?>
					<a href="<?php echo base_url().$dir_image;?>" target="_blank" title="Download File" target="_blank" download>
						<!-- Gambar hanya contoh, path tergantung proyek masing2 -->
						<img src="<?php echo base_url() ."uploads/file-icon/excel2019-96px.png";?>" id="panel_file_temp"/>
					</a>
				<?php }else{ ?>
					<a href="<?php echo base_url().$dir_image;?>" target="_blank" title="Download File" target="_blank" download>
						<!-- Gambar hanya contoh, path tergantung proyek masing2 -->
						<img src="<?php echo base_url() ."uploads/file-icon/txt-160px.png";?>" id="panel_file_temp"/>
					</a>
				<?php } } ?>
				<label class="control-label text-center"><?php echo $fileupload; ?></label>
			</div>
		</div>
		
		<div class="item form-group">
			<div class="col-md-8 col-sm-6 col-xs-12" style="padding-top: 10px;margin-left: 220px;">
				<input type="file" class="form-control" id="file_pemusnahan" name="file_pemusnahan" data-height="110" autocomplete="off" accept=".pdf, .txt, .doc, .docx"/>
				<span id="file_pemusnahan_tick">Hanya format file pdf,txt,doc,docx dengan besaran max 10Mb yang diterima.</span>
				<input type="hidden" id="old_file" name="old_file" value="<?php if(isset($pemusnahan[0]['file'])){ echo $pemusnahan[0]['file']; }?>">
			</div>
		</div>
		
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="keterangan">Keterangan </label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<textarea type="text" class="form-control" id="keterangan" name="keterangan" placeholder="Keterangan" autocomplete="off"><?php if(isset($pemusnahan[0]['keterangan'])) {echo $pemusnahan[0]['keterangan'];} ?></textarea>
			</div>
		</div>
		
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Barang : </label>
			<div class="col-md-8 col-sm-6 col-xs-12 add_item">
				<a class="btn btn-primary" onclick="pemusnahan_edit_item()">
					<i class="fa fa-plus"></i> Tambah Barang Pemusnahan
				</a> 
				<input type="hidden" id="tambah_barang" name="tambah_barang" value="0">
			</div>
		</div>

	
		<div class="item form-group">
			<table id="listeditpemusnahan" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th></th>
						<th>Kode Stok</th>
						<th>Nama Material</th>
						<th>Jenis Material</th>
						<th>Jumlah Qty</th>
						<th>Jumlah Qty</th>
						<th>Sisa Qty</th>
						<th>Keterangan</th>
						<th>Keterangan</th>
						<th>Option</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	
		<hr>
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12"></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5">Simpan</button>
				<input type="hidden" class="form-control" id="id_pemusnahan" name="id_pemusnahan" required readonly value="<?php if(isset($pemusnahan[0]['id_pemusnahan'])) {echo $pemusnahan[0]['id_pemusnahan'];} ?>">
			</div>
		</div>
	</form>
	
	<script type="text/javascript">
	var t_editPemusnahan;
	var last_no_pemusnahan = $('#no_pemusnahan').val();
	
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
		
		$('#tanggal_pengajuan').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true,
			changeYear: true,
			minDate: '-3M',
			maxDate: '+30D',
		});
		
		$('#tanggal_pemusnahan').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true,
			changeYear: true,
			minDate: '-3M',
			maxDate: '+30D',
		});
		
		$('#file_pemusnahan').bind('change', function() {
			if(this.files[0].size >= 9437184) {
				$('#file_pemusnahan').css('border', '3px #C33 solid');
				$('#file_pemusnahan_tick').empty();
				$("#file_pemusnahan_tick").append('<span class="fa fa-close"> Ukuran File Lebih dari 9Mb</span>');
				$('#file_pemusnahan_tick').show();
			}else {
				$('#file_pemusnahan').removeAttr("style");
				$('#file_pemusnahan_tick').empty();
				$("#file_pemusnahan_tick").append('Hanya format file pdf,txt,doc,docx dengan besaran max 10Mb yang diterima.');
				$('#file_pemusnahan_tick').show();
			}
		});
		
		dtPemusnahan();
	});
	
	$('#no_pemusnahan').on('input',function(event) {
		if($('#no_pemusnahan').val() != last_no_pemusnahan) {
			no_pemusnahan_check();
		}
	});

	function no_pemusnahan_check() {
		var no_pemusnahan = $('#no_pemusnahan').val();
		if(no_pemusnahan.length > 3) {
			var post_data = {
				'no_pemusnahan': no_pemusnahan
			};

			$('#tick').empty();
			$('#tick').hide();
			$('#loading-us').show();
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url('pemusnahan/check_no_pemusnahan');?>",
				data: post_data,
				cache: false,
				success: function(response){
					if(response.success == true){
						$('#no_pemusnahan').css('border', '3px #090 solid');
						$('#loading-us').hide();
						$('#tick').empty();
						$("#tick").append('<span class="fa fa-check"> '+response.message+'</span>');
						$('#tick').show();
					}else {
						$('#no_pemusnahan').css('border', '3px #C33 solid');
						$('#loading-us').hide();
						$('#tick').empty();
						$("#tick").append('<span class="fa fa-close"> '+response.message+'</span>');
						$('#tick').show();
					}
				}
			});
		}else {
			$('#no_pemusnahan').css('border', '3px #C33 solid');
			$('#loading-us').hide();
			$('#tick').empty();
			$("#tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
			$('#tick').show();
		}
	}
  
	function dtPemusnahan() {
		t_editPemusnahan = $('#listeditpemusnahan').DataTable( {
			"processing": true,
			"searching": false,
			"paging": false,
			"responsive": true,
			"lengthChange": false,
			"ajax": {
				"type" : "GET",
				"url" : "<?php echo base_url().'pemusnahan/pemusnahan_edit_list_detail/'.$pemusnahan[0]['id_pemusnahan'];?>",
            },
			"info": false,
			"bSort": false,
			"columnDefs": [{
				"targets": [0],
				"visible": false,
				"searchable": false
			},{
				"targets": [5],
				"visible": false,
				"searchable": false
			},{
				"targets": [8],
				"visible": false,
				"searchable": false
			}]
		});
	}
	
	function redrawTable(row) {
		var rowData = t_editPemusnahan.row(row).data();
		rowData[5] = $('#edit_jml_qty'+row).val();
		
		if(rowData[5] > rowData[6]) {
			swal('Warning', 'Data jumlah Qty tidak boleh lebih besar dari sisa qty');
		}else if(rowData[5] <= 0) {
			swal('Warning', 'Data jumlah Qty tidak boleh lebih kecil dari 0 atau tidak berjumlah 0');
		}
		
		rowData[7] = $('#edit_keterangan'+row).val();
		t_editPemusnahan.draw();
	}
	
	function pemusnahan_edit_item(){
		$('#panel-modal-detail').removeData('bs.modal');
		$('#panel-modal-detail  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal-detail  .panel-body').load('<?php echo base_url('pemusnahan/pemusnahan_edit_item');?>');
		$('#panel-modal-detail  .panel-title').html('<i class="fa fa-plus"></i> Tambah Material Pemusnahan');
		$('#panel-modal-detail').modal({backdrop:'static',keyboard:false},'show');
	}

	$('#listeditpemusnahan').on("click", "button", function(){
		t_editPemusnahan.row($(this).parents('tr')).remove().draw(false);
		for (var i = 0; i < t_editPemusnahan.rows().data().length; i++) {
			var rowData = t_editPemusnahan.row(i).data();
		}
	});
	
	$('#pemusnahan_edit').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Mengubah data...");
		e.preventDefault();
		var formData = new FormData(this);
		
		if($('#file_pemusnahan')[0].files.length > 0) {
			if($('#file_pemusnahan')[0].files[0].size <= 9437184) {
				save_Form(formData, $(this).attr('action'));
			}else {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Update Data");
				swal("Failed!", "Ukuran File Terlalu besar, silahkan cek kembali", "error");
			}
		}else {
			save_Form(formData, $(this).attr('action'));
		}
	}));
	
	function save_Form(formData, url) {
		$.ajax({
			type:'POST',
			url: url,
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						save_pemusnahan(response.id_pemusnahan);
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Simpan");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submites').text("Simpan");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}
	
	function save_pemusnahan(id_pemusnahan) {
		var arrTemp = [];
		for (var i = 0; i < t_editPemusnahan.rows().data().length; i++) {
			var rowData = t_editPemusnahan.row(i).data();
			arrTemp.push(rowData); 
		}

		var datapost = {
			"id_pemusnahan"		: id_pemusnahan,
			'listpemusnahan'	: arrTemp
		};
		
		$.ajax({
			type: "POST",
			url: "<?php echo base_url();?>pemusnahan/update_pemusnahan_detail",
			data : JSON.stringify(datapost),
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						$('#panel-modal').modal('toggle');
						listpemusnahan();
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Simpan Ulang");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Simpan");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}
	
	function isNumber(evt) {
		var iKeyCode = (evt.which) ? evt.which : evt.keyCode
		if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
			return false;

		return true;
	}
	</script>