<!-- <script type="text/javascript" src="<?php echo base_url('assets'); ?>/gentelella-master/vendors/parsleyjs/dist/parsley.min.js"></script> -->

<style>
	#loading-us{display:none}
	#tick{display:none}

	#loading-mail{display:none}
	#cross{display:none}
</style>

<div class="row">
	<div class="col-md-12" style="margin-bottom: 10px;">
		<div class="row">
			<label class="col-sm-4" for="type_material">Type Material</label>
			<div class="col-md-8">
				<select class="form-control" id="type_material" name="type_material">
					<option value="">-- Select Type Material --</option>
				<?php foreach($typeMaterial as $tm) { ?>
					<option value="<?php echo $tm['id_type_material']; ?>"><?php echo $tm['type_material_name']; ?></option>
				<?php } ?>
				</select>
			</div>
		</div>
	</div>

	<div class="col-md-12" style="margin-bottom: 10px;">
		<div class="row">
			<label class="col-sm-4" for="select_material">Pilih Material</label>
			<div class="col-md-8">
				<select class="form-control" id="select_material" name="select_material" onchange="getMaterial()">
					<option value="">-- Select Material --</option>
				</select>
				<!-- <select class="form-control" id="select_material" name="select_material" onchange="getMaterial()">
					<option value="">-- Select Material --</option>
				<?php foreach($listMaterial as $km) { ?>
					<option value="<?php echo $km['id']; ?>"><?php echo $km['stock_name']; ?></option>
				<?php } ?>
				</select> -->
			</div>
		</div>
	</div>

	<div class="col-md-12">
		<div class="row">
			<label class="col-sm-4">Kode Bc:</label>
			<label class="col-md-8" id="lb_kode_bc"></label>
		</div>

		<div class="row">
			<label class="col-sm-4">Kode Stok:</label>
			<label class="col-md-8" id="lb_kode_stok"></label>
		</div>

		<div class="row">
			<label class="col-sm-4">Nama Stok:</label>
			<label class="col-md-8" id="lb_nama_stok"></label>
		</div>

		<div class="row">
			<label class="col-sm-4">Unit:</label>
			<label class="col-md-8" id="lb_unit"></label>
		</div>

		<div class="row">
			<label class="col-sm-4">Type:</label>
			<label class="col-md-8" id="lb_type"></label>
		</div>

		<div class="row">
			<label class="col-sm-4">Valas:</label>
			<label class="col-md-8" id="lb_valas"></label>
		</div>

		<div class="row">
			<label class="col-sm-4">Harga:</label>
			<label class="col-md-8" id="lb_harga"></label>
		</div>

		<div class="row">
			<label class="col-sm-4">Stok:</label>
			<label class="col-md-8" id="lb_stok"></label>
		</div>

		<div class="row">
			<label class="col-sm-4">Gudang:</label>
			<label class="col-md-8" id="lb_gudang"></label>
		</div>
	</div>
</div>

<div class="row" style="margin-top: 10px;">
	<div class="col-md-12">
		<form class="form-horizontal " id="mutasi_form" role="form" action="<?php echo base_url('mutasi_masuk/save_add_mutasi_new');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
			<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

			<div class="item form-group">
				<label class="control-label col-md-4" for="stock_pindah" style="text-align:left">Jumlah Stok Pindah <span class="required"><sup>*</sup></span></label>
				<div class="col-md-8">
					<input type="number" id="stock_pindah" name="stock_pindah" class="form-control " placeholder="Jumlah Stok Pindah" value="0" min="0" max="0" onInput="cal_stock()" required="required">
					<span id="sisa_stock"></span>
				</div>
			</div>

			<!-- <div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="gudang_pindah">Gudang <span class="required"><sup>*</sup></span></label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<select class="form-control" id="gudang_pindah" name="gudang_pindah" style="width: 100%" required>
						<option value="" >-- Select Gudang --</option>
					<?php foreach($gudang as $kg) { ?>
						<option value="<?php echo $kg['id_gudang']; ?>"><?php echo $kg['nama_gudang']; ?></option>
					<?php } ?>
					</select>
				</div>
			</div> -->

			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Mutasi Material</button>
					<input type="hidden" id="id_material" name="id_material" value="">
					<input type="hidden" id="stock_awal" name="stock_awal" value="">
					<input type="hidden" id="stock_akhir" name="stock_akhir" value="0">
					<input type="hidden" id="gudang_awal" name="gudang_awal" value="">
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	var selectMaterial;
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
		$('#select_material').select2();
		$('#type_material').on('change',function(event) {
			var id_type = $(this).val();
			if(id_type != '') {
				var _this = $(this);
				$('select#select_material').empty(); 
				var strHtml = '';

				$.ajax({
					type: "POST",
					url: '<?php echo base_url();?>mutasi_masuk/get_list_material',
					data : JSON.stringify({'id_type': id_type}),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(r) {
						strHtml += '<option value="">-- Select Material --</option>';
						for(var i=0; i < r.data.length; i++) {
							strHtml += "<option value='"+r.data[i].id+"'>" +r.data[i].stock_code + " | " + r.data[i].stock_name +"</option>"
						}
						$('select#select_material').append(strHtml);
						$('select#select_material').select2();
					},
					fail: function(){
							strHtml = '<option value="">Please try again</option>';
							$('select#select_material').append(strHtml);
					}
				});
			};
		});

		$('#file_mutasi').bind('change', function() {
			if(this.files[0].size >= 9437184) {
				$('#file_mutasi').css('border', '3px #C33 solid');
				$('#file_mutasi_tick').empty();
				$("#file_mutasi_tick").append('<span class="fa fa-close"> Ukuran File Lebih dari 9Mb</span>');
				$('#file_mutasi_tick').show();
			}else {
				$('#file_mutasi').removeAttr("style");
				$('#file_mutasi_tick').empty();
				$("#file_mutasi_tick").append('Hanya format file pdf,txt,doc,docx dengan besaran max 10Mb yang diterima.');
				$('#file_mutasi_tick').show();
			}
		});

		$('#btn_add_bc').on('click', function () {
			$('#panel-modal-child').removeData('bs.modal');
			$('#panel-modal-child  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
			$('#panel-modal-child  .panel-body').load('<?php echo base_url('mutasi_masuk/add_bc');?>');
			$('#panel-modal-child  .panel-title').html('<i class="fa fa-dollar"></i> Tambah BC');
			$('#panel-modal-child').modal({backdrop:'static',keyboard:false},'show');
		});
	});

	function getMaterial() {
		var id_select = $('#select_material').val();
		if(id_select != '') {
			var datapost = {
				"id_select" : id_select
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>mutasi_masuk/get_material",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(r) {
					setValue(r.data);
				}
			});
		}else resetValue();
	}

	function cal_stock() {
		var stockAwal 	= $('#stock_awal').val();
		var stockPindah = $('#stock_pindah').val();

		var calStock = stockAwal - stockPindah;
		$('#sisa_stock').html('Sisa Stock : ' + calStock);
		$('#stock_akhir').val(calStock);
	}

	function checkStock() {
		var stockAwal 	= parseInt($('#stock_awal').val());
		var stockPindah = parseInt($('#stock_pindah').val());
		
		console.log(stockPindah,stockAwal);
		if(stockPindah > 0) {
			if(stockPindah > stockAwal) return false;
			else return true;
		}else return false;
	}


	$('#mutasi_form').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		var validateStock	= checkStock();

		if(validateStock == true ) {
			var formData = new FormData(this);
			save_Form(formData, $(this).attr('action'));
		}else if(validateStock == false) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Mutasi Material");
			swal("Failed!", "Invalid masukan stock salah, silahkan cek kembali.", "error");
		}
	}));

	function save_Form(formData, url) {
		$.ajax({
			type:'POST',
			url: url,
			data:formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function() {
						window.location.href = "<?php echo base_url('mutasi_masuk');?>";
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Mutasi Material");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Mutasi Material");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}

	function setValue(data) {
		$("select#bc_pindah option").filter(function() {
			return $(this).text() == data.no_bc;
		}).prop('selected', true);

		$('#lb_kode_bc').html(data.no_bc);
		$('#lb_kode_stok').html(data.stock_code);
		$('#lb_nama_stok').html(data.stock_name);
		$('#lb_unit').html(data.uom_name);
		$('#lb_type').html(data.type_material_name);
		$('#lb_valas').html(data.nama_valas);
		$('#lb_harga').html(data.price);
		$('#lb_stok').html(parseFloat(data.qty).toFixed(4).replace('.', ','));
		$('#lb_gudang').html(data.nama_gudang);

		$('#id_material').val(data.id);
		$('#stock_awal').val(data.qty);
		$('#gudang_awal').val(data.id_gudang);
		$('#stock_pindah').attr({
			"max" : data.qty
		});
		$('#gudang_pindah').val(data.id_gudang);
	}

	function resetValue() {
		$('#lb_kode_bc').html('');
		$('#lb_kode_stok').html('');
		$('#lb_nama_stok').html('');
		$('#lb_unit').html('');
		$('#lb_type').html('');
		$('#lb_valas').html('');
		$('#lb_harga').html('');
		$('#lb_stok').html('');
		$('#lb_gudang').html('');

		$('#id_material').val('');
		$('#stock_awal').val('');
		$('#stock_akhir').val('');
		$('#gudang_awal').val('');
		$('#stock_pindah').attr({
			"max" : 0
		});
		$('#sisa_stock').html('');
	}
</script>