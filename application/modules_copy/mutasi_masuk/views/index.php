<style type="text/css">
	.dt-body-right {
		text-align: right;
	}
	.dt-body-center {
		text-align: center;
	}
</style>

 <div class="container">
 	<div class="row">
 		<div class="col-sm-12">
 			<h4 class="page-title">Mutasi Produksi</h4>
 		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<table id="listmutasi" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="width: 5px;">No</th>
							<th>Kode BC</th>
							<th>Kode Stok</th>
							<th>Nama Stok</th>
							<th>Unit</th>
							<th>Tipe</th>
							<th>Stok</th>
							<th>Gudang</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>


<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width: 610px;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal-child" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width: 610px;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$(document).ready(function() {
		$("#listmutasi").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'mutasi_masuk/lists/';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false, 
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip', 
			"initComplete": function(){
				$("div.toolbar").prepend('<div class="btn-group pull-left"><a class="btn btn-primary" onClick="addMutasi()"><i class="fa fa-plus"></i> Tambah Mutasi</a></div>');
			},
			"columnDefs": [{
				targets: [0],
				className: 'dt-body-center'
			},{
				targets: [6],
				className: 'dt-body-right'
			}]
		});
	});

	function addMutasi(){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('mutasi_masuk/add');?>');
		$('#panel-modal  .panel-title').html('<i class="fa fa-plus"></i> Tambah Mutasi Produksi');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function editMutasi(id) {
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal .panel-body').load('<?php echo base_url('mutasi_masuk/edit/');?>'+"/"+id);
		$('#panel-modal .panel-title').html('<i class="fa fa-plus"></i> Edit Mutasi');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}
</script>