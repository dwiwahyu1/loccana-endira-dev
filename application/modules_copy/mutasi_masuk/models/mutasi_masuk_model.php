<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mutasi_Masuk_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function type_bc() {
		$sql 	= 'SELECT * FROM m_type_bc WHERE type_bc = 0;';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function lists($params = array()) {
		$sql 	= 'CALL mp_list_new(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function typeMaterial() {
		//$sql 	= 'SELECT * FROM m_type_material WHERE id_type_material BETWEEN 1 AND 3';
		$sql 	= 'SELECT * FROM m_type_material ';
		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function listbc() {
		$sql 	= 'SELECT * FROM t_bc a WHERE NOT EXISTS(SELECT * FROM t_bc_stock b WHERE b.id_stock = a.id);';
		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function getlistMaterial($id) {
		$sql 	= 'SELECT id, stock_name, stock_code FROM m_material WHERE type=? AND STATUS = 1';
		$query 	= $this->db->query($sql,
			array($id)
		);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function listMaterial() {
		$sql 	= 'SELECT * FROM m_material WHERE STATUS = 1';
		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function gudang() {
		$sql_all 	= 'CALL gudang_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				1000,
				0,
				NULL,
				NULL,
				''
			));
		$result = $query_all->result_array();
		
		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}

	public function existing_price($id) {
		$sql 	= 'SELECT * FROM d_price WHERE id_master=?';
		$query 	= $this->db->query($sql,
			array($id)
		);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_new_material($data) {
		$sql 	= 'CALL material_add(?,?,?,?,?,?,?,?,?,?,?)';
		$query 	=  $this->db->query($sql,
			array(
				$data['no_bc'],
				$data['stock_code'],
				$data['stock_name'],
				$data['stock_description'],
				$data['unit'],
				$data['type'],
				$data['qty'],
				$data['treshold'],
				$data['id_properties'],
				$data['id_gudang'],
				$data['status']
		));
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		
		return $arr_result;
	}
	public function mp_save($data) {
		$sql 	= 'CALL mp_add(?,?)';
		$query 	=  $this->db->query($sql,
			array(
				$data['id_material'],
				$data['amount_mutasi'],
			)
		);
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['result'] = $result;

		return $arr_result;
	}
	public function save_mutasi($data) {
		$sql 	= 'CALL mutasi_add(?,?,?,?)';
		$query 	=  $this->db->query($sql,
			array(
				$data['id_stock_awal'],
				$data['id_stock_akhir'],
				$data['amount_mutasi'],
				1
			)
		);
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function save_add_price($data) {
		$sql 	= 'CALL price_add(?,?,?,?,?)';
		$query 	=  $this->db->query($sql,
			array(
				$data['id_komponen'],
				$data['price'],
				$data['id_valas'],
				$data['id_master'],
				$data['start_price']
			)
		);
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function edit($id) {
		$sql 	= 'CALL material_search_id(?)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$id
			));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function existing_mutasi($id) {
		$sql 	= 'SELECT * FROM t_mutasi WHERE id_stock_akhir=?';
		$query 	= $this->db->query($sql,
			array($id)
		);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function save_edit_material($data) {
		$sql 	= 'UPDATE m_material SET qty=?  WHERE id=?;';
		$result 	= $this->db->query($sql, array(
			$data['qty'],
			$data['id_material']
		));

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function save_edit_mutasi($data) {
		$sql 	= 'CALL mutasi_update(?,?,?,?)';
		$query 	=  $this->db->query($sql,
			array(
				$data['id_mutasi'],
				$data['id_stock_awal'],
				$data['id_stock_akhir'],
				$data['amount_mutasi']
			)
		);
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function save_edit_price($data) {
		$sql 	= 'CALL price_update(?,?,?,?,?,?)';
		$query 	=  $this->db->query($sql,
			array(
				$data['id'],
				$data['id_komponen'],
				$data['price'],
				$data['id_valas'],
				$data['id_master'],
				$data['start_price']
			)
		);
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['result'] = $result;

		return $arr_result;
	}
}