<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Mutasi_Masuk extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('mutasi_masuk/mutasi_masuk_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string)
	{
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index()
	{
		$this->template->load('maintemplate', 'mutasi_masuk/views/index');
	}

	public function lists()
	{
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 2;
		$order_fields = array('', 'no_bc', 'stock_code', 'stock_name', 'stock_description', 'unit', 'type', 'qty', 'treshold');

		$search = $this->input->get_post('search');
		$search_val = (!empty($search['value'])) ? $search['value'] : null;
		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->mutasi_masuk_model->lists($params);
		// echo "<pre>";
		// print_r($list);
		// echo "</pre>";
		// die;
		
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = $params['offset'];
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;

			array_push($data, array(
				$i,
				$v['no_pendaftaran'],
				$v['stock_code'],
				$v['stock_name'],
				$v['uom_name'],
				$v['type_material_name'],
				// $v['nama_valas'],
				// 'Rp. ' . number_format($v['price'], 0, ",", "."),
				str_replace('.', ',', (string) $v['qty']),
				$v['nama_gudang']
			));
		}

		$result["data"] = $data;
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add()
	{
		$result_type = $this->mutasi_masuk_model->typeMaterial();
		$result_bc = $this->mutasi_masuk_model->listbc();
		// $result_list = $this->mutasi_masuk_model->listMaterial();
		$result_gudang = $this->mutasi_masuk_model->gudang();
		$data = array(
			'typeMaterial'	=> $result_type,
			'bc'			=> $result_bc,
			// 'listMaterial'	=> $result_list,
			'gudang'		=> $result_gudang
		);

		$this->load->view('add_modal_view', $data);
	}

	public function add_bc()
	{
		$result_type = $this->mutasi_masuk_model->type_bc();
		$data = array(
			'jenis_bc' => $result_type
		);

		$this->load->view('add_bc_modal_view', $data);
	}

	public function get_list_material()
	{
		$data		= file_get_contents("php://input");
		$params		= json_decode($data, true);
		$result_list = $this->mutasi_masuk_model->getlistMaterial($params['id_type']);

		echo json_encode(['data' => $result_list]);
	}

	public function get_material()
	{
		$data		= file_get_contents("php://input");
		$params		= json_decode($data, true);
		$result = $this->mutasi_masuk_model->edit($params['id_select']);

		echo json_encode(['data' => $result[0]]);
	}

	public function save_add_mutasi_new()
	{
		$this->form_validation->set_rules('stock_pindah', 'Stock Pindah', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			$id_material = $this->Anti_sql_injection($this->input->post('id_material', TRUE));
			$stock_awal = (int) $this->Anti_sql_injection($this->input->post('stock_awal', TRUE));
			$stock_pindah = (int) $this->Anti_sql_injection($this->input->post('stock_pindah', TRUE));
			$stock_akhir = (int) $this->Anti_sql_injection($this->input->post('stock_akhir', TRUE));
			// var_dump($this->input->post());die;
			if ($stock_awal != 0 && $stock_pindah < $stock_awal) {

				$dataMaterial = array(
					'id_material'	=> $id_material,
					'qty'			=> $stock_akhir,
					'amount_mutasi'		=> $stock_pindah
				);
				$this->mutasi_masuk_model->save_edit_material($dataMaterial);
				$oldDataMutasi = array(
					'id_stock_awal'		=> $id_material,
					'id_stock_akhir'	=> $id_material,
					'amount_mutasi'		=> $stock_pindah
				);
				$this->mutasi_masuk_model->mp_save($dataMaterial);
				// $resultNewMaterial	= $this->mutasi_masuk_model->add_new_material($newMaterial);
				$resultOldMutasi	= $this->mutasi_masuk_model->save_mutasi($oldDataMutasi);
				//End

				if ($resultOldMutasi > 0) $result = 1;
				else $result = 0;
			} else $result = 0;

			if ($result > 0) {
				$this->log_activity->insert_activity('insert', 'Insert Mutasi Material');
				$result = array('success' => true, 'message' => 'Berhasil menambahkan Mutasi Material ke database');
			} else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert Mutasi Material');
				$result = array('success' => false, 'message' => 'Gagal menambahkan Mutasi Material ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function save_add_mutasi()
	{
		$this->form_validation->set_rules('stock_pindah', 'Stock Pindah', 'trim|required');
		$this->form_validation->set_rules('gudang_pindah', 'Gudang Pindah', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			$id_material = $this->Anti_sql_injection($this->input->post('id_material', TRUE));
			$stock_awal = (int) $this->Anti_sql_injection($this->input->post('stock_awal', TRUE));
			// $stock_akhir = (int) $this->Anti_sql_injection($this->input->post('stock_akhir', TRUE));
			$stock_pindah = (int) $this->Anti_sql_injection($this->input->post('stock_pindah', TRUE));
			$gudang_awal = (int) $this->Anti_sql_injection($this->input->post('gudang_awal', TRUE));
			$gudang_pindah = (int) $this->Anti_sql_injection($this->input->post('gudang_pindah', TRUE));

			if ($stock_awal != 0 && $stock_pindah < $stock_awal) {
				if ($gudang_pindah != $gudang_awal) {
					$oldMaterial = $this->mutasi_masuk_model->edit($id_material);
					$oldPrice = $this->mutasi_masuk_model->existing_price($id_material);
					$oldMutasi = $this->mutasi_masuk_model->existing_mutasi($id_material);

					//insert New Material And Edit existing price, mutasi
					$newMaterial = array(
						'no_bc' => $oldMaterial[0]['no_bc'],
						'stock_code' => $oldMaterial[0]['stock_code'],
						'stock_name' => $oldMaterial[0]['stock_name'],
						'stock_description' => $oldMaterial[0]['stock_description'],
						'unit' => $oldMaterial[0]['unit'],
						'type' => $oldMaterial[0]['type'],
						'qty' => $stock_awal,
						'treshold' => $oldMaterial[0]['treshold'],
						'id_properties' => $oldMaterial[0]['id_properties'],
						'id_gudang' => $gudang_pindah,
						'status' => '1'
					);

					$oldDataPrice = array(
						'id'			=> $oldPrice[0]['id'],
						'id_komponen'	=> $oldPrice[0]['id_komponen'],
						'price'			=> ($stock_awal - $stock_pindah) * $oldPrice[0]['start_price'],
						'id_valas'		=> $oldPrice[0]['id_valas'],
						'id_master'		=> $oldPrice[0]['id_master'],
						'start_price'	=> $oldPrice[0]['start_price']
					);

					$oldDataMutasi = array(
						'id_mutasi'			=> $oldMutasi[0]['id_mutasi'],
						'id_stock_awal'		=> $id_material,
						'id_stock_akhir'	=> $id_material,
						'amount_mutasi'		=> ($stock_awal - $stock_pindah)
					);

					// $resultNewMaterial	= $this->mutasi_masuk_model->add_new_material($newMaterial);
					$resultOldPrice		= $this->mutasi_masuk_model->save_edit_price($oldDataPrice);
					$resultOldMutasi	= $this->mutasi_masuk_model->save_edit_mutasi($oldDataMutasi);
					//End

					//Insert New Mutasi and New Price
					$newDataMutasi = array(
						'id_stock_awal'		=> $id_material,
						// 'id_stock_akhir'	=> $resultNewMaterial['lastid'],
						'amount_mutasi'		=> $stock_pindah
					);

					$newDataPrice = array(
						'id_komponen'	=> $oldPrice[0]['id_komponen'],
						'price'			=> $stock_pindah * $oldPrice[0]['start_price'],
						'id_valas'		=> $oldPrice[0]['id_valas'],
						// 'id_master'		=> $resultNewMaterial['lastid'],
						'start_price'	=> $oldPrice[0]['start_price']
					);

					$resultOldMutasi	= $this->mutasi_masuk_model->save_mutasi($newDataMutasi);
					$resultOldPrice		= $this->mutasi_masuk_model->save_add_price($newDataPrice);
					//End

					if ($resultOldPrice > 0 && $resultOldMutasi > 0 && $newDataMutasi > 0 && $resultOldPrice > 0) $result = 1;
					else $result = 0;
				} else $result = 0;
			} else $result = 0;

			if ($result > 0) {
				$this->log_activity->insert_activity('insert', 'Insert Mutasi Material');
				$result = array('success' => true, 'message' => 'Berhasil menambahkan Mutasi Material ke database');
			} else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert Mutasi Material');
				$result = array('success' => false, 'message' => 'Gagal menambahkan Mutasi Material ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function edit($id)
	{
		$result = $this->mutasi_masuk_model->edit($id);
		$result_mutasi = $this->mutasi_masuk_model->existing_mutasi($id);
		$result_gudang = $this->mutasi_masuk_model->gudang();
		$data = array(
			'material'	=> $result,
			'mutasi'	=> $result_mutasi,
			'gudang'	=> $result_gudang
		);

		$this->load->view('edit_modal_view', $data);
	}

	public function save_edit_mutasi()
	{
		$this->form_validation->set_rules('stock_pindah', 'Stock Pindah', 'trim|required');
		$this->form_validation->set_rules('gudang_pindah', 'Gudang Pindah', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			$id_material = $this->Anti_sql_injection($this->input->post('id_material', TRUE));
			$id_mutasi = $this->Anti_sql_injection($this->input->post('id_mutasi', TRUE));
			$stock_awal = (int) $this->Anti_sql_injection($this->input->post('stock_awal', TRUE));
			$stock_akhir = (int) $this->Anti_sql_injection($this->input->post('stock_akhir', TRUE));
			$stock_pindah = (int) $this->Anti_sql_injection($this->input->post('stock_pindah', TRUE));
			$gudang_awal = (int) $this->Anti_sql_injection($this->input->post('gudang_awal', TRUE));
			$gudang_pindah = (int) $this->Anti_sql_injection($this->input->post('gudang_pindah', TRUE));

			if ($stock_awal != 0 && $stock_pindah < $stock_awal) {
				if ($gudang_pindah != $gudang_awal) {
					$dataMaterial = array(
						'id_material'	=> $id_material,
						'qty'			=> $stock_akhir,
						'id_gudang'		=> $gudang_pindah,
					);
					$resultMaterial = $this->mutasi_masuk_model->save_edit_material($dataMaterial);

					$dataMutasi = array(
						'id_mutasi'	=> $id_mutasi,
						'date_mutasi'	=> date('Y-m-d'),
						'stock_pindah'	=> $stock_pindah
					);
					$resultMutasi = $this->mutasi_masuk_model->save_edit_mutasi($dataMutasi);
					if ($resultMaterial > 0 && $resultMutasi > 0) $result = 1;
					else $result = 0;
				} else $result = 0;
			} else $result = 0;

			if ($result > 0) {
				$this->log_activity->insert_activity('update', 'Update Mutasi Material id : ' . $id_mutasi);
				$result = array('success' => true, 'message' => 'Berhasil mengubah Mutasi Material ke database');
			} else {
				$this->log_activity->insert_activity('update', 'Gagal Update Mutasi Material id : ' . $id_mutasi);
				$result = array('success' => false, 'message' => 'Gagal mengubah Mutasi Material ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}
}
