<style>
	.dt-body-left {text-align:left;}
	.dt-body-right {text-align:right;}
	.dt-body-center {text-align:center; vertical-align: middle;}
	.force-overflow {height: 650px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow {min-height: 650px}
	#modal-customer::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	#modal-customer::-webkit-scrollbar
	{
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-customer::-webkit-scrollbar-thumb
	{
		background-image: -webkit-gradient(linear,
		   left bottom,
		   left top,
		   color-stop(0.44, rgb(122,153,217)),
		   color-stop(0.72, rgb(73,125,189)),
		   color-stop(0.86, rgb(28,58,148)));
	}
</style>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">Customer</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">

                    <table id="listcust" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Kode Customer</th>
                          <th>Nama</th>
                          <th>Alamat</th>
                          <th>Ship Address</th>
                          <th>Phone 1</th>
                          <th>Phone 2</th>
                          <th>Fax</th>
                          <th>Email</th>
                          <th>PIC</th>
                          <th>Lokasi</th>
                          <th>Option</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>

            </div>
        </div><!-- end col -->
    </div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body force-overflow" id="modal-customer">
					<div class="scroll-overflow">
						<p></p>
					</div>
				</div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  function add_customer(){
      $('#panel-modal').removeData('bs.modal');
      $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modal  .panel-body').load('<?php echo base_url('customer/add');?>');
      $('#panel-modal  .panel-title').html('<i class="fa fa-user-plus"></i> Add Customer');
      $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
  }

  function editcustomer(id){
      $('#panel-modal').removeData('bs.modal');
      $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modal  .panel-body').load('<?php echo base_url('customer/edit/');?>'+"/"+id);
      $('#panel-modal  .panel-title').html('<i class="fa fa-user"></i> Edit Customer');
      $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
  }

  function deletecustomer(id){
    swal({
        title: 'Yakin akan Menghapus ?',
        text: 'data tidak dapat dikembalikan bila sudah dihapus !',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya',
        cancelButtonText: 'Tidak'
      }).then(function () {
          var datapost={
            "id"  :   id
          };

          $.ajax({
                type:'POST',
                url: "<?php echo base_url().'customer/delete_customer';?>",
                data:JSON.stringify(datapost),
                cache:false,
                contentType: false,
                processData: false,
                success: function(response) {
                    $('.panel-heading button').trigger('click');
                    listcust();
                    swal({
                      title: 'Success!',
                      text: response.message,
                      type: 'success',
                      showCancelButton: false,
                      confirmButtonText: 'Ok'
                    }).then(function () {
                    })
                }
            });
      });
  }

  function listcust(){
      $("#listcust").dataTable({
          "processing": true,
          "serverSide": true,
          "ajax": "<?php echo base_url().'customer/lists';?>",
          "searchDelay": 700,
          "responsive": true,
          "lengthChange": false,
          "destroy": true,
          "info": false,
          "bSort": false,
          "dom": 'l<"toolbar">frtip',
          "initComplete": function(){
              var element = '<div class="btn-group pull-left">';
                  element += '  <a class="btn btn-primary" onClick="add_customer()">';
                  element += '    <i class="fa fa-user-plus"></i> Add Customer';
                  element += '  </a>';
                  element += '</div>';
              $("div.toolbar").prepend(element);
          },
		  "columnDefs": [{
				"targets": [0],
				"searchable": false,
				"className": 'dt-body-center',
				"width": 10
			},{
				"targets": [2,3,4],
				"searchable": false,
				"width": 150
			},{
				"targets": [5,6,7,8],
				"searchable": false,
				"width": 80
			},{
				"targets": [11],
				"searchable": false,
				"className": 'dt-body-center',
				"width": 100
			}]
      });
  }

	$(document).ready(function(){
      listcust();
  });
</script>