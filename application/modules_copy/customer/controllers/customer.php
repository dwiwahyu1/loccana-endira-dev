<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('customer/customer_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 * @return string
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	  * This function is redirect to index customer page
	  * @return Void
	  */
	public function index() {
		$this->template->load('maintemplate', 'customer/views/index');
	}

	/**
	  * This function is used for showing customers list
	  * @return Array
	  */
	function lists() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

		$order_fields = array('', 'id', 'cust_name');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search = $this->input->get_post('search');

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['type_eksternal'] = 1;
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->customer_model->lists($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		//print_r($result);die;

		$data = array();

		$i = $params['offset'];
		foreach ($list['data'] as $k => $v) {
			$i++;
			$actions = '<div class="btn-group">';
			$actions .= '   <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="editcustomer(\'' . $v['id'] . '\')">';
			$actions .= '       <i class="fa fa-edit"></i>';
			$actions .= '   </button>';
			$actions .= '</div>';
			$actions .= '<div class="btn-group">';
			$actions .= '   <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deletecustomer(\'' . $v['id'] . '\')">';
			$actions .= '       <i class="fa fa-trash"></i>';
			$actions .= '   </button>';
			$actions .= '</div>';
			
			array_push($data, array(
				$i,
				$v['kode_eksternal'],
				$v['name_eksternal'],
				$v['eksternal_address'],
				$v['ship_address'],
				$v['phone_1'],
				$v['phone_2'],
				$v['fax'],
				$v['email'],
				$v['pic'],
				$v['name_eksternal_loc'],
				$actions
					)
			);
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add() {
		$result = $this->customer_model->location();

		$data = array(
			'location' => $result
		);

		$this->load->view('add_modal_view',$data);
	}

	public function add_customer() {
		$kode_customer 	= $this->Anti_sql_injection($this->input->post('kode_customer', TRUE));
		$cust_name 		= $this->Anti_sql_injection($this->input->post('cust_name', TRUE));
		$cust_address 	= $this->Anti_sql_injection($this->input->post('cust_address', TRUE));
		$ship_address 	= $this->Anti_sql_injection($this->input->post('ship_address', TRUE));
		$phone_1 		= $this->Anti_sql_injection($this->input->post('phone_1', TRUE));
		$phone_2 		= $this->Anti_sql_injection($this->input->post('phone_2', TRUE));
		$fax 			= $this->Anti_sql_injection($this->input->post('fax', TRUE));
		$email 			= $this->Anti_sql_injection($this->input->post('email', TRUE));
		$pic 			= $this->Anti_sql_injection($this->input->post('pic', TRUE));
		$location 		= $this->Anti_sql_injection($this->input->post('location', TRUE));

		$data = array(
			'kode_customer'		=> $kode_customer,
			'name_eksternal'	=> $cust_name,
			'eksternal_address'	=> $cust_address,
			'ship_address'		=> $ship_address,
			'phone_1'			=> $phone_1,
			'phone_2'			=> $phone_2,
			'fax'				=> $fax,
			'email'				=> $email,
			'pic'				=> $pic,
			'eksternal_loc'		=> $location,
			'type_eksternal'	=> 1
		);

		$add_customer_result = $this->customer_model->add_customer($data);
		if ($add_customer_result['result'] > 0) {
			$dataCustomerCoa = array(
				'id_parent'		=> '20100',
				'keterangan'	=> $cust_name,
				'type_coa'		=> 0,
				'id_eksternal'	=> $add_customer_result['lastid']
			);
			$add_customer_coa_result = $this->customer_model->add_customer_coa($dataCustomerCoa);

			if($add_customer_coa_result > 0) {
				$msg = 'Berhasil menambahkan customer';

				$result = array('success' => true, 'message' => $msg);
				$this->log_activity->insert_activity('insert', $msg. ' dengan kode customer ' .$kode_customer);
			}
		}else {
			$msg = 'Gagal menambahkan customer ke database';

			$result = array('success' => false, 'message' => $msg);
			$this->log_activity->insert_activity('insert', $msg. ' dengan kode customer ' .$kode_customer);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function edit($id) {
		$result = $this->customer_model->detail($id);
		$location = $this->customer_model->location();

		$data = array('detail' => $result, 'location' => $location);
		$this->load->view('edit_modal_view', $data);
	}

	public function edit_customer() {
		$id 			= $this->Anti_sql_injection($this->input->post('id', TRUE));
		$kode_customer 	= $this->Anti_sql_injection($this->input->post('kode_customer', TRUE));
		$cust_name 		= $this->Anti_sql_injection($this->input->post('cust_name', TRUE));
		$cust_address 	= $this->Anti_sql_injection($this->input->post('cust_address', TRUE));
		$ship_address 	= $this->Anti_sql_injection($this->input->post('ship_address', TRUE));
		$phone_1 		= $this->Anti_sql_injection($this->input->post('phone_1', TRUE));
		$phone_2 		= $this->Anti_sql_injection($this->input->post('phone_2', TRUE));
		$fax			= $this->Anti_sql_injection($this->input->post('fax', TRUE));
		$email 			= $this->Anti_sql_injection($this->input->post('email', TRUE));
		$pic 			= $this->Anti_sql_injection($this->input->post('pic', TRUE));
		$location 		= $this->Anti_sql_injection($this->input->post('location', TRUE));
		$status 		= 0;
		
		$data = array(
			'id' => $id,
			'kode_customer' 	=> $kode_customer,
			'name_eksternal' 	=> $cust_name,
			'eksternal_address' => $cust_address,
			'ship_address' 		=> $ship_address,
			'phone_1' 			=> $phone_1,
			'phone_2' 			=> $phone_2,
			'fax' 				=> $fax,
			'email' 			=> $email,
			'pic' 				=> $pic,
			'eksternal_loc' 	=> $location
		);

		$dataCoa = array(
			'id_eksternal'	=> $id,
			'keterangan'	=> $cust_name
		);
		
		$result_customer 		= $this->customer_model->edit_customer($data);
		$result_customer_coa	= $this->customer_model->edit_customer_coa($dataCoa);
		if ($result_customer['status'] > 0 && $result_customer_coa['status'] > 0) {
			$msg = 'Berhasil merubah customer';

			$result = array('success' => true, 'message' => $msg);
			$this->log_activity->insert_activity('update', $msg. ' dengan kode customer ' .$kode_customer);
		}else {
			$msg = 'Gagal merubah customer ke database';

			$result = array('success' => false, 'message' => $msg);
			$this->log_activity->insert_activity('update', $msg. ' dengan kode customer ' .$kode_customer);
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function delete_customer() {
		$data 		= file_get_contents("php://input");
		$params     = json_decode($data,true);

		$result_customer = $this->customer_model->delete_customer($params['id']);
		$result_customer_coa = $this->customer_model->delete_customer_coa($params['id']);

		$msg = 'Berhasil menghapus data customer.';
		$result = array('success' => true, 'message' => $msg);

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function check_kode(){
		$this->form_validation->set_rules('kode_customer', 'kode_customer', 'trim|required|min_length[3]|max_length[10]|is_unique[t_eksternal.kode_eksternal]');
		$this->form_validation->set_message('is_unique', 'Kode Customer Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'Kode Customer Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}
}