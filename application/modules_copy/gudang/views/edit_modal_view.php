<!--Parsley-->
<script type="text/javascript" src="<?php echo base_url('assets'); ?>/gentelella-master/vendors/parsleyjs/dist/parsley.min.js"></script>
<style>
	#loading-us{display:none}
	#tick{display:none}

	#loading-mail{display:none}
	#cross{display:none}
</style>

<form class="form-horizontal form-label-left" id="edit_form" role="form" action="<?php echo base_url('gudang/save_edit_gudang');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="gudangname">Nama Gudang <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-minlength="4" data-parsley-maxlength="100" type="text" id="gudangname" name="gudangname" class="form-control col-md-7 col-xs-12" placeholder="nama properties minimal 4 karakter" value="<?php if(isset($gudang[0]['nama_gudang'])){ echo $gudang[0]['nama_gudang']; }?>" required="required">
			<span id="loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking gudang...</span>
			<span id="tick"></span>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamatgudang">Alamat Gudang <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<textarea data-parsley-maxlength="255" id="alamatgudang" name="alamatgudang" class="form-control col-md-7 col-xs-12" cols="5" placeholder="alamat gudang"><?php if(isset($gudang[0]['alamat_gudang'])){ echo $gudang[0]['alamat_gudang']; }?></textarea>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id">Type Gudang <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="type_id" name="type_id" style="width: 100%" required>
				<option value="" selected="selected">-- Select Type --</option>
			<?php foreach($type_gudang as $key) { ?>
				<option value="<?php echo $key['id']; ?>"<?php
					if(isset($gudang[0]['type_gudang'])) {
						if( $gudang[0]['type_gudang'] == $key['id'] ) { echo "selected"; }
					}?> ><?php
						echo $key['nama_type_gudang']; ?>
				</option>
			<?php } ?>
			</select>
			<input data-parsley-maxlength="255" type="hidden" id="gudang_id" name="gudang_id" value="<?php if(isset($gudang[0]['id_gudang'])){ echo $gudang[0]['id_gudang']; }?>">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Gudang</button>
		</div>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
		$('#type_id').select2();
	});

	var last_gudangname = $('#gudangname').val();
	$('#gudangname').on('input',function(event) {
		if($('#gudangname').val() != last_gudangname) {
			gudangname_check();
		}
	});

	function gudangname_check() {
		var gudangname = $('#gudangname').val();
		if(gudangname.length > 3) {
			var post_data = {
				'gudangname': gudangname
			};

			$('#tick').empty();
			$('#tick').hide();
			$('#loading-us').show();
			jQuery.ajax({
				type: "POST",
				url: "<?php echo base_url('gudang/check_gudangname');?>",
				data: post_data,
				cache: false,
				success: function(response){
					if(response.success == true){
						$('#gudangname').css('border', '3px #090 solid');
						$('#loading-us').hide();
						$('#tick').empty();
						$("#tick").append('<span class="fa fa-check"> '+response.message+'</span>');
						$('#tick').show();
					}else {
						$('#gudangname').css('border', '3px #C33 solid');
						$('#loading-us').hide();
						$('#tick').empty();
						$("#tick").append('<span class="fa fa-close"> '+response.message+'</span>');
						$('#tick').show();
					}
				}
			});
		}else {
			$('#gudangname').css('border', '3px #C33 solid');
			$('#loading-us').hide();
			$('#tick').empty();
			$("#tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
			$('#tick').show();
		}
	}

	$('#edit_form').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		var formData = new FormData(this);

		$.ajax({
			type:'POST',
			url: $(this).attr('action'),
			data:formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						window.location.href = "<?php echo base_url('gudang');?>";
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Edit Gudang");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Edit Gudang");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}));
</script>
