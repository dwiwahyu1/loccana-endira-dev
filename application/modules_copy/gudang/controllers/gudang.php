<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Gudang extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('gudang/gudang_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'gudang/views/index');
	}

	function list_gudang() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 2;
		$order_fields = array('', 'id_gudang', 'nama_gudang', 'alamat_gudang', 'pic');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->gudang_model->list_gudang($params);
		$typegudang = $this->gudang_model->type_gudang($params);
		// print_r($list);
		// print_r($typegudang);
		// die;

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$status_akses =
				'<div class="btn-group">'.
					'<button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
						onClick="edit_gudang(\'' . $v['id_gudang'] . '\')">'.
						'<i class="fa fa-edit"></i>'.
					'</button>'.
				'</div>'.
				'<div class="btn-group">'.
					'<button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete"
						onClick="delete_gudang(\'' . $v['id_gudang'] . '\')">'.
						'<i class="fa fa-trash"></i>'.
					'</button>'.
				'</div>';
			$strType = "";
			foreach ($typegudang as $l => $o) {
				if($v['type_gudang'] == $o['id']) $strType = $o['nama_type_gudang'];
			}

			array_push($data, array(
				$i,
				ucwords($v['nama_gudang']),
				ucwords($v['alamat_gudang']),
				$strType,
				$status_akses
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_gudang() {
		$result_type = $this->gudang_model->type_gudang();
		$data = array(
			'type_gudang' => $result_type
		);

		$this->load->view('add_modal_view', $data);
	}

	public function check_gudangname(){
		$this->form_validation->set_rules('gudangname', 'Gudang Name', 'trim|required|min_length[4]|max_length[100]|is_unique[t_gudang.nama_gudang]');
		$this->form_validation->set_message('is_unique', 'Detail Gudang Already Exists.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'Nama Gudang Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}

	public function save_gudang() {
		$this->form_validation->set_rules('gudangname', 'Gudang Name', 'trim|required|min_length[4]|max_length[100]|is_unique[t_gudang.nama_gudang]',array('is_unique' => 'This %s already exists.'));
		$this->form_validation->set_rules('alamatgudang', 'Alamat Gudang', 'trim|max_length[100]');
		$this->form_validation->set_rules('type_id', 'Type Gudang', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$gudangname = ucwords($this->Anti_sql_injection($this->input->post('gudangname', TRUE)));
			$alamatgudang = ucwords($this->Anti_sql_injection($this->input->post('alamatgudang', TRUE)));
			$type_id = $this->Anti_sql_injection($this->input->post('type_id', TRUE));

			$data = array(
				'gudangname' => $gudangname,
				'alamatgudang' => $alamatgudang,
				'pic' => NULL,
				'type_id' => $type_id
			);

			$result = $this->gudang_model->add_gudang($data);

			if ($result > 0) {
				$msg = 'Berhasil menambahkan gudang.';
				$this->log_activity->insert_activity('insert', 'Berhasil Insert Gudang');
				$result = array('success' => true, 'message' => $msg);
			}else {
				$msg = 'Gagal menambahkan gudang ke database.';
				$this->log_activity->insert_activity('insert', 'Gagal Insert Gudang');
				$result = array('success' => false, 'message' => $msg);
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function edit_gudang($id) {
		$result = $this->gudang_model->edit_gudang($id);
		$typegudang = $this->gudang_model->type_gudang();

		$data = array(
			'gudang' => $result,
			'type_gudang' => $typegudang
		);
		// print_r($data);die;

		$this->load->view('edit_modal_view', $data);
	}

	public function save_edit_gudang() {
		$this->form_validation->set_rules('gudangname', 'Gudang Name', 'trim|required|min_length[4]|max_length[100]',array('is_unique' => 'This %s already exists.'));
		$this->form_validation->set_rules('alamatgudang', 'Alamat Gudang', 'trim|max_length[100]');
		$this->form_validation->set_rules('type_id', 'Type Gudang', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$id_gudang = $this->Anti_sql_injection($this->input->post('gudang_id', TRUE));
			$gudangname = ucwords($this->Anti_sql_injection($this->input->post('gudangname', TRUE)));
			$alamatgudang = ucwords($this->Anti_sql_injection($this->input->post('alamatgudang', TRUE)));
			$type_id = $this->Anti_sql_injection($this->input->post('type_id', TRUE));

			$data = array(
				'id_gudang' => $id_gudang,
				'gudangname' => $gudangname,
				'alamatgudang' => $alamatgudang,
				'pic' => NULL,
				'type_id' => $type_id
			);

			$result = $this->gudang_model->save_edit_gudang($data);

			if ($result > 0) {
				$msg = 'Berhasil merubah gudang.';
				$this->log_activity->insert_activity('insert', 'Berhasil Update Gudang Id Gudang = '.$id_gudang);
				$result = array('success' => true, 'message' => $msg);
			}else {
				$msg = 'Gagal merubah gudang.';
				$this->log_activity->insert_activity('insert', 'Gagal Update Gudang Id Gudang = '.$id_gudang);
				$result = array('success' => false, 'message' => $msg);
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function delete_gudang() {
		$data 	= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		$list = $this->gudang_model->delete_gudang($params['id']);

		$this->log_activity->insert_activity('insert', 'Berhasil Delete Gudang Id Gudang = '.$params['id']);
		$res = array('status' => 'success', 'message' => 'Data telah di hapus');

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}
}