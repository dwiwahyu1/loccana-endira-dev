<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Gudang_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function type_gudang() {
		$sql 	= 'SELECT * FROM t_type_gudang;';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function check_userid($params = array()) {
		$sql 	= 'SELECT COUNT(id) AS jumlah FROM u_user WHERE id = ? AND status_akses = 1;';
		$query 	= $this->db->query($sql,
			array($params['user_id'])
		);
		$return = $query->row_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function list_gudang($params = array()) {
		$sql 	= 'CALL gudang_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function add_gudang($data) {
		$sql 	= 'CALL gudang_add(?,?,?,?)';

		$query 	=  $this->db->query($sql,
			array(
				$data['gudangname'],
				$data['alamatgudang'],
				$data['pic'],
				$data['type_id'],
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_gudang($id) {
		$sql 	= 'CALL gudang_search_id(?)';

		$query 	=  $this->db->query($sql,
			array($id)
		);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function save_edit_gudang($data) {
		$sql 	= 'CALL gudang_update(?,?,?,?,?)';
		
		$query 	=  $this->db->query($sql,
			array(
				$data['id_gudang'],
				$data['gudangname'],
				$data['alamatgudang'],
				$data['pic'],
				$data['type_id'],
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function delete_gudang($data) {
		$sql 	= 'CALL gudang_delete(?)';
		$query 	=  $this->db->query($sql,
			array($data)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}