<style>
	#loading-us{display:none}
	#tick{display:none}

	#loading-mail{display:none}
	#cross{display:none}
	.add_item{cursor:pointer;#96b6e8;padding-top: 6px;}
	.add_item:hover{color:#ff8c00}
	.add_item a:hover{color:#ff8c00}
	.inputStyleNum {text-align: right;}
</style>

<form class="form-horizontal form-label-left" id="approve_rtrn" role="form" action="<?php echo base_url('barang_return/save_approve_rtrn');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<br>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="po_quot">PO</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="po_quot" name="po_quot" value="<?php
				if(isset($data_rtrn[0]['order_no'])) echo $data_rtrn[0]['order_no'];
			?>" placeholder="PO" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="kode_stok">Kode Stok</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="kode_stok" name="kode_stok" value="<?php
				if(isset($data_rtrn[0]['stock_code'])) echo $data_rtrn[0]['stock_code'];
			?>" placeholder="Kode Stok" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="bc">BC</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="bc" name="bc" value="<?php
				if(isset($data_rtrn[0]['no_pendaftaran']) && isset($data_rtrn[0]['no_pengajuan'])) echo $data_rtrn[0]['no_pendaftaran'].' / '.$data_rtrn[0]['no_pengajuan'];
			?>" placeholder="BC" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_stok">Nama Stok</label>
		<div class="col-md-8 col-sm-6 col-xs-12">

			<input type="text" class="form-control" id="nama_stok" name="nama_stok" value="<?php
				if(isset($data_rtrn[0]['stock_name'])) echo $data_rtrn[0]['stock_name'];
			?>" placeholder="Nama Stok" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="harga_dasar">Harga Dasar</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control inputStyleNum" id="harga_dasar" name="harga_dasar" value="<?php
				if(isset($data_rtrn[0]['base_price'])) echo number_format($data_rtrn[0]['base_price'], 0, ',', '.');
			?>" placeholder="Harga Dasar" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty">Qty</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control inputStyleNum" id="qty" name="qty" value="<?php
				if(isset($data_rtrn[0]['qty'])) echo number_format($data_rtrn[0]['qty'], 0);
			?>" placeholder="Qty" autocomplete="off" required>
		</div>
	</div>

<?php if($data_rtrn[0]['type'] == 5) { ?>
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_scrap">Scrap</label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<select class="form-control" id="type_scrap" name="type_scrap" required>
					<option value="new">Buat Stok Baru</option>
					<option value="old">Stok Yang Sudah Ada</option>
				</select>
			</div>
			<input type="hidden" id="non_scrap" name="non_scrap" value="false">
		</div>
<?php }else { ?>
		<input type="hidden" id="non_scrap" name="non_scrap" value="true">
<?php } ?>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Approve Barang Return</button>
			<input type="hidden" id="id_rtrn" name="id_rtrn" value="<?php if(isset($data_rtrn[0]['id_retur'])) echo $data_rtrn[0]['id_retur']; ?>">
			<input type="hidden" id="id_material" name="id_material" value="<?php if(isset($data_rtrn[0]['id_produk'])) echo $data_rtrn[0]['id_produk']; ?>">
			<input type="hidden" id="id_bc" name="id_bc" value="<?php if(isset($data_rtrn[0]['id_bc'])) echo $data_rtrn[0]['id_bc']; ?>">
			<input type="hidden" id="id_po_quot" name="id_po_quot" value="<?php if(isset($data_rtrn[0]['id_po_quot'])) echo $data_rtrn[0]['id_po_quot']; ?>">
			<input type="hidden" id="cust_id" name="cust_id" value="<?php if(isset($data_rtrn[0]['cust_id'])) echo $data_rtrn[0]['cust_id']; ?>">
		</div>
	</div>
</form><!-- /page content -->

<script type="text/javascript">
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();

		$('#qty').on('focus', function() {
			var qty = parseInt(this.value.replace(/[^0-9]/g, ""));
			this.value = qty;
		});
		$('#qty').on('keyup', function() {
			var maxValue = tempQty;
			var qty = parseInt(this.value.replace(/[^0-9]/g, ""));
			if(qty == '' || qty == 0) this.value = 1;
			else {
				if(qty > maxValue) this.value = formatNumber(maxValue);
				else this.value = formatNumber(parseInt(qty));
			}
		});
		$('#qty').on('change', function() {
			var qty = parseInt(this.value.replace(/[^0-9]/g, ""));
			if(qty == '' || qty == 0) this.value = 1;
			else this.value = formatNumber(this.value.replace(/[^0-9]/g, ""));
		});
	});

	$('#approve_rtrn').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		var new_qty = parseInt($('#qty').val().replace(/[^0-9]/g, ""));

		var formData = new FormData(this);
		save_Form(formData, $(this).attr('action'));
	}));

	function save_Form(formData, url) {
		$.ajax({
			type:'POST',
			url: url,
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						listreturn();
						$('#panel-modal').modal('toggle');
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Approve Barang Return");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Approve Barang Return");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}
</script>