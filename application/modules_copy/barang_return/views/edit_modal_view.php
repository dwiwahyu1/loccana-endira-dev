<style>
	#loading-us{display:none}
	#tick{display:none}

	#loading-mail{display:none}
	#cross{display:none}
	.add_item{cursor:pointer;#96b6e8;padding-top: 6px;}
	.add_item:hover{color:#ff8c00}
	.add_item a:hover{color:#ff8c00}
	.inputStyleNum {text-align: right;}
</style>
<form class="form-horizontal form-label-left" id="add_rtrn" role="form" action="<?php echo base_url('barang_return/save_edit_rtrn');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<br>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="po_quot">PO <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select style="width: 100%;" class="form-control" id="po_quot" name="po_quot">
				<option value="">Pilih PO...</option>
			<?php foreach ($po_quot as $kpoq => $vpoq) {?>
				<option value="<?php echo $vpoq['id']; ?>" <?php if($vpoq['id'] == $data_quot['id']) echo "selected"; ?>><?php echo $vpoq['order_no']; ?></option>
			<?php }?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="kode_stok">Kode Stok <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select style="width: 100%;" class="form-control" id="kode_stok" name="kode_stok">
				<option value="">Pilih Stok...</option>
			<?php foreach ($stok as $kstk => $vstk) {?>
				<option value="<?php echo $vstk['id']; ?>" <?php if($vstk['id'] == $data_rtrn['id_produk']) echo "selected"; ?>><?php echo $vstk['stock_code']; ?></option>
			<?php }?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="bc">BC <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select style="width: 100%;" class="form-control" id="bc" name="bc">
				<option value="">Pilih BC(No Pendaftaran / No Pengajuan)</option>
			<?php foreach ($bc as $kbc => $vbc) {?>
				<option value="<?php echo $vbc['id']; ?>" <?php if($vbc['id'] == $data_rtrn['id_bc']) echo "selected"; ?>><?php echo $vbc['no_pendaftaran'].' / '.$vbc['no_pengajuan']; ?></option>
			<?php }?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_stok">Nama Stok</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="nama_stok" name="nama_stok" placeholder="Nama Stok" value="<?php if(isset($data_material['stock_name'])) echo $data_material['stock_name'];?>" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="harga_dasar">Harga Dasar</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control inputStyleNum" id="harga_dasar" name="harga_dasar" placeholder="Harga Dasar" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="qty">Qty</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control inputStyleNum" id="qty" name="qty" placeholder="Qty" autocomplete="off" required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Barang Return</button>
			<input type="hidden" id="id_rtrn" name="id_rtrn" value="<?php if(isset($data_rtrn['id_retur'])) echo $data_rtrn['id_retur']; ?>">
		</div>
	</div>
</form><!-- /page content -->

<script type="text/javascript">
	var tempQty = parseInt('<?php if(isset($data_material['qty'])) echo $data_material['qty'];?>').toFixed(0);
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
		$('#harga_dasar').val('IDR '+formatNumber(parseInt('<?php if(isset($data_material['base_price'])) echo $data_material['base_price'];?>').toFixed(0)));
		$('#qty').val(formatNumber(parseInt('<?php if(isset($data_rtrn['qty'])) echo $data_rtrn['qty'];?>').toFixed(0)));
		$('#po_quot').select2().attr('readonly', true).attr('disabled', true);
		$('#kode_stok').select2().attr('readonly', true).attr('disabled', true);
		$('#bc').select2();
	});

	$('#po_quot').on('change', function() {
		$('#kode_stok').children('option:not(:first)').remove().trigger('change');
		$('#kode_stok').attr('disabled', true);
		$('#bc').val('').attr('disabled', true);
		resetInput();
		if(this.value != '') getOrder(this.value);
	});

	$('#kode_stok').on('change', function() {
		$('#bc').val('').attr('disabled', true);
		resetInput();
		if(this.value != '') getMaterial(this.value);
	});

	$('#qty').on('focus', function() {
		var qty = parseInt(this.value.replace(/[^0-9]/g, ""));
		this.value = qty;
	});
	$('#qty').on('keyup', function() {
		var maxValue = tempQty;
		var qty = parseInt(this.value.replace(/[^0-9]/g, ""));
		if(qty == '' || qty == 0) this.value = 1;
		else {
			if(qty > maxValue) this.value = formatNumber(maxValue);
			else this.value = formatNumber(parseInt(qty));
		}
	});
	$('#qty').on('change', function() {
		var qty = parseInt(this.value.replace(/[^0-9]/g, ""));
		if(qty == '' || qty == 0) this.value = 1;
		else this.value = formatNumber(this.value.replace(/[^0-9]/g, ""));
	});

	function getOrder(id_order) {
		jQuery.ajax({
			type: "POST",
			url: "<?php echo base_url('barang_return/getOrder');?>",
			data: {id : id_order},
			cache: false,
			success: function(response) {
				if(response.success == true) {
					var data = response.data;
					for (var i = 0; i < data.length; i++) {
						var newOption = new Option(data[i].stock_code, data[i].id, true, true);
						$('#kode_stok').append(newOption);
					}
					$('#kode_stok').val('').removeAttr('disabled').trigger('change');
					$('#bc').val('').trigger('change');
				}else swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			}
		}).fail(function(xhr, status, message) {
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}

	function getMaterial(id_material) {
		jQuery.ajax({
			type: "POST",
			url: "<?php echo base_url('barang_return/getDetailMaterial');?>",
			data: {id : id_material},
			cache: false,
			success: function(response) {
				if(response.success == true) {
					var data = response.data;
					$('#nama_stok').val(data.stock_name);
					$('#harga_dasar').val('IDR '+formatNumber(parseInt(data.base_price).toFixed(0)));
					$('#qty').val(formatNumber(parseInt(data.qty).toFixed(0)));
					tempQty = parseInt(data.qty).toFixed(0);
					$('#bc').val('').removeAttr('disabled').trigger('change');
				}else swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			}
		}).fail(function(xhr, status, message) {
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}

	function resetInput() {
		$('#nama_stok').val('');
		$('#harga_dasar').val(0);
		$('#qty').val(0);
		tempQty = 0;
	}

	$('#add_rtrn').on('submit',(function(e) {
		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		var new_qty = parseInt($('#qty').val().replace(/[^0-9]/g, ""));

		if(new_qty > tempQty) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Edit Barang Return");
			$('#jumlah_bayar').css('border', '3px #C33 solid').focus();
			$('#tick').empty();
			$("#tick").append('<span class="fa fa-close"> Jumlah Qty lebih besar dari Qty Sebelumnya</span>');
			$('#tick').show();
			swal("Failed!", "Jumlah qty lebih besar dari qty sebelumnya.", "error");
		}else {
			var formData = new FormData(this);
			save_Form(formData, $(this).attr('action'));
		}
	}));

	function save_Form(formData, url) {
		$.ajax({
			type:'POST',
			url: url,
			data: formData,
			cache:false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						window.location.href = "<?php echo base_url('barang_return');?>";
					})
				}else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Edit Barang Return");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Edit Barang Return");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}
</script>