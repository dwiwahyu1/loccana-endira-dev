<style>
	.changed_status{cursor:pointer;text-decoration: underline;color:#96b6e8;}
	.changed_status:hover{color:#ff8c00}
	tbody .dt-body-left{text-align: left;}
	tbody .dt-body-center{text-align: center;}
	tbody .dt-body-right{text-align: right;}
</style>

<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">Barang Return</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<table id="listreturn" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>Kode BC</th>
							<th>Kode Stok</th>
							<th>Nama Stok</th>
							<th>Unit</th>
							<th>Tipe</th>
							<th>Valas</th>
							<th>Harga</th>
							<th>Stok</th>
							<th>Gudang</th>
							<th>Option</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div><!-- end col -->
	</div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:950px;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild-barang" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:950px;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$(document).ready(function() {
		listreturn();
	});

	function listreturn() {
		$("#listreturn").DataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'barang_return/lists/';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false,
			"destroy": true,
			"info": false,
			"bSort": false,
			/*"dom": 'l<"toolbar">frtip',
			"initComplete": function(){
				var element =
					'<div class="btn-group pull-left">'+
						'<a class="btn btn-primary" onClick="add_rtrn()">'+
							'<i class="fa fa-plus"></i> Add Barang'+
						'</a>'+
					'</div>';
				$("div.toolbar").prepend(element);
			},*/
			"columnDefs": [{
				"targets": 0,
				"className": 'dt-body-center'
			}, {
				"targets": [7, 8],
				"className": 'dt-body-right'
			}]
		});
	}

	function add_rtrn() {
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal .panel-body').load('<?php echo base_url('barang_return/add');?>');
		$('#panel-modal .panel-title').html('<i class="fa fa-plus"></i> Add Barang Return');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}
	
	function edit_rtrn(id_rtrn) {
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('barang_return/edit_rtrn/');?>'+"/"+id_rtrn);
		$('#panel-modal  .panel-title').html('<i class="fa fa-pencil"></i> Edit Barang Return');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function approve_rtrn(id_rtrn) {
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('barang_return/approve_brg_rtrb/');?>'+"/"+id_rtrn);
		$('#panel-modal  .panel-title').html('<i class="fa fa-pencil"></i> Approve Barang Return');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function delete_rtrn(id_rtrn) {
		swal({
			title: 'Yakin akan Menghapus ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			$.ajax({
				type 			: 'POST',
				url 			: "<?php echo base_url().'barang_return/delete_rtrn';?>",
				data 			: JSON.stringify({'id_rtrn' : id_rtrn}),
				cache 			: false,
				contentType 	: false,
				processData 	: false,
				success: function(response) {
					if(response.status == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function () {
							listreturn();
							// window.location.href = "<?php echo base_url('barang_return');?>";
						})
					}else swal("Failed!", response.message, "error");
				}
			});
		});
	}
</script>