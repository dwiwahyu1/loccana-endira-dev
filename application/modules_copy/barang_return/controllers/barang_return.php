<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang_Return extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('barang_return/barang_return_model');
		$this->load->model('mutasi/mutasi_model');
		$this->load->library('log_activity');
	}

	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'barang_return/views/index');
	}

	function lists() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

		$order_fields = array('', 'id');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->barang_return_model->lists($params);

		// print_r($list);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$no = $start+1;
		$data = array();

		foreach ($list['data'] as $k => $v) {
			$options = '';

			if($v['status_retur'] != 1) {
				$options =
					'<div class="btn-group">'.
						'<a class="btn btn-success" title="Approve Barang" onClick="approve_rtrn(\''. $v['id_retur'] .'\')">'.
							'<i class="fa fa-check"></i>'.
						'</a>'.
					'</div>';
				/*$options =
					'<div class="btn-group">'.
						'<button class="btn btn-warning btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
							onClick="edit_rtrn(\'' . $v['id_retur'] . '\')">'.
							'<i class="fa fa-edit"></i>'.
						'</button>'.
					'</div>'.
					'<div class="btn-group">'.
						'<button class="btn btn-danger btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Delete"
							onClick="delete_rtrn(\'' . $v['id_retur'] . '\')">'.
							'<i class="fa fa-trash"></i>'.
						'</button>'.
					'</div>';*/
			}

			// $options = '';
			
			array_push($data, array(
				$no,
				$v['no_pengajuan'],
				$v['stock_code'],
				$v['stock_name'],
				$v['uom_name'],
				$v['type_material_name'],
				$v['nama_valas'],
				$v['symbol_valas']." ".number_format($v['base_price'], 2,",","."),
				number_format($v['qty_retur'], 2,",","."),
				$v['nama_gudang'],
				$options
			));
			$no++;
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add() {
		$resultBC		= $this->barang_return_model->getBC();
		$resultPOQuot	= $this->barang_return_model->get_po_quot();
		
		$data = array(
			'bc'		=> $resultBC,
			'po_quot'	=> $resultPOQuot
		);
		$this->load->view('add_modal_view', $data);
	}

	public function getOrder() {
		$id		= $this->Anti_sql_injection($this->input->post('id', TRUE));

		$getData = $this->barang_return_model->getOrder($id);
		if(sizeof($getData) > 0) $result = array('success' => true, 'data' => $getData);
		else $result = array('success' => false);

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function getDetailMaterial() {
		$id		= $this->Anti_sql_injection($this->input->post('id', TRUE));

		$getData = $this->barang_return_model->getDetailMaterial($id);
		if(sizeof($getData) > 0) $result = array('success' => true, 'data' => $getData[0]);
		else $result = array('success' => false);

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_rtrn() {
		$this->form_validation->set_rules('po_quot', 'PO', 'trim|required');
		$this->form_validation->set_rules('kode_stok', 'Kode Stok', 'trim|required');
		$this->form_validation->set_rules('bc', 'BC', 'trim|required');
		$this->form_validation->set_rules('qty', 'No Pendaftaran', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$po_quot	= $this->Anti_sql_injection($this->input->post('po_quot', TRUE));
			$kode_stok	= $this->Anti_sql_injection($this->input->post('kode_stok', TRUE));
			$bc 		= $this->Anti_sql_injection($this->input->post('bc', TRUE));
			$qty		= $this->Anti_sql_injection($this->input->post('qty', TRUE));
			$qty 		= preg_replace("/[^0-9?! ]/","",$qty);
			$getIdOrder	= $this->barang_return_model->getIdOrder($po_quot);	
			$getData	= $this->barang_return_model->getDetailMaterial($kode_stok);
			
			if(sizeof($getData) > 0 && sizeof($getIdOrder) > 0) {
				$dataMaterial = array(
					'no_bc'					=> $getData[0]['no_bc'],
					'stock_code'			=> 'RTRN_'.$getData[0]['stock_code'],
					'stock_name'			=> $getData[0]['stock_name'],
					'stock_description'		=> $getData[0]['stock_description'],
					'unit'					=> $getData[0]['unit'],
					'type'					=> $getData[0]['type'],
					'qty'					=> $qty,
					'weight'				=> $getData[0]['weight'],
					'treshold'				=> $getData[0]['treshold'],
					'id_properties'			=> $getData[0]['id_properties'],
					'id_gudang'				=> $getData[0]['id_gudang'],
					'status'				=> 8,
					'base_price'			=> $getData[0]['base_price'],
					'base_qty'				=> $getData[0]['base_qty'],
					'cust_id'				=> $getIdOrder[0]['cust_id']
				);

				$resultAdd = $this->barang_return_model->add_material($dataMaterial);
				if($resultAdd['result'] > 0) {
					$dataRetur = array(
						'id_order'			=> $getIdOrder[0]['id_order'],
						'id_produk'			=> $kode_stok,
						'id_produk_retur'	=> $resultAdd['lastid'],
						'id_bc'				=> $bc,
						'qty'				=> $qty
					);

					$resultRetur = $this->barang_return_model->add_retur($dataRetur);

					if($resultRetur > 0) {
						$this->log_activity->insert_activity('insert', 'Insert Barang Return');
						$results = array('success' => true, 'message' => 'Berhasil menambahkan Barang Return ke database.');
					}else {
						$this->log_activity->insert_activity('insert', 'Gagal Insert Barang Return');
						$results = array('success' => false, 'message' => 'Gagal menambahkan Barang Return ke database.');
					}
				}else {
					$this->log_activity->insert_activity('insert', 'Gagal Insert Barang Return');
					$results = array('success' => false, 'message' => 'Gagal menambahkan Barang Return ke database.');
				}
			}else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert Barang Return');
				$results = array('success' => false, 'message' => 'Gagal menambahkan Barang Return ke database.');
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}

	public function edit_rtrn($id_rtrn) {
		$resultBC		= $this->barang_return_model->getBC();
		$resultPOQuot	= $this->barang_return_model->get_po_quot();

		$getDataRtrn		= $this->barang_return_model->getDataRtrn($id_rtrn);
		$getDataQuot		= $this->barang_return_model->getDataQuot($getDataRtrn[0]['id_order']);
		$getDataStok 		= $this->barang_return_model->getOrder($getDataQuot[0]['id']);
		$getDataMaterial	= $this->barang_return_model->getDetailMaterial($getDataRtrn[0]['id_produk']);

		$data = array(
			'bc'			=> $resultBC,
			'po_quot'		=> $resultPOQuot,
			'stok'			=> $getDataStok,
			'data_quot'		=> $getDataQuot[0],
			'data_rtrn'		=> $getDataRtrn[0],
			'data_material'	=> $getDataMaterial[0]
		);

		$this->load->view('edit_modal_view', $data);
	}

	public function save_edit_rtrn() {
		$this->form_validation->set_rules('bc', 'BC', 'trim|required');
		$this->form_validation->set_rules('qty', 'No Pendaftaran', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$id_rtrn	= $this->Anti_sql_injection($this->input->post('id_rtrn', TRUE));
			$bc			= $this->Anti_sql_injection($this->input->post('bc', TRUE));
			$qty		= $this->Anti_sql_injection($this->input->post('qty', TRUE));
			$qty 		= preg_replace("/[^0-9?! ]/","",$qty);

			$getDataRtrn		= $this->barang_return_model->getDataRtrn($id_rtrn);
			if(sizeof($getDataRtrn) > 0) {
				$getDataMaterial	= $this->barang_return_model->getDetailMaterial($getDataRtrn[0]['id_produk_retur']);
				if(sizeof($getDataMaterial) > 0) {
					$dataMaterial = array(
						'id'	=> $getDataRtrn[0]['id_produk_retur'],
						'qty'	=> $qty
					);
	
					$resultMaterial = $this->barang_return_model->update_material($dataMaterial);
					if($resultMaterial > 0) {
						$dataRtrn = array(
							'id_retur'	=> $id_rtrn,
							'id_bc'		=> $bc,
							'qty'		=> $qty
						);
		
						$resultRetur = $this->barang_return_model->update_rtrn($dataRtrn);
	
						if($resultRetur > 0) {
							$this->log_activity->insert_activity('update', 'Update Barang Return id : '.$id_retur);
							$results = array('success' => true, 'message' => 'Berhasil mengubah Barang Return ke database.');
						}else {
							$this->log_activity->insert_activity('update', 'Gagal Update Barang Return id : '.$id_retur);
							$results = array('success' => false, 'message' => 'Gagal mengubah Barang Return ke database.');
						}
					}else {
						$this->log_activity->insert_activity('update', 'Gagal Update Barang Return id : '.$id_retur);
						$results = array('success' => false, 'message' => 'Gagal mengubah Barang Return ke database.');
					}
				}else {
					$this->log_activity->insert_activity('update', 'Gagal Update Barang Return id : '.$id_retur);
					$results = array('success' => false, 'message' => 'Gagal mengubah Barang Return ke database.');
				}
			}else {
				$this->log_activity->insert_activity('update', 'Gagal Update Barang Return id : '.$id_retur);
				$results = array('success' => false, 'message' => 'Gagal mengubah Barang Return ke database.');
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}

	public function approve_brg_rtrb($id_rtrn) {
		$result_retur	= $this->barang_return_model->getBrgRetur($id_rtrn);

		$data = array(
			'data_rtrn'		=> $result_retur
		);

		$this->load->view('approve_modal_view', $data);
	}

	public function save_approve_rtrn() {
		$this->form_validation->set_rules('qty', 'No Pendaftaran', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$id_rtrn		= $this->Anti_sql_injection($this->input->post('id_rtrn', TRUE));
			$id_material	= $this->Anti_sql_injection($this->input->post('id_material', TRUE));
			$id_bc			= $this->Anti_sql_injection($this->input->post('id_bc', TRUE));
			$id_po_quot		= $this->Anti_sql_injection($this->input->post('id_po_quot', TRUE));
			$cust_id		= $this->Anti_sql_injection($this->input->post('cust_id', TRUE));
			$non_scrap		= $this->Anti_sql_injection($this->input->post('non_scrap', TRUE));
			$qty			= $this->Anti_sql_injection($this->input->post('qty', TRUE));
			$qty 			= preg_replace("/[^0-9?! ]/","",$qty);

			$dataMaterial 	= $this->barang_return_model->getDetailMaterial($id_material);

			$data = array(
				'id_rtrn'		=> $id_rtrn,
				'id_material'	=> $id_material,
				'id_bc'			=> $id_bc,
				'id_po_quot'	=> $id_po_quot,
				'cust_id'		=> $cust_id,
				'non_scrap'		=> $non_scrap,
				'qty'			=> $qty
			);
			
			if($non_scrap == 'true') {
				$dataUpdateMaterial = array(
					'id'	=> $id_material,
					'qty'	=> ($dataMaterial[0]['qty'] + $qty)
				);

				$resultMaterial = $this->barang_return_model->update_material($dataUpdateMaterial);
				if($resultMaterial > 0) {
					$dataRtrn = array(
						'id_retur'	=> $id_rtrn,
						'qty'		=> $qty,
						'status'	=> 1
					);

					$dataMutasi = array(
						'id_stock_awal'		=> $id_material,
						'id_stock_akhir'	=> $id_material,
						'amount_mutasi'		=> $qty,
						'no_bc'				=> $id_bc,
						'type_mutasi'		=> 0,
						'date_mutasi'		=> date('Y-m-d H:i:s', strtotime(date('Y-m-d'). ' ' .date('H:i:s')))
					);

					$resultMutasi = $this->mutasi_model->save_mutasi($dataMutasi);
					$resultStatus = $this->barang_return_model->retur_update_status($dataRtrn);
					if($resultMutasi > 0 && $resultStatus > 0) $resultRetur = 1;
					else $resultRetur = 0;
				}else $resultRetur = 0;
			}else {
				$type_scrap = $this->Anti_sql_injection($this->input->post('type_scrap', TRUE));

				if($type_scrap == 'new') {
					$getIdOrder	= $this->barang_return_model->getIdOrder($id_po_quot);

					$dataNewMaterial = array(
						'no_bc'					=> NULL,
						'stock_code'			=> 'RTRN_'.$dataMaterial[0]['stock_code'],
						'stock_name'			=> $dataMaterial[0]['stock_name'],
						'stock_description'		=> $dataMaterial[0]['stock_description'],
						'unit'					=> $dataMaterial[0]['unit'],
						'type'					=> 5,
						'qty'					=> $qty,
						'weight'				=> $dataMaterial[0]['weight'],
						'treshold'				=> $dataMaterial[0]['treshold'],
						'id_properties'			=> $dataMaterial[0]['id_properties'],
						'id_gudang'				=> $dataMaterial[0]['id_gudang'],
						'status'				=> 8,
						'base_price'			=> $dataMaterial[0]['base_price'],
						'base_qty'				=> $qty,
						'cust_id'				=> $cust_id
					);

					$resultAdd = $this->barang_return_model->add_material($dataNewMaterial);
					if($resultAdd['result'] > 0) {
						$dataRtrn = array(
							'id_retur'			=> $id_rtrn,
							'id_order'			=> $getIdOrder[0]['id_order'],
							'id_produk'			=> $id_material,
							'id_produk_retur'	=> $resultAdd['lastid'],
							'id_bc'				=> $id_bc,
							'qty'				=> $qty,
							'status'			=> 1
						);

						$dataMutasi = array(
							'id_stock_awal'		=> $resultAdd['lastid'],
							'id_stock_akhir'	=> $resultAdd['lastid'],
							'amount_mutasi'		=> $qty,
							'no_bc'				=> $id_bc,
							'type_mutasi'		=> 0,
							'date_mutasi'		=> date('Y-m-d H:i:s', strtotime(date('Y-m-d'). ' ' .date('H:i:s')))
						);

						$resultMutasi 		= $this->mutasi_model->save_mutasi($dataMutasi);
						$resultReturUpdate 	= $this->barang_return_model->retur_update($dataRtrn);
						$resultReturStatus 	= $this->barang_return_model->retur_update_status($dataRtrn);

						if($resultReturUpdate > 0 && $resultReturStatus > 0 && $resultMutasi > 0) $resultRetur = 1;
						else $resultRetur = 0;
					}else $resultRetur = 0;
				}else {
					$dataUpdateMaterial = array(
						'id'	=> $id_material,
						'qty'	=> ($dataMaterial[0]['qty'] + $qty)
					);

					$resultMaterial = $this->barang_return_model->update_material($dataUpdateMaterial);
					if($resultMaterial > 0) {
						$dataRtrn = array(
							'id_retur'	=> $id_rtrn,
							'qty'		=> $qty,
							'status'	=> 1
						);

						$dataMutasi = array(
							'id_stock_awal'		=> $id_material,
							'id_stock_akhir'	=> $id_material,
							'amount_mutasi'		=> $qty,
							'no_bc'				=> $id_bc,
							'type_mutasi'		=> 0,
							'date_mutasi'		=> date('Y-m-d H:i:s', strtotime(date('Y-m-d'). ' ' .date('H:i:s')))
						);

						$resultMutasi 		= $this->mutasi_model->save_mutasi($dataMutasi);
						$resultStatus = $this->barang_return_model->retur_update_status($dataRtrn);

						if($resultMutasi > 0 && $resultStatus > 0) $resultRetur = 1;
						else $resultRetur = 0;
					}else $resultRetur = 0;
				}
			}

			if($resultRetur > 0) {
				$this->log_activity->insert_activity('update', 'Update Barang Return');
				$results = array('success' => true, 'message' => 'Berhasil mengubah Barang Return ke database.');
			}else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert Barang Return');
				$results = array('success' => false, 'message' => 'Gagal mengubah Barang Return ke database.');
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($results));
	}

	public function delete_rtrn() {
		$data			= file_get_contents("php://input");
		$params			= json_decode($data,true);
		$getDataRtrn	= $this->barang_return_model->getDataRtrn($params['id_rtrn']);

		if(sizeof($getDataRtrn) > 0) {
			$deleteMaterial = $this->barang_return_model->deleteMaterial($getDataRtrn[0]['id_produk_retur']);
			$deleteRetur	= $this->barang_return_model->deleteRetur($params['id_rtrn']);

			if($deleteMaterial > 0 && $deleteRetur > 0) {
				$this->log_activity->insert_activity('delete', 'Delete Barang Return id : '.$getDataRtrn[0]['id_retur']);
				$res = array('status' => true, 'message' => 'Data telah di hapus');
			}else {
				$this->log_activity->insert_activity('delete', 'Gagal Delete Barang Return id : '.$getDataRtrn[0]['id_retur']);
				$res = array('status'	=> 'failed', 'message' => 'Data gagal di hapus');
			}
		}else {
			$this->log_activity->insert_activity('delete', 'Gagal Delete Barang Return id : '.$getDataRtrn[0]['id_retur']);
			$res = array('status' => 'failed', 'message' => 'Data gagal di hapus');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}
}