<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Barang_Return_model extends CI_Model {

	public function __construct() {
		parent::__construct();
	}

	public function lists($params = array()) {
		$sql 	= 'CALL retur_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function get_po_quot() {
		$sql	= 'SELECT id, order_no FROM t_po_quotation WHERE order_no IS NOT NULL';

		$query	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function getBC() {
		$sql 	= 'SELECT a.id, a.no_pendaftaran, a.no_pengajuan FROM t_bc a LEFT JOIN m_type_bc b ON b.id = a.jenis_bc WHERE b.type_bc = 0';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function getOrder($id) {
		$sql = 'SELECT b.id, b.stock_code FROM t_order a LEFT JOIN m_material b ON a.id_produk = b.id WHERE a.id_po_quotation = \''.$id.'\'';
		//$sql = 'SELECT * FROM m_material WHERE id = \''.$id.'\'';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function getDetailMaterial($id) {
		$sql = 'SELECT * FROM m_material WHERE id = \''.$id.'\'';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function getIdOrder($id_quot) {
		$sql = 'SELECT a.*, b.cust_id FROM t_order  a LEFT JOIN t_po_quotation b ON a.id_po_quotation=b.id LEFT JOIN t_eksternal c ON b.cust_id=c.id WHERE a.id_po_quotation = \''.$id_quot.'\'';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_material($data) {
		$sql 	= 'CALL material_add(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)';

		$query 	=  $this->db->query($sql, array(
				$data['no_bc'],
				$data['stock_code'],
				$data['stock_name'],
				$data['stock_description'],
				$data['unit'],
				$data['type'],
				$data['qty'],
				$data['weight'],
				$data['treshold'],
				$data['id_properties'],
				$data['id_gudang'],
				$data['status'],
				$data['base_price'],
				$data['base_qty'],
				$data['cust_id']
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		
		return $arr_result;
	}

	public function add_retur($data) {
		$sql 	= 'CALL retur_add(?,?,?,?,?)';

		$query 	=  $this->db->query($sql, array(
				$data['id_order'],
				$data['id_produk'],
				$data['id_produk_retur'],
				$data['id_bc'],
				$data['qty']
		));
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}

	public function getBrgRetur($id) {
		$sql = 'CALL retur_search_id(?)';

		$query 	= $this->db->query($sql, array(
			$id
		));

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function getDataRtrn($id) {
		$sql = 'SELECT * FROM t_retur WHERE id_retur = \''.$id.'\'';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function getDataQuot($id) {
		$sql = 'SELECT a.* FROM t_po_quotation a LEFT JOIN t_order b ON b.id_po_quotation = a.id WHERE b.id_order = \''.$id.'\'';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function update_material($data) {
		$sql = 'UPDATE m_material SET qty = '.$data['qty'].' WHERE  id = '.$data['id'];

		$query 	= $this->db->query($sql);
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}

	public function retur_update_status($data) {
		$sql = 'CALL retur_update_status(?,?,?)';

		$query 	= $this->db->query($sql, array(
			$data['id_retur'],
			$data['qty'],
			$data['status']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}

	public function retur_update($data) {
		$sql = 'CALL retur_update(?,?,?,?,?,?)';

		$query 	= $this->db->query($sql, array(
			$data['id_retur'],
			$data['id_order'],
			$data['id_produk'],
			$data['id_produk_retur'],
			$data['id_bc'],
			$data['qty']
		));
		
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		
		return $result;
	}

	public function deleteMaterial($id_material) {
		$sql = 'DELETE FROM m_material WHERE id = \''.$id_material.'\'';

		$query 	= $this->db->query($sql);
		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function deleteRetur($id_retur) {
		$sql = 'DELETE FROM t_retur WHERE id_retur = \''.$id_retur.'\'';

		$query 	= $this->db->query($sql);
		$return = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
}