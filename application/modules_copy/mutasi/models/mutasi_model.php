<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Mutasi_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
  public function add_coa_values($data)
	{
		$sql 	= 'CALL coavalue_add(?,?,?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_coa'],
			$data['id_parent'],
			$data['id_valas'],
			$data['value'],
			$data['adjusment'],
			$data['type_cash'],
			$data['note'],
			$data['rate'],
			''
		));
		
		$return['result'] = $query->result_array();
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$return['lastid'] = $lastid;

		$this->db->close();
		$this->db->initialize();

		return $return;
  }
  public function add_kartu_hp($data)
	{
		$sql 	= 'CALL hp_add(?,?,?,?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['ref'],
			$data['source'],
			$data['keterangan'],
			$data['status'],
			$data['saldo'],
			$data['saldo_akhir'],
			$data['id_valas'],
			$data['type_kartu'],
			$data['id_master'],
			$data['type_master']
		));
		
		$return['code'] = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	public function type_bc()
	{
		$sql 	= 'SELECT * FROM m_type_bc WHERE type_bc = 0;';

		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function lists($params = array())
	{
		$sql 	= 'CALL mutasi_list_new(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query(
			$sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			)
		);

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}

	public function typeMaterial()
	{
		//$sql 	= 'SELECT * FROM m_type_material WHERE id_type_material BETWEEN 1 AND 3';
		$sql 	= 'SELECT * FROM m_type_material WHERE id_type_material ';
		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function listbc($jenis_mutasi)
	{
		$sql 	= 'SELECT a.* FROM t_bc a
		LEFT JOIN m_type_bc b ON b.`id` = a.`jenis_bc`
		WHERE b.`type_bc` =?';
		$query 	= $this->db->query(
			$sql,
			array($jenis_mutasi)
		);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function getlistMaterial($id)
	{
		$sql 	= 'SELECT id, stock_name, stock_code FROM m_material WHERE type=? AND STATUS = 1';
		$query 	= $this->db->query(
			$sql,
			array($id)
		);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function listMaterial()
	{
		$sql 	= 'SELECT * FROM m_material WHERE STATUS = 1';
		$query 	= $this->db->query($sql);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function gudang()
	{
		$sql_all 	= 'CALL gudang_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query(
			$sql_all,
			array(
				1000,
				0,
				NULL,
				NULL,
				''
			)
		);
		$result = $query_all->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function existing_price($id)
	{
		$sql 	= 'SELECT * FROM d_price WHERE id_master=?';
		$query 	= $this->db->query(
			$sql,
			array($id)
		);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_new_material($data)
	{
		$sql 	= 'CALL material_add(?,?,?,?,?,?,?,?,?,?,?)';
		$query 	=  $this->db->query(
			$sql,
			array(
				$data['no_bc'],
				$data['stock_code'],
				$data['stock_name'],
				$data['stock_description'],
				$data['unit'],
				$data['type'],
				$data['qty'],
				$data['treshold'],
				$data['id_properties'],
				$data['id_gudang'],
				$data['status']
			)
		);
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	public function mp_save($data)
	{
		$sql 	= 'CALL mp_add(?,?)';
		$query 	=  $this->db->query(
			$sql,
			array(
				$data['id_material'],
				$data['amount_mutasi'],
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['result'] = $result;

		return $arr_result;
	}
	public function save_mutasi($data)
	{
		// $this->save_edit_material($data);
		$sql 	= 'CALL mutasi_add(?,?,?,?,?)';
		$query 	=  $this->db->query(
			$sql,
			array(
				$data['id_stock_awal'],
				$data['id_stock_akhir'],
				$data['date_mutasi'],
				$data['amount_mutasi'],
				$data['type_mutasi']
			)
		);

		$result	= $this->db->affected_rows();


		$arr_result['result'] = $result;
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		$data['id_mutasi'] = $lastid;
		$this->mutasi_bc_save($data);
		return $arr_result;
	}
	
	public function approve_mutasi($data)
	{
		// $this->save_edit_material($data);
		$sql 	= 'CALL mutasi_approve(?,?,?,?)';
		// IN `pin_id_mutasi` INT(11),
		// IN `pin_id_mutasi_bc` INT(11),
		// IN `pin_id_material` INT(11),
		// IN `pin_amount_mutasi` DECIMAL(20,4)
		$query 	=  $this->db->query(
			$sql,
			array(
				$data['id_mutasi'],
				$data['id_mutasi_bc'],
				$data['id_material'],
				$data['amount_mutasi'],
			)
		);

		$result	= $this->db->affected_rows();
		$arr_result['result'] = $result;
		
		return $arr_result;
	}
	
	public function update_mutasi_masuk($data)
	{
		$sql 	= 'UPDATE t_mutasi SET type_mutasi = 0 WHERE type_mutasi = 10 AND id_mutasi = ?';
		$query 	=  $this->db->query(
			$sql,
			array(
				$data['id_mutasi']
			)
		);

		$result	= $this->db->affected_rows();
		$arr_result['result'] = $result;
		
		return $arr_result;
	}
	
	public function update_mutasi_keluar($data)
	{
		$sql 	= 'UPDATE t_mutasi SET type_mutasi = 1 WHERE type_mutasi = 11 AND id_mutasi = ?';
		$query 	=  $this->db->query(
			$sql,
			array(
				$data['id_mutasi']
			)
		);

		$result	= $this->db->affected_rows();
		$arr_result['result'] = $result;
		
		return $arr_result;
	}
	
	public function update_material_qty($data)
	{

		if ($data['type_mutasi'] == 0) {

			$sql 	= 'CALL update_qty_material(?,?)';
			$query 	=  $this->db->query(
				$sql,
				array(
					$data['id_stock_akhir'],
					$data['amount_mutasi'],
				)
			);
		} else {

			$sql 	= 'CALL update_qty_material_min(?,?)';
			$query 	=  $this->db->query(
				$sql,
				array(
					$data['id_stock_akhir'],
					$data['amount_mutasi'],
				)
			);
		}

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function mutasi_bc_save($data)
	{
		$sql 	= 'CALL mutasi_bc_add(?,?)';
		$query 	=  $this->db->query(
			$sql,
			array(
				$data['id_mutasi'],
				$data['no_bc'],
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['result'] = $result;

		return $arr_result;
	}
	public function save_add_price($data)
	{
		$sql 	= 'CALL price_add(?,?,?,?,?)';
		$query 	=  $this->db->query(
			$sql,
			array(
				$data['id_komponen'],
				$data['price'],
				$data['id_valas'],
				$data['id_master'],
				$data['start_price']
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['result'] = $result;

		return $arr_result;
	}
	public function delete_mutasi($data)
	{
		$sql 	= 'CALL mutasi_delete_new(?)';
		$query 	=  $this->db->query(
			$sql,
			array(
				$data['id'],
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['result'] = $result;

		return $arr_result;
	}
	public function edit($id)
	{
		$sql 	= 'CALL material_search_id(?)';

		$out = array();
		$query 	=  $this->db->query(
			$sql,
			array(
				$id
			)
		);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function existing_mutasi($id)
	{
		$sql 	= 'SELECT * FROM t_mutasi WHERE id_stock_akhir=?';
		$query 	= $this->db->query(
			$sql,
			array($id)
		);
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function save_edit_material($data)
	{
		$sql 	= 'UPDATE m_material SET qty=?  WHERE id=?;';
		$result 	= $this->db->query($sql, array(
			$data['qty'],
			$data['id_material']
		));

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function save_edit_mutasi($data)
	{
		$sql 	= 'CALL mutasi_update(?,?,?,?)';
		$query 	=  $this->db->query(
			$sql,
			array(
				$data['id_mutasi'],
				$data['id_stock_awal'],
				$data['id_stock_akhir'],
				$data['amount_mutasi']
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function save_edit_mutasi_new($data)
	{
		$sql 	= 'CALL mutasi_update_new(?,?,?,?,?,?,?)';
		// IN `pin_id_mutasi` INT(11),
		// IN `pin_id_mutasi_bc` INT(11),
		// IN `pin_amount_mutasi` DECIMAL(20,10),
		// IN `pin_type_mutasi` INT(11),
		// IN `pin_id_bc` INT(11),
		// IN `pin_date_mutasi` DATETIME,
		// IN `pin_id_material` INT(11)
		if($data['id_mutasi_bc']==null) $data['id_mutasi_bc']=0;
		$query 	=  $this->db->query(
			$sql,
			array(
				$data['id_mutasi'],
				$data['id_mutasi_bc'],
				$data['amount_mutasi'],
				$data['type_mutasi'],
				$data['id_bc'],
				$data['date_mutasi'],
				$data['id_material']
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function save_edit_price($data)
	{
		$sql 	= 'CALL price_update(?,?,?,?,?,?)';
		$query 	=  $this->db->query(
			$sql,
			array(
				$data['id'],
				$data['id_komponen'],
				$data['price'],
				$data['id_valas'],
				$data['id_master'],
				$data['start_price']
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function get_mutasi($id)
	{
		$sql 	= 'CALL mutasi_search_id_new(?)';

		$out = array();
		$query 	=  $this->db->query(
			$sql,
			array(
				$id
			)
		);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}
