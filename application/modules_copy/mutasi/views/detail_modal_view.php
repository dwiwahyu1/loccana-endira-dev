<!-- <script type="text/javascript" src="<?php echo base_url('assets'); ?>/gentelella-master/vendors/parsleyjs/dist/parsley.min.js"></script> -->

<style>
	#loading-us {
		display: none
	}

	#tick {
		display: none
	}

	#loading-mail {
		display: none
	}

	#cross {
		display: none
	}
</style>
<?php
	/*echo "<pre>";
	print_r($type);
	print_r($mutasi);
	print_r($gudang);
	echo "</pre>";*/
?>
<div class="row">
	<div class="col-md-12" style="margin-bottom: 10px;">
		<div class="row">
			<label class="col-sm-4" for="type_material">No BC</label>
			<div class="col-md-8">
				<input type="text" class="form-control" id="type_material" name="type_material" value="<?php
					if(isset($mutasi[0]['no_pendaftaran'])) echo $mutasi[0]['no_pendaftaran'];
					else echo "Tidak Ada BC!";
				?>" disabled readonly>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12" style="margin-bottom: 10px;">
		<div class="row">
			<label class="col-sm-4" for="type_material">Tanggal Mutasi</label>
			<div class="col-md-8">
				<input type="text" class="form-control" id="type_material" name="type_material" value="<?php
					if(isset($mutasi[0]['date_mutasi'])) echo date('d-M-Y', strtotime($mutasi[0]['date_mutasi']));
					else echo "Tidak Ada BC!";
				?>" disabled readonly>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12" style="margin-bottom: 10px;">
		<div class="row">
			<label class="col-sm-4" for="type_material">Kode Stok</label>
			<div class="col-md-8">
				<input type="text" class="form-control" id="type_material" name="type_material" value="<?php
					if(isset($mutasi[0]['stock_code'])) echo $mutasi[0]['stock_code'];
				?>" disabled readonly>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12" style="margin-bottom: 10px;">
		<div class="row">
			<label class="col-sm-4" for="type_material">Nama Stok</label>
			<div class="col-md-8">
				<input type="text" class="form-control" id="type_material" name="type_material" value="<?php
					if(isset($mutasi[0]['stock_name'])) echo $mutasi[0]['stock_name'];
				?>" disabled readonly>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12" style="margin-bottom: 10px;">
		<div class="row">
			<label class="col-sm-4" for="type_material">Unit</label>
			<div class="col-md-8">
				<input type="text" class="form-control" id="type_material" name="type_material" value="<?php
					if(isset($mutasi[0]['uom_name'])) echo $mutasi[0]['uom_name'];
				?>" disabled readonly>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12" style="margin-bottom: 10px;">
		<div class="row">
			<label class="col-sm-4" for="type_material">Type</label>
			<div class="col-md-8">
				<input type="text" class="form-control" id="type_material" name="type_material" value="<?php
					if(isset($mutasi[0]['type_material_name'])) echo $mutasi[0]['type_material_name'];
				?>" disabled readonly>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12" style="margin-bottom: 10px;">
		<div class="row">
			<label class="col-sm-4" for="type_material">Amount Mutasi</label>
			<div class="col-md-8">
				<input type="text" class="form-control" id="type_material" name="type_material" value="<?php
					if(isset($mutasi[0]['amount_mutasi'])) echo number_format($mutasi[0]['amount_mutasi'], 4, ',', '.');
				?>" disabled readonly>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12" style="margin-bottom: 10px;">
		<div class="row">
			<label class="col-sm-4" for="type_material">Gudang</label>
			<div class="col-md-8">
				<input type="text" class="form-control" id="type_material" name="type_material" value="<?php
					if(isset($mutasi[0]['nama_gudang'])) echo $mutasi[0]['nama_gudang'];
				?>" disabled readonly>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12" style="margin-bottom: 10px;">
		<div class="row">
			<label class="col-sm-4" for="type_material">Type Mutasi</label>
			<div class="col-md-8">
				<input type="text" class="form-control" id="type_material" name="type_material" value="<?php
					if(isset($mutasi[0]['type_mutasi'])) {
						if($mutasi[0]['type_mutasi'] == 0) echo "Masuk";
						else echo "Keluar";
					}
				?>" disabled readonly>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var selectMaterial;
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();
		$('#select_material').select2();
		$('#no_bc').select2();
		$('#type_material').on('change', function(event) {
			var id_type = $(this).val();
			if (id_type != '') {
				var _this = $(this);
				$('select#select_material').empty();
				var strHtml = '';

				$.ajax({
					type: "POST",
					url: '<?php echo base_url(); ?>mutasi/get_list_material',
					data: JSON.stringify({
						'id_type': id_type
					}),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(r) {
						strHtml += '<option value="">-- Select Material --</option>';
						for (var i = 0; i < r.data.length; i++) {
							strHtml += "<option value='" + r.data[i].id + "'>" + r.data[i].stock_code + " | " + r.data[i].stock_name + "</option>"
						}
						// console.log(strHtml,r.data.length,r);
						$('select#select_material').append(strHtml);
						$('select#select_material').select2();
					},
					fail: function() {
						strHtml = '<option value="">Please try again</option>';
						$('select#select_material').append(strHtml);
					}
				});
			};
		});
		$('#type_mutasi').on('change', function(event) {
			var type_mutasi = $(this).val();
			if (type_mutasi != '') {
				var _this = $(this);
				$('select#no_bc').empty();
				var strHtml = '';

				$.ajax({
					type: "POST",
					url: '<?php echo base_url(); ?>mutasi/get_bc',
					data: JSON.stringify({
						'type_mutasi': type_mutasi
					}),
					dataType: 'json',
					contentType: 'application/json; charset=utf-8',
					success: function(r) {
						strHtml += '<option value="">-- Tidak Ada BC --</option>';
						for (var i = 0; i < r.data.length; i++) {
							strHtml += "<option value='" + r.data[i].id + "'>" + r.data[i].no_pendaftaran + " | " + r.data[i].no_pengajuan + "</option>";
						}
						// console.log(strHtml,r.data.length,r);
						$('select#no_bc').append(strHtml);
						$('select#no_bc').select2();
					},
					fail: function() {
						strHtml = '<option value="">Please try again</option>';
						$('select#no_bc').append(strHtml);
					}
				});
			};
		});
		$('#file_mutasi').bind('change', function() {
			if (this.files[0].size >= 9437184) {
				$('#file_mutasi').css('border', '3px #C33 solid');
				$('#file_mutasi_tick').empty();
				$("#file_mutasi_tick").append('<span class="fa fa-close"> Ukuran File Lebih dari 9Mb</span>');
				$('#file_mutasi_tick').show();
			} else {
				$('#file_mutasi').removeAttr("style");
				$('#file_mutasi_tick').empty();
				$("#file_mutasi_tick").append('Hanya format file pdf,txt,doc,docx dengan besaran max 10Mb yang diterima.');
				$('#file_mutasi_tick').show();
			}
		});

		$('#btn_add_bc').on('click', function() {
			$('#panel-modal-child').removeData('bs.modal');
			$('#panel-modal-child  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
			$('#panel-modal-child  .panel-body').load('<?php echo base_url('mutasi/add_bc'); ?>');
			$('#panel-modal-child  .panel-title').html('<i class="fa fa-dollar"></i> Tambah BC');
			$('#panel-modal-child').modal({
				backdrop: 'static',
				keyboard: false
			}, 'show');
		});
	});

	function getMaterial() {
		var id_select = $('#select_material').val();
		if (id_select != '') {
			var datapost = {
				"id_select": id_select
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url(); ?>mutasi/get_material",
				data: JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(r) {
					setValue(r.data);
				}
			});
		} else resetValue();
	}

	function cal_stock() {
		var stockAwal = parseInt($('#stock_awal').val());
		var stockPindah = parseInt($('#stock_pindah').val());
		var type_mutasi = parseInt($('#type_mutasi').val());
		if(type_mutasi == 1)
			var calStock = stockAwal - stockPindah;
		else
			var calStock = stockAwal + stockPindah;
		$('#sisa_stock').html('Sisa Stock : ' + calStock);
		$('#stock_akhir').val(calStock);
	}

	function checkStock() {
		var stockAwal = parseInt($('#stock_awal').val());
		var stockPindah = parseInt($('#stock_pindah').val());

		console.log(stockPindah, stockAwal);
		if (stockPindah > 0) {
			if (stockPindah > stockAwal) return false;
			else return true;
		} else return false;
	}


	$('#mutasi_form').on('submit', (function(e) {
		$('#btn-submit').attr('disabled', 'disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		var validateStock = checkStock();

		if (validateStock == true) {
			var formData = new FormData(this);
			save_Form(formData, $(this).attr('action'));
		} else if (validateStock == false) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Mutasi Material");
			swal("Failed!", "Invalid masukan stock salah, silahkan cek kembali.", "error");
		}
	}));

	function save_Form(formData, url) {
		$.ajax({
			type: 'POST',
			url: url,
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function() {
						window.location.href = "<?php echo base_url('mutasi'); ?>";
					})
				} else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Mutasi Material");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Mutasi Material");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}

	function setValue(data) {
		$("select#bc_pindah option").filter(function() {
			return $(this).text() == data.no_bc;
		}).prop('selected', true);

		$('#lb_kode_stok').html(data.stock_code);
		$('#lb_nama_stok').html(data.stock_name);
		$('#lb_unit').html(data.uom_name);
		$('#lb_type').html(data.type_material_name);
		$('#lb_valas').html(data.nama_valas);
		$('#lb_harga').html(data.price);
		$('#lb_stok').html(data.qty);
		$('#lb_gudang').html(data.nama_gudang);

		$('#id_material').val(data.id);
		$('#stock_awal').val(data.qty);
		$('#gudang_awal').val(data.id_gudang);
		$('#stock_pindah').attr({
			"max": data.qty
		});
		$('#gudang_pindah').val(data.id_gudang);
	}

	function resetValue() {
		$('#lb_kode_bc').html('');
		$('#lb_kode_stok').html('');
		$('#lb_nama_stok').html('');
		$('#lb_unit').html('');
		$('#lb_type').html('');
		$('#lb_valas').html('');
		$('#lb_harga').html('');
		$('#lb_stok').html('');
		$('#lb_gudang').html('');

		$('#id_material').val('');
		$('#stock_awal').val('');
		$('#stock_akhir').val('');
		$('#gudang_awal').val('');
		$('#stock_pindah').attr({
			"max": 0
		});
		$('#sisa_stock').html('');
	}
</script>