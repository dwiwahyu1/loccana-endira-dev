<!-- <script type="text/javascript" src="<?php echo base_url('assets'); ?>/gentelella-master/vendors/parsleyjs/dist/parsley.min.js"></script> -->

<style>
	#loading-us {
		display: none
	}

	#tick {
		display: none
	}

	#loading-mail {
		display: none
	}

	#cross {
		display: none
	}
</style>
<?php
/*echo "<pre>";
	print_r($type);
	print_r($mutasi);
	print_r($gudang);
	echo "</pre>";*/
?>

<form class="form-horizontal " id="mutasi_form" role="form" action="<?php echo base_url('mutasi/save_approve_mutasi'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<div class="row">
		<div class="col-md-12" style="margin-bottom: 10px;">
			<label class="col-sm-4" for="type_material">No BC</label>
			<div class="col-md-8">
				<input type="text" class="form-control" id="no_pendaftaran" name="no_pendaftaran" value="<?php
																																																	if (isset($mutasi[0]['no_pendaftaran'])) echo $mutasi[0]['no_pendaftaran'];
																																																	else echo "Tidak Ada BC!";
																																																	?>"  readonly>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12" style="margin-bottom: 10px;">
			<label class="col-sm-4" for="type_material">Tanggal Mutasi</label>
			<div class="col-md-8">
				<input type="text" class="form-control" id="date_mutasi" name="date_mutasi" value="<?php
																																														if (isset($mutasi[0]['date_mutasi'])) echo date('d-M-Y', strtotime($mutasi[0]['date_mutasi']));
																																														else echo "Tidak Ada BC!";
																																														?>"  readonly>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12" style="margin-bottom: 10px;">
			<label class="col-sm-4" for="type_material">Kode Stok</label>
			<div class="col-md-8">
				<input type="text" class="form-control" id="stock_code" name="stock_code" value="<?php
																																													if (isset($mutasi[0]['stock_code'])) echo $mutasi[0]['stock_code'];
																																													?>"  readonly>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12" style="margin-bottom: 10px;">
			<label class="col-sm-4" for="type_material">Nama Stok</label>
			<div class="col-md-8">
				<input type="text" class="form-control" id="stock_name" name="stock_name" value="<?php
																																													if (isset($mutasi[0]['stock_name'])) echo $mutasi[0]['stock_name'];
																																													?>"  readonly>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12" style="margin-bottom: 10px;">
			<label class="col-sm-4" for="type_material">Unit</label>
			<div class="col-md-8">
				<input type="text" class="form-control" id="uom_name" name="uom_name" value="<?php
																																											if (isset($mutasi[0]['uom_name'])) echo $mutasi[0]['uom_name'];
																																											?>"  readonly>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12" style="margin-bottom: 10px;">
			<label class="col-sm-4" for="type_material">Type</label>
			<div class="col-md-8">
				<input type="text" class="form-control" id="type_material_name" name="type_material_name" value="<?php
																																																					if (isset($mutasi[0]['type_material_name'])) echo $mutasi[0]['type_material_name'];
																																																					?>"  readonly>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12" style="margin-bottom: 10px;">
			<label class="col-sm-4" for="type_material">Amount Mutasi</label>
			<div class="col-md-8">
				<input type="text" class="form-control" id="amount_mutasi" name="amount_mutasi" value="<?php
																																																if (isset($mutasi[0]['amount_mutasi'])) echo number_format($mutasi[0]['amount_mutasi'], 4, '.', '');
																																																?>"  readonly>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12" style="margin-bottom: 10px;">
			<label class="col-sm-4" for="type_material">Gudang</label>
			<div class="col-md-8">
				<input type="text" class="form-control" id="nama_gudang" name="nama_gudang" value="<?php
																																														if (isset($mutasi[0]['nama_gudang'])) echo $mutasi[0]['nama_gudang'];
																																														?>"  readonly>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12" style="margin-bottom: 10px;">
			<label class="col-sm-4" for="type_material">Type Mutasi</label>
			<div class="col-md-8">
				<input type="text" class="form-control" id="type_mutasi_text" name="type_mutasi_text" value="<?php
																																																			if (isset($mutasi[0]['type_mutasi'])) {
																																																				if ($mutasi[0]['type_mutasi'] == 0) echo "Masuk";
																																																				else echo "Keluar";
																																																			}
																																																			?>" disabled readonly>
			</div>
		</div>
	</div>
	<input type="hidden" id="type_mutasi" name="type_mutasi" value="<?php echo $mutasi[0]['type_mutasi']; ?>">
	<input type="hidden" id="id_stock_akhir" name="id_stock_akhir" value="<?php echo $mutasi[0]['id_stock_akhir']; ?>">
	<input type="hidden" id="id_mutasi" name="id_mutasi" value="<?php echo $mutasi[0]['id_mutasi']; ?>">
	<input type="hidden" id="id_mutasi_bc" name="id_mutasi_bc" value="<?php echo $mutasi[0]['id_mutasi_bc']; ?>">
	<input type="hidden" id="id_material" name="id_material" value="<?php echo $mutasi[0]['id_material']; ?>">
	<input type="hidden" id="base_price" name="base_price" value="<?php echo $mutasi[0]['base_price']; ?>">
	<input type="hidden" id="id_valas" name="id_valas" value="<?php echo $mutasi[0]['id_valas']; ?>">
	<input type="hidden" id="id_coa" name="id_coa" value="<?php echo $mutasi[0]['id_coa']; ?>">
	<input type="hidden" id="rate" name="rate" value="<?php echo $mutasi[0]['rate']; ?>">
	<div class="row" style="margin-top: 10px;">
		<div class="col-md-12">
			<div class="item form-group">
				<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
				<div class="col-md-8 col-sm-6 col-xs-12">
					<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Approve Mutasi Material</button>
				</div>
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
	var selectMaterial;
	$(document).ready(function() {
		$('form').parsley();
		$('[data-toggle="tooltip"]').tooltip();

	});


	$('#mutasi_form').on('submit', (function(e) {
		$('#btn-submit').attr('disabled', 'disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();
		// var validateStock = checkStock();
		var validateStock = true;
		if (validateStock == true) {
			var formData = new FormData(this);
			console.log(formData);
			save_Form(formData, $(this).attr('action'));
		} else if (validateStock == false) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Mutasi Material");
			swal("Failed!", "Invalid masukan stock salah, silahkan cek kembali.", "error");
		}
	}));

	function save_Form(formData, url) {
		$.ajax({
			type: 'POST',
			url: url,
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			success: function(response) {
				if (response.success == true) {
					swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function() {
						window.location.href = "<?php echo base_url('mutasi'); ?>";
					})
				} else {
					$('#btn-submit').removeAttr('disabled');
					$('#btn-submit').text("Mutasi Material");
					swal("Failed!", response.message, "error");
				}
			}
		}).fail(function(xhr, status, message) {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Mutasi Material");
			swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
		});
	}

	function setValue(data) {
		$("select#bc_pindah option").filter(function() {
			return $(this).text() == data.no_bc;
		}).prop('selected', true);

		$('#lb_kode_stok').html(data.stock_code);
		$('#lb_nama_stok').html(data.stock_name);
		$('#lb_unit').html(data.uom_name);
		$('#lb_type').html(data.type_material_name);
		$('#lb_valas').html(data.nama_valas);
		$('#lb_harga').html(data.price);
		$('#lb_stok').html(data.qty);
		$('#lb_gudang').html(data.nama_gudang);

		$('#id_material').val(data.id);
		$('#stock_awal').val(data.qty);
		$('#gudang_awal').val(data.id_gudang);
		$('#stock_pindah').attr({
			"max": data.qty
		});
		$('#gudang_pindah').val(data.id_gudang);
	}

	function resetValue() {
		$('#lb_kode_bc').html('');
		$('#lb_kode_stok').html('');
		$('#lb_nama_stok').html('');
		$('#lb_unit').html('');
		$('#lb_type').html('');
		$('#lb_valas').html('');
		$('#lb_harga').html('');
		$('#lb_stok').html('');
		$('#lb_gudang').html('');

		$('#id_material').val('');
		$('#stock_awal').val('');
		$('#stock_akhir').val('');
		$('#gudang_awal').val('');
		$('#stock_pindah').attr({
			"max": 0
		});
		$('#sisa_stock').html('');
	}
</script>