<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Mutasi extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('mutasi/mutasi_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string)
	{
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index()
	{
		$this->template->load('maintemplate', 'mutasi/views/index');
	}

	public function lists()
	{
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'DESC ';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;
		$order_fields = array('', 'date_mutasi', 'no_bc', 'stock_code', 'stock_name', 'stock_description', 'unit', 'type', 'qty', 'treshold');

		$search = $this->input->get_post('search');
		$search_val = (!empty($search['value'])) ? $search['value'] : null;
		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;
		$list = $this->mutasi_model->lists($params);

		/*echo "<pre>";
		print_r($list);
		echo "</pre>";
		die;*/

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = $params['offset'];
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$strOption =
				'<div class="btn-group">' .
				'<button class="btn btn-info btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Details"
						onClick="detail_mutasi(\'' . $v['id_mutasi'] . '\')">' .
				'<i class="fa fa-search"></i>' .
				'</button>' .
				'</div>';
				if($v['status']==null){
					$strOption = $strOption .
					'<div class="btn-group">' .
					'<button class="btn btn-warning btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
						onClick="edit_mutasi(\'' . $v['id_mutasi'] . '\')">' .
					'<i class="fa fa-edit"></i>' .
					'</button>' .
					'</div>'.
					'<div class="btn-group">' .
					'<button class="btn btn-danger btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Delete"
						onClick="delete_mutasi(\'' . $v['id_mutasi'] . '\')">' .
					'<i class="fa fa-trash"></i>' .
					'</button>' .
					'</div>';
				}elseif ($v['status'] == 0) {
				$strOption = $strOption .
					'<div class="btn-group">' .
					'<button class="btn btn-success btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Approve"
						onClick="approve_mutasi(\'' . $v['id_mutasi'] . '\')">' .
					'<i class="fa fa-check"></i>' .
					'</button>' .
					'</div>' .
					'<div class="btn-group">' .
					'<button class="btn btn-warning btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
						onClick="edit_mutasi(\'' . $v['id_mutasi'] . '\')">' .
					'<i class="fa fa-edit"></i>' .
					'</button>' .
					'</div>'.
					'<div class="btn-group">' .
					'<button class="btn btn-danger btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Delete"
						onClick="delete_mutasi(\'' . $v['id_mutasi'] . '\')">' .
					'<i class="fa fa-trash"></i>' .
					'</button>' .
					'</div>';
			}
			if ($v['id_bc'] == 0) $v['no_pendaftaran'] = "Tidak Ada BC!";
			// if ($v['type_mutasi']==0) $v['type_mutasi'] = "Masuk"; else $v['type_mutasi'] = "Keluar";

			if ($v['type_mutasi'] == 0 || $v['type_mutasi'] == 10 ) {
				$debit		= number_format($v['amount_mutasi'], 2, ',', '.');
				$kredit		= number_format(0, 2, ',', '.');
			} else {
				$debit		= number_format(0, 2, ',', '.');
				$kredit		= number_format($v['amount_mutasi'], 2, ',', '.');
			}
			array_push($data, array(
				$i,
				$v['no_pendaftaran'],
				$v['stock_code'],
				$v['stock_name'],
				$v['uom_name'],
				$v['type_material_name'],
				$debit,
				$kredit,
				number_format($v['qty'], 2, ',', '.'),
				// $v['nama_valas'],
				// 'Rp. ' . number_format($v['price'], 2, ",", "."),
				// number_format($v['amount_mutasi'], 2, ',', '.'),
				// str_replace('.', ',', (string) $v['amount_mutasi']),
				/*$v['nama_gudang'],
				$v['type_mutasi']*/
				date('d-F-Y H:i:s', strtotime($v['date_mutasi'])),
				$strOption
			));
		}

		$result["data"] = $data;
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add()
	{
		$result_type = $this->mutasi_model->typeMaterial();
		// $result_list = $this->mutasi_model->listMaterial();
		$result_gudang = $this->mutasi_model->gudang();
		$data = array(
			'typeMaterial'	=> $result_type,
			// 'listMaterial'	=> $result_list,
			'gudang'		=> $result_gudang
		);

		$this->load->view('add_modal_view', $data);
	}

	public function add_bc()
	{
		$result_type = $this->mutasi_model->type_bc();
		$data = array(
			'jenis_bc' => $result_type
		);

		$this->load->view('add_bc_modal_view', $data);
	}

	public function get_list_material()
	{
		$data		= file_get_contents("php://input");
		$params		= json_decode($data, true);
		$result_list = $this->mutasi_model->getlistMaterial($params['id_type']);

		echo json_encode(['data' => $result_list]);
	}

	public function get_material()
	{
		$data		= file_get_contents("php://input");
		$params		= json_decode($data, true);
		$result = $this->mutasi_model->edit($params['id_select']);

		echo json_encode(['data' => $result[0]]);
	}

	public function get_bc()
	{
		$data		= file_get_contents("php://input");
		$params		= json_decode($data, true);
		$result = $this->mutasi_model->listbc($params['type_mutasi']);

		echo json_encode(['data' => $result]);
	}

	public function save_add_mutasi_new()
	{
		$this->form_validation->set_rules('tgl_pindah', 'Tanggal Pindah', 'trim|required');
		$this->form_validation->set_rules('stock_pindah', 'Stock Pindah', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			$id_material = $this->Anti_sql_injection($this->input->post('id_material', TRUE));
			$stock_awal = (float) $this->Anti_sql_injection($this->input->post('stock_awal', TRUE));
			$tgl_pindah = $this->Anti_sql_injection($this->input->post('tgl_pindah', TRUE));
			$stock_pindah = (float) $this->Anti_sql_injection($this->input->post('stock_pindah', TRUE));
			$stock_akhir = (float) $this->Anti_sql_injection($this->input->post('stock_akhir', TRUE));
			$type_mutasi = (int) $this->Anti_sql_injection($this->input->post('type_mutasi', TRUE));
			$no_bc = (int) $this->Anti_sql_injection($this->input->post('no_bc', TRUE));

			$data = array(
				'id_stock_awal'		=> $id_material,
				'id_stock_akhir'	=> $id_material,
				'amount_mutasi'		=> $stock_pindah,
				'no_bc'				=> $no_bc,
				'type_mutasi'		=> $type_mutasi,
				'id_material'		=> $id_material,
				'qty'				=> $stock_akhir,
				'date_mutasi'		=> date('Y-m-d H:i:s', strtotime($tgl_pindah . ' ' . date('H:i:s')))
			);

			$resultOldMutasi	= $this->mutasi_model->save_mutasi($data);

			//End
			$resultOldMutasi = $resultOldMutasi;
			if ($resultOldMutasi > 0) $result = 1;
			else $result = 0;

			if ($result > 0) {
				$this->log_activity->insert_activity('insert', 'Insert Mutasi Material');
				$result = array('success' => true, 'message' => 'Berhasil menambahkan Mutasi Material ke database');
			} else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert Mutasi Material');
				$result = array('success' => false, 'message' => 'Gagal menambahkan Mutasi Material ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}
	
	public function save_approve_mutasi()
	{
    // echo "<pre>";
    // print_r($this->input->post());
    // die;
		$amount_mutasi = $this->Anti_sql_injection($this->input->post('amount_mutasi', TRUE));
		$type_mutasi = (int) $this->Anti_sql_injection($this->input->post('type_mutasi', TRUE));
		$id_stock_akhir = $this->Anti_sql_injection($this->input->post('id_stock_akhir', TRUE));
		$id_mutasi = (int) $this->Anti_sql_injection($this->input->post('id_mutasi', TRUE));
		$id_mutasi_bc = (int) $this->Anti_sql_injection($this->input->post('id_mutasi_bc', TRUE));
		$id_material = (int) $this->Anti_sql_injection($this->input->post('id_material', TRUE));
		$id_valas = (int) $this->Anti_sql_injection($this->input->post('id_valas', TRUE));
		$id_coa = (int) $this->Anti_sql_injection($this->input->post('id_coa', TRUE));
		$stock_name =  $this->Anti_sql_injection($this->input->post('stock_name', TRUE));
		$stock_code =  $this->Anti_sql_injection($this->input->post('stock_code', TRUE));
		$type_material_name =  $this->Anti_sql_injection($this->input->post('type_material_name', TRUE));
		$rate =  (int)$this->Anti_sql_injection($this->input->post('rate', TRUE));
    $delivery_date     = $this->Anti_sql_injection($this->input->post('date_mutasi', TRUE));
    $delivery_date   = explode("-", $delivery_date);
    $delivery_date   =date('Y-m-d', strtotime($delivery_date[2] . '-' . $delivery_date[1] . '-' . $delivery_date[0]));
    $arrIdItem   = ($this->input->post('id_material', TRUE));
    $arrQty   = ($this->input->post('qty', TRUE));
    $base_price   =(float) ($this->input->post('base_price', TRUE));

		$data_up = array(
			'id_mutasi'			=> $id_mutasi,
			'id_mutasi_bc'		=> $id_mutasi_bc,
			'id_material'		=> $id_material,
			'type_mutasi'		=> $type_mutasi
		);
		
		if ($type_mutasi == 10 ||$type_mutasi == 0 ) {
      $type_mutasi_bc = 1; //type mutasi in
      $type_cash = 0; //piutang
			$result_update	= $this->mutasi_model->update_mutasi_masuk($data_up);
		}else{
      $type_mutasi_bc = -1; //type mutasi out
      $type_cash = 1; //utang
			$result_update	= $this->mutasi_model->update_mutasi_keluar($data_up);
		}
		
		$amount_mutasi = (float) $amount_mutasi  * (int) $type_mutasi_bc;

		$data = array(
			'amount_mutasi'		=> $amount_mutasi,
			'id_stock_akhir'	=> $id_stock_akhir,
			'id_mutasi'			=> $id_mutasi,
			'id_mutasi_bc'		=> $id_mutasi_bc,
			'id_material'		=> $id_material
		);

		$resultOldMutasi	= $this->mutasi_model->approve_mutasi($data);
    $data_coa = array(
      'id_coa'  => 487,
      'id_parent'  => 0,
      'id_valas'  => $id_valas,
      'value'    => $base_price * $arrQty,
      'adjusment'  => 0,
      'type_cash'  => $type_cash,
      'note'    => 'Mutasi  Kode Barang : ' .$stock_code.' Nama Barang : '.$stock_name.' ID Mutasi : '.$id_mutasi ,
      'rate'    => $rate
    );

    $result_add_coa = $this->mutasi_model->add_coa_values($data_coa);
		//End
		$resultOldMutasi = $resultOldMutasi;
		if ($resultOldMutasi > 0) $result = 1;
		else $result = 0;

		if ($result > 0) {
			$this->log_activity->insert_activity('insert', 'Insert Mutasi Material');
			$result = array('success' => true, 'message' => 'Berhasil menambahkan Mutasi Material ke database');
		} else {
			$this->log_activity->insert_activity('insert', 'Gagal Insert Mutasi Material');
			$result = array('success' => false, 'message' => 'Gagal menambahkan Mutasi Material ke database');
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
	public function save_add_mutasi()
	{
		$this->form_validation->set_rules('stock_pindah', 'Stock Pindah', 'trim|required');
		$this->form_validation->set_rules('gudang_pindah', 'Gudang Pindah', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			$id_material = $this->Anti_sql_injection($this->input->post('id_material', TRUE));
			$stock_awal = (int) $this->Anti_sql_injection($this->input->post('stock_awal', TRUE));
			// $stock_akhir = (int) $this->Anti_sql_injection($this->input->post('stock_akhir', TRUE));
			$stock_pindah = (int) $this->Anti_sql_injection($this->input->post('stock_pindah', TRUE));
			$gudang_awal = (int) $this->Anti_sql_injection($this->input->post('gudang_awal', TRUE));
			$gudang_pindah = (int) $this->Anti_sql_injection($this->input->post('gudang_pindah', TRUE));

			if ($stock_awal != 0 && $stock_pindah < $stock_awal) {
				if ($gudang_pindah != $gudang_awal) {
					$oldMaterial = $this->mutasi_model->edit($id_material);
					$oldPrice = $this->mutasi_model->existing_price($id_material);
					$oldMutasi = $this->mutasi_model->existing_mutasi($id_material);

					//insert New Material And Edit existing price, mutasi
					$newMaterial = array(
						'no_bc' => $oldMaterial[0]['no_bc'],
						'stock_code' => $oldMaterial[0]['stock_code'],
						'stock_name' => $oldMaterial[0]['stock_name'],
						'stock_description' => $oldMaterial[0]['stock_description'],
						'unit' => $oldMaterial[0]['unit'],
						'type' => $oldMaterial[0]['type'],
						'qty' => $stock_awal,
						'treshold' => $oldMaterial[0]['treshold'],
						'id_properties' => $oldMaterial[0]['id_properties'],
						'id_gudang' => $gudang_pindah,
						'status' => '1'
					);

					$oldDataPrice = array(
						'id'			=> $oldPrice[0]['id'],
						'id_komponen'	=> $oldPrice[0]['id_komponen'],
						'price'			=> ($stock_awal - $stock_pindah) * $oldPrice[0]['start_price'],
						'id_valas'		=> $oldPrice[0]['id_valas'],
						'id_master'		=> $oldPrice[0]['id_master'],
						'start_price'	=> $oldPrice[0]['start_price']
					);

					$oldDataMutasi = array(
						'id_mutasi'			=> $oldMutasi[0]['id_mutasi'],
						'id_stock_awal'		=> $id_material,
						'id_stock_akhir'	=> $id_material,
						'amount_mutasi'		=> ($stock_awal - $stock_pindah)
					);

					// $resultNewMaterial	= $this->mutasi_model->add_new_material($newMaterial);
					$resultOldPrice		= $this->mutasi_model->save_edit_price($oldDataPrice);
					$resultOldMutasi	= $this->mutasi_model->save_edit_mutasi($oldDataMutasi);
					//End

					//Insert New Mutasi and New Price
					$newDataMutasi = array(
						'id_stock_awal'		=> $id_material,
						// 'id_stock_akhir'	=> $resultNewMaterial['lastid'],
						'amount_mutasi'		=> $stock_pindah
					);

					$newDataPrice = array(
						'id_komponen'	=> $oldPrice[0]['id_komponen'],
						'price'			=> $stock_pindah * $oldPrice[0]['start_price'],
						'id_valas'		=> $oldPrice[0]['id_valas'],
						// 'id_master'		=> $resultNewMaterial['lastid'],
						'start_price'	=> $oldPrice[0]['start_price']
					);

					$resultOldMutasi	= $this->mutasi_model->save_mutasi($newDataMutasi);
					$resultOldPrice		= $this->mutasi_model->save_add_price($newDataPrice);
					//End

					if ($resultOldPrice > 0 && $resultOldMutasi > 0 && $newDataMutasi > 0 && $resultOldPrice > 0) $result = 1;
					else $result = 0;
				} else $result = 0;
			} else $result = 0;

			if ($result > 0) {
				$this->log_activity->insert_activity('insert', 'Insert Mutasi Material');
				$result = array('success' => true, 'message' => 'Berhasil menambahkan Mutasi Material ke database');
			} else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert Mutasi Material');
				$result = array('success' => false, 'message' => 'Gagal menambahkan Mutasi Material ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function edit($id)
	{
		$result_type = $this->mutasi_model->typeMaterial();
		$result_mutasi 	= $this->mutasi_model->get_mutasi($id);
		$result_material = $this->mutasi_model->getlistMaterial($result_mutasi[0]['id_type_material']);
		$result_gudang = $this->mutasi_model->gudang();
		$result_bc = $this->mutasi_model->listbc($result_mutasi[0]['type_mutasi']);
		$data = array(
			'typeMaterial'	=> $result_type,
			'material'	=> $result_material,
			'mutasi'	=> $result_mutasi,
			'gudang'	=> $result_gudang,
			'result_bc' => $result_bc
		);
		$this->load->view('edit_modal_view', $data);
	}

	public function save_edit_mutasi()
	{
		$this->form_validation->set_rules('stock_pindah', 'Stock Pindah', 'trim|required');
		$this->form_validation->set_rules('gudang_pindah', 'Gudang Pindah', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array(
				'success' => false,
				'message' => $msg
			);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			$id_material = $this->Anti_sql_injection($this->input->post('id_material', TRUE));
			$id_mutasi = $this->Anti_sql_injection($this->input->post('id_mutasi', TRUE));
			$stock_awal = (float) $this->Anti_sql_injection($this->input->post('stock_awal', TRUE));
			$stock_akhir = (float) $this->Anti_sql_injection($this->input->post('stock_akhir', TRUE));
			$stock_pindah = (float) $this->Anti_sql_injection($this->input->post('stock_pindah', TRUE));
			$gudang_awal = (int) $this->Anti_sql_injection($this->input->post('gudang_awal', TRUE));
			$gudang_pindah = (int) $this->Anti_sql_injection($this->input->post('gudang_pindah', TRUE));

			if ($stock_awal != 0 && $stock_pindah < $stock_awal) {
				if ($gudang_pindah != $gudang_awal) {
					$dataMaterial = array(
						'id_material'	=> $id_material,
						'qty'			=> $stock_akhir,
						'id_gudang'		=> $gudang_pindah,
					);
					// $resultMaterial = $this->mutasi_model->save_edit_material($dataMaterial);

					$dataMutasi = array(
						'id_mutasi'	=> $id_mutasi,
						'date_mutasi'	=> date('Y-m-d'),
						'stock_pindah'	=> $stock_pindah,
						'id_material'	=> $id_material,
						'qty'			=> $stock_akhir,
						'id_gudang'		=> $gudang_pindah,
					);
					$resultMutasi = $this->mutasi_model->save_edit_mutasi($dataMutasi);
					if ( $resultMutasi > 0) $result = 1;
					else $result = 0;
				} else $result = 0;
			} else $result = 0;

			if ($result > 0) {
				$this->log_activity->insert_activity('update', 'Update Mutasi Material id : ' . $id_mutasi);
				$result = array('success' => true, 'message' => 'Berhasil mengubah Mutasi Material ke database');
			} else {
				$this->log_activity->insert_activity('update', 'Gagal Update Mutasi Material id : ' . $id_mutasi);
				$result = array('success' => false, 'message' => 'Gagal mengubah Mutasi Material ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}
	public function save_edit_mutasi_new()
	{
		$id_mutasi = $this->Anti_sql_injection($this->input->post('id_mutasi', TRUE));
		$id_mutasi_bc = $this->Anti_sql_injection($this->input->post('id_mutasi_bc', TRUE));
		$amount_mutasi = (float) $this->Anti_sql_injection($this->input->post('stock_pindah', TRUE));
		$type_mutasi = (float) $this->Anti_sql_injection($this->input->post('type_mutasi', TRUE));
		$id_bc = (int) $this->Anti_sql_injection($this->input->post('id_bc', TRUE));
		$date_mutasi =  $this->Anti_sql_injection($this->input->post('date_mutasi', TRUE));
		$id_material = $this->Anti_sql_injection($this->input->post('id_material', TRUE));
		$dataMutasi = array(
			'id_mutasi'	=> $id_mutasi,
			'id_mutasi_bc'	=> $id_mutasi_bc,
			'amount_mutasi'	=> $amount_mutasi,
			'type_mutasi'	=> $type_mutasi,
			'id_bc'	=> $id_bc,
			'date_mutasi'	=> date('Y-m-d H:i:s', strtotime($date_mutasi)),
			'id_material'	=> $id_material,
		);
		$resultMutasi = $this->mutasi_model->save_edit_mutasi_new($dataMutasi);
		if ($resultMutasi > 0) $result = 1;
		else $result = 0;

		if ($result > 0) {
			$this->log_activity->insert_activity('update', 'Update Mutasi Material id : ' . $id_mutasi);
			$result = array('success' => true, 'message' => 'Berhasil mengubah Mutasi Material ke database');
		} else {
			$this->log_activity->insert_activity('update', 'Gagal Update Mutasi Material id : ' . $id_mutasi);
			$result = array('success' => false, 'message' => 'Gagal mengubah Mutasi Material ke database');
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function detail($id_mutasi)
	{
		$type 		= $this->mutasi_model->typeMaterial();
		$mutasi 	= $this->mutasi_model->get_mutasi($id_mutasi);
		// $mutasi 	= $this->mutasi_model->existing_mutasi($id_mutasi);
		$gudang 	= $this->mutasi_model->gudang();
		$data = array(
			'type'		=> $type,
			'mutasi'	=> $mutasi,
			'gudang'	=> $gudang
		);

		$this->load->view('detail_modal_view', $data);
	}
	public function approve($id_mutasi)
	{
		$type 		= $this->mutasi_model->typeMaterial();
		$mutasi 	= $this->mutasi_model->get_mutasi($id_mutasi);
		// $mutasi 	= $this->mutasi_model->existing_mutasi($id_mutasi);
		$gudang 	= $this->mutasi_model->gudang();
		$data = array(
			'type'		=> $type,
			'mutasi'	=> $mutasi,
			'gudang'	=> $gudang
		);

		$this->load->view('approve_modal_view', $data);
	}
	
	public function delete_mutasi()
	{
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data, true);
		$result = $this->mutasi_model->delete_mutasi($params);
		if ($result > 0) {
			$this->log_activity->insert_activity('delete', 'Delete Mutasi id : ' . $params['id']);
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		} else {
			$this->log_activity->insert_activity('delete', 'Gagal Delete Mutasi id : ' . $params['id']);
			$res = array('status' => 'failed', 'message' => 'Data gagal di hapus');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

}
