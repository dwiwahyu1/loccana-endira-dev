<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Report_Pembelian_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}
	
	public function get_report_pembelian_lokal($data) {
		$sql 	= 'CALL report_lokal(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['tanggal_masuk'],
			$data['tanggal_keluar']
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function get_report_pembelian_import($data) {
		$sql 	= 'CALL report_import(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['tanggal_masuk'],
			$data['tanggal_keluar']
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function get_report_pembelian_service($data) {
		$sql 	= 'CALL report_service(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['tanggal_masuk'],
			$data['tanggal_keluar']
		));

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
}