<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Report_Pembelian extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('report_pembelian/Report_pembelian_model');
		$this->load->library('excel');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'report_pembelian/views/index');
	}

	public function report_detail() {
		$this->load->view('report_detail_view');
	}

	public function view_report() {
		$jenis_report = $this->Anti_sql_injection($this->input->post('jenis_report', TRUE));
		$tanggalMasuk = $this->Anti_sql_injection($this->input->post('tanggal_masuk', TRUE));
		$tanggalKeluar = $this->Anti_sql_injection($this->input->post('tanggal_keluar', TRUE));
		
		$tglMsk = explode("/", $tanggalMasuk);
		$tglKel = explode("/", $tanggalKeluar);
		$tanggal_masuk = date('Y-m-d', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0]));
		$tanggal_keluar = date('Y-m-d', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0]));

		if($jenis_report == 'lokal') {
			//LOKAL
			$lokal = array('tanggal_masuk' => $tanggal_masuk, 'tanggal_keluar' => $tanggal_keluar);
			$result_lokal = $this->Report_pembelian_model->get_report_pembelian_lokal($lokal);
			
			$data = array(
				'report'			=> 'Pembelian Lokal',
				'tanggal_masuk'		=> date('d-F-Y', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0])),
				'tanggal_keluar'	=> date('d-F-Y', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0])),
				'hasil'				=> $result_lokal
			);
				
			if(count($result_lokal) > 0){
				$msg = 'Lihat report';
				
				$data['msg'] = array(
					'message'	=> 0
				);
				
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}else{
				$msg = 'Gagal melihat report';
				
				$data['msg'] = array(
					'message'	=> 1
				);
				
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}
			
			$this->load->view('report_pembelian_view', $data);
		}else if($jenis_report == 'import') {
			//IMPORT
			$import = array('tanggal_masuk' => $tanggal_masuk, 'tanggal_keluar' => $tanggal_keluar);
			$result_import = $this->Report_pembelian_model->get_report_pembelian_import($import);
			
			$data = array(
				'report'			=> 'Pembelian Import',
				'tanggal_masuk'		=> date('d-F-Y', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0])),
				'tanggal_keluar'	=> date('d-F-Y', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0])),
				'hasil'				=> $result_import
			);
				
			if(count($result_import) > 0){
				$msg = 'Lihat report';
				
				$data['msg'] = array(
					'message'	=> 0
				);
				
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}else{
				$msg = 'Gagal melihat report';
				
				$data['msg'] = array(
					'message'	=> 1
				);
				
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}
			
			$this->load->view('report_pembelian_view', $data);
		}else if($jenis_report == 'service') {
			//SERVICE
			$service = array('tanggal_masuk' => $tanggal_masuk, 'tanggal_keluar' => $tanggal_keluar);
			$result_service = $this->Report_pembelian_model->get_report_pembelian_service($service);
			
			$data = array(
				'report'			=> 'Pembelian Service',
				'tanggal_masuk'		=> date('d-F-Y', strtotime($tglMsk[2].'-'.$tglMsk[1].'-'.$tglMsk[0])),
				'tanggal_keluar'	=> date('d-F-Y', strtotime($tglKel[2].'-'.$tglKel[1].'-'.$tglKel[0])),
				'hasil'				=> $result_service
			);
			
			if(count($result_mutasi_barjad) > 0){
				$msg = 'Lihat report';
				
				$data['msg'] = array(
					'message'	=> 0
				);
				
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}else{
				$msg = 'Gagal melihat report';
				
				$data['msg'] = array(
					'message'	=> 1
				);
				
				$this->log_activity->insert_activity('view', $msg. ' dengan jenis report ' .$jenis_report);
			}
			
			$this->load->view('report_pembelian_view', $data);
		}else {
			$msg = 'Gagal melihat report pembelian';
			
			$result = array(
				'success' => false,
			);
			
			$this->log_activity->insert_activity('view', $msg);
			
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}
	
	public function export_excel()
    {
		$jenis_report = $this->Anti_sql_injection($this->input->post('report', TRUE));
		$tanggalMasuk = $this->Anti_sql_injection($this->input->post('tanggal_awal', TRUE));
		$tanggalKeluar = $this->Anti_sql_injection($this->input->post('tanggal_akhir', TRUE));
		
		$datemasuk=date_create($tanggalMasuk);
		$date_masuk_fix = date_format($datemasuk,"Y-m-d");
		
		$datekeluar=date_create($tanggalKeluar);
		$date_keluar_fix = date_format($datekeluar,"Y-m-d");
		
		if($jenis_report == 'Pembelian Lokal') {
		
			$lokal = array('tanggal_masuk' => $date_masuk_fix, 'tanggal_keluar' => $date_keluar_fix);
			$result_lokal = $this->Report_pembelian_model->get_report_pembelian_lokal($lokal);
			
			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getActiveSheet()->setTitle("Report Pembelian Lokal");
			$objPHPExcel->getProperties()->setCreator("System")
				->setLastModifiedBy("System")
				->setTitle("Report Pembelian Lokal")
				->setSubject("Report Bea Cukai")
				->setDescription("Report Pembelian Lokal")
				->setKeywords("Pembelian Lokal")
				->setCategory("Report");
							 
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'LAPORAN '.strtoupper($jenis_report))
				->setCellValue('A2', 'PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA')
				->setCellValue('A3', 'PERIODE '.strtoupper($tanggalMasuk).' S.D '.strtoupper($tanggalKeluar).'');
								
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A5', 'No')
				->setCellValue('B5', 'PR')
				->setCellValue('B6', 'No')
				->setCellValue('C6', 'Kode')
				->setCellValue('D6', 'Qty')
				->setCellValue('E6', 'Unit')
				->setCellValue('F5', 'PO')
				->setCellValue('F6', 'No')
				->setCellValue('G6', 'PO')
				->setCellValue('H6', 'TOP')
				->setCellValue('I5', 'Deskripsi')
				->setCellValue('J5', 'Qty')
				->setCellValue('K5', 'Unit')
				->setCellValue('L5', 'UOM')
				->setCellValue('L6', 'IDR')
				->setCellValue('M6', 'SGD')
				->setCellValue('N6', 'USD')
				->setCellValue('O5', 'Amount')
				->setCellValue('O6', 'IDR')
				->setCellValue('P6', 'SGD')
				->setCellValue('Q6', 'USD')
				->setCellValue('R5', 'BTB')
				->setCellValue('R6', 'No')
				->setCellValue('S6', 'Tanggal')
				->setCellValue('T6', 'Qty')
				->setCellValue('U6', 'Unit')
				->setCellValue('V5', 'No BC')
				->setCellValue('V6', 'No')
				->setCellValue('W6', 'Tanggal');
			
			$styleArray = array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			
						
			$i = 7;
			$no = 1;
			foreach($result_lokal as $lokal){
				
				$objPHPExcel->setActiveSheetIndex()->getStyle('A5:W'.$i)->applyFromArray($styleArray);
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B'.$i, $lokal['no_spb'])
					->setCellValue('C'.$i, $lokal['stock_code'])
					->setCellValue('D'.$i, $lokal['base_qty'])
					->setCellValue('F'.$i, $lokal['no_po'])
					->setCellValue('I'.$i, $lokal['stock_description'])
					->setCellValue('J'.$i, $lokal['base_qty'])
					->setCellValue('M'.$i, $lokal['SGD'])
					->setCellValue('N'.$i, $lokal['USD'])
					->setCellValue('P'.$i, $lokal['SGD'])
					->setCellValue('Q'.$i, $lokal['USD'])
					->setCellValue('R'.$i, $lokal['no_btb'])
					->setCellValue('T'.$i, $lokal['base_qty'])
					->setCellValue('V'.$i, $lokal['no_pendaftaran']);
				
				//CUSTOM COLUMN -->
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$i, $no)->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('E'.$i, $lokal['uom_name'])->getStyle('E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('G'.$i, $lokal['date_po'])->getStyle('G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
				$objPHPExcel->setActiveSheetIndex(0)		
					->setCellValue('H'.$i, '-')->getStyle('H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('K'.$i, $lokal['uom_name'])->getStyle('K'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('S'.$i, $lokal['approval_date'])->getStyle('S'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('U'.$i, $lokal['uom_name'])->getStyle('U'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('W'.$i, $lokal['tanggal_pengajuan'])->getStyle('U'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValueExplicit('L'.$i, str_replace('.','',number_format($lokal['Rp'],4,',','.')),PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('L'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValueExplicit('O'.$i, str_replace('.','',number_format($lokal['Rp'],4,',','.')),PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('O'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(35);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(15);
				
				$i++;
				$no++;
				
				
			}
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:W1');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:W1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:W2');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:W2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:W3');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:W3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:W4');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:A6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:E5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:E5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B6:B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('C6:C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('D6:D6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('E6:E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('F6:F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('G6:G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('H6:H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L6:L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('M6:M6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('N6:N6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('O6:O6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('P6:P6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('Q6:Q6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('R6:R6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('S6:S6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('T6:T6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('U6:U6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('V6:V6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('W6:W6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F5:H5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('F5:H5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L5:N5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:N5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('O5:Q5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('O5:Q5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('R5:U5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('R5:U5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('V5:W5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('V5:W5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I5:I6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J5:J6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('K5:K6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Pembelian Lokal '.$tanggalMasuk.' S.D '.$tanggalKeluar.'.xls"');
			
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit; 

		} elseif($jenis_report == 'Pembelian Import') {
			
			$import = array('tanggal_masuk' => $date_masuk_fix, 'tanggal_keluar' => $date_keluar_fix);
			$result_import = $this->Report_pembelian_model->get_report_pembelian_import($import);
			
			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getActiveSheet()->setTitle("Report Pembelian Import");
			$objPHPExcel->getProperties()->setCreator("System")
				->setLastModifiedBy("System")
				->setTitle("Report Pembelian Import")
				->setSubject("Report Bea Cukai")
				->setDescription("Report Pembelian Import")
				->setKeywords("Pembelian Import")
				->setCategory("Report");
							 
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'LAPORAN '.strtoupper($jenis_report))
				->setCellValue('A2', 'PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA')
				->setCellValue('A3', 'PERIODE '.strtoupper($tanggalMasuk).' S.D '.strtoupper($tanggalKeluar).'');
								
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A5', 'No')
				->setCellValue('B5', 'PR')
				->setCellValue('B6', 'No')
				->setCellValue('C6', 'Kode')
				->setCellValue('D6', 'Qty')
				->setCellValue('E6', 'Unit')
				->setCellValue('F5', 'PO')
				->setCellValue('F6', 'No')
				->setCellValue('G6', 'PO')
				->setCellValue('H6', 'TOP')
				->setCellValue('I5', 'Deskripsi')
				->setCellValue('J5', 'Qty')
				->setCellValue('K5', 'Unit')
				->setCellValue('L5', 'UOM')
				->setCellValue('L6', 'IDR')
				->setCellValue('M6', 'SGD')
				->setCellValue('N6', 'USD')
				->setCellValue('O5', 'Amount')
				->setCellValue('O6', 'IDR')
				->setCellValue('P6', 'SGD')
				->setCellValue('Q6', 'USD')
				->setCellValue('R5', 'BTB')
				->setCellValue('R6', 'No')
				->setCellValue('S6', 'Tanggal')
				->setCellValue('T6', 'Qty')
				->setCellValue('U6', 'Unit')
				->setCellValue('V5', 'No BC')
				->setCellValue('V6', 'No')
				->setCellValue('W6', 'Tanggal');
			
			$styleArray = array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			
						
			$i = 7;
			$no = 1;
			foreach($result_import as $import){
				
				$objPHPExcel->setActiveSheetIndex()->getStyle('A5:W'.$i)->applyFromArray($styleArray);
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B'.$i, $import['no_spb'])
					->setCellValue('C'.$i, $import['stock_code'])
					->setCellValue('D'.$i, $import['base_qty'])
					->setCellValue('F'.$i, $import['no_po'])
					->setCellValue('I'.$i, $import['stock_description'])
					->setCellValue('J'.$i, $import['base_qty'])
					->setCellValue('M'.$i, $import['SGD'])
					->setCellValue('N'.$i, $import['USD'])
					->setCellValue('P'.$i, $import['SGD'])
					->setCellValue('Q'.$i, $import['USD'])
					->setCellValue('R'.$i, $import['no_btb'])
					->setCellValue('T'.$i, $import['base_qty'])
					->setCellValue('V'.$i, $import['no_pendaftaran']);
				
				//CUSTOM COLUMN -->
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$i, $no)->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('E'.$i, $import['uom_name'])->getStyle('E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('G'.$i, $import['date_po'])->getStyle('G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
				$objPHPExcel->setActiveSheetIndex(0)		
					->setCellValue('H'.$i, '-')->getStyle('H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('K'.$i, $import['uom_name'])->getStyle('K'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('S'.$i, $import['approval_date'])->getStyle('S'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('U'.$i, $import['uom_name'])->getStyle('U'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('W'.$i, $import['tanggal_pengajuan'])->getStyle('U'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValueExplicit('L'.$i, str_replace('.','',number_format($import['Rp'],4,',','.')),PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('L'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValueExplicit('O'.$i, str_replace('.','',number_format($import['Rp'],4,',','.')),PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('O'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(35);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(15);
				
				$i++;
				$no++;
				
				
			}
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:W1');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:W1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:W2');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:W2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:W3');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:W3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:W4');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:A6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:E5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:E5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B6:B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('C6:C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('D6:D6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('E6:E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('F6:F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('G6:G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('H6:H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L6:L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('M6:M6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('N6:N6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('O6:O6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('P6:P6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('Q6:Q6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('R6:R6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('S6:S6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('T6:T6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('U6:U6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('V6:V6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('W6:W6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F5:H5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('F5:H5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L5:N5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:N5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('O5:Q5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('O5:Q5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('R5:U5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('R5:U5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('V5:W5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('V5:W5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I5:I6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J5:J6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('K5:K6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Pembelian Import '.$tanggalMasuk.' S.D '.$tanggalKeluar.'.xls"');
			
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit; 
			
		} elseif($jenis_report == 'Pembelian Service') {
			
			$service = array('tanggal_masuk' => $date_masuk_fix, 'tanggal_keluar' => $date_keluar_fix);
			$result_service = $this->Report_pembelian_model->get_report_pembelian_service($service);
			
			$objPHPExcel = new PHPExcel();
			
			$objPHPExcel->getActiveSheet()->setTitle("Report Pembelian Service");
			$objPHPExcel->getProperties()->setCreator("System")
				->setLastModifiedBy("System")
				->setTitle("Report Pembelian Service")
				->setSubject("Report Bea Cukai")
				->setDescription("Report Pembelian Service")
				->setKeywords("Pembelian Service")
				->setCategory("Report");
							 
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A1', 'LAPORAN '.strtoupper($jenis_report))
				->setCellValue('A2', 'PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA')
				->setCellValue('A3', 'PERIODE '.strtoupper($tanggalMasuk).' S.D '.strtoupper($tanggalKeluar).'');
								
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A5', 'No')
				->setCellValue('B5', 'PR')
				->setCellValue('B6', 'No')
				->setCellValue('C6', 'Kode')
				->setCellValue('D6', 'Qty')
				->setCellValue('E6', 'Unit')
				->setCellValue('F5', 'PO')
				->setCellValue('F6', 'No')
				->setCellValue('G6', 'PO')
				->setCellValue('H6', 'TOP')
				->setCellValue('I5', 'Deskripsi')
				->setCellValue('J5', 'Qty')
				->setCellValue('K5', 'Unit')
				->setCellValue('L5', 'UOM')
				->setCellValue('L6', 'IDR')
				->setCellValue('M6', 'SGD')
				->setCellValue('N6', 'USD')
				->setCellValue('O5', 'Amount')
				->setCellValue('O6', 'IDR')
				->setCellValue('P6', 'SGD')
				->setCellValue('Q6', 'USD')
				->setCellValue('R5', 'BTB')
				->setCellValue('R6', 'No')
				->setCellValue('S6', 'Tanggal')
				->setCellValue('T6', 'Qty')
				->setCellValue('U6', 'Unit')
				->setCellValue('V5', 'No BC')
				->setCellValue('V6', 'No')
				->setCellValue('W6', 'Tanggal');
			
			$styleArray = array(
				'borders' => array(
					'allborders' => array(
						'style' => PHPExcel_Style_Border::BORDER_THIN
					)
				)
			);
			
						
			$i = 7;
			$no = 1;
			foreach($result_service as $service){
				
				$objPHPExcel->setActiveSheetIndex()->getStyle('A5:W'.$i)->applyFromArray($styleArray);
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('B'.$i, $service['no_spb'])
					->setCellValue('C'.$i, $service['stock_code'])
					->setCellValue('D'.$i, $service['base_qty'])
					->setCellValue('F'.$i, $service['no_po'])
					->setCellValue('I'.$i, $service['stock_description'])
					->setCellValue('J'.$i, $service['base_qty'])
					->setCellValue('M'.$i, $service['SGD'])
					->setCellValue('N'.$i, $service['USD'])
					->setCellValue('P'.$i, $service['SGD'])
					->setCellValue('Q'.$i, $service['USD'])
					->setCellValue('R'.$i, $service['no_btb'])
					->setCellValue('T'.$i, $service['base_qty'])
					->setCellValue('V'.$i, $service['no_pendaftaran']);
				
				//CUSTOM COLUMN -->
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('A'.$i, $no)->getStyle('A'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('E'.$i, $service['uom_name'])->getStyle('E'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('G'.$i, $service['date_po'])->getStyle('G'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				
				$objPHPExcel->setActiveSheetIndex(0)		
					->setCellValue('H'.$i, '-')->getStyle('H'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('K'.$i, $service['uom_name'])->getStyle('K'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('S'.$i, $service['approval_date'])->getStyle('S'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('U'.$i, $service['uom_name'])->getStyle('U'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValue('W'.$i, $service['tanggal_pengajuan'])->getStyle('U'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValueExplicit('L'.$i, str_replace('.','',number_format($service['Rp'],4,',','.')),PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('L'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				
				$objPHPExcel->setActiveSheetIndex(0)
					->setCellValueExplicit('O'.$i, str_replace('.','',number_format($service['Rp'],4,',','.')),PHPExcel_Cell_DataType::TYPE_STRING)->getStyle('O'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				
				$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
				$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(35);
				$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
				$objPHPExcel->getActiveSheet()->getColumnDimension('R')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('S')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('T')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('U')->setWidth(12);
				$objPHPExcel->getActiveSheet()->getColumnDimension('V')->setWidth(13);
				$objPHPExcel->getActiveSheet()->getColumnDimension('W')->setWidth(15);
				
				$i++;
				$no++;
				
				
			}
			
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:W1');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A1:W1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:W2');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A2:W2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:W3');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A3:W3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A4:W4');
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('A5:A6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('A5:A6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('B5:E5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B5:E5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('B6:B6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('C6:C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('D6:D6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('E6:E6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('F6:F6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('G6:G6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('H6:H6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L6:L6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('M6:M6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('N6:N6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('O6:O6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('P6:P6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('Q6:Q6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('R6:R6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('S6:S6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('T6:T6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('U6:U6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('V6:V6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('W6:W6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('F5:H5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('F5:H5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('L5:N5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('L5:N5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('O5:Q5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('O5:Q5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('R5:U5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('R5:U5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('V5:W5');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('V5:W5')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('I5:I6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('I5:I6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('J5:J6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('J5:J6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->mergeCells('K5:K6');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$objPHPExcel->setActiveSheetIndex(0)->getStyle('K5:K6')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Pembelian Service '.$tanggalMasuk.' S.D '.$tanggalKeluar.'.xls"');
			
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			header('Cache-Control: max-age=1');

			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			exit; 
			
		}
		
	}
}