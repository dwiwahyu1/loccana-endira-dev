<div class="container" style="height: auto;">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title" id="title_menu">Laporan Pembelian <a class="btn btn-primary" onclick="report_detail()"><i class="fa fa-search"></i></a></h4>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12" style="height: 500px;">
			<div id="div_report"></div>
		</div>
	</div>
</div>

<div id="panel-modal-detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
function report_detail(){
	$('#panel-modal-detail').removeData('bs.modal');
	$('#panel-modal-detail .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
	$('#panel-modal-detail .panel-body').load('<?php echo base_url('report_pembelian/report_detail');?>');
	$('#panel-modal-detail .panel-title').html('<i class="fa fa-building-o"></i> Detail Report');
	$('#panel-modal-detail').modal({backdrop:'static',keyboard:false},'show');
}
</script>