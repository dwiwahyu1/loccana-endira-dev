<style>
	.changed_status{cursor:pointer;text-decoration: underline;color:#96b6e8;}
	.changed_status:hover{color:#ff8c00}
	.custom-tables, th{text-align:center;vertical-align:middle;}
	.custom-tables.align-text, th{vertical-align:middle;}
	.dt-body-right{text-align:right;}
	#titleTanggal{margin-bottom:10px;}
	.center_numb{text-align:center;}
	.left_numb{text-align:left;}
	.right_numb{text-align:right;font-weight:bold;}
	.tableReport {
		box-sizing: border-box;
		text-align: left;
		width: 90%;
		margin: 10px auto;
		padding-bottom:5px;
		page-break-before: always;
		z-index:99;
	}
	.table-scroll {
		position:relative;
		max-width:100%;
		margin:auto;
		overflow:hidden;
		border:1px solid #f0fffd;
	}
	.table-wrap {
		width:100%;
		overflow:auto;
	}
	.table-scroll table {
		width:100%;
		margin:auto;
		border-spacing:0;
	}
	.table-scroll th, .table-scroll td {
		padding:5px 10px;
		border:1px solid #000;
		background:#fff;
		white-space:nowrap;
		vertical-align:top;
	}
</style>

<div class="card-box">
	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<a class="btn btn-icon waves-effect waves-light btn-success m-b-5" data-toggle="tooltip" data-placement="top" title="Export to Excel" id="export_excel">
					<i class="fa fa-file-excel-o"></i>
				</a>
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download PDF" id="btn_download">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>
	
	<div class="col-md-12 text-center titleReport">
		<h2 id="titlePerusahaan">LAPORAN <?php echo strtoupper($report); ?></h2>
		<h2 id="namaPerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
		<h3 id="titleTanggal">PERIODE <?php echo $tanggal_masuk.' S.D '.$tanggal_keluar; ?></h3>
	</div>
	<hr>
	<div id="table-scroll" class="table-scroll">
		<div class="table-wrap">
			<table id="listreportpembelian" class="table table-striped table-bordered table-responsive dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th rowspan="2" class="custom-tables align-text">No</th>
						<th colspan="4" class="custom-tables align-text">PR</th>
						<th colspan="3" class="custom-tables align-text">PO</th>
						<th rowspan="2" class="custom-tables align-text">Deskripsi</th>
						<th rowspan="2" class="custom-tables align-text">Qty</th>
						<th rowspan="2" class="custom-tables align-text">Unit</th>
						<th colspan="3" class="custom-tables align-text">UOM</th>
						<th colspan="3" class="custom-tables align-text">Amount</th>
						<th colspan="4" class="custom-tables align-text">BTB</th>
						<th colspan="2" class="custom-tables align-text">No BC</th>
					</tr>
					<tr>
						<th class="custom-tables">No</th>
						<th class="custom-tables">Kode</th>
						<th class="custom-tables">Qty</th>
						<th class="custom-tables">Unit</th>
						<th class="custom-tables">No</th>
						<th class="custom-tables">PO</th>
						<th class="custom-tables">TOP</th>
						<th class="custom-tables">IDR</th>
						<th class="custom-tables">SGD</th>
						<th class="custom-tables">USD</th>
						<th class="custom-tables">IDR</th>
						<th class="custom-tables">SGD</th>
						<th class="custom-tables">USD</th>
						<th class="custom-tables">No</th>
						<th class="custom-tables">Tgl</th>
						<th class="custom-tables">Qty</th>
						<th class="custom-tables">Unit</th>
						<th class="custom-tables">No</th>
						<th class="custom-tables">Tgl</th>
					</tr>
				</thead>
				<tbody>
					<?php $no = 1;
					foreach($hasil as $h) { 
						$date_create1 = date_create($h['date_po']);
						$date_create2 = date_create($h['tanggal_pengajuan']);
						$date_create3 = date_create($h['approval_date']);
						$tanggal_po = date_format($date_create1,'d-F-Y');
						$tanggal_pengajuan = date_format($date_create2,'d-F-Y');
						$tanggal_approve = date_format($date_create3,'d-F-Y');
					?>
						<tr>
							<td class="center_numb"><?php echo $no; ?></td>
							<td class="left_numb"><?php echo $h['no_spb']; ?></td>
							<td class="left_numb"><?php echo $h['stock_code']; ?></td>
							<td class="right_numb"><?php echo number_format($h['base_qty'],4,',','.'); ?></td>
							<td class="center_numb"><?php echo $h['uom_name']; ?></td>
							<td class="left_numb"><?php echo $h['no_po']; ?></td>
							<td class="center_numb"><?php echo $h['date_po']; ?></td>
							<td class="center_numb">-</td>
							<td class="left_numb"><?php echo $h['stock_description']; ?></td>
							<td class="right_numb"><?php echo number_format($h['base_qty'],4,',','.'); ?></td>
							<td class="center_numb"><?php echo $h['uom_name']; ?></td>
							<td class="right_numb"><?php echo number_format($h['Rp'],4,',','.'); ?></td>
							<td class="right_numb"><?php echo number_format($h['SGD'],4,',','.'); ?></td>
							<td class="right_numb"><?php echo number_format($h['USD'],4,',','.'); ?></td>
							<td class="right_numb"><?php echo number_format($h['Rp'],4,',','.'); ?></td>
							<td class="right_numb"><?php echo number_format($h['SGD'],4,',','.'); ?></td>
							<td class="right_numb"><?php echo number_format($h['USD'],4,',','.'); ?></td>
							<td class="left_numb"><?php echo $h['no_btb']; ?></td>
							<td class="center_numb"><?php echo $h['approval_date']; ?></td>
							<td class="right_numb"><?php echo number_format($h['base_qty'],4,',','.'); ?></td>
							<td class="left_numb"><?php echo $h['uom_name']; ?></td>
							<td class="left_numb"><?php echo $h['no_pendaftaran']; ?></td>
							<td class="center_numb"><?php echo $h['tanggal_pengajuan']; ?></td>
						</tr>
					<?php $no++; } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script>

$(document).ready(function() {
	var pesan = '<?php echo $msg['message']; ?>';
	var tgl_masuk = '<?php echo $tanggal_masuk; ?>';
	var tgl_keluar = '<?php echo $tanggal_keluar; ?>';
	$('#btn_print').on('click', function() {
		var divToPrint=document.getElementById("listreportpembelian");
		newWin= window.open('', '', 'height=700,width=700');
		newWin.document.write(divToPrint.outerHTML);
		newWin.print();
		newWin.close();
	});
	
	$('#btn_download').click(function () {
		var doc = new jsPDF("l", "mm", "a4");
		var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
		var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
		
		// FOOTER
		doc.setTextColor(50);
		doc.setFontSize(12); 
		doc.setFontStyle('helvetica','arial','sans-serif','bold');
		doc.text($('#titlePerusahaan').html(), pageWidth / 2, 20, 'center');
		doc.text($('#namaPerusahaan').html(), pageWidth / 2, 25, 'center');
		doc.text($('#titleTanggal').html(), pageWidth / 2, 30, 'center');
		
		doc.autoTable({
			html 			: '#listreportpembelian',
			headStyles		: {
				fontSize 	: 7,
				valign		: 'middle', 
				halign		: 'center',
			},
			bodyStyles		: {
				fontSize 	: 6,
			},
			theme			: 'plain',
			styles			: {
				fontSize : 6, 
				lineColor: [0, 0, 0],
				lineWidth: 0.35,
				cellWidth : 'auto',
				
			},
			margin 			: 4,
			/*columnStyles	: {
				0: {halign: 'center'},
				1: {halign: 'left'},
				2: {halign: 'left'},
				3: {halign: 'left'},
				4: {halign: 'left'},
				5: {halign: 'right'},
				6: {halign: 'left'},
				7: {halign: 'left'},
				8: {halign: 'left'},
				9: {halign: 'center'},
				10: {halign: 'right'},
				11: {halign: 'center'},
				12: {halign: 'right'}
			},*/
			rowPageBreak	: 'auto',
			showHead 		: 'firstPage',
			showFoot		: 'lastPage',
			startY			: 40
		});
		doc.save('Report <?php echo $report; ?>.pdf');
	});
	
	$('#export_excel').click(function () {
		
		var url = '<?php echo base_url(); ?>report_pembelian/export_excel'; 
		var tanggal_awal = '<?php echo $tanggal_masuk; ?>';
		var tanggal_akhir = '<?php echo $tanggal_keluar; ?>';
		var report = '<?php echo $report ?>';
		  
		var form = $("<form action='" + url + "' method='post' target='_blank'>" +
			"<input type='hidden' name='tanggal_awal' value='" + tanggal_awal + "' />" +
			"<input type='hidden' name='tanggal_akhir' value='" + tanggal_akhir + "' />" +
			"<input type='hidden' name='report' value='" + report + "' />" +
			"</form>");
		$('body').append(form);
		form.submit();
	});
	
	function objectSize(obj) {
		var size = 0;
		for (key in obj) {
			if (obj.hasOwnProperty(key)) size++;
		}
		return size;
	}
	
	if(pesan == 1){
		swal("Maaf!", "Laporan periode "+tgl_masuk+" S.D "+tgl_keluar+" tidak ditemukan...!!!", "info");
	}
});
</script>