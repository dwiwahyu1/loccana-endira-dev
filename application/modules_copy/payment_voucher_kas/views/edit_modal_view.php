<form class="form-horizontal form-label-left" id="add_form" role="form" action="<?php echo base_url('payment_voucher_kas/save_edit_payment_backup'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_voucher">No Voucher <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="no_pvk" name="no_pvk" class="form-control" placeholder="No Voucher" autocomplete="off" value="<?php if (isset($pv[0]['no_pvk'])) {
																																																															echo $pv[0]['no_pvk'];
																																																														} ?>" readonly>
		</div>
	</div>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tgl_bpvk">Tanggal <span class="required"><sup>*</sup></span></label>

		<div class="col-md-8 col-sm-6 col-xs-12">
			<div class="input-group date">
				<input placeholder="Tanggal" type="text" class="form-control col-md-8 col-sm-6 col-xs-12 datepicker" id="tgl_bpvk" name="tgl_bpvk" required="required" value="<?php if (isset($pv[0]['date_pvk'])) {
																																																																																				echo $pv[0]['date_pvk'];
																																																																																			} ?>" required>
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
	</div>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_penerima">Nama Penerima <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="nama_penerima" name="nama_penerima" class="form-control" placeholder="Nama Penerima" autocomplete="off" value="<?php if (isset($pv[0]['nama_penerima'])) {
																																																																							echo $pv[0]['nama_penerima'];
																																																																						} ?>">
		</div>
	</div>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Kas : </label>
		<div class="col-md-8 col-sm-6 col-xs-12 add_item" onclick="add_coa_value()">
			<a type="button" class="btn btn-default btn-icon waves-effect waves-light m-b-5">
				<i class="fa fa-plus"></i>
			</a>
			Tambah Kas
		</div>
	</div>

	<div class="item form-group">
		<div class="col-md-9 col-md-offset-2">
			<table id="list_barang" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th style="width: 5%;">No</th>
						<th>Perkiraan</th>
						<th>COA</th>
						<th>Keterangan</th>
						<th>Valas</th>
						<th>Nilai</th>
						<th>Tipe</th>
						<th style="width: 5%">Option</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$itemsLen = count($coa);
					?>
				</tbody>
			</table>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jumlah">Jumlah</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="jumlah" name="jumlah" class="form-control" placeholder="Jumlah" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group" id="div_list_bank">
		<div class="col-md-9 col-md-offset-2">
			<table id="list_bank" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<th>Bank</th>
					<th>Rate</th>
					<th>Nilai</th>
					<th style="width: 5%;"><a id="btn_bank_add" class="btn btn-primary"><i class="fa fa-plus"></i></a></td>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit Payment Voucher</button>
			</div>
		</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	var formData = new FormData();
	var itemKas = [];
	var itemBank = [];
	var idRowBank = 0;
	var arrBank = [];
	var arrRek = [];
	var arrNominal = [];

	function reload_date() {
		$(".date").datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});
	}

	function draw_bank_table_blank() {
		idRowBank++;
		$('#list_bank tbody').append(
			'<tr id="trRowBank' + idRowBank + '">' +
			'<td>' +
			'<select class="form-control" id="ddl_bank' + idRowBank + '" name="ddl_bank[]" style="width: 100%" required>' +
			<?php foreach ($list_bank as $key) { ?> '<option value="' +
				'<?php echo $key['id_coa']; ?>' + '">' + '<?php echo $key['nama_bank'] . "-" . $key['no_rek'] . "-" . $key['nama_valas']; ?>' + '</option>' +
			<?php } ?> '</select>' +
			'</td>' +
			'<td>' +
			'<input type="number" class="form-control" id="rate' + idRowBank + '" name="rate[]" value="0" placeholder="Rate" autocomplete="off" required>' +
			'</td>' +
			'<td>' +
			'<input type="number" class="form-control" id="nominal_bank' + idRowBank + '" name="nominal_bank[]" value="0" placeholder="Nominal" autocomplete="off">' +
			'</td>' +
			'<td id="td_btn">' +
			'<a class="btn btn-danger" onclick="removeRowBank(' + idRowBank + ')"><i class="fa fa-minus"></i></a>' +
			'</td>' +
			'</tr>'
		);
	}

	function draw_bank_table() {
		idRowBank++;
		for (i = 0; i < itemBank.length; i++) {
			selectedText = '';
			selectBankText = '<td>' +
				'<select class="form-control" id="ddl_bank' + idRowBank + '" name="ddl_bank[]" style="width: 100%" required >' +
				<?php foreach ($list_bank as $key) { ?> '<option value="' +
					'<?php echo $key['id_coa']; ?>';
			if (itemBank[i].id_bank == <?php if (isset($key['id_bank'])) echo $key['id_bank'];
																		else echo 'null'; ?>)
				selectedText = 'selected="selected"';
			else selectedText = '';
			selectBankText = selectBankText + '"' + selectedText +
				'>' + '<?php echo $key['nama_bank'] . "-" . $key['no_rek'] . "-" . $key['nama_valas']; ?>' + '</option>' +
			<?php } ?> '</select>' +
			'</td>';
			$('#list_bank tbody').append(
				'<tr id="trRowBank' + idRowBank + '">' +
				selectBankText +
				'<td>' +
				'<input type="number" class="form-control" id="rate' + idRowBank + '" name="rate[]" value="' + itemBank[i].rate + '" placeholder="Rate" autocomplete="off" required>' +
				'</td>' +
				'<td>' +
				'<input type="number" class="form-control" id="nominal_bank' + idRowBank + '" name="nominal_bank[]" value="' + itemBank[i].value + '" placeholder="Nominal" autocomplete="off">' +
				'</td>' +
				'<td id="td_btn">' +
				'<a class="btn btn-danger" onclick="removeRowBank(' + idRowBank + ')"><i class="fa fa-minus"></i></a>' +
				'</td>' +
				'</tr>'
			);
			// $("#ddl_bank" + idRowBank).select2();
		}
	}
	$(document).ready(function() {
		var delete_kas_id = null;
		reload_date();
		initiate_item_kas();
		initiate_item_bank();
		draw_kas_table();
		draw_bank_table();
		// $('#id_pvk').hide();
		$('#btn_bank_add').on('click', function() {
			draw_bank_table_blank();
			reload_date();
		});
	});

	function initiate_item_kas() {
		"<?php
			for ($i = 0; $i < $itemsLen; $i++) {
				?>"
		var temp_kas_data = {
			id: "<?php echo $coa[$i]['id']; ?>",
			id_coa: "<?php echo $coa[$i]['id_coa']; ?>",
			text_coa: "<?php echo $coa[$i]['keterangan']; ?>",
			coa_number: "<?php echo $coa[$i]['coa']; ?>",
			id_parent: "<?php echo $coa[$i]['id_parent']; ?>",
			id_valas: "<?php echo $coa[$i]['id_valas']; ?>",
			text_valas: "<?php echo $coa[$i]['symbol']; ?>",
			value: "<?php echo $coa[$i]['value']; ?>",
			rate: "<?php echo $coa[$i]['rate']; ?>",
			type_cash: "<?php echo $coa[$i]['type_cash']; ?>",
			type_cash_text: "<?php if ($coa[$i]['type_cash'] == 1) echo "Pengeluaran";
													else echo "Pemasukan"; ?>",
			note: "<?php echo $coa[$i]['note']; ?>",
			// file: new Blob(["<?php //if($coa[$i]['file_content']) echo $coa[$i]['file_content']; else echo '';
														?>"]),
			have_file: "<?php echo $coa[$i]['have_file']; ?>",
			file_name: "<?php echo $coa[$i]['bukti']; ?>"
		}
		formData.append('id[]', temp_kas_data.id);
		formData.append('id_coa[]', temp_kas_data.id_coa);
		formData.append('text_coa[]', temp_kas_data.text_coa);
		formData.append('coa_number[]', temp_kas_data.coa_number);
		formData.append('id_parent[]', temp_kas_data.id_parent);
		formData.append('date[]', temp_kas_data.date);
		formData.append('id_valas[]', temp_kas_data.id_valas);
		formData.append('value[]', temp_kas_data.value);
		formData.append('rate[]', temp_kas_data.rate);
		formData.append('type_cash[]', temp_kas_data.type_cash);
		formData.append('type_cash_text[]', temp_kas_data.type_cash_text);
		formData.append('note[]', temp_kas_data.note);
		formData.append('file_kas[]', new Blob(), temp_kas_data.file_name);
		formData.append('have_file[]', temp_kas_data.have_file);

		itemKas.push(temp_kas_data);
		"<?php } ?>"
	}

	function initiate_item_bank() {
		"<?php

			for ($i = 0; $i < sizeof($pvd); $i++) {
				?>"
		var temp_bank_data = {
			id_bank: "<?php echo $pvd[$i]['id_bank']; ?>",
			id_coa: "<?php echo $pvd[$i]['id_coa']; ?>",
			rate: "<?php echo $pvd[$i]['rate']; ?>",
			value: "<?php echo $pvd[$i]['nominal']; ?>",
		}

		itemBank.push(temp_bank_data);

		"<?php
			}
			?>"
	}

	function draw_kas_table() {
		var itemslen = itemKas.length;
		var element = '';
		var totalValue = 0;
		var text_number = 1;
		for (var i = 0; i < itemslen; i++) {
			element += '   <tr>' +
				'<tr id="trRowBank' + idRowBank + '">' +
				'<td>' + text_number++ + '</td>' +
				'<td>' + itemKas[i]["text_coa"] + " - " + itemKas[i]["coa_number"] + '</td>' +
				'<td>' + itemKas[i]["coa_number"] + '</td>' +
				'<td>' + itemKas[i]["note"] + '</td>' +
				'<td>' + itemKas[i]["text_valas"] + '</td>' +
				'<td>' +
				'<input type="number" class="form-control" id="value_kas' + '" name="value[]" value="' + itemKas[i]["value"] + '" placeholder="Nominal" autocomplete="off">' +
				'</td>' +
				'<td>' + itemKas[i]["type_cash_text"] + '</td>' +
				'<td>' +
				'  <div class="btn-group">' +
				'    <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete Kas" onClick="delete_kas_item(' + i + ')">' +
				'    <i class="fa fa-trash"></i>' +
				'  </div>' +
				'</td>' +
				'</tr>';
			totalValue = totalValue + parseInt(itemKas[i]["value"]);
		}
		$('#list_barang tbody').html(element);
		$('#jumlah').val(totalValue);
	}

	function add_kas_temp_data(itemsKas) {
		itemKas.push(itemsKas);
		draw_kas_table();
	}

	function removeRowBank(rowBank) {
		$('#trRowBank' + rowBank).remove();
		// draw_bank_table();
	}

	function delete_kas_item(rowBank) {
		itemKas.splice(rowBank, 1);
		formData = new FormData();
		for (i = 0; i < itemKas.length; i++) {
			formData.append('id[]', itemKas[i].id);
			formData.append('id_coa[]', itemKas[i].id_coa);
			formData.append('text_coa[]', itemKas[i].text_coa);
			formData.append('coa_number[]', itemKas[i].coa_number);
			formData.append('id_parent[]', itemKas[i].id_parent);
			formData.append('date[]', itemKas[i].date);
			formData.append('id_valas[]', itemKas[i].id_valas);
			formData.append('value[]', itemKas[i].value);
			formData.append('rate[]', itemKas[i].rate);
			formData.append('type_cash[]', itemKas[i].type_cash);
			formData.append('type_cash_text[]', itemKas[i].type_cash_text);
			formData.append('note[]', itemKas[i].note);
			formData.append('file_kas[]', new Blob(), itemKas[i].file_name);
			formData.append('have_file[]', itemKas[i].have_file);
		}

		draw_kas_table();
	}
	$('#add_form').on('submit', (function(e) {
		arrBank = [];
		arrDate = [];
		arrRate = [];
		arrNominal = [];
		arrValue = [];
		formData.delete('arrBank[]', '');
		formData.delete('arrDate[]', '');
		formData.delete('arrRate[]', '');
		formData.delete('arrNominal[]', '');
		$('#btn-submit').attr('disabled', 'disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();

		// $('input[name="bank[]"]').each(function() {
		// 	if (this.value) {
		// 		if (this.value != undefined && this.value != '') arrBank.push(this.value);
		// 	}
		// })

		$('select[name="ddl_bank[]"] option:selected').each(function() {
			if (this.value) {
				if (this.value != undefined && this.value != '') {
					formData.append('arrBank[]', this.value);
					arrBank.push(this.value);
				}
			}
		})

		$('input[name="date[]"]').each(function() {
			if (this.value) {
				if (this.value != undefined && this.value != '') {
					formData.append('arrDate[]', this.value);
					arrDate.push(this.value);
				}
			}
		})
		$('input[name="rate[]"]').each(function() {
			if (this.value) {
				if (this.value != undefined && this.value != '') {
					formData.append('arrRate[]', this.value);
					arrRate.push(this.value);
				}
			}
		})

		$('input[name="nominal_bank[]"]').each(function() {
			if (this.value) {
				if (this.value != undefined && this.value != '' && this.value > 0) {
					formData.append('arrNominal[]', this.value);
					arrNominal.push(this.value);
				}
			}
		})
		$('input[name="value[]"]').each(function() {
			if (this.value) {
				if (this.value != undefined && this.value != '' && this.value > 0) {
					formData.append('arrValue[]', this.value);
					arrValue.push(this.value);
				}
			}
		})
		if (arrBank.length > 0 && arrRate.length > 0 && arrNominal.length > 0) {
			formData.set('tgl_bpvk', $('#tgl_bpvk').val());
			formData.set('jumlah', $('#jumlah').val());
			formData.set('id_pvk', <?php echo $pv[0]['id_vk']; ?>);
			formData.set('no_pvk', $('#no_pvk').val());
			formData.set('nama_penerima', $('#nama_penerima').val());
			console.log(formData);
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: formData,
				cache: false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function() {
							window.location.href = "<?php echo base_url('payment_voucher_kas'); ?>";
						})
					} else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Tambah Payment Voucher");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Tambah Payment Voucher");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		} else {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah Payment Voucher");
			swal("Failed!", "Invalid Inputan Bank, silahkan cek kembali.", "error");
		}
	}));

	function edit_kas_item(id) {
		delete_kas_id = id;
		$('#panel-modal-lv1').removeData('bs.modal');
		$('#panel-modal-lv1  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal-lv1  .panel-body').load('<?php echo base_url('payment_voucher_kas/edit_coa_value'); ?>');
		$('#panel-modal-lv1  .panel-title').html('<i class="fa fa-plus"></i> Tambah Kas');
		$('#panel-modal-lv1').modal({
			backdrop: 'static',
			keyboard: false
		}, 'show');
	}
</script>