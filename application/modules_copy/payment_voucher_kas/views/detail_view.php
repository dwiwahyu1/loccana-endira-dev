<form class="form-horizontal form-label-left" id="add_form" role="form" action="<?php echo base_url('payment_voucher_kas/save_edit_payment'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_voucher">No Voucher<span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="no_pvk" name="no_pvk" class="form-control" placeholder="No Voucher" autocomplete="off" value="<?php if (isset($pv[0]['no_pvk'])) {
																																																															echo $pv[0]['no_pvk'];
																																																														} ?>" readonly>
		</div>
	</div>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tgl_bpvk">Tanggal <span class="required"><sup>*</sup></span></label>

		<div class="col-md-8 col-sm-6 col-xs-12">
			<div class="input-group date">
				<input placeholder="Tanggal" type="text" class="form-control col-md-8 col-sm-6 col-xs-12 datepicker" id="tgl_bpvk" name="tgl_bpvk" required="required" value="<?php if (isset($pv[0]['date_pvk'])) {
																																																																																				echo date('d-M-Y', strtotime($pv[0]['date_pvk']));
																																																																																			} ?>" readonly>
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
	</div>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_penerima">Nama Penerima <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="nama_penerima" name="nama_penerima" class="form-control" placeholder="Nama Penerima" autocomplete="off" value="<?php if (isset($pv[0]['nama_penerima'])) {
																																																																							echo $pv[0]['nama_penerima'];
																																																																						} ?>" readonly>
		</div>
	</div>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Kas : </label>
	</div>

	<div class="item form-group">
		<div class="col-md-9 col-md-offset-2">
			<table id="list_barang" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th style="width: 5%;">No</th>
						<th>Perkiraan</th>
						<th>COA</th>
						<th>Keterangan</th>
						<th>Valas</th>
						<th>Nilai</th>
						<th>Tipe</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$itemsLen = count($coa);
					?>
				</tbody>
			</table>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jumlah">Jumlah</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="jumlah" name="jumlah" class="form-control" placeholder="Jumlah" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group" id="div_list_bank">
		<div class="col-md-9 col-md-offset-2">
			<table id="list_bank" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th style="width: 35%;">Bank / Cash</th>
						<th>Rate</th>
						<th>Nilai</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	var formData = new FormData();
	var itemKas = [];
	var itemBank = [];
	var idRowBank = 0;
	var arrBank = [];
	var arrRek = [];
	var arrNominal = [];

	function reload_date() {
		$(".date").datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});
	}

	function draw_bank_table() {
		idRowBank++;
		for (i = 0; i < itemBank.length; i++) {
			selectedText = '';
			selectBankText = '<td>' +
				'<select class="form-control" id="ddl_bank' + idRowBank + '" name="ddl_bank[]" style="width: 100%" required disabled>' +
				<?php foreach ($list_bank as $key) { ?> '<option value="' +
					'<?php echo $key['id_coa']; ?>';
			if (itemBank[i].id_bank == <?php if (isset($key['id_bank'])) echo $key['id_bank'];
																		else echo 'null'; ?>)
				selectedText = 'selected="selected"';
				else selectedText = '';
				
				console.log(itemBank[i].id_bank,<?php if (isset($key['id_bank'])) echo $key['id_bank'];
																		else echo 'null'; ?>,itemBank[i].id_bank == <?php if (isset($key['id_bank'])) echo $key['id_bank'];
																		else echo 'null'; ?>,selectedText);
			selectBankText = selectBankText + '"' + selectedText +
				'>' + '<?php echo $key['nama_bank'] . "-" . $key['no_rek'] . "-" . $key['nama_valas']; ?>' + '</option>' +
			<?php } ?> '</select>' +
			'</td>';

			$('#list_bank tbody').append(
				'<tr id="trRowBank' + idRowBank + '">' +
				selectBankText +
				'<td>' +
				'<input type="number" class="form-control" id="rate' + idRowBank + '" name="rate[]" value="' + itemBank[i].rate + '" placeholder="Rate" autocomplete="off" readonly>' +
				'</td>' +
				'<td>' +
				'<input type="number" class="form-control" id="nominal_bank' + idRowBank + '" name="nominal_bank[]" value="' + itemBank[i].value + '" placeholder="Nominal" autocomplete="off" readonly>' +
				'</tr>'
			);
			// $("#ddl_bank" + idRowBank).select2();
		}
	}
	$(document).ready(function() {
		reload_date();
		initiate_item_kas();
		initiate_item_bank();
		draw_kas_table();
		draw_bank_table();
		// $('#id_pvk').hide();
	});

	function initiate_item_kas() {
		"<?php
			for ($i = 0; $i < $itemsLen; $i++) {
				?>"
		var temp_kas_data = {
			id: "<?php echo $coa[$i]['id']; ?>",
			id_coa: "<?php echo $coa[$i]['id_coa']; ?>",
			text_coa: "<?php echo $coa[$i]['keterangan']; ?>",
			coa_number: "<?php echo $coa[$i]['coa']; ?>",
			id_parent: "<?php echo $coa[$i]['id_parent']; ?>",

			id_valas: "<?php echo $coa[$i]['id_valas']; ?>",
			text_valas: "<?php echo $coa[$i]['symbol']; ?>",
			value: "<?php echo $coa[$i]['value']; ?>",
			rate: "<?php echo $coa[$i]['rate']; ?>",
			type_cash: "<?php echo $coa[$i]['type_cash']; ?>",
			type_cash_text: "<?php if ($coa[$i]['type_cash'] == 1) echo "Pengeluaran";
													else echo "Pemasukan"; ?>",
			note: "<?php echo $coa[$i]['note']; ?>",
			// file: new Blob(["<?php //if($coa[$i]['file_content']) echo $coa[$i]['file_content']; else echo '';
														?>"]),
			have_file: "<?php echo $coa[$i]['have_file']; ?>",
			file_name: "<?php echo $coa[$i]['bukti']; ?>"
		}
		formData.append('id[]', temp_kas_data.id);
		formData.append('id_coa[]', temp_kas_data.id_coa);
		formData.append('text_coa[]', temp_kas_data.text_coa);
		formData.append('coa_number[]', temp_kas_data.coa_number);
		formData.append('id_parent[]', temp_kas_data.id_parent);
		formData.append('date[]', temp_kas_data.date);
		formData.append('id_valas[]', temp_kas_data.id_valas);
		formData.append('value[]', temp_kas_data.value);
		formData.append('rate[]', temp_kas_data.rate);
		formData.append('type_cash[]', temp_kas_data.type_cash);
		formData.append('type_cash_text[]', temp_kas_data.type_cash_text);
		formData.append('note[]', temp_kas_data.note);
		formData.append('file_kas[]', new Blob(), temp_kas_data.file_name);
		formData.append('have_file[]', temp_kas_data.have_file);

		itemKas.push(temp_kas_data);
		"<?php } ?>"
		// console.log(itemKas);
	}

	function initiate_item_bank() {
		"<?php

			for ($i = 0; $i < sizeof($pvd); $i++) {
				?>"
		var temp_bank_data = {
			id_bank: "<?php echo $pvd[$i]['id_bank']; ?>",
			id_coa: "<?php echo $pvd[$i]['id_coa']; ?>",

			rate: "<?php echo $pvd[$i]['rate']; ?>",
			value: "<?php echo $pvd[$i]['nominal']; ?>",
		}

		itemBank.push(temp_bank_data);

		"<?php
			}
			?>"
	}

	function draw_kas_table() {
		var itemslen = itemKas.length;
		var element = '';
		var totalValue = 0;
		var text_number = 1;
		for (var i = 0; i < itemslen; i++) {
			element += '   <tr>' +
				'<tr id="trRowBank' + idRowBank + '">' +
				'<td>' + text_number++ + '</td>' +
				'<td>' + itemKas[i]["text_coa"] + '</td>' +
				'<td>' + itemKas[i]["coa_number"] + '</td>' +
				'<td>' + itemKas[i]["note"] + '</td>' +
				'<td>' + itemKas[i]["text_valas"] + '</td>' +
				'<td>' + itemKas[i]["value"] + '</td>' +
				'<td>' + itemKas[i]["type_cash_text"] + '</td>' +
				'</tr>';
			totalValue = totalValue + parseInt(itemKas[i]["value"]);
		}
		$('#list_barang tbody').html(element);
		$('#jumlah').val("RP. " + formatCurrencyComa(totalValue));
	}
</script>