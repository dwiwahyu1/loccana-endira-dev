<style type="text/css">
	.dt-body-center {
		text-align: center;
	}
	.dt-body-right {
		text-align: right;
	}
</style>
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title" id="title_menu">Bank/Cash Payment Voucher</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<table id="list_payment" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>COA</th>
							<th>No BPV/CPV</th>
							<th>Tanggal BPV/CPV</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-dialog-scrollable" style="width: 80%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modal-lv1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog modal-dialog-scrollable" style="width: 80%;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	var tablePayment;
	$(document).ready(function(){
		get_list_payment();
	});

	function get_list_payment() {
		tablePayment = $("#list_payment").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'payment_voucher_kas/list_payment/';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false,
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">Bfrtip',
			"buttons" : [{
				extend: 'excelHtml5',
				text: '<i class="fa fa-file-excel-o" title="Excel"></i>',
				filename : 'Bank Payment Voucher',
				header : true,
				action: function (e, dt, button, node, config) {
					$('.buttons-excel').attr('disabled', 'disabled');
					var excelButtonConfig 				= $.fn.DataTable.ext.buttons.excelHtml5;
					excelButtonConfig.filename 			= 'Bank Bank/Cash Payment Voucher';
					excelButtonConfig.exportOptions 	= {
						columns: [0, 1, 2, 3, 4]
					};
					
					$.ajax({
						type: "POST",
						url: "<?php echo base_url().'payment_voucher_kas/list_payment?length=-1';?>",
						dataType: 'json',
						contentType: 'application/json; charset=utf-8',
						success: function(response) {
							tablePayment.api().rows().remove();
							tablePayment.api().rows.add(response.data);
							excelButtonConfig.action(e, dt, button, excelButtonConfig);
							$('.buttons-excel').removeAttr('disabled');
						}
					});
				}
			}, {
				extend 			: 'pdfHtml5',
				text 			: '<i class="fa fa-file-pdf-o" title="Pdf"></i>',
				action : function(e, dt, button, node, config) {
					$('.buttons-pdf').attr('disabled', 'disabled');
					var pdfButtonConfig 			= $.fn.DataTable.ext.buttons.pdfHtml5;
					pdfButtonConfig.filename 		= 'Bank Bank/Cash Payment Voucher';
					pdfButtonConfig.title 			= 'Bank Bank/Cash Payment Voucher';
					pdfButtonConfig.orientation 	= 'landscape';
					pdfButtonConfig.pageSize 		= 'A4';
					pdfButtonConfig.exportOptions 	= {
						columns: [0, 1, 2, 3, 4]
					};
					$.ajax({
						type: "POST",
						url: "<?php echo base_url().'payment_voucher_kas/list_payment?length=-1';?>",
						dataType: 'json',
						contentType: 'application/json; charset=utf-8',
						success: function(response) {
							tablePayment.api().rows().remove();
							tablePayment.api().rows.add(response.data);
							pdfButtonConfig.action(e, dt, button, pdfButtonConfig);
							$('.buttons-pdf').removeAttr('disabled');
						}
					});
				}
			}],
			"initComplete": function(){
				$("div.toolbar").prepend(
					'<div class="btn-group pull-left">'+
						'<a class="btn btn-primary" onClick="add_payment()"><i class="fa fa-plus"></i> Tambah Bank/Cash Payment Voucher</a>'+
					'</div>');
			},
			"columnDefs": [{
				targets: [0],
				width: 10,
				className: 'dt-body-center'
			},{
				targets: [4],
				width: 160,
				className: 'dt-body-center'
			}]
		});
	}

	function add_payment(){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('payment_voucher_kas/add_payment');?>');
		$('#panel-modal  .panel-title').html('<i class="fa fa-plus"></i> Tambah Bank/Cash Payment Voucher');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}
	function add_coa_value(){
		$('#panel-modal-lv1').removeData('bs.modal');
		$('#panel-modal-lv1  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal-lv1  .panel-body').load('<?php echo base_url('payment_voucher_kas/add_coa_value');?>');
		$('#panel-modal-lv1  .panel-title').html('<i class="fa fa-plus"></i> Tambah Kas');
		$('#panel-modal-lv1').modal({backdrop:'static',keyboard:false},'show');
	}

	function edit_payment(id){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('payment_voucher_kas/edit_payment/');?>'+"/"+id);
		$('#panel-modal  .panel-title').html('<i class="fa fa-edit"></i> Edit Bank/Cash Payment Voucher');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}
	function detail_payment(id){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('payment_voucher_kas/detail_payment/');?>'+"/"+id);
		$('#panel-modal  .panel-title').html('<i class="fa fa-info"></i> Detail Bank/Cash Payment Voucher');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}
	function print_payment(id){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('payment_voucher_kas/print_payment/');?>'+"/"+id);
		$('#panel-modal  .panel-title').html('<i class="fa fa-print"></i> Print Bank/Cash Payment Voucher');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function delete_payment(id) {
		swal({
			title: 'Yakin akan Menghapus ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "Ya!",
			showCancelButton: true,
			cancelButtonClass: "btn-default",
			cancelButtonText: "Tidak!"
		}).then(function () {
			var datapost = {'id' : id};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>payment_voucher_kas/delete_payment",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
				   swal({
						title: 'Success!',
						text: response.message,
						type: 'success',
						showCancelButton: false,
						confirmButtonText: 'Ok'
					}).then(function () {
						window.location.href = "<?php echo base_url('payment_voucher_kas');?>";
					})

					if (response.status != "success") swal("Failed!", response.message, "error");
				}
			});
		})
	}
</script>