	<style>


		.dt-body-left {
			text-align: left;
			vertical-align: middle;
		}

		.dt-body-right {
			text-align: right;
			vertical-align: middle;
		}

		.dt-body-center {
			text-align: center;
			vertical-align: middle;
		}

		img {
			max-width: 85%;
			height: auto;
		}

		.nama-perusahaan-margin {
			margin-top: 30px;

		}

		.margin-ttd {
			margin-bottom: 65px
		}
	</style>

	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" data-toggle="tooltip" data-placement="top" title="Download Invoice" id="btn_download">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="col-md-2 logo-place">
				<img src="<?php echo site_url(); ?>assets/images/logo-celebit.jpg" alt="logo-celebit">
			</div>
			<div class="col-md-10 titleReport text-center">
				<div class="col-md-12 ">
					<h1 id="titleCelebit"></h1>
					<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
					<h4 id="titleAlamat">BUKTI PENGELUARAN KAS</h4>
					<h4 id="titleTlp">(CASH PAYMENT VOUCHER / CPV)</h4>
				</div>
			</div>
		</div>
	</div>
	<hr>

	<div class="row">
		<div class="col-md-12">
			<div class="col-md-8 col-customer">
				<div class="row">
					<div class="col-md-12">
						<label class="control-label" id="namaCustomer">Dibayar Kepada : <?php if (isset($pv[0]['nama_penerima'])) {
																																							echo $pv[0]['nama_penerima'];
																																						} ?></label><br>
						<label class="control-label" id="lokasiCustomer"></label><br>
						<label class="control-label" id="alamatCustomer"></label><br>
						<label class="control-label" id="divCustomer"></label><br>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-3" id="noInvoice1">No.</label>
						<label class="control-label col-md-1" id="noInvoice2">:</label>
						<input id="noInvoice3" value="<?php if (isset($pv[0]['no_pvk'])) {
																						echo $pv[0]['no_pvk'];
																					} ?>"></input>
						<!-- <label class="control-label col-md-8"  id="noInvoice3"><?php if (isset($invoice[0]['no_po'])) {
																																					echo $invoice[0]['no_po'];
																																				} ?></label> -->
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-3" id="tanggalInvoice1">Date</label>
						<label class="control-label col-md-1" id="tanggalInvoice2">:</label>
						<label class="control-label col-md-8" id="tanggalInvoice3">
							<?php if (isset($pv[0]['date_pvk'])) {
								echo date('d-M-Y', strtotime($pv[0]['date_pvk']));
							} ?>
						</label>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<label class="control-label col-md-3" id="dueDateInvoice1"></label>
						<label class="control-label col-md-1" id="dueDateInvoice2"></label>
						<label class="control-label col-md-8" id="dueDateInvoice3">
						</label>
					</div>
				</div>

			</div>
		</div>
	</div>

	<hr>
	<div class="row">
		<div class="col-md-12">
			<table id="listinvoice" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th style="text-align:right" width="20%">Nomor Akun</th>
						<th style="text-align:left" >Keterangan</th>
						<th style="text-align:right" width="20%">Nominal</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					$total = 0;
					foreach($coa as $coa_list)
					{?>
					<tr>
						<td style="text-align:right"><?php echo $coa_list['coa']?></td>
						<td style="text-align:left"><?php echo $coa_list['keterangan']?></td>
						<td style="text-align:right"><?php echo $coa_list['symbol']." ".$coa_list['value']?></td>
					</tr>	
					<?php 
					$total = $total +$coa_list['value'];
					}
					?>
					<tr>
						<td colspan="2"> <label id="label-terbilang-table"></td>
						<td style="text-align:right"><?php echo $total ?></td>
					</tr>
				</tbody>
			</table>
			<table id="listttd" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%" style="display:none">
				<thead>
					<tr>
						<th>dibukukan<br><br><br></th>
						<th>disetujui<br><br><br></th>
						<th>diperiksa<br><br><br></th>
						<th>disiapkan<br><br><br></th>
						<th>diterima<br><br><br></th>
					</tr>
				</thead>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
				Terbilang : <label id="label-terbilang"></label>
		</div>
	</div>

	<div class="row">
		<input type="hidden" id="totHide" name="totHide">
	</div>

	<script type="text/javascript">
		var dataImage = null;
		$(document).ready(function() {
			$('#label-terbilang-table').text(terbilang("<?php echo $total?>"));
			$('#label-terbilang').text(terbilang("<?php echo $total?>"));
			$('#btn_download').click(function() {
				var doc = new jsPDF('p', 'mm', 'letter');
				var imgData = dataImage;
				var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
				var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();

				// FOOTER
				doc.setTextColor(100);
				doc.addImage(imgData, 'JPEG', 5, 5, 25, 25)
				doc.setFontSize(12);
				doc.text($('#titleCelebit').html(), 33, 10, 'left');
				doc.setFontSize(10);
				doc.text($('#titlePerusahaan').html(), 33, 15, 'left');
				doc.setFontSize(11);
				doc.text($('#titleAlamat').html(), 33, 20, 'left');
				doc.setFontSize(11);
				doc.text($('#titleTlp').html(), 33, 25, 'left');

				doc.setDrawColor(116, 119, 122);
				doc.setLineWidth(0.1);
				doc.line(4, 30, 500, 30);

				doc.setFontSize(11);
				// doc.text($('#titleInvoice').html(), pageWidth / 2, 40, 'center');

				doc.setDrawColor(116, 119, 122);
				doc.setLineWidth(0.2);
				doc.line(4, 45, 110, 45);
				doc.setLineWidth(0.2);
				doc.line(4, 45, 4, 65);
				doc.setLineWidth(0.2);
				doc.line(110, 45, 110, 65);
				doc.setLineWidth(0.2);
				doc.line(4, 65, 110, 65);

				doc.setFontSize(7);
				doc.text($('#namaCustomer').html(), 6, 50);
				doc.text($('#lokasiCustomer').html(), 6, 55);
				doc.text($('#alamatCustomer').html(), 6, 60);
				doc.text($('#divCustomer').html(), 6, 65);

				doc.setFontSize(7);
				doc.text($('#noInvoice1').html(), 115, 50, 'left');
				doc.text($('#noInvoice2').html(), 128, 50, 'left');
				doc.text($('#noInvoice3').val(), 135, 50, 'left'); //dari inputan

				doc.text($('#tanggalInvoice1').html(), 115, 57, 'left');
				doc.text($('#tanggalInvoice2').html(), 128, 57, 'left');
				doc.text($('#tanggalInvoice3').html(), 96, 54, 'left');

				doc.text($('#dueDateInvoice1').html(), 115, 64, 'left');
				doc.text($('#dueDateInvoice2').html(), 128, 64, 'left');
				doc.text($('#dueDateInvoice3').html(), 96, 61, 'left');

				doc.autoTable({
					html: '#listinvoice',
					theme: 'plain',
					styles: {
						fontSize: 8,
						lineColor: [116, 119, 122],
						lineWidth: 0.1,
						cellWidth: 'auto',

					},
					margin: 4.5,
					tableWidth: (pageWidth - 10),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					didParseCell: function(data) {
						if (data.table.foot[0]) {
							if (data.table.foot[0].cells[4]) {
								data.table.foot[0].cells[4].styles.halign = 'right';
							}
							if (data.table.foot[0].cells[5]) {
								data.table.foot[0].cells[5].styles.halign = 'right';
							}
						}
					},
					columnStyles: {
						0: {
							tableWidth: 10,
							halign: 'right'
						},
						1: {
							tableWidth: 10,
							halign: 'left'
						},
						2: {
							halign: 'right'
						},
					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: 70
				});
				doc.autoTable({
					html: '#listttd',
					theme: 'plain',
					styles: {
						fontSize: 8,
						lineColor: [116, 119, 122],
						lineWidth: 0.1,
						cellWidth: 'auto',

					},
					margin: 4.5,
					tableWidth: (pageWidth - 20),
					headStyles: {
						valign: 'middle',
						halign: 'center',
					},
					columnStyles: {

					},
					rowPageBreak: 'auto',
					showHead: 'firstPage',
					showFoot: 'lastPage',
					startY: pageHeight * 65 / 100
				});
				var x = pageWidth * 70 / 100;
				var y = pageHeight * 85 / 100;
				doc.setFontSize(10);
				// doc.text("PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA ", x, y, 'center');
				// doc.text($('#penandaTangan').val(), x, y + 30, 'center');
				doc.save('Payment Voucher Kas  <?php echo $coa[0]['date']; ?>.pdf');
			});
		});

		function toDataURL(url, callback) {
			var xhr = new XMLHttpRequest();
			xhr.onload = function() {
				var reader = new FileReader();
				reader.onloadend = function() {
					callback(reader.result);
				}
				reader.readAsDataURL(xhr.response);
			};
			xhr.open('GET', url);
			xhr.responseType = 'blob';
			xhr.send();
		}

		toDataURL('<?php echo site_url(); ?>assets/images/logo-celebit.jpg', function(dataUrl) {
			//console.log('RESULT:', dataUrl)
			dataImage = dataUrl;
		})

		function terb_depan(uang) {
			var sub = '';
			if (uang == 1) {
				sub = 'Satu '
			} else
			if (uang == 2) {
				sub = 'Dua '
			} else
			if (uang == 3) {
				sub = 'Tiga '
			} else
			if (uang == 4) {
				sub = 'Empat '
			} else
			if (uang == 5) {
				sub = 'Lima '
			} else
			if (uang == 6) {
				sub = 'Enam '
			} else
			if (uang == 7) {
				sub = 'Tujuh '
			} else
			if (uang == 8) {
				sub = 'Delapan '
			} else
			if (uang == 9) {
				sub = 'Sembilan '
			} else
			if (uang == 0) {
				sub = '  '
			} else
			if (uang == 10) {
				sub = 'Sepuluh '
			} else
			if (uang == 11) {
				sub = 'Sebelas '
			} else
			if ((uang >= 11) && (uang <= 19)) {
				sub = terb_depan(uang % 10) + 'Belas ';
			} else
			if ((uang >= 20) && (uang <= 99)) {
				sub = terb_depan(Math.floor(uang / 10)) + 'Puluh ' + terb_depan(uang % 10);
			} else
			if ((uang >= 100) && (uang <= 199)) {
				sub = 'Seratus ' + terb_depan(uang - 100);
			} else
			if ((uang >= 200) && (uang <= 999)) {
				sub = terb_depan(Math.floor(uang / 100)) + 'Ratus ' + terb_depan(uang % 100);
			} else
			if ((uang >= 1000) && (uang <= 1999)) {
				sub = 'Seribu ' + terb_depan(uang - 1000);
			} else
			if ((uang >= 2000) && (uang <= 999999)) {
				sub = terb_depan(Math.floor(uang / 1000)) + 'Ribu ' + terb_depan(uang % 1000);
			} else
			if ((uang >= 1000000) && (uang <= 999999999)) {
				sub = terb_depan(Math.floor(uang / 1000000)) + 'Juta ' + terb_depan(uang % 1000000);
			} else
			if ((uang >= 100000000) && (uang <= 999999999999)) {
				sub = terb_depan(Math.floor(uang / 1000000000)) + 'Milyar ' + terb_depan(uang % 1000000000);
			} else
			if ((uang >= 1000000000000)) {
				sub = terb_depan(Math.floor(uang / 1000000000000)) + 'Triliun ' + terb_depan(uang % 1000000000000);
			}
			return sub;
		}

		function terb_belakang(t) {
			if (t.length == 0) {
				return '';
			}
			return t
				.split('0').join('Kosong ')
				.split('1').join('Satu ')
				.split('2').join('Dua ')
				.split('3').join('Tiga ')
				.split('4').join('Empat ')
				.split('5').join('Lima ')
				.split('6').join('Enam ')
				.split('7').join('Tujuh ')
				.split('8').join('Delapan ')
				.split('9').join('Dembilan ');
		}

		function terbilang(nangka) {
			var
				v = 0,
				sisa = 0,
				tanda = '',
				tmp = '',
				sub = '',
				subkoma = '',
				p1 = '',
				p2 = '',
				pkoma = 0;
			// nangka = nangka.replace(/[^\d.-]/g, '');
			if (nangka > 999999999999999999) {
				return 'Buset dah buanyak amat...';
			}
			v = nangka;
			if (v < 0) {
				tanda = 'Minus ';
			}
			v = Math.abs(v);
			tmp = v.toString().split('.');
			p1 = tmp[0];
			p2 = '';
			if (tmp.length > 1) {
				p2 = tmp[1];
			}
			v = parseFloat(p1);
			sub = terb_depan(v);
			/* sisa = parseFloat('0.'+p2);
			subkoma = terb_belakang(sisa); */
			subkoma = 'Koma ' + terb_belakang(p2).replace('  ', ' ');;
			if (subkoma = 'Koma ')
				subkoma = '';
			sub = tanda + sub.replace('  ', ' ') + subkoma;
			return sub.replace('  ', ' ') + " Rupiah";
		}
		function terbilangIND() {
			var bilangan = document.getElementById("totHide").value;
			var kalimat = terbilang(bilangan);
			// var angka = new Array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
			// var kata = new Array('', 'Satu', 'Dua', 'Tiga', 'Empat', 'Lima', 'Enam', 'Tujuh', 'Delapan', 'Sembilan');
			// var tingkat = new Array('', 'Ribu', 'Juta', 'Milyar', 'Triliun');
			// var panjang_bilangan = bilangan.length;

			// /* pengujian panjang bilangan */
			// if (panjang_bilangan > 15) {
			// 	kalimat = "Diluar Batas";
			// } else {
			// 	/* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
			// 	for (i = 1; i <= panjang_bilangan; i++) {
			// 		angka[i] = bilangan.substr(-(i), 1);
			// 	}

			// 	var i = 1;
			// 	var j = 0;

			// 	/* mulai proses iterasi terhadap array angka */
			// 	while (i <= panjang_bilangan) {
			// 		subkalimat = "";
			// 		kata1 = "";
			// 		kata2 = "";
			// 		kata3 = "";

			// 		/* untuk Ratusan */
			// 		if (angka[i + 2] != "0") {
			// 			if (angka[i + 2] == "1") {
			// 				kata1 = "Seratus";
			// 			} else {
			// 				kata1 = kata[angka[i + 2]] + " Ratus";
			// 			}
			// 		}

			// 		/* untuk Puluhan atau Belasan */
			// 		if (angka[i + 1] != "0") {
			// 			if (angka[i + 1] == "1") {
			// 				if (angka[i] == "0") {
			// 					kata2 = "Sepuluh";
			// 				} else if (angka[i] == "1") {
			// 					kata2 = "Sebelas";
			// 				} else {
			// 					kata2 = kata[angka[i]] + " Belas";
			// 				}
			// 			} else {
			// 				kata2 = kata[angka[i + 1]] + " Puluh";
			// 			}
			// 		}

			// 		/* untuk Satuan */
			// 		if (angka[i] != "0") {
			// 			if (angka[i + 1] != "1") {
			// 				kata3 = kata[angka[i]];
			// 			}
			// 		}

			// 		/* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
			// 		if ((angka[i] != "0") || (angka[i + 1] != "0") || (angka[i + 2] != "0")) {
			// 			subkalimat = kata1 + " " + kata2 + " " + kata3 + " " + tingkat[j] + " ";
			// 		}

			// 		/* gabungkan variabe sub kalimat (untuk Satu blok 3 angka) ke variabel kalimat */
			// 		kalimat = subkalimat + kalimat;
			// 		i = i + 3;
			// 		j = j + 1;
			// 	}

			// 	/* mengganti Satu Ribu jadi Seribu jika diperlukan */
			// 	if ((angka[5] == "0") && (angka[6] == "0")) {
			// 		kalimat = kalimat.replace("Satu Ribu", "Seribu");
			// 	}
			// }

			document.getElementById("totalBilangan").innerHTML = kalimat;
		}

		function terbilangENG() {
			var bilangan = document.getElementById("totHide").value;
			var kalimat = "";
			var angka = new Array('0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0');
			var kata = new Array('', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine');
			var kataBelas = new Array('', 'One', 'Twelve', 'Thir', 'Four', 'Fif', 'Six', 'Seven', 'Eigh', 'Nine');
			var Puluh = new Array('', 'One', 'Twen', 'Thir', 'For', 'Fif', 'Six', 'Seven', 'Eigh', 'Nine');
			var tingkat = new Array('', 'Thousand', 'Million', 'Billion', 'Trillion');
			var panjang_bilangan = bilangan.length;

			/* pengujian panjang bilangan */
			if (panjang_bilangan > 15) {
				kalimat = "Diluar Batas";
			} else {
				/* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
				for (i = 1; i <= panjang_bilangan; i++) {
					angka[i] = bilangan.substr(-(i), 1);
				}

				var i = 1;
				var j = 0;

				/* mulai proses iterasi terhadap array angka */
				while (i <= panjang_bilangan) {
					subkalimat = "";
					kata1 = "";
					kata2 = "";
					kata3 = "";

					/* untuk Ratusan */
					if (angka[i + 2] != "0") {
						if (angka[i + 2] == "1") {
							kata1 = "Hundred";
						} else {
							kata1 = kata[angka[i + 2]] + " Hundred";
						}
					}

					/* untuk Puluhan atau Belasan */
					if (angka[i + 1] != "0") {
						if (angka[i + 1] == "1") {
							if (angka[i] == "0") {
								kata2 = "Ten";
							} else if (angka[i] == "1") {
								kata2 = "Eleven";
							} else if (angka[i] == "2") {
								kata2 = "Twelve";
							} else {
								kata2 = kataBelas[angka[i]] + "teen";
							}
						} else {
							kata2 = kataPuluh[angka[i + 1]] + "ty";
						}
					}

					/* untuk Satuan */
					if (angka[i] != "0") {
						if (angka[i + 1] != "1") {
							kata3 = kata[angka[i]];
						}
					}

					/* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
					if ((angka[i] != "0") || (angka[i + 1] != "0") || (angka[i + 2] != "0")) {
						subkalimat = kata1 + " " + kata2 + " " + kata3 + " " + tingkat[j] + " ";
					}

					/* gabungkan variabe sub kalimat (untuk Satu blok 3 angka) ke variabel kalimat */
					kalimat = subkalimat + kalimat;
					i = i + 3;
					j = j + 1;
				}

				/* mengganti Satu Ribu jadi Seribu jika diperlukan */
				if ((angka[5] == "0") && (angka[6] == "0")) {
					kalimat = kalimat.replace("One Thousand", "Thousand");
				}
			}

			document.getElementById("totalBilangan").innerHTML = kalimat;
		}
	</script>