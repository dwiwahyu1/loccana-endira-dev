<style>
	#loading {
		position: absolute;
		background: #FFFFFF;
		opacity: 0.5;
		width: 93%;
		height: 85%;
		z-index: 3;
		text-align: center;
		display: none;
	}
</style>
<div id="loading"><img src="<?php echo base_url(); ?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/preloader.gif" /></div>
<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
<form class="form-horizontal form-label-left">
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id">Coa <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control select-control" id="level1" name="level1" style="width: 100%" required>
				<?php foreach ($level1 as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>"><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group" id="level2_temp">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control select-control" id="level2" name="level2" style="width: 100%">
				<?php foreach ($level2 as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>"><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group" id="level3_temp">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control select-control" id="level3" name="level3" style="width: 100%">
				<?php foreach ($level3 as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>"><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group" id="level4_temp">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="level4" name="level4" style="width: 100%">
				<?php foreach ($level4 as $key) { ?>
					<option value="<?php echo $key['id_coa']; ?>" data-coa="<?php echo $key['coa']; ?>"><?php echo $key['coa'].' - '.$key['keterangan']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<!-- <div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Tanggal
			<span class="required">
				<sup>*</sup>
			</span>
		</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<div class="input-group date">
				<input placeholder="Tanggal" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="date" name="date" required="required" value="<?php echo date('Y-m-d'); ?>">
				<div class="input-group-addon">
					<span class="glyphicon glyphicon-th"></span>
				</div>
			</div>
		</div>
	</div> -->

	<div class="item form-group" id="subdetail_temp">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id">Valas <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="id_valas" name="id_valas" style="width: 100%" required>
				<?php foreach ($valas as $key) { ?>
					<option value="<?php echo $key['valas_id']; ?>"><?php echo $key['nama_valas']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Rate <span class="required"><sup>*</sup></span>
		</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="number" id="rate" name="rate" min="1" class="form-control col-md-7 col-xs-12" placeholder="Rate" value="1" required="required">
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nilai <span class="required"><sup>*</sup></span>
		</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input data-parsley-maxlength="255" type="number" id="value" name="value" min="0" class="form-control col-md-7 col-xs-12" placeholder="Nilai" value="0" required="required">
		</div>
	</div>

	<!-- <div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id">Type <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="type_cash" name="type_cash" style="width: 100%" required>
				<option value="1">Pengeluaran</option>
				<option value="0">Pemasukan</option>
			</select>
		</div>
	</div> -->
	<input type="hidden" id="type_cash" name="type_cash" value="1">
	<div class="item form-group form-item">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="keterangan">Catatan</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<textarea data-parsley-maxlength="255" type="text" id="note" name="note" class="form-control col-md-7 col-xs-12" placeholder="Catatan"></textarea>
		</div>
	</div>

	<div class="item form-group has-feedback">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="file_kas">File Bukti</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="file" class="form-control" id="file_kas" name="file_kas" data-height="110" accept=".pdf, .txt, .doc, .docx" />
			<span id="file_bc_tick">Hanya format file pdf,txt,doc,docx dengan besaran max 10Mb yang diterima.</span>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit-kas" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah KAS</button>
		</div>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	function coa(level, id) {
		$('#loading').show();
		$.ajax({
			type: 'GET',
			url: "<?php echo base_url(); ?>payment_voucher_kas/change_coa",
			data: {
				id: id
			},
			success: function(response) {
				if (response.length) {
					var element = '';
					$.each(response, function(key, val) {
						element += '<option value="' + val.id_coa + '" data-coa="' + val.coa + '">' + val.keterangan + '</option>';
					});

					if (level === 'level1') {
						$('#level2').html(element);
						$('#level2_temp').show();

						coa('level2', response[0].coa);
					} else if (level === 'level2') {
						$('#level3').html(element);
						$('#level3_temp').show();

						coa('level3', response[0].coa);
					} else {
						$('#level4').html(element);
						$('#level4_temp').show();
						$('#loading').hide();
					}
				} else {
					if (level === 'level1') {
						$('#level2_temp,#level3_temp,#level4_temp').hide();
					} else if (level === 'level2') {
						$('#level3_temp,#level4_temp').hide();
					} else {
						$('#level4_temp').hide();
					}
					$('#loading').hide();
				}
			}
		});
	}
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();

		$('#level1').select2();
		$('#level2').select2();
		$('#level3').select2();
		$('#level4').select2();
		
		// $("#date").datepicker({
		// 	format: 'yyyy-mm-dd',
		// 	autoclose: true,
		// 	todayHighlight: true,
		// });

		$('.select-control').on('change', (function(e) {
			var id = $(this).children('option:selected').data('coa'),
				level = $(this).attr('id');

			coa(level, id);
		}));
	});

	$('#btn-submit-kas').on('click', (function(e) {
		console.log(delete_kas_id);
		delete_kas_item(delete_kas_id);
		$('#btn-submit-kas').attr('disabled', 'disabled');
		$('#btn-submit-kas').text("Memasukkan data...");
		e.preventDefault();
		// var formData = new FormData();
		var id_coa = '0',
			id_parent = $('#level1').val(),
			coa_number = $('#level1 option:selected').data("coa"),
			text_coa = $('#level1 option:selected').text();

		if ($('#level4_temp').is(':visible')) {
			id_coa = $('#level4').val();
			text_coa = $('#level4 option:selected').text();
			coa_number = $('#level4 option:selected').data("coa");
		} else if ($('#level3_temp').is(':visible')) {
			id_coa = $('#level3').val();
			text_coa = $('#level3 option:selected').text();
			coa_number = $('#level3 option:selected').data("coa");
		} else if ($('#level2_temp').is(':visible')) {
			id_coa = $('#level2').val();
			text_coa = $('#level2 option:selected').text();
			coa_number = $('#level2 option:selected').data("coa");
		} else id_coa = id_parent;
		var temp_kas_data = {
			id_coa: id_coa,
			text_coa: text_coa,
			coa_number: coa_number,
			id_parent: id_parent,
			// date: $('#date').val(),
			id_valas: $('#id_valas').val(),
			text_valas: $('#id_valas option:selected').text(),
			value: $('#value').val(),
			rate: $('#rate').val(),
			type_cash: $('#type_cash').val(),
			type_cash_text: $('#type_cash option:selected').text(),
			note: $('#note').val(),
		}
		formData.append('id_coa[]', temp_kas_data.id_coa);
		formData.append('text_coa[]', temp_kas_data.text_coa);
		formData.append('coa_number[]', temp_kas_data.coa_number);
		formData.append('id_parent[]', temp_kas_data.id_parent);
		// formData.append('date[]', temp_kas_data.date);
		formData.append('id_valas[]', temp_kas_data.id_valas);
		formData.append('value[]', temp_kas_data.value);
		formData.append('rate[]', temp_kas_data.rate);
		formData.append('type_cash[]', temp_kas_data.type_cash);
		formData.append('type_cash_text[]', temp_kas_data.type_cash_text);
		formData.append('note[]', temp_kas_data.note);
		formData.append('have_file[]', "new");
		if ($('#file_kas')[0].files.length > 0) {
			if ($('#file_kas')[0].files[0].size <= 9437184) {
				formData.append('file_kas[]', $('#file_kas')[0].files[0], $('#file_kas')[0].files[0].name);
				temp_kas_data.file = new Blob();
				temp_kas_data.file_name = $('#file_kas')[0].files[0].name;

			} else {
				$('#btn-submit-kas').removeAttr('disabled');
				$('#btn-submit-kas').text("Tambah KAS");
				swal("Failed!", "Ukuran File Terlalu besar, silahkan cek kembali", "error");
			}
		} else {
			formData.append('file_kas[]', new Blob, '');
			temp_kas_data.file = new Blob;
			temp_kas_data.file_name = '';
		}

		add_kas_temp_data(temp_kas_data);
		$('#panel-modal-lv1 .panel-heading button').trigger('click');
	}));
</script>