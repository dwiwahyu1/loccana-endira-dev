<form class="form-horizontal form-label-left" id="add_form" role="form" action="<?php echo base_url('payment_voucher_kas/save_payment'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tgl_bpvk">Tanggal <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="tgl_bpvk" name="tgl_bpvk" class="form-control" placeholder="Tanggal Payment Voucher" autocomplete="off" required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Kas : </label>
		<div class="col-md-8 col-sm-6 col-xs-12 add_item" onclick="add_kas()">
			<a type="button" class="btn btn-default btn-icon waves-effect waves-light m-b-5">
				<i class="fa fa-plus"></i>
			</a>
			Tambah Kas
		</div>
	</div>
	<div class="item form-group">
		<div class="col-md-12" style="padding-left: 50px; padding-right: 50px;">
			<table id="list_barang" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th style="width: 5%;">No</th>
						<th>Perkiraan</th>
						<th>COA</th>
						<th>Keterangan</th>
						<th>Valas</th>
						<th>Nilai</th>
						<th>Tipe</th>
						<th style="width: 5%">Option</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
				<!-- <tfoot>
					<tr>
						<th colspan="3" style="text-align: right;">Total</th>
						<td id="tdFootTotal">0</td>
					</tr>
				</tfoot> -->
			</table>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="jumlah">Jumlah</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" id="jumlah" name="jumlah" class="form-control" placeholder="Jumlah" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group" id="div_list_bank">
		<table id="list_bank" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th style="width: 35%;">Bank</th>
					<th>No Rekening</th>
					<th>Nominal</th>
					<td style="width: 5%;"><a id="btn_bank_add" class="btn btn-primary"><i class="fa fa-plus"></i></a></td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<input type="text" class="form-control" id="bank1" name="bank[]" placeholder="Nama Bank" autocomplete="off" required>
					</td>
					<td>
						<input type="number" class="form-control" id="no_rek1" name="no_rek[]" placeholder="No Rekening" autocomplete="off" required>
					</td>
					<td>
						<input type="number" class="form-control" id="nominal_bank1" name="nominal_bank[]" value="0" placeholder="Nominal" autocomplete="off" required>
					</td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Payment Voucher</button>
		</div>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	var items = [];
	var idRowBank = 1;
	var arrBank = [];
	var arrRek = [];
	var arrNominal = [];
	$(document).ready(function() {
		$('#tgl_bpvk').datepicker({
			format: "d-M-yyyy",
			autoclose: true,
			todayHighlight: true
		});

		$('#btn_bank_add').on('click', function() {
			idRowBank++;
			$('#list_bank tbody').append(
				'<tr id="trRowBank' + idRowBank + '">' +
				'<td>' +
				'<input type="text" class="form-control" id="bank' + idRowBank + '" name="bank[]" placeholder="Nama Bank" autocomplete="off" required>' +
				'</td>' +
				'<td>' +
				'<input type="number" class="form-control" id="no_rek' + idRowBank + '" name="no_rek[]" placeholder="No Rekening" autocomplete="off" required>' +
				'</td>' +
				'<td>' +
				'<input type="number" class="form-control" id="nominal_bank' + idRowBank + '" name="nominal_bank[]" value="0" placeholder="Nominal" autocomplete="off">' +
				'</td>' +
				'<td id="td_btn">' +
				'<a class="btn btn-danger" onclick="removeRowBank(' + idRowBank + ')"><i class="fa fa-minus"></i></a>' +
				'</td>' +
				'</tr>'
			);
		});
	});

	function draw_kas_table() {
		var itemslen = items.length;
		var element = '';
		var totalValue = 0;
		var text_number=1;
		for (var i = 0; i < itemslen; i++) {
			element += '   <tr>' +
				'<tr id="trRowBank' + idRowBank + '">' +
				'<td>' + text_number++ + '</td>' +
				'<td>' + items[i]["text_coa"] + '</td>' +
				'<td>' + items[i]["coa_number"] + '</td>' +
				'<td>' + items[i]["note"] + '</td>' +
				// '<td>' + items[i]["date"] + '</td>' +
				'<td>' + items[i]["text_valas"] + '</td>' +
				'<td>' + items[i]["value"] + '</td>' +
				'<td>' + items[i]["type_cash_text"] + '</td>' +
				'<td>' +
				'  <div class="btn-group">' +
				'    <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="delete_kas_item(' + i + ')">' +
				'    <i class="fa fa-trash"></i>' +
				'  </div>' +
				'</td>' +
				'</tr>';
			totalValue = totalValue + parseInt(items[i]["value"]);
		}
		$('#list_barang tbody').html(element);
		$('#jumlah').val(totalValue);
	}

	function add_kas_temp_data(formData) {
		items.push(formData);
		draw_kas_table();
	}
	
	function removeRowBank(rowBank) {
		$('#trRowBank'+rowBank).remove();
	}
	function delete_kas_item(rowBank) {
		items.splice(rowBank, 1);
		draw_kas_table();
	}

	$('#add_form').on('submit', (function(e) {
		arrBank = [];
		arrRek = [];
		arrNominal = [];
		$('#btn-submit').attr('disabled', 'disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();

		$('input[name="bank[]"]').each(function() {
			if (this.value) {
				if (this.value != undefined && this.value != '') arrBank.push(this.value);
			}
		})

		$('input[name="no_rek[]"]').each(function() {
			if (this.value) {
				if (this.value != undefined && this.value != '') arrRek.push(this.value);
			}
		})

		$('input[name="nominal_bank[]"]').each(function() {
			if (this.value) {
				if (this.value != undefined && this.value != '' && this.value > 0) arrNominal.push(this.value);
			}
		})

		if (arrBank.length > 0 && arrRek.length > 0 && arrNominal.length > 0) {
			var formData = new FormData();
			formData.set('tgl_bpvk', $('#tgl_bpvk').val());
			formData.set('jumlah', $('#jumlah').val());
			formData.set('arrBank', arrBank);
			formData.set('arrRek', arrRek);
			formData.set('arrNominal', arrNominal);
			var sendData={
				tgl_bpvk:$('#tgl_bpvk').val(),
				jumlah:$('#jumlah').val(),
				arrBank:arrBank,
				arrRek:arrRek,
				arrNominal:arrNominal,
				kasData:items
			};
			console.log(sendData);
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: JSON.stringify(sendData),
				cache: false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function() {
							window.location.href = "<?php echo base_url('payment_voucher_kas'); ?>";
						})
					} else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Tambah Payment Voucher");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Tambah Payment Voucher");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		} else {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah Payment Voucher");
			swal("Failed!", "Invalid Inputan Bank, silahkan cek kembali.", "error");
		}
	}));
</script>