<form class="form-horizontal form-label-left" id="add_form" role="form" action="<?php echo base_url('payment_voucher_kas/save_payment_backup'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>
	<div class="item form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12" for="tgl_bpvk">No Voucher <span class="required"><sup>*</sup></span></label>
		<div class="col-md-9 col-sm-6 col-xs-12 input-group ">
			<input type="text" id="no_pvk" name="no_pvk" class="form-control" placeholder="No. BPV/CPV" autocomplete="off" value="<?php if (isset($no_voucher)) {
																																																															echo $no_voucher;
																																																														} ?>" >
		</div>
	</div>
	<div class="item form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12" for="tgl_bpvk">Tanggal <span class="required"><sup>*</sup></span></label>

		<div class="col-md-9 col-sm-6 col-xs-12 input-group date">
			<input placeholder="Tanggal" type="text" class="form-control datepicker" id="tgl_bpvk" name="tgl_bpvk" required="required" value="<?php echo date('Y-m-d'); ?>">
			<div class="input-group-addon">
				<span class="glyphicon glyphicon-th"></span>
			</div>
		</div>
	</div>
	<div class="item form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12" for="nama_penerima">Nama Penerima <span class="required"><sup>*</sup></span></label>

		<div class="col-md-9 col-sm-6 col-xs-12 input-group ">
			<input placeholder="Nama Penerima" type="text" class="form-control" id="nama_penerima" name="nama_penerima" required="required" >
		</div>
	</div>
	<div class="item form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12" for="nama">Daftar Kas : </label>
		<div class="col-md-9 col-sm-6 col-xs-12 add_item" onclick="add_coa_value()">
			<a type="button" class="btn btn-default btn-icon waves-effect waves-light m-b-5">
				<i class="fa fa-plus"></i>
			</a>
			Tambah Kas
		</div>
	</div>
	<div class="item form-group">
		<div class="col-md-9 col-md-offset-2">
			<table id="list_barang" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th style="width: 5%;">No</th>
						<th>Perkiraan</th>
						<th>COA</th>
						<th>Keterangan</th>
						<th>Valas</th>
						<th>Nilai</th>
						<th>Tipe</th>
						<th style="width: 5%">Option</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12" for="jumlah">Jumlah</label>
		<div class="col-md-9 col-sm-6 col-xs-12">
			<input type="text" id="jumlah" name="jumlah" class="form-control" placeholder="Jumlah" autocomplete="off" readonly>
		</div>
	</div>

	<div class="item form-group" id="div_list_bank">
		<div class="col-md-9 col-md-offset-2">
			<table id="list_bank" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
				<thead>
					<th>Bank / Cash</th>
					<th>Rate</th>
					<th>Nilai</th>
					<th style="width: 5%;"><a id="btn_bank_add" class="btn btn-primary"><i class="fa fa-plus"></i></a></td>
				</thead>
				<tbody>
					<tr id="trRowBank1">
						<td>
							<select class="form-control" id="ddl_bank1" name="ddl_bank[]" style="width: 100%" required>
								<?php foreach ($list_bank as $key) { ?>
									<option value='<?php echo $key['id_coa']; ?>'>
										<?php echo $key['nama_bank'] . "-" . $key['no_rek'] . "-" . $key['nama_valas']; ?>
									</option>
								<?php } ?>
							</select>
						</td>
						<td>
							<input type="number" class="form-control" id="rate1" name="rate[]" value="1" placeholder="Rate" autocomplete="off" required>
						</td>
						<td>
							<input type="number" class="form-control" id="nominal_bank1" name="nominal_bank[]" value="0" placeholder="Nominal" autocomplete="off" required>
						</td>
						<td>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-2 col-sm-2 col-xs-12" for="submit"></label>
		<div class="col-md-9 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Payment Voucher</button>
		</div>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	var formData = new FormData();
	var items = [];
	var idRowBank = 1;
	var arrBank = [];
	var arrRek = [];
	var arrNominal = [];
	$(document).ready(function() {
		$(".date").datepicker({
			format: 'yyyy-mm-dd',
			autoclose: true,
			todayHighlight: true,
		});

		$('#btn_bank_add').on('click', function() {
			idRowBank++;
			$('#list_bank tbody').append(
				'<tr id="trRowBank' + idRowBank + '">' +
				'<td>' +
				'<select class="form-control" id="ddl_bank' + idRowBank + '" name="ddl_bank[]" style="width: 100%" required>' +
				<?php foreach ($list_bank as $key) { ?> '<option value="' +
					'<?php
							echo
								$key['id_coa'];
							?>' + '">' + '<?php echo $key['nama_bank'] . "-" . $key['no_rek'] . "-" . $key['nama_valas']; ?>' + '</option>' +
				<?php } ?> '</select>' +
				'</td>' +
				// '<td>' +
				// '<div class="input-group date">' +
				// '<input placeholder="Tanggal" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="date' + idRowBank + '" name="date[]" required="required" value="<?php echo date('Y-m-d'); ?>">' +
				// '<div class="input-group-addon">' +
				// '<span class="glyphicon glyphicon-th"></span>' +
				// '</div>' +
				// '</div>' +
				// '</td>' +
				'<td>' +
				'<input type="number" class="form-control" id="rate' + idRowBank + '" name="rate[]" value="1" placeholder="Rate" autocomplete="off" required>' +
				'</td>' +
				'<td>' +
				'<input type="number" class="form-control" id="nominal_bank' + idRowBank + '" name="nominal_bank[]" value="0" placeholder="Nominal" autocomplete="off">' +
				'</td>' +
				'<td id="td_btn">' +
				'<a class="btn btn-danger" onclick="removeRowBank(' + idRowBank + ')"><i class="fa fa-minus"></i></a>' +
				'</td>' +
				'</tr>'
			);

			$(".date").datepicker({
				format: 'yyyy-mm-dd',
				autoclose: true,
				todayHighlight: true,
			});
		});
	});

	function draw_kas_table() {
		var itemslen = items.length;
		var element = '';
		var totalValue = 0;
		var text_number = 1;
		for (var i = 0; i < itemslen; i++) {
			element += '   <tr>' +
				'<tr id="trRowBank' + idRowBank + '">' +
				'<td>' + text_number++ + '</td>' +
				'<td>' + items[i]["text_coa"] + '</td>' +
				'<td>' + items[i]["coa_number"] + '</td>' +
				'<td>' + items[i]["note"] + '</td>' +
				// '<td>' + items[i]["date"] + '</td>' +
				'<td>' + items[i]["text_valas"] + '</td>' +
				'<td>' + items[i]["value"] + '</td>' +
				'<td>' + items[i]["type_cash_text"] + '</td>' +
				'<td>' +
				'  <div class="btn-group">' +
				'    <button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="delete_kas_item(' + i + ')">' +
				'    <i class="fa fa-trash"></i>' +
				'  </div>' +
				'</td>' +
				'</tr>';
			totalValue = totalValue + parseInt(items[i]["value"]);
		}
		$('#list_barang tbody').html(element);
		$('#jumlah').val("RP. "+formatCurrencyComa(totalValue));
	}

	function add_kas_temp_data(itemsKas) {
		items.push(itemsKas);
		draw_kas_table();
	}

	function removeRowBank(rowBank) {
		$('#trRowBank' + rowBank).remove();
	}

	function delete_kas_item(rowBank) {
		items.splice(rowBank, 1);
		console.log(items);
		formData = new FormData();
		for (i = 0; i < formData.length; i++) {
			formData.append('id_coa[]', items[i].id_coa);
			formData.append('text_coa[]', items[i].text_coa);
			formData.append('coa_number[]', items[i].coa_number);
			formData.append('id_parent[]', items[i].id_parent);
			// formData.append('date[]', items[i].date);
			formData.append('id_valas[]', items[i].id_valas);
			formData.append('value[]', items[i].value);
			formData.append('rate[]', items[i].rate);
			formData.append('type_cash[]', items[i].type_cash);
			formData.append('type_cash_text[]', items[i].type_cash_text);
			formData.append('note[]', items[i].note);
			formData.append('file_kas[]', items[i].file, items[i].file_name);
		}

		draw_kas_table();
	}

	$('#add_form').on('submit', (function(e) {
		arrBank = [];
		// arrDate = [];
		arrRate = [];
		arrNominal = [];
		formData.delete('arrBank[]', '');
		// formData.delete('arrDate[]', '');
		formData.delete('arrRate[]', '');
		formData.delete('arrNominal[]', '');
		$('#btn-submit').attr('disabled', 'disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();

		// $('input[name="bank[]"]').each(function() {
		// 	if (this.value) {
		// 		if (this.value != undefined && this.value != '') arrBank.push(this.value);
		// 	}
		// })

		$('select[name="ddl_bank[]"] option:selected').each(function() {
			if (this.value) {
				if (this.value != undefined && this.value != '') {
					formData.append('arrBank[]', this.value);
					arrBank.push(this.value);
				}
			}
		})

		// $('input[name="date[]"]').each(function() {
		// 	if (this.value) {
		// 		if (this.value != undefined && this.value != '') {
		// 			formData.append('arrDate[]', this.value);
		// 			arrDate.push(this.value);
		// 		}
		// 	}
		// })
		$('input[name="rate[]"]').each(function() {
			if (this.value) {
				if (this.value != undefined && this.value != '') {
					formData.append('arrRate[]', this.value);
					arrRate.push(this.value);
				}
			}
		})

		$('input[name="nominal_bank[]"]').each(function() {
			if (this.value) {
				if (this.value != undefined && this.value != '' && this.value > 0) {
					formData.append('arrNominal[]', this.value);
					arrNominal.push(this.value);
				}
			}
		})
		if (arrBank.length > 0 && arrRate.length > 0 && arrNominal.length > 0) {
			formData.set('tgl_bpvk', $('#tgl_bpvk').val());
			formData.set('jumlah', $('#jumlah').val());
			formData.set('nama_penerima', $('#nama_penerima').val());
			formData.set('no_pvk', $('#no_pvk').val());
			// formData.append('arrBank', arrBank);
			// formData.append('arrDate', arrDate);
			// formData.append('arrRate', arrRate);
			// formData.append('arrNominal', arrNominal);
			var sendData = {
				tgl_bpvk: $('#tgl_bpvk').val(),
				jumlah: $('#jumlah').val(),
				arrBank: arrBank,
				arrRek: arrRek,
				arrNominal: arrNominal,
				kasData: items
			};
			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: formData,
				cache: false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function() {
							window.location.href = "<?php echo base_url('payment_voucher_kas'); ?>";
						})
					} else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Tambah Payment Voucher");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Tambah Payment Voucher");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		} else {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Tambah Payment Voucher");
			swal("Failed!", "Invalid Inputan Bank, silahkan cek kembali.", "error");
		}
	}));
</script>