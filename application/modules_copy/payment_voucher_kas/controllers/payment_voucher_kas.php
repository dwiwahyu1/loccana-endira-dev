<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class payment_voucher_kas extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('payment_voucher_kas/payment_voucher_kas_model');
		$this->load->model('jurnal/jurnal_model');
		$this->load->library('log_activity');
		$this->load->library('sequence');
	}
	/**
	 * upload file
	 * @return string
	 */
	public function upload($data)
	{
		$this->load->library('upload');
		$config = array(
			'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/payment_voucher_kas",
			'upload_url' => base_url() . "uploads/payment_voucher_kas",
			'encrypt_name' => TRUE,
			'overwrite' => FALSE,
			'max_size' => '10000000',
			''
		);

		$this->upload->initialize($config);

		$this->upload->do_upload($data);
		// General result data
		$result = $this->upload->data();
		// Add our stuff
		$img = $result['file_name'];

		return $img;
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string)
	{
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index()
	{
		$this->template->load('maintemplate', 'payment_voucher_kas/views/index');
	}

	function list_payment()
	{
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

		$order_fields = array('', 'no_pvk', 'date_pvk');
		$search = $this->input->get_post('search');
		$search_val = (!empty($search['value'])) ? $search['value'] : null;
		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		if($params['limit'] < 0) $params['limit'] = $this->payment_voucher_kas_model->count_payment($params);

		$list = $this->payment_voucher_kas_model->list_payment($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = $params['offset'] + 1;
		foreach ($list['data'] as  $v) {
			$strBtn =
				'<div class="btn-group">' .
				'<a class="btn btn-info btn-sm" title="Detail" onClick="detail_payment(\'' . $v['id_vk'] . '\')">' .
				'<i class="fa fa-info"></i>' .
				'</a>' .
				'</div>' .
				'<div class="btn-group">' .
				'<a class="btn btn-info btn-sm" title="Edit" onClick="edit_payment(\'' . $v['id_vk'] . '\')">' .
				'<i class="fa fa-edit"></i>' .
				'</a>' .
				'</div>' .
				'<div class="btn-group">' .
				'<a class="btn btn-info btn-sm" title="Print" onClick="print_payment(\'' . $v['id_vk'] . '\')">' .
				'<i class="fa fa-print"></i>' .
				'</a>' .
				'</div>' .
				'<div class="btn-group">' .
				'<a class="btn btn-danger btn-sm" title="Delete" onClick="delete_payment(\'' . $v['id_vk'] . '\')">' .
				'<i class="fa fa-trash"></i>' .
				'</a>' .
				'</div>';
			array_push($data, array(
				$i++,
				$v['name_coa'],
				$v['no_pvk'],
				date('d-M-Y', strtotime($v['date_pvk'])),
				// $v['symbol_valas'] . ' ' . number_format(round($v['nilai']), 0, ',', '.'),
				$strBtn
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	public function change_coa()
	{
		$id = $this->input->get('id');
		$result = $this->jurnal_model->coa("id_parent", $id);

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	public function add_coa_value()
	{
		$level1 = $this->jurnal_model->coa("id_parent", 0);
		$level2 = $this->jurnal_model->coa("id_parent", $level1[0]["coa"]);
		$level3 = $this->jurnal_model->coa("id_parent", $level2[0]["coa"]);
		$level4 = $this->jurnal_model->coa("id_parent", $level3[0]["coa"]);

		$valas = $this->jurnal_model->valas();

		$data = array(
			'level1' => $level1,
			'level2' => $level2,
			'level3' => $level3,
			'level4' => $level4,
			'valas' => $valas
		);

		$this->load->view('add_modal_kas_backup', $data);
		// $this->load->view('add_modal_kas', $data);
	}
	public function edit_coa_value()
	{
		$level1 = $this->jurnal_model->coa("id_parent", 0);
		$level2 = $this->jurnal_model->coa("id_parent", $level1[0]["coa"]);
		$level3 = $this->jurnal_model->coa("id_parent", $level2[0]["coa"]);
		$level4 = $this->jurnal_model->coa("id_parent", $level3[0]["coa"]);

		$valas = $this->jurnal_model->valas();

		$data = array(
			'level1' => $level1,
			'level2' => $level2,
			'level3' => $level3,
			'level4' => $level4,
			'valas' => $valas
		);

		$this->load->view('edit_modal_kas', $data);
		// $this->load->view('add_modal_kas', $data);
	}
	public function add_payment()
	{
		$list_bank = $this->payment_voucher_kas_model->list_bank();
		$data = array(
			'no_voucher' => $this->sequence->get_max_no('voucher_bank'),
			'list_bank' => $list_bank
		);
		$this->load->view('add_modal_view_backup', $data);
	}

	public function save_payment_backup()
	{
		// $no_voucher 	= $this->sequence->get_no('voucher_bank');
		$no_voucher 	= ($this->input->post('no_pvk', TRUE));
		$id_coa			= ($this->input->post('id_coa', TRUE));
		$id_parent		= ($this->input->post('id_parent', TRUE));
		$date 			= ($this->input->post('date', TRUE));
		$id_valas 		= ($this->input->post('id_valas', TRUE));
		$value 			= ($this->input->post('value', TRUE));
		$rate 			= ($this->input->post('rate', TRUE));
		$type_cash 		= ($this->input->post('type_cash', TRUE));
		$note 			= ($this->input->post('note', TRUE));
		$tgl_bpvk 			= ($this->input->post('tgl_bpvk', TRUE));
		$jumlah 			= ($this->input->post('jumlah', TRUE));
		$nama_penerima 			= ($this->input->post('nama_penerima', TRUE));


		$upload_error 	= NULL;
		$file_kas	= array();

		// echo "<pre>";print_r($file_kas);echo"</pre>";die;
		for ($i = 0; $i < sizeof($_FILES['file_kas']['name']); $i++) {
			$file_kas[$i]['file_kas']['name'] = $_FILES['file_kas']['name'][$i];
			if ($_FILES['file_kas']['name'][$i]) $file_kas[$i]['file_kas']['final_name'] = 'uploads/payment_voucher_kas/' . $_FILES['file_kas']['name'][$i];
			else 	$file_kas[$i]['file_kas']['final_name'] = $_FILES['file_kas']['name'][$i];
			$file_kas[$i]['file_kas']['type'] = $_FILES['file_kas']['type'][$i];
			$file_kas[$i]['file_kas']['tmp_name'] = $_FILES['file_kas']['tmp_name'][$i];
			$file_kas[$i]['file_kas']['error'] = $_FILES['file_kas']['error'][$i];
			$file_kas[$i]['file_kas']['size'] = $_FILES['file_kas']['size'][$i];
		}

		for ($i = 0; $i < sizeof($file_kas); $i++) {
			if (sizeof($file_kas) > 0) {
				if ($file_kas[$i]['file_kas']['name']) {
					if (!move_uploaded_file($file_kas[$i]['file_kas']['tmp_name'], $file_kas[$i]['file_kas']['final_name'])) {
						$pesan = "Gagal Upload File!";
						$upload_error = strip_tags(str_replace("\n", '', $pesan));

						$result = array('success' => false, 'message' => $upload_error);
					}
				}
			}
		}
		$result_payment = array();
		$result_coa_value = array();
		$id_masuk = array();
		$id_keluar = array();
		if (!isset($upload_error)) {
			$data = array(
				'no_voucher'	=> $no_voucher,
				'tgl_bpvk'		=> date('Y-m-d', strtotime($tgl_bpvk)),
				'jumlah'		=> preg_replace('/\D/', '', $jumlah),
				'nama_penerima' => $nama_penerima
			);
			$result_payment = $this->payment_voucher_kas_model->add_payment($data);
			if ($result_payment) {
				$this->log_activity->insert_activity('insert', 'Insert Bank Payment Voucher Kas No Voucher : ' . $no_voucher);

				$tempbank 			= ($this->input->post('arrBank', TRUE));
				$tempdate 			= ($this->input->post('arrDate', TRUE));
				$temprate 			= ($this->input->post('arrRate', TRUE));
				$tempnominal 		= ($this->input->post('arrNominal', TRUE));

				for ($i = 0; $i < sizeof($tempbank); $i++) {
					$result_list_bank[$i] = $this->payment_voucher_kas_model->list_bank($tempbank[$i])[0];
				}
				for ($i = 0; $i < sizeof($result_list_bank); $i++) {
					$dataBank = array(
						'no_rek'	=> $result_list_bank[$i]['no_rek'],
						'nama_bank'	=> $result_list_bank[$i]['id_bank'],
						'id_vk' 	=> $result_payment['lastid'],
						'nominal'	=> $tempnominal[$i]
					);
					$result_bank[$i] = $this->payment_voucher_kas_model->add_payment_bank($dataBank);
					$status = true;
					for ($i = 0; $i < sizeof($result_bank); $i++) {
						if ($result_bank[$i] <= 0) $status = false;
						if ($status) $this->log_activity->insert_activity('insert', 'Insert List Bank Payment Voucher Kas Id : ' . $tempbank[$i]);
						else $this->log_activity->insert_activity('insert', 'Gagal Insert List Bank Payment Voucher Kas Id = ' . $tempbank[$i]);

						$this->log_activity->insert_activity('insert', 'Insert Bank Payment Voucher Kas No Voucher : ' . $no_voucher);
					}
				}

				for ($i = 0; $i < sizeof($id_coa); $i++) {
					$data =  array(
						'id_coa'		=> $id_coa[$i],
						'id_parent'	=> $id_parent[$i],
						'id_valas'	=> $id_valas[$i],
						'value'			=> $value[$i],
						'date'			=> date($date[$i]),
						'type_cash'	=> $type_cash[$i],
						'note'			=> $note[$i],
						'rate'			=> $rate[$i],
						'bukti'			=> $file_kas[$i]['file_kas']['final_name'],
						'pic'				=> $this->session->userdata['logged_in']['user_id']
					);
					$result_coa_value[$i] = $this->payment_voucher_kas_model->add_coa_value($data);
					$id_keluar[$i] = $result_coa_value[$i]['lastid'];
				}
				for ($j = 0; $j < sizeof($result_list_bank); $j++) {
					$data =  array(
						'id_coa'		=> $result_list_bank[$j]['id_coa'],
						'id_parent'	=> $result_list_bank[$j]['id_parent'],
						'id_valas'	=> $result_list_bank[$j]['id_valas'],
						'value'			=> $tempnominal[$j],
						'date'			=> date($tempdate[$j]),
						'type_cash'	=> 0, //auto pemasukan
						'note'			=> '',
						'rate'			=> $temprate[$j],
						'bukti'			=> '',
						'pic'				=> $this->session->userdata['logged_in']['user_id']
					);

					$result_coa_value[$i + $j] = $this->payment_voucher_kas_model->add_coa_value($data);
					$id_masuk[$j] = $result_coa_value[$i + $j]['lastid'];
				}
				$status = true;
				for ($i = 0; $i > sizeof($result_coa_value); $i++) {
					if ($result_coa_value[$i]['result'] <= 0) $status = false;
				}
				if ($status > 0) $this->log_activity->insert_activity('insert', 'Menambahkan kas ke database');
				else $this->log_activity->insert_activity('insert', 'Gagal Menambahkan kas ke database ');


				for ($i = 0; $i < sizeof($result_coa_value); $i++) {
					$dataVoucherCoa = array(
						"id_coa" => $result_coa_value[$i]['lastid'],
						"id_pvk" => $result_payment['lastid']
					);
					$result_voucher_coa[$i] = $this->payment_voucher_kas_model->add_voucher_coa($dataVoucherCoa);
					if ($result_voucher_coa[$i] > 0) $result = array('success' => true, 'message' => 'Berhasil menambahkan  Voucher  COA ke database');
					else $result = array('success' => false, 'message' => 'Gagal menambahkan  Voucher COA ke database');
				}
				for ($i = 0; $i < sizeof($id_masuk); $i++) {
					for ($j = 0; $j < sizeof($id_keluar); $j++)
						$this->payment_voucher_kas_model->add_counter_coa($id_keluar[$j], $id_masuk[$i]);
				}
			} else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert Bank Payment Voucher No Voucher = ' . $no_voucher);
				$result = array('success' => false, 'message' => 'Gagal menambahkan Bank Payment Voucher ke database');
			}
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function delete_payment()
	{
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data, true);
		$result = $this->payment_voucher_kas_model->payment_delete($params);
		if ($result > 0) {
			$this->log_activity->insert_activity('delete', 'Delete Bank Payment Voucher id : ' . $params['id']);
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		} else {
			$this->log_activity->insert_activity('delete', 'Gagal Delete Bank Payment Voucher id : ' . $params['id']);
			$res = array('status' => 'failed', 'message' => 'Data gagal di hapus');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function edit_payment($id)
	{
		$list_bank = $this->payment_voucher_kas_model->list_bank();
		$result_payment		= $this->payment_voucher_kas_model->edit_payment($id);
		$result_bank		= $this->payment_voucher_kas_model->edit_payment_bank($id);
		$data = array(
			'id' => $id
		);
		$result_voucher_coa = $this->payment_voucher_kas_model->get_coa_value_list_by_pvk($data);
		for ($i = 0; $i < sizeof($result_voucher_coa); $i++) {
			if ($result_voucher_coa[$i]['bukti'] != null) {
				$handle = fopen($result_voucher_coa[$i]['bukti'], "r");
				$result_voucher_coa[$i]['have_file'] = "yes";
			} else {
				$result_voucher_coa[$i]['have_file'] = "no";
			}
		}
		$data = array(
			'pv'		=> $result_payment,
			'pvd'		=> $result_bank,
			'coa'		=> $result_voucher_coa,
			'list_bank' => $list_bank
		);
		// echo "<pre>";print_r($data);echo"</pre>";die;
		$this->load->view('edit_modal_view', $data);
	}

	public function detail_payment($id)
	{
		$list_bank = $this->payment_voucher_kas_model->list_bank();
		$result_payment		= $this->payment_voucher_kas_model->edit_payment($id);
		$result_bank		= $this->payment_voucher_kas_model->edit_payment_bank($id);
		$data = array(
			'id' => $id
		);
		$result_voucher_coa = $this->payment_voucher_kas_model->get_coa_value_list_by_pvk($data);
		for ($i = 0; $i < sizeof($result_voucher_coa); $i++) {
			if ($result_voucher_coa[$i]['bukti'] != null) {
				$handle = fopen($result_voucher_coa[$i]['bukti'], "r");
				$result_voucher_coa[$i]['have_file'] = "yes";
			} else {
				$result_voucher_coa[$i]['have_file'] = "no";
			}
		}
		$data = array(
			'pv'		=> $result_payment,
			'pvd'		=> $result_bank,
			'coa'		=> $result_voucher_coa,
			'list_bank' => $list_bank
		);
		$this->load->view('detail_view', $data);
	}
	public function print_payment($id)
	{
		$list_bank = $this->payment_voucher_kas_model->list_bank();
		$result_payment		= $this->payment_voucher_kas_model->edit_payment($id);
		$result_bank		= $this->payment_voucher_kas_model->edit_payment_bank($id);
		$data = array(
			'id' => $id
		);
		$result_voucher_coa = $this->payment_voucher_kas_model->get_coa_value_list_by_pvk($data);
		$data = array(
			'pv'		=> $result_payment,
			'pvd'		=> $result_bank,
			'coa'		=> $result_voucher_coa,
			'list_bank' => $list_bank
		);
		$this->load->view('print_view', $data);
	}
	public function save_edit_payment_backup()
	{

		// $no_voucher 	= $this->sequence->get_no('voucher_bank');
		$id_bpvk 	= ($this->input->post('id_pvk', TRUE));
		$data = array(
			'id' => $id_bpvk
		);
		$result = $this->payment_voucher_kas_model->payment_delete($data);
		$no_voucher 	= ($this->input->post('no_pvk', TRUE));
		$id_coa			= ($this->input->post('id_coa', TRUE));
		$id_parent		= ($this->input->post('id_parent', TRUE));
		$date 			= ($this->input->post('date', TRUE));
		$id_valas 		= ($this->input->post('id_valas', TRUE));
		$value 			= ($this->input->post('arrValue', TRUE));
		$rate 			= ($this->input->post('rate', TRUE));
		$type_cash 		= ($this->input->post('type_cash', TRUE));
		$note 			= ($this->input->post('note', TRUE));
		$tgl_bpvk 			= ($this->input->post('tgl_bpvk', TRUE));
		$jumlah 			= ($this->input->post('jumlah', TRUE));
	$nama_penerima 			= ($this->input->post('nama_penerima', TRUE));


		$upload_error 	= NULL;
		$file_kas	= array();

		// for ($i = 0; $i < sizeof($_FILES['file_kas']['name']); $i++) {
		// 	$file_kas = $this->upload('file_kas');
		// }
		// echo "<pre>";print_r($this->input->post());echo"</pre>";die;
		for ($i = 0; $i < sizeof($_FILES['file_kas']['name']); $i++) {
			$file_kas[$i]['file_kas']['name'] = $_FILES['file_kas']['name'][$i];
			if ($_FILES['file_kas']['name'][$i]) $file_kas[$i]['file_kas']['final_name'] = 'uploads/payment_voucher_kas/' . $_FILES['file_kas']['name'][$i];
			else 	$file_kas[$i]['file_kas']['final_name'] = $_FILES['file_kas']['name'][$i];
			$file_kas[$i]['file_kas']['type'] = $_FILES['file_kas']['type'][$i];
			$file_kas[$i]['file_kas']['tmp_name'] = $_FILES['file_kas']['tmp_name'][$i];
			$file_kas[$i]['file_kas']['error'] = $_FILES['file_kas']['error'][$i];
			$file_kas[$i]['file_kas']['size'] = $_FILES['file_kas']['size'][$i];
		}

		for ($i = 0; $i < sizeof($file_kas); $i++) {
			if (sizeof($file_kas) > 0) {
				if ($file_kas[$i]['file_kas']['name']) {
					if (!move_uploaded_file($file_kas[$i]['file_kas']['tmp_name'], $file_kas[$i]['file_kas']['final_name'])) {
						$pesan = "Gagal Upload File!";
						$upload_error = strip_tags(str_replace("\n", '', $pesan));

						$result = array('success' => false, 'message' => $upload_error);
					}
				}
			}
		}
		$result_payment = array();
		$result_coa_value = array();
		$id_masuk = array();
		$id_keluar = array();
		if (!isset($upload_error)) {
			$data = array(
				'no_voucher'	=> $no_voucher,
				'tgl_bpvk'		=> date('Y-m-d', strtotime($tgl_bpvk)),
				'jumlah'		=> preg_replace('/\D/', '', $jumlah),
				'nama_penerima' => $nama_penerima
			);
			$result_payment = $this->payment_voucher_kas_model->add_payment($data);
			if ($result_payment) {
				$this->log_activity->insert_activity('insert', 'Insert Bank Payment Voucher Kas No Voucher : ' . $no_voucher);

				$tempbank 			= ($this->input->post('arrBank', TRUE));
				$tempdate 			= ($this->input->post('arrDate', TRUE));
				$temprate 			= ($this->input->post('arrRate', TRUE));
				$tempnominal 		= ($this->input->post('arrNominal', TRUE));

				for ($i = 0; $i < sizeof($tempbank); $i++) {
					$result_list_bank[$i] = $this->payment_voucher_kas_model->list_bank($tempbank[$i])[0];
				}
				for ($i = 0; $i < sizeof($result_list_bank); $i++) {
					$dataBank = array(
						'no_rek'	=> $result_list_bank[$i]['no_rek'],
						'nama_bank'	=> $result_list_bank[$i]['id_bank'],
						'id_vk' 	=> $result_payment['lastid'],
						'nominal'	=> $tempnominal[$i]
					);
					$result_bank[$i] = $this->payment_voucher_kas_model->add_payment_bank($dataBank);
				}
				$status = true;
				for ($i = 0; $i < sizeof($result_bank); $i++) {
					if ($result_bank[$i] <= 0) $status = false;
					if ($status) $this->log_activity->insert_activity('insert', 'Insert List Bank Payment Voucher Kas Id : ' . $tempbank[$i]);
					else $this->log_activity->insert_activity('insert', 'Gagal Insert List Bank Payment Voucher Kas Id = ' . $tempbank[$i]);

					$this->log_activity->insert_activity('insert', 'Insert Bank Payment Voucher Kas No Voucher : ' . $no_voucher);
				}
				for ($i = 0; $i < sizeof($id_coa); $i++) {
					$data =  array(
						'id_coa'		=> $id_coa[$i],
						'id_parent'	=> $id_parent[$i],
						'id_valas'	=> $id_valas[$i],
						'value'			=> $value[$i],
						'date'			=> date('Y-m-d', strtotime($tgl_bpvk)),
						'type_cash'	=> $type_cash[$i],
						'note'			=> $note[$i],
						'rate'			=> $rate[$i],
						'bukti'			=> $file_kas[$i]['file_kas']['final_name'],
						'pic'				=> $this->session->userdata['logged_in']['user_id']
					);
					$result_coa_value[$i] = $this->payment_voucher_kas_model->add_coa_value($data);
					$id_keluar[$i] = $result_coa_value[$i]['lastid'];
				}
				for ($j = 0; $j < sizeof($result_list_bank); $j++) {
					$data =  array(
						'id_coa'		=> $result_list_bank[$j]['id_coa'],
						'id_parent'	=> $result_list_bank[$j]['id_parent'],
						'id_valas'	=> $result_list_bank[$j]['id_valas'],
						'value'			=> $tempnominal[$j],
						'date'			=> date('Y-m-d', strtotime($tgl_bpvk)),
						'type_cash'	=> 0, //auto pemasukan
						'note'			=> '',
						'rate'			=> $temprate[$j],
						'bukti'			=> '',
						'pic'				=> $this->session->userdata['logged_in']['user_id']
					);

					$result_coa_value[$i + $j] = $this->payment_voucher_kas_model->add_coa_value($data);
					$id_masuk[$j] = $result_coa_value[$i + $j]['lastid'];
				}
				$status = true;
				for ($i = 0; $i > sizeof($result_coa_value); $i++) {
					if ($result_coa_value[$i]['result'] <= 0) $status = false;
				}
				if ($status > 0) $this->log_activity->insert_activity('insert', 'Menambahkan kas ke database');
				else $this->log_activity->insert_activity('insert', 'Gagal Menambahkan kas ke database ');


				for ($i = 0; $i < sizeof($result_coa_value); $i++) {
					$dataVoucherCoa = array(
						"id_coa" => $result_coa_value[$i]['lastid'],
						"id_pvk" => $result_payment['lastid']
					);
					$result_voucher_coa[$i] = $this->payment_voucher_kas_model->add_voucher_coa($dataVoucherCoa);
					if ($result_voucher_coa[$i] > 0) $result = array('success' => true, 'message' => 'Berhasil menambahkan  Voucher  COA ke database');
					else $result = array('success' => false, 'message' => 'Gagal menambahkan  Voucher COA ke database');
				}
				for ($i = 0; $i < sizeof($id_masuk); $i++) {
					for ($j = 0; $j < sizeof($id_keluar); $j++)
						$this->payment_voucher_kas_model->add_counter_coa($id_keluar[$j], $id_masuk[$i]);
				}
			} else {
				$this->log_activity->insert_activity('insert', 'Gagal Insert Bank Payment Voucher No Voucher = ' . $no_voucher);
				$result = array('success' => false, 'message' => 'Gagal menambahkan Bank Payment Voucher ke database');
			}
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function save_edit_payment()
	{
		$no_voucher 	= ($this->input->post('no_pvk', TRUE));
		$id_bpvk 	= ($this->input->post('id_pvk', TRUE));
		$id 			= ($this->input->post('id', TRUE));
		$id_coa			= ($this->input->post('id_coa', TRUE));
		$id_parent		= ($this->input->post('id_parent', TRUE));
		$date 			= ($this->input->post('date', TRUE));
		$id_valas 		= ($this->input->post('id_valas', TRUE));
		$value 			= ($this->input->post('value', TRUE));
		$rate 			= ($this->input->post('rate', TRUE));
		$type_cash 		= ($this->input->post('type_cash', TRUE));
		$note 			= ($this->input->post('note', TRUE));
		$tgl_bpvk 			= ($this->input->post('tgl_bpvk', TRUE));
		$jumlah 			= ($this->input->post('jumlah', TRUE));
		$have_file 			= ($this->input->post('have_file', TRUE));
		$upload_error 	= NULL;
		$file_kas	= array();
		// var_dump($this->input->post());die;
		for ($i = 0; $i < sizeof($_FILES['file_kas']['name']); $i++) {
			$file_kas[$i]['file_kas']['name'] = $_FILES['file_kas']['name'][$i];
			if ($_FILES['file_kas']['name'][$i]) $file_kas[$i]['file_kas']['final_name'] = 'uploads/payment_voucher_kas/' . $_FILES['file_kas']['name'][$i];
			else 	$file_kas[$i]['file_kas']['final_name'] = $_FILES['file_kas']['name'][$i];
			$file_kas[$i]['file_kas']['type'] = $_FILES['file_kas']['type'][$i];
			$file_kas[$i]['file_kas']['tmp_name'] = $_FILES['file_kas']['tmp_name'][$i];
			$file_kas[$i]['file_kas']['error'] = $_FILES['file_kas']['error'][$i];
			$file_kas[$i]['file_kas']['size'] = $_FILES['file_kas']['size'][$i];
		}

		for ($i = 0; $i < sizeof($file_kas); $i++) {
			if (sizeof($file_kas) > 0 && $have_file[$i] == "new") {
				if ($file_kas[$i]['file_kas']['name']) {
					if (!move_uploaded_file($file_kas[$i]['file_kas']['tmp_name'], $file_kas[$i]['file_kas']['final_name'])) {
						$pesan = "Gagal Upload File!";
						$upload_error = strip_tags(str_replace("\n", '', $pesan));

						$result = array('success' => false, 'message' => $upload_error);
					}
				}
			}
		}
		$result_payment = array();
		$result_coa_value = array();
		if (!isset($upload_error)) {
			$data = array(
				'id_payment' => $id_bpvk,
				'no_voucher'	=> $no_voucher,
				'tgl_bpv'		=> date('Y-m-d', strtotime($tgl_bpvk)),
				'jumlah'		=> preg_replace('/\D/', '', $jumlah)
			);
			$result_payment = $this->payment_voucher_kas_model->save_edit_payment($data);
			$this->log_activity->insert_activity('update', 'Update Bank Payment Voucher Kas No Voucher : ' . $no_voucher);

			$tempbank 			= ($this->input->post('arrBank', TRUE));
			$tempdate 			= ($this->input->post('arrDate', TRUE));
			$temprate 			= ($this->input->post('arrRate', TRUE));
			$tempnominal 		= ($this->input->post('arrNominal', TRUE));

			$result = $this->payment_voucher_kas_model->payment_d_delete(array('id' => $id_bpvk));
			for ($i = 0; $i < sizeof($tempbank); $i++) {
				$result_list_bank[$i] = $this->payment_voucher_kas_model->list_bank($tempbank[$i])[0];
			}

			// echo "<pre>";print_r($result_list_bank);echo"</pre>";die;
			for ($i = 0; $i < sizeof($result_list_bank); $i++) {
				$dataBank = array(
					'no_rek'	=> $result_list_bank[$i]['no_rek'],
					'nama_bank'	=> $result_list_bank[$i]['id_bank'],
					'id_vk' 	=> $result_payment['lastid'],
					'nominal'	=> $tempnominal[$i]
				);
				$result_bank[$i] = $this->payment_voucher_kas_model->add_payment_bank($dataBank);
				$status = true;
				for ($j = 0; $j < sizeof($result_bank); $i++)
					if ($result_bank[$j] <= 0) $status = false;
				if ($status) $this->log_activity->insert_activity('update', 'Update List Bank Payment Voucher Kas Id : ' . $tempbank[$j]);
				else $this->log_activity->insert_activity('update', 'Gagal Update List Bank Payment Voucher Kas Id = ' . $tempbank[$j]);

				$this->log_activity->insert_activity('update', 'Update Bank Payment Voucher Kas No Voucher : ' . $no_voucher);
			}
			$result_coa_value = array();
			for ($i = 0; $i < sizeof($id_coa); $i++) {
				if (($have_file[$i]) == "new") {
					$data =  array(
						'id_coa'		=> $id_coa[$i],
						'id_parent'	=> $id_parent[$i],
						'id_valas'	=> $id_valas[$i],
						'value'			=> $value[$i],
						'date'			=> date('Y-m-d', strtotime($tgl_bpvk)),
						'type_cash'	=> $type_cash[$i],
						'note'			=> $note[$i],
						'rate'			=> $rate[$i],
						'bukti'			=> $file_kas[$i]['file_kas']['final_name'],
						'pic'				=> $this->session->userdata['logged_in']['user_id']
					);
					$result_coa_value[$i] = $this->payment_voucher_kas_model->add_coa_value($data);
				} else {
					$data =  array(
						'id'				=> $id[$i],
						'id_coa'		=> $id_coa[$i],
						'id_parent'	=> $id_parent[$i],
						'id_valas'	=> $id_valas[$i],
						'value'			=> $value[$i],
						'date'			=> date('Y-m-d', strtotime($tgl_bpvk)),
						'type_cash'	=> $type_cash[$i],
						'note'			=> $note[$i],
						'rate'			=> $rate[$i],
						'bukti'			=> $file_kas[$i]['file_kas']['final_name'],
						'pic'				=> $this->session->userdata['logged_in']['user_id']
					);

					$result_coa_value[$i] = $this->payment_voucher_kas_model->edit_coa_value($data);
					$result_coa_value[$i]['lastid'] = $id[$i];
				}
			}

			$result_coa_value_db = $this->payment_voucher_kas_model->get_coa_value_list_by_pvk($data = array('id' => $id_bpvk), 0);
			for ($i = 0; $i < sizeof($result_coa_value_db); $i++)
				$this->payment_voucher_kas_model->delete_coa($result_coa_value_db[$i]['id_coa_value']);
			for ($j = 0; $j < sizeof($result_list_bank); $j++) {
				$data =  array(
					'id_coa'		=> $result_list_bank[$j]['id_coa'],
					'id_parent'	=> $result_list_bank[$j]['id_parent'],
					'id_valas'	=> $result_list_bank[$j]['id_valas'],
					'value'			=> $tempnominal[$j],
					'date'			=> date('Y-m-d', strtotime($tgl_bpvk)),
					'type_cash'	=> 0, //auto pemasukan
					'note'			=> '',
					'rate'			=> $temprate[$j],
					'bukti'			=> '',
					'pic'				=> $this->session->userdata['logged_in']['user_id']
				);

				$result_coa_value[$i + $j] = $this->payment_voucher_kas_model->add_coa_value($data);
			}

			$status = true;
			for ($i = 0; $i > sizeof($result_coa_value); $i++) {
				if ($result_coa_value[$i]['result'] <= 0) $status = false;
			}
			if ($status > 0) $this->log_activity->insert_activity('update', 'Update kas ke database');
			else $this->log_activity->insert_activity('update', 'Gagal Update kas ke database ');


			$result = $this->payment_voucher_kas_model->payment_delete($params = array('id' => $id_bpvk));
			for ($i = 0; $i < sizeof($result_coa_value); $i++) {
				$dataVoucherCoa = array(
					"id_coa" => $result_coa_value[$i]['lastid'],
					"id_pvk" => $result_payment['lastid']
				);
				$result_voucher_coa[$i] = $this->payment_voucher_kas_model->add_voucher_coa($dataVoucherCoa);

				// var_dump($result_voucher_coa);die;
				if ($result_voucher_coa[$i] > 0) $result = array('success' => true, 'message' => 'Berhasil menambahkan  Voucher  COA ke database');
				else $result = array('success' => false, 'message' => 'Gagal menambahkan  Voucher COA ke database');
			}
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
}
