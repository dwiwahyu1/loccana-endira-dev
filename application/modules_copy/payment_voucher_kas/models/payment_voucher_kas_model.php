<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Payment_Voucher_Kas_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function list_payment($params){
		$sql 	= 'CALL pvk_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query(
			$sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter']
			)
		);

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total['@total_filtered'],
			'total' => $total['@total'],
		);

		return $return;
	}
	
	public function count_payment($params) {
		$sql 	= 'CALL pvk_list(?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,array(NULL, NULL, NULL, NULL, NULL));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		return $total['@total'];
	}
	public function list_bank($id_coa=null){
		$sql 	= 'SELECT a.*, b.*, c.* 
							FROM `t_bank` a
							LEFT JOIN  `t_coa` b ON a.`id_coa` = b.`id_coa`
							LEFT JOIN `m_valas` c ON a.`id_valas` = c.`valas_id`';
		if($id_coa) $sql=$sql.'WHERE a.id_coa ='.$id_coa;
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	public function list_coa($id,$type_cash=1){
		$sql 	= 'CALL coavalue_search_id_temporary2(?,?)';

		$out = array();
		$query 	=  $this->db->query($sql, array(
			$id,
			$type_cash //default nya pengeluaran
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	public function add_coa_value($data){
		$sql 	= 'CALL pvk_coavalue_add(?,?,?,?,?,?,?,?,?)';
		// IN `pin_id_coa` INT(11),
		// IN `pin_id_parent` INT(11),
		// IN `pin_id_valas` INT(11),
		// IN `pin_value` DECIMAL(20,4),
		// IN `pin_adjusment` VARCHAR(255),
		// IN `pin_type_cash` INT(11),
		// IN `pin_note` VARCHAR(255),
		// IN `pin_rate` DECIMAL(20,4),
		// IN `pin_bukti` VARCHAR(255)
		$query = $this->db->query($sql, array(
			$data['id_coa'],
			$data['id_parent'],
			$data['id_valas'],
			$data['value'],
			0,
			$data['type_cash'],
			$data['note'],
			$data['rate'],
			$data['bukti']
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		return $arr_result;
	}
	public function add_payment($data){
		$sql 	= 'CALL pvk_add(?, ?, ?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['no_voucher'],
			$data['tgl_bpvk'],
			$data['jumlah'],
			$data['nama_penerima']
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		return $arr_result;
	}

	public function add_counter_coa($item_keluar,$item_masuk){
		$sql 	= 'CALL pvk_counter_coa_add(?, ?)';

		$query 	=  $this->db->query($sql, array(
			$item_keluar,
			$item_masuk
		));

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;
		return $arr_result;
	}

	public function add_voucher_coa($data){
		$sql 	= 'CALL vc_add(?, ?)';

		$query 	=  $this->db->query($sql, array(
			$data['id_coa'],
			$data['id_pvk']
		));
		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	public function add_payment_bank($data){
		$sql 	= 'CALL pvk_d_add(?,?,?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['id_vk'],
			$data['no_rek'],
			$data['nama_bank'],
			$data['nominal']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_payment($id){
		$sql 	= 'CALL pvk_search_id(?)';

		$out = array();
		$query 	=  $this->db->query($sql, array(
			$id
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function edit_payment_bank($id){
		$sql 	= 'CALL pvk_d_search_id(?)';

		$out = array();
		$query 	=  $this->db->query($sql, array(
			$id
		));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	public function edit_coa_value($data) {
		$sql 	= 'CALL coavalue_update_temporary(?,?,?,?,?,?,?,?,?,?)';

		$this->db->query($sql, array(
			$data['id'],
			$data['id_coa'],
			$data['id_parent'],
			$data['date'],
			$data['id_valas'],
			$data['value'],
			$data['type_cash'],
			$data['note'],
			$data['rate'],
			$data['bukti']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = 0;
		$arr_result['result'] = $result;
		return $arr_result;
	}
	public function save_edit_payment($data){
		$sql 	= 'CALL pvk_update(?,?,?,?)';

		$query 	=  $this->db->query(
			$sql,
			array(
				$data['id_payment'],
				$data['no_voucher'],
				$data['tgl_bpv'],
				$data['jumlah']
			)
		);

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function save_edit_payment_bank($data){
		$sql 	= 'CALL pvk_d_update(?,?,?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['id_detail'],
			$data['no_rek'],
			$data['nama_bank'],
			$data['nominal']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	public function payment_d_delete($data){
		$sql 	= 'CALL pvk_d_delete(?)';
		$query 	=  $this->db->query($sql, array(
			$data['id']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	public function payment_delete($data){
		$sql 	= 'CALL pvk_delete(?)';
		$query 	=  $this->db->query($sql, array(
			$data['id']
		));

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	public function get_coa_value_list_by_pvk($data,$type_cash=1){
		$sql = 'CALL coavalue_search_id_temporary2(?,?)';
		$query = $this->db->query($sql,array(
			$data['id'],
			$type_cash
		));
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
		/**
     * This function is used to delete coa_value
     * @param: $id - id of coa_value table
     */
		function delete_coa($id) {
			$this->db->where('id', $id);  
			$this->db->delete('t_coa_value');
			$result	= $this->db->affected_rows();
	
			$this->db->close();
			$this->db->initialize();
			return $result;
		}
}
