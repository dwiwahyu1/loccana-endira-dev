<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Esft extends MX_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('esft/esft_model');
		$this->load->library('log_activity');
    }

    /**
     * anti sql injection
     */
    public function Anti_sql_injection($string) {
        $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
        return $string;
    }

    /**
      * This function is redirect to index btb page
      * @return Void
      */
    public function index() {
        $this->template->load('maintemplate', 'esft/views/index');
    }

    /**
      * This function is used for showing esft list
      * @return Array
      */
    function lists() {
        $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
        $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
        $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
        $order = $this->input->get_post('order');
        $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'asc';
        $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

        $order_fields = array('', 'id', 'name_eksternal', 'stock_name');

        $search = $this->input->get_post('search');

        $search_val = (!empty($search['value'])) ? $search['value'] : null;

        $search = $this->input->get_post('search');

        $search_value = $this->Anti_sql_injection($search_val);

        // Build params for calling model
        $params['limit'] = (int) $length;
        $params['offset'] = (int) $start;
        $params['order_column'] = $order_fields[$order_column];
        $params['order_dir'] = $order_dir;
        $params['filter'] = $search_value;

        $list = $this->esft_model->lists($params);

        $result["recordsTotal"] = $list['total'];
        $result["recordsFiltered"] = $list['total_filtered'];
        $result["draw"] = $draw;

        $data = array();

        $i = $params['offset'];
        foreach ($list['data'] as $k => $v) {
            $i++;
            $actions = '<div class="btn-group">';
            $actions .= '   <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edit_esft(\'' . $v['id'] . '\')">';
            $actions .= '       <i class="fa fa-edit"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';
            
            array_push($data, array(
                $i,
                $v['esft_no'],
                $v['esft_date'],
                $v['name_eksternal'],
                $v['stock_name'],
                $v['name_to'],
                $v['due_date'],
                $v['status'],
                $actions
                    )
            );
        }

        $result["data"] = $data;

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    /**
      * This function is redirect to add esft page
      * @return Void
      */
    public function add() {;
        $this->load->view('add_modal_view');
    }

    /**
      * This function is used to add esft data
      * @return Array
      */
    public function get_esf() {
        $data   = file_get_contents("php://input");
        $params = json_decode($data,true);

        $list   = $this->esft_model->get_esf($params);

        $data = array();

        $i = 0;
        foreach ($list as $k => $v) {
            $i++;

            $index = $i-1;

            $strOption =
                '<div class="radiobutton">'.
                    '<input id="option['.$index.']" type="radio" value="'.$v['id'].'">'.
                    '<label for="option['.$index.']"></label>'.
                '</div>';
            
            array_push($data, array(
                $i,
                $v['esf_no'],
                $v['esf_date'],
                $v['name_eksternal'],
                $v['stock_name'],
                $strOption
            ));
        }
        
        $res = array(
            'status'    => 'success',
            'data'      => $data
        );

        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Content-type: application/json');
        header("access-control-allow-origin: *");
        echo json_encode($res);
    }

    /**
      * This function is redirect to add esft page
      * @return Void
      */
    public function new($id) {
        $to = $this->esft_model->listto();
        $detail = $this->esft_model->detail($id);

        $data = array(
            'to'    => $to,
            'detail'=> $detail
        );

        $this->load->view('new_modal_view',$data);
    }

    /**
      * This function is used to add esft data
      * @return Array
      */
    public function add_esft() {
      $data     = file_get_contents("php://input");
      $params   = json_decode($data,true);

      $this->esft_model->add_esft($params);

      $msg = 'Berhasil menambah data ESFT';

      $result = array(
          'success' => true,
          'message' => $msg
      );

		$this->log_activity->insert_activity('insert', $msg. ' dengan No ESFT ' .$params['esft_no']);

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
}