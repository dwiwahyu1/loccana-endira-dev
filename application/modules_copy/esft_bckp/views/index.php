<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">ESFT</h4>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">

				<table id="listesft" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th>No</th>
							<th>ESFT No</th>
							<th>ESFT Date</th>
							<th>Customer</th>
							<th>Item</th>
							<th>To</th>
							<th>Due date</th>
							<th>Status</th>
							<th>Option</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>No</td>
							<td>ESFT No</td>
							<td>ESFT Date</td>
							<td>Customer</td>
							<td>Item</td>
							<td>To</td>
							<td>Due date</td>
							<td>Status</td>
							<td>Option</td>
						</tr>
                      </tbody>
					</tbody>
				</table>

            </div>
        </div><!-- end col -->
    </div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog" style="width:50%;">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <p></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <p></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
	function add_esft(){
      $('#panel-modal').removeData('bs.modal');
      $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modal  .panel-body').load('<?php echo base_url('esft/add');?>');
      $('#panel-modal  .panel-title').html('<i class="fa fa-signal"></i> Choose ESF');
      $('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
  	}

	function listesft(){
      $("#listesft").dataTable({
          "processing": true,
          "serverSide": true,
          "ajax": "<?php echo base_url().'esft/lists';?>",
          "searchDelay": 700,
          "responsive": true,
          "lengthChange": false,
          "destroy": true,
          "info": false,
          "bSort": false,
          "dom": 'l<"toolbar">frtip',
          "initComplete": function(){
              var element = '<div class="btn-group pull-left">';
                  element += '  <a class="btn btn-primary" onClick="add_esft()">';
                  element += '    <i class="fa fa-plus"></i> Add ESFT';
                  element += '  </a>';
                  element += '</div>';
              $("div.toolbar").prepend(element);
          }
      });
  }

	$(document).ready(function(){
      listesft();
  });
</script>