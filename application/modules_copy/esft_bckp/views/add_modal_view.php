  <style>
    #loading-us{display:none}
    #tick{display:none}

    #loading-mail{display:none}
    #cross{display:none}
    .input-group.date{width:233px;}
    #btn-search{margin-right: -10px;}
    .form-group{margin-top: 15px;overflow-y: auto;overflow-x: hidden;}
    #loading{
      position: absolute;
      background: #FFFFFF;
      opacity: 0.5;
      width: 96%;
      height: 80%;
      z-index: 3;
      text-align: center;
      display:none;
    }
    .totalproducts{cursor:pointer;text-decoration: underline;color:#96b6e8;}
    .totalproducts:hover{color:#ff8c00}
  </style>

  <div id="loading"><img src="<?php echo base_url();?>assets/adminto-14/adminto-14/Admin/Horizontal/assets/images/preloader.gif"/></div>
  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama" style="float:left">ESF date</label>
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama" style="float:left;width: 50px;">from</label>
    <div class="input-group date" style="float:left">
      <input type="text" class="form-control col-md-7 col-xs-12 datepicker" id="datefrom">
      <div class="input-group-addon">
        <span class="glyphicon glyphicon-th"></span>
      </div>
    </div>
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama" style="float:left;width: 50px;">to</label>
    <div class="input-group date" style="float:left">
      <input type="text" class="form-control col-md-7 col-xs-12 datepicker" id="dateto">
      <div class="input-group-addon">
        <span class="glyphicon glyphicon-th"></span>
      </div>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama" style="float:left">Search by</label>
    <div class="input-group" style="float:left;width: 170px;">
      <select class="form-control" id="searchby" name="searchby" style="width: 155px;border-radius: 4px;">
          <option value="0">--All--</option>
          <option value="1">Customer Name</option>
          <option value="2">ESF No</option>
        </select>
    </div>
    <input data-parsley-maxlength="255" type="text" id="searchby_text" name="searchby_text" class="form-control col-md-7 col-xs-12" style="width: 395px;">
  </div>

  <div class="item form-group">
    <div class="col-md-8 col-sm-6 col-xs-12" style="float:right;text-align: right;">
      <button id="btn-search" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" onclick="getESF();">Search</button>
    </div>
  </div>

  <div class="item form-group">
    <table id="listesf" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>No</th>
          <th>ESF No</th>
          <th>ESF Date</th>
          <th>Customer</th>
          <th>Item</th>
          <th>Choose</th>
        </tr>
      </thead>
      <tbody></tbody>
    </table>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
    <div class="col-md-8 col-sm-6 col-xs-12" style="padding-left: 130px;">
      <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Choose ESF</button>
    </div>
  </div>

<script type="text/javascript">
  var table_po;
  $(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();

    $(".datepicker").datepicker({
      autoclose: true,
      todayHighlight: true,
    });

    resetDt();

    $('#btn-search').trigger('click');
  });

  function resetDt() {
    table_po = $('#listesf').DataTable( {
      "processing": true,
      "searching": false,
      "responsive": true,
      "lengthChange": false,
      "info": false,
      "bSort": false,
      "columnDefs": [{
        "targets": [0],
        "visible": false,
        "searchable": false
      }]
    });
  }

  function getESF() {
    var datefrom      = $("#datefrom").val(),
        dateto        = $("#dateto").val(),
        searchby      = $("#searchby").val(),
        searchby_text = $("#searchby_text").val();

    const date1 = new Date(datefrom);
    const date2 = new Date(dateto);
    const diffTime = date2.getTime() - date1.getTime();
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

    if(datefrom !== '' && dateto !== '' && diffDays < 0){
      swal('Warning', 'Date range is not valid');
    }else{
      sendNo(datefrom,dateto,searchby,searchby_text);
    } 
  }

  function sendNo(datefrom,dateto,searchby,searchby_text) {
    var datapost = {
      "datefrom"      : datefrom,
      "dateto"        : dateto,
      "searchby"      : searchby,
      "searchby_text" : searchby_text
    };

    $('#loading').show();
    $.ajax({
      type: "POST",
      url: "<?php echo base_url();?>esft/get_esf",
      data : JSON.stringify(datapost),
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      success: function(r) {
        $('#loading').hide();

        table_po.clear();
        table_po.destroy();
        table_po = $('#listesf').DataTable( {
          "processing": true,
          "searching": false,
          "responsive": true,
          "lengthChange": false,
          "info": false,
          "bSort": false,
          "data": r.data,
          "columnDefs": [{
            "targets": [0],
            "visible": false,
            "searchable": false
          }]
        });
      }
    });
  }

  $('#btn-submit').on('click',(function(e) {
    var data = $('input[type="radio"]:checked').val();

    if (typeof data !== 'undefined'){
      $(this).attr('disabled','disabled');
      $(this).text("Memilih data...");
      e.preventDefault();
      
      $('.panel-heading button').trigger('click');
      newesft(data);
    }
  }));

  function newesft(id){
      $('#panel-modalchild').removeData('bs.modal');
      $('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
      $('#panel-modalchild  .panel-body').load('<?php echo base_url('esft/new/');?>'+"/"+id);
      $('#panel-modalchild  .panel-title').html('<i class="fa fa-plus"></i> Add ESFT');
      $('#panel-modalchild').modal({backdrop:'static',keyboard:false},'show');
  }
</script>
