  <form class="form-horizontal form-label-left" id="add_esft" role="form" action="<?php echo base_url('esft/add_esft');?>" method="post" enctype="multipart/form-data" data-parsley-validate>

    <p style="text-align: center;">Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Esft No <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="esft_no" name="esft_no" class="form-control col-md-7 col-xs-12" placeholder="Esft No" required="required" value="<?php echo strtoupper(randomString());?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Customer <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="customer" name="customer" class="form-control col-md-7 col-xs-12" placeholder="Customer" required="required" value="<?php if(isset($detail[0]['name_eksternal'])){ echo $detail[0]['name_eksternal']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Item <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="item" name="item" class="form-control col-md-7 col-xs-12" placeholder="Item" required="required" value="<?php if(isset($detail[0]['stock_name'])){ echo $detail[0]['stock_name']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Lam Thk Base Copper <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="pcb" name="pcb" class="form-control col-md-7 col-xs-12" placeholder="Lam Thk Base Copper" required="required" value="<?php if(isset($detail[0]['lam_thk_bc'])){ echo $detail[0]['lam_thk_bc']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">PCB Out Line Size <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="pcb_out" name="pcb_out" class="form-control col-md-7 col-xs-12" placeholder="PCB Out Line Size" required="required" value="<?php if(isset($detail[0]['pcb_out'])){ echo $detail[0]['pcb_out']; }?>" readonly>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Esft Date
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <div class="input-group">
           <input placeholder="Esft Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="esft_date" name="esft_date" required="required" value="<?php echo date('Y-m-d');?>">
          <div class="input-group-addon">
            <span class="glyphicon glyphicon-th"></span>
          </div>
        </div>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Due Date
        <span class="required">
          <sup>*</sup>
        </span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <div class="input-group">
           <input placeholder="Due Date" type="text" class="form-control col-md-7 col-xs-12 datepicker" id="due_date" name="due_date" required="required" value="<?php echo date('Y-m-d');?>">
          <div class="input-group-addon">
            <span class="glyphicon glyphicon-th"></span>
          </div>
        </div>
      </div>
    </div>
    
    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">To<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <select class="form-control select-control" id="to" name="to" required>
          <option value="" >-- Select To --</option>
          <?php foreach($to as $dk) { ?>
            <option value="<?php echo $dk['id']; ?>" <?php if(isset($detail[0]['id_eksternal'])){ if( $detail[0]['id_eksternal'] == $dk['id'] ){ echo "selected"; } }?>><?php echo $dk['name_eksternal']; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Tool<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <select class="form-control select-control" id="tool" name="tool" required>
          <option value="">-- Select Tool --</option>
          <option value="0">New</option>
          <option value="1">Modification</option>
        </select>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Punching<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <select class="form-control select-control" id="punching" name="punching" required>
          <option value="">-- Select Punching --</option>
          <option value="0">Cold</option>
          <option value="1">Hot</option>
        </select>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Type Of Tool<span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <select class="form-control select-control" id="type_tool" name="type_tool" required>
          <option value="">-- Select Type Of Tool --</option>
          <option value="0">Hard</option>
          <option value="1">Soft</option>
        </select>
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Guide Hole Pins <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="guide_hool" name="guide_hool" class="form-control col-md-7 col-xs-12" placeholder="Guide Hole Pins" required="required" value="0.0">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Tonnage <span class="required"><sup>*</sup></span>
      </label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <input data-parsley-maxlength="255" type="text" id="tonnage" name="tonnage" class="form-control col-md-7 col-xs-12" placeholder="Guide Hole Pins" required="required" value="0.0">
      </div>
    </div>

    <div class="item form-group">
      <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
      <div class="col-md-8 col-sm-6 col-xs-12">
        <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Add ESFT</button>
      </div>
    </div>

    <input type="hidden" id="id_cust_spec" name="id_cust_spec" value="<?php if(isset($detail[0]['id_cust_spec'])){ echo $detail[0]['id_cust_spec']; }?>">
    <input type="hidden" id="id_esf" name="id_esf" value="<?php if(isset($detail[0]['id_esf'])){ echo $detail[0]['id_esf']; }?>">
  </form><!-- /page content -->

<script type="text/javascript">
  $(document).ready(function() {
    $('form').parsley();
    $('[data-toggle="tooltip"]').tooltip();

    $(".select-control").select2();

    $('body').addClass('modal-open');
  });

  $('#add_esft').on('submit',(function(e) {
    $('#btn-submit').attr('disabled','disabled');
    $('#btn-submit').text("Menambah data...");
    e.preventDefault();

    var esft_no       = $('#esft_no').val(),
        id_cust_spec  = $('#id_cust_spec').val(),
        id_esf        = $('#id_esf').val(),
        esft_date     = $('#esft_date').val(),
        due_date      = $('#due_date').val(),
        to            = $('#to').val(),
        tool          = $('#tool').val(),
        punching      = $('#punching').val(),
        type_tool     = $("#type_tool").val(),
        guide_hool    = $("#guide_hool").val(),
        tonnage       = $("#tonnage").val();

    var datapost={
        "esft_no"        : esft_no,
        "id_cust_spec"   : id_quotation,
        "id_esf"         : id_esf,
        "esft_date"      : esft_date,
        "due_date"       : due_date,
        "to"             : to,
        "tool"           : tool,
        "punching"       : punching,
        "type_tool"      : type_tool,
        "guide_hool"     : guide_hool,
        "tonnage"        : tonnage
      };

    $.ajax({
        type:'POST',
        url: $(this).attr('action'),
        data : JSON.stringify(datapost),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function(response) {
            if (response.success == true) {
              $('.panel-heading button').trigger('click');
                listesft();
                swal({
                  title: 'Success!',
                  text: response.message,
                  type: 'success',
                  showCancelButton: false,
                  confirmButtonText: 'Ok'
                }).then(function () {
                });
            } else{
                $('#btn-submit').removeAttr('disabled');
                $('#btn-submit').text("Add ESFT");
                swal("Failed!", response.message, "error");
            }
        }
    }).fail(function(xhr, status, message) {
        $('#btn-submit').removeAttr('disabled');
        $('#btn-submit').text("Add ESFT");
        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
    });
  }));
</script>
