<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title">BOM</h4>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="card-box">
				<table id="listbom" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
					<thead>
						<tr>
							<th style="width: 5px;">No</th>
							<th>PO No</th>
							<th>BOM No</th>
							<th>Part No</th>
							<th>Customer</th>
							<th>Total Items</th>
							<th style="width: 150px;">Option</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td style="width: 5px;">No</td>
							<td>PO No</td>
							<td>BOM No</td>
							<td>Part No</td>
							<td>Customer</td>
							<td>Total Items</td>
							<td style="width: 150px;">Option</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog" style="width:750px;">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="panel-modalchild" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary panel-filled">
                <div class="panel-heading">
                    <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 class="panel-title"></h3>
                </div>
                <div class="panel-body">
                    <p></p>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$(document).ready(function(){
		listbom();
	});

	function listbom(){
		var cust_id = (typeof $('.select_customer').val() !== "undefined")?$('.select_customer').val():'0';

        $("#listbom").dataTable({
			"processing": true,
			"serverSide": true,
			"ajax": "<?php echo base_url().'bom/lists?cust_id="+cust_id+"';?>",
			"searchDelay": 700,
			"responsive": true,
			"lengthChange": false,
			"destroy": true,
			"info": false,
			"bSort": false,
			"dom": 'l<"toolbar">frtip',
			"initComplete": function(result){
				var element = '<div class="btn-group pull-left">';
                    element += '  <label>Filter</label>';
                    element += '  <label class="filter_separator">|</label>';
                    element += '  <label>Customer:';
                    element += '    <select class="form-control select_customer" onChange="listbom()" style="height:30px;width: 340px;">';
                    element += '      <option value="0">-- Semua Customer --</option>';
                    $.each( result.json.customer, function( key, val ) {
                      var selectcust = (val.id===cust_id)?'selected':'';
                            element += '    <option value="'+val.id+'" '+selectcust+'>'+val.name_eksternal+'</option>';
                        });
                    element += '    </select>';
                    element += '  </label>';
                        
               $("#listbom_filter").prepend(element);
			}
		});
    }

	function edit_bom(id){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal  .panel-body').load('<?php echo base_url('bom/edit/');?>'+'/'+id);
		$('#panel-modal  .panel-title').html('<i class="fa fa-at"></i> Edit BOM');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}

	function detail_item(row){
		var uom_id = $('#item_id'+row).val();

		if(uom_id !== ''){
		    $('#panel-modalchild').removeData('bs.modal');
		    $('#panel-modalchild  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		    $('#panel-modalchild  .panel-body').load('<?php echo base_url('bom/detail_item');?>'+'/'+uom_id);
		    $('#panel-modalchild  .panel-title').html('<i class="fa fa-book"></i> Detail Barang');
		    $('#panel-modalchild').modal({backdrop:'static',keyboard:false},'show');
	    }
	}
</script>