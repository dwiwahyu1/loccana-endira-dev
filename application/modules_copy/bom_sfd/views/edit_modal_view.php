<form class="form-horizontal form-label-left" id="edit_bom" role="form" action="<?php echo base_url('bom/edit_bom');?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p style="text-align:center;">Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Bom No
        	<span class="required">
        		<sup>*</sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
        	<input data-parsley-maxlength="255" type="text" name="bom_no" class="form-control col-md-7 col-xs-12" placeholder="Bom No" value="<?php if(isset($detail[0]['bom_no'])){ echo $detail[0]['bom_no']; }?>" readonly>
		</div>
   	</div>

   	<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Part No
        	<span class="required">
        		<sup>*</sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
        	<input data-parsley-maxlength="255" type="text" name="part_no" class="form-control col-md-7 col-xs-12" placeholder="Part No" value="<?php if(isset($detail[0]['part_no'])){ echo $detail[0]['part_no']; }?>" readonly>
		</div>
   	</div>

   	<div class="item form-group" id="subdetail_temp">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="type_id">Material Utama <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="id_mainproduk" name="id_mainproduk" style="width: 100%" required>
				<option value="">-- Select Item --</option>
				<?php foreach($mainitem as $key) { ?>
					<option value="<?php echo $key['id']; ?>" <?php if(isset($detail[0]['id_mainproduk'])){ if( $detail[0]['id_mainproduk'] == $key['id'] ){ echo "selected"; } }?>><?php echo $key['stock_name']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Qty
        	<span class="required">
        		<sup>*</sup>
        	</span>
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
        	<input data-parsley-maxlength="255" type="text" id="qty_main" name="qty_main" class="form-control col-md-7 col-xs-12" placeholder="Bom No" value="<?php if(isset($detail[0]['qty_main'])){ echo $detail[0]['qty_main']; }?>">
		</div>
   	</div>

	<div class="item form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Item Pendamping : </label>
    </div>

    <div class="item form-group">
		<table id="listedititem" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>Item</th>
					<th>Qty</th>
					<th>Option</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$i=1; 
				foreach($detail as $key) {?>
					<tr>
	                  	<td>
	                  		<select class="form-control select-control item-control" id="item_id<?php echo $i-1; ?>" name="item_id<?php echo $i-1; ?>" style="width: 340px" required>
								<option value="">-- Select Item --</option>
								<?php foreach($item as $itemkey) { ?>
									<option value="<?php echo $itemkey['id']; ?>" <?php if(isset($key['id_produk'])){ if( $key['id_produk'] == $itemkey['id'] ){ echo "selected"; } }?>><?php echo $itemkey['stock_name']; ?></option>
								<?php } ?>
							</select>
	                  	</td>
	                  	<td>
		                	<input type="text" class="form-control qty-control" id="qty<?php echo $i-1;?>" value="<?php if(isset($key['qty'])){ echo number_format($key['qty'],10,",","."); }?>" style="width:230px;">
		                </td>
		                <td>
		                	<div class="btn-group">
		                		<button class="btn btn-danger btn-delete" type="button" data-toggle="tooltip" data-placement="top" title="Hapus">
		                			<i class="fa fa-trash"></i>
		                		</button>
		                	</div>
		                	<div class="btn-group">
		                		<button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Detail"  onClick="detail_item('<?php echo $i-1; ?>')">
		                			<i class="fa fa-book"></i>
		                		</button>
		                	</div>
		                </td>
		            </tr>
	            <?php $i++;} ?>
	            <tr>
                  	<td>
                  		<select class="form-control select-control item-control" id="item_id<?php echo $i-1; ?>" name="item_id<?php echo $i-1; ?>" style="width: 340px">
							<option value="" >-- Select Item --</option>
							<?php foreach($item as $itemkey) { ?>
								<option value="<?php echo $itemkey['id']; ?>"><?php echo $itemkey['stock_name']; ?></option>
							<?php } ?>
						</select>
                  	</td>
                  	<td>
		                <input type="text" class="form-control qty-control" id="qty<?php echo $i-1;?>" value="0,0000000000" style="width:230px;">
		            </td>
	                <td>
	                	<div class="btn-group">
	                		<button class="btn btn-primary btn-delete" type="button" data-toggle="tooltip" data-placement="top" title="Tambah Baru" onClick="add_item('<?php echo $i-1; ?>')">
	                			<i class="fa fa-plus"></i>
	                		</button>
	                	</div>
	                	<div class="btn-group">
		                	<button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Detail" onClick="detail_item('<?php echo $i-1; ?>')">
		                		<i class="fa fa-book"></i>
		                	</button>
		                </div>
	                </td>
	            </tr>
			</tbody>
		</table>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Edit BOM</button>
		</div>
	</div>

	<input type="hidden" id="bom_no" value="<?php if(isset($detail[0]['bom_no'])){ echo $detail[0]['bom_no']; }?>">
	<input type="hidden" id="id_quotation" value="<?php if(isset($detail[0]['id_po_quot'])){ echo $detail[0]['id_po_quot']; }?>">
</form>
<!-- /page content -->

<script type="text/javascript">
	var items = [],
		itemsLen = '<?php echo count($detail)+1;?>';
	'<?php
	    foreach($item as $key) {
	      $id = $key['id'];
	      $stock_name = $key['stock_name'];
  ?>'
  	  var id = "<?php echo $id; ?>";
      var stock_name = "<?php echo $stock_name; ?>";

      var dataitem = {
          id,
          stock_name
      };

      items.push(dataitem);
  '<?php    
    	}
  ?>'
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
		
		$(".select-control").select2();

		$( ".btn-danger" ).on( "click", function() {
			$(this).parent().parent().parent().remove();
			itemsLen--;

			var itemCur = itemsLen-1;
			$('.fa-plus').parent().parent().parent().prev().children().attr('id','qty'+itemCur);
			$('.fa-plus').parent().parent().parent().prev().prev().children().attr('id','item_id'+itemCur);
			$('.fa-plus').parent().parent().parent().prev().prev().children().attr('name','item_id'+itemCur);
		});
	});

	function add_item(row){
		var uom_id = $('#item_id'+row).val();

		if(uom_id !== ''){
			$('#listedititem .btn-group .btn-delete').removeClass('btn-primary');
			$('#listedititem .btn-group .btn-delete').addClass('btn-danger');
			$('#listedititem .btn-group .btn-delete').attr('title','Hapus');
			$('#listedititem .btn-group .btn-delete').attr('data-original-title','Hapus');
			$('#listedititem .btn-group .btn-delete').removeAttr('onClick');
			$('#listedititem .btn-group .btn-delete .fa').removeClass('fa-plus');
			$('#listedititem .btn-group .btn-delete .fa').addClass('fa-trash');
			var products 	= $(".qty-control")

			var itemCount = items.length,
				productsLen = products.length;

				itemsLen++;

			var element = '<tr>';
				element += '	<td>';
				element += '		<select class="form-control select-control item-control" id="item_id'+productsLen+'" name="item_id'+productsLen+'" style="width: 340px" required>';
				element += '			<option value="">-- Select Item --</option>';
				for(var i=0;i<itemCount;i++){
					element += '		<option value="'+items[i]['id']+'">'+items[i]['stock_name']+'</option>';
				}
				element += '		</select>';
				element += '	</td>';
				element += '	<td>';
				element += '		<input type="text" class="form-control qty-control" id="qty'+productsLen+'" value="0,0000000000" style="width:230px;">';
				element += '	</td>';
				element += '	<td>';
				element += '		<div class="btn-group">';
				element += '			<button class="btn btn-primary btn-delete" type="button" data-toggle="tooltip" data-placement="top" title="Tambah Baru" onClick="add_item('+productsLen+')">';
				element += '				<i class="fa fa-plus"></i>';
				element += '			</button>';
				element += '		</div>';
				element += '		<div class="btn-group">';
				element += '			<button class="btn btn-info" type="button" data-toggle="tooltip" data-placement="top" title="Detail" onClick="detail_item()">';
				element += '				<i class="fa fa-book"></i>';
				element += '			</button>';
				element += '		</div>';
				element += '	</td>';
				element += '</tr>';
			$('#listedititem tbody').append(element);
			
			$("#item_id"+productsLen).select2();
			$('[data-toggle="tooltip"]').tooltip();

			$( ".btn-danger" ).on( "click", function() {
			  $(this).parent().parent().parent().remove();
			  itemsLen--;

			  var itemCur = itemsLen-1;
			  $('.fa-plus').parent().parent().parent().prev().children().attr('id','qty'+itemCur);
			  $('.fa-plus').parent().parent().parent().prev().prev().children().attr('id','item_id'+itemCur);
			  $('.fa-plus').parent().parent().parent().prev().prev().children().attr('name','item_id'+itemCur);
			});
		}
	}

	$('#edit_bom').on('submit',(function(e) {
		var products 	= $(".qty-control"),
			datapost    = [];

		var productsLen    = products.length;

		for(var k = 0; k < productsLen; k++){
			if($("select#item_id"+k).val() !== ''){
				datapost.push({
					"bom_no"        : $('#bom_no').val(),
					"id_po_quot"    : $('#id_quotation').val(),
					"id_produk"     : $("select#item_id"+k).val(),
					"id_mainproduk" : $("#id_mainproduk").val(),
					"qty"           : ($("#qty"+k).val()).replace(",", "."),
					"qty_main" 		: $("#qty_main").val(),
				});
			}
		}

	    $('#btn-submit').attr('disabled','disabled');
	    $('#btn-submit').text("Mengubah data...");
	    e.preventDefault();

	    $.ajax({
	        type:'POST',
	        url: $(this).attr('action'),
	        data : JSON.stringify(datapost),
	        dataType: 'json',
			contentType: 'application/json; charset=utf-8',
	        success: function(response) {
	            if (response.success == true) {
	              $('.panel-heading button').trigger('click');
	                listbom();
	                swal({
	                  title: 'Success!',
	                  text: response.message,
	                  type: 'success',
	                  showCancelButton: false,
	                  confirmButtonText: 'Ok'
	                }).then(function () {
	                });
	            } else{
	                $('#btn-submit').removeAttr('disabled');
	                $('#btn-submit').text("Edit BOM");
	                swal("Failed!", response.message, "error");
	            }
	        }
	    }).fail(function(xhr, status, message) {
	        $('#btn-submit').removeAttr('disabled');
	        $('#btn-submit').text("Edit BOM");
	        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
	    });
	  }));
</script>