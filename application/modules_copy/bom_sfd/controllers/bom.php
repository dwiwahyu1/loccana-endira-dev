<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Bom extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('bom/bom_model');
		$this->load->model('quotation/quotation_model');
		$this->load->model('material/material_model');
		$this->load->library('log_activity');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
    * This function is redirect to index bom page
    * @return Void
    */
	public function index() {
		$this->template->load('maintemplate', 'bom/views/index');
	}

	/**
    * This function is used for showing bom list
    * @return Array
    */
	function lists() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;
		$order_fields = array('', 'order_no', 'name_eksternal');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);
		$cust_id = $this->input->get_post('cust_id');

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;
		$params['cust_id'] = $cust_id;

		$list = $this->bom_model->lists($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["customer"] = $this->quotation_model->customer();
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$actions = '<div class="btn-group">';
            $actions .= '   <button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edit_bom(\'' . $v['bom_no'] .'\')">';
            $actions .= '       <i class="fa fa-edit"></i>';
            $actions .= '   </button>';
            $actions .= '</div>';

			array_push($data, array(
				$i,
				$v['order_no'],
				$v['bom_no'],
				$v['part_no'],
				$v['name_eksternal'],
				$v['total_item'],
				$actions
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	/**
    * This function is redirect to edit bom page
    * @return Void
    */
  public function edit($id) {
      $item = $this->bom_model->item(3);
      $mainitem = $this->bom_model->item(1);
      $detail = $this->bom_model->detail($id);

      $data = array(
          'item' => $item,
          'mainitem' => $mainitem,
          'detail' => $detail
      );
      
      $this->load->view('edit_modal_view',$data);
  }

  /**
    * This function is redirect to edit bom page
    * @return Void
    */
  public function detail_item($id) {
      $result = $this->material_model->edit($id);

      $data = array(
          'uom' => $result,
      );

      $this->load->view('edit_modal_material_view',$data);
  }

  /**
      * This function is used to edit bom data
      * @return Array
      */
    public function edit_bom() {
        $data   = file_get_contents("php://input");
        $params = json_decode($data,true);

        $schedule = $this->bom_model->edit_bom($params);

        $msg = 'Berhasil mengubah bom';

        $result = array(
            'success' => true,
            'message' => $msg
        );

		$this->log_activity->insert_activity('update', $msg);

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
}