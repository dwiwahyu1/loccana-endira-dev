<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Bom_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	/**
      * This function is get the list data in coa table
      * @param : $params is where condition for select query
      */

	public function lists($params = array())
	{
		$sql_all 	= 'CALL bom_list_all(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out2 = array();
		$query_all 	=  $this->db->query($sql_all,
			array(
				NULL,
				NULL,
				NULL,
				NULL,
				$params['filter'],
				$params['cust_id']
			));

		$result_all = $query_all->result_array();
		$total_row = $result_all[0]['count_all'];
		
		$this->load->helper('db');
		free_result($this->db->conn_id);

		$sql 	= 'CALL bom_list(?, ?, ?, ?, ?, ?, @total_filtered, @total)';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$params['limit'],
				$params['offset'],
				$params['order_column'],
				$params['order_dir'],
				$params['filter'],
				$params['cust_id']
			));

		$result = $query->result_array();

		$this->load->helper('db');
		free_result($this->db->conn_id);

		$total = $this->db->query('select @total_filtered, @total')->row_array();

		$return = array(
			'data' => $result,
			'total_filtered' => $total_row,
			'total' => $total_row,
		);

		return $return;
	}

	/**
      * This function is get data in bom table by id
      * @param : $id is where condition for select query
      */

	public function detail($id)
	{
		$sql 	= 'CALL bom_search_id(?)';

		$query 	= $this->db->query($sql,array(
				$id
			));
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	/**
      * This function is get data in production_schedule table by id
      * @param : $id is where condition for select query
      */

	public function edit_bom($data)
	{
		$itemsLen = count($data);

		if($itemsLen){
			$this->db->where('id_po_quot', $data[0]['id_po_quot']);
				  
			$this->db->delete('t_bom');

			for($j=0;$j<$itemsLen;$j++){
				$sql 	= 'CALL bom_add(?, ?, ?, ?, ?, ?)';

				$this->db->query($sql,array(
					$data[$j]['bom_no'],
					$data[$j]['id_po_quot'],
					$data[$j]['id_produk'],
					$data[$j]['id_mainproduk'],
					$data[$j]['qty'],
					$data[$j]['qty_main']
				));
			}

			$this->db->close();
			$this->db->initialize();
		}
	}

	/**
      * This function is get the list data in material table
      */
	public function item($type)
	{
		$this->db->select('id,stock_name');
		$this->db->from('m_material');
		$this->db->where('type', $type);
		$this->db->where('status', 1);

		$query 	= $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
}