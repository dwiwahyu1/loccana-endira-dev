<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class BC_Keluar extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('bc_keluar/bc_keluar_model');
		$this->load->library('log_activity');
		$this->load->library('sequence');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		$this->template->load('maintemplate', 'bc_keluar/views/index');
	}

	function list_bc() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 2;
		$order_fields = array('', 'jenis_bc', 'no_pendaftaran', 'no_pengajuan', 'tgl_pengajuan');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['filter'] = $search_value;

		$list = $this->bc_keluar_model->list_bc($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		$data = array();
		$i = 0;
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$btnstr = '';

			if($v['file_loc'] != '' || $v['file_loc'] != NULL) {
				$btnstr =
					'<div class="btn-group">'.
						'<a href="'. $v['file_loc'] .'" class="btn btn-info btn-sm" target="_blank" title="Download File" download>'.
							'<i class="fa fa-file"></i>'.
						'</a>'.
					'</div>';
			}

			$status_akses =
				'<div class="btn-group">'.
					'<button class="btn btn-warning btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
						onClick="edit_bc(\'' . $v['id'] . '\')">'.
						'<i class="fa fa-edit"></i>'.
					'</button>'.
				'</div>'.
				'<div class="btn-group">'.
					'<button class="btn btn-danger btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Delete"
						onClick="delete_bc(\'' . $v['id'] . '\')">'.
						'<i class="fa fa-trash"></i>'.
					'</button>'.
				'</div>'.
				'<div class="btn-group">'.
					'<button class="btn btn-info btn-sm" type="button" data-toggle="tooltip" data-placement="top" title="Details"
						onClick="details_bc(\'' . $v['id'] . '\')">'.
						'<i class="fa fa-list-alt"></i>'.
					'</button>'.
				'</div>';

			array_push($data, array(
				$i,
				$v['jenis_bc'],
				ucwords($v['no_pendaftaran']),
				ucwords($v['no_pengajuan']),
				date('d M Y', strtotime($v['tanggal_pengajuan'])),
				$btnstr,
				$status_akses
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_bc() {
		$result_type = $this->bc_keluar_model->type_bc();
		$result_do = $this->bc_keluar_model->list_do();
		$data = array(
			'jenis_bc' => $result_type,
			'do' => $$result_do
		);

		$this->load->view('add_modal_view', $data);
	}

	public function check_nopendaftaran() {
		$this->form_validation->set_rules('nopendaftaran', 'No Pendaftaran', 'trim|required|min_length[4]|max_length[100]|is_unique[t_bc.no_pendaftaran]');
		$this->form_validation->set_message('is_unique', 'No Pendaftaran Already Exists.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'No Pendaftaran Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}

	public function check_nopengajuan() {
		$this->form_validation->set_rules('nopengajuan', 'No Pengajuan', 'trim|required|min_length[4]|max_length[100]|is_unique[t_bc.no_pengajuan]');
		$this->form_validation->set_message('is_unique', 'No Pengajuan Already Exists.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'No pengajuan Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}

	public function save_bc() {
		$this->form_validation->set_rules('jenis_bc', 'Jenis BC', 'trim|required');
		$this->form_validation->set_rules('nopendaftaran', 'No Pendaftaran', 'trim|required|min_length[4]|max_length[100]|is_unique[t_bc.no_pendaftaran]');
		$this->form_validation->set_rules('tglpengajuan', 'Tanggal Pengajuan', 'trim|required|max_length[100]');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$jenis_bc = $this->Anti_sql_injection($this->input->post('jenis_bc', TRUE));
			$nopendaftaran = $this->Anti_sql_injection($this->input->post('nopendaftaran', TRUE));
			$nopengajuan = ucwords($this->Anti_sql_injection($this->input->post('nopengajuan', TRUE)));
			$no_aju = $this->sequence->get_max_no('bc_masuk_40');
			$tglinput = $this->Anti_sql_injection($this->input->post('tglpengajuan', TRUE));
			$temptgl = explode("/", $tglinput);
			$tglpengajuan = date('Y-m-d', strtotime($temptgl[2].'-'.$temptgl[1].'-'.$temptgl[0]));
			$type_text = ucwords($this->Anti_sql_injection($this->input->post('type_text', TRUE)));
			$upload_error = NULL;
			$file_bc = NULL;

			if ($_FILES['file_bc']['name']) {
				$this->load->library('upload');
				$new_filename =
					preg_replace('/\s/', '', $type_text) .' '.
					$nopendaftaran .' '.
					date('d-m-Y', strtotime($temptgl[2].'-'.$temptgl[1].'-'.$temptgl[0])).
					'.'.pathinfo($_FILES['file_bc']['name'], PATHINFO_EXTENSION);

				$config = array(
					'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/bc",
					'upload_url' => base_url() . "uploads/bc",
					'encrypt_name' => FALSE,
					'max_filename' => 100,
					'file_name' => $new_filename,
					'overwrite' => FALSE,
					'allowed_types' => 'pdf|txt|doc|docx',
					'max_size' => '10000'
				);
				$this->upload->initialize($config);

				if ($this->upload->do_upload("file_bc")) {
					// General result data
					$result = $this->upload->data();

					// Add our stuff
					$file_bc = 'uploads/bc/'.$result['file_name'];
				}else {
					$pesan = $this->upload->display_errors();
					$upload_error = strip_tags(str_replace("\n", '', $pesan));

					$result = array(
						'success' => false,
						'message' => $upload_error
					);
				}
			}

			if (!isset($upload_error)) {
				$data = array(
					'jenis_bc' => $jenis_bc,
					'no_pendaftaran' => $nopendaftaran,
					'no_pengajuan' => $nopengajuan,
					'tanggal_pengajuan' => $tglpengajuan,
					'file_loc' => $file_bc
				);

				$result = $this->bc_keluar_model->add_bc($data);

				if ($result > 0) {
					$this->log_activity->insert_activity('insert', 'Insert BC Keluar');
					$result = array('success' => true, 'message' => 'Berhasil menambahkan BC.');
				}else {
					$this->log_activity->insert_activity('insert', 'Gagal Insert BC Keluar');
					$result = array('success' => false, 'message' => 'Gagal menambahkan BC ke database.');
				}
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function edit_bc($id) {
		$result = $this->bc_keluar_model->edit_bc($id);
		$result_type = $this->bc_keluar_model->type_bc();
		$temptgl = explode('-', $result[0]['tanggal_pengajuan']);
		$result[0]['tanggal_pengajuan'] = date('d/M/Y', strtotime($temptgl[0].'-'.$temptgl[1].'-'.$temptgl[2]));
		if($result[0]['file_loc'] != '' || $result[0]['file_loc'] != NULL) $result[0]['file_loc'] = explode('uploads/bc/', $result[0]['file_loc'])[1];

		$data = array(
			'bc' => $result,
			'jenis_bc' => $result_type,
			'folder' => 'uploads/bc/',
			'url' => base_url()
		);

		$this->load->view('edit_modal_view', $data);
	}

	public function save_edit_bc() {
		$this->form_validation->set_rules('jenis_bc', 'Jenis BC', 'trim|required');
		$this->form_validation->set_rules('nopendaftaran', 'No Pendaftaran', 'trim|required|min_length[4]|max_length[100]');
		$this->form_validation->set_rules('nopengajuan', 'No Pengajuan', 'trim|required|min_length[4]|max_length[100]');
		$this->form_validation->set_rules('tglpengajuan', 'Tanggal Pengajuan', 'trim|required|max_length[100]');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$bc_id = $this->Anti_sql_injection($this->input->post('bc_id', TRUE));
			$old_file = $this->Anti_sql_injection($this->input->post('old_file', TRUE));
			$jenis_bc = $this->Anti_sql_injection($this->input->post('jenis_bc', TRUE));
			$nopendaftaran = $this->Anti_sql_injection($this->input->post('nopendaftaran', TRUE));
			$nopengajuan = ucwords($this->Anti_sql_injection($this->input->post('nopengajuan', TRUE)));
			$tglinput = $this->Anti_sql_injection($this->input->post('tglpengajuan', TRUE));
			$temptgl = explode("/", $tglinput);
			$tglpengajuan = date('Y-m-d', strtotime($temptgl[2].'-'.$temptgl[1].'-'.$temptgl[0]));
			$type_text = ucwords($this->Anti_sql_injection($this->input->post('type_text', TRUE)));

			$upload_error = NULL;
			$file_bc = NULL;
			if ($_FILES['file_bc']['name']) {
				$this->load->library('upload');
				$new_filename =
					preg_replace('/\s/', '', $type_text) .' '.
					$nopendaftaran .' '.
					date('d-m-Y', strtotime($temptgl[2].'-'.$temptgl[1].'-'.$temptgl[0])).
					'.'.pathinfo($_FILES['file_bc']['name'], PATHINFO_EXTENSION);

				$config = array(
					'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/bc",
					'upload_url' => base_url() . "uploads/bc",
					'encrypt_name' => FALSE,
					'max_filename' => 100,
					'file_name' => $new_filename,
					'overwrite' => FALSE,
					'allowed_types' => 'pdf|txt|doc|docx',
					'max_size' => '10000'
				);
				$this->upload->initialize($config);

				if ($this->upload->do_upload("file_bc")) {
					// General result data
					$result = $this->upload->data();

					// Add our stuff
					$file_bc = 'uploads/bc/'.$result['file_name'];
				}else {
					$pesan = $this->upload->display_errors();
					$upload_error = strip_tags(str_replace("\n", '', $pesan));

					$result = array(
						'success' => false,
						'message' => $upload_error
					);
				}
			}

			if (!isset($upload_error)) {
				if($file_bc == '' || $file_bc == NULL) {
					$file_bc = '/uploads/bc/'.$old_file;
				}else {
					$filepath = dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/bc/" . $old_file;
					if (is_file($filepath)) {
						unlink($filepath);
					}
				}
				$data = array(
					'bc_id' => $bc_id,
					'jenis_bc' => $jenis_bc,
					'no_pendaftaran' => $nopendaftaran,
					'no_pengajuan' => $nopengajuan,
					'tanggal_pengajuan' => $tglpengajuan,
					'file_loc' => $file_bc
				);
				$result = $this->bc_keluar_model->save_edit_bc($data);

				if ($result > 0) {
					$this->log_activity->insert_activity('update', 'Update BC Keluar id : '.$bc_id);
					$result = array('success' => true, 'message' => 'Berhasil merubah BC');
				} else {
					$this->log_activity->insert_activity('update', 'Gagal Update BC Keluar id : '.$bc_id);
					$result = array('success' => false, 'message' => 'Gagal merubah BC');
				}
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function delete_bc() {
		$data 	= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		$result = $this->bc_keluar_model->edit_bc($params['id']);

		//Delete File if exists
		$filepath = dirname($_SERVER["SCRIPT_FILENAME"]) ."/". $result[0]['file_loc'];
		if (is_file($filepath)) {
			unlink($filepath);
		}
		$result = $this->bc_keluar_model->delete_bc($params['id']);
		if($result > 0) {
			$this->log_activity->insert_activity('delete', 'Delete BC Keluar id : '.$params['id']);
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}else {
			$this->log_activity->insert_activity('delete', 'Gagal Delete BC Keluar id : '.$params['id']);
			$res = array('status' => 'failed', 'message' => 'Data telah di hapus');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}

	public function detail_bc($id) {
		$data = array(
			'bc_id' => $id
		);

		$this->load->view('detail_bc_view', $data);
	}

	public function list_detail_bc($id) {
		$result = $this->bc_keluar_model->detail_bc($id);
		$result_bc = $this->bc_keluar_model->edit_bc($id);
		$result_stock = $this->bc_keluar_model->stock($id);

		$data = array();
		$i = 0;
		foreach ($result['data'] as $k => $v) {
			$i = $i + 1;
			$bc_stock = '';
			$status_akses =
				'<div class="btn-group">'.
					'<button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit"
						onClick="edit_detail_bc(\'' . $v['id'] . '\')">'.
						'<i class="fa fa-edit"></i>'.
					'</button>'.
				'</div>'.
				'<div class="btn-group">'.
					'<button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete"
						onClick="delete_detail_bc(\'' . $v['id'] . '\')">'.
						'<i class="fa fa-trash"></i>'.
					'</button>'.
				'</div>';
			/*foreach ($result_stock as $s => $st) {
				if($st['id'] == $v['kode_stock']) $bc_stock = $st['stock'];
			}*/

			array_push($data, array(
				$i,
				ucwords($v['kode_barang_bc']),
				ucwords($v['kode_barang']),
				number_format($v['uom'], 2,",","."),
				number_format($v['valas'], 2,",","."),
				'Rp '. number_format(floatval($v['price']), 2,",","."),
				number_format($v['weight'], 2,",","."),
				number_format($v['qty'], 2,",","."),
				$status_akses
			));
		}

		$result['data'] = $data;
		$result['draw'] = 1;
		$result['recordsFiltered'] = 2;
		$result['recordsTotal'] = sizeof($data);
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_detail_bc() {
		$result_uom = $this->bc_keluar_model->search_uom();
		$result_valas = $this->bc_keluar_model->search_valas();
		//$result_stock = $this->bc_keluar_model->stock();
		
		$data = array(
			'uom' => $result_uom,
			'valas' => $result_valas
			//'stock' => $result_stock
		);

		$this->load->view('add_modal_d_bc_view', $data);
	}

	public function check_kd_brg_bc(){
		$this->form_validation->set_rules('kd_brg_bc', 'KodeBarangBC', 'trim|required|min_length[4]|max_length[20]|is_unique[d_bc_list.kode_barang_bc]');
		$this->form_validation->set_message('is_unique', 'Kode barang BC Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'Kode Barang BC Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}
	
	public function check_kd_brg(){
		$this->form_validation->set_rules('kd_brg', 'KodeBarang', 'trim|required|min_length[4]|max_length[20]|is_unique[d_bc_list.kode_barang]');
		$this->form_validation->set_message('is_unique', 'Kode barang Already Registered.');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$return = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		} else if ($this->form_validation->run() == TRUE) {
			$return = array('success' => true, 'message' => 'Kode Barang Available');
			$this->output->set_content_type('application/json')->set_output(json_encode($return));
		}
	}
	
	public function save_detail() {
		$this->form_validation->set_rules('kd_brg_bc', 'KodeBarangBC', 'trim|required|min_length[4]|max_length[20]|is_unique[d_bc_list.kode_barang_bc]',array('is_unique' => 'This %s Kode Barang BC already exists.'));
		$this->form_validation->set_rules('kd_brg', 'KodeBarang', 'trim|required|min_length[4]|max_length[20]|is_unique[d_bc_list.kode_barang]',array('is_unique' => 'This %s Kode Barang already exists.'));
		//$this->form_validation->set_rules('stock_id', 'Stock', 'trim|required');
		// $this->form_validation->set_rules('prop_id', 'Properties', 'trim|required');
		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$bc_id = $this->Anti_sql_injection($this->input->post('bc_id', TRUE));
			$kd_brg_bc = strtoupper($this->Anti_sql_injection($this->input->post('kd_brg_bc', TRUE)));
			$kd_brg = strtoupper($this->Anti_sql_injection($this->input->post('kd_brg', TRUE)));
			$uom_id = $this->Anti_sql_injection($this->input->post('uom_id', TRUE));
			$valas_id = $this->Anti_sql_injection($this->input->post('valas_id', TRUE));
			$price = $this->Anti_sql_injection($this->input->post('price', TRUE));
			$weight = $this->Anti_sql_injection($this->input->post('weight', TRUE));
			$qty = $this->Anti_sql_injection($this->input->post('qty', TRUE));

			$data = array(
				'id_bc' 			=> $bc_id,
				'kode_barang_bc' 	=> $kd_brg_bc,
				'kode_barang' 		=> $kd_brg,
				'uom'		 		=> $uom_id,
				'valas' 			=> $valas_id,
				'price' 			=> $price,
				'weight' 			=> $weight,
				'qty' 				=> $qty
			);
			
			$result = $this->bc_keluar_model->add_detail_bc($data);
			
			if($result > 0) {
				$cek_kode_barang = $this->bc_keluar_model->search_kode_barang($kd_brg);
				if(count($cek_kode_barang) > 0) {
					$data_material = array(
						'kode_barang_bc' 	=> $kd_brg_bc,
						'kode_barang' 		=> $cek_kode_barang[0]['stock_code'],
						'stock_name' 		=> $cek_kode_barang[0]['stock_name'],
						'stock_description'	=> $cek_kode_barang[0]['stock_description'],
						'unit'		 		=> $uom_id,
						'type'		 		=> $cek_kode_barang[0]['type'],
						'qty' 				=> $qty,
						'treshold'	 		=> $cek_kode_barang[0]['treshold'],
						'id_properties' 	=> $cek_kode_barang[0]['id_properties'],
						'id_gudang' 		=> NULL,
						'status' 			=> 3
					);
				}else{
					$data_material = array(
						'kode_barang_bc' 	=> $kd_brg_bc,
						'kode_barang' 		=> $kd_brg,
						'stock_name' 		=> NULL,
						'stock_description'	=> NULL,
						'unit'		 		=> $uom_id,
						'type'		 		=> NULL,
						'qty' 				=> $qty,
						'treshold'	 		=> 10,
						'id_properties' 	=> NULL,
						'id_gudang' 		=> NULL,
						'status' 			=> 3
					);
				}
				
				$add = $this->bc_keluar_model->add_material($data_material);
				
				if($add['result'] > 0) {
					$this->bc_keluar_model->add_bc_stock($data['id_bc'],$add['lastid']);
					$this->bc_keluar_model->add_bc_price($data['price'],$data['valas'],$add['lastid']);
					$this->log_activity->insert_activity('insert', 'Insert Detail BC Keluar id : '.$bc_id);
				}
				$result = array('success' => true, 'message' => 'Berhasil menambahkan detail bea cukai ke database');
			}else{
				$this->log_activity->insert_activity('insert', 'Gagal Insert Detail BC Keluar id : '.$bc_id);
				$result = array('success' => false, 'message' => 'Gagal menambahkan detail bea cukai ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function edit_detail_bc($id) {
		$result = $this->bc_keluar_model->edit_detail_bc($id);
		$result_uom = $this->bc_keluar_model->search_uom();
		$result_valas = $this->bc_keluar_model->search_valas();
		//$result_stock = $this->bc_keluar_model->stock();
		
		$data = array(
			'detail' => $result,
			'uom' => $result_uom,
			'valas' => $result_valas
			//'stock' => $result_stock
		);
		// print_r($data);die;

		$this->load->view('edit_modal_d_bc_view', $data);
	}

	public function save_edit_detail() {
		$this->form_validation->set_rules('kd_brg_bc', 'KodeBarangBC', 'trim|required|min_length[4]|max_length[20]',array('is_unique' => 'This %s Kode Barang BC already exists.'));
		$this->form_validation->set_rules('kd_brg', 'KodeBarang', 'trim|required|min_length[4]|max_length[20]',array('is_unique' => 'This %s Kode Barang already exists.'));
		//$this->form_validation->set_rules('stock_id', 'Stock', 'trim|required');
		//var_dump($this->form_validation->run());die;
		
		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$dbc_id = $this->Anti_sql_injection($this->input->post('dbc_id', TRUE));
			$bc_id = $this->Anti_sql_injection($this->input->post('bc_id', TRUE));
			$kd_brg_bc = strtoupper($this->Anti_sql_injection($this->input->post('kd_brg_bc', TRUE)));
			$kd_brg = strtoupper($this->Anti_sql_injection($this->input->post('kd_brg', TRUE)));
			$uom_id = $this->Anti_sql_injection($this->input->post('uom_id', TRUE));
			$valas_id = $this->Anti_sql_injection($this->input->post('valas_id', TRUE));
			$price = $this->Anti_sql_injection($this->input->post('price', TRUE));
			$weight = $this->Anti_sql_injection($this->input->post('weight', TRUE));
			$qty = $this->Anti_sql_injection($this->input->post('qty', TRUE));

			$data = array(
				'id' 				=> $dbc_id,
				'id_bc' 			=> $bc_id,
				'kode_barang_bc' 	=> $kd_brg_bc,
				'kode_barang' 		=> $kd_brg,
				'uom'		 		=> $uom_id,
				'valas' 			=> $valas_id,
				'price' 			=> $price,
				'weight' 			=> $weight,
				'qty' 				=> $qty
			);

			$result = $this->bc_keluar_model->save_edit_detail($data);

			if ($result > 0) {
				$this->log_activity->insert_activity('update', 'Update Detail BC Keluar id : '.$dbc_id);
				$result = array('success' => true, 'message' => 'Berhasil mengubah detail bea cukai');
			}else {
				$this->log_activity->insert_activity('update', 'Gagal Update Detail BC Keluar id : '.$dbc_id);
				$result = array('success' => false, 'message' => 'Gagal mengubah detail bea cukai ke database');
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}
	}

	public function delete_detail_bc() {
		$data 		= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		$result 	= $this->bc_keluar_model->delete_detail_bc($params['id']);

		if($result > 0) {
			$this->log_activity->insert_activity('delete', 'Delete Detail BC Keluar id : '.$params['id']);
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}else {
			$this->log_activity->insert_activity('delete', 'Gagal Delete Detail BC Keluar id : '.$params['id']);
			$res = array('status' => 'failed', 'message' => 'Data gagal di hapus');
		}

		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
	}
}