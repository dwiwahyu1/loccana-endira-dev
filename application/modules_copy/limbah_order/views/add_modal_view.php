<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-center {
    text-align: center;
  }

  .dt-body-right {
    text-align: right;
  }

  #listItemMaintenance {
    counter-reset: rowNumber;
  }

  #listItemMaintenance tbody tr>td:first-child {
    counter-increment: rowNumber;
    text-align: center;
  }

  #listItemMaintenance tbody tr td:first-child::before {
    content: counter(rowNumber);
    min-width: 1em;
    margin-right: 0.5em;
  }
</style>

<form class="form-horizontal form-label-left" id="form_addLimbahOrder" role="form" action="<?php echo base_url('limbah_order/add_limbahOrder'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
  <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="delivery_date">Delivery Date <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="text" class="form-control" id="delivery_date" name="delivery_date" value="<?php
                                                                                              echo date('d-M-Y'); ?>" placeholder="Work Date" required>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cust_id">Customer Name <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <select class="form-control" id="cust_id" name="cust_id" style="width: 100%" required>
        <option value="" selected>-- Pilih Customer --</option>
        <?php
        foreach ($customer as $ck => $cv) { ?>
          <option value="<?php echo $cv['id']; ?>"><?php echo $cv['name_eksternal']; ?></option>
        <?php
        }
        ?>
      </select>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="term_of_pay">Term of Payment <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <select class="form-control" id="term_of_pay" name="term_of_pay" style="width: 100%" required>
        <option value="" selected>-- Select Term --</option>
        <option value="1 Day">1 Day</option>
        <option value="15 Day">15 Day</option>
        <option value="30 Day"> 30 Day</option>
        <option value="45 Day"> 45 Day</option>
        <option value="60 Day"> 60 Day</option>
        <option value="90 Day"> 90 Day</option>
        <option value="120 Day"> 120 Day</option>
        <option value="150 Day"> 150 Day</option>
        <option value="180 Day"> 180 Day</option>
      </select>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="valas">Valas <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <select class="form-control" id="valas" name="valas" style="width: 100%" required>
        <option value="" selected>-- Pilih Valas --</option>
        <?php foreach ($valas as $vd) { ?>
          <option value="<?php echo $vd['valas_id']; ?>"><?php echo $vd['nama_valas']; ?></option>
        <?php } ?>
      </select>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rate">Rate <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="number" class="form-control" id="rate" name="rate" placeholder="Rate" autocomplete="off" step=".0001" value='1' required>
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="diskon">Diskon <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="number" class="form-control" id="diskon" name="diskon" placeholder="Diskon" autocomplete="off" step=".0001" value='0' required>
    </div>
  </div>

  <div class="item form-group" style="display:none;">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tujuan">Tujuan<span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="hidden" class="form-control" id="tujuan" name="tujuan" placeholder="Tujuan" autocomplete="off" value="">
    </div>
  </div>
  <div class="item form-group" >
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tujuan">Nomor Jalan <span class="required"><sup>*</sup></span></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <input type="text" class="form-control" id="no_jalan" name="no_jalan" placeholder="Nomor Jalan" autocomplete="off" value="">
    </div>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12">Daftar Limbah : </label>
    <div class="col-md-8 col-sm-6 col-xs-12"></div>
  </div>

  <div class="item form-group">
    <table id="listItemMaintenance" class="table table-striped table-bordered dt-responsive nowrap" width="80%">
      <thead>
        <tr>
          <th class="dt-body-center" style="width: 5%;">No</th>
          <th >Nama Materia Limbah</th>
          <th >QTY</th>
          <th >Harga</th>
          <th >Amount</th>
          <th >Remark</th>
          <th ><a id="btn_item_add" class="btn btn-primary"><i class="fa fa-plus"></i></a></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td></td>
          <td>
            <select class="form-control" id="no_dm1" name="no_dm[]">
              <option value="" selected>-- Pilih Material Limbah --</option>
              <?php
              foreach ($maintenance as $mk => $mv) { ?>
                <option value="<?php echo $mv['id']; ?>"><?php echo $mv['stock_code'].'-'.$mv['stock_name'].' ('.$mv['uom_symbol'].')'; ?></option>
              <?php
              }
              ?>
            </select>
          </td>
          <td>
            <input type="number" class="form-control" id="qty1" name="qty[]" placeholder="Qty" autocomplete="off" step=".0001" required>
          </td>
          <td>
            <input type="price" class="form-control" id="price1" name="price[]" placeholder="Harga" autocomplete="off" step=".0001" required>
          </td>
          <!-- <td>
            <label id="qty_main1"></label>
          </td> -->
          <!-- <td>
						<input type="number" class="form-control" id="dm_price1" name="dm_price[]" placeholder="Harga" autocomplete="off" step=".0001" disabled>
					</td> -->
          <td class="dt-body-right">
						<label id="rowAmount1">0.0000</label> 
						<input type="hidden" id="amount1" name="amount[]" value="0">
					 </td>
          <td>
            <textarea class="form-control" id="desc_main1" name="desc_main[]" placeholder="Deskripsi" rows="3" readonly></textarea>
          </td>
          <td></td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td class="dt-body-right" colspan="3" rowspan="3"></td>
          <td class="dt-body-right">Total</td>
          <td class="dt-body-right"><label id="totalAmountRow">0.0000</label></td>
          <td colspan="3" rowspan="3"></td>
        </tr>
        <tr>
          <td class="dt-body-right">Diskon</td>
          <td class="dt-body-right"><label id="totalAmountDiskon">0</label></td>
        </tr>
        <tr>
          <td class="dt-body-right">Total Amount</td>
          <td class="dt-body-right"><label id="totalAmount">0.0000</label></td>
        </tr>
      </tfoot>
    </table>
  </div>

  <div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
    <div class="col-md-8 col-sm-6 col-xs-12">
      <button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Tambah Limbah Order</button>
      <input type="hidden" id="total_amount" name="total_amount" value="0">
    </div>
  </div>
</form>

<script type="text/javascript">
  var idRowItem = 1;
  var arrNoDm = [];
  var arrPrice = [];
  var arrRemark = [];

  $(document).ready(function() {
    $("#delivery_date").datepicker({
      format: 'dd-M-yyyy',
      autoclose: true,
      todayHighlight: true,
    });

    $('#cust_id').select2();
    $('#term_of_pay').select2();
    $('#valas').select2();
    $('#diskon').on('keyup', function() {
      cal_amount();
    })
    $('#diskon').on('change', function() {
      cal_amount();
    })

    $('#no_dm1').select2();
    $('#no_dm1').on('change', function() {
      set_itemValue(this.value, this.id);
      // cal_amount();
    })

    $('#price1').on('keyup', function() {
      cal_amount();
    })
    $('#price1').on('change', function() {
      cal_amount();
    })
  });

  $('#btn_item_add').on('click', function() {
    idRowItem++;
    $('#listItemMaintenance tbody').append(
      '<tr id="trRowItem' + idRowItem + '">' +
      '<td></td>' +
      '<td>' +
      '<select class="form-control" id="no_dm' + idRowItem + '" name="no_dm[]" disabled>' +
      '<option value="" selected>-- Pilih Material Limbah --</option>' +
      '</select>' +
      '</td>' +
      //<td>
      //<input type="number" class="form-control" id="qty" name="qty[]" placeholder="Qty" autocomplete="off" step=".0001" required>
      //</td>
      //<td>
      //<input type="price" class="form-control" id="price" name="price[]" placeholder="Harga" autocomplete="off" step=".0001" required>
      //</td>
      '<td>' +
      '<input type="number" class="form-control" id="qty' + idRowItem + '" name="qty[]" placeholder="Harga" autocomplete="off" step=".0001" required>' +
      '</td>' +
      '<td>' +
      '<input type="number" class="form-control" id="price' + idRowItem + '" name="price[]" placeholder="Harga" autocomplete="off" step=".0001" required>' +
      '</td>' +
      '<td class="dt-body-right">' +
      '<label id="rowAmount' + idRowItem + '">0.0000</label>' +
      '<input type="hidden" id="amount' + idRowItem + '" name="amount[]" value="0">' +
       '</td>' +
      '<td>' +
      '<textarea class="form-control" id="desc_main' + idRowItem + '" name="desc_main[]" placeholder="Deskripsi" rows="3" readonly></textarea>' +
      '</td>' +
      '<td>' +
      '<a class="btn btn-danger" onclick="removeRow(' + idRowItem + ')"><i class="fa fa-minus"></i></a>' +
      '</td>' +
      '</tr>'
    );
    $("#no_dm" + idRowItem).select2();
    $('#no_dm' + idRowItem).on('change', function() {
      set_itemValue(this.value, this.id);
      // cal_amount();
    })
    $('#price' + idRowItem).on('keyup', function() {
      cal_amount();
    })
    $('#price' + idRowItem).on('change', function() {
      cal_amount();
    })

    $.ajax({
      type: "GET",
      url: "<?php echo base_url('limbah_order/get_material'); ?>",
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      success: function(r) {
        if (r.length > 0) {
          for (var i = 0; i < r.length; i++) {
            var newOption = new Option(r[i].stock_code+'-'+r[i].stock_name+' ('+r[i].uom_symbol+')', r[i].id_detail_main, false, false);
            $('#no_dm' + idRowItem).append(newOption);
          }
          $('#no_dm' + idRowItem).removeAttr('disabled');
        } else $('#no_dm' + idRowItem).removeAttr('disabled');
      }
    });
  })

  function set_itemValue(dm, rowSelect) {
    var row = rowSelect.replace(/[^0-9]+/g, "");
    if (dm != '' && dm != null) {
      $.ajax({
        type: "GET",
        url: "<?php echo base_url('limbah_order/get_material_desc'); ?>" + '/' + dm,
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        success: function(r) {
          if (r.length > 0) {
            // $('#nama_main' + row).html(r[0].nama_main);
            // $('#qty_main' + row).html(formatCurrencyComaNull(r[0].qty));
            // $('#uom_main' + row).html(r[0].stock_name);
            // $('#dm_price' + row).val(0).removeAttr('disabled');
            // $('#amount' + row).val(0);
            $('#desc_main' + row).val(r[0].stock_description);
          }
        }
      });
    } else {
      // $('#nama_main' + row).html('');
      // $('#qty_main' + row).html('');
      // $('#uom_main' + row).html('');
      // $('#dm_price' + row).val('').attr('disabled', 'disabled');
      // $('#amount' + row).val(0);
      $('#desc_main' + row).val('').attr('disabled', 'disabled');
    }
  }

  function removeRow(rowItem) {
    $('#trRowItem' + rowItem).remove();
    cal_amount();
  }

  function cal_amount() {
    var diskon = $('#diskon').val();
    var tempDiskon = 0;
    var qtyRow = 0;
    var price = 0;
    var totalRow = 0;
    var tempTotal = 0;
    var totalAmount = 0;
    if (diskon == '' || diskon == undefined || diskon == null) diskon = 0;

    $('input[name="price[]"]').each(function() {
      var idRow = this.id.replace(/[^0-9]+/g, "");

      var tempQty = $('#qty' + idRow).val();
      if (tempQty != '' && tempQty != null && tempQty != undefined) qtyRow = parseFloat(tempQty);
      if (this.value != '' && this.value != null && this.value != undefined) price = parseFloat(this.value);
      console.log(totalRow,price,qtyRow);
      totalRow = (price * qtyRow);
      if (this.value != '' && this.value != undefined && this.value != null) {
        $('#rowAmount' + idRow).html(formatCurrencyComa(totalRow));
        $('#amount' + idRow).val(totalRow);
      } else {
        $('#rowAmount' + idRow).html(formatCurrencyComa(0));
        $('#amount' + idRow).val(0);
      }
    })

    $('input[name="amount[]"]').each(function() {
      var amountRow = 0;
      if (this.value != '' && this.value != null && this.value != undefined) amountRow = parseFloat(this.value);
      tempTotal = (tempTotal + amountRow);
    });

    if (diskon > 100) {
      totalAmount = tempTotal - diskon;
      $('#totalAmountDiskon').html(formatCurrencyComa(parseFloat(diskon)));
    } else {
      var tempTotalvDiskon = 0;
      tempDiskon = diskon / 100;
      tempTotalvDiskon = tempTotal * tempDiskon;
      totalAmount = (tempTotal - tempTotalvDiskon);
      $('#totalAmountDiskon').html(diskon + '%');
    }

    $('#totalAmountRow').html(formatCurrencyComa(parseFloat(tempTotal)));
    $('#totalAmount').html(formatCurrencyComa(parseFloat(totalAmount)));
    $('#total_amount').val(totalAmount);
  }

  $('#form_addLimbahOrder').on('submit', (function(e) {
    cal_amount();
    arrNoDm = [];
    arrPrice = [];
    arrRemark = [];

    $('#btn-submit').attr('disabled', 'disabled');
    $('#btn-submit').text("Memasukkan data...");
    e.preventDefault();

    $('select[name="no_dm[]"]').each(function() {
      if (this.value) {
        if (this.value != undefined && this.value != '') arrNoDm.push(this.value);
      }
    })

    $('input[name="qty[]"]').each(function() {
      if (this.value) {
        if (this.value != undefined && this.value != '') arrPrice.push(this.value);
      }
    })

    $('input[name="price[]"]').each(function() {
      if (this.value) {
        if (this.value != undefined && this.value != '') arrRemark.push(this.value);
        else arrRemark.push('NULL');
      } else arrRemark.push('NULL');
    })
    if (arrNoDm.length > 0 && arrPrice.length > 0 && arrRemark.length > 0) {
      var formData = new FormData(this);
      // formData.set('delivery_date', $('#delivery_date').val());
      // formData.set('cust_id', $('#cust_id').val());
      // formData.set('term_of_pay', $('#term_of_pay').val());
      // formData.set('valas', $('#valas').val());
      // formData.set('rate', $('#rate').val());
      // formData.set('diskon', $('#diskon').val());
      // formData.set('tujuan', $('#tujuan').val());
      // formData.set('total_amount', $('#total_amount').val());
      // formData.set('arrNoDm', arrNoDm);
      // formData.set('arrPrice', arrPrice);
      // formData.set('arrRemark', arrRemark);

      $.ajax({
        type: 'POST',
        url: $(this).attr('action'),
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
          if (response.success == true) {
            swal({
              title: 'Success!',
              text: response.message,
              type: 'success',
              showCancelButton: false,
              confirmButtonText: 'Ok'
            }).then(function() {
              $('#panel-modal').modal('toggle');
              listLimbahOrder();
            })
          } else {
            $('#btn-submit').removeAttr('disabled');
            $('#btn-submit').text("Tambah Limbah Order");
            swal("Failed!", response.message, "error");
          }
        }
      }).fail(function(xhr, status, message) {
        $('#btn-submit').removeAttr('disabled');
        $('#btn-submit').text("Tambah Limbah Order");
        swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
      });
    } else {
      $('#btn-submit').removeAttr('disabled');
      $('#btn-submit').text("Tambah Limbah Order");
      swal("Failed!", "Invalid Inputan List Limbah, silahkan cek kembali.", "error");
    }
  }));
</script>