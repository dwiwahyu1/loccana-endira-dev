<style>
	.dt-body-left {text-align: left;}
	.dt-body-center {text-align: center;}
	.dt-body-right {text-align: right;}

	#listItemMaintenance {
		counter-reset: rowNumber;
	}

	#listItemMaintenance tbody tr > td:first-child {
		counter-increment: rowNumber;
		text-align: center;
	}

	#listItemMaintenance tbody tr td:first-child::before {
		content: counter(rowNumber);
		min-width: 1em;
		margin-right: 0.5em;
	}
</style>

<form class="form-horizontal form-label-left" id="detail_mainOrder" role="form" action="<?php echo base_url('maintenance_order/detail_mainOrder'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="delivery_date">Delivery Date</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="delivery_date" name="delivery_date" value="<?php
				if(isset($mainOrder[0]['delivery_date'])) echo date('d-M-Y', strtotime($mainOrder[0]['delivery_date']));
			?>" placeholder="Work Date" disabled>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="cust_id">Customer Name</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="cust_id" name="cust_id" style="width: 100%" disabled>
				<option value="">-- Pilih Customer --</option>
		<?php
			foreach ($customer as $ck => $cv) { ?>
				<option value="<?php echo $cv['id']; ?>" <?php
				if(isset($mainOrder[0]['id_cust'])) {
					if($cv['id'] == $mainOrder[0]['id_cust']) echo "selected";
				} ?>><?php echo $cv['name_eksternal']; ?></option>
		<?php
			}
		?>
			</select>
	  </div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="term_of_pay">Term of Payment</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="term_of_pay" name="term_of_pay" style="width: 100%" disabled>
				<option value="">-- Select Term --</option>
				<option value="1 Day" <?php
					if(isset($mainOrder[0]['term_of_payment'])) if($mainOrder[0]['term_of_payment'] == '1 Day') echo "selected";
				?>>1 Day</option>
				<option value="15 Day" <?php
					if(isset($mainOrder[0]['term_of_payment'])) if($mainOrder[0]['term_of_payment'] == '15 Day') echo "selected";
				?>>15 Day</option>
				<option value="30 Day" <?php
					if(isset($mainOrder[0]['term_of_payment'])) if($mainOrder[0]['term_of_payment'] == '30 Day') echo "selected";
				?>> 30 Day</option>
				<option value="45 Day" <?php
					if(isset($mainOrder[0]['term_of_payment'])) if($mainOrder[0]['term_of_payment'] == '45 Day') echo "selected";
				?>> 45 Day</option>
				<option value="60 Day" <?php
					if(isset($mainOrder[0]['term_of_payment'])) if($mainOrder[0]['term_of_payment'] == '60 Day') echo "selected";
				?>> 60 Day</option>
				<option value="90 Day" <?php
					if(isset($mainOrder[0]['term_of_payment'])) if($mainOrder[0]['term_of_payment'] == '90 Day') echo "selected";
				?>> 90 Day</option>
				<option value="120 Day" <?php
					if(isset($mainOrder[0]['term_of_payment'])) if($mainOrder[0]['term_of_payment'] == '120 Day') echo "selected";
				?>> 120 Day</option>
				<option value="150 Day" <?php
					if(isset($mainOrder[0]['term_of_payment'])) if($mainOrder[0]['term_of_payment'] == '120 Day') echo "selected";
				?>> 150 Day</option>
				<option value="180 Day" <?php
					if(isset($mainOrder[0]['term_of_payment'])) if($mainOrder[0]['term_of_payment'] == '150 Day') echo "selected";
				?>> 180 Day</option>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="valas">Valas</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="valas" name="valas" style="width: 100%" disabled>
				<option value="" selected>-- Pilih Valas --</option>
				<?php foreach($valas as $vd) { ?>
					<option value="<?php echo $vd['valas_id']; ?>" <?php
					if(isset($mainOrder[0]['id_valas'])) {
						if($vd['valas_id'] == $mainOrder[0]['id_valas']) echo "selected";
					}
					?>><?php echo $vd['nama_valas']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="rate">Rate</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="number" class="form-control" id="rate" name="rate" placeholder="Rate" autocomplete="off" step=".0001" value="<?php
				if(isset($mainOrder[0]['rate'])) echo $mainOrder[0]['rate'];
			?>" disabled>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="diskon">Diskon</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="number" class="form-control" id="diskon" name="diskon" placeholder="Diskon" autocomplete="off" step=".0001" value="<?php
				if(isset($mainOrder[0]['diskon'])) echo $mainOrder[0]['diskon'];
			?>" disabled>
		</div>
	</div>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="diskon">No Delivery</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="no_delivery" name="no_delivery" placeholder="No Delivery" autocomplete="off" value="<?php
				if(isset($mainOrder[0]['no_delivery'])) echo $mainOrder[0]['no_delivery'];
			?>" disabled>
		</div>
	</div>

	<!-- <div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tujuan">Tujuan</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="tujuan" name="tujuan" placeholder="Tujuan" value="<?php
				// if(isset($mainOrder[0]['tujuan'])) echo $mainOrder[0]['tujuan'];
			?>" autocomplete="off" disabled>
		</div>
	</div> -->

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Daftar Maintenance : </label>
		<div class="col-md-8 col-sm-6 col-xs-12"></div>
	</div>

	<div class="item form-group">
		<table id="listItemMaintenance" class="table table-striped table-bordered dt-responsive nowrap" width="100%">
			<thead>
				<tr>
					<th>No</th>
					<th>Material Limbah</th>
					<th >QTY</th>
					<th >Harga</th>
					<th >Amount</th>
					<th >Remark</th>
				</tr>
			</thead>
			<tbody>
		<?php
			$i=1;
			$rowAmount 			= 0;
			$totalAmountRow		= 0;
			$totalAmount 		= 0;
			if(sizeof($detail) > 0) {
				foreach($detail as $dk) {
					$rowAmount 		= (floatval($dk['price'])) * (floatval($dk['qty_limbah']));
					$totalAmountRow = $totalAmountRow + $rowAmount; ?>
					<tr id="trRowItem<?php echo $i; ?>">
						<td></td>
						<td><?php echo $dk['stock_code'].' - '.$dk['stock_name'].' ('.$dk['uom_symbol'].')'; ?></td>
						<!-- <td><?php //echo $dk['qty']; ?></td>
						<td><?php //echo $dk['price']; ?></td> -->
						<td class="dt-body-center"><?php echo number_format($dk['qty_limbah'], 0); ?></td>
						<td class="dt-body-right"><?php echo $dk['symbol_valas'].' '.number_format($dk['price'], 4, ',', '.'); ?></td>
						<td class="dt-body-right"><?php echo $dk['symbol_valas'].' '.number_format($rowAmount, 4, '.', ',') ?></td>
						<td><?php echo $dk['remark']; ?></td>
					</tr>
		<?php
					$i++;
				}
			}else { ?>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td class="dt-body-center"></td>
					<td class="dt-body-center"></td>
					<td class="dt-body-right"></td>
					<td class="dt-body-right"></td>
					<td></td>
				</tr>
		<?php
			}
			
			if($dk['diskon'] >= 100) {
				$totalAmount = $totalAmountRow - $dk['diskon'];
			}else {
				$tempDiskon 	= $dk['diskon'] / 100;
				$tempTotal 		= $totalAmountRow * $tempDiskon;
				$totalAmount 	= ($totalAmountRow - $tempTotal);
			}
		?>
			</tbody>
			<tfoot>
				<tr>
				<td class="dt-body-right" colspan="3" rowspan="3"></td>
					<td class="dt-body-right">Total</td>
					<td class="dt-body-right"><label><?php echo $dk['symbol_valas'].' '.number_format($totalAmountRow, 4, ',', '.'); ?></label></td>
					<td colspan="3" rowspan="3"></td>
				</tr>
				<tr>
					<td class="dt-body-right">Diskon</td>
					<td class="dt-body-right"><?php
						if($dk['diskon'] >= 100) echo $dk['symbol_valas'].' '.number_format($dk['diskon'], 4, ',', '.');
						else echo $dk['diskon'].'%';
					?></td>
				</tr>
				<tr>
					<td class="dt-body-right">Total Amount</td>
					<td class="dt-body-right"><label><?php echo $dk['symbol_valas'].' '.number_format($totalAmount, 4, ',', '.'); ?></label></td>
				</tr>
			</tfoot>
		</table>
	</div>
</form>

<script type="text/javascript">
	$(document).ready(function() {
		$("#delivery_date").datepicker({
			format: 'dd-M-yyyy',
			autoclose: true,
			todayHighlight: true,
		});

		$('#cust_id').select2();
		$('#term_of_pay').select2();
		$('#valas').select2();
	});
</script>