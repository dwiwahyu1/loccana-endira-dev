<style>
	.dt-body-left {text-align: left;}
	.dt-body-center {text-align: center;}
	.dt-body-right {text-align: right;}

	#listItemMaintenance {
		counter-reset: rowNumber;
	}

	#listItemMaintenance tbody tr > td:first-child {
		counter-increment: rowNumber;
		text-align: center;
	}

	#listItemMaintenance tbody tr td:first-child::before {
		content: counter(rowNumber);
		min-width: 1em;
		margin-right: 0.5em;
	}
</style>

<form class="form-horizontal form-label-left" id="edit_mainOrder" role="form" action="<?php echo base_url('limbah_order/edit_limbahOrder'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>
	<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="delivery_date">Delivery Date <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="delivery_date" name="delivery_date" value="<?php
				if(isset($mainOrder[0]['delivery_date'])) echo date('d-M-Y', strtotime($mainOrder[0]['delivery_date']));
			?>" placeholder="Work Date" required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="cust_id">Customer Name <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="cust_id" name="cust_id" style="width: 100%" required>
				<option value="">-- Pilih Customer --</option>
		<?php
			foreach ($customer as $ck => $cv) { ?>
				<option value="<?php echo $cv['id']; ?>" <?php
				if(isset($mainOrder[0]['id_cust'])) {
					if($cv['id'] == $mainOrder[0]['id_cust']) echo "selected";
				} ?>><?php echo $cv['name_eksternal']; ?></option>
		<?php
			}
		?>
			</select>
	  </div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="term_of_pay">Term of Payment <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="term_of_pay" name="term_of_pay" style="width: 100%" required>
				<option value="">-- Select Term --</option>
				<option value="1 Day" <?php
					if(isset($mainOrder[0]['term_of_payment'])) if($mainOrder[0]['term_of_payment'] == '1 Day') echo "selected";
				?>>1 Day</option>
				<option value="15 Day" <?php
					if(isset($mainOrder[0]['term_of_payment'])) if($mainOrder[0]['term_of_payment'] == '15 Day') echo "selected";
				?>>15 Day</option>
				<option value="30 Day" <?php
					if(isset($mainOrder[0]['term_of_payment'])) if($mainOrder[0]['term_of_payment'] == '30 Day') echo "selected";
				?>> 30 Day</option>
				<option value="45 Day" <?php
					if(isset($mainOrder[0]['term_of_payment'])) if($mainOrder[0]['term_of_payment'] == '45 Day') echo "selected";
				?>> 45 Day</option>
				<option value="60 Day" <?php
					if(isset($mainOrder[0]['term_of_payment'])) if($mainOrder[0]['term_of_payment'] == '60 Day') echo "selected";
				?>> 60 Day</option>
				<option value="90 Day" <?php
					if(isset($mainOrder[0]['term_of_payment'])) if($mainOrder[0]['term_of_payment'] == '90 Day') echo "selected";
				?>> 90 Day</option>
				<option value="120 Day" <?php
					if(isset($mainOrder[0]['term_of_payment'])) if($mainOrder[0]['term_of_payment'] == '120 Day') echo "selected";
				?>> 120 Day</option>
				<option value="150 Day" <?php
					if(isset($mainOrder[0]['term_of_payment'])) if($mainOrder[0]['term_of_payment'] == '120 Day') echo "selected";
				?>> 150 Day</option>
				<option value="180 Day" <?php
					if(isset($mainOrder[0]['term_of_payment'])) if($mainOrder[0]['term_of_payment'] == '150 Day') echo "selected";
				?>> 180 Day</option>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="valas">Valas <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<select class="form-control" id="valas" name="valas" style="width: 100%" required>
				<option value="" selected>-- Pilih Valas --</option>
				<?php foreach($valas as $vd) { ?>
					<option value="<?php echo $vd['valas_id']; ?>" <?php
					if(isset($mainOrder[0]['id_valas'])) {
						if($vd['valas_id'] == $mainOrder[0]['id_valas']) echo "selected";
					}
					?>><?php echo $vd['nama_valas']; ?></option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="rate">Rate <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="number" class="form-control" id="rate" name="rate" placeholder="Rate" autocomplete="off" step=".0001" value="<?php
				if(isset($mainOrder[0]['rate'])) echo $mainOrder[0]['rate'];
			?>" required>
		</div>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="diskon">Diskon <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="number" class="form-control" id="diskon" name="diskon" placeholder="Diskon" autocomplete="off" step=".0001" value="<?php
				if(isset($mainOrder[0]['diskon'])) echo $mainOrder[0]['diskon'];
			?>" required>
		</div>
	</div>

	<div class="item form-group" style="display:none;">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="tujuan">Tujuan <span class="required"><sup>*</sup></span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="hidden" class="form-control" id="tujuan" name="tujuan" placeholder="Tujuan" value="<?php
				if(isset($mainOrder[0]['tujuan'])) echo $mainOrder[0]['tujuan'];
			?>" autocomplete="off">
		</div>
	</div>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="diskon">No Delivery</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<input type="text" class="form-control" id="no_delivery" name="no_delivery" placeholder="No Delivery" autocomplete="off" value="<?php
				if(isset($mainOrder[0]['no_delivery'])) echo $mainOrder[0]['no_delivery'];
			?>" >
		</div>
	</div>
	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12">Daftar Maintenance : </label>
		<div class="col-md-8 col-sm-6 col-xs-12"></div>
	</div>

	<div class="item form-group">
		<table id="listItemMaintenance" class="table table-striped table-bordered dt-responsive nowrap" width="100%">
			<thead>
				<tr>
					<th class="dt-body-center" style="width: 5%;">No</th>
					<th>Nama Material Limbah</th>
					<th >QTY</th>
					<th >Harga</th>
					<th >Amount</th>
					<th >Remark</th>
					<th ><a id="btn_item_add" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a></th>
				</tr>
			</thead>
			<tbody>
		<?php
			$i=1;
			if(sizeof($detail) > 0) {
				foreach($detail as $dk) { ?>
					<tr id="trRowItem<?php echo $i; ?>">
						<td></td>
						<td>
							<select class="form-control" id="id_material<?php echo $i; ?>" name="id_material[]">
								<option value="">-- Pilih Material Limbah --</option>
						<?php
							if($dk['id_material'] != '' && $dk['id_material'] != NULL) { ?>
								<option value="<?php echo $dk['id_material']; ?>" selected><?php echo $dk['stock_code'].' - '.$dk['stock_name'].' ('.$dk['uom_symbol'].')'; ?></option>
						<?php
							}

							foreach ($material as $mk => $mv) { ?>
								<option value="<?php echo $mv['id']; ?>"><?php echo $mv['stock_code'].' - '.$mv['stock_name'].' ('.$mv['uom_symbol'].')'; ?></option>
						<?php
							} ?>
							</select>
						</td>
						<td>
							<input type="number" class="form-control" id="qty<?php echo $i; ?>" name="qty[]" placeholder="Harga" value="<?php
								echo $dk['qty_limbah'];
							?>" autocomplete="off" step=".0001">
						</td>
						<td>
							<input type="number" class="form-control" id="dm_price<?php echo $i; ?>" name="dm_price[]" placeholder="Harga" value="<?php
								echo $dk['price'];
							?>" autocomplete="off" step=".0001">
						</td>
						<td class="dt-body-right">
						<?php
							$rowAmount = (floatval($dk['price'])) * (floatval($dk['qty_limbah']));
						?>
							<label id="rowAmount<?php echo $i; ?>"><?php echo number_format($rowAmount, 4, '.', ',') ?></label>
							<input type="hidden" id="amount<?php echo $i; ?>" name="amount[]" value="<?php echo number_format($rowAmount, 4, '.', ''); ?>">
						</td>
						<td>
							<textarea class="form-control" id="desc_main<?php echo $i; ?>" name="desc_main[]" placeholder="Deskripsi" rows="3" readonly><?php
								echo $dk['remark'];
							?></textarea>
						</td>
						<td>
							<input type="hidden" id="id_lo_dm<?php echo $i; ?>" name="id_lo_dm[]" value="<?php echo $dk['id_lo_dm']; ?>">
							<a id="btn_item_remove" class="btn btn-danger btn-sm" title="Hapus" onclick="delItem(this, '<?php
								echo $dk['id_lo_dm']; ?>')">
								<i class="fa fa-trash"></i>
							</a>
						</td>
					</tr>
		<?php
					$i++;
				}
			}else { ?>
				<tr>
					<td></td>
					<td>
						<select class="form-control" id="id_material<?php echo $i; ?>" name="id_material[]">
							<option value="" selected>-- Pilih Material Limbah --</option>
					<?php
						foreach ($material as $mk => $mv) { ?>
							<option value="<?php echo $mv['id_material']; ?>"><?php  echo $mv['stock_code'].' - '.$mv['stock_name'].' ('.$mv['uom_symbol'].')'; ?></option>
					<?php
						}
					?>
						</select>
					</td>
					<td>
						<input type="number" class="form-control" id="qty<?php echo $i; ?>" name="qty[]" placeholder="Qty" autocomplete="off" step=".0001" disabled>
					</td>
					<td>
						<input type="number" class="form-control" id="dm_price<?php echo $i; ?>" name="dm_price[]" placeholder="Harga" autocomplete="off" step=".0001" disabled>
					</td>
					<td class="dt-body-right">
						<label id="rowAmount<?php echo $i; ?>">0.0000</label>
						<input type="hidden" id="amount<?php echo $i; ?>" name="amount[]" value="0">
					</td>
					<td>
						<textarea class="form-control" id="desc_main<?php echo $i; ?>" name="desc_main[]" placeholder="Deskripsi" rows="3" disabled></textarea>
					</td>
					<td>
						<input type="hidden" id="id_lo_dm<?php echo $i; ?>" name="id_lo_dm[]" value="new">
					</td>
				</tr>
		<?php
			} ?>
			</tbody>
			<tfoot>
				<tr>
					<td class="dt-body-right" colspan="3" rowspan="3"></td>
					<td class="dt-body-right">Total</td>
					<td class="dt-body-right"><label id="totalAmountRow">0.0000</label></td>
					<td colspan="3" rowspan="3"></td>
				</tr>
				<tr>
					<td class="dt-body-right">Diskon</td>
					<td class="dt-body-right"><label id="totalAmountDiskon">0</label></td>
				</tr>
				<tr>
					<td class="dt-body-right">Total Amount</td>
					<td class="dt-body-right"><label id="totalAmount">0.0000</label></td>
				</tr>
			</tfoot>
		</table>
	</div>

	<div class="item form-group">
		<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Simpan Limbah Order</button>
			<input type="hidden" id="total_amount" name="total_amount" value="0">
			<input type="hidden" id="id_lo" name="id_lo" value="<?php if(isset($mainOrder[0]['id_lo'])) echo $mainOrder[0]['id_lo']; ?>">
		</div>
	</div>
</form>

<script type="text/javascript">
	var idRowItem 		= '<?php echo $i; ?>';
	var arrIdItem		= [];
	var arrNoDm 		= [];
	var arrPrice		= [];
	var arrRemark		= [];

	$(document).ready(function() {
		$("#delivery_date").datepicker({
			format: 'dd-M-yyyy',
			autoclose: true,
			todayHighlight: true,
		});

		$('#cust_id').select2();
		$('#term_of_pay').select2();
		$('#valas').select2();
		$('#diskon').on('keyup', function() {
			cal_amount();
		})
		$('#diskon').on('change', function() {
			cal_amount();
		})

		for (var i = 1; i <= idRowItem; i++) {
			$('#id_material'+i).select2();
				$('#id_material'+i).on('change', function() {
				set_itemValue(this.value, this.id);
				cal_amount();
			})
			$('#dm_price'+i).on('keyup', function() {
				cal_amount();
			})
			$('#dm_price'+i).on('change', function() {
				cal_amount();
			})
		}

		cal_amount();
	});

	$('#btn_item_add').on('click', function() {
		idRowItem++;
		$('#listItemMaintenance tbody').append(
			'<tr id="trRowItem'+idRowItem+'">'+
				'<td></td>'+
				'<td>'+
					'<select class="form-control" id="id_material'+idRowItem+'" name="id_material[]" disabled>'+
						'<option value="" selected>-- Pilih Maintenance --</option>'+
					'</select>'+
				'</td>'+
				'<td>'+
					'<input type="number" class="form-control" id="qty'+idRowItem+'" name="qty[]" placeholder="Qty" autocomplete="off" step=".0001" >'+
				'</td>'+
				'<td>'+
					'<input type="number" class="form-control" id="dm_price'+idRowItem+'" name="dm_price[]" placeholder="Harga" autocomplete="off" step=".0001" >'+
				'</td>'+
				'<td class="dt-body-right">'+
					'<label id="rowAmount'+idRowItem+'">0.0000</label>'+
					'<input type="hidden" id="amount'+idRowItem+'" name="amount[]" value="0">'+
				'</td>'+
				'<td>'+
					'<textarea class="form-control" id="desc_main'+idRowItem+'" name="desc_main[]" placeholder="Deskripsi" rows="3" readonly></textarea>'+
				'</td>'+
				'<td>'+
					'<input type="hidden" id="id_lo_dm'+idRowItem+'" name="id_lo_dm[]" value="new">'+
					'<a class="btn btn-danger btn-sm" onclick="removeRow('+idRowItem+')"><i class="fa fa-minus"></i></a>'+
				'</td>'+
			'</tr>'
		);
		$("#id_material"+idRowItem).select2();
		$('#id_material'+idRowItem).on('change', function() {
			set_itemValue(this.value, this.id);
			cal_amount();
		})
		$('#dm_price'+idRowItem).on('keyup', function() {
			cal_amount();
		})
		$('#dm_price'+idRowItem).on('change', function() {
			cal_amount();
		})

		$.ajax({
			type: "GET",
			url: "<?php echo base_url('limbah_order/get_material');?>",
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			success: function(r) {
				if(r.length > 0) {
					for (var i = 0; i < r.length; i++) {
            var newOption = new Option(r[i].stock_code+'-'+r[i].stock_name+' ('+r[i].uom_symbol+')', r[i].id, false, false);
						$('#id_material'+idRowItem).append(newOption);
						cal_amount();
					}
					$('#id_material'+idRowItem).removeAttr('disabled');
				}else $('#id_material'+idRowItem).removeAttr('disabled');
			}
		});
	})

	function set_itemValue(dm, rowSelect) {
		var row = rowSelect.replace(/[^0-9]+/g, "");
		if(dm != '' && dm != null) {
			$.ajax({
				type: "GET",
        url: "<?php echo base_url('limbah_order/get_material_desc'); ?>" + '/' + dm,
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(r) {
					if(r.length > 0) {
						// $('#nama_main'+row).html(r[0].nama_main);
						// $('#qty_main'+row).html(formatCurrencyComaNull(r[0].qty));
						// $('#uom_main'+row).html(r[0].stock_name);
						// $('#dm_price'+row).val(0).removeAttr('disabled');
						// $('#amount'+row).val(0);
						$('#desc_main'+row).val(r[0].stock_description).removeAttr('disabled');
					}
				}
			});
		}else {
			// $('#nama_main'+row).html('');
			// $('#qty_main'+row).html('');
			// $('#uom_main'+row).html('');
			// $('#dm_price'+row).val('').attr('disabled', 'disabled');
			// $('#amount'+row).val(0);
			$('#desc_main'+row).val('').attr('disabled', 'disabled');
		}
	}

	function removeRow(rowItem) {
		$('#trRowItem'+rowItem).remove();
		cal_amount();
	}

	function cal_amount() {
		var diskon 			= $('#diskon').val();
		var tempDiskon		= 0;
		var qtyRow			= 0;
		var price 			= 0;
		var totalRow		= 0;
		var tempTotal 		= 0;
		var totalAmount 	= 0;
		if(diskon == '' || diskon == undefined || diskon == null) diskon = 0;

		$('input[name="dm_price[]"]').each(function() {
			var idRow 		= this.id.replace(/[^0-9]+/g, "");

			var tempQty		= $('#qty'+idRow).val();
			if(tempQty != '' && tempQty != null && tempQty != undefined) qtyRow = parseFloat(tempQty);
			if(this.value != '' && this.value != null && this.value != undefined) price = parseFloat(this.value);

			totalRow = (price * qtyRow);
			if(this.value != '' && this.value != undefined && this.value != null) {
				$('#rowAmount'+idRow).html(formatCurrencyComa(totalRow));
				$('#amount'+idRow).val(totalRow);
			}else {
				$('#rowAmount'+idRow).html(formatCurrencyComa(0));
				$('#amount'+idRow).val(0);
			}
		})

		$('input[name="amount[]"]').each(function() {
			var amountRow = 0;
			if(this.value != '' && this.value != null && this.value != undefined) amountRow = parseFloat(this.value);
			tempTotal = (tempTotal + amountRow);
		});

		if(diskon > 100) {
			totalAmount = tempTotal - diskon;
			$('#totalAmountDiskon').html(formatCurrencyComa(parseFloat(diskon)));
		}else {
			var tempTotalvDiskon = 0;
			tempDiskon			= diskon / 100;
			tempTotalvDiskon	= tempTotal * tempDiskon;
			totalAmount 		= (tempTotal - tempTotalvDiskon);
			$('#totalAmountDiskon').html(diskon+'%');
		}

		$('#totalAmountRow').html(formatCurrencyComa(parseFloat(tempTotal)));		
		$('#totalAmount').html(formatCurrencyComa(parseFloat(totalAmount)));
		$('#total_amount').val(totalAmount);
	}

	function delItem(btnDel, idDetal) {
		var rowParent = $(btnDel).parents('tr');

		swal({
			title: 'Yakin akan Menghapus Detail yang sudah ada ?',
			text: 'data tidak dapat dikembalikan bila sudah dihapus !',
			type: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Ya',
			cancelButtonText: 'Tidak'
		}).then(function () {
			var datapost = {
				"id"		: parseInt(idDetal)
			};

			$.ajax({
				type: "POST",
				url: "<?php echo base_url();?>limbah_order/delete_detail_limbahOrder",
				data : JSON.stringify(datapost),
				dataType: 'json',
				contentType: 'application/json; charset=utf-8',
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function () {
							rowParent.remove();
							cal_amount();
						})
					}else swal("Failed!", response.message, "error");
				}
			});
		});
	}

	$('#edit_mainOrder').on('submit', (function(e) {
		cal_amount();
		arrIdItem		= [];
		arrNoDm 		= [];
		arrPrice		= [];
		arrRemark		= [];
		arrQty		= [];

		$('#btn-submit').attr('disabled','disabled');
		$('#btn-submit').text("Memasukkan data...");
		e.preventDefault();

		$('input[name="id_lo_dm[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '')  arrIdItem.push(this.value);
			}
		})

		$('select[name="id_material[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '')  arrNoDm.push(this.value);
			}
		})

		$('input[name="qty[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '')  arrQty.push(this.value);
			}
		})
		$('input[name="dm_price[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '')  arrPrice.push(this.value);
			}
		})

		$('textarea[name="desc_main[]"]').each(function() {
			if(this.value) {
				if(this.value != undefined && this.value != '') arrRemark.push(this.value);
				else arrRemark.push('NULL');
			}else arrRemark.push('NULL');
		})

		if(arrIdItem.length >0 && arrNoDm.length > 0 && arrPrice.length > 0 && arrRemark.length > 0 && arrQty.length > 0) {
			var formData = new FormData(this);
			// formData.set('id_lo',			$('#id_lo').val());
			// formData.set('delivery_date',	$('#delivery_date').val());
			// formData.set('cust_id',			$('#cust_id').val());
			// formData.set('term_of_pay',		$('#term_of_pay').val());
			// formData.set('valas',			$('#valas').val());
			// formData.set('rate',			$('#rate').val());
			// formData.set('diskon',			$('#diskon').val());
			// formData.set('tujuan',			$('#tujuan').val());
			// formData.set('total_amount',	$('#total_amount').val());
			// formData.set('arrIdItem',		arrIdItem);
			// formData.set('arrNoDm',			arrNoDm);
			// formData.set('arrPrice',		arrPrice);
			// formData.set('arrRemark',		arrRemark);

			$.ajax({
				type:'POST',
				url: $(this).attr('action'),
				data: formData,
				cache:false,
				contentType: false,
				processData: false,
				success: function(response) {
					if (response.success == true) {
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function () {
							$('#panel-modal').modal('toggle');
							listLimbahOrder();
						})
					}else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Simpan Maintenance Order");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Simpan Maintenance Order");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}else {
			$('#btn-submit').removeAttr('disabled');
			$('#btn-submit').text("Simpan Maintenance Order");
			swal("Failed!", "Invalid Inputan List Maintenance, silahkan cek kembali.", "error");
		}
	}));
</script>