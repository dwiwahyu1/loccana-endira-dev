<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Limbah_Order_model extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  }

  public function lists($params = array())
  {
    $sql   = 'CALL limbah_order_list(?, ?, ?, ?, ?, @total_filtered, @total)';

    $out = array();
    $query   =  $this->db->query(
      $sql,
      array(
        $params['limit'],
        $params['offset'],
        $params['order_column'],
        $params['order_dir'],
        $params['filter']
      )
    );

    $result = $query->result_array();

    $this->load->helper('db');
    free_result($this->db->conn_id);

    $total = $this->db->query('select @total_filtered, @total')->row_array();

    $return = array(
      'data' => $result,
      'total_filtered' => $total['@total_filtered'],
      'total' => $total['@total']
    );

    return $return;
  }

  public function customer()
  {
    $query =   $this->db->query("SELECT id,name_eksternal,kode_eksternal 
      FROM t_eksternal
      WHERE id = 195
      ");

    $result = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function valas()
  {
    $sql   = "SELECT * FROM m_valas";

    $query   = $this->db->query($sql);
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }

  public function get_material()
  {
    $sql   = 'SELECT *
    FROM m_material a
    LEFT JOIN m_uom b on a.unit = b.id_uom
    where id=77737';

    $query   = $this->db->query($sql);

    $result = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function get_material_desc($id)
  {
    $sql   = 'SELECT * 
    FROM m_material
    where id= ?';

    $query   = $this->db->query($sql, array(
      $id
    ));

    $result = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function get_limbahOrder($id)
  {
    $sql   = 'CALL limbah_order_search_id(?)';

    $query   = $this->db->query($sql, array(
      $id
    ));

    $result = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function add_limbahOrder($data)
  {
    $sql   = 'CALL limbah_order_add(?,?,?,?,?,?,?,?,?,?)';

    $query   =  $this->db->query($sql, array(
      $data['no'],
      $data['delivery_date'],
      $data['tujuan'],
      $data['cust_id'],
      $data['term_of_pay'],
      $data['diskon'],
      $data['total_amount'],
      $data['valas'],
      $data['rate'],
      $data['no_jalan']
    ));

    $query = $this->db->query('SELECT LAST_INSERT_ID()');
    $row = $query->row_array();
    $lastid = $row['LAST_INSERT_ID()'];

    $result['result'] = $this->db->affected_rows();
    $result['lastid'] = $lastid;

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function add_detail_limbah_order($data)
  {
    $sql   = 'CALL limbah_order_dm_add(?,?,?,?,?)';

    $query   =  $this->db->query($sql, array(
      $data['id_mo'],
      $data['id_dm'],
      $data['qty'],
      $data['unit_price'],
      $data["remark"]
    ));

    $result = $this->db->affected_rows();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function edit_limbahOrder($data)
  {
    $sql   = 'CALL limbah_order_update(?,?,?,?,?,?,?,?,?,?)';
    $query   =  $this->db->query($sql, array(
      $data['id_mo'],
      $data['delivery_date'],
      $data['tujuan'],
      $data['cust_id'],
      $data['term_of_pay'],
      $data['diskon'],
      $data['total_amount'],
      $data['valas'],
      $data['rate'],
      $data['no_delivery']
    ));

    $result = $this->db->affected_rows();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function edit_detail_limbahOrder($data)
  {
    $sql   = 'CALL limbah_order_dm_update(?,?,?,?,?)';

    $query   =  $this->db->query($sql, array(
      $data['id_mo_dm'],
      $data['id_dm'],
      $data['unit_price'],
      $data['qty'],
      $data["remark"]
    ));

    $result = $this->db->affected_rows();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function detail_pekerjaan($id)
  {
    $sql   = 'CALL permintaan_pekerjaan_detail_search_id_permintaan(?)';

    $query   = $this->db->query($sql, array(
      $id
    ));

    $result = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function get_detail_limbahOrder($id)
  {
    $sql   = 'CALL limbah_order_search_dm_id(?)';

    $query   = $this->db->query($sql, array(
      $id
    ));

    $result = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function get_detail_limbahOrder_id_dm($id)
  {
    $sql   = 'CALL limbah_order_dm_search_id_dm(?)';

    $query   = $this->db->query($sql, array(
      $id
    ));

    $result = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function status_limbahOrder($data)
  {
    $sql   =
      'UPDATE `t_limbah_order` SET
				`status` = ' . $data['status'] . ',
				`notes` = \'' . $data['notes'] . '\'
			WHERE `id_lo` = ' . $data['id_mo'];

    $query   = $this->db->query($sql);

    $result = $this->db->affected_rows();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  function delete_limbahOrder($data)
  {
    $sql   = 'CALL limbah_order_delete(?)';

    $query   = $this->db->query($sql, array(
      $data['id']
    ));

    $result = $this->db->affected_rows();

    $this->db->close();
    $this->db->initialize();

    return $result;
  }

  public function delete_detail_mainOrder($data)
  {
    $sql   = 'CALL maintenance_order_dm_delete(?)';

    $query   = $this->db->query($sql, array(
      $data['id']
    ));
    $return = $query->result_array();

    $this->db->close();
    $this->db->initialize();

    return $return;
  }
  public function add_mutasi_limbah($data)
  {
    // $this->save_edit_material($data);
    $sql   = 'CALL mutasi_add(?,?,?,?,?);';
    $query   =  $this->db->query(
      $sql,
      array(
        $data['id_detail'],
        $data['id_detail'],
        $data['date'],
        $data['qty'],
        0
      )
    );
    $result  = $this->db->affected_rows();
    $sql   = 'CALL mutasi_add(?,?,?,?,?);';
    $query   =  $this->db->query(
      $sql,
      array(
        $data['id_detail'],
        $data['id_detail'],
        $data['date'],
        $data['qty'],
        1
      )
    );
    $result  = $this->db->affected_rows();
    return $result;
  }
  public function add_coa_values($data)
	{
		$sql 	= 'CALL coavalue_add(?,?,?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['id_coa'],
			$data['id_parent'],
			$data['id_valas'],
			$data['value'],
			$data['adjusment'],
			$data['type_cash'],
			$data['note'],
			$data['rate'],
			''
		));
		
		$return['result'] = $query->result_array();
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid= $row['LAST_INSERT_ID()'];
		
		$return['lastid'] = $lastid;

		$this->db->close();
		$this->db->initialize();

		return $return;
  }
  public function add_kartu_hp($data)
	{
		$sql 	= 'CALL hp_add(?,?,?,?,?,?,?,?,?,?)';

		$query 	= $this->db->query($sql,array(
			$data['ref'],
			$data['source'],
			$data['keterangan'],
			$data['status'],
			$data['saldo'],
			$data['saldo_akhir'],
			$data['id_valas'],
			$data['type_kartu'],
			$data['id_master'],
			$data['type_master']
		));
		
		$return['code'] = $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
}
