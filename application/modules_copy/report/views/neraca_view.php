<style type="text/css">
	@media print {
		body {
			page-break-before: avoid;
			width:100%;
			height:100%;
			-webkit-transform: rotate(-90deg) scale(.68,.68); 
			-moz-transform:rotate(-90deg) scale(.58,.58);
			zoom: 200%;
		}
	}

	/*table, th, td {
		border: 1px solid black;
	}*/

	.bodyReport {
		padding: 5px 20px;
	}

	.reportBox {
		border: 1px solid black;
		page-break-before: always;
	}

	.reportTitle {
		margin-top: 20px;
		page-break-before: always;
	}

	.reportBodyBox {
		border-top: 0;
		border-left: 1px;
		border-right: 1px;
		border-bottom: 0;
		border-style: solid;
		border-color: black;
		page-break-before: always;
	}

	.titleReport {
		padding: 0;
		margin: 20px 0;
	}

	.separatorReport {
		padding: 0;
		margin: 10px 0;
	}

	.subHrTitle {
		border-top: 1px;
		border-left: 0;
		border-right: 0;
		border-bottom: 1px;
		border-style: solid;
	}

	.tableReport {
		box-sizing: border-box;
		text-align: left;
		width: 90%;
		margin: 10px auto;
		page-break-before: always;
	}

	.tableReport th, td {
		padding: 5px 0;
	}

	.tableCellSumTOP {
		border-top: 1px;
		border-left: 0;
		border-right: 0;
		border-bottom: 0;
		border-style: solid;
		border-color: black;
	}

	.tableCellSumBottom {
		border-top: 0;
		border-left: 0;
		border-right: 0;
		border-bottom: 1px;
		border-style: solid;
		border-color: black;
	}

	.tableCellSumTopBottom {
		border-top: 1px;
		border-left: 0;
		border-right: 0;
		border-bottom: 1px;
		border-style: solid;
		border-color: black;
	}

	.labelTitle {
		margin-left: -20px;
		margin-bottom: 0 !important;
		padding: 0 !important;
	}

	.labelTitleNoMargin {
		margin-bottom: 0 !important;
		padding: 0 !important;
	}
</style>

<div class="card-box">
	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" id="btn_download">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>

	<div class="bodyReport">
		<div class="row">
			<div class="col-md-12 text-center titleReport">
				<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
				<h3 id="titleJenisReport">Neraca</h3>
				<h3 id="titleTanggal">Per, <?php if(isset($dateSearch)) echo $dateSearch; else echo date('d-M-Y'); ?></h3>
			</div>

			<div id="divBodyReport">
				<div class="col-md-12 text-center reportBox">
					<h4 id="titleAktiva">AKTIVA</h4>
				</div>
				<div class="col-md-12 text-center reportBodyBox">
					<table class="tableReport" id="t_aktiva_lancar">
						<thead>
							<tr>
								<td colspan="3" align="left">
									<label class="labelTitle">AKTIVA LANCAR</label>
								</td>
							</tr>
						</thead>
						<tbody>
						<?php
						$totalAktivaLancar = 0;
						foreach($aktiva_lancar as $al) {
							$totalAktivaLancar = $totalAktivaLancar + $al['total'];
						?>
							<tr>
								<td><?php echo trim($al['name_field']); ?></td>
								<td style="width: 20%;" align="right">
									Rp. <?php echo number_format(trim($al['total']),2,",","."); ?></td>
								<td></td>
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td>
									<label class="labelTitle">TOTAL AKTIVA LANCAR</label>
								</td>
								<td class="tableCellSumTOP"></td>
								<td style="width: 20%;" align="right">
									<label class="labelTitleNoMargin">Rp. <?php echo number_format(trim($totalAktivaLancar),2,",","."); ?></label></td>
							</tr>
						</tfoot>
					</table>

					<table class="tableReport" id="t_aktiva_tetap">
						<thead>
							<tr>
								<td colspan="3" align="left">
									<label class="labelTitle">AKTIVA TETAP</label>
								</td>
							</tr>
						</thead>
						<tbody>
						<?php
						$totalAktivaTetap = 0;
						foreach($aktiva_tetap as $at) {
							$totalAktivaTetap = $totalAktivaTetap + $at['total'];
						?>
							<tr>
								<td><?php echo trim($at['name_field']); ?></td>
								<td style="width: 20%" align="right">
									Rp. <?php echo number_format(trim($at['total']),2,",","."); ?></td>
								<td></td>
							</tr>
						<?php } ?>
							<tr>
								<td></td>
								<td style="width: 20%;" class="tableCellSumTOP" align="right">
									<label class="labelTitleNoMargin">Rp. <?php echo number_format(trim($totalAktivaTetap),2,",","."); ?></label></td>
								<td></td>
							</tr>
						</tbody>
						<tfoot>
						<?php
						$totalPenyusutan = 0;
						foreach($penyusutan as $kp) {
							$totalPenyusutan = $totalPenyusutan + $kp['total'];
						}
						$totalAktivaTetapPenyusutan = $totalAktivaTetap - $totalPenyusutan;
						?>
							<tr>
								<td>AKUMULASI PENYUSUTAN</td>
								<td style="width: 20%;" align="right">
									Rp. <?php echo number_format(trim($totalPenyusutan),2,",","."); ?></td>
								<td></td>
							</tr>
							<tr>
								<td>
									<label class="labelTitle">TOTAL AKTIVA TETAP</label>
								</td>
								<td class="tableCellSumTOP"></td>
								<td style="width: 20%;" align="right">
									<label class="labelTitleNoMargin">Rp. <?php echo number_format(trim($totalAktivaTetapPenyusutan),2,",","."); ?></label></td>
							</tr>
						</tfoot>
					</table>
				</div>

				<div class="col-sm-12 text-center reportBox">
					<table class="tableReport" id="t_total_aktiva">
						<?php
							$totalAktiva = $totalAktivaLancar + $totalAktivaTetapPenyusutan;
						?>
						<tr>
							<td style="width: 80%;" align="center">
								<label>TOTAL AKTIVA</label>
							</td>
							<td style="width: 20%;" align="right">
								<label class="labelTitleNoMargin">Rp. <?php echo number_format(trim($totalAktiva),2,",","."); ?></label></td>
						</tr>
					</table>
				</div>

				<div class="col-md-12 text-center reportBox reportTitle">
					<h4 id="titlePasiva">PASIVA</h4>
				</div>
				<div class="col-md-12 text-center reportBodyBox">
					<table class="tableReport" id="t_hutang">
						<thead>
							<tr>
								<td colspan="3">
									<label class="labelTitle">HUTANG</label>
								</td>
							</tr>
						<?php
						$totalHutangDagang = 0;
						foreach($pasivaHutangDagang as $phd) {
							$totalHutangDagang = $totalHutangDagang + $phd['total'];
						}?>
							<tr>
								<td>
									<label class="labelTitleNoMargin">HUTANG DAGANG</label>
								</td>
								<td align="right">Rp. <?php
									if($totalHutangDagang < 0) $totalHutangDagang = $totalHutangDagang * -1;
									echo number_format(trim($totalHutangDagang),2,",",".");
								?></td>
								<td></td>
							</tr>
						</thead>
						<tbody>
						<?php
						$totalHutangLain = 0;
						foreach($pasivaHutangLain as $phl) {
							$totalHutangLain = $totalHutangLain + $phl['total'];
						?>
							<tr>
								<td><?php echo trim($phl['name_field']); ?></td>
								<td style="width: 20%;" align="right">Rp. <?php
									if($phl['total'] < 0) $phl['total'] = $phl['total'] * -1;
									echo number_format(trim($phl['total']),2,",",".");
								?></td>
								<td></td>
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td>
									<label class="labelTitle">TOTAL HUTANG LAIN - LAIN</label>
								</td>
								<td class="tableCellSumTOP" align="right">
									<label class="labelTitleNoMargin">Rp. <?php
										if($totalHutangLain < 0) $totalHutangLain = $totalHutangLain * -1;
										echo number_format(trim($totalHutangLain),2,",",".");
									?></label></td>
								<td></td>
							</tr>

						<?php
							$totalHutang = $totalHutangDagang + $totalHutangLain;
						?>
							<tr>
								<td>
									<label class="labelTitle">TOTAL HUTANG</label>
								</td>
								<td></td>
								<td style="width: 20%" align="right">
									<label>Rp. <?php
										if($totalHutang < 0) $totalHutang = $totalHutang * -1;
										echo number_format(trim($totalHutang),2,",",".");
									?></label></td>
							</tr>
						</tfoot>
					</table>

					<table class="tableReport" id="t_modal">
						<thead>
							<tr>
								<td colspan="3">
									<label class="labelTitle">MODAL</label>
								</td>
							</tr>
						</thead>
						<tbody>
						<?php
						$totalModal = 0;
						foreach($pasivaModal as $pm) {
							
							
							if( $pm['name_field'] == 'LABA (RUGI) TAHUN BERJALAN' ){
								if(isset($laba_berjalan)){ 
									$rlw = $laba_berjalan;
								}else {
									$rlw = 0;
								}
								
								$totalModal = $totalModal + $rlw;
						?>
							<tr>
								<td><?php echo $pm['name_field']; ?></td>
								<td style="width: 20%;" align="right">Rp. <?php
									if($laba_berjalan < 0) $laba_berjalan = $laba_berjalan * -1;
									echo number_format($laba_berjalan,2,",",".");
								?></td>
								<td></td>
							</tr>
						<?php 
							}else {
								$totalModal = $totalModal + $pm['total'];
						?>
								
								
								
								<tr>
								<td><?php echo $pm['name_field']; ?></td>
								<td style="width: 20%;" align="right">Rp. <?php
									if($pm['total'] < 0) $pm['total'] = $pm['total'] * -1;
									echo number_format($pm['total'], 2, ",", ".");
								?></td>
								<td></td>
							</tr>
								
						<?php	}
						} ?>
						</tbody>
						<tfoot>
							<tr>
								<td>
									<label class="labelTitle">TOTAL MODAL</label>
								</td>
								<td class="tableCellSumTOP"></td>
								<td style="width: 20%;" align="right">
									<label class="labelTitleNoMargin">Rp. <?php
										if($totalModal < 0) $totalModal = $totalModal * -1;
										echo number_format($totalModal,2,",",".");
									?></label></td>
							</tr>
						</tfoot>
					</table>
				</div>
				<div class="col-md-12 text-center reportBox">
					<table class="tableReport" id="t_total_pasiva">
						<?php
						$totalPasiva = $totalHutang + $totalModal;
					?>
						<tr>
							<td align="center">
								<label>TOTAL PASIVA</label>
							</td>
							<td style="width: 20%;" align="right">
								<label class="labelTitleNoMargin">Rp. <?php
									if($totalPasiva < 0) $totalPasiva = $totalPasiva * -1;
									echo number_format($totalPasiva,2,",",".");
								?></label></td>
						</tr>
					</table>
				</div>
			</div>

		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){

		$('#btn_download').click(function () {
			var doc = new jsPDF();
			var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
			var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
			
			// FOOTER
			doc.setTextColor(100);
			doc.setFontSize(16);
			doc.text($('#titlePerusahaan').html(), pageWidth / 2, 20, 'center');
			doc.setFontSize(12);
			doc.text($('#titleJenisReport').html(), pageWidth / 2, 30, 'center');
			doc.setFontSize(14);
			doc.text($('#titleTanggal').html(), pageWidth / 2, 40, 'center');

			doc.setFontSize(12);
			doc.text($('#titleAktiva').html(), pageWidth / 2, 55, 'center');
			doc.autoTable({
				html 			: '#t_aktiva_lancar',
				theme			: 'plain',
				styles			: {fontSize : 10, cellWidth: 50},
				margin 			: 20,
				tableWidth		: (pageWidth - 40),
				headStyles		: {halign : 'left'},
				didParseCell	: function (data) {
					if(data.table.foot[0]) {
						if(data.table.foot[0].cells[2]) {
							data.table.foot[0].cells[2].styles.halign = 'right';
						}
					}
				},
				columnStyles	: {
					0: {halign: 'left'},
					1: {halign: 'right'},
					2: {halign: 'right'}
				},
				rowPageBreak	: 'auto',
				showHead 		: 'firstPage',
				showFoot		: 'lastPage',
				startY			: 60
			});
			doc.autoTable({
				html 			: '#t_aktiva_tetap',
				theme			: 'plain',
				styles			: {fontSize : 10, cellWidth: 50},
				margin 			: 20,
				tableWidth		: (pageWidth - 40),
				headStyles		: {halign : 'left'},
				didParseCell	: function (data) {
					if(data.table.foot.length > 0) {
						for (var i = 0; i < data.table.foot.length; i++) {
							var objSize = objectSize(data.table.foot[i].cells);
							if(objSize > 0) {
								if(data.table.foot[i].cells[1]) data.table.foot[i].cells[1].styles.halign = 'right';
								if(data.table.foot[i].cells[2]) data.table.foot[i].cells[2].styles.halign = 'right';
							}
						}
					}
				},
				columnStyles	: {1: {halign: 'right'}, 2: {halign: 'right'}},
				rowPageBreak	: 'auto',
				showHead 		: 'firstPage',
				showFoot		: 'lastPage',
				startY			: doc.autoTable.previous.finalY + 3
			});
			doc.autoTable({
				html 		: '#t_total_aktiva',
				theme			: 'plain',
				styles			: {fontSize : 10, cellWidth: 50},
				margin 			: 20,
				tableWidth		: (pageWidth - 40),
				columnStyles	: {1: {halign: 'right'}, 2: {halign: 'right'}},
				rowPageBreak	: 'auto',
				showHead 		: 'firstPage',
				showFoot		: 'lastPage',
				startY			: doc.autoTable.previous.finalY + 3
			});

			doc.text($('#titlePasiva').html(), pageWidth / 2, doc.autoTable.previous.finalY + 10, 'center');
			doc.autoTable({
				html 			: '#t_hutang',
				theme			: 'plain',
				styles			: {fontSize : 10, cellWidth: 50},
				margin 			: 20,
				tableWidth		: (pageWidth - 40),
				headStyles		: {halign : 'left'},
				didParseCell	: function (data) {
					if(data.table.head.length > 0) {
						for (var i = 0; i < data.table.head.length; i++) {
							var objSize = objectSize(data.table.head[i].cells);
							if(objSize > 0) {
								if(data.table.head[i].cells[1]) data.table.head[i].cells[1].styles.halign = 'right';
								if(data.table.head[i].cells[2]) data.table.head[i].cells[2].styles.halign = 'right';
							}
						}
					}

					if(data.table.foot.length > 0) {
						for (var i = 0; i < data.table.foot.length; i++) {
							var objSize = objectSize(data.table.foot[i].cells);
							if(objSize > 0) {
								if(data.table.foot[i].cells[1]) data.table.foot[i].cells[1].styles.halign = 'right';
								if(data.table.foot[i].cells[2]) data.table.foot[i].cells[2].styles.halign = 'right';
							}
						}
					}
				},
				columnStyles	: {1: {halign: 'right'}, 2: {halign: 'right'}},
				rowPageBreak	: 'auto',
				showHead 		: 'firstPage',
				showFoot		: 'lastPage',
				startY			: doc.autoTable.previous.finalY + 15
			});
			doc.autoTable({
				html 			: '#t_modal',
				theme			: 'plain',
				styles			: {fontSize : 10, cellWidth: 50},
				margin 			: 20,
				tableWidth		: (pageWidth - 40),
				headStyles		: {halign : 'left'},
				didParseCell	: function (data) {
					if(data.table.foot.length > 0) {
						for (var i = 0; i < data.table.foot.length; i++) {
							var objSize = objectSize(data.table.foot[i].cells);
							if(objSize > 0) {
								if(data.table.foot[i].cells[1]) data.table.foot[i].cells[1].styles.halign = 'right';
								if(data.table.foot[i].cells[2]) data.table.foot[i].cells[2].styles.halign = 'right';
							}
						}
					}
				},
				columnStyles	: {1: {halign: 'right'}, 2: {halign: 'right'}},
				rowPageBreak	: 'auto',
				showHead 		: 'firstPage',
				showFoot		: 'lastPage'
			});
			doc.autoTable({
				html 			: '#t_total_pasiva',
				theme			: 'plain',
				styles			: {fontSize : 10, cellWidth: 50},
				margin 			: 20,
				tableWidth		: (pageWidth - 40),
				headStyles		: {halign : 'left'},
				didParseCell	: function (data) {
					if(data.table.foot.length > 0) {
						for (var i = 0; i < data.table.foot.length; i++) {
							var objSize = objectSize(data.table.foot[i].cells);
							if(objSize > 0) {
								if(data.table.foot[i].cells[1]) data.table.foot[i].cells[1].styles.halign = 'right';
								if(data.table.foot[i].cells[2]) data.table.foot[i].cells[2].styles.halign = 'right';
							}
						}
					}
				},
				columnStyles	: {1: {halign: 'right'}, 2: {halign: 'right'}},
				rowPageBreak	: 'auto',
				showHead 		: 'firstPage',
				showFoot		: 'lastPage'
			});
			doc.save('Report_Neraca.pdf');
		});
	});

	function objectSize(obj) {
		var size = 0;
		for (key in obj) {
			if (obj.hasOwnProperty(key)) size++;
		}
		return size;
	}
</script>