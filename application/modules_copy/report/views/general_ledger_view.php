<style type="text/css">
	@media print {
		body {
			page-break-before: avoid;
			width:100%;
			height:100%;
			-webkit-transform: rotate(-90deg) scale(.68,.68); 
			-moz-transform:rotate(-90deg) scale(.58,.58);
			zoom: 200%;
		}
	}

	/*table, th, td {
		border: 1px solid black;
	}*/

	.bodyReport {
		padding: 5px 20px;
	}

	.reportBox {
		border: 1px solid black;
		page-break-before: always;
	}

	.reportTitle {
		margin-top: 20px;
		page-break-before: always;
	}

	.reportBodyBox {
		border-top: 0;
		border-left: 1px;
		border-right: 1px;
		border-bottom: 0;
		border-style: solid;
		border-color: black;
		page-break-before: always;
	}

	.reportBodyBoxNoTittle {
		margin-top: 20px;
		border-top: 1px;
		border-left: 1px;
		border-right: 1px;
		border-bottom: 1px;
		border-style: solid;
		border-color: black;
		page-break-before: always;
	}

	.titleReport {
		padding: 0;
		margin: 20px 0;
	}

	.tableReport {
		box-sizing: border-box;
		width: 100%;
		margin: 10px auto;
		page-break-before: always;
		font-size: 12px;
	}

	.tableReport th, td {
		padding: 5px 0;
	}

	.tableCellSumTOP {
		border-top: 1px;
		border-left: 0;
		border-right: 0;
		border-bottom: 0;
		border-style: solid;
		border-color: black;
	}

	.tableCellSumBottom {
		border-top: 0;
		border-left: 0;
		border-right: 0;
		border-bottom: 1px;
		border-style: solid;
		border-color: black;
	}

	.tableCellSumTopBottom {
		border-top: 1px;
		border-left: 0;
		border-right: 0;
		border-bottom: 1px;
		border-style: solid;
		border-color: black;
	}

	.labelTitle {
		margin-left: -20px;
		margin-bottom: 0 !important;
		padding: 0 !important;
	}

	.labelTitleNoMargin {
		margin-bottom: 0 !important;
		padding: 0 !important;
	}
</style>

<div class="card-box">
	<div class="row">
		<div class="col-md-12">
			<div class="pull-right" id="divButton">
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" id="btn_download">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>

	<div class="bodyReport">
		<div class="row">
			<div class="col-md-12 text-left titleReport">
			<?php
				$no_coa		= '';
				$nama_coa	= '';
				if(isset($dataCoa[0])) {
					$no_coa		= $dataCoa[0]['coa'];
					$nama_coa	= $dataCoa[0]['keterangan'];
				}
			?>
				<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
				<h3 id="titleNomor">NOMOR PERKIRAAN : <?php echo $no_coa; ?></h3>
				<h3 id="titleNama">NAMA PERKIRAAN : <?php echo $nama_coa; ?></h3>
				<!-- <h3 id="titleTanggal">Per, <?php if(isset($dateSearch)) echo $dateSearch; else echo date('d-M-Y'); ?></h3> -->
			</div>

			<div id="divBodyReport">
				<table class="table table-bordered" id="t_ledger">
					<thead>
						<tr>
							<th style="width: 8%; text-align: center; vertical-align: middle;" rowspan="2">TANGGAL</th>
							<th style="width: 10%; text-align: center; vertical-align: middle;" rowspan="2">NO. PERK LAWAN</th>
							<th style="width: 10%; text-align: center; vertical-align: middle;" rowspan="2">NOMOR INVOICE</th>
							<th style="text-align: center; vertical-align: middle;" rowspan="2">KETERANGAN</th>
							<th style="text-align: center; vertical-align: middle;" colspan="3">USD</th>
							<th style="text-align: center; vertical-align: middle;" rowspan="2">Kurs</th>
							<th style="text-align: center; vertical-align: middle;" colspan="3">IDR</th>
						</tr>
						<tr>
							<th style="text-align: center; vertical-align: middle;">DEBIT</th>
							<th style="text-align: center; vertical-align: middle;">KREDIT</th>
							<th style="text-align: center; vertical-align: middle;">SALDO</th>
							<th style="text-align: center; vertical-align: middle;">DEBIT</th>
							<th style="text-align: center; vertical-align: middle;">KREDIT</th>
							<th style="text-align: center; vertical-align: middle;">SALDO</th>
						</tr>
					</thead>
					<tbody>
				<?php
					$saldoDollarVGL = 0;
					$saldoRpVGL		= 0;
					foreach ($saldoAwal as $key => $vSA) {
						$saldoAwalDollar	= $vSA['debitdollar'] - $vSA['kreditdollar'];
						$saldoAwalRp		= $vSA['debitrp'] - $vSA['kreditrp'];
				?>
						<tr>
							<td><?php echo $vSA['date']; ?></td>
							<td><?php echo $vSA['coa']; ?></td>
							<td><?php echo $vSA['no_bukti']; ?></td>
							<td><?php echo $vSA['note']; ?></td>
							<td style="text-align: right;"><?php echo '$ '.number_format($vSA['debitdollar'],2,",","."); ?></td>
							<td style="text-align: right;"><?php echo '$ '.number_format($vSA['kreditdollar'],2,",","."); ?></td>
							<td style="text-align: right;"><?php echo '$ '.number_format($saldoAwalDollar,2,",","."); ?></td>
							<td style="text-align: right;"><?php echo number_format($vSA['kurs'],2,",","."); ?></td>
							<td style="text-align: right;"><?php echo 'Rp. '.number_format($vSA['debitrp'],2,",","."); ?></td>
							<td style="text-align: right;"><?php echo 'Rp. '.number_format($vSA['kreditrp'],2,",","."); ?></td>
							<td style="text-align: right;"><?php echo 'Rp. '.number_format($saldoAwalRp,2,",","."); ?></td>
						</tr>
				<?php
					}
					foreach ($dataGeneralLedger as $key => $vGL) {
						if($vGL['debitdollar'] > 0) $saldoAwalDollar = $saldoAwalDollar + $vGL['debitdollar'];
						else if($vGL['kreditdollar'] > 0) $saldoAwalDollar = $saldoAwalDollar - $vGL['kreditdollar'];
						
						if($vGL['debitrp'] > 0) $saldoAwalRp = $saldoAwalRp + $vGL['debitrp'];
						else if($vGL['kreditrp'] > 0) $saldoAwalRp = $saldoAwalRp - $vGL['kreditrp'];
				?>
						<tr>
							<td><?php echo $vGL['date']; ?></td>
							<td><?php echo $vGL['coa']; ?></td>
							<td><?php echo $vGL['no_invoice']; ?></td>
							<td><?php echo $vGL['note']; ?></td>
							<td style="text-align: right;"><?php echo '$ '.number_format($vGL['debitdollar'],2,",","."); ?></td>
							<td style="text-align: right;"><?php echo '$ '.number_format($vGL['kreditdollar'],2,",","."); ?></td>
							<td style="text-align: right;"><?php echo '$ '.number_format($saldoAwalDollar,2,",","."); ?></td>
							<td style="text-align: right;"><?php echo number_format($vGL['kurs'],2,",","."); ?></td>
							<td style="text-align: right;"><?php echo 'Rp. '.number_format($vGL['debitrp'],2,",","."); ?></td>
							<td style="text-align: right;"><?php echo 'Rp. '.number_format($vGL['kreditrp'],2,",","."); ?></td>
							<td style="text-align: right;"><?php echo 'Rp. '.number_format($saldoAwalRp,2,",","."); ?></td>
						</tr>
				<?php
					}?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var dataCoa;
	$(document).ready(function(){
		dataCoa = $('#t_ledger').dataTable({
			searching: false,
			aLengthMenu: [[10,25, 50, 75, -1], [10,25, 50, 75, "All"]],
			columnDefs: [
				{"orderable": false, "targets": 4},
				{"orderable": false, "targets": 5},
				{"orderable": false, "targets": 6},
				{"orderable": false, "targets": 7},
				{"orderable": false, "targets": 8},
				{"orderable": false, "targets": 9},
				{"orderable": false, "targets": 10}
			],
			buttons: [
				'pdf'
			]
		});

		$('#btn_download').click(function () {
			var doc = new jsPDF('l');
			var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
			var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
			
			doc.setTextColor(100);
			doc.setFontSize(16);
			doc.text($('#titlePerusahaan').html(), 10, 20, 'left');
			doc.setFontSize(12);
			doc.text($('#titleNomor').html(), 10, 30, 'left');
			doc.setFontSize(12);
			doc.text($('#titleNama').html(), 10, 40, 'left');

			doc.autoTable({
				body 			: dataCoa.fnGetData(),
				head 			: [[
					{content: 'TANGGAL', rowSpan: 2, styles: {halign: 'center', valign: 'middle'}},
					{content: 'NO. PERK LAWAN', rowSpan: 2, styles: {halign: 'center', valign: 'middle'}},
					{content: 'NOMOR BUKTI', rowSpan: 2, styles: {halign: 'center', valign: 'middle'}},
					{content: 'CUSTOMER', rowSpan: 2, styles: {halign: 'center', valign: 'middle'}},
					{content: 'USD', colSpan: 3, styles: {halign: 'center', valign: 'middle'}},
					{content: 'Kurs', rowSpan: 2, styles: {halign: 'center', valign: 'middle'}},
					{content: 'IDR', colSpan: 3, styles: {halign: 'center', valign: 'middle'}}
				],[
					{content: 'DEBIT', styles: {halign: 'center', valign: 'middle'}},
					{content: 'KREDIT', styles: {halign: 'center', valign: 'middle'}},
					{content: 'SALDO', styles: {halign: 'center', valign: 'middle'}},
					{content: 'DEBIT', styles: {halign: 'center', valign: 'middle'}},
					{content: 'KREDIT', styles: {halign: 'center', valign: 'middle'}},
					{content: 'SALDO', styles: {halign: 'center', valign: 'middle'}}
				]],
				theme			: 'plain',
				styles			: {fontSize : 7},
				margin 			: 10,
				tableWidth		: (pageWidth - 20),
				headStyles		: {halign : 'center', valign: 'middle', lineWidth: number = 0.5},
				bodyStyles		: {lineWidth: number = 0.5},
				columnStyles	: {
					4: {halign: 'right'},
					5: {halign: 'right'},
					6: {halign: 'right'},
					7: {halign: 'right'},
					8: {halign: 'right'},
					9: {halign: 'right'},
					10: {halign: 'right'}
				},
				rowPageBreak	: 'auto',
				showHead 		: 'firstPage',
				showFoot		: 'lastPage',
				startY			: 50
			});

			doc.save('General Ledger.pdf');
		});
	});

	function objectSize(obj) {
		var size = 0;
		for (key in obj) {
			if (obj.hasOwnProperty(key)) size++;
		}
		return size;
	}
</script>