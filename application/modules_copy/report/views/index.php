<div class="container" style="height: 500px;">
	<div class="row">
		<div class="col-sm-12">
			<h4 class="page-title" id="title_menu">Report <a class="btn btn-primary" onclick="report_detail()"><i class="fa fa-search"></i></a></h4>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div id="div_report"></div>
			<!-- <div class="card-box">
				<div class="row">
					<div class="col-md-12">
						<div class="btn-group pull-left">
							<a class="btn btn-primary" onclick="report_detail()"><i class="fa fa-search"></i> Report</a>
						</div>
					</div>
				</div>
			</div> -->
		</div>
	</div>
</div>

<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content p-0 b-0">
			<div class="panel panel-color panel-primary panel-filled">
				<div class="panel-heading">
					<button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
					<h3 class="panel-title"></h3>
				</div>
				<div class="panel-body">
					<p></p>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	// $(document).ready(function(){

	// });

	function report_detail(){
		$('#panel-modal').removeData('bs.modal');
		$('#panel-modal .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
		$('#panel-modal .panel-body').load('<?php echo base_url('report/report_detail');?>');
		$('#panel-modal .panel-title').html('<i class="fa fa-building-o"></i> Detail Report');
		$('#panel-modal').modal({backdrop:'static',keyboard:false},'show');
	}
</script>