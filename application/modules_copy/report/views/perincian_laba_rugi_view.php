<style type="text/css">
	@media print {
		body {
			page-break-before: avoid;
			width:100%;
			height:100%;
			-webkit-transform: rotate(-90deg) scale(.68,.68); 
			-moz-transform:rotate(-90deg) scale(.58,.58);
			zoom: 200%;
		}
	}

	/*table, th, td {
		border: 1px solid black;
	}*/

	.bodyReport {
		padding: 5px 20px;
	}

	.reportBox {
		border: 1px solid black;
		page-break-before: always;
	}

	.reportTitle {
		margin-top: 20px;
		page-break-before: always;
	}

	.reportBodyBox {
		border-top: 0;
		border-left: 1px;
		border-right: 1px;
		border-bottom: 0;
		border-style: solid;
		border-color: black;
		page-break-before: always;
	}

	.reportBodyBoxNoTittle {
		margin-top: 20px;
		border-top: 1px;
		border-left: 1px;
		border-right: 1px;
		border-bottom: 1px;
		border-style: solid;
		border-color: black;
		page-break-before: always;
	}

	.titleReport {
		padding: 0;
		margin: 20px 0;
	}

	.separatorReport {
		padding: 0;
		margin: 10px 0;
	}

	.subHrTitle {
		border-top: 1px;
		border-left: 0;
		border-right: 0;
		border-bottom: 1px;
		border-style: solid;
	}

	.tableReport {
		box-sizing: border-box;
		text-align: left;
		width: 90%;
		margin: 10px auto;
		page-break-before: always;
	}

	.tableReport th, td {
		padding: 5px 0;
	}

	.tableCellSumTOP {
		border-top: 1px;
		border-left: 0;
		border-right: 0;
		border-bottom: 0;
		border-style: solid;
		border-color: black;
	}

	.tableCellSumBottom {
		border-top: 0;
		border-left: 0;
		border-right: 0;
		border-bottom: 1px;
		border-style: solid;
		border-color: black;
	}

	.tableCellSumTopBottom {
		border-top: 1px;
		border-left: 0;
		border-right: 0;
		border-bottom: 1px;
		border-style: solid;
		border-color: black;
	}

	.labelTitle {
		margin-left: -20px;
		margin-bottom: 0 !important;
		padding: 0 !important;
	}

	.labelTitleNoMargin {
		margin-bottom: 0 !important;
		padding: 0 !important;
	}
</style>

<div class="card-box">
	<div class="row">
		<div class="col-md-12">
			<div class="pull-right">
				<a class="btn btn-icon waves-effect waves-light btn-primary m-b-5" id="btn_download">
					<i class="fa fa-download"></i>
				</a>
			</div>
		</div>
	</div>

	<div class="bodyReport">
		<div class="row">
			<div class="col-md-12 text-center titleReport">
				<h2 id="titlePerusahaan">PT. CELEBIT CIRCUIT TECHNOLOGY INDONESIA</h2>
				<h3 id="titleJenisReport">Perincian Laba Rugi</h3>
				<h3 id="titleTanggal">Per, <?php if(isset($dateSearch)) echo $dateSearch; else echo date('d-M-Y'); ?></h3>
			</div>

			<div id="divBodyReport">
				<div class="col-md-12 text-center reportBodyBoxNoTittle">
					<table class="tableReport" id="t_penjualan">
						<thead>
							<tr>
								<th align="left" colspan="3">PENJUALAN</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$totalPenjualan = 0;
							foreach($penjualan as $kp) {
								$totalPenjualan = $totalPenjualan + $kp['total'];
						?>
							<tr>
								<td style="width: 5%;"></td>
								<td align="left"><?php echo $kp['name_field']; ?></td>
								<td style="width: 20%;" align="right">Rp. <?php
									if($kp['total'] < 0 ) $kp['total'] = $kp['total'] * -1;
									echo number_format($kp['total'],2,",",".");
								?></td>
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td style="width: 5%;"></td>
								<th style="text-align: left;" align="left">PENJUALAN BERSIH</th>
								<th style="width: 20%; text-align: right;" class="tableCellSumTopBottom" align="right">Rp. <?php
									if($totalPenjualan < 0) $totalPenjualan = $totalPenjualan * -1;
									echo number_format($totalPenjualan,2,",",".");
								?></th>
							</tr>
						</tfoot>
					</table>

					<table class="tableReport" id="t_pembelian">
						<thead>
							<tr>
								<th align="left" colspan="3">PEMBELIAN</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$totalPembelian = 0;
							foreach($pembelian as $kpb) {
								$totalPembelian = $totalPembelian + $kpb['total'];
						?>
							<tr>
								<td style="width: 5%;"></td>
								<td align="left"><?php echo $kpb['name_field']; ?></td>
								<td style="width: 20%;" align="right">Rp. <?php
									if($kpb['total'] < 0) $kpb['total'] = $kpb['total'] * -1;
									echo number_format($kpb['total'],2,",",".");
								?></td>
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td style="width: 5%;"></td>
								<th style="text-align: left;" align="left">TOTAL PEMBELIAN</th>
								<th style="width: 20%; text-align: right;" class="tableCellSumTopBottom" align="right">Rp. <?php
									if($totalPembelian < 0) $totalPembelian = $totalPembelian * -1;
									echo number_format($totalPembelian,2,",",".");
								?></th>
							</tr>
						</tfoot>
					</table>

					<table class="tableReport" id="t_btkl">
						<?php
							$totalBTKL = 0;
							foreach($bTKL as $kBTKL) {
								$totalBTKL = $totalBTKL + $kBTKL['total'];
							}
						?>
						<tfoot>
							<tr>
								<th style="width: 60%; text-align: left;" align="left">BIAYA TENAGA KERJA LANGSUNG</th>
								<td style="width: 20%;"></td>
								<th style="width: 20%; text-align: right;" align="right">Rp. <?php
									if($totalBTKL < 0) $totalBTKL = $totalBTKL * -1;
									echo number_format($totalBTKL,2,",",".");
								?></th>
							</tr>
						</tfoot>
					</table>

					<table class="tableReport" id="t_bptl">
						<thead>
							<tr>
								<th class="labelTitle" colspan="3" align="left">BIAYA PRODUKSI TIDAK LANGSUNG</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$totalBPTL = 0;
							foreach($bPTL as $kBPTL) {
								$totalBPTL = $totalBPTL + $kBPTL['total'];
						?>
							<tr>
								<td style="width: 5%;"></td>
								<td align="left"><?php echo $kBPTL['name_field']; ?></td>
								<td style="width: 20%;" align="right">Rp. <?php echo number_format($kBPTL['total'],2,",","."); ?></td>
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td style="width: 5%;"></td>
								<th style="text-align: left;" align="left">TOTAL BIAYA PRODUKSI TIDAK LANGSUNG</th>
								<th style="width: 20%; text-align: right;" class="tableCellSumTopBottom" 
									align="right">Rp. <?php echo number_format($totalBPTL,2,",","."); ?></th>
							</tr>
						</tfoot>
					</table>

					<table class="tableReport" id="t_bp">
						<thead>
							<tr>
								<th class="labelTitle" colspan="3" align="left">BIAYA PENJUALAN</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$totalBP = 0;
							foreach($bO as $kBP) {
								$totalBP = $totalBP + $kBP['total'];
						?>
							<tr>
								<td style="width: 5%;"></td>
								<td align="left"><?php echo $kBP['name_field']; ?></td>
								<td style="width: 20%;" align="right">Rp. <?php echo number_format($kBP['total'],2,",","."); ?></td>
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td style="width: 5%;"></td>
								<th style="text-align: left;" align="left">TOTAL BIAYA PENJUALAN</th>
								<th style="width: 20%; text-align: right;" class="tableCellSumTopBottom" 
									align="right">Rp. <?php echo number_format($totalBP,2,",","."); ?></th>
							</tr>
						</tfoot>
					</table>
				</div>

				<div class="col-md-12 text-center reportBodyBoxNoTittle">
					<table class="tableReport" id="t_bau">
						<thead>
							<tr>
								<th align="left" colspan="3">BIAYA ADMINISTRASI & UMUM</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$totalBAU = 0;
							foreach($bAU as $kBAU) {
								$totalBAU = $totalBAU + $kBAU['total'];
						?>
							<tr>
								<td style="width: 5%;"></td>
								<td align="left"><?php echo $kBAU['name_field']; ?></td>
								<td style="width: 20%;" align="right">Rp. <?php echo number_format($kBAU['total'],2,",","."); ?></td>
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td style="width: 5%;"></td>
								<th style="text-align: left;" align="left">TOTAL BIAYA ADMINISTRASI & UMUM</th>
								<th style="width: 20%; text-align: right;" class="tableCellSumTopBottom" 
									align="right">Rp. <?php echo number_format($totalBAU,2,",","."); ?></th>
							</tr>
						</tfoot>
					</table>

					<table class="tableReport" id="t_pll">
						<thead>
							<tr>
								<th align="left" colspan="3">PENDAPATAN LAIN-LAIN</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$totalPLL = 0;
							foreach($pLL as $kPLL) {
								$totalPLL = $totalPLL + $kPLL['total'];
						?>
							<tr>
								<td style="width: 5%;"></td>
								<td align="left"><?php echo $kPLL['name_field']; ?></td>
								<td style="width: 20%;" align="right">Rp. <?php echo number_format($kPLL['total'],2,",","."); ?></td>
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td style="width: 5%;"></td>
								<th style="text-align: left;" align="left">TOTAL PENDAPATAN LAIN-LAIN</th>
								<th style="width: 20%; text-align: right;" class="tableCellSumTopBottom" 
									align="right">Rp. <?php echo number_format($totalPLL,2,",","."); ?></th>
							</tr>
						</tfoot>
					</table>

					<table class="tableReport" id="t_bll">
						<thead>
							<tr>
								<th align="left" colspan="3">BIAYA LAIN-LAIN</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$totalBLL = 0;
							foreach($bLL as $kBLL) {
								$totalBLL = $totalBLL + $kBLL['total'];
						?>
							<tr>
								<td style="width: 5%;"></td>
								<td align="left"><?php echo $kBLL['name_field']; ?></td>
								<td style="width: 20%;" align="right">Rp. <?php echo number_format($kBLL['total'],2,",","."); ?></td>
							</tr>
						<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<td style="width: 5%;"></td>
								<th style="text-align: left;" align="left">BIAYA LAIN-LAIN</th>
								<th style="width: 20%; text-align: right;" class="tableCellSumTopBottom" 
									align="right">Rp. <?php echo number_format($totalBLL,2,",","."); ?></th>
							</tr>
						</tfoot>
					</table>

				</div>
				
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){

		$('#btn_download').click(function () {
			var doc = new jsPDF();
			var pageHeight = doc.internal.pageSize.height || doc.internal.pageSize.getHeight();
			var pageWidth = doc.internal.pageSize.width || doc.internal.pageSize.getWidth();
			
			// FOOTER
			doc.setTextColor(100);
			doc.setFontSize(16);
			doc.text($('#titlePerusahaan').html(), pageWidth / 2, 20, 'center');
			doc.setFontSize(12);
			doc.text($('#titleJenisReport').html(), pageWidth / 2, 30, 'center');
			doc.setFontSize(14);
			doc.text($('#titleTanggal').html(), pageWidth / 2, 40, 'center');

			doc.setFontSize(12);
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_penjualan', 50));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_pembelian', doc.autoTable.previous.finalY + 3));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_btkl', doc.autoTable.previous.finalY + 3));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_bptl', doc.autoTable.previous.finalY + 3));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_bp', doc.autoTable.previous.finalY + 3));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_bau', doc.autoTable.previous.finalY + 10));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_pll', doc.autoTable.previous.finalY + 3));
			doc.autoTable(setAutoTable(1, pageWidth, pageHeight, '#t_bll', doc.autoTable.previous.finalY + 3));

			doc.save('Perincian Laba Rugi.pdf');
		});
	});

	function objectSize(obj) {
		var size = 0;
		for (key in obj) {
			if (obj.hasOwnProperty(key)) size++;
		}
		return size;
	}

	function setAutoTable(typeTable, pageWidth, pageHeight, idTable, setStart) {
		var data = {};
		if(typeTable == 0) {
			data = {
				html 			: idTable,
				theme			: 'plain',
				styles			: {fontSize : 10},
				margin 			: 20,
				tableWidth		: (pageWidth - 40),
				headStyles		: {halign : 'left'},
				rowPageBreak	: 'auto',
				showHead 		: 'firstPage',
				showFoot		: 'lastPage',
				startY			: setStart
			};
		}else if(typeTable == 1) {
			data = {
				html 			: idTable,
				theme			: 'plain',
				styles			: {fontSize : 10},
				margin 			: 20,
				tableWidth		: (pageWidth - 40),
				headStyles		: {halign : 'left'},
				didParseCell	: function (data) {
					if(data.table.body.length > 0) {
						for (var i = 0; i < data.table.body.length; i++) {
							var objSize = objectSize(data.table.body[i].cells);
							if(objSize > 0) {
								if(data.table.body[i].cells[1]) data.table.body[i].cells[1].styles.halign		= 'left';
								if(data.table.body[i].cells[2]) data.table.body[i].cells[2].styles.halign		= 'right';
								if(data.table.body[i].cells[3]) data.table.body[i].cells[3].styles.halign		= 'right';
							}
						}
					}
					if(data.table.foot.length > 0) {
						for (var i = 0; i < data.table.foot.length; i++) {
							var objSize = objectSize(data.table.foot[i].cells);
							if(objSize > 0) {
								if(data.table.foot[i].cells[1]) data.table.foot[i].cells[1].styles.halign		= 'left';
								if(data.table.foot[i].cells[2]) data.table.foot[i].cells[2].styles.halign		= 'right';
								if(data.table.foot[i].cells[3]) data.table.foot[i].cells[3].styles.halign		= 'right';
							}
						}
					}
				},
				columnStyles	: {
					0: {halign: 'left'},
					1: {halign: 'left', cellWidth: 90},
					2: {halign: 'right', cellWidth: 40},
					3: {halign: 'right', cellWidth: 40}
				},
				rowPageBreak	: 'auto',
				showHead 		: 'firstPage',
				showFoot		: 'lastPage',
				startY			: setStart
			};
		}

		return data;
	}
</script>