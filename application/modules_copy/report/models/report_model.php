<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Report_model extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function get_rl($dd) {
	
		
		$sql 	= "select abs(if(a.income is null,0,a.income)  - IF(b.outcome IS NULL,0,b.outcome)) as rp from (

					SELECT if(b.`type_coa` =1,a.`value_real` * -1,a.`value_real`) as income,a.*,b.`type_coa`, 1 AS kk FROM t_coa_value a
					left join t_coa b on a.`id_coa` = b.`id_coa`
					where a.id_coa in (461,462,463,464,465,466,492,493,494,495,496,497,498,499,500,501,502,541,542,543,473,474,475,476,477,478,479,480
					,481,482,483,484,485,486,487,488,489,490,491)
					and `date` < '".$dd."' AND a.type_cash = 0

					) a left join (

					SELECT IF(b.`type_coa` =1,a.`value_real` * -1,a.`value_real`) AS outcome,a.*,b.`type_coa`, 1 as oo FROM t_coa_value a
					LEFT JOIN t_coa b ON a.`id_coa` = b.`id_coa`
					WHERE a.id_coa IN (461,462,463,464,465,466,492,493,494,495,496,497,498,499,500,501,502,541,542,543,473,474,475,476,477,478,479,480
					,481,482,483,484,485,486,487,488,489,490,491)
					AND `date` < '".$dd."' AND a.type_cash = 1

					) b on a.kk - b.oo ";

//echo $sql;die;

		$query 	=  $this->db->query($sql);
		
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}			
	
	public function persediaan_per($data) {
	
		
		$sql 	= "
			SELECT ABS(pemasukan-pengeluaran) AS nilai FROM (
			SELECT IF(SUM(a.`value_real`) IS NULL,0,SUM(a.`value_real`)) AS pemasukan, 'kk' AS ok FROM `t_coa_value` a
			WHERE `date` < '".$data['start_date']."' AND a.`id_coa` = 120 AND a.`type_cash` = 0
			) a LEFT JOIN (
			SELECT IF(SUM(a.`value_real`) IS NULL,0,SUM(a.`value_real`)) AS pengeluaran, 'kk' AS ok  FROM `t_coa_value` a
			WHERE `date` < '".$data['start_date']."' AND a.`id_coa` = 120 AND a.`type_cash` = 1
			) b ON a.ok = b.ok
		";

//echo $sql;die;

		$query 	=  $this->db->query($sql);
		
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}		
	
	public function persediaan_per_akhir($data) {
	
		
		$sql 	= "
			SELECT abs(pemasukan-pengeluaran) AS nilai FROM (
			SELECT IF(SUM(a.`value_real`) IS NULL,0,SUM(a.`value_real`)) AS pemasukan, 'kk' AS ok FROM `t_coa_value` a
			WHERE DATE BETWEEN '".$data['start_date']."' AND '".$data['end_date']."' AND a.`id_coa` = 120 AND a.`type_cash` = 0
			) a LEFT JOIN (
			SELECT IF(SUM(a.`value_real`) IS NULL,0,SUM(a.`value_real`)) AS pengeluaran, 'kk' AS ok  FROM `t_coa_value` a
			WHERE DATE BETWEEN '".$data['start_date']."' AND '".$data['end_date']."' AND a.`id_coa` = 120 AND a.`type_cash` = 1
			) b ON a.ok = b.ok
		";

//echo $sql;die;

		$query 	=  $this->db->query($sql);
		
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}		
	
	public function penjualan_import($data) {
	
		
		$sql 	= "
			SELECT abs(pengeluaran-pemasukan) AS nilai FROM (
			SELECT IF(SUM(a.`value_real`) IS NULL,0,SUM(a.`value_real`)) AS pemasukan, 'kk' AS ok FROM `t_coa_value` a
			WHERE ( `date` between '".$data['start_date']."' and '".$data['end_date']."' ) AND a.`id_coa` = 461 AND a.`type_cash` = 0
			) a LEFT JOIN (
			SELECT IF(SUM(a.`value_real`) IS NULL,0,SUM(a.`value_real`)) AS pengeluaran, 'kk' AS ok  FROM `t_coa_value` a
			WHERE ( `date` between '".$data['start_date']."' and '".$data['end_date']."' ) AND a.`id_coa` = 461 AND a.`type_cash` = 1
			) b ON a.ok = b.ok
		";

//echo $sql;die;

		$query 	=  $this->db->query($sql);
		
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}	
	
	public function penjualan_lokal($data) {
	
		
		$sql 	= "
			SELECT abs(pengeluaran-pemasukan) AS nilai FROM (
			SELECT IF(SUM(a.`value_real`) IS NULL,0,SUM(a.`value_real`)) AS pemasukan, 'kk' AS ok FROM `t_coa_value` a
			WHERE ( `date` between '".$data['start_date']."' and '".$data['end_date']."' ) AND a.`id_coa` = 462 AND a.`type_cash` = 0
			) a LEFT JOIN (
			SELECT IF(SUM(a.`value_real`) IS NULL,0,SUM(a.`value_real`)) AS pengeluaran, 'kk' AS ok  FROM `t_coa_value` a
			WHERE ( `date` between '".$data['start_date']."' and '".$data['end_date']."' ) AND a.`id_coa` = 462 AND a.`type_cash` = 1
			) b ON a.ok = b.ok
		";

//echo $sql;die;

		$query 	=  $this->db->query($sql);
		
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}		
	
	public function potongan_penjualan($data) {
	
		
		$sql 	= "
			SELECT abs(pengeluaran-pemasukan) AS nilai FROM (
			SELECT IF(SUM(a.`value_real`) IS NULL,0,SUM(a.`value_real`)) AS pemasukan, 'kk' AS ok FROM `t_coa_value` a
			WHERE ( `date` between '".$data['start_date']."' and '".$data['end_date']."' ) AND a.`id_coa` = 463 AND a.`type_cash` = 0
			) a LEFT JOIN (
			SELECT IF(SUM(a.`value_real`) IS NULL,0,SUM(a.`value_real`)) AS pengeluaran, 'kk' AS ok  FROM `t_coa_value` a
			WHERE ( `date` between '".$data['start_date']."' and '".$data['end_date']."' ) AND a.`id_coa` = 463 AND a.`type_cash` = 1
			) b ON a.ok = b.ok
		";

//echo $sql;die;

		$query 	=  $this->db->query($sql);
		
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}		
	
	public function retur_penjualan($data) {
	
		
		$sql 	= "
			SELECT abs(pengeluaran-pemasukan) AS nilai FROM (
			SELECT IF(SUM(a.`value_real`) IS NULL,0,SUM(a.`value_real`)) AS pemasukan, 'kk' AS ok FROM `t_coa_value` a
			WHERE ( `date` between '".$data['start_date']."' and '".$data['end_date']."' ) AND a.`id_coa` = 464 AND a.`type_cash` = 0
			) a LEFT JOIN (
			SELECT IF(SUM(a.`value_real`) IS NULL,0,SUM(a.`value_real`)) AS pengeluaran, 'kk' AS ok  FROM `t_coa_value` a
			WHERE ( `date` between '".$data['start_date']."' and '".$data['end_date']."' ) AND a.`id_coa` = 464 AND a.`type_cash` = 1
			) b ON a.ok = b.ok
		";

//echo $sql;die;

		$query 	=  $this->db->query($sql);
		
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}		
	
	public function persediaan_wip_akhir($data) {
	
		
		$sql 	= "
			SELECT abs(income - outcome) AS saldo_akhir_wip FROM (
			SELECT 'a'AS a,IF(SUM(amount_mutasi*base_price)IS NULL,0,SUM(amount_mutasi*base_price)) income FROM m_material a
			LEFT JOIN t_coa b ON a.`cust_id`=b.`id_eksternal`
			LEFT JOIN t_mutasi c ON a.`id`=c.`id_stock_akhir`
			WHERE a.`type` IN (4) AND c.date_mutasi < '".$data['end_date']."' AND c.`type_mutasi` = 0
			) a LEFT JOIN (
			SELECT 'a'AS a,IF(SUM(amount_mutasi*base_price)IS NULL,0,SUM(amount_mutasi*base_price)) outcome FROM m_material a
			LEFT JOIN t_coa b ON a.`cust_id`=b.`id_eksternal`
			LEFT JOIN t_mutasi c ON a.`id`=c.`id_stock_akhir`
			WHERE a.`type` IN (4) AND c.date_mutasi < '".$data['end_date']."' AND c.`type_mutasi` = 1
			) b ON a.a = b.a
		";

//echo $sql;die;

		$query 	=  $this->db->query($sql);
		
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}		
	
	public function persediaan_wip_awal($data) {
	
		
		$sql 	= "
			SELECT abs(income - outcome) AS saldo_awal_wip FROM (
			SELECT 'a'AS a,IF(SUM(amount_mutasi*base_price)IS NULL,0,SUM(amount_mutasi*base_price)) income FROM m_material a
			LEFT JOIN t_coa b ON a.`cust_id`=b.`id_eksternal`
			LEFT JOIN t_mutasi c ON a.`id`=c.`id_stock_akhir`
			WHERE a.`type` IN (4) AND c.date_mutasi < '".$data['start_date']."' AND c.`type_mutasi` = 0
			) a LEFT JOIN (
			SELECT 'a'AS a,IF(SUM(amount_mutasi*base_price)IS NULL,0,SUM(amount_mutasi*base_price)) outcome FROM m_material a
			LEFT JOIN t_coa b ON a.`cust_id`=b.`id_eksternal`
			LEFT JOIN t_mutasi c ON a.`id`=c.`id_stock_akhir`
			WHERE a.`type` IN (4) AND c.date_mutasi < '".$data['start_date']."' AND c.`type_mutasi` = 1
			) b ON a.a = b.a
		";

//echo $sql;die;

		$query 	=  $this->db->query($sql);
		
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}		
	
	public function persediaan_beli($data) {
	
		
		$sql 	= "
			SELECT abs(pemasukan-pengeluaran) AS nilai FROM (
			SELECT IF(SUM(a.`value_real`) IS NULL,0,SUM(a.`value_real`)) AS pemasukan, 'kk' AS ok FROM `t_coa_value` a
			LEFT JOIN t_coa b ON a.`id_coa` = b.`id_coa`
			WHERE (`date` BETWEEN '".$data['start_date']."' AND  '".$data['end_date']."' ) AND b.`id_parent` = '30100' AND a.`type_cash` = 0
			) a LEFT JOIN (
			SELECT IF(SUM(a.`value_real`) IS NULL,0,SUM(a.`value_real`)) AS pengeluaran, 'kk' AS ok  FROM `t_coa_value` a
			LEFT JOIN t_coa b ON a.`id_coa` = b.`id_coa`
			WHERE (`date` BETWEEN '".$data['start_date']."' AND  '".$data['end_date']."' ) AND b.`id_parent` = '30100' AND a.`type_cash` = 1
			) b ON a.ok = b.ok
		";

//echo $sql;die;

		$query 	=  $this->db->query($sql);
		
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}		
	
	public function get_report($data) {
		$sql 	= 'CALL report_finance(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['date_search'],
			$data['report_finance']
		));
		
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}		
	
	public function get_reportBo($data) {
		$sql 	= 'CALL report_finance_bo(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['date_search'],
			$data['report_finance']
		));
		
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}	
	
	public function get_report2($data) {
		$sql 	= 'CALL report_finance_hutang(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['date_search'],
			$data['report_finance']
		));
		
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function finance_hpp_up($data) {
		$sql 	= 'CALL report_finance_hpp_up(?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['start_date'],
			$data['end_date']
		));
		
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function getDataCOA($strQry) {
		$sql = 'SELECT id_coa, keterangan FROM t_coa WHERE '.$strQry;
		$query 	=  $this->db->query($sql);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function getDataBBK() {
		$sql = 'SELECT id_coa, keterangan FROM t_coa WHERE id_parent = 10120';
		$query 	=  $this->db->query($sql);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function getIdCOA($id) {
		$sql = 'SELECT coa, keterangan FROM t_coa WHERE id_coa = '.$id;
		$query 	=  $this->db->query($sql);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function getIdBBK($id) {
		$sql = 'SELECT a.id_coa,a.coa, a.keterangan,b.nama_bank,b.no_rek FROM t_coa a JOIN t_bank b ON b.id_coa = a.id_coa WHERE a.id_coa ='.$id;
		$query 	=  $this->db->query($sql);

		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function getSaldoAwal($data) {
		$sql 	= 'CALL general_ledger_saldo_awal(?,?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['coa'],
			$data['start_date'],
			$data['end_date']
		));
		
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function dataGeneralLedger($data) {
		$sql 	= 'CALL general_ledger(?,?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['coa'],
			$data['start_date'],
			$data['end_date']
		));
		
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function dataBBK($data) {
		$sql 	= 'CALL general_ledger_bank(?,?,?)';

		$query 	=  $this->db->query($sql, array(
			$data['coa'],
			$data['start_date'],
			$data['end_date']
		));
		
		$result	= $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
}