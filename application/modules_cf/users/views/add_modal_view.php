	<style>
		#loading-us {
			display: none
		}

		#tick {
			display: none
		}

		#loading-mail {
			display: none
		}

		#cross {
			display: none
		}
	</style>

	<form class="form-horizontal form-label-left" id="add_distributor" role="form" action="<?php echo base_url('distributor/add_distributor'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>

		<p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="kode_distributor">Kode Distributor <span class="required"><sup>*</sup></span>
			</label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input data-parsley-maxlength="255" type="text" id="kode_distributor" name="kode_distributor" class="form-control col-md-7 col-xs-12" placeholder="Kode Distributor" required="required">
				<span id="loading-us" class="fa fa-spinner fa-spin fa-fw"> Checking kode distributor...</span>
				<span id="tick"></span>
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama <span class="required"><sup>*</sup></span>
			</label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input data-parsley-maxlength="255" type="text" id="dist_name" name="dist_name" class="form-control col-md-7 col-xs-12" placeholder="Nama" required="required">
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Alamat <span class="required"><sup>*</sup></span>
			</label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<textarea data-parsley-maxlength="255" type="text" id="dist_address" name="dist_address" class="form-control col-md-7 col-xs-12" placeholder="Alamat" required="required"></textarea>
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="ship_address">Ship Address<span class="required"><sup>*</sup></span>
			</label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<textarea data-parsley-maxlength="255" type="text" id="ship_address" name="ship_address" class="form-control col-md-7 col-xs-12" placeholder="Ship Address"></textarea>
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Phone 1 <span class="required"><sup>*</sup></span>
			</label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input data-parsley-maxlength="15" type="number" id="phone_1" name="phone_1" class="form-control col-md-7 col-xs-12" placeholder="Phone 1" required="required">
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Phone 2</label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input data-parsley-maxlength="255" type="text" id="phone_2" name="phone_2" class="form-control col-md-7 col-xs-12" placeholder="Phone 2">
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Fax</label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input data-parsley-maxlength="255" type="text" id="fax" name="fax" class="form-control col-md-7 col-xs-12" placeholder="Fax">
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Email<span class="required"><sup>*</sup></span>
			</label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input data-parsley-maxlength="255" type="email" id="email" name="email" class="form-control col-md-7 col-xs-12" placeholder="Email" required="required">
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">PIC <span class="required"><sup>*</sup></span>
			</label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<input data-parsley-maxlength="255" type="text" id="pic" name="pic" class="form-control col-md-7 col-xs-12" placeholder="PIC" required="required">
			</div>
		</div>

		<div class="item form-group form-item">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Lokasi
				<span class="required">
					<sup>*</sup>
				</span>
			</label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<select class="form-control" name="location" id="location" style="width: 100%" required>
					<?php foreach ($location as $key) { ?>
						<option value="<?php echo $key['id']; ?>"><?php echo $key['name_eksternal_loc']; ?></option>
					<?php } ?>
				</select>
			</div>
		</div>

		<div class="item form-group">
			<label class="control-label col-md-3 col-sm-3 col-xs-12" for="submit"></label>
			<div class="col-md-8 col-sm-6 col-xs-12">
				<button id="btn-submit" class="btn btn-primary btn-bordred waves-effect w-md waves-light m-b-5" type="submit">Add Distributor</button>
			</div>
		</div>

	</form><!-- /page content -->

	<script type="text/javascript">
		$(document).ready(function() {
			$('form').parsley();
			$('[data-toggle="tooltip"]').tooltip();
		});

		var kode_eksternal = $('#kode_distributor').val();
		$('#kode_distributor').on('input', function(event) {
			if ($('#kode_distributor').val() != kode_eksternal) {
				kode_check();
			}
		});

		function kode_check() {
			var kode_distributor = $('#kode_distributor').val();
			if (kode_distributor.length >= 3) {
				var post_data = {
					'kode_distributor': kode_distributor
				};

				$('#tick').empty();
				$('#tick').hide();
				$('#loading-us').show();
				jQuery.ajax({
					type: "POST",
					url: "<?php echo base_url('distributor/check_kode'); ?>",
					data: post_data,
					cache: false,
					success: function(response) {
						if (response.success == true) {
							$('#kode_distributor').css('border', '3px #090 solid');
							$('#loading-us').hide();
							$('#tick').empty();
							$("#tick").append('<span class="fa fa-check"> ' + response.message + '</span>');
							$('#tick').show();
						} else {
							$('#kode_distributor').css('border', '3px #C33 solid');
							$('#loading-us').hide();
							$('#tick').empty();
							$("#tick").append('<span class="fa fa-close"> ' + response.message + '</span>');
							$('#tick').show();
						}
					}
				});
			} else {
				$('#kode_distributor').css('border', '3px #C33 solid');
				$('#loading-us').hide();
				$('#tick').empty();
				$("#tick").append('<span class="fa fa-close"> This value is too short. It should have 4 characters or more</span>');
				$('#tick').show();
			}
		}

		$('#add_distributor').on('submit', (function(e) {
			$('#btn-submit').attr('disabled', 'disabled');
			$('#btn-submit').text("Memasukkan data...");
			e.preventDefault();
			var formData = new FormData(this);

			$.ajax({
				type: 'POST',
				url: $(this).attr('action'),
				data: formData,
				cache: false,
				contentType: false,
				processData: false,
				success: function(response) {
					console.log(response);
					if (response.success == true) {
						$('.panel-heading button').trigger('click');
						listdist();
						swal({
							title: 'Success!',
							text: response.message,
							type: 'success',
							showCancelButton: false,
							confirmButtonText: 'Ok'
						}).then(function() {

						});
					} else {
						$('#btn-submit').removeAttr('disabled');
						$('#btn-submit').text("Tambah Distributor");
						swal("Failed!", response.message, "error");
					}
				}
			}).fail(function(xhr, status, message) {
				$('#btn-submit').removeAttr('disabled');
				$('#btn-submit').text("Tambah Distributor");
				swal("Failed!", "Invalid respon, silahkan cek koneksi.", "error");
			});
		}));
	</script>