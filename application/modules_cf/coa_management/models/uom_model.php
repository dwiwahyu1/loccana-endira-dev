<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Uom_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
    
    public function get_uom_data($id)
	{
		$sql 	= 'SELECT b.coa AS coa_parent, b.keterangan AS ket_parent, a.* FROM `t_coa` a
					LEFT JOIN `t_coa` b
					ON a.id_parent = b.id_coa where a.id_coa = '.$id;

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_cash_acc(){
		
		$sql 	= 'select * from t_coa order by coa';

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
		
	}

	public function lists($params = array())
	{
		
			$query_s = ' ';
			
		// // }else{
			
			// // $query_s = ' AND a.payment_status = '.$params['filter'];
		// // }
		
		// if($params['filter_month'] == 'ALL'){
			// $query_m = ' ';
		// }else{
			// $query_m = ' AND DATE_FORMAT(a.date,"%M") = "'.$params['filter_month'].'"';
		// }
		
		// if($params['filter_year'] == 'ALL'){
			// $query_y = ' ';
		// }else{
			// $query_y = 'AND DATE_FORMAT(a.date,"%Y") = "'.$params['filter_year'].'" ';
		// }
		
		$where_sts = $query_s;
		
			$query = 	'
				SELECT COUNT(*) AS jumlah FROM `t_coa` a
					LEFT JOIN `t_coa` b ON a.id_parent = b.id_coa where 1=1 '.$where_sts.'
				AND ( 
					a.coa LIKE "%'.$params['searchtxt'].'%" OR
					a.keterangan LIKE "%'.$params['searchtxt'].'%" 
				)
			';
						
			$query2 = 	'
				SELECT z.*, rank() over ( ORDER BY coa ) AS Rangking from ( 
					
					SELECT b.coa AS coa_parent, b.keterangan AS ket_parent, a.* FROM `t_coa` a
					LEFT JOIN `t_coa` b
					ON a.id_parent = b.id_coa
					where 1=1 '.$where_sts.'  AND ( 
					a.coa LIKE "%'.$params['searchtxt'].'%" OR
					a.keterangan LIKE "%'.$params['searchtxt'].'%" 
				)  ORDER BY coa) z
				ORDER BY `coa` ASC
				
				LIMIT '.$params['limit'].' 
				OFFSET '.$params['offset'].' 
			';
		
			//echo $query2;die;
		
			 $out		= array();
			  $querys		= $this->db->query($query);
			  $result = $querys->row();
			  
			  $total_filtered = $result->jumlah;
			  $total 			= $result->jumlah;
		  
				if(($params['offset']+10) > $total_filtered){
				$limit_data = $total_filtered - $params['offset'];
			  }else{
				$limit_data = $params['limit'] ;
			  }
		  
		
		  
		  //echo $query;die;
			//echo $query;die;
			 $query2s		= $this->db->query($query2);
		  $result2 = $query2s->result_array();						
		  $return = array(
			  'data' => $result2,
			  'total_filtered' => $total_filtered,
			  'total' => $total,
		  );
		  return $return;
	}
	
	public function edit_uom($data)
	{
		$datas = array(
			'coa' => $data['nama_uom'],
			'id_parent' => $data['coa'],
			'keterangan' => $data['keterangan']
		);

	//	print_r($datas);die;
		$this->db->where('id_coa',$data['id_uom']);
		$this->db->update('t_coa',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_uom($data)
	{
		$datas = array(
			'coa' => $data['nama_uom'],
			'id_parent' => $data['coa'],
			'keterangan' => $data['keterangan']
		);

	//	print_r($datas);die;

		$this->db->insert('t_coa',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	
	public function deletes($id_uom)
	{
		$this->db->where('id_coa', $id_uom);
		$this->db->delete('t_coa'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}


}
