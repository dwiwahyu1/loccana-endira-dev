<?php defined('BASEPATH') or exit('No direct script access allowed');

class Purchase_order extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('purchase_order/purchase_order_model');
		$this->load->library('log_activity');
		$this->load->library('priv');

		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong');
		$this->form_validation->set_message('min_length', '%s Minimal 4 Karakter');
		$this->form_validation->set_message('max_length', '%s Maksimal 10 Karakter');
	}

	/**
	 * anti sql injection
	 * @return string
	 */
	public function Anti_sql_injection($string)
	{
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	 * This function is redirect to index distributor page
	 * @return Void
	 */
	public function index()
	{

		$priv = $this->priv->get_priv();

		$year_count = date('Y') - 2019;

		$filter_year = '<select class="form-control" id="filter_year" onChange="changeFilter()"><option value="ALL" >ALL</option>';

		for ($i = $year_count; $i >= 0; $i--) {

			$y = 2019 + $i;

			if ($y == date('Y')) {
				$filter_year .= '<option value="' . $y . '" selected="selected" >' . $y . '</option>';
			} else {
				$filter_year .= '<option value="' . $y . '" >' . $y . '</option>';
			}
		}
		$filter_year .= '</select>';

		$array_month = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

		$filter_month = '<select class="form-control" id="filter_month" onChange="changeFilter()"><option value="ALL" >ALL</option>';
		foreach ($array_month as $array_months) {
			if ($array_months == date('F')) {
				$filter_month .= '<option value="' . $array_months . '" selected="selected">' . $array_months . '</option>';
			} else {
				$filter_month .= '<option value="' . $array_months . '" >' . $array_months . '</option>';
			}
		}

		$filter_month .= '</select>';
		$today_month = date('F');
		$today_year = date('Y');
		//echo date("F");die;

		$data = array(
			'priv' => $priv,
			'filter_month' => $filter_month,
			'filter_year' => $filter_year,
			'today_month' => $today_month,
			'today_year' => $today_year
		);

		$this->template->load('maintemplate', 'purchase_order/views/index', $data);
	}

	/**
	 * This function is used for showing distributor list
	 * @return Array
	 */
	function lists()
	{

		if (!empty($_GET['sess_user_id'])) {
			$sess_user_id = $_GET['sess_user_id'];
		} else {
			$sess_user_id = NULL;
		}

		if (!empty($_GET['sess_token'])) {
			$sess_token = $_GET['sess_token'];
		} else {
			$sess_token = NULL;
		}

		if (!empty($_GET['filter'])) {
			$filter = $_GET['filter'];
		} else {
			$filter = NULL;
		}

		if (!empty($_GET['filter_year'])) {
			$filter_year = $_GET['filter_year'];
		} else {
			$filter_year = NULL;
		}

		if ($this->input->get_post('draw') != FALSE) {
			$draw   = $this->input->get_post('draw');
		} else {
			$draw   = 1;
		};
		if ($this->input->get_post('length') != FALSE) {
			$length = $this->input->get_post('length');
		} else {
			$length = 10;
		};
		if ($this->input->get_post('start') != FALSE) {
			$start  = $this->input->get_post('start');
		} else {
			$start  = 0;
		};
		$order_fields = array('Rangking', 'no_po', 'name_eksternal', 'date_po', 'total_amount', 'term_of_payment', 'status', 'status'); // , 'COST'
		$order = $this->input->get_post('order');
		if (!empty($order[0]['dir'])) {
			$order_dir    = $order[0]['dir'];
		} else {
			$order_dir    = 'desc';
		};
		if (!empty($order[0]['column'])) {
			$order_column = $order[0]['column'];
		} else {
			$order_column = 0;
		};

		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['sess_user_id'] = $sess_user_id;
		$params['filter'] 		= $_GET['filter'];
		$params['filter_year'] 		= $_GET['filter_year'];
		$params['sess_token'] 	= $sess_token;
		$params['searchtxt'] 	= $_GET['search']['value'];

		// print_r($params);die;
		$priv = $this->priv->get_priv();

		$list = $this->purchase_order_model->list_po($params);
		//print_r($list['data']);die;
		$data = array();
		$rowNumber = $start + 1;
		foreach ($list['data'] as $key => $row) {

			if ((int) $row['status'] === 0) {
				$sts = '<button type="button" class="btn btn-custon-rounded-two btn-danger" onClick="konfirmasi(' . $row['id_po'] . ')"  > Konfirmasi </button>';
				$action = '
				<div class="btn-group" style="display:' . $priv['update'] . '"><button type="button" class="btn btn-custon-rounded-two btn-warning" data-toggle="tooltip" data-placement="top" title="Edit" onClick="updatepo(' . $row['id_po'] . ')" > <i class="fa fa-edit"></i> </button></div>
				<div class="btn-group" style="display:' . $priv['delete'] . '"><button type="button" class="btn btn-custon-rounded-two btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"  onClick="deletepo(' . $row['id_po'] . ')" > <i class="fa fa-trash"></i> </button></div>
				<div class="btn-group" style="display:' . $priv['detail'] . '"><button type="button" class="btn btn-custon-rounded-two btn-info" data-toggle="tooltip" data-placement="top" title="Detail"   onClick="detail(' . $row['id_po'] . ')" > <i class="fa fa-search-plus"></i> </button></div>
				<div class="btn-group" style="display:' . $priv['update'] . '"><button type="button" class="btn btn-custon-rounded-two btn-secondary" data-toggle="tooltip" data-placement="top" title="Print" onClick="print_pdf(' . $row['id_po'] . ')" > <i class="fa fa-download"></i> </button></div>
				';
			} else if ((int) $row['status'] === 1) {
				$sts = '<button type="button" class="btn btn-custon-rounded-two btn-primary" disabled > Disetujui </button>';
				$action = '
				<div class="btn-group" style="display:' . $priv['detail'] . '"><button type="button" class="btn btn-custon-rounded-two btn-info" data-toggle="tooltip" data-placement="top" title="Detail" onClick="detail(' . $row['id_po'] . ')" > <i class="fa fa-search-plus"></i> </button></div>
				<div class="btn-group" style="display:' . $priv['detail'] . '"><button type="button" class="btn btn-custon-rounded-two btn-secondary" data-toggle="tooltip" data-placement="top" title="Print"  onClick="print_pdf(' . $row['id_po'] . ')" >  <i class="fa fa-download"></i> </button></div>
				';
			} else if ((int) $row['status'] === 2) {
				$sts = '<button type="button" class="btn btn-custon-rounded-two btn-danger" disabled > Tidak disetujui </button>';
				$action = '
				<div class="btn-group" style="display:' . $priv['detail'] . '"><button type="button" class="btn btn-custon-rounded-two btn-info" data-toggle="tooltip" data-placement="top" title="Detail" onClick="detail(' . $row['id_po'] . ')" > <i class="fa fa-search-plus"></i> </button></div>
				<div class="btn-group" style="display:' . $priv['detail'] . '"><button type="button" class="btn btn-custon-rounded-two btn-secondary" data-toggle="tooltip" data-placement="top" title="Print"  onClick="print_pdf(' . $row['id_po'] . ')" >  <i class="fa fa-download"></i> </button></div>
				';
			} else {
				$sts = '-';
				$action = '-';
			}

			if ($row['term_of_payment'] == '0' || $row['term_of_payment'] == 'Cash') {
				$jatuhTempo = 'Cash';
			} elseif ($row['term_of_payment'] == 'null') {
				$jatuhTempo = 'Tanpa Tempo';
			} else {
				$jatuhTempo =  $row['term_of_payment'] . ' Hari';
			}


			array_push(
				$data,
				array(
					// number_format($row['Rangking'], 0, ',', '.'),
					$rowNumber++,
					$row['no_po'],
					$row['name_eksternal'],
					$row['date_po'],
					$row['symbol'] . ' ' . number_format($row['total_amount'], 0, ',', '.'),
					$jatuhTempo,
					$sts,
					$action
				)
			);
			//$idx++;
		}

		$result["data"] = $data;
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		//$result["data"] = $list['data'];

		echo json_encode($result);

		//$this->json_result($result);

	}

	/**
	 * This function is redirect to add distributor page
	 * @return Void
	 */


	public function get_code()
	{

		$result_g = $this->purchase_order_model->get_kode();
		if ($result_g[0]['max'] + 1 < 1000) {
			if ($result_g[0]['max'] + 1 < 100) {
				if ($result_g[0]['max'] + 1 < 10) {
					$kode = 'R' . date('Y') . '000' . ($result_g[0]['max'] + 1);
				} else {
					$kode = 'R' . date('Y') . '00' . ($result_g[0]['max'] + 1);
				}
			} else {
				$kode = 'R' . date('Y') . '0' . ($result_g[0]['max'] + 1);
			}
		} else {
			$kode = 'R' . date('Y') . '' . ($result_g[0]['max'] + 1);
		}
		//print_r($kode);die;

		$data = array(
			'kode' => $kode
		);

		$this->output->set_content_type('application/json')->set_output(json_encode($data));
	}

	public function get_principal()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$result['principal'] = $this->purchase_order_model->get_principal_byid($params);
		$item = $this->purchase_order_model->get_item_byprin($params);
		$result_g = $this->purchase_order_model->get_kode();
		if ($result_g[0]['max'] + 1 < 1000) {
			if ($result_g[0]['max'] + 1 < 100) {
				if ($result_g[0]['max'] + 1 < 10) {
					$kode = 'R' . date('Y') . '000' . ($result_g[0]['max'] + 1);
				} else {
					$kode = 'R' . date('Y') . '00' . ($result_g[0]['max'] + 1);
				}
			} else {
				$kode = 'R' . date('Y') . '0' . ($result_g[0]['max'] + 1);
			}
		} else {
			$kode = 'R' . date('Y') . '' . ($result_g[0]['max'] + 1);
		}

		$list = '<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		foreach ($item as $items) {

			$list .= '<option value="' . $items['id_mat'] . '" >' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '</option>';
		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';

		$result['list'] = $list;
		$result['kode'] = $kode;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function get_all_item()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$item = $this->purchase_order_model->get_item_byprin($params);

		$list = '<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		foreach ($item as $items) {

			$list .= '<option value="' . $items['id_mat'] . '" >' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '</option>';
		}
		$list .= '<option value="0" selected="selected" disabled>-- Pilih Item Lainnya --</option>';

		$item2 = $this->purchase_order_model->get_item_byprin_all($params);

		foreach ($item2 as $items) {

			$list .= '<option value="' . $items['id_mat'] . '" >' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '</option>';
		}
		//	$list .= '------------------';

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function get_prin_item()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$item = $this->purchase_order_model->get_item_byprin($params);

		$list = '<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		foreach ($item as $items) {

			$list .= '<option value="' . $items['id_mat'] . '" >' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '</option>';
		}
		//$list .= '<option value="0" selected="selected" disabled>-- Pilih Item Lainnya --</option>';

		//	$list .= '------------------';

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}


	public function get_price_mat()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		//print_r($params);die;

		$ddd = $this->purchase_order_model->get_price_mat($params);
		//$item = $this->purchase_order_model->get_item_byprin($params);

		if (count($ddd) == 0) {
			$list = 0;
		} else {
			// NOTE: pemakaian number_format() dibawah membuat form input harga menjadi hilang ketika focus/click.
			// $list = number_format($ddd[0]['buy_price'], 2, ',', '.');
			$list = number_format($ddd[0]['base_price'], 2, ',', '');
		}
		//print_r($ddd);die;

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add()
	{
		$result = $this->purchase_order_model->get_principal();

		$result_g = $this->purchase_order_model->get_kode();
		if ($result_g[0]['max'] + 1 < 1000) {
			if ($result_g[0]['max'] + 1 < 100) {
				if ($result_g[0]['max'] + 1 < 10) {
					$kode = 'R' . date('Y') . '000' . ($result_g[0]['max'] + 1);
				} else {
					$kode = 'R' . date('Y') . '00' . ($result_g[0]['max'] + 1);
				}
			} else {
				$kode = 'R' . date('Y') . '0' . ($result_g[0]['max'] + 1);
			}
		} else {
			$kode = 'R' . date('Y') . '' . ($result_g[0]['max'] + 1);
		}
		//print_r($kode);die;

		$data = array(
			'principal' => $result,
			'kode' => $kode
		);

		$this->template->load('maintemplate', 'purchase_order/views/addPo', $data);
		//$this->load->view('addPrinciple',$data);
	}


	public function updatepo()
	{
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),

		);

		//print_r($data);

		$po_result = $this->purchase_order_model->get_po($data);
		$po_detail_result = $this->purchase_order_model->get_po_detail2($data);
		$result = $this->purchase_order_model->get_principal();

		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);


		$item = $this->purchase_order_model->get_item_byprin($params);
		$item_all = $this->purchase_order_model->get_item_byprin_all($params);
		//print_r($po_detail_result);die;

		// $result = $this->distributor_model->location();

		$data = array(
			'po' => $po_result[0],
			'principal' => $result,
			'po_mat' => $po_detail_result,
			'item' => $item,
			'item_all' => $item_all
		);

		$this->template->load('maintemplate', 'purchase_order/views/updatePo', $data);
		//$this->load->view('addPrinciple',$data);
	}

	public function detail()
	{
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),

		);

		//print_r($data);

		$po_result = $this->purchase_order_model->get_po($data);
		$po_detail_result = $this->purchase_order_model->get_po_detail($data);
		$result = $this->purchase_order_model->get_principal();

		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);

		$item = $this->purchase_order_model->get_item_byprin($params);
		$item_all = $this->purchase_order_model->get_item_byprin_all2($params);
		//print_r($principal_result);die;

		// $result = $this->distributor_model->location();

		$data = array(
			'po' => $po_result[0],
			'principal' => $result,
			'po_mat' => $po_detail_result,
			'item' => $item,
			'item_all' => $item_all
		);

		$this->template->load('maintemplate', 'purchase_order/views/detailPo', $data);
		//$this->load->view('addPrinciple',$data);
	}

	public function konfirmasi()
	{
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),

		);

		//print_r($data);

		$po_result = $this->purchase_order_model->get_po($data);
		$po_detail_result = $this->purchase_order_model->get_po_detail($data);
		$result = $this->purchase_order_model->get_principal();

		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);

		$item = $this->purchase_order_model->get_item_byprin($params);
		$item_all = $this->purchase_order_model->get_item_byprin_all2($params);
		//print_r($principal_result);die;

		// $result = $this->distributor_model->location();

		$data = array(
			'po' => $po_result[0],
			'principal' => $result,
			'po_mat' => $po_detail_result,
			'item' => $item,
			'item_all' => $item_all
		);

		$this->template->load('maintemplate', 'purchase_order/views/approvallPo', $data);
		//$this->load->view('addPrinciple',$data);
	}

	public function approve_po()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$data = array(
			'id_po' => $params['id'],
			'sts' => $params['sts']

		);

		//print_r($data);die;

		$add_prin_result = $this->purchase_order_model->edit_po_apr($data);
	}

	public function add_po()
	{

		// $this->form_validation->set_rules('kode', 'Kode Principal', 'required|min_length[4]|max_length[10]');
		// $this->form_validation->set_rules('alamat', 'Alamat Principal', 'required');
		// $this->form_validation->set_rules('term', 'Term Pembayaran Harus Diisi', 'required');
		$this->form_validation->set_rules('name', 'Principle', 'required');
		$this->form_validation->set_rules('tgl', 'Tanggal PO', 'required');
		$this->form_validation->set_rules('kode_0', 'Item', 'required');



		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();

			//echo $pesan;die;

			//$msg = strip_tags(str_replace("\r", '', $pesan));
			$msg = $pesan;

			$result = array('success' => false, 'message' => $msg);
		} else {

			$int_val = $this->Anti_sql_injection($this->input->post('int_flo', TRUE));

			$array_items = [];

			$total_amount = 0;
			for ($i = 0; $i <= $int_val; $i++) {

				//if(isset($this->input->post('kode_'.$i, TRUE))){
				$array_items[$i]['kode'] = $this->Anti_sql_injection($this->input->post('kode_' . $i, TRUE));
				$array_items[$i]['harga'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE)))));
				$array_items[$i]['diskon'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('diskon_' . $i, TRUE)))));
				$array_items[$i]['total'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('total_' . $i, TRUE)))));
				//$array_items[$i]['remark'] = $this->Anti_sql_injection($this->input->post('remark_'.$i, TRUE));
				$array_items[$i]['remark'] = "";
				$array_items[$i]['qty'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('qty_' . $i, TRUE)))));
				//}

				$total_amount = $total_amount + ((($array_items[$i]['harga']) - (($array_items[$i]['harga'] * ($array_items[$i]['diskon'] / 100)))) * $array_items[$i]['qty']);
			}



			$total_amounts = $total_amount + ($total_amount * (floatval($this->Anti_sql_injection($this->input->post('ppn', TRUE))) / 100));


			if ($this->Anti_sql_injection($this->input->post('term', TRUE)) == 'other') {
				$terms = $this->Anti_sql_injection($this->input->post('term_o', TRUE));
			} else {
				$terms = $this->Anti_sql_injection($this->input->post('term', TRUE));
			}


			$data = array(
				'no_po' => $this->Anti_sql_injection($this->input->post('kode', TRUE)),
				'date_po' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
				'ints' => $this->Anti_sql_injection($this->input->post('ints', TRUE)),
				'ints2' => $this->Anti_sql_injection($this->input->post('ints2', TRUE)),
				'id_distributor' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
				'ppn' => $this->Anti_sql_injection($this->input->post('ppn', TRUE)),
				'term_of_patyment' => $terms,
				'int_flo' => $this->Anti_sql_injection($this->input->post('int_flo', TRUE)),
				'keterangan' => $this->Anti_sql_injection($this->input->post('ket', TRUE)),
				'total_amount' => $total_amounts
			);

			//print_r($data);die;


			$add_prin_result = $this->purchase_order_model->add_po($data);

			foreach ($array_items as $array_itemss) {

				if ($array_itemss['kode'] == "") {
				} else {

					$datas = array(

						'id_po' => $add_prin_result['lastid'],
						'id_material' => $array_itemss['kode'],
						'unit_price' => $array_itemss['harga'],
						'price' => $array_itemss['harga'] * $array_itemss['qty'],
						'qty' => $array_itemss['qty'],
						//'remark' => $array_itemss['remark'],
						'remark' => "",
						'diskon' => $array_itemss['diskon']

					);

					$prin_result = $this->purchase_order_model->add_po_mat($datas);
				}
			}



			//print_r($add_prin_result);die;

			if ($prin_result['result'] > 0) {


				$result_g = $this->purchase_order_model->update_seq('po');
				//$new_seq = $result_g[0]['max']+1;
				$result_g = $this->purchase_order_model->get_kode();
				if ($result_g[0]['max'] + 1 < 1000) {
					if ($result_g[0]['max'] + 1 < 100) {
						if ($result_g[0]['max'] + 1 < 10) {
							$kode = 'R' . date('Y') . '000' . ($result_g[0]['max'] + 1);
						} else {
							$kode = 'R' . date('Y') . '00' . ($result_g[0]['max'] + 1);
						}
					} else {
						$kode = 'R' . date('Y') . '0' . ($result_g[0]['max'] + 1);
					}
				} else {
					$kode = 'R' . date('Y') . '' . ($result_g[0]['max'] + 1);
				}

				$msg = 'Berhasil Menambah data Purchase Order';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => true, 'message' => $msg, 'new_kode' => $kode);
			} else {
				$msg = 'Gagal Menambah data Purchase Order';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => false, 'message' => $msg);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);

		//echo $kode;die;

	}

	public function edit_po()
	{

		// $this->form_validation->set_rules('kode', 'Kode Principal', 'required|min_length[4]|max_length[10]');
		// $this->form_validation->set_rules('alamat', 'Alamat Principal', 'required');
		$this->form_validation->set_rules('tgl', 'Nama Principal', 'required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();

			//echo $pesan;die;

			//$msg = strip_tags(str_replace("\r", '', $pesan));
			$msg = $pesan;

			$result = array('success' => false, 'message' => $msg);
		} else {

			$int_val = $this->Anti_sql_injection($this->input->post('int_flo', TRUE));

			$array_items = [];

			$total_amount = 0;
			for ($i = 0; $i <= $int_val; $i++) {

				//if(isset($this->input->post('kode_'.$i, TRUE))){
				$array_items[$i]['kode'] = $this->Anti_sql_injection($this->input->post('kode_' . $i, TRUE));
				$array_items[$i]['harga'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE)))));
				$array_items[$i]['diskon'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('diskon_' . $i, TRUE)))));
				$array_items[$i]['total'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('total_' . $i, TRUE)))));
				//$array_items[$i]['remark'] = $this->Anti_sql_injection($this->input->post('remark_'.$i, TRUE));
				$array_items[$i]['remark'] = "";
				$array_items[$i]['qty'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('qty_' . $i, TRUE)))));
				//}

				//$total_amount = $total_amount + ( (floatval($array_items[$i]['harga'])*floatval($array_items[$i]['qty'])) - ((floatval($array_items[$i]['harga'])*floatval($array_items[$i]['qty']))*(floatval($array_items[$i]['diskon'])/100)) );
				//$total_amount = $total_amount + ( ($array_items[$i]['harga']*$array_items[$i]['qty']) - (($array_items[$i]['harga']*($array_items[$i]['diskon']/100))*$array_items[$i]['qty']) ) ;
				$total_amount = $total_amount + ((($array_items[$i]['harga']) - (($array_items[$i]['harga'] * ($array_items[$i]['diskon'] / 100)))) * $array_items[$i]['qty']);
			}

			//print_r($array_items);die;

			$total_amounts = $total_amount + ($total_amount * (floatval($this->Anti_sql_injection($this->input->post('ppn', TRUE))) / 100));

			$result_g = $this->purchase_order_model->get_kode();
			if ($result_g[0]['max'] + 1 < 1000) {
				if ($result_g[0]['max'] + 1 < 100) {
					if ($result_g[0]['max'] + 1 < 10) {
						$kode = 'R' . date('Y') . '000' . ($result_g[0]['max'] + 1);
					} else {
						$kode = 'R' . date('Y') . '00' . ($result_g[0]['max'] + 1);
					}
				} else {
					$kode = 'R' . date('Y') . '0' . ($result_g[0]['max'] + 1);
				}
			} else {
				$kode = 'R' . date('Y') . '' . ($result_g[0]['max'] + 1);
			}

			$new_seq = $result_g[0]['max'] + 1;

			if ($this->Anti_sql_injection($this->input->post('term', TRUE)) == 'other') {
				$terms = $this->Anti_sql_injection($this->input->post('term_o', TRUE));
			} else {
				$terms = $this->Anti_sql_injection($this->input->post('term', TRUE));
			}

			$data = array(
				'id_po' => $this->Anti_sql_injection($this->input->post('id', TRUE)),
				//'no_po' => $this->Anti_sql_injection($this->input->post('kode', TRUE)),
				'no_po' => $kode,
				'date_po' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
				'ints' => $this->Anti_sql_injection($this->input->post('ints', TRUE)),
				'ints2' => $this->Anti_sql_injection($this->input->post('ints2', TRUE)),
				'id_distributor' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
				'keterangan' => $this->Anti_sql_injection($this->input->post('ket', TRUE)),
				'ppn' => $this->Anti_sql_injection($this->input->post('ppn', TRUE)),
				'term_of_patyment' => $terms,
				'int_flo' => $this->Anti_sql_injection($this->input->post('int_flo', TRUE)),
				'total_amount' => $total_amounts,
				'seq_n' => $new_seq
			);


			$add_prin_result = $this->purchase_order_model->edit_po($data);

			$this->purchase_order_model->delete_po_mat($this->Anti_sql_injection($this->input->post('id', TRUE)));

			foreach ($array_items as $array_itemss) {

				if ($array_itemss['kode'] == "") {
				} else {

					$datas = array(

						'id_po' => $this->Anti_sql_injection($this->input->post('id', TRUE)),
						'id_material' => $array_itemss['kode'],
						'unit_price' => $array_itemss['harga'],
						'price' => $array_itemss['harga'] * $array_itemss['qty'],
						'qty' => $array_itemss['qty'],
						//'remark' => $array_itemss['remark'],
						'remark' => "",
						'diskon' => $array_itemss['diskon']

					);

					$prin_result = $this->purchase_order_model->add_po_mat($datas);
				}
			}



			//print_r($add_prin_result);die;

			if ($add_prin_result['result'] > 0) {





				$msg = 'Berhasil Merubah data Purchase Order';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => true, 'message' => $msg);
			} else {
				$msg = 'Gagal Menambah data Purchase Order';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => false, 'message' => $msg);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);

		//echo $kode;die;

	}

	public function delete_po()
	{
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		//print_r($params);die;

		$result_dist 		= $this->purchase_order_model->delete_po($params['id']);
		$result_dist2 		= $this->purchase_order_model->delete_po_mat($params['id']);

		$msg = 'Berhasil menghapus data Purchase Order.';
		$result = array('success' => true, 'message' => $msg);
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function print_pdf()
	{

		$data   	= file_get_contents("php://input");
		//$params     = json_decode($data,true);


		//echo "aaa";
		$params = (explode('=', $data));

		$data = array(
			'id' => $params[1],

		);

		//print_r($data);die;

		$po_result = $this->purchase_order_model->get_po($data);
		$po_detail_result = $this->purchase_order_model->get_po_detail_full($data);
		$result = $this->purchase_order_model->get_principal();



		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);

		$array_items = [];

		$total_sub = 0;
		$total_sub_all = 0;
		$total_disk = 0;
		$grand_total = 0;

		$htmls = '';
		foreach ($po_detail_result as $po_detail_results) {

			$amn = number_format(($po_detail_results['price'] - ($po_detail_results['price'] * ($po_detail_results['diskon'] / 100))), 0, ',', '');
			$htmls = $htmls . '
			<tr style="border-top: 1px solid black;" >
								<td  align="left" style="border-top: 1px solid black;"><p>' . $po_detail_results['stock_code'] . '</p></td>
								<td  align="left" style="border-top: 1px solid black;" ><p>' . $po_detail_results['stock_name'] . ' '.number_format($po_detail_results['base_qty'], '2', ',', '.') . ' ' . $po_detail_results['uom_symbol'].'</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['qty_order'], '2', ',', '.') . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['unit_price'], '0', ',', '.') . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['diskon'], '2', ',', '.') . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($amn, '0', ',', '.') . '</p></td>

							</tr>
			';

			//$total_sub = $total_sub + number_format(($po_detail_results['price']-($po_detail_results['price']*($po_detail_results['diskon']/100))),0,',',''); 
			$total_sub = $total_sub + number_format(($po_detail_results['price'] - ($po_detail_results['price'] * ($po_detail_results['diskon'] / 100))), 0, ',', '');
			$total_sub_all = $total_sub_all + number_format(($po_detail_results['price']), 0, ',', '');
			$total_disk = $total_disk + ($po_detail_results['price'] * ($po_detail_results['diskon'] / 100));
			//$grand_total = $grand_total +  number_format(($po_detail_results['price']),0,',',''); 

		}

		$total_ppn = $total_sub * (floatval($po_result[0]['ppn'])) / 100;
		$grand_total = $total_ppn + $total_sub;

		//print_r($htmls);die;

		$item = $this->purchase_order_model->get_item_byprin($params);

		$rtext = '<br><p style="font-size: 10px;">PT. ENDRIA ALDA</p> ';

		$ctext = '<br><p style="font-size: 12px"> PURCHASE ORDER</p> ';

		//$this->load->library('Pdft');

		$pdf = new Pdf('P', 'mm', 'A4', false, 'UTF-8', false);
		$pdf->SetTitle('Purchase Order');
		$pdf->SetHeaderMargin(25);
		$pdf->setPrintHeader(false);
		$pdf->SetTopMargin(6);
		$pdf->setFooterMargin(5);
		$pdf->SetAutoPageBreak(true);
		$pdf->SetAuthor('Author');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->SetPrintFooter(false);

		$pdf->AddPage();


		$html = '
		
		<style>
		
		  .floatedTable {
            float:left;
        }
        .inlineTable {
            display: inline-block;
        }
		p {
							font-size: 10px;
							
						}
		p {
							font-size: 10px;
							
						}
		</style>
  <table style=" float:left;border: none;" >
						<tr style="border: none;">
							<th  align="left" ><h5>PT.ENDIRA ALDA</h5></th>
							<th  align="RIGHT"></th>
						</tr>
						<tr style="border: none;">
							<th  align="left" ><h4>PURCHASE ORDER</h4></th>
							<th  align="RIGHT"><p>NOMOR, TGL : ' . $po_result[0]['no_po'] . '   ' . $po_result[0]['date_po'] . '</p></th>
						</tr>
					  </table><br><br><br>
					  
					<table>
					  <tr>
						<td>

						  <table cellspacing="0" cellpadding="0" border="0" style="float:right;width:100%">
							<tr>
							  <td style="width:18%;"><p>Kepada</p></td>
							  <td><p>: ' . $po_result[0]['name_eksternal'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Alamat</p></td>
							  <td><p>: ' . $po_result[0]['eksternal_address'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Att</p></td>
							  <td><p>: ' . $po_result[0]['pic'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Telp</p></td>
							  <td><p>: ' . $po_result[0]['phone_1'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Fax</p></td>
							  <td><p>: ' . $po_result[0]['fax'] . '</p></td>
							</tr>

						  </table>
						</td>
						<td>
						  <table cellspacing="0" cellpadding="0" border="0" style="float:right;width:100%">
							<tr>
							  <td style="width:18%;"><p>Ship To</p></td>
							  <td ><p style="font-size:9px">:JL.SANGKURIANG NO 38-A<BR>NPWP:01.555.161.7.428.000</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Email</p></td>
							  <td><p>: </p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Telp/Fax</p></td>
							  <td><p>: (022)6626-946</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Delivery</p></td>
							  <td><p>: </p></td>
							</tr>
						

						  </table>
						</td>
					  </tr>
					</table>
					<br><br><br>

					
					  <table style=" float:left;border-top: 1px solid black;border-bottom: 1px solid black;" >
						<tr style=" border-top: 1px solid black;font-size: 10px;">
							<th width="50px" align="left" >Code</th>
							<th width="150px" align="CENTER">Description</th>
							<th width="60px" align="RIGHT">Quantity<br>Lt/Kg</th>
							<th  align="RIGHT">Unit Price</th>
							<th  align="RIGHT">Disc</th>
							<th  align="RIGHT">Amount</th>
						</tr>
						' . $htmls . '
					  </table>
					  	<br><br>
					  
					    <table style=" float:left;border-top: 1px solid black;" >
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Sub Total :</th>
							<th  align="RIGHT">' . number_format($total_sub_all, 0, ',', '.') . '</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Discount :</th>
							<th  align="RIGHT">' . number_format($total_disk, 0, ',', '.') . '</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;"> 
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"> Taxable :</th>
							<th  align="RIGHT">' . number_format($total_sub, 0, ',', '.') . '</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Vat/PPN :</th>
							<th  align="RIGHT">' . number_format($total_ppn, 0, ',', '.') . '</th>
						</tr>
					  </table>
					  
					  <table style=" float:left;border-top: 1px solid black;" >
						<tr style=" border-top: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Total :</th>
							<th  align="RIGHT">' . number_format($grand_total, 0, ',', '.') . '</th>
						</tr>
					  </table>
					  
					   <table style="font-size: 10px;" style="width:60%" >
					   
						<tr >
							<td  align="LEFT" style="font-size: 10px;">Keterangan : </td>
						</tr>
						<tr >
							<td  align="LEFT" style="font-size: 10px;"> '.$po_result[0]['keterangan'].' </td>
						</tr>
					   
					   </table>
					 
					<br><br>
 					
					 <table style="font-size: 10px;" >
						<tr style="">
							<th  align="CENTER" style="width:25%" ></th>
							<th  align="CENTER" style="width:12%" ></th>
							<th  align="CENTER" style="width:25%"></th>
							<th  align="CENTER" style="width:13%" ></th>
							<th  align="CENTER" style="width:25%">Cimahi, ' . date('m/d/Y') . '</th>
						</tr>
						<tr style="">
							<th  align="CENTER" style="width:25%" >Disetujui Oleh,</th>
							<th  align="CENTER" style="width:12%" ></th>
							<th  align="CENTER" style="width:25%">Diperiksa Oleh,</th>
							<th  align="CENTER" style="width:13%" ></th>
							<th  align="CENTER" style="width:25%">Dipesan Oleh,</th>
						</tr>
					  </table>
					  <br><br><br><br><br> 
					  <table style="font-size: 10px;" >
						<tr style="">
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Ratna S. Iskandar Dinata</span></th>
							<th  align="CENTER" style="width:12%"></th>
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Rienaldy Aryanto</span></th>
							<th  align="CENTER" style="width:13%"></th>
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Rangga Dean</span></th>
						</tr>
					  </table>
';

		//echo $html;die;

		// $html = <<<EOD
		// <h5>PT.ENDIRA ALDA</h5>
		// <table style=" float:left;border: none;" >
		// <tr style="border: none;">
		// <th  align="left" ><h4>PURCHASE ORDER</h4></th>
		// <th  align="RIGHT">NOMOR, TGL : ".."</th>
		// </tr>
		// </table>

		// EOD;

		// Print text using writeHTMLCell()
		$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);



		$pdf->SetPrintFooter(false);

		$pdf->lastPage();

		// $pdf->writeHTMLCell(85, 5, '<p>'.$kategoriby.' : '.$kategori.'<br>Periode : '.$start_date.' / '.$end_date.'</p> ', 1, 'L', 0, 0, '', '', true);
		// $pdf->writeHTMLCell(85, 5, $img, 1, 'R', 0, 1, '', '', true);
		// $pdf->writeHTMLCell(180, 5, '[CENTER] ', 1, 'C', 0, 1, '', '', true);
		// $pdf->writeHTMLCell(85, 5, '[LEFT] ', 1, 'L', 0, 0, '', '', true);
		// $pdf->writeHTMLCell(85, 5, '[RIGHT] ', 1, 'L', 0, 1, '', '', true);

		// $pdf->Output('/var/www/html/tmp_doc/Report_Postbuy.pdf', 'F');

		// $this->output->set_content_type('application/json')->set_output(json_encode('aaa'));

		$pdf->Output('Purchase_order.pdf', 'I');


		// if ( $list ) {			
		// $this->output->set_content_type('application/json')->set_output(json_encode($list));
		// } else {
		// $result = array( 'Value not found!' );
		// $this->output->set_content_type('application/json')->set_output(json_encode($list));
		// }

	}
}
