<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report_dasar_penjualan extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('report_dasar_penjualan/return_model');
		$this->load->library('log_activity');

		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong');
		$this->form_validation->set_message('min_length', '%s Minimal 4 Karakter');
		$this->form_validation->set_message('max_length', '%s Maksimal 10 Karakter');
	}

	/**
	 * anti sql injection
	 * @return string
	 */
	public function Anti_sql_injection($string)
	{
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	 * This function is redirect to index distributor page
	 * @return Void
	 */
	public function index()
	{

		$params['region'] = 4;

		$result['result'] = $this->return_model->get_principal($params);

		//print();die;

		$this->template->load('maintemplate', 'report_dasar_penjualan/views/index', $result);
	}


	public function export_excel()
	{



		$data = array(
			'principal' => $this->Anti_sql_injection($this->input->post('name')),
			'tgl_awal' => $this->Anti_sql_injection($this->input->post('tgl_awal')),
			'tgl_akhir' => $this->Anti_sql_injection($this->input->post('tgl_akhir')),

		);

		if ($data['principal'] == '0') {
			$result = $this->return_model->get_report_penjualan_export($data);
			$prin = "Semua Principal";
		} else {
			$result = $this->return_model->get_report_penjualan_prin($data);
			$prin = "";
		}



		//print_r($result);die;

		$this->load->library('excel');

		$objPHPExcel = new PHPExcel();

		// $objPHPExcel->setActiveSheetIndex(0)
		// ->setCellValue('A1', 'TAHUN')
		// ->setCellValue('A2', 'BULAN')
		// ->setCellValue('A3', 'PRINCIPLE')
		// ->setCellValue('B1', '2020')
		// ->setCellValue('B2', $data['tgl_awal'].' - '.$data['tgl_akhir'])
		// ->setCellValue('B3', $prin);



		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'Tgl')
			->setCellValue('B1', 'Bulan')
			->setCellValue('C1', 'Tahun')
			->setCellValue('D1', 'Wilayah')
			->setCellValue('E1', 'Faktur')
			->setCellValue('F1', 'Kode Cust')
			->setCellValue('G1', 'Custumer')
			->setCellValue('H1', 'Kode Prod')
			->setCellValue('I1', 'Produk')
			->setCellValue('J1', 'Kemasan')
			->setCellValue('K1', 'Principle')
			->setCellValue('L1', 'Lt | Kg')
			->setCellValue('M1', 'Pcs')
			->setCellValue('N1', 'Total')
			->setCellValue('O1', 'PPN')
			->setCellValue('P1', 'Tot + PPN')
			->setCellValue('Q1', 'Jenis')
			->setCellValue('R1', 'Harga Pokok')
			->setCellValue('S1', 'lt/kg')
			->setCellValue('T1', 'Harga Pokok Per Kemasan')
			->setCellValue('U1', 'Laba/Rugi Perkemasan')
			->setCellValue('V1', 'Laba/Rugi perproduk')
			->setCellValue('W1', 'Harga Jual Per lt/kg')
			->setCellValue('X1', 'Percent')
			->setCellValue('Y1', 'Keterangan')
			->setCellValue('Z1', 'Area')
			->setCellValue('AA1', 'Minggu')
			->setCellValue('AB1', 'Tax/Non Tax')
			->setCellValue('AC1', 'HPP');

		$rt = 2;
		$ints = 1;
		foreach ($result as $results) {

			$expl = explode(' ', $results['kemasan']);
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A' . $rt, $results['date_penjualan'])
				->setCellValue('B' . $rt, $results['bulan'])
				->setCellValue('C' . $rt, $results['tahun'])
				->setCellValue('D' . $rt, $results['region'])
				->setCellValue('E' . $rt, $results['no_invoice'])
				->setCellValue('F' . $rt, $results['code_cust'])
				->setCellValue('G' . $rt, $results['cust_name'])
				->setCellValue('H' . $rt, $results['kode_produk'])
				->setCellValue('I' . $rt, $results['produk'])
				->setCellValue('J' . $rt, $results['kemasan'])
				->setCellValue('K' . $rt, $results['name_eksternal'])
				->setCellValue('L' . $rt, number_format($results['Sum_of_ltkg'], 2, '.', ''))
				->setCellValue('M' . $rt, number_format($results['Sum_of_pcs'], 2, '.', ''))
				->setCellValue('N' . $rt, number_format($results['total'], 2, '.', ''))
				->setCellValue('O' . $rt, number_format($results['ppn'], 2, '.', ''))
				->setCellValue('P' . $rt, number_format($results['tot_ppn'], 2, '.', ''))
				->setCellValue('Q' . $rt, $results['type_material_name'])
				->setCellValue('R' . $rt, number_format($results['harga_pokok'], 2, '.', ''))
				->setCellValue('S' . $rt, $expl[1])
				->setCellValue('T' . $rt, number_format($results['harga_kemasan'], 2, '.', ''))
				->setCellValue('U' . $rt, number_format($results['laba_rugi_kemasan'], 2, '.', ''))
				->setCellValue('V' . $rt, number_format($results['laba_rugi_produk'], 2, '.', ''))
				->setCellValue('W' . $rt, number_format($results['harga_jual_produk'], 2, '.', ''))
				->setCellValue('X' . $rt, number_format($results['persen'], 2, '.', ''))
				->setCellValue('Y' . $rt, '')
				->setCellValue('Z' . $rt, $results['city'])
				->setCellValue('AA' . $rt, $results['minggu'])
				->setCellValue('AB' . $rt, "TAX")
				->setCellValue('AC' . $rt, number_format($results['hpp'], 2, '.', ''));

			$rt++;
			$ints++;
		};
		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename=Report_dasar_Penjualan.xls');
		header('Cache-Control: max-age=0');
		// If you’re serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you’re serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		unset($objPHPExcel);
	}

	public function reports()
	{

		if (!empty($_GET['sess_user_id'])) {
			$sess_user_id = $_GET['sess_user_id'];
		} else {
			$sess_user_id = NULL;
		}

		if (!empty($_GET['sess_token'])) {
			$sess_token = $_GET['sess_token'];
		} else {
			$sess_token = NULL;
		}

		if (!empty($_GET['name'])) {
			$principal = $_GET['name'];
		} else {
			$principal = NULL;
		}

		if (!empty($_GET['tgl_awal'])) {
			$tgl_awal = $_GET['tgl_awal'];
		} else {
			$tgl_awal = NULL;
		}

		if (!empty($_GET['tgl_akhir'])) {
			$tgl_akhir = $_GET['tgl_akhir'];
		} else {
			$tgl_akhir = NULL;
		}

		if ($this->input->get_post('draw') != FALSE) {
			$draw   = $this->input->get_post('draw');
		} else {
			$draw   = 1;
		};
		if ($this->input->get_post('length') != FALSE) {
			$length = $this->input->get_post('length');
		} else {
			$length = 10;
		};
		if ($this->input->get_post('start') != FALSE) {
			$start  = $this->input->get_post('start');
		} else {
			$start  = 0;
		};
		$order_fields = array('Rangking'); // , 'COST'
		$order = $this->input->get_post('order');
		if (!empty($order[0]['dir'])) {
			$order_dir    = $order[0]['dir'];
		} else {
			$order_dir    = 'desc';
		};
		if (!empty($order[0]['column'])) {
			$order_column = $order[0]['column'];
		} else {
			$order_column = 0;
		};


		//echo $principal;die;

		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['sess_user_id'] = $sess_user_id;
		$params['sess_token'] 	= $sess_token;
		$params['principal'] 	= $_GET['name'];
		$params['tgl_awal'] 	= $tgl_awal;
		$params['tgl_akhir'] 	= $tgl_akhir;
		$params['searchtxt'] 	= $_GET['search']['value'];

		//print_r($params);die;

		// $params = array(
		// 'principal' => $this->Anti_sql_injection($this->input->post('name')),
		// 'tgl_awal' => $this->Anti_sql_injection($this->input->post('tgl_awal')),
		// 'tgl_akhir' => $this->Anti_sql_injection($this->input->post('tgl_akhir')),

		// );


		$list = $this->return_model->get_report_penjualan($params);
		$prin = "Semua Principal";


		//print_r($list);die;

		$data = array();
		foreach ($list['data'] as $k => $v) {

			array_push(
				$data,
				array(
					$v['tanggal_invoice'],
					$v['Kode_produk'] . " " . $v['Produk'] . " " . $v['kemasan'],
					$v['cust_name'],
					$v['no_invoice'],
					number_format($v['Sum_of_ltkg'], 0, ',', '.'),
					number_format($v['Sum_of_pcs'], 0, ',', '.'),
					number_format($v['total'], 0, ',', '.'),
					number_format($v['harga_pokok'], 0, ',', '.'),
					number_format($v['HargaKemasan'], 0, ',', '.'),
					number_format($v['laba_rugi_kemasan'], 0, ',', '.'),
					number_format($v['laba_rugi_produk'], 0, ',', '.')
				)
			);
			//$idx++;
		}

		$result["data"] = $data;
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		echo json_encode($result);
		//echo json_encode($result);
		//var_dump($result);die;

	}
	/**
	 * This function is used for showing distributor list
	 * @return Array
	 */
	function lists()
	{

		if (!empty($_GET['sess_user_id'])) {
			$sess_user_id = $_GET['sess_user_id'];
		} else {
			$sess_user_id = NULL;
		}

		if (!empty($_GET['sess_token'])) {
			$sess_token = $_GET['sess_token'];
		} else {
			$sess_token = NULL;
		}

		if ($this->input->get_post('draw') != FALSE) {
			$draw   = $this->input->get_post('draw');
		} else {
			$draw   = 1;
		};
		if ($this->input->get_post('length') != FALSE) {
			$length = $this->input->get_post('length');
		} else {
			$length = 10;
		};
		if ($this->input->get_post('start') != FALSE) {
			$start  = $this->input->get_post('start');
		} else {
			$start  = 0;
		};
		$order_fields = array('kode_eksternal'); // , 'COST'
		$order = $this->input->get_post('order');
		if (!empty($order[0]['dir'])) {
			$order_dir    = $order[0]['dir'];
		} else {
			$order_dir    = 'desc';
		};
		if (!empty($order[0]['column'])) {
			$order_column = $order[0]['column'];
		} else {
			$order_column = 0;
		};

		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['sess_user_id'] = $sess_user_id;
		$params['sess_token'] 	= $sess_token;
		$params['searchtxt'] 	= $_GET['search']['value'];

		// print_r($params);die;

		// $list = $this->return_model->list_penjualan($params); 
		// //print_r($list['data']);die;
		// $data = array();	
		// foreach ( $list['data'] as $k => $v ) {

		// if($v['status_retur']==0){
		// $sts = '<button type="button" class="btn btn-custon-rounded-two btn-danger" onClick="konfirmasi('. $v['id_retur'].')"  > Konfirmasi </button>';
		// $action = '<button type="button" class="btn btn-custon-rounded-two btn-default" onClick="updatepo('. $v['id_retur'].')" > Edit </button>
		// <button type="button" class="btn btn-custon-rounded-two btn-danger" onClick="deletepo('. $v['id_retur'].')" > Hapus </button>
		// <button type="button" class="btn btn-custon-rounded-two btn-primary" onClick="detail('. $v['id_retur'].')" > Detail </button>
		// ';
		// }else{
		// $sts = '<button type="button" class="btn btn-custon-rounded-two btn-primary" disabled > Disetujui </button>';
		// $action = '<button type="button" class="btn btn-custon-rounded-two btn-primary" onClick="detail('. $v['id_retur'].')" > Detail </button>
		// ';
		// }

		// if($v['term_of_payment'] == '0'){
		// $hhh = 'Cash';
		// }else{
		// $hhh =  $v['term_of_payment'].' Hari';
		// }

		// $date=date_create($v['date_penjualan']);
		// $date_due = date_add($date,date_interval_create_from_date_string($v['term_of_payment']." days"));
		// $due_date = date_format($date_due,"Y-m-d");;


		// array_push($data, 
		// array(
		// number_format($v['Rangking'],0,',','.'),
		// $v['no_penjualan'],
		// $v['cust_name'],
		// $v['date_penjualan'],
		// $v['pic'],
		// $due_date,
		// $sts,
		// $action
		// )
		// );
		// //$idx++;
		// }

		$data = array();

		$result["data"] = $data;
		$result["recordsTotal"] = 0;
		$result["recordsFiltered"] = 0;
		$result["draw"] = 0;
		//$result["data"] = $list['data'];

		echo json_encode($result);

		//$this->json_result($result);

	}

	/**
	 * This function is redirect to add distributor page
	 * @return Void
	 */

	public function get_customer()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$result['customer'] = $this->return_model->get_customer_byid($params);

		//print_r($result['customer']);die;
		$item = $this->return_model->get_item_byprin_all();

		//print_r($item);die;

		$list = '<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		foreach ($item as $items) {


			$list .= '<option value="' . $items['id_mat'] . '|' . $items['unit_box'] . '|' . $items['bottom_price'] . '|' . $items['qty'] . '" >' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '</option>';
		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function get_penjualan_detail()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$result['penjualan'] = $this->return_model->get_penjualan_byid($params);

		//print_r($result['penjualan']);die;

		//print_r($result['customer']);die;
		$item = $this->return_model->get_penjualan_detail($params);



		$htl = '';
		$ii = 0;
		foreach ($item as $items) {

			$htl .=	'<div id="row_' . $ii . '" >';
			$htl .=	'									<div class="row" style="border-top-style:solid;">';
			$htl .=	'									<div style="margin-top:10px">';
			$htl .=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
			$htl .=	'												<input name="kode_v' . $ii . '" id="kode_v' . $ii . '" type="text" class="form-control " placeholder="Qty" value="' . $items['stock_name'] . ' ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '"  readOnly >';
			$htl .=	'												<input name="kode_' . $ii . '" id="kode_' . $ii . '" type="hidden" class="form-control " placeholder="Qty" value="' . $items['id_material'] . '|' . $items['box_ammount'] . '|' . $items['unit_price'] . '|' . $items['qty'] . '" readOnly  >';
			$htl .=	'												<input name="id_d_' . $ii . '" id="id_d_' . $ii . '" type="hidden" class="form-control " placeholder="Qty" value="' . $items['id_dp'] . '" readOnly  >';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_' . $ii . '" id="qty_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Qty" value=0  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<label id="iteml_' . $ii . '">Box @ ' . $items['box_ammount'] . '</label>';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qtys_' . $ii . '" id="qtys_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Qty" value=' . number_format($items['qty_satuan'], 0, ',', '.') . '  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_retur_' . $ii . '" id="qty_retur_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Qty" value=' . number_format($items['qty_satuan'], 0, ',', '.') . ' readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="totalqty_' . $ii . '" id="totalqty_' . $ii . '" type="number"  class="form-control" placeholder="Diskon" value=' . number_format($items['qty'], 0, ',', '.') . ' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="harga_' . $ii . '" id="harga_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Harga" value=' . number_format($items['unit_price'], 0, ',', '.') . ' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="total_' . $ii . '" id="total_' . $ii . '" type="text" class="form-control rupiah" placeholder="Total" value=0 readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'									</div>';
			$htl .=	'									</div>';
			$htl .=	'								</div>';

			//$list .= '<option value="'.$items['id_mat'].'|'.$items['unit_box'].'|'.$items['bottom_price'].'|'.$items['qty'].'" >'.$items['stock_code'].' - '.$items['stock_name'].' - '.$items['base_qty'].' '.$items['uom_symbol'].'</option>';
			$ii++;
		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';

		$result['list'] = $htl;
		$result['int'] = $ii;
		//$result['total_amount'] = number_format(floatval($result['penjualan'][0]['total_amount']),0,',','.');
		//$result['ppn'] = number_format(floatval($result['penjualan'][0]['total_amount'])/11,2,',','.');
		//$result['dpp'] = number_format(floatval($result['penjualan'][0]['total_amount'])-floatval($result['ppn']),2,',','.');


		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function get_all_item()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$item = $this->return_model->get_item_byprin($params);

		$list = '<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		foreach ($item as $items) {

			$list .= '<option value="' . $items['id_mat'] . '" >' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '</option>';
		}
		$list .= '<option value="0" selected="selected" disabled>-- Pilih Item Lainnya --</option>';

		$item2 = $this->return_model->get_item_byprin_all($params);

		foreach ($item2 as $items) {

			$list .= '<option value="' . $items['id_mat'] . '" >' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '</option>';
		}
		//	$list .= '------------------';

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}


	public function get_price_mat()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		//print_r($params);die;

		$ddd = $this->return_model->get_price_mat($params);
		//$item = $this->return_model->get_item_byprin($params);

		if (count($ddd) == 0) {
			$list = 0;
		} else {
			$list = number_format($ddd[0]['unit_price'], 0, ',', '.');
		}
		//print_r($ddd);die;

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add()
	{

		$params['region'] = 4;

		$result = $this->return_model->get_customer($params);
		$result_p = $this->return_model->get_penjualan_all();

		$result_g = $this->return_model->get_kode();
		if ($result_g[0]['max'] + 1 < 1000) {
			if ($result_g[0]['max'] + 1 < 100) {
				if ($result_g[0]['max'] + 1 < 10) {
					$kode = '' . date('Y') . '000' . ($result_g[0]['max'] + 1);
				} else {
					$kode = '' . date('Y') . '00' . ($result_g[0]['max'] + 1);
				}
			} else {
				$kode = '' . date('Y') . '0' . ($result_g[0]['max'] + 1);
			}
		} else {
			$kode = '' . date('Y') . '' . ($result_g[0]['max'] + 1);
		}
		//print_r($kode);die;

		$data = array(
			'principal' => $result,
			'kode' => $kode,
			'item' => $result_g,
			'result_p' => $result_p
		);

		$this->template->load('maintemplate', 'report_dasar_penjualan/views/addPo', $data);
		//$this->load->view('addPrinciple',$data);
	}


	public function updatepo()
	{
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			'id_penjualan' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),

		);

		$params['region'] = 4;

		$result = $this->return_model->get_customer($params);
		$result_p = $this->return_model->get_penjualan_all();
		$result_retur = $this->return_model->get_retur_id($data);

		//print_r($result_retur);die;

		$result_g = $this->return_model->get_kode();
		if ($result_g[0]['max'] + 1 < 1000) {
			if ($result_g[0]['max'] + 1 < 100) {
				if ($result_g[0]['max'] + 1 < 10) {
					$kode = '' . date('Y') . '000' . ($result_g[0]['max'] + 1);
				} else {
					$kode = '' . date('Y') . '00' . ($result_g[0]['max'] + 1);
				}
			} else {
				$kode = '' . date('Y') . '0' . ($result_g[0]['max'] + 1);
			}
		} else {
			$kode = '' . date('Y') . '' . ($result_g[0]['max'] + 1);
		}
		//print_r($kode);die;

		$htl = '';
		$ii = 0;

		$item = $this->return_model->get_penjualan_detail_update($data);

		//print_r($item);die;
		$total_retur = 0;
		foreach ($item as $items) {

			$htl .=	'<div id="row_' . $ii . '" >';
			$htl .=	'									<div class="row" style="border-top-style:solid;">';
			$htl .=	'									<div style="margin-top:10px">';
			$htl .=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
			$htl .=	'												<input name="kode_v' . $ii . '" id="kode_v' . $ii . '" type="text" class="form-control " placeholder="Qty" value="' . $items['stock_name'] . ' ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '"  readOnly >';
			$htl .=	'												<input name="kode_' . $ii . '" id="kode_' . $ii . '" type="hidden" class="form-control " placeholder="Qty" value="' . $items['id_material'] . '|' . $items['box_ammount'] . '|' . $items['unit_price'] . '|' . $items['qty'] . '" readOnly  >';
			$htl .=	'												<input name="id_d_' . $ii . '" id="id_d_' . $ii . '" type="hidden" class="form-control " placeholder="Qty" value="' . $items['id_dp'] . '" readOnly  >';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_' . $ii . '" id="qty_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Qty" value=' . $items['qty_box_retur'] . '  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<label id="iteml_' . $ii . '">Box @ ' . $items['box_ammount'] . '</label>';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qtys_' . $ii . '" id="qtys_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Qty" value=' . number_format($items['qty_retur_sat'], 0, ',', '.') . '  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_retur_' . $ii . '" id="qty_retur_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Qty" value=' . number_format($items['qty_retur'], 0, ',', '.') . ' readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="totalqty_' . $ii . '" id="totalqty_' . $ii . '" type="number"  class="form-control" placeholder="Diskon" value=' . number_format($items['qty'], 0, ',', '.') . ' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="harga_' . $ii . '" id="harga_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Harga" value=' . number_format($items['unit_price'], 0, ',', '.') . ' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="total_' . $ii . '" id="total_' . $ii . '" type="text" class="form-control rupiah" placeholder="Total" value=' . number_format($items['amount'], 0, ',', '.') . ' readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'									</div>';
			$htl .=	'									</div>';
			$htl .=	'								</div>';

			$total_retur += $items['amount'];
			//$list .= '<option value="'.$items['id_mat'].'|'.$items['unit_box'].'|'.$items['bottom_price'].'|'.$items['qty'].'" >'.$items['stock_code'].' - '.$items['stock_name'].' - '.$items['base_qty'].' '.$items['uom_symbol'].'</option>';
			$ii++;
		}

		$data = array(
			'principal' => $result,
			'kode' => $kode,
			'item' => $result_g,
			'retur' => $result_retur,
			'int_flo' => $ii,
			'result_p' => $result_p,
			'total_retur' => number_format($total_retur, 0, ',', '.'),
			'item_form' => $htl,
			'id_retur' => $data['id']
		);

		$this->template->load('maintemplate', 'report_dasar_penjualan/views/updatePo', $data);
		//$this->load->view('addPrinciple',$data);
	}

	public function detail()
	{
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			'id_penjualan' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),

		);

		$params['region'] = 4;

		$result = $this->return_model->get_customer($params);
		$result_p = $this->return_model->get_penjualan_all();
		$result_retur = $this->return_model->get_retur_id($data);

		//print_r($result_retur);die;

		$result_g = $this->return_model->get_kode();
		if ($result_g[0]['max'] + 1 < 1000) {
			if ($result_g[0]['max'] + 1 < 100) {
				if ($result_g[0]['max'] + 1 < 10) {
					$kode = '' . date('Y') . '000' . ($result_g[0]['max'] + 1);
				} else {
					$kode = '' . date('Y') . '00' . ($result_g[0]['max'] + 1);
				}
			} else {
				$kode = '' . date('Y') . '0' . ($result_g[0]['max'] + 1);
			}
		} else {
			$kode = '' . date('Y') . '' . ($result_g[0]['max'] + 1);
		}
		//print_r($kode);die;

		$htl = '';
		$ii = 0;

		$item = $this->return_model->get_penjualan_detail_update($data);

		//print_r($item);die;
		$total_retur = 0;
		foreach ($item as $items) {

			$htl .=	'<div id="row_' . $ii . '" >';
			$htl .=	'									<div class="row" style="border-top-style:solid;">';
			$htl .=	'									<div style="margin-top:10px">';
			$htl .=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
			$htl .=	'												<input name="kode_v' . $ii . '" id="kode_v' . $ii . '" type="text" class="form-control " placeholder="Qty" value="' . $items['stock_name'] . ' ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '"  readOnly >';
			$htl .=	'												<input name="kode_' . $ii . '" id="kode_' . $ii . '" type="hidden" class="form-control " placeholder="Qty" value="' . $items['id_material'] . '|' . $items['box_ammount'] . '|' . $items['unit_price'] . '|' . $items['qty'] . '" readOnly  >';
			$htl .=	'												<input name="id_d_' . $ii . '" id="id_d_' . $ii . '" type="hidden" class="form-control " placeholder="Qty" value="' . $items['id_dp'] . '" readOnly  >';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_' . $ii . '" id="qty_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Qty" value=' . $items['qty_box_retur'] . ' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<label id="iteml_' . $ii . '">Box @ ' . $items['box_ammount'] . '</label>';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qtys_' . $ii . '" id="qtys_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Qty" value=' . number_format($items['qty_retur_sat'], 0, ',', '.') . ' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_retur_' . $ii . '" id="qty_retur_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Qty" value=' . number_format($items['qty_retur'], 0, ',', '.') . ' readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="totalqty_' . $ii . '" id="totalqty_' . $ii . '" type="number"  class="form-control" placeholder="Diskon" value=' . number_format($items['qty'], 0, ',', '.') . ' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="harga_' . $ii . '" id="harga_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Harga" value=' . number_format($items['unit_price'], 0, ',', '.') . ' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="total_' . $ii . '" id="total_' . $ii . '" type="text" class="form-control rupiah" placeholder="Total" value=' . number_format($items['amount'], 0, ',', '.') . ' readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'									</div>';
			$htl .=	'									</div>';
			$htl .=	'								</div>';

			$total_retur += $items['amount'];
			//$list .= '<option value="'.$items['id_mat'].'|'.$items['unit_box'].'|'.$items['bottom_price'].'|'.$items['qty'].'" >'.$items['stock_code'].' - '.$items['stock_name'].' - '.$items['base_qty'].' '.$items['uom_symbol'].'</option>';
			$ii++;
		}

		$data = array(
			'principal' => $result,
			'kode' => $kode,
			'item' => $result_g,
			'retur' => $result_retur,
			'int_flo' => $ii,
			'result_p' => $result_p,
			'total_retur' => number_format($total_retur, 0, ',', '.'),
			'item_form' => $htl,
			'id_retur' => $data['id']
		);

		$this->template->load('maintemplate', 'report_dasar_penjualan/views/detailPo', $data);
		//$this->load->view('addPrinciple',$data);
	}

	public function konfirmasi()
	{
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			'id_penjualan' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),

		);

		$params['region'] = 4;

		$result = $this->return_model->get_customer($params);
		$result_p = $this->return_model->get_penjualan_all();
		$result_retur = $this->return_model->get_retur_id($data);

		//print_r($result_retur);die;

		$result_g = $this->return_model->get_kode();
		if ($result_g[0]['max'] + 1 < 1000) {
			if ($result_g[0]['max'] + 1 < 100) {
				if ($result_g[0]['max'] + 1 < 10) {
					$kode = '' . date('Y') . '000' . ($result_g[0]['max'] + 1);
				} else {
					$kode = '' . date('Y') . '00' . ($result_g[0]['max'] + 1);
				}
			} else {
				$kode = '' . date('Y') . '0' . ($result_g[0]['max'] + 1);
			}
		} else {
			$kode = '' . date('Y') . '' . ($result_g[0]['max'] + 1);
		}
		//print_r($kode);die;

		$htl = '';
		$ii = 0;

		$item = $this->return_model->get_penjualan_detail_update($data);

		//print_r($item);die;
		$total_retur = 0;
		foreach ($item as $items) {

			$htl .=	'<div id="row_' . $ii . '" >';
			$htl .=	'									<div class="row" style="border-top-style:solid;">';
			$htl .=	'									<div style="margin-top:10px">';
			$htl .=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
			$htl .=	'												<input name="kode_v' . $ii . '" id="kode_v' . $ii . '" type="text" class="form-control " placeholder="Qty" value="' . $items['stock_name'] . ' ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '"  readOnly >';
			$htl .=	'												<input name="kode_' . $ii . '" id="kode_' . $ii . '" type="hidden" class="form-control " placeholder="Qty" value="' . $items['id_material'] . '|' . $items['box_ammount'] . '|' . $items['unit_price'] . '|' . $items['qty'] . '" readOnly  >';
			$htl .=	'												<input name="id_d_' . $ii . '" id="id_d_' . $ii . '" type="hidden" class="form-control " placeholder="Qty" value="' . $items['id_dp'] . '" readOnly  >';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_' . $ii . '" id="qty_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Qty" value=' . $items['qty_box_retur'] . ' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<label id="iteml_' . $ii . '">Box @ ' . $items['box_ammount'] . '</label>';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qtys_' . $ii . '" id="qtys_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Qty" value=' . number_format($items['qty_retur_sat'], 0, ',', '.') . ' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_retur_' . $ii . '" id="qty_retur_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Qty" value=' . number_format($items['qty_retur'], 0, ',', '.') . ' readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="totalqty_' . $ii . '" id="totalqty_' . $ii . '" type="number"  class="form-control" placeholder="Diskon" value=' . number_format($items['qty'], 0, ',', '.') . ' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="harga_' . $ii . '" id="harga_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Harga" value=' . number_format($items['unit_price'], 0, ',', '.') . ' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="total_' . $ii . '" id="total_' . $ii . '" type="text" class="form-control rupiah" placeholder="Total" value=' . number_format($items['amount'], 0, ',', '.') . ' readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'									</div>';
			$htl .=	'									</div>';
			$htl .=	'								</div>';

			$total_retur += $items['amount'];
			//$list .= '<option value="'.$items['id_mat'].'|'.$items['unit_box'].'|'.$items['bottom_price'].'|'.$items['qty'].'" >'.$items['stock_code'].' - '.$items['stock_name'].' - '.$items['base_qty'].' '.$items['uom_symbol'].'</option>';
			$ii++;
		}

		$data = array(
			'principal' => $result,
			'kode' => $kode,
			'item' => $result_g,
			'retur' => $result_retur,
			'int_flo' => $ii,
			'result_p' => $result_p,
			'total_retur' => number_format($total_retur, 0, ',', '.'),
			'item_form' => $htl,
			'id_retur' => $data['id']
		);

		$this->template->load('maintemplate', 'report_dasar_penjualan/views/konfirmasiPo', $data);
		//$this->load->view('addPrinciple',$data);

	}

	public function approve_retur()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$data = array(
			'id_po' => $params['id'],
			'id_penjualan' => $params['id'],
			'sts' => $params['sts']

		);

		//print_r($data);die;

		$add_prin_result = $this->return_model->edit_retur_apr($data);

		if ($params['sts'] == 1) {
			//$this->return_model->history_retur_apr($data);

			$item = $this->return_model->get_penjualan_detail_update($data);

			foreach ($item as $items) {

				//print_r($items);die;

				$data_mutasi = array(
					'id_material'  => $items['id_material'],
					'qty'  => $items['qty_retur']

				);

				$add_mutasi_out = $this->return_model->add_mutasi_out($data_mutasi);

				$data_mutasi = array(
					'id_material'  => $items['id_material'],
					'amount_mutasi'  => $items['qty_retur'],
					'id_dp'  => $items['id_dp'],
					'qty_retur'  => floatval($items['qty']) - floatval($items['qty_retur']),
					'qty_box_retur'  => floatval($items['qty_box']) - floatval($items['qty_box_retur']),
					'qty_retur_sat'  => floatval($items['qty_satuan']) - floatval($items['qty_retur_sat']),
					'amount'  => floatval($items['price']) - floatval($items['amount']),
					'mutasi_id'  => $add_mutasi_out['lastid']

				);

				$this->return_model->edit_detail_penjualan($data_mutasi);
			}
		}

		if ($add_prin_result['result'] > 0) {

			$msg = 'Berhasil Menambah data Retur';

			$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
			$result = array('success' => true, 'message' => $msg);
		} else {
			$msg = 'Gagal Menambah data Retur';

			$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
			$result = array('success' => false, 'message' => $msg);
		}
	}

	public function add_po()
	{

		$this->form_validation->set_rules('name', 'nama Principal', 'required|min_length[1]|max_length[10]');
		// $this->form_validation->set_rules('alamat', 'Alamat Principal', 'required');
		//$this->form_validation->set_rules('tgl', 'Tanggal Tidak Boleh Kosong', 'required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();

			//echo $pesan;die;

			//$msg = strip_tags(str_replace("\r", '', $pesan));
			$msg = $pesan;

			$result = array('success' => false, 'message' => $msg);
		} else {

			$int_val = $this->Anti_sql_injection($this->input->post('int_flo', TRUE));

			$array_items = [];

			$total_amount = 0;
			for ($i = 0; $i <= $int_val; $i++) {


				$kode_split = explode('|', $this->Anti_sql_injection($this->input->post('kode_' . $i, TRUE)));
				if ($kode_split[0]) {
					$array_items[$i]['kode'] = $kode_split[0];
					$array_items[$i]['qty_stock'] = $kode_split[3];
					//$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE));
					$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE));
					$array_items[$i]['qty'] = $this->Anti_sql_injection($this->input->post('qty_' . $i, TRUE));
					$array_items[$i]['qtys'] = $this->Anti_sql_injection($this->input->post('qtys_' . $i, TRUE));
					$array_items[$i]['id_d'] = $this->Anti_sql_injection($this->input->post('id_d_' . $i, TRUE));
					$array_items[$i]['qty_box'] = $kode_split[1];
					$array_items[$i]['total'] = ((intval(str_replace('.', '', $array_items[$i]['qty'])) + intval(str_replace('.', '', $array_items[$i]['qtys']))) * intval($kode_split[1])) * floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE)))));
					//	$array_items[$i]['remark'] = $this->Anti_sql_injection($this->input->post('remark_'.$i, TRUE));
					$array_items[$i]['totalqty'] = $this->Anti_sql_injection($this->input->post('totalqty_' . $i, TRUE));
					$total_amount = $total_amount + (((intval(str_replace('.', '', $array_items[$i]['qty'])) + intval(str_replace('.', '', $array_items[$i]['qtys']))) * intval($kode_split[1])) * floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE))))));
				}
			}

			//print_r($array_items);die;

			//$total_amounts = $total_amount + ($total_amount * (floatval($this->Anti_sql_injection($this->input->post('ppn', TRUE)))/100));

			$data = array(
				'id_penjualan' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
				'keterangan' => $this->Anti_sql_injection($this->input->post('ket_retur', TRUE)),
				'pic'          => $this->session->userdata['logged_in']['user_id'],
				'date' => date('Y-m-d')

			);

			//print_r($data);die;


			$add_prin_result = $this->return_model->add_returm($data);
			//$this->return_model->edit_credit($data);

			foreach ($array_items as $array_itemss) {

				if ($array_itemss['kode'] == "") {
				} else {

					$datas = array(

						'id_retur' => $add_prin_result['lastid'],
						'id_detail_mat' => $array_itemss['id_d'],
						'harga_satuan' => str_replace(',', '.', str_replace('.', '', $array_itemss['harga'])),
						'amount' => intval(str_replace('.', '', $array_itemss['harga'])) * ((intval(str_replace('.', '', $array_itemss['qty'])) + intval(str_replace('.', '', $array_itemss['qtys']))) * intval($array_itemss['qty_box'])),
						'qty' => (intval(str_replace('.', '', $array_itemss['qty'])) + intval(str_replace('.', '', $array_itemss['qtys']))) * intval($array_itemss['qty_box']),
						'qty_per_box' => intval($array_itemss['qty_box']),
						'qty_box' => intval(str_replace('.', '', $array_itemss['qty'])),
						'qty_satuan' => intval(str_replace('.', '', $array_itemss['qtys']))

						//'remark' => $array_itemss['remark'],

					);

					//$id_mutasi = $this->return_model->add_mutasi($datas);
					//$this->return_model->edit_qty($datas);



					$prin_result = $this->return_model->add_detail_retur($datas);
				}
			}



			//print_r($add_prin_result);die;

			if ($prin_result['result'] > 0) {





				$msg = 'Berhasil Menambah data Retur';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => true, 'message' => $msg);
			} else {
				$msg = 'Gagal Menambah data Retur';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => false, 'message' => $msg);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);

		//echo $kode;die;

	}

	public function edit_po()
	{

		// $this->form_validation->set_rules('kode', 'Kode Principal', 'required|min_length[4]|max_length[10]');
		// $this->form_validation->set_rules('alamat', 'Alamat Principal', 'required');
		$this->form_validation->set_rules('name', 'nama Principal', 'required|min_length[1]|max_length[10]');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();

			//echo "aasasasas";die;

			//$msg = strip_tags(str_replace("\r", '', $pesan));
			$msg = $pesan;

			$result = array('success' => false, 'message' => $msg);
		} else {

			$int_val = $this->Anti_sql_injection($this->input->post('int_flo', TRUE));
			$id_retur = $this->Anti_sql_injection($this->input->post('id_retur', TRUE));

			$array_items = [];

			$total_amount = 0;
			for ($i = 0; $i <= $int_val; $i++) {

				$kode_split = explode('|', $this->Anti_sql_injection($this->input->post('kode_' . $i, TRUE)));
				if ($kode_split[0]) {
					$array_items[$i]['kode'] = $kode_split[0];
					$array_items[$i]['qty_stock'] = $kode_split[3];
					//$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE));
					$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE));
					$array_items[$i]['qty'] = $this->Anti_sql_injection($this->input->post('qty_' . $i, TRUE));
					$array_items[$i]['qtys'] = $this->Anti_sql_injection($this->input->post('qtys_' . $i, TRUE));
					$array_items[$i]['id_d'] = $this->Anti_sql_injection($this->input->post('id_d_' . $i, TRUE));
					$array_items[$i]['qty_box'] = $kode_split[1];
					$array_items[$i]['total'] = ((intval(str_replace('.', '', $array_items[$i]['qty'])) + intval(str_replace('.', '', $array_items[$i]['qtys']))) * intval($kode_split[1])) * floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE)))));
					//	$array_items[$i]['remark'] = $this->Anti_sql_injection($this->input->post('remark_'.$i, TRUE));
					$array_items[$i]['totalqty'] = $this->Anti_sql_injection($this->input->post('totalqty_' . $i, TRUE));
					$total_amount = $total_amount + (((intval(str_replace('.', '', $array_items[$i]['qty'])) + intval(str_replace('.', '', $array_items[$i]['qtys']))) * intval($kode_split[1])) * floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE))))));
				}
			}


			$data = array(
				'id_penjualan' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
				'keterangan' => $this->Anti_sql_injection($this->input->post('ket_retur', TRUE)),
				'pic'          => $this->session->userdata['logged_in']['user_id'],
				'date' => date('Y-m-d'),
				'id_retur' => $id_retur

			);

			$add_prin_result = $this->return_model->edit_retur($data);

			$this->return_model->delete_detail_retur($id_retur);



			foreach ($array_items as $array_itemss) {

				if ($array_itemss['kode'] == "") {
				} else {

					$datas = array(

						'id_retur' => $id_retur,
						'id_detail_mat' => $array_itemss['id_d'],
						'harga_satuan' => str_replace(',', '.', str_replace('.', '', $array_itemss['harga'])),
						'amount' => intval(str_replace('.', '', $array_itemss['harga'])) * ((intval(str_replace('.', '', $array_itemss['qty'])) + intval(str_replace('.', '', $array_itemss['qtys']))) * intval($array_itemss['qty_box'])),
						'qty' => (intval(str_replace('.', '', $array_itemss['qty'])) + intval(str_replace('.', '', $array_itemss['qtys']))) * intval($array_itemss['qty_box']),
						'qty_per_box' => intval($array_itemss['qty_box']),
						'qty_box' => intval(str_replace('.', '', $array_itemss['qty'])),
						'qty_satuan' => intval(str_replace('.', '', $array_itemss['qtys']))

						//'remark' => $array_itemss['remark'],

					);

					$prin_result = $this->return_model->add_detail_retur($datas);
					//$this->return_model->update_qty_m($data_q);

				}
			}



			//print_r($add_prin_result);die;

			if ($add_prin_result['result'] > 0) {





				$msg = 'Berhasil Merubah data Retur';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => true, 'message' => $msg);
			} else {
				$msg = 'Gagal Menambah data Retur';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => false, 'message' => $msg);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);

		//echo $kode;die;

	}

	public function delete_po()
	{
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$this->return_model->delete_detail_retur($params['id']);
		$this->return_model->delete_retur($params['id']);


		//$result_dist 		= $this->return_model->delete_po($params['id']);
		//$result_dist2 		= $this->return_model->delete_po_mat($params['id']);

		$msg = 'Berhasil menghapus data Retur.';
		$result = array('success' => true, 'message' => $msg);
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function print_pdf()
	{

		$data   	= file_get_contents("php://input");
		//$params     = json_decode($data,true);


		//echo "aaa";
		$params = (explode('=', $data));

		$data = array(
			'id' => $params[1],

		);

		//print_r($data);die;

		$po_result = $this->return_model->get_po($data);
		$po_detail_result = $this->return_model->get_po_detail_full($data);
		$result = $this->return_model->get_principal();



		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);

		$array_items = [];

		$total_sub = 0;
		$total_sub_all = 0;
		$total_disk = 0;
		$grand_total = 0;

		$htmls = '';
		foreach ($po_detail_result as $po_detail_results) {

			$amn = number_format(($po_detail_results['price'] - ($po_detail_results['price'] * ($po_detail_results['diskon'] / 100))), 0, ',', '');
			$htmls = $htmls . '
			<tr style="border-top: 1px solid black;" >
								<td  align="left" style="border-top: 1px solid black;"><p>' . $po_detail_results['stock_code'] . '</p></td>
								<td  align="left" style="border-top: 1px solid black;"><p>' . $po_detail_results['stock_name'] . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['base_qty'], '2', ',', '.') . ' ' . $po_detail_results['uom_symbol'] . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['qty_order'], '2', ',', '.') . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['unit_price'], '0', ',', '.') . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['diskon'], '2', ',', '.') . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($amn, '0', ',', '.') . '</p></td>

							</tr>
			';

			//$total_sub = $total_sub + number_format(($po_detail_results['price']-($po_detail_results['price']*($po_detail_results['diskon']/100))),0,',',''); 
			$total_sub = $total_sub + number_format(($po_detail_results['price'] - ($po_detail_results['price'] * ($po_detail_results['diskon'] / 100))), 0, ',', '');
			$total_sub_all = $total_sub_all + number_format(($po_detail_results['price']), 0, ',', '');
			$total_disk = $total_disk + ($po_detail_results['price'] * ($po_detail_results['diskon'] / 100));
			//$grand_total = $grand_total +  number_format(($po_detail_results['price']),0,',',''); 

		}

		$total_ppn = $total_sub * (floatval($po_result[0]['ppn'])) / 100;
		$grand_total = $total_ppn + $total_sub;

		//print_r($htmls);die;

		$item = $this->return_model->get_item_byprin($params);

		$rtext = '<br><p style="font-size: 10px;">PT. ENDRIA ALDA</p> ';

		$ctext = '<br><p style="font-size: 12px"> PURCHASE ORDER</p> ';

		$this->load->library('Pdf');

		$pdf = new Pdf('P', 'mm', 'A4', false, 'UTF-8', false);
		$pdf->SetTitle('Purchase Order');
		$pdf->SetHeaderMargin(25);
		$pdf->setPrintHeader(false);
		$pdf->SetTopMargin(6);
		$pdf->setFooterMargin(5);
		$pdf->SetAutoPageBreak(true);
		$pdf->SetAuthor('Author');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->SetPrintFooter(false);

		$pdf->AddPage();


		$html = '
		
		<style>
		
		  .floatedTable {
            float:left;
        }
        .inlineTable {
            display: inline-block;
        }
		p {
							font-size: 8px;
							
						}
		p {
							font-size: 8px;
							
						}
		</style>
  <table style=" float:left;border: none;" >
						<tr style="border: none;">
							<th  align="left" ><h5>PT.ENDIRA ALDA</h5></th>
							<th  align="RIGHT"></th>
						</tr>
						<tr style="border: none;">
							<th  align="left" ><h4>PURCHASE ORDER</h4></th>
							<th  align="RIGHT"><p>NOMOR, TGL : ' . $po_result[0]['no_po'] . '   ' . $po_result[0]['date_po'] . '</p></th>
						</tr>
					  </table><br><br><br>
					  
					<table>
					  <tr>
						<td>

						  <table cellspacing="0" cellpadding="0" border="0" style="float:right;width:100%.">
							<tr>
							  <td style="width:18%;"><p>Kepada</p></td>
							  <td><p>: ' . $po_result[0]['name_eksternal'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Alamat</p></td>
							  <td><p>: ' . $po_result[0]['eksternal_address'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Att</p></td>
							  <td><p>: ' . $po_result[0]['pic'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Telp</p></td>
							  <td><p>: ' . $po_result[0]['phone_1'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Fax</p></td>
							  <td><p>: ' . $po_result[0]['fax'] . '</p></td>
							</tr>

						  </table>
						</td>
						<td>
						  <table cellspacing="0" cellpadding="0" border="0" style="float:right;width:100%">
							<tr>
							  <td style="width:18%;"><p>Ship To</p></td>
							  <td><p>:JL.SANGKURIANG NO 38-A<BR>NPWP:01.555.161.7.428.000</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Email</p></td>
							  <td><p>: </p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Telp/Fax</p></td>
							  <td><p>: (022)6626-946</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Delivery</p></td>
							  <td><p>: </p></td>
							</tr>
						

						  </table>
						</td>
					  </tr>
					</table>
					<br><br><br>

					
					  <table style=" float:left;border-top: 1px solid black;border-bottom: 1px solid black;" >
						<tr style=" border-top: 1px solid black;font-size: 10px;">
							<th  align="left" >Code</th>
							<th  align="RIGHT">Description</th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Quantity<br>Lt/Kg</th>
							<th  align="RIGHT">Unit Price</th>
							<th  align="RIGHT">Disc</th>
							<th  align="RIGHT">Amount</th>
						</tr>
						' . $htmls . '
					  </table>
					  	<br><br><br>
						<br><br><br>
					  
					    <table style=" float:left;border-top: 1px solid black;" >
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Sub Total :</th>
							<th  align="RIGHT">' . number_format($total_sub_all, 0, ',', '.') . '</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Discount :</th>
							<th  align="RIGHT">' . number_format($total_disk, 0, ',', '.') . '</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"> Taxable :</th>
							<th  align="RIGHT">' . number_format($total_sub, 0, ',', '.') . '</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Vat/PPN :</th>
							<th  align="RIGHT">' . number_format($total_ppn, 0, ',', '.') . '</th>
						</tr>
					  </table>
					  
					  <table style=" float:left;border-top: 1px solid black;" >
						<tr style=" border-top: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Total :</th>
							<th  align="RIGHT">' . number_format($grand_total, 0, ',', '.') . '</th>
						</tr>
					  </table>
					<br><br><br><br><br> 
					<br><br><br><br><br> 
					
					 <table style="font-size: 10px;" >
						<tr style="">
							<th  align="CENTER" style="width:25%" ></th>
							<th  align="CENTER" style="width:12%" ></th>
							<th  align="CENTER" style="width:25%"></th>
							<th  align="CENTER" style="width:13%" ></th>
							<th  align="CENTER" style="width:25%">Cimahi, ' . date('m/d/Y') . '</th>
						</tr>
						<tr style="">
							<th  align="CENTER" style="width:25%" >Disetujui Oleh,</th>
							<th  align="CENTER" style="width:12%" ></th>
							<th  align="CENTER" style="width:25%">Diperiksa Oleh,</th>
							<th  align="CENTER" style="width:13%" ></th>
							<th  align="CENTER" style="width:25%">Dipesan Oleh,</th>
						</tr>
					  </table>
					  <br><br><br><br><br> 
					  <table style="font-size: 10px;" >
						<tr style="">
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Ratna S. Iskandar Dinata</span></th>
							<th  align="CENTER" style="width:12%"></th>
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Rienaldy Aryanto</span></th>
							<th  align="CENTER" style="width:13%"></th>
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Rangga Dean</span></th>
						</tr>
					  </table>
';

		//echo $html;die;

		// $html = <<<EOD
		// <h5>PT.ENDIRA ALDA</h5>
		// <table style=" float:left;border: none;" >
		// <tr style="border: none;">
		// <th  align="left" ><h4>PURCHASE ORDER</h4></th>
		// <th  align="RIGHT">NOMOR, TGL : ".."</th>
		// </tr>
		// </table>

		// EOD;

		// Print text using writeHTMLCell()
		$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);



		$pdf->SetPrintFooter(false);

		$pdf->lastPage();

		// $pdf->writeHTMLCell(85, 5, '<p>'.$kategoriby.' : '.$kategori.'<br>Periode : '.$start_date.' / '.$end_date.'</p> ', 1, 'L', 0, 0, '', '', true);
		// $pdf->writeHTMLCell(85, 5, $img, 1, 'R', 0, 1, '', '', true);
		// $pdf->writeHTMLCell(180, 5, '[CENTER] ', 1, 'C', 0, 1, '', '', true);
		// $pdf->writeHTMLCell(85, 5, '[LEFT] ', 1, 'L', 0, 0, '', '', true);
		// $pdf->writeHTMLCell(85, 5, '[RIGHT] ', 1, 'L', 0, 1, '', '', true);

		// $pdf->Output('/var/www/html/tmp_doc/Report_Postbuy.pdf', 'F');

		// $this->output->set_content_type('application/json')->set_output(json_encode('aaa'));

		$pdf->Output('Purchase_order.pdf', 'I');


		// if ( $list ) {			
		// $this->output->set_content_type('application/json')->set_output(json_encode($list));
		// } else {
		// $result = array( 'Value not found!' );
		// $this->output->set_content_type('application/json')->set_output(json_encode($list));
		// }

	}
}
