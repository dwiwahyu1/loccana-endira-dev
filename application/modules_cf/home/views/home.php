<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<style>
    #carousel2 .carousel-caption {
        left: 0;
        right: 0;
        bottom: 0;
        text-align: left;
        padding: 10px;
        background: rgba(0, 0, 0, 0.6);
        text-shadow: none;
    }

    #carousel2 .list-group {
        position: absolute;
        top: 0;
        right: 0;
    }

    #carousel2 .list-group-item {
        border-radius: 0px;
        cursor: pointer;
    }

    #carousel2 .carousel-inner {
        background: rgba(0, 0, 0, 0.9);
    }

    #carousel2 .list-group .active {
        background-color: #eee;
    }

    @media (min-width: 992px) {
        #carousel2 {
            padding-right: 33.3333%;
        }

        #carousel2 .carousel-controls {
            display: none;
        }
    }

    @media (max-width: 991px) {

        /* .carousel-caption p, */
        #carousel2 .list-group {
            display: none;
        }
    }
</style>
<div class="analytics-sparkle-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="analytics-sparkle-line reso-mg-b-30">
                    <div class="analytics-content">
                        <h5>Total Penjualan</h5>
                        <h2>Rp <span class="counter"><?= number_format($total_penjualan[0]['total_penjualan'], 2, ',', '.'); ?></span> <span class="tuition-fees"></span></h2>

                        <div class="progress m-b-0">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;"> <span class="sr-only">20% Complete</span> </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="analytics-sparkle-line reso-mg-b-30">
                    <div class="analytics-content">
                        <h5>Total Pembelian</h5>
                        <h2>Rp <span class="counter"><?= number_format($total_pembelian[0]['total_pembelian'], 2, ',', '.'); ?></span> <span class="tuition-fees"></span></h2>

                        <div class="progress m-b-0">
                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;"> <span class="sr-only">230% Complete</span> </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="analytics-sparkle-line reso-mg-b-30 table-mg-t-pro dk-res-t-pro-30">
                    <div class="analytics-content">
                        <h5>Penjualan MTD</h5>
                        <h2>Rp <span class="counter"><?= number_format($mtd_penjualan[0]['mtd_penjualan'], 2, ',', '.'); ?></span> <span class="tuition-fees"></span></h2>

                        <div class="progress m-b-0">
                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;"> <span class="sr-only">20% Complete</span> </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                <div class="analytics-sparkle-line table-mg-t-pro dk-res-t-pro-30">
                    <div class="analytics-content">
                        <h5>Pembelian MTD</h5>
                        <h2>Rp <span class="counter"><?= number_format($mtd_pembelian[0]['mtd_pembelian'], 2, ',', '.'); ?></span> <span class="tuition-fees"></span></h2>

                        <div class="progress m-b-0">
                            <div class="progress-bar progress-bar-inverse" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;"> <span class="sr-only">230% Complete</span> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="product-sales-area mg-tb-30">
    <div class="container-fluid">
        <div class="container">
            <div id="myCarousel" class="carousel slide" data-ride="carousel" style="width: 720px;">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <?php foreach ($informasi as $key => $info) : ?>
                        <li data-target="#myCarousel" data-slide-to="<?= $key; ?>" class="<?= ($key == 0) ? "active" : ""; ?>"></li>
                    <?php endforeach; ?>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <?php foreach ($informasi as $key => $info) : ?>
                        <div class="item <?= ($key == 0) ? "active" : ""; ?> " style="height: 480px;">
                            <?php if ($info['gambar'] ?? false) : ?>
                                <img src="<?= base_url('uploads/informasi/' . $info['gambar']); ?>" style="width:100%; height: 100%; object-fit:cover; object-position:center;" alt="Image of every carousel" />
                            <?php endif; ?>
                            <div class="carousel-caption" style="right:0; left:0; width:100%; background-color:rgba(0, 0, 0, 0.4);">
                                <h3><?= $info['judul']; ?></h3>
                                <p><?= $info['keterangan']; ?></p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>

</div>