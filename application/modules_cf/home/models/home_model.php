<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Home_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		
	}
	public function total_penjualan(){
		$query 		= 	'SELECT SUM(total_amount) as total_penjualan
						FROM t_penjualan 
						WHERE YEAR(date_penjualan) = YEAR(CURRENT_DATE())';
		$query2s	= $this->db->query($query);
		$return 	= $query2s->result_array();
		return $return;		
	}
	public function total_pembelian(){
		$query 		= 	'SELECT SUM(total_amount) as total_pembelian
						FROM t_purchase_order 
						WHERE YEAR(date_po) = YEAR(CURRENT_DATE())';
		$query2s	= $this->db->query($query);
		$return 	= $query2s->result_array();
		return $return;		
	}
	public function mtd_penjualan(){
		$query 		= 	'SELECT SUM(total_amount) as mtd_penjualan
						FROM t_penjualan 
						WHERE MONTH(date_penjualan) = MONTH(CURRENT_DATE())
						AND YEAR(date_penjualan) = YEAR(CURRENT_DATE())';
		$query2s	= $this->db->query($query);
		$return 	= $query2s->result_array();
		return $return;		
	}
	public function mtd_pembelian(){
		$query 		= 	'SELECT SUM(total_amount) as mtd_pembelian
						FROM t_purchase_order 
						WHERE MONTH(date_po) = MONTH(CURRENT_DATE())
						AND YEAR(date_po) = YEAR(CURRENT_DATE())';
		$query2s	= $this->db->query($query);
		$return 	= $query2s->result_array();
		return $return;		
	}
	public function informasi(){
		$query 		= 	'SELECT * 
						FROM t_informasi
						ORDER BY id_info desc
						LIMIT 5';
		$query2s	= $this->db->query($query);
		$return 	= $query2s->result_array();
		return $return;		
	}
	
			
				
	
}	