<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('home_model');
	}

	public function index()
	{
		$result['total_penjualan'] 	= $this->home_model->total_penjualan();
		$result['total_pembelian'] 	= $this->home_model->total_pembelian();
		$result['mtd_penjualan'] 	= $this->home_model->mtd_penjualan();
		$result['mtd_pembelian'] 	= $this->home_model->mtd_pembelian();
		$result['informasi'] 		= $this->home_model->informasi();
		$this->template->load('maintemplate', 'home/views/home', $result);
	}
}
