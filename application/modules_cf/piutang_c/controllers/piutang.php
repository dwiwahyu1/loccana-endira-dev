<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Piutang extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('piutang/return_model');
		$this->load->library('log_activity');
		$this->load->library('priv');

		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong');
		$this->form_validation->set_message('min_length', '%s Minimal 4 Karakter');
		$this->form_validation->set_message('max_length', '%s Maksimal 10 Karakter');
	}

	/**
	 * anti sql injection
	 * @return string
	 */
	public function Anti_sql_injection($string)
	{
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	 * This function is redirect to index distributor page
	 * @return Void
	 */
	public function index()
	{

		$priv = $this->priv->get_priv();

		$year_count = date('Y') - 2019;

		$filter_year = '<select class="form-control" id="filter_year" onChange="changeFilter()"><option value="ALL" >ALL</option>';

		for ($i = $year_count; $i >= 0; $i--) {

			$y = 2019 + $i;

			if ($y == date('Y')) {
				$filter_year .= '<option value="' . $y . '" selected="selected" >' . $y . '</option>';
			} else {
				$filter_year .= '<option value="' . $y . '" >' . $y . '</option>';
			}
		}
		$filter_year .= '</select>';

		$array_month = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

		$filter_month = '<select class="form-control" id="filter_month" onChange="changeFilter()"><option value="ALL" >ALL</option>';
		foreach ($array_month as $array_months) {
			if ($array_months == date('F')) {
				$filter_month .= '<option value="' . $array_months . '" selected="selected">' . $array_months . '</option>';
			} else {
				$filter_month .= '<option value="' . $array_months . '" >' . $array_months . '</option>';
			}
		}

		$filter_month .= '</select>';
		$today_month = date('F');
		$today_year = date('Y');

		$data = array(
			'priv' => $priv,
			'filter_month' => $filter_month,
			'filter_year' => $filter_year,
			'today_month' => $today_month,
			'today_year' => $today_year
		);

		$this->template->load('maintemplate', 'piutang/views/index', $data);
	}

	/**
	 * This function is used for showing distributor list
	 * @return Array
	 */
	function lists()
	{

		if (!empty($_GET['sess_user_id'])) {
			$sess_user_id = $_GET['sess_user_id'];
		} else {
			$sess_user_id = NULL;
		}

		if (!empty($_GET['sess_token'])) {
			$sess_token = $_GET['sess_token'];
		} else {
			$sess_token = NULL;
		}

		if (!empty($_GET['filter'])) {
			$filter = $_GET['filter'];
		} else {
			$filter = NULL;
		}

		if (!empty($_GET['filter_month'])) {
			$filter_month = $_GET['filter_month'];
		} else {
			$filter_month = NULL;
		}

		if (!empty($_GET['filter_year'])) {
			$filter_year = $_GET['filter_year'];
		} else {
			$filter_year = NULL;
		}

		if ($this->input->get_post('draw') != FALSE) {
			$draw   = $this->input->get_post('draw');
		} else {
			$draw   = 1;
		};
		if ($this->input->get_post('length') != FALSE) {
			$length = $this->input->get_post('length');
		} else {
			$length = 10;
		};
		if ($this->input->get_post('start') != FALSE) {
			$start  = $this->input->get_post('start');
		} else {
			$start  = 0;
		};
		$order_fields = array('Rangking', 'no_invoice', 'cust_name', 'total_invoice', 'sisa', 'tanggal_invoice', 'id_invoice', 'id_invoice', 'id_invoice'); // , 'COST'
		$order = $this->input->get_post('order');
		if (!empty($order[0]['dir'])) {
			$order_dir    = $order[0]['dir'];
		} else {
			$order_dir    = 'desc';
		};
		if (!empty($order[0]['column'])) {
			$order_column = $order[0]['column'];
		} else {
			$order_column = 0;
		};

		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['sess_user_id'] = $sess_user_id;
		$params['sess_token'] 	= $sess_token;
		$params['filter'] 		= $filter;
		$params['filter_month'] 		= $_GET['filter_month'];
		$params['filter_year'] 		= $_GET['filter_year'];
		$params['searchtxt'] 	= $_GET['search']['value'];

		// print_r($params);die;

		$list = $this->return_model->list_penjualan($params);
		$priv = $this->priv->get_priv();
		//print_r($list['data']);die;
		$data = array();
		$rowNumber = $start + 1;
		foreach ($list['data'] as $k => $v) {

			if ($v['status_invoice'] == 0) {
				$sts = '<button type="button" class="btn btn-custon-rounded-two btn-danger" "  > Konfirmasi </button>';
				$action = '
						<div class="btn-group" style="display:' . $priv['detail'] . '"><button type="button" class="btn btn-custon-rounded-two btn-info" data-toggle="tooltip" data-placement="top" title="Detail"  onClick="detail(' . $v['id_invoice'] . ')" > <i class="fa fa-search-plus"></i>  </button></div>
						';
			} else {
				$sts = '<button type="button" class="btn btn-custon-rounded-two btn-primary" disabled > Sudah Dibayar </button>';
				$action = '<div class="btn-group" style="display:' . $priv['detail'] . '"><button type="button" class="btn btn-custon-rounded-two btn-info" data-toggle="tooltip" data-placement="top" title="Detail"  onClick="detail(' . $v['id_invoice'] . ')" > <i class="fa fa-search-plus"></i>  </button></div>
						';
			}

			// if($v['term_of_payment'] == '0'){
			// $date=date_create($v['date_penjualan']);
			// $date_due = date_add($date,date_interval_create_from_date_string($v['term_of_payment']." days"));
			// $due_date = date_format($date_due,"Y-m-d");
			// }else{
			// $date=date_create($v['date_penjualan']);
			// $date_due = date_add($date,date_interval_create_from_date_string($v['term_of_payment']." days"));
			// $due_date = date_format($date_due,"Y-m-d");
			// }


			if ($v['term_of_payment'] == '0') {
				$hhh = 'Cash';
			} else {
				$hhh =  $v['term_of_payment'] . ' Hari';
			}

			if ($v['sisa'] == 0) {
				$sts = '<button type="button" class="btn btn-custon-rounded-two btn-primary" > Sudah Lunas </button>';
			} else {

				$sts = '<button type="button" class="btn btn-custon-rounded-two btn-danger" > Belum Lunas </button>';
			}

			array_push(
				$data,
				array(
					// number_format($v['Rangking'], 0, ',', '.'),
					$rowNumber++,
					$v['no_invoice'],
					$v['cust_name'],
					number_format(floatval($v['total_invoice']), 2, ',', '.'),
					number_format(floatval($v['sisa']), 2, ',', '.'),
					$v['tanggal_invoice'],
					$hhh,
					$sts,
					$action
				)
			);
			//$idx++;
		}

		$result["data"] = $data;
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		//$result["data"] = $list['data'];

		echo json_encode($result);

		//$this->json_result($result);

	}

	function lists_bayar()
	{

		if (!empty($_GET['sess_user_id'])) {
			$sess_user_id = $_GET['sess_user_id'];
		} else {
			$sess_user_id = NULL;
		}

		if (!empty($_GET['sess_token'])) {
			$sess_token = $_GET['sess_token'];
		} else {
			$sess_token = NULL;
		}

		if (!empty($_GET['filter'])) {
			$filter = $_GET['filter'];
		} else {
			$filter = NULL;
		}

		if (!empty($_GET['filter_month'])) {
			$filter_month = $_GET['filter_month'];
		} else {
			$filter_month = NULL;
		}

		if (!empty($_GET['filter_year'])) {
			$filter_year = $_GET['filter_year'];
		} else {
			$filter_year = NULL;
		}

		if ($this->input->get_post('draw') != FALSE) {
			$draw   = $this->input->get_post('draw');
		} else {
			$draw   = 1;
		};
		if ($this->input->get_post('length') != FALSE) {
			$length = $this->input->get_post('length');
		} else {
			$length = 10;
		};
		if ($this->input->get_post('start') != FALSE) {
			$start  = $this->input->get_post('start');
		} else {
			$start  = 0;
		};
		$order_fields = array('Rangking', 'no_payment', 'cust_name', 'total', 'tgl_terbit', 'account_type', 'id_hp', 'id_hp'); // , 'COST'
		$order = $this->input->get_post('order');
		if (!empty($order[0]['dir'])) {
			$order_dir    = $order[0]['dir'];
		} else {
			$order_dir    = 'desc';
		};
		if (!empty($order[0]['column'])) {
			$order_column = $order[0]['column'];
		} else {
			$order_column = 0;
		};

		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['sess_user_id'] = $sess_user_id;
		$params['sess_token'] 	= $sess_token;
		$params['filter'] 		= $filter;
		$params['filter_month'] 		= $_GET['filter_month'];
		$params['filter_year'] 		= $_GET['filter_year'];
		$params['searchtxt'] 	= $_GET['search']['value'];

		// print_r($params);die;
		$priv = $this->priv->get_priv();

		$list = $this->return_model->list_pembayaran($params);
		//print_r($list['data']);die;
		$data = array();
		foreach ($list['data'] as $k => $v) {

			if ($v['payment_status'] == 0) {
				$sts = '<button type="button" class="btn btn-custon-rounded-two btn-danger" data-toggle="tooltip" data-placement="top" title="Edit"  onClick="konfirmasi(' . $v['id_hp'] . ')"  > Konfirmasi </button>';
				// $action = '
				// <button type="button" class="btn btn-custon-rounded-two btn-primary" onClick="detail('. $v['id_invoice'].')" > Detail </button>
				// ';
				$action = '
						<div class="btn-group" style="display:' . $priv['update'] . '"><button type="button" class="btn btn-custon-rounded-two btn-warning" data-toggle="tooltip" data-placement="top" title="Edit"  onClick="updatepo(' . $v['id_hp'] . ')" > <i class="fa fa-edit"></i> </button></div>
						<div class="btn-group" style="display:' . $priv['delete'] . '"><button type="button" class="btn btn-custon-rounded-two btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"  onClick="deletepo(' . $v['id_hp'] . ')" > <i class="fa fa-trash"></i> </button></div>
						<div class="btn-group" style="display:' . $priv['detail'] . '"><button type="button" class="btn btn-custon-rounded-two btn-info" data-toggle="tooltip" data-placement="top" title="Detail"  onClick="detail(' . $v['id_hp'] . ')" > <i class="fa fa-search-plus"></i>  </button></div>
						';
			} else {
				$sts = '<button type="button" class="btn btn-custon-rounded-two btn-primary" disabled > Sudah Dibayar </button>';
				//$action = '<button type="button" class="btn btn-custon-rounded-two btn-primary" onClick="detail('. $v['id_invoice'].')" > Detail </button>
				$action = '<div class="btn-group" style="display:' . $priv['detail'] . '"><button type="button" class="btn btn-custon-rounded-two btn-info" data-toggle="tooltip" data-placement="top" title="Detail"  onClick="detail(' . $v['id_hp'] . ')" > <i class="fa fa-search-plus"></i>  </button></div>
						';
			}

			// if($v['term_of_payment'] == '0'){
			// $date=date_create($v['date_penjualan']);
			// $date_due = date_add($date,date_interval_create_from_date_string($v['term_of_payment']." days"));
			// $due_date = date_format($date_due,"Y-m-d");
			// }else{
			// $date=date_create($v['date_penjualan']);
			// $date_due = date_add($date,date_interval_create_from_date_string($v['term_of_payment']." days"));
			// $due_date = date_format($date_due,"Y-m-d");
			// }


			// if($v['term_of_payment'] == '0'){
			// $hhh = 'Cash';
			// }else{
			// $hhh =  $v['term_of_payment'].' Hari';
			// }

			// if($v['sisa'] == 0){
			// $sts = '<button type="button" class="btn btn-custon-rounded-two btn-primary" > Sudah Lunas </button>';

			// }else{

			// $sts = '<button type="button" class="btn btn-custon-rounded-two btn-danger" > Belum Lunas </button>';

			// }




			array_push(
				$data,
				array(
					number_format($v['Rangking'], 0, ',', '.'),
					$v['no_payment'],
					$v['cust_name'],
					number_format(floatval($v['total']), 2, ',', '.'),
					$v['tgl_terbit'],
					$v['account_type'],
					$sts,
					$action
				)
			);
			//$idx++;
		}

		$result["data"] = $data;
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		//$result["data"] = $list['data'];

		echo json_encode($result);

		//$this->json_result($result);

	}

	/**
	 * This function is redirect to add distributor page
	 * @return Void
	 */

	public function get_customer()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$result['customer'] = $this->return_model->get_customer_byid($params);

		//print_r($result['customer']);die;
		$item = $this->return_model->get_item_byprin_all();

		//print_r($item);die;

		$list = '<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		foreach ($item as $items) {


			$list .= '<option value="' . $items['id_mat'] . '|' . $items['unit_box'] . '|' . $items['bottom_price'] . '|' . $items['qty'] . '" >' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '</option>';
		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function get_penjualan_detail()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		//print_r($params);die;

		$result['penjualan'] = $this->return_model->get_penjualan_byid($params);
		//print_r($result['penjualan']);die;

		$Date = $result['penjualan'][0]['date_penjualan'];
		$result['due_date'] = date('Y-m-d', strtotime($Date . ' + ' . $result['penjualan'][0]['term_of_payment'] . ' days'));


		//print_r($result['customer']);die;
		$item = $this->return_model->get_penjualan_detail($params);

		//print_r($item);die;

		$grand_total = 0;

		$htl = '';
		$ii = 0;
		foreach ($item as $items) {

			$htl .=	'<div id="row_' . $ii . '" >';
			$htl .=	'									<div class="row" style="border-top-style:solid;">';
			$htl .=	'									<div style="margin-top:10px">';
			$htl .=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
			$htl .=	'												<input name="kode_v' . $ii . '" id="kode_v' . $ii . '" type="text" class="form-control " placeholder="Qty" value="' . $items['stock_name'] . ' ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '"  readOnly >';
			$htl .=	'												<input name="kode_' . $ii . '" id="kode_' . $ii . '" type="hidden" class="form-control " placeholder="Qty" value="' . $items['id_material'] . '|' . $items['box_ammount'] . '|' . $items['unit_price'] . '|' . $items['qty'] . '" readOnly  >';
			$htl .=	'												<input name="id_d_' . $ii . '" id="id_d_' . $ii . '" type="hidden" class="form-control " placeholder="Qty" value="' . $items['id_dp'] . '" readOnly  >';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_' . $ii . '" id="qty_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Qty" value=' . number_format($items['qty_box'], 0, ',', '.') . '  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<label id="iteml_' . $ii . '">Box @ ' . $items['box_ammount'] . '</label>';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qtys_' . $ii . '" id="qtys_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Qty" value=' . number_format($items['qty_satuan'], 0, ',', '.') . '  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_retur_' . $ii . '" id="qty_retur_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Qty" value=' . number_format($items['qty'], 0, ',', '.') . ' readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="totalqty_' . $ii . '" id="totalqty_' . $ii . '" type="number"  class="form-control" placeholder="Diskon" value=' . number_format($items['qty'], 0, ',', '.') . ' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="harga_' . $ii . '" id="harga_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Harga" value=' . number_format($items['unit_price'], 0, ',', '.') . ' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="total_' . $ii . '" id="total_' . $ii . '" type="text" class="form-control rupiah" placeholder="Total" value=' . number_format($items['price'], 0, ',', '.') . ' readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'									</div>';
			$htl .=	'									</div>';
			$htl .=	'								</div>';

			//$list .= '<option value="'.$items['id_mat'].'|'.$items['unit_box'].'|'.$items['bottom_price'].'|'.$items['qty'].'" >'.$items['stock_code'].' - '.$items['stock_name'].' - '.$items['base_qty'].' '.$items['uom_symbol'].'</option>';
			$ii++;

			$grand_total = $grand_total + $items['price'];
		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';

		$result['list'] = $htl;
		$result['int'] = $ii;
		$result['grand_total'] = number_format($grand_total, 2, ',', '.');
		$result['ppn'] = number_format(($grand_total / 11), 2, ',', '.');
		$result['dpp'] = number_format($grand_total - ($grand_total / 11), 2, ',', '.');

		//print_r($result['ppn']);die;
		//$result['total_amount'] = number_format(floatval($result['penjualan'][0]['total_amount']),0,',','.');
		//$result['ppn'] = number_format(floatval($result['penjualan'][0]['total_amount'])/11,2,',','.');
		//$result['dpp'] = number_format(floatval($result['penjualan'][0]['total_amount'])-floatval($result['ppn']),2,',','.');


		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}


	public function get_penjualan_detail_update()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		//print_r($params);die;

		$result['penjualan'] = $this->return_model->get_penjualan_byid_inv($params);
		//print_r($result['penjualan']);die;

		$Date = $result['penjualan'][0]['date_penjualan'];
		$result['due_date'] = date('Y-m-d', strtotime($Date . ' + ' . $result['penjualan'][0]['term_of_payment'] . ' days'));


		//print_r($result['customer']);die;
		$item = $this->return_model->get_penjualan_detail_update($params);

		//print_r($item);die;

		$grand_total = 0;

		$htl = '';
		$ii = 0;
		foreach ($item as $items) {

			$htl .=	'<div id="row_' . $ii . '" >';
			$htl .=	'									<div class="row" style="border-top-style:solid;">';
			$htl .=	'									<div style="margin-top:10px">';
			$htl .=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
			$htl .=	'												<input name="kode_v' . $ii . '" id="kode_v' . $ii . '" type="text" class="form-control " placeholder="Qty" value="' . $items['stock_name'] . ' ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '"  readOnly >';
			$htl .=	'												<input name="kode_' . $ii . '" id="kode_' . $ii . '" type="hidden" class="form-control " placeholder="Qty" value="' . $items['id_material'] . '|' . $items['box_ammount'] . '|' . $items['unit_price'] . '|' . $items['qty'] . '" readOnly  >';
			$htl .=	'												<input name="id_d_' . $ii . '" id="id_d_' . $ii . '" type="hidden" class="form-control " placeholder="Qty" value="' . $items['id_dp'] . '" readOnly  >';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_' . $ii . '" id="qty_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Qty" value=' . number_format($items['qty_box'], 0, ',', '.') . '  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<label id="iteml_' . $ii . '">Box @ ' . $items['box_ammount'] . '</label>';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qtys_' . $ii . '" id="qtys_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Qty" value=' . number_format($items['qty_satuan'], 0, ',', '.') . '  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="qty_retur_' . $ii . '" id="qty_retur_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Qty" value=' . number_format($items['qty'], 0, ',', '.') . ' readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="totalqty_' . $ii . '" id="totalqty_' . $ii . '" type="number"  class="form-control" placeholder="Diskon" value=' . number_format($items['qty'], 0, ',', '.') . ' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="harga_' . $ii . '" id="harga_' . $ii . '" type="text" onKeyup="change_sum(' . $ii . ')" class="form-control rupiah" placeholder="Harga" value=' . number_format($items['unit_price'], 0, ',', '.') . ' readOnly >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
			$htl .=	'											<div class="form-group">';
			$htl .=	'												<input name="total_' . $ii . '" id="total_' . $ii . '" type="text" class="form-control rupiah" placeholder="Total" value=' . number_format($items['price'], 0, ',', '.') . ' readOnly  >';
			$htl .=	'											</div>';
			$htl .=	'										</div>';
			$htl .=	'									</div>';
			$htl .=	'									</div>';
			$htl .=	'								</div>';

			//$list .= '<option value="'.$items['id_mat'].'|'.$items['unit_box'].'|'.$items['bottom_price'].'|'.$items['qty'].'" >'.$items['stock_code'].' - '.$items['stock_name'].' - '.$items['base_qty'].' '.$items['uom_symbol'].'</option>';
			$ii++;

			$grand_total = $grand_total + $items['price'];
		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';

		$result['list'] = $htl;
		$result['int'] = $ii;
		$result['grand_total'] = number_format($grand_total, 2, ',', '.');
		$result['ppn'] = number_format(($grand_total / 11), 2, ',', '.');
		$result['dpp'] = number_format($grand_total - ($grand_total / 11), 2, ',', '.');

		//print_r($result['ppn']);die;
		//$result['total_amount'] = number_format(floatval($result['penjualan'][0]['total_amount']),0,',','.');
		//$result['ppn'] = number_format(floatval($result['penjualan'][0]['total_amount'])/11,2,',','.');
		//$result['dpp'] = number_format(floatval($result['penjualan'][0]['total_amount'])-floatval($result['ppn']),2,',','.');


		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function get_all_item()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$item = $this->return_model->get_item_byprin($params);

		$list = '<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		foreach ($item as $items) {

			$list .= '<option value="' . $items['id_mat'] . '" >' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '</option>';
		}
		$list .= '<option value="0" selected="selected" disabled>-- Pilih Item Lainnya --</option>';

		$item2 = $this->return_model->get_item_byprin_all($params);

		foreach ($item2 as $items) {

			$list .= '<option value="' . $items['id_mat'] . '" >' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '</option>';
		}
		//	$list .= '------------------';

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}


	public function get_price_mat()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		//print_r($params);die;

		$ddd = $this->return_model->get_price_mat($params);
		//$item = $this->return_model->get_item_byprin($params);

		if (count($ddd) == 0) {
			$list = 0;
		} else {
			$list = number_format($ddd[0]['unit_price'], 0, ',', '.');
		}
		//print_r($ddd);die;

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add()
	{

		$params['region'] = 4;

		$result = $this->return_model->get_customer($params);
		$result_p = $this->return_model->get_penjualan_all();

		$year_count = date('Y') - 2019;

		$filter_year = '<select class="form-control" id="filter_year" onChange="changeFilter()"><option value="ALL" >ALL</option>';

		for ($i = $year_count; $i >= 0; $i--) {

			$y = 2019 + $i;

			if ($y == date('Y')) {
				$filter_year .= '<option value="' . $y . '" selected="selected" >' . $y . '</option>';
			} else {
				$filter_year .= '<option value="' . $y . '" >' . $y . '</option>';
			}
		}
		$filter_year .= '</select>';

		$array_month = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

		$filter_month = '<select class="form-control" id="filter_month" onChange="changeFilter()"><option value="ALL" >ALL</option>';
		foreach ($array_month as $array_months) {
			if ($array_months == date('F')) {
				$filter_month .= '<option value="' . $array_months . '" selected="selected">' . $array_months . '</option>';
			} else {
				$filter_month .= '<option value="' . $array_months . '" >' . $array_months . '</option>';
			}
		}

		$filter_month .= '</select>';
		$today_month = date('F');
		$today_year = date('Y');

		$result_g = $this->return_model->get_kode();
		if ($result_g[0]['max'] + 1 < 1000) {
			if ($result_g[0]['max'] + 1 < 100) {
				if ($result_g[0]['max'] + 1 < 10) {
					$kode = '' . date('Y') . '000' . ($result_g[0]['max'] + 1);
				} else {
					$kode = '' . date('Y') . '00' . ($result_g[0]['max'] + 1);
				}
			} else {
				$kode = '' . date('Y') . '0' . ($result_g[0]['max'] + 1);
			}
		} else {
			$kode = '' . date('Y') . '' . ($result_g[0]['max'] + 1);
		}
		//print_r($kode);die;
		$priv = $this->priv->get_priv();

		// $data = array(
		// 'priv' => $priv
		// );

		$data = array(
			'principal' => $result,
			'kode' => $kode,
			'item' => $result_g,
			'result_p' => $result_p,
			'priv' => $priv,
			'filter_month' => $filter_month,
			'filter_year' => $filter_year,
			'today_month' => $today_month,
			'today_year' => $today_year
		);

		$this->template->load('maintemplate', 'piutang/views/addPo', $data);
		//$this->load->view('addPrinciple',$data);
	}

	public function get_invoice()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		//$result['principal'] = $this->return_model->get_principal_byid($params);
		//$item = $this->purchase_order_model->get_item_byprin($params);
		$item = $this->return_model->get_invoice_byprin($params);

		//print_r($item);die;

		$list = '<option ></option>';;
		foreach ($item as $items) {



			$list .= '<option value="' . $items['id_invoice'] . '|' . $items['bayar'] . '|' . $items['sisa'] . '|' . $items['amount'] . '" >' . $items['no_invoice'] . '</option>';
		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}


	public function get_invoice_detail()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		//print_r($params);die;

		$item = $this->return_model->get_invoice_detail($params);



		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add_bayar()
	{

		$params['region'] = 4;

		$result = $this->return_model->get_customer($params);
		$result_p = $this->return_model->get_penjualan_all();
		$cash_account = $this->return_model->get_cash_account();

		$result_g = $this->return_model->get_kode();
		if ($result_g[0]['max'] + 1 < 1000) {
			if ($result_g[0]['max'] + 1 < 100) {
				if ($result_g[0]['max'] + 1 < 10) {
					$kode = '' . date('Y') . '000' . ($result_g[0]['max'] + 1);
				} else {
					$kode = '' . date('Y') . '00' . ($result_g[0]['max'] + 1);
				}
			} else {
				$kode = '' . date('Y') . '0' . ($result_g[0]['max'] + 1);
			}
		} else {
			$kode = '' . date('Y') . '' . ($result_g[0]['max'] + 1);
		}
		//print_r($result);die;

		$data = array(
			'principal' => $result,
			'kode' => $kode,
			'item' => $result_g,
			'cash_account' => $cash_account,
			'result_p' => $result_p
		);

		$this->template->load('maintemplate', 'piutang/views/add_bayar', $data);
		//$this->load->view('addPrinciple',$data);
	}


	public function updatepo()
	{
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			'id_penjualan' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),

		);

		$date_detail = $this->return_model->get_piutang_detail($data);

		$result = $this->return_model->get_customer($data);
		$cash_account = $this->return_model->get_cash_account();
		$cust = $this->return_model->get_cust_by_hp($data);

		$datas = array(
			'id_prin' => $cust[0]['id_customer'],

		);



		$invoice = $this->return_model->get_invoice_byprin($datas);
		//print_r($cust );die;
		$list = '';
		foreach ($invoice as $items) {

			// if($items['id_invoice'] == $date_detail[0]['id_invoice']){
			// $list .= '<option value="'.$items['id_invoice'].'|'.$items['bayar'].'|'.$items['sisa'].'|'.$items['amount'].'" selected >'.$items['no_invoice'].'</option>';
			// }else{
			$list .= '<option value="' . $items['id_invoice'] . '|' . $items['bayar'] . '|' . $items['sisa'] . '|' . $items['amount'] . '" >' . $items['no_invoice'] . '</option>';
			// }

		}


		//print_r($invoice);die;

		$data = array(
			'date_detail' => $date_detail[0],
			'cust' => $cust[0],
			'date_detail_item' => $date_detail,
			'cash_account' => $cash_account,
			'principal' => $result,
			'invoice' => $invoice,
			'list' => $list,
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE))

		);

		$this->template->load('maintemplate', 'piutang/views/updatePo', $data);
		//$this->load->view('addPrinciple',$data);
	}

	public function detail()
	{
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			'id_penjualan' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),

		);

		$date_detail = $this->return_model->get_piutang_detail($data);
		$result = $this->return_model->get_customer($data);
		$cash_account = $this->return_model->get_cash_account();
		$cust = $this->return_model->get_cust_by_hp($data);
		//print_r($date_detail);die;

		$data = array(
			'date_detail' => $date_detail[0],
			'cust' => $cust[0],
			'date_detail_item' => $date_detail,
			'cash_account' => $cash_account,
			'principal' => $result,
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE))

		);

		$this->template->load('maintemplate', 'piutang/views/detailPo', $data);
		//$this->load->view('addPrinciple',$data);
	}

	public function konfirmasi()
	{

		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			'id_penjualan' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),

		);

		$date_detail = $this->return_model->get_piutang_detail($data);
		$result = $this->return_model->get_customer($data);
		$cash_account = $this->return_model->get_cash_account();
		$cust = $this->return_model->get_cust_by_hp($data);

		//print_r($date_detail);die;

		$data = array(
			'date_detail' => $date_detail[0],
			'cust' => $cust[0],
			'date_detail_item' => $date_detail,
			'cash_account' => $cash_account,
			'principal' => $result,
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE))

		);

		$this->template->load('maintemplate', 'piutang/views/konfirmasi_bayar', $data);
		//$this->load->view('addPrinciple',$data);

	}

	public function konfirmasi_bayar()
	{

		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('id', TRUE))

		);

		$id = $this->Anti_sql_injection($this->input->post('id', TRUE));

		$result = $this->return_model->get_piutang_detail($data);
		//$result_now  = $this->return_model->get_payment_detail_now($id);

		foreach ($result as $resultss) {

			$data = array(
				'user_id' => $this->session->userdata['logged_in']['user_id'],
				'coa'     => $resultss['cash_account'],
				'tgl' => date('Y-m-d'),
				'date_insert' => date('Y-m-d H:i:s'),
				'jumlah' => $resultss['ammount'],
				'bukti' => "Bayar Piutang",
				'id_coa_temp' => $id,
				'ket' => '',
				'id_parent' => 0,
				'type_cash' => 0
			);

			$results = $this->return_model->add_uom($data);

			// if($result[0]['due_date'] <> '1970-01-01' ){
			$data = array(
				'user_id' => $this->session->userdata['logged_in']['user_id'],
				'coa'     => 24,
				'tgl' => date('Y-m-d'),
				'date_insert' => date('Y-m-d H:i:s'),
				'jumlah' => $resultss['ammount'],
				'bukti' => "Bayar Piutang",
				'id_coa_temp' => $id,
				'ket' => '',
				'id_parent' => $results['lastid'],
				'type_cash' => 1
			);

			$prin_result = $this->return_model->add_uom($data);
			//  }



			if ($resultss['total_amount'] - $resultss['bayar'] - $resultss['ammount'] <= 0) {
				$this->return_model->update_invoice_sts($result[0]['id_invoice']);
			}

			$this->return_model->update_konfirmas($id);
		}

		if ($prin_result['result'] > 0) {



			$msg = 'Berhasil Konfirmasi Pembayaran Piutang';

			$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
			$result = array('success' => true, 'message' => $msg);
		} else {
			$msg = 'Gagal Konfirmasi Pembayaran Piutang';

			$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
			$result = array('success' => false, 'message' => $msg);
		}




		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);

		//echo $kode;die;

	}

	public function approve_retur()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$data = array(
			'id_po' => $params['id']

		);

		//print_r($data);die;

		$add_prin_result = $this->return_model->edit_invoice_sts($data);

		// if($params['sts'] == 1){
		// //$this->return_model->history_retur_apr($data);

		// $item = $this->return_model->get_penjualan_detail_update($data);

		// foreach($item as $items){

		// //print_r($items);die;

		// $data_mutasi = array(
		// 'id_material'  => $items['id_material'],
		// 'qty'  => $items['qty_retur']

		// );

		// $add_mutasi_out = $this->return_model->add_mutasi_out($data_mutasi);

		// $data_mutasi = array(
		// 'id_material'  => $items['id_material'],
		// 'amount_mutasi'  => $items['qty_retur'],
		// 'id_dp'  => $items['id_dp'],
		// 'qty_retur'  => floatval($items['qty'])-floatval($items['qty_retur']),
		// 'qty_box_retur'  => floatval($items['qty_box'])-floatval($items['qty_box_retur']),
		// 'qty_retur_sat'  => floatval($items['qty_satuan'])-floatval($items['qty_retur_sat']),
		// 'amount'  => floatval($items['price'])-floatval($items['amount']),
		// 'mutasi_id'  => $add_mutasi_out['lastid']

		// );

		// $this->return_model->edit_detail_penjualan($data_mutasi);


		// }


		// }

		if ($add_prin_result['result'] > 0) {

			$msg = 'Berhasil Menambah data Retur';

			$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
			$result = array('success' => true, 'message' => $msg);
		} else {
			$msg = 'Gagal Menambah data Retur';

			$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
			$result = array('success' => false, 'message' => $msg);
		}
	}

	public function add_bayar_save()
	{

		$this->form_validation->set_rules('name', 'Nama Customer', 'required|min_length[1]|max_length[10]');
		$this->form_validation->set_rules('tgl', 'Tanggal Pembayaran Hutang', 'required');
		$this->form_validation->set_rules('casha', 'Cash Account', 'required');
		$this->form_validation->set_rules('kode_0', 'Invoice', 'required', ['required' => 'Invoice harus dipilih minimal satu']);
		// $this->form_validation->set_rules('alamat', 'Alamat Principal', 'required');
		//$this->form_validation->set_rules('tgl', 'Tanggal Tidak Boleh Kosong', 'required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();

			//echo $pesan;die;

			//$msg = strip_tags(str_replace("\r", '', $pesan));
			$msg = $pesan;

			$result = array('success' => false, 'message' => $msg);
		} else {

			$int_val = $this->Anti_sql_injection($this->input->post('int_flo', TRUE));

			$array_items = [];

			$total_amount = 0;
			for ($i = 0; $i <= $int_val; $i++) {


				$kode_split = explode('|', $this->Anti_sql_injection($this->input->post('kode_' . $i, TRUE)));
				if ($kode_split[0]) {
					$array_items[$i]['kode'] = $kode_split[0];
					//$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE));
					//$array_items[$i]['bayar'] = $this->Anti_sql_injection($this->input->post('bayar_'.$i, TRUE));
					$array_items[$i]['bayar'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('bayar_' . $i, TRUE)))));
				}
			}

			//print_r($array_items);die;

			//$total_amounts = $total_amount + ($total_amount * (floatval($this->Anti_sql_injection($this->input->post('ppn', TRUE)))/100));

			$data = array(
				'kode' => $this->Anti_sql_injection($this->input->post('kode', TRUE)),
				'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
				'customer' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
				'tipe_bayar' => $this->Anti_sql_injection($this->input->post('term', TRUE)),
				'cash_acc' => $this->Anti_sql_injection($this->input->post('casha', TRUE)),
				'tglterbit' => $this->Anti_sql_injection($this->input->post('tglterbit', TRUE)),
				'tglaktif' => $this->Anti_sql_injection($this->input->post('tglaktif', TRUE)),
				'tgljatuhtempo' => $this->Anti_sql_injection($this->input->post('tgljatuhtempo', TRUE))
			);

			//print_r($data);die;


			$add_prin_result = $this->return_model->add_payment($data);
			//$this->return_model->edit_credit($data);

			foreach ($array_items as $array_itemss) {

				if ($array_itemss['kode'] == "") {
				} else {

					$datas = array(

						'id_hp' => $add_prin_result['lastid'],
						'id_invoice' => $array_itemss['kode'],
						'ammount' => $array_itemss['bayar'],


						//'remark' => $array_itemss['remark'],

					);

					//print_r($datas);die;

					//$id_mutasi = $this->return_model->add_mutasi($datas);
					//$this->return_model->edit_qty($datas);



					$prin_result = $this->return_model->add_detail_payment($datas);
				}
			}



			//print_r($add_prin_result);die;

			if ($prin_result['result'] > 0) {


				$result_g = $this->return_model->update_seq('pembayaran_k');


				$msg = 'Berhasil Menambah Pembayaran Piutang';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => true, 'message' => $msg);
			} else {
				$msg = 'Gagal Menambah Pembayaran Piutang';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => false, 'message' => $msg);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);

		//echo $kode;die;

	}

	public function edit_bayar()
	{

		$this->form_validation->set_rules('name', 'nama Principal', 'required|min_length[1]|max_length[10]');
		// $this->form_validation->set_rules('alamat', 'Alamat Principal', 'required');
		//$this->form_validation->set_rules('tgl', 'Tanggal Tidak Boleh Kosong', 'required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();

			//echo $pesan;die;

			//$msg = strip_tags(str_replace("\r", '', $pesan));
			$msg = $pesan;

			$result = array('success' => false, 'message' => $msg);
		} else {

			$int_val = $this->Anti_sql_injection($this->input->post('int_flo', TRUE));
			$id_hp = $this->Anti_sql_injection($this->input->post('id', TRUE));

			$result_dist2 		= $this->return_model->delete_hp_detail($id_hp);
			$result_dist 		= $this->return_model->delete_hp($id_hp);

			$array_items = [];

			$total_amount = 0;
			for ($i = 0; $i <= $int_val; $i++) {


				$kode_split = explode('|', $this->Anti_sql_injection($this->input->post('kode_' . $i, TRUE)));
				if ($kode_split[0]) {
					$array_items[$i]['kode'] = $kode_split[0];
					//$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE));
					//$array_items[$i]['bayar'] = $this->Anti_sql_injection($this->input->post('bayar_'.$i, TRUE));
					$array_items[$i]['bayar'] = floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('bayar_' . $i, TRUE)))));
				}
			}

			//print_r($array_items);die;

			//$total_amounts = $total_amount + ($total_amount * (floatval($this->Anti_sql_injection($this->input->post('ppn', TRUE)))/100));

			$data = array(
				'kode' => $this->Anti_sql_injection($this->input->post('kode', TRUE)),
				'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
				'customer' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
				'tipe_bayar' => $this->Anti_sql_injection($this->input->post('term', TRUE)),
				'cash_acc' => $this->Anti_sql_injection($this->input->post('casha', TRUE)),
				'tglterbit' => $this->Anti_sql_injection($this->input->post('tglterbit', TRUE)),
				'tglaktif' => $this->Anti_sql_injection($this->input->post('tglaktif', TRUE)),
				'tgljatuhtempo' => $this->Anti_sql_injection($this->input->post('tgljatuhtempo', TRUE))
			);

			//print_r($data);die;


			$add_prin_result = $this->return_model->add_payment($data);
			//$this->return_model->edit_credit($data);

			foreach ($array_items as $array_itemss) {

				if ($array_itemss['kode'] == "") {
				} else {

					$datas = array(

						'id_hp' => $add_prin_result['lastid'],
						'id_invoice' => $array_itemss['kode'],
						'ammount' => $array_itemss['bayar'],


						//'remark' => $array_itemss['remark'],

					);

					//print_r($datas);die;

					//$id_mutasi = $this->return_model->add_mutasi($datas);
					//$this->return_model->edit_qty($datas);



					$prin_result = $this->return_model->add_detail_payment($datas);
				}
			}



			//print_r($add_prin_result);die;

			if ($prin_result['result'] > 0) {


				$result_g = $this->return_model->update_seq('pembayaran_k');


				$msg = 'Berhasil Merubah Pembayaran Piutang';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => true, 'message' => $msg);
			} else {
				$msg = 'Gagal Merubah Pembayaran Piutang';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => false, 'message' => $msg);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);

		//echo $kode;die;

	}

	public function add_po()
	{

		$this->form_validation->set_rules('name', 'nama Principal', 'required|min_length[1]|max_length[10]');
		// $this->form_validation->set_rules('alamat', 'Alamat Principal', 'required');
		//$this->form_validation->set_rules('tgl', 'Tanggal Tidak Boleh Kosong', 'required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();

			//echo $pesan;die;

			//$msg = strip_tags(str_replace("\r", '', $pesan));
			$msg = $pesan;

			$result = array('success' => false, 'message' => $msg);
		} else {

			$int_val = $this->Anti_sql_injection($this->input->post('int_flo', TRUE));

			$array_items = [];

			$total_amount = 0;
			for ($i = 0; $i <= $int_val; $i++) {


				$kode_split = explode('|', $this->Anti_sql_injection($this->input->post('kode_' . $i, TRUE)));
				if ($kode_split[0]) {
					$array_items[$i]['kode'] = $kode_split[0];
					$array_items[$i]['qty_stock'] = $kode_split[3];
					//$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE));
					$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE));
					$array_items[$i]['qty'] = $this->Anti_sql_injection($this->input->post('qty_' . $i, TRUE));
					$array_items[$i]['qtys'] = $this->Anti_sql_injection($this->input->post('qtys_' . $i, TRUE));
					$array_items[$i]['id_d'] = $this->Anti_sql_injection($this->input->post('id_d_' . $i, TRUE));
					$array_items[$i]['qty_box'] = $kode_split[1];
					$array_items[$i]['total'] = ((intval(str_replace('.', '', $array_items[$i]['qty'])) + intval(str_replace('.', '', $array_items[$i]['qtys']))) * intval($kode_split[1])) * floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE)))));
					//	$array_items[$i]['remark'] = $this->Anti_sql_injection($this->input->post('remark_'.$i, TRUE));
					$array_items[$i]['totalqty'] = $this->Anti_sql_injection($this->input->post('totalqty_' . $i, TRUE));
					$total_amount = $total_amount + (((intval(str_replace('.', '', $array_items[$i]['qty'])) + intval(str_replace('.', '', $array_items[$i]['qtys']))) * intval($kode_split[1])) * floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE))))));
				}
			}

			//print_r($array_items);die;

			//$total_amounts = $total_amount + ($total_amount * (floatval($this->Anti_sql_injection($this->input->post('ppn', TRUE)))/100));

			$data = array(
				'id_penjualan' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
				'note' => $this->Anti_sql_injection($this->input->post('ket_inv', TRUE)),
				'tanggal_invoice' => $this->Anti_sql_injection($this->input->post('tgl_inv', TRUE)),
				'due_date' => $this->Anti_sql_injection($this->input->post('tgl_due', TRUE)),
				'no_invoice' => $this->Anti_sql_injection($this->input->post('no_inv', TRUE)),
				'attention' => $this->Anti_sql_injection($this->input->post('att', TRUE))
			);

			//print_r($data);die;


			$add_prin_result = $this->return_model->add_invoice($data);
			//$this->return_model->edit_credit($data);

			foreach ($array_items as $array_itemss) {

				if ($array_itemss['kode'] == "") {
				} else {

					$datas = array(

						'id_inv_penjualan' => $add_prin_result['lastid'],
						'id_d_penjualan' => $array_itemss['id_d'],
						'unit_price' => str_replace(',', '.', str_replace('.', '', $array_itemss['harga'])),
						'price' => intval(str_replace('.', '', $array_itemss['harga'])) * ((intval(str_replace('.', '', $array_itemss['qty'])) + intval(str_replace('.', '', $array_itemss['qtys']))) * intval($array_itemss['qty_box'])),
						'qty' => (intval(str_replace('.', '', $array_itemss['qty'])) + intval(str_replace('.', '', $array_itemss['qtys']))) * intval($array_itemss['qty_box']),
						'box_ammount' => intval($array_itemss['qty_box']),
						'qty_box' => intval(str_replace('.', '', $array_itemss['qty'])),
						'qty_satuan' => intval(str_replace('.', '', $array_itemss['qtys']))

						//'remark' => $array_itemss['remark'],

					);

					//print_r($datas);die;

					//$id_mutasi = $this->return_model->add_mutasi($datas);
					//$this->return_model->edit_qty($datas);



					$prin_result = $this->return_model->add_detail_invoice($datas);
				}
			}



			//print_r($add_prin_result);die;

			if ($prin_result['result'] > 0) {





				$msg = 'Berhasil Menambah data Retur';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => true, 'message' => $msg);
			} else {
				$msg = 'Gagal Menambah data Retur';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => false, 'message' => $msg);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);

		//echo $kode;die;

	}

	public function edit_po()
	{

		// $this->form_validation->set_rules('kode', 'Kode Principal', 'required|min_length[4]|max_length[10]');
		// $this->form_validation->set_rules('alamat', 'Alamat Principal', 'required');
		$this->form_validation->set_rules('name', 'nama Principal', 'required|min_length[1]|max_length[10]');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();

			//echo "aasasasas";die;

			//$msg = strip_tags(str_replace("\r", '', $pesan));
			$msg = $pesan;

			$result = array('success' => false, 'message' => $msg);
		} else {

			$int_val = $this->Anti_sql_injection($this->input->post('int_flo', TRUE));
			$id_invoice = $this->Anti_sql_injection($this->input->post('id_invoice', TRUE));

			$array_items = [];

			$total_amount = 0;
			for ($i = 0; $i <= $int_val; $i++) {

				$kode_split = explode('|', $this->Anti_sql_injection($this->input->post('kode_' . $i, TRUE)));
				if ($kode_split[0]) {
					$array_items[$i]['kode'] = $kode_split[0];
					$array_items[$i]['qty_stock'] = $kode_split[3];
					//$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE));
					$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE));
					$array_items[$i]['qty'] = $this->Anti_sql_injection($this->input->post('qty_' . $i, TRUE));
					$array_items[$i]['qtys'] = $this->Anti_sql_injection($this->input->post('qtys_' . $i, TRUE));
					$array_items[$i]['id_d'] = $this->Anti_sql_injection($this->input->post('id_d_' . $i, TRUE));
					$array_items[$i]['qty_box'] = $kode_split[1];
					$array_items[$i]['total'] = ((intval(str_replace('.', '', $array_items[$i]['qty'])) + intval(str_replace('.', '', $array_items[$i]['qtys']))) * intval($kode_split[1])) * floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE)))));
					//	$array_items[$i]['remark'] = $this->Anti_sql_injection($this->input->post('remark_'.$i, TRUE));
					$array_items[$i]['totalqty'] = $this->Anti_sql_injection($this->input->post('totalqty_' . $i, TRUE));
					$total_amount = $total_amount + (((intval(str_replace('.', '', $array_items[$i]['qty'])) + intval(str_replace('.', '', $array_items[$i]['qtys']))) * intval($kode_split[1])) * floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE))))));
				}
			}


			$data = array(
				'id_penjualan' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
				'note' => $this->Anti_sql_injection($this->input->post('ket_inv', TRUE)),
				'tanggal_invoice' => $this->Anti_sql_injection($this->input->post('tgl_inv', TRUE)),
				'due_date' => $this->Anti_sql_injection($this->input->post('tgl_due', TRUE)),
				'no_invoice' => $this->Anti_sql_injection($this->input->post('no_inv', TRUE)),
				'attention' => $this->Anti_sql_injection($this->input->post('att', TRUE)),
				'id_invoice' => $id_invoice

			);

			$add_prin_result = $this->return_model->edit_invoice($data);

			$this->return_model->delete_detail_invoice($id_invoice);



			foreach ($array_items as $array_itemss) {

				if ($array_itemss['kode'] == "") {
				} else {

					$datas = array(

						'id_inv_penjualan' => $id_invoice,
						'id_d_penjualan' => $array_itemss['id_d'],
						'unit_price' => str_replace(',', '.', str_replace('.', '', $array_itemss['harga'])),
						'price' => intval(str_replace('.', '', $array_itemss['harga'])) * ((intval(str_replace('.', '', $array_itemss['qty'])) + intval(str_replace('.', '', $array_itemss['qtys']))) * intval($array_itemss['qty_box'])),
						'qty' => (intval(str_replace('.', '', $array_itemss['qty'])) + intval(str_replace('.', '', $array_itemss['qtys']))) * intval($array_itemss['qty_box']),
						'box_ammount' => intval($array_itemss['qty_box']),
						'qty_box' => intval(str_replace('.', '', $array_itemss['qty'])),
						'qty_satuan' => intval(str_replace('.', '', $array_itemss['qtys']))

						//'remark' => $array_itemss['remark'],

					);

					$prin_result = $this->return_model->add_detail_invoice($datas);
					//$this->return_model->update_qty_m($data_q);

				}
			}



			//print_r($add_prin_result);die;

			if ($add_prin_result['result'] > 0) {





				$msg = 'Berhasil Merubah data Retur';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => true, 'message' => $msg);
			} else {
				$msg = 'Gagal Menambah data Retur';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => false, 'message' => $msg);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);

		//echo $kode;die;

	}

	public function delete_po()
	{
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		//print_r($params);die;

		$result_dist2 		= $this->return_model->delete_hp_detail($params['id']);
		$result_dist 		= $this->return_model->delete_hp($params['id']);


		$msg = 'Berhasil menghapus data Pembayara.';
		$result = array('success' => true, 'message' => $msg);
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function print_pdf()
	{

		$data   	= file_get_contents("php://input");
		//$params     = json_decode($data,true);


		//echo "aaa";
		$params = (explode('=', $data));

		$data = array(
			'id' => $params[1],

		);

		//print_r($data);die;

		$po_result = $this->return_model->get_po($data);
		$po_detail_result = $this->return_model->get_po_detail_full($data);
		$result = $this->return_model->get_principal();



		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);

		$array_items = [];

		$total_sub = 0;
		$total_sub_all = 0;
		$total_disk = 0;
		$grand_total = 0;

		$htmls = '';
		foreach ($po_detail_result as $po_detail_results) {

			$amn = number_format(($po_detail_results['price'] - ($po_detail_results['price'] * ($po_detail_results['diskon'] / 100))), 0, ',', '');
			$htmls = $htmls . '
			<tr style="border-top: 1px solid black;" >
								<td  align="left" style="border-top: 1px solid black;"><p>' . $po_detail_results['stock_code'] . '</p></td>
								<td  align="left" style="border-top: 1px solid black;"><p>' . $po_detail_results['stock_name'] . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['base_qty'], '2', ',', '.') . ' ' . $po_detail_results['uom_symbol'] . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['qty_order'], '2', ',', '.') . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['unit_price'], '0', ',', '.') . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['diskon'], '2', ',', '.') . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($amn, '0', ',', '.') . '</p></td>

							</tr>
			';

			//$total_sub = $total_sub + number_format(($po_detail_results['price']-($po_detail_results['price']*($po_detail_results['diskon']/100))),0,',',''); 
			$total_sub = $total_sub + number_format(($po_detail_results['price'] - ($po_detail_results['price'] * ($po_detail_results['diskon'] / 100))), 0, ',', '');
			$total_sub_all = $total_sub_all + number_format(($po_detail_results['price']), 0, ',', '');
			$total_disk = $total_disk + ($po_detail_results['price'] * ($po_detail_results['diskon'] / 100));
			//$grand_total = $grand_total +  number_format(($po_detail_results['price']),0,',',''); 

		}

		$total_ppn = $total_sub * (floatval($po_result[0]['ppn'])) / 100;
		$grand_total = $total_ppn + $total_sub;

		//print_r($htmls);die;

		$item = $this->return_model->get_item_byprin($params);

		$rtext = '<br><p style="font-size: 10px;">PT. ENDRIA ALDA</p> ';

		$ctext = '<br><p style="font-size: 12px"> PURCHASE ORDER</p> ';

		$this->load->library('Pdf');

		$pdf = new Pdf('P', 'mm', 'A4', false, 'UTF-8', false);
		$pdf->SetTitle('Purchase Order');
		$pdf->SetHeaderMargin(25);
		$pdf->setPrintHeader(false);
		$pdf->SetTopMargin(6);
		$pdf->setFooterMargin(5);
		$pdf->SetAutoPageBreak(true);
		$pdf->SetAuthor('Author');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->SetPrintFooter(false);

		$pdf->AddPage();


		$html = '
		
		<style>
		
		  .floatedTable {
            float:left;
        }
        .inlineTable {
            display: inline-block;
        }
		p {
							font-size: 8px;
							
						}
		p {
							font-size: 8px;
							
						}
		</style>
  <table style=" float:left;border: none;" >
						<tr style="border: none;">
							<th  align="left" ><h5>PT.ENDIRA ALDA</h5></th>
							<th  align="RIGHT"></th>
						</tr>
						<tr style="border: none;">
							<th  align="left" ><h4>PURCHASE ORDER</h4></th>
							<th  align="RIGHT"><p>NOMOR, TGL : ' . $po_result[0]['no_po'] . '   ' . $po_result[0]['date_po'] . '</p></th>
						</tr>
					  </table><br><br><br>
					  
					<table>
					  <tr>
						<td>

						  <table cellspacing="0" cellpadding="0" border="0" style="float:right;width:100%.">
							<tr>
							  <td style="width:18%;"><p>Kepada</p></td>
							  <td><p>: ' . $po_result[0]['name_eksternal'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Alamat</p></td>
							  <td><p>: ' . $po_result[0]['eksternal_address'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Att</p></td>
							  <td><p>: ' . $po_result[0]['pic'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Telp</p></td>
							  <td><p>: ' . $po_result[0]['phone_1'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Fax</p></td>
							  <td><p>: ' . $po_result[0]['fax'] . '</p></td>
							</tr>

						  </table>
						</td>
						<td>
						  <table cellspacing="0" cellpadding="0" border="0" style="float:right;width:100%">
							<tr>
							  <td style="width:18%;"><p>Ship To</p></td>
							  <td><p>:JL.SANGKURIANG NO 38-A<BR>NPWP:01.555.161.7.428.000</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Email</p></td>
							  <td><p>: </p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Telp/Fax</p></td>
							  <td><p>: (022)6626-946</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Delivery</p></td>
							  <td><p>: </p></td>
							</tr>
						

						  </table>
						</td>
					  </tr>
					</table>
					<br><br><br>

					
					  <table style=" float:left;border-top: 1px solid black;border-bottom: 1px solid black;" >
						<tr style=" border-top: 1px solid black;font-size: 10px;">
							<th  align="left" >Code</th>
							<th  align="RIGHT">Description</th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Quantity<br>Lt/Kg</th>
							<th  align="RIGHT">Unit Price</th>
							<th  align="RIGHT">Disc</th>
							<th  align="RIGHT">Amount</th>
						</tr>
						' . $htmls . '
					  </table>
					  	<br><br><br>
						<br><br><br>
					  
					    <table style=" float:left;border-top: 1px solid black;" >
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Sub Total :</th>
							<th  align="RIGHT">' . number_format($total_sub_all, 0, ',', '.') . '</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Discount :</th>
							<th  align="RIGHT">' . number_format($total_disk, 0, ',', '.') . '</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"> Taxable :</th>
							<th  align="RIGHT">' . number_format($total_sub, 0, ',', '.') . '</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Vat/PPN :</th>
							<th  align="RIGHT">' . number_format($total_ppn, 0, ',', '.') . '</th>
						</tr>
					  </table>
					  
					  <table style=" float:left;border-top: 1px solid black;" >
						<tr style=" border-top: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Total :</th>
							<th  align="RIGHT">' . number_format($grand_total, 0, ',', '.') . '</th>
						</tr>
					  </table>
					<br><br><br><br><br> 
					<br><br><br><br><br> 
					
					 <table style="font-size: 10px;" >
						<tr style="">
							<th  align="CENTER" style="width:25%" ></th>
							<th  align="CENTER" style="width:12%" ></th>
							<th  align="CENTER" style="width:25%"></th>
							<th  align="CENTER" style="width:13%" ></th>
							<th  align="CENTER" style="width:25%">Cimahi, ' . date('m/d/Y') . '</th>
						</tr>
						<tr style="">
							<th  align="CENTER" style="width:25%" >Disetujui Oleh,</th>
							<th  align="CENTER" style="width:12%" ></th>
							<th  align="CENTER" style="width:25%">Diperiksa Oleh,</th>
							<th  align="CENTER" style="width:13%" ></th>
							<th  align="CENTER" style="width:25%">Dipesan Oleh,</th>
						</tr>
					  </table>
					  <br><br><br><br><br> 
					  <table style="font-size: 10px;" >
						<tr style="">
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Ratna S. Iskandar Dinata</span></th>
							<th  align="CENTER" style="width:12%"></th>
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Rienaldy Aryanto</span></th>
							<th  align="CENTER" style="width:13%"></th>
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Rangga Dean</span></th>
						</tr>
					  </table>
';

		//echo $html;die;

		// $html = <<<EOD
		// <h5>PT.ENDIRA ALDA</h5>
		// <table style=" float:left;border: none;" >
		// <tr style="border: none;">
		// <th  align="left" ><h4>PURCHASE ORDER</h4></th>
		// <th  align="RIGHT">NOMOR, TGL : ".."</th>
		// </tr>
		// </table>

		// EOD;

		// Print text using writeHTMLCell()
		$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);



		$pdf->SetPrintFooter(false);

		$pdf->lastPage();

		// $pdf->writeHTMLCell(85, 5, '<p>'.$kategoriby.' : '.$kategori.'<br>Periode : '.$start_date.' / '.$end_date.'</p> ', 1, 'L', 0, 0, '', '', true);
		// $pdf->writeHTMLCell(85, 5, $img, 1, 'R', 0, 1, '', '', true);
		// $pdf->writeHTMLCell(180, 5, '[CENTER] ', 1, 'C', 0, 1, '', '', true);
		// $pdf->writeHTMLCell(85, 5, '[LEFT] ', 1, 'L', 0, 0, '', '', true);
		// $pdf->writeHTMLCell(85, 5, '[RIGHT] ', 1, 'L', 0, 1, '', '', true);

		// $pdf->Output('/var/www/html/tmp_doc/Report_Postbuy.pdf', 'F');

		// $this->output->set_content_type('application/json')->set_output(json_encode('aaa'));

		$pdf->Output('Purchase_order.pdf', 'I');


		// if ( $list ) {			
		// $this->output->set_content_type('application/json')->set_output(json_encode($list));
		// } else {
		// $result = array( 'Value not found!' );
		// $this->output->set_content_type('application/json')->set_output(json_encode($list));
		// }

	}
}
