<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Uom_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
    
	// function get_report_keu($params){
		
		// $sql 	= "
		// SELECT a.* ,coa,keterangan,
		// IF(masuk IS  NULL,0,masuk) masuk,
		// IF(keluar IS  NULL,0,keluar) keluar,
		// IF(masuk IS  NULL,0,masuk)-IF(keluar IS  NULL,0,keluar) AS nilai
		// FROM t_report_keu a
		// LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS masuk FROM t_coa_value b 
		// WHERE b.`type_cash`=0 AND b.date  <= '".$params['end_time']."' GROUP BY b.`id_coa`)b ON a.id_coa=b.id_coa
		// LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS keluar FROM t_coa_value b 
		// WHERE b.`type_cash`=1 AND b.date <= '".$params['end_time']."' GROUP BY b.`id_coa`)c ON a.id_coa=c.id_coa
		// JOIN t_coa t ON a.id_coa=t.id_coa
		// WHERE a.`nama_report`='".$params['report']."'
		// ORDER BY a.`seq`
		
		// ";
		
		// $out = array();
		// $query 	=  $this->db->query($sql);

		// $result = $query->result_array();

		// $this->db->close();
		// $this->db->initialize();

		// return $result;
		
		
	// }		
	
	function get_report_keu($params){
		
		$start_time = explode('-',$params['end_time']);
		
		$sql 	= "
			SELECT A.coa,A.keterangan,A.`id_report`,A.`nama_report`,A.`id_coa`,A.`seq`,A.`type_data`,A.`summ_type`,A.`summ_type_detail`,IF(B.nilai IS NULL,A.nilai,B.nilai) AS nilai FROM (
				SELECT a.* ,coa,keterangan,
				IF(masuk IS  NULL,0,masuk) masuk,
				IF(keluar IS  NULL,0,keluar) keluar,
				IF(masuk IS  NULL,0,masuk)-IF(keluar IS  NULL,0,keluar) AS nilai
				FROM t_report_keu a
				LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS masuk FROM t_coa_value b 
				WHERE b.`type_cash`=0 AND b.date <= '".$params['end_time']."' GROUP BY b.`id_coa`)b ON a.id_coa=b.id_coa
				LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS keluar FROM t_coa_value b 
				WHERE b.`type_cash`=1 AND b.date <= '".$params['end_time']."' GROUP BY b.`id_coa`)c ON a.id_coa=c.id_coa
				JOIN t_coa t ON a.id_coa=t.id_coa
				WHERE a.`nama_report`='laba_rugi'
				ORDER BY a.`seq`
			) A LEFT JOIN (
				SELECT a.* ,coa,'Persediaan Awal' keterangan,
				IF(masuk IS  NULL,0,masuk) masuk,
				IF(keluar IS  NULL,0,keluar) keluar,
				IF(masuk IS  NULL,0,masuk)-IF(keluar IS  NULL,0,keluar) AS nilai
				FROM t_report_keu a
				LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS masuk FROM t_coa_value b 
				WHERE b.`type_cash`=0 AND b.date < '".$start_time[0]."-".$start_time[1]."-01' GROUP BY b.`id_coa`)b ON a.id_coa=b.id_coa
				LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS keluar FROM t_coa_value b 
				WHERE b.`type_cash`=1 AND b.date < '".$start_time[0]."-".$start_time[1]."-01' GROUP BY b.`id_coa`)c ON a.id_coa=c.id_coa
				JOIN t_coa t ON a.id_coa=t.id_coa
				WHERE a.id_coa = 33
				ORDER BY a.`seq`
			) B ON A.keterangan = B.keterangan
		
		";
		
		//echo $sql;die;
		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
		
		
	}	
	
	// function get_report_keu_summ($params){
		
		// $sql 	= "
			// SELECT SUM(IF(summ_type_detail = 0,nilai,0)) plus, SUM(IF(summ_type_detail = 1,nilai,0)) minus,SUM(IF(summ_type_detail = 0,nilai,0)) - SUM(IF(summ_type_detail = 1,nilai,0)) AS nilai, `summ_type` FROM (

			// SELECT a.* ,coa,keterangan,
			// IF(masuk IS  NULL,0,masuk) masuk,
			// IF(keluar IS  NULL,0,keluar) keluar,
			// IF(masuk IS  NULL,0,masuk)-IF(keluar IS  NULL,0,keluar) AS nilai
			// FROM t_report_keu a
			// LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS masuk FROM t_coa_value b 
			// WHERE b.`type_cash`=0 AND b.date <= '".$params['end_time']."' GROUP BY b.`id_coa`)b ON a.id_coa=b.id_coa
			// LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS keluar FROM t_coa_value b 
			// WHERE b.`type_cash`=1 AND b.date <= '".$params['end_time']."' GROUP BY b.`id_coa`)c ON a.id_coa=c.id_coa
			// JOIN t_coa t ON a.id_coa=t.id_coa
			// WHERE a.`nama_report`='".$params['report']."'
			// ORDER BY a.`seq`
			
			
	// ) OP
 // GROUP BY `summ_type`
 // ORDER BY SEQ
// ";
		
		// // echo $sql;die;
		// $out = array();
		// $query 	=  $this->db->query($sql);

		// $result = $query->result_array();

		// $this->db->close();
		// $this->db->initialize();

		// return $result;
		
		
	// }	
	
	function get_report_keu_summ($params){
		
		$start_time = explode('-',$params['end_time']);
		
		$sql 	= "
			SELECT SUM(IF(summ_type_detail = 0,nilai,0)) plus, SUM(IF(summ_type_detail = 1,nilai,0)) minus,SUM(IF(summ_type_detail = 0,nilai,0)) - SUM(IF(summ_type_detail = 1,nilai,0)) AS nilai, `summ_type` FROM (

			SELECT A.coa,A.keterangan,A.`id_report`,A.`nama_report`,A.`id_coa`,A.`seq`,A.`type_data`,A.`summ_type`,A.`summ_type_detail`,IF(B.nilai IS NULL,A.nilai,B.nilai) AS nilai FROM (
				SELECT a.* ,coa,keterangan,
				IF(masuk IS  NULL,0,masuk) masuk,
				IF(keluar IS  NULL,0,keluar) keluar,
				IF(masuk IS  NULL,0,masuk)-IF(keluar IS  NULL,0,keluar) AS nilai
				FROM t_report_keu a
				LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS masuk FROM t_coa_value b 
				WHERE b.`type_cash`=0 AND b.date <= '".$params['end_time']."' GROUP BY b.`id_coa`)b ON a.id_coa=b.id_coa
				LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS keluar FROM t_coa_value b 
				WHERE b.`type_cash`=1 AND b.date <= '".$params['end_time']."' GROUP BY b.`id_coa`)c ON a.id_coa=c.id_coa
				JOIN t_coa t ON a.id_coa=t.id_coa
				WHERE a.`nama_report`='".$params['report']."'
				ORDER BY a.`seq`
			) A LEFT JOIN (
				SELECT a.* ,coa,'Persediaan Awal' keterangan,
				IF(masuk IS  NULL,0,masuk) masuk,
				IF(keluar IS  NULL,0,keluar) keluar,
				IF(masuk IS  NULL,0,masuk)-IF(keluar IS  NULL,0,keluar) AS nilai
				FROM t_report_keu a
				LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS masuk FROM t_coa_value b 
				WHERE b.`type_cash`=0 AND b.date < '".$start_time[0]."-".$start_time[1]."-01' GROUP BY b.`id_coa`)b ON a.id_coa=b.id_coa
				LEFT JOIN (SELECT b.id_coa, SUM(b.`value_real`)AS keluar FROM t_coa_value b 
				WHERE b.`type_cash`=1 AND b.date < '".$start_time[0]."-".$start_time[1]."-01' GROUP BY b.`id_coa`)c ON a.id_coa=c.id_coa
				JOIN t_coa t ON a.id_coa=t.id_coa
				WHERE a.id_coa = 33
				ORDER BY a.`seq`
			) B ON A.keterangan = B.keterangan
			
			
	) OP
 GROUP BY `summ_type`
 ORDER BY SEQ
";
		
		// echo $sql;die;
		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
		
		
	}
	
    public function get_uom_data($id)
	{
		//$sql 	= 'CALL uom_search_id(?)';
		$sql 	= 'select a.*,b.coa,b.keterangan FROM t_coa_value a join t_coa b on a.id_coa = b.id_coa
		where a.id = ?';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$id
			));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}   

	public function sales_perbulan($year,$month)
	{
		//$sql 	= 'CALL uom_search_id(?)';
		$sql 	= "
			SELECT SUM(b.`price`) AS sumtotppn,SUM(b.`price`)/11 AS sumppn,SUM(b.`price`)- (SUM(b.`price`)/11) AS sumtot,d.`type_material_name`,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan
	FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_type_material d ON c.`type`=d.`id_type_material`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=?
	AND DATE_FORMAT(a.`date_penjualan`,'%c')=?
	GROUP BY c.`type`,DATE_FORMAT(a.`date_penjualan`,'%m')
	ORDER BY DATE_FORMAT(a.`date_penjualan`,'%m'),c.`type`
		";

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$year,$month
			));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function sales_perproduct($year,$month)
	{
		//$sql 	= 'CALL uom_search_id(?)';
		
		if($month == 'All'){
		$sql 	= "
			SELECT e.`name_eksternal` AS Principle,c.`stock_name` AS Produk,CONCAT(c.`base_qty`,' ',d.`uom_symbol`) Kemasan,
	(SUM(b.`unit_price`)*(f.`convertion`/c.`unit_terkecil`))/f.`convertion` sumoftotal_lt_kg,SUM(b.`price`) totalsumoftotal,
	DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,c.`unit_terkecil`/f.`convertion` AS konversi_rate,g.uom_symbol AS konversi_symbol
	FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom d ON c.`unit_terkecil`=d.`id_uom`
	JOIN t_uom_convert f ON d.`id_uom`=f.`id_uom`
	JOIN m_uom g ON f.`id_uom_parent`=g.id_uom
	JOIN t_eksternal e ON c.`dist_id`=e.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')= ?
	#AND DATE_FORMAT(a.`date_penjualan`,'%M')='?'
	GROUP BY e.`name_eksternal`,c.id_mat,DATE_FORMAT(a.`date_penjualan`,'%Y')
	ORDER BY `kode_eksternal`,c.`stock_name`,c.`base_qty`
		";
		}else{
			$sql 	= "
			SELECT e.`name_eksternal` AS Principle,c.`stock_name` AS Produk,CONCAT(c.`base_qty`,' ',d.`uom_symbol`) Kemasan,
	(SUM(b.`unit_price`)*(f.`convertion`/c.`unit_terkecil`))/f.`convertion` sumoftotal_lt_kg,SUM(b.`price`) totalsumoftotal,
	DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,c.`unit_terkecil`/f.`convertion` AS konversi_rate,g.uom_symbol AS konversi_symbol
	FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom d ON c.`unit_terkecil`=d.`id_uom`
	JOIN t_uom_convert f ON d.`id_uom`=f.`id_uom`
	JOIN m_uom g ON f.`id_uom_parent`=g.id_uom
	JOIN t_eksternal e ON c.`dist_id`=e.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')= ?
	AND DATE_FORMAT(a.`date_penjualan`,'%M')='?'
	GROUP BY e.`name_eksternal`,c.id_mat,DATE_FORMAT(a.`date_penjualan`,'%Y')
	ORDER BY `kode_eksternal`,c.`stock_name`,c.`base_qty`
		";
		}

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$year,$month
			));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
	public function sales_perproductcust($year,$month)
	{
		//$sql 	= 'CALL uom_search_id(?)';
		
		if($month == 'All'){
		$sql 	= "
			SELECT A.*, 
IF(B.totalsumoftotal IS NULL,0,B.sumoftotal_lt_kg) AS sumtot_jan,
IF(B.sumoftotal_lt_kg IS NULL,0,B.sumoftotal_lt_kg) AS sumtotkg_jan,
IF(C.totalsumoftotal IS NULL,0,C.sumoftotal_lt_kg) AS sumtot_feb,
IF(C.sumoftotal_lt_kg IS NULL,0,C.sumoftotal_lt_kg) AS sumtotkg_feb,
IF(D.totalsumoftotal IS NULL,0,D.sumoftotal_lt_kg) AS sumtot_mar,
IF(D.sumoftotal_lt_kg IS NULL,0,D.sumoftotal_lt_kg) AS sumtotkg_mar,
IF(E.totalsumoftotal IS NULL,0,E.sumoftotal_lt_kg) AS sumtot_apr,
IF(E.sumoftotal_lt_kg IS NULL,0,E.sumoftotal_lt_kg) AS sumtotkg_apr,
IF(F.totalsumoftotal IS NULL,0,F.sumoftotal_lt_kg) AS sumtot_may,
IF(F.sumoftotal_lt_kg IS NULL,0,F.sumoftotal_lt_kg) AS sumtotkg_may,
IF(G.totalsumoftotal IS NULL,0,G.sumoftotal_lt_kg) AS sumtot_jun,
IF(G.sumoftotal_lt_kg IS NULL,0,G.sumoftotal_lt_kg) AS sumtotkg_jun,
IF(H.totalsumoftotal IS NULL,0,H.sumoftotal_lt_kg) AS sumtot_jul,
IF(H.sumoftotal_lt_kg IS NULL,0,H.sumoftotal_lt_kg) AS sumtotkg_jul,
IF(I.totalsumoftotal IS NULL,0,I.sumoftotal_lt_kg) AS sumtot_aug,
IF(I.sumoftotal_lt_kg IS NULL,0,I.sumoftotal_lt_kg) AS sumtotkg_aug,
IF(J.totalsumoftotal IS NULL,0,J.sumoftotal_lt_kg) AS sumtot_sep,
IF(J.sumoftotal_lt_kg IS NULL,0,J.sumoftotal_lt_kg) AS sumtotkg_sep,
IF(K.totalsumoftotal IS NULL,0,K.sumoftotal_lt_kg) AS sumtot_oct,
IF(K.sumoftotal_lt_kg IS NULL,0,K.sumoftotal_lt_kg) AS sumtotkg_oct,
IF(L.totalsumoftotal IS NULL,0,L.sumoftotal_lt_kg) AS sumtot_nov,
IF(L.sumoftotal_lt_kg IS NULL,0,L.sumoftotal_lt_kg) AS sumtotkg_nov,
IF(M.totalsumoftotal IS NULL,0,M.sumoftotal_lt_kg) AS sumtot_dec,
IF(M.sumoftotal_lt_kg IS NULL,0,M.sumoftotal_lt_kg) AS sumtotkg_dec

FROM (

SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
SUM(b.`price`) totalsumoftotal,
(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
c.id_mat, d.`id_t_cust`
#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')='2020' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`

) A LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year." 
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='01' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) B ON A.id_mat = B.id_mat AND A.id_t_cust = B.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year."  
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='02' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) C ON A.id_mat = C.id_mat AND A.id_t_cust = C.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year."  
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='03' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) D ON A.id_mat = D.id_mat AND A.id_t_cust = D.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year." 
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='04' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) E ON A.id_mat = E.id_mat AND A.id_t_cust = E.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year." 
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='05' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) F ON A.id_mat = F.id_mat AND A.id_t_cust = F.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year." 
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='06' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) G ON A.id_mat = G.id_mat AND A.id_t_cust = G.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year."  
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='07' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) H ON A.id_mat = H.id_mat AND A.id_t_cust = H.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year."  
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='08' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) I ON A.id_mat = I.id_mat AND A.id_t_cust = I.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year."  
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='09' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) J ON A.id_mat = J.id_mat AND A.id_t_cust = J.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year."  
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='10' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) K ON A.id_mat = K.id_mat AND A.id_t_cust = K.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year."  
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='11' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) L ON A.id_mat = L.id_mat AND A.id_t_cust = L.id_t_cust
LEFT JOIN (
	SELECT c.`stock_name`,d.`cust_name`,CONCAT(c.`unit_box`,' ',f.`uom_symbol`) Kemasan,
	SUM(b.`price`) totalsumoftotal,
	(SUM(b.`unit_price`)*(h.`convertion`/c.`unit_terkecil`))/h.`convertion` sumoftotal_lt_kg,
	c.id_mat, d.`id_t_cust`
	#,DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,DATE_FORMAT(a.`date_penjualan`,'%Y') AS tahun,e.`region`,g.`name_eksternal`,i.uom_symbol
	 FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom f ON c.`unit_terkecil`=f.`id_uom`
	JOIN t_uom_convert h ON f.`id_uom`=h.`id_uom`
	JOIN m_uom i ON h.`id_uom_parent`=i.id_uom
	JOIN t_customer d ON a.`id_customer`=d.`id_t_cust`
	JOIN t_region e ON d.`region`=e.`id_t_region`
	JOIN t_eksternal g ON c.`dist_id`=g.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')=".$year."  
	AND DATE_FORMAT(a.`date_penjualan`,'%m')='12' 
	#AND g.id=824 
	#AND e.`id_t_region`='4'
	GROUP BY c.`stock_name`,d.id_t_cust,DATE_FORMAT(a.`date_penjualan`,'%y')
	ORDER BY c.`stock_name`,`cust_name`
) M ON A.id_mat = M.id_mat AND A.id_t_cust = M.id_t_cust

		";
		}else{
			$sql 	= "
			SELECT e.`name_eksternal` AS Principle,c.`stock_name` AS Produk,CONCAT(c.`base_qty`,' ',d.`uom_symbol`) Kemasan,
	(SUM(b.`unit_price`)*(f.`convertion`/c.`unit_terkecil`))/f.`convertion` sumoftotal_lt_kg,SUM(b.`price`) totalsumoftotal,
	DATE_FORMAT(a.`date_penjualan`,'%M') AS bulan,c.`unit_terkecil`/f.`convertion` AS konversi_rate,g.uom_symbol AS konversi_symbol
	FROM t_penjualan a
	JOIN d_penjualan b ON a.`id_penjualan`=b.`id_penjualan`
	JOIN m_material c ON b.`id_material`=c.`id_mat`
	JOIN m_uom d ON c.`unit_terkecil`=d.`id_uom`
	JOIN t_uom_convert f ON d.`id_uom`=f.`id_uom`
	JOIN m_uom g ON f.`id_uom_parent`=g.id_uom
	JOIN t_eksternal e ON c.`dist_id`=e.`id`
	WHERE DATE_FORMAT(a.`date_penjualan`,'%Y')= ?
	AND DATE_FORMAT(a.`date_penjualan`,'%M')='?'
	GROUP BY e.`name_eksternal`,c.id_mat,DATE_FORMAT(a.`date_penjualan`,'%Y')
	ORDER BY `kode_eksternal`,c.`stock_name`,c.`base_qty`
		";
		}

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$year,$month
			));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_coa(){
		
		$query =  $this->db->select('*')
		->from('t_coa');
		
		
		$query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
		
	}

	public function lists($params = array())
	{
			$query = 	'
				SELECT COUNT(*) AS jumlah FROM t_coa_value a join t_coa b on a.id_coa = b.id_coa where 1 = 1
				AND ( 
					coa LIKE "%'.$params['searchtxt'].'%" OR
					keterangan LIKE "%'.$params['searchtxt'].'%" 
				)
			';
						
			$query2 = 	'
				SELECT z.*, rank() over ( ORDER BY `id` DESC) AS Rangking from ( 
					select a.*,b.coa,b.keterangan FROM t_coa_value a join t_coa b on a.id_coa = b.id_coa
					where 1 = 1  AND ( 
					coa LIKE "%'.$params['searchtxt'].'%" OR
					keterangan LIKE "%'.$params['searchtxt'].'%" 
				)  order by `id` DESC) z
				ORDER BY `id` DESC
				
				LIMIT '.$params['limit'].' 
				OFFSET '.$params['offset'].' 
			';
		
			//echo $query2;die;
		
			 $out		= array();
			  $querys		= $this->db->query($query);
			  $result = $querys->row();
			  
			  $total_filtered = $result->jumlah;
			  $total 			= $result->jumlah;
		  
				if(($params['offset']+10) > $total_filtered){
				$limit_data = $total_filtered - $params['offset'];
			  }else{
				$limit_data = $params['limit'] ;
			  }
		  
		
		  
		  //echo $query;die;
			//echo $query;die;
			 $query2s		= $this->db->query($query2);
		  $result2 = $query2s->result_array();						
		  $return = array(
			  'data' => $result2,
			  'total_filtered' => $total_filtered,
			  'total' => $total,
		  );
		  return $return;
	}
	
	public function edit_uom($data)
	{
		$datas = array(
			'id_coa' => $data['coa'],
			'date' => $data['tgl'],
			'date_insert' => date('Y-m-d H:i:s'),
			'value' => $data['jumlah'],
			'value_real' => $data['jumlah'],
			'type_cash' => $data['cash'],
			'id_valas' => 1,
			'note' => $data['ket']
		);

	//	print_r($datas);die;
		$this->db->where('id',$data['id']);
		$this->db->update('t_coa_value',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_uom($data)
	{
		$datas = array(
			'id_coa' => $data['coa'],
			'date' => $data['tgl'],
			'date_insert' => date('Y-m-d H:i:s'),
			'value' => $data['jumlah'],
			'value_real' => $data['jumlah'],
			'type_cash' => $data['cash'],
			'id_valas' => 1,
			'note' => $data['ket']
		);

	//	print_r($datas);die;

		$this->db->insert('t_coa_value',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	
	public function deletes($id_uom)
	{
		$this->db->where('id', $id_uom);
		$this->db->delete('t_coa_value'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}


}
