<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Purchase_order_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}


	public function list_po($params = array()){
		
		
		if($params['filter'] == 'ALL'){
			$query_m = ' ';
		}else{
			$query_m = ' AND DATE_FORMAT(a.date_po,"%M") = "'.$params['filter'].'"';
		}
		
		if($params['filter_year'] == 'ALL'){
			$query_y = ' ';
		}else{
			$query_y = 'AND DATE_FORMAT(a.date_po,"%Y") = "'.$params['filter_year'].'" ';
		}
		
		$where_sts = $query_m.' '.$query_y;
		
			$query = 	'
				SELECT COUNT(*) AS jumlah FROM t_purchase_order a join t_eksternal b on a.id_distributor = b.id where seq_n <> 8
				'.$where_sts.'
				AND ( 
					no_po LIKE "%'.$params['searchtxt'].'%" OR
					name_eksternal LIKE "%'.$params['searchtxt'].'%" 
				)
			';
						
			$query2 = 	'
				SELECT z.*, rank() over ( ORDER BY no_po DESC,date_po,name_eksternal DESC) AS Rangking from ( 
					select a.*,b.name_eksternal,c.symbol FROM t_purchase_order a 
					join t_eksternal b on a.id_distributor = b.id 
					join m_valas c on a.id_valas = c.valas_id
					where seq_n <> 8 '.$where_sts.'  AND ( 
					no_po LIKE "%'.$params['searchtxt'].'%" OR
					name_eksternal LIKE "%'.$params['searchtxt'].'%" 
				)  order by no_po DESC,date_po,name_eksternal DESC) z
				ORDER BY '.$params['order_column'].' '.$params['order_dir'].'
				
				LIMIT '.$params['limit'].' 
				OFFSET '.$params['offset'].' 
			';
		
			//echo $query2;die;
		
			 $out		= array();
			  $querys		= $this->db->query($query);
			  $result = $querys->row();
			  
			  $total_filtered = $result->jumlah;
			  $total 			= $result->jumlah;
		  
				if(($params['offset']+10) > $total_filtered){
				$limit_data = $total_filtered - $params['offset'];
			  }else{
				$limit_data = $params['limit'] ;
			  }
		  
		
		  
		  //echo $query;die;
			//echo $query;die;
			 $query2s		= $this->db->query($query2);
		  $result2 = $query2s->result_array();						
		  $return = array(
			  'data' => $result2,
			  'total_filtered' => $total_filtered,
			  'total' => $total,
		  );
		  return $return;
		
		
	}
	
	
	public function get_principal($params = array()){
		
		$query = $this->db->get_where('t_eksternal', array('type_eksternal' => 1));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}			
	
	public function get_price_mat($params = array()){
		
		// $query = $this->db->get_where('t_po_mat', array('id_po' => $params['kode']));
		// $query = $this->db->order_by('id_t_ps', 'desc');
		
		// $this->db->select('*')
		 // ->from('t_po_mat')
		 // ->where(array('id_material' => $params['kode']))
		 // ->order_by('id_t_ps', 'desc');
		 // $query = $this->db->get();
		 
		 $this->db->select('*')
		 ->from('m_material')
		 ->where(array('id_mat' => $params['kode']));
		 $query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}		
	
	public function get_principal_byid($params = array()){
		
		$query = $this->db->get_where('t_eksternal', array('id' => $params['id_prin']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
		
		
	public function get_item_byprin($params = array()){
		$this->db->select('m_material.*, m_uom.uom_symbol')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('m_material')
        ->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
		->where(array('dist_id' => $params['id_prin']))
		->order_by('m_material.stock_name');
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function get_item_byprin_all($params = array()){
		$this->db->select('m_material.*, m_uom.uom_symbol')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('m_material')
        ->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
		//->where(array('dist_id' => $params['id_prin']));
		->where('m_material.dist_id != ',$params['id_prin'],FALSE);
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function get_item_byprin_all2($params = array()){
		$this->db->select('m_material.*, m_uom.uom_symbol')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('m_material')
        ->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom');
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function get_kode($params = array()){
		
		$query =  $this->db->select('seq_max as max')
		->from('t_ordering')
		->where('nama_menu','po');
		;
		
		$query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function get_po($params = array()){
		
		// $query = $this->db->get_where('t_purchase_order', array('id_po' => $params['id']));
		$this->db->select('t_purchase_order.*, t_eksternal.*')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('t_purchase_order')
        ->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id')
		->where(array('id_po' => $params['id']));
		$query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_po_detail($params = array()){
		
		$query = $this->db->get_where('t_po_mat', array('id_po' => $params['id']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function get_po_detail2($params = array()){
		
	$query = "
		SELECT b.*,IF(a.id_distributor = c.dist_id,0,1) AS all_item FROM `t_purchase_order` a
		JOIN `t_po_mat` b ON a.`id_po` = b.`id_po`
		JOIN m_material c ON b.id_material = c.id_mat
		JOIN m_uom d ON c.unit_terkecil = d.id_uom
		WHERE a.`id_po` = ".$params['id']."

		";
		
		$query    = $this->db->query($query);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
		
	public function get_po_detail_full($params = array()){
		
		//$query = $this->db->get_where('t_po_mat', array('id_po' => $params['id']));
		$this->db->select('t_po_mat.*,t_po_mat.qty as qty_order, m_material.*, m_uom.*')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('t_po_mat')
        ->join('m_material', 't_po_mat.id_material = m_material.id_mat')
        ->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
		->where(array('id_po' => $params['id']));
		$query = $this->db->get();
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function check_principal($params = array()){
		
		$query = $this->db->get_where('t_eksternal', array('kode_eksternal' => $params['kode']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_po($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'no_po' => $data['no_po'],
			'date_po' => $data['date_po'],
			'id_distributor' => $data['id_distributor'],
			'ppn' => $data['ppn'],
			'term_of_payment' => $data['term_of_patyment'],
			'status' => 0,
			'id_valas' => 1,
			'rate' => 1,
			'seq_n' => 0,
			'keterangan' => $data['keterangan'],
			'total_amount' => $data['total_amount']
		);

	//	print_r($datas);die;

		$this->db->insert('t_purchase_order',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function add_po_mat($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'id_po' => $data['id_po'],
			'id_material' => $data['id_material'],
			'unit_price' => $data['unit_price'],
			'price' => $data['price'],
			'remarks' => $data['remark'],
			'qty' => $data['qty'],
			'diskon' => $data['diskon']
		);

	//	print_r($datas);die;

		$this->db->insert('t_po_mat',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function edit_po($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			//'no_po' => $data['no_po'],
			'date_po' => $data['date_po'],
			'id_distributor' => $data['id_distributor'],
			'ppn' => $data['ppn'],
			'term_of_payment' => $data['term_of_patyment'],
			'keterangan' => $data['keterangan'],
			'status' => 0,
			'id_valas' => 1,
			'rate' => 1,
			'total_amount' => $data['total_amount']
		);

	//	print_r($datas);die;
		$this->db->where('id_po',$data['id_po']);
		$this->db->update('t_purchase_order',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}	
	
	public function update_seq($data) {
		$sql 	= 'update t_ordering set seq_max = seq_max+1 where nama_menu = "'.$data.'" ';

			
		$query 	= $this->db->query($sql);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function edit_po_apr($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'status' => $data['sts']
		);

	//	print_r($datas);die;
		$this->db->where('id_po',$data['id_po']);
		$this->db->update('t_purchase_order',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	function delete_po($id) {

		$this->db->where('id_po', $id);
		$this->db->delete('t_purchase_order'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		

		return $result;
	}	
	
	function delete_po_mat($id) {

		$this->db->where('id_po', $id);
		$this->db->delete('t_po_mat'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();
		

		return $result;
	}

}