<style>
	.dt-body-left {text-align:left;}
	.dt-body-right {text-align:right;}
	.dt-body-center {text-align:center; vertical-align: middle;}
	.force-overflow {height: 650px; overflow-y: auto;overflow-x: auto}
	.scroll-overflow {min-height: 650px}
	#modal-distributor::-webkit-scrollbar-track
	{
		-webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar
	{
		width: 10px;
		background-color: #F5F5F5;
	}

	#modal-distributor::-webkit-scrollbar-thumb
	{
		background-image: -webkit-gradient(linear,
		   left bottom,
		   left top,
		   color-stop(0.44, rgb(122,153,217)),
		   color-stop(0.72, rgb(73,125,189)),
		   color-stop(0.86, rgb(28,58,148)));
	}
	
	.select2-container .select2-choice {
		
		height : 35px !important;
		
	}

</style>

 <div class="product-sales-area mg-tb-30">
            <div class="container-fluid">
                        <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="product-payment-inner-st">
                            <ul id="myTabedu1" class="tab-review-design">
                                <li class="active"><a href="#description">Update Penjualan</a></li>
                            </ul>
                            <div id="myTabContent" class="tab-content custom-product-edit">
                                <div class="product-tab-list tab-pane fade active in" id="description">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="review-content-section">
                                                <form id="edit_po" action="<?php echo base_url().'selling/edit_po';?>" class="add-department" autocomplete="off">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<div class="form-group">
																<label>Kode</label>
                                                                <input name="kode" id="kode" type="text" class="form-control" placeholder="Kode Purchase Order" value="<?php echo $po['no_penjualan']; ?>" readOnly>
                                                            </div>
															<div class="form-group date">
																<label>Tanggal</label>
																<input name="tgl" id="tgl" type="text" class="form-control" placeholder="Tanggal Purchase Order" value="<?php echo $po['date_penjualan']; ?>" >
                                                            </div>
                                                            <div class="form-group">
																<label>Customer</label>
																<input name="ints" id="ints" type="hidden" class="form-control" placeholder="Tanggal Purchase Order" value= '<?php echo count($po_mat); ?>' >
																<input name="ints2" id="ints2" type="hidden" class="form-control" placeholder="Tanggal Purchase Order" value= '<?php echo count($po_mat); ?>' >
                                                                <select name="name" id="name" onChange="select_prin()" class="form-control" placeholder="Nama Principal">
																	<option value="0" selected="selected" disabled>-- Pilih Principal --</option>
																	<?php
																		foreach($principal as $principals){
																			
																			if($principals['id_t_cust'] == $po['id_customer']){
																				echo '<option value="'.$principals['id_t_cust'].'" selected="selected" >'.$principals['cust_name'].'</option>';
																			}else{
																				echo '<option value="'.$principals['id_t_cust'].'" >'.$principals['cust_name'].'</option>';
																			}
																			
																		}
																	?>
																</select>
                                                            </div>
                                                            <div class="form-group">
																<label>Alamat</label>
                                                                <textarea name="alamat" id="alamat"  type="text" class="form-control" placeholder="Alamat Principal" readOnly><?php echo $po['cust_address']; ?></textarea>
                                                            </div>
                                                            <div class="form-group">
																<label>Att</label>
                                                                <input name="att" id="att"  type="text" class="form-control" placeholder="Att" value="<?php echo ''; ?>" readOnly>
                                                            </div><div class="form-group">
																<label>No Telp</label>
                                                                <input name="telp" id="telp"  type="text" class="form-control" placeholder="Telephone" value="<?php echo $po['phone']; ?>" readOnly>
                                                            </div>
															 <div class="form-group">
																<label>Email</label>
                                                                <input name="email" id="fax"  type="text" class="form-control" placeholder="Fax" value="<?php echo $po['email']; ?>" readOnly>
                                                            </div>
															<div class="form-group">
																<label>Limit Kredit</label>
                                                                <input name="limit" id="limit"  type="text" class="form-control" placeholder="Limit Kredit" value="<?php echo number_format($po['credit_limit'],0,',','.'); ?>" readOnly>
                                                            </div>
															<div class="form-group">
																<label>Sisa Kredit</label>
                                                                <input name="sisa" id="sisa"  type="text" class="form-control" placeholder="Sisa Kredit" value="<?php echo number_format($po['sisa_credit'],0,',','.'); ?>" readOnly>
                                                            </div>
                                                        </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<div class="form-group">
																<label>Ship To:</label>
                                                                 <textarea name="alamat_kirim" id="alamat_kirim"  type="text" class="form-control" placeholder="Alamat Principal" >JL. Sangkuriang NO.38-A 
NPWP: 01.555.161.7.428.000</textarea>
                                                            </div>
															<div class="form-group date">
																<label>Email</label>
																<input name="email" id="email" type="email" class="form-control" placeholder="Email" >
                                                            </div>
															<div class="form-group date">
																<label>Telp/Fax</label>
																<input name="telpfax" id="telpfax" type="text" class="form-control" placeholder="Telp/Fax" value="(022) 6626-946" >
                                                            </div>
															<div class="form-group date">
																<label>VAT/PPN</label>
																<input name="ppn" id="ppn" type="text" class="form-control" placeholder="VAT/PPN" onKeyup="change_ppn()" value="<?php echo $po['ppn']; ?>" >
                                                            </div>
															<div class="form-group date">
																<label>Term Pembayaran</label>
																  <select name="term" id="term" class="form-control" placeholder="Nama Principal" onChange="term_other()" >
																	<option value="h" selected="selected" disabled>-- Pilih Term --</option>
																	<option value="0" <?php if($po['term_of_payment'] == 0){ echo "selected='selected'"; } ?> >Cash</option>
																	<option value="15" <?php if($po['term_of_payment'] == 15){ echo "selected='selected'"; } ?> >15 Hari</option>
																	<option value="30" <?php if($po['term_of_payment'] == 30){ echo "selected='selected'"; } ?> >30 Hari</option>
																	<option value="45" <?php if($po['term_of_payment'] == 45){ echo "selected='selected'"; } ?> >45 Hari</option>
																	<option value="60" <?php if($po['term_of_payment'] == 60){ echo "selected='selected'"; } ?> >60 Hari</option>
																	<option value="90" <?php if($po['term_of_payment'] == 90){ echo "selected='selected'"; } ?> >90 Hari</option>
																	<option value="other" <?php $people = array(0,15,30,45,60,90);
																	if (!in_array($po['term_of_payment'], $people)){ echo "selected='selected'"; } ?> >Lainnya</option>
																</select>
                                                            </div>
															
															<div class="form-group date">
																<label>Term Pembayaran Lain</label> 
																<input name="term_o" id="term_o" type="number" class="form-control" placeholder="Term (Hari)" 
																<?php $people = array(0,15,30,45,60,90);
																	if (in_array($po['term_of_payment'], $people)){ echo 'value="0" style="" readonly >'; }else{ echo 'value="'.$po['term_of_payment'].'" style="" readonly  > '; } 
																?> 
																
                                                            </div>
															
															<div class="form-group date">
																<label>Keterangan</label>
																<textarea name="ket" id="ket" class="form-control" placeholder="keterangan"  ><?php echo $po['keterangan']; ?></textarea>
                                                            </div>
															
                                                    </div>
													</div>

                                                    
                                               
                                            </div>
                                        </div>
                                    </div>
									<br><br>
									<div class="row">
										<h3>Items</h3>
									</div>
									
											<div class="row">
											<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Kode</label>
												</div>
											</div>
											<div class="col-lg-2 col-md-1 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Qty Box</label>
												</div>
											</div>
											<div class="col-lg-1 col-md-2 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Qty Satuan</label>
												</div>
											</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Total Qty</label>
												</div>
											</div>
											
											<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Harga</label>
												</div>
											</div>
										
											<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
												<div class="form-group">
													<label>Total</label>
												</div>
											</div>
											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
												<div class="form-group">
													<label></label>
												</div>
											</div>
										</div>
											<input name="int_flo" id="int_flo" type="hidden" class="form-control" placeholder="Qty" value=<?php echo count($po_mat); ?>; >
									<div id="table_items">
									
										<?php $total_sub = 0;$total_sub_all = 0;$total_disk = 0; $int_row = 0; foreach($po_mat as $po_mats){ ?>
									
										<div id='row_<?php echo $int_row; ?>' >
											<div class="row" style="border-top-style:solid;">
											<div style="margin-top:10px">
												<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
														<select name="kode_<?php echo $int_row; ?>" id="kode_<?php echo $int_row; ?>" class="form-control" placeholder="Nama Principal">
															<option value="0" selected="selected" disabled>-- Pilih Item --</option>
															<?php foreach($item as $items){ if($po_mats['id_material'] == $items['id_mat']){ $selc = "selected='selected'"; }else{ $selc = ''; } echo '<option value="'.$items['id_mat'].'|'.$items['unit_box'].'|'.$items['bottom_price'].'|'.$items['qty'].'" '.$selc.' >'.$items['stock_code'].' - '.$items['stock_name'].' - '.$items['base_qty'].' '.$items['uom_symbol'].'</option>'; } ?>
														</select>
											
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<div class="form-group">
														<input name="qty_<?php echo $int_row; ?>" id="qty_<?php echo $int_row; ?>" type="text" onKeyup="change_sum(<?php echo $int_row; ?>)" class="form-control rupiahs" placeholder="Qty" value=<?php echo number_format($po_mats['qty_box'],0,',','.'); ?>  >
													</div>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<div class="form-group">
														<label id="iteml_<?php echo $int_row; ?>">Box @ <?php echo number_format($po_mats['box_ammount'],0,',','.'); ?><br>Sisa <?php echo number_format($po_mats['kkk'],0,',','.'); ?></label>
														<label id="iteml2_<?php echo $int_row; ?>"></label>
													</div>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<div class="form-group">
														<input name="qtys_<?php echo $int_row; ?>" id="qtys_<?php echo $int_row; ?>" type="text" onKeyup="change_sum(<?php echo $int_row; ?>)" class="form-control rupiahs" placeholder="Qty" value=<?php echo number_format($po_mats['qty_satuan'],0,',','.'); ?>   >
													</div>
												</div>
													<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<div class="form-group">
														<input name="totalqty_<?php echo $int_row; ?>" id="totalqty_<?php echo $int_row; ?>" type="number"  class="form-control" placeholder="Diskon" value=<?php echo number_format($po_mats['qty'],0,',','.'); ?> readOnly >
													</div>
												</div>
												<div class="col-lg-2 col-md-1 col-sm-3 col-xs-12">
													<div class="form-group">
														<input name="harga_<?php echo $int_row; ?>" id="harga_<?php echo $int_row; ?>" type="text" onKeyup="change_sum(<?php echo $int_row; ?>)" class="form-control rupiahs" placeholder="Harga" value=<?php echo number_format($po_mats['unit_price'],0,',','.'); ?> >
													</div>
												</div>
												<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
													<div class="form-group">
														<input name="total_<?php echo $int_row; ?>" id="total_<?php echo $int_row; ?>" type="text" class="form-control" placeholder="Total" value=<?php echo number_format(($po_mats['price']-($po_mats['price']*($po_mats['diskon']/100))),0,',','.'); ?> readOnly  >
													</div>
												</div>
												
												<?php if($int_row <> 0){ ?>
												
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<div class="form-group">
													<button type="button" class="btn btn-danger waves-effect waves-light " onClick="kurang_row(<?php echo $int_row; ?>)">-</button>
													</div>
												</div>
												
												<?php } ?>
											</div>
											</div>
										</div>
										
										<?php //$total_sub = $total_sub + number_format(($po_mats['price']-($po_mats['price']*($po_mats['diskon']/100))),0,',',''); ?>
										<?php $total_sub_all = $total_sub_all + number_format(($po_mats['price']),0,',',''); ?>
										<?php //$total_disk = $total_disk + ($po_mats['price']*($po_mats['diskon']/100)); ?>
										
										<?php $int_row++; } ?>
										
									</div>
									<div class="row" style="border-top-style:solid;" id='plus' >
									<br>
										<div class="col-lg-11 col-md-11 col-sm-3 col-xs-12">
										</div>
											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
											
													<div class="form-group">
														<button type="button" class="btn btn-danger waves-effect waves-light tbh_btn" onClick="tambah_row()">+</button>
													</div>
												</div>
									</div>
									<br>
									<div class="row" style="border-top-style:solid;">
											<div style="margin-top:10px">
												<div class="col-lg-8 col-md-3 col-sm-3 col-xs-12">
													
												</div>
												<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
													<label>Total</label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<label id="grand_total" style="float:right"><?php echo number_format($total_sub_all,2,',','.'); ?></label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
												</div>
											</div>
									</div>
									<div class="row" style="">
											<div style="margin-top:10px">
												<div class="col-lg-8 col-md-3 col-sm-3 col-xs-12">
													
												</div>
												<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
													<label>DPP</label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<label id='subttl2' style="float:right"><?php echo number_format($total_sub_all - ($total_sub_all/11),2,',','.'); ?></label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
												</div>
											</div>
									</div>
									<div class="row" style="">
											<div style="margin-top:10px">
												<div class="col-lg-8 col-md-3 col-sm-3 col-xs-12">
													
												</div>
												<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">
													<label>VAT/PPN</label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
													<label id="vatppn" style="float:right"><?php echo number_format($total_sub_all/11,2,',','.'); ?></label>
												</div>
												<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">
												</div>
											</div>
									</div>
									<div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="payment-adress">
                                                                <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
																<button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()"> Batal</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                </div>
                               </div> </form>
                        </div>
                    </div>
                </div>
</div>
</div>

                        <div id="ModalAddPo" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-close-area modal-close-df">
                                        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                                    </div>
                                    <div class="modal-body">
                                        <i class="educate-icon educate-checked modal-check-pro"></i>
                                        <h2>Data Berhasil Dirubah</h2>
                                        <p></p>
                                    </div>
                                    <div class="modal-footer">
                                        <a data-dismiss="modal" href="#" onClick="back()">Ok</a>
                                        <!--<a data-dismiss="modal" href="#" onClick="clearform()" >Ya</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
						
						<div id="ModalChangePrincipal" class="modal modal-edu-general Customwidth-popup-WarningModal PrimaryModal-bgcolor fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-close-area modal-close-df">
                                        <!--<a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>-->
                                    </div>
                                    <div class="modal-body">
                                        <span class="educate-icon educate-warning modal-check-pro information-icon-pro"></span>
                                        <h2>Warning!</h2>
                                        <p id="msg_err">The Modal plugin is a dialog box/popup window that is displayed on top of the current page</p>
                                    </div>
                                    <div class="modal-footer footer-modal-admin warning-md">
                                        <a data-dismiss="modal" href="#" onClick="okchange()">Ok</a>
                                        <a data-dismiss="modal" href="#" onClick="cancelchange()">Batal</a>
                                    </div>
                                </div>
                            </div>
                        </div>

<script>

function  change_kode(rs){
	
	  var datapost = {
		"kode": $('#kode_'+rs+'').val()
      };
	
	$.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'selling/get_price_mat'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
			
			$('#harga_'+rs+'').val(response.list);
			
	    }
    });
}

function change_item(rs){
	
	
	
	var val = $('#kode_'+rs+'').val();
	
	var sss = val.split('|');
	
	//alert(val);
	
	var harga = new Intl.NumberFormat('de-DE', {}).format(sss[2]);
			
	
	$('#iteml_'+rs+'').html("Box @ "+sss[1]+"<br>Stock :"+qty_s);
	//$('#harga_'+rs+'').val(harga);
	
	change_sum(rs);
	
	
}

function lainnya(int_fal){
	
	 var datapost = {
		"id_prin": $('#name').val()
      };
	
	  $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'selling/get_all_item'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {

			$('#kode_'+int_fal).html(response.list);
			
			$('#kode_'+int_fal).select2();
        }
      });
	
}

function tambah_row(){
	
	$('.hapus_btn').hide();
	var values = $('#int_flo').val();
	
	var new_fl = parseInt(values)+1;
	
	$('#int_flo').val(new_fl);
   var datapost = {
		"id_t_cust": $('#name').val()
      };

      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'selling/get_customer'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {

		var htl = '';
		htl +=	'<div id="row_'+new_fl+'" >';
		htl +=	'									<div class="row" style="border-top-style:solid;">';
		htl +=	'									<div style="margin-top:10px">';
		htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
		htl +=	'												<select onChange="change_item('+new_fl+')" name="kode_'+new_fl+'" id="kode_'+new_fl+'" class="form-control" placeholder="Nama Item">';
		htl +=	'													<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		htl +=	'												</select>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="qty_'+new_fl+'" id="qty_'+new_fl+'" type="text" onKeyup="change_sum('+new_fl+')" class="form-control rupiahs" placeholder="Qty" value=0  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<label id="iteml_'+new_fl+'"></label><label id="iteml2_'+new_fl+'"></label>';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="qtys_'+new_fl+'" id="qtys_'+new_fl+'" type="text" onKeyup="change_sum('+new_fl+')" class="form-control rupiahs" placeholder="Qty" value=0  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="totalqty_'+new_fl+'" id="totalqty_'+new_fl+'" type="number"  class="form-control" placeholder="Diskon" value=0 readOnly >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="harga_'+new_fl+'" id="harga_'+new_fl+'" type="text" onKeyup="change_sum('+new_fl+')" class="form-control rupiahs" placeholder="Harga" value=0 >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="total_'+new_fl+'" id="total_'+new_fl+'" type="text" class="form-control rupiahs" placeholder="Total" value=0 readOnly  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<button type="button" class="btn btn-danger waves-effect waves-light " onClick="kurang_row('+new_fl+')">-</button>';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'									</div>';
		htl +=	'									</div>';
		htl +=	'								</div>';
	
		$('#table_items').append(htl);
			
			$('#kode_'+new_fl+'').html(response.list);
			
			$('#kode_'+new_fl).select2();
			//$("kode_'+new_fl).css("background-color", "yellow");
			
			$(".rupiahs").inputFilter(function(value) {
			return /^-?\d*[,]?\d*$/.test(value); 
			});
			
			$('.rupiah').priceFormat({
          		prefix: '',
          		centsSeparator: ',',
          		centsLimit: 0,
          		thousandsSeparator: '.'
        	});	
			
        }
      });
	
}

function cancelchange(){
	
	var prin = $('#ints2').val();
	$('#ints').val(prin);
	$('#name').val(prin);
	
}

// function okchange(){

	// $('#edit_po').trigger("reset");
	// $('#term').val("h");
	// $('#ppn').val("10");
	// $('#tgl').val("");
	// $('#subttl').html("0");
	// $('#subttl2').html("0");
	// $('#subttl3').html("0");
	// $('#vatppn').html("0");
	// $('#grand_total').html("0");
	

	   // var datapost = {
		// "id_prin": $('#name').val()
      // };

      // $.ajax({
        // type: 'POST',
        // url: "<?php echo base_url() . 'selling/get_principal'; ?>",
        // data: JSON.stringify(datapost),
        // cache: false,
        // contentType: false,
        // processData: false,
        // success: function(response) {
			// //var obj = JSON.parse(response);
			// $('#alamat').val(response['principal'][0].eksternal_address);
			// $('#att').val(response['principal'][0].pic);
			// $('#telp').val(response['principal'][0].phone_1);
			// $('#fax').val(response['principal'][0].fax);
			// //console.log(response.phone_1);
			
		// $('#table_items').html('')	
		
		// var htl = '';
		// htl +=	'<div id="row_0" >';
		// htl +=	'									<div class="row" style="border-top-style:solid;">';
		// htl +=	'									<div style="margin-top:10px">';
		// htl +=	'										<div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">';
		// htl +=	'											<div class="form-group">';
		// htl +=	'												<select name="kode_0" id="kode_0" class="form-control list_code" placeholder="Nama Principal" onChange="change_kode(0)">';
		// htl +=	'													<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		// htl +=	'												</select>';
		// htl +=	'											</div>';
		// htl +=	'										</div>';
		// htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		// htl +=	'											<div class="form-group">';
		// htl +=	'												<input name="qty_0" id="qty_0" type="text" class="form-control rupiahs" onKeyup="change_sum(0)"  placeholder="Qty" value=0 >';
		// htl +=	'											</div>';
		// htl +=	'										</div>';
		// htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		// htl +=	'											<div class="form-group">';
		// htl +=	'												<input name="harga_0" id="harga_0" type="text" class="form-control rupiahs" onKeyup="change_sum(0)" placeholder="Harga" value=0 >';
		// htl +=	'											</div>';
		// htl +=	'										</div>';
		// htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		// htl +=	'											<div class="form-group">';
		// htl +=	'												<input name="diskon_0" id="diskon_0" type="number" class="form-control" onKeyup="change_sum(0)" placeholder="Diskon" value=0 >';
		// htl +=	'											</div>';
		// htl +=	'										</div>';
		// htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		// htl +=	'											<div class="form-group">';
		// htl +=	'												<input name="total_0" id="total_0" type="text" class="form-control rupiahs" placeholder="Total" readOnly value=0 >';
		// htl +=	'											</div>';
		// htl +=	'										</div>';
		// htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		// htl +=	'											<div class="form-group">';
		// //htl +=	'												<button type="button" class="btn btn-danger waves-effect waves-light " onClick="kurang_row()">-</button>';
		// htl +=	'											</div>';
		// htl +=	'										</div>';
		// htl +=	'									</div>';
		// htl +=	'									</div>';
		// htl +=	'								</div>';
	
		// $('#table_items').append(htl);
			
			// $('#kode_0').html(response.list);
			
			// $('#kode_0').select2();
			
			// $('.rupiah').priceFormat({
          		// prefix: '',
          		// centsSeparator: '',
          		// centsLimit: 0,
          		// thousandsSeparator: '.'
        	// });	
			
        // }
      // });
	  
// }


function select_prin(){
	
	var prin = $('#ints').val();
	var prin2 = $('#name').val();
	
	$('#ints2').val(prin);
//	alert(prin2);
	$('#ints').val(prin2);
		
	  var datapost = {
		"id_t_cust": $('#name').val()
      };

      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'selling/get_principal'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
			//var obj = JSON.parse(response);
			$('#alamat').val(response['customer'][0].cust_address);
			$('#att').val(response['customer'][0].contact_person);
			$('#telp').val(response['customer'][0].phone);
			$('#email').val(response['customer'][0].email);
			//console.log(response.phone_1);
			var credit_limits = new Intl.NumberFormat('de-DE', {}).format(response['customer'][0].credit_limit);
			
			$('#limit').val(credit_limits);
			
			var sisa_credits = new Intl.NumberFormat('de-DE', {}).format(response['customer'][0].sisa_credit);
			
			$('#sisa').val(sisa_credits);
			
			$('#table_items').html('')	
		
		var htl = '';
		htl +=	'<div id="row_0" >';
		htl +=	'									<div class="row" style="border-top-style:solid;">';
		htl +=	'									<div style="margin-top:10px">';
		htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
		htl +=	'												<select onChange="change_item(0)" name="kode_0" id="kode_0" class="form-control" placeholder="Nama Principal">';
		htl +=	'													<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		htl +=	'												</select>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="qty_0" id="qty_0" type="text" onKeyup="change_sum(0)" class="form-control rupiahs" placeholder="Qty" value=0  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<label id="iteml_0"></label><label id="iteml2_0"></label>';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="qtys_0" id="qtys_0" type="text" onKeyup="change_sum(0)" class="form-control rupiahs" placeholder="Qty" value=0  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="totalqty_0" id="totalqty_0" type="number"  class="form-control" placeholder="Diskon" value=0 readOnly >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="harga_0" id="harga_0" type="text" onKeyup="change_sum(0)" class="form-control rupiahs" placeholder="Harga" value=0 >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="total_0" id="total_0" type="text" class="form-control rupiahs" placeholder="Total" value=0 readOnly  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'									</div>';
		htl +=	'									</div>';
		htl +=	'								</div>';
		
			$('#table_items').append(htl);
			$('#kode_0').html(response.list);
			
			$('#kode_0').select2();
			
			$(".rupiahs").inputFilter(function(value) {
			return /^-?\d*[,]?\d*$/.test(value); 
			});
			
			 $('.rupiah').priceFormat({
          		prefix: '',
          		centsSeparator: '',
          		centsLimit: 0,
          		thousandsSeparator: '.'
        	});	
			
        }
      });
		
	
	//$('#ints').val('1');
	
}

function kurang_row(new_fl){
	
	// $('#row_'+new_fl).remove();
	// change_sum_all();
		$('#row_'+new_fl).remove();
	
		var int_val = parseInt($('#int_flo').val());
	
	var total_all = 0;
	
	for(var yo = 0;yo <= int_val;yo++ ){
				
		if($('#kode_'+yo).val() !== undefined){
			
			var qty = 	$('#qty_'+yo).val();
			var kode = 	$('#kode_'+yo).val();
			var qtys = 	$('#qtys_'+yo).val();
			var harga = $('#harga_'+yo).val();
			
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace(',','.');
			
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');

			//var val = $('#kode_'+rs+'').val();
			
			var sss = kode.split('|');
			
			//alert(qtys);
			if(qtys == ""){
				var qtys = 0;
			}else{
				var qtys = qtys;
			}
			
			if(qty == ""){
				var qty = 0;
			}else{
				var qty = qty;
			}
			
			var total_qty = ( parseInt(qty)*parseInt(sss[1]) ) + parseInt(qtys);
			
			var total_harga = total_qty * parseFloat(harga);
			//var total_harga = new Intl.NumberFormat('de-DE', {}).format(total_harga);
			
			total_all = total_all + total_harga;
			
		}
				
	}
	
	//alert(total_all);
	
	var gt = new Intl.NumberFormat('de-DE', {}).format(total_all.toFixed(2));
	$('#grand_total').html(gt);
	
	var ppn = total_all/11;
	var ppns = new Intl.NumberFormat('de-DE', {}).format(ppn.toFixed(2));
	$('#vatppn').html(ppns);
	
	var dpp = total_all - ppn;
	var dpps = new Intl.NumberFormat('de-DE', {}).format(dpp.toFixed(2));
	$('#subttl2').html(dpps);
	
	
}

function clearform(){
	
	$('#edit_po').trigger("reset");
	$('#term').val("h");
	$('#ppn').val("10");
	$('#tgl').val("");
	$('#subttl').html("0");
	$('#subttl2').html("0");
	$('#tvatppngl').html("0");
	$('#grand_total').html("0");
	$('#vatppn').html("0");
	$('#subttl3').html("0");
	
	$('#table_items').html('')	
		
			var htl = '';
		htl +=	'<div id="row_0" >';
		htl +=	'									<div class="row" style="border-top-style:solid;">';
		htl +=	'									<div style="margin-top:10px">';
		htl +=	'										<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">';
		htl +=	'												<select name="kode_0" onKeyup="change_sum(0)" id="kode_0" class="form-control" placeholder="Nama Principal">';
		htl +=	'													<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		htl +=	'												</select>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="qty_0" id="qty_0" type="text" onKeyup="change_sum(0)" class="form-control rupiahs" placeholder="Qty" value=0  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<label id="iteml_0"></label><label id="iteml2_0"></label>';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="qtys_0" id="qtys_0" type="text" onKeyup="change_sum(0)" class="form-control rupiahs" placeholder="Qty" value=0  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'											<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="totalqty_0" id="totalqty_0" type="number"  class="form-control" placeholder="Diskon" value=0 readOnly >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="harga_0" id="harga_0" type="text" onKeyup="change_sum(0)" class="form-control rupiahs" placeholder="Harga" value=0 >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-2 col-md-2 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<input name="total_0" id="total_0" type="text" class="form-control rupiahs" placeholder="Total" value=0 readOnly  >';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'										<div class="col-lg-1 col-md-1 col-sm-3 col-xs-12">';
		htl +=	'											<div class="form-group">';
		htl +=	'												<button type="button" class="btn btn-danger waves-effect waves-light " onClick="kurang_row()">-</button>';
		htl +=	'											</div>';
		htl +=	'										</div>';
		htl +=	'									</div>';
		htl +=	'									</div>';
		htl +=	'								</div>';
		
			$('#table_items').append(htl);
			
			$('#kode_0').select2();
			
			$(".rupiahs").inputFilter(function(value) {
			return /^-?\d*[,]?\d*$/.test(value); 
			});
			
			 $('.rupiah').priceFormat({
          		prefix: '',
          		centsSeparator: '',
          		centsLimit: 0,
          		thousandsSeparator: '.'
        	});	
	
}

function change_sum(fl){
	
	var qty = 	$('#qty_'+fl).val();
	var kode = 	$('#kode_'+fl).val();
	var qtys = 	$('#qtys_'+fl).val();
	var harga = $('#harga_'+fl).val();
	
	var harga = harga.replace('.','');
	var harga = harga.replace('.','');
	var harga = harga.replace('.','');
	var harga = harga.replace('.','');
	var harga = harga.replace('.','');
	var harga = harga.replace(',','.');
	
	var qty = qty.replace('.','');
	var qty = qty.replace('.','');
	var qty = qty.replace('.','');
	var qty = qty.replace('.','');
	var qty = qty.replace('.','');
	
	var qtys = qtys.replace('.','');
	var qtys = qtys.replace('.','');
	var qtys = qtys.replace('.','');
	var qtys = qtys.replace('.','');
	var qtys = qtys.replace('.','');

	//var val = $('#kode_'+rs+'').val();
	
	var sss = kode.split('|');
	
	//alert(qtys);
	if(qtys == ""){
		var qtys = 0;
	}else{
		var qtys = qtys;
	}
	
	if(qty == ""){
		var qty = 0;
	}else{
		var qty = qty;
	}
	
	var total_qty = ( parseInt(qty)*parseInt(sss[1]) ) + parseInt(qtys);
	
	if(total_qty > parseInt(sss[3])){
		
		$('#iteml2_'+fl).html('<span style="color:red">Stok Tidak Cukup</span>');
		
	}else{
		$('#iteml2_'+fl).html('');
	}
	
	var total_harga = total_qty * parseFloat(harga);
	var total_harga = new Intl.NumberFormat('de-DE', {}).format(total_harga);
	
	$('#totalqty_'+fl+'').val(total_qty);
	$('#total_'+fl+'').val(total_harga);
	
	// if(total_qty > parseInt(sss[3]) ){
		
		
		
	// }
	
	// var diskon = diskon.replace('.','');
	// var diskon = diskon.replace('.','');
	// var diskon = diskon.replace('.','');
	// var diskon = diskon.replace('.','');

	// var qty = qty.replace('.','');
	// var qty = qty.replace('.','');
	// var qty = qty.replace('.','');
	// var qty = qty.replace('.','');
	// //alert(harga);
	
	
	//var total = (parseFloat(qty) * parseFloat(harga) )- ((parseFloat(qty) * parseFloat(harga))*(parseFloat(diskon)/100)); 
	// var totals = new Intl.NumberFormat('de-DE', { }).format(total);
	
	
	// if( isNaN(total)){
		// //var total_num = Number((total).toFixed(1)).toLocaleString();
		// //$('#subttl').html('0');
		// $('#total_'+fl).val('0');
	// }else{
		// //var total_num = Number((total).toFixed(1)).toLocaleString();
		// //$('#subttl').html(total);
		// $('#total_'+fl).val(totals);
	// }
	
	// var sub_total = 0;
	// var sub_total_bd = 0;
	// var sub_disk = 0;
	var int_val = parseInt($('#int_flo').val());
	
	var total_all = 0;
	
	for(var yo = 0;yo <= int_val;yo++ ){
				
		if($('#kode_'+yo).val() !== undefined){
			
			var qty = 	$('#qty_'+yo).val();
			var kode = 	$('#kode_'+yo).val();
			var qtys = 	$('#qtys_'+yo).val();
			var harga = $('#harga_'+yo).val();
			
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace('.','');
			var harga = harga.replace(',','.');
			
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			var qty = qty.replace('.','');
			
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');
			var qtys = qtys.replace('.','');

			//var val = $('#kode_'+rs+'').val();
			
			var sss = kode.split('|');
			
			//alert(qtys);
			if(qtys == ""){
				var qtys = 0;
			}else{
				var qtys = qtys;
			}
			
			if(qty == ""){
				var qty = 0;
			}else{
				var qty = qty;
			}
			
			var total_qty = ( parseInt(qty)*parseInt(sss[1]) ) + parseInt(qtys);
			
			var total_harga = total_qty * parseFloat(harga);
			//var total_harga = new Intl.NumberFormat('de-DE', {}).format(total_harga);
			
			total_all = total_all + total_harga;
			
		}
				
	}
	
	//alert(total_all);
	
	var gt = new Intl.NumberFormat('de-DE', {}).format(total_all.toFixed(2));
	$('#grand_total').html(gt);
	
	var ppn = total_all/11;
	var ppns = new Intl.NumberFormat('de-DE', {}).format(ppn.toFixed(2));
	$('#vatppn').html(ppns);
	
	var dpp = total_all - ppn;
	var dpps = new Intl.NumberFormat('de-DE', {}).format(dpp.toFixed(2));
	$('#subttl2').html(dpps);
	
	// var total_ppn = (sub_total * (parseFloat($('#ppn').val())/100 ));
	// var grand_total = sub_total + (sub_total * (parseFloat($('#ppn').val())/100 ));
	
	
	
	// var gt = new Intl.NumberFormat('de-DE', { }).format(grand_total);
	// var sub_totals = new Intl.NumberFormat('de-DE', { }).format(sub_total);
	// var total_ppns = new Intl.NumberFormat('de-DE', { }).format(total_ppn);
	// var total_bds = new Intl.NumberFormat('de-DE', { }).format(sub_total_bd);
	// var total_sub_disk = new Intl.NumberFormat('de-DE', { }).format(sub_disk);
	
	// $('#subttl').html(total_bds);
	// $('#subttl2').html(sub_totals);
	// $('#subttl3').html(total_sub_disk);
	// $('#vatppn').html(total_ppns);
	// $('#grand_total').html(gt);
	
	
	
	
	//alert(qty+' '+harga+' '+diskon);
	
}

function change_sum_all(){
	
	var sub_total = 0;
	var int_val = parseInt($('#int_flo').val());
		
	for(var yo = 0;yo <= int_val;yo++ ){
				
		if($('#kode_'+yo).val() !== undefined){
				sub_total = sub_total +	parseFloat($('#total_'+yo).val());
		}
				
	}
	
	var total_ppn = (sub_total * (parseFloat($('#ppn').val())/100 ));
	var grand_total = sub_total + (sub_total * (parseFloat($('#ppn').val())/100 ));
	
	$('#subttl').html(sub_total);
	$('#subttl2').html(sub_total);
	$('#vatppn').html(total_ppn);
	$('#grand_total').html(grand_total);
	
	
	
	
	//alert(qty+' '+harga+' '+diskon);
	
}

function back(){
	
	window.location.href = "<?php echo base_url().'selling';?>";
	
}

function term_other(){
	
	//alert($('#term').val());
	
	if( $('#term').val() == "other"){
		//$('#term_o').show();
		$("#term_o").prop("readonly",false);

		
	}else{
		// $('#term_o').hide();
		
		$("#term_o").prop("readonly",true);
	}
	
}

  function listdist(){
	  var user_id = '0001';
	  var token = '093940349';
	  
	  
   $('#datatable_pricipal').DataTable({
	   //"dom": 'rtip',
		"bFilter": false,
		"aaSorting": [],
		"bLengthChange": true,
		'iDisplayLength': 10,
		"sPaginationType": "simple_numbers",
		"Info" : false,
		"processing": true,
        "serverSide": true,
        "destroy": true,
		"ajax": "<?php echo base_url().'selling/lists'?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token,
		"searching": true,
		"language": {
            "decimal": ",",
            "thousands": "."
        },
		"dom": 'l<"toolbar">frtip',
		"initComplete": function(){
                           $("div.toolbar").prepend('<div class="btn-group pull-left"><button type="button" class="btn btn-custon-rounded-two btn-primary" > Tambah </button></div>');
        }
	});	
  }

	$(document).ready(function(){
		var iy = 0;
	<?php foreach($po_mat as $po_mats){ ?>
		$('#kode_'+iy).select2();
		iy++;
	<?php } ?>
		$('#name').select2();
		
		$(".rupiahs").inputFilter(function(value) {
			return /^-?\d*[,]?\d*$/.test(value); 
			});
		
			 $('.rupiah').priceFormat({
          		prefix: '',
          		centsSeparator: '',
          		centsLimit: 0,
          		thousandsSeparator: '.'
        	});	
		
		$('#tgl').datepicker({
			format: "yyyy-mm-dd",
			autoclose: true,
			todayHighlight: true
		});
		
      listdist();
	  
	  $('#edit_po').on('submit', function(e){
		// validation code here
		//if(!valid) {
			e.preventDefault();
			
			// var formData = new FormData(this);
			var formData = new FormData();
			formData.append('id', <?php echo $po['id_penjualan']; ?>);
			formData.append('kode', $('#kode').val());
			formData.append('tgl', $('#tgl').val());
			formData.append('ints', $('#ints').val());
			formData.append('ints2', $('#ints2').val());
			formData.append('name', $('#name').val());
			formData.append('ppn', $('#ppn').val());
			formData.append('term', $('#term').val());
			formData.append('term_o', $('#term_o').val());
			formData.append('ket', $('#ket').val());
			formData.append('sisa', $('#sisa').val());
			formData.append('int_flo', $('#int_flo').val());
			
			var int_val = parseInt($('#int_flo').val());
			
			for(var yo = 0;yo <= int_val;yo++ ){
				
				if($('#kode_'+yo).val() !== undefined){
					
					formData.append('kode_'+yo, $('#kode_'+yo).val());
					formData.append('harga_'+yo, $('#harga_'+yo).val());
					formData.append('total_'+yo, $('#total_'+yo).val());
					formData.append('qty_'+yo, $('#qty_'+yo).val());
					formData.append('totalqty_'+yo, $('#totalqty_'+yo).val());
				//	formData.append('remark_'+yo, $('#remark_'+yo).val());
					formData.append('qtys_'+yo, $('#qtys_'+yo).val());
					
				}
				
			}
			
		
			
			
			//console.log(formData);

          $.ajax({
              type:'POST',
				url: $(this).attr('action'),
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                success: function(response) {
					
					//console.log(response);
					if(response.success == true){
						$('#ModalAddPo').modal('show');
					}else{
						$('#msg_err').html(response.message);
						$('#ModalChangePo').modal('show');
					}
                }
            });
			
			
			//alert(kode);
	
	
		//}
	  });
  });
</script>