<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto
  }

  .scroll-overflow {
    min-height: 650px
  }

  #modal-distributor::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="product-payment-inner-st">
          <ul id="myTabedu1" class="tab-review-design">
            <li class="active"><a href="#description">History Mutasi Item</a></li>
          </ul>
          <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="review-content-section">
                    <table id="listitems" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Kode Item</th>
                          <th>Nama Item</th>
                          <th>Tanggal Mutasi</th>
                          <th>Type Mutasi</th>
                          <th>Jumlah Mutasi</th>
                          <th>Nama Priciple</th>
                          <!-- <th>Status</th> -->
                          <!-- <th>Option</th> -->
                        </tr>
                      </thead>
                      <tbody></tbody>
                    </table>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="PrimaryModalalert" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-close-area modal-close-df">
        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
      </div>
      <div class="modal-body">
        <i class="educate-icon educate-checked modal-check-pro"></i>
        <h2>Data Berhasil Dirubah</h2>
        <p></p>
      </div>
      <!-- <div class="modal-footer">
                                        <a data-dismiss="modal" href="#" onClick="back()">Ya</a>
                                    </div>-->
    </div>
  </div>
</div>


<script>
  function clearform() {

    $('#edit_material').trigger("reset");

  }

  function back() {

    window.location.href = "<?php echo base_url() . 'stock'; ?>";

  }


  $(document).ready(function() {
    $('#date_mutasi').datepicker({
      isRTL: true,
      format: "dd-M-yyyy",
      autoclose: true,
      todayHighlight: true,
      changeYear: true,
      minDate: '-3M',
      maxDate: '+30D',
    });

    $(document).ready(function() {

      $("#listitems").dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "<?php echo base_url() . 'stock/lists_mutasi/'.$stok[0]['id_mat']; ?>",
        "searchDelay": 700,
        "responsive": true,
        "lengthChange": false,
        "info": false,
        "bSort": false,
        "dom": 'l<"toolbar">frtip',
        "initComplete": function() {
        },
        "columnDefs": [{
          "targets": [0],
          "className": 'dt-body-right',
          "width": "5%"
        },{
          "targets": [3,4,5],
          "className": 'dt-body-right'
        }]
      });
    });
  });
</script>