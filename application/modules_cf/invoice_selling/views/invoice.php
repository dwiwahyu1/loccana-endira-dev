<?php
    
     $path = base_url() . 'assets/kiaalap/';
?>
	<link rel="stylesheet" href="<?php echo $path ?>css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo $path; ?>style.css">
	<link rel="stylesheet" href="<?php echo $path; ?>css/main.css">
	<link rel="stylesheet" href="<?php echo $path; ?>css/font-awesome.min.css">
<!------ Include the above in your HEAD tag ---------->
<style>
.invoice-title h2, .invoice-title h3 {
    display: inline-block;
}

.table > tbody > tr > .no-line {
    border-top: none;
}

.table > thead > tr > .no-line {
    border-bottom: none;
}

.table > tbody > tr > .thick-line {
    border-top: 2px solid;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
    		<div class="invoice-title">
    			<h2>Invoice</h2><h3 class="pull-right">Order # 12345</h3>
    		</div>
    		<hr>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    				<strong>PT. ENDIRA ALDA</strong><br>
    					JL. Sangkuriang NO.38-A<br>
    					NPWP: 01.555.161.7.428.000<br>
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				<address>
        			<strong>FAKTUR</strong><br>
    					No Faktur : <?php echo $invoice[0]['no_penjualan']?><br>
    					Tanggal : <?php echo $invoice[0]['date_penjualan']?><br> 
    					Alamat Pengiriman :<br>
    					<?php echo $invoice[0]['cust_address']?><br>
						Phone: <?php echo $invoice[0]['phone']?>
    				</address>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    					<strong>Pembeli</strong><br>
    					<?php echo $invoice[0]['cust_name']?><br>
    					<?php echo $invoice[0]['cust_address']?>
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				
    			</div>
    		</div>
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
        							<td><strong>No</strong></td>
        							<td><strong>Kode</strong></td>
        							<td class="text-center"><strong>Nama Barang</strong></td>
        							<td class="text-center"><strong></strong></td>
        							<td class="text-center"><strong></strong></td>
        							<td class="text-center"><strong>Banyaknya</strong></td>
        							<td class="text-right"><strong>Jumlah Rp</strong></td>
                                </tr>
    						</thead>
    						<tbody>
    							<!-- foreach ($order->lineItems as $line) or some such thing here -->
    							 <?php echo $item_form; ?>
                                
    						</tbody>
    					</table>
    				</div>
					
					<div class="row">
						<div class="col-xs-6">
							<p style="text-align:center; margin:20px;">MENGETAHUI,</p>
							</br>
							</br>
							<hr>
							<p style="text-align:center; margin:20px;">Dadang Suryadi</p>
						</div>
						<div class="col-xs-6 text-right pull-right">
							<p><strong>Total : <?php echo $grand_total; ?></strong></p>
							<p>DPP : <?php echo $dpp; ?> </p>
							<p>PPN : <?php echo $ppn; ?> </p>
						</div>
						</div>
					</div>
    			</div>
    		</div>
    	</div>
    </div>
	<hr>
	<div class="row">
        <div class="col-xs-12">
    		<div class="invoice-title">
    			<h2>Invoice</h2><h3 class="pull-right">Order # 12345</h3>
    		</div>
    		<hr>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    				<strong>PT. ENDIRA ALDA</strong><br>
    					JL. Sangkuriang NO.38-A<br>
    					NPWP: 01.555.161.7.428.000<br>
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				<address>
        			<strong>FAKTUR</strong><br>
    					No Faktur : <?php echo $invoice[0]['no_penjualan']?><br>
    					Tanggal : <?php echo $invoice[0]['date_penjualan']?><br>
    					Alamat Pengiriman :<br>
    					<?php echo $invoice[0]['cust_address']?><br>
						Phone: <?php echo $invoice[0]['phone']?>
    				</address>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    					<strong>Pembeli</strong><br>
    					<?php echo $invoice[0]['cust_name']?><br>
    					<?php echo $invoice[0]['cust_address']?>
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				
    			</div>
    		</div>
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
        							<td><strong>No</strong></td>
        							<td><strong>Kode</strong></td>
        							<td class="text-center"><strong>Nama Barang</strong></td>
        							<td class="text-center"><strong></strong></td>
        							<td class="text-center"><strong></strong>Jumlah Box</td>
        							<td class="text-center"><strong>Banyaknya Kemasan</strong></td>
        							<td class="text-right"><strong>Liter/Kg</strong></td>
                                </tr>
    						</thead>
    						<tbody>
    							
    							 <?php echo $item_pengiriman; ?>
                                
    						</tbody>
							<tfoot>
									<tr>
										<td></td>
										<td></td>
										<td class="text-center">Jumlah :</td>
										<td class="text-center">15</td>
										<td class="text-center"></td>
										<td class="text-center">10</td>
										<td class="text-right">9</td>
									 </tr>			
							   </tfoot>
    					</table>
    				</div>
					
					<div class="row">
						<div class="col-xs-4 ">
							<p style="text-align:center; margin:20px;">Penerima,</p>
							</br>
							</br>
							<hr>
							<p style="text-align:center; margin:20px;"></p>
						</div>
						<div class="col-xs-4 "> 
							<p style="text-align:center; margin:20px;">Menyetujui,</p>
							</br>
							</br>
							<hr>
							<p style="text-align:center; margin:20px;">Dadang Suryadi</p>
						</div>
						<div class="col-xs-4 ">
							<p style="text-align:center; margin:20px;">Pengirim,</p>
							</br>
							</br>
							<hr>
							<p style="text-align:center; margin:20px;"></p>
						</div>
					</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>