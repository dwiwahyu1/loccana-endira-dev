<?php if (isset($group_rows) && is_array($group_rows) && count($group_rows) > 0) : ?>

    <?php
    $grand_total_qty = 0;
    $grand_total_saldo = 0;
    $grand_total_bln_berjalan = 0;
    ?>

    <?php foreach ($group_rows as $group) : ?>

        <?php
        $total_qty = 0;
        $total_saldo = 0;
        $total_bln_berjalan = 0;
        ?>

        <?php foreach ($group as $row) : ?>

            <?php
            $blnBerjalan = false;
            if (strpos($row['date_penjualan'], $currentYearMonth) === 0 || strpos($row['date_penjualan'], $currentYearMonth) > 0) {
                $blnBerjalan = true;
            }

            $umur = null;
            if (isset($row['tanggal_invoice']) && !empty($row['tanggal_invoice'])) {
                $umur = $now->diffInDays($row['tanggal_invoice']);
            }
            ?>

            <tr>
                <td style="width: 150px;"><?= ($row['cust_name'] ?? '-') ?></td>
                <td style="width: 100px;" class="text-center"><?= ($row['tanggal_invoice'] ?? '-') ?></td>
                <td style="width: 100px;"><?= ($row['faktur_pajak'] ?? '-') ?></td>
                <td style="width: 150px;"><?= ($row['stock_name'] ?? '-') ?></td>
                <td style="width: 100px;" class="text-right"><?= (($row['base_qty'] ?? '') . ($row['uom_symbol'] ?? '')) ?></td>
                <td style="width: 100px;" class="text-right"><?= indonesia_currency_format($row['qty']); ?></td>
                <td style="width: 150px;" class="text-right"><?= $blnBerjalan ? '-' : indonesia_currency_format($row['price']); ?></td>
                <td style="width: 150px;" class="text-right"><?= $blnBerjalan ? indonesia_currency_format($row['price']) : '-'; ?></td>
                <td style="width: 100px;" class="text-right"><?= $umur ?? '-'; ?></td>
            </tr>

            <?php
            $total_qty += ($row['qty'] ?? 0);
            $total_saldo += ($blnBerjalan ? 0 : $row['price']);
            $total_bln_berjalan += ($blnBerjalan ? $row['price'] : 0);
            ?>

        <?php endforeach; ?>

        <tr style="background-color: yellow; font-weight:600;">
            <td style="width: 150px;" class="text-center">Total</td>
            <td style="width: 100px;"></td>
            <td style="width: 100px;"></td>
            <td style="width: 150px;"></td>
            <td style="width: 100px;"></td>
            <td style="width: 100px;" class="text-right"><?= indonesia_currency_format($total_qty); ?></td>
            <td style="width: 150px;" class="text-right"><?= indonesia_currency_format($total_saldo); ?></td>
            <td style="width: 150px;" class="text-right"><?= indonesia_currency_format($total_bln_berjalan); ?></td>
            <td style="width: 100px;"></td>
        </tr>

        <?php
        $grand_total_qty += $total_qty;
        $grand_total_saldo += $total_saldo;
        $grand_total_bln_berjalan += $total_bln_berjalan;
        ?>

    <?php endforeach; ?>
    <tr style="background-color:yellowgreen; font-weight:600;">
        <td style="width: 150px;" class="text-center">Grand Total</td>
        <td style="width: 100px;"></td>
        <td style="width: 100px;"></td>
        <td style="width: 150px;"></td>
        <td style="width: 100px;"></td>
        <td style="width: 100px;" class="text-right"><?= indonesia_currency_format($grand_total_qty); ?></td>
        <td style="width: 150px;" class="text-right"><?= indonesia_currency_format($grand_total_saldo); ?></td>
        <td style="width: 150px;" class="text-right"><?= indonesia_currency_format($grand_total_bln_berjalan); ?></td>
        <td style="width: 100px;"></td>
    </tr>
<?php else : ?>
    <tr>
        <td class="text-center" colspan="9">Data tidak tersedia</td>
    </tr>
<?php endif; ?>