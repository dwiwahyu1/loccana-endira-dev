<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Report_dasar_pembelian_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function list_pembelian_new($params = [])
	{
		$total = $this->db
			->select('*')
			->from('t_purchase_order')
			->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id')
			->join('t_po_mat', 't_purchase_order.id_po = t_po_mat.id_po')
			->join('t_bpb_detail', 't_po_mat.id_t_ps = t_bpb_detail.id_po_mat')
			// ->join('t_bpb', 't_bpb_detail.id_bpb = t_bpb.id_bpb')
			->join('m_material', 't_po_mat.id_material = m_material.id_mat')
			->join('m_type_material', 'm_material.type = m_type_material.id_type_material')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->join('t_uom_convert', 'm_uom.id_uom = t_uom_convert.id_uom')
			->join('t_invoice_pembelian', 't_bpb_detail.id_bpb = t_invoice_pembelian.id_bpb')
			->join('d_invoice_pembelian', 't_invoice_pembelian.id_invoice = d_invoice_pembelian.id_inv_pembelian')
			->group_by('t_purchase_order.id_po')
			->get()
			->result_array();

		// these field doesnt compatible in query builder:
		// CASE WHEN t_purchase_order.date_po = '' THEN '' ELSE DATE_FORMAT(t_purchase_order.date_po,'%Y') END AS tahun,
		// CASE WHEN t_purchase_order.date_po = '' THEN '' ELSE DATE_FORMAT(t_purchase_order.date_po,'%M') END AS bulan,

		$queryBuilder = $this->db
			->select("
				*,
				t_purchase_order.keterangan AS 't_purchase_order.keterangan',
				t_po_mat.qty AS 't_po_mat.qty',
				t_po_mat.qty*t_po_mat.unit_price-(t_po_mat.unit_price*(t_po_mat.diskon/100)) AS 'jumlah',
				((t_po_mat.qty * t_po_mat.unit_price) - (t_po_mat.qty * (t_po_mat.unit_price*(t_po_mat.diskon/100))))*0.1 AS ppn,
				(t_po_mat.qty*t_po_mat.unit_price) - (t_po_mat.qty *(t_po_mat.unit_price*(t_po_mat.diskon/100)))+((t_po_mat.qty * t_po_mat.unit_price) - (t_po_mat.qty * (t_po_mat.unit_price*(t_po_mat.diskon/100))))*0.1 AS rp_ppn,
				(t_invoice_pembelian.tanggal_invoice) - (t_purchase_order.date_po) AS lama_hari,
				DATE_ADD(`t_purchase_order`.`date_po`, INTERVAL `t_purchase_order`.`term_of_payment` DAY) as jatuh_tempo,
				CONCAT(m_material.base_qty, ' ', m_uom.uom_name) AS kemasan
			", false)
			->from('t_purchase_order')
			->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id')
			->join('t_po_mat', 't_purchase_order.id_po = t_po_mat.id_po')
			->join('t_bpb_detail', 't_po_mat.id_t_ps = t_bpb_detail.id_po_mat')
			// ->join('t_bpb', 't_bpb_detail.id_bpb = t_bpb.id_bpb')
			->join('m_material', 't_po_mat.id_material = m_material.id_mat')
			->join('m_type_material', 'm_material.type = m_type_material.id_type_material')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->join('t_uom_convert', 'm_uom.id_uom = t_uom_convert.id_uom')
			->join('t_invoice_pembelian', 't_bpb_detail.id_bpb = t_invoice_pembelian.id_bpb')
			->join('d_invoice_pembelian', 't_invoice_pembelian.id_invoice = d_invoice_pembelian.id_inv_pembelian');

		if (isset($params['searchtxt']) && !empty($params['searchtxt'])) {
			$queryBuilder = $queryBuilder->like('t_eksternal.name_eksternal', $params['searchtxt']);
			$queryBuilder = $queryBuilder->or_like('m_material.stock_code', $params['searchtxt']);
			$queryBuilder = $queryBuilder->or_like('m_material.stock_name', $params['searchtxt']);
			
			$total = $total->like('t_eksternal.name_eksternal', $params['searchtxt']);
			$total = $total->or_like('m_material.stock_code', $params['searchtxt']);
			$total = $total->or_like('m_material.stock_name', $params['searchtxt']);
		}

		if (isset($params['principle_id']) && !empty($params['principle_id'])) {
			$queryBuilder = $queryBuilder->where('t_purchase_order.id_distributor', $params['principle_id']);
			$total = $total->where('t_purchase_order.id_distributor', $params['principle_id']);
		}

		if (isset($params['tgl_awal']) && !empty($params['tgl_awal']) && isset($params['tgl_akhir']) && !empty($params['tgl_akhir'])) {
			$queryBuilder = $queryBuilder->where('t_purchase_order.date_po >=', $params['tgl_awal']);
			//$total = $total->where('t_purchase_order.date_po >=', $params['tgl_awal']);
			//$total = $total->where('t_purchase_order.date_po >=', $params['tgl_awal']);
			$queryBuilder = $queryBuilder->where('t_purchase_order.date_po <=', $params['tgl_akhir']);
			//$total = $total->where('t_purchase_order.date_po <=', $params['tgl_akhir']);
		} else if (isset($params['tgl_awal']) && !empty($params['tgl_awal'])) {
			$queryBuilder = $queryBuilder->where('t_purchase_order.date_po >=', $params['tgl_awal']);
			$total = $total->where('t_purchase_order.date_po >=', $params['tgl_awal']);
		} else if (isset($params['tgl_akhir']) && !empty($params['tgl_akhir'])) {
			$queryBuilder = $queryBuilder->where('t_purchase_order.date_po <=', $params['tgl_akhir']);
			$total = $total->where('t_purchase_order.date_po <=', $params['tgl_akhir']);
		}
		
		

		if (isset($params['limit']) && $params['limit'] >= 0 && isset($params['offset'])) {
			$queryBuilder = $queryBuilder->limit($params['limit'], $params['offset']);
			//$total = $total->limit($params['limit'], $params['offset']);
		}

		$result = $queryBuilder->get()->result_array();

		return [
			'data' => $result,
			'total_filtered' => count($total),
			'total' => count($total),
		];
		//
	}

	public function list_pembelian($params = [])
	{
		$select = "
			SELECT
				purchase_order.date_po,
				t_invoice_pembelian.tanggal_invoice AS tanggal_invoice,
				material.stock_code ,
				material.stock_name ,
				CONCAT(material.base_qty, ' ', uom.uom_name) AS kemasan,
				eksternal.name_eksternal ,
				bpb_detail.qty AS qty,
				po_mat.unit_price ,
				bpb_detail.qty*po_mat.unit_price-(po_mat.unit_price*(po_mat.diskon/100)) AS jumlah,
				(bpb_detail.qty*po_mat.unit_price) - (bpb_detail.qty *(po_mat.unit_price*(po_mat.diskon/100))) AS harga_sebelum_ppn,
				((bpb_detail.qty * po_mat.unit_price) - (bpb_detail.qty * (po_mat.unit_price*(po_mat.diskon/100))))*0.1 AS ppn,
				(bpb_detail.qty*po_mat.unit_price) - (bpb_detail.qty *(po_mat.unit_price*(po_mat.diskon/100)))+((bpb_detail.qty * po_mat.unit_price) - (bpb_detail.qty * (po_mat.unit_price*(po_mat.diskon/100))))*0.1 AS rp_ppn,
				po_mat.remarks AS no_trans,
				t_invoice_pembelian.no_invoice,
				t_invoice_pembelian.tanggal_invoice-purchase_order.date_po AS lama_hari,
				purchase_order.term_of_payment,
				t_invoice_pembelian.faktur_pajak,
				type_material.type_material_name ";

		$from = "
			FROM t_purchase_order purchase_order
				JOIN t_po_mat po_mat ON purchase_order.id_po=po_mat.id_po
				JOIN t_bpb_detail bpb_detail ON po_mat.id_t_ps=bpb_detail.id_po_mat
				JOIN t_bpb bpb ON bpb_detail.id_bpb=bpb.id_bpb
				JOIN m_material material ON po_mat.id_material=material.id_mat
				JOIN m_type_material type_material ON material.type=type_material.id_type_material
				JOIN m_uom uom ON material.unit_terkecil=uom.id_uom
				JOIN t_uom_convert uom_convert ON uom.id_uom=uom_convert.id_uom
				LEFT JOIN t_invoice_pembelian t_invoice_pembelian ON bpb.id_bpb=t_invoice_pembelian.id_bpb
				LEFT JOIN d_invoice_pembelian d_invoice_pembelian ON t_invoice_pembelian.id_invoice=d_invoice_pembelian.id_inv_pembelian
				JOIN t_eksternal eksternal ON purchase_order.id_distributor=eksternal.id ";

		// $where = " ";
		// if ($params['tgl_awal'] ?? false && $params['tgl_akhir'] ?? false && $params['principle_id'] ?? false && $params['principle_id'] != 0) {
		// 	$where = " WHERE purchase_order.date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "' AND purchase_order.id_distributor = " . $params['principle_id'] . " ";
		// } else if ($params['tgl_awal'] ?? false && $params['tgl_akhir']) {
		// 	$where = " WHERE purchase_order.date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "' ";
		// } else if ($params['principle_id'] ?? false && $params['principle_id'] != 0) {
		// 	$where = " WHERE purchase_order.id_distributor = " . $params['principle_id'] . " ";
		// }

		$where = " WHERE bpb.tanggal BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "' ";
		if ($params['principle_id'] ?? false && $params['principle_id'] != 0) {
			$where .= " AND purchase_order.id_distributor = " . $params['principle_id'] . " ";
		}

		if ($params['searchtxt'] ?? false && !empty($params['searchtxt'])) {
			$where .= " AND (
				material.stock_code LIKE '%" . $params['searchtxt'] . "%' OR 
				material.stock_name LIKE '%" . $params['searchtxt'] . "%' OR
				eksternal.name_eksternal LIKE '%" . $params['searchtxt'] . "%'
			) ";
		}

		$groupBy = "GROUP BY po_mat.id_t_ps ";

		$countQuery = $select . $from . $where . $groupBy;
		$countResult = count($this->db->query($countQuery)->result_array());

		$limitOffset = (isset($params['limit']) && isset($params['offset'])) ? "LIMIT " . $params['limit'] . " OFFSET " . $params['offset'] . " " : " ";

		$dataQuery = $select . $from . $where . $groupBy . $limitOffset;
		$dataResult = $this->db->query($dataQuery)->result_array();

		$return = array(
			'data' => $dataResult,
			// 'total_filtered' => count($dataResult),
			'total_filtered' => (int) $countResult,
			'total' => (int) $countResult,
		);

		return $return;
	}

	public function get_principles()
	{
		$this->db->select('*')
			->from('t_eksternal')
			->order_by('name_eksternal', 'asc');
		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_report_penjualan_export($params = array())
	{
		$select = "
			SELECT
				purchase_order.date_po AS tanggal_po,
				purchase_order.keterangan,
				CASE
					WHEN purchase_order.date_po = '' THEN ''
					ELSE DATE_FORMAT(purchase_order.date_po,'%Y')
				END AS tahun,
				CASE
					WHEN purchase_order.date_po = '' THEN ''
					ELSE DATE_FORMAT(purchase_order.date_po,'%M')
				END AS bulan,

				t_invoice_pembelian.tanggal_invoice AS tanggal_invoice,
				material.stock_code AS kode,
				material.stock_name AS nama_barang,
				CONCAT(material.base_qty, ' ', uom.uom_name) AS kemasan,
				eksternal.name_eksternal AS principle,
				po_mat.qty AS qty,
				po_mat.unit_price AS harga,
				po_mat.qty*po_mat.unit_price-(po_mat.unit_price*(po_mat.diskon/100)) AS jumlah,
				(po_mat.qty*po_mat.unit_price) - (po_mat.qty *(po_mat.unit_price*(po_mat.diskon/100))) AS harga_sebelum_ppn,
				((po_mat.qty * po_mat.unit_price) - (po_mat.qty * (po_mat.unit_price*(po_mat.diskon/100))))*0.1 AS ppn,
				(po_mat.qty*po_mat.unit_price) - (po_mat.qty *(po_mat.unit_price*(po_mat.diskon/100)))+((po_mat.qty * po_mat.unit_price) - (po_mat.qty * (po_mat.unit_price*(po_mat.diskon/100))))*0.1 AS rp_ppn,
				po_mat.remarks AS no_trans,
				t_invoice_pembelian.no_invoice,
				t_invoice_pembelian.tanggal_invoice-purchase_order.date_po AS lama_hari,
				purchase_order.term_of_payment,
				DATE_ADD(purchase_order.date_po, INTERVAL purchase_order.term_of_payment DAY) as jatuh_tempo,
				t_invoice_pembelian.faktur_pajak,
				t_invoice_pembelian.tanggal_faktur,
				type_material.type_material_name ";

		$from = "
			FROM t_purchase_order purchase_order
				JOIN t_po_mat po_mat ON purchase_order.id_po=po_mat.id_po
				JOIN t_bpb_detail bpb_detail ON po_mat.id_t_ps=bpb_detail.id_po_mat
				JOIN t_bpb bpb ON bpb_detail.id_bpb=bpb.id_bpb
				JOIN m_material material ON po_mat.id_material=material.id_mat
				JOIN m_type_material type_material ON material.type=type_material.id_type_material
				JOIN m_uom uom ON material.unit_terkecil=uom.id_uom
				JOIN t_uom_convert uom_convert ON uom.id_uom=uom_convert.id_uom
				JOIN t_invoice_pembelian t_invoice_pembelian ON bpb.id_bpb=t_invoice_pembelian.id_bpb
				JOIN d_invoice_pembelian d_invoice_pembelian ON t_invoice_pembelian.id_invoice=d_invoice_pembelian.id_inv_pembelian
				JOIN t_eksternal eksternal ON purchase_order.id_distributor=eksternal.id ";

		$where = " ";
		// if ($params['tgl_awal'] ?? false && $params['tgl_akhir'] ?? false && $params['principle_id'] ?? false && $params['principle_id'] != 0) {
		// 	$where = " WHERE purchase_order.date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "' AND purchase_order.id_distributor = " . $params['principle_id'] . " ";
		// } else if ($params['tgl_awal'] ?? false && $params['tgl_akhir']) {
		// 	$where = " WHERE purchase_order.date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "' ";
		// } else if ($params['principle_id'] ?? false && $params['principle_id'] != 0) {
		// 	$where = " WHERE purchase_order.id_distributor = " . $params['principle_id'] . " ";
		// }

		// $where = " WHERE purchase_order.date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "' ";
		// if ($params['principle_id'] ?? false && $params['principle_id'] != 0) {
		// 	$where .= " AND purchase_order.id_distributor = " . $params['principle_id'] . " ";
		// }

		$groupBy = "GROUP BY po_mat.id_t_ps ";

		$sql = $select . $from . $where . $groupBy;
		//echo $sql;die;
		$query 	= $this->db->query($sql);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_report_penjualan_prin($params = array())
	{
		$select = "
			SELECT
				purchase_order.date_po AS tanggal_po,
				purchase_order.keterangan,
				CASE
					WHEN purchase_order.date_po = '' THEN ''
					ELSE DATE_FORMAT(purchase_order.date_po,'%Y')
				END AS tahun,
				CASE
					WHEN purchase_order.date_po = '' THEN ''
					ELSE DATE_FORMAT(purchase_order.date_po,'%M')
				END AS bulan,
				t_invoice_pembelian.tanggal_invoice AS tanggal_invoice,
				material.stock_code AS kode,
				material.stock_name AS nama_barang,
				CONCAT(material.base_qty, ' ', uom.uom_name) AS kemasan,
				eksternal.name_eksternal AS principle,
				po_mat.qty AS qty,
				po_mat.unit_price AS harga,
				po_mat.qty*po_mat.unit_price-(po_mat.unit_price*(po_mat.diskon/100)) AS jumlah,
				(po_mat.qty*po_mat.unit_price) - (po_mat.qty *(po_mat.unit_price*(po_mat.diskon/100))) AS harga_sebelum_ppn,
				((po_mat.qty * po_mat.unit_price) - (po_mat.qty * (po_mat.unit_price*(po_mat.diskon/100))))*0.1 AS ppn,
				(po_mat.qty*po_mat.unit_price) - (po_mat.qty *(po_mat.unit_price*(po_mat.diskon/100)))+((po_mat.qty * po_mat.unit_price) - (po_mat.qty * (po_mat.unit_price*(po_mat.diskon/100))))*0.1 AS rp_ppn,
				po_mat.remarks AS no_trans,
				t_invoice_pembelian.no_invoice,
				t_invoice_pembelian.tanggal_invoice-purchase_order.date_po AS lama_hari,
				purchase_order.term_of_payment,
				DATE_ADD(purchase_order.date_po, INTERVAL purchase_order.term_of_payment DAY) as jatuh_tempo,
				t_invoice_pembelian.faktur_pajak,
				t_invoice_pembelian.tanggal_faktur,
				type_material.type_material_name ";

		$from = "
			FROM t_purchase_order purchase_order
				JOIN t_po_mat po_mat ON purchase_order.id_po=po_mat.id_po
				JOIN t_bpb_detail bpb_detail ON po_mat.id_t_ps=bpb_detail.id_po_mat
				JOIN t_bpb bpb ON bpb_detail.id_bpb=bpb.id_bpb
				JOIN m_material material ON po_mat.id_material=material.id_mat
				JOIN m_type_material type_material ON material.type=type_material.id_type_material
				JOIN m_uom uom ON material.unit_terkecil=uom.id_uom
				JOIN t_uom_convert uom_convert ON uom.id_uom=uom_convert.id_uom
				JOIN t_invoice_pembelian t_invoice_pembelian ON bpb.id_bpb=t_invoice_pembelian.id_bpb
				JOIN d_invoice_pembelian d_invoice_pembelian ON t_invoice_pembelian.id_invoice=d_invoice_pembelian.id_inv_pembelian
				JOIN t_eksternal eksternal ON purchase_order.id_distributor=eksternal.id ";

		$where = " ";
		// if ($params['tgl_awal'] ?? false && $params['tgl_akhir'] ?? false && $params['principle_id'] ?? false && $params['principle_id'] != 0) {
		// 	$where = " WHERE purchase_order.date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "' AND purchase_order.id_distributor = " . $params['principle_id'] . " ";
		// } else if ($params['tgl_awal'] ?? false && $params['tgl_akhir']) {
		// 	$where = " WHERE purchase_order.date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "' ";
		// } else if ($params['principle_id'] ?? false && $params['principle_id'] != 0) {
		// 	$where = " WHERE purchase_order.id_distributor = " . $params['principle_id'] . " ";
		// }

		$where = " WHERE purchase_order.date_po BETWEEN '" . $params['tgl_awal'] . "' AND '" . $params['tgl_akhir'] . "' ";
		if ($params['principle_id'] ?? false && $params['principle_id'] != 0) {
			$where .= " AND purchase_order.id_distributor = " . $params['principle_id'] . " ";
		}

		$groupBy = "GROUP BY po_mat.id_t_ps ";

		$sql = $select . $from . $where . $groupBy;
		//echo $sql;die;
		$query 	= $this->db->query($sql);

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
}
