<form class="form-horizontal form-label-left" id="detail_quotation" role="form" action="" method="post" enctype="multipart/form-data" data-parsley-validate>

	 <div class="item form-group">
        <label class="col-md-3 col-sm-3 col-xs-12" for="nama">Issue Date
        	
        </label>
        <div class="col-md-8 col-sm-6 col-xs-12">
			: <?php if(isset($detail['issue_date'])){ echo $detail['issue_date']; }?>
		</div>
   	</div>

   	<div class="item form-group" id="subdetail_temp">
		<label class="col-md-3 col-sm-3 col-xs-12" for="type_id">Currency</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			: <?php if(isset($detail['nama_valas'])){ echo $detail['nama_valas']; }?>
		</div>
	</div>

   	<div class="item form-group">
		<label class="col-md-3 col-sm-3 col-xs-12" for="term_of_pay">Term of Payment</span></label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			: <?php if(isset($detail['price_term'])){ echo $detail['price_term']; }?>
		</div>
	</div>

	<div class="item form-group">
		<label class="col-md-3 col-sm-3 col-xs-12" for="dist">Customer</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			: <?php if(isset($detail['name_eksternal'])){ echo $detail['name_eksternal']; }?>
		</div>
	</div>

	<div class="item form-group">
		<label class="col-md-3 col-sm-3 col-xs-12" for="dist">Status</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			: <?php if(isset($detail['status'])){ echo $detail['status']; }?>
		</div>
	</div>

	<div class="item form-group">
		<label class="col-md-3 col-sm-3 col-xs-12" for="dist">Approval Date</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			: <?php if(isset($detail['approval_date'])){ echo $detail['approval_date']; }?>
		</div>
	</div>

	<div class="item form-group">
		<label class="col-md-3 col-sm-3 col-xs-12" for="dist">Notes</label>
		<div class="col-md-8 col-sm-6 col-xs-12">
			: <?php if(isset($detail['notes'])){ echo $detail['notes']; }?>
		</div>
	</div>

	<div class="item form-group">
        <label class="col-md-3 col-sm-3 col-xs-12" for="nama">Daftar Item : </label>
    </div>

    <div class="item form-group">
		<table id="listedititem" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th>No</th>
					<th>Items</th>
					<th>Normal Price</th>
					<th>Discount</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$i=1; 
				foreach($detail['item'] as $orderkey) {?>
					<tr>
						<td style="padding-top: 17px;"><?php echo $i; ?></td>
	                  	<td>
	                  		<?php if(isset($orderkey['stock_name'])){ echo $orderkey['stock_name']; }?>
	                  	</td>
		                <td>
		                	<?php if(isset($orderkey['price'])){ echo number_format($orderkey['price'],2,",","."); }?>
		                </td>
		                <td>
		                	<?php if(isset($orderkey['diskon'])){ echo $orderkey['diskon']; }?> %
		                </td>
		            </tr>
	            <?php $i++;} ?>
			</tbody>
		</table>
	</div>
</form>
<!-- /page content -->

<script type="text/javascript">
	$(document).ready(function() {
		$('[data-toggle="tooltip"]').tooltip();
	});
</script>
