<?php

use function PHPSTORM_META\type;

defined('BASEPATH') or exit('No direct script access allowed');

class Report_hutang extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('report_hutang/report_hutang_model', 'reportHutangModel');
		$this->load->library('log_activity');
		$this->load->library('priv');
	}

	public function index()
	{
		$principle_list = $this->reportHutangModel->get_principle_list();

		$this->template->load('maintemplate', 'report_hutang/views/index', compact('principle_list'));
	}

	public function list()
	{
		$sess_user_id = !empty($_GET['sess_user_id']) ? $_GET['sess_user_id'] : NULL;
		$sess_token = !empty($_GET['sess_token']) ? $_GET['sess_token'] : NULL;
		$principle_id = !empty($_GET['principle_id']) ? $_GET['principle_id'] : NULL;
		$filter = !empty($_GET['filter']) ? $_GET['filter'] : NULL;
		$filter_month = !empty($_GET['filter_month']) ? $_GET['filter_month'] : NULL;
		$filter_year = !empty($_GET['filter_year']) ? $_GET['filter_year'] : NULL;
		$draw = $this->input->get_post('draw') != FALSE ? $this->input->get_post('draw') : 1;
		$length = $this->input->get_post('length') != FALSE ? $this->input->get_post('length') : 10;
		$start = $this->input->get_post('start') != FALSE ? $this->input->get_post('start') : 0;

		$order_fields = array('id_hp', 'no_payment', 'total', 'tgl_terbit', 'keterangan', 'id_hp', 'id_hp'); // , 'COST'

		$order = $this->input->get_post('order');
		$order_dir = !empty($order[0]['dir']) ? $order[0]['dir'] : 'desc';
		$order_column = !empty($order[0]['column']) ? $order[0]['column'] : 0;

		$params['limit']		= (int) $length;
		$params['offset']		= (int) $start;
		$params['order_column']	= $order_fields[$order_column];
		$params['order_dir']	= $order_dir;
		$params['sess_user_id']	= $sess_user_id;
		$params['sess_token']	= $sess_token;
		$params['filter']		= $filter;
		$params['filter_month']	= $filter_month;
		$params['filter_year']	= $filter_year;
		$params['searchtxt']	= $_GET['search']['value'] ?? null;
		$params['principle_id']		= $principle_id;

		// print_r($params);die;
		$priv = $this->priv->get_priv();

		$list = $this->reportHutangModel->list_report_hutang($params);
		$data = [];

		foreach ($list['data'] as $row) {

			$tgl = $row['t_payment_hp.date'] ?? null;
			$datePo = $row['date_po'] ?? null;
			$jatuhTempo = $row['term_of_payment'] ?? false ? date('Y-m-d', strtotime($row['date_po'] . " {$row['term_of_payment']} days")) : null;

			$umur = null;
			if (!empty($tgl) && !empty($datePo)) {
				$dpo = new DateTime($datePo);
				$tg = new DateTime(date('Y-m-d', strtotime($tgl)));
				$interval = $dpo->diff($tg);
				$umur = $interval->format('%a');
			}

			array_push($data, [
				$row['name_eksternal'] ?? '-',
				$datePo ?? '-',
				$row['stock_name'] ?? '-',
				($row['base_qty'] ?? '') . ($row['uom_symbol'] ?? ''),
				$row['no_po'] ?? '-',
				$row['no_invoice'] ?? '-',
				$jatuhTempo ?? '-',
				$umur ?? '-',
				$row['payment_type'] == 0 ? ($row['price'] ? indonesia_currency_format($row['price']) : '-') : '-',
				$row['payment_type'] == 1 ? ($row['price'] ? indonesia_currency_format($row['price']) : '-') : '-',
				$tgl ? date('Y-m-d', strtotime($tgl)) : '-',
				$row['payment_status'] == 1 && $row['payment_type'] == 0 ? indonesia_currency_format($row['price']) : '-',
			]);
		}

		$result = [
			'data' => $data,
			'recordsTotal' => $list['total'],
			'recordsFiltered' => $list['total_filtered'],
			'draw' => $draw,
		];

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function excel()
	{
		$params = array(
			'principle_id' => anti_sql_injection($this->input->post('principle_id')),
		);

		$this->load->library('excel');

		$objPHPExcel = new PHPExcel();

		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'Principle')
			->setCellValue('B1', 'Tanggal Terima')
			->setCellValue('C1', 'Keterangan')
			->setCellValue('D1', 'Kemasan')
			->setCellValue('E1', 'Order Pembelian')
			->setCellValue('F1', 'No. Invoice')
			->setCellValue('G1', 'Jatuh Tempo')
			->setCellValue('H1', 'Umur')
			->setCellValue('I1', 'Saldo')
			->setCellValue('J1', 'Bulan Berjalan')
			->setCellValue('K1', 'Tgl')
			->setCellValue('L1', 'Dibayar');


		$list = $this->reportHutangModel->list_report_hutang($params);
		$start = 2;
		foreach ($list['data'] as $row) {

			$tgl = $row['t_payment_hp.date'] ?? null;
			$datePo = $row['date_po'] ?? null;
			$jatuhTempo = $row['term_of_payment'] ?? false ? date('Y-m-d', strtotime($row['date_po'] . " {$row['term_of_payment']} days")) : null;

			$umur = null;
			if (!empty($tgl) && !empty($datePo)) {
				$dpo = new DateTime($datePo);
				$tg = new DateTime(date('Y-m-d', strtotime($tgl)));
				$interval = $dpo->diff($tg);
				$umur = $interval->format('%a');
			}

			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A' . $start, $row['name_eksternal'] ?? '-') // principle
				->setCellValue('B' . $start, $row['date_po'] ?? '-') // tanggal terima
				->setCellValue('C' . $start, $row['stock_name'] ?? '-') // keterangan
				->setCellValue('D' . $start, ($row['base_qty'] ?? '') . ($row['uom_symbol'] ?? '')) // kemasan
				->setCellValue('E' . $start, $row['no_po'] ?? '-') // order pembelian
				->setCellValue('F' . $start, $row['no_invoice'] ?? '-') // no.invoice
				->setCellValue('G' . $start, $jatuhTempo ?? '-') // jatuh tempo
				->setCellValue('H' . $start, $umur ?? '-') // umur
				->setCellValue('I' . $start, $row['payment_type'] == 0 ? ($row['price'] ? indonesia_currency_format($row['price']) : '-') : '-') // saldo
				->setCellValue('J' . $start, $row['payment_type'] == 1 ? ($row['price'] ? indonesia_currency_format($row['price']) : '-') : '-') // bulan berjalan
				->setCellValue('K' . $start, $tgl ? date('Y-m-d', strtotime($tgl)) : '-') // tgl
				->setCellValue('L' . $start, $row['payment_status'] == 1 && $row['payment_type'] == 0 ? indonesia_currency_format($row['price']) : '-') // dibayar
				//
			;

			$start++;
		}

		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename=report_cash.xls');
		header('Cache-Control: max-age=0');
		// If you’re serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you’re serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		unset($objPHPExcel);
	}
}
