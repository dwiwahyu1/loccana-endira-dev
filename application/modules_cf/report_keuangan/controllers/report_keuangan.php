<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Report_keuangan extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('report_keuangan/uom_model');
	$this->load->library('log_activity');
		$this->load->library('priv');
  }

  /**
   * anti sql injection
   */
  public function Anti_sql_injection($string)
  {
    $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
    return $string;
  }

  public function index()
  {
	  $priv = $this->priv->get_priv();

	// sales per montg
	  
	  $year = date('Y');
	  
	    $html_sb = '';
			  $html_sbs = '';
			  $html_all = '';
			  $cur_month = '';
			  
	  for($mi=1;$mi<13;$mi++){
		  
		  $sales_perbulan = $this->uom_model->sales_perbulan($year,$mi);
		  //print_r($sales_perbulan);die;

			  $sumtot = 0;
			  $sumppn = 0;
			  $sumtotppn = 0;
			  foreach($sales_perbulan as $sales_perbulans){
				  
				  $month_var = $sales_perbulans['bulan'];
				   $html_sbs .= '<tr><td> - '.$sales_perbulans['type_material_name'].'</td><td style="text-align:right">'.number_format($sales_perbulans['sumtot'],2,',','.').'</td><td style="text-align:right">'.number_format($sales_perbulans['sumppn'],2,',','.').'</td><td style="text-align:right">'.number_format($sales_perbulans['sumtotppn'],2,',','.').'</td></tr>';
				   
				    $sumtot = $sumtot + $sales_perbulans['sumtot'];
					$sumppn = $sumppn + $sales_perbulans['sumppn'];
					$sumtotppn = $sumtotppn + $sales_perbulans['sumtotppn'];
				  
			  }
			  
			  if(count($sales_perbulan) > 0){
				 $html_sb = '<tr style="background:#888"><td>'.$month_var.'</td><td style="text-align:right">'.number_format($sumtot,2,',','.').'</td><td style="text-align:right">'.number_format($sumppn,2,',','.').'</td><td style="text-align:right">'.number_format($sumtotppn,2,',','.').'</td></tr>'. $html_sbs;
				 
				 $html_all .= $html_sb;
			  }
			  
			  
	    // $html_sb = '';
			  $html_sbs = '';
			  $cur_month = '';
	  }
	  
	  // sales per product
	  
	 // $year = date('Y');
	  $month = "All";
	  
			  $html_sbs2 = '';
			  
	  //for($mi=1;$mi<13;$mi++){
		  
		  $sales_perbulan = $this->uom_model->sales_perproduct($year,$month);
		  //print_r($sales_perbulan);die;

			  $sumtot = 0;
			  $sumppn = 0;
			  $sumtotppn = 0;
			  $rowspan_principle = 0;
			  foreach($sales_perbulan as $sales_perbulans){
				  
				  //$month_var = $sales_perbulans['bulan'];
				   $html_sbs2 .= '<tr><td> '.$sales_perbulans['Principle'].'</td><td> '.$sales_perbulans['Produk'].'</td><td style="text-align:right"> '.$sales_perbulans['Kemasan'].'</td><td style="text-align:right">Rp. '.number_format($sales_perbulans['sumoftotal_lt_kg'],2,',','.').'</td><td style="text-align:right">Rp. '.number_format($sales_perbulans['totalsumoftotal'],2,',','.').'</td></tr>';
			  
			  }
			  
 // sales per product Customer
	  
	 // $year = date('Y');
	  $month = "All";
	  
			  $html_sbs3 = '';
			  
	  //for($mi=1;$mi<13;$mi++){
		  
		  $sales_perbulan = $this->uom_model->sales_perproductcust($year,$month);
		  //print_r($sales_perbulan);die;

			  $sumtot = 0;
			  $sumppn = 0;
			  $sumtotppn = 0;
			  $rowspan_principle = 0;
			  foreach($sales_perbulan as $sales_perbulans){
				  
				  //$month_var = $sales_perbulans['bulan'];
				   $html_sbs3 .= '
				   <tr>
				   <td> '.$sales_perbulans['stock_name'].'</td>
				   <td> '.$sales_perbulans['cust_name'].'</td>
				   <td> '.$sales_perbulans['Kemasan'].'</td>
				   <td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtot_jan'],2,',','.').'</td>
				   <td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtotkg_jan'],2,',','.').'</td>
				    <td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtot_feb'],2,',','.').'</td>
				   <td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtotkg_feb'],2,',','.').'</td>
					<td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtot_mar'],2,',','.').'</td>
				   <td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtotkg_mar'],2,',','.').'</td>
					<td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtot_apr'],2,',','.').'</td>
				   <td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtotkg_apr'],2,',','.').'</td>
					<td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtot_may'],2,',','.').'</td>
				   <td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtotkg_may'],2,',','.').'</td>
					<td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtot_jun'],2,',','.').'</td>
				   <td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtotkg_jun'],2,',','.').'</td>
					<td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtot_jul'],2,',','.').'</td>
				   <td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtotkg_jul'],2,',','.').'</td>
					<td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtot_aug'],2,',','.').'</td>
				   <td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtotkg_aug'],2,',','.').'</td>
					<td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtot_sep'],2,',','.').'</td>
				   <td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtotkg_sep'],2,',','.').'</td>
					<td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtot_oct'],2,',','.').'</td>
				   <td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtotkg_oct'],2,',','.').'</td>
					<td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtot_nov'],2,',','.').'</td>
				   <td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtotkg_nov'],2,',','.').'</td>
					<td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtot_dec'],2,',','.').'</td>
				   <td style="text-align:right">Rp. '.number_format($sales_perbulans['sumtotkg_dec'],2,',','.').'</td>
					<td style="text-align:right">Rp. '.number_format($sales_perbulans['totalsumoftotal'],2,',','.').'</td>
				   <td style="text-align:right">Rp. '.number_format($sales_perbulans['sumoftotal_lt_kg'],2,',','.').'</td>
				   </tr>';
				   
				    // $sumtot = $sumtot + $sales_perbulans['sumtot'];
					// $sumppn = $sumppn + $sales_perbulans['sumppn'];
					// $sumtotppn = $sumtotppn + $sales_perbulans['sumtotppn'];
				  
			  }
			  
	  
	  $data = array(
			'priv' => $priv,
			'sales_b' => $html_all,
			'sales_p' => $html_sbs2,
			'sales_pc' => $html_sbs3
		);
	  
    $this->template->load('maintemplate', 'report_keuangan/views/index',$data);
  }

  function lists()
  {

    if( !empty($_GET['sess_user_id']) ) {
			  $sess_user_id = $_GET['sess_user_id'];
		  } else {
			  $sess_user_id = NULL;
		  }
		  
		   if( !empty($_GET['sess_token']) ) {
			  $sess_token = $_GET['sess_token'];
		  } else {
			  $sess_token = NULL;
		  }
		  
		    if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		  if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		  if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 				
		  $order_fields = array('coa'); // , 'COST'
		  $order = $this->input->get_post('order');
		  if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'desc';}; 
		  if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 	
		  
		    $params['limit'] 		= (int) $length;
			$params['offset'] 		= (int) $start;
			$params['order_column'] = $order_fields[$order_column];
			$params['order_dir'] 	= $order_dir;
			$params['sess_user_id'] = $sess_user_id;
			$params['sess_token'] 	= $sess_token;
			$params['searchtxt'] 	= $_GET['search']['value'];
		  

    //print_r($params);die;

    $list = $this->uom_model->lists($params);
	$priv = $this->priv->get_priv();
    //print_r($list);die;

    $result["recordsTotal"] = $list['total'];
    $result["recordsFiltered"] = $list['total_filtered'];
    $result["draw"] = $draw;

    //print_r($result);die;

    $data = array();
    $i = $params['offset'];
    $username = $this->session->userdata['logged_in']['username'];
    foreach ($list['data'] as $k => $v) {
      $i = $i + 1;


      $status_akses = '
                <div class="btn-group" style="display:'.$priv['update'].'"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edituom(\'' . $v['id'] . '\')"><i class="fa fa-edit"></i></button></div>
				  <div class="btn-group" style="display:'.$priv['delete'].'"><button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteuom(\'' . $v['id'] . '\')"><i class="fa fa-trash"></i></button></div>';
				  
				  if($v['type_cash'] == 0){
					  $sss = 'Pemasukan';
				  }else{
					  $sss = 'Pengeluaran';
				  }

      array_push($data, array(
        $i,
        $v['coa'].'-'.$v['keterangan'],
        $sss,
        number_format($v['value_real'],2,',','.'),
        $v['note'],
        $status_akses
      ));
    }

    $result["data"] = $data;

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function add()
  {
	  
	  
    $coa = $this->uom_model->get_coa();



    // $this->load->view('add_modal_view', $data);

    // $result = $this->distributor_model->location();

    $data = array(
      'group' => '',
      'coa' => $coa
    );

    $this->template->load('maintemplate', 'report_keuangan/views/add_modal_view', $data);
  }

  public function edit()
  {
    //$id = $this->Anti_sql_injection($this->input->post('sid', TRUE));
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('iduom', TRUE)),
		);
    $result = $this->uom_model->get_uom_data($this->Anti_sql_injection($data['id']));
	 $coa = $this->uom_model->get_coa();

    //print_r($result);die;
    // $roles = $this->uom_model->roles($id);

    $data = array(
      'uom' => $result[0],
      'id' => $this->Anti_sql_injection($this->input->post('iduom', TRUE)),
	  'coa' => $coa
    );

    $this->template->load('maintemplate', 'report_keuangan/views/edit_modal_view', $data);
  }

  public function deletes()
  {

    $data   = file_get_contents("php://input");
    $params   = json_decode($data, true);

    $list = $this->uom_model->deletes($this->Anti_sql_injection($params['id']));

    $res = array(
      'status' => 'success',
      'message' => 'Data telah di hapus'
    );

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }

  public function edit_uom()
  {
	$this->form_validation->set_rules('coa', 'coa', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('tgl', 'tgl', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('cash', 'cash', 'trim|required|max_length[255]');
    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));
      $result = array(
        'success' => false,
        'message' => $msg
      );
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      // die;
      $message = "";
      $data = array(
       'user_id' => $this->session->userdata['logged_in']['user_id'],
        'coa'     => $this->Anti_sql_injection($this->input->post('coa', TRUE)),
        'id'     => $this->Anti_sql_injection($this->input->post('id', TRUE)),
        'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
        'cash' => $this->Anti_sql_injection($this->input->post('cash', TRUE)),
        'jumlah' => $this->Anti_sql_injection($this->input->post('jumlah', TRUE)),
        'ket' => $this->Anti_sql_injection($this->input->post('ket', TRUE))
      );
      $result = $this->uom_model->edit_uom($data);
      if ($result > 0) {
        $msg = 'Berhasil merubah uom.';

        $result = array(
          'success' => true,
          'message' => $msg
        );
      } else {
        $msg = 'Gagal merubah uom.';

        $result = array(
          'success' => false,
          'message' => $msg
        );
      }

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
  }

  public function add_uom()
  {
    $this->form_validation->set_rules('coa', 'coa', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('tgl', 'tgl', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('cash', 'cash', 'trim|required|max_length[255]');
    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));
      $result = array(
        'success' => false,
        'message' => $msg
      );

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      // die;
      $message = "";
      $data = array(
        'user_id' => $this->session->userdata['logged_in']['user_id'],
        'coa'     => $this->Anti_sql_injection($this->input->post('coa', TRUE)),
        'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
        'cash' => $this->Anti_sql_injection($this->input->post('cash', TRUE)),
        'jumlah' => $this->Anti_sql_injection($this->input->post('jumlah', TRUE)),
        'ket' => $this->Anti_sql_injection($this->input->post('ket', TRUE))
      );

		//print_r($data);die;

      $result = $this->uom_model->add_uom($data);

      if ($result > 0) {
        $msg = 'Berhasil menambahkan uom.';

        $result = array(
          'success' => true,
          'message' => $msg
        );
      } else {
        $msg = 'Gagal menambahkan uom ke database.';
        $result = array(
          'success' => false,
          'message' => $msg
        );
      }
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
  }
}
