<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Item extends REST_Controller {

  public function __construct() {
    parent::__construct();
	$this->load->model('item_model');
	$this->load->model('selling/selling_model');
	$this->load->model('invoice_selling/return_model');
  }

  /**
   * anti sql injection
   */
  public function Anti_sql_injection($string) {
    $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
    return $string;
  }

  public function index() {

    $this->template->load('login_view');
  }

  function get_item_post() {
    $data = json_decode(file_get_contents("php://input"), true);
	
	//$lokasi = $this->session->userdata['logged_in']['lokasi'];
	
	//print_r($lokasi);die;
	
	$lokasi = $this->item_model->get_lokasi($data); 
	
	
	
	if($lokasi[0]['lokasi'] == 0){
		$list = $this->item_model->get_selling_a($data); 
	}else{
		$list = $this->item_model->get_selling($data,$lokasi[0]['lokasi']); 
	}
	
	
	
	$check_token = $this->item_model->check_token($data);
	
	if($check_token[0]['CNT'] == 0){
		
		$header = 'HTTP/1.1 400 Bad Request';
		$res = array(
                'status' => 'Error',
                'message' => 'Token Tidak Sama',
                'data' => []
		);
		
	}else{
		
		$header = 'HTTP/1.1 200 OK';
		
		//$list = $this->item_model->get_selling($data);  
		$res = array(
                'status' => 'Success',
                'message' => 'Success Request',
                'data' => $list
		);
		
		
	}


    header($header);
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }


  function get_item2_post() {
    $data = json_decode(file_get_contents("php://input"), true);
	
	$list = $this->item_model->get_item($data); 
	$check_token = $this->item_model->check_token($data);
	
	if($check_token[0]['CNT'] == 0){
		
		$header = 'HTTP/1.1 400 Bad Request';
		$res = array(
                'status' => 'Error',
                'message' => 'Token Tidak Sama',
                'data' => []
		);
		
	}else{
		
		$header = 'HTTP/1.1 200 OK';
		
		$list = $this->item_model->get_item($data);  
		$array_this=array();
		foreach($list as $list2){
			$array=array(
			"id_mat"=> $list2["id_mat"],
            "stock_code"=> $list2["stock_code"],
            "stock_name"=> $list2["stock_name"],
            "stock_description"=> $list2["stock_description"],
            "qty"=>floatval($list2["qty"]),
            "base_qty"=> floatval($list2["base_qty"]),
            "uom_name"=> $list2["uom_name"],
            "uom_symbol"=> $list2["uom_symbol"],
            "unit_box"=> floatval($list2["unit_box"]),
            "name_eksternal" =>$list2["name_eksternal"],
			"kode_eksternal" => $list2["kode_eksternal"],
			"pajak" => $list2["pajak"],
			"box_sisa" => floatval($list2["box_sisa"])
			);
			$array_this[]=$array;
		}
		$res = array(
                'status' => 'Success',
                'message' => 'Success Request',
                'data' => $array_this
		);
		
		
	}


    header($header);
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }

  function get_dpenjualan_post() {
    $data = json_decode(file_get_contents("php://input"), true);
	
	$list = $this->item_model->get_item($data); 
	$check_token = $this->item_model->check_token($data);
	
	if($check_token[0]['CNT'] == 0){
		
		$header = 'HTTP/1.1 400 Bad Request';
		$res = array(
                'status' => 'Error',
                'message' => 'Token Tidak Sama',
                'data' => []
		);
		
	}else{
		
		$header = 'HTTP/1.1 200 OK';
		
		
		$data = array(
			'id' => $this->Anti_sql_injection($data['id_penjualan'], TRUE),
			
		);
		
		// $params['region'] = 4;
		
		// $result = $this->selling_model->get_customer($params);
		
		$po_result = $this->item_model->get_penjualan($data);
		$po_detail_result = $this->item_model->get_penjualan_detail($data);
		
		
		// $params = array(
		// 	'id_cust' => $po_result[0]['id_customer'],
		// );
		
		$item = $this->selling_model->get_item_byprin_all();
		//print_r($principal_result);die;
		
		// $result = $this->distributor_model->location();

		$data = array(
			'penjualan' => $po_result[0],
			//'principal' => $result,
			'detail' => $po_detail_result,
			//'item' => $item
		);
		
		
		//$list = $this->item_model->get_item($data);  
		$res = array(
                'status' => 'Success',
                'message' => 'Success Request',
                'data' => $data
		);
		
		
	}


    header($header);
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }
  
  function get_invoice_penjualan_post() {
    $data = json_decode(file_get_contents("php://input"), true);
	
	$list = $this->item_model->get_item($data); 
	$check_token = $this->item_model->check_token($data);
	
	if($check_token[0]['CNT'] == 0){
		
		$header = 'HTTP/1.1 400 Bad Request';
		$res = array(
                'status' => 'Error',
                'message' => 'Token Tidak Sama',
                'data' => []
		);
		
	}else{
		
		$header = 'HTTP/1.1 200 OK';
		
		
		$params = array(
			'id' => $this->Anti_sql_injection($data['id_penjualan'], TRUE),
			
		);
		
		$params['region'] = 4;
		
		$result = $this->item_model->get_invoice_penjualan($params);
		
		//$list = $this->item_model->get_item($data);  
		$res = array(
                'status' => 'Success',
                'message' => 'Success Request',
                'data' => $result
		);
		
		
	}


    header($header);
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }
  
   function request_invoice_post() {
    $data = json_decode(file_get_contents("php://input"), true);

    // $param = array(
      // 'id_invoice' => $data['id_invoice'],
      // 'id_invoice' => $data['id_invoice']
      
    // );

	$check_token = $this->item_model->check_token($data);
	
	if($check_token[0]['CNT'] == 0){
		
		$header = 'HTTP/1.1 400 Bad Request';
		$res = array(
                'status' => 'Error',
                'message' => 'Token Tidak Sama',
                'data' => []
		);
		
	}else{
		
		$header = 'HTTP/1.1 200 OK';
		
		$list = $this->item_model->insert_request($data);  
		$res = array(
                'status' => 'Success',
                'message' => 'Success Request',
                'data' => $list
		);
		
		
	}


    header($header);
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }

  function insert_customer_post() {
    $data = json_decode(file_get_contents("php://input"), true);

    $param = array(
      'code_cust' => $data['code_cust'],
			'cust_name' => $data['cust_name'],
			'region' => $data['region'],
			'no_npwp' => $data['no_npwp'],
			'npwp_name' => $data['npwp_name'],
			'npwp_address' => $data['npwp_address'],
			'cust_address' => $data['cust_address'],
			'district' => $data['district'],
			'city' => $data['city'],
			'group' => $data['group'],
			'phone' => $data['phone'],
			'email' => $data['email'],
			'contact_person' => $data['contact_person'],
			'credit_limit' => $data['credit_limit'],
			'status_credit' => 1
      
    );

	$check_token = $this->item_model->check_token($data);
	
	if($check_token[0]['CNT'] == 0){
		
		$header = 'HTTP/1.1 400 Bad Request';
		$res = array(
                'status' => 'Error',
                'message' => 'Token Tidak Sama',
                'data' => []
		);
		
	}else{
		
		$header = 'HTTP/1.1 200 OK';
		
		$list = $this->item_model->insert_customer($param);  
		$res = array(
                'status' => 'Success',
                'message' => 'Success Request',
                'data' => $list
		);
		
		
	}


    header($header);
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }

  function edit_customer_post() {
    $data = json_decode(file_get_contents("php://input"), true);

    $param = array(
      'id_t_cust' => $data['id_t_cust'],
      'code_cust' => $data['code_cust'],
			'cust_name' => $data['cust_name'],
			'region' => $data['region'],
			'no_npwp' => $data['no_npwp'],
			'npwp_name' => $data['npwp_name'],
			'npwp_address' => $data['npwp_address'],
			'cust_address' => $data['cust_address'],
			'district' => $data['district'],
			'city' => $data['city'],
			'group' => $data['group'],
			'phone' => $data['phone'],
			'email' => $data['email'],
			'contact_person' => $data['contact_person'],
			'credit_limit' => $data['credit_limit'],
			'status_credit' => 1
      
    );

	$check_token = $this->item_model->check_token($data);
	
	if($check_token[0]['CNT'] == 0){
		
		$header = 'HTTP/1.1 400 Bad Request';
		$res = array(
                'status' => 'Error',
                'message' => 'Token Tidak Sama',
                'data' => []
		);
		
	}else{
		
		$header = 'HTTP/1.1 200 OK';
		
		$list = $this->item_model->edit_customer($param);  
		$res = array(
                'status' => 'Success',
                'message' => 'Success Request',
                'data' => $list
		);
		
		
	}


    header($header);
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }

  function delete_customer_post() {
    $data = json_decode(file_get_contents("php://input"), true);

    $param = array(
      'id_t_cust' => $data['id_t_cust']
			
      
    );

	$check_token = $this->item_model->check_token($data);
	
	if($check_token[0]['CNT'] == 0){
		
		$header = 'HTTP/1.1 400 Bad Request';
		$res = array(
                'status' => 'Error',
                'message' => 'Token Tidak Sama',
                'data' => []
		);
		
	}else{
		
		$header = 'HTTP/1.1 200 OK';
		
		$list = $this->item_model->delete_customer($param);  
		$res = array(
                'status' => 'Success',
                'message' => 'Success Request',
                'data' => $list
		);
		
		
	}


    header($header);
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }

  function get_customer_post() {
    $data = json_decode(file_get_contents("php://input"), true);
	
	
	
	$check_token = $this->item_model->check_token($data);
	
	if($check_token[0]['CNT'] == 0){
		
		$header = 'HTTP/1.1 400 Bad Request';
		$res = array(
                'status' => 'Error',
                'message' => 'Token Tidak Sama',
                'data' => []
		);
		
	}else{
		
		$header = 'HTTP/1.1 200 OK';
		
		$list2 = $this->item_model->get_user_location($data);
		
		
		
		//$data['lokasi'] = $list2[0]['lokasi'];
		
	
		
		$list = $this->item_model->get_customer($data,$list2[0]['lokasi']);  
			//print_r($list);die;
		$res = array(
                'status' => 'Success',
                'message' => 'Success Request',
                'data' => $list
		);
		
		
	}

	//print_r($res);die;

    header($header);
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }

  
  function get_user_post() {
    $data = json_decode(file_get_contents("php://input"), true);
	
	$list = $this->item_model->get_user($data); 
	$check_token = $this->item_model->check_token($data);
	
	if($check_token[0]['CNT'] == 0){
		
		$header = 'HTTP/1.1 400 Bad Request';
		$res = array(
                'status' => 'Error',
                'message' => 'Token Tidak Sama',
                'data' => []
		);
		
	}else{
		
		$header = 'HTTP/1.1 200 OK';
		
		$list = $this->item_model->get_user($data);  
		$res = array(
                'status' => 'Success',
                'message' => 'Success Request',
                'data' => $list
		);
		
		
	}


    header($header);
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }

  function get_info_post() {
    $data = json_decode(file_get_contents("php://input"), true);
	
	$list = $this->item_model->get_info($data); 
	$check_token = $this->item_model->check_token($data);
	
	if($check_token[0]['CNT'] == 0){
		
		$header = 'HTTP/1.1 400 Bad Request';
		$res = array(
                'status' => 'Error',
                'message' => 'Token Tidak Sama',
                'data' => []
		);
		
	}else{
		
		$header = 'HTTP/1.1 200 OK';
		
		$list = $this->item_model->get_info($data);  
		$res = array(
                'status' => 'Success',
                'message' => 'Success Request',
                'data' => $list
		);
		
		
	}


    header($header);
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }

 function insert_penjualan_post(){
	 
	  $data = json_decode(file_get_contents("php://input"), true);
	
	// print_r($data);die; 
	
	
	$check_token = $this->item_model->check_token($data);
	
	if($check_token[0]['CNT'] == 0){
		
		$header = 'HTTP/1.1 400 Bad Request';
		$res = array(
                'status' => 'Error',
                'message' => 'Token Tidak Sama',
                'data' => []
		);
		
	}else{
		
		$header = 'HTTP/1.1 200 OK';
		
		$items = $data['item'];
		
		if(count($items) == 0){
			$header = 'HTTP/1.1 400 Bad Request';
			$res = array(
					'status' => 'Error',
					'message' => 'Item Tidak boleh Kosong',
					'data' => []
			);
		}else{
			
			$result_g = $this->item_model->get_kode('po');
			if($result_g[0]['max']+1 < 1000){
				if($result_g[0]['max']+1 < 100){
					if($result_g[0]['max']+1 < 10){
						$kode = ''.date('Y').'000'.($result_g[0]['max']+1);
					}else{
						$kode = ''.date('Y').'00'.($result_g[0]['max']+1);
					}
				}else{
					$kode = ''.date('Y').'0'.($result_g[0]['max']+1);
				}
			}else{
				$kode = ''.date('Y').''.($result_g[0]['max']+1);
			}
			
			$total_harga = 0;
			$price_item = 0;
			 foreach($items as $itemss){
				 //$itemss['harga_satuan'] = $itemss['harga_satuan'];
				$get_material = $this->item_model->get_item_by_id($itemss);  
				 
				$total_item = (floatval($itemss['qty_box'])*floatval($get_material[0]['unit_box']))+floatval($itemss['qty_satuan']);
				$price_item =  floatval($total_item*floatval($itemss['harga_satuan']));
				
				$total_harga = floatval($total_harga) + floatval($price_item);
				
			 }
			 
			 $array_penjualan = array(
				
				"id_sales" => $data['id'],
				"no_penjualan" => $kode,
				"date_penjualan" => $data['tanggal_penjualan'],
				"id_customer"=> $data["id_customer"],
				"term_of_payment" => $data['term'],
				"keterangan" => $data['keterangan'],
				"seq_n" => $result_g[0]['max']+1,
				"ppn" => 10,
				"rate" => 1,
				"id_valas" => 1,
				"status" => 0,
				"total_amount" => $total_harga
			 
			 );
			
			$list = $this->item_model->insert_penjualan($array_penjualan);
			$this->selling_model->edit_credit($array_penjualan);

			$date_start = $this->Anti_sql_injection($data['tanggal_penjualan']);
			$due_date_inv = date('Y-m-d', strtotime($date_start. ' + '.$data['term'].' days'));

			$data_inv = array(
				'id_penjualan' => $list['lastid'],
				'note' => $data['keterangan'],
				'tanggal_invoice' => $data['tanggal_penjualan'],
				'due_date' => $due_date_inv,
				'no_invoice' => $kode,
				'attention' => ''
			);
			
			$id_invoice = $this->return_model->add_invoice($data_inv);
			
			
			
			foreach($items as $itemss){
				$itemss['kode'] = $itemss['id_mat'];
				//$itemss['harga_satuan'] = $itemss['harga_satuan'];
				$convertion = $this->selling_model->get_convertion($itemss);
				
				$array_detail_penjualan = array(
					'id_penjualan' => $list['lastid'],
					'id_sales' => $data['id'],
					'id_material' => $itemss['id_mat'],
					'unit_price' => $itemss['harga_satuan'],
					'qty' => (floatval($itemss['qty_box'])*floatval($get_material[0]['unit_box']))+floatval($itemss['qty_satuan']),
					'qty_box' =>$itemss['qty_box'],
					'qty_satuan' =>$itemss['qty_satuan'],
					'price'=>floatval($itemss['harga_satuan'])*((floatval($itemss['qty_box'])*floatval($get_material[0]['unit_box']))+floatval($itemss['qty_satuan'])),
					'box_ammount' =>$itemss['qty_box'],
					'convertion' => $convertion[0]['convertion'],
					'base_qty' => $convertion[0]['base_qty']
				);
				// var_dump($array_detail_penjualan);
				// die;

				$idmutasi = $this->selling_model->add_mutasi($array_detail_penjualan);
				$this->selling_model->edit_qty($array_detail_penjualan);
				$detailpenjualan = $this->selling_model->add_detail_penjualan($array_detail_penjualan,$idmutasi);


				$datas_detail_inv = array(
						
					'id_inv_penjualan' => $id_invoice['lastid'], 
					'id_d_penjualan' => $detailpenjualan['lastid'],
					'unit_price' => $itemss['harga_satuan'],
					'price' => floatval($itemss['harga_satuan'])*((floatval($itemss['qty_box'])*floatval($get_material[0]['unit_box']))+floatval($itemss['qty_satuan'])),
					'qty' => (floatval($itemss['qty_box'])*floatval($get_material[0]['unit_box']))+floatval($itemss['qty_satuan']), 
					'box_ammount' => $itemss['qty_box'],
					'qty_box' => $itemss['qty_box'],
					'qty_satuan' => $itemss['qty_satuan']
					
					//'remark' => $array_itemss['remark'],
				
				);
				
				$detail_invoice = $this->return_model->add_detail_invoice($datas_detail_inv);
				
				 
			 }

			$result_g = $this->item_model->update_seq('penjualan');  

			$res = array(
					'status' => 'Success',
					'message' => 'Success Request'
			);
		}
		

		
		
	}
	 
	  header($header);
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
	 
 }
  
  function test_get() {
    $data = json_decode(file_get_contents("php://input"));

    $res = array(
      'status' => 'success',
      'message' => 'Ini Hanya test'
    );
    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }
  
  
}
