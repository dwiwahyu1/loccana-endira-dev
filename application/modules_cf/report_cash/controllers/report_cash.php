<?php

use function PHPSTORM_META\type;

defined('BASEPATH') or exit('No direct script access allowed');

class Report_cash extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('report_cash/report_cash_model', 'reportCashModel');
		$this->load->model('selling/selling_model');
		$this->load->model('invoice_selling/return_model');
		$this->load->library('log_activity');
		$this->load->library('priv');

		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong');
		$this->form_validation->set_message('min_length', '%s Minimal 4 Karakter');
		$this->form_validation->set_message('max_length', '%s Maksimal 10 Karakter');
	}

	public function index()
	{
		$coa_list = $this->reportCashModel->get_coa_list();

		$this->template->load('maintemplate', 'report_cash/views/index', compact('coa_list'));
	}

	public function list()
	{
		$sess_user_id = !empty($_GET['sess_user_id']) ? $_GET['sess_user_id'] : NULL;
		$sess_token = !empty($_GET['sess_token']) ? $_GET['sess_token'] : NULL;
		$coa_id = !empty($_GET['coa_id']) ? $_GET['coa_id'] : NULL;
		$filter = !empty($_GET['filter']) ? $_GET['filter'] : NULL;
		$filter_month = !empty($_GET['filter_month']) ? $_GET['filter_month'] : NULL;
		$filter_year = !empty($_GET['filter_year']) ? $_GET['filter_year'] : NULL;
		$draw = $this->input->get_post('draw') != FALSE ? $this->input->get_post('draw') : 1;
		$length = $this->input->get_post('length') != FALSE ? $this->input->get_post('length') : 10;
		$start = $this->input->get_post('start') != FALSE ? $this->input->get_post('start') : 0;

		$order_fields = array('id_hp', 'no_payment', 'total', 'tgl_terbit', 'keterangan', 'id_hp', 'id_hp'); // , 'COST'

		$order = $this->input->get_post('order');
		$order_dir = !empty($order[0]['dir']) ? $order[0]['dir'] : 'desc';
		$order_column = !empty($order[0]['column']) ? $order[0]['column'] : 0;

		$params['limit']		= (int) $length;
		$params['offset']		= (int) $start;
		$params['order_column']	= $order_fields[$order_column];
		$params['order_dir']	= $order_dir;
		$params['sess_user_id']	= $sess_user_id;
		$params['sess_token']	= $sess_token;
		$params['filter']		= $filter;
		$params['filter_month']	= $filter_month;
		$params['filter_year']	= $filter_year;
		$params['searchtxt']	= $_GET['search']['value'] ?? null;
		$params['coa_id']		= $coa_id;

		// print_r($params);die;
		$priv = $this->priv->get_priv();

		$list = $this->reportCashModel->list_report_cash($params);
		$typeCoa = ['Debit', 'Kredit'];
		$data = [];
		$balance = 0.00;

		foreach ($list['data'] as $row) {

			if ($row['type_cash'] == 0) {
				$balance += $row['value_real'];
			} else if ($row['type_cash'] == 1) {
				$balance -= $row['value_real'];
			}

			array_push($data, [
				$row['date'], // tanggal
				// $typeCoa[$row['type_cash']] ?? '-', // transaksi
				$row['keterangan'], // transaksi
				$row['coa'], // kode akun
				$row['keterangan'], // akun
				$row['note'] ?? '-', // uraian
				'-', // no.faktur
				$row['type_cash'] == 0 ? indonesia_currency_format($row['value_real']) : '-', // debit
				$row['type_cash'] == 1 ? indonesia_currency_format($row['value_real']) : '-', // kredit
				indonesia_currency_format($balance), // saldo
				// '-', // action
			]);
		}

		$result = [
			'data' => $data,
			'recordsTotal' => $list['total'],
			'recordsFiltered' => $list['total_filtered'],
			'draw' => $draw,
		];

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function excel()
	{
		$params = array(
			'coa_id' => anti_sql_injection($this->input->post('coa_id')),
		);

		$this->load->library('excel');

		$objPHPExcel = new PHPExcel();

		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'Tanggal')
			->setCellValue('B1', 'Transaksi')
			->setCellValue('C1', 'Kode')
			->setCellValue('D1', 'Akun')
			->setCellValue('E1', 'Uraian')
			->setCellValue('F1', 'No. Faktur')
			->setCellValue('G1', 'Debit')
			->setCellValue('H1', 'Kredit')
			->setCellValue('I1', 'Saldo');


		$list = $this->reportCashModel->list_report_cash($params);
		$start = 2;
		$balance = 0.00;
		foreach ($list['data'] as $row) {
			if ($row['type_cash'] == 0) {
				$balance += $row['value_real'];
			} else if ($row['type_cash'] == 1) {
				$balance -= $row['value_real'];
			}

			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A' . $start, $row['date']) // tanggal
				->setCellValue('B' . $start, $row['keterangan']) // transaksi
				->setCellValue('C' . $start, $row['coa']) // kode akun
				->setCellValue('D' . $start, $row['keterangan']) // akun
				->setCellValue('E' . $start, $row['note']) // uraian
				->setCellValue('F' . $start, '-') // no. faktur
				->setCellValue('G' . $start, $row['type_cash'] == 0 ? indonesia_currency_format($row['value_real']) : '-') // debit
				->setCellValue('H' . $start, $row['type_cash'] == 1 ? indonesia_currency_format($row['value_real']) : '-') // kredit
				->setCellValue('I' . $start, indonesia_currency_format($balance)); // saldo

			$start++;
		}

		// Redirect output to a client’s web browser (Excel2007)
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename=report_cash.xls');
		header('Cache-Control: max-age=0');
		// If you’re serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');
		// If you’re serving to IE over SSL, then the following may be needed
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
		header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
		unset($objPHPExcel);
	}
}
