<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Selling extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('selling/selling_model');
		$this->load->model('invoice_selling/return_model');
		$this->load->library('log_activity');
		$this->load->library('priv');

		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong');
		$this->form_validation->set_message('min_length', '%s Minimal 4 Karakter');
		$this->form_validation->set_message('max_length', '%s Maksimal 10 Karakter');
	}

	/**
	 * anti sql injection
	 * @return string
	 */
	public function Anti_sql_injection($string)
	{
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	 * This function is redirect to index distributor page
	 * @return Void
	 */
	public function index()
	{

		$priv = $this->priv->get_priv();

		$year_count = date('Y') - 2019;

		$filter_year = '<select class="form-control" id="filter_year" onChange="changeFilter()"><option value="ALL" >ALL</option>';

		for ($i = $year_count; $i >= 0; $i--) {

			$y = 2019 + $i;

			if ($y == date('Y')) {
				$filter_year .= '<option value="' . $y . '" selected="selected" >' . $y . '</option>';
			} else {
				$filter_year .= '<option value="' . $y . '" >' . $y . '</option>';
			}
		}
		$filter_year .= '</select>';

		$array_month = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

		$filter_month = '<select class="form-control" id="filter_month" onChange="changeFilter()"><option value="ALL" >ALL</option>';
		foreach ($array_month as $array_months) {
			if ($array_months == date('F')) {
				$filter_month .= '<option value="' . $array_months . '" selected="selected">' . $array_months . '</option>';
			} else {
				$filter_month .= '<option value="' . $array_months . '" >' . $array_months . '</option>';
			}
		}

		$filter_month .= '</select>';
		$today_month = date('F');
		$today_year = date('Y');

		$data = array(
			'priv' => $priv,
			'filter_month' => $filter_month,
			'filter_year' => $filter_year,
			'today_month' => $today_month,
			'today_year' => $today_year
		);

		$this->template->load('maintemplate', 'selling/views/index', $data);
	}

	/**
	 * This function is used for showing distributor list
	 * @return Array
	 */
	function lists()
	{

		$session = $this->session->userdata;
		$params['region'] = $session['logged_in']['idlokasi'];


		if (!empty($_GET['sess_user_id'])) {
			$sess_user_id = $_GET['sess_user_id'];
		} else {
			$sess_user_id = NULL;
		}

		if (!empty($_GET['sess_token'])) {
			$sess_token = $_GET['sess_token'];
		} else {
			$sess_token = NULL;
		}

		if (!empty($_GET['filter_month'])) {
			$filter_month = $_GET['filter_month'];
		} else {
			$filter_month = NULL;
		}

		if (!empty($_GET['filter_year'])) {
			$filter_year = $_GET['filter_year'];
		} else {
			$filter_year = NULL;
		}

		if ($this->input->get_post('draw') != FALSE) {
			$draw   = $this->input->get_post('draw');
		} else {
			$draw   = 1;
		};
		if ($this->input->get_post('length') != FALSE) {
			$length = $this->input->get_post('length');
		} else {
			$length = 10;
		};
		if ($this->input->get_post('start') != FALSE) {
			$start  = $this->input->get_post('start');
		} else {
			$start  = 0;
		};
		$order_fields = array('Rangking', 'no_penjualan', 'cust_name', 'date_penjualan', 'symbol_valas', 'term_of_payment', 'nama', 'Rangking', 'Rangking'); // , 'COST'
		$order = $this->input->get_post('order');
		if (!empty($order[0]['dir'])) {
			$order_dir    = $order[0]['dir'];
		} else {
			$order_dir    = 'asc';
		};
		if (!empty($order[0]['column'])) {
			$order_column = $order[0]['column'];
		} else {
			$order_column = 0;
		};

		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['sess_user_id'] = $sess_user_id;
		$params['sess_token'] 	= $sess_token;
		$params['filter_month'] = $_GET['filter_month'];
		$params['filter_year'] 	= $_GET['filter_year'];
		$params['searchtxt'] 	= $_GET['search']['value'];

		// print_r($params);die;

		$list = $this->selling_model->list_penjualan($params);

		$priv = $this->priv->get_priv();

		//print_r($list['data']);die;
		$data = array();
		foreach ($list['data'] as $k => $v) {

			//if($v['status']==0){
			//if($v['no_invoice']==NULL){
			$sts = '<button type="button" class="btn btn-custon-rounded-two btn-danger" onClick="konfirmasi(' . $v['id_penjualan'] . ')"  > Konfirmasi </button>';
			$action = '
						<div class="btn-group" style="display:' . $priv['update'] . '"><button type="button" class="btn btn-custon-rounded-two btn-warning" data-toggle="tooltip" data-placement="top" title="Edit"  onClick="updatepo(' . $v['id_penjualan'] . ')" > <i class="fa fa-edit"></i> </button></div>
						<div class="btn-group" style="display:' . $priv['delete'] . '"><button type="button" class="btn btn-custon-rounded-two btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"  onClick="deletepo(' . $v['id_penjualan'] . ')" > <i class="fa fa-trash"></i> </button></div>
						<div class="btn-group" style="display:' . $priv['detail'] . '"><button type="button" class="btn btn-custon-rounded-two btn-info" data-toggle="tooltip" data-placement="top" title="Detail"  onClick="detail(' . $v['id_penjualan'] . ')" > <i class="fa fa-search-plus"></i>  </button></div>
						<div class="btn-group" style="display:' . $priv['detail'] . '"><button type="button" class="btn btn-custon-rounded-two btn-secondary" data-toggle="tooltip" data-placement="top" title="Print"  onClick="invoice(' . $v['id_penjualan'] . ')" > <i class="fa fa-download"></i>  </button></div>
						
						';
			// }else{
			// $sts = '<button type="button" class="btn btn-custon-rounded-two btn-primary" disabled > Disetujui </button>';
			// $action = '<div class="btn-group" data-toggle="tooltip" data-placement="top" title="Detail" style="display:'.$priv['detail'].'"><button type="button" class="btn btn-custon-rounded-two btn-primary" onClick="detail('. $v['id_penjualan'].')" > <i class="fa fa-search-plus"></i>  </button></div>
			// ';
			// }<div class="btn-group" style="display:'.$priv['update'].'"><button type="button" class="btn btn-custon-rounded-two btn-secondary" data-toggle="tooltip" data-placement="top" title="Print"  onClick="print_pdf('. $v['id_penjualan'].')" > <i class="fa fa-download"></i> </button></div>

			if ($v['term_of_payment'] == 'Cash') {
				$hhh = 'Cash';
				$due_date = '-';
			} else {
				$hhh =  $v['term_of_payment'] . ' Hari';
				$date = date_create($v['date_penjualan']);
				$date_due = date_add($date, date_interval_create_from_date_string($v['term_of_payment'] . " days"));
				$due_date = date_format($date_due, "Y-m-d");
			}

			if ($v['status'] == 0) {
				$stt = " Diterima ";
			} else {
				$stt = " Invoice ";
			}

			array_push(
				$data,
				array(
					number_format($v['Rangking'], 0, ',', '.'),
					$v['no_penjualan'],
					$v['cust_name'],
					$v['date_penjualan'],
					$v['symbol_valas'] . ' ' . number_format($v['total_amount'], 0, ',', '.'),
					$due_date,
					$v['nama'],
					$stt,
					$action
				)
			);
			//$idx++;
		}

		$result["data"] = $data;
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		//$result["data"] = $list['data'];

		echo json_encode($result);

		//$this->json_result($result);

	}

	/**
	 * This function is redirect to add distributor page
	 * @return Void
	 */

	public function invoice()
	{
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			'id_penjualan' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
		);

		//print_r($data);die;
		$session = $this->session->userdata;
		$params['region'] = $session['logged_in']['idlokasi'];
		//$params['region'] = 4;

		$invoice_id = $this->selling_model->get_invoice($data);

		$result = $this->return_model->get_customer($params);
		$result_p = $this->return_model->get_penjualan_all();
		$result_retur = $this->return_model->get_retur_id($data);

		//print_r($invoice_id[0]['id_invoice']);die;

		$result_g = $this->return_model->get_kode();
		$htl = '';
		$itms = '';
		$ii = 1;

		$data2 = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),
			'id_penjualan' => $invoice_id[0]['id_invoice']
		);

		$item = $this->return_model->get_penjualan_detail_update($data2);


		$total_retur = 0;

		$result_penjualan = $this->return_model->get_penjualan_byid_inv_sell($data);
		//print_r($item);die;
		$data = array(
			'principal' => $result,
			// 'kode' => $kode,
			'item' => $result_g,
			'retur' => $result_retur,
			'int_flo' => $ii,
			'result_p' => $result_p,
			'total_retur' => number_format($total_retur, 0, ',', '.'),
			'item_form' => $htl,
			'item_pengiriman' => $itms,
			'id_invoice' => $data['id'],
			// 'penjualan' => $item[0]['id_penjualan'],
			'invoice' => $result_penjualan,
			'grand_total' => number_format($total_retur, 2, ',', '.'),
			'ppn' => number_format(($total_retur / 11), 2, ',', '.'),
			'dpp' => number_format($total_retur - ($total_retur / 11), 2, ',', '.'),
		);

		//$page_size = [241,279];
		//$page_size = array(241, 279);
		$page_size = array('auto', 20);
		$pdf = new Pdf('P', 'mm', 'a5', true, 'UTF-8', false); // tcpdf

		$pdf->SetTitle('Invoice');
		// $pdf->AddPage('P', 'mm', array(210, 140), true, 'UTF-8', false); // old
		$pdf->addPage('L', 'A5', true, 'UTF-8', false); // new

		$pdf->SetFont('', 'B', 10);

		//Cell(width , height , text , border , end line , [align] )

		$pdf->Cell(130, 5, 'PT. ENDIRA ALDA', 0, 0);
		$pdf->Cell(59, 5, 'FAKTUR', 0, 1); //end of line

		//set font to arial, regular, 12pt
		$pdf->SetFont('', 'B', 8);

		$pdf->Cell(130, 5, '', 0, 0);
		$pdf->Cell(59, 5, '', 0, 1); //end of line

		$pdf->Cell(130, 5, 'JL. Sangkuriang NO.38-A', 0, 0);
		$pdf->Cell(25, 5, 'No Faktur ', 0, 0);
		$pdf->Cell(34, 5, ': ' . $result_penjualan[0]['no_penjualan'], 0, 1); //end of line

		$pdf->Cell(130, 5, 'NPWP: 01.555.161.7.428.000', 0, 0);
		$pdf->Cell(25, 5, 'Tanggal', 0, 0);
		$pdf->Cell(34, 5, ': ' . $result_penjualan[0]['date_penjualan'], 0, 1); //end of line
		$pdf->SetFont('', 'B', 10);
		$pdf->Cell(130, 5, 'Pembeli', 0, 0);
		$pdf->SetFont('', '', 8);
		$pdf->Cell(25, 5, 'Alamat Pengiriman   ', 0, 0);
		$pdf->MultiCell(34, 5, ': ' . $result_penjualan[0]['cust_address'], 0, 'L', 0);




		$pdf->Cell(130, 5, $result_penjualan[0]['cust_name'], 0, 0);
		$pdf->Cell(25, 5, 'Phone', 0, 0);
		$pdf->Cell(34, 5, ': ' . $result_penjualan[0]['phone'], 0, 1); //end of line

		$pdf->Cell(130, 5, $result_penjualan[0]['cust_address'], 0, 0);
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->Cell(34, 5, '', 0, 1); //end of line

		//make a dummy empty cell as a vertical spacer

		//	$pdf->Cell(189 ,10,'',0,1);//end of line

		//invoice contents
		// $pdf->SetFont('roboto','B',12);

		$pdf->Cell(10, 5, 'No', 1, 0);
		$pdf->Cell(25, 5, 'Kode', 1, 0);
		$pdf->Cell(50, 5, 'Nama Barang', 1, 0);
		$pdf->Cell(20, 5, 'Qty', 1, 0);
		$pdf->Cell(25, 5, 'Jumlah Box', 1, 0);
		$pdf->Cell(25, 5, 'Harga', 1, 0);
		$pdf->Cell(34, 5, 'Jumlah Rp', 1, 1); //end of line

		// $pdf->SetFont('roboto','',12);

		//Numbers are right-aligned so we give 'R' after new line parameter
		$ii = 1;
		$dpp = 0;
		foreach ($item as $items) {
			$pdf->Cell(10, 5, $ii, 1, 0);
			$pdf->Cell(25, 5, $items['stock_code'], 1, 0);
			$pdf->Cell(50, 5, $items['stock_name'] . ' ' . $items['base_qty'] . ' ' . $items['uom_symbol'], 1, 0);
			$pdf->Cell(20, 5, $items['qty_box_retur'], 1, 0, 'L');
			$pdf->Cell(25, 5, 'Box @ ' . $items['box_ammount'], 1, 0, 'R');
			$pdf->Cell(25, 5, number_format($items['unit_price'], 2, ',', '.'), 1, 0, 'R');
			$pdf->Cell(34, 5, number_format($items['price'], 2, ',', '.'), 1, 1, 'R'); //end of line
			$ii++;
			$total_retur += $items['price'];
			$dpp += (($items['price'] / (10 + ($items['pajak'] / 10))) * 10);
		}

		//summary
		$pdf->Cell(130, 5, 'MENGETAHUI,', 0, 0);
		$pdf->Cell(25, 5, 'Total', 0, 0);
		$pdf->Cell(6, 5, 'Rp', 1, 0);
		$pdf->Cell(28, 5, number_format($total_retur, 2, ',', '.'), 1, 1, 'R'); //end of line

		$pdf->Cell(130, 5, '', 0, 0);
		$pdf->Cell(25, 5, 'DPP', 0, 0);
		$pdf->Cell(6, 5, 'Rp', 1, 0);
		$pdf->Cell(28, 5, number_format($dpp, 2, ',', '.'), 1, 1, 'R'); //end of line

		$pdf->Cell(130, 5, '_____________', 0, 0);
		$pdf->Cell(25, 5, 'PPN', 0, 0);
		$pdf->Cell(6, 5, 'Rp', 1, 0);
		$pdf->Cell(28, 5, number_format(($total_retur - $dpp), 2, ',', '.'), 1, 1, 'R'); //end of line

		$pdf->Cell(130, 5, '', 0, 0);
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->Cell(6, 5, '', 0, 0);
		//$pdf->Cell(28, 5, '', 0, 1, 'R'); //end of line		
		$last_row = $pdf->GetY();
		$line = 130;
		$margin = $line - $last_row;
		$pdf->SetPrintFooter(false);
		// $pdf->AddPage('P', 'mm', array(210, 540), true, 'UTF-8', false); // old
		$pdf->AddPage('L', 'A5', true, 'UTF-8', false); // new
		//$pdf->Line(10,$line,190,$line);

		//$pdf->Cell(189, $margin, '', 0, 1); //end of line
		// print_r($pdf->GetY());
		$pdf->SetFont('', 'B', 10);

		//Cell(width , height , text , border , end line , [align] )

		$pdf->Cell(130, 5, 'PT. ENDIRA ALDA', 0, 0);
		$pdf->Cell(59, 5, 'DO', 0, 1); //end of line

		//set font to arial, regular, 12pt
		$pdf->SetFont('', '', 8);

		$pdf->Cell(130, 5, '', 0, 0);
		$pdf->Cell(59, 5, '', 0, 1); //end of line

		$pdf->Cell(130, 5, 'JL. Sangkuriang NO.38-A', 0, 0);
		$pdf->Cell(25, 5, 'No DO ', 0, 0);
		$pdf->Cell(34, 5, ': ' . $result_penjualan[0]['no_penjualan'], 0, 1); //end of line

		$pdf->Cell(130, 5, 'NPWP: 01.555.161.7.428.000', 0, 0);
		$pdf->Cell(25, 5, 'Tanggal', 0, 0);
		$pdf->Cell(34, 5, ': ' . $result_penjualan[0]['date_penjualan'], 0, 1); //end of line
		$pdf->SetFont('', 'B', 10);
		$pdf->Cell(130, 5, 'Pembeli', 0, 0);
		$pdf->SetFont('', '', 8);
		$pdf->Cell(25, 5, 'Alamat Pengiriman   ', 0, 0);
		$pdf->MultiCell(34, 5, ': ' . $result_penjualan[0]['cust_address'], 0, 'L', 0);




		$pdf->Cell(130, 5, $result_penjualan[0]['cust_name'], 0, 0);
		$pdf->Cell(25, 5, 'Phone', 0, 0);
		$pdf->Cell(34, 5, ': ' . $result_penjualan[0]['phone'], 0, 1); //end of line

		$pdf->Cell(130, 5, $result_penjualan[0]['cust_address'], 0, 0);
		$pdf->Cell(25, 5, '', 0, 0);
		$pdf->Cell(34, 5, '', 0, 1); //end of line
		//make a dummy empty cell as a vertical spacer
		//$pdf->Cell(189 ,10,'',0,1);//end of line

		//invoice contents
		// $pdf->SetFont('roboto','B',12);

		$pdf->Cell(10, 5, 'No', 1, 0);
		$pdf->Cell(25, 5, 'Kode', 1, 0);
		$pdf->Cell(50, 5, 'Nama Barang', 1, 0);
		$pdf->Cell(20, 5, 'Qty', 1, 0);
		$pdf->Cell(25, 5, 'Jumlah Box', 1, 0);
		$pdf->Cell(34, 5, 'Banyaknya Kemasan', 1, 0);
		$pdf->Cell(25, 5, 'Liter/Kg', 1, 1); //end of line

		// $pdf->SetFont('roboto','',12);

		//Numbers are right-aligned so we give 'R' after new line parameter
		$i = 1;
		$qty = 0;
		$price = 0;
		$qty_retur = 0;
		$ltkg = 0;
		foreach ($item as $items) {
			$pdf->Cell(10, 5, $i, 1, 0);
			$pdf->Cell(25, 5, $items['stock_code'], 1, 0);
			$pdf->Cell(50, 5, $items['stock_name'] . ' ' . $items['base_qty'] . ' ' . $items['uom_symbol'], 1, 0);
			$pdf->Cell(20, 5, $items['qty_box_retur'], 1, 0, 'L');
			$pdf->Cell(25, 5, 'Box @ ' . $items['box_ammount'], 1, 0, 'R');
			$pdf->Cell(34, 5, number_format($items['qty_retur'], 2, ',', '.'), 1, 0, 'R');
			$pdf->Cell(25, 5, number_format($items['bpl'] * $items['qty_box_retur'], 2, ',', '.'), 1, 1, 'R'); //end of line
			$i++;
			$qty += $items['qty_box_retur'];
			$qty_retur += $items['qty_retur'];
			$price += $items['unit_price'];
			$ltkg += $items['bpl'] * $items['qty_box_retur'];
		}

		$pdf->Cell(10, 5, '', 1, 0);
		$pdf->Cell(25, 5, '', 1, 0);
		$pdf->Cell(50, 5, 'Jumlah', 1, 0);
		$pdf->Cell(20, 5, $qty, 1, 0, 'L');
		$pdf->Cell(25, 5, '', 1, 0, 'R');
		$pdf->Cell(34, 5, number_format($qty_retur, 2, ',', '.'), 1, 0, 'R');
		$pdf->Cell(25, 5, number_format($ltkg, 2, ',', '.'), 1, 1, 'R'); //end of line


		// $pdf->Cell(189 ,10,'',0,1);//end of line
		// $pdf->Cell(189 ,10,'',0,1);//end of line

		$pdf->Cell(63, 5, 'Penerima', 0, 0, 'C');
		$pdf->Cell(63, 5, 'Menyetujui', 0, 0, 'C');
		$pdf->Cell(63, 5, 'Pengirim', 0, 1, 'C');

		$pdf->Cell(63, 5, '', 0, 0, 'C');
		$pdf->Cell(63, 5, '', 0, 0, 'C');
		$pdf->Cell(63, 5, '', 0, 1, 'C');

		$pdf->Cell(63, 5, '_____________', 0, 0, 'C');
		$pdf->Cell(63, 5, '_____________', 0, 0, 'C');
		$pdf->Cell(63, 5, '_____________', 0, 1, 'C');

		// $pdf->Cell(63, 5, '', 0, 0, 'C');
		// $pdf->Cell(63, 5, '', 0, 0, 'C');
		// $pdf->Cell(63, 5, '', 0, 1, 'C');
		// $pdf->Cell(28 ,5,'',0,1,'R');//end of line

		$pdf->SetPrintFooter(false);
		// $pdf->AddPage('P', 'mm', 'A4', true, 'UTF-8', false); //old
		//$pdf->AddPage('P', 'A4', true, 'UTF-8', false); // new
		$pdf->addPage('L', 'A5', true, 'UTF-8', false); // new
		// $pdf->Line(10,$line,190,$line);

		// $pdf->Cell(189 ,$margin,'',0,1);//end of line
		// print_r($pdf->GetY());
		$pdf->SetFont('', 'B', 10);

		//Cell(width , height , text , border , end line , [align] )

		$pdf->Cell(130, 5, date('Y-M-d'), 0, 0);
		$pdf->Cell(59, 5, '', 0, 1); //end of line

		//set font to arial, regular, 12pt
		$pdf->SetFont('', '', 8);

		$pdf->Cell(130, 5, '', 0, 0);
		$pdf->Cell(59, 5, '', 0, 1); //end of line


		$pdf->Cell(10, 5, 'No', 1, 0);
		$pdf->Cell(25, 5, 'Customer', 1, 0);
		$pdf->Cell(25, 5, 'Kota', 1, 0);
		$pdf->Cell(50, 5, 'Produk', 1, 0);
		$pdf->Cell(25, 5, 'Pack', 1, 0);
		$pdf->Cell(34, 5, 'Box', 1, 0);
		$pdf->Cell(25, 5, 'Liter/Kg', 1, 1); //end of line

		// $pdf->SetFont('roboto','',12);

		//Numbers are right-aligned so we give 'R' after new line parameter
		$i = 1;
		$qty = 0;
		$price = 0;
		$qty_retur = 0;
		$ltkg = 0;
		foreach ($item as $items) {
			$pdf->Cell(10, 5, $i, 1, 0);
			$pdf->Cell(25, 5, $items['cust_name'], 1, 0);
			$pdf->Cell(25, 5, $items['city'], 1, 0);
			//$pdf->Cell(50 ,5,$items['stock_name'].' '.$items['base_qty'].' '.$items['uom_symbol'],1,0);
			$pdf->Cell(50, 5, $items['stock_name'], 1, 0, 'L');
			$pdf->Cell(25, 5, $items['base_qty'], 1, 0, 'R');
			$pdf->Cell(34, 5, number_format($items['qty_box_retur'], 2, ',', '.'), 1, 0, 'R');
			$pdf->Cell(25, 5, number_format($items['bpl'] * $items['qty_box_retur'], 2, ',', '.'), 1, 1, 'R'); //end of line
			$i++;
			$qty += $items['qty_box_retur'];
			$qty_retur += $items['qty_retur'];
			$price += $items['unit_price'];
			$ltkg += $items['bpl'] * $items['qty_box_retur'];
		}

		$pdf->Cell(135, 5, '', 1, 0);
		// $pdf->Cell(25 ,5,'',1,0);
		// $pdf->Cell(50 ,5,'Jumlah',1,0);
		// $pdf->Cell(20 ,5,$qty,1,0, 'L');
		// $pdf->Cell(25 ,5,'',1,0, 'R');
		$pdf->Cell(34, 5, number_format($qty, 2, ',', '.'), 1, 0, 'R');
		$pdf->Cell(25, 5, number_format($qty_retur, 2, ',', '.'), 1, 1, 'R'); //end of line


		$pdf->Cell(189, 10, '', 0, 1); //end of line
		$pdf->Cell(189, 10, '', 0, 1); //end of line

		$pdf->Cell(63, 5, 'Diminta oleh', 0, 0, 'C');
		$pdf->Cell(63, 5, 'Menyetujui', 0, 0, 'C');
		$pdf->Cell(63, 5, 'Penerima', 0, 1, 'C');

		$pdf->Cell(63, 5, '', 0, 0, 'C');
		$pdf->Cell(63, 5, '', 0, 0, 'C');
		$pdf->Cell(63, 5, '', 0, 1, 'C');

		$pdf->Cell(63, 5, '_____________', 0, 0, 'C');
		$pdf->Cell(63, 5, '_____________', 0, 0, 'C');
		$pdf->Cell(63, 5, '_____________', 0, 1, 'C');

		$pdf->Cell(63, 5, '', 0, 0, 'C');
		$pdf->Cell(63, 5, '', 0, 0, 'C');
		$pdf->Cell(63, 5, '', 0, 1, 'C');


		$pdf->SetPrintFooter(false);

		$pdf->lastPage();

		$pdf->Output('Invoice_' . $result_penjualan[0]['no_penjualan'] . '.pdf', 'i');
		// $this->template->load('maintemplate', 'invoice_selling/views/invoice',$data);
	}

	public function get_customer()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$result['customer'] = $this->selling_model->get_customer_byid($params);

		//print_r($result['customer']);die;
		$item = $this->selling_model->get_item_byprin_all();

		//print_r($item);die;

		$list = '<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		foreach ($item as $items) {


			$list .= '<option value="' . $items['id_mat'] . '|' . $items['unit_box'] . '|' . $items['bottom_price'] . '|' . $items['qty'] . '|' . $items['pajak'] . '" >' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '</option>';
		}
		//$list .= '<option value="lain" selected="selected" >-- Pilih Item Lainnya --</option>';

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function get_all_item()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$item = $this->selling_model->get_item_byprin($params);

		$list = '<option value="0" selected="selected" disabled>-- Pilih Item --</option>';
		foreach ($item as $items) {

			$list .= '<option value="' . $items['id_mat'] . '" >' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '</option>';
		}
		$list .= '<option value="0" selected="selected" disabled>-- Pilih Item Lainnya --</option>';

		$item2 = $this->selling_model->get_item_byprin_all($params);

		foreach ($item2 as $items) {

			$list .= '<option value="' . $items['id_mat'] . '" >' . $items['stock_code'] . ' - ' . $items['stock_name'] . ' - ' . $items['base_qty'] . ' ' . $items['uom_symbol'] . '</option>';
		}
		//	$list .= '------------------';

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}


	public function get_price_mat()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		//print_r($params);die;

		$ddd = $this->selling_model->get_price_mat($params);
		//$item = $this->selling_model->get_item_byprin($params);

		if (count($ddd) == 0) {
			$list = 0;
		} else {
			$list = number_format($ddd[0]['unit_price'], 0, ',', '.');
		}
		//print_r($ddd);die;

		$result['list'] = $list;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add()
	{

		$session = $this->session->userdata;
		//print_r($session);die;
		if ($session['logged_in']['idlokasi'] == 0 || $session['logged_in']['idlokasi'] == 4) {
			$params['region'] = 0;
			$result = $this->selling_model->get_customer_all($params);
		} else {
			$params['region'] = $session['logged_in']['idlokasi'];
			$result = $this->selling_model->get_customer($params);
		}



		$result_g = $this->selling_model->get_kode();

		//print_r($result_g);die;

		if ($result_g[0]['max'] + 1 < 1000) {
			if ($result_g[0]['max'] + 1 < 100) {
				if ($result_g[0]['max'] + 1 < 10) {
					$kode = '' . date('Y') . '000' . ($result_g[0]['max'] + 1);
				} else {
					$kode = '' . date('Y') . '00' . ($result_g[0]['max'] + 1);
				}
			} else {
				$kode = '' . date('Y') . '0' . ($result_g[0]['max'] + 1);
			}
		} else {
			$kode = '' . date('Y') . '' . ($result_g[0]['max'] + 1);
		}
		//print_r($kode);die;

		$data = array(
			'principal' => $result,
			'kode' => $kode,
			'item' => $result_g
		);

		$this->template->load('maintemplate', 'selling/views/addPo', $data);
		//$this->load->view('addPrinciple',$data);
	}


	public function updatepo()
	{
		$session = $this->session->userdata;
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),

		);

		if ($session['logged_in']['idlokasi'] == 0 || $session['logged_in']['idlokasi'] == 4) {
			$params['region'] = 0;
			$result = $this->selling_model->get_customer_all($params);
		} else {
			$params['region'] = $session['logged_in']['idlokasi'];
			$result = $this->selling_model->get_customer($params);
		}

		//$result = $this->selling_model->get_customer($params);

		$po_result = $this->selling_model->get_penjualan($data);
		$po_detail_result = $this->selling_model->get_penjualan_detail($data);


		$params = array(
			'id_cust' => $po_result[0]['id_customer'],
		);

		$item = $this->selling_model->get_item_byprin_all();
		//print_r($po_detail_result);die;

		// $result = $this->distributor_model->location();

		$data = array(
			'po' => $po_result[0],
			'principal' => $result,
			'po_mat' => $po_detail_result,
			'item' => $item
		);

		$this->template->load('maintemplate', 'selling/views/updatePo', $data);
		//$this->load->view('addPrinciple',$data);
	}

	public function detail()
	{
		$session = $this->session->userdata;
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),

		);

		if ($session['logged_in']['idlokasi'] == 0 || $session['logged_in']['idlokasi'] == 4) {
			$params['region'] = 0;
			$result = $this->selling_model->get_customer_all($params);
		} else {
			$params['region'] = $session['logged_in']['idlokasi'];
			$result = $this->selling_model->get_customer($params);
		}

		$result = $this->selling_model->get_customer($params);

		$po_result = $this->selling_model->get_penjualan($data);
		$po_detail_result = $this->selling_model->get_penjualan_detail($data);


		$params = array(
			'id_cust' => $po_result[0]['id_customer'],
		);

		$item = $this->selling_model->get_item_byprin_all();
		//print_r($po_result);die;

		// $result = $this->distributor_model->location();

		$data = array(
			'po' => $po_result[0],
			'principal' => $result,
			'po_mat' => $po_detail_result,
			'item' => $item
		);

		$this->template->load('maintemplate', 'selling/views/detailPo', $data);
		//$this->load->view('addPrinciple',$data);
	}

	public function konfirmasi()
	{
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idpo', TRUE)),

		);

		//print_r($data);

		$po_result = $this->selling_model->get_po($data);
		$po_detail_result = $this->selling_model->get_po_detail($data);
		$result = $this->selling_model->get_principal();

		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);

		$item = $this->selling_model->get_item_byprin($params);
		//print_r($principal_result);die;

		// $result = $this->distributor_model->location();

		$data = array(
			'po' => $po_result[0],
			'principal' => $result,
			'po_mat' => $po_detail_result,
			'item' => $item
		);

		$this->template->load('maintemplate', 'selling/views/approvallPo', $data);
		//$this->load->view('addPrinciple',$data);
	}

	public function approve_po()
	{

		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$data = array(
			'id_po' => $params['id'],
			'sts' => $params['sts']

		);

		//print_r($data);die;

		$add_prin_result = $this->selling_model->edit_po_apr($data);
	}

	public function add_po()
	{

		// $this->form_validation->set_rules('kode', 'Kode Principal', 'required|min_length[4]|max_length[10]');
		// $this->form_validation->set_rules('alamat', 'Alamat Principal', 'required');
		$this->form_validation->set_rules('tgl', 'Tanggal Penjualan', 'required');
		$this->form_validation->set_rules('name', 'Customer ', 'required');
		//$this->form_validation->set_rules('tgl', 'Tanggal Tidak Boleh Kosong', 'required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();

			//echo $pesan;die;

			//$msg = strip_tags(str_replace("\r", '', $pesan));
			$msg = $pesan;

			$result = array('success' => false, 'message' => $msg);
		} else {

			$int_val = $this->Anti_sql_injection($this->input->post('int_flo', TRUE));

			$array_items = [];

			$total_amount = 0;
			for ($i = 0; $i <= $int_val; $i++) {


				$kode_split = explode('|', $this->Anti_sql_injection($this->input->post('kode_' . $i, TRUE)));
				if ($kode_split[0]) {
					$array_items[$i]['kode'] = $kode_split[0];
					$array_items[$i]['qty_stock'] = $kode_split[3];
					//$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE));
					$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE));
					$array_items[$i]['qty'] = $this->Anti_sql_injection($this->input->post('qty_' . $i, TRUE));
					$array_items[$i]['qtys'] = $this->Anti_sql_injection($this->input->post('qtys_' . $i, TRUE));
					$array_items[$i]['qty_box'] = $kode_split[1];
					$array_items[$i]['total'] = ((intval(str_replace('.', '', $array_items[$i]['qty']) * intval($kode_split[1])) + intval(str_replace('.', '', $array_items[$i]['qtys'])))) * floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE)))));
					//	$array_items[$i]['remark'] = $this->Anti_sql_injection($this->input->post('remark_'.$i, TRUE));
					$array_items[$i]['totalqty'] = $this->Anti_sql_injection($this->input->post('totalqty_' . $i, TRUE));
					$total_amount = $total_amount + (((intval(str_replace('.', '', $array_items[$i]['qty']) * intval($kode_split[1])) + intval(str_replace('.', '', $array_items[$i]['qtys'])))) * floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE))))));
				}
			}

			//print_r($array_items);die;

			//$total_amounts = $total_amount + ($total_amount * (floatval($this->Anti_sql_injection($this->input->post('ppn', TRUE)))/100));

			$result_g = $this->selling_model->get_kode();

			if ($result_g[0]['max'] + 1 < 1000) {
				if ($result_g[0]['max'] + 1 < 100) {
					if ($result_g[0]['max'] + 1 < 10) {
						$kode = '' . date('Y') . '000' . ($result_g[0]['max'] + 1);
					} else {
						$kode = '' . date('Y') . '00' . ($result_g[0]['max'] + 1);
					}
				} else {
					$kode = '' . date('Y') . '0' . ($result_g[0]['max'] + 1);
				}
			} else {
				$kode = '' . date('Y') . '' . ($result_g[0]['max'] + 1);
			}

			$new_seq = $result_g[0]['max'] + 1;

			if ($this->Anti_sql_injection($this->input->post('term', TRUE)) == 'other') {
				$terms = $this->Anti_sql_injection($this->input->post('term_o', TRUE));
			} else {
				$terms = $this->Anti_sql_injection($this->input->post('term', TRUE));
			}

			$date_start = $this->Anti_sql_injection($this->input->post('tgl', TRUE));
			$due_date_inv = date('Y-m-d', strtotime($date_start . ' + ' . $terms . ' days'));

			$data = array(
				//'no_penjualan' => $this->Anti_sql_injection($this->input->post('kode', TRUE)),
				'no_penjualan' => $kode,
				'id' => $this->Anti_sql_injection($this->input->post('kode', TRUE)),
				'date_penjualan' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
				'id_sales' => $this->session->userdata['logged_in']['user_id'],
				'id_customer' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
				//'ppn' => $this->Anti_sql_injection($this->input->post('ppn', TRUE)),
				'ppn' => 0,
				'term_of_payment' => $terms,
				'keterangan' => $this->Anti_sql_injection($this->input->post('ket', TRUE)),
				'sisa' => floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('sisa', TRUE))))),
				'total_amount' => $total_amount,
				//'seq_n' => $new_seq
				'seq_n' => 0
			);

			//print_r($data);die;


			$add_prin_result = $this->selling_model->add_penjualan($data);
			$this->selling_model->edit_credit($data);

			$data_inv = array(
				'id_penjualan' => $add_prin_result['lastid'],
				'note' => $this->Anti_sql_injection($this->input->post('ket', TRUE)),
				'tanggal_invoice' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
				'due_date' => $due_date_inv,
				'no_invoice' => $this->Anti_sql_injection($this->input->post('kode', TRUE)),
				'attention' => ''
			);

			$id_invoice = $this->return_model->add_invoice($data_inv);

			foreach ($array_items as $array_itemss) {

				if ($array_itemss['kode'] == "") {
				} else {

					if ((intval(str_replace('.', '', $array_itemss['qty'])) + intval(str_replace('.', '', $array_itemss['qtys']))) == 0) {
					} else {

						$convertion = $this->selling_model->get_convertion($array_itemss);
						// print_r($convertion);die;
						$datas = array(

							'id_penjualan' => $add_prin_result['lastid'],
							'id_sales' => $this->session->userdata['logged_in']['user_id'],
							'id_material' => $array_itemss['kode'],
							'id_customer' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
							'unit_price' => str_replace(',', '.', str_replace('.', '', $array_itemss['harga'])),
							'price' => intval(str_replace('.', '', $array_itemss['harga'])) * (((intval(str_replace('.', '', $array_itemss['qty'])) * intval($array_itemss['qty_box'])) + intval(str_replace('.', '', $array_itemss['qtys'])))),
							'qty' => ((intval(str_replace('.', '', $array_itemss['qty'])) * intval($array_itemss['qty_box'])) + intval(str_replace('.', '', $array_itemss['qtys']))),
							'qty_stock' => intval(str_replace('.', '', $array_itemss['qty_stock'])) - (((intval(str_replace('.', '', $array_itemss['qty'])) * intval($array_itemss['qty_box'])) + intval(str_replace('.', '', $array_itemss['qtys'])))),
							'box_ammount' => intval($array_itemss['qty_box']),
							'qty_box' => intval(str_replace('.', '', $array_itemss['qty'])),
							'qty_satuan' => intval(str_replace('.', '', $array_itemss['qtys'])),
							'id_sales' => $this->session->userdata['logged_in']['user_id'],
							'convertion' => $convertion[0]['convertion'],
							'base_qty' => $convertion[0]['base_qty']

							//'remark' => $array_itemss['remark'],

						);

						$id_mutasi = $this->selling_model->add_mutasi($datas);
						$this->selling_model->edit_qty($datas);
						$prin_result = $this->selling_model->add_detail_penjualan($datas, $id_mutasi);

						$datas_detial_inv = array(

							'id_inv_penjualan' => $id_invoice['lastid'],
							'id_d_penjualan' => $prin_result['lastid'],
							'unit_price' => str_replace(',', '.', str_replace('.', '', $array_itemss['harga'])),
							'price' => intval(str_replace('.', '', $array_itemss['harga'])) * (((intval(str_replace('.', '', $array_itemss['qty'])) * intval($array_itemss['qty_box'])) + intval(str_replace('.', '', $array_itemss['qtys'])))),
							'qty' => ((intval(str_replace('.', '', $array_itemss['qty'])) * intval($array_itemss['qty_box'])) + intval(str_replace('.', '', $array_itemss['qtys']))),
							'box_ammount' => intval($array_itemss['qty_box']),
							'qty_box' => intval(str_replace('.', '', $array_itemss['qty'])),
							'qty_satuan' => intval(str_replace('.', '', $array_itemss['qtys']))

							//'remark' => $array_itemss['remark'],

						);

						$detail_invoice = $this->return_model->add_detail_invoice($datas_detial_inv);
					}
				}
			}



			//print_r($add_prin_result);die;

			if ($prin_result['result'] > 0) {


				$result_g = $this->selling_model->update_seq('penjualan');
				//$new_seq = $result_g[0]['max']+1;
				$result_g = $this->selling_model->get_kode();
				if ($result_g[0]['max'] + 1 < 1000) {
					if ($result_g[0]['max'] + 1 < 100) {
						if ($result_g[0]['max'] + 1 < 10) {
							$kode = '' . date('Y') . '000' . ($result_g[0]['max'] + 1);
						} else {
							$kode = '' . date('Y') . '00' . ($result_g[0]['max'] + 1);
						}
					} else {
						$kode = '' . date('Y') . '0' . ($result_g[0]['max'] + 1);
					}
				} else {
					$kode = '' . date('Y') . '' . ($result_g[0]['max'] + 1);
				}


				$msg = 'Berhasil Menambah data Penjualan';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => true, 'message' => $msg, 'new_kode' => $kode);
			} else {
				$msg = 'Gagal Menambah data Penjualan';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => false, 'message' => $msg);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);

		//echo $kode;die;

	}

	public function edit_po()
	{

		// $this->form_validation->set_rules('kode', 'Kode Principal', 'required|min_length[4]|max_length[10]');
		// $this->form_validation->set_rules('alamat', 'Alamat Principal', 'required');
		$this->form_validation->set_rules('tgl', 'Nama Principal', 'required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();

			//echo $pesan;die;

			//$msg = strip_tags(str_replace("\r", '', $pesan));
			$msg = $pesan;

			$result = array('success' => false, 'message' => $msg);
		} else {

			$int_val = $this->Anti_sql_injection($this->input->post('int_flo', TRUE));

			$array_items = [];

			$total_amount = 0;
			for ($i = 0; $i <= $int_val; $i++) {

				$kode_split = explode('|', $this->Anti_sql_injection($this->input->post('kode_' . $i, TRUE)));
				if ($kode_split[0]) {
					$array_items[$i]['kode'] = $kode_split[0];
					$array_items[$i]['qty_stock'] = $kode_split[3];
					//$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_'.$i, TRUE));
					$array_items[$i]['harga'] = $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE));
					$array_items[$i]['qty'] = $this->Anti_sql_injection($this->input->post('qty_' . $i, TRUE));
					$array_items[$i]['qtys'] = $this->Anti_sql_injection($this->input->post('qtys_' . $i, TRUE));
					$array_items[$i]['qty_box'] = $kode_split[1];
					$array_items[$i]['total'] = ((intval(str_replace('.', '', $array_items[$i]['qty']) * intval($kode_split[1])) + intval(str_replace('.', '', $array_items[$i]['qtys'])))) * floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE)))));
					//	$array_items[$i]['remark'] = $this->Anti_sql_injection($this->input->post('remark_'.$i, TRUE));
					$array_items[$i]['totalqty'] = $this->Anti_sql_injection($this->input->post('totalqty_' . $i, TRUE));
					$total_amount = $total_amount + (((intval(str_replace('.', '', $array_items[$i]['qty']) * intval($kode_split[1])) + intval(str_replace('.', '', $array_items[$i]['qtys'])))) * floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('harga_' . $i, TRUE))))));
				}
			}

			$total_amounts = $total_amount + ($total_amount * (floatval($this->Anti_sql_injection($this->input->post('ppn', TRUE))) / 100));

			$result_g = $this->selling_model->get_kode();
			$new_seq = $result_g[0]['max'] + 1;

			if ($this->Anti_sql_injection($this->input->post('term', TRUE)) == 'other') {
				$terms = $this->Anti_sql_injection($this->input->post('term_o', TRUE));
			} else {
				$terms = $this->Anti_sql_injection($this->input->post('term', TRUE));
			}

			$date_start = $this->Anti_sql_injection($this->input->post('tgl', TRUE));
			$due_date_inv = date('Y-m-d', strtotime($date_start . ' + ' . $terms . ' days'));


			$data = array(
				'id' => $this->Anti_sql_injection($this->input->post('id', TRUE)),
				'no_penjualan' => $this->Anti_sql_injection($this->input->post('kode', TRUE)),
				'date_penjualan' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
				'id_sales' => $this->session->userdata['logged_in']['user_id'],
				'id_customer' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
				//'ppn' => $this->Anti_sql_injection($this->input->post('ppn', TRUE)),
				'ppn' => 0,
				'term_of_payment' => $terms,
				'keterangan' => $this->Anti_sql_injection($this->input->post('ket', TRUE)),
				'sisa' => floatval(str_replace(',', '.', str_replace('.', '', $this->Anti_sql_injection($this->input->post('sisa', TRUE))))),
				'total_amount' => $total_amount,
				'seq_n' => $new_seq
			);

			$selling_data = $this->selling_model->get_penjualan($data);
			// $sisa = $selling_data[0]['sisa'] + $selling_data[0]['total_amount'];
			// $sisa_new = $sisa - $total_amount;


			$add_prin_result = $this->selling_model->edit_penjualan($data);

			$add_prin_result = $this->selling_model->edit_credit2($data, $selling_data[0]);

			$data_invoice = array(
				'id_penjualan' => $this->Anti_sql_injection($this->input->post('id', TRUE)),
				'note' => $this->Anti_sql_injection($this->input->post('ket', TRUE)),
				'tanggal_invoice' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
				'due_date' => $due_date_inv,
				'no_invoice' => $this->Anti_sql_injection($this->input->post('kode', TRUE)),
				'attention' => '',
				'id_invoice' => $selling_data[0]['id_invoice']

			);

			$add_prin_result = $this->return_model->edit_invoice($data_invoice);

			$this->return_model->delete_detail_invoice($selling_data[0]['id_invoice']);

			$selling_data_detail = $this->selling_model->get_penjualan_detail($data);

			foreach ($selling_data_detail as $selling_data_detailss) {

				$convertion = $this->selling_model->get_convertion($selling_data_detailss);
				$data_q = array(
					'id_mat' => $selling_data_detailss['id_material'],
					'qty' => floatval($selling_data_detailss['qty']),
					'mutasi_id' => $selling_data_detailss['mutasi_id'],
					'convertion' => $convertion[0]['convertion'],
					'base_qty' => $convertion[0]['base_qty']
				);


				$this->selling_model->update_qty_m($data_q);
				$this->selling_model->delete_mutasi($data_q);
			}

			$this->selling_model->delete_detail_penjualan($this->Anti_sql_injection($this->input->post('id', TRUE)));

			$add_prin_result = $this->selling_model->edit_penjualan($data);
			$this->selling_model->edit_credit($data);

			foreach ($array_items as $array_itemss) {

				if ($array_itemss['kode'] == "") {
				} else {

					$convertion = $this->selling_model->get_convertion($array_itemss);
					$datas = array(
						'id' => $this->Anti_sql_injection($this->input->post('id', TRUE)),
						'id_penjualan' => $this->Anti_sql_injection($this->input->post('id', TRUE)),
						'id_sales' => $this->session->userdata['logged_in']['user_id'],
						'id_material' => $array_itemss['kode'],
						'id_customer' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
						'unit_price' => str_replace(',', '.', str_replace('.', '', $array_itemss['harga'])),
						'price' => intval(str_replace('.', '', $array_itemss['harga'])) * ((intval(str_replace('.', '', $array_itemss['qty']) * intval($array_itemss['qty_box'])) + intval(str_replace('.', '', $array_itemss['qtys'])))),
						'qty' => ((intval(str_replace('.', '', $array_itemss['qty'])) * intval($array_itemss['qty_box'])) + intval(str_replace('.', '', $array_itemss['qtys']))),
						'qty_stock' => intval(str_replace('.', '', $array_itemss['qty_stock'])) - (((intval(str_replace('.', '', $array_itemss['qty'])) * intval($array_itemss['qty_box'])) + intval(str_replace('.', '', $array_itemss['qtys'])))),
						'box_ammount' => intval($array_itemss['qty_box']),
						'qty_box' => intval(str_replace('.', '', $array_itemss['qty'])),
						'qty_satuan' => intval(str_replace('.', '', $array_itemss['qtys'])),
						'convertion' => $convertion[0]['convertion'],
						'base_qty' => $convertion[0]['base_qty']


					);
					$id_mutasi = $this->selling_model->add_mutasi($datas);
					$this->selling_model->edit_qty($datas);

					$prin_result = $this->selling_model->add_detail_penjualan($datas, $id_mutasi);

					$datas_detial_inv = array(

						'id_inv_penjualan' => $selling_data[0]['id_invoice'],
						'id_d_penjualan' => $prin_result['lastid'],
						'unit_price' => str_replace(',', '.', str_replace('.', '', $array_itemss['harga'])),
						'price' => intval(str_replace('.', '', $array_itemss['harga'])) * (((intval(str_replace('.', '', $array_itemss['qty'])) * intval($array_itemss['qty_box'])) + intval(str_replace('.', '', $array_itemss['qtys'])))),
						'qty' => ((intval(str_replace('.', '', $array_itemss['qty'])) * intval($array_itemss['qty_box'])) + intval(str_replace('.', '', $array_itemss['qtys']))),
						'box_ammount' => intval($array_itemss['qty_box']),
						'qty_box' => intval(str_replace('.', '', $array_itemss['qty'])),
						'qty_satuan' => intval(str_replace('.', '', $array_itemss['qtys']))

						//'remark' => $array_itemss['remark'],

					);

					$detail_invoice = $this->return_model->add_detail_invoice($datas_detial_inv);
					//$this->selling_model->update_qty_m($data_q);

				}
			}



			//print_r($add_prin_result);die;

			if ($add_prin_result['result'] > 0) {





				$msg = 'Berhasil Merubah data Purchase Order';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => true, 'message' => $msg);
			} else {
				$msg = 'Gagal Menambah data Purchase Order';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => false, 'message' => $msg);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);

		//echo $kode;die;

	}

	public function delete_po()
	{
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);


		//print_r($params);die;
		$selling_data = $this->selling_model->get_penjualan($params);

		$data2 = array(
			'id_customer' => $selling_data[0]['id_t_cust'],
			'id' => $params['id']
		);

		$add_prin_result = $this->selling_model->edit_credit2($data2, $selling_data[0]);


		$selling_data_detail = $this->selling_model->get_penjualan_detail($data2);
		//print_r($selling_data_detail);die;

		foreach ($selling_data_detail as $selling_data_detailss) {

			$convertion = $this->selling_model->get_convertion($selling_data_detailss);
			$data_q = array(
				'id_mat' => $selling_data_detailss['id_material'],
				'qty' => floatval($selling_data_detailss['qty']),
				'mutasi_id' => $selling_data_detailss['mutasi_id'],
				'convertion' => $convertion[0]['convertion'],
				'base_qty' => $convertion[0]['base_qty']
			);


			$this->selling_model->update_qty_m($data_q);
			$this->selling_model->delete_mutasi($data_q);
		}

		$this->selling_model->delete_detail_penjualan($params['id']);
		$this->selling_model->delete_penjualan($params['id']);


		$result_dist 		= $this->selling_model->delete_po($params['id']);
		//$result_dist2 		= $this->selling_model->delete_po_mat($params['id']);

		$msg = 'Berhasil menghapus data Purchase Order.';
		$result = array('success' => true, 'message' => $msg);
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function print_pdf()
	{
		$data   	= file_get_contents("php://input");
		//$params     = json_decode($data,true);


		//echo "aaa";
		$params = (explode('=', $data));

		$data = array(
			'id' => $params[1],

		);

		//print_r($data);die;

		$po_result = $this->selling_model->get_po_detail_full($data);
		$po_detail_result = $this->selling_model->get_po_detail_full($data);
		//$result = $this->selling_model->get_principal();



		$params = array(
			'id_prin' => $po_result[0]['id_distributor'],
		);

		$array_items = [];

		$total_sub = 0;
		$total_sub_all = 0;
		$total_disk = 0;
		$grand_total = 0;

		$htmls = '';
		foreach ($po_detail_result as $po_detail_results) {

			$amn = number_format(($po_detail_results['price'] - ($po_detail_results['price'] * ($po_detail_results['diskon'] / 100))), 0, ',', '');
			$htmls = $htmls . '
			<tr style="border-top: 1px solid black;" >
								<td  align="left" style="border-top: 1px solid black;"><p>' . $po_detail_results['stock_code'] . '</p></td>
								<td  align="left" style="border-top: 1px solid black;"><p>' . $po_detail_results['stock_name'] . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['base_qty'], '2', ',', '.') . ' ' . $po_detail_results['uom_symbol'] . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['qty_order'], '2', ',', '.') . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['unit_price'], '0', ',', '.') . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($po_detail_results['diskon'], '2', ',', '.') . '</p></td>
								<td  align="right" style="border-top: 1px solid black;"><p>' . number_format($amn, '0', ',', '.') . '</p></td>

							</tr>
			';

			//$total_sub = $total_sub + number_format(($po_detail_results['price']-($po_detail_results['price']*($po_detail_results['diskon']/100))),0,',',''); 
			$total_sub = $total_sub + number_format(($po_detail_results['price'] - ($po_detail_results['price'] * ($po_detail_results['diskon'] / 100))), 0, ',', '');
			$total_sub_all = $total_sub_all + number_format(($po_detail_results['price']), 0, ',', '');
			$total_disk = $total_disk + ($po_detail_results['price'] * ($po_detail_results['diskon'] / 100));
			//$grand_total = $grand_total +  number_format(($po_detail_results['price']),0,',',''); 

		}

		$total_ppn = $total_sub * (floatval($po_result[0]['ppn'])) / 100;
		$grand_total = $total_ppn + $total_sub;

		//print_r($htmls);die;

		$item = $this->selling_model->get_item_byprin($params);

		$rtext = '<br><p style="font-size: 10px;">PT. ENDRIA ALDA</p> ';

		$ctext = '<br><p style="font-size: 12px"> PURCHASE ORDER</p> ';

		$this->load->library('Pdf');

		$pdf = new Pdf('P', 'mm', 'A4', false, 'UTF-8', false);
		$pdf->SetTitle('Purchase Order');
		$pdf->SetHeaderMargin(25);
		$pdf->setPrintHeader(false);
		$pdf->SetTopMargin(6);
		$pdf->setFooterMargin(5);
		$pdf->SetAutoPageBreak(true);
		$pdf->SetAuthor('Author');
		$pdf->SetDisplayMode('real', 'default');
		$pdf->SetPrintFooter(false);

		$pdf->AddPage();


		$html = '
		
		<style>
		
		  .floatedTable {
            float:left;
        }
        .inlineTable {
            display: inline-block;
        }
		p {
							font-size: 8px;
							
						}
		p {
							font-size: 8px;
							
						}
		</style>
  <table style=" float:left;border: none;" >
						<tr style="border: none;">
							<th  align="left" ><h5>PT.ENDIRA ALDA</h5></th>
							<th  align="RIGHT"></th>
						</tr>
						<tr style="border: none;">
							<th  align="left" ><h4>PURCHASE ORDER</h4></th>
							<th  align="RIGHT"><p>NOMOR, TGL : ' . $po_result[0]['no_po'] . '   ' . $po_result[0]['date_po'] . '</p></th>
						</tr>
					  </table><br><br><br>
					  
					<table>
					  <tr>
						<td>

						  <table cellspacing="0" cellpadding="0" border="0" style="float:right;width:100%.">
							<tr>
							  <td style="width:18%;"><p>Kepada</p></td>
							  <td><p>: ' . $po_result[0]['name_eksternal'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Alamat</p></td>
							  <td><p>: ' . $po_result[0]['eksternal_address'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Att</p></td>
							  <td><p>: ' . $po_result[0]['pic'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Telp</p></td>
							  <td><p>: ' . $po_result[0]['phone_1'] . '</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Fax</p></td>
							  <td><p>: ' . $po_result[0]['fax'] . '</p></td>
							</tr>

						  </table>
						</td>
						<td>
						  <table cellspacing="0" cellpadding="0" border="0" style="float:right;width:100%">
							<tr>
							  <td style="width:18%;"><p>Ship To</p></td>
							  <td><p>:JL.SANGKURIANG NO 38-A<BR>NPWP:01.555.161.7.428.000</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Email</p></td>
							  <td><p>: </p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Telp/Fax</p></td>
							  <td><p>: (022)6626-946</p></td>
							</tr>
							<tr>
							  <td style="width:18%;"><p>Delivery</p></td>
							  <td><p>: </p></td>
							</tr>
						

						  </table>
						</td>
					  </tr>
					</table>
					<br><br><br>

					
					  <table style=" float:left;border-top: 1px solid black;border-bottom: 1px solid black;" >
						<tr style=" border-top: 1px solid black;font-size: 10px;">
							<th  align="left" >Code</th>
							<th  align="RIGHT">Description</th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Quantity<br>Lt/Kg</th>
							<th  align="RIGHT">Unit Price</th>
							<th  align="RIGHT">Disc</th>
							<th  align="RIGHT">Amount</th>
						</tr>
						' . $htmls . '
					  </table>
					  	<br><br><br>
						<br><br><br>
					  
					    <table style=" float:left;border-top: 1px solid black;" >
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Sub Total :</th>
							<th  align="RIGHT">' . number_format($total_sub_all, 0, ',', '.') . '</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Discount :</th>
							<th  align="RIGHT">' . number_format($total_disk, 0, ',', '.') . '</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"> Taxable :</th>
							<th  align="RIGHT">' . number_format($total_sub, 0, ',', '.') . '</th>
						</tr>
						<tr style=" border: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Vat/PPN :</th>
							<th  align="RIGHT">' . number_format($total_ppn, 0, ',', '.') . '</th>
						</tr>
					  </table>
					  
					  <table style=" float:left;border-top: 1px solid black;" >
						<tr style=" border-top: 1px solid black;font-size: 10px;">
							<th  align="left" ></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT"></th>
							<th  align="RIGHT">Total :</th>
							<th  align="RIGHT">' . number_format($grand_total, 0, ',', '.') . '</th>
						</tr>
					  </table>
					<br><br><br><br><br> 
					<br><br><br><br><br> 
					
					 <table style="font-size: 10px;" >
						<tr style="">
							<th  align="CENTER" style="width:25%" ></th>
							<th  align="CENTER" style="width:12%" ></th>
							<th  align="CENTER" style="width:25%"></th>
							<th  align="CENTER" style="width:13%" ></th>
							<th  align="CENTER" style="width:25%">Cimahi, ' . date('m/d/Y') . '</th>
						</tr>
						<tr style="">
							<th  align="CENTER" style="width:25%" >Disetujui Oleh,</th>
							<th  align="CENTER" style="width:12%" ></th>
							<th  align="CENTER" style="width:25%">Diperiksa Oleh,</th>
							<th  align="CENTER" style="width:13%" ></th>
							<th  align="CENTER" style="width:25%">Dipesan Oleh,</th>
						</tr>
					  </table>
					  <br><br><br><br><br> 
					  <table style="font-size: 10px;" >
						<tr style="">
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Ratna S. Iskandar Dinata</span></th>
							<th  align="CENTER" style="width:12%"></th>
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Rienaldy Aryanto</span></th>
							<th  align="CENTER" style="width:13%"></th>
							<th  align="CENTER" style="width:25% ;border-top: 1px solid black;"><span style=" text-decoration-line: overline; ">Rangga Dean</span></th>
						</tr>
					  </table>
';

		//echo $html;die;

		// $html = <<<EOD
		// <h5>PT.ENDIRA ALDA</h5>
		// <table style=" float:left;border: none;" >
		// <tr style="border: none;">
		// <th  align="left" ><h4>PURCHASE ORDER</h4></th>
		// <th  align="RIGHT">NOMOR, TGL : ".."</th>
		// </tr>
		// </table>

		// EOD;

		// Print text using writeHTMLCell()
		$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);



		$pdf->SetPrintFooter(false);

		$pdf->lastPage();

		// $pdf->writeHTMLCell(85, 5, '<p>'.$kategoriby.' : '.$kategori.'<br>Periode : '.$start_date.' / '.$end_date.'</p> ', 1, 'L', 0, 0, '', '', true);
		// $pdf->writeHTMLCell(85, 5, $img, 1, 'R', 0, 1, '', '', true);
		// $pdf->writeHTMLCell(180, 5, '[CENTER] ', 1, 'C', 0, 1, '', '', true);
		// $pdf->writeHTMLCell(85, 5, '[LEFT] ', 1, 'L', 0, 0, '', '', true);
		// $pdf->writeHTMLCell(85, 5, '[RIGHT] ', 1, 'L', 0, 1, '', '', true);

		// $pdf->Output('/var/www/html/tmp_doc/Report_Postbuy.pdf', 'F');

		// $this->output->set_content_type('application/json')->set_output(json_encode('aaa'));

		$pdf->Output('Purchase_order.pdf', 'O');


		// if ( $list ) {			
		// $this->output->set_content_type('application/json')->set_output(json_encode($list));
		// } else {
		// $result = array( 'Value not found!' );
		// $this->output->set_content_type('application/json')->set_output(json_encode($list));
		// }

	}

	public function invoice_new()
	{
		$penjualan_param = array(
			'id' => anti_sql_injection($this->input->post('idpo', TRUE)),
			'id_penjualan' => anti_sql_injection($this->input->post('idpo', TRUE)),
		);

		$result_penjualan = $this->return_model->get_penjualan_byid_inv_sell($penjualan_param);
		$invoice_id = $this->selling_model->get_invoice($penjualan_param);

		$item_param = array(
			'id' => anti_sql_injection($this->input->post('idpo', TRUE)),
			'id_penjualan' => $invoice_id[0]['id_invoice']
		);

		$items = $this->return_model->get_penjualan_detail_update($item_param);
		
		$data = array(
		
				'result_penjualan' => $result_penjualan,
				'invoice_id' => $invoice_id,
				'items' => $items,

		);

		//$this->template->load('maintemplate', 'selling/views/invoice_pdf_html', $data);

		$html = $this->load->view('../modules/selling/views/invoice_pdf_html_dev', compact('result_penjualan', 'items'), true);

		// echo $html; die; // for debug to browser

		$this->load->library('dompdfwrapper');

		$this->dompdfwrapper->load_html($html);
		// $customPaper = array(0,0,793.00,567.00);
		$customPaper = array(0,0, 993.00,567.00); // width ditambah 200px
		$this->dompdfwrapper->setPaper($customPaper, 'potrait');
		// Render the PDF
		$this->dompdfwrapper->render();

		// Output the generated PDF to Browser
		$this->dompdfwrapper->stream('invoice.pdf', array("Attachment" => false));
	}
}
