<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.3.0/paper.css">
    <style>
	@page { size: A5 }
	
        .paper {
            width: 100%;
            page-break-after: always;
        }

        .paper:last-child {
            page-break-after: avoid;
        }

        body {
            font-weight: bold;
        }

        .subtitle {
            font-size: 24px;
        }
		
		
    </style>
	
	<style type="text/css" media="print">
@page {
    size: A4;
}

html,
body {
    width: 210mm;
    height: 148mm;
    /* border-style: solid;
    border-width: 5px; */
}
</style>
	

 
 
<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
          <div class="card-box table-responsive">
    <div class="paper" id="faktur" class="A5" style="margin-left:20px;width:210mm;height:150mm">
        <div class="row" style="height: 180px;">
            <table  style="width: 50%; float: left; border-collapse:collapse;">
                <tr>
                    <td colspan="3"><strong><span class="subtitle">PT. ENDIRA ALDA</span></strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td>JL. Sangkuriang No.38-A</td>
                </tr>
                <tr>
                    <td>NPWP</td>
                    <td>:</td>
                    <td>01.555.161.7.428.000</td>
                </tr>
            </table>

            <table border="0.0px" style="width: 50%; float: left; border-collapse:collapse;">
                <tr>
                    <td colspan="3"><strong><span class="subtitle">FAKTUR</span></strong></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><strong>No.Faktur</strong></td>
                    <td>:</td>
                    <td><strong><?= $result_penjualan[0]['no_penjualan'] ?? ''; ?></strong></td>
                </tr>
                <tr>
                    <td><strong>Tanggal</strong></td>
                    <td>:</td>
                    <td><strong><?= $result_penjualan[0]['date_penjualan'] ?? ''; ?></strong></td>
                </tr>
                <tr>
                    <td>Alamat pengiriman</td>
                    <td>:</td>
                    <td><?= $result_penjualan[0]['cust_address'] ?? ''; ?></td>
                </tr>
                <tr>
                    <td>Phone</td>
                    <td>:</td>
                    <td><?= $result_penjualan[0]['phone'] ?? ''; ?></td>
                </tr>
            </table>
        </div>
        <div class="row" style="margin-bottom: 24px;">
            <span><strong><span class="subtitle">Pembeli</span></strong></span><br><br>
            <span><?= $result_penjualan[0]['cust_name'] ?? ''; ?></span><br>
            <span><?= $result_penjualan[0]['cust_address'] ?? ''; ?></span>
        </div>
        <div class="row">
            <table style="width: 100%; border-collapse:collapse;">
                <thead>
                    <tr>
                        <td style="border: 1px solid black; padding-left: 4px; width: 5px;">No.</td>
                        <td style="border: 1px solid black; padding-left: 4px;">Kode</td>
                        <td style="border: 1px solid black; padding-left: 4px;">Nama Barang</td>
                        <td style="border: 1px solid black; padding-left: 4px;">Qty</td>
                        <td style="border: 1px solid black; padding-left: 4px;">Jumlah Box</td>
                        <td style="border: 1px solid black; text-align: left; padding-left: 4px;">Harga</td>
                        <td colspan="2" style="border: 1px solid black; text-align: left; padding-left: 4px;">Jumlah Rp</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $rowNumber = 1;
                    $total_retur = 0;
                    $dpp = 0;
                    foreach ($items as $item) : ?>
                        <tr>
                            <td style="border: 1px solid black; padding-left: 4px; width: 5px;"><?= $rowNumber; ?></td>
                            <td style="border: 1px solid black; padding-left: 4px;"><?= $item['stock_code'] ?? ''; ?></td>
                            <td style="border: 1px solid black; padding-left: 4px;"><?= implode(' ', [$item['stock_name'], $item['base_qty'], $item['uom_symbol']]) ?></td>
                            <td style="border: 1px solid black; padding-left: 4px; text-align: center;"><?= $item['qty_box_retur'] ?? ''; ?></td>
                            <td style="border: 1px solid black; padding-left: 4px; text-align: center;"><?= 'Box @ ' . ($item['box_ammount'] ?? ''); ?></td>
                            <td style="border: 1px solid black; text-align: right; padding-right: 4px;"><?= indonesia_currency_format($item['unit_price']); ?></td>
                            <td colspan="2" style="border: 1px solid black; text-align: right; padding-right: 4px;"><?= indonesia_currency_format($item['price']); ?></td>
                        </tr>
                    <?php
                        $rowNumber++;
                        $total_retur += $item['price'];
                        $dpp += (($item['price'] / (10 + ($item['pajak'] / 10))) * 10);
                    endforeach; ?>
                    <tr>
                        <td></td>
                        <td style="border:none; text-align:center;">Mengetahui,</td>
                        <td colspan="3" style="border:none;"></td>
                        <td style="border:none;">Total</td>
                        <td style="border: 1px solid black; padding-left: 4px; width: 10px;">Rp</td>
                        <td style="border: 1px solid black; padding-right: 4px; text-align: right;"><?= indonesia_currency_format($total_retur); ?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="4" style="border:none;"></td>
                        <td style="border:none;">DPP</td>
                        <td style="border: 1px solid black; padding-left: 4px; width: 10px;">Rp</td>
                        <td style="border: 1px solid black; padding-right: 4px; text-align: right;"><?= indonesia_currency_format($dpp); ?></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="border:none; text-align:center;">________</td>
                        <td colspan="3" style="border:none;"></td>
                        <td style="border:none;">PPN</td>
                        <td style="border: 1px solid black; padding-left: 4px; width: 10px;">Rp</td>
                        <td style="border: 1px solid black; padding-right: 4px; text-align: right;"><?= indonesia_currency_format($total_retur - $dpp); ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div><!-- paper -->
	<div style='page-break-before:always'></div>

	<input type='button' id='btn' value='Print' onclick="PrintElem('.print_div');">

    
    

        </div><!-- end col -->
      </div>
    </div>
  </div>
  <span id="laod"></span>
</div>
</div>


<script>

function printDiv() 
{

  // var divToPrint=document.getElementById('faktur');

  // var newWin=window.open('','Print-Window');

  // newWin.document.open();

  // newWin.document.write('<html><body style="width:210mm;height:150mm" onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  // newWin.document.close();

  // setTimeout(function(){newWin.close();},10);
  
  
// var tagid = "faktur";
   // var hashid = "#"+ tagid;
    // //var hashid = "#faktur";
            // var tagname =  $(hashid).prop("tagName").toLowerCase() ;
            // var attributes = ""; 
            // var attrs = document.getElementById(tagid).attributes;
              // $.each(attrs,function(i,elem){
                // attributes +=  " "+  elem.name+" ='"+elem.value+"' " ;
              // })
            // var divToPrint= $(hashid).html() ;
            // var head = "<html><head>"+ $("head").html() + "</head>" ;
            // var allcontent = head + "<body  onload='window.print()' >"+ "<" + tagname + attributes + ">" +  divToPrint + "</" + tagname + ">" +  "</body></html>"  ;
            // var newWin=window.open('','Print-Window');
            // newWin.document.open();
            // newWin.document.write(allcontent);
            // newWin.document.close();
            // setTimeout(function(){newWin.close();},10);
			
			
			 var mywindow = window.open('', 'my div', 'height=400,width=600');
    mywindow.document.write('<html><head><title></title>');
    mywindow.document.write('<link rel="stylesheet" href="http://www.test.com/style.css" type="text/css" />');  
    mywindow.document.write('<style type="text/css">.test { color:red; } </style></head><body>');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');
    mywindow.document.close();
    mywindow.print();    
}


function PrintElem(elem) {
	
	console.log(elem);
	
    Popup(jQuery("#faktur").html());
}

function Popup(data) {
    var mywindow = window.open('', 'my div', 'height=400,width=600');
    mywindow.document.write('<html><head><title></title>');
    mywindow.document.write('<link rel="stylesheet" href="http://www.test.com/style.css" type="text/css" />');  
    mywindow.document.write('<style type="text/css">.test { color:red; } </style></head><body>');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');
    mywindow.document.close();
    mywindow.print();           

		//setTimeout(function(){mywindow.close();},10);	
}

</script>