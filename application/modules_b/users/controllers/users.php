<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('users/users_model');
		$this->load->library('log_activity');
		$this->load->library('priv');
		
		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong');
		$this->form_validation->set_message('min_length', '%s Minimal 4 Karakter');
		$this->form_validation->set_message('max_length', '%s Maksimal 10 Karakter');
	}

	/**
	 * anti sql injection
	 * @return string
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	  * This function is redirect to index distributor page
	  * @return Void
	  */
	public function index() {
		$priv = $this->priv->get_priv();

		$data = array(
			'priv' => $priv
		);
		
		$this->template->load('maintemplate', 'users/views/index', $data);
	}

	/**
	  * This function is used for showing distributor list
	  * @return Array
	  */
	function lists() {
		
		 if( !empty($_GET['sess_user_id']) ) {
			  $sess_user_id = $_GET['sess_user_id'];
		  } else {
			  $sess_user_id = NULL;
		  }
		  
		   if( !empty($_GET['sess_token']) ) {
			  $sess_token = $_GET['sess_token'];
		  } else {
			  $sess_token = NULL;
		  }
		  
		    if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		  if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		  if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 				
		  $order_fields = array('kode_eksternal'); // , 'COST'
		  $order = $this->input->get_post('order');
		  if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'desc';}; 
		  if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 	
		  
		    $params['limit'] 		= (int) $length;
			$params['offset'] 		= (int) $start;
			$params['order_column'] = $order_fields[$order_column];
			$params['order_dir'] 	= $order_dir;
			$params['sess_user_id'] = $sess_user_id;
			$params['sess_token'] 	= $sess_token;
			$params['searchtxt'] 	= $_GET['search']['value'];
		  $priv = $this->priv->get_priv();
		 // print_r($params);die;
		  
			$list = $this->users_model->list_users($params); 
			//print_r($list['data']);die;
			  $data = array();	
				   foreach ( $list['data'] as $k => $v ) {

						if($v['status_akses'] == 0){
							
							$sts = '<button type="button" class="btn btn-custon-rounded-two btn-danger"  > Tidak Aktif </button>';
							
						}else{
							$sts = '<button type="button" class="btn btn-custon-rounded-two btn-primary"  > Aktif </button>';
							//$sts = '<span class="color:green"><p >Disetujui</p></span>';
						}
						
						$action = '
						<div class="btn-group" style="display:'.$priv['update'].'"><button type="button" class="btn btn-custon-rounded-two btn-warning" onClick="updateuser('. $v['id_user'].')" > <i class="fa fa-edit"></i> </button></div>
						<div class="btn-group" style="display:'.$priv['update'].'"><button type="button" class="btn btn-custon-rounded-two btn-danger" onClick="deleteuser('. $v['id_user'].')" > <i class="fa fa-trash"></i></button></div>';
						   array_push($data, 
							  array(
								  number_format($v['Rangking'],0,',','.'),
								 '<img src="' . base_url() .'uploads/profile/'. $v['image'] . '" alt="picture" class="img-circle profile_img" style="width: 40px; height:40px">',
								  $v['nama'],
								  $v['username'],
								  $v['group'],
								  $v['region'],
								  $sts,
								  $action
							  )
							);
							//$idx++;
				   }
		   
			 $result["data"] = $data;
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		//$result["data"] = $list['data'];
	  
		echo json_encode($result);
	  
				//$this->json_result($result);
		  
	}

	/**
	  * This function is redirect to add distributor page
	  * @return Void
	  */
	public function add() {
		$region = $this->users_model->get_wilayah();
		$role = $this->users_model->get_role();
		
		//print_r($region);die;

		$data = array(
			'wilayah' => $region,
			'role' => $role
		);

		$this->template->load('maintemplate', 'users/views/addUsers',$data);
		//$this->load->view('addPrinciple',$data);
	}	


	public function updateusers() {
		
		//$region = $this->users_model->get_userdetail();
		$data = array(
			'iduser' => $this->Anti_sql_injection($this->input->post('iduser', TRUE)),
		);
		
		//print_r($data);die;
		
		$user_result = $this->users_model->get_userdetail($data);
		$region = $this->users_model->get_wilayah();
		$role = $this->users_model->get_role();
		
		//print_r($user_result);die;
		
		// $result = $this->distributor_model->location();

		$data = array(
			'user_result' => $user_result[0],
			'wilayah' => $region,
			'role' => $role
		);

		$this->template->load('maintemplate', 'users/views/updateUsers',$data);
		//$this->load->view('addPrinciple',$data);
	}		
	
	public function approvedcustomer() {
		
		$region = $this->users_model->get_wilayah();
		$data = array(
			'id_t_cust' => $this->Anti_sql_injection($this->input->post('idcustomer', TRUE)),
		);
		
		//print_r($data);die;
		
		$customer_result = $this->users_model->get_customer($data);
		
		//print_r($customer_result);die;
		
		// $result = $this->distributor_model->location();

		$data = array(
			'customer_result' => $customer_result[0],
			'wilayah' => $region
		);

		$this->template->load('maintemplate', 'users/views/KonfirmasiCustomer',$data);
		//$this->load->view('addPrinciple',$data);
	}	
	
	
	public function add_users(){
		
		$sss = $this->Anti_sql_injection($this->input->post('username', TRUE));
		
		//print_r($sss);die;
		
		$this->form_validation->set_rules('alamat', 'Alamat User', 'required');
		$this->form_validation->set_rules('role', 'Nama User', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Nama User', 'required');
		
		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			
			//echo $pesan;die;
			
			//$msg = strip_tags(str_replace("\r", '', $pesan));
			$msg = $pesan;

			$result = array('success' => false, 'message' => $msg);
		}else {
			
			
			$upload_error = NULL;
			$picture = NULL;

			if ($_FILES['picture']['name']) {
				$this->load->library('upload');
				$config = array(
					'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/profile",
					'upload_url' => base_url() . "uploads/profile",
					'encrypt_name' => TRUE,
					'overwrite' => FALSE,
					'allowed_types' => 'jpg|jpeg|png',
					'max_size' => '10000'
				);

				$this->upload->initialize($config);

				if ($this->upload->do_upload("picture")) { // Success
					// General result data
					$result = $this->upload->data();

					// Load resize library
					$this->load->library('image_lib');

					// Resizing parameters large
					$resize = array
						(
						'source_image' => $result['full_path'],
						'new_image' => $result['full_path'],
						'maintain_ratio' => TRUE,
						'width' => 300,
						'height' => 300
					);

					// Do resize
					$this->image_lib->initialize($resize);
					$this->image_lib->resize();
					$this->image_lib->clear();

					// Add our stuff
					$picture = $result['file_name'];
				}else {
					$pesan = $this->upload->display_errors();
					$upload_error = strip_tags(str_replace("\n", '', $pesan));

					$result = array(
						'success' => false,
						'message' => $upload_error
					);
				}
			}
			
			
			$password_generated = substr(md5(strtolower($this->Anti_sql_injection($this->input->post('password', TRUE)))), 0, 8);
			$password_hash = password_hash($this->Anti_sql_injection($this->input->post('password', TRUE)), PASSWORD_BCRYPT);
				
			$data = array(
				'nama' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
				'alamat' => $this->Anti_sql_injection($this->input->post('alamat', TRUE)),
				'nokontak' => $this->Anti_sql_injection($this->input->post('telp', TRUE)),
				'lokasi' => $this->Anti_sql_injection($this->input->post('region', TRUE)),
				'email' => $this->Anti_sql_injection($this->input->post('email', TRUE)),
				'role' => $this->Anti_sql_injection($this->input->post('role', TRUE)),
				'username' => $this->Anti_sql_injection($this->input->post('username', TRUE)),
				'password' => $password_hash,
				'image' => $picture
				
			);
		
			//print_r($data);die;
		
			$check_kode_result = $this->users_model->check_username($data);
			
			
			//print_r(count($check_kode_result));die;
			
			if(count($check_kode_result) > 0 ){
				$msg = "Username Sudah Ada ";
				$result = array('success' => false, 'message' => $msg);
			}else{
		
				$add_prin_result = $this->users_model->add_users($data);
				
				//print_r($add_prin_result);die;
				
				if ($add_prin_result['result'] > 0) {
					
					$data2 = array(
						'id_user' => $add_prin_result['lastid'],
						'id_role' => $this->Anti_sql_injection($this->input->post('role', TRUE)),
						'create_at' => date('Y-m-d'),
						'create_by' => $this->session->userdata['logged_in']['user_id'],
						'username' => $this->Anti_sql_injection($this->input->post('username', TRUE)),
						'password' => $password_hash
						
					);
					
					$add_prin_result = $this->users_model->add_group($data2);
					
					$msg = 'Berhasil Menambah data User';

					$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ');
					$result = array('success' => true, 'message' => $msg);
					
				}else{
					$msg = 'Gagal Menambah data User';

					$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ');
					$result = array('success' => false, 'message' => $msg);
					
				}
			}
			
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);
		
		//echo $kode;die;
		
	}	
	
	public function update_user(){
		
		$sss = $this->Anti_sql_injection($this->input->post('username', TRUE));
		//print_r($sss);die;
		
		//$this->form_validation->set_rules('kode_distributor', 'Kode Distributor', 'trim|required|min_length[4]|max_length[100]');
		$this->form_validation->set_rules('alamat', 'Alamat User', 'required');
		$this->form_validation->set_rules('role', 'Nama User', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		
		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			
			//echo $pesan;die;
			
			//$msg = strip_tags(str_replace("\r", '', $pesan));
			$msg = $pesan;

			$result = array('success' => false, 'message' => $msg);
		}else {
			
			
			$upload_error = NULL;
			$picture = NULL;

			if ($_FILES['picture']['name']) {
				$this->load->library('upload');
				$config = array(
					'upload_path' => dirname($_SERVER["SCRIPT_FILENAME"]) . "/uploads/profile",
					'upload_url' => base_url() . "uploads/profile",
					'encrypt_name' => TRUE,
					'overwrite' => FALSE,
					'allowed_types' => 'jpg|jpeg|png',
					'max_size' => '10000'
				);

				$this->upload->initialize($config);

				if ($this->upload->do_upload("picture")) { // Success
					// General result data
					$result = $this->upload->data();

					// Load resize library
					$this->load->library('image_lib');

					// Resizing parameters large
					$resize = array
						(
						'source_image' => $result['full_path'],
						'new_image' => $result['full_path'],
						'maintain_ratio' => TRUE,
						'width' => 300,
						'height' => 300
					);

					// Do resize
					$this->image_lib->initialize($resize);
					$this->image_lib->resize();
					$this->image_lib->clear();

					// Add our stuff
					$picture = $result['file_name'];
				}else {
					$pesan = $this->upload->display_errors();
					$upload_error = strip_tags(str_replace("\n", '', $pesan));

					$result = array(
						'success' => false,
						'message' => $upload_error
					);
				}
			}
			
			
			$password_generated = substr(md5(strtolower($this->Anti_sql_injection($this->input->post('password', TRUE)))), 0, 8);
			
			$data = array(
				'id' => $this->Anti_sql_injection($this->input->post('id', TRUE)),
				'nama' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
				'alamat' => $this->Anti_sql_injection($this->input->post('alamat', TRUE)),
				'nokontak' => $this->Anti_sql_injection($this->input->post('telp', TRUE)),
				'lokasi' => $this->Anti_sql_injection($this->input->post('region', TRUE)),
				'email' => $this->Anti_sql_injection($this->input->post('email', TRUE)),
				'role' => $this->Anti_sql_injection($this->input->post('role', TRUE)),
				'username' => $this->Anti_sql_injection($this->input->post('username', TRUE)),
				// 'password' => $password_hash,
				'image' => $picture
			);
			
			
			if (!empty($password = $this->Anti_sql_injection($this->input->post('password', TRUE)))) {
				$password_hash = password_hash($password, PASSWORD_BCRYPT);
				$data['password'] = $password_hash;
			}

		
			//print_r($data);die;
		
			$check_kode_result = $this->users_model->check_username_update($data);
			
			
			//print_r(count($check_kode_result));die;
			
			if(count($check_kode_result) > 0 ){
				$msg = "Username Sudah Ada ";
				$result = array('success' => false, 'message' => $msg);
			}else{
		
				$add_prin_result = $this->users_model->update_users($data);
				
				//print_r($add_prin_result);die;
				
				if ($add_prin_result['result'] > 0) {
					
					$data2 = array(
						'id_user' => $this->Anti_sql_injection($this->input->post('id', TRUE)),
						'id_role' => $this->Anti_sql_injection($this->input->post('role', TRUE)),
						'updated_at' => date('Y-m-d H:i:s'),
						'username' => $this->Anti_sql_injection($this->input->post('username', TRUE)),
						// 'password' => $password_hash
					);


					if (!empty($password)) {
						$data2['password'] = $password_hash;
					}
					
					$add_prin_result = $this->users_model->update_group($data2);
					
					$msg = 'Berhasil Merubah data User';

					$this->log_activity->insert_activity('insert', $msg. ' dengan id User ');
					$result = array('success' => true, 'message' => $msg);
					
				}else{
					$msg = 'Gagal Merubah data User';

					$this->log_activity->insert_activity('insert', $msg. ' dengan id User ');
					$result = array('success' => false, 'message' => $msg);
					
				}
			}
			
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);
		
		//echo $kode;die;
		
	}
	
	public function konfirmasi_customer(){
		//$this->form_validation->set_rules('kode_distributor', 'Kode Distributor', 'trim|required|min_length[4]|max_length[100]');
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);

		
		
		//print_r($asas);die;

		$result_dist 		= $this->users_model->update_status($params);
			if ($result_dist['result'] > 0) {
				
				
				$datas2 =  array(
						'id_cust' => $params['id_t_cust']
				);
				$asas = $this->users_model->get_limit_history($datas2);
				
				//print_r($asas);die;
				
				$datas3 =  array(
						'id_cust' => $params['id_t_cust'],
						'id_hl' => $asas[0]['id_hl'],
						'approval_date' => date('Y-m-d')
				);
				
				//print_r($datas3);die;
				$this->users_model->update_limit_history($datas3);
				
				$msg = 'Berhasil Menambah data Principal';

				$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ');
				$result = array('success' => true, 'message' => $msg);
				
			}else{
				$msg = 'Gagal Menambah data Principal';

				$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ');
				$result = array('success' => false, 'message' => $msg);
				
			}
		//print_r($data);
		
		//echo $kode;die;
		
	}

	public function delete_user() {
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);

		//print_r($params);die;

		$result_dist 		= $this->users_model->delete_user($params['id']);
		$result_dists		= $this->users_model->delete_user_group($params['id']);

		$msg = 'Berhasil menghapus data User.';
		$result = array('success' => true, 'message' => $msg);
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
}