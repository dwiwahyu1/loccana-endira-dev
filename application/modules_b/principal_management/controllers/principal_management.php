<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Principal_management extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('principal_management/principal_management_model');
		$this->load->library('log_activity');
		$this->load->library('priv');

		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong');
		$this->form_validation->set_message('min_length', '%s Minimal 4 Karakter');
		$this->form_validation->set_message('max_length', '%s Maksimal 10 Karakter');
	}

	/**
	 * This function is redirect to index distributor page
	 * @return Void
	 */
	public function index()
	{
		$priv = $this->priv->get_priv();
		$data = array(
			'priv' => $priv
		);
		$this->template->load('maintemplate', 'principal_management/views/index', $data);
	}

	/**
	 * This function is used for showing distributor list
	 * @return Array
	 */
	function lists()
	{
		$sess_user_id = !empty($_GET['sess_user_id']) ? $_GET['sess_user_id'] : NULL;
		$sess_token = !empty($_GET['sess_token']) ? $_GET['sess_token'] : NULL;
		$draw = $this->input->get_post('draw') != FALSE ? $this->input->get_post('draw') : 1;
		$length = $this->input->get_post('length') != FALSE ? $this->input->get_post('length') : 10;
		$start = $this->input->get_post('start') != FALSE ? $this->input->get_post('start') : 0;

		$order_fields = array('kode_eksternal'); // , 'COST'
		$order = $this->input->get_post('order');
		$order_dir = !empty($order[0]['dir']) ? $order[0]['dir'] : 'desc';
		$order_column = !empty($order[0]['column']) ? $order[0]['column'] : 0;

		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['sess_user_id'] = $sess_user_id;
		$params['sess_token'] 	= $sess_token;
		$params['searchtxt'] 	= $_GET['search']['value'];

		$priv = $this->priv->get_priv();
		$list = $this->principal_management_model->list_principal($params);

		$data = array();
		foreach ($list['data'] as $row) {

			$action = '
				<div class="btn-group" style="display:' . $priv['update'] . '"><button type="button" class="btn btn-custon-rounded-two btn-warning" data-toggle="tooltip" data-placement="top" title="Edit"  onClick="updateprincipal(' . $row['id'] . ')" > <i class="fa fa-edit"></i> </button></div>
				<div class="btn-group" style="display:' . $priv['delete'] . '"><button type="button" class="btn btn-custon-rounded-two btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"  onClick="deletedistributor(' . $row['id'] . ')" > <i class="fa fa-trash"></i> </button></div>
			';

			array_push($data, [
				$row['kode_eksternal'],
				$row['name_eksternal'],
				$row['eksternal_address'],
				$row['phone_1'],
				$row['fax'],
				$action
			]);
		}

		$result["data"] = $data;
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;

		echo json_encode($result);
	}

	public function add()
	{
		$this->template->load('maintemplate', 'principal_management/views/addPrinciple');
	}


	public function updateprincipal()
	{
		$data = array(
			'id' => anti_sql_injection($this->input->post('idprincipal', TRUE)),
		);

		//print_r($data);

		$principal_result = $this->principal_management_model->get_principal($data);

		//print_r($principal_result);die;

		// $result = $this->distributor_model->location();

		$data = array(
			'principal_data' => $principal_result[0]
		);

		$this->template->load('maintemplate', 'principal_management/views/updatePrinciple', $data);
		//$this->load->view('addPrinciple',$data);
	}


	public function add_principal()
	{
		$this->load_form_validations();

		if ($this->form_validation->run() == FALSE) {
			$msg = validation_errors();
			$result = array('success' => false, 'message' => $msg);
		} else {

			$data = array(
				'kode' => anti_sql_injection($this->input->post('kode', TRUE)),
				'alamat' => anti_sql_injection($this->input->post('alamat', TRUE)),
				'name' => anti_sql_injection($this->input->post('name', TRUE)),
				'telp' => anti_sql_injection($this->input->post('telp', TRUE)),
				'email' => anti_sql_injection($this->input->post('email', TRUE)),
				'fax' => anti_sql_injection($this->input->post('fax', TRUE)),
				'bank1' => anti_sql_injection($this->input->post('bank1', TRUE)),
				'norek1' => anti_sql_injection($this->input->post('norek1', TRUE)),
				'bank2' => anti_sql_injection($this->input->post('bank2', TRUE)),
				'norek2' => anti_sql_injection($this->input->post('norek2', TRUE)),
				'bank3' => anti_sql_injection($this->input->post('bank3', TRUE)),
				'norek3' => anti_sql_injection($this->input->post('norek3', TRUE)),
				'type_eksternal' => 1
			);

			$check_kode_result = $this->principal_management_model->check_principal($data);

			if (count($check_kode_result) > 0) {
				$msg = "Kode Principal Sudah Ada ";
				$result = array('success' => false, 'message' => $msg);
			} else {

				$add_prin_result = $this->principal_management_model->add_principal($data);

				if ($add_prin_result['result'] > 0) {
					$msg = 'Berhasil Menambah data Principal';
					$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
					$result = array('success' => true, 'message' => $msg);
				} else {
					$msg = 'Gagal Menambah data Principal';
					$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
					$result = array('success' => false, 'message' => $msg);
				}
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function update_principal()
	{
		$this->load_form_validations();

		if ($this->form_validation->run() == FALSE) {
			$msg = validation_errors();
			$result = array('success' => false, 'message' => $msg);
		} else {
			$data = array(
				'id' => anti_sql_injection($this->input->post('id', TRUE)),
				'kode' => anti_sql_injection($this->input->post('kode', TRUE)),
				'alamat' => anti_sql_injection($this->input->post('alamat', TRUE)),
				'name' => anti_sql_injection($this->input->post('name', TRUE)),
				'telp' => anti_sql_injection($this->input->post('telp', TRUE)),
				'fax' => anti_sql_injection($this->input->post('fax', TRUE)),
				'email' => anti_sql_injection($this->input->post('email', TRUE)),
				'bank1' => anti_sql_injection($this->input->post('bank1', TRUE)),
				'norek1' => anti_sql_injection($this->input->post('norek1', TRUE)),
				'bank2' => anti_sql_injection($this->input->post('bank2', TRUE)),
				'norek2' => anti_sql_injection($this->input->post('norek2', TRUE)),
				'bank3' => anti_sql_injection($this->input->post('bank3', TRUE)),
				'norek3' => anti_sql_injection($this->input->post('norek3', TRUE)),
				'type_eksternal' => 1
			);

			$add_prin_result = $this->principal_management_model->update_principal($data);
			if ($add_prin_result['result'] > 0) {

				$msg = 'Berhasil Menambah data Principal';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => true, 'message' => $msg);
			} else {
				$msg = 'Gagal Menambah data Principal';

				$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
				$result = array('success' => false, 'message' => $msg);
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function delete_principal()
	{
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		$result_dist 		= $this->principal_management_model->delete_principal($params['id']);

		$msg = 'Berhasil menghapus data Principal.';
		$result = array('success' => true, 'message' => $msg);
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	private function load_form_validations()
	{
		$this->form_validation->set_rules('kode', 'Kode Principal', 'required|min_length[4]|max_length[10]');
		$this->form_validation->set_rules('alamat', 'Alamat Principal', 'required');
		$this->form_validation->set_rules('name', 'Nama Principal', 'required');
	}
}
