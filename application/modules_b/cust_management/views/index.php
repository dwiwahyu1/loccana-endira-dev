<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto
  }

  .scroll-overflow {
    min-height: 650px
  }

  #modal-distributor::-webkit-scrollbar-track {
    box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
          <div class="card-box table-responsive">
            <h4 class="header-title m-t-0 m-b-30">List Customer</h4>
            <table id="datatable_pricipal" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <!-- <th width="5%">No</th> -->
                  <th>Kode Customer</th>
                  <th>Nama Customer</th>
                  <th>Wilayah</th>
                  <th>NPWP</th>
                  <th>Nama NPWP</th>
                  <th>Alamat NPWP</th>
                  <th>Alamat Toko</th>
                  <th>Credit Limit</th>
                  <th>Status Limit</th>
                  <th width="13%">Action</th>
                </tr>
              </thead>
              <tbody></tbody>
            </table>
          </div>
        </div><!-- end col -->
      </div>
    </div>
  </div>
  <span id="laod"></span>
</div>


<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog">
    <div class="modal-content p-0 b-0">
      <div class="panel panel-color panel-primary panel-filled">
        <div class="panel-heading">
          <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 class="panel-title"></h3>
        </div>
        <div class="panel-body force-overflow" id="modal-distributor">
          <div class="scroll-overflow">
            <p></p>
          </div>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
  function deletecustomer(id) {
    swal({
      title: 'Yakin akan Menghapus ?',
      text: 'data tidak dapat dikembalikan bila sudah dihapus !',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak'
    }).then(function() {
      var datapost = {
        "id": id
      };

      $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'cust_management/delete_customer'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
          $('.panel-heading button').trigger('click');
          listdist();
          swal({
            title: 'Success!',
            text: response.message,
            type: 'success',
            showCancelButton: false,
            confirmButtonText: 'Ok'
          }).then(function() {})
        }
      });
    });
  }

  function listdist() {
    var user_id = '0001';
    var token = '093940349';


    $('#datatable_pricipal').DataTable({
      //"dom": 'rtip',
      "ordering": false,
      "bFilter": false,
      "aaSorting": [],
      "bLengthChange": true,
      'iDisplayLength': 10,
      "sPaginationType": "simple_numbers",
      "Info": false,
      "processing": true,
      "serverSide": true,
      "destroy": true,
      "ajax": "<?php echo base_url() . 'cust_management/lists' ?>" + "/?sess_user_id=" + user_id + "&sess_token=" + token,
      "searching": true,
      "language": {
        "decimal": ",",
        "thousands": "."
      },
      "dom": 'l<"toolbar">frtip',
      "initComplete": function() {
        $("div.toolbar").prepend('<div class="btn-group pull-left"><a href="<?php echo base_url() . 'cust_management/add'; ?>" type="button" class="btn btn-custon-rounded-two btn-primary" style="display: <?php echo $priv['insert'] ?>;" data-toggle="tooltip" title="Insert" > <i class="fa fa-plus"></i>  </a></div>');
      }
    });
  }

  $(document).ready(function() {
    listdist();
  });

  function updatecustomer(id) {


    var url = '<?php echo base_url(); ?>cust_management/updatecustomer';

    $("#laod").append(' <img id="loading" src="<?php echo base_url(); ?>assets/urate-frontend-master/assets/images/icon_loader.gif">');
    var form = $("<form action='" + url + "' method='post'>" +
      "<input type='hidden' name='idcustomer' value='" + id + "' />" +
      "</form>");
    $('body').append(form);
    form.submit();

    //window.location.href = "<?php echo base_url() . 'principal_management/updateprincipal/'; ?>"+id;

  }

  function approved(id) {


    var url = '<?php echo base_url(); ?>cust_management/approvedcustomer';

    $("#laod").append(' <img id="loading" src="<?php echo base_url(); ?>assets/urate-frontend-master/assets/images/icon_loader.gif">');
    var form = $("<form action='" + url + "' method='post'>" +
      "<input type='hidden' name='idcustomer' value='" + id + "' />" +
      "</form>");
    $('body').append(form);
    form.submit();

    //window.location.href = "<?php echo base_url() . 'principal_management/updateprincipal/'; ?>"+id;

  }
</script>