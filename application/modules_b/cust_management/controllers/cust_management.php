<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cust_management extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('cust_management/cust_management_model');
		$this->load->library('log_activity');
		$this->load->library('priv');

		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong');
		$this->form_validation->set_message('min_length', '%s Minimal 4 Karakter');
		$this->form_validation->set_message('max_length', '%s Maksimal 10 Karakter');
	}

	/**
	 * anti sql injection
	 * @return string
	 */
	public function Anti_sql_injection($string)
	{
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	 * This function is redirect to index distributor page
	 * @return Void
	 */
	public function index()
	{

		$priv = $this->priv->get_priv();

		$data = array(
			'priv' => $priv
		);

		$this->template->load('maintemplate', 'cust_management/views/index', $data);
	}

	/**
	 * This function is used for showing distributor list
	 * @return Array
	 */
	function lists()
	{
		$sess_user_id = !empty($_GET['sess_user_id']) ? $_GET['sess_user_id'] : NULL;
		$sess_token = !empty($_GET['sess_token']) ? $_GET['sess_token'] : NULL;
		$draw = ($this->input->get_post('draw') != FALSE) ? $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;

		$order_fields = array('kode_eksternal'); // , 'COST'
		$order = $this->input->get_post('order');

		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 0;

		$params['limit'] 		= (int) $length;
		$params['offset'] 		= (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] 	= $order_dir;
		$params['sess_user_id'] = $sess_user_id;
		$params['sess_token'] 	= $sess_token;
		$params['searchtxt'] 	= $_GET['search']['value'];

		// print_r($params);die;
		$priv = $this->priv->get_priv();

		$list = $this->cust_management_model->list_principal($params);
		//print_r($list['data']);die;
		$data = array();
		foreach ($list['data'] as $row) {

			if ((int) $row['status_credit'] === 0) {
				$sts = '<div class="btn-group" style="display:' . $priv['update'] . '"><button type="button" class="btn btn-custon-rounded-two btn-danger" onClick="approved(' . $row['id_t_cust'] . ')" > Konfirmasi </button></div>';
			} else if ((int) $row['status_credit'] === 1) {
				$sts = '<div class="btn-group"><button type="button" class="btn btn-custon-rounded-two btn-primary" disabled="disabled"> Disetujui </button></div>';
			} else if ((int) $row['status_credit'] === 2) {
				$sts = '<div class="btn-group"><button type="button" class="btn btn-custon-rounded-two btn-warning" disabled="disabled"> Tidak disetujui </button></div>';
			}

			$action = '
				<div class="btn-group" style="display:' . $priv['update'] . '"><button type="button" class="btn btn-custon-rounded-two btn-warning" data-toggle="tooltip" data-placement="top" title="Edit"  onClick="updatecustomer(' . $row['id_t_cust'] . ')" > <i class="fa fa-edit"></i> </button></div>
				<div class="btn-group" style="display:' . $priv['delete'] . '"><button type="button" class="btn btn-custon-rounded-two btn-danger" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deletecustomer(' . $row['id_t_cust'] . ')" > <i class="fa fa-trash"></i> </button></div>
			';

			array_push($data, [
				// number_format($row['Rangking'], 0, ',', '.'),
				$row['code_cust'],
				$row['cust_name'],
				$row['wilayah'],
				$row['no_npwp'],
				$row['npwp_name'],
				$row['npwp_address'],
				$row['cust_address'],
				number_format($row['credit_limit'], 0, ',', '.'),
				$sts,
				$action,
			]);
		}

		$result["data"] = $data;
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		//$result["data"] = $list['data'];

		echo json_encode($result);

		//$this->json_result($result);

	}

	/**
	 * This function is redirect to add distributor page
	 * @return Void
	 */
	public function add()
	{
		$region = $this->cust_management_model->get_wilayah();

		//print_r($region);die;

		$data = array(
			'wilayah' => $region
		);

		$this->template->load('maintemplate', 'cust_management/views/addCustomer', $data);
		//$this->load->view('addPrinciple',$data);
	}


	public function updatecustomer()
	{

		$region = $this->cust_management_model->get_wilayah();
		$data = array(
			'id_t_cust' => anti_sql_injection($this->input->post('idcustomer', TRUE)),
		);

		//print_r($data);die;

		$customer_result = $this->cust_management_model->get_customer($data);

		//print_r($customer_result);die;

		// $result = $this->distributor_model->location();

		$data = array(
			'customer_result' => $customer_result[0],
			'wilayah' => $region
		);

		$this->template->load('maintemplate', 'cust_management/views/updateCustomer', $data);
		//$this->load->view('addPrinciple',$data);
	}

	public function approvedcustomer()
	{

		$region = $this->cust_management_model->get_wilayah();
		$data = array(
			'id_t_cust' => anti_sql_injection($this->input->post('idcustomer', TRUE)),
		);

		//print_r($data);die;

		$customer_result = $this->cust_management_model->get_customer($data);

		//print_r($customer_result);die;

		// $result = $this->distributor_model->location();

		$data = array(
			'customer_result' => $customer_result[0],
			'wilayah' => $region
		);

		$this->template->load('maintemplate', 'cust_management/views/KonfirmasiCustomer', $data);
		//$this->load->view('addPrinciple',$data);
	}


	public function add_customer()
	{

		$this->form_validation->set_rules('kode', 'Kode Custumer', 'required|min_length[4]|max_length[10]');
		$this->form_validation->set_rules('alamat', 'Alamat Customer', 'required');
		$this->form_validation->set_rules('name', 'Nama Customer', 'required');

		if ($this->form_validation->run() == FALSE) {
			$msg = validation_errors();
			$result = array('success' => false, 'message' => $msg);
		} else {

			if (anti_sql_injection($this->input->post('limit', TRUE)) == '') {
				$crt = 0;
			} else {
				$crt = anti_sql_injection($this->input->post('limit', TRUE));
			}

			$data = array(
				'code_cust' => anti_sql_injection($this->input->post('kode', TRUE)),
				'contact_person' => anti_sql_injection($this->input->post('cp', TRUE)),
				'cust_name' => anti_sql_injection($this->input->post('name', TRUE)),
				'region' => anti_sql_injection($this->input->post('region', TRUE)),
				'cust_address' => anti_sql_injection($this->input->post('alamat', TRUE)),
				'phone' => anti_sql_injection($this->input->post('telp', TRUE)),
				'email' => anti_sql_injection($this->input->post('email', TRUE)),
				'no_npwp' => anti_sql_injection($this->input->post('npwp', TRUE)),
				'npwp_name' => anti_sql_injection($this->input->post('npwp_name', TRUE)),
				'npwp_address' => anti_sql_injection($this->input->post('alamat_npwp', TRUE)),
				'district' => anti_sql_injection($this->input->post('distrik', TRUE)),
				'city' => anti_sql_injection($this->input->post('kota', TRUE)),
				'group' => anti_sql_injection($this->input->post('group', TRUE)),
				'credit_limit' => $crt,
				'status_credit' => 1

			);

			//print_r($data);die;

			$check_kode_result = $this->cust_management_model->check_customer($data);


			//print_r(count($check_kode_result));die;

			if (count($check_kode_result) > 0) {
				$msg = "Kode Customer Sudah Ada ";
				$result = array('success' => false, 'message' => $msg);
			} else {

				$add_prin_result = $this->cust_management_model->add_customer($data);

				//print_r($add_prin_result);die;

				if ($add_prin_result['result'] > 0) {

					// if(anti_sql_injection($this->input->post('limit', TRUE)) == ''){
					// $crt = 0;
					// }else{
					// $crt = anti_sql_injection($this->input->post('limit', TRUE));
					// }

					$data2 = array(
						'id_cust' => $add_prin_result['lastid'],
						'limit_price' => $crt,
						'create_date' => date('Y-m-d')

					);

					$add_prin_result = $this->cust_management_model->add_limit_history($data2);

					$msg = 'Berhasil Menambah data Customer';

					$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
					$result = array('success' => true, 'message' => $msg);
				} else {
					$msg = 'Gagal Menambah data Customer';

					$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
					$result = array('success' => false, 'message' => $msg);
				}
			}
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);

		//echo $kode;die;

	}

	public function upodate_customer()
	{
		//$this->form_validation->set_rules('kode_distributor', 'Kode Distributor', 'trim|required|min_length[4]|max_length[100]');

		if (anti_sql_injection($this->input->post('limit', TRUE)) == '') {
			$crt = 0;
		} else {
			$crt = anti_sql_injection($this->input->post('limit', TRUE));
		}

		$data = array(
			'id_t_cust' => anti_sql_injection($this->input->post('id', TRUE)),
			'code_cust' => anti_sql_injection($this->input->post('kode', TRUE)),
			'contact_person' => anti_sql_injection($this->input->post('cp', TRUE)),
			'cust_name' => anti_sql_injection($this->input->post('name', TRUE)),
			'region' => anti_sql_injection($this->input->post('region', TRUE)),
			'cust_address' => anti_sql_injection($this->input->post('alamat', TRUE)),
			'phone' => anti_sql_injection($this->input->post('telp', TRUE)),
			'email' => anti_sql_injection($this->input->post('email', TRUE)),
			'no_npwp' => anti_sql_injection($this->input->post('npwp', TRUE)),
			'npwp_name' => anti_sql_injection($this->input->post('npwp_name', TRUE)),
			'npwp_address' => anti_sql_injection($this->input->post('alamat_npwp', TRUE)),
			'district' => anti_sql_injection($this->input->post('distrik', TRUE)),
			'city' => anti_sql_injection($this->input->post('kota', TRUE)),
			'group' => anti_sql_injection($this->input->post('group', TRUE)),
			'credit_limit' => $crt,
			'status_credit' => 0
		);



		$add_prin_result = $this->cust_management_model->update_customer($data);
		if ($add_prin_result['result'] > 0) {

			$data2 = array(
				'id_cust' => $data['id_t_cust'],
				'limit_price' => anti_sql_injection($this->input->post('limit', TRUE)),
				'create_date' => date('Y-m-d')

			);

			$add_prin_result = $this->cust_management_model->add_limit_history($data2);

			$msg = 'Berhasil Menambah data Principal';

			$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
			$result = array('success' => true, 'message' => $msg);
		} else {
			$msg = 'Gagal Menambah data Principal';

			$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
			$result = array('success' => false, 'message' => $msg);
		}
		//print_r($data);
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//echo $kode;die;

	}

	public function konfirmasi_customer()
	{
		//$this->form_validation->set_rules('kode_distributor', 'Kode Distributor', 'trim|required|min_length[4]|max_length[100]');
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);



		//print_r($asas);die;

		$result_dist 		= $this->cust_management_model->update_status($params);
		if ($result_dist['result'] > 0) {


			$datas2 =  array(
				'id_cust' => $params['id_t_cust']
			);
			$asas = $this->cust_management_model->get_limit_history($datas2);

			//print_r($asas);die;

			$datas3 =  array(
				'id_cust' => $params['id_t_cust'],
				'id_hl' => $asas[0]['id_hl'],
				'approval_date' => date('Y-m-d')
			);

			//print_r($datas3);die;
			$this->cust_management_model->update_limit_history($datas3);

			$msg = 'Berhasil Menambah data Principal';

			$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
			$result = array('success' => true, 'message' => $msg);
		} else {
			$msg = 'Gagal Menambah data Principal';

			$this->log_activity->insert_activity('insert', $msg . ' dengan kode distributor ');
			$result = array('success' => false, 'message' => $msg);
		}
		//print_r($data);

		//echo $kode;die;

	}

	public function delete_customer()
	{
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data, true);

		//print_r($params);die;

		$result_dist 		= $this->cust_management_model->delete_customer($params['id']);

		$msg = 'Berhasil menghapus data Customer.';
		$result = array('success' => true, 'message' => $msg);
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
}
