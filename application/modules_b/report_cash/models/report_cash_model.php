<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Report_cash_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function list_report_cash($params = [])
	{
		$total = $this->db
			->select('id')
			->from('t_coa_value')
			->join('t_coa', 't_coa_value.id_coa = t_coa.id_coa')
			// ->join('(SELECT `date` AS `jdate`, SUM(`value`) AS jvalue  FROM t_coa_value GROUP BY `date`) as j', 't_coa_value.date = j.jdate')
			// ->join('t_po_mat', 't_coa_value.id_coa_temp = t_po_mat.id_t_ps', 'left')
			->get()
			->result_array();

		$queryBuilder = $this->db
			->select('*')
			->from('t_coa_value')
			->join('t_coa', 't_coa_value.id_coa = t_coa.id_coa');
		// ->join('(SELECT `date` AS `jdate`, SUM(`value`) AS jvalue  FROM t_coa_value GROUP BY `date`) as j', 't_coa_value.date = j.jdate')
		// ->join('t_po_mat', 't_coa_value.id_coa_temp = t_po_mat.id_t_ps', 'left');

		if (isset($params['coa_id']) && !empty($params['coa_id'])) {
			$queryBuilder = $queryBuilder->where(['t_coa.id_coa' => $params['coa_id']]);
			$queryBuilder = $queryBuilder->where('t_coa_value.date_insert between "'.$params["start_date"].' 00:00:00" and "'.$params["end_date"].' 23:59:59"');
		}

		if (isset($params['searchtxt']) && !empty($params['searchtxt'])) {
			$queryBuilder = $queryBuilder->like('t_coa_value.date_insert', $params['searchtxt']);
			$queryBuilder = $queryBuilder->or_like('t_coa.coa', $params['searchtxt']);
			$queryBuilder = $queryBuilder->or_like('t_coa.keterangan', $params['searchtxt']);
			$queryBuilder = $queryBuilder->or_like('t_po_mat.remarks', $params['searchtxt']);
		}

		$queryBuilder = $queryBuilder->order_by('t_coa_value.date ASC');

		if (isset($params['limit']) && $params['limit'] >= 0 && isset($params['offset'])) {
			$queryBuilder = $queryBuilder->limit($params['limit'], $params['offset']);
		}

		$result = $queryBuilder->get()->result_array();

		return [
			'data' => $result,
			'total_filtered' => count($total),
			'total' => count($total),
		];
	}

	/**
	 * get list of coa accounts
	 * 
	 * @return array
	 */
	public function get_coa_list()
	{
		$queryBuilder = $this->db
			->select('*')
			->from('t_coa')
			->where('type_coa','3');

		$result = $queryBuilder->get()->result_array();

		return $result;
	}
}
