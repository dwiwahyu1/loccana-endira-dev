<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Retur_pembelian_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function get_principles($params = array())
	{
		$this->db->select('*')
			->from('t_eksternal')
			->order_by('name_eksternal', 'asc');

		$query = $this->db->get();

		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_list_pembelian($idPembelianForUpdate = null)
	{
		$queryBuilder = $this->db
			->select('
				purchase_orders.no_po as no_pembelian,
				purchase_orders.id_po as id_pembelian,
				principles.name_eksternal as principle
			')
			->from('t_purchase_order AS purchase_orders')
			->join('t_eksternal AS principles', 'purchase_orders.id_distributor = principles.id');

		if ($idPembelianForUpdate) {
			$subQuery = "SELECT DISTINCT id_pembelian FROM t_retur_pembelian WHERE t_retur_pembelian.id_pembelian != " . $idPembelianForUpdate;
		} else {
			$subQuery = "SELECT DISTINCT id_pembelian FROM t_retur_pembelian";
		}

		$queryBuilder = $queryBuilder->where('purchase_orders.id_po NOT IN (' . $subQuery . ')',  NULL, FALSE);

		$result = $queryBuilder->get()->result_array();
		return $result;
	}

	public function get_list_retur_pembelian($params = array())
	{
		$queryBuilder = $this->db
			->select('
				retur_pembelian.id_retur_pembelian AS id_retur_pembelian,
				purchase_orders.no_po AS no_po,
				principles.name_eksternal AS principle,
				retur_pembelian.retur_date AS date,
				retur_pembelian.status AS status,
				users.nama AS pic_name
			')
			->from('t_retur_pembelian AS retur_pembelian')
			->join('t_purchase_order AS purchase_orders', 'retur_pembelian.id_pembelian = purchase_orders.id_po')
			->join('t_eksternal AS principles', 'purchase_orders.id_distributor = principles.id')
			->join('u_user AS users', 'retur_pembelian.pic = users.id', 'left');

		if (isset($params['searchtxt']) && !empty($params['searchtxt'])) {
			$queryBuilder = $queryBuilder->like('purchase_orders.no_po', $params['searchtxt']);
			$queryBuilder = $queryBuilder->or_like('principles.name_eksternal', $params['searchtxt']);
			$queryBuilder = $queryBuilder->or_like('retur_pembelian.retur_date', $params['searchtxt']);
			$queryBuilder = $queryBuilder->or_like('users.nama', $params['searchtxt']);
		}

		$result = $queryBuilder->get()->result_array();

		return [
			'data' => $result,
			'total' => count($result),
			'total_filtered' => count($result),
		];
	}

	public function get_retur_pembelian($id_retur_pembelian)
	{
		$queryBuilder = $this->db
			->select('*')
			->from('t_retur_pembelian')
			->join('t_purchase_order', 't_retur_pembelian.id_pembelian = t_purchase_order.id_po')
			->join('t_eksternal', 't_purchase_order.id_distributor = t_eksternal.id')
			->where([
				't_retur_pembelian.id_retur_pembelian' => $id_retur_pembelian,
			]);

		return $queryBuilder->get()->row_array();
	}

	public function get_detail_retur_pembelian($id_retur_pembelian)
	{
		$queryBuilder = $this->db->select('
				m_material.stock_code AS base_qty,
				m_material.stock_code AS stock_code,
				m_material.stock_name AS stock_name,
				m_uom.uom_symbol AS uom_symbol,
				t_po_mat.id_t_ps AS id_t_ps,
				t_po_mat.unit_price AS unit_price,
				t_po_mat.qty AS qty,
				t_detail_retur_pembelian.retur_qty AS retur_qty
			')
			->from('t_detail_retur_pembelian')
			->join('t_po_mat', 't_detail_retur_pembelian.id_po_mat = t_po_mat.id_t_ps')
			->join('m_material', 't_po_mat.id_material = m_material.id_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where([
				't_detail_retur_pembelian.id_retur_pembelian' => $id_retur_pembelian,
			]);

		return $queryBuilder->get()->result_array();
	}

	public function get_detail_pembelian($id_pembelian)
	{
		$queryBuilder = $this->db
			->select('
				purchase_orders.id_po AS purchase_id,
				purchase_orders.no_po AS purchase_number,
				purchase_orders.date_po AS purchase_date,
				purchase_orders.term_of_payment AS purchase_term,
				purchase_orders.keterangan AS purchase_note,
				purchase_orders.term_of_payment AS term_of_payment,
				principles.name_eksternal AS principle,
				principles.eksternal_address AS principle_address,
				principles.phone_1 AS principle_phone,
				principles.email AS principle_email
			')
			->from('t_purchase_order AS purchase_orders')
			->join('t_eksternal AS principles', 'purchase_orders.id_distributor = principles.id')
			->where(['purchase_orders.id_po' => $id_pembelian]);

		$result = $queryBuilder->get()->row_array();

		return $result;
	}

	public function get_detail_pembelian_items($id_pembelian)
	{
		$result = $this->db
			->select('
				po_mat.id_t_ps AS id_t_ps,
				po_mat.qty AS qty,
				po_mat.unit_price AS unit_price,
				m_material.stock_name AS stock_name,
				m_material.stock_code AS stock_code,
				m_material.base_qty AS base_qty,
				m_material.id_mat AS id_material,
				m_uom.uom_symbol AS uom_symbol
			')
			->from('t_po_mat AS po_mat')
			->join('m_material', 'po_mat.id_material = m_material.id_mat')
			->join('m_uom', 'm_material.unit_terkecil = m_uom.id_uom')
			->where([
				'po_mat.id_po' => $id_pembelian
			])
			->get()
			->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function add_retur_pembelian($params)
	{
		$this->db->insert('t_retur_pembelian', $params);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}	
	
	
	public function update_pomat($params)
	{
		
		$sql - "
		
		";
		$this->db->insert('t_retur_pembelian', $params);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_detail_retur_pembelian($params)
	{
		$this->db->insert('t_detail_retur_pembelian', $params);

		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function update_retur_pembelian($id_retur_pembelian, $params)
	{
		$queryBuilder = $this->db;

		foreach ($params as $key => $value) {
			$queryBuilder = $queryBuilder->set($key, $value);
		}

		$queryBuilder->where('id_retur_pembelian', $id_retur_pembelian)
			->update('t_retur_pembelian');

		return $this->get_retur_pembelian($id_retur_pembelian);
	}

	public function clear_detail_retur_pembelian($id_retur_pembelian)
	{
		$this->db->where('id_retur_pembelian', $id_retur_pembelian)
			->delete('t_detail_retur_pembelian');

		return true;
	}
}
