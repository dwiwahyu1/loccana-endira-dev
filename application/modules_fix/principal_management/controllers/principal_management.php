<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal_management extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('principal_management/principal_management_model');
		$this->load->library('log_activity');
		$this->load->library('priv');
		
		$this->form_validation->set_message('required', '%s Tidak Boleh Kosong');
		$this->form_validation->set_message('min_length', '%s Minimal 4 Karakter');
		$this->form_validation->set_message('max_length', '%s Maksimal 10 Karakter');
	}

	/**
	 * anti sql injection
	 * @return string
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	/**
	  * This function is redirect to index distributor page
	  * @return Void
	  */
	public function index() {
		
		$priv = $this->priv->get_priv();

		$data = array(
			'priv' => $priv
		);
		
		$this->template->load('maintemplate', 'principal_management/views/index', $data);
	}

	/**
	  * This function is used for showing distributor list
	  * @return Array
	  */
	function lists() {
		
		 if( !empty($_GET['sess_user_id']) ) {
			  $sess_user_id = $_GET['sess_user_id'];
		  } else {
			  $sess_user_id = NULL;
		  }
		  
		   if( !empty($_GET['sess_token']) ) {
			  $sess_token = $_GET['sess_token'];
		  } else {
			  $sess_token = NULL;
		  }
		  
		    if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		  if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		  if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 				
		  $order_fields = array('kode_eksternal'); // , 'COST'
		  $order = $this->input->get_post('order');
		  if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'desc';}; 
		  if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 	
		  
		    $params['limit'] 		= (int) $length;
			$params['offset'] 		= (int) $start;
			$params['order_column'] = $order_fields[$order_column];
			$params['order_dir'] 	= $order_dir;
			$params['sess_user_id'] = $sess_user_id;
			$params['sess_token'] 	= $sess_token;
			$params['searchtxt'] 	= $_GET['search']['value'];
		  
		 // print_r($params);die;
		  
		  $priv = $this->priv->get_priv();
		  
			$list = $this->principal_management_model->list_principal($params); 
			//print_r($list['data']);die;
			  $data = array();	
				   foreach ( $list['data'] as $k => $v ) {

						$action = '
						<div class="btn-group" style="display:'.$priv['update'].'"><button type="button" class="btn btn-custon-rounded-two btn-warning" data-toggle="tooltip" data-placement="top" title="Edit"  onClick="updateprincipal('. $v['id'].')" > <i class="fa fa-edit"></i> </button></div>
						<div class="btn-group" style="display:'.$priv['delete'].'"><button type="button" class="btn btn-custon-rounded-two btn-danger" data-toggle="tooltip" data-placement="top" title="Delete"  onClick="deletedistributor('. $v['id'].')" > <i class="fa fa-trash"></i> </button></div>';
						   array_push($data, 
							  array(
								  number_format($v['Rangking'],0,',','.'),
								  $v['kode_eksternal'],
								  $v['name_eksternal'],
								  $v['eksternal_address'],
								  $v['phone_1'],
								  $v['fax'],
								  $action
							  )
							);
							//$idx++;
				   }
		   
			 $result["data"] = $data;
		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		//$result["data"] = $list['data'];
	  
		echo json_encode($result);
	  
				//$this->json_result($result);
		  
	}

	/**
	  * This function is redirect to add distributor page
	  * @return Void
	  */
	public function add() {
		// $result = $this->distributor_model->location();

		$data = array(
			'location' => ''
		);

		$this->template->load('maintemplate', 'principal_management/views/addPrinciple',$data);
		//$this->load->view('addPrinciple',$data);
	}	


	public function updateprincipal() {
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('idprincipal', TRUE)),
		);
		
		//print_r($data);
		
		$principal_result = $this->principal_management_model->get_principal($data);
		
		//print_r($principal_result);die;
		
		// $result = $this->distributor_model->location();

		$data = array(
			'principal_data' => $principal_result[0]
		);

		$this->template->load('maintemplate', 'principal_management/views/updatePrinciple',$data);
		//$this->load->view('addPrinciple',$data);
	}	
	
	
	public function add_principal(){
		
		$this->form_validation->set_rules('kode', 'Kode Principal', 'required|min_length[4]|max_length[10]');
		$this->form_validation->set_rules('alamat', 'Alamat Principal', 'required');
		$this->form_validation->set_rules('name', 'Nama Principal', 'required');
		
		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			
			//echo $pesan;die;
			
			//$msg = strip_tags(str_replace("\r", '', $pesan));
			$msg = $pesan;

			$result = array('success' => false, 'message' => $msg);
		}else {
					
			$data = array(
				'kode' => $this->Anti_sql_injection($this->input->post('kode', TRUE)),
				'alamat' => $this->Anti_sql_injection($this->input->post('alamat', TRUE)),
				'name' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
				'telp' => $this->Anti_sql_injection($this->input->post('telp', TRUE)),
				'email' => $this->Anti_sql_injection($this->input->post('email', TRUE)),
				'fax' => $this->Anti_sql_injection($this->input->post('fax', TRUE)),
				'bank1' => $this->Anti_sql_injection($this->input->post('bank1', TRUE)),
				'norek1' => $this->Anti_sql_injection($this->input->post('norek1', TRUE)),
				'bank2' => $this->Anti_sql_injection($this->input->post('bank2', TRUE)),
				'norek2' => $this->Anti_sql_injection($this->input->post('norek2', TRUE)),
				'bank3' => $this->Anti_sql_injection($this->input->post('bank3', TRUE)),
				'norek3' => $this->Anti_sql_injection($this->input->post('norek3', TRUE)),
				'type_eksternal' => 1
			);
		
			$check_kode_result = $this->principal_management_model->check_principal($data);
			
			//print_r(count($check_kode_result));die;
			
			if(count($check_kode_result) > 0 ){
				$msg = "Kode Principal Sudah Ada ";
				$result = array('success' => false, 'message' => $msg);
			}else{
		
				$add_prin_result = $this->principal_management_model->add_principal($data);
				
				//print_r($add_prin_result);die;
				
				if ($add_prin_result['result'] > 0) {
					
					$msg = 'Berhasil Menambah data Principal';

					$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ');
					$result = array('success' => true, 'message' => $msg);
					
				}else{
					$msg = 'Gagal Menambah data Principal';

					$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ');
					$result = array('success' => false, 'message' => $msg);
					
				}
			}
			
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		//print_r($data);
		
		//echo $kode;die;
		
	}	
	
	public function update_principal(){
		//$this->form_validation->set_rules('kode_distributor', 'Kode Distributor', 'trim|required|min_length[4]|max_length[100]');
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('id', TRUE)),
			'kode' => $this->Anti_sql_injection($this->input->post('kode', TRUE)),
			'alamat' => $this->Anti_sql_injection($this->input->post('alamat', TRUE)),
			'name' => $this->Anti_sql_injection($this->input->post('name', TRUE)),
			'telp' => $this->Anti_sql_injection($this->input->post('telp', TRUE)),
			'fax' => $this->Anti_sql_injection($this->input->post('fax', TRUE)),
			'email' => $this->Anti_sql_injection($this->input->post('email', TRUE)),
			'bank1' => $this->Anti_sql_injection($this->input->post('bank1', TRUE)),
			'norek1' => $this->Anti_sql_injection($this->input->post('norek1', TRUE)),
			'bank2' => $this->Anti_sql_injection($this->input->post('bank2', TRUE)),
			'norek2' => $this->Anti_sql_injection($this->input->post('norek2', TRUE)),
			'bank3' => $this->Anti_sql_injection($this->input->post('bank3', TRUE)),
			'norek3' => $this->Anti_sql_injection($this->input->post('norek3', TRUE)),
			'type_eksternal' => 1
		);
		
	//	print_r($data);die;
		
		$add_prin_result = $this->principal_management_model->update_principal($data);
			if ($add_prin_result['result'] > 0) {
				
				$msg = 'Berhasil Menambah data Principal';

				$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ');
				$result = array('success' => true, 'message' => $msg);
				
			}else{
				$msg = 'Gagal Menambah data Principal';

				$this->log_activity->insert_activity('insert', $msg. ' dengan kode distributor ');
				$result = array('success' => false, 'message' => $msg);
				
			}
		//print_r($data);
		
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
		
		//echo $kode;die;
		
	}

	public function delete_principal() {
		$data   	= file_get_contents("php://input");
		$params     = json_decode($data,true);

		//print_r($params);die;

		$result_dist 		= $this->principal_management_model->delete_principal($params['id']);

		$msg = 'Berhasil menghapus data Principal.';
		$result = array('success' => true, 'message' => $msg);
		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}
	
}