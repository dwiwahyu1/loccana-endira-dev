<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto
  }

  .scroll-overflow {
    min-height: 650px
  }

  #modal-distributor::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }
  

</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="sparkline13-list">
          <div class="card-box table-responsive">
            <div class="dropdown pull-right">
              <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                <i class="zmdi zmdi-more-vert"></i>
              </a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
            </div>

            <h4 class="header-title m-t-0 m-b-30">Stock</h4>

            <table id="listitems" class="table table-striped table-bordered nowrap" cellspacing="0" width="100%">
              <thead id='rhead'>
                <tr>
                  <th rowspan=2>Kode</th>
                  <th rowspan=2 width="100">Produk</th>
                  <th rowspan=2>Kemasan</th>
                  <th rowspan=2 width="150px">Principal</th>
				  <th rowspan=2>Box per LT/KG</th>
                  <th colspan=2>Stock Awal </th>
                  <th colspan=1>Penerimaan </th>
                  <th colspan=1>DO Wilayah 1</th>
                  <th colspan=1>DO Wilayah 2</th>
                  <th colspan=1>DO Wilayah 3</th>
                  <th colspan=1>DO Office</th>
				  <th colspan=2>Stok Akhir </th>
                 
                  <!-- <th>Status</th> -->
                  <th rowspan=2 width="100px">Option</th>
                </tr>
				<tr>
					<th>Lt/Kg</th>
					<th>Box</th>
					<th><?php echo date('d/M/Y') ?></th>
					<th><?php echo date('d/M/Y') ?></th>
					<th><?php echo date('d/M/Y') ?></th>
					<th><?php echo date('d/M/Y') ?></th>
					<th><?php echo date('d/M/Y') ?></th>
					<th>Lt/Kg</th>
					<th>Box</th>
				</tr>
              </thead>
              <tbody id='rbody'></tbody>
            </table>
          </div>
        </div><!-- end col -->
      </div>
    </div>
  </div>
  <span id="laod"></span>
</div>


<div id="panel-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
  <div class="modal-dialog" style="width:800px;">
    <div class="modal-content p-0 b-0">
      <div class="panel panel-color panel-primary panel-filled">
        <div class="panel-heading">
          <button type="button" class="close m-t-5" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 class="panel-title"></h3>
        </div>
        <div class="panel-body force-overflow" id="modal-material">
          <div class="scroll-overflow">
            <p></p>
          </div>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">

  function editmaterial(id) {
    var url = '<?php echo base_url(); ?>stock/edit';

    $("#laod").append(' <img id="loading" src="<?php echo base_url(); ?>assets/urate-frontend-master/assets/images/icon_loader.gif">');
    var form = $("<form action='" + url + "' method='post'>" +
      "<input type='hidden' name='iditems' value='" + id + "' />" +
      "</form>");
    $('body').append(form);
    form.submit();
    // $('#panel-modal').removeData('bs.modal');
    // $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    // $('#panel-modal  .panel-body').load('<?php echo base_url('stock/edit/'); ?>' + "/" + id);
    // $('#panel-modal  .panel-title').html('<i class="fa fa-pencil"></i> Edit Stok');
    // $('#panel-modal').modal({
    //   backdrop: 'static',
    //   keyboard: false
    // }, 'show');
  }

  function detailmaterial(id) {
    var url = '<?php echo base_url(); ?>stock/detail';

    $("#laod").append(' <img id="loading" src="<?php echo base_url(); ?>assets/urate-frontend-master/assets/images/icon_loader.gif">');
    var form = $("<form action='" + url + "' method='post'>" +
      "<input type='hidden' name='iditems' value='" + id + "' />" +
      "</form>");
    $('body').append(form);
    form.submit();
    // $('#panel-modal').removeData('bs.modal');
    // $('#panel-modal  .panel-body').html('<i class="fa fa-cog fa-spin fa-2x fa-fw"></i> Loading...');
    // $('#panel-modal  .panel-body').load('<?php echo base_url('stock/edit/'); ?>' + "/" + id);
    // $('#panel-modal  .panel-title').html('<i class="fa fa-pencil"></i> Edit Stok');
    // $('#panel-modal').modal({
    //   backdrop: 'static',
    //   keyboard: false
    // }, 'show');
  }
  
  function filter(){

	//id=""
	//$('#btn_filter').attr();
	$("#btn_filter").prop('disabled', true);
	
	 var datapost = {
		"start_time": $('#start_time').val(),
		"end_time": $('#end_time').val()
      };
	  
	    // swal.fire({
                // title: 'Please Wait !',
                // html: 'data uploading',// add html attribute if you want or remove
                // allowOutsideClick: false,
                // onBeforeOpen: () => {
                    // Swal.showLoading()
                // },
            // });
			
		swal({
            title: 'Loading',
            text: 'Loading Data',
            // type: 'success',
            showCancelButton: false,
            showConfirmButton: false,
			 imageUrl: '<?php echo base_url(); ?>assets/images/ajax-loader.gif',
            confirmButtonText: 'Ok',
			allowOutsideClick: false

        }).then(function() {})
	
	  $.ajax({
        type: 'POST',
        url: "<?php echo base_url() . 'stock/filter'; ?>",
        data: JSON.stringify(datapost),
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {

			$('#listitems').DataTable().clear().destroy();
			
			$('#rhead').html('');
			$('#rbody').html('');
			$('#rhead').html(response.table);
			$('#rbody').html(response.body);
			
			// $(document).ready(function() {
				// $('#listitems').DataTable();
			// } );
			
			  $("#listitems").dataTable({
				  "processing": true,
				  "searchDelay": 700,
				  "responsive": false,
				  "lengthChange": false,
				  "info": false,
				  "bSort": false,
				  "dom": 'l<"toolbar">frtip',
					"scrollX":true,
				   "sScrollY": "300px",
				 fixedColumns: {
						leftColumns: 2
					},
					scrollX: true,
				  "initComplete": function() {
						$("div.toolbar").prepend('<div class="btn-group pull-left"><input class="form-control" type="text" name="start_time" id="start_time" value="<?php echo date('Y-m-d'); ?>" /> s/d <input class="form-control" type="text" name="end_time" id="end_time" value="<?php echo date('Y-m-d'); ?>" /></div>&nbsp<a href="#" type="button" class="btn btn-custon-rounded-two btn-primary" onClick="filter()" id="btn_filter" > Filter </a>&nbsp<a href="#" type="button" class="btn btn-custon-rounded-two btn-primary" onClick="exports()" id="btn_filter_exp" > Export </a>');
						
						$("#btn_filter").prop('disabled', false);
						
						$('#start_time').val(datapost.start_time)
						$('#end_time').val(datapost.end_time)
						
						swal.close()

						
						 $('#start_time').datepicker({
						  isRTL: true,
						 format: "yyyy-mm-dd",
						  autoclose: true,
						  todayHighlight: true,
						  changeYear: true,
						});
				
						 $('#end_time').datepicker({
						  isRTL: true,
						 format: "yyyy-mm-dd",
						  autoclose: true,
						  todayHighlight: true,
						  changeYear: true,
						});
						
				  },
						"columnDefs" : [{
							"targets" : [3],
							"className" : 'dt-body-right'
						},{
							"targets" : [4],
							"className" : 'dt-body-right'
						},{
							"targets" : [5],
							"className" : 'dt-body-right'
						},{
							"targets" : [6],
							"className" : 'dt-body-center'
						}]
				});

        }
      });
  }
  
  
   function exports(){
	var url = '<?php echo base_url(); ?>stock/export';

	// swal({
            // title: 'Loading',
            // text: 'Loading Data',
            // // type: 'success',
            // showCancelButton: false,
            // showConfirmButton: false,
			 // imageUrl: '<?php echo base_url(); ?>assets/images/ajax-loader.gif',
            // confirmButtonText: 'Ok',
			// allowOutsideClick: false

        // }).then(function() {})

    var form = $("<form action='" + url + "' method='post' target='_blank'>" +
      "<input type='hidden' name='tgl_awal' value='" + $('#start_time').val() + "' />" +
      "<input type='hidden' name='tgl_akhir' value='" + $('#end_time').val() + "' />" +
      "</form>");
    $('body').append(form);
    form.submit();
	
	//swal.close()
  }
  
  
  $(document).ready(function() {

    $("#listitems").dataTable({
      "processing": true,
      "serverSide": true,
      "ajax": "<?php echo base_url() . 'stock/lists/'; ?>",
      "searchDelay": 700,
      "responsive": false,
      "lengthChange": false,
      "info": false,
      "bSort": false,
      "dom": 'l<"toolbar">frtip',
	    "scrollX":true,
	   "sScrollY": "300px",
     fixedColumns: {
            leftColumns: 2
        },
        scrollX: true,
      "initComplete": function() {
			$("div.toolbar").prepend('<div class="btn-group pull-left"><input class="form-control" type="text" name="start_time" id="start_time" value="<?php echo date('Y-m-d'); ?>" /> s/d <input class="form-control" type="text" name="end_time" id="end_time" value="<?php echo date('Y-m-d'); ?>" /></div>&nbsp<a href="#" type="button" class="btn btn-custon-rounded-two btn-primary" onClick="filter()" id="btn_filter" > Filter </a>&nbsp<a href="#" type="button" class="btn btn-custon-rounded-two btn-primary" onClick="exports()" id="btn_filter_exp" > Export </a>');
			
			 $('#start_time').datepicker({
			  isRTL: true,
			 format: "yyyy-mm-dd",
			  autoclose: true,
			  todayHighlight: true,
			  changeYear: true,
			});
	
			 $('#end_time').datepicker({
			  isRTL: true,
			 format: "yyyy-mm-dd",
			  autoclose: true,
			  todayHighlight: true,
			  changeYear: true,
			});
			
      },
			"columnDefs" : [{
				"targets" : [3],
				"className" : 'dt-body-right'
			},{
				"targets" : [4],
				"className" : 'dt-body-right'
			},{
				"targets" : [5],
				"className" : 'dt-body-right'
			},{
				"targets" : [6],
				"className" : 'dt-body-center'
			}]
    });
	
	
	
	
  });
</script>