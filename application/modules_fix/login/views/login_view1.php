<?php
    $image = base_url() . 'assets/images/';
    $api   = base_url() . 'api/';
    $path2 = base_url() . 'assets/adminto-14/adminto-14/Admin/Light/';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <title>Inventory App - Login</title>
    <link rel="shortcut icon" href="<?php echo $path2;?>assets/images/favicon.ico?jsr=<?php echo jsversionstring();?>" />
    <link href="<?php echo $path2;?>assets/css/bootstrap.min.css?jsr=<?php echo jsversionstring();?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo $path2;?>assets/css/core.css?jsr=<?php echo jsversionstring();?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo $path2;?>assets/css/components.css?jsr=<?php echo jsversionstring();?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo $path2;?>assets/css/icons.css?jsr=<?php echo jsversionstring();?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo $path2;?>assets/css/pages.css?jsr=<?php echo jsversionstring();?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo $path2;?>assets/css/menu.css?jsr=<?php echo jsversionstring();?>" rel="stylesheet" type="text/css" />
    <link href="<?php echo $path2;?>assets/css/responsive.css?jsr=<?php echo jsversionstring();?>" rel="stylesheet" type="text/css" />
    
    <link href="<?php echo base_url();?>assets/css/custom.css?jsr=<?php echo jsversionstring();?>" rel="stylesheet" type="text/css" />      
   	
      
      
	<script src="<?php echo $path2;?>assets/js/jquery.min.js?jsr=<?php echo jsversionstring();?>"></script>
    <script src="<?php echo $path2;?>assets/js/bootstrap.min.js?jsr=<?php echo jsversionstring();?>"></script>
      
	 	 <link href="<?php echo base_url();?>assets/sweetalert/sweetalert2.min.css?jsr=<?php echo jsversionstring();?>" rel="stylesheet" type="text/css" />
	 <script src="<?php echo base_url();?>assets/sweetalert/sweetalert2.min.js?jsr=<?php echo jsversionstring();?>"></script>

  </head>
    
    
    
    
<body>

        <div class="account-pages"></div>
       
    
     
            
     <?php $message = $this->session->flashdata('msg'); if ($message): ?>
    <script>swal("<?php echo $message['status']; ?>", "<?php echo $message['message']; ?>", "<?php echo $message['status']; ?>")</script>
    <?php endif; ?>
        <div class="clearfix"></div>
        <div class="wrapper-page" >
            <div class="text-center">
                
               <h1 class="text-muted m-t-0 font-600">Inventory App</h1>
            </div>
        	<div class="login-container m-t-40 card-box">
                
                <div class="panel-body">
                    
                    <form id="losloginform" method="POST">
					  <div class="form-group">
						<label for="exampleInputEmail1">Username</label>
						<input type="text" class="form-control" id="username" placeholder="Username">
					  </div>
					  <div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<input type="password" class="form-control" id="password" placeholder="Password">
					  </div>
            <div class="loginalert error"></div>
                        
                    

                        <div class="form-group text-center m-t-10">
                             
                            <div>
                                <button class="btn btn-custom btn-bordred btn-block waves-effect waves-light" id="login" type="submit">Log In</button>
                            </div>
                           
                        </div>  
                    </form>
                    

                </div>
            </div>
        </div>
    
    
    <script>
      $(document).ready(function () {
        $("#loader").hide();

        $('#username,#password').keypress(function(e) {
          if(e.which == 13) {
              $('#losloginform').trigger('submit');
          }
        });
        
        //$("#login").on("click", function () {
        $("#losloginform").on("submit", function (e) {
			e.preventDefault();
        
          $("#loader").show();
          var vusername   = $('#username').val();
          var vpassword   = $('#password').val();

          var datapost={
            "appkey"    :   123456,
            "username"  :   vusername,
            "password"  :   vpassword
          };

          $.ajax({
            type: "POST",
            url: "api/login/auth",
            data : JSON.stringify(datapost),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function(response) {
              if (response.status == "success") {
				  console.log(response);
				  if(response.id_role == 99){
					
                window.location.href = "<?php echo base_url('home');?>";  
				  }else{
					  
                window.location.href = "<?php echo base_url('home');?>";
				  }
              } else{
            //alert("Failed!", response.message, "error");
                $(".loginalert").html(response.message);
              }
            }
          });
        });

      });
    </script>

  </body>
</html>
