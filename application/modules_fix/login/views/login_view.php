
<?php
    $image = base_url() . 'assets/ocr/images/';
    $api   = base_url() . 'api/';
    $path = base_url() . 'assets/ocr/material/base/';
    $path2 = base_url() . 'assets/ocr/material/';
?>

<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
    <title>Login </title>
    
   <!--  <link rel="apple-touch-icon" href="<?php echo $path; ?>assets/images/apple-touch-icon.png">
   <link rel="shortcut icon" href="<?php echo $path; ?>assets/images/favicon.ico"> -->
   <link rel="apple-touch-icon" href="<?php echo $image; ?>Logo_Gram_White.png">
    <link rel="shortcut icon" href="<?php echo $image; ?>Logo_Gram_White.png">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?php echo $path2; ?>global/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $path2; ?>global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="<?php echo $path; ?>assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="<?php echo $path2; ?>global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="<?php echo $path2; ?>global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="<?php echo $path2; ?>global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="<?php echo $path2; ?>global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="<?php echo $path2; ?>global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="<?php echo $path2; ?>global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="<?php echo $path2; ?>global/vendor/waves/waves.css">
        <link rel="stylesheet" href="<?php echo $path; ?>assets/examples/css/pages/login-v2.css">
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="<?php echo $path2; ?>global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="<?php echo $path2; ?>global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    
    <!--[if lt IE 9]>
    <script src="<?php echo $path2; ?>global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    
    <!--[if lt IE 10]>
    <script src="<?php echo $path2; ?>global/vendor/media-match/media.match.min.js"></script>
    <script src="<?php echo $path2; ?>global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script src="<?php echo $path2; ?>global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
  </head>
  <body class="animsition page-login-v2 layout-full page-dark">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->


    <!-- Page -->
    <div class="page" data-animsition-in="fade-in" data-animsition-out="fade-out">
      <div class="page-content">
        <div class="page-brand-info">
          <div class="brand">
           <!-- <img class="brand-img" src="<?php echo $path; ?>assets/images/logo@2x.png" alt="...">
            <img class="brand-img" src="<?php echo $image; ?>Logo_Gram_White.png" width="100px" alt="...">-->
            <img class="brand-img" src="<?php echo $image; ?>Landscape_White.png" width="500px" alt="...">
            <h2 class="brand-text font-size-40"></h2>
          </div>
          <p class="font-size-50"><br>Loccana <br> Inventory & Sales System.</p>
        </div>

        <div class="page-login-main">
          <div class="brand hidden-md-up">
            <img class="brand-img center" src="<?php echo $image; ?>Logo_Gram_Blue.png" width="200px" alt="...">
            <h3 class="brand-text font-size-20"><br>Loccana <br> Inventory & Sales System.</h3>
          </div>
          <h3 class="font-size-24">Login</h3>
          <p></p>

          <form method="post" action="login-v2.html" autocomplete="off" id="losloginform">
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="text" class="form-control empty" id="username" name="username">
              <label class="floating-label" for="inputEmail">Username</label>
            </div>
            <div class="form-group form-material floating" data-plugin="formMaterial">
              <input type="password" class="form-control empty" id="password" name="password">
              <label class="floating-label" for="inputPassword">Password</label>
            </div>
            <div class="form-group clearfix">
              <!--<div class="checkbox-custom checkbox-inline checkbox-primary float-left">
                <input type="checkbox" id="remember" name="checkbox">
                <label for="inputCheckbox">Remember me</label>
              </div>
              <a class="float-right" href="forgot-password.html">Forgot password?</a>-->
            </div>
            <button type="submit" class="btn btn-primary btn-block">Sign in</button>
          </form>

        <!--  <p>No account? <a href="register-v2.html">Sign Up</a></p> 

          <footer class="page-copyright">
            <p>WEBSITE BY Creation Studio</p>
            <p>© 2018. All RIGHT RESERVED.</p>
            <div class="social">
              <a class="btn btn-icon btn-round social-twitter mx-5" href="javascript:void(0)">
            <i class="icon bd-twitter" aria-hidden="true"></i>
          </a>
              <a class="btn btn-icon btn-round social-facebook mx-5" href="javascript:void(0)">
            <i class="icon bd-facebook" aria-hidden="true"></i>
          </a>
              <a class="btn btn-icon btn-round social-google-plus mx-5" href="javascript:void(0)">
            <i class="icon bd-google-plus" aria-hidden="true"></i>
          </a>
            </div>
          </footer>-->
        </div>

      </div>
    </div>
    <!-- End Page -->


    <!-- Core  -->
    <script src="<?php echo $path2; ?>global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="<?php echo $path2; ?>global/vendor/jquery/jquery.js"></script>
    <script src="<?php echo $path2; ?>global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="<?php echo $path2; ?>global/vendor/bootstrap/bootstrap.js"></script>
    <script src="<?php echo $path2; ?>global/vendor/animsition/animsition.js"></script>
    <script src="<?php echo $path2; ?>global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="<?php echo $path2; ?>global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="<?php echo $path2; ?>global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="<?php echo $path2; ?>global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
    <script src="<?php echo $path2; ?>global/vendor/waves/waves.js"></script>
    
    <!-- Plugins -->
    <script src="<?php echo $path2; ?>global/vendor/switchery/switchery.js"></script>
    <script src="<?php echo $path2; ?>global/vendor/intro-js/intro.js"></script>
    <script src="<?php echo $path2; ?>global/vendor/screenfull/screenfull.js"></script>
    <script src="<?php echo $path2; ?>global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="<?php echo $path2; ?>global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
    
    <!-- Scripts -->
    <script src="<?php echo $path2; ?>global/js/Component.js"></script>
    <script src="<?php echo $path2; ?>global/js/Plugin.js"></script>
    <script src="<?php echo $path2; ?>global/js/Base.js"></script>
    <script src="<?php echo $path2; ?>global/js/Config.js"></script>
    
    <script src="<?php echo $path; ?>assets/js/Section/Menubar.js"></script>
    <script src="<?php echo $path; ?>assets/js/Section/GridMenu.js"></script>
    <script src="<?php echo $path; ?>assets/js/Section/Sidebar.js"></script>
    <script src="<?php echo $path; ?>assets/js/Section/PageAside.js"></script>
    <script src="<?php echo $path; ?>assets/js/Plugin/menu.js"></script>
    
    <script src="<?php echo $path2; ?>global/js/config/colors.js"></script>
    <script src="../../assets/js/config/tour.js"></script>
    <script>Config.set('assets', '../../assets');</script>
    
    <!-- Page -->
    <script src="<?php echo $path; ?>assets/js/Site.js"></script>
    <script src="<?php echo $path2; ?>global/js/Plugin/asscrollable.js"></script>
    <script src="<?php echo $path2; ?>global/js/Plugin/slidepanel.js"></script>
    <script src="<?php echo $path2; ?>global/js/Plugin/switchery.js"></script>
        <script src="<?php echo $path2; ?>global/js/Plugin/jquery-placeholder.js"></script>
        <script src="<?php echo $path2; ?>global/js/Plugin/material.js"></script>
    
    <script>
      (function(document, window, $){
        'use strict';
    
        var Site = window.Site;
        $(document).ready(function(){
          Site.run();
        });
      })(document, window, jQuery);
    </script>
	
	  <script>
      $(document).ready(function () {
        $("#loader").hide();

        $('#username,#password').keypress(function(e) {
          if(e.which == 13) {
              $('#losloginform').trigger('submit');
          }
        });
        
        //$("#login").on("click", function () {
        $("#losloginform").on("submit", function (e) {
			e.preventDefault();
        
          $("#loader").show();
          var vusername   = $('#username').val();
          var vpassword   = $('#password').val();

          var datapost={
            "appkey"    :   123456,
            "username"  :   vusername,
            "password"  :   vpassword
          };

          $.ajax({
            type: "POST",
            url: "api/login/auth",
            data : JSON.stringify(datapost),
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            success: function(response) {
              if (response.status == "success") {
				  console.log(response);
				  if(response.id_role == 99){
					
                window.location.href = "<?php echo base_url('home');?>";  
				  }else{
					  
                window.location.href = "<?php echo base_url('home');?>";
				  }
              } else{
            //alert("Failed!", response.message, "error");
                $(".loginalert").html(response.message);
              }
            }
          });
        });

      });
    </script>
    
  </body>
</html>
