<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Items extends MX_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('items/material_model');
		$this->load->library('log_activity');
		$this->load->library('priv');
	}

	/**
	 * anti sql injection
	 */
	public function Anti_sql_injection($string) {
		$string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
		return $string;
	}

	public function index() {
		
		$priv = $this->priv->get_priv();

		$data = array(
			'priv' => $priv
		);
		
		$this->template->load('maintemplate', 'items/views/index', $data);
	}

	function lists() {
		$draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
		$length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
		$start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
		$order = $this->input->get_post('order');
		$order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
		$order_column = (!empty($order[0]['column'])) ? $order[0]['column'] :1;

		$order_fields = array('', 'stock_code', 'stock_name');

		$search = $this->input->get_post('search');

		$search_val = (!empty($search['value'])) ? $search['value'] : null;

		$search_value = $this->Anti_sql_injection($search_val);

		// Build params for calling model
		$params['limit'] = (int) $length;
		$params['offset'] = (int) $start;
		$params['order_column'] = $order_fields[$order_column];
		$params['order_dir'] = $order_dir;
		$params['searchtxt'] = $search_value;

		$list = $this->material_model->lists($params);

		$result["recordsTotal"] = $list['total'];
		$result["recordsFiltered"] = $list['total_filtered'];
		$result["draw"] = $draw;
		
		$priv = $this->priv->get_priv();

		$data = array();
		//$i = 0;
		$i = $params['offset'];
		$username = $this->session->userdata['logged_in']['username'];
		foreach ($list['data'] as $k => $v) {
			$i = $i + 1;
			$status_akses =
				'<div class="btn-group" style="display:'.$priv['update'].'"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="editmaterial(\'' . $v['id_mat'] . '\')"><i class="fa fa-edit"></i></button></div>
				<div class="btn-group" style="display:'.$priv['delete'].'"><button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deletematerial(\'' . $v['id_mat'] . '\')"><i class="fa fa-trash"></i></button></div>';
			
			array_push($data, array(
				$i,
				$v['stock_code'],
				$v['stock_name'].' '.$v['base_qty'].' '.$v['uom_symbol'],
				$v['stock_description'],
				$v['uom_name'],
				$v['unit_box'],
				$v['name_eksternal'],
				$status_akses
			));
		}

		$result["data"] = $data;

		$this->output->set_content_type('application/json')->set_output(json_encode($result));
	}

	public function add() {
		//$unit = $this->material_model->get_unit();
		$unit = $this->material_model->get_unit();
		$Gudang = $this->material_model->get_distributor();

		$data = array(
			'unit' => $unit,
			'Gudang' => $Gudang
		);

    $this->template->load('maintemplate', 'items/views/add_modal_view', $data);
	}
	
	public function edit() {
    $data = array(
			'id' => $this->Anti_sql_injection($this->input->post('iditems', TRUE)),
		);
		$result = $this->material_model->get_material($data['id']);
		
		$unit = $this->material_model->get_unit();
		$Gudang = $this->material_model->get_distributor();

		$data = array(
			'stok' 				=> $result,
			'unit' 				=> $unit,
			'Gudang' 			=> $Gudang
		);

    $this->template->load('maintemplate', 'items/views/edit_modal_view', $data);
	}

	public function deletes() {
		$data 	= file_get_contents("php://input");
		$params 	= json_decode($data,true);
		
		$list = $this->material_model->deletes($this->Anti_sql_injection($params['id']));
		
		if($list > 0) {
			$this->log_activity->insert_activity('insert', 'Berhasil Hapus Item');
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}else {
			$this->log_activity->insert_activity('insert', 'Berhasil Insert Item');
			$res = array('status' => 'success', 'message' => 'Data telah di hapus');
		}
		
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		header("access-control-allow-origin: *");
		echo json_encode($res);
		

	}

	public function edit_material() 
	{
		$this->form_validation->set_rules('kode_stok', 'Kode Item', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('nama_stok', 'Nama Item', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('desk_stok', 'Deskripsi', 'trim|required');
		
		$this->form_validation->set_rules('unit', 'Unit', 'trim|required');
		$this->form_validation->set_rules('id_mat', 'Unit', 'trim|required');
		$this->form_validation->set_rules('qty', 'Unit Box', 'trim|required');
		$this->form_validation->set_rules('gudang', 'Gudang', 'trim|required');
	  

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		} else {
			$user_id 			= $this->session->userdata['logged_in']['user_id'];
			$id_mat 			= $this->Anti_sql_injection($this->input->post('id_mat', TRUE));
			$kode_stok 			= $this->Anti_sql_injection($this->input->post('kode_stok', TRUE));
			$nama_stok 			= $this->Anti_sql_injection($this->input->post('nama_stok', TRUE));
			$desk_stok 			= $this->Anti_sql_injection($this->input->post('desk_stok', TRUE));
			$unit 				= $this->Anti_sql_injection($this->input->post('unit', TRUE));
			if($this->Anti_sql_injection($this->input->post('limit', TRUE)) == '' ){
				$limit 				= 0;
			}else{
				$limit 				= $this->Anti_sql_injection($this->input->post('limit', TRUE));
			}
			
			$daily_stock 		= $this->Anti_sql_injection($this->input->post('status_i', TRUE));
			$qty 				= $this->Anti_sql_injection($this->input->post('qty', TRUE));
			$base_qty 			= $this->Anti_sql_injection($this->input->post('satuan_kecil', TRUE));
			$gudang 			= $this->Anti_sql_injection($this->input->post('gudang', TRUE));
			//$gudang 			= $this->Anti_sql_injection($this->input->post('gudang', TRUE));
			
			$data = array(
				'id_mat' 	      => $id_mat,
				'kode_stok' 		=> $kode_stok,
				'nama_stok' 		=> $nama_stok,
				'base_qty' 			=> $base_qty,
				'desk_stok' 		=> $desk_stok,
				'limit' 			=> $limit,
				'unit' 			  	=> $unit,
				'qty' 			  	=> $qty,
				'dist_id' 			=> $gudang,
				'status' 			=> $daily_stock,
				'unit_box' 			=> $qty,
			);
			
			$result = $this->material_model->edit_material($data);

			if(count($result) > 0){

			}

			if (count($result) > 0) {
				$msg = 'Berhasil mengubah data item ke database';
				
				$this->log_activity->insert_activity('insert', 'Berhasil Update Item');
				$results = array(
					'success' => true, 
					'message' => $msg
				);
			} else {
				$msg = 'Gagal mengubah data item ke database';
				
				$this->log_activity->insert_activity('insert', 'Gagal Update Item');
				$results = array(
					'success' => false, 
					'message' => $msg
				);
			}
			
			$this->output->set_content_type('application/json')->set_output(json_encode($results),true);
		}
	}
	
	public function get_properties() {
		$id = $this->Anti_sql_injection($this->input->post('id', TRUE));
		$detail_prop = $this->material_model->detail_prop_full($id);
		 
		$result = array(
			'success' => true,
			'message' => '',
			'data' => $detail_prop
		);

		$this->output->set_content_type('application/json')->set_output(json_encode($result,true));
	}
	
	public function add_material() {
		$this->form_validation->set_rules('kode_stok', 'Kode Item', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('nama_stok', 'Nama Item', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('desk_stok', 'Deskripsi', 'trim|required');
		$this->form_validation->set_rules('unit', 'Unit', 'trim|required');
		$this->form_validation->set_rules('qty', 'Unit Box', 'trim|required');
		$this->form_validation->set_rules('gudang', 'Distributor', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$pesan = validation_errors();
			$msg = strip_tags(str_replace("\n", '', $pesan));

			$result = array('success' => false, 'message' => $msg);
			$this->output->set_content_type('application/json')->set_output(json_encode($result));
		}else {
			$user_id 		= $this->session->userdata['logged_in']['user_id'];
			$kode_stok 		= $this->Anti_sql_injection($this->input->post('kode_stok', TRUE));
			$nama_stok 		= $this->Anti_sql_injection($this->input->post('nama_stok', TRUE));
			$desk_stok 		= $this->Anti_sql_injection($this->input->post('desk_stok', TRUE));
			$base_qty 		= $this->Anti_sql_injection($this->input->post('satuan_kecil', TRUE));
			$unit 			= $this->Anti_sql_injection($this->input->post('unit', TRUE));
			$qty 			= $this->Anti_sql_injection($this->input->post('qty', TRUE));
			$gudang 		= $this->Anti_sql_injection($this->input->post('gudang', TRUE));

			$data = array(
				'kode_stok' 	    => $kode_stok,
				'nama_stok' 	    => $nama_stok,
				'desk_stok' 	    => $desk_stok,
				'unit'		        => $unit,
				'base_qty'		        => $base_qty,
				'unit_box' 			  => $qty,
				'dist_id' 	    	=> $gudang,
			);

			$result = $this->material_model->new_add_material($data);
			if ($result['result'] > 0) {

				$msg = 'Berhasil menambahkan Item ke database.';
				$this->log_activity->insert_activity('insert', 'Berhasil Insert Item');
				$results = array('success' => true, 'message' => $msg);
			}else {
				$msg = 'Gagal menambahkan Item ke database.';
				$this->log_activity->insert_activity('insert', 'Gagal Insert Item');
				$results = array('success' => false, 'message' => $msg);
			}

			$this->output->set_content_type('application/json')->set_output(json_encode($results));
		}
	}
}
