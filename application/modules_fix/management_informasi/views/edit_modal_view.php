<style>
  .dt-body-left {
    text-align: left;
  }

  .dt-body-right {
    text-align: right;
  }

  .dt-body-center {
    text-align: center;
    vertical-align: middle;
  }

  .force-overflow {
    height: 650px;
    overflow-y: auto;
    overflow-x: auto
  }

  .scroll-overflow {
    min-height: 650px
  }

  #modal-distributor::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar {
    width: 10px;
    background-color: #F5F5F5;
  }

  #modal-distributor::-webkit-scrollbar-thumb {
    background-image: -webkit-gradient(linear,
        left bottom,
        left top,
        color-stop(0.44, rgb(122, 153, 217)),
        color-stop(0.72, rgb(73, 125, 189)),
        color-stop(0.86, rgb(28, 58, 148)));
  }
</style>

<div class="product-sales-area mg-tb-30">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="product-payment-inner-st">
          <ul id="myTabedu1" class="tab-review-design">
            <li class="active"><a href="#description">Update Informasi</a></li>
          </ul>
          <div id="myTabContent" class="tab-content custom-product-edit">
            <div class="product-tab-list tab-pane fade active in" id="description">
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <div class="review-content-section">
                    <form class="form-horizontal form-label-left" id="edit_uom" role="form" action="<?php echo base_url('management_informasi/edit_informasi'); ?>" method="post" enctype="multipart/form-data" data-parsley-validate>

                      <p>Harap isi data yang telah ditandai dengan <code>*</code>, dan masukkan data dengan benar.</p>

                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Judul <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <input data-parsley-maxlength="255" type="text" id="judul_informasi" name="judul_informasi" class="form-control col-md-7 col-xs-12" placeholder="judul informasi" value="<?php if (isset($informasi[0]['judul'])) {
                                                                                                                                                                  echo $informasi[0]['judul'];
                                                                                                                                                                } ?>" required="required">

                          <input data-parsley-maxlength="255" type="hidden" id="id_info" name="id_info" class="form-control col-md-7 col-xs-12" placeholder="id_info" value="<?php if (isset($informasi[0]['id_info'])) {
                                                                                                                                                                          echo $informasi[0]['id_info'];
                                                                                                                                                                        } ?>" required="required">
                        </div>
                      </div>
                      <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Keterangan  <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <textarea data-parsley-maxlength="255" type="text" id="uom_ket" name="uom_ket" class="form-control col-md-7 col-xs-12" placeholder="Keterangan" required="required"> <?php if (isset($informasi[0]['keterangan'])) {
                                                                                                                                                                                                  echo $informasi[0]['keterangan'];
                                                                                                                                                                                                } ?></textarea>
                        </div>
                      </div>
					   <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Gambar <span class="required"><sup>*</sup></span>
                        </label>
                        <div class="col-md-8 col-sm-6 col-xs-12">
							<input data-parsley-maxlength="255" type="file" id="gambar_informasi" name="gambar_informasi" class="form-control col-md-7 col-xs-12" placeholder="gambar_informasi">
							<input type="hidden" id="old_logo" name="old_logo" value="">
							<img  src="<?php echo base_url();?>uploads/informasi/<?php echo $informasi[0]['gambar']?>" alt="your image" width="200px" />
                          
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-lg-12">
                          <div class="payment-adress">
                            <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                            <button type="button" class="btn btn-danger waves-effect waves-light" onClick="back()">Batal</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="PrimaryModalalert" class="modal modal-edu-general default-popup-PrimaryModal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-close-area modal-close-df">
        <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
      </div>
      <div class="modal-body">
        <i class="educate-icon educate-checked modal-check-pro"></i>
        <h2>Data Berhasil Dirubah</h2>
        <p></p>
      </div>
      <div class="modal-footer">
        <a data-dismiss="modal" href="#" onClick="back()">OK</a>
      </div>
    </div>
  </div>
</div>

<script>
  function clearform() {

    $('#edit_uom').trigger("reset");

  }

  function back() {

    window.location.href = "<?php echo base_url() . 'management_informasi'; ?>";

  }

  function listdist() {
    var user_id = '0001';
    var token = '093940349';

  }

  $(document).ready(function() {

    listdist();

    $('#edit_uom').on('submit', function(e) {
      // validation code here
      //if(!valid) {
      e.preventDefault();

      var formData = new FormData(this);
	   var urls = $(this).attr('action');


swal({
      title: 'Yakin akan Merubah Data ?',
      text: '',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Ya',
      cancelButtonText: 'Tidak'
    }).then(function() {


      $.ajax({
        type: 'POST',
        url: urls,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
	if(response.success == true){
						//$('#ModalAddPo').modal('show');
						
						swal({
						  title: 'Success!',
						  text: "Data Informasi Berhasil Dirubah ",
						  type: 'success',
						  showCancelButton: false,
						  confirmButtonText: 'Ok'
						}).then(function () {
							//$("#add_material").form('reset');
							back();
						})
						
					}else{
						// $('#msg_err').html(response.message);
						// $('#ModalChangePo').modal('show');
						
						swal({
						  title: 'Data Inputan Tidak Sesuai !! ',
						  text: response.message,
						  type: 'error',
						  showCancelButton: true,
						  confirmButtonText: 'Ok'
						}).then(function() {
						
						
						});
						
					}
		  
        }
      });
    });
	
	});
	
	
  });
</script>
