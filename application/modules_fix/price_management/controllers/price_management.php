<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Price_Management extends MX_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('price_management/price_model');
    $this->load->library('log_activity');
    $this->load->library('formatnumbering');
	$this->load->library('priv');
  }

  /**
   * anti sql injection
   */
  public function Anti_sql_injection($string) {
    $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
    return $string;
  }

  public function index() {
	  
		$priv = $this->priv->get_priv();

		$data = array(
			'priv' => $priv
		);
	  
    $this->template->load('maintemplate', 'price_management/views/index', $data);
  }

  function lists() {
    $draw = ($this->input->get_post('draw') != FALSE) ? $draw = $this->input->get_post('draw') : 1;
    $length = ($this->input->get_post('length') != FALSE) ? $this->input->get_post('length') : 10;
    $start = ($this->input->get_post('start') != FALSE) ? $this->input->get_post('start') : 0;
    $order = $this->input->get_post('order');
    $order_dir = (!empty($order[0]['dir'])) ? $order[0]['dir'] : 'desc';
    $order_column = (!empty($order[0]['column'])) ? $order[0]['column'] : 1;

    $order_fields = array('', 'stock_code', 'stock_name');

    $search = $this->input->get_post('search');

    $search_val = (!empty($search['value'])) ? $search['value'] : null;

    $search_value = $this->Anti_sql_injection($search_val);

    // Build params for calling model
    $params['limit'] = (int) $length;
    $params['offset'] = (int) $start;
    $params['order_column'] = $order_fields[$order_column];
    $params['order_dir'] = $order_dir;
    $params['searchtxt'] = $search_value;

    $list = $this->price_model->lists($params);
	
	$priv = $this->priv->get_priv();

    $result["recordsTotal"] = $list['total'];
    $result["recordsFiltered"] = $list['total_filtered'];
    $result["draw"] = $draw;

    $data = array();
    //$i = 0;
    $i = $params['offset'];
    $username = $this->session->userdata['logged_in']['username'];
    foreach ($list['data'] as $k => $v) {
      $i = $i + 1;
      $button_edit = '<div class="btn-group" style="display:'.$priv['update'].'"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="editmaterial(\'' . $v['id_mat'] . '\')"><i class="fa fa-edit"></i></button></div>';
      $button_appr = '<div class="btn-group" style="display:'.$priv['update'].'"><button class="btn btn-success" type="button" data-toggle="tooltip" data-placement="top" title="Approve" onClick="approvematerial(\'' . $v['id_mat'] . '\')"><i class="fa fa-check"></i></button></div>';
      if ($v['status_harga'] == 0) {
        $status = 'Konfirmasi';
        $button = $button_edit . $button_appr;
      } else {
        $status = 'Setuju';
        $button = $button_edit;
      }
      array_push($data, array(
        $i,
        $v['stock_code'],
        $v['stock_name'],
        $this->formatnumbering->price($v['base_price']),
        $this->formatnumbering->price($v['buy_price']),
        $this->formatnumbering->price($v['top_price']),
        $this->formatnumbering->price($v['bottom_price']),
        $status,
        $button
      ));
    }

    $result["data"] = $data;

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function edit() {
    $data = array(
      'id' => $this->Anti_sql_injection($this->input->post('iditems', TRUE)),
    );
    $result = $this->price_model->get_material($data['id']);

    // $unit = $this->price_model->get_unit();?
    // $Gudang = $this->price_model->get_distributor();

    $data = array(
      'stok'         => $result,
      // 'unit' 				=> $unit,
      // 'Gudang' 			=> $Gudang
    );

    $this->template->load('maintemplate', 'price_management/views/edit_modal_view', $data);
  }

  public function approve() {
    $data   = file_get_contents("php://input");
    $params   = json_decode($data, true);
    $id_mat = $this->Anti_sql_injection($params['id']);
    // $list = $this->price_model->deletes($id_mat);
    $array_ph = $this->price_model->get_price_history(array('id_mat' => $id_mat));
    $data = array(
      'approval_date' => date('Y-m-d'),
      'id_approval'   => $this->session->userdata['logged_in']['user_id'],
      'status_harga'  => 1, //sementara saat approve di update jadi 1
      'id_mat'        => $id_mat,
      'id_ph'         => $array_ph[0]['id_ph']
    );
    $list = $this->price_model->update_status($data);
    $list = $this->price_model->update_price_app($data);
    if ($list > 0) {
      $this->log_activity->insert_activity('insert', 'Berhasil Approve Price dengan id : ' . $id_mat);
      $res = array('status' => 'success', 'message' => 'Data telah di approve');
    } else {
      $this->log_activity->insert_activity('insert', 'Gagal Approve Price  dengan id : ' . $id_mat);
      $res = array('status' => 'failed', 'message' => 'Data gagal  di approve');
    }

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }

  public function edit_material() {

    $this->form_validation->set_rules('id_mat', 'Unit', 'trim|required');
    $this->form_validation->set_rules('top_price', 'Top Price', 'trim|required');
    $this->form_validation->set_rules('bottom_price', 'Bottom Price', 'trim|required');


    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));

      $result = array('success' => false, 'message' => $msg);
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      $id_mat = $this->Anti_sql_injection($this->input->post('id_mat', TRUE));
      $data_price = $this->price_model->get_price(array('id_mat' => $id_mat));
      $data = array(
        'id_mat'         => $this->Anti_sql_injection($this->input->post('id_mat', TRUE)),
        'top_price'     => $this->Anti_sql_injection($this->input->post('top_price', TRUE)),
        'bottom_price'   => $this->Anti_sql_injection($this->input->post('bottom_price', TRUE)),
        'buy_price'   => $this->Anti_sql_injection($this->input->post('buy_price', TRUE)),
        'harga_pokok'   => $data_price[0]['base_price'],
        'create_date'   => date('Y-m-d'),
        'id_pengaju'    => $this->session->userdata['logged_in']['user_id'],
        'status_harga'  => 0
      );
      $result = $this->price_model->edit_price($data);
      $result = $result + $this->price_model->add_price_history($data);
      if (count($result) > 1) {
        $msg = 'Berhasil mengubah data price ke database';

        $this->log_activity->insert_activity('insert', 'Berhasil Update Price dengan id Material ' . $id_mat);
        $results = array(
          'success' => true,
          'message' => $msg
        );
      } else {
        $msg = 'Gagal mengubah data price ke database';

        $this->log_activity->insert_activity('insert', 'Gagal Update Price dengan id material = ' . $id_mat);
        $results = array(
          'success' => false,
          'message' => $msg
        );
      }

      $this->output->set_content_type('application/json')->set_output(json_encode($results), true);
    }
  }

  public function get_properties() {
    $id = $this->Anti_sql_injection($this->input->post('id', TRUE));
    $detail_prop = $this->price_model->detail_prop_full($id);

    $result = array(
      'success' => true,
      'message' => '',
      'data' => $detail_prop
    );

    $this->output->set_content_type('application/json')->set_output(json_encode($result, true));
  }

  public function add_material() {
    $this->form_validation->set_rules('kode_stok', 'Kode Stok', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('nama_stok', 'Nama Stok', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('desk_stok', 'Deskripsi', 'trim|required');
    $this->form_validation->set_rules('unit', 'Unit', 'trim|required');
    $this->form_validation->set_rules('qty', 'Unit Box', 'trim|required');
    $this->form_validation->set_rules('gudang', 'Distributor', 'trim|required');

    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));

      $result = array('success' => false, 'message' => $msg);
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      $user_id     = $this->session->userdata['logged_in']['user_id'];
      $kode_stok     = $this->Anti_sql_injection($this->input->post('kode_stok', TRUE));
      $nama_stok     = $this->Anti_sql_injection($this->input->post('nama_stok', TRUE));
      $desk_stok     = $this->Anti_sql_injection($this->input->post('desk_stok', TRUE));
      $unit       = $this->Anti_sql_injection($this->input->post('unit', TRUE));
      $qty       = $this->Anti_sql_injection($this->input->post('qty', TRUE));
      $gudang     = $this->Anti_sql_injection($this->input->post('gudang', TRUE));

      $data = array(
        'kode_stok'       => $kode_stok,
        'nama_stok'       => $nama_stok,
        'desk_stok'       => $desk_stok,
        'unit'            => $unit,
        'unit_box'         => $qty,
        'dist_id'         => $gudang,
      );

      $result = $this->price_model->new_add_material($data);
      if ($result['result'] > 0) {

        $msg = 'Berhasil menambahkan Itemk ke database.';
        $this->log_activity->insert_activity('insert', 'Berhasil Insert Item');
        $results = array('success' => true, 'message' => $msg);
      } else {
        $msg = 'Gagal menambahkan Item ke database.';
        $this->log_activity->insert_activity('insert', 'Gagal Insert Item');
        $results = array('success' => false, 'message' => $msg);
      }

      $this->output->set_content_type('application/json')->set_output(json_encode($results));
    }
  }
}
