
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
		<style>
		
		#carousel2 .carousel-caption {
			left:0;
			right:0;
			bottom:0;
			text-align:left;
			padding:10px;
			background:rgba(0,0,0,0.6);
			text-shadow:none;
		}

		#carousel2 .list-group {
			position:absolute;
			top:0;
			right:0;
		}
		#carousel2 .list-group-item {
			border-radius:0px;
			cursor:pointer;
		}
		#carousel2 .list-group .active {
			background-color:#eee;	
		}

		@media (min-width: 992px) { 
			#carousel2 {padding-right:33.3333%;}
			#carousel2 .carousel-controls {display:none;} 	
		}
		@media (max-width: 991px) { 
			.carousel-caption p,
			#carousel2 .list-group {display:none;} 
		}
		</style>
        <div class="analytics-sparkle-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="analytics-sparkle-line reso-mg-b-30">
                            <div class="analytics-content">
                                <h5>Total Penjualan</h5>
                                <h2>Rp <span class="counter"><?php echo number_format($total_penjualan[0]['total_penjualan'] ,2,',','.');?></span> <span class="tuition-fees"></span></h2>
                               
                                <div class="progress m-b-0">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;"> <span class="sr-only">20% Complete</span> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="analytics-sparkle-line reso-mg-b-30">
                            <div class="analytics-content">
                                <h5>Total Pembelian</h5>
                                <h2>Rp <span class="counter"><?php echo number_format($total_pembelian[0]['total_pembelian'] ,2,',','.');?></span> <span class="tuition-fees"></span></h2>
                               
                                <div class="progress m-b-0">
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;"> <span class="sr-only">230% Complete</span> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="analytics-sparkle-line reso-mg-b-30 table-mg-t-pro dk-res-t-pro-30">
                            <div class="analytics-content">
                                <h5>Penjualan MTD</h5>
                                <h2>Rp <span class="counter"><?php echo number_format($mtd_penjualan[0]['mtd_penjualan'] ,2,',','.');?></span> <span class="tuition-fees"></span></h2>
                                
                                <div class="progress m-b-0">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;"> <span class="sr-only">20% Complete</span> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                        <div class="analytics-sparkle-line table-mg-t-pro dk-res-t-pro-30">
                            <div class="analytics-content">
                                <h5>Pembelian MTD</h5>
                               <h2>Rp <span class="counter"><?php echo number_format($mtd_pembelian[0]['mtd_pembelian'] ,2,',','.');?></span> <span class="tuition-fees"></span></h2>
                               
                                <div class="progress m-b-0">
                                    <div class="progress-bar progress-bar-inverse" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:100%;"> <span class="sr-only">230% Complete</span> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="product-sales-area mg-tb-30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
						<div class="carousel slide" id="carousel2">
							<ol class="carousel-indicators">
								<?php foreach($informasi as $key=>$info) { ?> 
								<li data-target="#carousel2" data-slide-to="<?php echo $key; ?>" class="<?php echo ($key == 0) ? "active" : ""; ?>"></li>
								<?php } ?>
							</ol>
							<div class="carousel-inner" style="height: 500px; width:150% !important;">
								<?php foreach($informasi as $key=>$info) { ?> 
									<div class="item <?php echo ($key == 0) ? "active" : ""; ?> ">
										<center>
										<img  src="<?php echo base_url();?>uploads/informasi/<?php echo $info['gambar'];?>" height="100px"  alt="Image of every carousel"/>
										<div class="carousel-caption">
											<h4><a href="#"><?php echo $info['judul'];?></a></h4>
											<p><?php echo $info['keterangan'];?><a class="label label-primary" href="#" target="_blank"></a></p>
										</div>
										</center>
									</div>
									
								<?php } ?>
							</div>
							<a data-slide="prev" href="#carousel2" class="left carousel-control">
								<span class="icon-prev"></span>
							</a>
							<a data-slide="next" href="#carousel2" class="right carousel-control">
								<span class="icon-next"></span>
							</a>
						</div>
					</div>
                </div>
            </div>
        </div>
        
     
