<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}


	public function list_users($params = array()){
		
			$query = 	'
				SELECT COUNT(*) AS jumlah 
				FROM u_user a
					JOIN u_user_group b ON a.id=b.id_user
					JOIN u_group c ON b.id_role=c.id
					LEFT JOIN t_region d ON a.lokasi=d.id_t_region
				where 1 = 1
				AND ( 
					nama LIKE "%'.$params['searchtxt'].'%" OR
					username LIKE "%'.$params['searchtxt'].'%" OR
					region LIKE "%'.$params['searchtxt'].'%" 
				)
			';
						
			$query2 = 	'
				SELECT z.*, rank() over ( ORDER BY nama ASC) AS Rangking from ( 
					SELECT a.id as id_user,a.nama,b.username,a.status_akses,c.group,d.region , a.image 
					FROM u_user a
					JOIN u_user_group b ON a.id=b.id_user
					JOIN u_group c ON b.id_role=c.id
					LEFT JOIN t_region d ON a.lokasi=d.id_t_region 
					where 1=1 AND ( 
					nama LIKE "%'.$params['searchtxt'].'%" OR 
					username LIKE "%'.$params['searchtxt'].'%" OR
					region LIKE "%'.$params['searchtxt'].'%"
				)  order by nama) z
				ORDER BY nama ASC
				
				LIMIT '.$params['limit'].' 
				OFFSET '.$params['offset'].' 
			';
		
			//echo $query2;die;
		
			 $out		= array();
			  $querys		= $this->db->query($query);
			  $result = $querys->row();
			  
			  $total_filtered = $result->jumlah;
			  $total 			= $result->jumlah;
		  
				if(($params['offset']+10) > $total_filtered){
				$limit_data = $total_filtered - $params['offset'];
			  }else{
				$limit_data = $params['limit'] ;
			  }
		  
		
		  
		  //echo $query;die;
			//echo $query;die;
			 $query2s		= $this->db->query($query2);
		  $result2 = $query2s->result_array();						
		  $return = array(
			  'data' => $result2,
			  'total_filtered' => $total_filtered,
			  'total' => $total,
		  );
		  return $return;
		
		
	}
	
	public function get_wilayah($params = array()){
		
		$query = $this->db->get('t_region');
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}
	
	public function get_role($params = array()){
		
		$query = $this->db->get('u_group');
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function get_userdetail($params = array()){
		$this->db->select('u_user.*, u_user_group.id_role, u_user_group.username')
		#$this->db->get_where('m_material.*, m_uom.uom_symbol', array('dist_id' => $params['id_prin']))
        ->from('u_user')
        ->join('u_user_group', 'u_user.id = u_user_group.id_user')
		->where(array('u_user.id' => $params['iduser']));
		$query = $this->db->get();

		//$query = $this->db->get_where('m_material', array('dist_id' => $params['id_prin']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	
	public function check_username($params = array()){
		
		$query = $this->db->get_where('u_user_group', array('username' => $params['username']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}	
	
	public function check_username_update($params = array()){
		
		$query = $this->db->get_where('u_user_group', array('username' => $params['username'],'id_user != ' => $params['id']));
		
		$return = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $return;
	}

	public function add_users($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'nama' => $data['nama'],
			'alamat' => $data['alamat'],
			'nokontak' => $data['nokontak'],
			'lokasi' => $data['lokasi'],
			'email' => $data['email'],
			'image' => $data['image']
		);

		// print_r($datas);die;

		$this->db->insert('u_user',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_group($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			'id_user' => $data['id_user'],
			'id_role' => $data['id_role'],
			'password' => $data['password'],
			'username' => $data['username'],
			'created_by' => $data['create_by'],
			'created_at' => $data['create_at']
		);

	//	print_r($datas);die;

		$this->db->insert('u_user_group',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	
	public function update_users($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

	if($data['image'] == ''){
		$datas = array(
			'nama' => $data['nama'],
			'alamat' => $data['alamat'],
			'nokontak' => $data['nokontak'],
			'lokasi' => $data['lokasi'],
			'email' => $data['email']
		);
	}else{
		$datas = array(
			'nama' => $data['nama'],
			'alamat' => $data['alamat'],
			'nokontak' => $data['nokontak'],
			'lokasi' => $data['lokasi'],
			'email' => $data['email'],
			'image' => $data['image']
		);
	}

	//	print_r($datas);die;
		$this->db->where('id',$data['id']);
		$this->db->update('u_user',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}
	
	public function update_group($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		if($data['password'] == ''){
			$datas = array(
				'id_role' => $data['id_role'],
				'updated_at' => $data['updated_at'],
				'username' => $data['username']
			);
		}else{
			$datas = array(
				'id_role' => $data['id_role'],
				'updated_at' => $data['updated_at'],
				'username' => $data['username'],
				'password' => $data['password']
			);
		}

		

	//	print_r($datas);die;
		$this->db->where('id_user',$data['id_user']);
		$this->db->update('u_user_group',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function update_status($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			
			'status_credit' => $data['status_credit']
		);

	//	print_r($datas);die;
		$this->db->where('id_t_cust',$data['id_t_cust']);
		$this->db->update('t_customer',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function get_limit_history($params = array()){
		
		$query=$this->db->order_by('id_hl','desc')
						->get_where('t_history_limit', array('id_cust' => $params['id_cust']), 1,0);
	
		// $this->db->order_by('id_hl','asc');
		// $query=$this->db->get();
		$return = $query->result_array();

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();


		return $return;
	}


	public function update_limit_history($data) {
		// $sql 	= 'insert into t_eksternal  (kode_eksternal,eksternal_address,name_eksternal,phone_1,fax,bank1,rek1,bank2,rek2,bank3,rek3,type_eksternal) values (?,?,?,?,?,?,?,?,?,?,?,?)';

		$datas = array(
			
			'id_cust' => $data['id_cust'],
			'approval_date' => $data['approval_date']
		);

	//	print_r($datas);die;
		$this->db->where('id_hl',$data['id_hl']);
		$this->db->update('t_history_limit',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	

	function delete_user($id_t_cust) {
		
		//print_r($id);die;
		
		$this->db->where('id', $id_t_cust);
		$this->db->delete('u_user'); 
		 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$this->db->where('id_cust', $id_t_cust);
		$this->db->delete('t_history_limit');

		return $result;
	}	
	
	function delete_user_group($id_t_cust) {
		
		//print_r($id);die;
		
		$this->db->where('id_user', $id_t_cust);
		$this->db->delete('u_user_group'); 
		 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$this->db->where('id_cust', $id_t_cust);
		$this->db->delete('t_history_limit');

		return $result;
	}

}