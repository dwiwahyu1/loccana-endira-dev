<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Uom_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
	}
    
    public function get_uom_data($id)
	{
		//$sql 	= 'CALL uom_search_id(?)';
		$sql 	= '
		select a.*,b.coa,b.keterangan,c.id_external FROM t_coa_value a 
		join t_coa b on a.id_coa = b.id_coa
		left join t_jurnal_trans c on a.id = c.coa_value
		where a.id = ?';

		$out = array();
		$query 	=  $this->db->query($sql,
			array(
				$id
			));

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}    
	
    public function get_principle()
	{
		//$sql 	= 'CALL uom_search_id(?)';
		$sql 	= 'select * from t_customer order by code_cust';

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}

	public function get_coa(){
		
		$sql 	= 'select * from t_coa where type_coa in (1,2) order by coa';

		$out = array();
		$query 	=  $this->db->query($sql);

		$result = $query->result_array();

		$this->db->close();
		$this->db->initialize();

		return $result;
		
	}

	public function lists($params = array())
	{
			$query = 	'
				SELECT COUNT(*) AS jumlah FROM t_coa_value a join t_coa b on a.id_coa = b.id_coa where 1 = 1
				 AND type_coa in (1,2)
				AND ( 
					coa LIKE "%'.$params['searchtxt'].'%" OR
					keterangan LIKE "%'.$params['searchtxt'].'%" 
				)
			';
						
			$query2 = 	'
				SELECT z.*, rank() over ( ORDER BY sss ) AS Rangking from ( 
					select a.*,b.coa,b.keterangan,b.id_coa as sss 
					FROM t_coa_value a join t_coa b on a.id_coa = b.id_coa
					where 1 = 1 AND type_coa in (1,2)  AND ( 
					coa LIKE "%'.$params['searchtxt'].'%" OR
					keterangan LIKE "%'.$params['searchtxt'].'%" 
				)  order by sss) z
				ORDER BY `sss` ASC
				
				LIMIT '.$params['limit'].' 
				OFFSET '.$params['offset'].' 
			';
		
			//echo $query2;die;
		
			 $out		= array();
			  $querys		= $this->db->query($query);
			  $result = $querys->row();
			  
			  $total_filtered = $result->jumlah;
			  $total 			= $result->jumlah;
		  
				if(($params['offset']+10) > $total_filtered){
				$limit_data = $total_filtered - $params['offset'];
			  }else{
				$limit_data = $params['limit'] ;
			  }
		  
		
		  
		  //echo $query;die;
			//echo $query;die;
			 $query2s		= $this->db->query($query2);
		  $result2 = $query2s->result_array();						
		  $return = array(
			  'data' => $result2,
			  'total_filtered' => $total_filtered,
			  'total' => $total,
		  );
		  return $return;
	}
	
	public function edit_uom($data)
	{
		$datas = array(
			'id_coa' => $data['coa'],
			'date' => $data['tgl'],
			'date_insert' => date('Y-m-d H:i:s'),
			'value' => $data['jumlah'],
			'value_real' => $data['jumlah'],
			'type_cash' => 1,
			'id_valas' => 1,
			'note' => $data['ket']
		);

	//	print_r($datas);die;
		$this->db->where('id',$data['id']);
		$this->db->update('t_coa_value',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	public function add_uom($data)
	{
		$datas = array(
			'id_coa' => $data['coa'],
			'date' => $data['tgl'],
			'date_insert' => date('Y-m-d H:i:s'),
			'value' => $data['jumlah'],
			'value_real' => $data['jumlah'],
			'type_cash' => 1,
			'id_valas' => 1,
			'note' => $data['ket']
		);

	//	print_r($datas);die;

		$this->db->insert('t_coa_value',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}	
	
	public function add_trans($data,$id_coa_val)
	{
		$datas = array(
			'id_external' => $data['prin'],
			'tipe_trans' => 0,
			'ammount' => $data['jumlah'],
			'pph' => $data['pph'],
			'tgl_terbit' => $data['tgl'],
			'coa_value' => $id_coa_val
		);

	//	print_r($datas);die;

		$this->db->insert('t_jurnal_trans',$datas);

			
		//$query 	= $this->db->query($sql,$data);
		
		$query = $this->db->query('SELECT LAST_INSERT_ID()');
		$row = $query->row_array();
		$lastid = $row['LAST_INSERT_ID()'];

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		$arr_result['lastid'] = $lastid;
		$arr_result['result'] = $result;

		return $arr_result;
	}

	
	public function deletes($id_uom)
	{
		$this->db->where('id', $id_uom);
		$this->db->delete('t_coa_value'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}
	
		
	public function delete_uom_trans($data)
	{
		$this->db->where('coa_value', $data['id']);
		$this->db->delete('t_jurnal_trans'); 

		$result	= $this->db->affected_rows();

		$this->db->close();
		$this->db->initialize();

		return $result;
	}


}
