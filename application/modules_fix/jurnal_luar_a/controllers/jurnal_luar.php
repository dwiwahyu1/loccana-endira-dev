<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class controller untuk Users
 *
 * @author 		Rizal Haibar
 * @email		rizalhaibar.rh@gmail.com
 * @copyright	2017
 *
 */
class Jurnal_luar extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('jurnal_luar/uom_model');
	$this->load->library('log_activity');
		$this->load->library('priv');
  }

  /**
   * anti sql injection
   */
  public function Anti_sql_injection($string)
  {
    $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
    return $string;
  }

  public function index()
  {
	  $priv = $this->priv->get_priv();

		$data = array(
			'priv' => $priv
		);
	  
    $this->template->load('maintemplate', 'jurnal_luar/views/index',$data);
  }

  function lists()
  {

    if( !empty($_GET['sess_user_id']) ) {
			  $sess_user_id = $_GET['sess_user_id'];
		  } else {
			  $sess_user_id = NULL;
		  }
		  
		   if( !empty($_GET['sess_token']) ) {
			  $sess_token = $_GET['sess_token'];
		  } else {
			  $sess_token = NULL;
		  }
		  
		    if( $this->input->get_post('draw') != FALSE )   {$draw   = $this->input->get_post('draw');}   else{$draw   = 1;}; 
		  if( $this->input->get_post('length') != FALSE ) {$length = $this->input->get_post('length');} else{$length = 10;}; 
		  if( $this->input->get_post('start') != FALSE )  {$start  = $this->input->get_post('start');}  else{$start  = 0;}; 				
		  $order_fields = array('coa'); // , 'COST'
		  $order = $this->input->get_post('order');
		  if( ! empty($order[0]['dir']))    {$order_dir    = $order[0]['dir'];}    else{$order_dir    = 'desc';}; 
		  if( ! empty($order[0]['column'])) {$order_column = $order[0]['column'];} else{$order_column = 0;}; 	
		  
		    $params['limit'] 		= (int) $length;
			$params['offset'] 		= (int) $start;
			$params['order_column'] = $order_fields[$order_column];
			$params['order_dir'] 	= $order_dir;
			$params['sess_user_id'] = $sess_user_id;
			$params['sess_token'] 	= $sess_token;
			$params['searchtxt'] 	= $_GET['search']['value'];
		  

    //print_r($params);die;

    $list = $this->uom_model->lists($params);
	$priv = $this->priv->get_priv();
    //print_r($list);die;

    $result["recordsTotal"] = $list['total'];
    $result["recordsFiltered"] = $list['total_filtered'];
    $result["draw"] = $draw;

    //print_r($result);die;

    $data = array();
    $i = $params['offset'];
    $username = $this->session->userdata['logged_in']['username'];
    foreach ($list['data'] as $k => $v) {
      $i = $i + 1;


      $status_akses = '
                <div class="btn-group" style="display:'.$priv['update'].'"><button class="btn btn-warning" type="button" data-toggle="tooltip" data-placement="top" title="Edit" onClick="edituom(\'' . $v['id'] . '\')"><i class="fa fa-edit"></i></button></div>
				  <div class="btn-group" style="display:'.$priv['delete'].'"><button class="btn btn-danger" type="button" data-toggle="tooltip" data-placement="top" title="Delete" onClick="deleteuom(\'' . $v['id'] . '\')"><i class="fa fa-trash"></i></button></div>';
				  
				  if($v['type_cash'] == 0){
					  $sss = 'Pemasukan';
				  }else{
					  $sss = 'Pengeluaran';
				  }

      array_push($data, array(
        $i,
        $v['coa'].'-'.$v['keterangan'],
		$v['date'],
        number_format($v['value_real'],2,',','.'),
        $v['note'],
        $status_akses
      ));
    }

    $result["data"] = $data;

    $this->output->set_content_type('application/json')->set_output(json_encode($result));
  }

  public function add()
  {
	  
	  
    $coa = $this->uom_model->get_coa();
    //$coa_bayar = $this->uom_model->get_cash_account_pay();
    $principle = $this->uom_model->get_principle();



    // $this->load->view('add_modal_view', $data);

    // $result = $this->distributor_model->location();

    $data = array(
      'group' => '',
      'coa' => $coa,
    //  'coa_bayar' => $coa_bayar,
	  'principle' => $principle
    );

    $this->template->load('maintemplate', 'jurnal_luar/views/add_modal_view', $data);
  }

  public function edit()
  {
    //$id = $this->Anti_sql_injection($this->input->post('sid', TRUE));
		$data = array(
			'id' => $this->Anti_sql_injection($this->input->post('iduom', TRUE)),
		);
    $result = $this->uom_model->get_uom_data($this->Anti_sql_injection($data['id']));
	 $coa = $this->uom_model->get_coa();
	 $principle = $this->uom_model->get_principle();

    //print_r($result);die;
    // $roles = $this->uom_model->roles($id);

    $data = array(
      'uom' => $result[0],
      'id' => $this->Anti_sql_injection($this->input->post('iduom', TRUE)),
	   'principle' => $principle,
	  'coa' => $coa
    );

    $this->template->load('maintemplate', 'jurnal_luar/views/edit_modal_view', $data);
  }

  public function deletes()
  {

    $data   = file_get_contents("php://input");
    $params   = json_decode($data, true);

    $list = $this->uom_model->deletes($this->Anti_sql_injection($params['id']));

    $res = array(
      'status' => 'success',
      'message' => 'Data telah di hapus'
    );

    header('Cache-Control: no-cache, must-revalidate');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Content-type: application/json');
    header("access-control-allow-origin: *");
    echo json_encode($res);
  }

  public function edit_uom()
  {
	$this->form_validation->set_rules('coa', 'coa', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('tgl', 'tgl', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('cash', 'cash', 'trim|required|max_length[255]');
    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));
      $result = array(
        'success' => false,
        'message' => $msg
      );
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      // die;
      $message = "";
	  
	  if($this->Anti_sql_injection($this->input->post('prin', TRUE)) == '0'  ){
	  
	  
      $data = array(
       'user_id' => $this->session->userdata['logged_in']['user_id'],
        'coa'     => $this->Anti_sql_injection($this->input->post('coa', TRUE)),
        'id'     => $this->Anti_sql_injection($this->input->post('id', TRUE)),
        'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
        'cash' => $this->Anti_sql_injection($this->input->post('cash', TRUE)),
        'jumlah' => $this->Anti_sql_injection($this->input->post('jumlah', TRUE)),
        'ket' => $this->Anti_sql_injection($this->input->post('ket', TRUE))
      );
      $result = $this->uom_model->edit_uom($data);
	  
	  $result = $this->uom_model->edit_uom($data);
	  $result2 = $this->uom_model->delete_uom_trans($data);
	  
	}else{
		
		  $data = array(
       'user_id' => $this->session->userdata['logged_in']['user_id'],
        'coa'     => $this->Anti_sql_injection($this->input->post('coa', TRUE)),
        'id'     => $this->Anti_sql_injection($this->input->post('id', TRUE)),
        'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
        'cash' => $this->Anti_sql_injection($this->input->post('cash', TRUE)),
        'jumlah' => $this->Anti_sql_injection($this->input->post('jumlah', TRUE)),
		'pph' => 10,
		'ppn' => ($this->Anti_sql_injection($this->input->post('jumlah', TRUE))*0.1),
		'prin' => $this->Anti_sql_injection($this->input->post('prin', TRUE)),
        'ket' => $this->Anti_sql_injection($this->input->post('ket', TRUE))
      );
		
		  $result = $this->uom_model->edit_uom($data);
		   $result3 = $this->uom_model->delete_uom_trans($data);
		   $result2 = $this->uom_model->add_trans($data,$data['id']);
		
	}
	
	
      if ($result > 0) {
        $msg = 'Berhasil merubah data.';

        $result = array(
          'success' => true,
          'message' => $msg
        );
      } else {
        $msg = 'Gagal merubah data.';

        $result = array(
          'success' => false,
          'message' => $msg
        );
      }

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
  }

  public function add_uom()
  {
    $this->form_validation->set_rules('coa', 'coa', 'trim|required|max_length[255]');
    $this->form_validation->set_rules('tgl', 'tgl', 'trim|required|max_length[255]');
    //$this->form_validation->set_rules('cash', 'cash', 'trim|required|max_length[255]');
    if ($this->form_validation->run() == FALSE) {
      $pesan = validation_errors();
      $msg = strip_tags(str_replace("\n", '', $pesan));
      $result = array(
        'success' => false,
        'message' => $msg
      );

      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } else {
      // die;
      $message = "";
	  
	//  echo  $this->Anti_sql_injection($this->input->post('prin', TRUE));die;
	  
	  if($this->Anti_sql_injection($this->input->post('prin', TRUE)) == '0'  ){
		  
		  $data = array(
			'user_id' => $this->session->userdata['logged_in']['user_id'],
			'coa'     => $this->Anti_sql_injection($this->input->post('coa', TRUE)),
			'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
			'prin' => $this->Anti_sql_injection($this->input->post('prin', TRUE)),
			'jumlah' => $this->Anti_sql_injection($this->input->post('jumlah', TRUE)),
			'pph' => 10,
			'ket' => $this->Anti_sql_injection($this->input->post('ket', TRUE))
		  );
		  
		  $result = $this->uom_model->add_uom($data);

		}else{
		  $data = array(
			'user_id' => $this->session->userdata['logged_in']['user_id'],
			'coa'     => $this->Anti_sql_injection($this->input->post('coa', TRUE)),
			'tgl' => $this->Anti_sql_injection($this->input->post('tgl', TRUE)),
			'prin' => $this->Anti_sql_injection($this->input->post('prin', TRUE)),
			'jumlah' => $this->Anti_sql_injection($this->input->post('jumlah', TRUE)),
			//'jumlah' => $this->Anti_sql_injection($this->input->post('jumlah', TRUE))-($this->Anti_sql_injection($this->input->post('jumlah', TRUE))*0.1),
			'pph' => 10,
			'ppn' => ($this->Anti_sql_injection($this->input->post('jumlah', TRUE))*0.1),
			'ket' => $this->Anti_sql_injection($this->input->post('ket', TRUE))
		  );
		   $result = $this->uom_model->add_uom($data);
		    $result2 = $this->uom_model->add_trans($data,$result['lastid']);
	  }
		//print_r($data);die;

		//$result = $this->uom_model->add_uom($data);
      
	 
	  
      if ($result > 0) {
        $msg = 'Berhasil menambahkan cash .';

        $result = array(
          'success' => true,
          'message' => $msg
        );
      } else {
        $msg = 'Gagal menambahkan cash ke database.';
        $result = array(
          'success' => false,
          'message' => $msg
        );
      }
      $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }
  }
}
