<?php

/**
 * This function is used to return js version
 * @return String
 */
function jsversionstring()
{
    $jsversionstring = "25-Jul-2019 11:37:00 ";

    return $jsversionstring;
}

/**
 * This function is used to generate a random string
 * @return String
 */
function randomString()
{
    $alpha = "abcdefghijklmnopqrstuvwxyz";
    $alpha_upper = strtoupper($alpha);
    $numeric = "0123456789";
    $chars = $alpha . $alpha_upper . $numeric;
    $chars = str_shuffle($chars);
    $pw = substr($chars, 8, 8);
    return $pw;
}

/**
 * Anti sql injection string filter
 * @param string
 * @return string
 */
function anti_sql_injection($string)
{
    $string = strip_tags(trim(addslashes(htmlspecialchars(stripslashes($string)))));
    return $string;
}

/**
 * Format the float (or integer) value into indonesian currency format
 * 
 * @param float value
 * @return string
 */
function indonesia_currency_format($value)
{
    $value = number_format($value, 2, ',', '.');
    return $value;
}
